#! /bin/sh

echo "export PATH=\""$(composer global config bin-dir --absolute)":\${PATH}\"" >> ~/.bashrc

composer global require php-stubs/generator

composer install --dev
git config --global --add safe.directory /workspace
