<?php

namespace PrestaShop\PrestaShop\Adapter\Currency;

/**
 * Class CurrencyManager is responsible for dealing with currency data using legacy classes.
 */
class CurrencyManager
{
    /**
     * Updates currency data after default currency has changed.
     */
    public function updateDefaultCurrency()
    {
    }
}
