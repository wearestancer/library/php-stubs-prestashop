<?php

namespace PrestaShop\PrestaShop\Adapter\Feature;

/**
 * This class manages Feature feature.
 */
class FeatureFeature implements \PrestaShop\PrestaShop\Core\Feature\FeatureInterface
{
    public function __construct(\PrestaShop\PrestaShop\Adapter\Configuration $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isUsed()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isActive()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function enable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function disable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($status)
    {
    }
}
