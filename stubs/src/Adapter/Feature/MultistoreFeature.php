<?php

namespace PrestaShop\PrestaShop\Adapter\Feature;

/**
 * Class MultistoreFeature provides data about multishop feature usage.
 *
 * @internal
 */
final class MultistoreFeature implements \PrestaShop\PrestaShop\Core\Feature\FeatureInterface
{
    /**
     * @param ConfigurationInterface $configuration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\ConfigurationInterface $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isUsed()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isActive()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function enable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function disable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($status)
    {
    }
}
