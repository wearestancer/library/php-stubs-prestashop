<?php

namespace PrestaShop\PrestaShop\Adapter;

/**
 * @deprecated since 1.7.4.0
 * @see \PrestaShop\PrestaShop\Adapter\Presenter\Object\ObjectPresenter
 */
class ObjectPresenter extends \PrestaShop\PrestaShop\Adapter\Presenter\Object\ObjectPresenter
{
}
