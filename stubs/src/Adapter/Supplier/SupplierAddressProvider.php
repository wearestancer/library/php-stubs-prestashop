<?php

namespace PrestaShop\PrestaShop\Adapter\Supplier;

/**
 * Class SupplierAddressProvider is responsible for supplier address data retrieval.
 */
class SupplierAddressProvider
{
    /**
     * Gets address id by supplier
     *
     * @param int $supplierId
     *
     * @return int
     */
    public function getIdBySupplier($supplierId)
    {
    }
}
