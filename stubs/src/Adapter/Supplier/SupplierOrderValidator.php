<?php

namespace PrestaShop\PrestaShop\Adapter\Supplier;

/**
 * Class SupplierOrderValidator is responsible for handling supplier and its corresponding order validity.
 */
class SupplierOrderValidator
{
    /**
     * Checks if the given supplier has pending orders.
     *
     * @param int $supplierId
     *
     * @return bool
     */
    public function hasPendingOrders($supplierId)
    {
    }
}
