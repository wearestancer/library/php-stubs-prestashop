<?php

namespace PrestaShop\PrestaShop\Adapter\Assets;

trait AssetUrlGeneratorTrait
{
    /**
     * @var string
     */
    protected $fqdn;
    /**
     * @param string $fullPath
     *
     * @return string
     */
    protected function getUriFromPath($fullPath)
    {
    }
    /**
     * @param string $fullUri
     *
     * @return string
     */
    protected function getPathFromUri($fullUri)
    {
    }
    /**
     * @return string
     */
    protected function getFQDN()
    {
    }
}
