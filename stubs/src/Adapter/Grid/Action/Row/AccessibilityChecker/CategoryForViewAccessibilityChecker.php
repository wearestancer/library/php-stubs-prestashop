<?php

namespace PrestaShop\PrestaShop\Adapter\Grid\Action\Row\AccessibilityChecker;

/**
 * Class CategoryForViewAccessibilityChecker.
 *
 * @internal
 */
final class CategoryForViewAccessibilityChecker implements \PrestaShop\PrestaShop\Core\Grid\Action\Row\AccessibilityChecker\AccessibilityCheckerInterface
{
    /**
     * @param int $contextLangId
     */
    public function __construct($contextLangId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isGranted(array $category)
    {
    }
}
