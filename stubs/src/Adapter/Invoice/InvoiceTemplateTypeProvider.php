<?php

namespace PrestaShop\PrestaShop\Adapter\Invoice;

/**
 * Class InvoiceTemplateTypeProvider provides invoice PDF template type.
 */
final class InvoiceTemplateTypeProvider implements \PrestaShop\PrestaShop\Core\PDF\PDFTemplateTypeProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getPDFTemplateType()
    {
    }
}
