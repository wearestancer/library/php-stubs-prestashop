<?php

namespace PrestaShop\PrestaShop\Adapter\Image;

class ImagePathFactory
{
    /**
     * @param string $pathToBaseDir
     */
    public function __construct(string $pathToBaseDir)
    {
    }
    /**
     * @param int $entityId
     *
     * @return string
     */
    public function getPath(int $entityId) : string
    {
    }
}
