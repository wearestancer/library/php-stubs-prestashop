<?php

namespace PrestaShop\PrestaShop\Adapter\Twig;

/**
 * Provides helper functions in Twig for formatting data using context locale
 */
final class LocaleExtension extends \PrestaShopBundle\Twig\LocaleExtension
{
}
