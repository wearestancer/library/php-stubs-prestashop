<?php

namespace PrestaShop\PrestaShop\Adapter\Product\Combination\Validate;

/**
 * Validates Combination properties using legacy object model
 */
class CombinationValidator extends \PrestaShop\PrestaShop\Adapter\AbstractObjectModelValidator
{
    /**
     * @param Combination $combination
     */
    public function validate(\Combination $combination) : void
    {
    }
}
