<?php

namespace PrestaShop\PrestaShop\Adapter\Product\Update;

/**
 * Updates product indexation
 */
class ProductIndexationUpdater
{
    public function __construct(\PrestaShop\PrestaShop\Adapter\ContextStateManager $contextStateManager, bool $isSearchIndexationOn)
    {
    }
    /**
     * @param Product $product
     *
     * @return bool
     */
    public function isVisibleOnSearch(\Product $product) : bool
    {
    }
    /**
     * @param Product $product
     *
     * @throws CannotUpdateProductException
     * @throws CoreException
     */
    public function updateIndexation(\Product $product, \PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint $shopConstraint) : void
    {
    }
}
