<?php

namespace PrestaShop\PrestaShop\Adapter\Product\Stock\Validate;

/**
 * Validates StockAvailable legacy object model
 */
class StockAvailableValidator extends \PrestaShop\PrestaShop\Adapter\AbstractObjectModelValidator
{
    /**
     * @param StockAvailable $stockAvailable
     *
     * @throws CoreException
     */
    public function validate(\StockAvailable $stockAvailable) : void
    {
    }
}
