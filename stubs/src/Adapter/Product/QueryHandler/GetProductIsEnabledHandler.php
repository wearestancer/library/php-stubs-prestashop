<?php

namespace PrestaShop\PrestaShop\Adapter\Product\QueryHandler;

/**
 * @internal
 */
final class GetProductIsEnabledHandler implements \PrestaShop\PrestaShop\Core\Domain\Product\QueryHandler\GetProductIsEnabledHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Product\Query\GetProductIsEnabled $query)
    {
    }
}
