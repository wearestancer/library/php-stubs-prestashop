<?php

namespace PrestaShop\PrestaShop\Adapter\Product;

/**
 * @deprecated since 8.1 and will be removed in next major.
 *
 * This class will provide data from DB / ORM about attachment.
 */
class AttachmentDataProvider
{
    /**
     * Get all attachments.
     *
     * @param int $id_lang
     *
     * @return array Attachment
     */
    public function getAllAttachments($id_lang)
    {
    }
}
