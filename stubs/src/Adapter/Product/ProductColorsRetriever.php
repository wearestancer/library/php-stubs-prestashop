<?php

namespace PrestaShop\PrestaShop\Adapter\Product;

/**
 * Retrieve colors of a Product, if any.
 */
class ProductColorsRetriever
{
    /**
     * @param int $id_product
     *
     * @return mixed|null
     */
    public function getColoredVariants($id_product)
    {
    }
}
