<?php

namespace PrestaShop\PrestaShop\Adapter\Product\Customization\Validate;

/**
 * Validates CustomizationField field using legacy object model
 */
class CustomizationFieldValidator extends \PrestaShop\PrestaShop\Adapter\AbstractObjectModelValidator
{
    /**
     * @param CustomizationField $customizationField
     *
     * @throws CoreException
     */
    public function validate(\CustomizationField $customizationField) : void
    {
    }
}
