<?php

namespace PrestaShop\PrestaShop\Adapter\Localization;

/**
 * Class AdvancedConfiguration is responsible for 'Improve > International > Localization' page
 * 'Advanced' form data.
 */
class AdvancedConfiguration implements \PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface
{
    /**
     * @param Configuration $configuration
     */
    public function __construct(\PrestaShop\PrestaShop\Adapter\Configuration $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateConfiguration(array $config)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateConfiguration(array $config)
    {
    }
}
