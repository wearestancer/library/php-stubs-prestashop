<?php

namespace PrestaShop\PrestaShop\Adapter\Address;

class AddressFormatter implements \PrestaShop\PrestaShop\Core\Address\AddressFormatterInterface
{
    /**
     * @param AddressId $addressId
     *
     * @return string
     */
    public function format(\PrestaShop\PrestaShop\Core\Domain\Address\ValueObject\AddressId $addressId) : string
    {
    }
}
