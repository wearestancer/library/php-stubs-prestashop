<?php

namespace PrestaShop\PrestaShop\Adapter\Address\CommandHandler;

final class AddCustomerAddressHandler extends \PrestaShop\PrestaShop\Adapter\Address\AbstractAddressHandler implements \PrestaShop\PrestaShop\Core\Domain\Address\CommandHandler\AddCustomerAddressHandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws AddressException
     * @throws AddressConstraintException
     * @throws CannotAddAddressException
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Address\Command\AddCustomerAddressCommand $command) : \PrestaShop\PrestaShop\Core\Domain\Address\ValueObject\AddressId
    {
    }
}
