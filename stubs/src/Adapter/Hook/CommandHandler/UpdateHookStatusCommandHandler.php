<?php

namespace PrestaShop\PrestaShop\Adapter\Hook\CommandHandler;

/**
 * @internal
 */
class UpdateHookStatusCommandHandler implements \PrestaShop\PrestaShop\Core\Domain\Hook\CommandHandler\UpdateHookStatusCommandHandlerInterface
{
    /**
     * @param UpdateHookStatusCommand $command
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Hook\Command\UpdateHookStatusCommand $command)
    {
    }
}
