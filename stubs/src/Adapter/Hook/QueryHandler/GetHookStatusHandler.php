<?php

namespace PrestaShop\PrestaShop\Adapter\Hook\QueryHandler;

/**
 * @internal
 */
final class GetHookStatusHandler implements \PrestaShop\PrestaShop\Core\Domain\Hook\QueryHandler\GetHookStatusHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Hook\Query\GetHookStatus $query)
    {
    }
}
