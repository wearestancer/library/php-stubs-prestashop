<?php

namespace PrestaShop\PrestaShop\Adapter\EntityTranslation\Exception;

class DataLangClassNameNotFoundException extends \RuntimeException
{
}
