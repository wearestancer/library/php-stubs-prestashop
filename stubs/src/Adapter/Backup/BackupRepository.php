<?php

namespace PrestaShop\PrestaShop\Adapter\Backup;

/**
 * Class BackupRepository is responsible for providing available backups.
 *
 * @internal
 */
final class BackupRepository implements \PrestaShop\PrestaShop\Core\Backup\Repository\BackupRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function retrieveBackups()
    {
    }
}
