<?php

namespace PrestaShop\PrestaShop\Adapter\Backup;

/**
 * Class DatabaseBackupCreator is responsible for creating database backups.
 *
 * @internal
 */
final class DatabaseBackupCreator implements \PrestaShop\PrestaShop\Core\Backup\Manager\BackupCreatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function createBackup()
    {
    }
}
