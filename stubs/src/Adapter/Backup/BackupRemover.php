<?php

namespace PrestaShop\PrestaShop\Adapter\Backup;

/**
 * Class BackupRemover deletes given backup.
 *
 * @internal
 */
final class BackupRemover implements \PrestaShop\PrestaShop\Core\Backup\Manager\BackupRemoverInterface
{
    /**
     * {@inheritdoc}
     */
    public function remove(\PrestaShop\PrestaShop\Core\Backup\BackupInterface $backup)
    {
    }
}
