<?php

namespace PrestaShop\PrestaShop\Adapter\Shop;

/**
 * This class loads and saves data configuration for the Maintenance page.
 */
class MaintenanceConfiguration extends \PrestaShop\PrestaShop\Core\Configuration\AbstractMultistoreConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateConfiguration(array $configurationInputValues)
    {
    }
    /**
     * @return OptionsResolver
     */
    protected function buildResolver() : \Symfony\Component\OptionsResolver\OptionsResolver
    {
    }
}
