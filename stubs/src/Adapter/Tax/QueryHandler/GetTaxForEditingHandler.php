<?php

namespace PrestaShop\PrestaShop\Adapter\Tax\QueryHandler;

/**
 * Handles query which gets tax for editing
 */
final class GetTaxForEditingHandler extends \PrestaShop\PrestaShop\Adapter\Tax\AbstractTaxHandler implements \PrestaShop\PrestaShop\Core\Domain\Tax\QueryHandler\GetTaxForEditingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Tax\Query\GetTaxForEditing $query)
    {
    }
}
