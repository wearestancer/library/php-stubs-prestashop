<?php

namespace PrestaShop\PrestaShop\Adapter\Tax\Ecotax;

/**
 * Resets ecotax for products using legacy object model
 */
final class ProductEcotaxResetter implements \PrestaShop\PrestaShop\Core\Tax\Ecotax\ProductEcotaxResetterInterface
{
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
