<?php

namespace PrestaShop\PrestaShop\Adapter;

/**
 * Not used in PrestaShop.
 *
 * @deprecated since 1.7.5, to be removed in 1.8
 */
class ClassLang
{
    /**
     * ClassLang constructor.
     *
     * @param string $locale
     */
    public function __construct($locale)
    {
    }
    /**
     * @param string $className
     *
     * @return bool|object
     */
    public function getClassLang($className)
    {
    }
}
