<?php

namespace PrestaShop\PrestaShop\Adapter\Meta;

/**
 * Class MetaEraser is responsible for removing data from meta entity.
 */
final class MetaEraser
{
    /**
     * Erases data from meta entity.
     *
     * @param array $metaIds
     *
     * @return array
     *
     * @throws PrestaShopException
     */
    public function erase(array $metaIds)
    {
    }
}
