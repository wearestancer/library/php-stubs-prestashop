<?php

namespace PrestaShop\PrestaShop\Adapter\Meta;

/**
 * Class MetaDataProvider is responsible for providing data related with meta entity.
 */
class MetaDataProvider implements \PrestaShop\PrestaShop\Core\Meta\MetaDataProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getIdByPage($pageName)
    {
    }
    /**
     * @return array
     */
    public function getAvailablePages()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultMetaPageNameById($metaId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getModuleMetaPageNameById($metaId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultMetaPageNamesExcludingFilled()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNotConfiguredModuleMetaPageNames()
    {
    }
}
