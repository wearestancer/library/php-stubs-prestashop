<?php

namespace PrestaShop\PrestaShop\Adapter\Meta\QueryHandler;

/**
 * Class GetMetaForEditingHandler is responsible for retrieving meta data.
 *
 * @internal
 */
final class GetMetaForEditingHandler implements \PrestaShop\PrestaShop\Core\Domain\Meta\QueryHandler\GetMetaForEditingHandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws MetaNotFoundException
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Meta\Query\GetMetaForEditing $query)
    {
    }
}
