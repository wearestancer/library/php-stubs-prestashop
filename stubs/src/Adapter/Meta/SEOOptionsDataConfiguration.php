<?php

namespace PrestaShop\PrestaShop\Adapter\Meta;

class SEOOptionsDataConfiguration extends \PrestaShop\PrestaShop\Core\Configuration\AbstractMultistoreConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateConfiguration(array $configuration)
    {
    }
    /**
     * @return OptionsResolver
     */
    protected function buildResolver() : \Symfony\Component\OptionsResolver\OptionsResolver
    {
    }
}
