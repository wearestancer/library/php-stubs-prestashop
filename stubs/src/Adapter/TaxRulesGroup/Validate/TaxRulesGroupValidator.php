<?php

namespace PrestaShop\PrestaShop\Adapter\TaxRulesGroup\Validate;

/**
 * Validates TaxRulesGroup properties using legacy object model validation
 */
class TaxRulesGroupValidator extends \PrestaShop\PrestaShop\Adapter\AbstractObjectModelValidator
{
    /**
     * @param TaxRulesGroup $taxRulesGroup
     */
    public function validate(\TaxRulesGroup $taxRulesGroup) : void
    {
    }
}
