<?php

namespace PrestaShop\PrestaShop\Adapter;

/**
 * Class CacheManager drives the cache behavior.
 *
 * Features to drive the legacy cache from new code architecture.
 */
class CacheManager
{
    /**
     * Cleans the cache for specific cache key.
     *
     * @param string $key
     */
    public function clean($key)
    {
    }
}
