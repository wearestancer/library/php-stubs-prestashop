<?php

namespace PrestaShop\PrestaShop\Adapter\Employee;

/**
 * Class ContextEmployeeProvider provides context employee data.
 */
final class ContextEmployeeProvider implements \PrestaShop\PrestaShop\Core\Employee\ContextEmployeeProviderInterface
{
    /**
     * @param Employee $contextEmployee
     */
    public function __construct(\Employee $contextEmployee)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isSuperAdmin()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLanguageId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProfileId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
}
