<?php

namespace PrestaShop\PrestaShop\Adapter\Notification\CommandHandler;

/**
 * Handle update employee's last notification element of a given type
 *
 * @internal
 */
final class UpdateEmployeeNotificationLastElementHandler implements \PrestaShop\PrestaShop\Core\Domain\Notification\CommandHandler\UpdateEmployeeNotificationLastElementCommandHandlerInterface
{
    /**
     * @param UpdateEmployeeNotificationLastElementCommand $command
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Notification\Command\UpdateEmployeeNotificationLastElementCommand $command)
    {
    }
}
