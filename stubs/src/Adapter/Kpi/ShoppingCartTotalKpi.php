<?php

namespace PrestaShop\PrestaShop\Adapter\Kpi;

/**
 * {@inheritdoc}
 */
final class ShoppingCartTotalKpi implements \PrestaShop\PrestaShop\Core\Kpi\KpiInterface
{
    /**
     * @param Locale $locale
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Localization\Locale $locale)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render()
    {
    }
    /**
     * Sets options for Kpi
     *
     * @param array $options
     */
    public function setOptions(array $options)
    {
    }
}
