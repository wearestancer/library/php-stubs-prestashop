<?php

namespace PrestaShop\PrestaShop\Adapter\Order\CommandHandler;

/**
 * @internal
 */
final class BulkChangeOrderStatusHandler implements \PrestaShop\PrestaShop\Core\Domain\Order\CommandHandler\BulkChangeOrderStatusHandlerInterface
{
    /**
     * @param BulkChangeOrderStatusCommand $command
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Order\Command\BulkChangeOrderStatusCommand $command)
    {
    }
}
