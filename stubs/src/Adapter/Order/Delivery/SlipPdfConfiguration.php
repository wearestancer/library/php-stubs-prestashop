<?php

namespace PrestaShop\PrestaShop\Adapter\Order\Delivery;

/**
 * This class manages Order delivery slip pdf configuration.
 */
final class SlipPdfConfiguration implements \PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface
{
    /**
     * Returns configuration used to manage Slip pdf in back office.
     *
     * @return array
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateConfiguration(array $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateConfiguration(array $configuration)
    {
    }
}
