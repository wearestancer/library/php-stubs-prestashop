<?php

namespace PrestaShop\PrestaShop\Adapter\Order;

/**
 * @deprecated since 1.7.4.0
 * @see \PrestaShop\PrestaShop\Adapter\Presenter\Order\OrderReturnPresenter
 */
class OrderReturnPresenter extends \PrestaShop\PrestaShop\Adapter\Presenter\Order\OrderReturnPresenter
{
}
