<?php

namespace PrestaShop\PrestaShop\Adapter\Support;

/**
 * Class ContactRepository is responsible for retrieving contact data from database.
 *
 * @internal
 */
final class ContactRepository implements \PrestaShop\PrestaShop\Core\Support\ContactRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByLangId($langId)
    {
    }
}
