<?php

namespace PrestaShop\PrestaShop\Adapter\Support;

/**
 * Class ContactDeleter deletes contact records, using legacy code.
 */
final class ContactDeleter
{
    /**
     * Delete contacts by given IDs.
     *
     * @param array $contactIds
     *
     * @return array of errors
     */
    public function delete(array $contactIds)
    {
    }
}
