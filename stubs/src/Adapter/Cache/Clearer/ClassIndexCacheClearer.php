<?php

namespace PrestaShop\PrestaShop\Adapter\Cache\Clearer;

/**
 * Class ClassIndexCacheClearer clears current class index and generates new one.
 *
 * @internal
 */
final class ClassIndexCacheClearer implements \PrestaShop\PrestaShop\Core\Cache\Clearer\CacheClearerInterface
{
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
