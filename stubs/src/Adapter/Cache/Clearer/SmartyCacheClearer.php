<?php

namespace PrestaShop\PrestaShop\Adapter\Cache\Clearer;

/**
 * Class SmartyCacheClearer clears Smarty cache.
 *
 * @internal
 */
final class SmartyCacheClearer implements \PrestaShop\PrestaShop\Core\Cache\Clearer\CacheClearerInterface
{
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
