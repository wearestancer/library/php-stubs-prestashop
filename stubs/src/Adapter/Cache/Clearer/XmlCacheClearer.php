<?php

namespace PrestaShop\PrestaShop\Adapter\Cache\Clearer;

/**
 * Class XmlCacheClearer clears cache under /config/xml/ directory.
 *
 * @internal
 */
final class XmlCacheClearer implements \PrestaShop\PrestaShop\Core\Cache\Clearer\CacheClearerInterface
{
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
