<?php

namespace PrestaShop\PrestaShop\Adapter\Cache\Clearer;

/**
 * Class MediaCacheClearer clears Front Office theme's Javascript & CSS cache.
 *
 * @internal
 */
final class MediaCacheClearer implements \PrestaShop\PrestaShop\Core\Cache\Clearer\CacheClearerInterface
{
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
