<?php

namespace PrestaShop\PrestaShop\Adapter;

/**
 * Class SymfonyContainer.
 *
 * This is a TEMPORARY class for quick access to the Symfony Container
 */
final class SymfonyContainer
{
    /**
     * Get a singleton instance of SymfonyContainer.
     *
     * @return ContainerInterface|null
     */
    public static function getInstance()
    {
    }
    public static function resetStaticCache()
    {
    }
}
