<?php

namespace PrestaShop\PrestaShop\Adapter\Hosting;

/**
 * Provides hosting system information.
 */
class HostingInformation
{
    /**
     * @return array
     */
    public function getDatabaseInformation()
    {
    }
    /**
     * @return array
     */
    public function getServerInformation()
    {
    }
    /**
     * @return string
     */
    public function getUname()
    {
    }
    /**
     * @return bool
     */
    public function isApacheInstawebModule()
    {
    }
}
