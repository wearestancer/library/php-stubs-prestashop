<?php

namespace PrestaShop\PrestaShop\Adapter;

/**
 * Retrieve all meta data of an ObjectModel.
 */
class EntityMetaDataRetriever
{
    /**
     * @param string $className
     *
     * @return EntityMetaData
     *
     * @throws \PrestaShop\PrestaShop\Adapter\CoreException
     */
    public function getEntityMetaData($className)
    {
    }
}
