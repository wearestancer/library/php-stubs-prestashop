<?php

namespace PrestaShop\PrestaShop\Adapter\OrderReturn\Validator;

class OrderReturnValidator extends \PrestaShop\PrestaShop\Adapter\AbstractObjectModelValidator
{
    /**
     * @param OrderReturn $orderReturn
     *
     * @throws OrderReturnConstraintException
     */
    public function validate(\OrderReturn $orderReturn) : void
    {
    }
}
