<?php

namespace PrestaShop\PrestaShop\Adapter;

class EntityMapper
{
    /**
     * Load ObjectModel.
     *
     * @param int $id
     * @param int $id_lang
     * @param ObjectModelCore $entity
     * @param array<string,string|array> $entity_defs
     * @param int $id_shop
     * @param bool $should_cache_objects
     *
     * @throws \PrestaShopDatabaseException
     */
    public function load($id, $id_lang, $entity, $entity_defs, $id_shop, $should_cache_objects)
    {
    }
}
