<?php

namespace PrestaShop\PrestaShop\Adapter\Module;

/**
 * Responsible of managing updates of modules.
 */
class ModuleDataUpdater
{
    /**
     * @param string $name
     *
     * @return bool
     */
    public function removeModuleFromDisk($name)
    {
    }
    /**
     * @param string $name
     *
     * @return bool
     */
    public function upgrade($name)
    {
    }
}
