<?php

namespace PrestaShop\PrestaShop\Adapter\Module;

/**
 * Class TabModuleListProvider is responsible for providing tab modules.
 *
 * @deprecated since 1.7.8.0
 */
final class TabModuleListProvider implements \PrestaShop\PrestaShop\Core\Module\DataProvider\TabModuleListProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTabModules($tabClassName)
    {
    }
}
