<?php

namespace PrestaShop\PrestaShop\Adapter\Module;

/**
 * @deprecated since 1.7.4.0
 * @see \PrestaShop\PrestaShop\Adapter\Presenter\Module\ModulePresenter
 */
class ModulePresenter extends \PrestaShop\PrestaShop\Adapter\Presenter\Module\ModulePresenter
{
}
