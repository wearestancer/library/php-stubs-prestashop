<?php

namespace PrestaShop\PrestaShop\Adapter\Carrier;

/**
 * Provides will modules advice alert show in carriers page.
 */
class CarrierModuleAdviceAlertChecker
{
    /**
     * Should this show modules advice alert on carriers page?
     *
     * @return bool
     */
    public function isAlertDisplayed() : bool
    {
    }
}
