<?php

namespace PrestaShop\PrestaShop\Adapter\Carrier\CommandHandler;

class ToggleCarrierIsFreeHandler extends \PrestaShop\PrestaShop\Adapter\Carrier\AbstractCarrierHandler implements \PrestaShop\PrestaShop\Core\Domain\Carrier\CommandHandler\ToggleCarrierIsFreeHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Carrier\Command\ToggleCarrierIsFreeCommand $command)
    {
    }
}
