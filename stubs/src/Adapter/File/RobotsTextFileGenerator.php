<?php

namespace PrestaShop\PrestaShop\Adapter\File;

/**
 * Class RobotsTextFileGenerator is responsible for generating robots txt file.
 */
class RobotsTextFileGenerator
{
    /**
     * Generates the robots.txt file.
     *
     * @return bool
     */
    public function generateFile()
    {
    }
}
