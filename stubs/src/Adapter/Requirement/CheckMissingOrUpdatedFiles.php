<?php

namespace PrestaShop\PrestaShop\Adapter\Requirement;

/**
 * Part of requirements for a PrestaShop website
 * Check if all required files exists.
 */
class CheckMissingOrUpdatedFiles
{
    /**
     * @param string|null $dir
     * @param string $path
     *
     * @return array
     */
    public function getListOfUpdatedFiles($dir = null, $path = '')
    {
    }
}
