<?php

namespace PrestaShop\PrestaShop\Adapter\Presenter\Order;

class OrderPresenter implements \PrestaShop\PrestaShop\Adapter\Presenter\PresenterInterface
{
    /**
     * @param Order $order
     *
     * @return OrderLazyArray
     *
     * @throws Exception
     */
    public function present($order)
    {
    }
}
