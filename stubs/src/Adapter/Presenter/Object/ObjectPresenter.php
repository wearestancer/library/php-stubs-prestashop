<?php

namespace PrestaShop\PrestaShop\Adapter\Presenter\Object;

class ObjectPresenter implements \PrestaShop\PrestaShop\Adapter\Presenter\PresenterInterface
{
    /**
     * @param ObjectModel $object
     *
     * @return array<string, mixed>
     *
     * @throws Exception
     */
    public function present($object)
    {
    }
}
