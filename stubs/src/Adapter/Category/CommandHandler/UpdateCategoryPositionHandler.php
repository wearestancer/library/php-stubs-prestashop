<?php

namespace PrestaShop\PrestaShop\Adapter\Category\CommandHandler;

/**
 * Updates category position using legacy object model
 */
final class UpdateCategoryPositionHandler implements \PrestaShop\PrestaShop\Core\Domain\Category\CommandHandler\UpdateCategoryPositionHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Category\Command\UpdateCategoryPositionCommand $command)
    {
    }
}
