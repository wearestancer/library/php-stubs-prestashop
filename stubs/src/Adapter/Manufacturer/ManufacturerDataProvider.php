<?php

namespace PrestaShop\PrestaShop\Adapter\Manufacturer;

/**
 * This class will provide data from DB / ORM about Manufacturer.
 */
class ManufacturerDataProvider
{
    /**
     * Get all Manufacturer.
     *
     * @param bool $get_nb_products
     * @param int $id_lang
     * @param bool $active
     * @param bool $p
     * @param bool $n
     * @param bool $all_group
     * @param bool $group_by
     *
     * @return array Manufacturer
     */
    public function getManufacturers($get_nb_products = false, $id_lang = 0, $active = true, $p = false, $n = false, $all_group = false, $group_by = false)
    {
    }
}
