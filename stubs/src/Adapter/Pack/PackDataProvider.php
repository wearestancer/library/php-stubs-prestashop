<?php

namespace PrestaShop\PrestaShop\Adapter\Pack;

/**
 * @deprecated since 8.1 and will be removed in next major.
 *
 * This class will provide data from DB / ORM about product pack.
 */
class PackDataProvider
{
    /**
     * Get product pack items.
     *
     * @param int $id_product
     * @param int $id_lang
     */
    public function getItems($id_product, $id_lang)
    {
    }
}
