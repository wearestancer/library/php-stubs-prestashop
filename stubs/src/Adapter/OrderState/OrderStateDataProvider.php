<?php

namespace PrestaShop\PrestaShop\Adapter\OrderState;

/**
 * Class OrderStateDataProvider provides OrderState data using legacy code.
 */
final class OrderStateDataProvider implements \PrestaShop\PrestaShop\Core\Order\OrderStateDataProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrderStates($languageId)
    {
    }
}
