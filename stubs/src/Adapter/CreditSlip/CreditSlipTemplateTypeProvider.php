<?php

namespace PrestaShop\PrestaShop\Adapter\CreditSlip;

/**
 * Provides credit slip PDF template type.
 */
final class CreditSlipTemplateTypeProvider implements \PrestaShop\PrestaShop\Core\PDF\PDFTemplateTypeProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getPDFTemplateType()
    {
    }
}
