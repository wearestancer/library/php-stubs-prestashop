<?php

namespace PrestaShop\PrestaShop\Adapter\Cart;

/**
 * @deprecated since 1.7.4.0
 * @see \PrestaShop\PrestaShop\Adapter\Presenter\Cart\CartPresenter
 */
class CartPresenter extends \PrestaShop\PrestaShop\Adapter\Presenter\Cart\CartPresenter
{
}
