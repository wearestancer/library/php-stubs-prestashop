<?php

namespace PrestaShop\PrestaShop\Adapter\CMS;

/**
 * Class CMSDataProvider provides CMS data using legacy code.
 */
class CMSDataProvider
{
    /**
     * Gets all CMS pages.
     *
     * @param int $languageId
     *
     * @return array
     */
    public function getCMSPages($languageId = null)
    {
    }
    /**
     * Gets one CMS object by ID.
     *
     * @param int $cmsId
     *
     * @return CMS
     */
    public function getCMSById($cmsId)
    {
    }
    /**
     * Gets CMS choices for choice type.
     *
     * @param int $languageId
     *
     * @return array
     */
    public function getCMSChoices($languageId = null)
    {
    }
}
