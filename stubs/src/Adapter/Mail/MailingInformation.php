<?php

namespace PrestaShop\PrestaShop\Adapter\Mail;

/**
 * Retrieve mailing information.
 */
class MailingInformation
{
    /**
     * @return bool
     */
    public function isNativeMailUsed()
    {
    }
    /**
     * @return array
     */
    public function getSmtpInformation()
    {
    }
}
