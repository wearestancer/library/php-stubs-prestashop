<?php

namespace PrestaShop\PrestaShop\Adapter\Form\ChoiceProvider;

final class InstalledPaymentModulesChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getChoices() : array
    {
    }
}
