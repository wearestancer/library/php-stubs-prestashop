<?php

namespace PrestaShop\PrestaShop\Adapter\Form\ChoiceProvider;

/**
 * Returns the list of selectable suppliers, including those which are disabled.
 */
final class SupplierNameByIdChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function getChoices()
    {
    }
}
