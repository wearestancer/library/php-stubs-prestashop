<?php

namespace PrestaShop\PrestaShop\Adapter\Form\ChoiceProvider;

/**
 * Class ProfileByIdChoiceProvider provides employee profile choices with name as label and profile id as value.
 */
final class ProfileByIdChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * @param int $contextLangId
     */
    public function __construct($contextLangId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
    }
}
