<?php

namespace PrestaShop\PrestaShop\Adapter\Language;

/**
 * Class LanguageValidator is responsible for supporting validations from legacy Language class part.
 */
final class LanguageValidator implements \PrestaShop\PrestaShop\Core\Language\LanguageValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function isInstalledByLocale($locale)
    {
    }
}
