<?php

namespace PrestaShop\PrestaShop\Adapter\Language;

/**
 * Class LanguageActivator is responsible for activating/deactivating language.
 */
final class LanguageActivator implements \PrestaShop\PrestaShop\Core\Language\LanguageActivatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function enable($langId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function disable($langId)
    {
    }
}
