<?php

namespace PrestaShop\PrestaShop\Adapter\Language\QueryHandler;

/**
 * Gets language for editing
 *
 * @internal
 */
final class GetLanguageForEditingHandler implements \PrestaShop\PrestaShop\Core\Domain\Language\QueryHandler\GetLanguageForEditingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Language\Query\GetLanguageForEditing $query)
    {
    }
}
