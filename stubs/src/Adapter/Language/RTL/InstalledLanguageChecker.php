<?php

namespace PrestaShop\PrestaShop\Adapter\Language\RTL;

/**
 * Class InstalledLanguageChecker
 */
final class InstalledLanguageChecker implements \PrestaShop\PrestaShop\Core\Language\RTL\InstalledLanguageCheckerInterface
{
    /**
     * @param LanguageDataProvider $languageDataProvider
     */
    public function __construct(\PrestaShop\PrestaShop\Adapter\Language\LanguageDataProvider $languageDataProvider)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isInstalledRtlLanguage()
    {
    }
}
