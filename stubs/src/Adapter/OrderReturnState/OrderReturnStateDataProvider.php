<?php

namespace PrestaShop\PrestaShop\Adapter\OrderReturnState;

/**
 * Class OrderReturnStateDataProvider provides OrderReturnState data using legacy code.
 */
final class OrderReturnStateDataProvider implements \PrestaShop\PrestaShop\Core\Order\OrderReturnStateDataProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrderReturnStates($languageId)
    {
    }
}
