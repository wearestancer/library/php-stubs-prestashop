<?php

namespace PrestaShop\PrestaShop\Adapter\OrderReturnState\QueryHandler;

/**
 * Handles command that gets orderReturnState for editing
 *
 * @internal
 */
final class GetOrderReturnStateForEditingHandler implements \PrestaShop\PrestaShop\Core\Domain\OrderReturnState\QueryHandler\GetOrderReturnStateForEditingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\OrderReturnState\Query\GetOrderReturnStateForEditing $query)
    {
    }
}
