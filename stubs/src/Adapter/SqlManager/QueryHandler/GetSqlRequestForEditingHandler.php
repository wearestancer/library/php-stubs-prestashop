<?php

namespace PrestaShop\PrestaShop\Adapter\SqlManager\QueryHandler;

/**
 * Class GetSqlRequestForEditingHandler.
 *
 * @internal
 */
final class GetSqlRequestForEditingHandler implements \PrestaShop\PrestaShop\Core\Domain\SqlManagement\QueryHandler\GetSqlRequestForEditingHandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws SqlRequestException
     * @throws SqlRequestNotFoundException
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\SqlManagement\Query\GetSqlRequestForEditing $query)
    {
    }
}
