<?php

namespace PrestaShop\PrestaShop\Adapter\Webservice\QueryHandler;

/**
 * Handles command that gets webservice key data for editing
 *
 * @internal
 */
final class GetWebserviceKeyForEditingHandler implements \PrestaShop\PrestaShop\Core\Domain\Webservice\QueryHandler\GetWebserviceKeyForEditingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Webservice\Query\GetWebserviceKeyForEditing $query)
    {
    }
}
