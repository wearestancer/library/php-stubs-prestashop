<?php

namespace PrestaShop\PrestaShop\Adapter\Webservice;

/**
 * Manages the configuration data about upload quota options.
 */
final class WebserviceConfiguration extends \PrestaShop\PrestaShop\Core\Configuration\AbstractMultistoreConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateConfiguration(array $configuration)
    {
    }
}
