<?php

namespace PrestaShop\PrestaShop\Adapter\Webservice\CommandHandler;

/**
 * Handles command that edits legacy WebserviceKey
 *
 * @internal
 */
final class EditWebserviceKeyHandler extends \PrestaShop\PrestaShop\Adapter\Webservice\CommandHandler\AbstractWebserviceKeyHandler implements \PrestaShop\PrestaShop\Core\Domain\Webservice\CommandHandler\EditWebserviceKeyHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Webservice\Command\EditWebserviceKeyCommand $command)
    {
    }
}
