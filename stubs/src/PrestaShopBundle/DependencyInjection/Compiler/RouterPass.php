<?php

namespace PrestaShopBundle\DependencyInjection\Compiler;

/**
 * Alias Custom Security Router to Symfony framework's one.
 *
 * Allows the CSRF Token in URL strategy
 *
 * @see https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)_Prevention_Cheat_Sheet#Disclosure_of_Token_in_URL
 */
class RouterPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
