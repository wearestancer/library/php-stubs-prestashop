<?php

namespace PrestaShopBundle\DependencyInjection;

/**
 * Adds main PrestaShop core services to the Symfony container.
 */
class PrestaShopExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfiguration(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
    }
}
