<?php

namespace PrestaShopBundle\Controller\Exception;

/**
 * Class FieldNotFoundException
 *
 * Thrown when field for error message is not found
 */
class FieldNotFoundException extends \Exception
{
}
