<?php

namespace PrestaShopBundle\Controller\Admin\Sell\Order;

/**
 * @deprecated since 8.0 and will be removed in next major. Use PrestaShop\PrestaShop\Core\ActionBar\ActionsBarButton instead
 */
class ActionsBarButton extends \PrestaShop\PrestaShop\Core\Action\ActionsBarButton implements \PrestaShopBundle\Controller\Admin\Sell\Order\ActionsBarButtonInterface
{
}
