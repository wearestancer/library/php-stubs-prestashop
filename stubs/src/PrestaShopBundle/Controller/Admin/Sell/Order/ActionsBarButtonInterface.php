<?php

namespace PrestaShopBundle\Controller\Admin\Sell\Order;

/**
 * @deprecated since 8.0 and will be removed in next major. Use PrestaShop\PrestaShop\Core\ActionBar\ActionsBarButtonInterface instead
 */
interface ActionsBarButtonInterface extends \PrestaShop\PrestaShop\Core\Action\ActionsBarButtonInterface
{
}
