<?php

namespace PrestaShopBundle\Controller\Admin;

/**
 * Admin controller to manage security pages.
 */
class SecurityController extends \PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController
{
    public function compromisedAccessAction(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
