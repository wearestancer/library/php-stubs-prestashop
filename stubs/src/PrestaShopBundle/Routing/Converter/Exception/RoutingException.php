<?php

namespace PrestaShopBundle\Routing\Converter\Exception;

/**
 * Class RoutingException.
 */
class RoutingException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
