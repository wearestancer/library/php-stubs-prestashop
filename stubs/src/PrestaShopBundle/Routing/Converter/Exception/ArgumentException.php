<?php

namespace PrestaShopBundle\Routing\Converter\Exception;

/**
 * Class ArgumentException.
 */
class ArgumentException extends \PrestaShopBundle\Routing\Converter\Exception\RoutingException
{
}
