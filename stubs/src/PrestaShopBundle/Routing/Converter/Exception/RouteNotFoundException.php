<?php

namespace PrestaShopBundle\Routing\Converter\Exception;

/**
 * Class RouteNotFoundException.
 */
class RouteNotFoundException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
