<?php

namespace PrestaShopBundle\Routing\Linter\Exception;

/**
 * Is thrown when error occurs during linting.
 */
class LinterException extends \RuntimeException
{
}
