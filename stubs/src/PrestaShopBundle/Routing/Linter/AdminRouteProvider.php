<?php

namespace PrestaShopBundle\Routing\Linter;

/**
 * Get all configured routes for Back Office
 */
final class AdminRouteProvider
{
    /**
     * @param RouterInterface $router
     */
    public function __construct(\Symfony\Component\Routing\RouterInterface $router)
    {
    }
    /**
     * @return Route[] As routeName => route
     */
    public function getRoutes()
    {
    }
}
