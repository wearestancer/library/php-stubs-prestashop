<?php

namespace PrestaShopBundle\Entity;

/**
 * StockMvt.
 *
 * @ORM\Table(indexes={@ORM\Index(name="id_stock", columns={"id_stock"}), @ORM\Index(name="id_stock_mvt_reason", columns={"id_stock_mvt_reason"})})
 * @ORM\Entity(repositoryClass="PrestaShopBundle\Entity\Repository\StockMovementRepository")
 */
class StockMvt
{
    public function __construct()
    {
    }
    /**
     * Get idStockMvt.
     *
     * @return int
     */
    public function getIdStockMvt()
    {
    }
    /**
     * Set idStock.
     *
     * @param int $idStock
     *
     * @return StockMvt
     */
    public function setIdStock($idStock)
    {
    }
    /**
     * Get idStock.
     *
     * @return int
     */
    public function getIdStock()
    {
    }
    /**
     * Set idOrder.
     *
     * @param int $idOrder
     *
     * @return StockMvt
     */
    public function setIdOrder($idOrder)
    {
    }
    /**
     * Get idOrder.
     *
     * @return int
     */
    public function getIdOrder()
    {
    }
    /**
     * Set idSupplyOrder.
     *
     * @param int $idSupplyOrder
     *
     * @return StockMvt
     */
    public function setIdSupplyOrder($idSupplyOrder)
    {
    }
    /**
     * Get idSupplyOrder.
     *
     * @return int
     */
    public function getIdSupplyOrder()
    {
    }
    /**
     * Set idStockMvtReason.
     *
     * @param int $idStockMvtReason
     *
     * @return StockMvt
     */
    public function setIdStockMvtReason($idStockMvtReason)
    {
    }
    /**
     * Get idStockMvtReason.
     *
     * @return int
     */
    public function getIdStockMvtReason()
    {
    }
    /**
     * Set idEmployee.
     *
     * @param int $idEmployee
     *
     * @return StockMvt
     */
    public function setIdEmployee($idEmployee)
    {
    }
    /**
     * Get idEmployee.
     *
     * @return int
     */
    public function getIdEmployee()
    {
    }
    /**
     * Set employeeLastname.
     *
     * @param string $employeeLastname
     *
     * @return StockMvt
     */
    public function setEmployeeLastname($employeeLastname)
    {
    }
    /**
     * Get employeeLastname.
     *
     * @return string
     */
    public function getEmployeeLastname()
    {
    }
    /**
     * Set employeeFirstname.
     *
     * @param string $employeeFirstname
     *
     * @return StockMvt
     */
    public function setEmployeeFirstname($employeeFirstname)
    {
    }
    /**
     * Get employeeFirstname.
     *
     * @return string
     */
    public function getEmployeeFirstname()
    {
    }
    /**
     * Set physicalQuantity.
     *
     * @param int $physicalQuantity
     *
     * @return StockMvt
     */
    public function setPhysicalQuantity($physicalQuantity)
    {
    }
    /**
     * Get physicalQuantity.
     *
     * @return int
     */
    public function getPhysicalQuantity()
    {
    }
    /**
     * Set dateAdd.
     *
     * @param \DateTime $dateAdd
     *
     * @return StockMvt
     */
    public function setDateAdd($dateAdd)
    {
    }
    /**
     * Get dateAdd.
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
    }
    /**
     * Set sign.
     *
     * @param int $sign
     *
     * @return StockMvt
     */
    public function setSign($sign)
    {
    }
    /**
     * Get sign.
     *
     * @return int
     */
    public function getSign()
    {
    }
    /**
     * Set priceTe.
     *
     * @param string $priceTe
     *
     * @return StockMvt
     */
    public function setPriceTe($priceTe)
    {
    }
    /**
     * Get priceTe.
     *
     * @return string
     */
    public function getPriceTe()
    {
    }
    /**
     * Set lastWa.
     *
     * @param string $lastWa
     *
     * @return StockMvt
     */
    public function setLastWa($lastWa)
    {
    }
    /**
     * Get lastWa.
     *
     * @return string
     */
    public function getLastWa()
    {
    }
    /**
     * Set currentWa.
     *
     * @param string $currentWa
     *
     * @return StockMvt
     */
    public function setCurrentWa($currentWa)
    {
    }
    /**
     * Get currentWa.
     *
     * @return string
     */
    public function getCurrentWa()
    {
    }
    /**
     * Set referer.
     *
     * @param int $referer
     *
     * @return StockMvt
     */
    public function setReferer($referer)
    {
    }
    /**
     * Get referer.
     *
     * @return int
     */
    public function getReferer()
    {
    }
}
