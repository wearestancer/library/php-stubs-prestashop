<?php

namespace PrestaShopBundle\Exception;

class ProductNotFoundException extends \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
{
}
