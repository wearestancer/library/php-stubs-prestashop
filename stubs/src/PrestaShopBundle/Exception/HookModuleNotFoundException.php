<?php

namespace PrestaShopBundle\Exception;

class HookModuleNotFoundException extends \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
{
}
