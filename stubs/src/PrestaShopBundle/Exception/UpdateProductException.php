<?php

namespace PrestaShopBundle\Exception;

/**
 * Exception thrown when an update of a data in the repository (DB) failed.
 */
class UpdateProductException extends \Exception
{
}
