<?php

namespace PrestaShopBundle\Exception;

/**
 * Exception that should be thrown when file uploading has failed.
 */
class FileUploadException extends \RuntimeException
{
}
