<?php

namespace PrestaShopBundle\Exception;

class InvalidModuleException extends \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
{
}
