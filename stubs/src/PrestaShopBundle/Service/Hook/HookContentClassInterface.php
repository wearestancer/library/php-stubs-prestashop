<?php

namespace PrestaShopBundle\Service\Hook;

interface HookContentClassInterface
{
    public function toArray();
}
