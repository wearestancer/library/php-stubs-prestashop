<?php

namespace PrestaShopBundle\Install;

class System extends \PrestaShopBundle\Install\AbstractInstall
{
    public function checkRequiredTests()
    {
    }
    public function checkOptionalTests()
    {
    }
    //get symfony requirements
    public function checkSf2Requirements()
    {
    }
    //get symfony recommendations
    public function checkSf2Recommendations()
    {
    }
    public function checkTests($list, $type)
    {
    }
}
