<?php

namespace PrestaShopBundle\Api\Stock;

class MovementsCollection
{
    /**
     * @param array $stockMovementsParams
     *
     * @return $this
     */
    public function fromArray(array $stockMovementsParams)
    {
    }
    /**
     * @param callable $callback
     *
     * @return array
     */
    public function map(callable $callback)
    {
    }
}
