<?php

namespace PrestaShopBundle\Api;

class QueryStockMovementParamsCollection extends \PrestaShopBundle\Api\QueryStockParamsCollection
{
    /**
     * @return array
     */
    protected function getValidFilterParams()
    {
    }
    /**
     * @return array
     */
    protected function getValidOrderParams()
    {
    }
    /**
     * @param array $queryParams
     *
     * @return mixed
     */
    protected function setDefaultOrderParam($queryParams)
    {
    }
}
