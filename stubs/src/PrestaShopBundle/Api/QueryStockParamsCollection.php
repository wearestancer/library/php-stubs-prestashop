<?php

namespace PrestaShopBundle\Api;

class QueryStockParamsCollection extends \PrestaShopBundle\Api\QueryParamsCollection
{
    /**
     * @param array $queryParams
     *
     * @return array|mixed
     */
    protected function parseOrderParams(array $queryParams)
    {
    }
    /**
     * @return array
     */
    protected function getValidFilterParams()
    {
    }
    /**
     * @return array
     */
    protected function getValidOrderParams()
    {
    }
    /**
     * @param array $queryParams
     *
     * @return mixed
     */
    protected function setDefaultOrderParam($queryParams)
    {
    }
}
