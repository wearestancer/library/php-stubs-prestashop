<?php

namespace PrestaShopBundle\Bridge\Helper\Listing;

/**
 * This class allow you to get filter prefix in different way.
 */
class FilterPrefix
{
    /**
     * @param string $className
     *
     * @return string|null
     */
    public static function getByClassName(string $className) : ?string
    {
    }
}
