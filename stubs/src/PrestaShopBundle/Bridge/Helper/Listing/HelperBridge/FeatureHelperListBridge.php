<?php

namespace PrestaShopBundle\Bridge\Helper\Listing\HelperBridge;

/**
 * This class customize the result of the list for the feature controller.
 */
class FeatureHelperListBridge extends \PrestaShopBundle\Bridge\Helper\Listing\HelperBridge\HelperListBridge
{
    /**
     * {@inheritDoc}
     */
    public function generateListQuery(\PrestaShopBundle\Bridge\Helper\Listing\HelperListConfiguration $helperListConfiguration, int $idLang) : string
    {
    }
}
