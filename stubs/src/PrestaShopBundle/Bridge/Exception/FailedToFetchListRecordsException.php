<?php

namespace PrestaShopBundle\Bridge\Exception;

/**
 * Thrown when fetching list records fails at some point using HelperListBridge.
 *
 * @see HelperListBridge
 */
class FailedToFetchListRecordsException extends \PrestaShopBundle\Bridge\Exception\BridgeException
{
}
