<?php

namespace PrestaShopBundle\Bridge\Exception;

/**
 * Base class for PrestaShop bridge exceptions.
 */
class BridgeException extends \Exception
{
}
