<?php

namespace PrestaShopBundle\Form\Admin\Improve\Design\MailTheme;

/**
 * Class TranslateMailsBodyType manages the form allowing to select a language
 * and translate Emails body content.
 */
class TranslateMailsBodyType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
