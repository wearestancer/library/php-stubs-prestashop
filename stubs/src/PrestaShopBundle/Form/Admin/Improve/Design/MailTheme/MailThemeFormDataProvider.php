<?php

namespace PrestaShopBundle\Form\Admin\Improve\Design\MailTheme;

/**
 * This class is responsible of managing the data manipulated using forms
 * in "Design > Mail Theme" page.
 */
class MailThemeFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $mailThemeConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
