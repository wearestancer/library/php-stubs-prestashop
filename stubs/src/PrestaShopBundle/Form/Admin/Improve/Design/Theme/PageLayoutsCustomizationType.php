<?php

namespace PrestaShopBundle\Form\Admin\Improve\Design\Theme;

/**
 * Class PageLayoutsCustomizationType is used to customize Front Office theme's page layouts.
 */
class PageLayoutsCustomizationType extends \Symfony\Component\Form\AbstractType
{
    /**
     * @param array $pageLayoutsChoices
     */
    public function __construct(array $pageLayoutsChoices)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
