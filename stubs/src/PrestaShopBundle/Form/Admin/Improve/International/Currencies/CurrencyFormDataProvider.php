<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Currencies;

/**
 * Class CurrencyFormDataProvider
 */
final class CurrencyFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
