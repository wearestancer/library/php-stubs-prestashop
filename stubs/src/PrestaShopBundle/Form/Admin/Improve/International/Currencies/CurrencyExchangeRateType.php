<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Currencies;

/**
 * Class CurrencyExchangeRateType
 */
class CurrencyExchangeRateType extends \PrestaShopBundle\Form\Admin\Type\CommonAbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
