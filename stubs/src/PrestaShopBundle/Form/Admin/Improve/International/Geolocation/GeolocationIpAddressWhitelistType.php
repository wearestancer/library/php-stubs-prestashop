<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Geolocation;

/**
 * Class GeolocationIpAddressWhitelistType is responsible for handling "Improve > International > Localization > Geolocation"
 * IP addresses whitelist form.
 */
class GeolocationIpAddressWhitelistType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
