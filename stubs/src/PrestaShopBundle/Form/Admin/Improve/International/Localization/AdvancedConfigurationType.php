<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Localization;

/**
 * Class AdvancedConfigurationType is responsible for building 'Improve > International > Localization' page
 * 'Advanced' form.
 */
class AdvancedConfigurationType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
