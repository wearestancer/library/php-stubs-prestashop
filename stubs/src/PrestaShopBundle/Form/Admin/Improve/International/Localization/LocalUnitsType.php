<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Localization;

/**
 * Class LocalUnitsType is responsible for building 'Improve > International > Localization' page
 * 'Local units' form.
 */
class LocalUnitsType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
