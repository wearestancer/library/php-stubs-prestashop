<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Localization;

/**
 * Class LocalizationFormDataProvider is responsible for handling
 * 'Improve > International > Localization' page forms data.
 */
class LocalizationFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * @param DataConfigurationInterface $dataConfiguration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $dataConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
