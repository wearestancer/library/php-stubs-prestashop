<?php

namespace PrestaShopBundle\Form\Admin\Improve\International\Translations;

final class TranslationsSettingsFormHandler implements \PrestaShop\PrestaShop\Core\Form\FormHandlerInterface
{
    /**
     * @param FormFactoryInterface $formFactory
     * @param HookDispatcherInterface $hookDispatcher
     * @param string $form
     * @param string $hookName
     */
    public function __construct(\Symfony\Component\Form\FormFactoryInterface $formFactory, \PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface $hookDispatcher, string $form, string $hookName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForm()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(array $data)
    {
    }
}
