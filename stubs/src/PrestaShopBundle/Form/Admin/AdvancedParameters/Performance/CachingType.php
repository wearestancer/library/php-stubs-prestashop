<?php

namespace PrestaShopBundle\Form\Admin\AdvancedParameters\Performance;

/**
 * This form class generates the "Caching" form in Performance page.
 */
class CachingType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
