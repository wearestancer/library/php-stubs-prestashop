<?php

namespace PrestaShopBundle\Form\Admin\AdvancedParameters\Performance;

/**
 * This class manages the data manipulated using forms
 * in "Configure > Advanced Parameters > Performance" page.
 *
 * @deprecated since 1.7.4.0, to be removed in the next major
 */
final class PerformanceFormHandler
{
    public function __construct(\Symfony\Component\Form\FormFactoryInterface $formFactory, \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface $formDataProvider, \PrestaShop\PrestaShop\Adapter\Feature\CombinationFeature $combinationFeature)
    {
    }
    public function getForm()
    {
    }
    public function save(array $data)
    {
    }
}
