<?php

namespace PrestaShopBundle\Form\Admin\AdvancedParameters\Performance;

/**
 * This form class generates the "Memcache server" form in Performance page.
 */
class MemcacheServerType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
