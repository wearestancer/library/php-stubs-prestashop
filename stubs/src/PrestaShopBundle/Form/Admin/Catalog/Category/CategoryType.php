<?php

namespace PrestaShopBundle\Form\Admin\Catalog\Category;

/**
 * Class CategoryType.
 */
class CategoryType extends \PrestaShopBundle\Form\Admin\Catalog\Category\AbstractCategoryType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
