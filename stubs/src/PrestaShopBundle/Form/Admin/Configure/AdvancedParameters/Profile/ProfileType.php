<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\Profile;

/**
 * Builds form for Profile
 */
class ProfileType extends \Symfony\Component\Form\AbstractType
{
    use \PrestaShopBundle\Translation\TranslatorAwareTrait;
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
