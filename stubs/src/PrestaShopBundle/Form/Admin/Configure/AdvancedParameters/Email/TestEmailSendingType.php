<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\Email;

/**
 * Class TestEmailSendingType is responsible for building form type used to send testing emails.
 */
class TestEmailSendingType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
