<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\Email;

/**
 * Class DkimConfigurationType build form for DKIM data configuration.
 */
class DkimConfigurationType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
