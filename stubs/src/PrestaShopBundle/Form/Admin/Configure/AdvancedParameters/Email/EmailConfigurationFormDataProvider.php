<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\Email;

/**
 * Class EmailConfigurationFormDataProvider.
 */
final class EmailConfigurationFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * @param DataConfigurationInterface $emailDataConfigurator
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $emailDataConfigurator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
