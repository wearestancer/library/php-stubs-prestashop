<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\Employee;

/**
 * Class EmployeeOptionsFormDataProvider manages data for employee options form.
 */
final class EmployeeOptionsFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * @param DataConfigurationInterface $employeeOptionsConfiguration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $employeeOptionsConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
