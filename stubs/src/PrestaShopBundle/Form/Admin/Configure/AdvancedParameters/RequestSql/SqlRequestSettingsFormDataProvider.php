<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\RequestSql;

/**
 * Class RequestSqlSettingsFormDataProvider is responsible for managing RequestSql settings.
 */
final class SqlRequestSettingsFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * @param DataConfigurationInterface $dataConfiguration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $dataConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
