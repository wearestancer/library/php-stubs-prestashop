<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\RequestSql;

/**
 * Class RequestSqlSettingsType build form type for "Configure > Advanced Parameters > Database > SQL Manager" page.
 */
class SqlRequestSettingsType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
