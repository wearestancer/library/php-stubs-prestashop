<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\RequestSql;

/**
 * Class RequestSqlType defines RequestSql entity form type.
 */
class SqlRequestType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
