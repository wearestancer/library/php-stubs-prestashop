<?php

namespace PrestaShopBundle\Form\Admin\Configure\AdvancedParameters\Security;

/**
 * This class is responsible of managing the data manipulated using forms
 * in "Configure > Advanced Parameters > Security" page.
 */
final class FormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $dataConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
