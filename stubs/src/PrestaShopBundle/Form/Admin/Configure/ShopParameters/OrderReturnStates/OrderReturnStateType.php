<?php

namespace PrestaShopBundle\Form\Admin\Configure\ShopParameters\OrderReturnStates;

/**
 * Type is used to created form for order return state add/edit actions
 */
class OrderReturnStateType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
