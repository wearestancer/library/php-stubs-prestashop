<?php

namespace PrestaShopBundle\Form\Admin\Configure\ShopParameters\SearchEngines;

/**
 * Form type for search engine add/edit.
 */
class SearchEngineType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) : void
    {
    }
}
