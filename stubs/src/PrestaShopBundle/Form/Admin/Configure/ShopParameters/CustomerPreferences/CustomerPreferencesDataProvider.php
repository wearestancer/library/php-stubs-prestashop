<?php

namespace PrestaShopBundle\Form\Admin\Configure\ShopParameters\CustomerPreferences;

/**
 * Class is responsible of managing the data manipulated using forms
 * in "Configure > Shop Parameters > Customer Settings" page.
 */
final class CustomerPreferencesDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $generalDataConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
