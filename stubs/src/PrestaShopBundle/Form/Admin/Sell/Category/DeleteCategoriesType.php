<?php

namespace PrestaShopBundle\Form\Admin\Sell\Category;

/**
 * Class DeleteCategoriesType.
 */
class DeleteCategoriesType extends \Symfony\Component\Form\AbstractType
{
    /**
     * @param array $categoryDeleteModelChoices
     */
    public function __construct(array $categoryDeleteModelChoices)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
