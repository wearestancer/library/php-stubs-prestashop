<?php

namespace PrestaShopBundle\Form\Admin\Sell\Order\Delivery;

/**
 * This form class generates the "Options" form in Delivery slips page.
 */
class SlipOptionsType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
