<?php

namespace PrestaShopBundle\Form\Admin\Sell\Order\Delivery;

/**
 * This form class generates the "Pdf" form in Delivery slips page.
 */
class SlipPdfType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
