<?php

namespace PrestaShopBundle\Form\Admin\Sell\Order\CreditSlip;

/**
 * Provides data for credit slip options form
 */
final class CreditSlipOptionsFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * @param DataConfigurationInterface $creditSlipOptionsConfiguration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $creditSlipOptionsConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
