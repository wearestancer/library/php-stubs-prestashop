<?php

namespace PrestaShopBundle\Form\Admin\Sell\Product\Combination;

/**
 * For combination update in bulk action
 */
class BulkCombinationType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) : void
    {
    }
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    public function getParent()
    {
    }
}
