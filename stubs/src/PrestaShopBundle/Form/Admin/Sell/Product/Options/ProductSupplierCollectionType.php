<?php

namespace PrestaShopBundle\Form\Admin\Sell\Product\Options;

class ProductSupplierCollectionType extends \Symfony\Component\Form\Extension\Core\Type\CollectionType
{
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
