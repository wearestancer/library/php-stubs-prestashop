<?php

namespace PrestaShopBundle\Form\Admin\Sell\CustomerService\MerchandiseReturn;

/**
 * Provides merchandise return options form with data
 */
final class MerchandiseReturnOptionsFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * @param DataConfigurationInterface $optionsDataConfiguration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\Configuration\DataConfigurationInterface $optionsDataConfiguration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
    }
}
