<?php

namespace PrestaShopBundle\Form\Admin\Sell\Customer;

/**
 * @todo: EntitySearchType component could be improved to avoid coupling form type with dataset
 */
class SearchedCustomerType extends \PrestaShopBundle\Form\Admin\Type\CommonAbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
