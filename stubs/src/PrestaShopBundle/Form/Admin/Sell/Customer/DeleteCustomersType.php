<?php

namespace PrestaShopBundle\Form\Admin\Sell\Customer;

/**
 * Class DeleteCustomersType
 */
class DeleteCustomersType extends \Symfony\Component\Form\AbstractType
{
    /**
     * @param array $customerDeleteMethodChoices
     */
    public function __construct(array $customerDeleteMethodChoices)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
