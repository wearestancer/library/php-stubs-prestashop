<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * Class LogSeverityChoiceType.
 */
class LogSeverityChoiceType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
}
