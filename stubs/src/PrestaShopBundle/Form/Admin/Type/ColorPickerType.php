<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * This form class is responsible to create a color picker field.
 */
class ColorPickerType extends \Symfony\Component\Form\AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * Returns the block prefix of this type.
     *
     * @return string The prefix name
     */
    public function getBlockPrefix()
    {
    }
}
