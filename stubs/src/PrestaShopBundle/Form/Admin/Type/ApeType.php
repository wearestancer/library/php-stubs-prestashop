<?php

namespace PrestaShopBundle\Form\Admin\Type;

class ApeType extends \Symfony\Component\Form\AbstractType implements \Symfony\Component\Form\DataTransformerInterface
{
    use \PrestaShopBundle\Translation\TranslatorAwareTrait;
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    public function getParent()
    {
    }
    public function transform($value)
    {
    }
    public function reverseTransform($value)
    {
    }
}
