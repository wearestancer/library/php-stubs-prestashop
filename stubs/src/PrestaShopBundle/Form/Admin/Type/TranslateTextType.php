<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * Class is responsible for creating translatable text inputs.
 *
 * @deprecated since version 1.7.6 and will be removed in 1.8. Use the TranslatableType instead.
 */
class TranslateTextType extends \Symfony\Component\Form\AbstractType
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
