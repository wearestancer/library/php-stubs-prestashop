<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * Default entry type used by @see EntitySearchInputType
 */
class EntityItemType extends \PrestaShopBundle\Form\Admin\Type\CommonAbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
