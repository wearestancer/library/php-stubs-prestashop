<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * Adds a button right into specified input and toggles it's availability on change
 *
 * Check admin-dev/themes/new-theme/js/components/form/submittable-input.ts for related javascript component
 */
class SubmittableInputType extends \Symfony\Component\Form\AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
