<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * This form class is responsible to create a date picker field.
 */
class DatePickerType extends \Symfony\Component\Form\AbstractType
{
    public function __construct(\Symfony\Component\Form\DataTransformerInterface $arabicToLatinDigitDataTransformer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * Returns the block prefix of this type.
     *
     * @return string The prefix name
     */
    public function getBlockPrefix()
    {
    }
}
