<?php

namespace PrestaShopBundle\Form\Admin\Type;

/**
 * Class ReorderPositionsButtonType.
 */
class ReorderPositionsButtonType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
}
