<?php

namespace PrestaShopBundle\Twig\Extension;

/**
 * Class NumberExtension provides helper function to create Prestashop/Decimal in twig
 */
class NumberExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
    }
    public function createNumber($number)
    {
    }
}
