<?php

namespace PrestaShop\PrestaShop\Core\Filter\FrontEndObject;

/**
 * Filters Product objects that will be sent to the client.
 */
class ProductFilter extends \PrestaShop\PrestaShop\Core\Filter\HashMapWhitelistFilter
{
    public function __construct()
    {
    }
}
