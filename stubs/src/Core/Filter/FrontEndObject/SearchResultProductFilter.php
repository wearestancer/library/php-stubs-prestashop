<?php

namespace PrestaShop\PrestaShop\Core\Filter\FrontEndObject;

/**
 * Filters Product objects for search results.
 */
class SearchResultProductFilter extends \PrestaShop\PrestaShop\Core\Filter\HashMapWhitelistFilter
{
    public function __construct()
    {
    }
}
