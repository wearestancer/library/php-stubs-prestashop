<?php

namespace PrestaShop\PrestaShop\Core\Module\SourceHandler\Exception;

class SourceNotHandledException extends \RuntimeException
{
}
