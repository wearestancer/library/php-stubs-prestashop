<?php

namespace PrestaShop\PrestaShop\Core\Module\SourceHandler;

class SourceHandlerNotFoundException extends \Exception
{
}
