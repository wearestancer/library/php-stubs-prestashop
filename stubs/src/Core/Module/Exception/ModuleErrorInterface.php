<?php

namespace PrestaShop\PrestaShop\Core\Module\Exception;

/**
 * If an exception that implements this ModuleErrorInterface
 * is thrown, its message will be displayed to the end-user
 */
interface ModuleErrorInterface
{
}
