<?php

namespace PrestaShop\PrestaShop\Core\Kpi;

/**
 * Interface KpiInterface describes a KPI.
 */
interface KpiInterface extends \PrestaShop\PrestaShop\Core\Kpi\RenderableKpi
{
}
