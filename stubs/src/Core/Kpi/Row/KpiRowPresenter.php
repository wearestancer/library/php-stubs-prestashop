<?php

namespace PrestaShop\PrestaShop\Core\Kpi\Row;

/**
 * Class KpiRowPresenter presents a KPI row.
 */
final class KpiRowPresenter implements \PrestaShop\PrestaShop\Core\Kpi\Row\KpiRowPresenterInterface
{
    /**
     * {@inheritdoc}
     */
    public function present(\PrestaShop\PrestaShop\Core\Kpi\Row\KpiRowInterface $kpiRow)
    {
    }
}
