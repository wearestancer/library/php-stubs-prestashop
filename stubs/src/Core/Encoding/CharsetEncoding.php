<?php

namespace PrestaShop\PrestaShop\Core\Encoding;

/**
 * Class CharsetEncoding defines file chartset encoding constants.
 */
final class CharsetEncoding
{
    public const UTF_8 = 'utf-8';
    public const ISO_8859_1 = 'iso-8859-1';
}
