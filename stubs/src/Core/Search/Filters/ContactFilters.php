<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class ContactFilters is responsible for providing default filter values for Contacts list.
 */
final class ContactFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
