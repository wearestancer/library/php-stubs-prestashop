<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Responsible for providing filter values for attribute groups list
 */
final class AttributeGroupFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
