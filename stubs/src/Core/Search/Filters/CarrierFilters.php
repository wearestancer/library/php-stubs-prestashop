<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Defines default filter for carriers listing
 */
final class CarrierFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
