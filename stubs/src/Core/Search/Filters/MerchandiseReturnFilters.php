<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Provides default filters for merchandise returns grid.
 */
final class MerchandiseReturnFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
