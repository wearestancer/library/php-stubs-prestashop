<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * This class manage the defaults values for user request filters
 * of page Configure > Advanced Parameters > Logs.
 */
final class LogsFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
