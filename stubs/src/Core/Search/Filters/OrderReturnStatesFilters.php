<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class OrderReturnStatesFilters provides default filters for order return states grid.
 */
final class OrderReturnStatesFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    public const LIST_LIMIT = 50;
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
