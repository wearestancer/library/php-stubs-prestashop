<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class SupplierFilters  defines default filters for Suppliers grid.
 */
final class SupplierFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
