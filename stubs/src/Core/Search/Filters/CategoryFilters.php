<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class CategoryFilters defines default filters for Category grid.
 */
final class CategoryFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
