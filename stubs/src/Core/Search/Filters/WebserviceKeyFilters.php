<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class WebserviceKeyFilters is responsible for providing default values for webservice account list.
 */
final class WebserviceKeyFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
