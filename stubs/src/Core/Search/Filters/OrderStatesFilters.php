<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class OrderStatesFilters provides default filters for order states grid.
 */
final class OrderStatesFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    public const LIST_LIMIT = 50;
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
