<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class CustomerFilters provides default filters for customers grid.
 */
final class CustomerFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
