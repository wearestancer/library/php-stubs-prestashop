<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Provides default filters for taxes grid.
 */
final class TaxFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
