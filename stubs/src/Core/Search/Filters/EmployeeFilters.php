<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class EmployeeFilters holds search criteria for Employee grid.
 */
final class EmployeeFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
