<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Gets product grid filters.
 */
final class ProductFilters extends \PrestaShop\PrestaShop\Core\Search\ShopFilters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
