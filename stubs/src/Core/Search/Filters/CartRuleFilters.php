<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Responsible for providing default filters for cart rule grid.
 */
final class CartRuleFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults() : array
    {
    }
}
