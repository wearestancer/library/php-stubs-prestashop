<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Defines default filters for Attachments grid.
 */
final class AttachmentFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
