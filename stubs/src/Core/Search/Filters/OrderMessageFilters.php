<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Defines filters for order message grid
 */
final class OrderMessageFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
