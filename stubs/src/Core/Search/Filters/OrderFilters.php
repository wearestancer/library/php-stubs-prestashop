<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Stores filters for Order grid
 */
final class OrderFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
