<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class ProfilesFilters is responsible for providing default filter values for Profiles grid.
 */
final class ProfileFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
