<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Responsible for providing filter values for attribute groups > attributes grid.
 */
final class AttributeFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
