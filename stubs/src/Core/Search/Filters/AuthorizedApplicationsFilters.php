<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * This class manage the defaults values for user request filters
 * of page Configure > Advanced Parameters > Logs.
 *
 * @experimental
 */
final class AuthorizedApplicationsFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults() : array
    {
    }
}
