<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters\Monitoring;

/**
 * Defines default filters for product without price grid.
 */
final class ProductWithoutPriceFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
