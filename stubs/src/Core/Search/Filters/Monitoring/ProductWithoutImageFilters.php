<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters\Monitoring;

/**
 * Defines default filters for product without image grid.
 */
final class ProductWithoutImageFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
