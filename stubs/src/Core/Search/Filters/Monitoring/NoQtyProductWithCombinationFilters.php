<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters\Monitoring;

/**
 * Defines default filters for product with combination grid.
 */
final class NoQtyProductWithCombinationFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
