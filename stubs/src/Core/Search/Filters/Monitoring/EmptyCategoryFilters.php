<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters\Monitoring;

/**
 * Defines default filters for empty category grid.
 */
final class EmptyCategoryFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
