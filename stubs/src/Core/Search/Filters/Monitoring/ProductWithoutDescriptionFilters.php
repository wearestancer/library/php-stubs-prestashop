<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters\Monitoring;

/**
 * Defines default filters for product without description grid.
 */
final class ProductWithoutDescriptionFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
