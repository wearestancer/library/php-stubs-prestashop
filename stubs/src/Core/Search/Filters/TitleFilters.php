<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class TitleFilters define default filters used for title filtering.
 */
final class TitleFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
