<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Responsible for providing default filters for catalog price rule grid.
 */
final class CatalogPriceRuleFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
