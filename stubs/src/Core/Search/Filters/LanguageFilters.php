<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class LanguageFilters define default filters used for languages filtering.
 */
final class LanguageFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
