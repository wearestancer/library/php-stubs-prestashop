<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class CmsPageCategoryFilters defines default filters for cms page category grid.
 */
final class CmsPageCategoryFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
