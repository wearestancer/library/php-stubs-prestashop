<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class MetaFilters is responsible for providing default filter values for Seo & urls list.
 */
final class MetaFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
