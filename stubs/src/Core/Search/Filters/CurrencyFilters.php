<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class CurrencyFilters is responsible for providing default filters for currency grid.
 */
final class CurrencyFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
