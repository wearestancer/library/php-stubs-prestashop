<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class BackupFilters defines filters for 'Configure > Advanced Parameters > Database > Backup' listing.
 */
final class BackupFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
