<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Default addresses list filters
 */
final class AddressFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults() : array
    {
    }
}
