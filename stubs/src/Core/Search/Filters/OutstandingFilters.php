<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Stores filters for Outstanding grid.
 */
final class OutstandingFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
