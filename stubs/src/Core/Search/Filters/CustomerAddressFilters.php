<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Default customer's addresses list filters
 */
final class CustomerAddressFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults() : array
    {
    }
}
