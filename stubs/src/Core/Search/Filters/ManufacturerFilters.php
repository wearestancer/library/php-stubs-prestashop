<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class ManufacturerFilters is responsible for providing filter values for manufacturer grid.
 */
final class ManufacturerFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
