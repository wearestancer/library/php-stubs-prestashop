<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Provides default filters for credit slip list
 */
final class CreditSlipFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
