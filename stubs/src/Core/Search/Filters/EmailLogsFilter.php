<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class EmailLogsFilter defines default filters for Email logs grid.
 */
final class EmailLogsFilter extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
