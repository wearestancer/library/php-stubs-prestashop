<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class ManufacturerAddressFilters is responsible for providing filter values for manufacturer address grid.
 */
final class ManufacturerAddressFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
