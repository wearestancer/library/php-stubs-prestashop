<?php

namespace PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Provides default filters for tax rule groups grid.
 */
final class TaxRulesGroupFilters extends \PrestaShop\PrestaShop\Core\Search\Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
    }
}
