<?php

namespace PrestaShop\PrestaShop\Core\Search\Builder;

/**
 * Collection of FiltersBuilderInterface.
 */
final class FiltersBuilderCollection extends \PrestaShop\PrestaShop\Core\Data\AbstractTypedCollection
{
}
