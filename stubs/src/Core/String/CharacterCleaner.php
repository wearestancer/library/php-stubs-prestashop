<?php

namespace PrestaShop\PrestaShop\Core\String;

class CharacterCleaner
{
    /**
     * Delete unicode class from regular expression patterns.
     *
     * @deprecated Since 8.0.0 and will be removed in the next major.
     *
     * @param string $pattern
     *
     * @return string pattern
     */
    public function cleanNonUnicodeSupport($pattern)
    {
    }
}
