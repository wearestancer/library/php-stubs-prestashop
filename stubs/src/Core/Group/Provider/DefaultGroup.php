<?php

namespace PrestaShop\PrestaShop\Core\Group\Provider;

/**
 * Stores information for default group
 */
class DefaultGroup
{
    /**
     * @param int $groupId
     * @param string $name
     */
    public function __construct($groupId, $name)
    {
    }
    /**
     * @return int
     */
    public function getId()
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
}
