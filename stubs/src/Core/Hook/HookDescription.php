<?php

namespace PrestaShop\PrestaShop\Core\Hook;

/**
 * Class holds descriptive information about the hook.
 */
final class HookDescription
{
    /**
     * @param string $name
     * @param string $title
     * @param string $description
     */
    public function __construct($name, $title, $description)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @return string
     */
    public function getTitle()
    {
    }
    /**
     * @return string
     */
    public function getDescription()
    {
    }
}
