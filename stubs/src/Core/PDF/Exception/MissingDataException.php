<?php

namespace PrestaShop\PrestaShop\Core\PDF\Exception;

/**
 * Thrown when required data for pdf generating is missing
 */
class MissingDataException extends \PrestaShop\PrestaShop\Core\PDF\Exception\PdfException
{
}
