<?php

namespace PrestaShop\PrestaShop\Core\PDF\Exception;

/**
 * Base exception for PDF generation related exceptions
 */
class PdfException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
