<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

final class CmsCategoriesChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * @param array $nestedCategories
     */
    public function __construct(array $nestedCategories)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
    }
}
