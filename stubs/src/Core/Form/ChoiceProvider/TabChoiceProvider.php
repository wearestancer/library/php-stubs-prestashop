<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

/**
 * Class TabChoiceProvider provides Tab choices with name values.
 *
 * This class is used for choosing Default page when creating employee
 */
final class TabChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * @param array $tabs
     */
    public function __construct(array $tabs)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
    }
}
