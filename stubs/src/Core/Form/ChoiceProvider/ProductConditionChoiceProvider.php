<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

final class ProductConditionChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getChoices()
    {
    }
}
