<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

/**
 * Class ProfileChoiceProvider provides employee profile choices.
 */
final class ProfileChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * @param array $profiles
     */
    public function __construct(array $profiles)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
    }
}
