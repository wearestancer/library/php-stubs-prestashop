<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

/**
 * Gets choices for predefined order message types.
 */
final class CustomerServiceOrderMessagesNameChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    public function __construct(array $orderMessages)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices() : array
    {
    }
}
