<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

/**
 * Class GenderProvider provides genders choices.
 */
class GenderChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * Get currency choices.
     *
     * @return array
     */
    public function getChoices() : array
    {
    }
}
