<?php

namespace PrestaShop\PrestaShop\Core\Form\ChoiceProvider;

final class ProductVisibilityChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface, \PrestaShop\PrestaShop\Core\Form\FormChoiceAttributeProviderInterface
{
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getChoices() : array
    {
    }
    public function getChoicesAttributes() : array
    {
    }
}
