<?php

namespace PrestaShop\PrestaShop\Core\Form\IdentifiableObject\CommandBuilder;

/**
 * Thrown when trying to create a CommandField with invalid type.
 */
class DataFieldException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
