<?php

namespace PrestaShop\PrestaShop\Core\Form\IdentifiableObject\OptionProvider;

class CombinationFormOptionsProvider implements \PrestaShop\PrestaShop\Core\Form\IdentifiableObject\OptionProvider\FormOptionsProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function getOptions(int $id, array $data) : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDefaultOptions(array $data) : array
    {
    }
}
