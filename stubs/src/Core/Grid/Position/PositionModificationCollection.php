<?php

namespace PrestaShop\PrestaShop\Core\Grid\Position;

/**
 * Class PositionModificationCollection holds collection of row modifications for grid.
 *
 * @property PositionModificationInterface[] $items
 */
final class PositionModificationCollection extends \PrestaShop\PrestaShop\Core\Grid\Collection\AbstractCollection implements \PrestaShop\PrestaShop\Core\Grid\Position\PositionModificationCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function add(\PrestaShop\PrestaShop\Core\Grid\Position\PositionModificationInterface $positionModification)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove(\PrestaShop\PrestaShop\Core\Grid\Position\PositionModificationInterface $positionModification)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
    }
}
