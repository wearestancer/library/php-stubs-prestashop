<?php

namespace PrestaShop\PrestaShop\Core\Grid\Position\Exception;

/**
 * Class PositionDataException, used by the PositionDataHandler.
 */
class PositionDataException extends \PrestaShop\PrestaShop\Core\Grid\Position\Exception\PositionException
{
}
