<?php

namespace PrestaShop\PrestaShop\Core\Grid\Position\Exception;

/**
 * Class PositionUpdateException throw by GridPositionUpdater.
 */
class PositionUpdateException extends \PrestaShop\PrestaShop\Core\Grid\Position\Exception\PositionException
{
}
