<?php

namespace PrestaShop\PrestaShop\Core\Grid\Position\Exception;

/**
 * Class PositionException used by the GridPositionUpdater component.
 */
class PositionException extends \PrestaShop\PrestaShop\Core\Exception\TranslatableCoreException
{
}
