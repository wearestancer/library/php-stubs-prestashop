<?php

namespace PrestaShop\PrestaShop\Core\Grid\Query;

/**
 * Builds search and count query builders for zone grid.
 */
class CountryQueryBuilder extends \PrestaShop\PrestaShop\Core\Grid\Query\AbstractDoctrineQueryBuilder
{
    /**
     * @var DoctrineSearchCriteriaApplicatorInterface
     */
    protected $searchCriteriaApplicator;
    /**
     * @var int[]
     */
    protected $contextShopIds;
    /**
     * @param Connection $connection
     * @param string $dbPrefix
     * @param DoctrineSearchCriteriaApplicatorInterface $searchCriteriaApplicator
     * @param int[] $contextShopIds
     */
    public function __construct(\Doctrine\DBAL\Connection $connection, string $dbPrefix, \PrestaShop\PrestaShop\Core\Grid\Query\DoctrineSearchCriteriaApplicatorInterface $searchCriteriaApplicator, array $contextShopIds, int $contextLangId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSearchQueryBuilder(\PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface $searchCriteria) : \Doctrine\DBAL\Query\QueryBuilder
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCountQueryBuilder(\PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface $searchCriteria) : \Doctrine\DBAL\Query\QueryBuilder
    {
    }
    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return QueryBuilder
     */
    protected function getQueryBuilder(\PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface $searchCriteria) : \Doctrine\DBAL\Query\QueryBuilder
    {
    }
    /**
     * @param QueryBuilder $builder
     * @param SearchCriteriaInterface $criteria
     */
    protected function applyFilters(\Doctrine\DBAL\Query\QueryBuilder $builder, \PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface $criteria) : void
    {
    }
}
