<?php

namespace PrestaShop\PrestaShop\Core\Grid\Query;

/**
 * This class offers a DBAL implementation of Query parser.
 */
final class DoctrineQueryParser implements \PrestaShop\PrestaShop\Core\Grid\Query\QueryParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function parse($query, array $queryParameters)
    {
    }
}
