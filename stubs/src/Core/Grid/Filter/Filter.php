<?php

namespace PrestaShop\PrestaShop\Core\Grid\Filter;

/**
 * Class Filter defines single filter for grid.
 */
final class Filter implements \PrestaShop\PrestaShop\Core\Grid\Filter\FilterInterface
{
    /**
     * @param string $name
     * @param string $filterFormType
     */
    public function __construct($name, $filterFormType)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setTypeOptions(array $filterTypeOptions)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeOptions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAssociatedColumn($columnId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAssociatedColumn()
    {
    }
}
