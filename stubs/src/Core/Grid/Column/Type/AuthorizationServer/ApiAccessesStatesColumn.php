<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\AuthorizationServer;

/**
 * @experimental
 */
final class ApiAccessesStatesColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function getType() : string
    {
    }
}
