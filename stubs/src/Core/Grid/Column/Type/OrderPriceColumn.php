<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type;

/**
 * @deprecated since 8.1 and will be removed in next major.
 * Use \PrestaShop\PrestaShop\Core\Grid\Column\Type\Order\OrderPriceColumn instead.
 */
final class OrderPriceColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\Type\Order\OrderPriceColumn
{
}
