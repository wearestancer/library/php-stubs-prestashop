<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\Currency;

/**
 * Class NameColumn displays currency name with unofficial icon if needed.
 */
final class NameColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
