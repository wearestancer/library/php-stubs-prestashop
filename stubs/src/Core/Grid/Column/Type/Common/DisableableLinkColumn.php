<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\Common;

class DisableableLinkColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    public function __construct($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setOptions(array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
    }
}
