<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\Attribute;

/**
 * Defines column which renders a block filled with color
 */
final class AttributeColorColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
