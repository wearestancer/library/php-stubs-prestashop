<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type;

/**
 * @deprecated since 8.1 and will be removed in next major.
 * Use \PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\LinkGroupColumn instead.
 */
final class LinkGroupColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\LinkGroupColumn
{
}
