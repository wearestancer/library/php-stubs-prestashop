<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\Order;

/**
 * Displays order price
 */
class OrderPriceColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
