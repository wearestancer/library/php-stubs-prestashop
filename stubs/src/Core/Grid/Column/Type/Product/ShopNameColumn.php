<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\Product;

/**
 * Display the detail of a shop with its name and associated color.
 */
class ShopNameColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
