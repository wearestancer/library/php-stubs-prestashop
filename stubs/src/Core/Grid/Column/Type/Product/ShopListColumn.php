<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type\Product;

/**
 * Display a list of shops, the first shop is highlighted as bold and the rest are truncated if
 * max_displayed_characters is set.
 */
final class ShopListColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
