<?php

namespace PrestaShop\PrestaShop\Core\Grid\Column\Type;

/**
 * @deprecated since 8.1 and will be removed in next major.
 * Use \PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\DataColumn instead.
 */
final class DataColumn extends \PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\DataColumn
{
}
