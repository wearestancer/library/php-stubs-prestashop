<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Type;

/**
 * Class LinkGridAction defines grid action which is link.
 */
final class LinkGridAction extends \PrestaShop\PrestaShop\Core\Grid\Action\AbstractGridAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
