<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Type;

/**
 * Class SubmitGridAction represents grid action that can be submitted.
 */
final class SubmitGridAction extends \PrestaShop\PrestaShop\Core\Grid\Action\AbstractGridAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
