<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action;

/**
 * Class ViewOptionsCollection is responsible for holding view options.
 *
 * @property array $items
 */
final class ViewOptionsCollection extends \PrestaShop\PrestaShop\Core\Grid\Collection\AbstractCollection implements \PrestaShop\PrestaShop\Core\Grid\Action\ViewOptionsCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function add(string $action, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
}
