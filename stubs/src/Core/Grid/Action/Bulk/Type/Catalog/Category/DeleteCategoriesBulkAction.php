<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\Catalog\Category;

/**
 * Class DeleteCategoriesBulkAction implements bulk deleting for categories grid.
 */
final class DeleteCategoriesBulkAction extends \PrestaShop\PrestaShop\Core\Grid\Action\Bulk\AbstractBulkAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
