<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type;

/**
 * Class ButtonBulkAction holds data to display a simple button, it allows you to
 * set a (required) class and additional (optional) attributes that can then be used
 * in javascript.
 */
final class ButtonBulkAction extends \PrestaShop\PrestaShop\Core\Grid\Action\Bulk\AbstractBulkAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
