<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type;

/**
 * Class BulkAction holds data about single bulk action available in grid.
 */
final class SubmitBulkAction extends \PrestaShop\PrestaShop\Core\Grid\Action\Bulk\AbstractBulkAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
