<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\Customer;

/**
 * Allows to configure "Delete" customers bulk action.
 */
final class DeleteCustomersBulkAction extends \PrestaShop\PrestaShop\Core\Grid\Action\Bulk\AbstractBulkAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
