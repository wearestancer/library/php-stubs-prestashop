<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type;

/**
 * Bulk actions that submit form in modal
 */
final class ModalFormSubmitBulkAction extends \PrestaShop\PrestaShop\Core\Grid\Action\Bulk\AbstractBulkAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
