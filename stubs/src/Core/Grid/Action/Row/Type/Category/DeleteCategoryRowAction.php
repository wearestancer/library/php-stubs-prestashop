<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Row\Type\Category;

/**
 * Class DeleteCategoryRowAction adds "Delete" action to row.
 */
final class DeleteCategoryRowAction extends \PrestaShop\PrestaShop\Core\Grid\Action\Row\AbstractRowAction
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
}
