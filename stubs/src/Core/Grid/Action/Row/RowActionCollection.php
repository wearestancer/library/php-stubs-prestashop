<?php

namespace PrestaShop\PrestaShop\Core\Grid\Action\Row;

/**
 * Class RowActionCollection defines contract for grid row action collection.
 */
final class RowActionCollection extends \PrestaShop\PrestaShop\Core\Grid\Collection\AbstractCollection implements \PrestaShop\PrestaShop\Core\Grid\Action\Row\RowActionCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function add(\PrestaShop\PrestaShop\Core\Grid\Action\Row\RowActionInterface $action)
    {
    }
}
