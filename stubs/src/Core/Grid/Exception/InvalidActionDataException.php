<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

class InvalidActionDataException extends \Exception implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
