<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

/**
 * Interface ExceptionInterface is implemented by all grid exceptions.
 */
interface ExceptionInterface
{
}
