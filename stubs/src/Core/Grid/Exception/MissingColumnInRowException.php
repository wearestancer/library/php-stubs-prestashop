<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

/**
 * Class MissingColumnInRowException.
 */
class MissingColumnInRowException extends \LogicException implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
