<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

/**
 * Class InvalidFilterDataException.
 */
class InvalidFilterDataException extends \Exception implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
