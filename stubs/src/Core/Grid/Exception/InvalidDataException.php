<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

class InvalidDataException extends \Exception implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
