<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

class InvalidColumnDataException extends \Exception implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
