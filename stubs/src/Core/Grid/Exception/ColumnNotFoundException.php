<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

class ColumnNotFoundException extends \Exception implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
