<?php

namespace PrestaShop\PrestaShop\Core\Grid\Exception;

class UnsupportedParameterException extends \Exception implements \PrestaShop\PrestaShop\Core\Grid\Exception\ExceptionInterface
{
}
