<?php

namespace PrestaShop\PrestaShop\Core\Grid\Data\Factory;

/**
 * Class CategoryDataFactory decorates DoctrineGridDataFactory configured for categories to modify category records.
 */
final class CategoryDataFactory extends \PrestaShop\PrestaShop\Core\Grid\Data\Factory\AbstractCategoryDataFactory
{
}
