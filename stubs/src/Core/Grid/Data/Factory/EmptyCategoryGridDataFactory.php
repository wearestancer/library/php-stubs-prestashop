<?php

namespace PrestaShop\PrestaShop\Core\Grid\Data\Factory;

/**
 * Modifies records from database for empty_category grid
 */
final class EmptyCategoryGridDataFactory extends \PrestaShop\PrestaShop\Core\Grid\Data\Factory\AbstractCategoryDataFactory
{
}
