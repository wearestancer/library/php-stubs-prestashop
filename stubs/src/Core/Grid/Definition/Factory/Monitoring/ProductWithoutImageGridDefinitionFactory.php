<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring;

/**
 * Builds Grid definition for product without image grid
 */
final class ProductWithoutImageGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring\AbstractProductGridDefinitionFactory
{
    public const GRID_ID = 'product_without_image';
}
