<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring;

/**
 * Builds Grid definition for product without description grid
 */
final class ProductWithoutDescriptionGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring\AbstractProductGridDefinitionFactory
{
    public const GRID_ID = 'product_without_description';
}
