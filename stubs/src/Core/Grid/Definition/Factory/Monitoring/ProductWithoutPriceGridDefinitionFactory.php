<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring;

/**
 * Builds Grid definition for product without price grid
 */
final class ProductWithoutPriceGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring\AbstractProductGridDefinitionFactory
{
    public const GRID_ID = 'product_without_price';
}
