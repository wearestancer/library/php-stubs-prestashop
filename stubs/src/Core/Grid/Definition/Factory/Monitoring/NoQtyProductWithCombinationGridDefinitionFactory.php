<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring;

/**
 * Builds Grid definition for product with combination and without quantities grid
 */
final class NoQtyProductWithCombinationGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring\AbstractProductGridDefinitionFactory
{
    public const GRID_ID = 'no_qty_product_with_combination';
}
