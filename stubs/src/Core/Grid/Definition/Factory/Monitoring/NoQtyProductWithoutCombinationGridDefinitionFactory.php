<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring;

/**
 * Builds Grid definition for product without combination and without quantities grid
 */
final class NoQtyProductWithoutCombinationGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\Monitoring\AbstractProductGridDefinitionFactory
{
    public const GRID_ID = 'no_qty_product_without_combination';
}
