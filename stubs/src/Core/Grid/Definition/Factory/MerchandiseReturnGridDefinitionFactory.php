<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory;

/**
 * Class MerchandiseReturnGridDefinitionFactory builds grid definition for merchandise returns grid.
 */
final class MerchandiseReturnGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractFilterableGridDefinitionFactory
{
    public const GRID_ID = 'merchandise_return';
}
