<?php

namespace PrestaShop\PrestaShop\Core\Grid\Definition\Factory;

/**
 * Class CustomerDiscountGridDefinitionFactory defines customer's discounts grid structure.
 */
final class CustomerDiscountGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractGridDefinitionFactory
{
    public const GRID_ID = 'customer_discount';
    /**
     * {@inheritdoc}
     */
    public function getViewOptions()
    {
    }
}
