<?php

namespace PrestaShop\PrestaShop\Core\Grid\Presenter;

/**
 * Class GridPresenter is responsible for presenting grid.
 */
final class GridPresenter implements \PrestaShop\PrestaShop\Core\Grid\Presenter\GridPresenterInterface
{
    public function __construct(\PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface $hookDispatcher)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function present(\PrestaShop\PrestaShop\Core\Grid\GridInterface $grid)
    {
    }
}
