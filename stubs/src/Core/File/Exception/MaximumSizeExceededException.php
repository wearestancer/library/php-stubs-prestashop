<?php

namespace PrestaShop\PrestaShop\Core\File\Exception;

/**
 * Thrown when maximum file size exceeded
 */
class MaximumSizeExceededException extends \PrestaShop\PrestaShop\Core\File\Exception\FileException
{
}
