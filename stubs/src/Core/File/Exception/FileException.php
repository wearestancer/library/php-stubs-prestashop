<?php

namespace PrestaShop\PrestaShop\Core\File\Exception;

/**
 * Base exception for files
 */
class FileException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
