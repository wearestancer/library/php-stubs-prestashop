<?php

namespace PrestaShop\PrestaShop\Core\File\Exception;

/**
 * Thrown when file unlink fails
 */
class CannotUnlinkFileException extends \PrestaShop\PrestaShop\Core\File\Exception\FileException
{
}
