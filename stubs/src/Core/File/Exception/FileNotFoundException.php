<?php

namespace PrestaShop\PrestaShop\Core\File\Exception;

/**
 * Thrown when file is not found
 */
class FileNotFoundException extends \PrestaShop\PrestaShop\Core\File\Exception\FileException
{
}
