<?php

namespace PrestaShop\PrestaShop\Core\File\Exception;

/**
 * Thrown when file upload fails
 */
class FileUploadException extends \PrestaShop\PrestaShop\Core\File\Exception\FileException
{
}
