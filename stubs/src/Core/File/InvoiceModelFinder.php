<?php

namespace PrestaShop\PrestaShop\Core\File;

/**
 * Class InvoiceModelFinder finds invoice model files.
 */
final class InvoiceModelFinder implements \PrestaShop\PrestaShop\Core\File\FileFinderInterface
{
    /**
     * @param array $invoiceModelDirectories
     */
    public function __construct(array $invoiceModelDirectories)
    {
    }
    /**
     * Finds all invoice model files.
     *
     * @return array
     */
    public function find()
    {
    }
}
