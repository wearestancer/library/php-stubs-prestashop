<?php

namespace PrestaShop\PrestaShop\Core;

class ContainerBuilder
{
    /**
     * Construct PrestaShop Core Service container.
     *
     * @return \PrestaShop\PrestaShop\Core\Foundation\IoC\Container
     *
     * @throws \PrestaShop\PrestaShop\Core\Foundation\IoC\Exception
     */
    public function build()
    {
    }
}
