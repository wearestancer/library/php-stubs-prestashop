<?php

namespace PrestaShop\PrestaShop\Core\MerchandiseReturn\Configuration;

/**
 * Provides data configuration for merchandise returns options form
 */
class MerchandiseReturnOptionsConfiguration extends \PrestaShop\PrestaShop\Core\Configuration\AbstractMultistoreConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateConfiguration(array $configuration)
    {
    }
    /**
     * @return OptionsResolver
     */
    protected function buildResolver() : \Symfony\Component\OptionsResolver\OptionsResolver
    {
    }
}
