<?php

namespace PrestaShop\PrestaShop\Core\Language\Copier;

/**
 * Class LanguageCopierConfig provides configuration for language copier.
 */
final class LanguageCopierConfig implements \PrestaShop\PrestaShop\Core\Language\Copier\LanguageCopierConfigInterface
{
    /**
     * @param string $themeFrom
     * @param string $languageFrom
     * @param string $themeTo
     * @param string $languageTo
     */
    public function __construct($themeFrom, $languageFrom, $themeTo, $languageTo)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getThemeFrom()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLanguageFrom()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getThemeTo()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLanguageTo()
    {
    }
}
