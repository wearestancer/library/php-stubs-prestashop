<?php

namespace PrestaShop\PrestaShop\Core\MailTemplate\Layout;

class LayoutCollection extends \PrestaShop\PrestaShop\Core\Data\AbstractTypedCollection implements \PrestaShop\PrestaShop\Core\MailTemplate\Layout\LayoutCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    protected function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function merge(\PrestaShop\PrestaShop\Core\MailTemplate\Layout\LayoutCollectionInterface $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function replace(\PrestaShop\PrestaShop\Core\MailTemplate\Layout\LayoutInterface $oldLayout, \PrestaShop\PrestaShop\Core\MailTemplate\Layout\LayoutInterface $newLayout)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLayout($layoutName, $moduleName)
    {
    }
}
