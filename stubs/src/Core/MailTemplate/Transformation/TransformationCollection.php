<?php

namespace PrestaShop\PrestaShop\Core\MailTemplate\Transformation;

class TransformationCollection extends \PrestaShop\PrestaShop\Core\Data\AbstractTypedCollection implements \PrestaShop\PrestaShop\Core\MailTemplate\Transformation\TransformationCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    protected function getType()
    {
    }
}
