<?php

namespace PrestaShop\PrestaShop\Core\CMS;

class CMSRoleRepository extends \PrestaShop\PrestaShop\Core\Foundation\Database\EntityRepository
{
    /**
     * Return all CMSRoles which are already associated.
     *
     * @return array|null
     */
    public function getCMSRolesAssociated()
    {
    }
}
