<?php

namespace PrestaShop\PrestaShop\Core\Shop\Url;

/**
 * Interface UrlProviderInterface
 */
interface UrlProviderInterface
{
    /**
     * @return string
     */
    public function getUrl();
}
