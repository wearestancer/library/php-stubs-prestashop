<?php

namespace PrestaShop\PrestaShop\Core\Shop;

/**
 * Interface ShopContextInterface
 */
interface ShopContextInterface
{
    /**
     * Get name for context shop.
     *
     * @return string
     */
    public function getShopName();
}
