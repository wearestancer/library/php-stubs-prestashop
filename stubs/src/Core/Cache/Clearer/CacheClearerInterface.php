<?php

namespace PrestaShop\PrestaShop\Core\Cache\Clearer;

/**
 * Interface CacheClearerInterface.
 */
interface CacheClearerInterface
{
    /**
     * Clear cache.
     */
    public function clear();
}
