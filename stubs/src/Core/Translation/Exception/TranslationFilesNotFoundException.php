<?php

namespace PrestaShop\PrestaShop\Core\Translation\Exception;

class TranslationFilesNotFoundException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
