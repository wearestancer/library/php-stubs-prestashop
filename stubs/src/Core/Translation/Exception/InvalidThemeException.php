<?php

namespace PrestaShop\PrestaShop\Core\Translation\Exception;

class InvalidThemeException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
