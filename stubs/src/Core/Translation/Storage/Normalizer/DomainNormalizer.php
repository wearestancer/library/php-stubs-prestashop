<?php

namespace PrestaShop\PrestaShop\Core\Translation\Storage\Normalizer;

/**
 * Normalizes domain names by removing dots
 */
class DomainNormalizer
{
    /**
     * @param string $domain Domain name
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public function normalize(string $domain) : string
    {
    }
}
