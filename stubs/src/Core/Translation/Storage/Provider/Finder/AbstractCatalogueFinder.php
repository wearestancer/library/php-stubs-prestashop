<?php

namespace PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Finder;

class AbstractCatalogueFinder
{
    /**
     * Validate if an array only have strings in it.
     *
     * @param array $array
     *
     * @return bool
     */
    protected function assertIsArrayOfString(array $array) : bool
    {
    }
}
