<?php

namespace PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Definition;

/**
 * Properties container for Backoffice translation provider.
 */
class BackofficeProviderDefinition extends \PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Definition\AbstractCoreProviderDefinition
{
    /**
     * {@inheritdoc}
     */
    public function getType() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilenameFilters() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTranslationDomains() : array
    {
    }
}
