<?php

namespace PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Definition;

class ProviderDefinitionFactory
{
    public function build(string $type, ?string $selectedValue = null) : \PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Definition\ProviderDefinitionInterface
    {
    }
}
