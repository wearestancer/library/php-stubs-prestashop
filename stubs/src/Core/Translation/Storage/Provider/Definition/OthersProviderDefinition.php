<?php

namespace PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Definition;

/**
 * Properties container for Backoffice translation provider.
 */
class OthersProviderDefinition extends \PrestaShop\PrestaShop\Core\Translation\Storage\Provider\Definition\AbstractCoreProviderDefinition
{
    public const OTHERS_DOMAIN_NAME = 'messages';
    /**
     * {@inheritdoc}
     */
    public function getType() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilenameFilters() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTranslationDomains() : array
    {
    }
}
