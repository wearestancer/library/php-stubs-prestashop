<?php

namespace PrestaShop\PrestaShop\Core\CommandBus\Parser;

/**
 * Parses commands and queries definitions
 */
final class CommandDefinitionParser
{
    /**
     * @param string $commandName
     *
     * @return CommandDefinition
     *
     * @throws ReflectionException
     */
    public function parseDefinition($commandName)
    {
    }
}
