<?php

namespace PrestaShop\PrestaShop\Core\Configuration;

/**
 * Gets ini configuration.
 */
class IniConfiguration
{
    /**
     * Get max post max size from ini configuration in bytes.
     *
     * @return int
     */
    public function getPostMaxSizeInBytes()
    {
    }
    /**
     * Get maximum upload size allowed by the server in bytes.
     *
     * @return int
     */
    public function getUploadMaxSizeInBytes()
    {
    }
}
