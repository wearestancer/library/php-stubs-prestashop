<?php

namespace PrestaShop\PrestaShop\Core\Configuration;

/**
 * Class PhpExtensionChecker provides object-oriented way to check if PHP extensions are loaded.
 */
final class PhpExtensionChecker implements \PrestaShop\PrestaShop\Core\Configuration\PhpExtensionCheckerInterface
{
    /**
     * {@inheritdoc}
     */
    public function loaded($extension)
    {
    }
}
