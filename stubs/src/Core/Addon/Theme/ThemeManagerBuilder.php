<?php

namespace PrestaShop\PrestaShop\Core\Addon\Theme;

class ThemeManagerBuilder
{
    public function __construct(\Context $context, \Db $db, \PrestaShop\PrestaShop\Core\Addon\Theme\ThemeValidator $themeValidator = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function build()
    {
    }
    public function buildRepository(\Shop $shop = null)
    {
    }
}
