<?php

namespace PrestaShop\PrestaShop\Core\Addon\Theme;

class ThemeValidator
{
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator, \PrestaShop\PrestaShop\Core\ConfigurationInterface $configuration)
    {
    }
    public function getErrors($themeName)
    {
    }
    public function isValid(\PrestaShop\PrestaShop\Core\Addon\Theme\Theme $theme)
    {
    }
    public function getRequiredProperties()
    {
    }
    public function getRequiredFiles()
    {
    }
}
