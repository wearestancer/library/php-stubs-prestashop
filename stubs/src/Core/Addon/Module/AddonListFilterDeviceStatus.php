<?php

namespace PrestaShop\PrestaShop\Core\Addon\Module;

class AddonListFilterDeviceStatus
{
    public const DEVICE_COMPUTER = 1;
    public const DEVICE_TABLET = 2;
    public const DEVICE_MOBILE = 4;
    public const ALL = 7;
}
