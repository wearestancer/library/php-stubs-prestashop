<?php

namespace PrestaShop\PrestaShop\Core\Addon;

class AddonListItem
{
    public $name;
    public $displayName;
    public $type;
    public $author;
    public $version;
    public $isEnabled;
}
