<?php

namespace PrestaShop\PrestaShop\Core\ConstraintValidator;

/**
 * Validates ranges of date range
 */
class DateRangeValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
