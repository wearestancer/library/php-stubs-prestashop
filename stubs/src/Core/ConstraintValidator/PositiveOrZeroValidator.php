<?php

namespace PrestaShop\PrestaShop\Core\ConstraintValidator;

/**
 * Validates @see PositiveOrZero constraint
 */
class PositiveOrZeroValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * {@inheritDoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
