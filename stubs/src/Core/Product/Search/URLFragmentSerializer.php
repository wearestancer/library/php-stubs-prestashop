<?php

namespace PrestaShop\PrestaShop\Core\Product\Search;

/**
 * This class is a serializer for URL fragments.
 *
 * @deprecated since version 1.7.8 and will be removed in the next major version.
 */
class URLFragmentSerializer
{
    /**
     * @param array $fragment
     *
     * @return string
     */
    public function serialize(array $fragment)
    {
    }
    /**
     * @param string $string
     *
     * @return array
     */
    public function unserialize($string)
    {
    }
}
