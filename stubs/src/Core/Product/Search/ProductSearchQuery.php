<?php

namespace PrestaShop\PrestaShop\Core\Product\Search;

/**
 * Define the Product Query to execute according the the encoded facets.
 */
class ProductSearchQuery
{
    /**
     * ProductSearchQuery constructor.
     */
    public function __construct()
    {
    }
    /**
     * @param string $queryType
     *
     * @return $this
     */
    public function setQueryType($queryType)
    {
    }
    /**
     * @return string
     */
    public function getQueryType()
    {
    }
    /**
     * @param int $idCategory
     *
     * @return $this
     */
    public function setIdCategory($idCategory)
    {
    }
    /**
     * @return int
     */
    public function getIdCategory()
    {
    }
    /**
     * @param int $idManufacturer
     *
     * @return $this
     */
    public function setIdManufacturer($idManufacturer)
    {
    }
    /**
     * @return int
     */
    public function getIdManufacturer()
    {
    }
    /**
     * @param int $idSupplier
     *
     * @return $this
     */
    public function setIdSupplier($idSupplier)
    {
    }
    /**
     * @return int
     */
    public function getIdSupplier()
    {
    }
    /**
     * @param int $resultsPerPage
     *
     * @return $this
     */
    public function setResultsPerPage($resultsPerPage)
    {
    }
    /**
     * @return int
     */
    public function getResultsPerPage()
    {
    }
    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage($page)
    {
    }
    /**
     * @return int
     */
    public function getPage()
    {
    }
    /**
     * @param SortOrder $sortOrder
     *
     * @return $this
     */
    public function setSortOrder(\PrestaShop\PrestaShop\Core\Product\Search\SortOrder $sortOrder)
    {
    }
    /**
     * @return SortOrder
     */
    public function getSortOrder()
    {
    }
    /**
     * @param string $searchString
     *
     * @return $this
     */
    public function setSearchString($searchString)
    {
    }
    /**
     * @return string
     */
    public function getSearchString()
    {
    }
    /**
     * @param string $searchTag
     *
     * @return $this
     */
    public function setSearchTag($searchTag)
    {
    }
    /**
     * @return string
     */
    public function getSearchTag()
    {
    }
    /**
     * @param array|string $encodedFacets
     *
     * @return $this
     */
    public function setEncodedFacets($encodedFacets)
    {
    }
    /**
     * @return array|string
     */
    public function getEncodedFacets()
    {
    }
}
