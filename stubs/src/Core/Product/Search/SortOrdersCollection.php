<?php

namespace PrestaShop\PrestaShop\Core\Product\Search;

/**
 * This class provide the list of default Sort Orders.
 */
final class SortOrdersCollection
{
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * @return array
     *
     * @throws \Exception
     */
    public function getDefaults()
    {
    }
}
