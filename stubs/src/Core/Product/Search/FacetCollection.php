<?php

namespace PrestaShop\PrestaShop\Core\Product\Search;

/**
 * Stores a list of facets.
 */
class FacetCollection
{
    /**
     * @param Facet $facet the facet to add
     *
     * @return $this
     */
    public function addFacet(\PrestaShop\PrestaShop\Core\Product\Search\Facet $facet)
    {
    }
    /**
     * @param array $facets the facets to add
     *
     * @return $this
     */
    public function setFacets(array $facets)
    {
    }
    /**
     * @return array returns the list of facets
     */
    public function getFacets()
    {
    }
}
