<?php

namespace PrestaShop\PrestaShop\Core\Product\Search;

class Pagination
{
    /**
     * @param int $pagesCount
     *
     * @return $this
     */
    public function setPagesCount($pagesCount)
    {
    }
    /**
     * @return int
     */
    public function getPagesCount()
    {
    }
    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage($page)
    {
    }
    /**
     * @return int
     */
    public function getPage()
    {
    }
    /**
     * @return array
     */
    public function buildLinks()
    {
    }
}
