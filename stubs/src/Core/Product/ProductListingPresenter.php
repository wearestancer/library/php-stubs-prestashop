<?php

namespace PrestaShop\PrestaShop\Core\Product;

/**
 * @deprecated since 1.7.4.0
 * @see \PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductListingPresenter
 *
 * Class ProductListingPresenter
 */
class ProductListingPresenter extends \PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductListingPresenter
{
}
