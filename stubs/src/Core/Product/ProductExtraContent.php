<?php

namespace PrestaShop\PrestaShop\Core\Product;

class ProductExtraContent implements \PrestaShopBundle\Service\Hook\HookContentClassInterface
{
    public function getTitle()
    {
    }
    public function getContent()
    {
    }
    public function getAttr()
    {
    }
    public function setTitle($title)
    {
    }
    public function setContent($content)
    {
    }
    public function addAttr($attr)
    {
    }
    public function setAttr($attr)
    {
    }
    public function toArray()
    {
    }
}
