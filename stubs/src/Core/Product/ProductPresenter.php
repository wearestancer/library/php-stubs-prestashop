<?php

namespace PrestaShop\PrestaShop\Core\Product;

/**
 * @deprecated since 1.7.4.0
 * @see \PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductPresenter
 *
 * Class ProductPresenter
 */
class ProductPresenter extends \PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductPresenter
{
}
