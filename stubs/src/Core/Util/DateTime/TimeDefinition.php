<?php

namespace PrestaShop\PrestaShop\Core\Util\DateTime;

/**
 * Class TimeDefinition defines common time intervals in different formats.
 */
final class TimeDefinition
{
    public const HOUR_IN_SECONDS = 3600;
    public const DAY_IN_SECONDS = 86400;
}
