<?php

namespace PrestaShop\PrestaShop\Core\Util\File;

/**
 * Converts int value to formatted file size value
 */
class FileSizeConverter
{
    /**
     * @param int $bytes
     * @param int $precision
     *
     * @return string
     */
    public function convert(int $bytes, int $precision = 2) : string
    {
    }
}
