<?php

namespace PrestaShop\PrestaShop\Core\Util\Url;

/**
 * Class UrlFileChecker
 */
final class UrlFileChecker implements \PrestaShop\PrestaShop\Core\Util\Url\UrlFileCheckerInterface
{
    /**
     * @param string $fileDir
     */
    public function __construct($fileDir)
    {
    }
    /**
     * @return bool
     */
    public function isHtaccessFileWritable()
    {
    }
    /**
     * @return bool
     */
    public function isRobotsFileWritable()
    {
    }
}
