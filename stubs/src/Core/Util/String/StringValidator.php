<?php

namespace PrestaShop\PrestaShop\Core\Util\String;

/**
 * This class defines reusable methods for checking strings under certain conditions.
 */
final class StringValidator implements \PrestaShop\PrestaShop\Core\Util\String\StringValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function startsWith($string, $prefix)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function endsWith($string, $suffix)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function startsWithAndEndsWith($string, $prefix, $suffix)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function doesContainsWhiteSpaces($string)
    {
    }
}
