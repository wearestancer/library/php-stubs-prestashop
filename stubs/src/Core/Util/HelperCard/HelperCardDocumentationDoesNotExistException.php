<?php

namespace PrestaShop\PrestaShop\Core\Util\HelperCard;

/**
 * Class HelperCardDocumentationDoesNotExistException
 */
class HelperCardDocumentationDoesNotExistException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
