<?php

namespace PrestaShop\PrestaShop\Core\Image\Exception;

class ImageFormatConfigurationException extends \PrestaShop\PrestaShop\Core\Image\Exception\ImageException
{
}
