<?php

namespace PrestaShop\PrestaShop\Core\Image\Exception;

/**
 * Thrown when image file unlink fails
 */
class CannotUnlinkImageException extends \PrestaShop\PrestaShop\Core\File\Exception\CannotUnlinkFileException
{
}
