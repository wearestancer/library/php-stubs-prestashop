<?php

namespace PrestaShop\PrestaShop\Core\Image\Exception;

/**
 * Class ImageOptimizationException is thrown when resizing, cutting or optimizing image fails.
 */
class ImageOptimizationException extends \PrestaShop\PrestaShop\Core\Image\Exception\ImageException
{
}
