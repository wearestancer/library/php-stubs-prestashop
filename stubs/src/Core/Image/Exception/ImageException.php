<?php

namespace PrestaShop\PrestaShop\Core\Image\Exception;

/**
 * Base exception for image infrastructure related errors
 */
class ImageException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
