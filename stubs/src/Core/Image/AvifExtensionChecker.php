<?php

namespace PrestaShop\PrestaShop\Core\Image;

/**
 * Class AvifExtensionChecker provides object-oriented way to check if AVIF extension is installed and available.
 */
class AvifExtensionChecker
{
    public function isAvailable() : bool
    {
    }
}
