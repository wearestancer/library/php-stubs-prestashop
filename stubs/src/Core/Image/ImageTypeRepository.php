<?php

namespace PrestaShop\PrestaShop\Core\Image;

class ImageTypeRepository
{
    public function __construct(\Db $db)
    {
    }
    public function setTypes(array $types)
    {
    }
    public function createType($name, $width, $height, array $scope)
    {
    }
    public function getScopeList()
    {
    }
    public function getIdByName($name)
    {
    }
    protected function removeAllTypes()
    {
    }
}
