<?php

namespace PrestaShop\PrestaShop\Core\Image\Uploader\Exception;

/**
 * Thrown when file by provided path was not found
 */
class ImageFileNotFoundException extends \PrestaShop\PrestaShop\Core\Image\Exception\ImageException
{
}
