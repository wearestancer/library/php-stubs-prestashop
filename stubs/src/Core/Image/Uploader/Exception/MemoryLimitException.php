<?php

namespace PrestaShop\PrestaShop\Core\Image\Uploader\Exception;

class MemoryLimitException extends \PrestaShop\PrestaShop\Core\Image\Exception\ImageException
{
}
