<?php

namespace PrestaShop\PrestaShop\Core\Image\Uploader\Exception;

class ImageUploadException extends \PrestaShop\PrestaShop\Core\Image\Exception\ImageException
{
}
