<?php

namespace PrestaShop\PrestaShop\Core\Foundation\Database;

class Exception extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
