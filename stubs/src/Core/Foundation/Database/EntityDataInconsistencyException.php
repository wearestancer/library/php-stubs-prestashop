<?php

namespace PrestaShop\PrestaShop\Core\Foundation\Database;

class EntityDataInconsistencyException extends \PrestaShop\PrestaShop\Core\Foundation\Database\Exception
{
}
