<?php

namespace PrestaShop\PrestaShop\Core\Foundation\Database;

class EntityNotFoundException extends \PrestaShop\PrestaShop\Core\Foundation\Database\Exception
{
}
