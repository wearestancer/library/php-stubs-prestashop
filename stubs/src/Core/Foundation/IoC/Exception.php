<?php

namespace PrestaShop\PrestaShop\Core\Foundation\IoC;

class Exception extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
