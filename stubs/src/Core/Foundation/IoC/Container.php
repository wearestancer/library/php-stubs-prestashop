<?php

namespace PrestaShop\PrestaShop\Core\Foundation\IoC;

class Container
{
    public function knows($serviceName)
    {
    }
    public function bind($serviceName, $constructor, $shared = false)
    {
    }
    public function aliasNamespace($alias, $namespacePrefix)
    {
    }
    public function resolveClassName($className)
    {
    }
    public function make($serviceName)
    {
    }
}
