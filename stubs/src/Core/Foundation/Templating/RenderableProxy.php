<?php

namespace PrestaShop\PrestaShop\Core\Foundation\Templating;

class RenderableProxy
{
    public function __construct(\PrestaShop\PrestaShop\Core\Foundation\Templating\RenderableInterface $renderable)
    {
    }
    public function setTemplate($templatePath)
    {
    }
    public function getTemplate()
    {
    }
    public function render(array $extraParams = [])
    {
    }
}
