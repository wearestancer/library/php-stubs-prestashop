<?php

namespace PrestaShop\PrestaShop\Core\Foundation\Filesystem;

class Exception extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
