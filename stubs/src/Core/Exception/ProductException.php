<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * Class ProductException used when an error linked to a product occurs.
 */
class ProductException extends \PrestaShop\PrestaShop\Core\Exception\TranslatableCoreException
{
}
