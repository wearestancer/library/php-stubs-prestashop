<?php

namespace PrestaShop\PrestaShop\Core\Exception;

class InvalidArgumentException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
