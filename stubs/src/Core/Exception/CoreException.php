<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * Base class for PrestaShop core exceptions
 */
class CoreException extends \Exception
{
}
