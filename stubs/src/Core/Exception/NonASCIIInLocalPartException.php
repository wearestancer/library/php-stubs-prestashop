<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * Exception thrown when email local part contains non-ascii characters
 */
class NonASCIIInLocalPartException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
