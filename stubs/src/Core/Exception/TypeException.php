<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * Class InvalidArgumentException is thrown when the provided argument of
 * a class method is invalid.
 */
class TypeException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
