<?php

namespace PrestaShop\PrestaShop\Core\Exception;

class NotImplementedException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
