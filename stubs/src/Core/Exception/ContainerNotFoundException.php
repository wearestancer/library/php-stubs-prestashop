<?php

namespace PrestaShop\PrestaShop\Core\Exception;

class ContainerNotFoundException extends \Exception
{
}
