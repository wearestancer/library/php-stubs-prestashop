<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * Used for exceptions which messages are already translated and can be displayed straight to end-user
 */
class TranslatedException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
