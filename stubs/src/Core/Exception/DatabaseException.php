<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * Class DatabaseException any error linked to the database use this class as a base.
 */
class DatabaseException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
