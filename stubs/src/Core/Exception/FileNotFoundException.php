<?php

namespace PrestaShop\PrestaShop\Core\Exception;

/**
 * @todo: deprecate this one and replace all usages with Core/File/Exception/FileNotFoundException in separate pr
 *
 * @see \PrestaShop\PrestaShop\Core\File\Exception\FileNotFoundException
 */
class FileNotFoundException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
