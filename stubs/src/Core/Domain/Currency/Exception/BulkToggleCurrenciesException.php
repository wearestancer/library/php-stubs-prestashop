<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Exception is thrown on currencies bulk toggle failure
 */
class BulkToggleCurrenciesException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\BulkDeleteCurrenciesException
{
}
