<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Is thrown when new currency cannot be created
 */
class CannotCreateCurrencyException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\CurrencyException
{
}
