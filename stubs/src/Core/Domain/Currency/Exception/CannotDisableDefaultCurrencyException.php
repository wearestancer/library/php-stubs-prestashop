<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Is thrown when currency cannot be disabled because it is default
 */
class CannotDisableDefaultCurrencyException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\CurrencyException
{
}
