<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Is thrown when required currency cannot be found
 */
class CurrencyNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\CurrencyException
{
}
