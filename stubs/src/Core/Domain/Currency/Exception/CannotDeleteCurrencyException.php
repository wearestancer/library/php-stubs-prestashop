<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Is thrown on failure while deleting currency
 */
class CannotDeleteCurrencyException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\CurrencyException
{
}
