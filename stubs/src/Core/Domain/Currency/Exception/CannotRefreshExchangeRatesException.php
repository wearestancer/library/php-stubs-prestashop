<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Is thrown when refreshing currency exchange rates fails
 */
class CannotRefreshExchangeRatesException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\CurrencyException
{
}
