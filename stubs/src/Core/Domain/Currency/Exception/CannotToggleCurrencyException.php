<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Exception;

/**
 * Is thrown when toggling currency status fails
 */
class CannotToggleCurrencyException extends \PrestaShop\PrestaShop\Core\Domain\Currency\Exception\CurrencyException
{
}
