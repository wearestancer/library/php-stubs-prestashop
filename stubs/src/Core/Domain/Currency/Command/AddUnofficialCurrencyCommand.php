<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Command;

/**
 * Class AddUnofficialCurrencyCommand used to add an alternative currency
 */
class AddUnofficialCurrencyCommand extends \PrestaShop\PrestaShop\Core\Domain\Currency\Command\AddCurrencyCommand
{
}
