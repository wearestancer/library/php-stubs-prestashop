<?php

namespace PrestaShop\PrestaShop\Core\Domain\Currency\Query;

/**
 * Class GetCurrencyForEditing
 */
class GetCurrencyForEditing
{
    /**
     * @param int $currencyId
     *
     * @throws CurrencyException
     */
    public function __construct($currencyId)
    {
    }
    /**
     * @return CurrencyId
     */
    public function getCurrencyId()
    {
    }
}
