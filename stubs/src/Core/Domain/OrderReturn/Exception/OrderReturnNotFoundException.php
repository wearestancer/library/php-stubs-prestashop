<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturn\Exception;

/**
 * Is thrown when order return is not found in order return subdomain
 */
class OrderReturnNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\OrderReturn\Exception\OrderReturnException
{
}
