<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturn\Exception;

/**
 * Is thrown when cannot update order return
 */
class UpdateOrderReturnException extends \PrestaShop\PrestaShop\Core\Domain\OrderReturn\Exception\OrderReturnException
{
}
