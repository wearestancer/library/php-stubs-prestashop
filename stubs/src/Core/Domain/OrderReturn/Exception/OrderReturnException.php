<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturn\Exception;

/**
 * Base exception for order return subdomain
 */
class OrderReturnException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
