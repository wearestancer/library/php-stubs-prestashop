<?php

namespace PrestaShop\PrestaShop\Core\Domain\SearchEngine\Exception;

/**
 * Class SearchEngineException is base search engine exception.
 */
class SearchEngineException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
