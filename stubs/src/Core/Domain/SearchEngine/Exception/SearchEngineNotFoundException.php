<?php

namespace PrestaShop\PrestaShop\Core\Domain\SearchEngine\Exception;

/**
 * Is thrown when search engine is not found by provided ID.
 */
class SearchEngineNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\SearchEngine\Exception\SearchEngineException
{
}
