<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Query;

/**
 * Retrieves feature data for editing
 */
class GetFeatureForEditing
{
    /**
     * @param int $featureId
     */
    public function __construct($featureId)
    {
    }
    /**
     * @return FeatureId
     */
    public function getFeatureId()
    {
    }
}
