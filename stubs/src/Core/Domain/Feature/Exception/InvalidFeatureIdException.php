<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when invalid feature id is used
 */
class InvalidFeatureIdException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureException
{
}
