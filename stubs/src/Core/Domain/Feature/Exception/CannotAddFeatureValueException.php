<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when feature value cannot be added.
 */
class CannotAddFeatureValueException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureValueException
{
}
