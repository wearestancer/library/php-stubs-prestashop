<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when feature cannot be edited.
 */
class CannotEditFeatureException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureException
{
}
