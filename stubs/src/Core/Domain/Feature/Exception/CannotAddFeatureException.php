<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when feature cannot be added.
 */
class CannotAddFeatureException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureException
{
}
