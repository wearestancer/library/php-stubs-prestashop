<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Base exception for FeatureValue domain
 */
class FeatureValueException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureException
{
}
