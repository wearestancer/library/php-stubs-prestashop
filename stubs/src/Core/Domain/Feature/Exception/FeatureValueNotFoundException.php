<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when feature value is not found.
 */
class FeatureValueNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureValueException
{
}
