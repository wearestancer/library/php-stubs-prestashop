<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown in Feature context.
 */
class FeatureException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
