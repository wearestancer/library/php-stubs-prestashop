<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when feature could not be found
 */
class FeatureNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureException
{
}
