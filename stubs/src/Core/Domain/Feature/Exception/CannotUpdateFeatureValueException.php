<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when feature value cannot be updated.
 */
class CannotUpdateFeatureValueException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureValueException
{
}
