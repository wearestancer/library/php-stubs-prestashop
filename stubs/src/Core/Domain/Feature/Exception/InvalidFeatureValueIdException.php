<?php

namespace PrestaShop\PrestaShop\Core\Domain\Feature\Exception;

/**
 * Thrown when invalid feature value id is used
 */
class InvalidFeatureValueIdException extends \PrestaShop\PrestaShop\Core\Domain\Feature\Exception\FeatureValueException
{
}
