<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturnState\Exception;

/**
 * Class InvalidOrderReturnStateIdException is thrown when OrderReturnStateId with invalid value is being created.
 */
class InvalidOrderReturnStateIdException extends \PrestaShop\PrestaShop\Core\Domain\OrderReturnState\Exception\OrderReturnStateException
{
}
