<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturnState\Exception;

/**
 * Thrown when trying to set invalid required fields for order state
 */
class InvalidOrderReturnStateRequiredFieldsException extends \PrestaShop\PrestaShop\Core\Domain\OrderReturnState\Exception\OrderReturnStateException
{
}
