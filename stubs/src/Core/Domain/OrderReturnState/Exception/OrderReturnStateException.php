<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturnState\Exception;

/**
 * Class OrderReturnStateException is base "OrderReturnState" context exception
 */
class OrderReturnStateException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
