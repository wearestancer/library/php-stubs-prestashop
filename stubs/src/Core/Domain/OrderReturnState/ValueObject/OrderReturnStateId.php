<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderReturnState\ValueObject;

/**
 * Defines OrderReturnState ID with it's constraints
 */
class OrderReturnStateId
{
    /**
     * @param int $orderReturnStateId
     */
    public function __construct($orderReturnStateId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
