<?php

namespace PrestaShop\PrestaShop\Core\Domain\Supplier\Exception;

/**
 * Is thrown when supplier product relation cannot be deleted
 */
class CannotDeleteSupplierProductRelationException extends \PrestaShop\PrestaShop\Core\Domain\Supplier\Exception\SupplierException
{
}
