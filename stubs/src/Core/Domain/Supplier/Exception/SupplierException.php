<?php

namespace PrestaShop\PrestaShop\Core\Domain\Supplier\Exception;

/**
 * Base exception for supplier sub-domain
 */
class SupplierException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
