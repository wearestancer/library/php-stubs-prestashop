<?php

namespace PrestaShop\PrestaShop\Core\Domain\Supplier\Exception;

/**
 * Is thrown when supplier status cannot be updated
 */
class CannotUpdateSupplierStatusException extends \PrestaShop\PrestaShop\Core\Domain\Supplier\Exception\SupplierException
{
}
