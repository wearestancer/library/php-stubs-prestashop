<?php

namespace PrestaShop\PrestaShop\Core\Domain\Supplier\Exception;

/**
 * Is thrown when supplier address cannot be deleted
 */
class CannotDeleteSupplierAddressException extends \PrestaShop\PrestaShop\Core\Domain\Supplier\Exception\SupplierException
{
}
