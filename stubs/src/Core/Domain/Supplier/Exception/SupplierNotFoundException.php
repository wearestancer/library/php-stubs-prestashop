<?php

namespace PrestaShop\PrestaShop\Core\Domain\Supplier\Exception;

/**
 * Is thrown when required supplier cannot be found
 */
class SupplierNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Supplier\Exception\SupplierException
{
}
