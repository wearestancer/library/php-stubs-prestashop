<?php

namespace PrestaShop\PrestaShop\Core\Domain\Supplier\Exception;

/**
 * Is thrown when supplier status cannot be enabled or disabled in toggling action
 */
class CannotToggleSupplierStatusException extends \PrestaShop\PrestaShop\Core\Domain\Supplier\Exception\SupplierException
{
}
