<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

class NotSupportedLogoImageExtensionException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
