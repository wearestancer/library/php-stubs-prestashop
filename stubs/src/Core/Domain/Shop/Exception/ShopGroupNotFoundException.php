<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

class ShopGroupNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopGroupException
{
}
