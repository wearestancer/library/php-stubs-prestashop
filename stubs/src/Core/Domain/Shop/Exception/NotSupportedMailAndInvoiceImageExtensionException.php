<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

class NotSupportedMailAndInvoiceImageExtensionException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
