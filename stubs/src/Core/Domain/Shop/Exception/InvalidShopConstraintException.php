<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Exception thrown when a shop constraint is built or used with invalid values.
 */
class InvalidShopConstraintException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
