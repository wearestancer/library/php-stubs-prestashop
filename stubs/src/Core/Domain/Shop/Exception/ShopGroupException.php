<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Base exception for shopGroup subdomain
 */
class ShopGroupException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
