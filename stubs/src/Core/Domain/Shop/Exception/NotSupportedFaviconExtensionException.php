<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Class NotSupportedFaviconExtensionException
 */
class NotSupportedFaviconExtensionException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
