<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Thrown when a shop association is checked but is nonexistent.
 */
class ShopAssociationNotFound extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
