<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Class ShopException is base exception for Shop domain.
 */
class ShopException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
