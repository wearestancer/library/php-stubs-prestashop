<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

class SearchShopException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
