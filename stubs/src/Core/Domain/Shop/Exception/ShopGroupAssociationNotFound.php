<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Thrown when a shop group association is checked but is nonexistent.
 */
class ShopGroupAssociationNotFound extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
