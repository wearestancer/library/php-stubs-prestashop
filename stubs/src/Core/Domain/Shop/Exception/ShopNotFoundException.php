<?php

namespace PrestaShop\PrestaShop\Core\Domain\Shop\Exception;

/**
 * Thrown when a shop is nonexistent.
 */
class ShopNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException
{
}
