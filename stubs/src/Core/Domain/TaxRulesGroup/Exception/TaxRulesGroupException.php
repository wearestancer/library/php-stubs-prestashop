<?php

namespace PrestaShop\PrestaShop\Core\Domain\TaxRulesGroup\Exception;

/**
 * Base exception for tax rules group subdomain
 */
class TaxRulesGroupException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
