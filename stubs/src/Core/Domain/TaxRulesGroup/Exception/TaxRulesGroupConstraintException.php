<?php

namespace PrestaShop\PrestaShop\Core\Domain\TaxRulesGroup\Exception;

/**
 * Thrown when tax rules group constraint is violated
 */
class TaxRulesGroupConstraintException extends \PrestaShop\PrestaShop\Core\Domain\TaxRulesGroup\Exception\TaxRulesGroupException
{
    /**
     * Thrown when provided tax rules group id value is not valid
     */
    public const INVALID_ID = 1;
    /**
     * @var int - error is raised when a value in array is not integer type
     */
    public const INVALID_SHOP_ASSOCIATION = 2;
    public const INVALID_NAME = 3;
    public const INVALID_ACTIVE = 4;
    public const INVALID_DELETED = 5;
    public const INVALID_CREATION_DATE = 6;
    public const INVALID_UPDATE_DATE = 7;
}
