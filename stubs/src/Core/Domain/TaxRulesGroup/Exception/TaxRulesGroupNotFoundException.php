<?php

namespace PrestaShop\PrestaShop\Core\Domain\TaxRulesGroup\Exception;

/**
 * Thrown on failure to find existing tax rules group
 */
class TaxRulesGroupNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\TaxRulesGroup\Exception\TaxRulesGroupException
{
}
