<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderState\ValueObject;

/**
 * Defines OrderState ID with it's constraints
 */
class OrderStateId
{
    /**
     * @param int $orderStateId
     */
    public function __construct($orderStateId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
