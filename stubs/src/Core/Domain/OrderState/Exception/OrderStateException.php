<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderState\Exception;

/**
 * Class OrderStateException is base "OrderState" context exception
 */
class OrderStateException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
