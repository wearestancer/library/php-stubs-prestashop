<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderState\Exception;

/**
 * Thrown when trying to set invalid required fields for order state
 */
class InvalidOrderStateRequiredFieldsException extends \PrestaShop\PrestaShop\Core\Domain\OrderState\Exception\OrderStateException
{
}
