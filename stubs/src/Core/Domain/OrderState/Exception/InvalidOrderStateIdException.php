<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderState\Exception;

/**
 * Class InvalidOrderStateIdException is thrown when OrderStateId with invalid value is being created.
 */
class InvalidOrderStateIdException extends \PrestaShop\PrestaShop\Core\Domain\OrderState\Exception\OrderStateException
{
}
