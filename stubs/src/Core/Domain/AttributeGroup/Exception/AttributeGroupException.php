<?php

namespace PrestaShop\PrestaShop\Core\Domain\AttributeGroup\Exception;

/**
 * Base exception for attribute group subdomain
 */
class AttributeGroupException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
