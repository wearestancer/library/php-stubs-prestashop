<?php

namespace PrestaShop\PrestaShop\Core\Domain\AttributeGroup\Exception;

/**
 * Is thrown when required attribute group cannot be found
 */
class AttributeGroupNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\AttributeGroup\Exception\AttributeGroupException
{
}
