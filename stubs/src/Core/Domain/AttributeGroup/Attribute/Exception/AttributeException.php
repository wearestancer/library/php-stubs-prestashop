<?php

namespace PrestaShop\PrestaShop\Core\Domain\AttributeGroup\Attribute\Exception;

/**
 * Base exception for attribute subdomain
 */
class AttributeException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
