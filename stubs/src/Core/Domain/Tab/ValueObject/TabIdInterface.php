<?php

namespace PrestaShop\PrestaShop\Core\Domain\Tab\ValueObject;

interface TabIdInterface
{
    public function getValue() : int;
}
