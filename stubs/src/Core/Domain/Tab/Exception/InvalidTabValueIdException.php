<?php

namespace PrestaShop\PrestaShop\Core\Domain\Tab\Exception;

/**
 * Base exception for TabValue domain
 */
class InvalidTabValueIdException extends \PrestaShop\PrestaShop\Core\Domain\Tab\Exception\TabValueException
{
}
