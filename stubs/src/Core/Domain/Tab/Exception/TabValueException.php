<?php

namespace PrestaShop\PrestaShop\Core\Domain\Tab\Exception;

/**
 * Base exception for TabValue domain
 */
class TabValueException extends \PrestaShop\PrestaShop\Core\Domain\Tab\Exception\TabException
{
}
