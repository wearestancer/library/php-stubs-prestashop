<?php

namespace PrestaShop\PrestaShop\Core\Domain\Tab\Exception;

/**
 * Thrown in Tab context.
 */
class TabException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
