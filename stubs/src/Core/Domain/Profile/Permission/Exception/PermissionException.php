<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Permission\Exception;

/**
 * Is base exception for Permission subdomain
 */
class PermissionException extends \PrestaShop\PrestaShop\Core\Domain\Profile\Exception\ProfileException
{
}
