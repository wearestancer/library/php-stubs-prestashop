<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Permission\Exception;

/**
 * Is thrown when updating permissions fails
 */
class PermissionUpdateException extends \PrestaShop\PrestaShop\Core\Domain\Profile\Permission\Exception\PermissionException
{
}
