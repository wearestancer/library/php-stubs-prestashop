<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Permission\Exception;

/**
 * Invalid Permission value base on PermissionException
 */
class InvalidPermissionValueException extends \PrestaShop\PrestaShop\Core\Domain\Profile\Permission\Exception\PermissionException
{
}
