<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Exception;

/**
 * Class ProfileException is a base exception for profiles context.
 */
class ProfileException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
