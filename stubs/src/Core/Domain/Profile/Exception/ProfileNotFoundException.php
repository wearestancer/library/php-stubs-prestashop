<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Exception;

/**
 * Class ProfileNotFoundException is thrown when Profile cannot be found.
 */
class ProfileNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Profile\Exception\ProfileException
{
}
