<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Exception;

/**
 * Class CannotDeleteSuperAdminProfileException thrown on super admin profile deletion attempt.
 */
class CannotDeleteSuperAdminProfileException extends \PrestaShop\PrestaShop\Core\Domain\Profile\Exception\ProfileException
{
}
