<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile;

final class ProfileSettings
{
    /**
     * Profile name max length as defined in the ObjectModel
     */
    public const NAME_MAX_LENGTH = 32;
}
