<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Command;

/**
 * Adds new profile
 */
class AddProfileCommand extends \PrestaShop\PrestaShop\Core\Domain\Profile\Command\AbstractProfileCommand
{
}
