<?php

namespace PrestaShop\PrestaShop\Core\Domain\Profile\Query;

/**
 * Get Profile data for editing
 */
class GetProfileForEditing
{
    /**
     * @param int $profileId
     */
    public function __construct($profileId)
    {
    }
    /**
     * @return ProfileId
     */
    public function getProfileId()
    {
    }
}
