<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Class CannotAddCategoryException is throw when adding category fails.
 */
class CannotAddCategoryException extends \PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryException
{
}
