<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Class CategoryException is base exception for Category bounded context.
 */
class CategoryException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
