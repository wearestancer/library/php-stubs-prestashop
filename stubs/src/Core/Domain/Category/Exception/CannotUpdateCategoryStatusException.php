<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Class CannotUpdateCategoryStatusException is thrown when Category status update failed.
 */
class CannotUpdateCategoryStatusException extends \PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryException
{
}
