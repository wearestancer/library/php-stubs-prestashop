<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Class MenuThumbnailsLimitException is thrown when maximum number of thumbnail images is reached.
 */
class MenuThumbnailsLimitException extends \PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryException
{
}
