<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Class CannotEditCategoryException is thrown when editing category fails.
 */
class CannotEditCategoryException extends \PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryException
{
}
