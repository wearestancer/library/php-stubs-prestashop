<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Class CannotEditRootCategoryException is thrown when trying to edit the ROOT category.
 */
class CannotEditRootCategoryException extends \PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryException
{
}
