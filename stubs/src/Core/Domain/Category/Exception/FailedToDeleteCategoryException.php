<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Exception;

/**
 * Is thrown when unable to delete category
 */
class FailedToDeleteCategoryException extends \PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryException
{
}
