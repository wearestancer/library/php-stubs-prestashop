<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\Query;

/**
 * Class GetCategoryForEditing retrieves category data for editing.
 */
class GetCategoryForEditing
{
    /**
     * @param int $categoryId
     */
    public function __construct($categoryId)
    {
    }
    /**
     * @return CategoryId
     */
    public function getCategoryId()
    {
    }
}
