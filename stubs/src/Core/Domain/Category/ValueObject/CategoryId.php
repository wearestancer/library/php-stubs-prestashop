<?php

namespace PrestaShop\PrestaShop\Core\Domain\Category\ValueObject;

/**
 * Class CategoryId.
 */
class CategoryId
{
    /**
     * @param int $categoryId
     */
    public function __construct($categoryId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
    /**
     * @param CategoryId $categoryId
     *
     * @return bool
     */
    public function isEqual(\PrestaShop\PrestaShop\Core\Domain\Category\ValueObject\CategoryId $categoryId)
    {
    }
}
