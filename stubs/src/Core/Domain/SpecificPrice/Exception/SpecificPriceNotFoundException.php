<?php

namespace PrestaShop\PrestaShop\Core\Domain\SpecificPrice\Exception;

/**
 * @deprecated since 8.0.0 and will be removed in the next major version.
 * @see ProductSpecificPriceNotFoundException
 */
class SpecificPriceNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\SpecificPrice\Exception\SpecificPriceException
{
}
