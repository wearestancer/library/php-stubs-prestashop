<?php

namespace PrestaShop\PrestaShop\Core\Domain\SpecificPrice\Exception;

/**
 * @deprecated since 8.0.0 and will be removed in the next major version.
 * @see CannotSetProductSpecificPricePrioritiesException
 */
class CannotSetSpecificPricePrioritiesException extends \PrestaShop\PrestaShop\Core\Domain\SpecificPrice\Exception\SpecificPriceException
{
}
