<?php

namespace PrestaShop\PrestaShop\Core\Domain\Store\ValueObject;

/**
 * Contains store ID with it's constraints
 */
class StoreId
{
    /**
     * @param int $storeId
     */
    public function __construct(int $storeId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
