<?php

namespace PrestaShop\PrestaShop\Core\Domain\Store\Exception;

/**
 * Thrown when cannot toggle store status
 */
class CannotUpdateStoreException extends \PrestaShop\PrestaShop\Core\Domain\Store\Exception\StoreException
{
}
