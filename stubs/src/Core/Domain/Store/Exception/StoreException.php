<?php

namespace PrestaShop\PrestaShop\Core\Domain\Store\Exception;

class StoreException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
