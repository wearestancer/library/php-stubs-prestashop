<?php

namespace PrestaShop\PrestaShop\Core\Domain\Store\Exception;

/**
 * Thrown when store was not found
 */
class StoreNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Store\Exception\StoreException
{
}
