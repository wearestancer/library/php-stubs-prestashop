<?php

namespace PrestaShop\PrestaShop\Core\Domain\Store\Exception;

/**
 * Thrown when cannot delete store
 */
class CannotDeleteStoreException extends \PrestaShop\PrestaShop\Core\Domain\Store\Exception\StoreException
{
}
