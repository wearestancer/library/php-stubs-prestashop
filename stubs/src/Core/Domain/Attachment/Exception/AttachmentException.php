<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

/**
 * Base exception for Attachment subdomain
 */
class AttachmentException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
