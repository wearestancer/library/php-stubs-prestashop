<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

/**
 * Attachment not found exception
 */
class AttachmentNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Attachment\Exception\AttachmentException
{
}
