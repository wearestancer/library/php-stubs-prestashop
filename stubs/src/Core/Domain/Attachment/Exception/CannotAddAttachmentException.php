<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

/**
 * Attachment can not be added exception
 */
class CannotAddAttachmentException extends \PrestaShop\PrestaShop\Core\Domain\Attachment\Exception\AttachmentException
{
}
