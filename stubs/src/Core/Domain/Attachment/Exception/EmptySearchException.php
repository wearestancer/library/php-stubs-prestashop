<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

class EmptySearchException extends \PrestaShop\PrestaShop\Core\Domain\Attachment\Exception\AttachmentException
{
}
