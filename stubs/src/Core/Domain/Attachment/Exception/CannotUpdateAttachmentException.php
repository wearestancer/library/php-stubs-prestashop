<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

/**
 * Is thrown when error occurred on attachment update
 */
class CannotUpdateAttachmentException extends \PrestaShop\PrestaShop\Core\Domain\Attachment\Exception\AttachmentException
{
}
