<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

/**
 * Thrown when upload error occurs
 */
class AttachmentUploadFailedException extends \PrestaShop\PrestaShop\Core\Domain\Attachment\Exception\AttachmentException
{
}
