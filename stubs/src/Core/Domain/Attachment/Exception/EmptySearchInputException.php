<?php

namespace PrestaShop\PrestaShop\Core\Domain\Attachment\Exception;

class EmptySearchInputException extends \PrestaShop\PrestaShop\Core\Domain\Attachment\Exception\AttachmentException
{
}
