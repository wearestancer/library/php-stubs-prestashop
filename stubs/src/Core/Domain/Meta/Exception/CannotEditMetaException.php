<?php

namespace PrestaShop\PrestaShop\Core\Domain\Meta\Exception;

/**
 * Is thrown when meta cannot be edited
 */
class CannotEditMetaException extends \PrestaShop\PrestaShop\Core\Domain\Meta\Exception\MetaException
{
}
