<?php

namespace PrestaShop\PrestaShop\Core\Domain\Meta\Exception;

/**
 * Is thrown when required meta cannot be found
 */
class MetaNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Meta\Exception\MetaException
{
}
