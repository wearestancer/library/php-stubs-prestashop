<?php

namespace PrestaShop\PrestaShop\Core\Domain\Meta\Exception;

/**
 * Is thrown when new meta cannot be added
 */
class CannotAddMetaException extends \PrestaShop\PrestaShop\Core\Domain\Meta\Exception\MetaException
{
}
