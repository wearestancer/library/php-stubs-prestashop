<?php

namespace PrestaShop\PrestaShop\Core\Domain\Meta\Exception;

/**
 * Base exception for meta sub-domain
 */
class MetaException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
