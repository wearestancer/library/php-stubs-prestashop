<?php

namespace PrestaShop\PrestaShop\Core\Domain\Meta\QueryResult;

/**
 * Class LayoutCustomizationPage.
 */
class LayoutCustomizationPage
{
    /**
     * @param string $page
     * @param string $title
     * @param string $description
     */
    public function __construct($page, $title, $description)
    {
    }
    /**
     * @return string
     */
    public function getPage()
    {
    }
    /**
     * @return string
     */
    public function getTitle()
    {
    }
    /**
     * @return string
     */
    public function getDescription()
    {
    }
}
