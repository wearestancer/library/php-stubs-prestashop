<?php

namespace PrestaShop\PrestaShop\Core\Domain\Exception;

/**
 * Base class for all domain exceptions
 */
class DomainException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
