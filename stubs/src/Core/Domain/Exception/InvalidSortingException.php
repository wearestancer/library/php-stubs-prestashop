<?php

namespace PrestaShop\PrestaShop\Core\Domain\Exception;

/**
 * Sorting parameter not supported
 */
class InvalidSortingException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
