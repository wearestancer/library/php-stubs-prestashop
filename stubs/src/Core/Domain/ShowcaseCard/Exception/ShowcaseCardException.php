<?php

namespace PrestaShop\PrestaShop\Core\Domain\ShowcaseCard\Exception;

/**
 * Base class for Showcase card exceptions
 */
class ShowcaseCardException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
