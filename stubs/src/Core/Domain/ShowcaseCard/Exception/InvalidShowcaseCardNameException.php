<?php

namespace PrestaShop\PrestaShop\Core\Domain\ShowcaseCard\Exception;

/**
 * Used when referencing an invalid showcase card name
 */
class InvalidShowcaseCardNameException extends \PrestaShop\PrestaShop\Core\Domain\ShowcaseCard\Exception\ShowcaseCardException
{
}
