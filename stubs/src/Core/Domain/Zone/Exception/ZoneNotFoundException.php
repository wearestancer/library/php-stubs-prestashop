<?php

namespace PrestaShop\PrestaShop\Core\Domain\Zone\Exception;

/**
 * Is thrown when required zone cannot be found
 */
class ZoneNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Zone\Exception\ZoneException
{
}
