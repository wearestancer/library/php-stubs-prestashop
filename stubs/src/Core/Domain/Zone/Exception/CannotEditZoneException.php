<?php

namespace PrestaShop\PrestaShop\Core\Domain\Zone\Exception;

/**
 * Is thrown when cannot edit zone.
 */
class CannotEditZoneException extends \PrestaShop\PrestaShop\Core\Domain\Zone\Exception\ZoneException
{
}
