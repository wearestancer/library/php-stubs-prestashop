<?php

namespace PrestaShop\PrestaShop\Core\Domain\Zone\Exception;

/**
 * Is thrown when cannot toggle zone.
 */
class CannotToggleZoneStatusException extends \PrestaShop\PrestaShop\Core\Domain\Zone\Exception\ZoneException
{
}
