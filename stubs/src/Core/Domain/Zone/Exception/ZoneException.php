<?php

namespace PrestaShop\PrestaShop\Core\Domain\Zone\Exception;

/**
 * Base exception for Zone subdomain
 */
class ZoneException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
