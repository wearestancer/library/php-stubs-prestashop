<?php

namespace PrestaShop\PrestaShop\Core\Domain\CatalogPriceRule\Exception;

/**
 * Thrown when unable to update catalog price rule
 */
class CannotUpdateCatalogPriceRuleException extends \PrestaShop\PrestaShop\Core\Domain\CatalogPriceRule\Exception\CatalogPriceRuleException
{
}
