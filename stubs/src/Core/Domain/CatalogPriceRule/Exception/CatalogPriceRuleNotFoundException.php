<?php

namespace PrestaShop\PrestaShop\Core\Domain\CatalogPriceRule\Exception;

/**
 * Thrown when cannot find required catalog price rule
 */
class CatalogPriceRuleNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\CatalogPriceRule\Exception\CatalogPriceRuleException
{
}
