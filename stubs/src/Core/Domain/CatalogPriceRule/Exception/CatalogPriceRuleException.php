<?php

namespace PrestaShop\PrestaShop\Core\Domain\CatalogPriceRule\Exception;

/**
 * Base exception for CatalogPriceRule subdomain
 */
class CatalogPriceRuleException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
