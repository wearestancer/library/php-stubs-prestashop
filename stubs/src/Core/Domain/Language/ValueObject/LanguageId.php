<?php

namespace PrestaShop\PrestaShop\Core\Domain\Language\ValueObject;

/**
 * Stores language's identity
 */
class LanguageId
{
    /**
     * @param int $id
     */
    public function __construct($id)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
