<?php

namespace PrestaShop\PrestaShop\Core\Domain\Language\Exception;

/**
 * Exception is thrown when trying to disable default language
 */
class CannotDisableDefaultLanguageException extends \PrestaShop\PrestaShop\Core\Domain\Language\Exception\LanguageException
{
}
