<?php

namespace PrestaShop\PrestaShop\Core\Domain\Country\Exception;

/**
 * Is thrown on failure to find existing country
 */
class CountryNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Country\Exception\CountryException
{
}
