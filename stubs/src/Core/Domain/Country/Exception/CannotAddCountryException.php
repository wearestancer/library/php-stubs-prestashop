<?php

namespace PrestaShop\PrestaShop\Core\Domain\Country\Exception;

/**
 * Is thrown when adding new country fails
 */
class CannotAddCountryException extends \PrestaShop\PrestaShop\Core\Domain\Country\Exception\CountryException
{
}
