<?php

namespace PrestaShop\PrestaShop\Core\Domain\Country\Exception;

/**
 * Base exception for countries subdomain
 */
class CountryException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
