<?php

namespace PrestaShop\PrestaShop\Core\Domain\Hook\ValueObject;

/**
 * Hook identity.
 */
class HookId
{
    /**
     * @param int $hookId
     */
    public function __construct($hookId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
