<?php

namespace PrestaShop\PrestaShop\Core\Domain\Hook\Exception;

/**
 * Is thrown when hook is not found
 */
class HookUpdateHookException extends \PrestaShop\PrestaShop\Core\Domain\Hook\Exception\HookException
{
}
