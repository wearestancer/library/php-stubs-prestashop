<?php

namespace PrestaShop\PrestaShop\Core\Domain\Hook\Exception;

/**
 * Is base exception for HookException subdomain
 */
class HookException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
