<?php

namespace PrestaShop\PrestaShop\Core\Domain\Hook\Exception;

/**
 * Is thrown when hook is not found
 */
class HookNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Hook\Exception\HookException
{
}
