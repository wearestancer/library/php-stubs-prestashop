<?php

namespace PrestaShop\PrestaShop\Core\Domain\Hook\Exception;

/**
 * Is thrown when updating a hook failed
 */
class CannotUpdateHookException extends \PrestaShop\PrestaShop\Core\Domain\Hook\Exception\HookException
{
}
