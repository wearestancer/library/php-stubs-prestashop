<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPageCategory\Query;

/**
 * Class GetCmsPageCategoryForEditing is responsible for retrieving cms page category form data.
 */
class GetCmsPageCategoryForEditing
{
    /**
     * @param int $cmsPageCategoryId
     *
     * @throws CmsPageCategoryException
     */
    public function __construct($cmsPageCategoryId)
    {
    }
    /**
     * @return CmsPageCategoryId
     */
    public function getCmsPageCategoryId()
    {
    }
}
