<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPageCategory\Exception;

/**
 * Is thrown when cms page category cannot be enabled or disabled in toggling action
 */
class CannotToggleCmsPageCategoryStatusException extends \PrestaShop\PrestaShop\Core\Domain\CmsPageCategory\Exception\CmsPageCategoryException
{
}
