<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPageCategory\Exception;

/**
 * Base exception for CmsPageCategory sub-domain
 */
class CmsPageCategoryException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
