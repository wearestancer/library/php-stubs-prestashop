<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPageCategory\Exception;

/**
 * Is thrown when cms page category cannot be enabled
 */
class CannotEnableCmsPageCategoryException extends \PrestaShop\PrestaShop\Core\Domain\CmsPageCategory\Exception\CmsPageCategoryException
{
}
