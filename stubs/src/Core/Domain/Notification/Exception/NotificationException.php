<?php

namespace PrestaShop\PrestaShop\Core\Domain\Notification\Exception;

/**
 * Base exception for Notification sub-domain
 */
class NotificationException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
