<?php

namespace PrestaShop\PrestaShop\Core\Domain\Notification\QueryResult;

/**
 * NotificationsResults is a collection of NotificationsResult
 */
class NotificationsResults
{
    /**
     * NotificationsResults constructor.
     *
     * @param NotificationsResult[] $notifications
     */
    public function __construct(array $notifications)
    {
    }
    /**
     * @return NotificationsResult[]
     */
    public function getNotificationsResults()
    {
    }
    public function getNotificationsResultsForJS()
    {
    }
}
