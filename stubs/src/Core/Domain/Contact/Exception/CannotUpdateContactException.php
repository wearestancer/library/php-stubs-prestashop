<?php

namespace PrestaShop\PrestaShop\Core\Domain\Contact\Exception;

/**
 * Raised when failed to update contact entity.
 */
class CannotUpdateContactException extends \PrestaShop\PrestaShop\Core\Domain\Contact\Exception\ContactException
{
}
