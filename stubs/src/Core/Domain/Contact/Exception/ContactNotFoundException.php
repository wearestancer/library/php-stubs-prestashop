<?php

namespace PrestaShop\PrestaShop\Core\Domain\Contact\Exception;

/**
 * Raised when contact was not found.
 */
class ContactNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Contact\Exception\ContactException
{
}
