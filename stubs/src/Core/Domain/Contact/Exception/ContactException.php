<?php

namespace PrestaShop\PrestaShop\Core\Domain\Contact\Exception;

/**
 * An abstraction for all contact related exceptions. Use this one in catch clause to detect all related exceptions.
 */
class ContactException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
