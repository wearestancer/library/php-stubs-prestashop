<?php

namespace PrestaShop\PrestaShop\Core\Domain\Contact\Exception;

/**
 * Raised when failed to add contact entity.
 */
class CannotAddContactException extends \PrestaShop\PrestaShop\Core\Domain\Contact\Exception\ContactException
{
}
