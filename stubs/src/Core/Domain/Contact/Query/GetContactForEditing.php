<?php

namespace PrestaShop\PrestaShop\Core\Domain\Contact\Query;

/**
 * Class GetContactForEditing is responsible for getting the data related with contact entity.
 */
class GetContactForEditing
{
    /**
     * @param int $contactId
     *
     * @throws ContactException
     */
    public function __construct($contactId)
    {
    }
    /**
     * @return ContactId
     */
    public function getContactId()
    {
    }
}
