<?php

namespace PrestaShop\PrestaShop\Core\Domain\Contact\ValueObject;

/**
 * Class ContactId
 */
class ContactId
{
    /**
     * @param int $contactId
     *
     * @throws ContactException
     */
    public function __construct($contactId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
