<?php

namespace PrestaShop\PrestaShop\Core\Domain\Cart\Exception;

/**
 * Is base exception for Cart subdomain
 */
class CartException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
