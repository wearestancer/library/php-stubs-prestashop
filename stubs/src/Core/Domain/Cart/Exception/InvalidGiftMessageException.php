<?php

namespace PrestaShop\PrestaShop\Core\Domain\Cart\Exception;

class InvalidGiftMessageException extends \PrestaShop\PrestaShop\Core\Domain\Cart\Exception\CartException
{
}
