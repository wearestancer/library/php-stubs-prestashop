<?php

namespace PrestaShop\PrestaShop\Core\Domain\Cart\Exception;

/**
 * Thrown on failure to update cart
 */
class CannotUpdateCartException extends \PrestaShop\PrestaShop\Core\Domain\Cart\Exception\CartException
{
}
