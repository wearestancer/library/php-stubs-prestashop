<?php

namespace PrestaShop\PrestaShop\Core\Domain\Cart\Exception;

/**
 * Class InvalidAddressTypeException
 */
class InvalidAddressTypeException extends \PrestaShop\PrestaShop\Core\Domain\Cart\Exception\CartException
{
}
