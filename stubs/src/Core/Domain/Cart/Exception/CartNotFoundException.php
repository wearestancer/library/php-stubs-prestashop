<?php

namespace PrestaShop\PrestaShop\Core\Domain\Cart\Exception;

/**
 * Is thrown when cart is not found
 */
class CartNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Cart\Exception\CartException
{
}
