<?php

namespace PrestaShop\PrestaShop\Core\Domain\Cart\Query;

/**
 * Get cart for viewing in Back Office
 */
class GetCartForViewing
{
    /**
     * @param int $cartId
     */
    public function __construct($cartId)
    {
    }
    /**
     * @return CartId
     */
    public function getCartId()
    {
    }
}
