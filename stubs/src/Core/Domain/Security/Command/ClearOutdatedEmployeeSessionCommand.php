<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Command;

/**
 * Class ClearEmployeeSessionCommand is a command to clear employee sessions.
 */
class ClearOutdatedEmployeeSessionCommand
{
}
