<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Command;

/**
 * Class ClearOutdatedCustomerSessionCommand is a command to clear customer sessions.
 */
class ClearOutdatedCustomerSessionCommand
{
}
