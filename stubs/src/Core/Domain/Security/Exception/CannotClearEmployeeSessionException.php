<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class CannotClearEmployeeSessionException is a base exception for security sessions context.
 */
class CannotClearEmployeeSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\SessionException
{
}
