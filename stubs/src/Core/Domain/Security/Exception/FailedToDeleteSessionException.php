<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class FailedToDeleteSessionException is a base exception for security sessions context.
 */
class FailedToDeleteSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\SessionException
{
}
