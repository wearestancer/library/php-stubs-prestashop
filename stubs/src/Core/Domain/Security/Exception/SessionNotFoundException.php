<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class SessionNotFoundException is a base exception for security sessions context.
 */
class SessionNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\SessionException
{
}
