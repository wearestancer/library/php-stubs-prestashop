<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class CannotClearCustomerSessionException is a base exception for security sessions context.
 */
class CannotClearCustomerSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\SessionException
{
}
