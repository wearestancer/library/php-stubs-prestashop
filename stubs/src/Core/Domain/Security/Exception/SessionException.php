<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class SessionException is a base exception for security sessions context.
 */
class SessionException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
