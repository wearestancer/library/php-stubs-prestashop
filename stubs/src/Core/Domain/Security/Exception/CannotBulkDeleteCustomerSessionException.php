<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class CannotBulkDeleteCustomerSessionException is a base exception for security sessions context.
 */
class CannotBulkDeleteCustomerSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\CannotBulkDeleteSessionException
{
}
