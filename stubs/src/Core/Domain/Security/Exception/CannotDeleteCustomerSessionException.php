<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class CannotDeleteCustomerSessionException is a base exception for security sessions context.
 */
class CannotDeleteCustomerSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\SessionException
{
}
