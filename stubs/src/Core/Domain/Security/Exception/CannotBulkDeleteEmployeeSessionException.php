<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class CannotBulkDeleteEmployeeSessionException is a base exception for security sessions context.
 */
class CannotBulkDeleteEmployeeSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\CannotBulkDeleteSessionException
{
}
