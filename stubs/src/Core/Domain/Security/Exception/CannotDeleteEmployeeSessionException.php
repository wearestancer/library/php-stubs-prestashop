<?php

namespace PrestaShop\PrestaShop\Core\Domain\Security\Exception;

/**
 * Class CannotDeleteEmployeeSessionException is a base exception for security sessions context.
 */
class CannotDeleteEmployeeSessionException extends \PrestaShop\PrestaShop\Core\Domain\Security\Exception\SessionException
{
}
