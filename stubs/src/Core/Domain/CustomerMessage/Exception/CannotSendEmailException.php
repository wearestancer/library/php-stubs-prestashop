<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerMessage\Exception;

class CannotSendEmailException extends \PrestaShop\PrestaShop\Core\Domain\CustomerMessage\Exception\CustomerMessageException
{
}
