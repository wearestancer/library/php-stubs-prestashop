<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Exception;

/**
 * Thrown on failure to find customer by email
 */
class CustomerByEmailNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Customer\Exception\CustomerException
{
}
