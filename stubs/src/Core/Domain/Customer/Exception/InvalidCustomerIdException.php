<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Exception;

/**
 * Class InvalidCustomerIdException is thrown when CustomerId with invalid value is being created.
 */
class InvalidCustomerIdException extends \PrestaShop\PrestaShop\Core\Domain\Customer\Exception\CustomerException
{
}
