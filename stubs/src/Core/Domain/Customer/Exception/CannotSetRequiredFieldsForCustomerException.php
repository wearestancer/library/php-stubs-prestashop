<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Exception;

/**
 * Is thrown when setting required fields for customer fails
 */
class CannotSetRequiredFieldsForCustomerException extends \PrestaShop\PrestaShop\Core\Domain\Customer\Exception\CustomerException
{
}
