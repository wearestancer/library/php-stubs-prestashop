<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Exception;

/**
 * Class CustomerException is base "Customer" context exception
 */
class CustomerException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
