<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Exception;

/**
 * Is thrown when customer is not found
 */
class CustomerNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Customer\Exception\CustomerException
{
}
