<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Exception;

/**
 * Thrown when trying to set invalid required fields for customer
 */
class InvalidCustomerRequiredFieldsException extends \PrestaShop\PrestaShop\Core\Domain\Customer\Exception\CustomerException
{
}
