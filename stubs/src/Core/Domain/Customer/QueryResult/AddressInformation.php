<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\QueryResult;

/**
 * Class AddressInformation.
 */
class AddressInformation
{
    /**
     * @param int $addressId
     * @param string $company
     * @param string $fullName
     * @param string $fullAddress
     * @param string $countryName
     * @param string $phone
     * @param string $phoneMobile
     */
    public function __construct($addressId, $company, $fullName, $fullAddress, $countryName, $phone, $phoneMobile)
    {
    }
    /**
     * @return int
     */
    public function getAddressId()
    {
    }
    /**
     * @return string
     */
    public function getCompany()
    {
    }
    /**
     * @return string
     */
    public function getFullName()
    {
    }
    /**
     * @return string
     */
    public function getFullAddress()
    {
    }
    /**
     * @return string
     */
    public function getCountryName()
    {
    }
    /**
     * @return string
     */
    public function getPhone()
    {
    }
    /**
     * @return string
     */
    public function getPhoneMobile()
    {
    }
}
