<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\QueryResult;

/**
 * Class GroupInformation holds customer group information.
 */
class GroupInformation
{
    /**
     * @param int $groupId
     * @param string $name
     */
    public function __construct($groupId, $name)
    {
    }
    /**
     * @return int
     */
    public function getGroupId()
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
}
