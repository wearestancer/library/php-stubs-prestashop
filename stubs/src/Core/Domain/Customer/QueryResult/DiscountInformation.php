<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\QueryResult;

/**
 * Class DiscountInformation.
 */
class DiscountInformation
{
    /**
     * @param int $discountId
     * @param string $code
     * @param string $name
     * @param bool $isActive
     * @param int $availableQuantity
     */
    public function __construct($discountId, $code, $name, $isActive, $availableQuantity)
    {
    }
    /**
     * @return int
     */
    public function getDiscountId()
    {
    }
    /**
     * @return string
     */
    public function getCode()
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @return bool
     */
    public function isActive()
    {
    }
    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
    }
}
