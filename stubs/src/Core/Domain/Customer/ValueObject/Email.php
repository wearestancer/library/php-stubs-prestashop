<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\ValueObject;

/**
 * @deprecated since version 1.7.6.4 use PrestaShop\PrestaShop\Core\Domain\ValueObject\Email instead
 */
class Email extends \PrestaShop\PrestaShop\Core\Domain\ValueObject\Email
{
    public function __construct($email)
    {
    }
}
