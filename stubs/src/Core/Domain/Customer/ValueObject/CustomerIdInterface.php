<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\ValueObject;

interface CustomerIdInterface
{
    public function getValue() : int;
}
