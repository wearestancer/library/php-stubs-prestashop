<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Group\Exception;

/**
 * Base exception for Group domain
 */
class GroupException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
