<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Group\Exception;

class GroupNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Customer\Group\Exception\GroupException
{
}
