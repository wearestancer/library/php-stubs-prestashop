<?php

namespace PrestaShop\PrestaShop\Core\Domain\Customer\Group\ValueObject;

/**
 * Defines contract for group identification value.
 * This interface allows to explicitly define whether group relation is optional or required.
 *
 * @see GroupId
 * @see NoGroupId
 */
interface GroupIdInterface
{
    /**
     * @return int
     */
    public function getValue() : int;
}
