<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\ValueObject;

/**
 * Class which holds the cms page id value.
 */
class CmsPageId
{
    /**
     * @param int $cmsPageId
     *
     * @throws CmsPageException
     */
    public function __construct($cmsPageId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
