<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception;

/**
 * Is thrown when required cms page is not found
 */
class CmsPageNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception\CmsPageException
{
}
