<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception;

/**
 * Is thrown when cms page cannot be edited
 */
class CannotEditCmsPageException extends \PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception\CmsPageException
{
}
