<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception;

/**
 * Is thrown when cms page cannot be disabled
 */
class CannotDisableCmsPageException extends \PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception\CmsPageException
{
}
