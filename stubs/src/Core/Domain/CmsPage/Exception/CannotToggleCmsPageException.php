<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception;

/**
 * Is thrown when cms page cannot be enabled or disabled in toggling action
 */
class CannotToggleCmsPageException extends \PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception\CmsPageException
{
}
