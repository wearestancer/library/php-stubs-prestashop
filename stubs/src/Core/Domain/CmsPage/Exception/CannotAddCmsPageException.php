<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception;

/**
 * Thrown on failure when adding new cms page
 */
class CannotAddCmsPageException extends \PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception\CmsPageException
{
}
