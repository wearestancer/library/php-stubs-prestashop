<?php

namespace PrestaShop\PrestaShop\Core\Domain\CmsPage\Exception;

/**
 * Base exception for cms page sub-domain
 */
class CmsPageException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
