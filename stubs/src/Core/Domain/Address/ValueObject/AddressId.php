<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\ValueObject;

/**
 * Provides address id
 */
class AddressId
{
    /**
     * @param int $addressId
     *
     * @throws AddressConstraintException
     */
    public function __construct($addressId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
