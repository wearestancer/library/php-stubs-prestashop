<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Thrown on failure to update address
 */
class CannotUpdateOrderAddressException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
