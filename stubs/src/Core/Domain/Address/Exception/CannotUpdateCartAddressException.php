<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Thrown on failure to update address
 */
class CannotUpdateCartAddressException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
