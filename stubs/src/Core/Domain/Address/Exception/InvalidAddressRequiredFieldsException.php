<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Thrown when invalid address required fields provided for saving
 */
class InvalidAddressRequiredFieldsException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
