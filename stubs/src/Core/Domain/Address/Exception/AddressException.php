<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Base exception for address sub-domain
 */
class AddressException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
