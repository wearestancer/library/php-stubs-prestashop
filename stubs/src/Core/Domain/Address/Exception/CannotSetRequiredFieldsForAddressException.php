<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Exception thrown on failure to update address required fields
 */
class CannotSetRequiredFieldsForAddressException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
