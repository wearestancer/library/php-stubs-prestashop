<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Thrown on failure to create address
 */
class CannotAddAddressException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
