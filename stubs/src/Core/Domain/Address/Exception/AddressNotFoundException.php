<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Is thrown when address is not found
 */
class AddressNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
