<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Exception;

/**
 * Is thrown when address has invalid fields on saving/updating
 */
class InvalidAddressFieldException extends \PrestaShop\PrestaShop\Core\Domain\Address\Exception\AddressException
{
}
