<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Query;

/**
 * Gets manufacturer address for editing
 */
class GetManufacturerAddressForEditing
{
    /**
     * @param int $addressId
     *
     * @throws AddressConstraintException
     */
    public function __construct($addressId)
    {
    }
    /**
     * @return AddressId
     */
    public function getAddressId()
    {
    }
}
