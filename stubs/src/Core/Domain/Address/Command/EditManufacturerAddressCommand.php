<?php

namespace PrestaShop\PrestaShop\Core\Domain\Address\Command;

/**
 * Edits manufacturer address
 */
class EditManufacturerAddressCommand
{
    /**
     * @param int $addressId
     *
     * @throws AddressConstraintException
     */
    public function __construct($addressId)
    {
    }
    /**
     * @return AddressId
     */
    public function getAddressId()
    {
    }
    /**
     * @param AddressId $addressId
     */
    public function setAddressId($addressId)
    {
    }
    /**
     * @return int|null
     */
    public function getManufacturerId()
    {
    }
    /**
     * @param int $manufacturerId
     *
     * @throws AddressConstraintException
     */
    public function setManufacturerId($manufacturerId)
    {
    }
    /**
     * @return string|null
     */
    public function getLastName()
    {
    }
    /**
     * @param string|null $lastName
     */
    public function setLastName($lastName)
    {
    }
    /**
     * @return string|null
     */
    public function getFirstName()
    {
    }
    /**
     * @param string|null $firstName
     */
    public function setFirstName($firstName)
    {
    }
    /**
     * @return string|null
     */
    public function getAddress()
    {
    }
    /**
     * @param string|null $address
     */
    public function setAddress($address)
    {
    }
    /**
     * @return string|null
     */
    public function getCity()
    {
    }
    /**
     * @param string|null $city
     */
    public function setCity($city)
    {
    }
    /**
     * @return string|null
     */
    public function getAddress2()
    {
    }
    /**
     * @param string|null $address2
     */
    public function setAddress2($address2)
    {
    }
    /**
     * @return int|null
     */
    public function getCountryId()
    {
    }
    /**
     * @param int|null $countryId
     */
    public function setCountryId($countryId)
    {
    }
    /**
     * @return string|null
     */
    public function getPostCode()
    {
    }
    /**
     * @param string|null $postCode
     */
    public function setPostCode($postCode)
    {
    }
    /**
     * @return int|null
     */
    public function getStateId()
    {
    }
    /**
     * @param int|null $stateId
     */
    public function setStateId($stateId)
    {
    }
    /**
     * @return string|null
     */
    public function getHomePhone()
    {
    }
    /**
     * @param string|null $homePhone
     */
    public function setHomePhone($homePhone)
    {
    }
    /**
     * @return string|null
     */
    public function getMobilePhone()
    {
    }
    /**
     * @param string|null $mobilePhone
     */
    public function setMobilePhone($mobilePhone)
    {
    }
    /**
     * @return string|null
     */
    public function getOther()
    {
    }
    /**
     * @param string|null $other
     */
    public function setOther($other)
    {
    }
    /**
     * @return string|null
     */
    public function getDni()
    {
    }
    /**
     * @param string|null $dni
     */
    public function setDni($dni)
    {
    }
}
