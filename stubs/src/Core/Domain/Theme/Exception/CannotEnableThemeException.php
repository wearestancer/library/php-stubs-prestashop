<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Thrown when fails to enable theme
 */
class CannotEnableThemeException extends \PrestaShop\PrestaShop\Core\Domain\Theme\Exception\ThemeException
{
}
