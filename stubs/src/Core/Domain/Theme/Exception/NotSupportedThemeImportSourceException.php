<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Class NotSupportedThemeImportSourceException is thrown when not supported theme import source is being used.
 */
class NotSupportedThemeImportSourceException extends \PrestaShop\PrestaShop\Core\Domain\Theme\Exception\ThemeException
{
}
