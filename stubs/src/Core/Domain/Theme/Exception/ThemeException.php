<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Class ThemeException is base exception for theme sub-domain.
 */
class ThemeException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
