<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Thrown when fails to adapt to RTL languages
 */
class CannotAdaptThemeToRTLLanguagesException extends \PrestaShop\PrestaShop\Core\Domain\Theme\Exception\ThemeException
{
}
