<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Thrown when fails to delete theme
 */
class CannotDeleteThemeException extends \PrestaShop\PrestaShop\Core\Domain\Theme\Exception\ThemeException
{
}
