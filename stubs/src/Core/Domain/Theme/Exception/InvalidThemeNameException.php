<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Class InvalidThemeNameException is thrown when invalid theme name is provided.
 */
class InvalidThemeNameException extends \PrestaShop\PrestaShop\Core\Domain\Theme\Exception\ThemeException
{
}
