<?php

namespace PrestaShop\PrestaShop\Core\Domain\Theme\Exception;

/**
 * Thrown when fails to reset theme layout
 */
class CannotResetThemeLayoutsException extends \PrestaShop\PrestaShop\Core\Domain\Theme\Exception\ThemeException
{
}
