<?php

namespace PrestaShop\PrestaShop\Core\Domain\Manufacturer\Exception;

/**
 * Is thrown when manufacturer is not found in Manufacturer subdomain
 */
class ManufacturerNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Manufacturer\Exception\ManufacturerException
{
}
