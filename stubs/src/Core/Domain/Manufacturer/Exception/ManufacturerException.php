<?php

namespace PrestaShop\PrestaShop\Core\Domain\Manufacturer\Exception;

/**
 * Base exception for Manufacturer subdomain
 */
class ManufacturerException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
