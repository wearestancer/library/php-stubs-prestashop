<?php

namespace PrestaShop\PrestaShop\Core\Domain\Webservice\Exception;

/**
 * Is thrown when webservice key is not found
 */
class WebserviceKeyNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Webservice\Exception\WebserviceException
{
}
