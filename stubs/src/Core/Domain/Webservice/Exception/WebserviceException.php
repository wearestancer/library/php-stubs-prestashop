<?php

namespace PrestaShop\PrestaShop\Core\Domain\Webservice\Exception;

/**
 * Base exception for Webservice subdomain
 */
class WebserviceException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
