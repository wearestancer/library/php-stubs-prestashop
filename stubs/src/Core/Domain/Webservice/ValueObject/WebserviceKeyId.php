<?php

namespace PrestaShop\PrestaShop\Core\Domain\Webservice\ValueObject;

/**
 * Encapsulates webservice key id value
 */
class WebserviceKeyId
{
    /**
     * @param int $webserviceKeyId
     */
    public function __construct($webserviceKeyId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
