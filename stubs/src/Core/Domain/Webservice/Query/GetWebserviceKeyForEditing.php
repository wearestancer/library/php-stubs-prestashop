<?php

namespace PrestaShop\PrestaShop\Core\Domain\Webservice\Query;

/**
 * Get webservice key data for editing
 */
class GetWebserviceKeyForEditing
{
    /**
     * @param int $webserviceKeyId
     */
    public function __construct($webserviceKeyId)
    {
    }
    /**
     * @return WebserviceKeyId
     */
    public function getWebserviceKeyId()
    {
    }
}
