<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order;

class OrderConstraints
{
    /**
     * Number of products that are displayed in order preview
     */
    public const PRODUCTS_PREVIEW_LIMIT = 10;
}
