<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\QueryResult;

class OrderDiscountsForViewing
{
    /**
     * @param OrderDiscountForViewing[] $discounts
     */
    public function __construct(array $discounts)
    {
    }
    /**
     * @return OrderDiscountForViewing[]
     */
    public function getDiscounts() : array
    {
    }
}
