<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\QueryResult;

class LinkedOrdersForViewing
{
    /**
     * @param LinkedOrderForViewing[] $linkedOrders
     */
    public function __construct(array $linkedOrders)
    {
    }
    /**
     * @return LinkedOrderForViewing[]
     */
    public function getLinkedOrders() : array
    {
    }
}
