<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\QueryResult;

class OrderSourcesForViewing
{
    /**
     * @param OrderSourceForViewing[] $sources
     */
    public function __construct(array $sources)
    {
    }
    /**
     * @return OrderSourceForViewing[]
     */
    public function getSources() : array
    {
    }
}
