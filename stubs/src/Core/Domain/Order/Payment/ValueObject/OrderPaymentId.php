<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Payment\ValueObject;

/**
 * Order payment identity
 */
class OrderPaymentId
{
    /**
     * @param int $orderPaymentId
     */
    public function __construct($orderPaymentId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
