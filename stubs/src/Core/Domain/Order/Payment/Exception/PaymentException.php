<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Payment\Exception;

/**
 * Is base exception for Payment subdomain
 */
class PaymentException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
