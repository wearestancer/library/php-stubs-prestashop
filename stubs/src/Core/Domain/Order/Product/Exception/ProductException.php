<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Product\Exception;

/**
 * Base exception for Order/Product subdomain
 */
class ProductException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
