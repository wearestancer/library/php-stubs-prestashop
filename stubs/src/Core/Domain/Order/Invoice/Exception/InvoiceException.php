<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Invoice\Exception;

/**
 * Base exception for Invoice subdomain
 */
class InvoiceException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
