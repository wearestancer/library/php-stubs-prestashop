<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Invoice\Exception;

/**
 * Thrown when order invoice is not found
 */
class InvoiceNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Order\Invoice\Exception\InvoiceException
{
}
