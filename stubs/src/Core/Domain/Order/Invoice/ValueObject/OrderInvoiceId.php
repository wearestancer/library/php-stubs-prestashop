<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Invoice\ValueObject;

/**
 * Order invoice identity
 */
class OrderInvoiceId
{
    /**
     * @param int $orderInvoiceId
     */
    public function __construct($orderInvoiceId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
