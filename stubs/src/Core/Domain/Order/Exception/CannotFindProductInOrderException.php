<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Thrown on failure to update product in an order
 */
class CannotFindProductInOrderException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
