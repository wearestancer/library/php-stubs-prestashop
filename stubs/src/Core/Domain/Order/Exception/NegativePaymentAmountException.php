<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Exception thrown when the payment amount is negative
 */
class NegativePaymentAmountException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
