<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

class CannotEditDeliveredOrderProductException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
