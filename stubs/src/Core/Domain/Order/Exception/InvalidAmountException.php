<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Exception thrown when the amount format is not valid
 */
class InvalidAmountException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
