<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Exception thrown when something went wrong while deleting a product from an order.
 */
class DeleteProductFromOrderException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
