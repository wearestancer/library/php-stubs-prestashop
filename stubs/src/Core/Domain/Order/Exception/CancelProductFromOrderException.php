<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Exception thrown when something went wrong during a product cancel action.
 */
class CancelProductFromOrderException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
