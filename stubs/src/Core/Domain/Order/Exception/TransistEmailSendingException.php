<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Thrown when when failed to send email to customer regarding order tracking
 */
class TransistEmailSendingException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
