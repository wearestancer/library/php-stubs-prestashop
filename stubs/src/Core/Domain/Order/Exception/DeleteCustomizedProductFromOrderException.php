<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Exception thrown when something went wrong while deleting a customized product from an order.
 */
class DeleteCustomizedProductFromOrderException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
