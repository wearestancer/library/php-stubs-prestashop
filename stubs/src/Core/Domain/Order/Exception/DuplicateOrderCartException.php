<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Is thrown when duplicating order cart fails
 */
class DuplicateOrderCartException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
