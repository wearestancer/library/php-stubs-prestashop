<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

class DuplicateProductInOrderException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
