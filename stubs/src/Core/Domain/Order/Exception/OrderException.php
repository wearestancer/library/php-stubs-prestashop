<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Is base exception for Order subdomain
 */
class OrderException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
