<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Class InvalidAddressTypeException
 */
class InvalidAddressTypeException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
