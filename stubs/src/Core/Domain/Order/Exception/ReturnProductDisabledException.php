<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Thrown when trying to perform a return product operation while it is disabled
 */
class ReturnProductDisabledException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
