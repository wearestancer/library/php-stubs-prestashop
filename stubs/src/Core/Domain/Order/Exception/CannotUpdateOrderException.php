<?php

namespace PrestaShop\PrestaShop\Core\Domain\Order\Exception;

/**
 * Thrown on failure to update order
 */
class CannotUpdateOrderException extends \PrestaShop\PrestaShop\Core\Domain\Order\Exception\OrderException
{
}
