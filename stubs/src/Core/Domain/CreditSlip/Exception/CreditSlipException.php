<?php

namespace PrestaShop\PrestaShop\Core\Domain\CreditSlip\Exception;

/**
 * Base class for CreditSlip subdomain
 */
class CreditSlipException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
