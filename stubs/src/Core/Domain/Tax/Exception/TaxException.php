<?php

namespace PrestaShop\PrestaShop\Core\Domain\Tax\Exception;

/**
 * Is base exception for Tax sub-domain
 */
class TaxException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
