<?php

namespace PrestaShop\PrestaShop\Core\Domain\Tax\Exception;

/**
 * Is thrown when tax is not found by provided id
 */
class TaxNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Tax\Exception\TaxException
{
}
