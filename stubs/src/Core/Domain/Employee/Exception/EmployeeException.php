<?php

namespace PrestaShop\PrestaShop\Core\Domain\Employee\Exception;

/**
 * Base exception for employee sub-domain
 */
class EmployeeException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
