<?php

namespace PrestaShop\PrestaShop\Core\Domain\Employee\Exception;

/**
 * Class InvalidPasswordException thrown when employee password is invalid.
 */
class InvalidPasswordException extends \PrestaShop\PrestaShop\Core\Domain\Employee\Exception\EmployeeException
{
}
