<?php

namespace PrestaShop\PrestaShop\Core\Domain\Employee\Exception;

/**
 * Thrown when invalid profile is selected for an employee.
 */
class InvalidProfileException extends \PrestaShop\PrestaShop\Core\Domain\Employee\Exception\EmployeeException
{
}
