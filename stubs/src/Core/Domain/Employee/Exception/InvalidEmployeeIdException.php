<?php

namespace PrestaShop\PrestaShop\Core\Domain\Employee\Exception;

/**
 * Class InvalidEmployeeIdException is thrown when EmployeeId with invalid value is being created.
 */
class InvalidEmployeeIdException extends \PrestaShop\PrestaShop\Core\Domain\Employee\Exception\EmployeeException
{
}
