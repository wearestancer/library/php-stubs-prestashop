<?php

namespace PrestaShop\PrestaShop\Core\Domain\Employee\Exception;

/**
 * Is thrown when warehouse manager cannot be deleted
 */
class CannotDeleteWarehouseManagerException extends \PrestaShop\PrestaShop\Core\Domain\Employee\Exception\EmployeeException
{
}
