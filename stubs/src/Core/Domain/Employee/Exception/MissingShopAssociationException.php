<?php

namespace PrestaShop\PrestaShop\Core\Domain\Employee\Exception;

/**
 * Thrown when shop association is missing for an employee.
 */
class MissingShopAssociationException extends \PrestaShop\PrestaShop\Core\Domain\Employee\Exception\EmployeeException
{
}
