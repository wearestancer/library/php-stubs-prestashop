<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\QueryResult;

class ProductCombination
{
    /**
     * @param int $combinationId
     * @param string $combinationName
     */
    public function __construct(int $combinationId, string $combinationName)
    {
    }
    /**
     * @return int
     */
    public function getCombinationId() : int
    {
    }
    /**
     * @return string
     */
    public function getCombinationName() : string
    {
    }
}
