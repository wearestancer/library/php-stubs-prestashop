<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\QueryResult;

class ProductCombinationsCollection
{
    /**
     * @param ProductCombination[] $productCombinations
     */
    public function __construct(array $productCombinations)
    {
    }
    /**
     * @return ProductCombination[]
     */
    public function getProductCombinations() : array
    {
    }
}
