<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\CommandHandler;

/**
 * Defines contract to handle @see UpdateCombinationCommand
 */
interface UpdateCombinationHandlerInterface
{
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Product\Combination\Command\UpdateCombinationCommand $command) : void;
}
