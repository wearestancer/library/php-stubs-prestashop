<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception;

/**
 * Thrown when fails to find combination
 */
class CombinationNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception\CombinationException
{
}
