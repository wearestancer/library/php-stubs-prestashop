<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception;

/**
 * Thrown when deleting combination fails
 */
class CannotDeleteCombinationException extends \PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception\CombinationException
{
}
