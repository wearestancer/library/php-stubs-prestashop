<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception;

/**
 * Base exception for product combination subdomain
 */
class CombinationException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
