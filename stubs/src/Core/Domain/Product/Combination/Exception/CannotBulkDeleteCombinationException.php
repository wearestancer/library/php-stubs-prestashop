<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception;

/**
 * Thrown when bulk deleting combination fails
 */
class CannotBulkDeleteCombinationException extends \PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception\BulkCombinationException
{
}
