<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception;

/**
 * Is thrown when new combination adding fails
 */
class CannotAddCombinationException extends \PrestaShop\PrestaShop\Core\Domain\Product\Combination\Exception\CombinationException
{
}
