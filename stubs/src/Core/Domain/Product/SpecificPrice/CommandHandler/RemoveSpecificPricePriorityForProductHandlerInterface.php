<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\CommandHandler;

interface RemoveSpecificPricePriorityForProductHandlerInterface
{
    public function handle(\PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Command\RemoveSpecificPricePriorityForProductCommand $command) : void;
}
