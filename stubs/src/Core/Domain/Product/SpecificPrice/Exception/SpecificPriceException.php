<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception;

/**
 * Base exception for SpecificPrice subdomain
 */
class SpecificPriceException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
