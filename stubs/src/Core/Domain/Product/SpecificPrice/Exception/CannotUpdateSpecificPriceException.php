<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception;

class CannotUpdateSpecificPriceException extends \PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception\SpecificPriceException
{
}
