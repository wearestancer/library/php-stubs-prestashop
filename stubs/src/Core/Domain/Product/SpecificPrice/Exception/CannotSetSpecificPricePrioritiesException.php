<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception;

/**
 * Thrown when setting specific price priorities fails
 */
class CannotSetSpecificPricePrioritiesException extends \PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception\SpecificPriceException
{
}
