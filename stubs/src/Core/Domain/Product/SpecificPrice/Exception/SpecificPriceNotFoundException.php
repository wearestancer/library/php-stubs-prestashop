<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception;

/**
 * Thrown when requested specific price could not be found
 */
class SpecificPriceNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception\SpecificPriceException
{
}
