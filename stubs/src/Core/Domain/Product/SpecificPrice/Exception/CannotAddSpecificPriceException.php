<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception;

/**
 * Exception thrown when SpecificPrice could not be created
 */
class CannotAddSpecificPriceException extends \PrestaShop\PrestaShop\Core\Domain\Product\SpecificPrice\Exception\SpecificPriceException
{
}
