<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\FeatureValue\Exception;

/**
 * Thrown when the input format of the @see SetProductFeatureValuesCommand is invalid
 */
class InvalidProductFeatureValuesFormatException extends \PrestaShop\PrestaShop\Core\Domain\Product\FeatureValue\Exception\ProductFeatureValueException
{
}
