<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\FeatureValue\Exception;

/**
 * Base exception for Product/FeatureValue subdomain
 */
class ProductFeatureValueException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
