<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\FeatureValue\Exception;

/**
 * Thrown when you try to associate the same value more than once.
 */
class DuplicateFeatureValueAssociationException extends \PrestaShop\PrestaShop\Core\Domain\Product\FeatureValue\Exception\ProductFeatureValueException
{
}
