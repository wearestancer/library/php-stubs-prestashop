<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Pack\ValueObject;

/**
 * Id of product which is a pack.
 */
class PackId extends \PrestaShop\PrestaShop\Core\Domain\Product\ValueObject\ProductId
{
}
