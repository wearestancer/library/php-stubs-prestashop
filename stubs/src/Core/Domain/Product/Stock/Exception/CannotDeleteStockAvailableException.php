<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception;

/**
 * Exception thrown when the deletion of a StockAvailable failed.
 */
class CannotDeleteStockAvailableException extends \PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception\ProductStockException
{
}
