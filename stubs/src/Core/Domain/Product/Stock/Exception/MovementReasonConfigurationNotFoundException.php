<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception;

/**
 * Thrown when configuration is missing for movement reason
 */
class MovementReasonConfigurationNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception\ProductStockException
{
}
