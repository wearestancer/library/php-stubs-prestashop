<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception;

/**
 * Thrown when StockAvailable creation fails
 */
class CannotAddStockAvailableException extends \PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception\ProductStockException
{
}
