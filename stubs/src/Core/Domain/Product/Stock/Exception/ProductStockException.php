<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Stock\Exception;

/**
 * Base exception for Product/Stock subdomain
 */
class ProductStockException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
