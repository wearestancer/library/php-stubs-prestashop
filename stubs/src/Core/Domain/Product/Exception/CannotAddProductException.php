<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Thrown when new product creation fails
 */
class CannotAddProductException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
