<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Is base exception for Product subdomain
 */
class ProductException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
