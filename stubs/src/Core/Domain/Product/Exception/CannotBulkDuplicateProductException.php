<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Thorwn when an exception happens during a bulk duplication.
 */
class CannotBulkDuplicateProductException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\BulkProductException
{
}
