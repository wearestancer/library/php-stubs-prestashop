<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Is thrown when product is not found
 */
class ProductNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
