<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Thrown when product deletion fails in bulk action
 */
class CannotBulkDeleteProductException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\BulkProductException
{
}
