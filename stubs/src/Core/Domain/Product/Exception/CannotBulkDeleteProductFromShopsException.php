<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Thrown when product deletion from shops failed
 */
class CannotBulkDeleteProductFromShopsException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\BulkProductException
{
}
