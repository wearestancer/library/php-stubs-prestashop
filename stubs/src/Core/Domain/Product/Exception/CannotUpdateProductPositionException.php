<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * This exception is thrown when updating the position of products failed.
 */
class CannotUpdateProductPositionException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
