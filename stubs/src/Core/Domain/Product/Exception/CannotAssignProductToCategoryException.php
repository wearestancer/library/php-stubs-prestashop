<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Is thrown when the assignation of a product to a category failed
 */
class CannotAssignProductToCategoryException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
