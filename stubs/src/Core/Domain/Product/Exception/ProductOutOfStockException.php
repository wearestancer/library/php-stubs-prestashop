<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Is thrown when using product (e.g. adding to cart) which is out of stock
 */
class ProductOutOfStockException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
