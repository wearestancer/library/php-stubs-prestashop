<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Thrown when product deletion fails
 */
class CannotDeleteProductException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
