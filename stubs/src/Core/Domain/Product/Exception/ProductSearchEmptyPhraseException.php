<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Is thrown when product search is an empty string
 */
class ProductSearchEmptyPhraseException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
