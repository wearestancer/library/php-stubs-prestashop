<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Exception;

/**
 * Is thrown when the customization of a product is not found
 */
class ProductCustomizationNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
