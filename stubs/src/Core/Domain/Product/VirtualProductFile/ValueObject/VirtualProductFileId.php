<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\ValueObject;

/**
 * Holds virtual product file identification value
 */
class VirtualProductFileId
{
    /**
     * @param int $virtualProductFileId
     */
    public function __construct(int $virtualProductFileId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
