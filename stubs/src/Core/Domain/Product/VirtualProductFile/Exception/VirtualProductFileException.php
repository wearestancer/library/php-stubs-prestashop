<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception;

/**
 * Base exception for virtual product file errors
 */
class VirtualProductFileException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
