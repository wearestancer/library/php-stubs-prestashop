<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception;

/**
 * Thrown when virtual product file is not found
 */
class VirtualProductFileNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception\VirtualProductFileException
{
}
