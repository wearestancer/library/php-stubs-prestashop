<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception;

/**
 * Is thrown when virtual product file deletion fails
 */
class CannotDeleteVirtualProductFileException extends \PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception\VirtualProductFileException
{
}
