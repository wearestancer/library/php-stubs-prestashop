<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception;

/**
 * Is thrown when update of VirtualProductFile fails
 */
class CannotUpdateVirtualProductFileException extends \PrestaShop\PrestaShop\Core\Domain\Product\VirtualProductFile\Exception\VirtualProductFileException
{
}
