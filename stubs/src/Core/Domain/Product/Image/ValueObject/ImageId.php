<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Image\ValueObject;

/**
 * Holds image identification
 */
class ImageId
{
    /**
     * @param int $value
     */
    public function __construct(int $value)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
