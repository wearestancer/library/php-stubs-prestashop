<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception;

/**
 * Thrown when adding product image fails
 */
class CannotAddProductImageException extends \PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception\ProductImageException
{
}
