<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception;

class CannotRemoveCoverException extends \PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception\ProductImageException
{
}
