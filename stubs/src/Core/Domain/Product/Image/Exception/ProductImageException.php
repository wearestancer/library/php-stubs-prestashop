<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception;

/**
 * Base exception for product image subdomain
 */
class ProductImageException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
