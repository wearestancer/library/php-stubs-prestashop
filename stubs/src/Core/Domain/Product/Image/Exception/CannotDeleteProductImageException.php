<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception;

class CannotDeleteProductImageException extends \PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception\ProductImageException
{
}
