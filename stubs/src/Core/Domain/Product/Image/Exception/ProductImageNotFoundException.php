<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception;

/**
 * Is thrown when product image is not found
 */
class ProductImageNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\Image\Exception\ProductImageException
{
}
