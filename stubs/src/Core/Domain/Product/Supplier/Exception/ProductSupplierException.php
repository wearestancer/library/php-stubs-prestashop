<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception;

/**
 * Base exception for Product Supplier subdomain
 */
class ProductSupplierException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
