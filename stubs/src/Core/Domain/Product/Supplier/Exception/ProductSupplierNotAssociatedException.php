<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception;

/**
 * Exception thrown when trying to set a default supplier which is not associated
 */
class ProductSupplierNotAssociatedException extends \PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception\ProductSupplierException
{
}
