<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception;

class InvalidProductSupplierAssociationException extends \PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception\ProductSupplierException
{
}
