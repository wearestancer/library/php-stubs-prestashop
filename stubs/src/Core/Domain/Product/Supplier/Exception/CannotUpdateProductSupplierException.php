<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception;

/**
 * Thrown when product supplier update fails
 */
class CannotUpdateProductSupplierException extends \PrestaShop\PrestaShop\Core\Domain\Product\Supplier\Exception\ProductSupplierException
{
}
