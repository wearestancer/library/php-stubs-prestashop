<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Supplier\ValueObject;

/**
 * Holds product supplier identification value
 */
class ProductSupplierId
{
    /**
     * @param int $value
     */
    public function __construct(int $value)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
