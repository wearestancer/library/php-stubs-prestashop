<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization;

/**
 * Defines settings for customization fields
 */
class CustomizationFieldSettings
{
    /**
     * Maximum allowed length pf customization field name
     */
    public const MAX_NAME_LENGTH = 255;
}
