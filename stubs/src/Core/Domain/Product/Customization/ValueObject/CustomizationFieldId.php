<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\ValueObject;

/**
 * Holds Customization Field Identification value
 */
class CustomizationFieldId
{
    /**
     * @param int $value
     */
    public function __construct(int $value)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
