<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception;

/**
 * Thrown when expected customization field is not found
 */
class CustomizationFieldNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception\CustomizationFieldException
{
}
