<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception;

/**
 * Base exception for Product/Customization subdomain
 */
class CustomizationException extends \PrestaShop\PrestaShop\Core\Domain\Product\Exception\ProductException
{
}
