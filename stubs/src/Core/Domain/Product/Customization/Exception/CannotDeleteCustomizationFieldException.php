<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception;

/**
 * Is thrown when customization field deletion fails
 */
class CannotDeleteCustomizationFieldException extends \PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception\CustomizationFieldException
{
}
