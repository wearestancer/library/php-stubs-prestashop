<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception;

/**
 * Base exception for customization field subdomain
 */
class CustomizationFieldException extends \PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception\CustomizationException
{
}
