<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception;

/**
 * Thrown when adding new customization field fails
 */
class CannotAddCustomizationFieldException extends \PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception\CustomizationException
{
}
