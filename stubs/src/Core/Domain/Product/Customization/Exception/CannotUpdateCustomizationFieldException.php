<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception;

/**
 * Thrown when customization field update fails
 */
class CannotUpdateCustomizationFieldException extends \PrestaShop\PrestaShop\Core\Domain\Product\Customization\Exception\CustomizationFieldException
{
}
