<?php

namespace PrestaShop\PrestaShop\Core\Domain\Product\Customization;

/**
 * Defines settings for customizations
 */
class CustomizationSettings
{
    /**
     * Maximum allowed length for customization text field value
     */
    public const MAX_TEXT_LENGTH = 255;
}
