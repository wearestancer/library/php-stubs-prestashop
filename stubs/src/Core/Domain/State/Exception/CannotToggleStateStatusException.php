<?php

namespace PrestaShop\PrestaShop\Core\Domain\State\Exception;

/**
 * Is thrown when cannot toggle state.
 */
class CannotToggleStateStatusException extends \PrestaShop\PrestaShop\Core\Domain\State\Exception\StateException
{
}
