<?php

namespace PrestaShop\PrestaShop\Core\Domain\State\Exception;

/**
 * Thrown on failure to update state
 */
class CannotUpdateStateException extends \PrestaShop\PrestaShop\Core\Domain\State\Exception\StateException
{
}
