<?php

namespace PrestaShop\PrestaShop\Core\Domain\State\Exception;

/**
 * Base exception for state subdomain
 */
class StateException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
