<?php

namespace PrestaShop\PrestaShop\Core\Domain\State\Exception;

/**
 * Is thrown when required state cannot be found
 */
class StateNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\State\Exception\StateException
{
}
