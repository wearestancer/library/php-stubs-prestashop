<?php

namespace PrestaShop\PrestaShop\Core\Domain\State\Exception;

/**
 * Thrown on failure to add state
 */
class CannotAddStateException extends \PrestaShop\PrestaShop\Core\Domain\State\Exception\StateException
{
}
