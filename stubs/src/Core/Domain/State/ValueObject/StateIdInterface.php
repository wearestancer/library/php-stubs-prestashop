<?php

namespace PrestaShop\PrestaShop\Core\Domain\State\ValueObject;

interface StateIdInterface
{
    public function getValue() : int;
}
