<?php

namespace PrestaShop\PrestaShop\Core\Domain\CartRule\ValueObject;

/**
 * Cart rule identity.
 */
class CartRuleId
{
    /**
     * @param int $cartRuleId
     */
    public function __construct(int $cartRuleId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
