<?php

namespace PrestaShop\PrestaShop\Core\Domain\CartRule\Exception;

/**
 * Thrown when a value is invalid
 */
class InvalidCartRuleValueException extends \PrestaShop\PrestaShop\Core\Domain\CartRule\Exception\CartRuleException
{
}
