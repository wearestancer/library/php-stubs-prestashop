<?php

namespace PrestaShop\PrestaShop\Core\Domain\CartRule\Exception;

/**
 * Thrown in cart rules context
 */
class CartRuleException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
