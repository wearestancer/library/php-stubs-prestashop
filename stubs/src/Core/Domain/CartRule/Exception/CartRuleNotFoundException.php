<?php

namespace PrestaShop\PrestaShop\Core\Domain\CartRule\Exception;

/**
 * Thrown in cart rules context
 */
class CartRuleNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
