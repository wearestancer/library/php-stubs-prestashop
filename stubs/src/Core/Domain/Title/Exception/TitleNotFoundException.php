<?php

namespace PrestaShop\PrestaShop\Core\Domain\Title\Exception;

/**
 * Is thrown when required title cannot be found
 */
class TitleNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Title\Exception\TitleException
{
}
