<?php

namespace PrestaShop\PrestaShop\Core\Domain\Title\Exception;

/**
 * Base exception for title subdomain
 */
class TitleException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
