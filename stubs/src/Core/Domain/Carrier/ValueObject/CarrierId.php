<?php

namespace PrestaShop\PrestaShop\Core\Domain\Carrier\ValueObject;

/**
 * Contains carrier ID with it's constraints
 */
class CarrierId
{
    /**
     * @param int $carrierId
     */
    public function __construct(int $carrierId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
