<?php

namespace PrestaShop\PrestaShop\Core\Domain\Carrier\Exception;

/**
 * Thrown when carrier was not found
 */
class CarrierNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Carrier\Exception\CarrierException
{
}
