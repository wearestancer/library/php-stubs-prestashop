<?php

namespace PrestaShop\PrestaShop\Core\Domain\Carrier\Exception;

/**
 * Thrown when unable to toggle carrier is-free status
 */
class CannotToggleCarrierIsFreeStatusException extends \PrestaShop\PrestaShop\Core\Domain\Carrier\Exception\CarrierException
{
}
