<?php

namespace PrestaShop\PrestaShop\Core\Domain\Carrier\Exception;

/**
 * Base exception for Carrier sub-domain
 */
class CarrierException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
