<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerService\QueryHandler;

/**
 * @internal
 */
class GetCustomerThreadForViewingHandler implements \PrestaShop\PrestaShop\Core\Domain\CustomerService\QueryHandler\GetCustomerThreadForViewingHandlerInterface
{
    /**
     * @param Context $context
     */
    public function __construct(\Context $context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\PrestaShop\PrestaShop\Core\Domain\CustomerService\Query\GetCustomerThreadForViewing $query)
    {
    }
}
