<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerService\QueryResult;

/**
 * Carries data for customer thread timeline
 */
class CustomerThreadTimeline
{
    /**
     * @param CustomerThreadTimelineItem[] $timelineItems
     */
    public function __construct(array $timelineItems)
    {
    }
    /**
     * @return CustomerThreadTimelineItem[]
     */
    public function getTimelineItems()
    {
    }
}
