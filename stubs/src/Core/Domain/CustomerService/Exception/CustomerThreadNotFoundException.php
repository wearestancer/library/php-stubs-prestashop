<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerService\Exception;

/**
 * Thrown when customer thread is not found
 */
class CustomerThreadNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\CustomerService\Exception\CustomerServiceException
{
}
