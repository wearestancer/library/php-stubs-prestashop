<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerService\Exception;

class CannotDeleteCustomerThreadException extends \PrestaShop\PrestaShop\Core\Domain\CustomerService\Exception\CustomerServiceException
{
}
