<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerService\Query;

/**
 * Gets signature for replying in customer thread
 */
class GetCustomerServiceSignature
{
    /**
     * @param int $languageId
     */
    public function __construct($languageId)
    {
    }
    /**
     * @return LanguageId
     */
    public function getLanguageId()
    {
    }
}
