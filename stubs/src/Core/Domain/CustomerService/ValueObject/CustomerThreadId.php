<?php

namespace PrestaShop\PrestaShop\Core\Domain\CustomerService\ValueObject;

/**
 * Defines customer thread id
 */
class CustomerThreadId
{
    /**
     * @param int $customerThreadId
     */
    public function __construct($customerThreadId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
