<?php

namespace PrestaShop\PrestaShop\Core\Domain\Module\Exception;

/**
 * Is thrown when required module cannot be found
 */
class ModuleNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\Module\Exception\ModuleException
{
}
