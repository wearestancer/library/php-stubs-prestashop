<?php

namespace PrestaShop\PrestaShop\Core\Domain\Module\ValueObject;

/**
 * Class ModuleId is responsible for providing currency id data.
 */
class ModuleId
{
    /**
     * @param int $moduleId
     */
    public function __construct(int $moduleId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
