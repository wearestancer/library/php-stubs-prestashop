<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderMessage\Exception;

class OrderMessageConstraintException extends \PrestaShop\PrestaShop\Core\Domain\OrderMessage\Exception\OrderMessageException
{
}
