<?php

namespace PrestaShop\PrestaShop\Core\Domain\OrderMessage\ValueObject;

/**
 * Order message identity
 */
class OrderMessageId
{
    /**
     * @param int $orderMessageId
     */
    public function __construct(int $orderMessageId)
    {
    }
    /**
     * @return int
     */
    public function getValue() : int
    {
    }
}
