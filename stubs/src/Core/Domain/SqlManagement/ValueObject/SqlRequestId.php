<?php

namespace PrestaShop\PrestaShop\Core\Domain\SqlManagement\ValueObject;

/**
 * Class SqlRequestId is SqlRequest identifier.
 */
class SqlRequestId
{
    /**
     * @param int $requestSqlId
     *
     * @throws SqlRequestException
     */
    public function __construct($requestSqlId)
    {
    }
    /**
     * @return int
     */
    public function getValue()
    {
    }
}
