<?php

namespace PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception;

/**
 * Class SqlManagementException base exception of SqlManagement bounded context.
 */
class SqlManagementException extends \PrestaShop\PrestaShop\Core\Domain\Exception\DomainException
{
}
