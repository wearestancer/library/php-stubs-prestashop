<?php

namespace PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception;

/**
 * Base exception for SqlRequest sub-domain
 */
class SqlRequestException extends \PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception\SqlManagementException
{
}
