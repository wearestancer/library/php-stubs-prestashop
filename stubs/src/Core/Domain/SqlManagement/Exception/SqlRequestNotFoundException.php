<?php

namespace PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception;

/**
 * Is thrown when required SqlRequest cannot be found
 */
class SqlRequestNotFoundException extends \PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception\SqlRequestException
{
}
