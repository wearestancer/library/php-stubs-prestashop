<?php

namespace PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception;

/**
 * Is thrown when new SqlRequest cannot be created
 */
class CannotAddSqlRequestException extends \PrestaShop\PrestaShop\Core\Domain\SqlManagement\Exception\SqlRequestException
{
}
