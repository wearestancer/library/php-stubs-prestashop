<?php

namespace PrestaShop\PrestaShop\Core\Export\Exception;

/**
 * Base class for PrestaShop core file export exceptions
 */
class ExportException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
