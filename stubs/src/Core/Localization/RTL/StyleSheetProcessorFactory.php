<?php

namespace PrestaShop\PrestaShop\Core\Localization\RTL;

/**
 * Class StyleSheetProcessorFactory
 */
final class StyleSheetProcessorFactory implements \PrestaShop\PrestaShop\Core\Localization\RTL\StyleSheetProcessorFactoryInterface
{
    /**
     * @param ConfigurationInterface $configuration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\ConfigurationInterface $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create()
    {
    }
}
