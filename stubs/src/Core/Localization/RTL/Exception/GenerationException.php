<?php

namespace PrestaShop\PrestaShop\Core\Localization\RTL\Exception;

/**
 * Is thrown when unable to generate RTL files
 */
class GenerationException extends \PrestaShop\PrestaShop\Core\Localization\Exception\LocalizationException
{
}
