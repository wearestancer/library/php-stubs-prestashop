<?php

namespace PrestaShop\PrestaShop\Core\Localization\Specification;

/**
 * Percentage specification class.
 *
 * Regroups specific rules and data used when formatting a percentage number in a given locale and a given numbering
 * system (latin, arab, ...).
 */
class Percentage extends \PrestaShop\PrestaShop\Core\Localization\Specification\Number
{
}
