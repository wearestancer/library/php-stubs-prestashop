<?php

namespace PrestaShop\PrestaShop\Core\Localization\Exception;

class LocalizationFileNotFoundException extends \PrestaShop\PrestaShop\Core\Localization\Exception\LocalizationException
{
}
