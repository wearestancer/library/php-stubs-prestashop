<?php

namespace PrestaShop\PrestaShop\Core\Localization\Exception;

/**
 * Base class for core localization exceptions
 */
class LocalizationException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
