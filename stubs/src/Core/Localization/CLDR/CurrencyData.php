<?php

namespace PrestaShop\PrestaShop\Core\Localization\CLDR;

/**
 * The CurrencyData class is the exact representation of Currency's data structure inside CLDR xml data files.
 *
 * This class is only used internally, it is mutable and overridable until fully built. It can then be used as
 * an intermediary data bag to build a real CLDR Currency (immutable) object.
 */
class CurrencyData
{
    /**
     * Alphabetic ISO 4217 currency code.
     *
     * @var string
     */
    protected $isoCode;
    /**
     * Numeric ISO 4217 currency code.
     *
     * @var string
     */
    protected $numericIsoCode;
    /**
     * Number of decimal digits to display for a price in this currency.
     *
     * @var int
     */
    protected $decimalDigits;
    /**
     * Possible names depending on count context.
     *
     * e.g.: "Used currency is dollar" (default), "I need one dollar" (one), "I need five dollars" (other)
     * [
     *     'default' => 'dollar',
     *     'one'     => 'dollar',
     *     'other'   => 'dollars',
     * ]
     *
     * @var string[]|null
     */
    protected $displayNames;
    /**
     * Possible symbols (PrestaShop is using narrow).
     *
     * e.g.:
     * [
     *     'default' => 'US$',
     *     'narrow' => '$',
     * ]
     *
     * @var string[]|null
     */
    protected $symbols;
    /**
     * Is the currency used somewhere, or was it deactivated in all territories
     *
     * @var bool|null
     */
    protected $active;
    /**
     * Override this object's data with another CurrencyData object.
     *
     * @param CurrencyData $currencyData
     *                                   Currency data to use for the override
     *
     * @return $this
     *               Fluent interface
     */
    public function overrideWith(\PrestaShop\PrestaShop\Core\Localization\CLDR\CurrencyData $currencyData)
    {
    }
    /**
     * @return string
     */
    public function getIsoCode()
    {
    }
    /**
     * @param string $isoCode
     *
     * @return CurrencyData
     */
    public function setIsoCode($isoCode)
    {
    }
    /**
     * @return string
     */
    public function getNumericIsoCode()
    {
    }
    /**
     * @param string $numericIsoCode
     *
     * @return CurrencyData
     */
    public function setNumericIsoCode($numericIsoCode)
    {
    }
    /**
     * @return int
     */
    public function getDecimalDigits()
    {
    }
    /**
     * @param int $decimalDigits
     *
     * @return CurrencyData
     */
    public function setDecimalDigits($decimalDigits)
    {
    }
    /**
     * @return string[]|null
     */
    public function getDisplayNames()
    {
    }
    /**
     * @param string[] $displayNames
     *
     * @return CurrencyData
     */
    public function setDisplayNames($displayNames)
    {
    }
    /**
     * @return string[]|null
     */
    public function getSymbols()
    {
    }
    /**
     * @param string[] $symbols
     *
     * @return CurrencyData
     */
    public function setSymbols($symbols)
    {
    }
    /**
     * is currency still active in some territory
     *
     * @return bool|null
     */
    public function isActive()
    {
    }
    /**
     * @param bool $active
     */
    public function setActive($active)
    {
    }
}
