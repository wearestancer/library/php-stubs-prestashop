<?php

namespace PrestaShop\PrestaShop\Core\Localization\Pack\Factory;

/**
 * Class LocalizationPackFactory is responsible for creating localization pack instances.
 */
final class LocalizationPackFactory implements \PrestaShop\PrestaShop\Core\Localization\Pack\Factory\LocalizationPackFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createNew()
    {
    }
}
