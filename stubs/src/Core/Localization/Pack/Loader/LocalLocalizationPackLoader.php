<?php

namespace PrestaShop\PrestaShop\Core\Localization\Pack\Loader;

/**
 * Class LocalLocalizationPackLoader is responsible for loading localization pack data from local host.
 */
final class LocalLocalizationPackLoader extends \PrestaShop\PrestaShop\Core\Localization\Pack\Loader\AbstractLocalizationPackLoader
{
    /**
     * @param ConfigurationInterface $configuration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\ConfigurationInterface $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocalizationPackList()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocalizationPack($countryIso)
    {
    }
}
