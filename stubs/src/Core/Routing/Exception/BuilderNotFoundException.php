<?php

namespace PrestaShop\PrestaShop\Core\Routing\Exception;

/**
 * Class BuilderNotFoundException thrown when the factory can't find a builder
 * matching with an entity.
 */
class BuilderNotFoundException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
