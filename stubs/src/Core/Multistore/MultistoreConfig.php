<?php

namespace PrestaShop\PrestaShop\Core\Multistore;

/**
 * Class MultistoreConfig.
 */
class MultistoreConfig
{
    /**
     * Name of configuration for Multistore feature status in ps_configuration table.
     */
    public const FEATURE_STATUS = 'PS_MULTISHOP_FEATURE_ACTIVE';
}
