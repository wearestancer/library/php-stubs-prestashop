<?php

namespace PrestaShop\PrestaShop\Core\B2b;

/**
 * Class B2bFeature checks manages B2B status.
 */
final class B2bFeature implements \PrestaShop\PrestaShop\Core\Feature\FeatureInterface
{
    /**
     * @param ConfigurationInterface $configuration
     */
    public function __construct(\PrestaShop\PrestaShop\Core\ConfigurationInterface $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isUsed()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isActive()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function enable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function disable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($status)
    {
    }
}
