<?php

namespace PrestaShop\PrestaShop\Core\Backup\Exception;

/**
 * Class BackupException is thrown whenever backup fails.
 */
class BackupException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
