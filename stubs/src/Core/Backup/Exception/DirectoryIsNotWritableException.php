<?php

namespace PrestaShop\PrestaShop\Core\Backup\Exception;

/**
 * Class DirectoryIsNotWritableException is thrown when backup directory is not writable.
 */
class DirectoryIsNotWritableException extends \PrestaShop\PrestaShop\Core\Backup\Exception\BackupException
{
}
