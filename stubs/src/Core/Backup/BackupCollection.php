<?php

namespace PrestaShop\PrestaShop\Core\Backup;

final class BackupCollection implements \PrestaShop\PrestaShop\Core\Backup\BackupCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function add(\PrestaShop\PrestaShop\Core\Backup\BackupInterface $backup)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
}
