<?php

namespace PrestaShop\PrestaShop\Core\Import\EntityField;

/**
 * Class EntityFieldCollection defines an entity field collection.
 */
final class EntityFieldCollection implements \PrestaShop\PrestaShop\Core\Import\EntityField\EntityFieldCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function addEntityField(\PrestaShop\PrestaShop\Core\Import\EntityField\EntityFieldInterface $entityField)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequiredFields()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function createFromArray(array $entityFields)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
}
