<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class SkippedIterationException thrown when an import iteration is skipped.
 */
class SkippedIterationException extends \PrestaShop\PrestaShop\Core\Import\Exception\ImportException
{
}
