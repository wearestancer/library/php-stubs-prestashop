<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class UnreadableFileException is thrown when the import file cannot be read.
 */
class UnreadableFileException extends \PrestaShop\PrestaShop\Core\Import\Exception\ImportException
{
}
