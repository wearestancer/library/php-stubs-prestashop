<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class NotSupportedImportEntityException is thrown when not supported import entity is provided.
 */
class NotSupportedImportEntityException extends \PrestaShop\PrestaShop\Core\Import\Exception\ImportException
{
}
