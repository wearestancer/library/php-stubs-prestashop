<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class NotSupportedImportTypeException is thrown when no supported import type is provided.
 */
class NotSupportedImportTypeException extends \PrestaShop\PrestaShop\Core\Import\Exception\ImportException
{
}
