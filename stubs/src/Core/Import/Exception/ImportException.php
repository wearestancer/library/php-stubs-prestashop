<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class ImportException is extended by all import exceptions.
 */
class ImportException extends \PrestaShop\PrestaShop\Core\Exception\CoreException
{
}
