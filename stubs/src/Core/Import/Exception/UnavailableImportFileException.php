<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class UnavailableImportFileException thrown when import file is not available.
 */
class UnavailableImportFileException extends \PrestaShop\PrestaShop\Core\Import\Exception\ImportException
{
}
