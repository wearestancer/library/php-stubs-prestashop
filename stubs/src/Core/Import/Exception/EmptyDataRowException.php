<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class EmptyDataRowException thrown when the import handler finds an empty data row.
 */
class EmptyDataRowException extends \PrestaShop\PrestaShop\Core\Import\Exception\InvalidDataRowException
{
}
