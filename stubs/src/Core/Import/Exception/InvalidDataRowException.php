<?php

namespace PrestaShop\PrestaShop\Core\Import\Exception;

/**
 * Class InvalidDataRowException thrown when import handler encounters an invalid data row.
 */
class InvalidDataRowException extends \PrestaShop\PrestaShop\Core\Import\Exception\ImportException
{
}
