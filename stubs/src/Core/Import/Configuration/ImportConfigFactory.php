<?php

namespace PrestaShop\PrestaShop\Core\Import\Configuration;

/**
 * Class ImportConfigFactory describes an import configuration factory.
 */
final class ImportConfigFactory implements \PrestaShop\PrestaShop\Core\Import\Configuration\ImportConfigFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildFromRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
