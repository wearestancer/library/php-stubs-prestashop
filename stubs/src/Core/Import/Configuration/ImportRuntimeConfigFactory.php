<?php

namespace PrestaShop\PrestaShop\Core\Import\Configuration;

/**
 * Class ImportRuntimeConfigFactory is responsible for building import runtime config.
 */
final class ImportRuntimeConfigFactory implements \PrestaShop\PrestaShop\Core\Import\Configuration\ImportRuntimeConfigFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildFromRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
