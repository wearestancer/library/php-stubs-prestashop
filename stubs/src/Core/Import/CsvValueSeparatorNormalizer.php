<?php

namespace PrestaShop\PrestaShop\Core\Import;

/**
 * Class CsvValueSeparatorNormalizer normalizes import separator before usage.
 */
final class CsvValueSeparatorNormalizer implements \PrestaShop\PrestaShop\Core\Import\StringNormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($value)
    {
    }
}
