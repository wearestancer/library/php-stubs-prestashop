<?php

namespace PrestaShop\PrestaShop\Core\Import\Validator;

/**
 * Class ImportRequestValidator is responsible for validating import request.
 */
final class ImportRequestValidator implements \PrestaShop\PrestaShop\Core\Import\Validator\ImportRequestValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
