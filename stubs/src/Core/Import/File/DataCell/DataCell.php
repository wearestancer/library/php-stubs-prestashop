<?php

namespace PrestaShop\PrestaShop\Core\Import\File\DataCell;

/**
 * Class DataCell defines a data cell of imported file.
 */
final class DataCell implements \PrestaShop\PrestaShop\Core\Import\File\DataCell\DataCellInterface
{
    /**
     * @param string $key
     * @param string $value
     */
    public function __construct($key, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
    }
}
