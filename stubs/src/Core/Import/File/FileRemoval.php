<?php

namespace PrestaShop\PrestaShop\Core\Import\File;

/**
 * FileRemoval is responsible for deleting import files.
 */
final class FileRemoval
{
    public function __construct(\PrestaShop\PrestaShop\Core\Import\ImportDirectory $importDirectory)
    {
    }
    /**
     * Remove file from import directory.
     *
     * @param string $filename
     */
    public function remove($filename)
    {
    }
}
