<?php

namespace PrestaShop\PrestaShop\Core\Import\File;

/**
 * Class responsible for finding import files.
 */
final class FileFinder
{
    public function __construct(\PrestaShop\PrestaShop\Core\Import\ImportDirectory $importDirectory)
    {
    }
    /**
     * Get import file names in import directory.
     *
     * @return array|string[]
     */
    public function getImportFileNames()
    {
    }
}
