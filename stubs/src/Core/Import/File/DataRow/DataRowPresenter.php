<?php

namespace PrestaShop\PrestaShop\Core\Import\File\DataRow;

/**
 * Class DataRowPresenter defines a data row presenter.
 */
final class DataRowPresenter implements \PrestaShop\PrestaShop\Core\Import\File\DataRow\DataRowPresenterInterface
{
    /**
     * {@inheritdoc}
     */
    public function present(\PrestaShop\PrestaShop\Core\Import\File\DataRow\DataRowInterface $dataRow)
    {
    }
}
