<?php

namespace PrestaShop\PrestaShop\Core\Import\File\DataRow;

/**
 * Class DataRow defines a basic data row of imported file.
 */
final class DataRow implements \PrestaShop\PrestaShop\Core\Import\File\DataRow\DataRowInterface
{
    /**
     * {@inheritdoc}
     */
    public function addCell(\PrestaShop\PrestaShop\Core\Import\File\DataCell\DataCellInterface $cell)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function createFromArray(array $data)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
    }
}
