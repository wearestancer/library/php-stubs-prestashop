<?php

namespace PHPSQLParser\lexer;

/**
 * This class holds a sorted array of characters, which are used as stop token.
 * On every part of the array the given SQL string will be split into single tokens.
 * The array must be sorted by element size, longest first (3 chars -> 2 chars -> 1 char).
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class LexerSplitter
{
    protected static $splitters = array("<=>", "\r\n", "!=", ">=", "<=", "<>", "<<", ">>", ":=", "\\", "&&", "||", ":=", "/*", "*/", "--", ">", "<", "|", "=", "^", "(", ")", "\t", "\n", "'", "\"", "`", ",", "@", " ", "+", "-", "*", "/", ";");
    /**
     * @var string Regex string pattern of splitters.
     */
    protected $splitterPattern;
    /**
     * Constructor.
     *
     * It initializes some fields.
     */
    public function __construct()
    {
    }
    /**
     * Get the regex pattern string of all the splitters
     *
     * @return string
     */
    public function getSplittersRegexPattern()
    {
    }
    /**
     * Convert an array of splitter tokens to a regex pattern string.
     *
     * @param array $splitters
     *
     * @return string
     */
    public function convertSplittersToRegexPattern($splitters)
    {
    }
}
