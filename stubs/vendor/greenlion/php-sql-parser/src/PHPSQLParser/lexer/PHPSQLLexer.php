<?php

namespace PHPSQLParser\lexer;

/**
 * This class splits the SQL string into little parts, which the parser can
 * use to build the result array.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class PHPSQLLexer
{
    protected $splitters;
    /**
     * Constructor.
     *
     * It initializes some fields.
     */
    public function __construct()
    {
    }
    /**
     * Ends the given string $haystack with the string $needle?
     *
     * @param string $haystack
     * @param string $needle
     *
     * @return boolean true, if the parameter $haystack ends with the character sequences $needle, false otherwise
     */
    protected function endsWith($haystack, $needle)
    {
    }
    public function split($sql)
    {
    }
    protected function concatNegativeNumbers($tokens)
    {
    }
    protected function concatScientificNotations($tokens)
    {
    }
    protected function concatUserDefinedVariables($tokens)
    {
    }
    protected function concatComments($tokens)
    {
    }
    protected function isBacktick($token)
    {
    }
    protected function balanceBackticks($tokens)
    {
    }
    // backticks are not balanced within one token, so we have
    // to re-combine some tokens
    protected function balanceCharacter($tokens, $idx, $char)
    {
    }
    /**
     * This function concats some tokens to a column reference.
     * There are two different cases:
     *
     * 1. If the current token ends with a dot, we will add the next token
     * 2. If the next token starts with a dot, we will add it to the previous token
     *
     */
    protected function concatColReferences($tokens)
    {
    }
    protected function concatEscapeSequences($tokens)
    {
    }
    protected function balanceParenthesis($tokens)
    {
    }
}
