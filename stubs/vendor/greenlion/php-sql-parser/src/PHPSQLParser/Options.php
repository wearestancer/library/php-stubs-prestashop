<?php

namespace PHPSQLParser;

/**
 *
 * @author  mfris
 * @package PHPSQLParser
 */
final class Options
{
    /**
     * @const string
     */
    const CONSISTENT_SUB_TREES = 'consistent_sub_trees';
    /**
     * @const string
     */
    const ANSI_QUOTES = 'ansi_quotes';
    /**
     * Options constructor.
     *
     * @param array $options
     */
    public function __construct(array $options)
    {
    }
    /**
     * @return bool
     */
    public function getConsistentSubtrees()
    {
    }
    /**
     * @return bool
     */
    public function getANSIQuotes()
    {
    }
}
