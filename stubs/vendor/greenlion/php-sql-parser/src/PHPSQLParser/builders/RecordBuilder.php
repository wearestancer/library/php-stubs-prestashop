<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the records within INSERT statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class RecordBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildOperator($parsed)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildColRef($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
