<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the SET part of INSERT statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class SetExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColRef($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildOperator($parsed)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildBracketExpression($parsed)
    {
    }
    protected function buildSign($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
