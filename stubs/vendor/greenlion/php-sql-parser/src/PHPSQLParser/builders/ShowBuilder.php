<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the SHOW statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ShowBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildTable($parsed, $delim)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildProcedure($parsed)
    {
    }
    protected function buildDatabase($parsed)
    {
    }
    protected function buildEngine($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
