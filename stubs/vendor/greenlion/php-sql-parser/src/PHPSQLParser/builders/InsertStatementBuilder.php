<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Insert statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class InsertStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildVALUES($parsed)
    {
    }
    protected function buildINSERT($parsed)
    {
    }
    protected function buildSELECT($parsed)
    {
    }
    protected function buildSET($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
