<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the DROP INDEX statement. You can overwrite
 * all functions to achieve another handling.
 */
class DropIndexBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildIndexTable($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
