<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Create statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildLIKE($parsed)
    {
    }
    protected function buildSelectStatement($parsed)
    {
    }
    protected function buildCREATE($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
