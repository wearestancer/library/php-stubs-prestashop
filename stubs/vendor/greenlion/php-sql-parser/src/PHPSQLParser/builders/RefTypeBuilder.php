<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the references type within a JOIN.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class RefTypeBuilder
{
    public function build($parsed)
    {
    }
}
