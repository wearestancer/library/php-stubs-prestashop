<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the PRIMARY KEY  statement part of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class PrimaryKeyBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColumnList($parsed)
    {
    }
    protected function buildConstraint($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    protected function buildIndexType($parsed)
    {
    }
    protected function buildIndexSize($parsed)
    {
    }
    protected function buildIndexParser($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
