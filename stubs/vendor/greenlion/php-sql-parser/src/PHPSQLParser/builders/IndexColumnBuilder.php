<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for index column entries of the column-list
 * parts of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class IndexColumnBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildLength($parsed)
    {
    }
    protected function buildDirection($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
