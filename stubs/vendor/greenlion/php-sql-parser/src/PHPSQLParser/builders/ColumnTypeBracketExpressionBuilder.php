<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for bracket expressions within a column type.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ColumnTypeBracketExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildSubTree($parsed, $delim)
    {
    }
    public function build(array $parsed)
    {
    }
}
