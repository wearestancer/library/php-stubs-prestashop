<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for bracket-expressions within the ORDER-BY part.
 * It must contain the direction.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OrderByBracketExpressionBuilder extends \PHPSQLParser\builders\WhereBracketExpressionBuilder
{
    protected function buildDirection($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
