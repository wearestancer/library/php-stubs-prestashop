<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the VALUES part of INSERT statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ValuesBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildRecord($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
    protected function getRecordDelimiter($parsed)
    {
    }
}
