<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for bracket expressions within the HAVING part.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  Ian Barker <ian@theorganicagency.com>
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class HavingBracketExpressionBuilder extends \PHPSQLParser\builders\WhereBracketExpressionBuilder
{
    protected function buildHavingExpression($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
