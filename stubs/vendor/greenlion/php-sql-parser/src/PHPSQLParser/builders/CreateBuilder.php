<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the [CREATE] part. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildCreateTable($parsed)
    {
    }
    protected function buildCreateIndex($parsed)
    {
    }
    protected function buildSubTree($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
