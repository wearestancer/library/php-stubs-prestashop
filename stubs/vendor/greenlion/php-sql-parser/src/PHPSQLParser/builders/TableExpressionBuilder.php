<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the table name and join options.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class TableExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildFROM($parsed)
    {
    }
    protected function buildAlias($parsed)
    {
    }
    protected function buildJoin($parsed)
    {
    }
    protected function buildRefType($parsed)
    {
    }
    protected function buildRefClause($parsed)
    {
    }
    public function build(array $parsed, $index = 0)
    {
    }
}
