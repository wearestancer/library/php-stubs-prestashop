<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Update statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UpdateStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildWHERE($parsed)
    {
    }
    protected function buildSET($parsed)
    {
    }
    protected function buildUPDATE($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
