<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for expressions within the WHERE part.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class WhereExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColRef($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildOperator($parsed)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildInList($parsed)
    {
    }
    protected function buildWhereExpression($parsed)
    {
    }
    protected function buildWhereBracketExpression($parsed)
    {
    }
    protected function buildUserVariable($parsed)
    {
    }
    protected function buildSubQuery($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
