<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the GROUP-BY clause.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class GroupByBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColRef($parsed)
    {
    }
    protected function buildPosition($parsed)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildGroupByAlias($parsed)
    {
    }
    protected function buildGroupByExpression($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
