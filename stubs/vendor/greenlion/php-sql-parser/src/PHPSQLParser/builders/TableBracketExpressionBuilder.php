<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the table expressions
 * within the create definitions of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class TableBracketExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColDef($parsed)
    {
    }
    protected function buildPrimaryKey($parsed)
    {
    }
    protected function buildForeignKey($parsed)
    {
    }
    protected function buildCheck($parsed)
    {
    }
    protected function buildLikeExpression($parsed)
    {
    }
    protected function buildIndexKey($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
