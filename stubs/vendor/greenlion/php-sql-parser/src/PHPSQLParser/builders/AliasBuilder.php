<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for aliases.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class AliasBuilder implements \PHPSQLParser\builders\Builder
{
    public function hasAlias($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
