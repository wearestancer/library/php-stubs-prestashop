<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the references clause within a JOIN.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class RefClauseBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildInList($parsed)
    {
    }
    protected function buildColRef($parsed)
    {
    }
    protected function buildOperator($parsed)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildBracketExpression($parsed)
    {
    }
    protected function buildColumnList($parsed)
    {
    }
    protected function buildSubQuery($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
