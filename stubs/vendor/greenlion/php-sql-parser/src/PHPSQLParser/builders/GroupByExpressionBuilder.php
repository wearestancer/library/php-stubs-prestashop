<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for an alias within the GROUP-BY clause.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  oohook <oohook@163.com>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class GroupByExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColRef($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
