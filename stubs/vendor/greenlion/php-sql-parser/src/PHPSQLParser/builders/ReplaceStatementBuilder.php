<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Replace statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ReplaceStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildVALUES($parsed)
    {
    }
    protected function buildREPLACE($parsed)
    {
    }
    protected function buildSELECT($parsed)
    {
    }
    protected function buildSET($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
