<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Truncate statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class TruncateStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildTRUNCATE($parsed)
    {
    }
    protected function buildFROM($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
