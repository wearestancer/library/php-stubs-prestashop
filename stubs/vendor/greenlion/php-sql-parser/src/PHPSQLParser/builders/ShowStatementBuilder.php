<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the SHOW statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ShowStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildWHERE($parsed)
    {
    }
    protected function buildSHOW($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
