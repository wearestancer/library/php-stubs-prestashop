<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for reserved keywords within the ORDER-BY part.
 * It must contain the direction.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OrderByReservedBuilder extends \PHPSQLParser\builders\ReservedBuilder
{
    protected function buildDirection($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
