<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the CHECK statement part of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CheckBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildSelectBracketExpression($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
