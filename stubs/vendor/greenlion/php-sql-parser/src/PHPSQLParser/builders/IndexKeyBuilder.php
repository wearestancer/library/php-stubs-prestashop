<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the index key part of a CREATE TABLE statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class IndexKeyBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildReserved($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildIndexType($parsed)
    {
    }
    protected function buildColumnList($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
