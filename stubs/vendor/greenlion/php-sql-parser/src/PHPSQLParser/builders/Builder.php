<?php

namespace PHPSQLParser\builders;

/**
 * A builder can create a part of an SQL statement. The necessary information
 * are provided by the function parameter as array. This array is a subtree
 * of the PHPSQLParser output.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 */
interface Builder
{
    /**
     * Builds a part of an SQL statement.
     *
     * @param array $parsed a subtree of the PHPSQLParser output array
     *
     * @return A string, which contains a part of an SQL statement.
     */
    public function build(array $parsed);
}
