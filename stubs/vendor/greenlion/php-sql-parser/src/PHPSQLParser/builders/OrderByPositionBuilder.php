<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for positions of the GROUP-BY clause.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OrderByPositionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildDirection($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
