<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the table part of a DROP INDEX statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class DropIndexTableBuilder implements \PHPSQLParser\builders\Builder
{
    public function build(array $parsed)
    {
    }
}
