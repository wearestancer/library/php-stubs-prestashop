<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Select statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class SelectStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildSELECT($parsed)
    {
    }
    protected function buildFROM($parsed)
    {
    }
    protected function buildWHERE($parsed)
    {
    }
    protected function buildGROUP($parsed)
    {
    }
    protected function buildHAVING($parsed)
    {
    }
    protected function buildORDER($parsed)
    {
    }
    protected function buildLIMIT($parsed)
    {
    }
    protected function buildUNION($parsed)
    {
    }
    protected function buildUNIONALL($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
