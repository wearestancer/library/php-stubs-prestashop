<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the CREATE TABLE statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateTableBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildCreateTableDefinition($parsed)
    {
    }
    protected function buildCreateTableOptions($parsed)
    {
    }
    protected function buildCreateTableSelectOption($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
