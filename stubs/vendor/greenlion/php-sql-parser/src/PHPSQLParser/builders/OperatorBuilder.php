<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for operators.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OperatorBuilder implements \PHPSQLParser\builders\Builder
{
    public function build(array $parsed)
    {
    }
}
