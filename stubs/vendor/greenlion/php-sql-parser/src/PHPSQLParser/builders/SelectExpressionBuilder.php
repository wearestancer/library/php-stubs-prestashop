<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for simple expressions within a SELECT statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class SelectExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildSubTree($parsed, $delim)
    {
    }
    protected function buildAlias($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
