<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the ORDER-BY clause.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OrderByBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildFunction($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    protected function buildColRef($parsed)
    {
    }
    protected function buildAlias($parsed)
    {
    }
    protected function buildExpression($parsed)
    {
    }
    protected function buildBracketExpression($parsed)
    {
    }
    protected function buildPosition($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
