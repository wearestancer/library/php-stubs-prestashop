<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the HAVING part.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  Ian Barker <ian@theorganicagency.com>
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class HavingBuilder extends \PHPSQLParser\builders\WhereBuilder
{
    protected function buildAliasReference($parsed)
    {
    }
    protected function buildHavingExpression($parsed)
    {
    }
    protected function buildHavingBracketExpression($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
