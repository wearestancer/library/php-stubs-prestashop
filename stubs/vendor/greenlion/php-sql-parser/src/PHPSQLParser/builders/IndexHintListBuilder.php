<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for index hint lists.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class IndexHintListBuilder implements \PHPSQLParser\builders\Builder
{
    public function hasHint($parsed)
    {
    }
    // TODO: the hint list should be enhanced to get base_expr fro position calculation
    public function build(array $parsed)
    {
    }
}
