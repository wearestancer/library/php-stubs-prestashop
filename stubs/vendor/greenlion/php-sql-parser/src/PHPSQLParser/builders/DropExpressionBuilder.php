<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the object list of a DROP statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class DropExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildTable($parsed, $index)
    {
    }
    protected function buildDatabase($parsed)
    {
    }
    protected function buildSchema($parsed)
    {
    }
    protected function buildTemporaryTable($parsed)
    {
    }
    protected function buildView($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
