<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the UPDATE statement parts.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UpdateBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildTable($parsed, $idx)
    {
    }
    public function build(array $parsed)
    {
    }
}
