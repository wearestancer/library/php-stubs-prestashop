<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the FOREIGN KEY statement part of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ForeignKeyBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildConstant($parsed)
    {
    }
    protected function buildColumnList($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    protected function buildForeignRef($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
