<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the parentheses around a statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class BracketStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildSelectBracketExpression($parsed)
    {
    }
    protected function buildSelectStatement($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
