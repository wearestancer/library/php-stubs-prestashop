<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for column-list parts of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ColumnListBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildIndexColumn($parsed)
    {
    }
    protected function buildColumnReference($parsed)
    {
    }
    public function build(array $parsed, $delim = ', ')
    {
    }
}
