<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the index type of a CREATE INDEX
 * statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateIndexTypeBuilder extends \PHPSQLParser\builders\IndexTypeBuilder
{
    public function build(array $parsed)
    {
    }
}
