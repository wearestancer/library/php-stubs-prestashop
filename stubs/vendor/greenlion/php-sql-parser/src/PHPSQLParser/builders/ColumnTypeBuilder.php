<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the column type statement part of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ColumnTypeBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColumnTypeBracketExpression($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    protected function buildDataType($parsed)
    {
    }
    protected function buildDefaultValue($parsed)
    {
    }
    protected function buildCharacterSet($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
