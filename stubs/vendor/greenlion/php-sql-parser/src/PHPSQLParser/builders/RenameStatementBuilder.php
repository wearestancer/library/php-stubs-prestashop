<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the RENAME statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class RenameStatementBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildReserved($parsed)
    {
    }
    protected function processSourceAndDestTable($v)
    {
    }
    public function build(array $parsed)
    {
    }
}
