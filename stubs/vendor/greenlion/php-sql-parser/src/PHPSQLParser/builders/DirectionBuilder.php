<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for directions (e.g. of the order-by clause).
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class DirectionBuilder implements \PHPSQLParser\builders\Builder
{
    public function build(array $parsed)
    {
    }
}
