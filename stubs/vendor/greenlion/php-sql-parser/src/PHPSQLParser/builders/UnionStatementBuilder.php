<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the whole Union statement. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  George Schneeloch <george_schneeloch@hms.harvard.edu>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UnionStatementBuilder implements \PHPSQLParser\builders\Builder
{
    public function build(array $parsed)
    {
    }
}
