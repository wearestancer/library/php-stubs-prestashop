<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for an alias within the ORDER-BY clause.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OrderByAliasBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildDirection($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
