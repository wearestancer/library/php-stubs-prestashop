<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for function calls.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class FunctionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildAlias($parsed)
    {
    }
    protected function buildColRef($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    protected function isReserved($parsed)
    {
    }
    protected function buildSelectExpression($parsed)
    {
    }
    protected function buildSelectBracketExpression($parsed)
    {
    }
    protected function buildSubQuery($parsed)
    {
    }
    protected function buildUserVariableExpression($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
