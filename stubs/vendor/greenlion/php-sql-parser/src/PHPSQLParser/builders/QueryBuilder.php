<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for queries within parentheses (no subqueries).
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class QueryBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildRefClause($parsed)
    {
    }
    protected function buildRefType($parsed)
    {
    }
    protected function buildJoin($parsed)
    {
    }
    protected function buildAlias($parsed)
    {
    }
    protected function buildSelectStatement($parsed)
    {
    }
    public function build(array $parsed, $index = 0)
    {
    }
}
