<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the [DROP] part. You can overwrite
 * all functions to achieve another handling.
 */
class DropBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildDropIndex($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    protected function buildExpression($parsed)
    {
    }
    protected function buildSubTree($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
