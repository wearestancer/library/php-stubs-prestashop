<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the (LIKE) keyword within a
 * CREATE TABLE statement. There are difference to LIKE (without parenthesis),
 * the latter is a top-level element of the output array.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class LikeExpressionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildTable($parsed, $index)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
