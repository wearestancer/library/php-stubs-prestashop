<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the [SELECT] field. You can overwrite
 * all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class SelectBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildConstant($parsed)
    {
    }
    protected function buildFunction($parsed)
    {
    }
    protected function buildSelectExpression($parsed)
    {
    }
    protected function buildSelectBracketExpression($parsed)
    {
    }
    protected function buildColRef($parsed)
    {
    }
    protected function buildReserved($parsed)
    {
    }
    /**
     * Returns a well-formatted delimiter string. If you don't need nice SQL,
     * you could simply return $parsed['delim'].
     *
     * @param array $parsed The part of the output array, which contains the current expression.
     * @return a string, which is added right after the expression
     */
    protected function getDelimiter($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
