<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the index options of a CREATE INDEX
 * statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateIndexOptionsBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildIndexParser($parsed)
    {
    }
    protected function buildIndexSize($parsed)
    {
    }
    protected function buildIndexType($parsed)
    {
    }
    protected function buildIndexComment($parsed)
    {
    }
    protected function buildIndexAlgorithm($parsed)
    {
    }
    protected function buildIndexLock($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
