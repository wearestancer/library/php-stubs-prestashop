<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the JOIN statement parts (within FROM).
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @author  George Schneeloch <noisecapella@gmail.com>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class JoinBuilder
{
    public function build($parsed)
    {
    }
}
