<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the index comment of CREATE INDEX statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class IndexCommentBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildReserved($parsed)
    {
    }
    protected function buildConstant($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
