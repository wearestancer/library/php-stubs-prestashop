<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder for the columndefinition statement part
 * of CREATE TABLE. You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ColumnDefinitionBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildColRef($parsed)
    {
    }
    protected function buildColumnType($parsed)
    {
    }
    public function build(array $parsed)
    {
    }
}
