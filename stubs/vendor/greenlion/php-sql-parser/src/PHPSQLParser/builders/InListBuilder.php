<?php

namespace PHPSQLParser\builders;

/**
 * This class implements the builder list of values for the IN statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class InListBuilder implements \PHPSQLParser\builders\Builder
{
    protected function buildSubTree($parsed, $delim)
    {
    }
    public function build(array $parsed)
    {
    }
}
