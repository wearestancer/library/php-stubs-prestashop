<?php

namespace PHPSQLParser;

/**
 * This class implements the parser functionality.
 *
 * @author  Justin Swanhart <greenlion@gmail.com>
 * @author  André Rothe <arothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 */
class PHPSQLParser
{
    public $parsed;
    /**
     * Constructor. It simply calls the parse() function.
     * Use the public variable $parsed to get the output.
     *
     * @param String|bool  $sql           The SQL statement.
     * @param bool $calcPositions True, if the output should contain [position], false otherwise.
     * @param array $options
     */
    public function __construct($sql = false, $calcPositions = false, array $options = array())
    {
    }
    /**
     * It parses the given SQL statement and generates a detailled
     * output array for every part of the statement. The method can
     * also generate [position] fields within the output, which hold
     * the character position for every statement part. The calculation
     * of the positions needs some time, if you don't need positions in
     * your application, set the parameter to false.
     *
     * @param String  $sql           The SQL statement.
     * @param boolean $calcPositions True, if the output should contain [position], false otherwise.
     *
     * @return array An associative array with all meta information about the SQL statement.
     */
    public function parse($sql, $calcPositions = false)
    {
    }
    /**
     * Add a custom function to the parser.  no return value
     *
     * @param String $token The name of the function to add
     *
     * @return null
     */
    public function addCustomFunction($token)
    {
    }
    /**
     * Remove a custom function from the parser.  no return value
     *
     * @param String $token The name of the function to remove
     *
     * @return null
     */
    public function removeCustomFunction($token)
    {
    }
    /**
     * Returns the list of custom functions
     *
     * @return array Returns an array of all custom functions
     */
    public function getCustomFunctions()
    {
    }
}
