<?php

namespace PHPSQLParser;

/**
 * This class generates SQL from the output of the PHPSQLParser.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class PHPSQLCreator
{
    public function __construct($parsed = false)
    {
    }
    public function create($parsed)
    {
    }
}
