<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the SQL chunks.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class SQLChunkProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function moveLIKE(&$out)
    {
    }
    public function process($out)
    {
    }
}
