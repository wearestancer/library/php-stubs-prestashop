<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the SET statements.
 *
 * @author arothe
 *
 */
class SetProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($tokens)
    {
    }
    /**
     * A SET list is simply a list of key = value expressions separated by comma (,).
     * This function produces a list of the key/value expressions.
     */
    protected function processAssignment($base_expr)
    {
    }
    public function process($tokens, $isUpdate = false)
    {
    }
}
