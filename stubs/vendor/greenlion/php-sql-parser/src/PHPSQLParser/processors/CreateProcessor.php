<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the CREATE statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    public function process($tokens)
    {
    }
}
