<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the incoming sql string.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class DefaultProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function isUnion($tokens)
    {
    }
    protected function processUnion($tokens)
    {
    }
    protected function processSQL($tokens)
    {
    }
    public function process($sql)
    {
    }
    public function revokeQuotation($sql)
    {
    }
}
