<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the INSERT statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class InsertProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processOptions($tokenList)
    {
    }
    protected function processKeyword($keyword, $tokenList)
    {
    }
    protected function processColumns($cols)
    {
    }
    public function process($tokenList, $token_category = 'INSERT')
    {
    }
}
