<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the DELETE statements.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class DeleteProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    public function process($tokens)
    {
    }
}
