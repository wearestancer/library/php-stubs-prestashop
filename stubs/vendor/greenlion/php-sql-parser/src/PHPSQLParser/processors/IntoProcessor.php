<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the INTO statements.
 *
 * @author arothe
 *
 */
class IntoProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    /**
     * TODO: This is a dummy function, we cannot parse INTO as part of SELECT
     * at the moment
     */
    public function process($tokenList)
    {
    }
}
