<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the PARTITION BY statements within CREATE TABLE.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class PartitionOptionsProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($unparsed)
    {
    }
    protected function processColumnList($unparsed)
    {
    }
    protected function processPartitionDefinition($unparsed)
    {
    }
    protected function getReservedType($token)
    {
    }
    protected function getConstantType($token)
    {
    }
    protected function getOperatorType($token)
    {
    }
    protected function getBracketExpressionType($token)
    {
    }
    public function process($tokens)
    {
    }
}
