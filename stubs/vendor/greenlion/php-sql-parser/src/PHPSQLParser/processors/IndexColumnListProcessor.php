<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the index column lists.
 *
 * @author arothe
 *
 */
class IndexColumnListProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function initExpression()
    {
    }
    public function process($sql)
    {
    }
}
