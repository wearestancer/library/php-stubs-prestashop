<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the VALUES statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ValuesProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($unparsed)
    {
    }
    protected function processRecord($unparsed)
    {
    }
    public function process($tokens)
    {
    }
}
