<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the base SQL statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @author  Marco Th. <marco64th@gmail.com>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class SQLProcessor extends \PHPSQLParser\processors\SQLChunkProcessor
{
    /**
     * This function breaks up the SQL statement into logical sections.
     * Some sections are delegated to specialized processors.
     */
    public function process($tokens)
    {
    }
}
