<?php

namespace PHPSQLParser\processors;

/**
 * This class implements the processor for the HAVING statement.
 * You can overwrite all functions to achieve another handling.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class HavingProcessor extends \PHPSQLParser\processors\ExpressionListProcessor
{
    public function process($tokens, $select = array())
    {
    }
}
