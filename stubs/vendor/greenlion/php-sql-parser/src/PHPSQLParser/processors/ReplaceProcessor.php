<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the REPLACE statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ReplaceProcessor extends \PHPSQLParser\processors\InsertProcessor
{
    public function process($tokenList, $token_category = 'REPLACE')
    {
    }
}
