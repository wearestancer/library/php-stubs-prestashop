<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the DESCRIBE statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class DescribeProcessor extends \PHPSQLParser\processors\ExplainProcessor
{
    protected function isStatement($keys, $needle = "DESCRIBE")
    {
    }
}
