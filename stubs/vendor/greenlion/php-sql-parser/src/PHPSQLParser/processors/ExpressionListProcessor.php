<?php

namespace PHPSQLParser\processors;

/**
 * This class processes expression lists.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ExpressionListProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    public function process($tokens)
    {
    }
}
