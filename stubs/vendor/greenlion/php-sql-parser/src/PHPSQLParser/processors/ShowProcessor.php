<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the SHOW statements.
 *
 * @author arothe
 *
 */
class ShowProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    public function __construct(\PHPSQLParser\Options $options)
    {
    }
    public function process($tokens)
    {
    }
}
