<?php

namespace PHPSQLParser\processors;

/**
 * This class processes records of an INSERT statement.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class RecordProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($unparsed)
    {
    }
    public function process($unparsed)
    {
    }
}
