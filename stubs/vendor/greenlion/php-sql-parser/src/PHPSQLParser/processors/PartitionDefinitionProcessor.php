<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the PARTITION statements within CREATE TABLE.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class PartitionDefinitionProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($unparsed)
    {
    }
    protected function processSubpartitionDefinition($unparsed)
    {
    }
    protected function getReservedType($token)
    {
    }
    protected function getConstantType($token)
    {
    }
    protected function getOperatorType($token)
    {
    }
    protected function getBracketExpressionType($token)
    {
    }
    public function process($tokens)
    {
    }
}
