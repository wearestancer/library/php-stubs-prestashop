<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes Oracle's WITH statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class WithProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processTopLevel($sql)
    {
    }
    protected function buildTableName($token)
    {
    }
    public function process($tokens)
    {
    }
}
