<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the column definition part of a CREATE TABLE statement.
 *
 * @author arothe
 *
 */
class ColumnDefinitionProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($parsed)
    {
    }
    protected function processReferenceDefinition($parsed)
    {
    }
    protected function removeComma($tokens)
    {
    }
    protected function buildColDef($expr, $base_expr, $options, $refs, $key)
    {
    }
    public function process($tokens)
    {
    }
}
