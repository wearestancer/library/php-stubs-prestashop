<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the create definition of the TABLE statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class CreateDefinitionProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($parsed)
    {
    }
    protected function processIndexColumnList($parsed)
    {
    }
    protected function processColumnDefinition($parsed)
    {
    }
    protected function processReferenceDefinition($parsed)
    {
    }
    protected function correctExpressionType(&$expr)
    {
    }
    public function process($tokens)
    {
    }
}
