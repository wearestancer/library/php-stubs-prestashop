<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the INDEX statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class IndexProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function getReservedType($token)
    {
    }
    protected function getConstantType($token)
    {
    }
    protected function getOperatorType($token)
    {
    }
    protected function processIndexColumnList($parsed)
    {
    }
    public function process($tokens)
    {
    }
}
