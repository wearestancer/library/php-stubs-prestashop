<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the RENAME statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class RenameProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    public function process($tokenList)
    {
    }
}
