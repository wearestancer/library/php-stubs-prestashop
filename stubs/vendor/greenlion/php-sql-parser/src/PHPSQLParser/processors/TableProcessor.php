<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the TABLE statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class TableProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function getReservedType($token)
    {
    }
    protected function getConstantType($token)
    {
    }
    protected function getOperatorType($token)
    {
    }
    protected function processPartitionOptions($tokens)
    {
    }
    protected function processCreateDefinition($tokens)
    {
    }
    protected function clear(&$expr, &$base_expr, &$category)
    {
    }
    public function process($tokens)
    {
    }
}
