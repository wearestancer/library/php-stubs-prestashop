<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the EXPLAIN statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class ExplainProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function isStatement($keys, $needle = "EXPLAIN")
    {
    }
    // TODO: refactor that function
    public function process($tokens, $keys = array())
    {
    }
}
