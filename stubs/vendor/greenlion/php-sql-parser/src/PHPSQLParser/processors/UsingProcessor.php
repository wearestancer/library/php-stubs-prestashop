<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the USING statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UsingProcessor extends \PHPSQLParser\processors\FromProcessor
{
}
