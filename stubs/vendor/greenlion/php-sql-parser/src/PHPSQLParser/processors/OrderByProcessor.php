<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the ORDER-BY statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class OrderByProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processSelectExpression($unparsed)
    {
    }
    protected function initParseInfo()
    {
    }
    protected function processOrderExpression(&$parseInfo, $select)
    {
    }
    public function process($tokens, $select = array())
    {
    }
}
