<?php

namespace PHPSQLParser\processors;

/**
 * This class contains some general functions for a processor.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
abstract class AbstractProcessor
{
    /**
     * @var Options
     */
    protected $options;
    /**
     * AbstractProcessor constructor.
     *
     * @param Options $options
     */
    public function __construct(\PHPSQLParser\Options $options = null)
    {
    }
    /**
     * This function implements the main functionality of a processor class.
     * Always use default valuses for additional parameters within overridden functions.
     */
    public abstract function process($tokens);
    /**
     * this function splits up a SQL statement into easy to "parse"
     * tokens for the SQL processor
     */
    public function splitSQLIntoTokens($sql)
    {
    }
    /**
     * Revokes the quoting characters from an expression
     * Possibibilies:
     *   `a`
     *   'a'
     *   "a"
     *   `a`.`b`
     *   `a.b`
     *   a.`b`
     *   `a`.b
     * It is also possible to have escaped quoting characters
     * within an expression part:
     *   `a``b` => a`b
     * And you can use whitespace between the parts:
     *   a  .  `b` => [a,b]
     */
    protected function revokeQuotation($sql)
    {
    }
    /**
     * This method removes parenthesis from start of the given string.
     * It removes also the associated closing parenthesis.
     */
    protected function removeParenthesisFromStart($token)
    {
    }
    protected function getVariableType($expression)
    {
    }
    protected function isCommaToken($token)
    {
    }
    protected function isWhitespaceToken($token)
    {
    }
    protected function isCommentToken($token)
    {
    }
    protected function isColumnReference($out)
    {
    }
    protected function isReserved($out)
    {
    }
    protected function isConstant($out)
    {
    }
    protected function isAggregateFunction($out)
    {
    }
    protected function isCustomFunction($out)
    {
    }
    protected function isFunction($out)
    {
    }
    protected function isExpression($out)
    {
    }
    protected function isBracketExpression($out)
    {
    }
    protected function isSubQuery($out)
    {
    }
    protected function isComment($out)
    {
    }
    public function processComment($expression)
    {
    }
    /**
     * translates an array of objects into an associative array
     */
    public function toArray($tokenList)
    {
    }
    protected function array_insert_after($array, $key, $entry)
    {
    }
}
