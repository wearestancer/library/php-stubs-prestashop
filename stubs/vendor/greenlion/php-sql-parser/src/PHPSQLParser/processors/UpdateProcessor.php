<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the UPDATE statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UpdateProcessor extends \PHPSQLParser\processors\FromProcessor
{
}
