<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the parentheses around the statement.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class BracketProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processTopLevel($sql)
    {
    }
    public function process($tokens)
    {
    }
}
