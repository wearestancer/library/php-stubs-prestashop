<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the DUPLICATE statements.
 *
 * @author arothe
 *
 */
class DuplicateProcessor extends \PHPSQLParser\processors\SetProcessor
{
    public function process($tokens, $isUpdate = false)
    {
    }
}
