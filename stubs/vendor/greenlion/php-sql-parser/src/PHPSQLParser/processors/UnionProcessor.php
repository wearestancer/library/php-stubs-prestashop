<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the UNION statements.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UnionProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processDefault($token)
    {
    }
    protected function processSQL($token)
    {
    }
    public static function isUnion($queries)
    {
    }
    /**
     * MySQL supports a special form of UNION:
     * (select ...)
     * union
     * (select ...)
     *
     * This function handles this query syntax. Only one such subquery
     * is supported in each UNION block. (select)(select)union(select) is not legal.
     * The extra queries will be silently ignored.
     */
    protected function processMySQLUnion($queries)
    {
    }
    /**
     * Moves the final union query into a separate output, so the remainder (such as ORDER BY) can
     * be processed separately.
     */
    protected function splitUnionRemainder($queries, $unionType, $outputArray)
    {
    }
    public function process($inputArray)
    {
    }
}
