<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the SELECT expressions.
 *
 * @author arothe
 *
 */
class SelectExpressionProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($unparsed)
    {
    }
    /**
     * This fuction processes each SELECT clause.
     * We determine what (if any) alias
     * is provided, and we set the type of expression.
     */
    public function process($expression)
    {
    }
}
