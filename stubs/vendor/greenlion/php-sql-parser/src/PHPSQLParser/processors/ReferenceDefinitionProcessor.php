<?php

namespace PHPSQLParser\processors;

/**
 *
 * This class processes the reference definition part of the CREATE TABLE statements.
 *
 * @author arothe
 */
class ReferenceDefinitionProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function buildReferenceDef($expr, $base_expr, $key)
    {
    }
    public function process($tokens)
    {
    }
}
