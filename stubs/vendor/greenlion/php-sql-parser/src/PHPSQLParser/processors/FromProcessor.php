<?php

namespace PHPSQLParser\processors;

/**
 * This class processes the FROM statement.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @author  Marco Th. <marco64th@gmail.com>
 * @author  George Schneeloch <noisecapella@gmail.com>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class FromProcessor extends \PHPSQLParser\processors\AbstractProcessor
{
    protected function processExpressionList($unparsed)
    {
    }
    protected function processColumnList($unparsed)
    {
    }
    protected function processSQLDefault($unparsed)
    {
    }
    protected function initParseInfo($parseInfo = false)
    {
    }
    protected function processFromExpression(&$parseInfo)
    {
    }
    public function process($tokens)
    {
    }
}
