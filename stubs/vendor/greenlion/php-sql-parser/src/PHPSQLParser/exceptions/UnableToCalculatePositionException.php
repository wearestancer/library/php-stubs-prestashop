<?php

namespace PHPSQLParser\exceptions;

/**
 * This exception will occur, if the PositionCalculator can not find the token
 * defined by a base_expr field within the original SQL statement. Please create
 * an issue in such a case, it is an application error.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UnableToCalculatePositionException extends \Exception
{
    protected $needle;
    protected $haystack;
    public function __construct($needle, $haystack)
    {
    }
    public function getNeedle()
    {
    }
    public function getHaystack()
    {
    }
}
