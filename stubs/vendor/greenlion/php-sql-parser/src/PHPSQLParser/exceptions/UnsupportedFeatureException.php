<?php

namespace PHPSQLParser\exceptions;

/**
 * This exception will occur in the PHPSQLCreator, if the creator finds
 * a field name, which is unknown. The developers have created some
 * additional output of the parser, but the creator class has not been
 * enhanced. Please open an issue in such a case.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UnsupportedFeatureException extends \Exception
{
    protected $key;
    public function __construct($key)
    {
    }
    public function getKey()
    {
    }
}
