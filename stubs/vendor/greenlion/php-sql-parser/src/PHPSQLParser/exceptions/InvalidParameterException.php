<?php

namespace PHPSQLParser\exceptions;

/**
 * This exception will occur in the parser, if the given SQL statement
 * is not a String type.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class InvalidParameterException extends \InvalidArgumentException
{
    protected $argument;
    public function __construct($argument)
    {
    }
    public function getArgument()
    {
    }
}
