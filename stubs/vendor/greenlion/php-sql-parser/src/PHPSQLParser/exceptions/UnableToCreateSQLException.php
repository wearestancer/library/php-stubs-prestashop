<?php

namespace PHPSQLParser\exceptions;

/**
 * This exception will occur within the PHPSQLCreator, if the creator can not find a
 * method, which can handle the current expr_type field. It could be an error within the parser
 * output or a special case has not been modelled within the creator. Please create an issue
 * in such a case.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class UnableToCreateSQLException extends \Exception
{
    protected $part;
    protected $partkey;
    protected $entry;
    protected $entrykey;
    public function __construct($part, $partkey, $entry, $entrykey)
    {
    }
    public function getEntry()
    {
    }
    public function getEntryKey()
    {
    }
    public function getSQLPart()
    {
    }
    public function getSQLPartKey()
    {
    }
}
