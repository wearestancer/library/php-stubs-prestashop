<?php

namespace PHPSQLParser\utils;

class ExpressionToken
{
    public function __construct($key = "", $token = "")
    {
    }
    # TODO: we could replace it with a constructor new ExpressionToken(this, "*")
    public function addToken($string)
    {
    }
    public function isEnclosedWithinParenthesis()
    {
    }
    public function setSubTree($tree)
    {
    }
    public function getSubTree()
    {
    }
    public function getUpper($idx = false)
    {
    }
    public function getTrim($idx = false)
    {
    }
    public function getToken($idx = false)
    {
    }
    public function setNoQuotes($token, $qchars, \PHPSQLParser\Options $options)
    {
    }
    public function setTokenType($type)
    {
    }
    public function endsWith($needle)
    {
    }
    public function isWhitespaceToken()
    {
    }
    public function isCommaToken()
    {
    }
    public function isVariableToken()
    {
    }
    public function isSubQueryToken()
    {
    }
    public function isExpression()
    {
    }
    public function isBracketExpression()
    {
    }
    public function isOperator()
    {
    }
    public function isInList()
    {
    }
    public function isFunction()
    {
    }
    public function isUnspecified()
    {
    }
    public function isVariable()
    {
    }
    public function isAggregateFunction()
    {
    }
    public function isCustomFunction()
    {
    }
    public function isColumnReference()
    {
    }
    public function isConstant()
    {
    }
    public function isSign()
    {
    }
    public function isSubQuery()
    {
    }
    public function toArray()
    {
    }
}
