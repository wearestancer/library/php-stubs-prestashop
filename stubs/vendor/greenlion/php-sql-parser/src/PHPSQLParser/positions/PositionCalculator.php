<?php

namespace PHPSQLParser\positions;

/**
 * This class implements the calculator for the string positions of the
 * base_expr elements within the output of the PHPSQLParser.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
class PositionCalculator
{
    protected static $allowedOnOperator = array("\t", "\n", "\r", " ", ",", "(", ")", "_", "'", "\"", "?", "@", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    protected static $allowedOnOther = array("\t", "\n", "\r", " ", ",", "(", ")", "<", ">", "*", "+", "-", "/", "|", "&", "=", "!", ";");
    protected $flippedBacktrackingTypes;
    protected static $backtrackingTypes = array(\PHPSQLParser\utils\ExpressionType::EXPRESSION, \PHPSQLParser\utils\ExpressionType::SUBQUERY, \PHPSQLParser\utils\ExpressionType::BRACKET_EXPRESSION, \PHPSQLParser\utils\ExpressionType::TABLE_EXPRESSION, \PHPSQLParser\utils\ExpressionType::RECORD, \PHPSQLParser\utils\ExpressionType::IN_LIST, \PHPSQLParser\utils\ExpressionType::MATCH_ARGUMENTS, \PHPSQLParser\utils\ExpressionType::TABLE, \PHPSQLParser\utils\ExpressionType::TEMPORARY_TABLE, \PHPSQLParser\utils\ExpressionType::COLUMN_TYPE, \PHPSQLParser\utils\ExpressionType::COLDEF, \PHPSQLParser\utils\ExpressionType::PRIMARY_KEY, \PHPSQLParser\utils\ExpressionType::CONSTRAINT, \PHPSQLParser\utils\ExpressionType::COLUMN_LIST, \PHPSQLParser\utils\ExpressionType::CHECK, \PHPSQLParser\utils\ExpressionType::COLLATE, \PHPSQLParser\utils\ExpressionType::LIKE, \PHPSQLParser\utils\ExpressionType::INDEX, \PHPSQLParser\utils\ExpressionType::INDEX_TYPE, \PHPSQLParser\utils\ExpressionType::INDEX_SIZE, \PHPSQLParser\utils\ExpressionType::INDEX_PARSER, \PHPSQLParser\utils\ExpressionType::FOREIGN_KEY, \PHPSQLParser\utils\ExpressionType::REFERENCE, \PHPSQLParser\utils\ExpressionType::PARTITION, \PHPSQLParser\utils\ExpressionType::PARTITION_HASH, \PHPSQLParser\utils\ExpressionType::PARTITION_COUNT, \PHPSQLParser\utils\ExpressionType::PARTITION_KEY, \PHPSQLParser\utils\ExpressionType::PARTITION_KEY_ALGORITHM, \PHPSQLParser\utils\ExpressionType::PARTITION_RANGE, \PHPSQLParser\utils\ExpressionType::PARTITION_LIST, \PHPSQLParser\utils\ExpressionType::PARTITION_DEF, \PHPSQLParser\utils\ExpressionType::PARTITION_VALUES, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_DEF, \PHPSQLParser\utils\ExpressionType::PARTITION_DATA_DIR, \PHPSQLParser\utils\ExpressionType::PARTITION_INDEX_DIR, \PHPSQLParser\utils\ExpressionType::PARTITION_COMMENT, \PHPSQLParser\utils\ExpressionType::PARTITION_MAX_ROWS, \PHPSQLParser\utils\ExpressionType::PARTITION_MIN_ROWS, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_COMMENT, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_DATA_DIR, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_INDEX_DIR, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_KEY, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_KEY_ALGORITHM, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_MAX_ROWS, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_MIN_ROWS, \PHPSQLParser\utils\ExpressionType::SUBPARTITION, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_HASH, \PHPSQLParser\utils\ExpressionType::SUBPARTITION_COUNT, \PHPSQLParser\utils\ExpressionType::CHARSET, \PHPSQLParser\utils\ExpressionType::ENGINE, \PHPSQLParser\utils\ExpressionType::QUERY, \PHPSQLParser\utils\ExpressionType::INDEX_ALGORITHM, \PHPSQLParser\utils\ExpressionType::INDEX_LOCK, \PHPSQLParser\utils\ExpressionType::SUBQUERY_FACTORING, \PHPSQLParser\utils\ExpressionType::CUSTOM_FUNCTION);
    /**
     * Constructor.
     *
     * It initializes some fields.
     */
    public function __construct()
    {
    }
    protected function printPos($text, $sql, $charPos, $key, $parsed, $backtracking)
    {
    }
    public function setPositionsWithinSQL($sql, $parsed)
    {
    }
    protected function findPositionWithinString($sql, $value, $expr_type)
    {
    }
    protected function lookForBaseExpression($sql, &$charPos, &$parsed, $key, &$backtracking)
    {
    }
}
