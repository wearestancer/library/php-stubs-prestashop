<?php

namespace PHPSQLParser;

/**
 * This class enhances the PHPSQLCreator to translate incoming
 * parser output into another SQL dialect (here: Oracle SQL).
 *
 * @author arothe
 *
 */
class OracleSQLTranslator extends \PHPSQLParser\PHPSQLCreator
{
    const ASTERISK_ALIAS = "[#RePl#]";
    public function __construct($con)
    {
    }
    public static function dbgprint($txt)
    {
    }
    public static function preprint($s, $return = false)
    {
    }
    protected function processAlias($parsed)
    {
    }
    protected function processDELETE($parsed)
    {
    }
    public static function getColumnNameFor($column)
    {
    }
    public static function getShortTableNameFor($table)
    {
    }
    protected function processTable($parsed, $index)
    {
    }
    protected function processFROM($parsed)
    {
    }
    protected function processTableExpression($parsed, $index)
    {
    }
    protected function isCLOBColumn($table, $column)
    {
    }
    protected function processOrderByExpression($parsed)
    {
    }
    protected function processColRef($parsed)
    {
    }
    protected function processFunctionOnSelect($parsed)
    {
    }
    protected function processSELECT($parsed)
    {
    }
    protected function processSelectStatement($parsed)
    {
    }
    public function create($parsed)
    {
    }
    public function process($sql)
    {
    }
}
