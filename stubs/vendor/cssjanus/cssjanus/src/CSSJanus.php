<?php

/**
 * PHP port of CSSJanus. https://github.com/cssjanus/php-cssjanus
 *
 * Copyright 2020 Timo Tijhof
 * Copyright 2014 Trevor Parscal
 * Copyright 2010 Roan Kattouw
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @file
 */
/**
 * CSSJanus is a a utility that converts CSS stylesheets
 * from left-to-right (LTR) to right-to-left (RTL).
 */
class CSSJanus
{
    /**
     * Transform an LTR stylesheet to RTL
     *
     * @param string $css Stylesheet to transform
     * @param bool|array{transformDirInUrl?:bool,transformEdgeInUrl?:bool} $options Options array,
     * or value of transformDirInUrl option (back-compat)
     *  - transformDirInUrl: Transform directions in URLs (ltr/rtl). Default: false.
     *  - transformEdgeInUrl: Transform edges in URLs (left/right). Default: false.
     * @param bool $transformEdgeInUrl [optional] For back-compat
     * @return string Transformed stylesheet
     */
    public static function transform($css, $options = array(), $transformEdgeInUrl = \false)
    {
    }
}
/**
 * Utility class used by CSSJanus that tokenizes and untokenizes things we want
 * to protect from being janused.
 */
class CSSJanusTokenizer
{
    /**
     * Constructor
     * @param string $regex Regular expression whose matches to replace by a token.
     * @param string $token Token
     */
    public function __construct($regex, $token)
    {
    }
    /**
     * Replace all occurrences of $regex in $str with a token and remember
     * the original strings.
     * @param string $str to tokenize
     * @return string Tokenized string
     */
    public function tokenize($str)
    {
    }
    /**
     * Replace tokens with their originals. If multiple strings were tokenized, it's important they be
     * detokenized in exactly the SAME ORDER.
     * @param string $str previously run through tokenize()
     * @return string Original string
     */
    public function detokenize($str)
    {
    }
}
