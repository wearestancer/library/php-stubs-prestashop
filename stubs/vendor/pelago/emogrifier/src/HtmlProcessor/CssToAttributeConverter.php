<?php

namespace Pelago\Emogrifier\HtmlProcessor;

/**
 * This HtmlProcessor can convert style HTML attributes to the corresponding other visual HTML attributes,
 * e.g. it converts style="width: 100px" to width="100".
 *
 * It will only add attributes, but leaves the style attribute untouched.
 *
 * To trigger the conversion, call the convertCssToVisualAttributes method.
 *
 * @author Oliver Klee <github@oliverklee.de>
 */
class CssToAttributeConverter extends \Pelago\Emogrifier\HtmlProcessor\AbstractHtmlProcessor
{
    /**
     * Maps the CSS from the style nodes to visual HTML attributes.
     *
     * @return self fluent interface
     */
    public function convertCssToVisualAttributes() : self
    {
    }
}
