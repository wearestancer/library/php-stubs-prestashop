<?php

namespace Pelago\Emogrifier;

/**
 * This class provides functions for converting CSS styles into inline style attributes in your HTML code.
 *
 * For Emogrifier 3.0.0, this will be the successor to the \Pelago\Emogrifier class (which then will be deprecated).
 *
 * For more information, please see the README.md file.
 *
 * @author Cameron Brooks
 * @author Jaime Prado
 * @author Oliver Klee <github@oliverklee.de>
 * @author Roman Ožana <ozana@omdesign.cz>
 * @author Sander Kruger <s.kruger@invessel.com>
 * @author Zoli Szabó <zoli.szabo+github@gmail.com>
 */
class CssInliner extends \Pelago\Emogrifier\HtmlProcessor\AbstractHtmlProcessor
{
    /**
     * Inlines the given CSS into the existing HTML.
     *
     * @param string $css the CSS to inline, must be UTF-8-encoded
     *
     * @return self fluent interface
     *
     * @throws ParseException in debug mode, if an invalid selector is encountered
     * @throws \RuntimeException in debug mode, if an internal PCRE error occurs
     */
    public function inlineCss(string $css = '') : self
    {
    }
    /**
     * Disables the parsing of inline styles.
     *
     * @return self fluent interface
     */
    public function disableInlineStyleAttributesParsing() : self
    {
    }
    /**
     * Disables the parsing of <style> blocks.
     *
     * @return self fluent interface
     */
    public function disableStyleBlocksParsing() : self
    {
    }
    /**
     * Marks a media query type to keep.
     *
     * @param string $mediaName the media type name, e.g., "braille"
     *
     * @return self fluent interface
     */
    public function addAllowedMediaType(string $mediaName) : self
    {
    }
    /**
     * Drops a media query type from the allowed list.
     *
     * @param string $mediaName the tag name, e.g., "braille"
     *
     * @return self fluent interface
     */
    public function removeAllowedMediaType(string $mediaName) : self
    {
    }
    /**
     * Adds a selector to exclude nodes from emogrification.
     *
     * Any nodes that match the selector will not have their style altered.
     *
     * @param string $selector the selector to exclude, e.g., ".editor"
     *
     * @return self fluent interface
     */
    public function addExcludedSelector(string $selector) : self
    {
    }
    /**
     * No longer excludes the nodes matching this selector from emogrification.
     *
     * @param string $selector the selector to no longer exclude, e.g., ".editor"
     *
     * @return self fluent interface
     */
    public function removeExcludedSelector(string $selector) : self
    {
    }
    /**
     * Sets the debug mode.
     *
     * @param bool $debug set to true to enable debug mode
     *
     * @return self fluent interface
     */
    public function setDebug(bool $debug) : self
    {
    }
    /**
     * Gets the array of selectors present in the CSS provided to `inlineCss()` for which the declarations could not be
     * applied as inline styles, but which may affect elements in the HTML.  The relevant CSS will have been placed in a
     * `<style>` element.  The selectors may include those used within `@media` rules or those involving dynamic
     * pseudo-classes (such as `:hover`) or pseudo-elements (such as `::after`).
     *
     * @return string[]
     *
     * @throws \BadMethodCallException if `inlineCss` has not been called first
     */
    public function getMatchingUninlinableSelectors() : array
    {
    }
    /**
     * Adds a style element with $css to $this->domDocument.
     *
     * This method is protected to allow overriding.
     *
     * @see https://github.com/MyIntervals/emogrifier/issues/103
     *
     * @param string $css
     */
    protected function addStyleElementToDocument(string $css) : void
    {
    }
}
