<?php

namespace GeoIp2\Model;

/**
 * @ignore
 */
abstract class AbstractModel implements \GeoIp2\Compat\JsonSerializable
{
    protected $raw;
    /**
     * @ignore
     */
    public function __construct($raw)
    {
    }
    /**
     * @ignore
     */
    protected function get($field)
    {
    }
    /**
     * @ignore
     */
    public function __get($attr)
    {
    }
    /**
     * @ignore
     */
    public function __isset($attr)
    {
    }
    public function jsonSerialize()
    {
    }
}
