<?php

namespace GeoIp2\Record;

abstract class AbstractRecord implements \GeoIp2\Compat\JsonSerializable
{
    /**
     * @ignore
     */
    public function __construct($record)
    {
    }
    /**
     * @ignore
     */
    public function __get($attr)
    {
    }
    public function __isset($attr)
    {
    }
    public function jsonSerialize()
    {
    }
}
