<?php

namespace GeoIp2\Record;

abstract class AbstractPlaceRecord extends \GeoIp2\Record\AbstractRecord
{
    /**
     * @ignore
     */
    public function __construct($record, $locales = array('en'))
    {
    }
    /**
     * @ignore
     */
    public function __get($attr)
    {
    }
    /**
     * @ignore
     */
    public function __isset($attr)
    {
    }
}
