<?php

namespace GeoIp2\Exception;

/**
 *  This class represents an HTTP transport error.
 */
class HttpException extends \GeoIp2\Exception\GeoIp2Exception
{
    /**
     * The URI queried
     */
    public $uri;
    public function __construct($message, $httpStatus, $uri, \Exception $previous = null)
    {
    }
}
