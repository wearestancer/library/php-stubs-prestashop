<?php

namespace GeoIp2\Exception;

/**
 * This class represents a generic error.
 */
class AddressNotFoundException extends \GeoIp2\Exception\GeoIp2Exception
{
}
