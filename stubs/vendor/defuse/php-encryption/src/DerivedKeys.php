<?php

namespace Defuse\Crypto;

/**
 * Class DerivedKeys
 * @package Defuse\Crypto
 */
final class DerivedKeys
{
    /**
     * Returns the authentication key.
     * @return string
     */
    public function getAuthenticationKey()
    {
    }
    /**
     * Returns the encryption key.
     * @return string
     */
    public function getEncryptionKey()
    {
    }
    /**
     * Constructor for DerivedKeys.
     *
     * @param string $akey
     * @param string $ekey
     */
    public function __construct($akey, $ekey)
    {
    }
}
