<?php

namespace Defuse\Crypto;

final class KeyProtectedByPassword
{
    const PASSWORD_KEY_CURRENT_VERSION = "\xde\xf1\x00\x00";
    /**
     * Creates a random key protected by the provided password.
     *
     * @param string $password
     *
     * @throws Ex\EnvironmentIsBrokenException
     *
     * @return KeyProtectedByPassword
     */
    public static function createRandomPasswordProtectedKey($password)
    {
    }
    /**
     * Loads a KeyProtectedByPassword from its encoded form.
     *
     * @param string $saved_key_string
     *
     * @throws Ex\BadFormatException
     *
     * @return KeyProtectedByPassword
     */
    public static function loadFromAsciiSafeString($saved_key_string)
    {
    }
    /**
     * Encodes the KeyProtectedByPassword into a string of printable ASCII
     * characters.
     *
     * @throws Ex\EnvironmentIsBrokenException
     *
     * @return string
     */
    public function saveToAsciiSafeString()
    {
    }
    /**
     * Decrypts the protected key, returning an unprotected Key object that can
     * be used for encryption and decryption.
     *
     * @throws Ex\EnvironmentIsBrokenException
     * @throws Ex\WrongKeyOrModifiedCiphertextException
     *
     * @param string $password
     * @return Key
     */
    public function unlockKey($password)
    {
    }
    /**
     * Changes the password.
     *
     * @param string $current_password
     * @param string $new_password
     *
     * @throws Ex\EnvironmentIsBrokenException
     * @throws Ex\WrongKeyOrModifiedCiphertextException
     *
     * @return KeyProtectedByPassword
     */
    public function changePassword($current_password, $new_password)
    {
    }
}
