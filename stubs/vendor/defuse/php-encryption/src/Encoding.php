<?php

namespace Defuse\Crypto;

final class Encoding
{
    const CHECKSUM_BYTE_SIZE = 32;
    const CHECKSUM_HASH_ALGO = 'sha256';
    const SERIALIZE_HEADER_BYTES = 4;
    /**
     * Converts a byte string to a hexadecimal string without leaking
     * information through side channels.
     *
     * @param string $byte_string
     *
     * @throws Ex\EnvironmentIsBrokenException
     *
     * @return string
     */
    public static function binToHex($byte_string)
    {
    }
    /**
     * Converts a hexadecimal string into a byte string without leaking
     * information through side channels.
     *
     * @param string $hex_string
     *
     * @throws Ex\BadFormatException
     * @throws Ex\EnvironmentIsBrokenException
     *
     * @return string
     * @psalm-suppress TypeDoesNotContainType
     */
    public static function hexToBin($hex_string)
    {
    }
    /**
     * Remove trialing whitespace without table look-ups or branches.
     *
     * Calling this function may leak the length of the string as well as the
     * number of trailing whitespace characters through side-channels.
     *
     * @param string $string
     * @return string
     */
    public static function trimTrailingWhitespace($string = '')
    {
    }
    /*
     * SECURITY NOTE ON APPLYING CHECKSUMS TO SECRETS:
     *
     *      The checksum introduces a potential security weakness. For example,
     *      suppose we apply a checksum to a key, and that an adversary has an
     *      exploit against the process containing the key, such that they can
     *      overwrite an arbitrary byte of memory and then cause the checksum to
     *      be verified and learn the result.
     *
     *      In this scenario, the adversary can extract the key one byte at
     *      a time by overwriting it with their guess of its value and then
     *      asking if the checksum matches. If it does, their guess was right.
     *      This kind of attack may be more easy to implement and more reliable
     *      than a remote code execution attack.
     *
     *      This attack also applies to authenticated encryption as a whole, in
     *      the situation where the adversary can overwrite a byte of the key
     *      and then cause a valid ciphertext to be decrypted, and then
     *      determine whether the MAC check passed or failed.
     *
     *      By using the full SHA256 hash instead of truncating it, I'm ensuring
     *      that both ways of going about the attack are equivalently difficult.
     *      A shorter checksum of say 32 bits might be more useful to the
     *      adversary as an oracle in case their writes are coarser grained.
     *
     *      Because the scenario assumes a serious vulnerability, we don't try
     *      to prevent attacks of this style.
     */
    /**
     * INTERNAL USE ONLY: Applies a version header, applies a checksum, and
     * then encodes a byte string into a range of printable ASCII characters.
     *
     * @param string $header
     * @param string $bytes
     *
     * @throws Ex\EnvironmentIsBrokenException
     *
     * @return string
     */
    public static function saveBytesToChecksummedAsciiSafeString($header, $bytes)
    {
    }
    /**
     * INTERNAL USE ONLY: Decodes, verifies the header and checksum, and returns
     * the encoded byte string.
     *
     * @param string $expected_header
     * @param string $string
     *
     * @throws Ex\EnvironmentIsBrokenException
     * @throws Ex\BadFormatException
     *
     * @return string
     */
    public static function loadBytesFromChecksummedAsciiSafeString($expected_header, $string)
    {
    }
}
