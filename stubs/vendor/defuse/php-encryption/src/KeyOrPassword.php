<?php

namespace Defuse\Crypto;

final class KeyOrPassword
{
    const PBKDF2_ITERATIONS = 100000;
    const SECRET_TYPE_KEY = 1;
    const SECRET_TYPE_PASSWORD = 2;
    /**
     * Initializes an instance of KeyOrPassword from a key.
     *
     * @param Key $key
     *
     * @return KeyOrPassword
     */
    public static function createFromKey(\Defuse\Crypto\Key $key)
    {
    }
    /**
     * Initializes an instance of KeyOrPassword from a password.
     *
     * @param string $password
     *
     * @return KeyOrPassword
     */
    public static function createFromPassword($password)
    {
    }
    /**
     * Derives authentication and encryption keys from the secret, using a slow
     * key derivation function if the secret is a password.
     *
     * @param string $salt
     *
     * @throws Ex\CryptoException
     * @throws Ex\EnvironmentIsBrokenException
     *
     * @return DerivedKeys
     */
    public function deriveKeys($salt)
    {
    }
}
