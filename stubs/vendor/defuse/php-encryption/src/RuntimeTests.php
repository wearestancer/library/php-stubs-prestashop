<?php

namespace Defuse\Crypto;

/*
 * We're using static class inheritance to get access to protected methods
 * inside Crypto. To make it easy to know where the method we're calling can be
 * found, within this file, prefix calls with `Crypto::` or `RuntimeTests::`,
 * and don't use `self::`.
 */
class RuntimeTests extends \Defuse\Crypto\Crypto
{
    /**
     * Runs the runtime tests.
     *
     * @throws Ex\EnvironmentIsBrokenException
     * @return void
     */
    public static function runtimeTest()
    {
    }
}
