<?php

namespace FOS\JsRoutingBundle\DependencyInjection;

/**
 * FOSJsRoutingExtension
 * Load configuration.
 *
 * @author      William DURAND <william.durand1@gmail.com>
 */
class FOSJsRoutingExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension
{
    /**
     * Load configuration.
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
