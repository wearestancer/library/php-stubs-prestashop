<?php

namespace FOS\JsRoutingBundle;

/**
 * FOSJsRoutingBundle class.
 *
 * @author      William DURAND <william.durand1@gmail.com>
 */
class FOSJsRoutingBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
}
