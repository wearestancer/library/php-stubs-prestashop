<?php

namespace FOS\JsRoutingBundle\Serializer\Denormalizer;

class RouteCollectionDenormalizer implements \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
{
    /**
     * {@inheritDoc}
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsDenormalization($data, $type, $format = null) : bool
    {
    }
}
