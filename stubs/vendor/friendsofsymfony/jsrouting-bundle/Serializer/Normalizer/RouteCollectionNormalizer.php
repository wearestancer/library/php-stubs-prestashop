<?php

namespace FOS\JsRoutingBundle\Serializer\Normalizer;

/**
 * Class RouteCollectionNormalizer
 */
class RouteCollectionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    /**
     * {@inheritDoc}
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($data, $format = null, array $context = array())
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsNormalization($data, $format = null) : bool
    {
    }
}
