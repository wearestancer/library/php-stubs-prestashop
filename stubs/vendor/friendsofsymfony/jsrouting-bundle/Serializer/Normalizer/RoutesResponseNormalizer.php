<?php

namespace FOS\JsRoutingBundle\Serializer\Normalizer;

/**
 * Class RoutesResponseNormalizer
 */
class RoutesResponseNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    /**
     * {@inheritdoc}
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($data, $format = null, array $context = array())
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsNormalization($data, $format = null) : bool
    {
    }
}
