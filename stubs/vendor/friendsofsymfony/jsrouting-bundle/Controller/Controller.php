<?php

namespace FOS\JsRoutingBundle\Controller;

/**
 * Controller class.
 *
 * @author William DURAND <william.durand1@gmail.com>
 */
class Controller
{
    /**
     * @var mixed
     */
    protected $serializer;
    /**
     * @var ExposedRoutesExtractorInterface
     */
    protected $exposedRoutesExtractor;
    /**
     * @var CacheControlConfig
     */
    protected $cacheControlConfig;
    /**
     * @var boolean
     */
    protected $debug;
    /**
     * Default constructor.
     *
     * @param object                          $serializer             Any object with a serialize($data, $format) method
     * @param ExposedRoutesExtractorInterface $exposedRoutesExtractor The extractor service.
     * @param array                           $cacheControl
     * @param boolean                         $debug
     */
    public function __construct($serializer, \FOS\JsRoutingBundle\Extractor\ExposedRoutesExtractorInterface $exposedRoutesExtractor, array $cacheControl = array(), $debug = false)
    {
    }
    /**
     * indexAction action.
     */
    public function indexAction(\Symfony\Component\HttpFoundation\Request $request, $_format)
    {
    }
}
