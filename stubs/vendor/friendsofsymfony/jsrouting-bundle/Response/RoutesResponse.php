<?php

namespace FOS\JsRoutingBundle\Response;

class RoutesResponse
{
    public function __construct($baseUrl, \Symfony\Component\Routing\RouteCollection $routes = null, $prefix = null, $host = null, $port = null, $scheme = null, $locale = null, $domains = array())
    {
    }
    public function getBaseUrl()
    {
    }
    public function getRoutes()
    {
    }
    public function getPrefix()
    {
    }
    public function getHost()
    {
    }
    public function getPort()
    {
    }
    public function getScheme()
    {
    }
    public function getLocale()
    {
    }
}
