<?php

namespace FOS\JsRoutingBundle\Extractor;

/**
 * @author      William DURAND <william.durand1@gmail.com>
 */
class ExposedRoutesExtractor implements \FOS\JsRoutingBundle\Extractor\ExposedRoutesExtractorInterface
{
    /**
     * @var RouterInterface
     */
    protected $router;
    /**
     * Base cache directory
     *
     * @var string
     */
    protected $cacheDir;
    /**
     * @var array
     */
    protected $bundles;
    /**
     * @var string
     */
    protected $pattern;
    /**
     * @var array
     */
    protected $availableDomains;
    /**
     * Default constructor.
     *
     * @param RouterInterface $router The router.
     * @param array $routesToExpose Some route names to expose.
     * @param string $cacheDir
     * @param array $bundles list of loaded bundles to check when generating the prefix
     *
     * @throws \Exception
     */
    public function __construct(\Symfony\Component\Routing\RouterInterface $router, array $routesToExpose, $cacheDir, $bundles = array())
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getRoutes()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBaseUrl()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPrefix($locale)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getHost()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPort()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getScheme()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCachePath($locale)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getResources()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isRouteExposed(\Symfony\Component\Routing\Route $route, $name)
    {
    }
    protected function getDomainByRouteMatches($matches, $name)
    {
    }
    protected function extractDomainPatterns($routesToExpose)
    {
    }
    /**
     * Convert the routesToExpose array in a regular expression pattern
     *
     * @param $domainPatterns
     * @return string
     * @throws \Exception
     */
    protected function buildPattern($domainPatterns)
    {
    }
}
