<?php

namespace FOS\JsRoutingBundle\Command;

/**
 * A console command for retrieving information about exposed routes.
 *
 * @author      William DURAND <william.durand1@gmail.com>
 */
class RouterDebugExposedCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'fos:js-routing:debug';
    public function __construct(\FOS\JsRoutingBundle\Extractor\ExposedRoutesExtractorInterface $extractor, \Symfony\Component\Routing\RouterInterface $router)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * @see Command
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
    protected function getRoutes($domain = array())
    {
    }
}
