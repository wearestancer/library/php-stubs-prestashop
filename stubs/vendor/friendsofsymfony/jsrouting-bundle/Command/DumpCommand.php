<?php

namespace FOS\JsRoutingBundle\Command;

/**
 * Dumps routes to the filesystem.
 *
 * @author Benjamin Dulau <benjamin.dulau@anonymation.com>
 */
class DumpCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'fos:js-routing:dump';
    public function __construct(\FOS\JsRoutingBundle\Extractor\ExposedRoutesExtractorInterface $extractor, \Symfony\Component\Serializer\SerializerInterface $serializer, $projectDir, $requestContextBaseUrl = null)
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
