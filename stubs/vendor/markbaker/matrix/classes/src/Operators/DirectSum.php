<?php

namespace Matrix\Operators;

class DirectSum extends \Matrix\Operators\Operator
{
    /**
     * Execute the addition
     *
     * @param mixed $value The matrix or numeric value to add to the current base value
     * @return $this The operation object, allowing multiple additions to be chained
     * @throws Exception If the provided argument is not appropriate for the operation
     */
    public function execute($value) : \Matrix\Operators\Operator
    {
    }
}
