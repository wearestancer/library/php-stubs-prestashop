<?php

namespace Matrix\Operators;

class Addition extends \Matrix\Operators\Operator
{
    /**
     * Execute the addition
     *
     * @param mixed $value The matrix or numeric value to add to the current base value
     * @throws Exception If the provided argument is not appropriate for the operation
     * @return $this The operation object, allowing multiple additions to be chained
     **/
    public function execute($value) : \Matrix\Operators\Operator
    {
    }
    /**
     * Execute the addition for a scalar
     *
     * @param mixed $value The numeric value to add to the current base value
     * @return $this The operation object, allowing multiple additions to be chained
     **/
    protected function addScalar($value) : \Matrix\Operators\Operator
    {
    }
    /**
     * Execute the addition for a matrix
     *
     * @param Matrix $value The numeric value to add to the current base value
     * @return $this The operation object, allowing multiple additions to be chained
     * @throws Exception If the provided argument is not appropriate for the operation
     **/
    protected function addMatrix(\Matrix\Matrix $value) : \Matrix\Operators\Operator
    {
    }
}
