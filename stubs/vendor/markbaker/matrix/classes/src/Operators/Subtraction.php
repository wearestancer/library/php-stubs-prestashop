<?php

namespace Matrix\Operators;

class Subtraction extends \Matrix\Operators\Operator
{
    /**
     * Execute the subtraction
     *
     * @param mixed $value The matrix or numeric value to subtract from the current base value
     * @throws Exception If the provided argument is not appropriate for the operation
     * @return $this The operation object, allowing multiple subtractions to be chained
     **/
    public function execute($value) : \Matrix\Operators\Operator
    {
    }
    /**
     * Execute the subtraction for a scalar
     *
     * @param mixed $value The numeric value to subtracted from the current base value
     * @return $this The operation object, allowing multiple additions to be chained
     **/
    protected function subtractScalar($value) : \Matrix\Operators\Operator
    {
    }
    /**
     * Execute the subtraction for a matrix
     *
     * @param Matrix $value The numeric value to subtract from the current base value
     * @return $this The operation object, allowing multiple subtractions to be chained
     * @throws Exception If the provided argument is not appropriate for the operation
     **/
    protected function subtractMatrix(\Matrix\Matrix $value) : \Matrix\Operators\Operator
    {
    }
}
