<?php

namespace Matrix\Operators;

class Multiplication extends \Matrix\Operators\Operator
{
    /**
     * Execute the multiplication
     *
     * @param mixed $value The matrix or numeric value to multiply the current base value by
     * @throws Exception If the provided argument is not appropriate for the operation
     * @return $this The operation object, allowing multiple multiplications to be chained
     **/
    public function execute($value, string $type = 'multiplication') : \Matrix\Operators\Operator
    {
    }
    /**
     * Execute the multiplication for a scalar
     *
     * @param mixed $value The numeric value to multiply with the current base value
     * @return $this The operation object, allowing multiple mutiplications to be chained
     **/
    protected function multiplyScalar($value, string $type = 'multiplication') : \Matrix\Operators\Operator
    {
    }
    /**
     * Execute the multiplication for a matrix
     *
     * @param Matrix $value The numeric value to multiply with the current base value
     * @return $this The operation object, allowing multiple mutiplications to be chained
     * @throws Exception If the provided argument is not appropriate for the operation
     **/
    protected function multiplyMatrix(\Matrix\Matrix $value, string $type = 'multiplication') : \Matrix\Operators\Operator
    {
    }
}
