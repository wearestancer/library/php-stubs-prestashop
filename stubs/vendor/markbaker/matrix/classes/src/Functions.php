<?php

namespace Matrix;

class Functions
{
    /**
     * Return the adjoint of this matrix
     * The adjugate, classical adjoint, or adjunct of a square matrix is the transpose of its cofactor matrix.
     * The adjugate has sometimes been called the "adjoint", but today the "adjoint" of a matrix normally refers
     *     to its corresponding adjoint operator, which is its conjugate transpose.
     *
     * @param Matrix|array $matrix The matrix whose adjoint we wish to calculate
     * @return Matrix
     * @throws Exception
     **/
    public static function adjoint($matrix)
    {
    }
    /**
     * Return the cofactors of this matrix
     *
     * @param Matrix|array $matrix The matrix whose cofactors we wish to calculate
     * @return Matrix
     *
     * @throws Exception
     */
    public static function cofactors($matrix)
    {
    }
    /**
     * Return the determinant of this matrix
     *
     * @param Matrix|array $matrix The matrix whose determinant we wish to calculate
     * @return float
     * @throws Exception
     **/
    public static function determinant($matrix)
    {
    }
    /**
     * Return the diagonal of this matrix
     *
     * @param Matrix|array $matrix The matrix whose diagonal we wish to calculate
     * @return Matrix
     * @throws Exception
     **/
    public static function diagonal($matrix)
    {
    }
    /**
     * Return the antidiagonal of this matrix
     *
     * @param Matrix|array $matrix The matrix whose antidiagonal we wish to calculate
     * @return Matrix
     * @throws Exception
     **/
    public static function antidiagonal($matrix)
    {
    }
    /**
     * Return the identity matrix
     * The identity matrix, or sometimes ambiguously called a unit matrix, of size n is the n × n square matrix
     *   with ones on the main diagonal and zeros elsewhere
     *
     * @param Matrix|array $matrix The matrix whose identity we wish to calculate
     * @return Matrix
     * @throws Exception
     **/
    public static function identity($matrix)
    {
    }
    /**
     * Return the inverse of this matrix
     *
     * @param Matrix|array $matrix The matrix whose inverse we wish to calculate
     * @return Matrix
     * @throws Exception
     **/
    public static function inverse($matrix, string $type = 'inverse')
    {
    }
    /**
     * Calculate the minors of the matrix
     *
     * @param Matrix $matrix The matrix whose minors we wish to calculate
     * @return array[]
     *
     * @throws Exception
     */
    protected static function getMinors(\Matrix\Matrix $matrix)
    {
    }
    /**
     * Return the minors of the matrix
     * The minor of a matrix A is the determinant of some smaller square matrix, cut down from A by removing one or
     *     more of its rows or columns.
     * Minors obtained by removing just one row and one column from square matrices (first minors) are required for
     *     calculating matrix cofactors, which in turn are useful for computing both the determinant and inverse of
     *     square matrices.
     *
     * @param Matrix|array $matrix The matrix whose minors we wish to calculate
     * @return Matrix
     * @throws Exception
     **/
    public static function minors($matrix)
    {
    }
    /**
     * Return the trace of this matrix
     * The trace is defined as the sum of the elements on the main diagonal (the diagonal from the upper left to the lower right)
     *     of the matrix
     *
     * @param Matrix|array $matrix The matrix whose trace we wish to calculate
     * @return float
     * @throws Exception
     **/
    public static function trace($matrix)
    {
    }
    /**
     * Return the transpose of this matrix
     *
     * @param Matrix|\a $matrix The matrix whose transpose we wish to calculate
     * @return Matrix
     **/
    public static function transpose($matrix)
    {
    }
}
