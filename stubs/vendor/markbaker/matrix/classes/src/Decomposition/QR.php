<?php

namespace Matrix\Decomposition;

class QR
{
    public function __construct(\Matrix\Matrix $matrix)
    {
    }
    public function getHouseholdVectors() : \Matrix\Matrix
    {
    }
    public function getQ() : \Matrix\Matrix
    {
    }
    public function getR() : \Matrix\Matrix
    {
    }
    public function isFullRank() : bool
    {
    }
    /**
     * Least squares solution of A*X = B.
     *
     * @param Matrix $B a Matrix with as many rows as A and any number of columns
     *
     * @throws Exception
     *
     * @return Matrix matrix that minimizes the two norm of Q*R*X-B
     */
    public function solve(\Matrix\Matrix $B) : \Matrix\Matrix
    {
    }
}
