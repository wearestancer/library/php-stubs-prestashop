<?php

namespace Matrix\Decomposition;

class LU
{
    public function __construct(\Matrix\Matrix $matrix)
    {
    }
    /**
     * Get lower triangular factor.
     *
     * @return Matrix Lower triangular factor
     */
    public function getL() : \Matrix\Matrix
    {
    }
    /**
     * Get upper triangular factor.
     *
     * @return Matrix Upper triangular factor
     */
    public function getU() : \Matrix\Matrix
    {
    }
    /**
     * Return pivot permutation vector.
     *
     * @return Matrix Pivot matrix
     */
    public function getP() : \Matrix\Matrix
    {
    }
    /**
     * Return pivot permutation vector.
     *
     * @return array Pivot vector
     */
    public function getPivot() : array
    {
    }
    /**
     *    Is the matrix nonsingular?
     *
     * @return bool true if U, and hence A, is nonsingular
     */
    public function isNonsingular() : bool
    {
    }
    /**
     * Solve A*X = B.
     *
     * @param Matrix $B a Matrix with as many rows as A and any number of columns
     *
     * @throws Exception
     *
     * @return Matrix X so that L*U*X = B(piv,:)
     */
    public function solve(\Matrix\Matrix $B) : \Matrix\Matrix
    {
    }
}
