<?php

namespace Complex;

class Functions
{
    /**
     * Returns the absolute value (modulus) of a complex number.
     * Also known as the rho of the complex number, i.e. the distance/radius
     *   from the centrepoint to the representation of the number in polar coordinates.
     *
     * This function is a synonym for rho()
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    float            The absolute (or rho) value of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     *
     * @see    rho
     *
     */
    public static function abs($complex) : float
    {
    }
    /**
     * Returns the inverse cosine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse cosine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function acos($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse hyperbolic cosine of a complex number.
     *
     * Formula from Wolfram Alpha:
     *   cosh^(-1)z = ln(z + sqrt(z + 1) sqrt(z - 1)).
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse hyperbolic cosine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function acosh($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse cotangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse cotangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function acot($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse hyperbolic cotangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse hyperbolic cotangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function acoth($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse cosecant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse cosecant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function acsc($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse hyperbolic cosecant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse hyperbolic cosecant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function acsch($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the argument of a complex number.
     * Also known as the theta of the complex number, i.e. the angle in radians
     *   from the real axis to the representation of the number in polar coordinates.
     *
     * This function is a synonym for theta()
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    float            The argument (or theta) value of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     *
     * @see    theta
     */
    public static function argument($complex) : float
    {
    }
    /**
     * Returns the inverse secant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse secant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function asec($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse hyperbolic secant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse hyperbolic secant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function asech($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse sine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse sine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function asin($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse hyperbolic sine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse hyperbolic sine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function asinh($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse tangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse tangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function atan($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse hyperbolic tangent of a complex number.
     *
     * Formula from Wolfram Alpha:
     *  tanh^(-1)z = 1/2 [ln(1 + z) - ln(1 - z)].
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse hyperbolic tangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function atanh($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the complex conjugate of a complex number
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The conjugate of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function conjugate($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the cosine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The cosine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function cos($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the hyperbolic cosine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The hyperbolic cosine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function cosh($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the cotangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The cotangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function cot($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the hyperbolic cotangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The hyperbolic cotangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function coth($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the cosecant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The cosecant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function csc($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the hyperbolic cosecant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The hyperbolic cosecant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function csch($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the exponential of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The exponential of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function exp($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the inverse of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The inverse of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    InvalidArgumentException    If function would result in a division by zero
     */
    public static function inverse($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the natural logarithm of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The natural logarithm of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    InvalidArgumentException  If the real and the imaginary parts are both zero
     */
    public static function ln($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the base-2 logarithm of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The base-2 logarithm of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    InvalidArgumentException  If the real and the imaginary parts are both zero
     */
    public static function log2($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the common logarithm (base 10) of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The common logarithm (base 10) of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    InvalidArgumentException  If the real and the imaginary parts are both zero
     */
    public static function log10($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the negative of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The negative value of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     *
     * @see    rho
     *
     */
    public static function negative($complex) : \Complex\Complex
    {
    }
    /**
     * Returns a complex number raised to a power.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @param     float|integer    $power      The power to raise this value to
     * @return    Complex          The complex argument raised to the real power.
     * @throws    Exception        If the power argument isn't a valid real
     */
    public static function pow($complex, $power) : \Complex\Complex
    {
    }
    /**
     * Returns the rho of a complex number.
     * This is the distance/radius from the centrepoint to the representation of the number in polar coordinates.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    float            The rho value of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function rho($complex) : float
    {
    }
    /**
     * Returns the secant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The secant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function sec($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the hyperbolic secant of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The hyperbolic secant of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function sech($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the sine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The sine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function sin($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the hyperbolic sine of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The hyperbolic sine of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function sinh($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the square root of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The Square root of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function sqrt($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the tangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The tangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    InvalidArgumentException    If function would result in a division by zero
     */
    public static function tan($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the hyperbolic tangent of a complex number.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    Complex          The hyperbolic tangent of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     * @throws    \InvalidArgumentException    If function would result in a division by zero
     */
    public static function tanh($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the theta of a complex number.
     *   This is the angle in radians from the real axis to the representation of the number in polar coordinates.
     *
     * @param     Complex|mixed    $complex    Complex number or a numeric value.
     * @return    float            The theta value of the complex argument.
     * @throws    Exception        If argument isn't a valid real or complex number.
     */
    public static function theta($complex) : float
    {
    }
}
