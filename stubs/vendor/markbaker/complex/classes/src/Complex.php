<?php

namespace Complex;

/**
 * Complex Number object.
 *
 * @package Complex
 *
 * @method float abs()
 * @method Complex acos()
 * @method Complex acosh()
 * @method Complex acot()
 * @method Complex acoth()
 * @method Complex acsc()
 * @method Complex acsch()
 * @method float argument()
 * @method Complex asec()
 * @method Complex asech()
 * @method Complex asin()
 * @method Complex asinh()
 * @method Complex atan()
 * @method Complex atanh()
 * @method Complex conjugate()
 * @method Complex cos()
 * @method Complex cosh()
 * @method Complex cot()
 * @method Complex coth()
 * @method Complex csc()
 * @method Complex csch()
 * @method Complex exp()
 * @method Complex inverse()
 * @method Complex ln()
 * @method Complex log2()
 * @method Complex log10()
 * @method Complex negative()
 * @method Complex pow(int|float $power)
 * @method float rho()
 * @method Complex sec()
 * @method Complex sech()
 * @method Complex sin()
 * @method Complex sinh()
 * @method Complex sqrt()
 * @method Complex tan()
 * @method Complex tanh()
 * @method float theta()
 * @method Complex add(...$complexValues)
 * @method Complex subtract(...$complexValues)
 * @method Complex multiply(...$complexValues)
 * @method Complex divideby(...$complexValues)
 * @method Complex divideinto(...$complexValues)
 */
class Complex
{
    /**
     * @constant    Euler's Number.
     */
    const EULER = 2.718281828459045;
    /**
     * @constant    Regexp to split an input string into real and imaginary components and suffix
     */
    const NUMBER_SPLIT_REGEXP = '` ^
            (                                   # Real part
                [-+]?(\\d+\\.?\\d*|\\d*\\.?\\d+)          # Real value (integer or float)
                ([Ee][-+]?[0-2]?\\d{1,3})?           # Optional real exponent for scientific format
            )
            (                                   # Imaginary part
                [-+]?(\\d+\\.?\\d*|\\d*\\.?\\d+)          # Imaginary value (integer or float)
                ([Ee][-+]?[0-2]?\\d{1,3})?           # Optional imaginary exponent for scientific format
            )?
            (                                   # Imaginary part is optional
                ([-+]?)                             # Imaginary (implicit 1 or -1) only
                ([ij]?)                             # Imaginary i or j - depending on whether mathematical or engineering
            )
        $`uix';
    /**
     * @var    float    $realPart    The value of of this complex number on the real plane.
     */
    protected $realPart = 0.0;
    /**
     * @var    float    $imaginaryPart    The value of of this complex number on the imaginary plane.
     */
    protected $imaginaryPart = 0.0;
    /**
     * @var    string    $suffix    The suffix for this complex number (i or j).
     */
    protected $suffix;
    public function __construct($realPart = 0.0, $imaginaryPart = null, $suffix = 'i')
    {
    }
    /**
     * Gets the real part of this complex number
     *
     * @return Float
     */
    public function getReal() : float
    {
    }
    /**
     * Gets the imaginary part of this complex number
     *
     * @return Float
     */
    public function getImaginary() : float
    {
    }
    /**
     * Gets the suffix of this complex number
     *
     * @return String
     */
    public function getSuffix() : string
    {
    }
    /**
     * Returns true if this is a real value, false if a complex value
     *
     * @return Bool
     */
    public function isReal() : bool
    {
    }
    /**
     * Returns true if this is a complex value, false if a real value
     *
     * @return Bool
     */
    public function isComplex() : bool
    {
    }
    public function format() : string
    {
    }
    public function __toString() : string
    {
    }
    /**
     * Validates whether the argument is a valid complex number, converting scalar or array values if possible
     *
     * @param     mixed    $complex   The value to validate
     * @return    Complex
     * @throws    Exception    If the argument isn't a Complex number or cannot be converted to one
     */
    public static function validateComplexArgument($complex) : \Complex\Complex
    {
    }
    /**
     * Returns the reverse of this complex number
     *
     * @return    Complex
     */
    public function reverse() : \Complex\Complex
    {
    }
    public function invertImaginary() : \Complex\Complex
    {
    }
    public function invertReal() : \Complex\Complex
    {
    }
    protected static $functions = ['abs', 'acos', 'acosh', 'acot', 'acoth', 'acsc', 'acsch', 'argument', 'asec', 'asech', 'asin', 'asinh', 'atan', 'atanh', 'conjugate', 'cos', 'cosh', 'cot', 'coth', 'csc', 'csch', 'exp', 'inverse', 'ln', 'log2', 'log10', 'negative', 'pow', 'rho', 'sec', 'sech', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'theta'];
    protected static $operations = ['add', 'subtract', 'multiply', 'divideby', 'divideinto'];
    /**
     * Returns the result of the function call or operation
     *
     * @return    Complex|float
     * @throws    Exception|\InvalidArgumentException
     */
    public function __call($functionName, $arguments)
    {
    }
}
