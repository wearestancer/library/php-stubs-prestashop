<?php

namespace Complex;

class Operations
{
    /**
     * Adds two or more complex numbers
     *
     * @param     array of string|integer|float|Complex    $complexValues   The numbers to add
     * @return    Complex
     */
    public static function add(...$complexValues) : \Complex\Complex
    {
    }
    /**
     * Divides two or more complex numbers
     *
     * @param     array of string|integer|float|Complex    $complexValues   The numbers to divide
     * @return    Complex
     */
    public static function divideby(...$complexValues) : \Complex\Complex
    {
    }
    /**
     * Divides two or more complex numbers
     *
     * @param     array of string|integer|float|Complex    $complexValues   The numbers to divide
     * @return    Complex
     */
    public static function divideinto(...$complexValues) : \Complex\Complex
    {
    }
    /**
     * Multiplies two or more complex numbers
     *
     * @param     array of string|integer|float|Complex    $complexValues   The numbers to multiply
     * @return    Complex
     */
    public static function multiply(...$complexValues) : \Complex\Complex
    {
    }
    /**
     * Subtracts two or more complex numbers
     *
     * @param     array of string|integer|float|Complex    $complexValues   The numbers to subtract
     * @return    Complex
     */
    public static function subtract(...$complexValues) : \Complex\Complex
    {
    }
}
