<?php

namespace MaxMind\Exception;

/**
 * Thrown when a MaxMind web service returns an error relating to the request.
 */
class InvalidRequestException extends \MaxMind\Exception\HttpException
{
    /**
     * @param string     $message    the exception message
     * @param string     $error      the error code returned by the MaxMind web service
     * @param int        $httpStatus the HTTP status code of the response
     * @param string     $uri        the URI queries
     * @param \Exception $previous   the previous exception, if any
     */
    public function __construct(string $message, string $error, int $httpStatus, string $uri, \Exception $previous = null)
    {
    }
    public function getErrorCode() : string
    {
    }
}
