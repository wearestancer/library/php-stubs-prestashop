<?php

namespace MaxMind\Exception;

/**
 * This class represents an error authenticating.
 */
class AuthenticationException extends \MaxMind\Exception\InvalidRequestException
{
}
