<?php

namespace MaxMind\Exception;

/**
 *  This class represents an HTTP transport error.
 */
class HttpException extends \MaxMind\Exception\WebServiceException
{
    /**
     * @param string     $message    a message describing the error
     * @param int        $httpStatus the HTTP status code of the response
     * @param string     $uri        the URI used in the request
     * @param \Exception $previous   the previous exception, if any
     */
    public function __construct(string $message, int $httpStatus, string $uri, \Exception $previous = null)
    {
    }
    public function getUri() : string
    {
    }
    public function getStatusCode() : int
    {
    }
}
