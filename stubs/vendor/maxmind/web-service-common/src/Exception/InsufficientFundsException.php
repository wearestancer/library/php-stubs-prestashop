<?php

namespace MaxMind\Exception;

/**
 * Thrown when the account is out of credits.
 */
class InsufficientFundsException extends \MaxMind\Exception\InvalidRequestException
{
}
