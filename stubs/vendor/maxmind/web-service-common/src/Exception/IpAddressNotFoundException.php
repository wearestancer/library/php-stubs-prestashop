<?php

namespace MaxMind\Exception;

class IpAddressNotFoundException extends \MaxMind\Exception\InvalidRequestException
{
}
