<?php

namespace MaxMind\WebService\Http;

/**
 * Class RequestFactory.
 *
 * @internal
 */
class RequestFactory
{
    public function __destruct()
    {
    }
    public function request(string $url, array $options) : \MaxMind\WebService\Http\Request
    {
    }
}
