<?php

namespace MaxMind\WebService\Http;

/**
 * This class is for internal use only. Semantic versioning does not not apply.
 *
 * @internal
 */
class CurlRequest implements \MaxMind\WebService\Http\Request
{
    public function __construct(string $url, array $options)
    {
    }
    /**
     * @throws HttpException
     */
    public function post(string $body) : array
    {
    }
    public function get() : array
    {
    }
}
