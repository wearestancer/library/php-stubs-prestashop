<?php

namespace MaxMind\WebService;

/**
 * This class is not intended to be used directly by an end-user of a
 * MaxMind web service. Please use the appropriate client API for the service
 * that you are using.
 *
 * @internal
 */
class Client
{
    public const VERSION = '0.2.0';
    /**
     * @param int    $accountId  your MaxMind account ID
     * @param string $licenseKey your MaxMind license key
     * @param array  $options    an array of options. Possible keys:
     *                           * `host` - The host to use when connecting to the web service.
     *                           * `useHttps` - A boolean flag for sending the request via https.(True by default)
     *                           * `userAgent` - The prefix of the User-Agent to use in the request.
     *                           * `caBundle` - The bundle of CA root certificates to use in the request.
     *                           * `connectTimeout` - The connect timeout to use for the request.
     *                           * `timeout` - The timeout to use for the request.
     *                           * `proxy` - The HTTP proxy to use. May include a schema, port,
     *                           username, and password, e.g., `http://username:password@127.0.0.1:10`.
     */
    public function __construct(int $accountId, string $licenseKey, array $options = [])
    {
    }
    /**
     * @param string $service name of the service querying
     * @param string $path    the URI path to use
     * @param array  $input   the data to be posted as JSON
     *
     * @throws InvalidInputException      when the request has missing or invalid
     *                                    data
     * @throws AuthenticationException    when there is an issue authenticating the
     *                                    request
     * @throws InsufficientFundsException when your account is out of funds
     * @throws InvalidRequestException    when the request is invalid for some
     *                                    other reason, e.g., invalid JSON in the POST.
     * @throws HttpException              when an unexpected HTTP error occurs
     * @throws WebServiceException        when some other error occurs. This also
     *                                    serves as the base class for the above exceptions.
     *
     * @return array|null The decoded content of a successful response
     */
    public function post(string $service, string $path, array $input) : ?array
    {
    }
    public function get(string $service, string $path) : ?array
    {
    }
}
