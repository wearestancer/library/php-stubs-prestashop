<?php

namespace Lcobucci\Clock;

interface Clock
{
    /** @return DateTimeImmutable */
    public function now();
}
final class FrozenClock implements \Lcobucci\Clock\Clock
{
    public function __construct(\DateTimeImmutable $now)
    {
    }
    /** @return self */
    public static function fromUTC()
    {
    }
    public function setTo(\DateTimeImmutable $now)
    {
    }
    public function now()
    {
    }
}
final class SystemClock implements \Lcobucci\Clock\Clock
{
    public function __construct(\DateTimeZone $timezone)
    {
    }
    /** @return self */
    public static function fromUTC()
    {
    }
    /** @return self */
    public static function fromSystemTimezone()
    {
    }
    public function now()
    {
    }
}
