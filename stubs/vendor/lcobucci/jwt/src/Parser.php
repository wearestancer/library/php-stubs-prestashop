<?php

namespace Lcobucci\JWT;

/**
 * This class parses the JWT strings and convert them into tokens
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
class Parser
{
    /**
     * Initializes the object
     *
     * @param Decoder $decoder
     */
    public function __construct(\Lcobucci\JWT\Parsing\Decoder $decoder = null)
    {
    }
    /**
     * Parses the JWT and returns a token
     *
     * @param string $jwt
     *
     * @return Token
     *
     * @throws InvalidArgumentException  When JWT is not a string or is invalid.
     * @throws RuntimeException          When something goes wrong while decoding
     */
    public function parse($jwt)
    {
    }
    /**
     * Splits the JWT string into an array
     *
     * @param string $jwt
     *
     * @return array
     *
     * @throws InvalidArgumentException When JWT is not a string or is invalid
     */
    protected function splitJwt($jwt)
    {
    }
    /**
     * Parses the header from a string
     *
     * @param string $data
     *
     * @return array
     *
     * @throws UnsupportedHeaderFound When an invalid header is informed
     */
    protected function parseHeader($data)
    {
    }
    /**
     * Parses the claim set from a string
     *
     * @param string $data
     *
     * @return array
     */
    protected function parseClaims($data)
    {
    }
    /**
     * Returns the signature from given data
     *
     * @param array $header
     * @param string $data
     *
     * @return Signature
     */
    protected function parseSignature(array $header, $data)
    {
    }
}
