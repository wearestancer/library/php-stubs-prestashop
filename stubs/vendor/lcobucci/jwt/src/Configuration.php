<?php

namespace Lcobucci\JWT;

/**
 * Configuration container for the JWT Builder and Parser
 *
 * Serves like a small DI container to simplify the creation and usage
 * of the objects.
 */
final class Configuration
{
    /** @return self */
    public static function forAsymmetricSigner(\Lcobucci\JWT\Signer $signer, \Lcobucci\JWT\Signer\Key $signingKey, \Lcobucci\JWT\Signer\Key $verificationKey, \Lcobucci\JWT\Parsing\Encoder $encoder = null, \Lcobucci\JWT\Parsing\Decoder $decoder = null)
    {
    }
    /** @return self */
    public static function forSymmetricSigner(\Lcobucci\JWT\Signer $signer, \Lcobucci\JWT\Signer\Key $key, \Lcobucci\JWT\Parsing\Encoder $encoder = null, \Lcobucci\JWT\Parsing\Decoder $decoder = null)
    {
    }
    /** @return self */
    public static function forUnsecuredSigner(\Lcobucci\JWT\Parsing\Encoder $encoder = null, \Lcobucci\JWT\Parsing\Decoder $decoder = null)
    {
    }
    /** @param callable(): Builder $builderFactory */
    public function setBuilderFactory(callable $builderFactory)
    {
    }
    /** @return Builder */
    public function builder()
    {
    }
    /** @return Parser */
    public function parser()
    {
    }
    public function setParser(\Lcobucci\JWT\Parser $parser)
    {
    }
    /** @return Signer */
    public function signer()
    {
    }
    /** @return Key */
    public function signingKey()
    {
    }
    /** @return Key */
    public function verificationKey()
    {
    }
    /** @return Validator */
    public function validator()
    {
    }
    public function setValidator(\Lcobucci\JWT\Validator $validator)
    {
    }
    /** @return Constraint[] */
    public function validationConstraints()
    {
    }
    public function setValidationConstraints(\Lcobucci\JWT\Validation\Constraint ...$validationConstraints)
    {
    }
}
