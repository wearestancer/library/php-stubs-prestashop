<?php

namespace Lcobucci\JWT;

/**
 * Basic interface for token claims
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.0.0
 */
interface Claim extends \JsonSerializable
{
    /**
     * Returns the claim name
     *
     * @return string
     */
    public function getName();
    /**
     * Returns the claim value
     *
     * @return mixed
     */
    public function getValue();
    /**
     * Returns the string representation of the claim
     *
     * @return string
     */
    public function __toString();
}
