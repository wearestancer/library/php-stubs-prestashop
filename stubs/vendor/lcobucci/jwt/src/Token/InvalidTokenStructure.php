<?php

namespace Lcobucci\JWT\Token;

final class InvalidTokenStructure extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /** @return self */
    public static function missingOrNotEnoughSeparators()
    {
    }
    /**
     * @param string $part
     *
     * @return self
     */
    public static function arrayExpected($part)
    {
    }
    /**
     * @param string $value
     *
     * @return self
     */
    public static function dateIsNotParseable($value)
    {
    }
}
