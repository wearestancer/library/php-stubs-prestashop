<?php

namespace Lcobucci\JWT\Token;

final class UnsupportedHeaderFound extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /** @return self */
    public static function encryption()
    {
    }
}
