<?php

namespace Lcobucci\JWT\Token;

final class RegisteredClaimGiven extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    const DEFAULT_MESSAGE = 'Builder#withClaim() is meant to be used for non-registered claims, ' . 'check the documentation on how to set claim "%s"';
    /**
     * @param string $name
     *
     * @return self
     */
    public static function forClaim($name)
    {
    }
}
