<?php

namespace Lcobucci\JWT\Token;

final class DataSet
{
    /**
     * @param array<string, mixed> $data
     * @param string               $encoded
     */
    public function __construct(array $data, $encoded)
    {
    }
    /**
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    public function get($name, $default = null)
    {
    }
    /**
     * @param string $name
     *
     * @return bool
     */
    public function has($name)
    {
    }
    /** @return array<string, mixed> */
    public function all()
    {
    }
    /** @return string */
    public function toString()
    {
    }
}
