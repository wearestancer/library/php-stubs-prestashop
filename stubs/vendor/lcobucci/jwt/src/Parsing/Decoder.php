<?php

namespace Lcobucci\JWT\Parsing;

/**
 * Class that decodes data according with the specs of RFC-4648
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 *
 * @link http://tools.ietf.org/html/rfc4648#section-5
 */
class Decoder
{
    /**
     * Decodes from JSON, validating the errors (will return an associative array
     * instead of objects)
     *
     * @param string $json
     * @return mixed
     *
     * @throws RuntimeException When something goes wrong while decoding
     */
    public function jsonDecode($json)
    {
    }
    /**
     * Decodes from base64url
     *
     * @param string $data
     * @return string
     */
    public function base64UrlDecode($data)
    {
    }
}
