<?php

namespace Lcobucci\JWT;

/**
 * Class that wraps validation values
 *
 * @deprecated This component has been removed from the interface in v4.0
 * @see \Lcobucci\JWT\Validation\Validator
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.0.0
 */
class ValidationData
{
    /**
     * Initializes the object
     *
     * @param int $currentTime
     * @param int $leeway
     */
    public function __construct($currentTime = null, $leeway = 0)
    {
    }
    /**
     * Configures the id
     *
     * @param string $id
     */
    public function setId($id)
    {
    }
    /**
     * Configures the issuer
     *
     * @param string $issuer
     */
    public function setIssuer($issuer)
    {
    }
    /**
     * Configures the audience
     *
     * @param string $audience
     */
    public function setAudience($audience)
    {
    }
    /**
     * Configures the subject
     *
     * @param string $subject
     */
    public function setSubject($subject)
    {
    }
    /**
     * Configures the time that "iat", "nbf" and "exp" should be based on
     *
     * @param int $currentTime
     */
    public function setCurrentTime($currentTime)
    {
    }
    /**
     * Returns the requested item
     *
     * @param string $name
     *
     * @return mixed
     */
    public function get($name)
    {
    }
    /**
     * Returns if the item is present
     *
     * @param string $name
     *
     * @return boolean
     */
    public function has($name)
    {
    }
}
