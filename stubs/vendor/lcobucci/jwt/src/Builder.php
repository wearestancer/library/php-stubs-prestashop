<?php

namespace Lcobucci\JWT;

/**
 * This class makes easier the token creation process
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
class Builder
{
    /**
     * Initializes a new builder
     *
     * @param Encoder $encoder
     * @param ClaimFactory $claimFactory
     */
    public function __construct(\Lcobucci\JWT\Parsing\Encoder $encoder = null, \Lcobucci\JWT\Claim\Factory $claimFactory = null)
    {
    }
    /**
     * Configures the audience
     *
     * @deprecated This method has been wrongly added and doesn't exist on v4
     * @see Builder::permittedFor()
     *
     * @param string $audience
     * @param bool $replicateAsHeader
     *
     * @return Builder
     */
    public function canOnlyBeUsedBy($audience, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the audience
     *
     * @param list<string|bool> $audiences A list of audiences and, optionally, the instruction to replicate as header
     *
     * @return Builder
     */
    public function permittedFor(...$audiences)
    {
    }
    /**
     * Configures the audience
     *
     * @deprecated This method will be removed on v4
     * @see Builder::permittedFor()
     *
     * @param string $audience
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setAudience($audience, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the expiration time
     *
     * @param int|DateTimeImmutable $expiration
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function expiresAt($expiration, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the expiration time
     *
     * @deprecated This method will be removed on v4
     * @see Builder::expiresAt()
     *
     * @param int|DateTimeImmutable $expiration
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setExpiration($expiration, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the token id
     *
     * @param string $id
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function identifiedBy($id, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the token id
     *
     * @deprecated This method will be removed on v4
     * @see Builder::identifiedBy()
     *
     * @param string $id
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setId($id, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the time that the token was issued
     *
     * @param int|DateTimeImmutable $issuedAt
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function issuedAt($issuedAt, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the time that the token was issued
     *
     * @deprecated This method will be removed on v4
     * @see Builder::issuedAt()
     *
     * @param int|DateTimeImmutable $issuedAt
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setIssuedAt($issuedAt, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the issuer
     *
     * @param string $issuer
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function issuedBy($issuer, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the issuer
     *
     * @deprecated This method will be removed on v4
     * @see Builder::issuedBy()
     *
     * @param string $issuer
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setIssuer($issuer, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the time before which the token cannot be accepted
     *
     * @param int|DateTimeImmutable $notBefore
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function canOnlyBeUsedAfter($notBefore, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the time before which the token cannot be accepted
     *
     * @deprecated This method will be removed on v4
     * @see Builder::canOnlyBeUsedAfter()
     *
     * @param int|DateTimeImmutable $notBefore
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setNotBefore($notBefore, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the subject
     *
     * @param string $subject
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function relatedTo($subject, $replicateAsHeader = false)
    {
    }
    /**
     * Configures the subject
     *
     * @deprecated This method will be removed on v4
     * @see Builder::relatedTo()
     *
     * @param string $subject
     * @param boolean $replicateAsHeader
     *
     * @return Builder
     */
    public function setSubject($subject, $replicateAsHeader = false)
    {
    }
    /**
     * Configures a registered claim
     *
     * @param string $name
     * @param mixed $value
     * @param boolean $replicate
     *
     * @return Builder
     */
    protected function setRegisteredClaim($name, $value, $replicate)
    {
    }
    /**
     * Configures a header item
     *
     * @param string $name
     * @param mixed $value
     *
     * @return Builder
     */
    public function withHeader($name, $value)
    {
    }
    /**
     * Configures a header item
     *
     * @deprecated This method will be removed on v4
     * @see Builder::withHeader()
     *
     * @param string $name
     * @param mixed $value
     *
     * @return Builder
     */
    public function setHeader($name, $value)
    {
    }
    /**
     * Configures a claim item
     *
     * @deprecated This method has been wrongly added and doesn't exist on v4
     * @see Builder::withClaim()
     *
     * @param string $name
     * @param mixed $value
     *
     * @return Builder
     */
    public function with($name, $value)
    {
    }
    /**
     * Configures a claim item
     *
     * @param string $name
     * @param mixed $value
     *
     * @return Builder
     *
     * @throws RegisteredClaimGiven
     */
    public function withClaim($name, $value)
    {
    }
    /**
     * Configures a claim item
     *
     * @deprecated This method will be removed on v4
     * @see Builder::withClaim()
     *
     * @param string $name
     * @param mixed $value
     *
     * @return Builder
     */
    public function set($name, $value)
    {
    }
    /**
     * Signs the data
     *
     * @deprecated This method will be removed on v4
     * @see Builder::getToken()
     *
     * @param Signer $signer
     * @param Key|string $key
     *
     * @return Builder
     */
    public function sign(\Lcobucci\JWT\Signer $signer, $key)
    {
    }
    /**
     * Removes the signature from the builder
     *
     * @deprecated This method will be removed on v4
     * @see Builder::getToken()
     *
     * @return Builder
     */
    public function unsign()
    {
    }
    /**
     * Returns the resultant token
     *
     * @return Token
     */
    public function getToken(\Lcobucci\JWT\Signer $signer = null, \Lcobucci\JWT\Signer\Key $key = null)
    {
    }
}
