<?php

namespace Lcobucci\JWT;

/**
 * Basic structure of the JWT
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
class Token
{
    /**
     * Initializes the object
     *
     * @param array|DataSet $headers
     * @param array|DataSet $claims
     * @param Signature|null $signature
     * @param array $payload
     * @param Factory|null $claimFactory
     */
    public function __construct($headers = ['alg' => 'none'], $claims = [], \Lcobucci\JWT\Signature $signature = null, array $payload = ['', ''], \Lcobucci\JWT\Claim\Factory $claimFactory = null)
    {
    }
    /** @return DataSet */
    public function headers()
    {
    }
    /**
     * Returns the token headers
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::headers()
     *
     * @return array
     */
    public function getHeaders()
    {
    }
    /**
     * Returns if the header is configured
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::headers()
     * @see DataSet::has()
     *
     * @param string $name
     *
     * @return boolean
     */
    public function hasHeader($name)
    {
    }
    /**
     * Returns the value of a token header
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::headers()
     * @see DataSet::has()
     *
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     *
     * @throws OutOfBoundsException
     */
    public function getHeader($name, $default = null)
    {
    }
    /** @return DataSet */
    public function claims()
    {
    }
    /**
     * Returns the token claim set
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::claims()
     *
     * @return array
     */
    public function getClaims()
    {
    }
    /**
     * Returns if the claim is configured
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::claims()
     * @see DataSet::has()
     *
     * @param string $name
     *
     * @return boolean
     */
    public function hasClaim($name)
    {
    }
    /**
     * Returns the value of a token claim
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::claims()
     * @see DataSet::get()
     *
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     *
     * @throws OutOfBoundsException
     */
    public function getClaim($name, $default = null)
    {
    }
    /**
     * Verify if the key matches with the one that created the signature
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see \Lcobucci\JWT\Validation\Validator
     *
     * @param Signer $signer
     * @param Key|string $key
     *
     * @return boolean
     */
    public function verify(\Lcobucci\JWT\Signer $signer, $key)
    {
    }
    /**
     * Validates if the token is valid
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see \Lcobucci\JWT\Validation\Validator
     *
     * @param ValidationData $data
     *
     * @return boolean
     */
    public function validate(\Lcobucci\JWT\ValidationData $data)
    {
    }
    /**
     * Determine if the token is expired.
     *
     * @param DateTimeInterface|null $now Defaults to the current time.
     *
     * @return bool
     */
    public function isExpired(\DateTimeInterface $now = null)
    {
    }
    /**
     * @param string $audience
     *
     * @return bool
     */
    public function isPermittedFor($audience)
    {
    }
    /**
     * @param string $id
     *
     * @return bool
     */
    public function isIdentifiedBy($id)
    {
    }
    /**
     * @param string $subject
     *
     * @return bool
     */
    public function isRelatedTo($subject)
    {
    }
    /**
     * @param list<string> $issuers
     *
     * @return bool
     */
    public function hasBeenIssuedBy(...$issuers)
    {
    }
    /**
     * @param DateTimeInterface $now
     *
     * @return bool
     */
    public function hasBeenIssuedBefore(\DateTimeInterface $now)
    {
    }
    /**
     * @param DateTimeInterface $now
     *
     * @return bool
     */
    public function isMinimumTimeBefore(\DateTimeInterface $now)
    {
    }
    /**
     * Returns the token payload
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::payload()
     *
     * @return string
     */
    public function getPayload()
    {
    }
    /**
     * Returns the token payload
     *
     * @return string
     */
    public function payload()
    {
    }
    /** @return Signature */
    public function signature()
    {
    }
    /**
     * Returns an encoded representation of the token
     *
     * @deprecated This method has been removed from the interface in v4.0
     * @see Token::toString()
     *
     * @return string
     */
    public function __toString()
    {
    }
    /** @return string */
    public function toString()
    {
    }
}
