<?php

namespace Lcobucci\JWT;

interface Validator
{
    /**
     * @throws RequiredConstraintsViolated
     * @throws NoConstraintsGiven
     */
    public function assert(\Lcobucci\JWT\Token $token, \Lcobucci\JWT\Validation\Constraint ...$constraints);
    /**
     * @return bool
     *
     * @throws NoConstraintsGiven
     */
    public function validate(\Lcobucci\JWT\Token $token, \Lcobucci\JWT\Validation\Constraint ...$constraints);
}
