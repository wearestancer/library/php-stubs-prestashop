<?php

namespace Lcobucci\JWT\Signer;

/**
 * Base class for signers
 *
 * @deprecated This class will be removed on v4
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
abstract class BaseSigner implements \Lcobucci\JWT\Signer
{
    /**
     * {@inheritdoc}
     */
    public function modifyHeader(array &$headers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function sign($payload, $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function verify($expected, $payload, $key)
    {
    }
    /**
     * Creates a hash with the given data
     *
     * @internal
     *
     * @param string $payload
     * @param Key $key
     *
     * @return string
     */
    public abstract function createHash($payload, \Lcobucci\JWT\Signer\Key $key);
    /**
     * Performs the signature verification
     *
     * @internal
     *
     * @param string $expected
     * @param string $payload
     * @param Key $key
     *
     * @return boolean
     */
    public abstract function doVerify($expected, $payload, \Lcobucci\JWT\Signer\Key $key);
}
