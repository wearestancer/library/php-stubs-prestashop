<?php

namespace Lcobucci\JWT\Signer;

final class CannotSignPayload extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /**
     * @pararm string $error
     *
     * @return self
     */
    public static function errorHappened($error)
    {
    }
}
