<?php

namespace Lcobucci\JWT\Signer\Key;

/** @deprecated Use \Lcobucci\JWT\Signer\Key\InMemory::file() instead */
final class LocalFileReference extends \Lcobucci\JWT\Signer\Key
{
    const PATH_PREFIX = 'file://';
    /**
     * @param string $path
     * @param string $passphrase
     *
     * @return self
     *
     * @throws FileCouldNotBeRead
     */
    public static function file($path, $passphrase = '')
    {
    }
}
