<?php

namespace Lcobucci\JWT\Signer\Key;

final class FileCouldNotBeRead extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /** @return self */
    public static function onPath(string $path, \Throwable $cause = null)
    {
    }
}
