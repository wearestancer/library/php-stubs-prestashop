<?php

namespace Lcobucci\JWT\Signer;

final class InvalidKeyProvided extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /**
     * @param string $details
     *
     * @return self
     */
    public static function cannotBeParsed($details)
    {
    }
    /** @return self */
    public static function incompatibleKey()
    {
    }
}
