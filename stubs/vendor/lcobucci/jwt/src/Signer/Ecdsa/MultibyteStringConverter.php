<?php

namespace Lcobucci\JWT\Signer\Ecdsa;

/**
 * ECDSA signature converter using ext-mbstring
 *
 * @internal
 */
final class MultibyteStringConverter implements \Lcobucci\JWT\Signer\Ecdsa\SignatureConverter
{
    const ASN1_SEQUENCE = '30';
    const ASN1_INTEGER = '02';
    const ASN1_MAX_SINGLE_BYTE = 128;
    const ASN1_LENGTH_2BYTES = '81';
    const ASN1_BIG_INTEGER_LIMIT = '7f';
    const ASN1_NEGATIVE_INTEGER = '00';
    const BYTE_SIZE = 2;
    public function toAsn1($signature, $length)
    {
    }
    public function fromAsn1($signature, $length)
    {
    }
}
