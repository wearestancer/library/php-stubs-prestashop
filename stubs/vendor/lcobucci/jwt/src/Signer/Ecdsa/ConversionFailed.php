<?php

namespace Lcobucci\JWT\Signer\Ecdsa;

final class ConversionFailed extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /** @return self */
    public static function invalidLength()
    {
    }
    /** @return self */
    public static function incorrectStartSequence()
    {
    }
    /** @return self */
    public static function integerExpected()
    {
    }
}
