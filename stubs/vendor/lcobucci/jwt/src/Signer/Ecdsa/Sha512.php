<?php

namespace Lcobucci\JWT\Signer\Ecdsa;

/**
 * Signer for ECDSA SHA-512
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.1.0
 */
class Sha512 extends \Lcobucci\JWT\Signer\Ecdsa
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlgorithm()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getKeyLength()
    {
    }
}
