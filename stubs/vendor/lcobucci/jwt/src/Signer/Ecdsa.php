<?php

namespace Lcobucci\JWT\Signer;

/**
 * Base class for ECDSA signers
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.1.0
 */
abstract class Ecdsa extends \Lcobucci\JWT\Signer\OpenSSL
{
    public function __construct(\Lcobucci\JWT\Signer\Ecdsa\SignatureConverter $converter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createHash($payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function doVerify($expected, $payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    /**
     * Returns the length of each point in the signature, so that we can calculate and verify R and S points properly
     *
     * @internal
     */
    public abstract function getKeyLength();
    /**
     * {@inheritdoc}
     */
    public final function getKeyType()
    {
    }
}
