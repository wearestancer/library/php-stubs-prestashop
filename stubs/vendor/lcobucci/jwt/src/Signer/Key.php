<?php

namespace Lcobucci\JWT\Signer;

/**
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 3.0.4
 */
class Key
{
    /**
     * @var string
     */
    protected $content;
    /**
     * @param string $content
     * @param string $passphrase
     */
    public function __construct($content, $passphrase = '')
    {
    }
    /** @return string */
    public function contents()
    {
    }
    /** @return string */
    public function passphrase()
    {
    }
    /**
     * @deprecated This method is no longer part of the public interface
     * @see Key::contents()
     *
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @deprecated This method is no longer part of the public interface
     * @see Key::passphrase()
     *
     * @return string
     */
    public function getPassphrase()
    {
    }
}
