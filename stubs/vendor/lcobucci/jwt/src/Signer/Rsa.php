<?php

namespace Lcobucci\JWT\Signer;

/**
 * Base class for RSASSA-PKCS1 signers
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.1.0
 */
abstract class Rsa extends \Lcobucci\JWT\Signer\OpenSSL
{
    public final function getKeyType()
    {
    }
}
