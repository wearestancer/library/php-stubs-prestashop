<?php

namespace Lcobucci\JWT\Signer;

abstract class OpenSSL extends \Lcobucci\JWT\Signer\BaseSigner
{
    public function createHash($payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    /**
     * @param $expected
     * @param $payload
     * @param $key
     * @return bool
     */
    public function doVerify($expected, $payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    /**
     * Returns the type of key to be used to create/verify the signature (using OpenSSL constants)
     *
     * @internal
     */
    public abstract function getKeyType();
    /**
     * Returns which algorithm to be used to create/verify the signature (using OpenSSL constants)
     *
     * @internal
     */
    public abstract function getAlgorithm();
}
