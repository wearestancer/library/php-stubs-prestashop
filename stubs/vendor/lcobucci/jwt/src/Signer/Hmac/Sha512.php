<?php

namespace Lcobucci\JWT\Signer\Hmac;

/**
 * Signer for HMAC SHA-512
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
class Sha512 extends \Lcobucci\JWT\Signer\Hmac
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlgorithm()
    {
    }
}
