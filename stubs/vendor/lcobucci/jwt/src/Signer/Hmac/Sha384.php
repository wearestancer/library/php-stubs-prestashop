<?php

namespace Lcobucci\JWT\Signer\Hmac;

/**
 * Signer for HMAC SHA-384
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
class Sha384 extends \Lcobucci\JWT\Signer\Hmac
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlgorithm()
    {
    }
}
