<?php

namespace Lcobucci\JWT\Signer\Rsa;

/**
 * Signer for RSA SHA-512
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.1.0
 */
class Sha512 extends \Lcobucci\JWT\Signer\Rsa
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlgorithm()
    {
    }
}
