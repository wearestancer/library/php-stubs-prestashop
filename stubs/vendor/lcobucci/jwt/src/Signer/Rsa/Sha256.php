<?php

namespace Lcobucci\JWT\Signer\Rsa;

/**
 * Signer for RSA SHA-256
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.1.0
 */
class Sha256 extends \Lcobucci\JWT\Signer\Rsa
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlgorithm()
    {
    }
}
