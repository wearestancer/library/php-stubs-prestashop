<?php

namespace Lcobucci\JWT\Signer\Rsa;

/**
 * Signer for RSA SHA-384
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.1.0
 */
class Sha384 extends \Lcobucci\JWT\Signer\Rsa
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlgorithm()
    {
    }
}
