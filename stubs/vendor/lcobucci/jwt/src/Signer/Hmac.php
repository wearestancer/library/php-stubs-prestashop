<?php

namespace Lcobucci\JWT\Signer;

/**
 * Base class for hmac signers
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
abstract class Hmac extends \Lcobucci\JWT\Signer\BaseSigner
{
    /**
     * {@inheritdoc}
     */
    public function createHash($payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function doVerify($expected, $payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    /**
     * Returns the algorithm name
     *
     * @internal
     *
     * @return string
     */
    public abstract function getAlgorithm();
}
