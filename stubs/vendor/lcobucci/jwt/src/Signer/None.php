<?php

namespace Lcobucci\JWT\Signer;

final class None extends \Lcobucci\JWT\Signer\BaseSigner
{
    public function getAlgorithmId()
    {
    }
    public function createHash($payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    public function doVerify($expected, $payload, \Lcobucci\JWT\Signer\Key $key)
    {
    }
}
