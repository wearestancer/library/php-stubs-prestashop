<?php

namespace Lcobucci\JWT\Claim;

/**
 * Basic interface for validatable token claims
 *
 * @deprecated This interface will be removed on v4
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.0.0
 */
interface Validatable
{
    /**
     * Returns if claim is valid according with given data
     *
     * @param ValidationData $data
     *
     * @return boolean
     */
    public function validate(\Lcobucci\JWT\ValidationData $data);
}
