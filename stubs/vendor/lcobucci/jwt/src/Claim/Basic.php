<?php

namespace Lcobucci\JWT\Claim;

/**
 * The default claim
 *
 * @deprecated This class will be removed on v4
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.0.0
 */
class Basic implements \Lcobucci\JWT\Claim
{
    /**
     * Initializes the claim
     *
     * @param string $name
     * @param mixed $value
     */
    public function __construct($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
    }
}
