<?php

namespace Lcobucci\JWT\Claim;

/**
 * Validatable claim that checks if value is greater or equals the given data
 *
 * @deprecated This class will be removed on v4
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 2.0.0
 */
class GreaterOrEqualsTo extends \Lcobucci\JWT\Claim\Basic implements \Lcobucci\JWT\Claim, \Lcobucci\JWT\Claim\Validatable
{
    /**
     * {@inheritdoc}
     */
    public function validate(\Lcobucci\JWT\ValidationData $data)
    {
    }
}
