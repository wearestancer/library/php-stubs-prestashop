<?php

namespace Lcobucci\JWT\Validation\Constraint;

final class LeewayCannotBeNegative extends \InvalidArgumentException implements \Lcobucci\JWT\Exception
{
    /** @return self */
    public static function create()
    {
    }
}
