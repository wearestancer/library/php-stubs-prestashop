<?php

namespace Lcobucci\JWT\Validation\Constraint;

final class IssuedBy implements \Lcobucci\JWT\Validation\Constraint
{
    /** @param list<string> $issuers */
    public function __construct(...$issuers)
    {
    }
    public function assert(\Lcobucci\JWT\Token $token)
    {
    }
}
