<?php

namespace Lcobucci\JWT\Validation\Constraint;

final class SignedWith implements \Lcobucci\JWT\Validation\Constraint
{
    public function __construct(\Lcobucci\JWT\Signer $signer, \Lcobucci\JWT\Signer\Key $key)
    {
    }
    public function assert(\Lcobucci\JWT\Token $token)
    {
    }
}
