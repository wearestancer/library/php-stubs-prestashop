<?php

namespace Lcobucci\JWT\Validation\Constraint;

final class ValidAt implements \Lcobucci\JWT\Validation\Constraint
{
    public function __construct(\Lcobucci\Clock\Clock $clock, \DateInterval $leeway = null)
    {
    }
    public function assert(\Lcobucci\JWT\Token $token)
    {
    }
}
