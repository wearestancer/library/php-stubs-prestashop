<?php

namespace Lcobucci\JWT\Validation\Constraint;

final class IdentifiedBy implements \Lcobucci\JWT\Validation\Constraint
{
    /** @param string $id */
    public function __construct($id)
    {
    }
    public function assert(\Lcobucci\JWT\Token $token)
    {
    }
}
