<?php

namespace Lcobucci\JWT\Validation;

final class ConstraintViolation extends \RuntimeException implements \Lcobucci\JWT\Exception
{
}
