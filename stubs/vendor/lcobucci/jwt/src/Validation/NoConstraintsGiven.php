<?php

namespace Lcobucci\JWT\Validation;

final class NoConstraintsGiven extends \RuntimeException implements \Lcobucci\JWT\Exception
{
}
