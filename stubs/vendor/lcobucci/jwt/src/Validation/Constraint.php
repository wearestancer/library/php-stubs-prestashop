<?php

namespace Lcobucci\JWT\Validation;

interface Constraint
{
    /** @throws ConstraintViolation */
    public function assert(\Lcobucci\JWT\Token $token);
}
