<?php

namespace Lcobucci\JWT\Validation;

final class RequiredConstraintsViolated extends \RuntimeException implements \Lcobucci\JWT\Exception
{
    /**
     * @param ConstraintViolation ...$violations
     * @return self
     */
    public static function fromViolations(\Lcobucci\JWT\Validation\ConstraintViolation ...$violations)
    {
    }
    /** @return ConstraintViolation[] */
    public function violations()
    {
    }
}
