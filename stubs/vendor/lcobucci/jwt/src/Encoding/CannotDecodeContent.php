<?php

namespace Lcobucci\JWT\Encoding;

final class CannotDecodeContent extends \RuntimeException implements \Lcobucci\JWT\Exception
{
    /**
     * @param JsonException $previous
     *
     * @return self
     */
    public static function jsonIssues(\JsonException $previous)
    {
    }
    /** @return self */
    public static function invalidBase64String()
    {
    }
}
