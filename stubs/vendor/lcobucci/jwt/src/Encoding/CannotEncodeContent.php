<?php

namespace Lcobucci\JWT\Encoding;

final class CannotEncodeContent extends \RuntimeException implements \Lcobucci\JWT\Exception
{
    /**
     * @param JsonException $previous
     *
     * @return self
     */
    public static function jsonIssues(\JsonException $previous)
    {
    }
}
