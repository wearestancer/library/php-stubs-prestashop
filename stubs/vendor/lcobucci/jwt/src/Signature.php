<?php

namespace Lcobucci\JWT;

/**
 * This class represents a token signature
 *
 * @author Luís Otávio Cobucci Oblonczyk <lcobucci@gmail.com>
 * @since 0.1.0
 */
class Signature
{
    /**
     * The resultant hash
     *
     * @var string
     */
    protected $hash;
    /**
     * Initializes the object
     *
     * @param string $hash
     * @param string $encoded
     */
    public function __construct($hash, $encoded = '')
    {
    }
    /** @return self */
    public static function fromEmptyData()
    {
    }
    /**
     * Verifies if the current hash matches with with the result of the creation of
     * a new signature with given data
     *
     * @param Signer $signer
     * @param string $payload
     * @param Key|string $key
     *
     * @return boolean
     */
    public function verify(\Lcobucci\JWT\Signer $signer, $payload, $key)
    {
    }
    /**
     * Returns the current hash as a string representation of the signature
     *
     * @deprecated This method has been removed from the public API in v4
     * @see Signature::hash()
     *
     * @return string
     */
    public function __toString()
    {
    }
    /** @return string */
    public function hash()
    {
    }
    /** @return string */
    public function toString()
    {
    }
}
