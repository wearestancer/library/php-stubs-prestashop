<?php

//
// check if the merge during mixin is overwriting values. should or should it not?
//
//
// 1. split apart class tags
//
class easyparse
{
    var $buffer;
    var $count;
    function __construct($str)
    {
    }
    function seek($where = \null)
    {
    }
    function preg_quote($what)
    {
    }
    function match($regex, &$out, $eatWhitespace = \true)
    {
    }
    function literal($what, $eatWhitespace = \true)
    {
    }
}
class tagparse extends \easyparse
{
    function parse()
    {
    }
    static function compileString($string)
    {
    }
    static function compilePaths($paths)
    {
    }
    // array of tags
    static function compilePath($path)
    {
    }
    static function compileTag($tag)
    {
    }
    function string(&$out)
    {
    }
    function tag(&$out)
    {
    }
    function ident(&$out)
    {
    }
    function value(&$out)
    {
    }
    function combinator(&$op)
    {
    }
}
class nodecounter
{
    var $count = 0;
    var $children = array();
    var $name;
    var $child_blocks;
    var $the_block;
    function __construct($name)
    {
    }
    function dump($stack = \null)
    {
    }
    static function compileProperties($c, $block)
    {
    }
    function compile($c, $path = \null)
    {
    }
    function getName()
    {
    }
    function getNode($name)
    {
    }
    function findNode($path)
    {
    }
    function addBlock($path, $block)
    {
    }
    function addToNode($path, $block)
    {
    }
}
/**
 * create a less file from a css file by combining blocks where appropriate
 */
class lessify extends \lessc
{
    public function dump()
    {
    }
    public function parse($str = \null)
    {
    }
}
