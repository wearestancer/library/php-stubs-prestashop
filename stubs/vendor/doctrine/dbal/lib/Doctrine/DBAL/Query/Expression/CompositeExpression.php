<?php

namespace Doctrine\DBAL\Query\Expression;

/**
 * Composite expression is responsible to build a group of similar expression.
 */
class CompositeExpression implements \Countable
{
    /**
     * Constant that represents an AND composite expression.
     */
    public const TYPE_AND = 'AND';
    /**
     * Constant that represents an OR composite expression.
     */
    public const TYPE_OR = 'OR';
    /**
     * @internal Use the and() / or() factory methods.
     *
     * @param string          $type  Instance type of composite expression.
     * @param self[]|string[] $parts Composition of expressions to be joined on composite expression.
     */
    public function __construct($type, array $parts = [])
    {
    }
    /**
     * @param self|string $part
     * @param self|string ...$parts
     */
    public static function and($part, ...$parts) : self
    {
    }
    /**
     * @param self|string $part
     * @param self|string ...$parts
     */
    public static function or($part, ...$parts) : self
    {
    }
    /**
     * Adds multiple parts to composite expression.
     *
     * @deprecated This class will be made immutable. Use with() instead.
     *
     * @param self[]|string[] $parts
     *
     * @return CompositeExpression
     */
    public function addMultiple(array $parts = [])
    {
    }
    /**
     * Adds an expression to composite expression.
     *
     * @deprecated This class will be made immutable. Use with() instead.
     *
     * @param mixed $part
     *
     * @return CompositeExpression
     */
    public function add($part)
    {
    }
    /**
     * Returns a new CompositeExpression with the given parts added.
     *
     * @param self|string $part
     * @param self|string ...$parts
     */
    public function with($part, ...$parts) : self
    {
    }
    /**
     * Retrieves the amount of expressions on composite expression.
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * Retrieves the string representation of this composite expression.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Returns the type of this composite expression (AND/OR).
     *
     * @return string
     */
    public function getType()
    {
    }
}
