<?php

namespace Doctrine\DBAL\Query;

/**
 * @psalm-immutable
 */
class QueryException extends \Doctrine\DBAL\Exception
{
    /**
     * @param string   $alias
     * @param string[] $registeredAliases
     *
     * @return QueryException
     */
    public static function unknownAlias($alias, $registeredAliases)
    {
    }
    /**
     * @param string   $alias
     * @param string[] $registeredAliases
     *
     * @return QueryException
     */
    public static function nonUniqueAlias($alias, $registeredAliases)
    {
    }
}
