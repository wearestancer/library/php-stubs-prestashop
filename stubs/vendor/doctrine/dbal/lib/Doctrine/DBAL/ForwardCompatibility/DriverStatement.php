<?php

namespace Doctrine\DBAL\ForwardCompatibility;

interface DriverStatement extends \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Result
{
}
