<?php

namespace Doctrine\DBAL\ForwardCompatibility;

interface DriverResultStatement extends \Doctrine\DBAL\Driver\ResultStatement, \Doctrine\DBAL\Result
{
}
