<?php

namespace Doctrine\DBAL\ForwardCompatibility;

/**
 * A wrapper around a Doctrine\DBAL\Driver\ResultStatement that adds 3.0 features
 * defined in Result interface
 */
class Result implements \IteratorAggregate, \Doctrine\DBAL\ForwardCompatibility\DriverStatement, \Doctrine\DBAL\ForwardCompatibility\DriverResultStatement
{
    public static function ensure(\Doctrine\DBAL\Driver\ResultStatement $stmt) : \Doctrine\DBAL\ForwardCompatibility\Result
    {
    }
    public function __construct(\Doctrine\DBAL\Driver\ResultStatement $stmt)
    {
    }
    /**
     * @return Driver\ResultStatement
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use Result::free() instead.
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function columnCount()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchAllKeyValue() : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchAllAssociativeIndexed() : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Traversable<int,array<int,mixed>>
     */
    public function iterateNumeric() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Traversable<int,array<string,mixed>>
     */
    public function iterateAssociative() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Traversable<mixed,mixed>
     */
    public function iterateKeyValue() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Traversable<mixed,array<string,mixed>>
     */
    public function iterateAssociativeIndexed() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Traversable<int,mixed>
     */
    public function iterateColumn() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     */
    public function rowCount()
    {
    }
    public function free() : void
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated This feature will no longer be available on Result object in 3.0.x version.
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated This feature will no longer be available on Result object in 3.0.x version.
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated This feature will no longer be available on Result object in 3.0.x version.
     */
    public function execute($params = null)
    {
    }
}
