<?php

namespace Doctrine\DBAL\Schema;

/**
 * Sqlite SchemaManager.
 */
class SqliteSchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * {@inheritdoc}
     */
    public function dropDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renameTable($name, $newName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createForeignKey(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropAndCreateForeignKey(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropForeignKey($foreignKey, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function listTableForeignKeys($table, $database = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://ezcomponents.org/docs/api/trunk/DatabaseSchema/ezcDbSchemaPgsqlReader.html
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * @deprecated
     *
     * @param array<string, mixed> $tableIndex
     *
     * @return array<string, bool|string>
     */
    protected function _getPortableTableIndexDefinition($tableIndex)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnList($table, $database, $tableColumns)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableViewDefinition($view)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys)
    {
    }
    /**
     * @param string $name
     */
    public function listTableDetails($name) : \Doctrine\DBAL\Schema\Table
    {
    }
}
