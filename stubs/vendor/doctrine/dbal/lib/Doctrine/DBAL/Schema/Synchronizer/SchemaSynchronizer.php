<?php

namespace Doctrine\DBAL\Schema\Synchronizer;

/**
 * The synchronizer knows how to synchronize a schema with the configured
 * database.
 *
 * @deprecated
 */
interface SchemaSynchronizer
{
    /**
     * Gets the SQL statements that can be executed to create the schema.
     *
     * @return string[]
     */
    public function getCreateSchema(\Doctrine\DBAL\Schema\Schema $createSchema);
    /**
     * Gets the SQL Statements to update given schema with the underlying db.
     *
     * @param bool $noDrops
     *
     * @return string[]
     */
    public function getUpdateSchema(\Doctrine\DBAL\Schema\Schema $toSchema, $noDrops = false);
    /**
     * Gets the SQL Statements to drop the given schema from underlying db.
     *
     * @return string[]
     */
    public function getDropSchema(\Doctrine\DBAL\Schema\Schema $dropSchema);
    /**
     * Gets the SQL statements to drop all schema assets from underlying db.
     *
     * @return string[]
     */
    public function getDropAllSchema();
    /**
     * Creates the Schema.
     *
     * @return void
     */
    public function createSchema(\Doctrine\DBAL\Schema\Schema $createSchema);
    /**
     * Updates the Schema to new schema version.
     *
     * @param bool $noDrops
     *
     * @return void
     */
    public function updateSchema(\Doctrine\DBAL\Schema\Schema $toSchema, $noDrops = false);
    /**
     * Drops the given database schema from the underlying db.
     *
     * @return void
     */
    public function dropSchema(\Doctrine\DBAL\Schema\Schema $dropSchema);
    /**
     * Drops all assets from the underlying db.
     *
     * @return void
     */
    public function dropAllSchema();
}
