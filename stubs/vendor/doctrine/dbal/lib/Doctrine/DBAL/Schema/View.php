<?php

namespace Doctrine\DBAL\Schema;

/**
 * Representation of a Database View.
 */
class View extends \Doctrine\DBAL\Schema\AbstractAsset
{
    /**
     * @param string $name
     * @param string $sql
     */
    public function __construct($name, $sql)
    {
    }
    /**
     * @return string
     */
    public function getSql()
    {
    }
}
