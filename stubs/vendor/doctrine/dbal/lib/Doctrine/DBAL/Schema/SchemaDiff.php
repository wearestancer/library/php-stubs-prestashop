<?php

namespace Doctrine\DBAL\Schema;

/**
 * Schema Diff.
 */
class SchemaDiff
{
    /** @var Schema|null */
    public $fromSchema;
    /**
     * All added namespaces.
     *
     * @var string[]
     */
    public $newNamespaces = [];
    /**
     * All removed namespaces.
     *
     * @var string[]
     */
    public $removedNamespaces = [];
    /**
     * All added tables.
     *
     * @var Table[]
     */
    public $newTables = [];
    /**
     * All changed tables.
     *
     * @var TableDiff[]
     */
    public $changedTables = [];
    /**
     * All removed tables.
     *
     * @var Table[]
     */
    public $removedTables = [];
    /** @var Sequence[] */
    public $newSequences = [];
    /** @var Sequence[] */
    public $changedSequences = [];
    /** @var Sequence[] */
    public $removedSequences = [];
    /** @var ForeignKeyConstraint[] */
    public $orphanedForeignKeys = [];
    /**
     * Constructs an SchemaDiff object.
     *
     * @param Table[]     $newTables
     * @param TableDiff[] $changedTables
     * @param Table[]     $removedTables
     */
    public function __construct($newTables = [], $changedTables = [], $removedTables = [], ?\Doctrine\DBAL\Schema\Schema $fromSchema = null)
    {
    }
    /**
     * The to save sql mode ensures that the following things don't happen:
     *
     * 1. Tables are deleted
     * 2. Sequences are deleted
     * 3. Foreign Keys which reference tables that would otherwise be deleted.
     *
     * This way it is ensured that assets are deleted which might not be relevant to the metadata schema at all.
     *
     * @return string[]
     */
    public function toSaveSql(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return string[]
     */
    public function toSql(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @param bool $saveMode
     *
     * @return string[]
     */
    protected function _toSql(\Doctrine\DBAL\Platforms\AbstractPlatform $platform, $saveMode = false)
    {
    }
}
