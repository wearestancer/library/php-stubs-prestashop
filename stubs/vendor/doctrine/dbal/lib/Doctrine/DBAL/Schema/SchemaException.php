<?php

namespace Doctrine\DBAL\Schema;

/**
 * @psalm-immutable
 */
class SchemaException extends \Doctrine\DBAL\Exception
{
    public const TABLE_DOESNT_EXIST = 10;
    public const TABLE_ALREADY_EXISTS = 20;
    public const COLUMN_DOESNT_EXIST = 30;
    public const COLUMN_ALREADY_EXISTS = 40;
    public const INDEX_DOESNT_EXIST = 50;
    public const INDEX_ALREADY_EXISTS = 60;
    public const SEQUENCE_DOENST_EXIST = 70;
    public const SEQUENCE_ALREADY_EXISTS = 80;
    public const INDEX_INVALID_NAME = 90;
    public const FOREIGNKEY_DOESNT_EXIST = 100;
    public const NAMESPACE_ALREADY_EXISTS = 110;
    /**
     * @param string $tableName
     *
     * @return SchemaException
     */
    public static function tableDoesNotExist($tableName)
    {
    }
    /**
     * @param string $indexName
     *
     * @return SchemaException
     */
    public static function indexNameInvalid($indexName)
    {
    }
    /**
     * @param string $indexName
     * @param string $table
     *
     * @return SchemaException
     */
    public static function indexDoesNotExist($indexName, $table)
    {
    }
    /**
     * @param string $indexName
     * @param string $table
     *
     * @return SchemaException
     */
    public static function indexAlreadyExists($indexName, $table)
    {
    }
    /**
     * @param string $columnName
     * @param string $table
     *
     * @return SchemaException
     */
    public static function columnDoesNotExist($columnName, $table)
    {
    }
    /**
     * @param string $namespaceName
     *
     * @return SchemaException
     */
    public static function namespaceAlreadyExists($namespaceName)
    {
    }
    /**
     * @param string $tableName
     *
     * @return SchemaException
     */
    public static function tableAlreadyExists($tableName)
    {
    }
    /**
     * @param string $tableName
     * @param string $columnName
     *
     * @return SchemaException
     */
    public static function columnAlreadyExists($tableName, $columnName)
    {
    }
    /**
     * @param string $name
     *
     * @return SchemaException
     */
    public static function sequenceAlreadyExists($name)
    {
    }
    /**
     * @param string $name
     *
     * @return SchemaException
     */
    public static function sequenceDoesNotExist($name)
    {
    }
    /**
     * @param string $fkName
     * @param string $table
     *
     * @return SchemaException
     */
    public static function foreignKeyDoesNotExist($fkName, $table)
    {
    }
    /**
     * @return SchemaException
     */
    public static function namedForeignKeyRequired(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * @param string $changeName
     *
     * @return SchemaException
     */
    public static function alterTableChangeNotSupported($changeName)
    {
    }
}
