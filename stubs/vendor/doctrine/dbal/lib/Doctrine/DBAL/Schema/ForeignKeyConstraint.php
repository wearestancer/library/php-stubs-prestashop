<?php

namespace Doctrine\DBAL\Schema;

/**
 * An abstraction class for a foreign key constraint.
 */
class ForeignKeyConstraint extends \Doctrine\DBAL\Schema\AbstractAsset implements \Doctrine\DBAL\Schema\Constraint
{
    /**
     * Instance of the referencing table the foreign key constraint is associated with.
     *
     * @var Table
     */
    protected $_localTable;
    /**
     * Asset identifier instances of the referencing table column names the foreign key constraint is associated with.
     * array($columnName => Identifier)
     *
     * @var Identifier[]
     */
    protected $_localColumnNames;
    /**
     * Table or asset identifier instance of the referenced table name the foreign key constraint is associated with.
     *
     * @var Table|Identifier
     */
    protected $_foreignTableName;
    /**
     * Asset identifier instances of the referenced table column names the foreign key constraint is associated with.
     * array($columnName => Identifier)
     *
     * @var Identifier[]
     */
    protected $_foreignColumnNames;
    /**
     * Options associated with the foreign key constraint.
     *
     * @var mixed[]
     */
    protected $_options;
    /**
     * Initializes the foreign key constraint.
     *
     * @param string[]     $localColumnNames   Names of the referencing table columns.
     * @param Table|string $foreignTableName   Referenced table.
     * @param string[]     $foreignColumnNames Names of the referenced table columns.
     * @param string|null  $name               Name of the foreign key constraint.
     * @param mixed[]      $options            Options associated with the foreign key constraint.
     */
    public function __construct(array $localColumnNames, $foreignTableName, array $foreignColumnNames, $name = null, array $options = [])
    {
    }
    /**
     * Returns the name of the referencing table
     * the foreign key constraint is associated with.
     *
     * @return string
     */
    public function getLocalTableName()
    {
    }
    /**
     * Sets the Table instance of the referencing table
     * the foreign key constraint is associated with.
     *
     * @param Table $table Instance of the referencing table.
     *
     * @return void
     */
    public function setLocalTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * @return Table
     */
    public function getLocalTable()
    {
    }
    /**
     * Returns the names of the referencing table columns
     * the foreign key constraint is associated with.
     *
     * @return string[]
     */
    public function getLocalColumns()
    {
    }
    /**
     * Returns the quoted representation of the referencing table column names
     * the foreign key constraint is associated with.
     *
     * But only if they were defined with one or the referencing table column name
     * is a keyword reserved by the platform.
     * Otherwise the plain unquoted value as inserted is returned.
     *
     * @param AbstractPlatform $platform The platform to use for quotation.
     *
     * @return string[]
     */
    public function getQuotedLocalColumns(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Returns unquoted representation of local table column names for comparison with other FK
     *
     * @return string[]
     */
    public function getUnquotedLocalColumns()
    {
    }
    /**
     * Returns unquoted representation of foreign table column names for comparison with other FK
     *
     * @return string[]
     */
    public function getUnquotedForeignColumns()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @see getLocalColumns
     */
    public function getColumns()
    {
    }
    /**
     * Returns the quoted representation of the referencing table column names
     * the foreign key constraint is associated with.
     *
     * But only if they were defined with one or the referencing table column name
     * is a keyword reserved by the platform.
     * Otherwise the plain unquoted value as inserted is returned.
     *
     * @see getQuotedLocalColumns
     *
     * @param AbstractPlatform $platform The platform to use for quotation.
     *
     * @return string[]
     */
    public function getQuotedColumns(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Returns the name of the referenced table
     * the foreign key constraint is associated with.
     *
     * @return string
     */
    public function getForeignTableName()
    {
    }
    /**
     * Returns the non-schema qualified foreign table name.
     *
     * @return string
     */
    public function getUnqualifiedForeignTableName()
    {
    }
    /**
     * Returns the quoted representation of the referenced table name
     * the foreign key constraint is associated with.
     *
     * But only if it was defined with one or the referenced table name
     * is a keyword reserved by the platform.
     * Otherwise the plain unquoted value as inserted is returned.
     *
     * @param AbstractPlatform $platform The platform to use for quotation.
     *
     * @return string
     */
    public function getQuotedForeignTableName(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Returns the names of the referenced table columns
     * the foreign key constraint is associated with.
     *
     * @return string[]
     */
    public function getForeignColumns()
    {
    }
    /**
     * Returns the quoted representation of the referenced table column names
     * the foreign key constraint is associated with.
     *
     * But only if they were defined with one or the referenced table column name
     * is a keyword reserved by the platform.
     * Otherwise the plain unquoted value as inserted is returned.
     *
     * @param AbstractPlatform $platform The platform to use for quotation.
     *
     * @return string[]
     */
    public function getQuotedForeignColumns(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Returns whether or not a given option
     * is associated with the foreign key constraint.
     *
     * @param string $name Name of the option to check.
     *
     * @return bool
     */
    public function hasOption($name)
    {
    }
    /**
     * Returns an option associated with the foreign key constraint.
     *
     * @param string $name Name of the option the foreign key constraint is associated with.
     *
     * @return mixed
     */
    public function getOption($name)
    {
    }
    /**
     * Returns the options associated with the foreign key constraint.
     *
     * @return mixed[]
     */
    public function getOptions()
    {
    }
    /**
     * Returns the referential action for UPDATE operations
     * on the referenced table the foreign key constraint is associated with.
     *
     * @return string|null
     */
    public function onUpdate()
    {
    }
    /**
     * Returns the referential action for DELETE operations
     * on the referenced table the foreign key constraint is associated with.
     *
     * @return string|null
     */
    public function onDelete()
    {
    }
    /**
     * Checks whether this foreign key constraint intersects the given index columns.
     *
     * Returns `true` if at least one of this foreign key's local columns
     * matches one of the given index's columns, `false` otherwise.
     *
     * @param Index $index The index to be checked against.
     *
     * @return bool
     */
    public function intersectsIndexColumns(\Doctrine\DBAL\Schema\Index $index)
    {
    }
}
