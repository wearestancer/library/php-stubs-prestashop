<?php

namespace Doctrine\DBAL\Schema;

/**
 * Oracle Schema Manager.
 */
class OracleSchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * {@inheritdoc}
     */
    public function dropDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableViewDefinition($view)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableUserDefinition($user)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://ezcomponents.org/docs/api/trunk/DatabaseSchema/ezcDbSchemaPgsqlReader.html
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableSequenceDefinition($sequence)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    protected function _getPortableFunctionDefinition($function)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableDatabaseDefinition($database)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $database
     *
     * Calling this method without an argument or by passing NULL is deprecated.
     */
    public function createDatabase($database = null)
    {
    }
    /**
     * @param string $table
     *
     * @return bool
     */
    public function dropAutoincrement($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropTable($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function listTableDetails($name) : \Doctrine\DBAL\Schema\Table
    {
    }
}
