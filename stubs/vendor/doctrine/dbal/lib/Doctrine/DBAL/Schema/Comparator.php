<?php

namespace Doctrine\DBAL\Schema;

/**
 * Compares two Schemas and return an instance of SchemaDiff.
 */
class Comparator
{
    /**
     * @return SchemaDiff
     */
    public static function compareSchemas(\Doctrine\DBAL\Schema\Schema $fromSchema, \Doctrine\DBAL\Schema\Schema $toSchema)
    {
    }
    /**
     * Returns a SchemaDiff object containing the differences between the schemas $fromSchema and $toSchema.
     *
     * The returned differences are returned in such a way that they contain the
     * operations to change the schema stored in $fromSchema to the schema that is
     * stored in $toSchema.
     *
     * @return SchemaDiff
     */
    public function compare(\Doctrine\DBAL\Schema\Schema $fromSchema, \Doctrine\DBAL\Schema\Schema $toSchema)
    {
    }
    /**
     * @return bool
     */
    public function diffSequence(\Doctrine\DBAL\Schema\Sequence $sequence1, \Doctrine\DBAL\Schema\Sequence $sequence2)
    {
    }
    /**
     * Returns the difference between the tables $fromTable and $toTable.
     *
     * If there are no differences this method returns the boolean false.
     *
     * @return TableDiff|false
     */
    public function diffTable(\Doctrine\DBAL\Schema\Table $fromTable, \Doctrine\DBAL\Schema\Table $toTable)
    {
    }
    /**
     * @return bool
     */
    public function diffForeignKey(\Doctrine\DBAL\Schema\ForeignKeyConstraint $key1, \Doctrine\DBAL\Schema\ForeignKeyConstraint $key2)
    {
    }
    /**
     * Returns the difference between the columns
     *
     * If there are differences this method returns $field2, otherwise the
     * boolean false.
     *
     * @return string[]
     */
    public function diffColumn(\Doctrine\DBAL\Schema\Column $column1, \Doctrine\DBAL\Schema\Column $column2)
    {
    }
    /**
     * Finds the difference between the indexes $index1 and $index2.
     *
     * Compares $index1 with $index2 and returns $index2 if there are any
     * differences or false in case there are no differences.
     *
     * @return bool
     */
    public function diffIndex(\Doctrine\DBAL\Schema\Index $index1, \Doctrine\DBAL\Schema\Index $index2)
    {
    }
}
