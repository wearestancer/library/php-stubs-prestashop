<?php

namespace Doctrine\DBAL\Schema;

/**
 * SQL Server Schema Manager.
 */
class SQLServerSchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * {@inheritdoc}
     */
    public function dropDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableSequenceDefinition($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableDatabaseDefinition($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getPortableNamespaceDefinition(array $namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableViewDefinition($view)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function listTableIndexes($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function alterTable(\Doctrine\DBAL\Schema\TableDiff $tableDiff)
    {
    }
    /**
     * @param string $name
     */
    public function listTableDetails($name) : \Doctrine\DBAL\Schema\Table
    {
    }
}
