<?php

namespace Doctrine\DBAL\Schema;

/**
 * Configuration for a Schema.
 */
class SchemaConfig
{
    /** @var bool */
    protected $hasExplicitForeignKeyIndexes = false;
    /** @var int */
    protected $maxIdentifierLength = 63;
    /** @var string */
    protected $name;
    /** @var mixed[] */
    protected $defaultTableOptions = [];
    /**
     * @return bool
     */
    public function hasExplicitForeignKeyIndexes()
    {
    }
    /**
     * @param bool $flag
     *
     * @return void
     */
    public function setExplicitForeignKeyIndexes($flag)
    {
    }
    /**
     * @param int $length
     *
     * @return void
     */
    public function setMaxIdentifierLength($length)
    {
    }
    /**
     * @return int
     */
    public function getMaxIdentifierLength()
    {
    }
    /**
     * Gets the default namespace of schema objects.
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Sets the default namespace name of schema objects.
     *
     * @param string $name The value to set.
     *
     * @return void
     */
    public function setName($name)
    {
    }
    /**
     * Gets the default options that are passed to Table instances created with
     * Schema#createTable().
     *
     * @return mixed[]
     */
    public function getDefaultTableOptions()
    {
    }
    /**
     * @param mixed[] $defaultTableOptions
     *
     * @return void
     */
    public function setDefaultTableOptions(array $defaultTableOptions)
    {
    }
}
