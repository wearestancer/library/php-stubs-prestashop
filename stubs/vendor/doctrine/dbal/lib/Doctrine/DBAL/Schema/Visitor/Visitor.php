<?php

namespace Doctrine\DBAL\Schema\Visitor;

/**
 * Schema Visitor used for Validation or Generation purposes.
 */
interface Visitor
{
    /**
     * @return void
     */
    public function acceptSchema(\Doctrine\DBAL\Schema\Schema $schema);
    /**
     * @return void
     */
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table);
    /**
     * @return void
     */
    public function acceptColumn(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Column $column);
    /**
     * @return void
     */
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint);
    /**
     * @return void
     */
    public function acceptIndex(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Index $index);
    /**
     * @return void
     */
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence);
}
