<?php

namespace Doctrine\DBAL\Schema;

/**
 * Represents the change of a column.
 */
class ColumnDiff
{
    /** @var string */
    public $oldColumnName;
    /** @var Column */
    public $column;
    /** @var string[] */
    public $changedProperties = [];
    /** @var Column|null */
    public $fromColumn;
    /**
     * @param string   $oldColumnName
     * @param string[] $changedProperties
     */
    public function __construct($oldColumnName, \Doctrine\DBAL\Schema\Column $column, array $changedProperties = [], ?\Doctrine\DBAL\Schema\Column $fromColumn = null)
    {
    }
    /**
     * @param string $propertyName
     *
     * @return bool
     */
    public function hasChanged($propertyName)
    {
    }
    /**
     * @return Identifier
     */
    public function getOldColumnName()
    {
    }
}
