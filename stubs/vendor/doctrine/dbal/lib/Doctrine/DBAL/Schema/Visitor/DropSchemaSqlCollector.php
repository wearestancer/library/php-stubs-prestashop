<?php

namespace Doctrine\DBAL\Schema\Visitor;

/**
 * Gathers SQL statements that allow to completely drop the current schema.
 */
class DropSchemaSqlCollector extends \Doctrine\DBAL\Schema\Visitor\AbstractVisitor
{
    public function __construct(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * @return void
     */
    public function clearQueries()
    {
    }
    /**
     * @return string[]
     */
    public function getQueries()
    {
    }
}
