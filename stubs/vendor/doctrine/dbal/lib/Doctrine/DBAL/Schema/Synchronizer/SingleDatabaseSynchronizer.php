<?php

namespace Doctrine\DBAL\Schema\Synchronizer;

/**
 * Schema Synchronizer for Default DBAL Connection.
 *
 * @deprecated
 */
class SingleDatabaseSynchronizer extends \Doctrine\DBAL\Schema\Synchronizer\AbstractSchemaSynchronizer
{
    public function __construct(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateSchema(\Doctrine\DBAL\Schema\Schema $createSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUpdateSchema(\Doctrine\DBAL\Schema\Schema $toSchema, $noDrops = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropSchema(\Doctrine\DBAL\Schema\Schema $dropSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropAllSchema()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createSchema(\Doctrine\DBAL\Schema\Schema $createSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateSchema(\Doctrine\DBAL\Schema\Schema $toSchema, $noDrops = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropSchema(\Doctrine\DBAL\Schema\Schema $dropSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropAllSchema()
    {
    }
}
