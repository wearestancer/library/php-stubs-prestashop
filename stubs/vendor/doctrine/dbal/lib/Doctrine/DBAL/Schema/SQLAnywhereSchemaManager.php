<?php

namespace Doctrine\DBAL\Schema;

/**
 * SAP Sybase SQL Anywhere schema manager.
 */
class SQLAnywhereSchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * {@inheritdoc}
     *
     * Starts a database after creation
     * as SQL Anywhere needs a database to be started
     * before it can be used.
     *
     * @see startDatabase
     */
    public function createDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     *
     * Tries stopping a database before dropping
     * as SQL Anywhere needs a database to be stopped
     * before it can be dropped.
     *
     * @see stopDatabase
     */
    public function dropDatabase($database)
    {
    }
    /**
     * Starts a database.
     *
     * @param string $database The name of the database to start.
     *
     * @return void
     */
    public function startDatabase($database)
    {
    }
    /**
     * Stops a database.
     *
     * @param string $database The name of the database to stop.
     *
     * @return void
     */
    public function stopDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableDatabaseDefinition($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableSequenceDefinition($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableViewDefinition($view)
    {
    }
}
