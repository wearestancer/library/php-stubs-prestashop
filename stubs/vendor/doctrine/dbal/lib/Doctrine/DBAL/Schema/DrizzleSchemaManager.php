<?php

namespace Doctrine\DBAL\Schema;

/**
 * Schema manager for the Drizzle RDBMS.
 */
class DrizzleSchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableDatabaseDefinition($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
}
