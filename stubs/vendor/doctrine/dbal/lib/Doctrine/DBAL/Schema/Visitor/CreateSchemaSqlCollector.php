<?php

namespace Doctrine\DBAL\Schema\Visitor;

class CreateSchemaSqlCollector extends \Doctrine\DBAL\Schema\Visitor\AbstractVisitor
{
    public function __construct(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptNamespace($namespaceName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * @return void
     */
    public function resetQueries()
    {
    }
    /**
     * Gets all queries collected so far.
     *
     * @return string[]
     */
    public function getQueries()
    {
    }
}
