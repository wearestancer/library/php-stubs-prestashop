<?php

namespace Doctrine\DBAL\Schema;

/**
 * Sequence structure.
 */
class Sequence extends \Doctrine\DBAL\Schema\AbstractAsset
{
    /** @var int */
    protected $allocationSize = 1;
    /** @var int */
    protected $initialValue = 1;
    /** @var int|null */
    protected $cache;
    /**
     * @param string   $name
     * @param int      $allocationSize
     * @param int      $initialValue
     * @param int|null $cache
     */
    public function __construct($name, $allocationSize = 1, $initialValue = 1, $cache = null)
    {
    }
    /**
     * @return int
     */
    public function getAllocationSize()
    {
    }
    /**
     * @return int
     */
    public function getInitialValue()
    {
    }
    /**
     * @return int|null
     */
    public function getCache()
    {
    }
    /**
     * @param int $allocationSize
     *
     * @return Sequence
     */
    public function setAllocationSize($allocationSize)
    {
    }
    /**
     * @param int $initialValue
     *
     * @return Sequence
     */
    public function setInitialValue($initialValue)
    {
    }
    /**
     * @param int $cache
     *
     * @return Sequence
     */
    public function setCache($cache)
    {
    }
    /**
     * Checks if this sequence is an autoincrement sequence for a given table.
     *
     * This is used inside the comparator to not report sequences as missing,
     * when the "from" schema implicitly creates the sequences.
     *
     * @return bool
     */
    public function isAutoIncrementsFor(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * @return void
     */
    public function visit(\Doctrine\DBAL\Schema\Visitor\Visitor $visitor)
    {
    }
}
