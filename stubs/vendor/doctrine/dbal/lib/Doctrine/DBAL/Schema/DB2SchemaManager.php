<?php

namespace Doctrine\DBAL\Schema;

/**
 * IBM Db2 Schema Manager.
 */
class DB2SchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * {@inheritdoc}
     *
     * Apparently creator is the schema not the user who created it:
     * {@link http://publib.boulder.ibm.com/infocenter/dzichelp/v2r2/index.jsp?topic=/com.ibm.db29.doc.sqlref/db2z_sysibmsystablestable.htm}
     */
    public function listTableNames()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTablesList($tables)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys)
    {
    }
    /**
     * @param string $def
     *
     * @return string|null
     */
    protected function _getPortableForeignKeyRuleDef($def)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableViewDefinition($view)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function listTableDetails($name) : \Doctrine\DBAL\Schema\Table
    {
    }
}
