<?php

namespace Doctrine\DBAL\Schema;

/**
 * Base class for schema managers. Schema managers are used to inspect and/or
 * modify the database schema/structure.
 */
abstract class AbstractSchemaManager
{
    /**
     * Holds instance of the Doctrine connection for this schema manager.
     *
     * @var Connection
     */
    protected $_conn;
    /**
     * Holds instance of the database platform used for this schema manager.
     *
     * @var AbstractPlatform
     */
    protected $_platform;
    /**
     * Constructor. Accepts the Connection instance to manage the schema for.
     */
    public function __construct(\Doctrine\DBAL\Connection $conn, ?\Doctrine\DBAL\Platforms\AbstractPlatform $platform = null)
    {
    }
    /**
     * Returns the associated platform.
     *
     * @return AbstractPlatform
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * Tries any method on the schema manager. Normally a method throws an
     * exception when your DBMS doesn't support it or if an error occurs.
     * This method allows you to try and method on your SchemaManager
     * instance and will return false if it does not work or is not supported.
     *
     * <code>
     * $result = $sm->tryMethod('dropView', 'view_name');
     * </code>
     *
     * @return mixed
     */
    public function tryMethod()
    {
    }
    /**
     * Lists the available databases for this connection.
     *
     * @return string[]
     */
    public function listDatabases()
    {
    }
    /**
     * Returns a list of all namespaces in the current database.
     *
     * @return string[]
     */
    public function listNamespaceNames()
    {
    }
    /**
     * Lists the available sequences for this connection.
     *
     * @param string|null $database
     *
     * @return Sequence[]
     */
    public function listSequences($database = null)
    {
    }
    /**
     * Lists the columns for a given table.
     *
     * In contrast to other libraries and to the old version of Doctrine,
     * this column definition does try to contain the 'primary' column for
     * the reason that it is not portable across different RDBMS. Use
     * {@see listTableIndexes($tableName)} to retrieve the primary key
     * of a table. Where a RDBMS specifies more details, these are held
     * in the platformDetails array.
     *
     * @param string      $table    The name of the table.
     * @param string|null $database
     *
     * @return Column[]
     */
    public function listTableColumns($table, $database = null)
    {
    }
    /**
     * Lists the indexes for a given table returning an array of Index instances.
     *
     * Keys of the portable indexes list are all lower-cased.
     *
     * @param string $table The name of the table.
     *
     * @return Index[]
     */
    public function listTableIndexes($table)
    {
    }
    /**
     * Returns true if all the given tables exist.
     *
     * The usage of a string $tableNames is deprecated. Pass a one-element array instead.
     *
     * @param string|string[] $names
     *
     * @return bool
     */
    public function tablesExist($names)
    {
    }
    /**
     * Returns a list of all tables in the current database.
     *
     * @return string[]
     */
    public function listTableNames()
    {
    }
    /**
     * Filters asset names if they are configured to return only a subset of all
     * the found elements.
     *
     * @param mixed[] $assetNames
     *
     * @return mixed[]
     */
    protected function filterAssetNames($assetNames)
    {
    }
    /**
     * @deprecated Use Configuration::getSchemaAssetsFilter() instead
     *
     * @return string|null
     */
    protected function getFilterSchemaAssetsExpression()
    {
    }
    /**
     * Lists the tables for this connection.
     *
     * @return Table[]
     */
    public function listTables()
    {
    }
    /**
     * @param string $name
     *
     * @return Table
     */
    public function listTableDetails($name)
    {
    }
    /**
     * Lists the views this connection has.
     *
     * @return View[]
     */
    public function listViews()
    {
    }
    /**
     * Lists the foreign keys for the given table.
     *
     * @param string      $table    The name of the table.
     * @param string|null $database
     *
     * @return ForeignKeyConstraint[]
     */
    public function listTableForeignKeys($table, $database = null)
    {
    }
    /* drop*() Methods */
    /**
     * Drops a database.
     *
     * NOTE: You can not drop the database this SchemaManager is currently connected to.
     *
     * @param string $database The name of the database to drop.
     *
     * @return void
     */
    public function dropDatabase($database)
    {
    }
    /**
     * Drops the given table.
     *
     * @param string $name The name of the table to drop.
     *
     * @return void
     */
    public function dropTable($name)
    {
    }
    /**
     * Drops the index from the given table.
     *
     * @param Index|string $index The name of the index.
     * @param Table|string $table The name of the table.
     *
     * @return void
     */
    public function dropIndex($index, $table)
    {
    }
    /**
     * Drops the constraint from the given table.
     *
     * @param Table|string $table The name of the table.
     *
     * @return void
     */
    public function dropConstraint(\Doctrine\DBAL\Schema\Constraint $constraint, $table)
    {
    }
    /**
     * Drops a foreign key from a table.
     *
     * @param ForeignKeyConstraint|string $foreignKey The name of the foreign key.
     * @param Table|string                $table      The name of the table with the foreign key.
     *
     * @return void
     */
    public function dropForeignKey($foreignKey, $table)
    {
    }
    /**
     * Drops a sequence with a given name.
     *
     * @param string $name The name of the sequence to drop.
     *
     * @return void
     */
    public function dropSequence($name)
    {
    }
    /**
     * Drops a view.
     *
     * @param string $name The name of the view.
     *
     * @return void
     */
    public function dropView($name)
    {
    }
    /* create*() Methods */
    /**
     * Creates a new database.
     *
     * @param string $database The name of the database to create.
     *
     * @return void
     */
    public function createDatabase($database)
    {
    }
    /**
     * Creates a new table.
     *
     * @return void
     */
    public function createTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * Creates a new sequence.
     *
     * @param Sequence $sequence
     *
     * @return void
     *
     * @throws ConnectionException If something fails at database level.
     */
    public function createSequence($sequence)
    {
    }
    /**
     * Creates a constraint on a table.
     *
     * @param Table|string $table
     *
     * @return void
     */
    public function createConstraint(\Doctrine\DBAL\Schema\Constraint $constraint, $table)
    {
    }
    /**
     * Creates a new index on a table.
     *
     * @param Table|string $table The name of the table on which the index is to be created.
     *
     * @return void
     */
    public function createIndex(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * Creates a new foreign key.
     *
     * @param ForeignKeyConstraint $foreignKey The ForeignKey instance.
     * @param Table|string         $table      The name of the table on which the foreign key is to be created.
     *
     * @return void
     */
    public function createForeignKey(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey, $table)
    {
    }
    /**
     * Creates a new view.
     *
     * @return void
     */
    public function createView(\Doctrine\DBAL\Schema\View $view)
    {
    }
    /* dropAndCreate*() Methods */
    /**
     * Drops and creates a constraint.
     *
     * @see dropConstraint()
     * @see createConstraint()
     *
     * @param Table|string $table
     *
     * @return void
     */
    public function dropAndCreateConstraint(\Doctrine\DBAL\Schema\Constraint $constraint, $table)
    {
    }
    /**
     * Drops and creates a new index on a table.
     *
     * @param Table|string $table The name of the table on which the index is to be created.
     *
     * @return void
     */
    public function dropAndCreateIndex(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * Drops and creates a new foreign key.
     *
     * @param ForeignKeyConstraint $foreignKey An associative array that defines properties
     *                                         of the foreign key to be created.
     * @param Table|string         $table      The name of the table on which the foreign key is to be created.
     *
     * @return void
     */
    public function dropAndCreateForeignKey(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey, $table)
    {
    }
    /**
     * Drops and create a new sequence.
     *
     * @return void
     *
     * @throws ConnectionException If something fails at database level.
     */
    public function dropAndCreateSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * Drops and creates a new table.
     *
     * @return void
     */
    public function dropAndCreateTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * Drops and creates a new database.
     *
     * @param string $database The name of the database to create.
     *
     * @return void
     */
    public function dropAndCreateDatabase($database)
    {
    }
    /**
     * Drops and creates a new view.
     *
     * @return void
     */
    public function dropAndCreateView(\Doctrine\DBAL\Schema\View $view)
    {
    }
    /* alterTable() Methods */
    /**
     * Alters an existing tables schema.
     *
     * @return void
     */
    public function alterTable(\Doctrine\DBAL\Schema\TableDiff $tableDiff)
    {
    }
    /**
     * Renames a given table to another name.
     *
     * @param string $name    The current name of the table.
     * @param string $newName The new name of the table.
     *
     * @return void
     */
    public function renameTable($name, $newName)
    {
    }
    /**
     * Methods for filtering return values of list*() methods to convert
     * the native DBMS data definition to a portable Doctrine definition
     */
    /**
     * @param mixed[] $databases
     *
     * @return string[]
     */
    protected function _getPortableDatabasesList($databases)
    {
    }
    /**
     * Converts a list of namespace names from the native DBMS data definition to a portable Doctrine definition.
     *
     * @param mixed[][] $namespaces The list of namespace names in the native DBMS data definition.
     *
     * @return string[]
     */
    protected function getPortableNamespacesList(array $namespaces)
    {
    }
    /**
     * @param mixed $database
     *
     * @return mixed
     */
    protected function _getPortableDatabaseDefinition($database)
    {
    }
    /**
     * Converts a namespace definition from the native DBMS data definition to a portable Doctrine definition.
     *
     * @param mixed[] $namespace The native DBMS namespace definition.
     *
     * @return mixed
     */
    protected function getPortableNamespaceDefinition(array $namespace)
    {
    }
    /**
     * @deprecated
     *
     * @param mixed[][] $functions
     *
     * @return mixed[][]
     */
    protected function _getPortableFunctionsList($functions)
    {
    }
    /**
     * @deprecated
     *
     * @param mixed[] $function
     *
     * @return mixed
     */
    protected function _getPortableFunctionDefinition($function)
    {
    }
    /**
     * @param mixed[][] $triggers
     *
     * @return mixed[][]
     */
    protected function _getPortableTriggersList($triggers)
    {
    }
    /**
     * @param mixed[] $trigger
     *
     * @return mixed
     */
    protected function _getPortableTriggerDefinition($trigger)
    {
    }
    /**
     * @param mixed[][] $sequences
     *
     * @return Sequence[]
     */
    protected function _getPortableSequencesList($sequences)
    {
    }
    /**
     * @param mixed[] $sequence
     *
     * @return Sequence
     *
     * @throws Exception
     */
    protected function _getPortableSequenceDefinition($sequence)
    {
    }
    /**
     * Independent of the database the keys of the column list result are lowercased.
     *
     * The name of the created column instance however is kept in its case.
     *
     * @param string    $table        The name of the table.
     * @param string    $database
     * @param mixed[][] $tableColumns
     *
     * @return Column[]
     */
    protected function _getPortableTableColumnList($table, $database, $tableColumns)
    {
    }
    /**
     * Gets Table Column Definition.
     *
     * @param mixed[] $tableColumn
     *
     * @return Column
     */
    protected abstract function _getPortableTableColumnDefinition($tableColumn);
    /**
     * Aggregates and groups the index results according to the required data result.
     *
     * @param mixed[][]   $tableIndexes
     * @param string|null $tableName
     *
     * @return Index[]
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * @param mixed[][] $tables
     *
     * @return string[]
     */
    protected function _getPortableTablesList($tables)
    {
    }
    /**
     * @param mixed $table
     *
     * @return string
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * @param mixed[][] $users
     *
     * @return string[][]
     */
    protected function _getPortableUsersList($users)
    {
    }
    /**
     * @param string[] $user
     *
     * @return string[]
     */
    protected function _getPortableUserDefinition($user)
    {
    }
    /**
     * @param mixed[][] $views
     *
     * @return View[]
     */
    protected function _getPortableViewsList($views)
    {
    }
    /**
     * @param mixed[] $view
     *
     * @return View|false
     */
    protected function _getPortableViewDefinition($view)
    {
    }
    /**
     * @param mixed[][] $tableForeignKeys
     *
     * @return ForeignKeyConstraint[]
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys)
    {
    }
    /**
     * @param mixed $tableForeignKey
     *
     * @return ForeignKeyConstraint
     */
    protected function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
    }
    /**
     * @param string[]|string $sql
     *
     * @return void
     */
    protected function _execSql($sql)
    {
    }
    /**
     * Creates a schema instance for the current database.
     *
     * @return Schema
     */
    public function createSchema()
    {
    }
    /**
     * Creates the configuration for this schema.
     *
     * @return SchemaConfig
     */
    public function createSchemaConfig()
    {
    }
    /**
     * The search path for namespaces in the currently connected database.
     *
     * The first entry is usually the default namespace in the Schema. All
     * further namespaces contain tables/sequences which can also be addressed
     * with a short, not full-qualified name.
     *
     * For databases that don't support subschema/namespaces this method
     * returns the name of the currently connected database.
     *
     * @return string[]
     */
    public function getSchemaSearchPaths()
    {
    }
    /**
     * Given a table comment this method tries to extract a typehint for Doctrine Type, or returns
     * the type given as default.
     *
     * @param string|null $comment
     * @param string      $currentType
     *
     * @return string
     */
    public function extractDoctrineTypeFromComment($comment, $currentType)
    {
    }
    /**
     * @param string|null $comment
     * @param string|null $type
     *
     * @return string|null
     */
    public function removeDoctrineTypeFromComment($comment, $type)
    {
    }
}
