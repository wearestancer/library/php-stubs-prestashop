<?php

namespace Doctrine\DBAL\Schema\Visitor;

/**
 * Visit a SchemaDiff.
 */
interface SchemaDiffVisitor
{
    /**
     * Visit an orphaned foreign key whose table was deleted.
     *
     * @return void
     */
    public function visitOrphanedForeignKey(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey);
    /**
     * Visit a sequence that has changed.
     *
     * @return void
     */
    public function visitChangedSequence(\Doctrine\DBAL\Schema\Sequence $sequence);
    /**
     * Visit a sequence that has been removed.
     *
     * @return void
     */
    public function visitRemovedSequence(\Doctrine\DBAL\Schema\Sequence $sequence);
    /** @return void */
    public function visitNewSequence(\Doctrine\DBAL\Schema\Sequence $sequence);
    /** @return void */
    public function visitNewTable(\Doctrine\DBAL\Schema\Table $table);
    /** @return void */
    public function visitNewTableForeignKey(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey);
    /** @return void */
    public function visitRemovedTable(\Doctrine\DBAL\Schema\Table $table);
    /** @return void */
    public function visitChangedTable(\Doctrine\DBAL\Schema\TableDiff $tableDiff);
}
