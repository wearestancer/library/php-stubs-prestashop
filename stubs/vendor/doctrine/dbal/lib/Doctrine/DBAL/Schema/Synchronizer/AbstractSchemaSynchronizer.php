<?php

namespace Doctrine\DBAL\Schema\Synchronizer;

/**
 * Abstract schema synchronizer with methods for executing batches of SQL.
 *
 * @deprecated
 */
abstract class AbstractSchemaSynchronizer implements \Doctrine\DBAL\Schema\Synchronizer\SchemaSynchronizer
{
    /** @var Connection */
    protected $conn;
    public function __construct(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * @param string[] $sql
     *
     * @return void
     */
    protected function processSqlSafely(array $sql)
    {
    }
    /**
     * @param string[] $sql
     *
     * @return void
     */
    protected function processSql(array $sql)
    {
    }
}
