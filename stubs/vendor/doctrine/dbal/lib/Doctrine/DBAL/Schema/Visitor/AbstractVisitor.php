<?php

namespace Doctrine\DBAL\Schema\Visitor;

/**
 * Abstract Visitor with empty methods for easy extension.
 */
class AbstractVisitor implements \Doctrine\DBAL\Schema\Visitor\Visitor, \Doctrine\DBAL\Schema\Visitor\NamespaceVisitor
{
    public function acceptSchema(\Doctrine\DBAL\Schema\Schema $schema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptNamespace($namespaceName)
    {
    }
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    public function acceptColumn(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Column $column)
    {
    }
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
    }
    public function acceptIndex(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
}
