<?php

namespace Doctrine\DBAL\Schema;

/**
 * PostgreSQL Schema Manager.
 */
class PostgreSqlSchemaManager extends \Doctrine\DBAL\Schema\AbstractSchemaManager
{
    /**
     * Gets all the existing schema names.
     *
     * @return string[]
     */
    public function getSchemaNames()
    {
    }
    /**
     * Returns an array of schema search paths.
     *
     * This is a PostgreSQL only function.
     *
     * @return string[]
     */
    public function getSchemaSearchPaths()
    {
    }
    /**
     * Gets names of all existing schemas in the current users search path.
     *
     * This is a PostgreSQL only function.
     *
     * @return string[]
     */
    public function getExistingSchemaSearchPaths()
    {
    }
    /**
     * Sets or resets the order of the existing schemas in the current search path of the user.
     *
     * This is a PostgreSQL only function.
     *
     * @return void
     */
    public function determineExistingSchemaSearchPaths()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropDatabase($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTriggerDefinition($trigger)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableViewDefinition($view)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableUserDefinition($user)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableDefinition($table)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://ezcomponents.org/docs/api/trunk/DatabaseSchema/ezcDbSchemaPgsqlReader.html
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableDatabaseDefinition($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableSequencesList($sequences)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getPortableNamespaceDefinition(array $namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableSequenceDefinition($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function listTableDetails($name) : \Doctrine\DBAL\Schema\Table
    {
    }
}
