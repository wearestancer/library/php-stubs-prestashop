<?php

namespace Doctrine\DBAL;

/**
 * A thin wrapper around a Doctrine\DBAL\Driver\Statement that adds support
 * for logging, DBAL mapping types, etc.
 */
class Statement implements \IteratorAggregate, \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Abstraction\Result
{
    /**
     * The SQL statement.
     *
     * @var string
     */
    protected $sql;
    /**
     * The bound parameters.
     *
     * @var mixed[]
     */
    protected $params = [];
    /**
     * The parameter types.
     *
     * @var int[]|string[]
     */
    protected $types = [];
    /**
     * The underlying driver statement.
     *
     * @var \Doctrine\DBAL\Driver\Statement
     */
    protected $stmt;
    /**
     * The underlying database platform.
     *
     * @var AbstractPlatform
     */
    protected $platform;
    /**
     * The connection this statement is bound to and executed on.
     *
     * @var Connection
     */
    protected $conn;
    /**
     * Creates a new <tt>Statement</tt> for the given SQL and <tt>Connection</tt>.
     *
     * @internal The statement can be only instantiated by {@link Connection}.
     *
     * @param string     $sql  The SQL of the statement.
     * @param Connection $conn The connection on which the statement should be executed.
     */
    public function __construct($sql, \Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * Binds a parameter value to the statement.
     *
     * The value can optionally be bound with a PDO binding type or a DBAL mapping type.
     * If bound with a DBAL mapping type, the binding type is derived from the mapping
     * type and the value undergoes the conversion routines of the mapping type before
     * being bound.
     *
     * @param string|int $param The name or position of the parameter.
     * @param mixed      $value The value of the parameter.
     * @param mixed      $type  Either a PDO binding type or a DBAL mapping type name or instance.
     *
     * @return bool TRUE on success, FALSE on failure.
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * Binds a parameter to a value by reference.
     *
     * Binding a parameter by reference does not support DBAL mapping types.
     *
     * @param string|int $param    The name or position of the parameter.
     * @param mixed      $variable The reference to the variable to bind.
     * @param int        $type     The PDO binding type.
     * @param int|null   $length   Must be specified when using an OUT bind
     *                             so that PHP allocates enough memory to hold the returned value.
     *
     * @return bool TRUE on success, FALSE on failure.
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null)
    {
    }
    /**
     * Executes the statement with the currently bound parameters.
     *
     * @deprecated Statement::execute() is deprecated, use Statement::executeQuery() or executeStatement() instead
     *
     * @param mixed[]|null $params
     *
     * @return bool TRUE on success, FALSE on failure.
     *
     * @throws Exception
     */
    public function execute($params = null)
    {
    }
    /**
     * Executes the statement with the currently bound parameters and return result.
     *
     * @param mixed[] $params
     *
     * @throws Exception
     */
    public function executeQuery(array $params = []) : \Doctrine\DBAL\Result
    {
    }
    /**
     * Executes the statement with the currently bound parameters and return affected rows.
     *
     * @param mixed[] $params
     *
     * @throws Exception
     */
    public function executeStatement(array $params = []) : int
    {
    }
    /**
     * Closes the cursor, freeing the database resources used by this statement.
     *
     * @deprecated Use Result::free() instead.
     *
     * @return bool TRUE on success, FALSE on failure.
     */
    public function closeCursor()
    {
    }
    /**
     * Returns the number of columns in the result set.
     *
     * @return int
     */
    public function columnCount()
    {
    }
    /**
     * Fetches the SQLSTATE associated with the last operation on the statement.
     *
     * @deprecated The error information is available via exceptions.
     *
     * @return string|int|bool
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * Required by interface IteratorAggregate.
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     *
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Result::fetchNumeric() instead
     *
     * @throws Exception
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Result::fetchAssociative() instead
     *
     * @throws Exception
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use Result::fetchOne() instead
     *
     * @throws Exception
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Result::fetchAllNumeric() instead
     *
     * @throws Exception
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Result::fetchAllAssociative() instead
     *
     * @throws Exception
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * Returns an associative array with the keys mapped to the first column and the values mapped to the second column.
     *
     * The result must contain at least two columns.
     *
     * @deprecated Use Result::fetchAllKeyValue() instead
     *
     * @return array<mixed,mixed>
     *
     * @throws Exception
     */
    public function fetchAllKeyValue() : array
    {
    }
    /**
     * Returns an associative array with the keys mapped to the first column and the values being
     * an associative array representing the rest of the columns and their values.
     *
     * @deprecated Use Result::fetchAllAssociativeIndexed() instead
     *
     * @return array<mixed,array<string,mixed>>
     *
     * @throws Exception
     */
    public function fetchAllAssociativeIndexed() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Result::fetchFirstColumn() instead
     *
     * @throws Exception
     */
    public function fetchFirstColumn() : array
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use Result::iterateNumeric() instead
     *
     * @return Traversable<int,array<int,mixed>>
     *
     * @throws Exception
     */
    public function iterateNumeric() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use Result::iterateAssociative() instead
     *
     * @return Traversable<int,array<string,mixed>>
     *
     * @throws Exception
     */
    public function iterateAssociative() : \Traversable
    {
    }
    /**
     * Returns an iterator over the result set with the keys mapped to the first column
     * and the values mapped to the second column.
     *
     * The result must contain at least two columns.
     *
     * @deprecated Use Result::iterateKeyValue() instead
     *
     * @return Traversable<mixed,mixed>
     *
     * @throws Exception
     */
    public function iterateKeyValue() : \Traversable
    {
    }
    /**
     * Returns an iterator over the result set with the keys mapped to the first column and the values being
     * an associative array representing the rest of the columns and their values.
     *
     * @deprecated Use Result::iterateAssociativeIndexed() instead
     *
     * @return Traversable<mixed,array<string,mixed>>
     *
     * @throws Exception
     */
    public function iterateAssociativeIndexed() : \Traversable
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use Result::iterateColumn() instead
     *
     * @return Traversable<int,mixed>
     *
     * @throws Exception
     */
    public function iterateColumn() : \Traversable
    {
    }
    /**
     * Returns the number of rows affected by the last execution of this statement.
     *
     * @return int|string The number of affected rows.
     */
    public function rowCount()
    {
    }
    public function free() : void
    {
    }
    /**
     * Gets the wrapped driver statement.
     *
     * @return \Doctrine\DBAL\Driver\Statement
     */
    public function getWrappedStatement()
    {
    }
}
