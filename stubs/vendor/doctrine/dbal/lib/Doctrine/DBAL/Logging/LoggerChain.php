<?php

namespace Doctrine\DBAL\Logging;

/**
 * Chains multiple SQLLogger.
 */
class LoggerChain implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * @param SQLLogger[] $loggers
     */
    public function __construct(array $loggers = [])
    {
    }
    /**
     * Adds a logger in the chain.
     *
     * @deprecated Inject list of loggers via constructor instead
     *
     * @return void
     */
    public function addLogger(\Doctrine\DBAL\Logging\SQLLogger $logger)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
    }
}
