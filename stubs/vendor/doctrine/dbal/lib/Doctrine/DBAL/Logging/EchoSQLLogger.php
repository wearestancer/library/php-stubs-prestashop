<?php

namespace Doctrine\DBAL\Logging;

/**
 * A SQL logger that logs to the standard output using echo/var_dump.
 *
 * @deprecated
 */
class EchoSQLLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
    }
}
