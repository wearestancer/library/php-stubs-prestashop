<?php

namespace Doctrine\DBAL\Logging;

/**
 * Includes executed SQLs in a Debug Stack.
 */
class DebugStack implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * Executed SQL queries.
     *
     * @var array<int, array<string, mixed>>
     */
    public $queries = [];
    /**
     * If Debug Stack is enabled (log queries) or not.
     *
     * @var bool
     */
    public $enabled = true;
    /** @var float|null */
    public $start = null;
    /** @var int */
    public $currentQuery = 0;
    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
    }
}
