<?php

namespace Doctrine\DBAL\Portability;

/**
 * Portability wrapper for a Connection.
 */
class Connection extends \Doctrine\DBAL\Connection
{
    public const PORTABILITY_ALL = 255;
    public const PORTABILITY_NONE = 0;
    public const PORTABILITY_RTRIM = 1;
    public const PORTABILITY_EMPTY_TO_NULL = 4;
    public const PORTABILITY_FIX_CASE = 8;
    /**#@+
     *
     * @deprecated Will be removed as internal implementation details.
     */
    public const PORTABILITY_DB2 = 13;
    public const PORTABILITY_ORACLE = 9;
    public const PORTABILITY_POSTGRESQL = 13;
    public const PORTABILITY_SQLITE = 13;
    public const PORTABILITY_OTHERVENDORS = 12;
    public const PORTABILITY_DRIZZLE = 13;
    public const PORTABILITY_SQLANYWHERE = 13;
    public const PORTABILITY_SQLSRV = 13;
    /**
     * {@inheritdoc}
     */
    public function connect()
    {
    }
    /**
     * @return int
     */
    public function getPortability()
    {
    }
    /**
     * @return int|null
     */
    public function getFetchCase()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function executeQuery($sql, array $params = [], $types = [], ?\Doctrine\DBAL\Cache\QueryCacheProfile $qcp = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $sql
     *
     * @return Statement
     */
    public function prepare($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function query()
    {
    }
}
