<?php

namespace Doctrine\DBAL\Cache;

/**
 * Query Cache Profile handles the data relevant for query caching.
 *
 * It is a value object, setter methods return NEW instances.
 */
class QueryCacheProfile
{
    /**
     * @param int         $lifetime
     * @param string|null $cacheKey
     */
    public function __construct($lifetime = 0, $cacheKey = null, ?\Doctrine\Common\Cache\Cache $resultCache = null)
    {
    }
    /**
     * @return Cache|null
     */
    public function getResultCacheDriver()
    {
    }
    /**
     * @return int
     */
    public function getLifetime()
    {
    }
    /**
     * @return string
     *
     * @throws CacheException
     */
    public function getCacheKey()
    {
    }
    /**
     * Generates the real cache key from query, params, types and connection parameters.
     *
     * @param string                                                               $sql
     * @param array<int, mixed>|array<string, mixed>                               $params
     * @param array<int, Type|int|string|null>|array<string, Type|int|string|null> $types
     * @param array<string, mixed>                                                 $connectionParams
     *
     * @return string[]
     */
    public function generateCacheKeys($sql, $params, $types, array $connectionParams = [])
    {
    }
    /**
     * @return QueryCacheProfile
     */
    public function setResultCacheDriver(\Doctrine\Common\Cache\Cache $cache)
    {
    }
    /**
     * @param string|null $cacheKey
     *
     * @return QueryCacheProfile
     */
    public function setCacheKey($cacheKey)
    {
    }
    /**
     * @param int $lifetime
     *
     * @return QueryCacheProfile
     */
    public function setLifetime($lifetime)
    {
    }
}
