<?php

namespace Doctrine\DBAL\Cache;

/**
 * @psalm-immutable
 */
class CacheException extends \Doctrine\DBAL\Exception
{
    /**
     * @return CacheException
     */
    public static function noCacheKey()
    {
    }
    /**
     * @return CacheException
     */
    public static function noResultDriverConfigured()
    {
    }
}
