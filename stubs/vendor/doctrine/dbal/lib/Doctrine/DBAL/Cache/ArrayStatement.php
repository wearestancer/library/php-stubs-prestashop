<?php

namespace Doctrine\DBAL\Cache;

/**
 * @deprecated
 */
class ArrayStatement implements \IteratorAggregate, \Doctrine\DBAL\Driver\ResultStatement, \Doctrine\DBAL\Driver\Result
{
    /**
     * @param mixed[] $data
     */
    public function __construct(array $data)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rowCount()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    public function free() : void
    {
    }
}
