<?php

namespace Doctrine\DBAL\Cache;

/**
 * Cache statement for SQL results.
 *
 * A result is saved in multiple cache keys, there is the originally specified
 * cache key which is just pointing to result rows by key. The following things
 * have to be ensured:
 *
 * 1. lifetime of the original key has to be longer than that of all the individual rows keys
 * 2. if any one row key is missing the query has to be re-executed.
 *
 * Also you have to realize that the cache will load the whole result into memory at once to ensure 2.
 * This means that the memory usage for cached results might increase by using this feature.
 *
 * @deprecated
 */
class ResultCacheStatement implements \IteratorAggregate, \Doctrine\DBAL\Driver\ResultStatement, \Doctrine\DBAL\Driver\Result
{
    /**
     * @param string $cacheKey
     * @param string $realKey
     * @param int    $lifetime
     */
    public function __construct(\Doctrine\DBAL\Driver\ResultStatement $stmt, \Doctrine\Common\Cache\Cache $resultCache, $cacheKey, $realKey, $lifetime)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * Be warned that you will need to call this method until no rows are
     * available for caching to happen.
     *
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * Be warned that you will need to call this method until no rows are
     * available for caching to happen.
     *
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * Be warned that you will need to call this method until no rows are
     * available for caching to happen.
     *
     * {@inheritdoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * Be warned that you will need to call this method until no rows are
     * available for caching to happen.
     *
     * {@inheritdoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * Be warned that you will need to call this method until no rows are
     * available for caching to happen.
     *
     * {@inheritdoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    /**
     * Returns the number of rows affected by the last DELETE, INSERT, or UPDATE statement
     * executed by the corresponding object.
     *
     * If the last SQL statement executed by the associated Statement object was a SELECT statement,
     * some databases may return the number of rows returned by that statement. However,
     * this behaviour is not guaranteed for all databases and should not be
     * relied on for portable applications.
     *
     * @return int|string The number of rows.
     */
    public function rowCount()
    {
    }
    public function free() : void
    {
    }
}
