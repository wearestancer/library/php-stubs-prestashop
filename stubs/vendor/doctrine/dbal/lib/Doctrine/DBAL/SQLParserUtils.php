<?php

namespace Doctrine\DBAL;

/**
 * Utility class that parses sql statements with regard to types and parameters.
 *
 * @internal
 */
class SQLParserUtils
{
    /**#@+
     *
     * @deprecated Will be removed as internal implementation details.
     */
    public const POSITIONAL_TOKEN = '\\?';
    public const NAMED_TOKEN = '(?<!:):[a-zA-Z_][a-zA-Z0-9_]*';
    // Quote characters within string literals can be preceded by a backslash.
    public const ESCAPED_SINGLE_QUOTED_TEXT = "(?:'(?:\\\\)+'|'(?:[^'\\\\]|\\\\'?|'')*')";
    public const ESCAPED_DOUBLE_QUOTED_TEXT = '(?:"(?:\\\\)+"|"(?:[^"\\\\]|\\\\"?)*")';
    public const ESCAPED_BACKTICK_QUOTED_TEXT = '(?:`(?:\\\\)+`|`(?:[^`\\\\]|\\\\`?)*`)';
    /**
     * Gets an array of the placeholders in an sql statements as keys and their positions in the query string.
     *
     * For a statement with positional parameters, returns a zero-indexed list of placeholder position.
     * For a statement with named parameters, returns a map of placeholder positions to their parameter names.
     *
     * @deprecated Will be removed as internal implementation detail.
     *
     * @param string $statement
     * @param bool   $isPositional
     *
     * @return int[]|string[]
     */
    public static function getPlaceholderPositions($statement, $isPositional = true)
    {
    }
    /**
     * For a positional query this method can rewrite the sql statement with regard to array parameters.
     *
     * @param string                                                               $query  SQL query
     * @param mixed[]                                                              $params Query parameters
     * @param array<int, Type|int|string|null>|array<string, Type|int|string|null> $types  Parameter types
     *
     * @return mixed[]
     *
     * @throws SQLParserUtilsException
     */
    public static function expandListParameters($query, $params, $types)
    {
    }
}
