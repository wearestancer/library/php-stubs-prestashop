<?php

namespace Doctrine\DBAL\Sharding;

/**
 * Sharding related Exceptions
 *
 * @deprecated
 *
 * @psalm-immutable
 */
class ShardingException extends \Doctrine\DBAL\Exception
{
    /**
     * @return ShardingException
     */
    public static function notImplemented()
    {
    }
    /**
     * @return ShardingException
     */
    public static function missingDefaultFederationName()
    {
    }
    /**
     * @return ShardingException
     */
    public static function missingDefaultDistributionKey()
    {
    }
    /**
     * @return ShardingException
     */
    public static function activeTransaction()
    {
    }
    /**
     * @return ShardingException
     */
    public static function noShardDistributionValue()
    {
    }
    /**
     * @return ShardingException
     */
    public static function missingDistributionType()
    {
    }
}
