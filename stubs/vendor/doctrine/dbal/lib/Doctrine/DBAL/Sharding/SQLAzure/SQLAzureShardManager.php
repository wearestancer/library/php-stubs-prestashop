<?php

namespace Doctrine\DBAL\Sharding\SQLAzure;

/**
 * Sharding using the SQL Azure Federations support.
 *
 * @deprecated
 */
class SQLAzureShardManager implements \Doctrine\DBAL\Sharding\ShardManager
{
    /**
     * @throws ShardingException
     */
    public function __construct(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * Gets the name of the federation.
     *
     * @return string
     */
    public function getFederationName()
    {
    }
    /**
     * Gets the distribution key.
     *
     * @return string
     */
    public function getDistributionKey()
    {
    }
    /**
     * Gets the Doctrine Type name used for the distribution.
     *
     * @return string
     */
    public function getDistributionType()
    {
    }
    /**
     * Sets Enabled/Disable filtering on the fly.
     *
     * @param bool $flag
     *
     * @return void
     */
    public function setFilteringEnabled($flag)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function selectGlobal()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function selectShard($distributionValue)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCurrentDistributionValue()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getShards()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function queryAll($sql, array $params = [], array $types = [])
    {
    }
    /**
     * Splits Federation at a given distribution value.
     *
     * @param mixed $splitDistributionValue
     *
     * @return void
     */
    public function splitFederation($splitDistributionValue)
    {
    }
}
