<?php

namespace Doctrine\DBAL\Sharding\SQLAzure;

/**
 * SQL Azure Schema Synchronizer.
 *
 * Will iterate over all shards when performing schema operations. This is done
 * by partitioning the passed schema into subschemas for the federation and the
 * global database and then applying the operations step by step using the
 * {@see \Doctrine\DBAL\Schema\Synchronizer\SingleDatabaseSynchronizer}.
 *
 * @deprecated
 */
class SQLAzureFederationsSynchronizer extends \Doctrine\DBAL\Schema\Synchronizer\AbstractSchemaSynchronizer
{
    public const FEDERATION_TABLE_FEDERATED = 'azure.federated';
    public const FEDERATION_DISTRIBUTION_NAME = 'azure.federatedOnDistributionName';
    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\DBAL\Sharding\SQLAzure\SQLAzureShardManager $shardManager, ?\Doctrine\DBAL\Schema\Synchronizer\SchemaSynchronizer $sync = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateSchema(\Doctrine\DBAL\Schema\Schema $createSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUpdateSchema(\Doctrine\DBAL\Schema\Schema $toSchema, $noDrops = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropSchema(\Doctrine\DBAL\Schema\Schema $dropSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createSchema(\Doctrine\DBAL\Schema\Schema $createSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateSchema(\Doctrine\DBAL\Schema\Schema $toSchema, $noDrops = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropSchema(\Doctrine\DBAL\Schema\Schema $dropSchema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropAllSchema()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dropAllSchema()
    {
    }
}
