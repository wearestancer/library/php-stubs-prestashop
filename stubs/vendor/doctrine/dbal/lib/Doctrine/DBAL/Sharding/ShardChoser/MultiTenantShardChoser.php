<?php

namespace Doctrine\DBAL\Sharding\ShardChoser;

/**
 * The MultiTenant Shard choser assumes that the distribution value directly
 * maps to the shard id.
 *
 * @deprecated
 */
class MultiTenantShardChoser implements \Doctrine\DBAL\Sharding\ShardChoser\ShardChoser
{
    /**
     * {@inheritdoc}
     */
    public function pickShard($distributionValue, \Doctrine\DBAL\Sharding\PoolingShardConnection $conn)
    {
    }
}
