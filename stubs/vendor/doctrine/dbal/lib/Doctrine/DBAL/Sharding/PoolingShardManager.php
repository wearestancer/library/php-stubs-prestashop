<?php

namespace Doctrine\DBAL\Sharding;

/**
 * Shard Manager for the Connection Pooling Shard Strategy
 *
 * @deprecated
 */
class PoolingShardManager implements \Doctrine\DBAL\Sharding\ShardManager
{
    public function __construct(\Doctrine\DBAL\Sharding\PoolingShardConnection $conn)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function selectGlobal()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function selectShard($distributionValue)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCurrentDistributionValue()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getShards()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @throws RuntimeException
     */
    public function queryAll($sql, array $params, array $types)
    {
    }
}
