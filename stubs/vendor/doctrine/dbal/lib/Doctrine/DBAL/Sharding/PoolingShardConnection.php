<?php

namespace Doctrine\DBAL\Sharding;

/**
 * Sharding implementation that pools many different connections
 * internally and serves data from the currently active connection.
 *
 * The internals of this class are:
 *
 * - All sharding clients are specified and given a shard-id during
 *   configuration.
 * - By default, the global shard is selected. If no global shard is configured
 *   an exception is thrown on access.
 * - Selecting a shard by distribution value delegates the mapping
 *   "distributionValue" => "client" to the ShardChoser interface.
 * - An exception is thrown if trying to switch shards during an open
 *   transaction.
 *
 * Instantiation through the DriverManager looks like:
 *
 * @deprecated
 *
 * @example
 *
 * $conn = DriverManager::getConnection(array(
 *    'wrapperClass' => 'Doctrine\DBAL\Sharding\PoolingShardConnection',
 *    'driver' => 'pdo_mysql',
 *    'global' => array('user' => '', 'password' => '', 'host' => '', 'dbname' => ''),
 *    'shards' => array(
 *        array('id' => 1, 'user' => 'slave1', 'password', 'host' => '', 'dbname' => ''),
 *        array('id' => 2, 'user' => 'slave2', 'password', 'host' => '', 'dbname' => ''),
 *    ),
 *    'shardChoser' => 'Doctrine\DBAL\Sharding\ShardChoser\MultiTenantShardChoser',
 * ));
 * $shardManager = $conn->getShardManager();
 * $shardManager->selectGlobal();
 * $shardManager->selectShard($value);
 */
class PoolingShardConnection extends \Doctrine\DBAL\Connection
{
    /**
     * {@inheritDoc}
     *
     * @internal The connection can be only instantiated by the driver manager.
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $params, \Doctrine\DBAL\Driver $driver, ?\Doctrine\DBAL\Configuration $config = null, ?\Doctrine\Common\EventManager $eventManager = null)
    {
    }
    /**
     * Get active shard id.
     *
     * @return string|int|null
     */
    public function getActiveShardId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParams()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getHost()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPort()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
    }
    /**
     * Connects to a given shard.
     *
     * @param string|int|null $shardId
     *
     * @return bool
     *
     * @throws ShardingException
     */
    public function connect($shardId = null)
    {
    }
    /**
     * Connects to a specific connection.
     *
     * @param string|int $shardId
     *
     * @return \Doctrine\DBAL\Driver\Connection
     */
    protected function connectTo($shardId)
    {
    }
    /**
     * @param string|int|null $shardId
     *
     * @return bool
     */
    public function isConnected($shardId = null)
    {
    }
    /**
     * @return void
     */
    public function close()
    {
    }
}
