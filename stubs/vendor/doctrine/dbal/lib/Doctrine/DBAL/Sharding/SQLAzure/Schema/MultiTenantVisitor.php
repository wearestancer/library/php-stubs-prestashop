<?php

namespace Doctrine\DBAL\Sharding\SQLAzure\Schema;

/**
 * Converts a single tenant schema into a multi-tenant schema for SQL Azure
 * Federations under the following assumptions:
 *
 * - Every table is part of the multi-tenant application, only explicitly
 *   excluded tables are non-federated. The behavior of the tables being in
 *   global or federated database is undefined. It depends on you selecting a
 *   federation before DDL statements or not.
 * - Every Primary key of a federated table is extended by another column
 *   'tenant_id' with a default value of the SQLAzure function
 *   `federation_filtering_value('tenant_id')`.
 * - You always have to work with `filtering=On` when using federations with this
 *   multi-tenant approach.
 * - Primary keys are either using globally unique ids (GUID, Table Generator)
 *   or you explicitly add the tenant_id in every UPDATE or DELETE statement
 *   (otherwise they will affect the same-id rows from other tenants as well).
 *   SQLAzure throws errors when you try to create IDENTIY columns on federated
 *   tables.
 *
 * @deprecated
 */
class MultiTenantVisitor implements \Doctrine\DBAL\Schema\Visitor\Visitor
{
    /**
     * @param string[]    $excludedTables
     * @param string      $tenantColumnName
     * @param string|null $distributionName
     */
    public function __construct(array $excludedTables = [], $tenantColumnName = 'tenant_id', $distributionName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSchema(\Doctrine\DBAL\Schema\Schema $schema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptColumn(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Column $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptIndex(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
}
