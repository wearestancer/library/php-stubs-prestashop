<?php

namespace Doctrine\DBAL;

/**
 * @psalm-immutable
 */
class Exception extends \Doctrine\DBAL\DBALException
{
}
