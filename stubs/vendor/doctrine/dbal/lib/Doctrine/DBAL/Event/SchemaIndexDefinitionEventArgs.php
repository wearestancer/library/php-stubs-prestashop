<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when the portable index definition is generated inside {@link AbstractSchemaManager}.
 */
class SchemaIndexDefinitionEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    /**
     * @param mixed[] $tableIndex
     * @param string  $table
     */
    public function __construct(array $tableIndex, $table, \Doctrine\DBAL\Connection $connection)
    {
    }
    /**
     * Allows to clear the index which means the index will be excluded from tables index list.
     *
     * @return SchemaIndexDefinitionEventArgs
     */
    public function setIndex(?\Doctrine\DBAL\Schema\Index $index = null)
    {
    }
    /**
     * @return Index|null
     */
    public function getIndex()
    {
    }
    /**
     * @return mixed[]
     */
    public function getTableIndex()
    {
    }
    /**
     * @return string
     */
    public function getTable()
    {
    }
    /**
     * @return Connection
     */
    public function getConnection()
    {
    }
    /**
     * @deprecated Use SchemaIndexDefinitionEventArgs::getConnection() and Connection::getDatabasePlatform() instead.
     *
     * @return AbstractPlatform
     */
    public function getDatabasePlatform()
    {
    }
}
