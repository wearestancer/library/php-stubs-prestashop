<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when SQL queries for renaming table columns are generated inside {@link AbstractPlatform}.
 */
class SchemaAlterTableRenameColumnEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    /**
     * @param string $oldColumnName
     */
    public function __construct($oldColumnName, \Doctrine\DBAL\Schema\Column $column, \Doctrine\DBAL\Schema\TableDiff $tableDiff, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return string
     */
    public function getOldColumnName()
    {
    }
    /**
     * @return Column
     */
    public function getColumn()
    {
    }
    /**
     * @return TableDiff
     */
    public function getTableDiff()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * Passing multiple SQL statements as an array is deprecated. Pass each statement as an individual argument instead.
     *
     * @param string|string[] $sql
     *
     * @return SchemaAlterTableRenameColumnEventArgs
     */
    public function addSql($sql)
    {
    }
    /**
     * @return string[]
     */
    public function getSql()
    {
    }
}
