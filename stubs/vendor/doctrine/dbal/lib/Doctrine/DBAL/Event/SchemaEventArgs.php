<?php

namespace Doctrine\DBAL\Event;

/**
 * Base class for schema related events.
 */
class SchemaEventArgs extends \Doctrine\Common\EventArgs
{
    /**
     * @return SchemaEventArgs
     */
    public function preventDefault()
    {
    }
    /**
     * @return bool
     */
    public function isDefaultPrevented()
    {
    }
}
