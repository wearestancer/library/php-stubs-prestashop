<?php

namespace Doctrine\DBAL\Event\Listeners;

/**
 * MySQL Session Init Event Subscriber which allows to set the Client Encoding of the Connection.
 *
 * @deprecated Use "charset" option to PDO MySQL Connection instead.
 */
class MysqlSessionInit implements \Doctrine\Common\EventSubscriber
{
    /**
     * Configure Charset and Collation options of MySQL Client for each Connection.
     *
     * @param string      $charset   The charset.
     * @param string|bool $collation The collation, or FALSE if no collation.
     */
    public function __construct($charset = 'utf8', $collation = false)
    {
    }
    /**
     * @return void
     */
    public function postConnect(\Doctrine\DBAL\Event\ConnectionEventArgs $args)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
    }
}
