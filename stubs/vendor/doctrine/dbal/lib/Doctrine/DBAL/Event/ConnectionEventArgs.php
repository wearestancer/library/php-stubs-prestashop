<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when a Driver connection is established inside {@link Connection}.
 */
class ConnectionEventArgs extends \Doctrine\Common\EventArgs
{
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
    }
    /**
     * @return Connection
     */
    public function getConnection()
    {
    }
    /**
     * @deprecated Use ConnectionEventArgs::getConnection() and Connection::getDriver() instead.
     *
     * @return Driver
     */
    public function getDriver()
    {
    }
    /**
     * @deprecated Use ConnectionEventArgs::getConnection() and Connection::getDatabasePlatform() instead.
     *
     * @return AbstractPlatform
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * @deprecated Use ConnectionEventArgs::getConnection() and Connection::getSchemaManager() instead.
     *
     * @return AbstractSchemaManager
     */
    public function getSchemaManager()
    {
    }
}
