<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when SQL queries for removing table columns are generated inside {@link AbstractPlatform}.
 */
class SchemaAlterTableRemoveColumnEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    public function __construct(\Doctrine\DBAL\Schema\Column $column, \Doctrine\DBAL\Schema\TableDiff $tableDiff, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return Column
     */
    public function getColumn()
    {
    }
    /**
     * @return TableDiff
     */
    public function getTableDiff()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * Passing multiple SQL statements as an array is deprecated. Pass each statement as an individual argument instead.
     *
     * @param string|string[] $sql
     *
     * @return SchemaAlterTableRemoveColumnEventArgs
     */
    public function addSql($sql)
    {
    }
    /**
     * @return string[]
     */
    public function getSql()
    {
    }
}
