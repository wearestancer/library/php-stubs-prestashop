<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when SQL queries for creating tables are generated inside {@link AbstractPlatform}.
 */
class SchemaAlterTableEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    public function __construct(\Doctrine\DBAL\Schema\TableDiff $tableDiff, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return TableDiff
     */
    public function getTableDiff()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * Passing multiple SQL statements as an array is deprecated. Pass each statement as an individual argument instead.
     *
     * @param string|string[] $sql
     *
     * @return SchemaAlterTableEventArgs
     */
    public function addSql($sql)
    {
    }
    /**
     * @return string[]
     */
    public function getSql()
    {
    }
}
