<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when the portable column definition is generated inside {@link AbstractPlatform}.
 */
class SchemaColumnDefinitionEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    /**
     * @param mixed[] $tableColumn
     * @param string  $table
     * @param string  $database
     */
    public function __construct(array $tableColumn, $table, $database, \Doctrine\DBAL\Connection $connection)
    {
    }
    /**
     * Allows to clear the column which means the column will be excluded from
     * tables column list.
     *
     * @return SchemaColumnDefinitionEventArgs
     */
    public function setColumn(?\Doctrine\DBAL\Schema\Column $column = null)
    {
    }
    /**
     * @return Column|null
     */
    public function getColumn()
    {
    }
    /**
     * @return mixed[]
     */
    public function getTableColumn()
    {
    }
    /**
     * @return string
     */
    public function getTable()
    {
    }
    /**
     * @return string
     */
    public function getDatabase()
    {
    }
    /**
     * @return Connection
     */
    public function getConnection()
    {
    }
    /**
     * @deprecated Use SchemaColumnDefinitionEventArgs::getConnection() and Connection::getDatabasePlatform() instead.
     *
     * @return AbstractPlatform
     */
    public function getDatabasePlatform()
    {
    }
}
