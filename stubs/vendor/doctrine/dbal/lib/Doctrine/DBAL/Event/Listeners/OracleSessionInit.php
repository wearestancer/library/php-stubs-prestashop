<?php

namespace Doctrine\DBAL\Event\Listeners;

/**
 * Should be used when Oracle Server default environment does not match the Doctrine requirements.
 *
 * The following environment variables are required for the Doctrine default date format:
 *
 * NLS_TIME_FORMAT="HH24:MI:SS"
 * NLS_DATE_FORMAT="YYYY-MM-DD HH24:MI:SS"
 * NLS_TIMESTAMP_FORMAT="YYYY-MM-DD HH24:MI:SS"
 * NLS_TIMESTAMP_TZ_FORMAT="YYYY-MM-DD HH24:MI:SS TZH:TZM"
 */
class OracleSessionInit implements \Doctrine\Common\EventSubscriber
{
    /** @var string[] */
    protected $_defaultSessionVars = ['NLS_TIME_FORMAT' => 'HH24:MI:SS', 'NLS_DATE_FORMAT' => 'YYYY-MM-DD HH24:MI:SS', 'NLS_TIMESTAMP_FORMAT' => 'YYYY-MM-DD HH24:MI:SS', 'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM', 'NLS_NUMERIC_CHARACTERS' => '.,'];
    /**
     * @param string[] $oracleSessionVars
     */
    public function __construct(array $oracleSessionVars = [])
    {
    }
    /**
     * @return void
     */
    public function postConnect(\Doctrine\DBAL\Event\ConnectionEventArgs $args)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
    }
}
