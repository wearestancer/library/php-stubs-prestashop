<?php

namespace Doctrine\DBAL\Event\Listeners;

/**
 * Session init listener for executing a single SQL statement right after a connection is opened.
 */
class SQLSessionInit implements \Doctrine\Common\EventSubscriber
{
    /** @var string */
    protected $sql;
    /**
     * @param string $sql
     */
    public function __construct($sql)
    {
    }
    /**
     * @return void
     */
    public function postConnect(\Doctrine\DBAL\Event\ConnectionEventArgs $args)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
    }
}
