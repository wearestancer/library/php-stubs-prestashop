<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when SQL queries for changing table columns are generated inside {@link AbstractPlatform}.
 */
class SchemaAlterTableChangeColumnEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    public function __construct(\Doctrine\DBAL\Schema\ColumnDiff $columnDiff, \Doctrine\DBAL\Schema\TableDiff $tableDiff, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return ColumnDiff
     */
    public function getColumnDiff()
    {
    }
    /**
     * @return TableDiff
     */
    public function getTableDiff()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * Passing multiple SQL statements as an array is deprecated. Pass each statement as an individual argument instead.
     *
     * @param string|string[] $sql
     *
     * @return SchemaAlterTableChangeColumnEventArgs
     */
    public function addSql($sql)
    {
    }
    /**
     * @return string[]
     */
    public function getSql()
    {
    }
}
