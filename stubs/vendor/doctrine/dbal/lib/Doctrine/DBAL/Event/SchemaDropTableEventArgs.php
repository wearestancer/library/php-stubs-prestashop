<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when the SQL query for dropping tables are generated inside {@link AbstractPlatform}.
 */
class SchemaDropTableEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    /**
     * @param string|Table $table
     *
     * @throws InvalidArgumentException
     */
    public function __construct($table, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return string|Table
     */
    public function getTable()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * @param string $sql
     *
     * @return SchemaDropTableEventArgs
     */
    public function setSql($sql)
    {
    }
    /**
     * @return string|null
     */
    public function getSql()
    {
    }
}
