<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when SQL queries for creating table columns are generated inside {@link AbstractPlatform}.
 */
class SchemaCreateTableColumnEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    public function __construct(\Doctrine\DBAL\Schema\Column $column, \Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return Column
     */
    public function getColumn()
    {
    }
    /**
     * @return Table
     */
    public function getTable()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * Passing multiple SQL statements as an array is deprecated. Pass each statement as an individual argument instead.
     *
     * @param string|string[] $sql
     *
     * @return SchemaCreateTableColumnEventArgs
     */
    public function addSql($sql)
    {
    }
    /**
     * @return string[]
     */
    public function getSql()
    {
    }
}
