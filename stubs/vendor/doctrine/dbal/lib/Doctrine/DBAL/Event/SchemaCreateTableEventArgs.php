<?php

namespace Doctrine\DBAL\Event;

/**
 * Event Arguments used when SQL queries for creating tables are generated inside {@link AbstractPlatform}.
 */
class SchemaCreateTableEventArgs extends \Doctrine\DBAL\Event\SchemaEventArgs
{
    /**
     * @param mixed[][] $columns
     * @param mixed[]   $options
     */
    public function __construct(\Doctrine\DBAL\Schema\Table $table, array $columns, array $options, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * @return Table
     */
    public function getTable()
    {
    }
    /**
     * @return mixed[][]
     */
    public function getColumns()
    {
    }
    /**
     * @return mixed[]
     */
    public function getOptions()
    {
    }
    /**
     * @return AbstractPlatform
     */
    public function getPlatform()
    {
    }
    /**
     * Passing multiple SQL statements as an array is deprecated. Pass each statement as an individual argument instead.
     *
     * @param string|string[] $sql
     *
     * @return SchemaCreateTableEventArgs
     */
    public function addSql($sql)
    {
    }
    /**
     * @return string[]
     */
    public function getSql()
    {
    }
}
