<?php

namespace Doctrine\DBAL\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class NoKeyValue extends \Doctrine\DBAL\Exception
{
    public static function fromColumnCount(int $columnCount) : self
    {
    }
}
