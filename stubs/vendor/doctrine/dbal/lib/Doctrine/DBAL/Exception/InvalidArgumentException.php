<?php

namespace Doctrine\DBAL\Exception;

/**
 * Exception to be thrown when invalid arguments are passed to any DBAL API
 *
 * @psalm-immutable
 */
class InvalidArgumentException extends \Doctrine\DBAL\Exception
{
    /**
     * @return self
     */
    public static function fromEmptyCriteria()
    {
    }
}
