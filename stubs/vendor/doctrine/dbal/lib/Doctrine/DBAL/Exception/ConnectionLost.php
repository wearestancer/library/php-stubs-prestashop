<?php

namespace Doctrine\DBAL\Exception;

/**
 * @psalm-immutable
 */
final class ConnectionLost extends \Doctrine\DBAL\Exception\ConnectionException
{
}
