<?php

namespace Doctrine\DBAL\Exception;

/**
 * Base class for all errors detected in the driver.
 *
 * @psalm-immutable
 */
class DriverException extends \Doctrine\DBAL\Exception
{
    /**
     * @param string                    $message         The exception message.
     * @param DeprecatedDriverException $driverException The DBAL driver exception to chain.
     */
    public function __construct($message, \Doctrine\DBAL\Driver\DriverException $driverException)
    {
    }
    /**
     * Returns the driver specific error code if given.
     *
     * Returns null if no error code was given by the driver.
     *
     * @return int|string|null
     */
    public function getErrorCode()
    {
    }
    /**
     * Returns the SQLSTATE the driver was in at the time the error occurred, if given.
     *
     * Returns null if no SQLSTATE was given by the driver.
     *
     * @return string|null
     */
    public function getSQLState()
    {
    }
}
