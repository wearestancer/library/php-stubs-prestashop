<?php

namespace Doctrine\DBAL\Types;

/**
 * The base class for so-called Doctrine mapping types.
 *
 * A Type object is obtained by calling the static {@link getType()} method.
 */
abstract class Type
{
    /** @deprecated Use {@see Types::BIGINT} instead. */
    public const BIGINT = \Doctrine\DBAL\Types\Types::BIGINT;
    /** @deprecated Use {@see Types::BINARY} instead. */
    public const BINARY = \Doctrine\DBAL\Types\Types::BINARY;
    /** @deprecated Use {@see Types::BLOB} instead. */
    public const BLOB = \Doctrine\DBAL\Types\Types::BLOB;
    /** @deprecated Use {@see Types::BOOLEAN} instead. */
    public const BOOLEAN = \Doctrine\DBAL\Types\Types::BOOLEAN;
    /** @deprecated Use {@see Types::DATE_MUTABLE} instead. */
    public const DATE = \Doctrine\DBAL\Types\Types::DATE_MUTABLE;
    /** @deprecated Use {@see Types::DATE_IMMUTABLE} instead. */
    public const DATE_IMMUTABLE = \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE;
    /** @deprecated Use {@see Types::DATEINTERVAL} instead. */
    public const DATEINTERVAL = \Doctrine\DBAL\Types\Types::DATEINTERVAL;
    /** @deprecated Use {@see Types::DATETIME_MUTABLE} instead. */
    public const DATETIME = \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE;
    /** @deprecated Use {@see Types::DATETIME_IMMUTABLE} instead. */
    public const DATETIME_IMMUTABLE = \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE;
    /** @deprecated Use {@see Types::DATETIMETZ_MUTABLE} instead. */
    public const DATETIMETZ = \Doctrine\DBAL\Types\Types::DATETIMETZ_MUTABLE;
    /** @deprecated Use {@see Types::DATETIMETZ_IMMUTABLE} instead. */
    public const DATETIMETZ_IMMUTABLE = \Doctrine\DBAL\Types\Types::DATETIMETZ_IMMUTABLE;
    /** @deprecated Use {@see Types::DECIMAL} instead. */
    public const DECIMAL = \Doctrine\DBAL\Types\Types::DECIMAL;
    /** @deprecated Use {@see Types::FLOAT} instead. */
    public const FLOAT = \Doctrine\DBAL\Types\Types::FLOAT;
    /** @deprecated Use {@see Types::GUID} instead. */
    public const GUID = \Doctrine\DBAL\Types\Types::GUID;
    /** @deprecated Use {@see Types::INTEGER} instead. */
    public const INTEGER = \Doctrine\DBAL\Types\Types::INTEGER;
    /** @deprecated Use {@see Types::JSON} instead. */
    public const JSON = \Doctrine\DBAL\Types\Types::JSON;
    /** @deprecated Use {@see Types::JSON_ARRAY} instead. */
    public const JSON_ARRAY = \Doctrine\DBAL\Types\Types::JSON_ARRAY;
    /** @deprecated Use {@see Types::OBJECT} instead. */
    public const OBJECT = \Doctrine\DBAL\Types\Types::OBJECT;
    /** @deprecated Use {@see Types::SIMPLE_ARRAY} instead. */
    public const SIMPLE_ARRAY = \Doctrine\DBAL\Types\Types::SIMPLE_ARRAY;
    /** @deprecated Use {@see Types::SMALLINT} instead. */
    public const SMALLINT = \Doctrine\DBAL\Types\Types::SMALLINT;
    /** @deprecated Use {@see Types::STRING} instead. */
    public const STRING = \Doctrine\DBAL\Types\Types::STRING;
    /** @deprecated Use {@see Types::ARRAY} instead. */
    public const TARRAY = \Doctrine\DBAL\Types\Types::ARRAY;
    /** @deprecated Use {@see Types::TEXT} instead. */
    public const TEXT = \Doctrine\DBAL\Types\Types::TEXT;
    /** @deprecated Use {@see Types::TIME_MUTABLE} instead. */
    public const TIME = \Doctrine\DBAL\Types\Types::TIME_MUTABLE;
    /** @deprecated Use {@see Types::TIME_IMMUTABLE} instead. */
    public const TIME_IMMUTABLE = \Doctrine\DBAL\Types\Types::TIME_IMMUTABLE;
    /**
     * @internal Do not instantiate directly - use {@see Type::addType()} method instead.
     */
    public final function __construct()
    {
    }
    /**
     * Converts a value from its PHP representation to its database representation
     * of this type.
     *
     * @param mixed            $value    The value to convert.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return mixed The database representation of the value.
     */
    public function convertToDatabaseValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Converts a value from its database representation to its PHP representation
     * of this type.
     *
     * @param mixed            $value    The value to convert.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return mixed The PHP representation of the value.
     */
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Gets the default length of this type.
     *
     * @deprecated Rely on information provided by the platform instead.
     *
     * @return int|null
     */
    public function getDefaultLength(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Gets the SQL declaration snippet for a column of this type.
     *
     * @param mixed[]          $column   The column definition
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public abstract function getSQLDeclaration(array $column, \Doctrine\DBAL\Platforms\AbstractPlatform $platform);
    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public abstract function getName();
    public static final function getTypeRegistry() : \Doctrine\DBAL\Types\TypeRegistry
    {
    }
    /**
     * Factory method to create type instances.
     * Type instances are implemented as flyweights.
     *
     * @param string $name The name of the type (as returned by getName()).
     *
     * @return Type
     *
     * @throws Exception
     */
    public static function getType($name)
    {
    }
    /**
     * Adds a custom type to the type map.
     *
     * @param string             $name      The name of the type. This should correspond to what getName() returns.
     * @param class-string<Type> $className The class name of the custom type.
     *
     * @return void
     *
     * @throws Exception
     */
    public static function addType($name, $className)
    {
    }
    /**
     * Checks if exists support for a type.
     *
     * @param string $name The name of the type.
     *
     * @return bool TRUE if type is supported; FALSE otherwise.
     */
    public static function hasType($name)
    {
    }
    /**
     * Overrides an already defined type to use a different implementation.
     *
     * @param string             $name
     * @param class-string<Type> $className
     *
     * @return void
     *
     * @throws Exception
     */
    public static function overrideType($name, $className)
    {
    }
    /**
     * Gets the (preferred) binding type for values of this type that
     * can be used when binding parameters to prepared statements.
     *
     * This method should return one of the {@link ParameterType} constants.
     *
     * @return int
     */
    public function getBindingType()
    {
    }
    /**
     * Gets the types array map which holds all registered types and the corresponding
     * type class
     *
     * @return array<string, string>
     */
    public static function getTypesMap()
    {
    }
    /**
     * @deprecated Relying on string representation is discouraged and will be removed in DBAL 3.0.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Does working with this column require SQL conversion functions?
     *
     * This is a metadata function that is required for example in the ORM.
     * Usage of {@link convertToDatabaseValueSQL} and
     * {@link convertToPHPValueSQL} works for any type and mostly
     * does nothing. This method can additionally be used for optimization purposes.
     *
     * @return bool
     */
    public function canRequireSQLConversion()
    {
    }
    /**
     * Modifies the SQL expression (identifier, parameter) to convert to a database value.
     *
     * @param string $sqlExpr
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Modifies the SQL expression (identifier, parameter) to convert to a PHP value.
     *
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
    }
    /**
     * Gets an array of database types that map to this Doctrine type.
     *
     * @return string[]
     */
    public function getMappedDatabaseTypes(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * If this Doctrine Type maps to an already mapped database type,
     * reverse schema engineering can't tell them apart. You need to mark
     * one of those types as commented, which will have Doctrine use an SQL
     * comment to typehint the actual Doctrine Type.
     *
     * @return bool
     */
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
