<?php

namespace Doctrine\DBAL\Types;

/**
 * Conversion Exception is thrown when the database to PHP conversion fails.
 *
 * @psalm-immutable
 */
class ConversionException extends \Doctrine\DBAL\Exception
{
    /**
     * Thrown when a Database to Doctrine Type Conversion fails.
     *
     * @param string $value
     * @param string $toType
     *
     * @return ConversionException
     */
    public static function conversionFailed($value, $toType, ?\Throwable $previous = null)
    {
    }
    /**
     * Thrown when a Database to Doctrine Type Conversion fails and we can make a statement
     * about the expected format.
     *
     * @param string $value
     * @param string $toType
     * @param string $expectedFormat
     *
     * @return ConversionException
     */
    public static function conversionFailedFormat($value, $toType, $expectedFormat, ?\Throwable $previous = null)
    {
    }
    /**
     * Thrown when the PHP value passed to the converter was not of the expected type.
     *
     * @param mixed    $value
     * @param string   $toType
     * @param string[] $possibleTypes
     *
     * @return ConversionException
     */
    public static function conversionFailedInvalidType($value, $toType, array $possibleTypes, ?\Throwable $previous = null)
    {
    }
    /**
     * @param mixed  $value
     * @param string $format
     * @param string $error
     *
     * @return ConversionException
     */
    public static function conversionFailedSerialization($value, $format, $error)
    {
    }
    public static function conversionFailedUnserialization(string $format, string $error) : self
    {
    }
}
