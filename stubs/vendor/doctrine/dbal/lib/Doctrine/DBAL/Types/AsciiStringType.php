<?php

namespace Doctrine\DBAL\Types;

final class AsciiStringType extends \Doctrine\DBAL\Types\StringType
{
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $column, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBindingType()
    {
    }
    public function getName() : string
    {
    }
}
