<?php

namespace Doctrine\DBAL\Types;

/**
 * Type that maps a database SMALLINT to a PHP integer.
 */
class SmallIntType extends \Doctrine\DBAL\Types\Type implements \Doctrine\DBAL\Types\PhpIntegerMappingType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $column, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBindingType()
    {
    }
}
