<?php

namespace Doctrine\DBAL\Types;

/**
 * Array Type which can be used to generate json arrays.
 *
 * @deprecated Use JsonType instead
 */
class JsonArrayType extends \Doctrine\DBAL\Types\JsonType
{
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
