<?php

namespace Doctrine\DBAL\Types;

/**
 * The type registry is responsible for holding a map of all known DBAL types.
 * The types are stored using the flyweight pattern so that one type only exists as exactly one instance.
 */
final class TypeRegistry
{
    /**
     * @param array<string, Type> $instances
     */
    public function __construct(array $instances = [])
    {
    }
    /**
     * Finds a type by the given name.
     *
     * @throws Exception
     */
    public function get(string $name) : \Doctrine\DBAL\Types\Type
    {
    }
    /**
     * Finds a name for the given type.
     *
     * @throws Exception
     */
    public function lookupName(\Doctrine\DBAL\Types\Type $type) : string
    {
    }
    /**
     * Checks if there is a type of the given name.
     */
    public function has(string $name) : bool
    {
    }
    /**
     * Registers a custom type to the type map.
     *
     * @throws Exception
     */
    public function register(string $name, \Doctrine\DBAL\Types\Type $type) : void
    {
    }
    /**
     * Overrides an already defined type to use a different implementation.
     *
     * @throws Exception
     */
    public function override(string $name, \Doctrine\DBAL\Types\Type $type) : void
    {
    }
    /**
     * Gets the map of all registered types and their corresponding type instances.
     *
     * @internal
     *
     * @return array<string, Type>
     */
    public function getMap() : array
    {
    }
}
