<?php

namespace Doctrine\DBAL\Id;

class TableGeneratorSchemaVisitor implements \Doctrine\DBAL\Schema\Visitor\Visitor
{
    /**
     * @param string $generatorTableName
     */
    public function __construct($generatorTableName = 'sequences')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSchema(\Doctrine\DBAL\Schema\Schema $schema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptColumn(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Column $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptIndex(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
}
