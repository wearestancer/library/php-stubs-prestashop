<?php

namespace Doctrine\DBAL\Driver\PDO;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class Exception extends \Doctrine\DBAL\Driver\PDOException
{
    public static function new(\PDOException $exception) : self
    {
    }
}
