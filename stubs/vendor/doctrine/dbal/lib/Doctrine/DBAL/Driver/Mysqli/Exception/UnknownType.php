<?php

namespace Doctrine\DBAL\Driver\Mysqli\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class UnknownType extends \Doctrine\DBAL\Driver\Mysqli\MysqliException
{
    /**
     * @param mixed $type
     */
    public static function new($type) : self
    {
    }
}
