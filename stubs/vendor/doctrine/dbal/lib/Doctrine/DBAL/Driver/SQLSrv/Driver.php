<?php

namespace Doctrine\DBAL\Driver\SQLSrv;

/**
 * Driver for ext/sqlsrv.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractSQLServerDriver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
