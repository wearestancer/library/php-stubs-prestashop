<?php

namespace Doctrine\DBAL\Driver\SQLSrv;

/**
 * @deprecated Use {@link \Doctrine\DBAL\Driver\Exception} instead
 *
 * @psalm-immutable
 */
class SQLSrvException extends \Doctrine\DBAL\Driver\AbstractDriverException
{
    /**
     * Helper method to turn sql server errors into exception.
     *
     * @return SQLSrvException
     */
    public static function fromSqlSrvErrors()
    {
    }
}
