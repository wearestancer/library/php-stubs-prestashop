<?php

namespace Doctrine\DBAL\Driver;

/**
 * The PDO implementation of the Statement interface.
 * Used by all PDO-based drivers.
 *
 * @deprecated Use {@link Statement} instead
 */
class PDOStatement extends \PDOStatement implements \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Driver\Result
{
    use \Doctrine\DBAL\Driver\PDOStatementImplementations;
    /**
     * Protected constructor.
     *
     * @internal The statement can be only instantiated by its driver connection.
     */
    protected function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * @param mixed    $param
     * @param mixed    $variable
     * @param int      $type
     * @param int|null $length
     * @param mixed    $driverOptions
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null, $driverOptions = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     */
    #[\ReturnTypeWillChange]
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function execute($params = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    #[\ReturnTypeWillChange]
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    #[\ReturnTypeWillChange]
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    public function free() : void
    {
    }
}
