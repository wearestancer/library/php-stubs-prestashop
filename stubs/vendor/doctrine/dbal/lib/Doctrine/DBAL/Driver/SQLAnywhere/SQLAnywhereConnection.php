<?php

namespace Doctrine\DBAL\Driver\SQLAnywhere;

/**
 * SAP Sybase SQL Anywhere implementation of the Connection interface.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 */
class SQLAnywhereConnection implements \Doctrine\DBAL\Driver\Connection, \Doctrine\DBAL\Driver\ServerInfoAwareConnection
{
    /**
     * Connects to database with given connection string.
     *
     * @internal The connection can be only instantiated by its driver.
     *
     * @param string $dsn        The connection string.
     * @param bool   $persistent Whether or not to establish a persistent connection.
     *
     * @throws SQLAnywhereException
     */
    public function __construct($dsn, $persistent = false)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws SQLAnywhereException
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws SQLAnywhereException
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exec($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lastInsertId($name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prepare($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function query()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws SQLAnywhereException
     */
    public function rollBack()
    {
    }
}
