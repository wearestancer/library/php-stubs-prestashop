<?php

namespace Doctrine\DBAL\Driver\PDOSqlite;

/**
 * The PDO Sqlite driver.
 *
 * @deprecated Use {@link PDO\SQLite\Driver} instead.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractSQLiteDriver
{
    /** @var mixed[] */
    protected $_userDefinedFunctions = ['sqrt' => ['callback' => [\Doctrine\DBAL\Platforms\SqlitePlatform::class, 'udfSqrt'], 'numArgs' => 1], 'mod' => ['callback' => [\Doctrine\DBAL\Platforms\SqlitePlatform::class, 'udfMod'], 'numArgs' => 2], 'locate' => ['callback' => [\Doctrine\DBAL\Platforms\SqlitePlatform::class, 'udfLocate'], 'numArgs' => -1]];
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * Constructs the Sqlite PDO DSN.
     *
     * @param mixed[] $params
     *
     * @return string The DSN.
     */
    protected function _constructPdoDsn(array $params)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
