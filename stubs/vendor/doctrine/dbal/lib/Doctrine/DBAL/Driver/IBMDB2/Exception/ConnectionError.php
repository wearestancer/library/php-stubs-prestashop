<?php

namespace Doctrine\DBAL\Driver\IBMDB2\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class ConnectionError extends \Doctrine\DBAL\Driver\AbstractException
{
    /**
     * @param resource $connection
     */
    public static function new($connection) : self
    {
    }
}
