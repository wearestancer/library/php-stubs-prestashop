<?php

namespace Doctrine\DBAL\Driver\Mysqli\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class ConnectionError extends \Doctrine\DBAL\Driver\Mysqli\MysqliException
{
    public static function new(\mysqli $connection) : self
    {
    }
    public static function upcast(\mysqli_sql_exception $exception) : self
    {
    }
}
