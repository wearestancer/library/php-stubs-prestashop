<?php

namespace Doctrine\DBAL\Driver;

/**
 * Abstract base implementation of the {@link Driver} interface for MySQL based drivers.
 */
abstract class AbstractMySQLDriver implements \Doctrine\DBAL\Driver, \Doctrine\DBAL\Driver\ExceptionConverterDriver, \Doctrine\DBAL\VersionAwarePlatformDriver
{
    /**
     * {@inheritdoc}
     *
     * @deprecated
     *
     * @link https://dev.mysql.com/doc/mysql-errors/8.0/en/client-error-reference.html
     * @link https://dev.mysql.com/doc/mysql-errors/8.0/en/server-error-reference.html
     */
    public function convertException($message, \Doctrine\DBAL\Driver\DriverException $exception)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function createDatabasePlatformForVersion($version)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Connection::getDatabase() instead.
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return MySqlPlatform
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return MySqlSchemaManager
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
}
