<?php

namespace Doctrine\DBAL\Driver\OCI8\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class SequenceDoesNotExist extends \Doctrine\DBAL\Driver\OCI8\OCI8Exception
{
    public static function new() : self
    {
    }
}
