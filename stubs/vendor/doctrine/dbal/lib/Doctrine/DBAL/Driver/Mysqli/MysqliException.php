<?php

namespace Doctrine\DBAL\Driver\Mysqli;

/**
 * @deprecated Use {@link \Doctrine\DBAL\Driver\Exception} instead
 *
 * @psalm-immutable
 */
class MysqliException extends \Doctrine\DBAL\Driver\AbstractDriverException
{
}
