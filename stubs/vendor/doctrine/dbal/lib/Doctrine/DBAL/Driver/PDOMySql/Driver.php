<?php

namespace Doctrine\DBAL\Driver\PDOMySql;

/**
 * PDO MySql driver.
 *
 * @deprecated Use {@link PDO\MySQL\Driver} instead.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractMySQLDriver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * Constructs the MySql PDO DSN.
     *
     * @param mixed[] $params
     *
     * @return string The DSN.
     */
    protected function constructPdoDsn(array $params)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
