<?php

namespace Doctrine\DBAL\Driver\Mysqli;

/**
 * @deprecated Use {@link Statement} instead
 */
class MysqliStatement implements \IteratorAggregate, \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Driver\Result
{
    /** @var string[] */
    protected static $_paramTypeMap = [\Doctrine\DBAL\ParameterType::ASCII => 's', \Doctrine\DBAL\ParameterType::STRING => 's', \Doctrine\DBAL\ParameterType::BINARY => 's', \Doctrine\DBAL\ParameterType::BOOLEAN => 'i', \Doctrine\DBAL\ParameterType::NULL => 's', \Doctrine\DBAL\ParameterType::INTEGER => 'i', \Doctrine\DBAL\ParameterType::LARGE_OBJECT => 'b'];
    /** @var mysqli */
    protected $_conn;
    /** @var mysqli_stmt */
    protected $_stmt;
    /** @var string[]|false|null */
    protected $_columnNames;
    /** @var mixed[] */
    protected $_rowBindedValues = [];
    /** @var mixed[] */
    protected $_bindedValues;
    /** @var string */
    protected $types;
    /**
     * Contains ref values for bindValue().
     *
     * @var mixed[]
     */
    protected $_values = [];
    /** @var int */
    protected $_defaultFetchMode = \Doctrine\DBAL\FetchMode::MIXED;
    /**
     * @internal The statement can be only instantiated by its driver connection.
     *
     * @param string $prepareString
     *
     * @throws MysqliException
     */
    public function __construct(\mysqli $conn, $prepareString)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function execute($params = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     *
     * @return string
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rowCount()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {
    }
    public function free() : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
