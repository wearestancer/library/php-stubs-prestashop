<?php

namespace Doctrine\DBAL\Driver\DrizzlePDOMySql;

/**
 * @deprecated
 */
class Connection extends \Doctrine\DBAL\Driver\PDOConnection
{
    /**
     * {@inheritdoc}
     */
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
}
