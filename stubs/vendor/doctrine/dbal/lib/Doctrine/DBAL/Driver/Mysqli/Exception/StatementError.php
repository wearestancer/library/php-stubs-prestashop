<?php

namespace Doctrine\DBAL\Driver\Mysqli\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class StatementError extends \Doctrine\DBAL\Driver\Mysqli\MysqliException
{
    public static function new(\mysqli_stmt $statement) : self
    {
    }
    public static function upcast(\mysqli_sql_exception $exception) : self
    {
    }
}
