<?php

namespace Doctrine\DBAL\Driver\SQLAnywhere;

/**
 * A Doctrine DBAL driver for the SAP Sybase SQL Anywhere PHP extension.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractSQLAnywhereDriver
{
    /**
     * {@inheritdoc}
     *
     * @throws Exception If there was a problem establishing the connection.
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
