<?php

namespace Doctrine\DBAL\Driver\AbstractSQLServerDriver\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class PortWithoutHost extends \Doctrine\DBAL\Driver\AbstractDriverException
{
    public static function new() : self
    {
    }
}
