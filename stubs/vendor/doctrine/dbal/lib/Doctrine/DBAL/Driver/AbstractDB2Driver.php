<?php

namespace Doctrine\DBAL\Driver;

/**
 * Abstract base implementation of the {@link Driver} interface for IBM DB2 based drivers.
 */
abstract class AbstractDB2Driver implements \Doctrine\DBAL\Driver
{
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Connection::getDatabase() instead.
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * @param string $message
     *
     * @return DriverException
     */
    public function convertException($message, \Doctrine\DBAL\Driver\DriverException $exception)
    {
    }
}
