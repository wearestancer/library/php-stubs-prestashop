<?php

namespace Doctrine\DBAL\Driver\IBMDB2\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class ConnectionFailed extends \Doctrine\DBAL\Driver\AbstractException
{
    public static function new() : self
    {
    }
}
