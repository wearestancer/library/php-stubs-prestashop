<?php

namespace Doctrine\DBAL\Driver\IBMDB2;

/**
 * @deprecated Use {@link \Doctrine\DBAL\Driver\Exception} instead
 *
 * @psalm-immutable
 */
class DB2Exception extends \Doctrine\DBAL\Driver\AbstractDriverException
{
}
