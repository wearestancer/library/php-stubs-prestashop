<?php

namespace Doctrine\DBAL\Driver\IBMDB2\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class PrepareFailed extends \Doctrine\DBAL\Driver\AbstractException
{
    /**
     * @psalm-param array{message: string}|null $error
     */
    public static function new(?array $error) : self
    {
    }
}
