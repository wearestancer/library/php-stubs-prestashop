<?php

namespace Doctrine\DBAL\Driver\SQLSrv;

/**
 * Last Id Data Container.
 *
 * @internal
 */
class LastInsertId
{
    /**
     * @param int $id
     *
     * @return void
     */
    public function setId($id)
    {
    }
    /**
     * @return int
     */
    public function getId()
    {
    }
}
