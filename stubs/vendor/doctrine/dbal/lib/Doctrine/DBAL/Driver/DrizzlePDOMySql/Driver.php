<?php

namespace Doctrine\DBAL\Driver\DrizzlePDOMySql;

/**
 * Drizzle driver using PDO MySql.
 *
 * @deprecated
 */
class Driver extends \Doctrine\DBAL\Driver\PDOMySql\Driver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createDatabasePlatformForVersion($version)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return DrizzlePlatform
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return DrizzleSchemaManager
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
