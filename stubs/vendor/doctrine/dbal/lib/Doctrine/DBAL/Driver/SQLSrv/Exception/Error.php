<?php

namespace Doctrine\DBAL\Driver\SQLSrv\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class Error extends \Doctrine\DBAL\Driver\SQLSrv\SQLSrvException
{
    public static function new() : self
    {
    }
}
