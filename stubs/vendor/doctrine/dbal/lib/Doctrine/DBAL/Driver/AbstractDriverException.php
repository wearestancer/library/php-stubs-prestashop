<?php

namespace Doctrine\DBAL\Driver;

/**
 * @deprecated
 *
 * @psalm-immutable
 */
class AbstractDriverException extends \Doctrine\DBAL\Driver\AbstractException
{
}
