<?php

namespace Doctrine\DBAL\Driver\Mysqli\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class FailedReadingStreamOffset extends \Doctrine\DBAL\Driver\Mysqli\MysqliException
{
    public static function new(int $offset) : self
    {
    }
}
