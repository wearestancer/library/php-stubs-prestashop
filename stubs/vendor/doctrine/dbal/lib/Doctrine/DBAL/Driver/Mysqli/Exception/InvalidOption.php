<?php

namespace Doctrine\DBAL\Driver\Mysqli\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class InvalidOption extends \Doctrine\DBAL\Driver\Mysqli\MysqliException
{
    /**
     * @param mixed $option
     * @param mixed $value
     */
    public static function fromOption($option, $value) : self
    {
    }
}
