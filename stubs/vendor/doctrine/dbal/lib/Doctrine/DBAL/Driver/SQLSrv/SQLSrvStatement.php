<?php

namespace Doctrine\DBAL\Driver\SQLSrv;

/**
 * SQL Server Statement.
 *
 * @deprecated Use {@link Statement} instead
 */
class SQLSrvStatement implements \IteratorAggregate, \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Driver\Result
{
    /**
     * Append to any INSERT query to retrieve the last insert id.
     *
     * @deprecated This constant has been deprecated and will be made private in 3.0
     */
    public const LAST_INSERT_ID_SQL = ';SELECT SCOPE_IDENTITY() AS LastInsertId;';
    /**
     * @internal The statement can be only instantiated by its driver connection.
     *
     * @param resource $conn
     * @param string   $sql
     */
    public function __construct($conn, $sql, ?\Doctrine\DBAL\Driver\SQLSrv\LastInsertId $lastInsertId = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function execute($params = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     *
     * @throws SQLSrvException
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rowCount()
    {
    }
    public function free() : void
    {
    }
}
