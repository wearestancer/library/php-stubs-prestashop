<?php

namespace Doctrine\DBAL\Driver\PDOOracle;

/**
 * PDO Oracle driver.
 *
 * @deprecated Use {@link PDO\OCI\Driver} instead.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractOracleDriver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
