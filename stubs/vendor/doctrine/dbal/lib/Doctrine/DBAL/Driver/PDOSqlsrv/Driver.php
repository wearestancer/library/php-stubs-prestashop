<?php

namespace Doctrine\DBAL\Driver\PDOSqlsrv;

/**
 * The PDO-based Sqlsrv driver.
 *
 * @deprecated Use {@link PDO\SQLSrv\Driver} instead.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractSQLServerDriver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
