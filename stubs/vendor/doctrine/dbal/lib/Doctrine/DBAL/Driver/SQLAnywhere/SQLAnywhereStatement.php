<?php

namespace Doctrine\DBAL\Driver\SQLAnywhere;

/**
 * SAP SQL Anywhere implementation of the Statement interface.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 */
class SQLAnywhereStatement implements \IteratorAggregate, \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Driver\Result
{
    /**
     * Prepares given statement for given connection.
     *
     * @internal The statement can be only instantiated by its driver connection.
     *
     * @param resource $conn The connection resource to use.
     * @param string   $sql  The SQL statement to prepare.
     *
     * @throws SQLAnywhereException
     */
    public function __construct($conn, $sql)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws SQLAnywhereException
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     *
     * @throws SQLAnywhereException
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws SQLAnywhereException
     */
    public function execute($params = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     *
     * @throws SQLAnywhereException
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function fetchOne()
    {
    }
    /**
     * @return array<int,array<int,mixed>>
     *
     * @throws Exception
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * @return array<int,array<string,mixed>>
     *
     * @throws Exception
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * @return array<int,mixed>
     *
     * @throws Exception
     */
    public function fetchFirstColumn() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rowCount()
    {
    }
    public function free() : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
}
