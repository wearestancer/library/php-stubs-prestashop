<?php

namespace Doctrine\DBAL\Driver;

/**
 * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn().
 */
class StatementIterator implements \IteratorAggregate
{
    public function __construct(\Doctrine\DBAL\Driver\ResultStatement $statement)
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
