<?php

namespace Doctrine\DBAL\Driver\OCI8;

/**
 * The OCI8 implementation of the Statement interface.
 *
 * @deprecated Use {@link Statement} instead
 */
class OCI8Statement implements \IteratorAggregate, \Doctrine\DBAL\Driver\Statement, \Doctrine\DBAL\Driver\Result
{
    /** @var resource */
    protected $_dbh;
    /** @var resource */
    protected $_sth;
    /** @var OCI8Connection */
    protected $_conn;
    /**
     * @deprecated
     *
     * @var string
     */
    protected static $_PARAM = ':param';
    /** @var int[] */
    protected static $fetchModeMap = [\Doctrine\DBAL\FetchMode::MIXED => \OCI_BOTH, \Doctrine\DBAL\FetchMode::ASSOCIATIVE => \OCI_ASSOC, \Doctrine\DBAL\FetchMode::NUMERIC => \OCI_NUM, \Doctrine\DBAL\FetchMode::COLUMN => \OCI_NUM];
    /** @var int */
    protected $_defaultFetchMode = \Doctrine\DBAL\FetchMode::MIXED;
    /** @var string[] */
    protected $_paramMap = [];
    /**
     * Creates a new OCI8Statement that uses the given connection handle and SQL statement.
     *
     * @internal The statement can be only instantiated by its driver connection.
     *
     * @param resource $dbh   The connection handle.
     * @param string   $query The SQL query.
     */
    public function __construct($dbh, $query, \Doctrine\DBAL\Driver\OCI8\OCI8Connection $conn)
    {
    }
    /**
     * Converts positional (?) into named placeholders (:param<num>).
     *
     * Oracle does not support positional parameters, hence this method converts all
     * positional parameters into artificially named parameters. Note that this conversion
     * is not perfect. All question marks (?) in the original statement are treated as
     * placeholders and converted to a named parameter.
     *
     * The algorithm uses a state machine with two possible states: InLiteral and NotInLiteral.
     * Question marks inside literal strings are therefore handled correctly by this method.
     * This comes at a cost, the whole sql statement has to be looped over.
     *
     * @internal
     *
     * @param string $statement The SQL statement to convert.
     *
     * @return mixed[] [0] => the statement value (string), [1] => the paramMap value (array).
     *
     * @throws OCI8Exception
     *
     * @todo extract into utility class in Doctrine\DBAL\Util namespace
     * @todo review and test for lost spaces. we experienced missing spaces with oci8 in some sql statements.
     */
    public static function convertPositionalToNamedPlaceholders($statement)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use free() instead.
     */
    public function closeCursor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function columnCount()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function execute($params = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use one of the fetch- or iterate-related methods.
     */
    public function setFetchMode($fetchMode, $arg2 = null, $arg3 = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use iterateNumeric(), iterateAssociative() or iterateColumn() instead.
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchNumeric(), fetchAssociative() or fetchOne() instead.
     */
    public function fetch($fetchMode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     */
    public function fetchAll($fetchMode = null, $fetchArgument = null, $ctorArgs = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use fetchOne() instead.
     */
    public function fetchColumn($columnIndex = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rowCount()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchNumeric()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAssociative()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchOne()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllNumeric() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchAllAssociative() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchFirstColumn() : array
    {
    }
    public function free() : void
    {
    }
}
