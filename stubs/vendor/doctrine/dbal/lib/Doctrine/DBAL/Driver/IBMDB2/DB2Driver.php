<?php

namespace Doctrine\DBAL\Driver\IBMDB2;

/**
 * IBM DB2 Driver.
 *
 * @deprecated Use {@link Driver} instead
 */
class DB2Driver extends \Doctrine\DBAL\Driver\AbstractDB2Driver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
