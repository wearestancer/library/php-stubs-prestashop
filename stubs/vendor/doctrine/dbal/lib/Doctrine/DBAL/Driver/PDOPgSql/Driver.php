<?php

namespace Doctrine\DBAL\Driver\PDOPgSql;

/**
 * Driver that connects through pdo_pgsql.
 *
 * @deprecated Use {@link PDO\PgSQL\Driver} instead.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractPostgreSQLDriver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
