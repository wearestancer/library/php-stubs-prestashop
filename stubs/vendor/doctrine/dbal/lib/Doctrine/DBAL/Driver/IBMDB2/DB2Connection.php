<?php

namespace Doctrine\DBAL\Driver\IBMDB2;

/**
 * @deprecated Use {@link Connection} instead
 */
class DB2Connection implements \Doctrine\DBAL\Driver\Connection, \Doctrine\DBAL\Driver\ServerInfoAwareConnection
{
    /**
     * @internal The connection can be only instantiated by its driver.
     *
     * @param mixed[] $params
     * @param string  $username
     * @param string  $password
     * @param mixed[] $driverOptions
     *
     * @throws DB2Exception
     */
    public function __construct(array $params, $username, $password, $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prepare($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function query()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exec($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lastInsertId($name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rollBack()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
}
