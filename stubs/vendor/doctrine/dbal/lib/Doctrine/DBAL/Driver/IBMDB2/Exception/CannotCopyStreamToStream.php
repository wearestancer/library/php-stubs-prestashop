<?php

namespace Doctrine\DBAL\Driver\IBMDB2\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class CannotCopyStreamToStream extends \Doctrine\DBAL\Driver\IBMDB2\DB2Exception
{
    /**
     * @psalm-param array{message: string}|null $error
     */
    public static function new(?array $error) : self
    {
    }
}
