<?php

namespace Doctrine\DBAL\Driver;

/**
 * PDO implementation of the Connection interface.
 * Used by all PDO-based drivers.
 *
 * @deprecated Use {@link Connection} instead
 */
class PDOConnection extends \PDO implements \Doctrine\DBAL\Driver\Connection, \Doctrine\DBAL\Driver\ServerInfoAwareConnection
{
    use \Doctrine\DBAL\Driver\PDOQueryImplementation;
    /**
     * @internal The connection can be only instantiated by its driver.
     *
     * @param string       $dsn
     * @param string|null  $user
     * @param string|null  $password
     * @param mixed[]|null $options
     *
     * @throws PDOException In case of an error.
     */
    public function __construct($dsn, $user = null, $password = null, ?array $options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function exec($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getServerVersion()
    {
    }
    /**
     * @param string          $sql
     * @param array<int, int> $driverOptions
     *
     * @return PDOStatement
     */
    #[\ReturnTypeWillChange]
    public function prepare($sql, $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $name
     *
     * @return string|int|false
     */
    #[\ReturnTypeWillChange]
    public function lastInsertId($name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
    }
}
