<?php

namespace Doctrine\DBAL\Driver\PDOSqlsrv;

/**
 * PDO SQL Server Statement
 *
 * @deprecated Use {@link PDO\SQLSrv\Statement} instead.
 */
class Statement extends \Doctrine\DBAL\Driver\PDO\Statement
{
    /**
     * {@inheritdoc}
     */
    public function bindParam($param, &$variable, $type = \Doctrine\DBAL\ParameterType::STRING, $length = null, $driverOptions = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bindValue($param, $value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
}
