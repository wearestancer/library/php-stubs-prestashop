<?php

namespace Doctrine\DBAL\Driver\SQLAnywhere;

/**
 * SAP Sybase SQL Anywhere driver exception.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 *
 * @psalm-immutable
 */
class SQLAnywhereException extends \Doctrine\DBAL\Driver\AbstractDriverException
{
    /**
     * Helper method to turn SQL Anywhere error into exception.
     *
     * @param resource|null $conn The SQL Anywhere connection resource to retrieve the last error from.
     * @param resource|null $stmt The SQL Anywhere statement resource to retrieve the last error from.
     *
     * @return SQLAnywhereException
     *
     * @throws InvalidArgumentException
     */
    public static function fromSQLAnywhereError($conn = null, $stmt = null)
    {
    }
}
