<?php

namespace Doctrine\DBAL\Driver;

/**
 * Base implementation of the {@link Exception} interface.
 *
 * @internal
 *
 * @psalm-immutable
 */
abstract class AbstractException extends \Exception implements \Doctrine\DBAL\Driver\DriverException
{
    /**
     * @param string          $message   The driver error message.
     * @param string|null     $sqlState  The SQLSTATE the driver is in at the time the error occurred, if any.
     * @param int|string|null $errorCode The driver specific error code if any.
     */
    public function __construct($message, $sqlState = null, $errorCode = null, ?\Throwable $previous = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getErrorCode()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSQLState()
    {
    }
}
