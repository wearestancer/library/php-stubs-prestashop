<?php

namespace Doctrine\DBAL\Driver\OCI8;

/**
 * OCI8 implementation of the Connection interface.
 *
 * @deprecated Use {@link Connection} instead
 */
class OCI8Connection implements \Doctrine\DBAL\Driver\Connection, \Doctrine\DBAL\Driver\ServerInfoAwareConnection
{
    /** @var resource */
    protected $dbh;
    /** @var int */
    protected $executeMode = \OCI_COMMIT_ON_SUCCESS;
    /**
     * Creates a Connection to an Oracle Database using oci8 extension.
     *
     * @internal The connection can be only instantiated by its driver.
     *
     * @param string $username
     * @param string $password
     * @param string $db
     * @param string $charset
     * @param int    $sessionMode
     * @param bool   $persistent
     *
     * @throws OCI8Exception
     */
    public function __construct($username, $password, $db, $charset = '', $sessionMode = \OCI_NO_AUTO_COMMIT, $persistent = false)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws UnexpectedValueException If the version string returned by the database server
     *                                  does not contain a parsable version number.
     */
    public function getServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prepare($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function query()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exec($sql)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $name
     *
     * @return int|false
     */
    public function lastInsertId($name = null)
    {
    }
    /**
     * Returns the current execution mode.
     *
     * @internal
     *
     * @return int
     */
    public function getExecuteMode()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rollBack()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
}
