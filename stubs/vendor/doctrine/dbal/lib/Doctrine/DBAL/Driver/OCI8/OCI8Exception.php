<?php

namespace Doctrine\DBAL\Driver\OCI8;

/**
 * @deprecated Use {@link \Doctrine\DBAL\Driver\Exception} instead
 *
 * @psalm-immutable
 */
class OCI8Exception extends \Doctrine\DBAL\Driver\AbstractDriverException
{
    /**
     * @param mixed[]|false $error
     *
     * @return OCI8Exception
     */
    public static function fromErrorInfo($error)
    {
    }
}
