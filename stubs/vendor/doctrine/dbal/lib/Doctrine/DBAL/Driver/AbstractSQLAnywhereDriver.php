<?php

namespace Doctrine\DBAL\Driver;

/**
 * Abstract base implementation of the {@link Driver} interface for SAP Sybase SQL Anywhere based drivers.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 */
abstract class AbstractSQLAnywhereDriver implements \Doctrine\DBAL\Driver, \Doctrine\DBAL\Driver\ExceptionConverterDriver, \Doctrine\DBAL\VersionAwarePlatformDriver
{
    /**
     * {@inheritdoc}
     *
     * @deprecated
     *
     * @link http://dcx.sybase.com/index.html#sa160/en/saerrors/sqlerror.html
     */
    public function convertException($message, \Doctrine\DBAL\Driver\DriverException $exception)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createDatabasePlatformForVersion($version)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Connection::getDatabase() instead.
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
}
