<?php

namespace Doctrine\DBAL\Driver;

/**
 * @deprecated Use {@link Exception} instead
 *
 * @psalm-immutable
 */
class PDOException extends \PDOException implements \Doctrine\DBAL\Driver\DriverException
{
    /**
     * @param \PDOException $exception The PDO exception to wrap.
     */
    public function __construct(\PDOException $exception)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getErrorCode()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSQLState()
    {
    }
}
