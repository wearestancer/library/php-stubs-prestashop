<?php

namespace Doctrine\DBAL\Driver;

/**
 * Abstract base implementation of the {@link Driver} interface for PostgreSQL based drivers.
 */
abstract class AbstractPostgreSQLDriver implements \Doctrine\DBAL\Driver, \Doctrine\DBAL\Driver\ExceptionConverterDriver, \Doctrine\DBAL\VersionAwarePlatformDriver
{
    /**
     * {@inheritdoc}
     *
     * @deprecated
     *
     * @link http://www.postgresql.org/docs/9.3/static/errcodes-appendix.html
     */
    public function convertException($message, \Doctrine\DBAL\Driver\DriverException $exception)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createDatabasePlatformForVersion($version)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Connection::getDatabase() instead.
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
}
