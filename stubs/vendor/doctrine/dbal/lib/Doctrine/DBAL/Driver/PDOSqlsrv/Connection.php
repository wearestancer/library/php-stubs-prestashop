<?php

namespace Doctrine\DBAL\Driver\PDOSqlsrv;

/**
 * Sqlsrv Connection implementation.
 *
 * @deprecated Use {@link PDO\SQLSrv\Connection} instead.
 */
class Connection extends \Doctrine\DBAL\Driver\PDO\Connection
{
    /**
     * {@inheritdoc}
     *
     * @internal The connection can be only instantiated by its driver.
     *
     * @param string       $dsn
     * @param string|null  $user
     * @param string|null  $password
     * @param mixed[]|null $options
     */
    public function __construct($dsn, $user = null, $password = null, ?array $options = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function lastInsertId($name = null)
    {
    }
}
