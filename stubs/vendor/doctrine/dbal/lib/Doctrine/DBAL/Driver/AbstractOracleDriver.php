<?php

namespace Doctrine\DBAL\Driver;

/**
 * Abstract base implementation of the {@link Driver} interface for Oracle based drivers.
 */
abstract class AbstractOracleDriver implements \Doctrine\DBAL\Driver, \Doctrine\DBAL\Driver\ExceptionConverterDriver
{
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function convertException($message, \Doctrine\DBAL\Driver\DriverException $exception)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Connection::getDatabase() instead.
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * Returns an appropriate Easy Connect String for the given parameters.
     *
     * @param mixed[] $params The connection parameters to return the Easy Connect String for.
     *
     * @return string
     */
    protected function getEasyConnectString(array $params)
    {
    }
}
