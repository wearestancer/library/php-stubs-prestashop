<?php

namespace Doctrine\DBAL\Driver\PDOIbm;

/**
 * Driver for the PDO IBM extension.
 *
 * @deprecated Use the driver based on the ibm_db2 extension instead.
 */
class Driver extends \Doctrine\DBAL\Driver\AbstractDB2Driver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated
     */
    public function getName()
    {
    }
}
