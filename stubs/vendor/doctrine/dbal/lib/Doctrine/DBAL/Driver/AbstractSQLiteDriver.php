<?php

namespace Doctrine\DBAL\Driver;

/**
 * Abstract base implementation of the {@link Driver} interface for SQLite based drivers.
 */
abstract class AbstractSQLiteDriver implements \Doctrine\DBAL\Driver, \Doctrine\DBAL\Driver\ExceptionConverterDriver
{
    /**
     * {@inheritdoc}
     *
     * @deprecated
     *
     * @link http://www.sqlite.org/c3ref/c_abort.html
     */
    public function convertException($message, \Doctrine\DBAL\Driver\DriverException $exception)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use Connection::getDatabase() instead.
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
    }
}
