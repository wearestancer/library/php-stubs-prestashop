<?php

namespace Doctrine\DBAL\Driver\IBMDB2;

/**
 * IBM DB2 DSN
 */
final class DataSourceName
{
    public function toString() : string
    {
    }
    /**
     * Creates the object from an array representation
     *
     * @param array<string,mixed> $params
     */
    public static function fromArray(array $params) : self
    {
    }
    /**
     * Creates the object from the given DBAL connection parameters.
     *
     * @param array<string,mixed> $params
     */
    public static function fromConnectionParameters(array $params) : self
    {
    }
}
