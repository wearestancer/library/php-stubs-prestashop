<?php

namespace Doctrine\DBAL\Driver;

/**
 * @internal
 */
trait PDOQueryImplementation
{
    /**
     * @return PDOStatement
     */
    #[\ReturnTypeWillChange]
    public function query(?string $query = null, ?int $fetchMode = null, mixed ...$fetchModeArgs)
    {
    }
}
