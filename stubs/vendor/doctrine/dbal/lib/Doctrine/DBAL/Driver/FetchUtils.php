<?php

namespace Doctrine\DBAL\Driver;

/**
 * @internal
 */
final class FetchUtils
{
    /**
     * @return mixed|false
     *
     * @throws Exception
     */
    public static function fetchOne(\Doctrine\DBAL\Driver\Result $result)
    {
    }
    /**
     * @return array<int,array<int,mixed>>
     *
     * @throws Exception
     */
    public static function fetchAllNumeric(\Doctrine\DBAL\Driver\Result $result) : array
    {
    }
    /**
     * @return array<int,array<string,mixed>>
     *
     * @throws Exception
     */
    public static function fetchAllAssociative(\Doctrine\DBAL\Driver\Result $result) : array
    {
    }
    /**
     * @return array<int,mixed>
     *
     * @throws Exception
     */
    public static function fetchFirstColumn(\Doctrine\DBAL\Driver\Result $result) : array
    {
    }
}
