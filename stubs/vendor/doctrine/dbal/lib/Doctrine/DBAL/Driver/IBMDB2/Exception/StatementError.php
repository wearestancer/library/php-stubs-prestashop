<?php

namespace Doctrine\DBAL\Driver\IBMDB2\Exception;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class StatementError extends \Doctrine\DBAL\Driver\AbstractException
{
    /**
     * @param resource $statement
     */
    public static function new($statement) : self
    {
    }
}
