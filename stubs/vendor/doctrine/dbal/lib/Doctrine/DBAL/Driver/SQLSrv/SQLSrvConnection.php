<?php

namespace Doctrine\DBAL\Driver\SQLSrv;

/**
 * SQL Server implementation for the Connection interface.
 *
 * @deprecated Use {@link Connection} instead
 */
class SQLSrvConnection implements \Doctrine\DBAL\Driver\Connection, \Doctrine\DBAL\Driver\ServerInfoAwareConnection
{
    /** @var resource */
    protected $conn;
    /** @var LastInsertId */
    protected $lastInsertId;
    /**
     * @internal The connection can be only instantiated by its driver.
     *
     * @param string  $serverName
     * @param mixed[] $connectionOptions
     *
     * @throws SQLSrvException
     */
    public function __construct($serverName, $connectionOptions)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function prepare($sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function exec($sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function lastInsertId($name = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function commit()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function rollBack()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated The error information is available via exceptions.
     */
    public function errorInfo()
    {
    }
}
