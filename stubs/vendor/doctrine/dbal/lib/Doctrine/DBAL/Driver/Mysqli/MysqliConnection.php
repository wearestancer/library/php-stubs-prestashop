<?php

namespace Doctrine\DBAL\Driver\Mysqli;

/**
 * @deprecated Use {@link Connection} instead
 */
class MysqliConnection implements \Doctrine\DBAL\Driver\Connection, \Doctrine\DBAL\Driver\PingableConnection, \Doctrine\DBAL\Driver\ServerInfoAwareConnection
{
    /**
     * Name of the option to set connection flags
     */
    public const OPTION_FLAGS = 'flags';
    /**
     * @internal The connection can be only instantiated by its driver.
     *
     * @param mixed[] $params
     * @param string  $username
     * @param string  $password
     * @param mixed[] $driverOptions
     *
     * @throws MysqliException
     */
    public function __construct(array $params, $username, $password, array $driverOptions = [])
    {
    }
    /**
     * Retrieves mysqli native resource handle.
     *
     * Could be used if part of your application is not using DBAL.
     *
     * @return mysqli
     */
    public function getWrappedResourceHandle()
    {
    }
    /**
     * {@inheritdoc}
     *
     * The server version detection includes a special case for MariaDB
     * to support '5.5.5-' prefixed versions introduced in Maria 10+
     *
     * @link https://jira.mariadb.org/browse/MDEV-4088
     */
    public function getServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function requiresQueryForServerVersion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prepare($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function query()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function quote($value, $type = \Doctrine\DBAL\ParameterType::STRING)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exec($sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lastInsertId($name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}non-PHPdoc)
     */
    public function rollBack()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     *
     * @return int
     */
    public function errorCode()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated The error information is available via exceptions.
     *
     * @return string
     */
    public function errorInfo()
    {
    }
    /**
     * Pings the server and re-connects when `mysqli.reconnect = 1`
     *
     * @deprecated
     *
     * @return bool
     */
    public function ping()
    {
    }
}
