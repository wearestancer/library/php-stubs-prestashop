<?php

namespace Doctrine\DBAL\Driver;

/**
 * @internal
 */
trait PDOStatementImplementations
{
    /**
     * @deprecated Use one of the fetch- or iterate-related methods.
     *
     * @param int   $mode
     * @param mixed ...$args
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function setFetchMode($mode, ...$args)
    {
    }
    /**
     * @deprecated Use fetchAllNumeric(), fetchAllAssociative() or fetchFirstColumn() instead.
     *
     * @param int|null $mode
     * @param mixed    ...$args
     *
     * @return mixed[]
     */
    #[\ReturnTypeWillChange]
    public function fetchAll($mode = null, ...$args)
    {
    }
}
