<?php

namespace Doctrine\DBAL\Connections;

/**
 * @deprecated Use PrimaryReadReplicaConnection instead
 *
 * @psalm-import-type Params from DriverManager
 */
class MasterSlaveConnection extends \Doctrine\DBAL\Connections\PrimaryReadReplicaConnection
{
    /**
     * Creates Primary Replica Connection.
     *
     * @internal The connection can be only instantiated by the driver manager.
     *
     * @param array<string,mixed> $params
     * @psalm-param Params $params
     * @phpstan-param array<string,mixed> $params
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $params, \Doctrine\DBAL\Driver $driver, ?\Doctrine\DBAL\Configuration $config = null, ?\Doctrine\Common\EventManager $eventManager = null)
    {
    }
    /**
     * Checks if the connection is currently towards the primary or not.
     */
    public function isConnectedToMaster() : bool
    {
    }
    /**
     * @param string|null $connectionName
     *
     * @return bool
     */
    public function connect($connectionName = null)
    {
    }
}
