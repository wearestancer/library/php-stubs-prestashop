<?php

namespace Doctrine\DBAL;

/**
 * @psalm-immutable
 */
class ConnectionException extends \Doctrine\DBAL\Exception
{
    /**
     * @return ConnectionException
     */
    public static function commitFailedRollbackOnly()
    {
    }
    /**
     * @return ConnectionException
     */
    public static function noActiveTransaction()
    {
    }
    /**
     * @return ConnectionException
     */
    public static function savepointsNotSupported()
    {
    }
    /**
     * @return ConnectionException
     */
    public static function mayNotAlterNestedTransactionWithSavepointsInTransaction()
    {
    }
}
