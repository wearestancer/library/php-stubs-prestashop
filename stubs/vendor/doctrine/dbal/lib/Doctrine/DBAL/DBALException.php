<?php

namespace Doctrine\DBAL;

/**
 * @deprecated Use {@link Exception} instead
 *
 * @psalm-immutable
 */
class DBALException extends \Exception
{
    /**
     * @param string $method
     *
     * @return Exception
     */
    public static function notSupported($method)
    {
    }
    /**
     * @deprecated Use {@link invalidPlatformType()} instead.
     */
    public static function invalidPlatformSpecified() : self
    {
    }
    /**
     * @param mixed $invalidPlatform
     */
    public static function invalidPlatformType($invalidPlatform) : self
    {
    }
    /**
     * Returns a new instance for an invalid specified platform version.
     *
     * @param string $version        The invalid platform version given.
     * @param string $expectedFormat The expected platform version format.
     *
     * @return Exception
     */
    public static function invalidPlatformVersionSpecified($version, $expectedFormat)
    {
    }
    /**
     * @deprecated Passing a PDO instance in connection parameters is deprecated.
     *
     * @return Exception
     */
    public static function invalidPdoInstance()
    {
    }
    /**
     * @param string|null $url The URL that was provided in the connection parameters (if any).
     *
     * @return Exception
     */
    public static function driverRequired($url = null)
    {
    }
    /**
     * @param string   $unknownDriverName
     * @param string[] $knownDrivers
     *
     * @return Exception
     */
    public static function unknownDriver($unknownDriverName, array $knownDrivers)
    {
    }
    /**
     * @deprecated
     *
     * @param string  $sql
     * @param mixed[] $params
     *
     * @return Exception
     */
    public static function driverExceptionDuringQuery(\Doctrine\DBAL\Driver $driver, \Throwable $driverEx, $sql, array $params = [])
    {
    }
    /**
     * @deprecated
     *
     * @return Exception
     */
    public static function driverException(\Doctrine\DBAL\Driver $driver, \Throwable $driverEx)
    {
    }
    /**
     * @param string $wrapperClass
     *
     * @return Exception
     */
    public static function invalidWrapperClass($wrapperClass)
    {
    }
    /**
     * @param string $driverClass
     *
     * @return Exception
     */
    public static function invalidDriverClass($driverClass)
    {
    }
    /**
     * @param string $tableName
     *
     * @return Exception
     */
    public static function invalidTableName($tableName)
    {
    }
    /**
     * @param string $tableName
     *
     * @return Exception
     */
    public static function noColumnsSpecifiedForTable($tableName)
    {
    }
    /**
     * @return Exception
     */
    public static function limitOffsetInvalid()
    {
    }
    /**
     * @param string $name
     *
     * @return Exception
     */
    public static function typeExists($name)
    {
    }
    /**
     * @param string $name
     *
     * @return Exception
     */
    public static function unknownColumnType($name)
    {
    }
    /**
     * @param string $name
     *
     * @return Exception
     */
    public static function typeNotFound($name)
    {
    }
    public static function typeNotRegistered(\Doctrine\DBAL\Types\Type $type) : self
    {
    }
    public static function typeAlreadyRegistered(\Doctrine\DBAL\Types\Type $type) : self
    {
    }
}
