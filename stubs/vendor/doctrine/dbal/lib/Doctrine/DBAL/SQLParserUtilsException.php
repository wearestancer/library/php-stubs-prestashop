<?php

namespace Doctrine\DBAL;

/**
 * Doctrine\DBAL\ConnectionException
 *
 * @psalm-immutable
 */
class SQLParserUtilsException extends \Doctrine\DBAL\Exception
{
    /**
     * @param string $paramName
     *
     * @return SQLParserUtilsException
     */
    public static function missingParam($paramName)
    {
    }
    /**
     * @param string $typeName
     *
     * @return SQLParserUtilsException
     */
    public static function missingType($typeName)
    {
    }
}
