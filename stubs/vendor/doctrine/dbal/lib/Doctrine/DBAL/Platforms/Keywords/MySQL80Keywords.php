<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * MySQL 8.0 reserved keywords list.
 */
class MySQL80Keywords extends \Doctrine\DBAL\Platforms\Keywords\MySQL57Keywords
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link https://dev.mysql.com/doc/refman/8.0/en/keywords.html
     */
    protected function getKeywords()
    {
    }
}
