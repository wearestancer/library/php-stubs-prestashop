<?php

namespace Doctrine\DBAL\Platforms;

/**
 * OraclePlatform.
 */
class OraclePlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    /**
     * Assertion for Oracle identifiers.
     *
     * @link http://docs.oracle.com/cd/B19306_01/server.102/b14200/sql_elements008.htm
     *
     * @param string $identifier
     *
     * @return void
     *
     * @throws Exception
     */
    public static function assertValidIdentifier($identifier)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSubstringExpression($string, $start, $length = null)
    {
    }
    /**
     * @param string $type
     *
     * @return string
     */
    public function getNowExpression($type = 'timestamp')
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBitAndComparisonExpression($value1, $value2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBitOrComparisonExpression($value1, $value2)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Need to specifiy minvalue, since start with is hidden in the system and MINVALUE <= START WITH.
     * Therefore we can use MINVALUE to be able to get a hint what START WITH was for later introspection
     * in {@see listSequences()}
     */
    public function getCreateSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSequenceNextValSQL($sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSetTransactionIsolationSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getTransactionIsolationLevelSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListDatabasesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListSequencesSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritDoc}
     *
     * @link http://ezcomponents.org/docs/api/trunk/DatabaseSchema/ezcDbSchemaOracleReader.html
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * @param string $name
     * @param string $table
     * @param int    $start
     *
     * @return string[]
     */
    public function getCreateAutoincrementSql($name, $table, $start = 1)
    {
    }
    /**
     * Returns the SQL statements to drop the autoincrement for the given table name.
     *
     * @param string $table The table name to drop the autoincrement for.
     *
     * @return string[]
     */
    public function getDropAutoincrementSql($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableForeignKeysSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableConstraintsSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropSequenceSQL($sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropForeignKeySQL($foreignKey, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAdvancedForeignKeyOptionsSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForeignKeyReferentialActionSQL($action)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getColumnDeclarationSQL($name, array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getRenameIndexSQL($oldIndexName, \Doctrine\DBAL\Schema\Index $index, $tableName)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated
     */
    public function prefersSequences()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function usesSequenceEmulatedIdentityColumns()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentitySequenceName($tableName, $columnName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsCommentOnStatement()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Oracle returns all column names in SQL result sets in uppercase.
     *
     * @deprecated
     */
    public function getSQLResultCasing($column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateTemporaryTableSnippetSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeFormatString()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated
     */
    public function fixSchemaElementName($schemaElementName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getMaxIdentifierLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsSequences()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated
     */
    public function supportsForeignKeyOnUpdate()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsReleaseSavepoints()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTruncateTableSQL($tableName, $cascade = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDummySelectSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function releaseSavePoint($savepoint)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    public function getListTableCommentsSQL(string $table, ?string $database = null) : string
    {
    }
}
