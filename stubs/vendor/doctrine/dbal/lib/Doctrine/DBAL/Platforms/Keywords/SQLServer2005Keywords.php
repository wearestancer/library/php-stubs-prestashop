<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * Microsoft SQL Server 2005 reserved keyword dictionary.
 *
 * @link    www.doctrine-project.com
 */
class SQLServer2005Keywords extends \Doctrine\DBAL\Platforms\Keywords\SQLServerKeywords
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://msdn.microsoft.com/en-US/library/ms189822%28v=sql.90%29.aspx
     */
    protected function getKeywords()
    {
    }
}
