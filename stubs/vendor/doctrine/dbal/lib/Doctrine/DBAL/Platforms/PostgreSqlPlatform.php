<?php

namespace Doctrine\DBAL\Platforms;

/**
 * PostgreSqlPlatform.
 *
 * @deprecated Use PostgreSQL 9.4 or newer
 *
 * @todo   Rename: PostgreSQLPlatform
 */
class PostgreSqlPlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    /**
     * PostgreSQL has different behavior with some drivers
     * with regard to how booleans have to be handled.
     *
     * Enables use of 'true'/'false' or otherwise 1 and 0 instead.
     *
     * @param bool $flag
     *
     * @return void
     */
    public function setUseBooleanTrueFalseStrings($flag)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSubstringExpression($string, $start, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getNowExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getRegexpExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsSequences()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsSchemas()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultSchemaName()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsPartialIndexes()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function usesSequenceEmulatedIdentityColumns()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentitySequenceName($tableName, $columnName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsCommentOnStatement()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated
     */
    public function prefersSequences()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasNativeGuidType()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListDatabasesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListNamespacesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListSequencesSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * @param string      $table
     * @param string|null $database
     *
     * @return string
     */
    public function getListTableForeignKeysSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableConstraintsSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @link http://ezcomponents.org/docs/api/trunk/DatabaseSchema/ezcDbSchemaPgsqlReader.html
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateDatabaseSQL($name)
    {
    }
    /**
     * Returns the SQL statement for disallowing new connections on the given database.
     *
     * This is useful to force DROP DATABASE operations which could fail because of active connections.
     *
     * @deprecated
     *
     * @param string $database The name of the database to disallow new connections for.
     *
     * @return string
     */
    public function getDisallowDatabaseConnectionsSQL($database)
    {
    }
    /**
     * Returns the SQL statement for closing currently active connections on the given database.
     *
     * This is useful to force DROP DATABASE operations which could fail because of active connections.
     *
     * @deprecated
     *
     * @param string $database The name of the database to close currently active connections for.
     *
     * @return string
     */
    public function getCloseActiveDatabaseConnectionsSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAdvancedForeignKeyOptionsSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getRenameIndexSQL($oldIndexName, \Doctrine\DBAL\Schema\Index $index, $tableName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCommentOnColumnSQL($tableName, $columnName, $comment)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropSequenceSQL($sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateSchemaSQL($schemaName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropForeignKeySQL($foreignKey, $table)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritDoc}
     *
     * Postgres wants boolean values converted to the strings 'true'/'false'.
     */
    public function convertBooleans($item)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function convertBooleansToDatabaseValue($item)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function convertFromBoolean($item)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSequenceNextValSQL($sequence)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSetTransactionIsolationSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getGuidTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     *
     * PostgreSQL returns all column names in SQL result sets in lowercase.
     *
     * @deprecated
     */
    public function getSQLResultCasing($column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getEmptyIdentityInsertSQL($quotedTableName, $quotedIdentifierColumnName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTruncateTableSQL($tableName, $cascade = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getReadLockSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getVarcharMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryDefaultLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultValueDeclarationSQL($column)
    {
    }
    public function getListTableMetadataSQL(string $table, ?string $schema = null) : string
    {
    }
}
