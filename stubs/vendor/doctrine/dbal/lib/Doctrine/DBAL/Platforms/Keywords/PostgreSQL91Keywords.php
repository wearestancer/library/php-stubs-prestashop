<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * PostgreSQL 9.1 reserved keywords list.
 */
class PostgreSQL91Keywords extends \Doctrine\DBAL\Platforms\Keywords\PostgreSQLKeywords
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://www.postgresql.org/docs/9.1/static/sql-keywords-appendix.html
     */
    protected function getKeywords()
    {
    }
}
