<?php

namespace Doctrine\DBAL\Platforms;

/**
 * The SQLServerPlatform provides the behavior, features and SQL dialect of the
 * Microsoft SQL Server database platform.
 *
 * @deprecated Use SQL Server 2012 or newer
 */
class SQLServerPlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    /**
     * {@inheritdoc}
     */
    public function getCurrentDateSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentTimeSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Microsoft SQL Server prefers "autoincrement" identity columns
     * since sequences can only be emulated with a table.
     */
    public function prefersIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     *
     * Microsoft SQL Server supports this through AUTO_INCREMENT columns.
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsReleaseSavepoints()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsSchemas()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultSchemaName()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsColumnCollation()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasNativeGuidType()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsCreateDropDatabase()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateSchemaSQL($schemaName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropForeignKeySQL($foreignKey, $table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropIndexSQL($index, $table = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreatePrimaryKeySQL(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * Returns the SQL statement for creating a column comment.
     *
     * SQL Server does not support native column comments,
     * therefore the extended properties functionality is used
     * as a workaround to store them.
     * The property name used to store column comments is "MS_Description"
     * which provides compatibility with SQL Server Management Studio,
     * as column comments are stored in the same property there when
     * specifying a column's "Description" attribute.
     *
     * @param string      $tableName  The quoted table name to which the column belongs.
     * @param string      $columnName The quoted column name to create the comment for.
     * @param string|null $comment    The column's comment.
     *
     * @return string
     */
    protected function getCreateColumnCommentSQL($tableName, $columnName, $comment)
    {
    }
    /**
     * Returns the SQL snippet for declaring a default constraint.
     *
     * @param string  $table  Name of the table to return the default constraint declaration for.
     * @param mixed[] $column Column definition.
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function getDefaultConstraintDeclarationSQL($table, array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getUniqueConstraintDeclarationSQL($name, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateIndexSQL(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getCreateIndexSQLFlags(\Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * Returns the SQL statement for altering a column comment.
     *
     * SQL Server does not support native column comments,
     * therefore the extended properties functionality is used
     * as a workaround to store them.
     * The property name used to store column comments is "MS_Description"
     * which provides compatibility with SQL Server Management Studio,
     * as column comments are stored in the same property there when
     * specifying a column's "Description" attribute.
     *
     * @param string      $tableName  The quoted table name to which the column belongs.
     * @param string      $columnName The quoted column name to alter the comment for.
     * @param string|null $comment    The column's comment.
     *
     * @return string
     */
    protected function getAlterColumnCommentSQL($tableName, $columnName, $comment)
    {
    }
    /**
     * Returns the SQL statement for dropping a column comment.
     *
     * SQL Server does not support native column comments,
     * therefore the extended properties functionality is used
     * as a workaround to store them.
     * The property name used to store column comments is "MS_Description"
     * which provides compatibility with SQL Server Management Studio,
     * as column comments are stored in the same property there when
     * specifying a column's "Description" attribute.
     *
     * @param string $tableName  The quoted table name to which the column belongs.
     * @param string $columnName The quoted column name to drop the comment for.
     *
     * @return string
     */
    protected function getDropColumnCommentSQL($tableName, $columnName)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getRenameIndexSQL($oldIndexName, \Doctrine\DBAL\Schema\Index $index, $tableName)
    {
    }
    /**
     * Returns the SQL statement for adding an extended property to a database object.
     *
     * @link http://msdn.microsoft.com/en-us/library/ms180047%28v=sql.90%29.aspx
     *
     * @param string      $name       The name of the property to add.
     * @param string|null $value      The value of the property to add.
     * @param string|null $level0Type The type of the object at level 0 the property belongs to.
     * @param string|null $level0Name The name of the object at level 0 the property belongs to.
     * @param string|null $level1Type The type of the object at level 1 the property belongs to.
     * @param string|null $level1Name The name of the object at level 1 the property belongs to.
     * @param string|null $level2Type The type of the object at level 2 the property belongs to.
     * @param string|null $level2Name The name of the object at level 2 the property belongs to.
     *
     * @return string
     */
    public function getAddExtendedPropertySQL($name, $value = null, $level0Type = null, $level0Name = null, $level1Type = null, $level1Name = null, $level2Type = null, $level2Name = null)
    {
    }
    /**
     * Returns the SQL statement for dropping an extended property from a database object.
     *
     * @link http://technet.microsoft.com/en-gb/library/ms178595%28v=sql.90%29.aspx
     *
     * @param string      $name       The name of the property to drop.
     * @param string|null $level0Type The type of the object at level 0 the property belongs to.
     * @param string|null $level0Name The name of the object at level 0 the property belongs to.
     * @param string|null $level1Type The type of the object at level 1 the property belongs to.
     * @param string|null $level1Name The name of the object at level 1 the property belongs to.
     * @param string|null $level2Type The type of the object at level 2 the property belongs to.
     * @param string|null $level2Name The name of the object at level 2 the property belongs to.
     *
     * @return string
     */
    public function getDropExtendedPropertySQL($name, $level0Type = null, $level0Name = null, $level1Type = null, $level1Name = null, $level2Type = null, $level2Name = null)
    {
    }
    /**
     * Returns the SQL statement for updating an extended property of a database object.
     *
     * @link http://msdn.microsoft.com/en-us/library/ms186885%28v=sql.90%29.aspx
     *
     * @param string      $name       The name of the property to update.
     * @param string|null $value      The value of the property to update.
     * @param string|null $level0Type The type of the object at level 0 the property belongs to.
     * @param string|null $level0Name The name of the object at level 0 the property belongs to.
     * @param string|null $level1Type The type of the object at level 1 the property belongs to.
     * @param string|null $level1Name The name of the object at level 1 the property belongs to.
     * @param string|null $level2Type The type of the object at level 2 the property belongs to.
     * @param string|null $level2Name The name of the object at level 2 the property belongs to.
     *
     * @return string
     */
    public function getUpdateExtendedPropertySQL($name, $value = null, $level0Type = null, $level0Name = null, $level1Type = null, $level1Name = null, $level2Type = null, $level2Name = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getEmptyIdentityInsertSQL($quotedTableName, $quotedIdentifierColumnName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * @param string      $table
     * @param string|null $database
     *
     * @return string
     */
    public function getListTableForeignKeysSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getModExpression($expression1, $expression2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTrimExpression($str, $mode = \Doctrine\DBAL\Platforms\TrimMode::UNSPECIFIED, $char = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getConcatExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListDatabasesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListNamespacesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSubstringExpression($string, $start, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLengthExpression($column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSetTransactionIsolationSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getGuidTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAsciiStringTypeDeclarationSQL(array $column) : string
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsLimitOffset()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function convertBooleans($item)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateTemporaryTableSnippetSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTemporaryTableName($tableName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function createSavePoint($savepoint)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function releaseSavePoint($savepoint)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function rollbackSavePoint($savepoint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForeignKeyReferentialActionSQL($action)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function appendLockHint($fromClause, $lockMode)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getForUpdateSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function quoteSingleIdentifier($str)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTruncateTableSQL($tableName, $cascade = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     *
     * Modifies column declaration order as it differs in Microsoft SQL Server.
     */
    public function getColumnDeclarationSQL($name, array $column)
    {
    }
    protected function getCommentOnTableSQL(string $tableName, ?string $comment) : string
    {
    }
    public function getListTableMetadataSQL(string $table) : string
    {
    }
}
