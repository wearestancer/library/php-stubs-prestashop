<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * SAP Sybase SQL Anywhere 10 reserved keywords list.
 */
class SQLAnywhereKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://infocenter.sybase.com/help/topic/com.sybase.dbrfen10/pdf/dbrfen10.pdf?noframes=true
     */
    protected function getKeywords()
    {
    }
}
