<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * PostgreSQL 10.0 reserved keywords list.
 */
class PostgreSQL100Keywords extends \Doctrine\DBAL\Platforms\Keywords\PostgreSQL94Keywords
{
    public function getName() : string
    {
    }
}
