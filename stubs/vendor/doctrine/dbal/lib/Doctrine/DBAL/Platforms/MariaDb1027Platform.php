<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Provides the behavior, features and SQL dialect of the MariaDB 10.2 (10.2.7 GA) database platform.
 *
 * Note: Should not be used with versions prior to 10.2.7.
 */
class MariaDb1027Platform extends \Doctrine\DBAL\Platforms\MySqlPlatform
{
    /**
     * {@inheritdoc}
     *
     * @link https://mariadb.com/kb/en/library/json-data-type/
     */
    public function getJsonTypeDeclarationSQL(array $column) : string
    {
    }
    protected function getReservedKeywordsClass() : string
    {
    }
    protected function initializeDoctrineTypeMappings() : void
    {
    }
}
