<?php

namespace Doctrine\DBAL\Platforms;

/**
 * The MySqlPlatform provides the behavior, features and SQL dialect of the
 * MySQL database platform. This platform represents a MySQL 5.0 or greater platform that
 * uses the InnoDB storage engine.
 *
 * @todo   Rename: MySQLPlatform
 */
class MySqlPlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    public const LENGTH_LIMIT_TINYTEXT = 255;
    public const LENGTH_LIMIT_TEXT = 65535;
    public const LENGTH_LIMIT_MEDIUMTEXT = 16777215;
    public const LENGTH_LIMIT_TINYBLOB = 255;
    public const LENGTH_LIMIT_BLOB = 65535;
    public const LENGTH_LIMIT_MEDIUMBLOB = 16777215;
    /**
     * {@inheritDoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIdentifierQuoteCharacter()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getRegexpExpression()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getConcatExpression()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListDatabasesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableConstraintsSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Two approaches to listing the table indexes. The information_schema is
     * preferred, because it doesn't cause problems with SQL keywords such as "order" or "table".
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * @param string      $table
     * @param string|null $database
     *
     * @return string
     */
    public function getListTableForeignKeysSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * Gets the SQL snippet used to declare a CLOB column type.
     *     TINYTEXT   : 2 ^  8 - 1 = 255
     *     TEXT       : 2 ^ 16 - 1 = 65535
     *     MEDIUMTEXT : 2 ^ 24 - 1 = 16777215
     *     LONGTEXT   : 2 ^ 32 - 1 = 4294967295
     *
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * Obtain DBMS specific SQL code portion needed to set the COLLATION
     * of a column declaration to be used in statements like CREATE TABLE.
     *
     * @deprecated Deprecated since version 2.5, Use {@link self::getColumnCollationDeclarationSQL()} instead.
     *
     * @param string $collation name of the collation
     *
     * @return string  DBMS specific SQL code portion needed to set the COLLATION
     *                 of a column declaration.
     */
    public function getCollationFieldDeclaration($collation)
    {
    }
    /**
     * {@inheritDoc}
     *
     * MySql prefers "autoincrement" identity columns since sequences can only
     * be emulated with a table.
     */
    public function prefersIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     *
     * MySql supports this through AUTO_INCREMENT columns.
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsInlineColumnComments()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsColumnCollation()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    public function getListTableMetadataSQL(string $table, ?string $database = null) : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultValueDeclarationSQL($column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getPreAlterTableIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * @param TableDiff $diff The table diff to gather the SQL for.
     *
     * @return string[]
     */
    protected function getPreAlterTableRenameIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getPostAlterTableIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * @param TableDiff $diff The table diff to gather the SQL for.
     *
     * @return string[]
     */
    protected function getPostAlterTableRenameIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getCreateIndexSQLFlags(\Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFloatDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDecimalTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getColumnCharsetDeclarationSQL($charset)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getColumnCollationDeclarationSQL($collation)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAdvancedForeignKeyOptionsSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropIndexSQL($index, $table = null)
    {
    }
    /**
     * @param string $table
     *
     * @return string
     */
    protected function getDropPrimaryKeySQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSetTransactionIsolationSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getReadLockSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getVarcharMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     *
     * MySQL commits a transaction implicitly when DROP TABLE is executed, however not
     * if DROP TEMPORARY TABLE is executed.
     */
    public function getDropTemporaryTableSQL($table)
    {
    }
    /**
     * Gets the SQL Snippet used to declare a BLOB column type.
     *     TINYBLOB   : 2 ^  8 - 1 = 255
     *     BLOB       : 2 ^ 16 - 1 = 65535
     *     MEDIUMBLOB : 2 ^ 24 - 1 = 16777215
     *     LONGBLOB   : 2 ^ 32 - 1 = 4294967295
     *
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function quoteStringLiteral($str)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultTransactionIsolationLevel()
    {
    }
    public function supportsColumnLengthIndexes() : bool
    {
    }
}
