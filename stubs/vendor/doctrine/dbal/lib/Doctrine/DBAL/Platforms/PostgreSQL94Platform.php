<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Provides the behavior, features and SQL dialect of the PostgreSQL 9.4 database platform.
 */
class PostgreSQL94Platform extends \Doctrine\DBAL\Platforms\PostgreSQL92Platform
{
    /**
     * {@inheritdoc}
     */
    public function getJsonTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
}
