<?php

namespace Doctrine\DBAL\Platforms;

/**
 * The SQLAnywhere12Platform provides the behavior, features and SQL dialect of the
 * SAP Sybase SQL Anywhere 12 database platform.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 */
class SQLAnywhere12Platform extends \Doctrine\DBAL\Platforms\SQLAnywhere11Platform
{
    /**
     * {@inheritdoc}
     */
    public function getCreateSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlterSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateTimeTzFormatString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateTimeTzTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropSequenceSQL($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListSequencesSQL($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSequenceNextValSQL($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsSequences()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getAdvancedIndexOptionsSQL(\Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
}
