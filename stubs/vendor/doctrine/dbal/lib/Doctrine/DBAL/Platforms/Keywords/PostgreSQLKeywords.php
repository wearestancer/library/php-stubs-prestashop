<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * PostgreSQL Keywordlist.
 */
class PostgreSQLKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKeywords()
    {
    }
}
