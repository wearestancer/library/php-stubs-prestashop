<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Provides the behavior, features and SQL dialect of the PostgreSQL 9.2 database platform.
 *
 * @deprecated Use PostgreSQL 9.4 or newer
 */
class PostgreSQL92Platform extends \Doctrine\DBAL\Platforms\PostgreSQL91Platform
{
    /**
     * {@inheritdoc}
     */
    public function getJsonTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasNativeJsonType()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCloseActiveDatabaseConnectionsSQL($database)
    {
    }
}
