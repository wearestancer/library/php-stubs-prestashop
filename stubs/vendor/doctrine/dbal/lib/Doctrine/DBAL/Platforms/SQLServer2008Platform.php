<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Platform to ensure compatibility of Doctrine with Microsoft SQL Server 2008 version.
 *
 * Differences to SQL Server 2005 and before are that a new DATETIME2 type was
 * introduced that has a higher precision.
 *
 * @deprecated Use SQL Server 2012 or newer
 */
class SQLServer2008Platform extends \Doctrine\DBAL\Platforms\SQLServer2005Platform
{
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTzFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateFormatString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeFormatString()
    {
    }
    /**
     * {@inheritDoc}
     *
     * Adding Datetime2 Type
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritdoc}
     *
     * Returns Microsoft SQL Server 2008 specific keywords class
     */
    protected function getReservedKeywordsClass()
    {
    }
    protected function getLikeWildcardCharacters() : string
    {
    }
}
