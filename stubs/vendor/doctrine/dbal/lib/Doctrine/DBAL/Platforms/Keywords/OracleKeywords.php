<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * Oracle Keywordlist.
 */
class OracleKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKeywords()
    {
    }
}
