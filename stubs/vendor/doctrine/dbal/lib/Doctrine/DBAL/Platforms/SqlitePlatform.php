<?php

namespace Doctrine\DBAL\Platforms;

/**
 * The SqlitePlatform class describes the specifics and dialects of the SQLite
 * database platform.
 *
 * @todo   Rename: SQLitePlatform
 */
class SqlitePlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    /**
     * {@inheritDoc}
     */
    public function getRegexpExpression()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * @param string $type
     *
     * @return string
     */
    public function getNowExpression($type = 'timestamp')
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTrimExpression($str, $mode = \Doctrine\DBAL\Platforms\TrimMode::UNSPECIFIED, $char = false)
    {
    }
    /**
     * {@inheritDoc}
     *
     * SQLite only supports the 2 parameter variant of this function
     */
    public function getSubstringExpression($string, $start, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getTransactionIsolationLevelSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSetTransactionIsolationSQL($level)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function prefersIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * @param array<string, mixed> $column
     *
     * @return string
     */
    public function getTinyIntTypeDeclarationSql(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * @param array<string, mixed> $column
     *
     * @return string
     */
    public function getMediumIntTypeDeclarationSql(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getForeignKeyDeclarationSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryDefaultLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableConstraintsSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAdvancedForeignKeyOptionsSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsColumnCollation()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsInlineColumnComments()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTruncateTableSQL($tableName, $cascade = false)
    {
    }
    /**
     * User-defined function for Sqlite that is used with PDO::sqliteCreateFunction().
     *
     * @param int|float $value
     *
     * @return float
     */
    public static function udfSqrt($value)
    {
    }
    /**
     * User-defined function for Sqlite that implements MOD(a, b).
     *
     * @param int $a
     * @param int $b
     *
     * @return int
     */
    public static function udfMod($a, $b)
    {
    }
    /**
     * @param string $str
     * @param string $substr
     * @param int    $offset
     *
     * @return int
     */
    public static function udfLocate($str, $substr, $offset = 0)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getForUpdateSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getInlineColumnCommentSQL($comment)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getPreAlterTableIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getPostAlterTableIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTemporaryTableName($tableName)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Sqlite Platform emulates schema by underscoring each dot and generating tables
     * into the default database.
     *
     * This hack is implemented to be able to use SQLite as testdriver when
     * using schema supporting databases.
     */
    public function canEmulateSchemas()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsForeignKeyConstraints()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreatePrimaryKeySQL(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateForeignKeySQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropForeignKeySQL($foreignKey, $table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateConstraintSQL(\Doctrine\DBAL\Schema\Constraint $constraint, $table)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @param int|null $createFlags
     */
    public function getCreateTableSQL(\Doctrine\DBAL\Schema\Table $table, $createFlags = null)
    {
    }
    /**
     * @param string      $table
     * @param string|null $database
     *
     * @return string
     */
    public function getListTableForeignKeysSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
}
