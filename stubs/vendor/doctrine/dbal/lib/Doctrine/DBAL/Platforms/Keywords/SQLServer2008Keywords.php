<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * Microsoft SQL Server 2008 reserved keyword dictionary.
 *
 * @link    www.doctrine-project.com
 */
class SQLServer2008Keywords extends \Doctrine\DBAL\Platforms\Keywords\SQLServer2005Keywords
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://msdn.microsoft.com/en-us/library/ms189822%28v=sql.100%29.aspx
     */
    protected function getKeywords()
    {
    }
}
