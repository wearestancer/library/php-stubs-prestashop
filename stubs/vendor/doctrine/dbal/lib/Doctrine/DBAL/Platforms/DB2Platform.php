<?php

namespace Doctrine\DBAL\Platforms;

class DB2Platform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    public function getCharMaxLength() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryDefaultLength()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getVarcharTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isCommentedDoctrineType(\Doctrine\DBAL\Types\Type $doctrineType)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBitAndComparisonExpression($value1, $value2)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBitOrComparisonExpression($value1, $value2)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTruncateTableSQL($tableName, $cascade = false)
    {
    }
    /**
     * This code fragment is originally from the Zend_Db_Adapter_Db2 class, but has been edited.
     *
     * @param string $table
     * @param string $database
     *
     * @return string
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableForeignKeysSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsCreateDropDatabase()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsReleaseSavepoints()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsCommentOnStatement()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCurrentDateSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCurrentTimeSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCurrentTimestampSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIndexDeclarationSQL($name, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getPreAlterTableIndexForeignKeySQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getRenameIndexSQL($oldIndexName, \Doctrine\DBAL\Schema\Index $index, $tableName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDefaultValueDeclarationSQL($column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getEmptyIdentityInsertSQL($quotedTableName, $quotedIdentifierColumnName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateTemporaryTableSnippetSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTemporaryTableName($tableName)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSubstringExpression($string, $start, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function prefersIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     *
     * DB2 returns all column names in SQL result sets in uppercase.
     *
     * @deprecated
     */
    public function getSQLResultCasing($column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getForUpdateSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDummySelectSQL()
    {
    }
    /**
     * {@inheritDoc}
     *
     * DB2 supports savepoints, but they work semantically different than on other vendor platforms.
     *
     * TODO: We have to investigate how to get DB2 up and running with savepoints.
     */
    public function supportsSavepoints()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    public function getListTableCommentsSQL(string $table) : string
    {
    }
}
