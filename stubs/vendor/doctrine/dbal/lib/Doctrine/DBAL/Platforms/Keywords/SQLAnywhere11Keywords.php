<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * SAP Sybase SQL Anywhere 11 reserved keywords list.
 */
class SQLAnywhere11Keywords extends \Doctrine\DBAL\Platforms\Keywords\SQLAnywhereKeywords
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://dcx.sybase.com/1100/en/dbreference_en11/alhakeywords.html
     */
    protected function getKeywords()
    {
    }
}
