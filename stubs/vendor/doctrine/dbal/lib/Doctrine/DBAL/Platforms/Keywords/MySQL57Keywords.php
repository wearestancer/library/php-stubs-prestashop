<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * MySQL 5.7 reserved keywords list.
 */
class MySQL57Keywords extends \Doctrine\DBAL\Platforms\Keywords\MySQLKeywords
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://dev.mysql.com/doc/mysqld-version-reference/en/mysqld-version-reference-reservedwords-5-7.html
     */
    protected function getKeywords()
    {
    }
}
