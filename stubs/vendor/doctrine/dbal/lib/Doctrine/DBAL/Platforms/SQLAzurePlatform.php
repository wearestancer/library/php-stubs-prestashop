<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Platform to ensure compatibility of Doctrine with SQL Azure
 *
 * On top of SQL Server 2008 the following functionality is added:
 *
 * - Create tables with the FEDERATED ON syntax.
 *
 * @deprecated
 */
class SQLAzurePlatform extends \Doctrine\DBAL\Platforms\SQLServer2008Platform
{
    /**
     * {@inheritDoc}
     */
    public function getCreateTableSQL(\Doctrine\DBAL\Schema\Table $table, $createFlags = self::CREATE_INDEXES)
    {
    }
}
