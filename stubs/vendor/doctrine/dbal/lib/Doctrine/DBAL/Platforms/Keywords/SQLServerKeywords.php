<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * Microsoft SQL Server 2000 reserved keyword dictionary.
 *
 * @link    www.doctrine-project.com
 */
class SQLServerKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @link http://msdn.microsoft.com/en-us/library/aa238507%28v=sql.80%29.aspx
     */
    protected function getKeywords()
    {
    }
}
