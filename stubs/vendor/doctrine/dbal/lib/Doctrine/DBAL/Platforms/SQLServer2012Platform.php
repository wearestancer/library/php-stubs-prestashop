<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Platform to ensure compatibility of Doctrine with Microsoft SQL Server 2012 version.
 *
 * Differences to SQL Server 2008 and before are that sequences are introduced,
 * and support for the new OFFSET... FETCH syntax for result pagination has been added.
 */
class SQLServer2012Platform extends \Doctrine\DBAL\Platforms\SQLServer2008Platform
{
    /**
     * {@inheritdoc}
     */
    public function getAlterSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateSequenceSQL(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropSequenceSQL($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListSequencesSQL($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSequenceNextValSQL($sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsSequences()
    {
    }
    /**
     * {@inheritdoc}
     *
     * Returns Microsoft SQL Server 2012 specific keywords class
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset = null)
    {
    }
}
