<?php

namespace Doctrine\DBAL\Platforms\Keywords;

class ReservedKeywordsValidator implements \Doctrine\DBAL\Schema\Visitor\Visitor
{
    /**
     * @param KeywordList[] $keywordLists
     */
    public function __construct(array $keywordLists)
    {
    }
    /**
     * @return string[]
     */
    public function getViolations()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptColumn(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Column $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptIndex(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSchema(\Doctrine\DBAL\Schema\Schema $schema)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
    }
}
