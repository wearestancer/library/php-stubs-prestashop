<?php

namespace Doctrine\DBAL\Platforms;

/**
 * The SQLAnywherePlatform provides the behavior, features and SQL dialect of the
 * SAP Sybase SQL Anywhere 10 database platform.
 *
 * @deprecated Support for SQLAnywhere will be removed in 3.0.
 */
class SQLAnywherePlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    public const FOREIGN_KEY_MATCH_SIMPLE = 1;
    public const FOREIGN_KEY_MATCH_FULL = 2;
    public const FOREIGN_KEY_MATCH_SIMPLE_UNIQUE = 129;
    public const FOREIGN_KEY_MATCH_FULL_UNIQUE = 130;
    /**
     * {@inheritdoc}
     */
    public function appendLockHint($fromClause, $lockMode)
    {
    }
    /**
     * {@inheritdoc}
     *
     * SQL Anywhere supports a maximum length of 128 bytes for identifiers.
     */
    public function fixSchemaElementName($schemaElementName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAdvancedForeignKeyOptionsSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * Returns the SQL clause for creating a column in a table alteration.
     *
     * @param Column $column The column to add.
     *
     * @return string
     */
    protected function getAlterTableAddColumnClause(\Doctrine\DBAL\Schema\Column $column)
    {
    }
    /**
     * Returns the SQL clause for altering a table.
     *
     * @param Identifier $tableName The quoted name of the table to alter.
     *
     * @return string
     */
    protected function getAlterTableClause(\Doctrine\DBAL\Schema\Identifier $tableName)
    {
    }
    /**
     * Returns the SQL clause for dropping a column in a table alteration.
     *
     * @param Column $column The column to drop.
     *
     * @return string
     */
    protected function getAlterTableRemoveColumnClause(\Doctrine\DBAL\Schema\Column $column)
    {
    }
    /**
     * Returns the SQL clause for renaming a column in a table alteration.
     *
     * @param string $oldColumnName The quoted name of the column to rename.
     * @param Column $column        The column to rename to.
     *
     * @return string
     */
    protected function getAlterTableRenameColumnClause($oldColumnName, \Doctrine\DBAL\Schema\Column $column)
    {
    }
    /**
     * Returns the SQL clause for renaming a table in a table alteration.
     *
     * @param Identifier $newTableName The quoted name of the table to rename to.
     *
     * @return string
     */
    protected function getAlterTableRenameTableClause(\Doctrine\DBAL\Schema\Identifier $newTableName)
    {
    }
    /**
     * Returns the SQL clause for altering a column in a table alteration.
     *
     * This method returns null in case that only the column comment has changed.
     * Changes in column comments have to be handled differently.
     *
     * @param ColumnDiff $columnDiff The diff of the column to alter.
     *
     * @return string|null
     */
    protected function getAlterTableChangeColumnClause(\Doctrine\DBAL\Schema\ColumnDiff $columnDiff)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryDefaultLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBinaryMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     *
     * BIT type columns require an explicit NULL declaration
     * in SQL Anywhere if they shall be nullable.
     * Otherwise by just omitting the NOT NULL clause,
     * SQL Anywhere will declare them NOT NULL nonetheless.
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCommentOnColumnSQL($tableName, $columnName, $comment)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConcatExpression()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateConstraintSQL(\Doctrine\DBAL\Schema\Constraint $constraint, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateDatabaseSQL($name)
    {
    }
    /**
     * {@inheritdoc}
     *
     * Appends SQL Anywhere specific flags if given.
     */
    public function getCreateIndexSQL(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreatePrimaryKeySQL(\Doctrine\DBAL\Schema\Index $index, $table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateTemporaryTableSnippetSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCreateViewSQL($name, $sql)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentDateSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentTimeSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentTimestampSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateTimeFormatString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateTimeTzFormatString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultTransactionIsolationLevel()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropDatabaseSQL($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropIndexSQL($index, $table = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDropViewSQL($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForeignKeyBaseDeclarationSQL(\Doctrine\DBAL\Schema\ForeignKeyConstraint $foreignKey)
    {
    }
    /**
     * Returns foreign key MATCH clause for given type.
     *
     * @param int $type The foreign key match type
     *
     * @return string
     *
     * @throws InvalidArgumentException If unknown match type given.
     */
    public function getForeignKeyMatchClauseSQL($type)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForeignKeyReferentialActionSQL($action)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForUpdateSQL()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getGuidTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIndexDeclarationSQL($name, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListDatabasesSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @todo Where is this used? Which information should be retrieved?
     */
    public function getListTableConstraintsSQL($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListTableForeignKeysSQL($table)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @todo Where is this used? Which information should be retrieved?
     */
    public function getListUsersSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListViewsSQL($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMaxIdentifierLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMd5Expression($column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * Obtain DBMS specific SQL code portion needed to set a primary key
     * declaration to be used in statements like ALTER TABLE.
     *
     * @param Index  $index Index definition
     * @param string $name  Name of the primary key
     *
     * @return string DBMS specific SQL code portion needed to set a primary key
     *
     * @throws InvalidArgumentException If the given index is not a primary key.
     */
    public function getPrimaryKeyDeclarationSQL(\Doctrine\DBAL\Schema\Index $index, $name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSetTransactionIsolationSQL($level)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * Returns the SQL statement for starting an existing database.
     *
     * In SQL Anywhere you can start and stop databases on a
     * database server instance.
     * This is a required statement after having created a new database
     * as it has to be explicitly started to be usable.
     * SQL Anywhere does not automatically start a database after creation!
     *
     * @param string $database Name of the database to start.
     *
     * @return string
     */
    public function getStartDatabaseSQL($database)
    {
    }
    /**
     * Returns the SQL statement for stopping a running database.
     *
     * In SQL Anywhere you can start and stop databases on a
     * database server instance.
     * This is a required statement before dropping an existing database
     * as it has to be explicitly stopped before it can be dropped.
     *
     * @param string $database Name of the database to stop.
     *
     * @return string
     */
    public function getStopDatabaseSQL($database)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubstringExpression($string, $start, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTemporaryTableSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTimeFormatString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTrimExpression($str, $mode = \Doctrine\DBAL\Platforms\TrimMode::UNSPECIFIED, $char = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTruncateTableSQL($tableName, $cascade = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUniqueConstraintDeclarationSQL($name, \Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getVarcharDefaultLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getVarcharMaxLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasNativeGuidType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prefersIdentityColumns()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsCommentOnStatement()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function _getTransactionIsolationLevelSQL($level)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset)
    {
    }
    /**
     * Return the INDEX query section dealing with non-standard
     * SQL Anywhere options.
     *
     * @param Index $index Index definition
     *
     * @return string
     */
    protected function getAdvancedIndexOptionsSQL(\Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * Returns the SQL snippet for creating a table constraint.
     *
     * @param Constraint  $constraint The table constraint to create the SQL snippet for.
     * @param string|null $name       The table constraint name to use if any.
     *
     * @return string
     *
     * @throws InvalidArgumentException If the given table constraint type is not supported by this method.
     */
    protected function getTableConstraintDeclarationSQL(\Doctrine\DBAL\Schema\Constraint $constraint, $name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getCreateIndexSQLFlags(\Doctrine\DBAL\Schema\Index $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getRenameIndexSQL($oldIndexName, \Doctrine\DBAL\Schema\Index $index, $tableName)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
}
