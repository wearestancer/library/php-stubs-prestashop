<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * SQLite Keywordlist.
 */
class SQLiteKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKeywords()
    {
    }
}
