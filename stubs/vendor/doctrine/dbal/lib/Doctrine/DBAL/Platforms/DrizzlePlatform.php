<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Drizzle platform
 *
 * @deprecated
 */
class DrizzlePlatform extends \Doctrine\DBAL\Platforms\AbstractPlatform
{
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIdentifierQuoteCharacter()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getConcatExpression()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateDiffExpression($date1, $date2)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCreateDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropDatabaseSQL($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = [])
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListDatabasesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
    }
    /**
     * @param string      $table
     * @param string|null $database
     *
     * @return string
     */
    public function getListTableForeignKeysSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListTableIndexesSQL($table, $database = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function prefersIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsIdentityColumns()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsInlineColumnComments()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supportsViews()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsColumnCollation()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropIndexSQL($index, $table = null)
    {
    }
    /**
     * @param string $table
     *
     * @return string
     */
    protected function getDropPrimaryKeySQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $column)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAlterTableSQL(\Doctrine\DBAL\Schema\TableDiff $diff)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getDropTemporaryTableSQL($table)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function convertBooleans($item)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getLocateExpression($str, $substr, $startPos = false)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @deprecated Use application-generated UUIDs instead
     */
    public function getGuidExpression()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getRegexpExpression()
    {
    }
}
