<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * MySQL Keywordlist.
 */
class MySQLKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKeywords()
    {
    }
}
