<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * DB2 Keywords.
 */
class DB2Keywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKeywords()
    {
    }
}
