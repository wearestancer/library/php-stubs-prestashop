<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * Drizzle Keywordlist.
 */
class DrizzleKeywords extends \Doctrine\DBAL\Platforms\Keywords\KeywordList
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKeywords()
    {
    }
}
