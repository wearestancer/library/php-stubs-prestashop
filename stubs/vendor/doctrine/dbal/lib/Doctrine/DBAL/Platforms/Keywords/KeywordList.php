<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * Abstract interface for a SQL reserved keyword dictionary.
 *
 * @psalm-consistent-constructor
 */
abstract class KeywordList
{
    /**
     * Checks if the given word is a keyword of this dialect/vendor platform.
     *
     * @param string $word
     *
     * @return bool
     */
    public function isKeyword($word)
    {
    }
    /**
     * @return void
     */
    protected function initializeKeywords()
    {
    }
    /**
     * Returns the list of keywords.
     *
     * @return string[]
     */
    protected abstract function getKeywords();
    /**
     * Returns the name of this keyword list.
     *
     * @return string
     */
    public abstract function getName();
}
