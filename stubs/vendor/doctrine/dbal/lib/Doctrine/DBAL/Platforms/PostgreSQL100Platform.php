<?php

namespace Doctrine\DBAL\Platforms;

/**
 * Provides the behavior, features and SQL dialect of the PostgreSQL 10.0 database platform.
 */
class PostgreSQL100Platform extends \Doctrine\DBAL\Platforms\PostgreSQL94Platform
{
    protected function getReservedKeywordsClass() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getListSequencesSQL($database) : string
    {
    }
}
