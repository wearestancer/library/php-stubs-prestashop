<?php

namespace Doctrine\DBAL\Platforms\Keywords;

/**
 * MariaDb reserved keywords list.
 *
 * @link https://mariadb.com/kb/en/the-mariadb-library/reserved-words/
 */
final class MariaDb102Keywords extends \Doctrine\DBAL\Platforms\Keywords\MySQLKeywords
{
    public function getName() : string
    {
    }
}
