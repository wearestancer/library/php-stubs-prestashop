<?php

namespace Doctrine\DBAL\Tools\Console;

final class ConnectionNotFound extends \OutOfBoundsException
{
}
