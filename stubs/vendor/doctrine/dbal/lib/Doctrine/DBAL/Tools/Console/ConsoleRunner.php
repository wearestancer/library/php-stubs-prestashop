<?php

namespace Doctrine\DBAL\Tools\Console;

/**
 * Handles running the Console Tools inside Symfony Console context.
 */
class ConsoleRunner
{
    /**
     * Create a Symfony Console HelperSet
     *
     * @deprecated use a ConnectionProvider instead.
     *
     * @return HelperSet
     */
    public static function createHelperSet(\Doctrine\DBAL\Connection $connection)
    {
    }
    /**
     * Runs console with the given connection provider or helperset (deprecated).
     *
     * @param ConnectionProvider|HelperSet $helperSetOrConnectionProvider
     * @param Command[]                    $commands
     *
     * @return void
     */
    public static function run($helperSetOrConnectionProvider, $commands = [])
    {
    }
    /**
     * @return void
     */
    public static function addCommands(\Symfony\Component\Console\Application $cli, ?\Doctrine\DBAL\Tools\Console\ConnectionProvider $connectionProvider = null)
    {
    }
    /**
     * Prints the instructions to create a configuration file
     *
     * @return void
     */
    public static function printCliConfigTemplate()
    {
    }
}
