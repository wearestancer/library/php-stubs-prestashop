<?php

namespace Doctrine\DBAL\Tools\Console;

interface ConnectionProvider
{
    public function getDefaultConnection() : \Doctrine\DBAL\Connection;
    /**
     * @throws ConnectionNotFound in case a connection with the given name does not exist.
     */
    public function getConnection(string $name) : \Doctrine\DBAL\Connection;
}
