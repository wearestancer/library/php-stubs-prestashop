<?php

namespace Doctrine\DBAL\Tools\Console\ConnectionProvider;

class SingleConnectionProvider implements \Doctrine\DBAL\Tools\Console\ConnectionProvider
{
    public function __construct(\Doctrine\DBAL\Connection $connection, string $defaultConnectionName = 'default')
    {
    }
    public function getDefaultConnection() : \Doctrine\DBAL\Connection
    {
    }
    public function getConnection(string $name) : \Doctrine\DBAL\Connection
    {
    }
}
