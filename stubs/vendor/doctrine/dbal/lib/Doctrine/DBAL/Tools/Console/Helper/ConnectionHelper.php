<?php

namespace Doctrine\DBAL\Tools\Console\Helper;

/**
 * Doctrine CLI Connection Helper.
 *
 * @deprecated use a ConnectionProvider instead.
 */
class ConnectionHelper extends \Symfony\Component\Console\Helper\Helper
{
    /**
     * The Doctrine database Connection.
     *
     * @var Connection
     */
    protected $_connection;
    /**
     * @param Connection $connection The Doctrine database Connection.
     */
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
    }
    /**
     * Retrieves the Doctrine database Connection.
     *
     * @return Connection
     */
    public function getConnection()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
