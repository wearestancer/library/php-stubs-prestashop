<?php

namespace Doctrine\DBAL\Tools;

/**
 * Static class used to dump the variable to be used on output.
 * Simplified port of Util\Debug from doctrine/common.
 *
 * @internal
 */
final class Dumper
{
    /**
     * Returns a dump of the public, protected and private properties of $var.
     *
     * @link https://xdebug.org/
     *
     * @param mixed $var      The variable to dump.
     * @param int   $maxDepth The maximum nesting level for object properties.
     */
    public static function dump($var, int $maxDepth = 2) : string
    {
    }
    /**
     * @param mixed $var
     *
     * @return mixed
     */
    public static function export($var, int $maxDepth)
    {
    }
}
