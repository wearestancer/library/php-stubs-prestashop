<?php

namespace Doctrine\DBAL\Tools\Console\Command;

class ReservedWordsCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(?\Doctrine\DBAL\Tools\Console\ConnectionProvider $connectionProvider = null)
    {
    }
    /**
     * If you want to add or replace a keywords list use this command.
     *
     * @param string                    $name
     * @param class-string<KeywordList> $class
     *
     * @return void
     */
    public function setKeywordListClass($name, $class)
    {
    }
    /** @return void */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
