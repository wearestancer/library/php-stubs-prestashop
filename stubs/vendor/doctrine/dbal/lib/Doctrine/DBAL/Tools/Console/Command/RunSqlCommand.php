<?php

namespace Doctrine\DBAL\Tools\Console\Command;

/**
 * Task for executing arbitrary SQL that can come from a file or directly from
 * the command line.
 */
class RunSqlCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(?\Doctrine\DBAL\Tools\Console\ConnectionProvider $connectionProvider = null)
    {
    }
    /** @return void */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
