<?php

namespace Doctrine\Common\Cache\Psr6;

final class TypedCacheItem implements \Psr\Cache\CacheItemInterface
{
    /**
     * @internal
     */
    public function __construct(private string $key, private mixed $value, private bool $isHit)
    {
    }
    public function getKey() : string
    {
    }
    public function get() : mixed
    {
    }
    public function isHit() : bool
    {
    }
    public function set(mixed $value) : static
    {
    }
    /**
     * {@inheritDoc}
     */
    public function expiresAt($expiration) : static
    {
    }
    /**
     * {@inheritDoc}
     */
    public function expiresAfter($time) : static
    {
    }
    /**
     * @internal
     */
    public function getExpiry() : ?float
    {
    }
}
