<?php

namespace Doctrine\Common\Cache\Psr6;

/**
 * This class was copied from the Symfony Framework, see the original copyright
 * notice above. The code is distributed subject to the license terms in
 * https://github.com/symfony/symfony/blob/ff0cf61278982539c49e467db9ab13cbd342f76d/LICENSE
 */
final class DoctrineProvider extends \Doctrine\Common\Cache\CacheProvider
{
    public static function wrap(\Psr\Cache\CacheItemPoolInterface $pool) : \Doctrine\Common\Cache\Cache
    {
    }
    /** @internal */
    public function getPool() : \Psr\Cache\CacheItemPoolInterface
    {
    }
}
