<?php

namespace Doctrine\Common\Cache;

/**
 * Base class for cache provider implementations.
 */
abstract class CacheProvider implements \Doctrine\Common\Cache\Cache, \Doctrine\Common\Cache\FlushableCache, \Doctrine\Common\Cache\ClearableCache, \Doctrine\Common\Cache\MultiOperationCache
{
    public const DOCTRINE_NAMESPACE_CACHEKEY = 'DoctrineNamespaceCacheKey[%s]';
    /**
     * Sets the namespace to prefix all cache ids with.
     *
     * @param string $namespace
     *
     * @return void
     */
    public function setNamespace($namespace)
    {
    }
    /**
     * Retrieves the namespace that prefixes all cache ids.
     *
     * @return string
     */
    public function getNamespace()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetch($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function fetchMultiple(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function saveMultiple(array $keysAndValues, $lifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save($id, $data, $lifeTime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function deleteMultiple(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStats()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function flushAll()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function deleteAll()
    {
    }
    /**
     * Default implementation of doFetchMultiple. Each driver that supports multi-get should owerwrite it.
     *
     * @param string[] $keys Array of keys to retrieve from cache
     *
     * @return mixed[] Array of values retrieved for the given keys.
     */
    protected function doFetchMultiple(array $keys)
    {
    }
    /**
     * Fetches an entry from the cache.
     *
     * @param string $id The id of the cache entry to fetch.
     *
     * @return mixed|false The cached data or FALSE, if no cache entry exists for the given id.
     */
    protected abstract function doFetch($id);
    /**
     * Tests if an entry exists in the cache.
     *
     * @param string $id The cache id of the entry to check for.
     *
     * @return bool TRUE if a cache entry exists for the given cache id, FALSE otherwise.
     */
    protected abstract function doContains($id);
    /**
     * Default implementation of doSaveMultiple. Each driver that supports multi-put should override it.
     *
     * @param mixed[] $keysAndValues Array of keys and values to save in cache
     * @param int     $lifetime      The lifetime. If != 0, sets a specific lifetime for these
     *                               cache entries (0 => infinite lifeTime).
     *
     * @return bool TRUE if the operation was successful, FALSE if it wasn't.
     */
    protected function doSaveMultiple(array $keysAndValues, $lifetime = 0)
    {
    }
    /**
     * Puts data into the cache.
     *
     * @param string $id       The cache id.
     * @param string $data     The cache entry/data.
     * @param int    $lifeTime The lifetime. If != 0, sets a specific lifetime for this
     *                           cache entry (0 => infinite lifeTime).
     *
     * @return bool TRUE if the entry was successfully stored in the cache, FALSE otherwise.
     */
    protected abstract function doSave($id, $data, $lifeTime = 0);
    /**
     * Default implementation of doDeleteMultiple. Each driver that supports multi-delete should override it.
     *
     * @param string[] $keys Array of keys to delete from cache
     *
     * @return bool TRUE if the operation was successful, FALSE if it wasn't
     */
    protected function doDeleteMultiple(array $keys)
    {
    }
    /**
     * Deletes a cache entry.
     *
     * @param string $id The cache id.
     *
     * @return bool TRUE if the cache entry was successfully deleted, FALSE otherwise.
     */
    protected abstract function doDelete($id);
    /**
     * Flushes all cache entries.
     *
     * @return bool TRUE if the cache entries were successfully flushed, FALSE otherwise.
     */
    protected abstract function doFlush();
    /**
     * Retrieves cached information from the data store.
     *
     * @return mixed[]|null An associative array with server's statistics if available, NULL otherwise.
     */
    protected abstract function doGetStats();
}
