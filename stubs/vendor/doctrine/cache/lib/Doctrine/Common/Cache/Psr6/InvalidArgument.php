<?php

namespace Doctrine\Common\Cache\Psr6;

/**
 * @internal
 */
final class InvalidArgument extends \InvalidArgumentException implements \Psr\Cache\InvalidArgumentException
{
}
