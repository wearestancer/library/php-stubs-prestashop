<?php

namespace Doctrine\Common\Cache\Psr6;

final class CacheItem implements \Psr\Cache\CacheItemInterface
{
    /**
     * @internal
     *
     * @param mixed $data
     */
    public function __construct(string $key, $data, bool $isHit)
    {
    }
    public function getKey() : string
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return mixed
     */
    public function get()
    {
    }
    public function isHit() : bool
    {
    }
    /**
     * {@inheritDoc}
     */
    public function set($value) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function expiresAt($expiration) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function expiresAfter($time) : self
    {
    }
    /**
     * @internal
     */
    public function getExpiry() : ?float
    {
    }
}
