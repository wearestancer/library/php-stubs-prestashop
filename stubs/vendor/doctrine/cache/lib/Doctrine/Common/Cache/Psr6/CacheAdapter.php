<?php

namespace Doctrine\Common\Cache\Psr6;

final class CacheAdapter implements \Psr\Cache\CacheItemPoolInterface
{
    public static function wrap(\Doctrine\Common\Cache\Cache $cache) : \Psr\Cache\CacheItemPoolInterface
    {
    }
    /** @internal */
    public function getCache() : \Doctrine\Common\Cache\Cache
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getItem($key) : \Psr\Cache\CacheItemInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getItems(array $keys = []) : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasItem($key) : bool
    {
    }
    public function clear() : bool
    {
    }
    /**
     * {@inheritDoc}
     */
    public function deleteItem($key) : bool
    {
    }
    /**
     * {@inheritDoc}
     */
    public function deleteItems(array $keys) : bool
    {
    }
    public function save(\Psr\Cache\CacheItemInterface $item) : bool
    {
    }
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item) : bool
    {
    }
    public function commit() : bool
    {
    }
    public function __destruct()
    {
    }
}
