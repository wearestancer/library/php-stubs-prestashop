<?php

namespace Doctrine\ORM;

/**
 * Exception for a unexpected query result.
 */
class UnexpectedResultException extends \Doctrine\ORM\Exception\ORMException
{
}
