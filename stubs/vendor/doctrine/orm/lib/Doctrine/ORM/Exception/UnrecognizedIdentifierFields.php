<?php

namespace Doctrine\ORM\Exception;

final class UnrecognizedIdentifierFields extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ManagerException
{
    /**
     * @param string[] $fieldNames
     */
    public static function fromClassAndFieldNames(string $className, array $fieldNames) : self
    {
    }
}
