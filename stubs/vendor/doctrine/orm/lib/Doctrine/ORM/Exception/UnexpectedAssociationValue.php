<?php

namespace Doctrine\ORM\Exception;

final class UnexpectedAssociationValue extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function create(string $class, string $association, string $given, string $expected) : self
    {
    }
}
