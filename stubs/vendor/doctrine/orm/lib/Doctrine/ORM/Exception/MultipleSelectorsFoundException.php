<?php

namespace Doctrine\ORM\Exception;

final class MultipleSelectorsFoundException extends \Doctrine\ORM\Exception\ORMException
{
    public const MULTIPLE_SELECTORS_FOUND_EXCEPTION = 'Multiple selectors found: %s. Please select only one.';
    /**
     * @param string[] $selectors
     */
    public static function create(array $selectors) : self
    {
    }
}
