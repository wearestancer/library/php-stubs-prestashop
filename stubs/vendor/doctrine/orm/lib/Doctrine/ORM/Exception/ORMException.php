<?php

namespace Doctrine\ORM\Exception;

/**
 * Should become an interface in 3.0
 */
class ORMException extends \Doctrine\ORM\ORMException
{
}
