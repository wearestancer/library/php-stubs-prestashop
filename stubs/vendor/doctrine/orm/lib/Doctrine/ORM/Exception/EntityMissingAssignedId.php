<?php

namespace Doctrine\ORM\Exception;

final class EntityMissingAssignedId extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @param object $entity
     */
    public static function forField($entity, string $field) : self
    {
    }
}
