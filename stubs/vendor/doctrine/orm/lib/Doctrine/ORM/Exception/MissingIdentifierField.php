<?php

namespace Doctrine\ORM\Exception;

final class MissingIdentifierField extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ManagerException
{
    public static function fromFieldAndClass(string $fieldName, string $className) : self
    {
    }
}
