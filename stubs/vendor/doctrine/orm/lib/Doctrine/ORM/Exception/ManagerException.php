<?php

namespace Doctrine\ORM\Exception;

interface ManagerException extends \Throwable
{
}
