<?php

namespace Doctrine\ORM\Exception;

final class InvalidHydrationMode extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ManagerException
{
    public static function fromMode(string $mode) : self
    {
    }
}
