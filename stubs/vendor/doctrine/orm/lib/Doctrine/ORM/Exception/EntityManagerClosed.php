<?php

namespace Doctrine\ORM\Exception;

final class EntityManagerClosed extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ManagerException
{
    public static function create() : self
    {
    }
}
