<?php

namespace Doctrine\ORM\Exception;

final class ProxyClassesAlwaysRegenerating extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ConfigurationException
{
    public static function create() : self
    {
    }
}
