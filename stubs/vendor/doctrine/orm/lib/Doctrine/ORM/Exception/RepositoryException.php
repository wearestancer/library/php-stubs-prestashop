<?php

namespace Doctrine\ORM\Exception;

/**
 * This interface should be implemented by all exceptions in the Repository
 * namespace.
 */
interface RepositoryException extends \Throwable
{
}
