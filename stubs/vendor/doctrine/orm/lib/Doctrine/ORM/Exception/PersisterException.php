<?php

namespace Doctrine\ORM\Exception;

class PersisterException extends \Doctrine\ORM\Persisters\PersisterException
{
}
