<?php

namespace Doctrine\ORM\Exception;

final class InvalidEntityRepository extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ConfigurationException
{
    public static function fromClassName(string $className) : self
    {
    }
}
