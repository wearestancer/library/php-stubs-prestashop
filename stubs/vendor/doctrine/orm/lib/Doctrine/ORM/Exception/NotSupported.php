<?php

namespace Doctrine\ORM\Exception;

final class NotSupported extends \Doctrine\ORM\Exception\ORMException
{
    public static function create() : self
    {
    }
    public static function createForDbal3(string $context) : self
    {
    }
    public static function createForPersistence3(string $context) : self
    {
    }
}
