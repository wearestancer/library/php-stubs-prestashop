<?php

namespace Doctrine\ORM\Exception;

/**
 * @deprecated No replacement planned.
 */
final class UnknownEntityNamespace extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ConfigurationException
{
    public static function fromNamespaceAlias(string $entityNamespaceAlias) : self
    {
    }
}
