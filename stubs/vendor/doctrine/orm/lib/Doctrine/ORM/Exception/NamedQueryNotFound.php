<?php

namespace Doctrine\ORM\Exception;

final class NamedQueryNotFound extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\ConfigurationException
{
    public static function fromName(string $name) : self
    {
    }
}
