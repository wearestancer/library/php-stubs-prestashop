<?php

namespace Doctrine\ORM\Exception;

interface ConfigurationException extends \Throwable
{
}
