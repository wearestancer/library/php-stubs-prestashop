<?php

namespace Doctrine\ORM;

/**
 * Base contract for ORM queries. Base class for Query and NativeQuery.
 *
 * @link    www.doctrine-project.org
 */
abstract class AbstractQuery
{
    /* Hydration mode constants */
    /**
     * Hydrates an object graph. This is the default behavior.
     */
    public const HYDRATE_OBJECT = 1;
    /**
     * Hydrates an array graph.
     */
    public const HYDRATE_ARRAY = 2;
    /**
     * Hydrates a flat, rectangular result set with scalar values.
     */
    public const HYDRATE_SCALAR = 3;
    /**
     * Hydrates a single scalar value.
     */
    public const HYDRATE_SINGLE_SCALAR = 4;
    /**
     * Very simple object hydrator (optimized for performance).
     */
    public const HYDRATE_SIMPLEOBJECT = 5;
    /**
     * Hydrates scalar column value.
     */
    public const HYDRATE_SCALAR_COLUMN = 6;
    /**
     * The parameter map of this query.
     *
     * @var ArrayCollection|Parameter[]
     * @psalm-var ArrayCollection<int, Parameter>
     */
    protected $parameters;
    /**
     * The user-specified ResultSetMapping to use.
     *
     * @var ResultSetMapping|null
     */
    protected $_resultSetMapping;
    /**
     * The entity manager used by this query object.
     *
     * @var EntityManagerInterface
     */
    protected $_em;
    /**
     * The map of query hints.
     *
     * @psalm-var array<string, mixed>
     */
    protected $_hints = [];
    /**
     * The hydration mode.
     *
     * @var string|int
     * @psalm-var string|AbstractQuery::HYDRATE_*
     */
    protected $_hydrationMode = self::HYDRATE_OBJECT;
    /** @var QueryCacheProfile|null */
    protected $_queryCacheProfile;
    /**
     * Whether or not expire the result cache.
     *
     * @var bool
     */
    protected $_expireResultCache = false;
    /** @var QueryCacheProfile|null */
    protected $_hydrationCacheProfile;
    /**
     * Whether to use second level cache, if available.
     *
     * @var bool
     */
    protected $cacheable = false;
    /** @var bool */
    protected $hasCache = false;
    /**
     * Second level cache region name.
     *
     * @var string|null
     */
    protected $cacheRegion;
    /**
     * Second level query cache mode.
     *
     * @var int|null
     * @psalm-var Cache::MODE_*|null
     */
    protected $cacheMode;
    /** @var CacheLogger|null */
    protected $cacheLogger;
    /** @var int */
    protected $lifetime = 0;
    /**
     * Initializes a new instance of a class derived from <tt>AbstractQuery</tt>.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Enable/disable second level query (result) caching for this query.
     *
     * @param bool $cacheable
     *
     * @return $this
     */
    public function setCacheable($cacheable)
    {
    }
    /**
     * @return bool TRUE if the query results are enable for second level cache, FALSE otherwise.
     */
    public function isCacheable()
    {
    }
    /**
     * @param string $cacheRegion
     *
     * @return $this
     */
    public function setCacheRegion($cacheRegion)
    {
    }
    /**
     * Obtain the name of the second level query cache region in which query results will be stored
     *
     * @return string|null The cache region name; NULL indicates the default region.
     */
    public function getCacheRegion()
    {
    }
    /**
     * @return bool TRUE if the query cache and second level cache are enabled, FALSE otherwise.
     */
    protected function isCacheEnabled()
    {
    }
    /**
     * @return int
     */
    public function getLifetime()
    {
    }
    /**
     * Sets the life-time for this query into second level cache.
     *
     * @param int $lifetime
     *
     * @return $this
     */
    public function setLifetime($lifetime)
    {
    }
    /**
     * @return int|null
     * @psalm-return Cache::MODE_*|null
     */
    public function getCacheMode()
    {
    }
    /**
     * @param int $cacheMode
     * @psalm-param Cache::MODE_* $cacheMode
     *
     * @return $this
     */
    public function setCacheMode($cacheMode)
    {
    }
    /**
     * Gets the SQL query that corresponds to this query object.
     * The returned SQL syntax depends on the connection driver that is used
     * by this query object at the time of this method call.
     *
     * @return string SQL query
     */
    public abstract function getSQL();
    /**
     * Retrieves the associated EntityManager of this Query instance.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
    /**
     * Frees the resources used by the query object.
     *
     * Resets Parameters, Parameter Types and Query Hints.
     *
     * @return void
     */
    public function free()
    {
    }
    /**
     * Get all defined parameters.
     *
     * @return ArrayCollection The defined query parameters.
     * @psalm-return ArrayCollection<int, Parameter>
     */
    public function getParameters()
    {
    }
    /**
     * Gets a query parameter.
     *
     * @param mixed $key The key (index or name) of the bound parameter.
     *
     * @return Parameter|null The value of the bound parameter, or NULL if not available.
     */
    public function getParameter($key)
    {
    }
    /**
     * Sets a collection of query parameters.
     *
     * @param ArrayCollection|mixed[] $parameters
     * @psalm-param ArrayCollection<int, Parameter>|mixed[] $parameters
     *
     * @return $this
     */
    public function setParameters($parameters)
    {
    }
    /**
     * Sets a query parameter.
     *
     * @param string|int      $key   The parameter position or name.
     * @param mixed           $value The parameter value.
     * @param string|int|null $type  The parameter type. If specified, the given value will be run through
     *                               the type conversion of this type. This is usually not needed for
     *                               strings and numeric types.
     *
     * @return $this
     */
    public function setParameter($key, $value, $type = null)
    {
    }
    /**
     * Processes an individual parameter value.
     *
     * @param mixed $value
     *
     * @return mixed[]|string|int|float|bool
     * @psalm-return array|scalar
     *
     * @throws ORMInvalidArgumentException
     */
    public function processParameterValue($value)
    {
    }
    /**
     * Sets the ResultSetMapping that should be used for hydration.
     *
     * @return $this
     */
    public function setResultSetMapping(\Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
    }
    /**
     * Gets the ResultSetMapping used for hydration.
     *
     * @return ResultSetMapping|null
     */
    protected function getResultSetMapping()
    {
    }
    /**
     * Set a cache profile for hydration caching.
     *
     * If no result cache driver is set in the QueryCacheProfile, the default
     * result cache driver is used from the configuration.
     *
     * Important: Hydration caching does NOT register entities in the
     * UnitOfWork when retrieved from the cache. Never use result cached
     * entities for requests that also flush the EntityManager. If you want
     * some form of caching with UnitOfWork registration you should use
     * {@see AbstractQuery::setResultCacheProfile()}.
     *
     * @return $this
     *
     * @example
     * $lifetime = 100;
     * $resultKey = "abc";
     * $query->setHydrationCacheProfile(new QueryCacheProfile());
     * $query->setHydrationCacheProfile(new QueryCacheProfile($lifetime, $resultKey));
     */
    public function setHydrationCacheProfile(?\Doctrine\DBAL\Cache\QueryCacheProfile $profile = null)
    {
    }
    /**
     * @return QueryCacheProfile|null
     */
    public function getHydrationCacheProfile()
    {
    }
    /**
     * Set a cache profile for the result cache.
     *
     * If no result cache driver is set in the QueryCacheProfile, the default
     * result cache driver is used from the configuration.
     *
     * @return $this
     */
    public function setResultCacheProfile(?\Doctrine\DBAL\Cache\QueryCacheProfile $profile = null)
    {
    }
    /**
     * Defines a cache driver to be used for caching result sets and implicitly enables caching.
     *
     * @deprecated Use {@see setResultCache()} instead.
     *
     * @param \Doctrine\Common\Cache\Cache|null $resultCacheDriver Cache driver
     *
     * @return $this
     *
     * @throws InvalidResultCacheDriver
     */
    public function setResultCacheDriver($resultCacheDriver = null)
    {
    }
    /**
     * Defines a cache driver to be used for caching result sets and implicitly enables caching.
     *
     * @return $this
     */
    public function setResultCache(?\Psr\Cache\CacheItemPoolInterface $resultCache = null)
    {
    }
    /**
     * Returns the cache driver used for caching result sets.
     *
     * @deprecated
     *
     * @return \Doctrine\Common\Cache\Cache Cache driver
     */
    public function getResultCacheDriver()
    {
    }
    /**
     * Set whether or not to cache the results of this query and if so, for
     * how long and which ID to use for the cache entry.
     *
     * @deprecated 2.7 Use {@see enableResultCache} and {@see disableResultCache} instead.
     *
     * @param bool   $useCache      Whether or not to cache the results of this query.
     * @param int    $lifetime      How long the cache entry is valid, in seconds.
     * @param string $resultCacheId ID to use for the cache entry.
     *
     * @return $this
     */
    public function useResultCache($useCache, $lifetime = null, $resultCacheId = null)
    {
    }
    /**
     * Enables caching of the results of this query, for given or default amount of seconds
     * and optionally specifies which ID to use for the cache entry.
     *
     * @param int|null    $lifetime      How long the cache entry is valid, in seconds.
     * @param string|null $resultCacheId ID to use for the cache entry.
     *
     * @return $this
     */
    public function enableResultCache(?int $lifetime = null, ?string $resultCacheId = null) : self
    {
    }
    /**
     * Disables caching of the results of this query.
     *
     * @return $this
     */
    public function disableResultCache() : self
    {
    }
    /**
     * Defines how long the result cache will be active before expire.
     *
     * @param int|null $lifetime How long the cache entry is valid, in seconds.
     *
     * @return $this
     */
    public function setResultCacheLifetime($lifetime)
    {
    }
    /**
     * Retrieves the lifetime of resultset cache.
     *
     * @deprecated
     *
     * @return int
     */
    public function getResultCacheLifetime()
    {
    }
    /**
     * Defines if the result cache is active or not.
     *
     * @param bool $expire Whether or not to force resultset cache expiration.
     *
     * @return $this
     */
    public function expireResultCache($expire = true)
    {
    }
    /**
     * Retrieves if the resultset cache is active or not.
     *
     * @return bool
     */
    public function getExpireResultCache()
    {
    }
    /**
     * @return QueryCacheProfile|null
     */
    public function getQueryCacheProfile()
    {
    }
    /**
     * Change the default fetch mode of an association for this query.
     *
     * $fetchMode can be one of ClassMetadata::FETCH_EAGER or ClassMetadata::FETCH_LAZY
     *
     * @param string $class
     * @param string $assocName
     * @param int    $fetchMode
     *
     * @return $this
     */
    public function setFetchMode($class, $assocName, $fetchMode)
    {
    }
    /**
     * Defines the processing mode to be used during hydration / result set transformation.
     *
     * @param string|int $hydrationMode Doctrine processing mode to be used during hydration process.
     *                                  One of the Query::HYDRATE_* constants.
     * @psalm-param string|AbstractQuery::HYDRATE_* $hydrationMode
     *
     * @return $this
     */
    public function setHydrationMode($hydrationMode)
    {
    }
    /**
     * Gets the hydration mode currently used by the query.
     *
     * @return string|int
     * @psalm-return string|AbstractQuery::HYDRATE_*
     */
    public function getHydrationMode()
    {
    }
    /**
     * Gets the list of results for the query.
     *
     * Alias for execute(null, $hydrationMode = HYDRATE_OBJECT).
     *
     * @param string|int $hydrationMode
     * @psalm-param string|AbstractQuery::HYDRATE_* $hydrationMode
     *
     * @return mixed
     */
    public function getResult($hydrationMode = self::HYDRATE_OBJECT)
    {
    }
    /**
     * Gets the array of results for the query.
     *
     * Alias for execute(null, HYDRATE_ARRAY).
     *
     * @return mixed[]
     */
    public function getArrayResult()
    {
    }
    /**
     * Gets one-dimensional array of results for the query.
     *
     * Alias for execute(null, HYDRATE_SCALAR_COLUMN).
     *
     * @return mixed[]
     */
    public function getSingleColumnResult()
    {
    }
    /**
     * Gets the scalar results for the query.
     *
     * Alias for execute(null, HYDRATE_SCALAR).
     *
     * @return mixed[]
     */
    public function getScalarResult()
    {
    }
    /**
     * Get exactly one result or null.
     *
     * @param string|int|null $hydrationMode
     * @psalm-param string|AbstractQuery::HYDRATE_*|null $hydrationMode
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getOneOrNullResult($hydrationMode = null)
    {
    }
    /**
     * Gets the single result of the query.
     *
     * Enforces the presence as well as the uniqueness of the result.
     *
     * If the result is not unique, a NonUniqueResultException is thrown.
     * If there is no result, a NoResultException is thrown.
     *
     * @param string|int|null $hydrationMode
     * @psalm-param string|AbstractQuery::HYDRATE_*|null $hydrationMode
     *
     * @return mixed
     *
     * @throws NonUniqueResultException If the query result is not unique.
     * @throws NoResultException        If the query returned no result.
     */
    public function getSingleResult($hydrationMode = null)
    {
    }
    /**
     * Gets the single scalar result of the query.
     *
     * Alias for getSingleResult(HYDRATE_SINGLE_SCALAR).
     *
     * @return mixed The scalar result.
     *
     * @throws NoResultException        If the query returned no result.
     * @throws NonUniqueResultException If the query result is not unique.
     */
    public function getSingleScalarResult()
    {
    }
    /**
     * Sets a query hint. If the hint name is not recognized, it is silently ignored.
     *
     * @param string $name  The name of the hint.
     * @param mixed  $value The value of the hint.
     *
     * @return $this
     */
    public function setHint($name, $value)
    {
    }
    /**
     * Gets the value of a query hint. If the hint name is not recognized, FALSE is returned.
     *
     * @param string $name The name of the hint.
     *
     * @return mixed The value of the hint or FALSE, if the hint name is not recognized.
     */
    public function getHint($name)
    {
    }
    /**
     * Check if the query has a hint
     *
     * @param string $name The name of the hint
     *
     * @return bool False if the query does not have any hint
     */
    public function hasHint($name)
    {
    }
    /**
     * Return the key value map of query hints that are currently set.
     *
     * @return array<string,mixed>
     */
    public function getHints()
    {
    }
    /**
     * Executes the query and returns an IterableResult that can be used to incrementally
     * iterate over the result.
     *
     * @deprecated 2.8 Use {@see toIterable} instead. See https://github.com/doctrine/orm/issues/8463
     *
     * @param ArrayCollection|mixed[]|null $parameters    The query parameters.
     * @param string|int|null              $hydrationMode The hydration mode to use.
     * @psalm-param string|AbstractQuery::HYDRATE_*|null $hydrationMode The hydration mode to use.
     *
     * @return IterableResult
     */
    public function iterate($parameters = null, $hydrationMode = null)
    {
    }
    /**
     * Executes the query and returns an iterable that can be used to incrementally
     * iterate over the result.
     *
     * @param ArrayCollection|array|mixed[] $parameters    The query parameters.
     * @param string|int|null               $hydrationMode The hydration mode to use.
     * @psalm-param ArrayCollection<int, Parameter>|mixed[] $parameters
     * @psalm-param string|AbstractQuery::HYDRATE_*|null    $hydrationMode
     *
     * @return iterable<mixed>
     */
    public function toIterable(iterable $parameters = [], $hydrationMode = null) : iterable
    {
    }
    /**
     * Executes the query.
     *
     * @param ArrayCollection|mixed[]|null $parameters    Query parameters.
     * @param string|int|null              $hydrationMode Processing mode to be used during the hydration process.
     * @psalm-param ArrayCollection<int, Parameter>|mixed[]|null $parameters
     * @psalm-param string|AbstractQuery::HYDRATE_*|null         $hydrationMode
     *
     * @return mixed
     */
    public function execute($parameters = null, $hydrationMode = null)
    {
    }
    /**
     * Get the result cache id to use to store the result set cache entry.
     * Will return the configured id if it exists otherwise a hash will be
     * automatically generated for you.
     *
     * @return string[] ($key, $hash)
     * @psalm-return array{string, string} ($key, $hash)
     */
    protected function getHydrationCacheId()
    {
    }
    /**
     * Set the result cache id to use to store the result set cache entry.
     * If this is not explicitly set by the developer then a hash is automatically
     * generated for you.
     *
     * @param string|null $id
     *
     * @return $this
     */
    public function setResultCacheId($id)
    {
    }
    /**
     * Get the result cache id to use to store the result set cache entry if set.
     *
     * @deprecated
     *
     * @return string|null
     */
    public function getResultCacheId()
    {
    }
    /**
     * Executes the query and returns a the resulting Statement object.
     *
     * @return Result|int The executed database statement that holds
     *                    the results, or an integer indicating how
     *                    many rows were affected.
     */
    protected abstract function _doExecute();
    /**
     * Cleanup Query resource when clone is called.
     *
     * @return void
     */
    public function __clone()
    {
    }
    /**
     * Generates a string of currently query to use for the cache second level cache.
     *
     * @return string
     */
    protected function getHash()
    {
    }
}
