<?php

namespace Doctrine\ORM\Cache\Exception;

class FeatureNotImplemented extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function scalarResults() : self
    {
    }
    public static function multipleRootEntities() : self
    {
    }
    public static function nonSelectStatements() : self
    {
    }
    public static function partialEntities() : self
    {
    }
}
