<?php

namespace Doctrine\ORM\Cache\Logging;

/**
 * Provide basic second level cache statistics.
 */
class StatisticsCacheLogger implements \Doctrine\ORM\Cache\Logging\CacheLogger
{
    /**
     * {@inheritdoc}
     */
    public function collectionCacheMiss($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectionCacheHit($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectionCachePut($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function entityCacheMiss($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function entityCacheHit($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function entityCachePut($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function queryCacheHit($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function queryCacheMiss($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function queryCachePut($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key)
    {
    }
    /**
     * Get the number of entries successfully retrieved from cache.
     *
     * @param string $regionName The name of the cache region.
     *
     * @return int
     */
    public function getRegionHitCount($regionName)
    {
    }
    /**
     * Get the number of cached entries *not* found in cache.
     *
     * @param string $regionName The name of the cache region.
     *
     * @return int
     */
    public function getRegionMissCount($regionName)
    {
    }
    /**
     * Get the number of cacheable entries put in cache.
     *
     * @param string $regionName The name of the cache region.
     *
     * @return int
     */
    public function getRegionPutCount($regionName)
    {
    }
    /**
     * @return array<string, int>
     */
    public function getRegionsMiss()
    {
    }
    /**
     * @return array<string, int>
     */
    public function getRegionsHit()
    {
    }
    /**
     * @return array<string, int>
     */
    public function getRegionsPut()
    {
    }
    /**
     * Clear region statistics
     *
     * @param string $regionName The name of the cache region.
     *
     * @return void
     */
    public function clearRegionStats($regionName)
    {
    }
    /**
     * Clear all statistics
     *
     * @return void
     */
    public function clearStats()
    {
    }
    /**
     * Get the total number of put in cache.
     *
     * @return int
     */
    public function getPutCount()
    {
    }
    /**
     * Get the total number of entries successfully retrieved from cache.
     *
     * @return int
     */
    public function getHitCount()
    {
    }
    /**
     * Get the total number of cached entries *not* found in cache.
     *
     * @return int
     */
    public function getMissCount()
    {
    }
}
