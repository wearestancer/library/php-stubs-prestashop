<?php

namespace Doctrine\ORM\Cache\Persister\Entity;

/**
 * Specific read-write entity persister
 */
class ReadWriteCachedEntityPersister extends \Doctrine\ORM\Cache\Persister\Entity\AbstractEntityPersister
{
    public function __construct(\Doctrine\ORM\Persisters\Entity\EntityPersister $persister, \Doctrine\ORM\Cache\ConcurrentRegion $region, \Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function afterTransactionComplete()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function afterTransactionRolledBack()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($entity)
    {
    }
}
