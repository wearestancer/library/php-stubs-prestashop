<?php

namespace Doctrine\ORM\Cache;

/**
 * Timestamp cache entry
 */
class TimestampCacheEntry implements \Doctrine\ORM\Cache\CacheEntry
{
    /**
     * @readonly Public only for performance reasons, it should be considered immutable.
     * @var float
     */
    public $time;
    /**
     * @param float|null $time
     */
    public function __construct($time = null)
    {
    }
    /**
     * Creates a new TimestampCacheEntry
     *
     * This method allow Doctrine\Common\Cache\PhpFileCache compatibility
     *
     * @param array<string,float> $values array containing property values
     *
     * @return TimestampCacheEntry
     */
    public static function __set_state(array $values)
    {
    }
}
