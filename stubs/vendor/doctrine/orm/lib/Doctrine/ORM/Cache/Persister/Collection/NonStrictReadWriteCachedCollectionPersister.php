<?php

namespace Doctrine\ORM\Cache\Persister\Collection;

class NonStrictReadWriteCachedCollectionPersister extends \Doctrine\ORM\Cache\Persister\Collection\AbstractCollectionPersister
{
    /**
     * {@inheritdoc}
     */
    public function afterTransactionComplete()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function afterTransactionRolledBack()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
}
