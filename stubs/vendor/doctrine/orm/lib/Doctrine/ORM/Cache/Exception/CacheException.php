<?php

namespace Doctrine\ORM\Cache\Exception;

/**
 * Exception for cache.
 */
class CacheException extends \Doctrine\ORM\Cache\CacheException
{
}
