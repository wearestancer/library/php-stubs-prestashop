<?php

namespace Doctrine\ORM\Cache;

/**
 * Configuration container for second-level cache.
 */
class CacheConfiguration
{
    /**
     * @return CacheFactory|null
     */
    public function getCacheFactory()
    {
    }
    /**
     * @return void
     */
    public function setCacheFactory(\Doctrine\ORM\Cache\CacheFactory $factory)
    {
    }
    /**
     * @return CacheLogger|null
     */
    public function getCacheLogger()
    {
    }
    /**
     * @return void
     */
    public function setCacheLogger(\Doctrine\ORM\Cache\Logging\CacheLogger $logger)
    {
    }
    /**
     * @return RegionsConfiguration
     */
    public function getRegionsConfiguration()
    {
    }
    /**
     * @return void
     */
    public function setRegionsConfiguration(\Doctrine\ORM\Cache\RegionsConfiguration $regionsConfig)
    {
    }
    /**
     * @return QueryCacheValidator
     */
    public function getQueryValidator()
    {
    }
    /**
     * @return void
     */
    public function setQueryValidator(\Doctrine\ORM\Cache\QueryCacheValidator $validator)
    {
    }
}
