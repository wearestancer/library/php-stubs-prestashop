<?php

namespace Doctrine\ORM\Cache\Region;

/**
 * A cache region that enables the retrieval of multiple elements with one call
 *
 * @deprecated Use {@link DefaultRegion} instead.
 */
class DefaultMultiGetRegion extends \Doctrine\ORM\Cache\Region\DefaultRegion
{
}
