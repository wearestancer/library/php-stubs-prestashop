<?php

namespace Doctrine\ORM\Cache\Persister\Entity;

/**
 * Interface for second level cache entity persisters.
 */
interface CachedEntityPersister extends \Doctrine\ORM\Cache\Persister\CachedPersister, \Doctrine\ORM\Persisters\Entity\EntityPersister
{
    /**
     * @return EntityHydrator
     */
    public function getEntityHydrator();
    /**
     * @param object $entity
     *
     * @return bool
     */
    public function storeEntityCache($entity, \Doctrine\ORM\Cache\EntityCacheKey $key);
}
