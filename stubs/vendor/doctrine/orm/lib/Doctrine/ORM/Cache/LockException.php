<?php

namespace Doctrine\ORM\Cache;

/**
 * Lock exception for cache.
 */
class LockException extends \Doctrine\ORM\Cache\Exception\CacheException
{
}
