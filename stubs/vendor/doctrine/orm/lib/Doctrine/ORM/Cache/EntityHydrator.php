<?php

namespace Doctrine\ORM\Cache;

/**
 * Hydrator cache entry for entities
 */
interface EntityHydrator
{
    /**
     * @param ClassMetadata  $metadata The entity metadata.
     * @param EntityCacheKey $key      The entity cache key.
     * @param object         $entity   The entity.
     *
     * @return EntityCacheEntry
     */
    public function buildCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\EntityCacheKey $key, $entity);
    /**
     * @param ClassMetadata    $metadata The entity metadata.
     * @param EntityCacheKey   $key      The entity cache key.
     * @param EntityCacheEntry $entry    The entity cache entry.
     * @param object|null      $entity   The entity to load the cache into. If not specified, a new entity is created.
     *
     * @return object|null
     */
    public function loadCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\EntityCacheKey $key, \Doctrine\ORM\Cache\EntityCacheEntry $entry, $entity = null);
}
