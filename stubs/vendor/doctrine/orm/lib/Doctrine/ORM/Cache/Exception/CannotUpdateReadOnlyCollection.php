<?php

namespace Doctrine\ORM\Cache\Exception;

class CannotUpdateReadOnlyCollection extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function fromEntityAndField(string $sourceEntity, string $fieldName) : self
    {
    }
}
