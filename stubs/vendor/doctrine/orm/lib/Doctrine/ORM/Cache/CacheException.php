<?php

namespace Doctrine\ORM\Cache;

/**
 * Exception for cache.
 */
class CacheException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @param string $sourceEntity
     * @param string $fieldName
     *
     * @return CacheException
     */
    public static function updateReadOnlyCollection($sourceEntity, $fieldName)
    {
    }
    /**
     * @deprecated This method is not used anymore.
     *
     * @param string $entityName
     *
     * @return CacheException
     */
    public static function updateReadOnlyEntity($entityName)
    {
    }
    /**
     * @param string $entityName
     *
     * @return CacheException
     */
    public static function nonCacheableEntity($entityName)
    {
    }
    /**
     * @deprecated This method is not used anymore.
     *
     * @param string $entityName
     * @param string $field
     *
     * @return CacheException
     */
    public static function nonCacheableEntityAssociation($entityName, $field)
    {
    }
}
