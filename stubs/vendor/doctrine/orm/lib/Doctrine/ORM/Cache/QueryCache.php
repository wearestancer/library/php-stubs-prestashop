<?php

namespace Doctrine\ORM\Cache;

/**
 * Defines the contract for caches capable of storing query results.
 * These caches should only concern themselves with storing the matching result ids.
 */
interface QueryCache
{
    /**
     * @return bool
     */
    public function clear();
    /**
     * @param mixed   $result
     * @param mixed[] $hints
     *
     * @return bool
     */
    public function put(\Doctrine\ORM\Cache\QueryCacheKey $key, \Doctrine\ORM\Query\ResultSetMapping $rsm, $result, array $hints = []);
    /**
     * @param mixed[] $hints
     *
     * @return mixed[]|null
     */
    public function get(\Doctrine\ORM\Cache\QueryCacheKey $key, \Doctrine\ORM\Query\ResultSetMapping $rsm, array $hints = []);
    /**
     * @return Region
     */
    public function getRegion();
}
