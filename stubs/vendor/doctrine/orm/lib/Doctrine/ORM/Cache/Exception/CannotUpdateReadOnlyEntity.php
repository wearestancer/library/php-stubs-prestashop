<?php

namespace Doctrine\ORM\Cache\Exception;

class CannotUpdateReadOnlyEntity extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function fromEntity(string $entityName) : self
    {
    }
}
