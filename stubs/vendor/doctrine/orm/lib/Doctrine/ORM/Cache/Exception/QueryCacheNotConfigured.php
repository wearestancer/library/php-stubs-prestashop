<?php

namespace Doctrine\ORM\Cache\Exception;

final class QueryCacheNotConfigured extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function create() : self
    {
    }
}
