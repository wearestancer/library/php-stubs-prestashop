<?php

namespace Doctrine\ORM\Cache\Exception;

final class MetadataCacheUsesNonPersistentCache extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function fromDriver(\Doctrine\Common\Cache\Cache $cache) : self
    {
    }
}
