<?php

namespace Doctrine\ORM\Cache;

/**
 * Hydrator cache entry for collections
 */
interface CollectionHydrator
{
    /**
     * @param array|mixed[]|Collection $collection The collection.
     *
     * @return CollectionCacheEntry
     */
    public function buildCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\CollectionCacheKey $key, $collection);
    /**
     * @return mixed[]|null
     */
    public function loadCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\CollectionCacheKey $key, \Doctrine\ORM\Cache\CollectionCacheEntry $entry, \Doctrine\ORM\PersistentCollection $collection);
}
