<?php

namespace Doctrine\ORM\Cache;

class DefaultCacheFactory implements \Doctrine\ORM\Cache\CacheFactory
{
    /**
     * @param CacheItemPoolInterface $cacheItemPool
     */
    public function __construct(\Doctrine\ORM\Cache\RegionsConfiguration $cacheConfig, $cacheItemPool)
    {
    }
    /**
     * @param string $fileLockRegionDirectory
     *
     * @return void
     */
    public function setFileLockRegionDirectory($fileLockRegionDirectory)
    {
    }
    /**
     * @return string
     */
    public function getFileLockRegionDirectory()
    {
    }
    /**
     * @return void
     */
    public function setRegion(\Doctrine\ORM\Cache\Region $region)
    {
    }
    /**
     * @return void
     */
    public function setTimestampRegion(\Doctrine\ORM\Cache\TimestampRegion $region)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildCachedEntityPersister(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Persisters\Entity\EntityPersister $persister, \Doctrine\ORM\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildCachedCollectionPersister(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Persisters\Collection\CollectionPersister $persister, array $mapping)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildQueryCache(\Doctrine\ORM\EntityManagerInterface $em, $regionName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildCollectionHydrator(\Doctrine\ORM\EntityManagerInterface $em, array $mapping)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildEntityHydrator(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRegion(array $cache)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTimestampRegion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createCache(\Doctrine\ORM\EntityManagerInterface $entityManager)
    {
    }
}
