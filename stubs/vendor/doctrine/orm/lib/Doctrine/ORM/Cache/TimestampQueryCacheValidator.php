<?php

namespace Doctrine\ORM\Cache;

class TimestampQueryCacheValidator implements \Doctrine\ORM\Cache\QueryCacheValidator
{
    public function __construct(\Doctrine\ORM\Cache\TimestampRegion $timestampRegion)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isValid(\Doctrine\ORM\Cache\QueryCacheKey $key, \Doctrine\ORM\Cache\QueryCacheEntry $entry)
    {
    }
}
