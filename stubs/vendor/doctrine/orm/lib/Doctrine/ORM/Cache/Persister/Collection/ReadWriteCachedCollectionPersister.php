<?php

namespace Doctrine\ORM\Cache\Persister\Collection;

class ReadWriteCachedCollectionPersister extends \Doctrine\ORM\Cache\Persister\Collection\AbstractCollectionPersister
{
    /**
     * @param mixed[] $association The association mapping.
     */
    public function __construct(\Doctrine\ORM\Persisters\Collection\CollectionPersister $persister, \Doctrine\ORM\Cache\ConcurrentRegion $region, \Doctrine\ORM\EntityManagerInterface $em, array $association)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function afterTransactionComplete()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function afterTransactionRolledBack()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
}
