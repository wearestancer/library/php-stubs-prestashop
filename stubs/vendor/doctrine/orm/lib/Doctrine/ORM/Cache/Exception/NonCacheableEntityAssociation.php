<?php

namespace Doctrine\ORM\Cache\Exception;

class NonCacheableEntityAssociation extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function fromEntityAndField(string $entityName, string $field) : self
    {
    }
}
