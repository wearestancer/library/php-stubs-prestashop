<?php

namespace Doctrine\ORM\Cache\Persister\Collection;

abstract class AbstractCollectionPersister implements \Doctrine\ORM\Cache\Persister\Collection\CachedCollectionPersister
{
    /** @var UnitOfWork */
    protected $uow;
    /** @var ClassMetadataFactory */
    protected $metadataFactory;
    /** @var CollectionPersister */
    protected $persister;
    /** @var ClassMetadata */
    protected $sourceEntity;
    /** @var ClassMetadata */
    protected $targetEntity;
    /** @var mixed[] */
    protected $association;
    /** @var mixed[] */
    protected $queuedCache = [];
    /** @var Region */
    protected $region;
    /** @var string */
    protected $regionName;
    /** @var CollectionHydrator */
    protected $hydrator;
    /** @var CacheLogger|null */
    protected $cacheLogger;
    /**
     * @param CollectionPersister    $persister   The collection persister that will be cached.
     * @param Region                 $region      The collection region.
     * @param EntityManagerInterface $em          The entity manager.
     * @param mixed[]                $association The association mapping.
     */
    public function __construct(\Doctrine\ORM\Persisters\Collection\CollectionPersister $persister, \Doctrine\ORM\Cache\Region $region, \Doctrine\ORM\EntityManagerInterface $em, array $association)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCacheRegion()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSourceEntityMetadata()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTargetEntityMetadata()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadCollectionCache(\Doctrine\ORM\PersistentCollection $collection, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function storeCollectionCache(\Doctrine\ORM\Cache\CollectionCacheKey $key, $elements)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains(\Doctrine\ORM\PersistentCollection $collection, $element)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsKey(\Doctrine\ORM\PersistentCollection $collection, $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(\Doctrine\ORM\PersistentCollection $collection, $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function slice(\Doctrine\ORM\PersistentCollection $collection, $offset, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadCriteria(\Doctrine\ORM\PersistentCollection $collection, \Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * Clears cache entries related to the current collection
     *
     * @deprecated This method is not used anymore.
     *
     * @return void
     */
    protected function evictCollectionCache(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * @deprecated This method is not used anymore.
     *
     * @param string $targetEntity
     * @param object $element
     * @psalm-param class-string $targetEntity
     *
     * @return void
     */
    protected function evictElementCache($targetEntity, $element)
    {
    }
}
