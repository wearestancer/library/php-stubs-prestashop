<?php

namespace Doctrine\ORM\Cache\Logging;

/**
 * Interface for logging.
 */
interface CacheLogger
{
    /**
     * Log an entity put into second level cache.
     *
     * @param string         $regionName The name of the cache region.
     * @param EntityCacheKey $key        The cache key of the entity.
     *
     * @return void
     */
    public function entityCachePut($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key);
    /**
     * Log an entity get from second level cache resulted in a hit.
     *
     * @param string         $regionName The name of the cache region.
     * @param EntityCacheKey $key        The cache key of the entity.
     *
     * @return void
     */
    public function entityCacheHit($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key);
    /**
     * Log an entity get from second level cache resulted in a miss.
     *
     * @param string         $regionName The name of the cache region.
     * @param EntityCacheKey $key        The cache key of the entity.
     *
     * @return void
     */
    public function entityCacheMiss($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key);
    /**
     * Log an entity put into second level cache.
     *
     * @param string             $regionName The name of the cache region.
     * @param CollectionCacheKey $key        The cache key of the collection.
     *
     * @return void
     */
    public function collectionCachePut($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key);
    /**
     * Log an entity get from second level cache resulted in a hit.
     *
     * @param string             $regionName The name of the cache region.
     * @param CollectionCacheKey $key        The cache key of the collection.
     *
     * @return void
     */
    public function collectionCacheHit($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key);
    /**
     * Log an entity get from second level cache resulted in a miss.
     *
     * @param string             $regionName The name of the cache region.
     * @param CollectionCacheKey $key        The cache key of the collection.
     *
     * @return void
     */
    public function collectionCacheMiss($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key);
    /**
     * Log a query put into the query cache.
     *
     * @param string        $regionName The name of the cache region.
     * @param QueryCacheKey $key        The cache key of the query.
     *
     * @return void
     */
    public function queryCachePut($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key);
    /**
     * Log a query get from the query cache resulted in a hit.
     *
     * @param string        $regionName The name of the cache region.
     * @param QueryCacheKey $key        The cache key of the query.
     *
     * @return void
     */
    public function queryCacheHit($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key);
    /**
     * Log a query get from the query cache resulted in a miss.
     *
     * @param string        $regionName The name of the cache region.
     * @param QueryCacheKey $key        The cache key of the query.
     *
     * @return void
     */
    public function queryCacheMiss($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key);
}
