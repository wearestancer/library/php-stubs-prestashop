<?php

namespace Doctrine\ORM\Cache;

/**
 * A key that identifies a timestamped space.
 */
class TimestampCacheKey extends \Doctrine\ORM\Cache\CacheKey
{
    /**
     * @param string $space Result cache id
     */
    public function __construct($space)
    {
    }
}
