<?php

namespace Doctrine\ORM\Cache;

/**
 * Provides an API for querying/managing the second level cache regions.
 */
class DefaultCache implements \Doctrine\ORM\Cache
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEntityCacheRegion($className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollectionCacheRegion($className, $association)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsEntity($className, $identifier)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictEntity($className, $identifier)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictEntityRegion($className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictEntityRegions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsCollection($className, $association, $ownerIdentifier)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictCollection($className, $association, $ownerIdentifier)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictCollectionRegion($className, $association)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictCollectionRegions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsQuery($regionName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictQueryRegion($regionName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictQueryRegions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getQueryCache($regionName = null)
    {
    }
}
