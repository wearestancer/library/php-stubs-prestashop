<?php

namespace Doctrine\ORM\Cache\Persister\Entity;

/**
 * Specific non-strict read/write cached entity persister
 */
class NonStrictReadWriteCachedEntityPersister extends \Doctrine\ORM\Cache\Persister\Entity\AbstractEntityPersister
{
    /**
     * {@inheritdoc}
     */
    public function afterTransactionComplete()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function afterTransactionRolledBack()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($entity)
    {
    }
}
