<?php

namespace Doctrine\ORM\Cache\Persister\Entity;

/**
 * Specific read-only region entity persister
 */
class ReadOnlyCachedEntityPersister extends \Doctrine\ORM\Cache\Persister\Entity\NonStrictReadWriteCachedEntityPersister
{
    /**
     * {@inheritdoc}
     */
    public function update($entity)
    {
    }
}
