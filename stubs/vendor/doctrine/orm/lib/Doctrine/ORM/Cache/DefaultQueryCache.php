<?php

namespace Doctrine\ORM\Cache;

/**
 * Default query cache implementation.
 */
class DefaultQueryCache implements \Doctrine\ORM\Cache\QueryCache
{
    /** @var CacheLogger|null */
    protected $cacheLogger;
    /**
     * @param EntityManagerInterface $em     The entity manager.
     * @param Region                 $region The query region.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Cache\Region $region)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(\Doctrine\ORM\Cache\QueryCacheKey $key, \Doctrine\ORM\Query\ResultSetMapping $rsm, array $hints = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function put(\Doctrine\ORM\Cache\QueryCacheKey $key, \Doctrine\ORM\Query\ResultSetMapping $rsm, $result, array $hints = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRegion()
    {
    }
}
