<?php

namespace Doctrine\ORM\Cache;

/**
 * Default hydrator cache for collections
 */
class DefaultCollectionHydrator implements \Doctrine\ORM\Cache\CollectionHydrator
{
    /**
     * @param EntityManagerInterface $em The entity manager.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\CollectionCacheKey $key, $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\CollectionCacheKey $key, \Doctrine\ORM\Cache\CollectionCacheEntry $entry, \Doctrine\ORM\PersistentCollection $collection)
    {
    }
}
