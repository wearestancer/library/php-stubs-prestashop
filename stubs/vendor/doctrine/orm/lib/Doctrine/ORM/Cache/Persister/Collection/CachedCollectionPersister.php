<?php

namespace Doctrine\ORM\Cache\Persister\Collection;

/**
 * Interface for second level cache collection persisters.
 */
interface CachedCollectionPersister extends \Doctrine\ORM\Cache\Persister\CachedPersister, \Doctrine\ORM\Persisters\Collection\CollectionPersister
{
    /**
     * @return ClassMetadata
     */
    public function getSourceEntityMetadata();
    /**
     * @return ClassMetadata
     */
    public function getTargetEntityMetadata();
    /**
     * Loads a collection from cache
     *
     * @return mixed[]|null
     */
    public function loadCollectionCache(\Doctrine\ORM\PersistentCollection $collection, \Doctrine\ORM\Cache\CollectionCacheKey $key);
    /**
     * Stores a collection into cache
     *
     * @param mixed[]|Collection $elements
     *
     * @return void
     */
    public function storeCollectionCache(\Doctrine\ORM\Cache\CollectionCacheKey $key, $elements);
}
