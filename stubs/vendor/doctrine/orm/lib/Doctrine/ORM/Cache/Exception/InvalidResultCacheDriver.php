<?php

namespace Doctrine\ORM\Cache\Exception;

/**
 * @deprecated
 */
final class InvalidResultCacheDriver extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function create() : self
    {
    }
}
