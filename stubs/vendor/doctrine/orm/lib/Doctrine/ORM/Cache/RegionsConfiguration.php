<?php

namespace Doctrine\ORM\Cache;

/**
 * Cache regions configuration
 */
class RegionsConfiguration
{
    /**
     * @param int $defaultLifetime
     * @param int $defaultLockLifetime
     */
    public function __construct($defaultLifetime = 3600, $defaultLockLifetime = 60)
    {
    }
    /**
     * @return int
     */
    public function getDefaultLifetime()
    {
    }
    /**
     * @param int $defaultLifetime
     *
     * @return void
     */
    public function setDefaultLifetime($defaultLifetime)
    {
    }
    /**
     * @return int
     */
    public function getDefaultLockLifetime()
    {
    }
    /**
     * @param int $defaultLockLifetime
     *
     * @return void
     */
    public function setDefaultLockLifetime($defaultLockLifetime)
    {
    }
    /**
     * @param string $regionName
     *
     * @return int
     */
    public function getLifetime($regionName)
    {
    }
    /**
     * @param string $name
     * @param int    $lifetime
     *
     * @return void
     */
    public function setLifetime($name, $lifetime)
    {
    }
    /**
     * @param string $regionName
     *
     * @return int
     */
    public function getLockLifetime($regionName)
    {
    }
    /**
     * @param string $name
     * @param int    $lifetime
     *
     * @return void
     */
    public function setLockLifetime($name, $lifetime)
    {
    }
}
