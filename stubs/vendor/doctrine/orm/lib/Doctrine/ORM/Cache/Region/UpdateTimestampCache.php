<?php

namespace Doctrine\ORM\Cache\Region;

/**
 * Tracks the timestamps of the most recent updates to particular keys.
 */
class UpdateTimestampCache extends \Doctrine\ORM\Cache\Region\DefaultRegion implements \Doctrine\ORM\Cache\TimestampRegion
{
    /**
     * {@inheritdoc}
     */
    public function update(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
}
