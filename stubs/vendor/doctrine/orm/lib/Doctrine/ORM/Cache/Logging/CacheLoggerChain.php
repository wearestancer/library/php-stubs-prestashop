<?php

namespace Doctrine\ORM\Cache\Logging;

class CacheLoggerChain implements \Doctrine\ORM\Cache\Logging\CacheLogger
{
    /**
     * @param string $name
     *
     * @return void
     */
    public function setLogger($name, \Doctrine\ORM\Cache\Logging\CacheLogger $logger)
    {
    }
    /**
     * @param string $name
     *
     * @return CacheLogger|null
     */
    public function getLogger($name)
    {
    }
    /**
     * @return array<string, CacheLogger>
     */
    public function getLoggers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectionCacheHit($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectionCacheMiss($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectionCachePut($regionName, \Doctrine\ORM\Cache\CollectionCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function entityCacheHit($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function entityCacheMiss($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function entityCachePut($regionName, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function queryCacheHit($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function queryCacheMiss($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function queryCachePut($regionName, \Doctrine\ORM\Cache\QueryCacheKey $key)
    {
    }
}
