<?php

namespace Doctrine\ORM\Cache\Persister\Collection;

class ReadOnlyCachedCollectionPersister extends \Doctrine\ORM\Cache\Persister\Collection\NonStrictReadWriteCachedCollectionPersister
{
    /**
     * {@inheritdoc}
     */
    public function update(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
}
