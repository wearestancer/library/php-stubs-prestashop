<?php

namespace Doctrine\ORM\Cache;

/**
 * Default hydrator cache for entities
 */
class DefaultEntityHydrator implements \Doctrine\ORM\Cache\EntityHydrator
{
    /**
     * @param EntityManagerInterface $em The entity manager.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\EntityCacheKey $key, $entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadCacheEntry(\Doctrine\ORM\Mapping\ClassMetadata $metadata, \Doctrine\ORM\Cache\EntityCacheKey $key, \Doctrine\ORM\Cache\EntityCacheEntry $entry, $entity = null)
    {
    }
}
