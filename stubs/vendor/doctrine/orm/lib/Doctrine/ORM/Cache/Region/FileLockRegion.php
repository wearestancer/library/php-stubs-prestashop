<?php

namespace Doctrine\ORM\Cache\Region;

/**
 * Very naive concurrent region, based on file locks.
 */
class FileLockRegion implements \Doctrine\ORM\Cache\ConcurrentRegion
{
    public const LOCK_EXTENSION = 'lock';
    /**
     * @param string         $directory
     * @param numeric-string $lockLifetime
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Doctrine\ORM\Cache\Region $region, $directory, $lockLifetime)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMultiple(\Doctrine\ORM\Cache\CollectionCacheEntry $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function put(\Doctrine\ORM\Cache\CacheKey $key, \Doctrine\ORM\Cache\CacheEntry $entry, ?\Doctrine\ORM\Cache\Lock $lock = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evict(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function evictAll()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lock(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function unlock(\Doctrine\ORM\Cache\CacheKey $key, \Doctrine\ORM\Cache\Lock $lock)
    {
    }
}
