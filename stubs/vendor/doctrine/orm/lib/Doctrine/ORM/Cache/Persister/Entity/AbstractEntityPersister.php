<?php

namespace Doctrine\ORM\Cache\Persister\Entity;

abstract class AbstractEntityPersister implements \Doctrine\ORM\Cache\Persister\Entity\CachedEntityPersister
{
    /** @var UnitOfWork */
    protected $uow;
    /** @var ClassMetadataFactory */
    protected $metadataFactory;
    /** @var EntityPersister */
    protected $persister;
    /** @var ClassMetadata */
    protected $class;
    /** @var mixed[] */
    protected $queuedCache = [];
    /** @var Region */
    protected $region;
    /** @var TimestampRegion */
    protected $timestampRegion;
    /** @var TimestampCacheKey */
    protected $timestampKey;
    /** @var EntityHydrator */
    protected $hydrator;
    /** @var Cache */
    protected $cache;
    /** @var CacheLogger|null */
    protected $cacheLogger;
    /** @var string */
    protected $regionName;
    /**
     * Associations configured as FETCH_EAGER, as well as all inverse one-to-one associations.
     *
     * @var array<string>|null
     */
    protected $joinedAssociations;
    /**
     * @param EntityPersister        $persister The entity persister to cache.
     * @param Region                 $region    The entity cache region.
     * @param EntityManagerInterface $em        The entity manager.
     * @param ClassMetadata          $class     The entity metadata.
     */
    public function __construct(\Doctrine\ORM\Persisters\Entity\EntityPersister $persister, \Doctrine\ORM\Cache\Region $region, \Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addInsert($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInserts()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSelectSQL($criteria, $assoc = null, $lockMode = null, $limit = null, $offset = null, ?array $orderBy = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCountSQL($criteria = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInsertSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResultSetMapping()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSelectConditionStatementSQL($field, $value, $assoc = null, $comparison = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists($entity, ?\Doctrine\Common\Collections\Criteria $extraConditions = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCacheRegion()
    {
    }
    /**
     * @return EntityHydrator
     */
    public function getEntityHydrator()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function storeEntityCache($entity, \Doctrine\ORM\Cache\EntityCacheKey $key)
    {
    }
    /**
     * Generates a string of currently query
     *
     * @param string            $query
     * @param string[]|Criteria $criteria
     * @param string[]          $orderBy
     * @param int|null          $limit
     * @param int|null          $offset
     *
     * @return string
     */
    protected function getHash($query, $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function expandParameters($criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function expandCriteriaParameters(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassMetadata()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getManyToManyCollection(array $assoc, $sourceEntity, $offset = null, $limit = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOneToManyCollection(array $assoc, $sourceEntity, $offset = null, $limit = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOwningTable($fieldName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function executeInserts()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(array $criteria, $entity = null, $assoc = null, array $hints = [], $lockMode = null, $limit = null, ?array $orderBy = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadAll(array $criteria = [], ?array $orderBy = null, $limit = null, $offset = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadById(array $identifier, $entity = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function count($criteria = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadCriteria(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadManyToManyCollection(array $assoc, $sourceEntity, \Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadOneToManyCollection(array $assoc, $sourceEntity, \Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadOneToOneEntity(array $assoc, $sourceEntity, array $identifier = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lock(array $criteria, $lockMode)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refresh(array $id, $entity, $lockMode = null)
    {
    }
    /**
     * @param array<string, mixed> $association
     * @param array<string, mixed> $ownerId
     *
     * @return CollectionCacheKey
     */
    protected function buildCollectionCacheKey(array $association, $ownerId)
    {
    }
}
