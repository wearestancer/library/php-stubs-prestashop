<?php

namespace Doctrine\ORM\Cache\Exception;

final class MetadataCacheNotConfigured extends \Doctrine\ORM\Cache\Exception\CacheException
{
    public static function create() : self
    {
    }
}
