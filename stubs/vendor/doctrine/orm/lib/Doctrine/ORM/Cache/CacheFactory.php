<?php

namespace Doctrine\ORM\Cache;

/**
 * Contract for building second level cache regions components.
 */
interface CacheFactory
{
    /**
     * Build an entity persister for the given entity metadata.
     *
     * @param EntityManagerInterface $em        The entity manager.
     * @param EntityPersister        $persister The entity persister that will be cached.
     * @param ClassMetadata          $metadata  The entity metadata.
     *
     * @return CachedEntityPersister
     */
    public function buildCachedEntityPersister(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Persisters\Entity\EntityPersister $persister, \Doctrine\ORM\Mapping\ClassMetadata $metadata);
    /**
     * Build a collection persister for the given relation mapping.
     *
     * @param mixed[] $mapping The association mapping.
     *
     * @return CachedCollectionPersister
     */
    public function buildCachedCollectionPersister(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Persisters\Collection\CollectionPersister $persister, array $mapping);
    /**
     * Build a query cache based on the given region name
     *
     * @param string|null $regionName The region name.
     *
     * @return QueryCache The built query cache.
     */
    public function buildQueryCache(\Doctrine\ORM\EntityManagerInterface $em, $regionName = null);
    /**
     * Build an entity hydrator
     *
     * @return EntityHydrator The built entity hydrator.
     */
    public function buildEntityHydrator(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $metadata);
    /**
     * Build a collection hydrator
     *
     * @param mixed[] $mapping The association mapping.
     *
     * @return CollectionHydrator The built collection hydrator.
     */
    public function buildCollectionHydrator(\Doctrine\ORM\EntityManagerInterface $em, array $mapping);
    /**
     * Build a cache region
     *
     * @param array<string,mixed> $cache The cache configuration.
     *
     * @return Region The cache region.
     */
    public function getRegion(array $cache);
    /**
     * Build timestamp cache region
     *
     * @return TimestampRegion The timestamp region.
     */
    public function getTimestampRegion();
    /**
     * Build \Doctrine\ORM\Cache
     *
     * @return Cache
     */
    public function createCache(\Doctrine\ORM\EntityManagerInterface $entityManager);
}
