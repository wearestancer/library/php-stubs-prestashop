<?php

namespace Doctrine\ORM\Cache;

/**
 * Query cache entry
 */
class QueryCacheEntry implements \Doctrine\ORM\Cache\CacheEntry
{
    /**
     * List of entity identifiers
     *
     * @readonly Public only for performance reasons, it should be considered immutable.
     * @var array<string, mixed>
     */
    public $result;
    /**
     * Time creation of this cache entry
     *
     * @readonly Public only for performance reasons, it should be considered immutable.
     * @var float
     */
    public $time;
    /**
     * @param array<string, mixed> $result
     * @param float|null           $time
     */
    public function __construct($result, $time = null)
    {
    }
    /**
     * @param array<string, mixed> $values
     *
     * @return QueryCacheEntry
     */
    public static function __set_state(array $values)
    {
    }
}
