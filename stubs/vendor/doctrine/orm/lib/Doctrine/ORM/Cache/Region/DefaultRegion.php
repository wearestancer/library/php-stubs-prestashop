<?php

namespace Doctrine\ORM\Cache\Region;

/**
 * The simplest cache region compatible with all doctrine-cache drivers.
 */
class DefaultRegion implements \Doctrine\ORM\Cache\Region
{
    /**
     * @internal since 2.11, this constant will be private in 3.0.
     */
    public const REGION_KEY_SEPARATOR = '_';
    /**
     * @deprecated since 2.11, this property will be removed in 3.0.
     *
     * @var LegacyCache
     */
    protected $cache;
    /**
     * @internal since 2.11, this property will be private in 3.0.
     *
     * @var string
     */
    protected $name;
    /**
     * @internal since 2.11, this property will be private in 3.0.
     *
     * @var int
     */
    protected $lifetime = 0;
    /**
     * @param CacheItemPoolInterface $cacheItemPool
     */
    public function __construct(string $name, $cacheItemPool, int $lifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * @deprecated
     *
     * @return CacheProvider
     */
    public function getCache()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMultiple(\Doctrine\ORM\Cache\CollectionCacheEntry $collection)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function put(\Doctrine\ORM\Cache\CacheKey $key, \Doctrine\ORM\Cache\CacheEntry $entry, ?\Doctrine\ORM\Cache\Lock $lock = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function evict(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function evictAll()
    {
    }
    /**
     * @internal since 2.11, this method will be private in 3.0.
     *
     * @return string
     */
    protected function getCacheEntryKey(\Doctrine\ORM\Cache\CacheKey $key)
    {
    }
}
