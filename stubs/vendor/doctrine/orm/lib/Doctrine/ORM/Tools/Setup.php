<?php

namespace Doctrine\ORM\Tools;

/**
 * Convenience class for setting up Doctrine from different installations and configurations.
 *
 * @deprecated Use {@see ORMSetup} instead.
 */
class Setup
{
    /**
     * Use this method to register all autoloads for a downloaded Doctrine library.
     * Pick the directory the library was uncompressed into.
     *
     * @deprecated Use Composer's autoloader instead.
     *
     * @param string $directory
     *
     * @return void
     */
    public static function registerAutoloadDirectory($directory)
    {
    }
    /**
     * Creates a configuration with an annotation metadata driver.
     *
     * @param string[]    $paths
     * @param bool        $isDevMode
     * @param string|null $proxyDir
     * @param bool        $useSimpleAnnotationReader
     *
     * @return Configuration
     */
    public static function createAnnotationMetadataConfiguration(array $paths, $isDevMode = false, $proxyDir = null, ?\Doctrine\Common\Cache\Cache $cache = null, $useSimpleAnnotationReader = true)
    {
    }
    /**
     * Creates a configuration with an attribute metadata driver.
     *
     * @param string[]    $paths
     * @param bool        $isDevMode
     * @param string|null $proxyDir
     */
    public static function createAttributeMetadataConfiguration(array $paths, $isDevMode = false, $proxyDir = null, ?\Doctrine\Common\Cache\Cache $cache = null) : \Doctrine\ORM\Configuration
    {
    }
    /**
     * Creates a configuration with an XML metadata driver.
     *
     * @param string[]    $paths
     * @param bool        $isDevMode
     * @param string|null $proxyDir
     *
     * @return Configuration
     */
    public static function createXMLMetadataConfiguration(array $paths, $isDevMode = false, $proxyDir = null, ?\Doctrine\Common\Cache\Cache $cache = null)
    {
    }
    /**
     * Creates a configuration with a YAML metadata driver.
     *
     * @deprecated YAML metadata mapping is deprecated and will be removed in 3.0
     *
     * @param string[]    $paths
     * @param bool        $isDevMode
     * @param string|null $proxyDir
     *
     * @return Configuration
     */
    public static function createYAMLMetadataConfiguration(array $paths, $isDevMode = false, $proxyDir = null, ?\Doctrine\Common\Cache\Cache $cache = null)
    {
    }
    /**
     * Creates a configuration without a metadata driver.
     *
     * @param bool        $isDevMode
     * @param string|null $proxyDir
     *
     * @return Configuration
     */
    public static function createConfiguration($isDevMode = false, $proxyDir = null, ?\Doctrine\Common\Cache\Cache $cache = null)
    {
    }
}
