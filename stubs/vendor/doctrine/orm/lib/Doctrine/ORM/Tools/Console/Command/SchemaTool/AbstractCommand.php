<?php

namespace Doctrine\ORM\Tools\Console\Command\SchemaTool;

/**
 * Base class for CreateCommand, DropCommand and UpdateCommand.
 *
 * @link    www.doctrine-project.org
 */
abstract class AbstractCommand extends \Doctrine\ORM\Tools\Console\Command\AbstractEntityManagerCommand
{
    /**
     * @param mixed[] $metadatas
     *
     * @return int|null Null or 0 if everything went fine, or an error code.
     */
    protected abstract function executeSchemaCommand(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output, \Doctrine\ORM\Tools\SchemaTool $schemaTool, array $metadatas, \Symfony\Component\Console\Style\SymfonyStyle $ui);
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
