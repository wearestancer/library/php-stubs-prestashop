<?php

namespace Doctrine\ORM\Tools\Console;

interface EntityManagerProvider
{
    public function getDefaultManager() : \Doctrine\ORM\EntityManagerInterface;
    public function getManager(string $name) : \Doctrine\ORM\EntityManagerInterface;
}
