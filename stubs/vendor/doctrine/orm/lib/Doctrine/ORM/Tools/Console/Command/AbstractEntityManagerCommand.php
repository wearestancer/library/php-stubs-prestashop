<?php

namespace Doctrine\ORM\Tools\Console\Command;

abstract class AbstractEntityManagerCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(?\Doctrine\ORM\Tools\Console\EntityManagerProvider $entityManagerProvider = null)
    {
    }
    protected final function getEntityManager(\Symfony\Component\Console\Input\InputInterface $input) : \Doctrine\ORM\EntityManagerInterface
    {
    }
}
