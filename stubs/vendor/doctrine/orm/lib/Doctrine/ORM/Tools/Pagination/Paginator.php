<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * The paginator can handle various complex scenarios with DQL.
 *
 * @template T
 */
class Paginator implements \Countable, \IteratorAggregate
{
    use \Doctrine\ORM\Internal\SQLResultCasing;
    /**
     * @param Query|QueryBuilder $query               A Doctrine ORM query or query builder.
     * @param bool               $fetchJoinCollection Whether the query joins a collection (true by default).
     */
    public function __construct($query, $fetchJoinCollection = true)
    {
    }
    /**
     * Returns the query.
     *
     * @return Query
     */
    public function getQuery()
    {
    }
    /**
     * Returns whether the query joins a collection.
     *
     * @return bool Whether the query joins a collection.
     */
    public function getFetchJoinCollection()
    {
    }
    /**
     * Returns whether the paginator will use an output walker.
     *
     * @return bool|null
     */
    public function getUseOutputWalkers()
    {
    }
    /**
     * Sets whether the paginator will use an output walker.
     *
     * @param bool|null $useOutputWalkers
     *
     * @return $this
     * @psalm-return static<T>
     */
    public function setUseOutputWalkers($useOutputWalkers)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return ArrayIterator
     * @psalm-return ArrayIterator<array-key, T>
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
