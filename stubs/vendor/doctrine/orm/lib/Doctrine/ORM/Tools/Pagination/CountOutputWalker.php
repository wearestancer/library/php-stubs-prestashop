<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * Wraps the query in order to accurately count the root objects.
 *
 * Given a DQL like `SELECT u FROM User u` it will generate an SQL query like:
 * SELECT COUNT(*) (SELECT DISTINCT <id> FROM (<original SQL>))
 *
 * Works with composite keys but cannot deal with queries that have multiple
 * root entities (e.g. `SELECT f, b from Foo, Bar`)
 *
 * @psalm-import-type QueryComponent from Parser
 */
class CountOutputWalker extends \Doctrine\ORM\Query\SqlWalker
{
    /**
     * Stores various parameters that are otherwise unavailable
     * because Doctrine\ORM\Query\SqlWalker keeps everything private without
     * accessors.
     *
     * @param Query        $query
     * @param ParserResult $parserResult
     * @param mixed[]      $queryComponents
     * @psalm-param array<string, QueryComponent> $queryComponents
     */
    public function __construct($query, $parserResult, array $queryComponents)
    {
    }
    /**
     * Walks down a SelectStatement AST node, wrapping it in a COUNT (SELECT DISTINCT).
     *
     * Note that the ORDER BY clause is not removed. Many SQL implementations (e.g. MySQL)
     * are able to cache subqueries. By keeping the ORDER BY clause intact, the limitSubQuery
     * that will most likely be executed next can be read from the native SQL cache.
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public function walkSelectStatement(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
}
