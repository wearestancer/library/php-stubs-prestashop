<?php

namespace Doctrine\ORM\Tools\Event;

/**
 * Event Args used for the Events::postGenerateSchema event.
 *
 * @link        www.doctrine-project.com
 */
class GenerateSchemaEventArgs extends \Doctrine\Common\EventArgs
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\DBAL\Schema\Schema $schema)
    {
    }
    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
    /**
     * @return Schema
     */
    public function getSchema()
    {
    }
}
