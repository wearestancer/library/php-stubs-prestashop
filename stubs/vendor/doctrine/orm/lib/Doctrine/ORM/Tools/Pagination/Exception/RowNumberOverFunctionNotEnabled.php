<?php

namespace Doctrine\ORM\Tools\Pagination\Exception;

final class RowNumberOverFunctionNotEnabled extends \Doctrine\ORM\Exception\ORMException
{
    public static function create() : self
    {
    }
}
