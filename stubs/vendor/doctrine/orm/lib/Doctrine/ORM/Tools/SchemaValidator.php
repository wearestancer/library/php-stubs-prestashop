<?php

namespace Doctrine\ORM\Tools;

/**
 * Performs strict validation of the mapping schema
 *
 * @link        www.doctrine-project.com
 */
class SchemaValidator
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Checks the internal consistency of all mapping files.
     *
     * There are several checks that can't be done at runtime or are too expensive, which can be verified
     * with this command. For example:
     *
     * 1. Check if a relation with "mappedBy" is actually connected to that specified field.
     * 2. Check if "mappedBy" and "inversedBy" are consistent to each other.
     * 3. Check if "referencedColumnName" attributes are really pointing to primary key columns.
     *
     * @psalm-return array<string, list<string>>
     */
    public function validateMapping()
    {
    }
    /**
     * Validates a single class of the current.
     *
     * @return string[]
     * @psalm-return list<string>
     */
    public function validateClass(\Doctrine\ORM\Mapping\ClassMetadataInfo $class)
    {
    }
    /**
     * Checks if the Database Schema is in sync with the current metadata state.
     *
     * @return bool
     */
    public function schemaInSyncWithMetadata()
    {
    }
    /**
     * Returns the list of missing Database Schema updates.
     *
     * @return array<string>
     */
    public function getUpdateSchemaList() : array
    {
    }
}
