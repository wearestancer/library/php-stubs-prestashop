<?php

namespace Doctrine\ORM\Tools\Console\EntityManagerProvider;

final class SingleManagerProvider implements \Doctrine\ORM\Tools\Console\EntityManagerProvider
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager, string $defaultManagerName = 'default')
    {
    }
    public function getDefaultManager() : \Doctrine\ORM\EntityManagerInterface
    {
    }
    public function getManager(string $name) : \Doctrine\ORM\EntityManagerInterface
    {
    }
}
