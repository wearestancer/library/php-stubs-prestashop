<?php

namespace Doctrine\ORM\Tools\Console\EntityManagerProvider;

/**
 * @deprecated This class will be removed in ORM 3.0 without replacement.
 */
final class HelperSetManagerProvider implements \Doctrine\ORM\Tools\Console\EntityManagerProvider
{
    public function __construct(\Symfony\Component\Console\Helper\HelperSet $helperSet)
    {
    }
    public function getManager(string $name) : \Doctrine\ORM\EntityManagerInterface
    {
    }
    public function getDefaultManager() : \Doctrine\ORM\EntityManagerInterface
    {
    }
}
