<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * Wraps the query in order to select root entity IDs for pagination.
 *
 * Given a DQL like `SELECT u FROM User u` it will generate an SQL query like:
 * SELECT DISTINCT <id> FROM (<original SQL>) LIMIT x OFFSET y
 *
 * Works with composite keys but cannot deal with queries that have multiple
 * root entities (e.g. `SELECT f, b from Foo, Bar`)
 *
 * @psalm-import-type QueryComponent from Parser
 */
class LimitSubqueryOutputWalker extends \Doctrine\ORM\Query\SqlWalker
{
    /**
     * Stores various parameters that are otherwise unavailable
     * because Doctrine\ORM\Query\SqlWalker keeps everything private without
     * accessors.
     *
     * @param Query        $query
     * @param ParserResult $parserResult
     * @param mixed[]      $queryComponents
     * @psalm-param array<string, QueryComponent> $queryComponents
     */
    public function __construct($query, $parserResult, array $queryComponents)
    {
    }
    /**
     * Walks down a SelectStatement AST node, wrapping it in a SELECT DISTINCT.
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public function walkSelectStatement(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
    /**
     * Walks down a SelectStatement AST node, wrapping it in a SELECT DISTINCT.
     * This method is for use with platforms which support ROW_NUMBER.
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public function walkSelectStatementWithRowNumber(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
    /**
     * Walks down a SelectStatement AST node, wrapping it in a SELECT DISTINCT.
     * This method is for platforms which DO NOT support ROW_NUMBER.
     *
     * @param bool $addMissingItemsFromOrderByToSelect
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public function walkSelectStatementWithoutRowNumber(\Doctrine\ORM\Query\AST\SelectStatement $AST, $addMissingItemsFromOrderByToSelect = true)
    {
    }
    /**
     * getter for $orderByPathExpressions
     *
     * @return list<PathExpression>
     */
    public function getOrderByPathExpressions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkPathExpression($pathExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSubSelect($subselect)
    {
    }
}
