<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * Replaces the selectClause of the AST with a COUNT statement.
 */
class CountWalker extends \Doctrine\ORM\Query\TreeWalkerAdapter
{
    /**
     * Distinct mode hint name.
     */
    public const HINT_DISTINCT = 'doctrine_paginator.distinct';
    /**
     * Walks down a SelectStatement AST node, modifying it to retrieve a COUNT.
     *
     * @return void
     *
     * @throws RuntimeException
     */
    public function walkSelectStatement(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
}
