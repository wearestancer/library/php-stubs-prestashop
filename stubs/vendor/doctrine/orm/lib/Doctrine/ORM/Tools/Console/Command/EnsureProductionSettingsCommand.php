<?php

namespace Doctrine\ORM\Tools\Console\Command;

/**
 * Command to ensure that Doctrine is properly configured for a production environment.
 *
 * @deprecated
 *
 * @link    www.doctrine-project.org
 */
class EnsureProductionSettingsCommand extends \Doctrine\ORM\Tools\Console\Command\AbstractEntityManagerCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
