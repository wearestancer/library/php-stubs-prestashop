<?php

namespace Doctrine\ORM\Tools\Console\EntityManagerProvider;

final class UnknownManagerException extends \OutOfBoundsException
{
    /**
     * @psalm-param list<string> $knownManagers
     */
    public static function unknownManager(string $unknownManager, array $knownManagers = []) : self
    {
    }
}
