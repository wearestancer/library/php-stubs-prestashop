<?php

namespace Doctrine\ORM\Tools\Exception;

final class MissingColumnException extends \Doctrine\ORM\Exception\ORMException
{
    public static function fromColumnSourceAndTarget(string $column, string $source, string $target) : self
    {
    }
}
