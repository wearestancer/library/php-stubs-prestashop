<?php

namespace Doctrine\ORM\Tools;

/**
 * Tools related Exceptions.
 */
class ToolsException extends \Doctrine\ORM\Exception\ORMException
{
    public static function schemaToolFailure(string $sql, \Throwable $e) : self
    {
    }
    /**
     * @param string $type
     *
     * @return ToolsException
     */
    public static function couldNotMapDoctrine1Type($type)
    {
    }
}
