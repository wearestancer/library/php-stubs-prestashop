<?php

namespace Doctrine\ORM\Tools\Export;

/**
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 */
class ExportException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @param string $type
     *
     * @return ExportException
     */
    public static function invalidExporterDriverType($type)
    {
    }
    /**
     * @param string $type
     *
     * @return ExportException
     */
    public static function invalidMappingDriverType($type)
    {
    }
    /**
     * @param string $file
     *
     * @return ExportException
     */
    public static function attemptOverwriteExistingFile($file)
    {
    }
}
