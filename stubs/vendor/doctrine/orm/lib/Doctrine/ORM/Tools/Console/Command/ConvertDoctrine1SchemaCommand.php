<?php

namespace Doctrine\ORM\Tools\Console\Command;

/**
 * Command to convert a Doctrine 1 schema to a Doctrine 2 mapping file.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
class ConvertDoctrine1SchemaCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @return EntityGenerator
     */
    public function getEntityGenerator()
    {
    }
    /**
     * @return void
     */
    public function setEntityGenerator(\Doctrine\ORM\Tools\EntityGenerator $entityGenerator)
    {
    }
    /**
     * @return ClassMetadataExporter
     */
    public function getMetadataExporter()
    {
    }
    /**
     * @return void
     */
    public function setMetadataExporter(\Doctrine\ORM\Tools\Export\ClassMetadataExporter $metadataExporter)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * @param mixed[]     $fromPaths
     * @param string      $destPath
     * @param string      $toType
     * @param int         $numSpaces
     * @param string|null $extend
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function convertDoctrine1Schema(array $fromPaths, $destPath, $toType, $numSpaces, $extend, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
