<?php

namespace Doctrine\ORM\Tools\Event;

/**
 * Event Args used for the Events::postGenerateSchemaTable event.
 *
 * @link        www.doctrine-project.com
 */
class GenerateSchemaTableEventArgs extends \Doctrine\Common\EventArgs
{
    public function __construct(\Doctrine\ORM\Mapping\ClassMetadata $classMetadata, \Doctrine\DBAL\Schema\Schema $schema, \Doctrine\DBAL\Schema\Table $classTable)
    {
    }
    /**
     * @return ClassMetadata
     */
    public function getClassMetadata()
    {
    }
    /**
     * @return Schema
     */
    public function getSchema()
    {
    }
    /**
     * @return Table
     */
    public function getClassTable()
    {
    }
}
