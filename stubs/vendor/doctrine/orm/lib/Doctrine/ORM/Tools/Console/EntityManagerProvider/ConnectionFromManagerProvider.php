<?php

namespace Doctrine\ORM\Tools\Console\EntityManagerProvider;

final class ConnectionFromManagerProvider implements \Doctrine\DBAL\Tools\Console\ConnectionProvider
{
    public function __construct(\Doctrine\ORM\Tools\Console\EntityManagerProvider $entityManagerProvider)
    {
    }
    public function getDefaultConnection() : \Doctrine\DBAL\Connection
    {
    }
    public function getConnection(string $name) : \Doctrine\DBAL\Connection
    {
    }
}
