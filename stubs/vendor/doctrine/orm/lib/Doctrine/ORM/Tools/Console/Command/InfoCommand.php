<?php

namespace Doctrine\ORM\Tools\Console\Command;

/**
 * Show information about mapped entities.
 *
 * @link    www.doctrine-project.org
 */
class InfoCommand extends \Doctrine\ORM\Tools\Console\Command\AbstractEntityManagerCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
