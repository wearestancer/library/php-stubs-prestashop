<?php

namespace Doctrine\ORM\Tools;

/**
 * Generic class used to generate PHP5 entity classes from ClassMetadataInfo instances.
 *
 *     [php]
 *     $classes = $em->getClassMetadataFactory()->getAllMetadata();
 *
 *     $generator = new \Doctrine\ORM\Tools\EntityGenerator();
 *     $generator->setGenerateAnnotations(true);
 *     $generator->setGenerateStubMethods(true);
 *     $generator->setRegenerateEntityIfExists(false);
 *     $generator->setUpdateEntityIfExists(true);
 *     $generator->generate($classes, '/path/to/generate/entities');
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
class EntityGenerator
{
    /**
     * Specifies class fields should be protected.
     */
    public const FIELD_VISIBLE_PROTECTED = 'protected';
    /**
     * Specifies class fields should be private.
     */
    public const FIELD_VISIBLE_PRIVATE = 'private';
    /** @var bool */
    protected $backupExisting = true;
    /**
     * The extension to use for written php files.
     *
     * @var string
     */
    protected $extension = '.php';
    /**
     * Whether or not the current ClassMetadataInfo instance is new or old.
     *
     * @var bool
     */
    protected $isNew = true;
    /** @var mixed[] */
    protected $staticReflection = [];
    /**
     * Number of spaces to use for indention in generated code.
     *
     * @var int
     */
    protected $numSpaces = 4;
    /**
     * The actual spaces to use for indention.
     *
     * @var string
     */
    protected $spaces = '    ';
    /**
     * The class all generated entities should extend.
     *
     * @var string
     */
    protected $classToExtend;
    /**
     * Whether or not to generation annotations.
     *
     * @var bool
     */
    protected $generateAnnotations = false;
    /** @var string */
    protected $annotationsPrefix = '';
    /**
     * Whether or not to generate sub methods.
     *
     * @var bool
     */
    protected $generateEntityStubMethods = false;
    /**
     * Whether or not to update the entity class if it exists already.
     *
     * @var bool
     */
    protected $updateEntityIfExists = false;
    /**
     * Whether or not to re-generate entity class if it exists already.
     *
     * @var bool
     */
    protected $regenerateEntityIfExists = false;
    /**
     * Visibility of the field
     *
     * @var string
     */
    protected $fieldVisibility = 'private';
    /**
     * Whether or not to make generated embeddables immutable.
     *
     * @var bool
     */
    protected $embeddablesImmutable = false;
    /**
     * Hash-map for handle types.
     *
     * @psalm-var array<Types::*|'json_array', string>
     */
    protected $typeAlias = [\Doctrine\DBAL\Types\Types::DATETIMETZ_MUTABLE => '\\DateTime', \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE => '\\DateTime', \Doctrine\DBAL\Types\Types::DATE_MUTABLE => '\\DateTime', \Doctrine\DBAL\Types\Types::TIME_MUTABLE => '\\DateTime', \Doctrine\DBAL\Types\Types::OBJECT => '\\stdClass', \Doctrine\DBAL\Types\Types::INTEGER => 'int', \Doctrine\DBAL\Types\Types::BIGINT => 'int', \Doctrine\DBAL\Types\Types::SMALLINT => 'int', \Doctrine\DBAL\Types\Types::TEXT => 'string', \Doctrine\DBAL\Types\Types::BLOB => 'string', \Doctrine\DBAL\Types\Types::DECIMAL => 'string', \Doctrine\DBAL\Types\Types::GUID => 'string', 'json_array' => 'array', \Doctrine\DBAL\Types\Types::JSON => 'array', \Doctrine\DBAL\Types\Types::SIMPLE_ARRAY => 'array', \Doctrine\DBAL\Types\Types::BOOLEAN => 'bool'];
    /**
     * Hash-map to handle generator types string.
     *
     * @psalm-var array<ClassMetadataInfo::GENERATOR_TYPE_*, string>
     */
    protected static $generatorStrategyMap = [\Doctrine\ORM\Mapping\ClassMetadataInfo::GENERATOR_TYPE_AUTO => 'AUTO', \Doctrine\ORM\Mapping\ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE => 'SEQUENCE', \Doctrine\ORM\Mapping\ClassMetadataInfo::GENERATOR_TYPE_IDENTITY => 'IDENTITY', \Doctrine\ORM\Mapping\ClassMetadataInfo::GENERATOR_TYPE_NONE => 'NONE', \Doctrine\ORM\Mapping\ClassMetadataInfo::GENERATOR_TYPE_UUID => 'UUID', \Doctrine\ORM\Mapping\ClassMetadataInfo::GENERATOR_TYPE_CUSTOM => 'CUSTOM'];
    /**
     * Hash-map to handle the change tracking policy string.
     *
     * @psalm-var array<ClassMetadataInfo::CHANGETRACKING_*, string>
     */
    protected static $changeTrackingPolicyMap = [\Doctrine\ORM\Mapping\ClassMetadataInfo::CHANGETRACKING_DEFERRED_IMPLICIT => 'DEFERRED_IMPLICIT', \Doctrine\ORM\Mapping\ClassMetadataInfo::CHANGETRACKING_DEFERRED_EXPLICIT => 'DEFERRED_EXPLICIT', \Doctrine\ORM\Mapping\ClassMetadataInfo::CHANGETRACKING_NOTIFY => 'NOTIFY'];
    /**
     * Hash-map to handle the inheritance type string.
     *
     * @psalm-var array<ClassMetadataInfo::INHERITANCE_TYPE_*, string>
     */
    protected static $inheritanceTypeMap = [\Doctrine\ORM\Mapping\ClassMetadataInfo::INHERITANCE_TYPE_NONE => 'NONE', \Doctrine\ORM\Mapping\ClassMetadataInfo::INHERITANCE_TYPE_JOINED => 'JOINED', \Doctrine\ORM\Mapping\ClassMetadataInfo::INHERITANCE_TYPE_SINGLE_TABLE => 'SINGLE_TABLE', \Doctrine\ORM\Mapping\ClassMetadataInfo::INHERITANCE_TYPE_TABLE_PER_CLASS => 'TABLE_PER_CLASS'];
    /** @var string */
    protected static $classTemplate = '<?php

<namespace>
<useStatement>
<entityAnnotation>
<entityClassName>
{
<entityBody>
}
';
    /** @var string */
    protected static $getMethodTemplate = '/**
 * <description>
 *
 * @return <variableType>
 */
public function <methodName>()
{
<spaces>return $this-><fieldName>;
}';
    /** @var string */
    protected static $setMethodTemplate = '/**
 * <description>
 *
 * @param <variableType> $<variableName>
 *
 * @return <entity>
 */
public function <methodName>(<methodTypeHint>$<variableName><variableDefault>)
{
<spaces>$this-><fieldName> = $<variableName>;

<spaces>return $this;
}';
    /** @var string */
    protected static $addMethodTemplate = '/**
 * <description>
 *
 * @param <variableType> $<variableName>
 *
 * @return <entity>
 */
public function <methodName>(<methodTypeHint>$<variableName>)
{
<spaces>$this-><fieldName>[] = $<variableName>;

<spaces>return $this;
}';
    /** @var string */
    protected static $removeMethodTemplate = '/**
 * <description>
 *
 * @param <variableType> $<variableName>
 *
 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
 */
public function <methodName>(<methodTypeHint>$<variableName>)
{
<spaces>return $this-><fieldName>->removeElement($<variableName>);
}';
    /** @var string */
    protected static $lifecycleCallbackMethodTemplate = '/**
 * @<name>
 */
public function <methodName>()
{
<spaces>// Add your code here
}';
    /** @var string */
    protected static $constructorMethodTemplate = '/**
 * Constructor
 */
public function __construct()
{
<spaces><collections>
}
';
    /** @var string */
    protected static $embeddableConstructorMethodTemplate = '/**
 * Constructor
 *
 * <paramTags>
 */
public function __construct(<params>)
{
<spaces><fields>
}
';
    /** @var Inflector */
    protected $inflector;
    public function __construct()
    {
    }
    /**
     * Generates and writes entity classes for the given array of ClassMetadataInfo instances.
     *
     * @param string $outputDirectory
     * @psalm-param list<ClassMetadataInfo> $metadatas
     *
     * @return void
     */
    public function generate(array $metadatas, $outputDirectory)
    {
    }
    /**
     * Generates and writes entity class to disk for the given ClassMetadataInfo instance.
     *
     * @param string $outputDirectory
     *
     * @return void
     *
     * @throws RuntimeException
     */
    public function writeEntityClass(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata, $outputDirectory)
    {
    }
    /**
     * Generates a PHP5 Doctrine 2 entity class from the given ClassMetadataInfo instance.
     *
     * @return string
     */
    public function generateEntityClass(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * Generates the updated code for the given ClassMetadataInfo and entity at path.
     *
     * @param string $path
     *
     * @return string
     */
    public function generateUpdatedEntityClass(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata, $path)
    {
    }
    /**
     * Sets the number of spaces the exported class should have.
     *
     * @param int $numSpaces
     *
     * @return void
     */
    public function setNumSpaces($numSpaces)
    {
    }
    /**
     * Sets the extension to use when writing php files to disk.
     *
     * @param string $extension
     *
     * @return void
     */
    public function setExtension($extension)
    {
    }
    /**
     * Sets the name of the class the generated classes should extend from.
     *
     * @param string $classToExtend
     *
     * @return void
     */
    public function setClassToExtend($classToExtend)
    {
    }
    /**
     * Sets whether or not to generate annotations for the entity.
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setGenerateAnnotations($bool)
    {
    }
    /**
     * Sets the class fields visibility for the entity (can either be private or protected).
     *
     * @param string $visibility
     * @psalm-param self::FIELD_VISIBLE_*
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function setFieldVisibility($visibility)
    {
    }
    /**
     * Sets whether or not to generate immutable embeddables.
     *
     * @param bool $embeddablesImmutable
     *
     * @return void
     */
    public function setEmbeddablesImmutable($embeddablesImmutable)
    {
    }
    /**
     * Sets an annotation prefix.
     *
     * @param string $prefix
     *
     * @return void
     */
    public function setAnnotationPrefix($prefix)
    {
    }
    /**
     * Sets whether or not to try and update the entity if it already exists.
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setUpdateEntityIfExists($bool)
    {
    }
    /**
     * Sets whether or not to regenerate the entity if it exists.
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setRegenerateEntityIfExists($bool)
    {
    }
    /**
     * Sets whether or not to generate stub methods for the entity.
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setGenerateStubMethods($bool)
    {
    }
    /**
     * Should an existing entity be backed up if it already exists?
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setBackupExisting($bool)
    {
    }
    public function setInflector(\Doctrine\Inflector\Inflector $inflector) : void
    {
    }
    /**
     * @param string $type
     *
     * @return string
     */
    protected function getType($type)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityNamespace(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityUse()
    {
    }
    /**
     * @return string
     */
    protected function generateEntityClassName(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityBody(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityConstructor(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @param string $src
     *
     * @return void
     *
     * @todo this won't work if there is a namespace in brackets and a class outside of it.
     * @psalm-suppress UndefinedConstant
     */
    protected function parseTokensInEntityFile($src)
    {
    }
    /**
     * @param string $property
     *
     * @return bool
     */
    protected function hasProperty($property, \Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @param string $method
     *
     * @return bool
     */
    protected function hasMethod($method, \Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return ReflectionClass[]
     * @psalm-return array<trait-string, ReflectionClass<object>>
     *
     * @throws ReflectionException
     */
    protected function getTraits(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return bool
     */
    protected function hasNamespace(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return bool
     */
    protected function extendsClass()
    {
    }
    /**
     * @return string
     */
    protected function getClassToExtend()
    {
    }
    /**
     * @return string
     */
    protected function getClassToExtendName()
    {
    }
    /**
     * @return string
     */
    protected function getClassName(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function getNamespace(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityDocBlock(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityAnnotation(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateTableAnnotation(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @param string $constraintName
     * @psalm-param array<string, array<string, mixed>> $constraints
     *
     * @return string
     */
    protected function generateTableConstraints($constraintName, array $constraints)
    {
    }
    /**
     * @return string
     */
    protected function generateInheritanceAnnotation(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateDiscriminatorColumnAnnotation(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string|null
     */
    protected function generateDiscriminatorMapAnnotation(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityStubMethods(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @psalm-param array<string, mixed> $associationMapping
     *
     * @return bool
     */
    protected function isAssociationIsNullable(array $associationMapping)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityLifecycleCallbackMethods(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityAssociationMappingProperties(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityFieldMappingProperties(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function generateEntityEmbeddedProperties(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @param string      $type
     * @param string      $fieldName
     * @param string|null $typeHint
     * @param string|null $defaultValue
     *
     * @return string
     */
    protected function generateEntityStubMethod(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata, $type, $fieldName, $typeHint = null, $defaultValue = null)
    {
    }
    /**
     * @param string $name
     * @param string $methodName
     *
     * @return string
     */
    protected function generateLifecycleCallbackMethod($name, $methodName, \Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @psalm-param array<string, mixed> $joinColumn
     *
     * @return string
     */
    protected function generateJoinColumnAnnotation(array $joinColumn)
    {
    }
    /**
     * @param mixed[] $associationMapping
     *
     * @return string
     */
    protected function generateAssociationMappingPropertyDocBlock(array $associationMapping, \Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @param mixed[] $fieldMapping
     *
     * @return string
     */
    protected function generateFieldMappingPropertyDocBlock(array $fieldMapping, \Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @psalm-param array<string, mixed> $embeddedClass
     *
     * @return string
     */
    protected function generateEmbeddedPropertyDocBlock(array $embeddedClass)
    {
    }
    /**
     * @param string $code
     * @param int    $num
     *
     * @return string
     */
    protected function prefixCodeWithSpaces($code, $num = 1)
    {
    }
    /**
     * @param int $type The inheritance type used by the class and its subclasses.
     *
     * @return string The literal string for the inheritance type.
     *
     * @throws InvalidArgumentException When the inheritance type does not exist.
     */
    protected function getInheritanceTypeString($type)
    {
    }
    /**
     * @param int $type The policy used for change-tracking for the mapped class.
     *
     * @return string The literal string for the change-tracking type.
     *
     * @throws InvalidArgumentException When the change-tracking type does not exist.
     */
    protected function getChangeTrackingPolicyString($type)
    {
    }
    /**
     * @param int $type The generator to use for the mapped class.
     *
     * @return string The literal string for the generator type.
     *
     * @throws InvalidArgumentException When the generator type does not exist.
     */
    protected function getIdGeneratorTypeString($type)
    {
    }
}
