<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * RowNumberOverFunction
 *
 * Provides ROW_NUMBER() OVER(ORDER BY...) construct for use in LimitSubqueryOutputWalker
 */
class RowNumberOverFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var OrderByClause */
    public $orderByClause;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @throws RowNumberOverFunctionNotEnabled
     *
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
