<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * Appends a condition "id IN (:foo_1, :foo_2)" to the whereClause of the AST.
 */
class WhereInWalker extends \Doctrine\ORM\Query\TreeWalkerAdapter
{
    /**
     * ID Count hint name.
     */
    public const HINT_PAGINATOR_ID_COUNT = 'doctrine.id.count';
    /**
     * Primary key alias for query.
     */
    public const PAGINATOR_ID_ALIAS = 'dpid';
    /**
     * Appends a condition equivalent to "WHERE IN (:dpid_1, :dpid_2, ...)" to the whereClause of the AST.
     *
     * The parameter namespace (dpid) is defined by
     * the PAGINATOR_ID_ALIAS
     *
     * The total number of parameters is retrieved from
     * the HINT_PAGINATOR_ID_COUNT query hint.
     *
     * @return void
     *
     * @throws RuntimeException
     */
    public function walkSelectStatement(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
}
