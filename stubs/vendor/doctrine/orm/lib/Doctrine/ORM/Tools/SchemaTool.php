<?php

namespace Doctrine\ORM\Tools;

/**
 * The SchemaTool is a tool to create/drop/update database schemas based on
 * <tt>ClassMetadata</tt> class descriptors.
 *
 * @link    www.doctrine-project.org
 */
class SchemaTool
{
    /**
     * Initializes a new SchemaTool instance that uses the connection of the
     * provided EntityManager.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Creates the database schema for the given array of ClassMetadata instances.
     *
     * @psalm-param list<ClassMetadata> $classes
     *
     * @return void
     *
     * @throws ToolsException
     */
    public function createSchema(array $classes)
    {
    }
    /**
     * Gets the list of DDL statements that are required to create the database schema for
     * the given list of ClassMetadata instances.
     *
     * @psalm-param list<ClassMetadata> $classes
     *
     * @return string[] The SQL statements needed to create the schema for the classes.
     */
    public function getCreateSchemaSql(array $classes)
    {
    }
    /**
     * Creates a Schema instance from a given set of metadata classes.
     *
     * @psalm-param list<ClassMetadata> $classes
     *
     * @return Schema
     *
     * @throws NotSupported
     */
    public function getSchemaFromMetadata(array $classes)
    {
    }
    /**
     * Drops the database schema for the given classes.
     *
     * In any way when an exception is thrown it is suppressed since drop was
     * issued for all classes of the schema and some probably just don't exist.
     *
     * @psalm-param list<ClassMetadata> $classes
     *
     * @return void
     */
    public function dropSchema(array $classes)
    {
    }
    /**
     * Drops all elements in the database of the current connection.
     *
     * @return void
     */
    public function dropDatabase()
    {
    }
    /**
     * Gets the SQL needed to drop the database schema for the connections database.
     *
     * @return string[]
     */
    public function getDropDatabaseSQL()
    {
    }
    /**
     * Gets SQL to drop the tables defined by the passed classes.
     *
     * @psalm-param list<ClassMetadata> $classes
     *
     * @return string[]
     */
    public function getDropSchemaSQL(array $classes)
    {
    }
    /**
     * Updates the database schema of the given classes by comparing the ClassMetadata
     * instances to the current database schema that is inspected.
     *
     * @param mixed[] $classes
     * @param bool    $saveMode If TRUE, only performs a partial update
     *                           without dropping assets which are scheduled for deletion.
     *
     * @return void
     */
    public function updateSchema(array $classes, $saveMode = false)
    {
    }
    /**
     * Gets the sequence of SQL statements that need to be performed in order
     * to bring the given class mappings in-synch with the relational schema.
     *
     * @param mixed[] $classes  The classes to consider.
     * @param bool    $saveMode If TRUE, only generates SQL for a partial update
     *                           that does not include SQL for dropping assets which are scheduled for deletion.
     *
     * @return string[] The sequence of SQL statements.
     */
    public function getUpdateSchemaSql(array $classes, $saveMode = false)
    {
    }
}
