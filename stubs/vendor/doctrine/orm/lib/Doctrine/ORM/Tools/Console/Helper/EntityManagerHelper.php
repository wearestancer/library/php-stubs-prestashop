<?php

namespace Doctrine\ORM\Tools\Console\Helper;

/**
 * Doctrine CLI Connection Helper.
 *
 * @deprecated This class will be removed in ORM 3.0 without replacement.
 */
class EntityManagerHelper extends \Symfony\Component\Console\Helper\Helper
{
    /**
     * Doctrine ORM EntityManagerInterface.
     *
     * @var EntityManagerInterface
     */
    protected $_em;
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Retrieves Doctrine ORM EntityManager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName()
    {
    }
}
