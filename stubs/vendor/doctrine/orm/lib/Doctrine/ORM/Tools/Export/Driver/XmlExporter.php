<?php

namespace Doctrine\ORM\Tools\Export\Driver;

/**
 * ClassMetadata exporter for Doctrine XML mapping files.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
class XmlExporter extends \Doctrine\ORM\Tools\Export\Driver\AbstractExporter
{
    /** @var string */
    protected $_extension = '.dcm.xml';
    /**
     * {@inheritdoc}
     */
    public function exportClassMetadata(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
}
