<?php

namespace Doctrine\ORM\Tools\Console\Command;

/**
 * Show information about mapped entities.
 *
 * @link    www.doctrine-project.org
 */
final class MappingDescribeCommand extends \Doctrine\ORM\Tools\Console\Command\AbstractEntityManagerCommand
{
}
