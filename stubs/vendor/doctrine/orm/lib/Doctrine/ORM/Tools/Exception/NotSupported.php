<?php

namespace Doctrine\ORM\Tools\Exception;

final class NotSupported extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\SchemaToolException
{
    public static function create() : self
    {
    }
}
