<?php

namespace Doctrine\ORM\Tools\Export\Driver;

/**
 * ClassMetadata exporter for Doctrine YAML mapping files.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
class YamlExporter extends \Doctrine\ORM\Tools\Export\Driver\AbstractExporter
{
    /** @var string */
    protected $_extension = '.dcm.yml';
    /**
     * {@inheritdoc}
     */
    public function exportClassMetadata(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * Dumps a PHP array to a YAML string.
     *
     * The yamlDump method, when supplied with an array, will do its best
     * to convert the array into friendly YAML.
     *
     * @param mixed[] $array  PHP array
     * @param int     $inline [optional] The level where you switch to inline YAML
     *
     * @return string A YAML string representing the original PHP array
     */
    protected function yamlDump($array, $inline = 2)
    {
    }
}
