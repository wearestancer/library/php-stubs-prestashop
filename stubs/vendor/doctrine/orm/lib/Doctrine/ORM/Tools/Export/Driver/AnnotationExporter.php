<?php

namespace Doctrine\ORM\Tools\Export\Driver;

/**
 * ClassMetadata exporter for PHP classes with annotations.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
class AnnotationExporter extends \Doctrine\ORM\Tools\Export\Driver\AbstractExporter
{
    /** @var string */
    protected $_extension = '.php';
    /**
     * {@inheritdoc}
     */
    public function exportClassMetadata(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return string
     */
    protected function _generateOutputPath(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @return void
     */
    public function setEntityGenerator(\Doctrine\ORM\Tools\EntityGenerator $entityGenerator)
    {
    }
}
