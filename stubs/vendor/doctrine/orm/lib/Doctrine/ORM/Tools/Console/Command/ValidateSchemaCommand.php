<?php

namespace Doctrine\ORM\Tools\Console\Command;

/**
 * Command to validate that the current mapping is valid.
 *
 * @link        www.doctrine-project.com
 */
class ValidateSchemaCommand extends \Doctrine\ORM\Tools\Console\Command\AbstractEntityManagerCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
