<?php

namespace Doctrine\ORM\Tools\Pagination;

/**
 * Replaces the selectClause of the AST with a SELECT DISTINCT root.id equivalent.
 */
class LimitSubqueryWalker extends \Doctrine\ORM\Query\TreeWalkerAdapter
{
    public const IDENTIFIER_TYPE = 'doctrine_paginator.id.type';
    public const FORCE_DBAL_TYPE_CONVERSION = 'doctrine_paginator.scalar_result.force_dbal_type_conversion';
    /**
     * Walks down a SelectStatement AST node, modifying it to retrieve DISTINCT ids
     * of the root Entity.
     *
     * @return void
     *
     * @throws RuntimeException
     */
    public function walkSelectStatement(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
}
