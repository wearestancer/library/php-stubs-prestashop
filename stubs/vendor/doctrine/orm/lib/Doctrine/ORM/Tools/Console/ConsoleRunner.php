<?php

namespace Doctrine\ORM\Tools\Console;

/**
 * Handles running the Console Tools inside Symfony Console context.
 */
final class ConsoleRunner
{
    /**
     * Create a Symfony Console HelperSet
     *
     * @deprecated This method will be removed in ORM 3.0 without replacement.
     */
    public static function createHelperSet(\Doctrine\ORM\EntityManagerInterface $entityManager) : \Symfony\Component\Console\Helper\HelperSet
    {
    }
    /**
     * Runs console with the given helper set.
     *
     * @param HelperSet|EntityManagerProvider $helperSetOrProvider
     * @param SymfonyCommand[]                $commands
     */
    public static function run($helperSetOrProvider, array $commands = []) : void
    {
    }
    /**
     * Creates a console application with the given helperset and
     * optional commands.
     *
     * @param HelperSet|EntityManagerProvider $helperSetOrProvider
     * @param SymfonyCommand[]                $commands
     *
     * @throws OutOfBoundsException
     */
    public static function createApplication($helperSetOrProvider, array $commands = []) : \Symfony\Component\Console\Application
    {
    }
    public static function addCommands(\Symfony\Component\Console\Application $cli, ?\Doctrine\ORM\Tools\Console\EntityManagerProvider $entityManagerProvider = null) : void
    {
    }
    /**
     * @deprecated This method will be removed in ORM 3.0 without replacement.
     */
    public static function printCliConfigTemplate() : void
    {
    }
}
