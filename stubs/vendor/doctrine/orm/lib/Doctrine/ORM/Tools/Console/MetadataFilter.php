<?php

namespace Doctrine\ORM\Tools\Console;

/**
 * Used by CLI Tools to restrict entity-based commands to given patterns.
 *
 * @link        www.doctrine-project.com
 */
class MetadataFilter extends \FilterIterator implements \Countable
{
    /**
     * Filter Metadatas by one or more filter options.
     *
     * @param ClassMetadata[] $metadatas
     * @param string[]|string $filter
     *
     * @return ClassMetadata[]
     */
    public static function filter(array $metadatas, $filter)
    {
    }
    /**
     * @param mixed[]|string $filter
     */
    public function __construct(\ArrayIterator $metadata, $filter)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function accept()
    {
    }
    /**
     * @return ArrayIterator<int, ClassMetadata>
     */
    #[\ReturnTypeWillChange]
    public function getInnerIterator()
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
}
