<?php

namespace Doctrine\ORM\Tools\Export\Driver;

/**
 * Abstract base class which is to be used for the Exporter drivers
 * which can be found in \Doctrine\ORM\Tools\Export\Driver.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
abstract class AbstractExporter
{
    /** @var ClassMetadata[] */
    protected $_metadata = [];
    /** @var string|null */
    protected $_outputDir;
    /** @var string|null */
    protected $_extension;
    /** @var bool */
    protected $_overwriteExistingFiles = false;
    /**
     * @param string|null $dir
     */
    public function __construct($dir = null)
    {
    }
    /**
     * @param bool $overwrite
     *
     * @return void
     */
    public function setOverwriteExistingFiles($overwrite)
    {
    }
    /**
     * Converts a single ClassMetadata instance to the exported format
     * and returns it.
     *
     * @return string
     */
    public abstract function exportClassMetadata(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata);
    /**
     * Sets the array of ClassMetadata instances to export.
     *
     * @psalm-param list<ClassMetadata> $metadata
     *
     * @return void
     */
    public function setMetadata(array $metadata)
    {
    }
    /**
     * Gets the extension used to generated the path to a class.
     *
     * @return string|null
     */
    public function getExtension()
    {
    }
    /**
     * Sets the directory to output the mapping files to.
     *
     *     [php]
     *     $exporter = new YamlExporter($metadata);
     *     $exporter->setOutputDir(__DIR__ . '/yaml');
     *     $exporter->export();
     *
     * @param string $dir
     *
     * @return void
     */
    public function setOutputDir($dir)
    {
    }
    /**
     * Exports each ClassMetadata instance to a single Doctrine Mapping file
     * named after the entity.
     *
     * @return void
     *
     * @throws ExportException
     */
    public function export()
    {
    }
    /**
     * Generates the path to write the class for the given ClassMetadataInfo instance.
     *
     * @return string
     */
    protected function _generateOutputPath(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * Sets the directory to output the mapping files to.
     *
     *     [php]
     *     $exporter = new YamlExporter($metadata, __DIR__ . '/yaml');
     *     $exporter->setExtension('.yml');
     *     $exporter->export();
     *
     * @param string $extension
     *
     * @return void
     */
    public function setExtension($extension)
    {
    }
    /**
     * @param int $type
     * @psalm-param ClassMetadataInfo::INHERITANCE_TYPE_* $type
     *
     * @return string
     */
    protected function _getInheritanceTypeString($type)
    {
    }
    /**
     * @param int $mode
     * @psalm-param ClassMetadataInfo::FETCH_* $mode
     *
     * @return string
     */
    protected function _getFetchModeString($mode)
    {
    }
    /**
     * @param int $policy
     * @psalm-param ClassMetadataInfo::CHANGETRACKING_* $policy
     *
     * @return string
     */
    protected function _getChangeTrackingPolicyString($policy)
    {
    }
    /**
     * @param int $type
     * @psalm-param ClassMetadataInfo::GENERATOR_TYPE_* $type
     *
     * @return string
     */
    protected function _getIdGeneratorTypeString($type)
    {
    }
}
