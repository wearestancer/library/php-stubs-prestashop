<?php

namespace Doctrine\ORM\Tools;

/**
 * Use this logger to dump the identity map during the onFlush event. This is useful for debugging
 * weird UnitOfWork behavior with complex operations.
 */
class DebugUnitOfWorkListener
{
    /**
     * Pass a stream and context information for the debugging session.
     *
     * The stream can be php://output to print to the screen.
     *
     * @param string $file
     * @param string $context
     */
    public function __construct($file = 'php://output', $context = '')
    {
    }
    /**
     * @return void
     */
    public function onFlush(\Doctrine\ORM\Event\OnFlushEventArgs $args)
    {
    }
    /**
     * Dumps the contents of the identity map into a stream.
     *
     * @return void
     */
    public function dumpIdentityMap(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
}
