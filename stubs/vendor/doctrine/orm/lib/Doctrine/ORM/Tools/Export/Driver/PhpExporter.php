<?php

namespace Doctrine\ORM\Tools\Export\Driver;

/**
 * ClassMetadata exporter for PHP code.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 *
 * @link    www.doctrine-project.org
 */
class PhpExporter extends \Doctrine\ORM\Tools\Export\Driver\AbstractExporter
{
    /** @var string */
    protected $_extension = '.php';
    /**
     * {@inheritdoc}
     */
    public function exportClassMetadata(\Doctrine\ORM\Mapping\ClassMetadataInfo $metadata)
    {
    }
    /**
     * @param mixed $var
     *
     * @return string
     */
    protected function _varExport($var)
    {
    }
}
