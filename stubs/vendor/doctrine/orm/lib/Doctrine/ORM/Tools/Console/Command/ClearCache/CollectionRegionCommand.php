<?php

namespace Doctrine\ORM\Tools\Console\Command\ClearCache;

/**
 * Command to clear a collection cache region.
 */
class CollectionRegionCommand extends \Doctrine\ORM\Tools\Console\Command\AbstractEntityManagerCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
