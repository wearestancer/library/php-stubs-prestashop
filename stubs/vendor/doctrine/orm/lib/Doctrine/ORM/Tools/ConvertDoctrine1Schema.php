<?php

namespace Doctrine\ORM\Tools;

/**
 * Class to help with converting Doctrine 1 schema files to Doctrine 2 mapping files
 *
 * @link    www.doctrine-project.org
 */
class ConvertDoctrine1Schema
{
    /**
     * Constructor passes the directory or array of directories
     * to convert the Doctrine 1 schema files from.
     *
     * @param string[]|string $from
     * @psalm-param list<string>|string $from
     */
    public function __construct($from)
    {
    }
    /**
     * Gets an array of ClassMetadataInfo instances from the passed
     * Doctrine 1 schema.
     *
     * @return ClassMetadataInfo[] An array of ClassMetadataInfo instances
     * @psalm-return list<ClassMetadataInfo>
     */
    public function getMetadata()
    {
    }
}
