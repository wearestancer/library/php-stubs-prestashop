<?php

namespace Doctrine\ORM\Tools\Console\Command\SchemaTool;

/**
 * Command to generate the SQL needed to update the database schema to match
 * the current mapping information.
 *
 * @link    www.doctrine-project.org
 */
class UpdateCommand extends \Doctrine\ORM\Tools\Console\Command\SchemaTool\AbstractCommand
{
    /** @var string */
    protected $name = 'orm:schema-tool:update';
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function executeSchemaCommand(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output, \Doctrine\ORM\Tools\SchemaTool $schemaTool, array $metadatas, \Symfony\Component\Console\Style\SymfonyStyle $ui)
    {
    }
}
