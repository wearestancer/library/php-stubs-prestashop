<?php

namespace Doctrine\ORM;

/**
 * An OptimisticLockException is thrown when a version check on an object
 * that uses optimistic locking through a version field fails.
 */
class OptimisticLockException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @param string      $msg
     * @param object|null $entity
     */
    public function __construct($msg, $entity)
    {
    }
    /**
     * Gets the entity that caused the exception.
     *
     * @return object|null
     */
    public function getEntity()
    {
    }
    /**
     * @param object $entity
     *
     * @return OptimisticLockException
     */
    public static function lockFailed($entity)
    {
    }
    /**
     * @param object                $entity
     * @param int|DateTimeInterface $expectedLockVersion
     * @param int|DateTimeInterface $actualLockVersion
     *
     * @return OptimisticLockException
     */
    public static function lockFailedVersionMismatch($entity, $expectedLockVersion, $actualLockVersion)
    {
    }
    /**
     * @param string $entityName
     *
     * @return OptimisticLockException
     */
    public static function notVersioned($entityName)
    {
    }
}
