<?php

namespace Doctrine\ORM\Id;

/**
 * Id generator that uses a single-row database table and a hi/lo algorithm.
 *
 * @deprecated no replacement planned
 */
class TableGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
    /**
     * @param string $tableName
     * @param string $sequenceName
     * @param int    $allocationSize
     */
    public function __construct($tableName, $sequenceName = 'default', $allocationSize = 10)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function generateId(\Doctrine\ORM\EntityManagerInterface $em, $entity)
    {
    }
}
