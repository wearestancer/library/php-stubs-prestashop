<?php

namespace Doctrine\ORM\Id;

abstract class AbstractIdGenerator
{
    /**
     * Generates an identifier for an entity.
     *
     * @deprecated Call {@see generateId()} instead.
     *
     * @param object|null $entity
     *
     * @return mixed
     */
    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
    }
    /**
     * Generates an identifier for an entity.
     *
     * @param object|null $entity
     *
     * @return mixed
     */
    public function generateId(\Doctrine\ORM\EntityManagerInterface $em, $entity)
    {
    }
    /**
     * Gets whether this generator is a post-insert generator which means that
     * {@link generateId()} must be called after the entity has been inserted
     * into the database.
     *
     * By default, this method returns FALSE. Generators that have this requirement
     * must override this method and return TRUE.
     *
     * @return bool
     */
    public function isPostInsertGenerator()
    {
    }
}
