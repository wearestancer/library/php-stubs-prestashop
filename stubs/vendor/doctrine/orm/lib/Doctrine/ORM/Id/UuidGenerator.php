<?php

namespace Doctrine\ORM\Id;

/**
 * Represents an ID generator that uses the database UUID expression
 *
 * @deprecated use an application-side generator instead
 */
class UuidGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
    public function __construct()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @throws NotSupported
     */
    public function generateId(\Doctrine\ORM\EntityManagerInterface $em, $entity)
    {
    }
}
