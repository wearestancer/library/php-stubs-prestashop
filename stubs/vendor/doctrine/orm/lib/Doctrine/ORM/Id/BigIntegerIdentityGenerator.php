<?php

namespace Doctrine\ORM\Id;

/**
 * Id generator that obtains IDs from special "identity" columns. These are columns
 * that automatically get a database-generated, auto-incremented identifier on INSERT.
 * This generator obtains the last insert id after such an insert.
 */
class BigIntegerIdentityGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
    /**
     * @param string|null $sequenceName The name of the sequence to pass to lastInsertId()
     *                                  to obtain the last generated identifier within the current
     *                                  database session/connection, if any.
     */
    public function __construct($sequenceName = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function generateId(\Doctrine\ORM\EntityManagerInterface $em, $entity)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isPostInsertGenerator()
    {
    }
}
