<?php

namespace Doctrine\ORM\Id;

/**
 * Special generator for application-assigned identifiers (doesn't really generate anything).
 */
class AssignedGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
    /**
     * Returns the identifier assigned to the given entity.
     *
     * {@inheritdoc}
     *
     * @throws EntityMissingAssignedId
     */
    public function generateId(\Doctrine\ORM\EntityManagerInterface $em, $entity)
    {
    }
}
