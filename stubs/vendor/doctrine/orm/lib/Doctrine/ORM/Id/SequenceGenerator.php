<?php

namespace Doctrine\ORM\Id;

/**
 * Represents an ID generator that uses a database sequence.
 */
class SequenceGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator implements \Serializable
{
    /**
     * Initializes a new sequence generator.
     *
     * @param string $sequenceName   The name of the sequence.
     * @param int    $allocationSize The allocation size of the sequence.
     */
    public function __construct($sequenceName, $allocationSize)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function generateId(\Doctrine\ORM\EntityManagerInterface $em, $entity)
    {
    }
    /**
     * Gets the maximum value of the currently allocated bag of values.
     *
     * @return int|null
     */
    public function getCurrentMaxValue()
    {
    }
    /**
     * Gets the next value that will be returned by generate().
     *
     * @return int
     */
    public function getNextValue()
    {
    }
    /**
     * @return string
     *
     * @final
     */
    public function serialize()
    {
    }
    /**
     * @return array<string, mixed>
     */
    public function __serialize() : array
    {
    }
    /**
     * @param string $serialized
     *
     * @return void
     *
     * @final
     */
    public function unserialize($serialized)
    {
    }
    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data) : void
    {
    }
}
