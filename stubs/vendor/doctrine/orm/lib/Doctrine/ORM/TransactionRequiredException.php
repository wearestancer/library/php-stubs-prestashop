<?php

namespace Doctrine\ORM;

/**
 * Is thrown when a transaction is required for the current operation, but there is none open.
 *
 * @link        www.doctrine-project.com
 */
class TransactionRequiredException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @return TransactionRequiredException
     */
    public static function transactionRequired()
    {
    }
}
