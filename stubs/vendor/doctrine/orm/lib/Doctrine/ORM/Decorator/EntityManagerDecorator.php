<?php

namespace Doctrine\ORM\Decorator;

/**
 * Base class for EntityManager decorators
 *
 * @extends ObjectManagerDecorator<EntityManagerInterface>
 */
abstract class EntityManagerDecorator extends \Doctrine\Persistence\ObjectManagerDecorator implements \Doctrine\ORM\EntityManagerInterface
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $wrapped)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExpressionBuilder()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @psalm-param class-string<T> $className
     *
     * @psalm-return EntityRepository<T>
     *
     * @template T of object
     */
    public function getRepository($className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassMetadata($className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function transactional($func)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function wrapInTransaction(callable $func)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rollback()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createQuery($dql = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNamedQuery($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNamedNativeQuery($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createQueryBuilder()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getReference($entityName, $id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function copy($entity, $deep = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function flush($entity = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEventManager()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isOpen()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUnitOfWork()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getHydrator($hydrationMode)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function newHydrator($hydrationMode)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProxyFactory()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFiltersStateClean()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasFilters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCache()
    {
    }
}
