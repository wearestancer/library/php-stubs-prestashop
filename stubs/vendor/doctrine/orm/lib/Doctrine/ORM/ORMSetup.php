<?php

namespace Doctrine\ORM;

final class ORMSetup
{
    /**
     * Creates a configuration with an annotation metadata driver.
     *
     * @param string[] $paths
     */
    public static function createAnnotationMetadataConfiguration(array $paths, bool $isDevMode = false, ?string $proxyDir = null, ?\Psr\Cache\CacheItemPoolInterface $cache = null) : \Doctrine\ORM\Configuration
    {
    }
    /**
     * Adds a new default annotation driver with a correctly configured annotation reader.
     *
     * @param string[] $paths
     */
    public static function createDefaultAnnotationDriver(array $paths = [], ?\Psr\Cache\CacheItemPoolInterface $cache = null) : \Doctrine\ORM\Mapping\Driver\AnnotationDriver
    {
    }
    /**
     * Creates a configuration with an attribute metadata driver.
     *
     * @param string[] $paths
     */
    public static function createAttributeMetadataConfiguration(array $paths, bool $isDevMode = false, ?string $proxyDir = null, ?\Psr\Cache\CacheItemPoolInterface $cache = null) : \Doctrine\ORM\Configuration
    {
    }
    /**
     * Creates a configuration with an XML metadata driver.
     *
     * @param string[] $paths
     */
    public static function createXMLMetadataConfiguration(array $paths, bool $isDevMode = false, ?string $proxyDir = null, ?\Psr\Cache\CacheItemPoolInterface $cache = null) : \Doctrine\ORM\Configuration
    {
    }
    /**
     * Creates a configuration with a YAML metadata driver.
     *
     * @deprecated YAML metadata mapping is deprecated and will be removed in 3.0
     *
     * @param string[] $paths
     */
    public static function createYAMLMetadataConfiguration(array $paths, bool $isDevMode = false, ?string $proxyDir = null, ?\Psr\Cache\CacheItemPoolInterface $cache = null) : \Doctrine\ORM\Configuration
    {
    }
    /**
     * Creates a configuration without a metadata driver.
     */
    public static function createConfiguration(bool $isDevMode = false, ?string $proxyDir = null, ?\Psr\Cache\CacheItemPoolInterface $cache = null) : \Doctrine\ORM\Configuration
    {
    }
}
