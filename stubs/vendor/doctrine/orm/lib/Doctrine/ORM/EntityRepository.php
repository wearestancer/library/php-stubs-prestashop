<?php

namespace Doctrine\ORM;

/**
 * An EntityRepository serves as a repository for entities with generic as well as
 * business specific methods for retrieving entities.
 *
 * This class is designed for inheritance and users can subclass this class to
 * write their own repositories with business-specific methods to locate entities.
 *
 * @template T of object
 * @template-implements Selectable<int,T>
 * @template-implements ObjectRepository<T>
 */
class EntityRepository implements \Doctrine\Persistence\ObjectRepository, \Doctrine\Common\Collections\Selectable
{
    /**
     * @internal This property will be private in 3.0, call {@see getEntityName()} instead.
     *
     * @var string
     * @psalm-var class-string<T>
     */
    protected $_entityName;
    /**
     * @internal This property will be private in 3.0, call {@see getEntityManager()} instead.
     *
     * @var EntityManagerInterface
     */
    protected $_em;
    /**
     * @internal This property will be private in 3.0, call {@see getClassMetadata()} instead.
     *
     * @var ClassMetadata
     * @psalm-var ClassMetadata<T>
     */
    protected $_class;
    /**
     * @psalm-param ClassMetadata<T> $class
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
    }
    /**
     * Creates a new QueryBuilder instance that is prepopulated for this entity name.
     *
     * @param string      $alias
     * @param string|null $indexBy The index for the from.
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
    }
    /**
     * Creates a new result set mapping builder for this entity.
     *
     * The column naming strategy is "INCREMENT".
     *
     * @param string $alias
     *
     * @return ResultSetMappingBuilder
     */
    public function createResultSetMappingBuilder($alias)
    {
    }
    /**
     * Creates a new Query instance based on a predefined metadata named query.
     *
     * @deprecated
     *
     * @param string $queryName
     *
     * @return Query
     */
    public function createNamedQuery($queryName)
    {
    }
    /**
     * Creates a native SQL query.
     *
     * @deprecated
     *
     * @param string $queryName
     *
     * @return NativeQuery
     */
    public function createNativeNamedQuery($queryName)
    {
    }
    /**
     * Clears the repository, causing all managed entities to become detached.
     *
     * @deprecated 2.8 This method is being removed from the ORM and won't have any replacement
     *
     * @return void
     */
    public function clear()
    {
    }
    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param mixed    $id          The identifier.
     * @param int|null $lockMode    One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     * @psalm-param LockMode::*|null $lockMode
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     * @psalm-return ?T
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
    }
    /**
     * Finds all entities in the repository.
     *
     * @psalm-return list<T> The entities.
     */
    public function findAll()
    {
    }
    /**
     * Finds entities by a set of criteria.
     *
     * @param int|null $limit
     * @param int|null $offset
     * @psalm-param array<string, mixed> $criteria
     * @psalm-param array<string, string>|null $orderBy
     *
     * @return object[] The objects.
     * @psalm-return list<T>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
    }
    /**
     * Finds a single entity by a set of criteria.
     *
     * @psalm-param array<string, mixed> $criteria
     * @psalm-param array<string, string>|null $orderBy
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     * @psalm-return ?T
     */
    public function findOneBy(array $criteria, ?array $orderBy = null)
    {
    }
    /**
     * Counts entities by a set of criteria.
     *
     * @psalm-param array<string, mixed> $criteria
     *
     * @return int The cardinality of the objects that match the given criteria.
     *
     * @todo Add this method to `ObjectRepository` interface in the next major release
     */
    public function count(array $criteria)
    {
    }
    /**
     * Adds support for magic method calls.
     *
     * @param string  $method
     * @param mixed[] $arguments
     * @psalm-param list<mixed> $arguments
     *
     * @return mixed The returned value from the resolved method.
     *
     * @throws BadMethodCallException If the method called is invalid.
     */
    public function __call($method, $arguments)
    {
    }
    /**
     * @return string
     * @psalm-return class-string<T>
     */
    protected function getEntityName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassName()
    {
    }
    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager()
    {
    }
    /**
     * @return ClassMetadata
     * @psalm-return ClassMetadata<T>
     */
    protected function getClassMetadata()
    {
    }
    /**
     * Select all elements from a selectable that match the expression and
     * return a new collection containing these elements.
     *
     * @return AbstractLazyCollection
     * @psalm-return AbstractLazyCollection<int, T>&Selectable<int, T>
     */
    public function matching(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
}
