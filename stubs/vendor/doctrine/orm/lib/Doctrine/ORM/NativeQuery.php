<?php

namespace Doctrine\ORM;

/**
 * Represents a native SQL query.
 */
final class NativeQuery extends \Doctrine\ORM\AbstractQuery
{
    /**
     * Sets the SQL of the query.
     *
     * @param string $sql
     *
     * @return $this
     */
    public function setSQL($sql) : self
    {
    }
    /**
     * Gets the SQL query.
     *
     * @return mixed The built SQL query or an array of all SQL queries.
     *
     * @override
     */
    public function getSQL()
    {
    }
}
