<?php

namespace Doctrine\ORM;

/**
 * Exception thrown when a Proxy fails to retrieve an Entity result.
 */
class EntityNotFoundException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * Static constructor.
     *
     * @param string   $className
     * @param string[] $id
     *
     * @return self
     */
    public static function fromClassNameAndIdentifier($className, array $id)
    {
    }
    /**
     * Instance for which no identifier can be found
     */
    public static function noIdentifierFound(string $className) : self
    {
    }
}
