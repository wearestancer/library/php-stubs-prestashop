<?php

namespace Doctrine\ORM;

/**
 * Contains exception messages for all invalid lifecycle state exceptions inside UnitOfWork
 */
class ORMInvalidArgumentException extends \InvalidArgumentException
{
    /**
     * @param object $entity
     *
     * @return ORMInvalidArgumentException
     */
    public static function scheduleInsertForManagedEntity($entity)
    {
    }
    /**
     * @param object $entity
     *
     * @return ORMInvalidArgumentException
     */
    public static function scheduleInsertForRemovedEntity($entity)
    {
    }
    /**
     * @param object $entity
     *
     * @return ORMInvalidArgumentException
     */
    public static function scheduleInsertTwice($entity)
    {
    }
    /**
     * @param string $className
     * @param object $entity
     *
     * @return ORMInvalidArgumentException
     */
    public static function entityWithoutIdentity($className, $entity)
    {
    }
    /**
     * @param object $entity
     *
     * @return ORMInvalidArgumentException
     */
    public static function readOnlyRequiresManagedEntity($entity)
    {
    }
    /**
     * @param array[][]|object[][] $newEntitiesWithAssociations non-empty an array
     *                                                              of [array $associationMapping, object $entity] pairs
     *
     * @return ORMInvalidArgumentException
     */
    public static function newEntitiesFoundThroughRelationships($newEntitiesWithAssociations)
    {
    }
    /**
     * @param object $entry
     * @psalm-param array<string, string> $associationMapping
     *
     * @return ORMInvalidArgumentException
     */
    public static function newEntityFoundThroughRelationship(array $associationMapping, $entry)
    {
    }
    /**
     * @param object $entry
     * @psalm-param array<string, string> $assoc
     *
     * @return ORMInvalidArgumentException
     */
    public static function detachedEntityFoundThroughRelationship(array $assoc, $entry)
    {
    }
    /**
     * @param object $entity
     *
     * @return ORMInvalidArgumentException
     */
    public static function entityNotManaged($entity)
    {
    }
    /**
     * @param object $entity
     * @param string $operation
     *
     * @return ORMInvalidArgumentException
     */
    public static function entityHasNoIdentity($entity, $operation)
    {
    }
    /**
     * @param object $entity
     * @param string $operation
     *
     * @return ORMInvalidArgumentException
     */
    public static function entityIsRemoved($entity, $operation)
    {
    }
    /**
     * @param object $entity
     * @param string $operation
     *
     * @return ORMInvalidArgumentException
     */
    public static function detachedEntityCannot($entity, $operation)
    {
    }
    /**
     * @param string $context
     * @param mixed  $given
     * @param int    $parameterIndex
     *
     * @return ORMInvalidArgumentException
     */
    public static function invalidObject($context, $given, $parameterIndex = 1)
    {
    }
    /**
     * @return ORMInvalidArgumentException
     */
    public static function invalidCompositeIdentifier()
    {
    }
    /**
     * @return ORMInvalidArgumentException
     */
    public static function invalidIdentifierBindingEntity()
    {
    }
    /**
     * @param mixed[] $assoc
     * @param mixed   $actualValue
     *
     * @return self
     */
    public static function invalidAssociation(\Doctrine\ORM\Mapping\ClassMetadata $targetClass, $assoc, $actualValue)
    {
    }
    /**
     * Used when a given entityName hasn't the good type
     *
     * @deprecated This method will be removed in 3.0.
     *
     * @param mixed $entityName The given entity (which shouldn't be a string)
     *
     * @return self
     */
    public static function invalidEntityName($entityName)
    {
    }
}
