<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class InheritanceType implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * The inheritance type used by the class and its subclasses.
     *
     * @var string
     * @Enum({"NONE", "JOINED", "SINGLE_TABLE", "TABLE_PER_CLASS"})
     */
    public $value;
    public function __construct(string $value)
    {
    }
}
