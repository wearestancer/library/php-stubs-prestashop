<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class Table implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $name;
    /** @var string|null */
    public $schema;
    /** @var array<\Doctrine\ORM\Mapping\Index>|null */
    public $indexes;
    /** @var array<\Doctrine\ORM\Mapping\UniqueConstraint>|null */
    public $uniqueConstraints;
    /** @var array<string,mixed> */
    public $options = [];
    /**
     * @param array<\Doctrine\ORM\Mapping\Index>            $indexes
     * @param array<\Doctrine\ORM\Mapping\UniqueConstraint> $uniqueConstraints
     * @param array<string,mixed>                           $options
     */
    public function __construct(?string $name = null, ?string $schema = null, ?array $indexes = null, ?array $uniqueConstraints = null, array $options = [])
    {
    }
}
