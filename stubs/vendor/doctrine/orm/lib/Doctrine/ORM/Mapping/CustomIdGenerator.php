<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class CustomIdGenerator implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $class;
    public function __construct(?string $class = null)
    {
    }
}
