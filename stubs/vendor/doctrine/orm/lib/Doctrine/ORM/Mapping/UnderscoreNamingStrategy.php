<?php

namespace Doctrine\ORM\Mapping;

/**
 * Naming strategy implementing the underscore naming convention.
 * Converts 'MyEntity' to 'my_entity' or 'MY_ENTITY'.
 *
 * @link    www.doctrine-project.org
 */
class UnderscoreNamingStrategy implements \Doctrine\ORM\Mapping\NamingStrategy
{
    /**
     * Underscore naming strategy construct.
     *
     * @param int $case CASE_LOWER | CASE_UPPER
     */
    public function __construct($case = \CASE_LOWER, bool $numberAware = false)
    {
    }
    /**
     * @return int CASE_LOWER | CASE_UPPER
     */
    public function getCase()
    {
    }
    /**
     * Sets string case CASE_LOWER | CASE_UPPER.
     * Alphabetic characters converted to lowercase or uppercase.
     *
     * @param int $case
     *
     * @return void
     */
    public function setCase($case)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function classToTableName($className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function propertyToColumnName($propertyName, $className = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function embeddedFieldToColumnName($propertyName, $embeddedColumnName, $className = null, $embeddedClassName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function referenceColumnName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function joinColumnName($propertyName, $className = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function joinTableName($sourceEntity, $targetEntity, $propertyName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function joinKeyColumnName($entityName, $referencedColumnName = null)
    {
    }
}
