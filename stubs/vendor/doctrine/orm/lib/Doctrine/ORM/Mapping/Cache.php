<?php

namespace Doctrine\ORM\Mapping;

/**
 * Caching to an entity or a collection.
 *
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target({"CLASS","PROPERTY"})
 */
#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::TARGET_PROPERTY)]
final class Cache implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * @Enum({"READ_ONLY", "NONSTRICT_READ_WRITE", "READ_WRITE"})
     * @var string The concurrency strategy.
     */
    public $usage = 'READ_ONLY';
    /** @var string|null Cache region name. */
    public $region;
    public function __construct(string $usage = 'READ_ONLY', ?string $region = null)
    {
    }
}
