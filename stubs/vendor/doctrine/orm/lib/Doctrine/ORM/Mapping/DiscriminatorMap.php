<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class DiscriminatorMap implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * @var array<string, string>
     * @psalm-var array<string, class-string>
     */
    public $value;
    /**
     * @param array<string, string> $value
     * @psalm-param array<string, class-string> $value
     */
    public function __construct(array $value)
    {
    }
}
