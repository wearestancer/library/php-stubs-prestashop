<?php

namespace Doctrine\ORM\Mapping;

/**
 * The default DefaultEntityListener
 */
class DefaultEntityListenerResolver implements \Doctrine\ORM\Mapping\EntityListenerResolver
{
    /**
     * {@inheritdoc}
     */
    public function clear($className = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function register($object)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve($className)
    {
    }
}
