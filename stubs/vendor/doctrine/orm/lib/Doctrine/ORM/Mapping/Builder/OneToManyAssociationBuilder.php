<?php

namespace Doctrine\ORM\Mapping\Builder;

/**
 * OneToMany Association Builder
 *
 * @link        www.doctrine-project.com
 */
class OneToManyAssociationBuilder extends \Doctrine\ORM\Mapping\Builder\AssociationBuilder
{
    /**
     * @psalm-param array<string, string> $fieldNames
     *
     * @return $this
     */
    public function setOrderBy(array $fieldNames)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return $this
     */
    public function setIndexBy($fieldName)
    {
    }
    /**
     * @return ClassMetadataBuilder
     */
    public function build()
    {
    }
}
