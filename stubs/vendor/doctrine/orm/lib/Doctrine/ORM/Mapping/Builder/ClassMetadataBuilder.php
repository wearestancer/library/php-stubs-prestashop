<?php

namespace Doctrine\ORM\Mapping\Builder;

/**
 * Builder Object for ClassMetadata
 *
 * @link        www.doctrine-project.com
 */
class ClassMetadataBuilder
{
    public function __construct(\Doctrine\ORM\Mapping\ClassMetadataInfo $cm)
    {
    }
    /**
     * @return ClassMetadataInfo
     */
    public function getClassMetadata()
    {
    }
    /**
     * Marks the class as mapped superclass.
     *
     * @return $this
     */
    public function setMappedSuperClass()
    {
    }
    /**
     * Marks the class as embeddable.
     *
     * @return $this
     */
    public function setEmbeddable()
    {
    }
    /**
     * Adds and embedded class
     *
     * @param string            $fieldName
     * @param string            $class
     * @param string|false|null $columnPrefix
     *
     * @return $this
     */
    public function addEmbedded($fieldName, $class, $columnPrefix = null)
    {
    }
    /**
     * Sets custom Repository class name.
     *
     * @param string $repositoryClassName
     *
     * @return $this
     */
    public function setCustomRepositoryClass($repositoryClassName)
    {
    }
    /**
     * Marks class read only.
     *
     * @return $this
     */
    public function setReadOnly()
    {
    }
    /**
     * Sets the table name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setTable($name)
    {
    }
    /**
     * Adds Index.
     *
     * @param string $name
     * @psalm-param list<string> $columns
     *
     * @return $this
     */
    public function addIndex(array $columns, $name)
    {
    }
    /**
     * Adds Unique Constraint.
     *
     * @param string $name
     * @psalm-param list<string> $columns
     *
     * @return $this
     */
    public function addUniqueConstraint(array $columns, $name)
    {
    }
    /**
     * Adds named query.
     *
     * @param string $name
     * @param string $dqlQuery
     *
     * @return $this
     */
    public function addNamedQuery($name, $dqlQuery)
    {
    }
    /**
     * Sets class as root of a joined table inheritance hierarchy.
     *
     * @return $this
     */
    public function setJoinedTableInheritance()
    {
    }
    /**
     * Sets class as root of a single table inheritance hierarchy.
     *
     * @return $this
     */
    public function setSingleTableInheritance()
    {
    }
    /**
     * Sets the discriminator column details.
     *
     * @param string $name
     * @param string $type
     * @param int    $length
     *
     * @return $this
     */
    public function setDiscriminatorColumn($name, $type = 'string', $length = 255)
    {
    }
    /**
     * Adds a subclass to this inheritance hierarchy.
     *
     * @param string $name
     * @param string $class
     *
     * @return $this
     */
    public function addDiscriminatorMapClass($name, $class)
    {
    }
    /**
     * Sets deferred explicit change tracking policy.
     *
     * @return $this
     */
    public function setChangeTrackingPolicyDeferredExplicit()
    {
    }
    /**
     * Sets notify change tracking policy.
     *
     * @return $this
     */
    public function setChangeTrackingPolicyNotify()
    {
    }
    /**
     * Adds lifecycle event.
     *
     * @param string $methodName
     * @param string $event
     *
     * @return $this
     */
    public function addLifecycleEvent($methodName, $event)
    {
    }
    /**
     * Adds Field.
     *
     * @param string $name
     * @param string $type
     * @psalm-param array<string, mixed> $mapping
     *
     * @return $this
     */
    public function addField($name, $type, array $mapping = [])
    {
    }
    /**
     * Creates a field builder.
     *
     * @param string $name
     * @param string $type
     *
     * @return FieldBuilder
     */
    public function createField($name, $type)
    {
    }
    /**
     * Creates an embedded builder.
     *
     * @param string $fieldName
     * @param string $class
     *
     * @return EmbeddedBuilder
     */
    public function createEmbedded($fieldName, $class)
    {
    }
    /**
     * Adds a simple many to one association, optionally with the inversed by field.
     *
     * @param string      $name
     * @param string      $targetEntity
     * @param string|null $inversedBy
     *
     * @return ClassMetadataBuilder
     */
    public function addManyToOne($name, $targetEntity, $inversedBy = null)
    {
    }
    /**
     * Creates a ManyToOne Association Builder.
     *
     * Note: This method does not add the association, you have to call build() on the AssociationBuilder.
     *
     * @param string $name
     * @param string $targetEntity
     *
     * @return AssociationBuilder
     */
    public function createManyToOne($name, $targetEntity)
    {
    }
    /**
     * Creates a OneToOne Association Builder.
     *
     * @param string $name
     * @param string $targetEntity
     *
     * @return AssociationBuilder
     */
    public function createOneToOne($name, $targetEntity)
    {
    }
    /**
     * Adds simple inverse one-to-one association.
     *
     * @param string $name
     * @param string $targetEntity
     * @param string $mappedBy
     *
     * @return ClassMetadataBuilder
     */
    public function addInverseOneToOne($name, $targetEntity, $mappedBy)
    {
    }
    /**
     * Adds simple owning one-to-one association.
     *
     * @param string      $name
     * @param string      $targetEntity
     * @param string|null $inversedBy
     *
     * @return ClassMetadataBuilder
     */
    public function addOwningOneToOne($name, $targetEntity, $inversedBy = null)
    {
    }
    /**
     * Creates a ManyToMany Association Builder.
     *
     * @param string $name
     * @param string $targetEntity
     *
     * @return ManyToManyAssociationBuilder
     */
    public function createManyToMany($name, $targetEntity)
    {
    }
    /**
     * Adds a simple owning many to many association.
     *
     * @param string      $name
     * @param string      $targetEntity
     * @param string|null $inversedBy
     *
     * @return ClassMetadataBuilder
     */
    public function addOwningManyToMany($name, $targetEntity, $inversedBy = null)
    {
    }
    /**
     * Adds a simple inverse many to many association.
     *
     * @param string $name
     * @param string $targetEntity
     * @param string $mappedBy
     *
     * @return ClassMetadataBuilder
     */
    public function addInverseManyToMany($name, $targetEntity, $mappedBy)
    {
    }
    /**
     * Creates a one to many association builder.
     *
     * @param string $name
     * @param string $targetEntity
     *
     * @return OneToManyAssociationBuilder
     */
    public function createOneToMany($name, $targetEntity)
    {
    }
    /**
     * Adds simple OneToMany association.
     *
     * @param string $name
     * @param string $targetEntity
     * @param string $mappedBy
     *
     * @return ClassMetadataBuilder
     */
    public function addOneToMany($name, $targetEntity, $mappedBy)
    {
    }
}
