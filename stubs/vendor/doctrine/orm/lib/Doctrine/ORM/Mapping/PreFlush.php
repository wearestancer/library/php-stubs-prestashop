<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @Target("METHOD")
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
final class PreFlush implements \Doctrine\ORM\Mapping\Annotation
{
}
