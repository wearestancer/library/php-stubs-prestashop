<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * {@inheritDoc}
 *
 * @deprecated this driver will be removed. Use Doctrine\Persistence\Mapping\Driver\MappingDriverChain instead
 */
class DriverChain extends \Doctrine\Persistence\Mapping\Driver\MappingDriverChain
{
}
