<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * @internal This class will be removed in ORM 3.0.
 */
abstract class CompatibilityAnnotationDriver extends \Doctrine\Persistence\Mapping\Driver\AnnotationDriver
{
}
