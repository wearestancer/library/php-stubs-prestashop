<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class OrderBy implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var array<string> */
    public $value;
    /**
     * @param array<string> $value
     */
    public function __construct(array $value)
    {
    }
}
