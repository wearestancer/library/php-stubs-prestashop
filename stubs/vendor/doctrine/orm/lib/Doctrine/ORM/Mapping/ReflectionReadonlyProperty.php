<?php

namespace Doctrine\ORM\Mapping;

/**
 * @internal
 */
final class ReflectionReadonlyProperty extends \ReflectionProperty
{
    public function __construct(private \ReflectionProperty $wrappedProperty)
    {
    }
    public function getValue(?object $object = null) : mixed
    {
    }
    public function setValue(mixed $objectOrValue, mixed $value = null) : void
    {
    }
}
