<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class ManyToOne implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var class-string|null */
    public $targetEntity;
    /** @var string[]|null */
    public $cascade;
    /**
     * The fetching strategy to use for the association.
     *
     * @var string
     * @Enum({"LAZY", "EAGER", "EXTRA_LAZY"})
     */
    public $fetch = 'LAZY';
    /** @var string|null */
    public $inversedBy;
    /**
     * @param class-string|null $targetEntity
     * @param string[]|null     $cascade
     */
    public function __construct(?string $targetEntity = null, ?array $cascade = null, string $fetch = 'LAZY', ?string $inversedBy = null)
    {
    }
}
