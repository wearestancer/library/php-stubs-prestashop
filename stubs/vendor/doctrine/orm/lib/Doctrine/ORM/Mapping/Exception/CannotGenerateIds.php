<?php

namespace Doctrine\ORM\Mapping\Exception;

final class CannotGenerateIds extends \Doctrine\ORM\Exception\ORMException
{
    public static function withPlatform(\Doctrine\DBAL\Platforms\AbstractPlatform $platform) : self
    {
    }
}
