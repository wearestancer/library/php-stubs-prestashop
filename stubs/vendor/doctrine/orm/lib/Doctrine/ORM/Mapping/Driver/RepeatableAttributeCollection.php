<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * @template-extends ArrayObject<int, T>
 * @template T of Annotation
 */
final class RepeatableAttributeCollection extends \ArrayObject
{
}
