<?php

namespace Doctrine\ORM\Mapping\Builder;

/**
 * Field Builder
 *
 * @link        www.doctrine-project.com
 */
class FieldBuilder
{
    /**
     * @param mixed[] $mapping
     */
    public function __construct(\Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder $builder, array $mapping)
    {
    }
    /**
     * Sets length.
     *
     * @param int $length
     *
     * @return $this
     */
    public function length($length)
    {
    }
    /**
     * Sets nullable.
     *
     * @param bool $flag
     *
     * @return $this
     */
    public function nullable($flag = true)
    {
    }
    /**
     * Sets Unique.
     *
     * @param bool $flag
     *
     * @return $this
     */
    public function unique($flag = true)
    {
    }
    /**
     * Sets column name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function columnName($name)
    {
    }
    /**
     * Sets Precision.
     *
     * @param int $p
     *
     * @return $this
     */
    public function precision($p)
    {
    }
    /**
     * Sets insertable.
     *
     * @return $this
     */
    public function insertable(bool $flag = true) : self
    {
    }
    /**
     * Sets updatable.
     *
     * @return $this
     */
    public function updatable(bool $flag = true) : self
    {
    }
    /**
     * Sets scale.
     *
     * @param int $s
     *
     * @return $this
     */
    public function scale($s)
    {
    }
    /**
     * Sets field as primary key.
     *
     * @deprecated Use makePrimaryKey() instead
     *
     * @return FieldBuilder
     */
    public function isPrimaryKey()
    {
    }
    /**
     * Sets field as primary key.
     *
     * @return $this
     */
    public function makePrimaryKey()
    {
    }
    /**
     * Sets an option.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function option($name, $value)
    {
    }
    /**
     * @param string $strategy
     *
     * @return $this
     */
    public function generatedValue($strategy = 'AUTO')
    {
    }
    /**
     * Sets field versioned.
     *
     * @return $this
     */
    public function isVersionField()
    {
    }
    /**
     * Sets Sequence Generator.
     *
     * @param string $sequenceName
     * @param int    $allocationSize
     * @param int    $initialValue
     *
     * @return $this
     */
    public function setSequenceGenerator($sequenceName, $allocationSize = 1, $initialValue = 1)
    {
    }
    /**
     * Sets column definition.
     *
     * @param string $def
     *
     * @return $this
     */
    public function columnDefinition($def)
    {
    }
    /**
     * Set the FQCN of the custom ID generator.
     * This class must extend \Doctrine\ORM\Id\AbstractIdGenerator.
     *
     * @param string $customIdGenerator
     *
     * @return $this
     */
    public function setCustomIdGenerator($customIdGenerator)
    {
    }
    /**
     * Finalizes this field and attach it to the ClassMetadata.
     *
     * Without this call a FieldBuilder has no effect on the ClassMetadata.
     *
     * @return ClassMetadataBuilder
     */
    public function build()
    {
    }
}
