<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class ChangeTrackingPolicy implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * The change tracking policy.
     *
     * @var string
     * @Enum({"DEFERRED_IMPLICIT", "DEFERRED_EXPLICIT", "NOTIFY"})
     */
    public $value;
    public function __construct(string $value)
    {
    }
}
