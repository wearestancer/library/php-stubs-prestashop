<?php

namespace Doctrine\ORM\Mapping\Driver;

class AttributeDriver extends \Doctrine\ORM\Mapping\Driver\CompatibilityAnnotationDriver
{
    use \Doctrine\Persistence\Mapping\Driver\ColocatedMappingDriver;
    /** @var array<string,int> */
    // @phpcs:ignore
    protected $entityAnnotationClasses = [\Doctrine\ORM\Mapping\Entity::class => 1, \Doctrine\ORM\Mapping\MappedSuperclass::class => 2];
    /**
     * The annotation reader.
     *
     * @internal this property will be private in 3.0
     *
     * @var AttributeReader
     */
    protected $reader;
    /**
     * @param array<string> $paths
     */
    public function __construct(array $paths)
    {
    }
    /**
     * Retrieve the current annotation reader
     *
     * @deprecated no replacement planned.
     *
     * @return AttributeReader
     */
    public function getReader()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isTransient($className)
    {
    }
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata) : void
    {
    }
}
