<?php

namespace Doctrine\ORM\Mapping;

/**
 * This annotation is used to override association mappings of relationship properties.
 *
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class AssociationOverrides implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * Mapping overrides of relationship properties.
     *
     * @var array<AssociationOverride>
     */
    public $overrides = [];
    /**
     * @param array<AssociationOverride>|AssociationOverride $overrides
     */
    public function __construct($overrides)
    {
    }
}
