<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class JoinTable implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $name;
    /** @var string|null */
    public $schema;
    /** @var array<\Doctrine\ORM\Mapping\JoinColumn> */
    public $joinColumns = [];
    /** @var array<\Doctrine\ORM\Mapping\JoinColumn> */
    public $inverseJoinColumns = [];
    public function __construct(?string $name = null, ?string $schema = null, $joinColumns = [], $inverseJoinColumns = [])
    {
    }
}
