<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class OneToOne implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var class-string|null */
    public $targetEntity;
    /** @var string|null */
    public $mappedBy;
    /** @var string|null */
    public $inversedBy;
    /** @var array<string>|null */
    public $cascade;
    /**
     * The fetching strategy to use for the association.
     *
     * @var string
     * @Enum({"LAZY", "EAGER", "EXTRA_LAZY"})
     */
    public $fetch = 'LAZY';
    /** @var bool */
    public $orphanRemoval = false;
    /**
     * @param class-string|null  $targetEntity
     * @param array<string>|null $cascade
     */
    public function __construct(?string $mappedBy = null, ?string $inversedBy = null, ?string $targetEntity = null, ?array $cascade = null, string $fetch = 'LAZY', bool $orphanRemoval = false)
    {
    }
}
