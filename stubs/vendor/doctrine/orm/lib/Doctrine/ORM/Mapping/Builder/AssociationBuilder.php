<?php

namespace Doctrine\ORM\Mapping\Builder;

class AssociationBuilder
{
    /** @var ClassMetadataBuilder */
    protected $builder;
    /** @var mixed[] */
    protected $mapping;
    /** @var mixed[]|null */
    protected $joinColumns;
    /** @var int */
    protected $type;
    /**
     * @param mixed[] $mapping
     * @param int     $type
     */
    public function __construct(\Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder $builder, array $mapping, $type)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return $this
     */
    public function mappedBy($fieldName)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return $this
     */
    public function inversedBy($fieldName)
    {
    }
    /**
     * @return $this
     */
    public function cascadeAll()
    {
    }
    /**
     * @return $this
     */
    public function cascadePersist()
    {
    }
    /**
     * @return $this
     */
    public function cascadeRemove()
    {
    }
    /**
     * @return $this
     */
    public function cascadeMerge()
    {
    }
    /**
     * @return $this
     */
    public function cascadeDetach()
    {
    }
    /**
     * @return $this
     */
    public function cascadeRefresh()
    {
    }
    /**
     * @return $this
     */
    public function fetchExtraLazy()
    {
    }
    /**
     * @return $this
     */
    public function fetchEager()
    {
    }
    /**
     * @return $this
     */
    public function fetchLazy()
    {
    }
    /**
     * Add Join Columns.
     *
     * @param string      $columnName
     * @param string      $referencedColumnName
     * @param bool        $nullable
     * @param bool        $unique
     * @param string|null $onDelete
     * @param string|null $columnDef
     *
     * @return $this
     */
    public function addJoinColumn($columnName, $referencedColumnName, $nullable = true, $unique = false, $onDelete = null, $columnDef = null)
    {
    }
    /**
     * Sets field as primary key.
     *
     * @return $this
     */
    public function makePrimaryKey()
    {
    }
    /**
     * Removes orphan entities when detached from their parent.
     *
     * @return $this
     */
    public function orphanRemoval()
    {
    }
    /**
     * @return ClassMetadataBuilder
     *
     * @throws InvalidArgumentException
     */
    public function build()
    {
    }
}
