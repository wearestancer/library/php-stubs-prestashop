<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class ManyToMany implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var class-string|null */
    public $targetEntity;
    /** @var string|null */
    public $mappedBy;
    /** @var string|null */
    public $inversedBy;
    /** @var string[]|null */
    public $cascade;
    /**
     * The fetching strategy to use for the association.
     *
     * @var string
     * @Enum({"LAZY", "EAGER", "EXTRA_LAZY"})
     */
    public $fetch = 'LAZY';
    /** @var bool */
    public $orphanRemoval = false;
    /** @var string|null */
    public $indexBy;
    /**
     * @param class-string|null $targetEntity
     * @param string[]|null     $cascade
     */
    public function __construct(?string $targetEntity = null, ?string $mappedBy = null, ?string $inversedBy = null, ?array $cascade = null, string $fetch = 'LAZY', bool $orphanRemoval = false, ?string $indexBy = null)
    {
    }
}
