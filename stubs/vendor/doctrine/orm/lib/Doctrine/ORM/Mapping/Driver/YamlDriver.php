<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * The YamlDriver reads the mapping metadata from yaml schema files.
 *
 * @deprecated 2.7 This class is being removed from the ORM and won't have any replacement
 */
class YamlDriver extends \Doctrine\Persistence\Mapping\Driver\FileDriver
{
    public const DEFAULT_FILE_EXTENSION = '.dcm.yml';
    /**
     * {@inheritDoc}
     */
    public function __construct($locator, $fileExtension = self::DEFAULT_FILE_EXTENSION)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function loadMappingFile($file)
    {
    }
}
