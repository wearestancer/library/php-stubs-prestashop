<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * XmlDriver that additionally looks for mapping information in a global file.
 */
class SimplifiedXmlDriver extends \Doctrine\ORM\Mapping\Driver\XmlDriver
{
    public const DEFAULT_FILE_EXTENSION = '.orm.xml';
    /**
     * {@inheritDoc}
     */
    public function __construct($prefixes, $fileExtension = self::DEFAULT_FILE_EXTENSION)
    {
    }
}
