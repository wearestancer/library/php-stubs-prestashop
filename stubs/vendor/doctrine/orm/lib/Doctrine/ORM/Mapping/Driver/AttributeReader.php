<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * @internal
 */
final class AttributeReader
{
    /**
     * @psalm-return class-string-map<T, T|RepeatableAttributeCollection<T>>
     *
     * @template T of Annotation
     */
    public function getClassAnnotations(\ReflectionClass $class) : array
    {
    }
    /**
     * @return class-string-map<T, T|RepeatableAttributeCollection<T>>
     *
     * @template T of Annotation
     */
    public function getMethodAnnotations(\ReflectionMethod $method) : array
    {
    }
    /**
     * @return class-string-map<T, T|RepeatableAttributeCollection<T>>
     *
     * @template T of Annotation
     */
    public function getPropertyAnnotations(\ReflectionProperty $property) : array
    {
    }
    /**
     * @param class-string<T> $annotationName The name of the annotation.
     *
     * @return T|null
     *
     * @template T of Annotation
     */
    public function getPropertyAnnotation(\ReflectionProperty $property, $annotationName)
    {
    }
    /**
     * @param class-string<T> $annotationName The name of the annotation.
     *
     * @return RepeatableAttributeCollection<T>
     *
     * @template T of Annotation
     */
    public function getPropertyAnnotationCollection(\ReflectionProperty $property, string $annotationName) : \Doctrine\ORM\Mapping\Driver\RepeatableAttributeCollection
    {
    }
}
