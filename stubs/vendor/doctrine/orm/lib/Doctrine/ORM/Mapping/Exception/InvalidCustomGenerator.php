<?php

namespace Doctrine\ORM\Mapping\Exception;

final class InvalidCustomGenerator extends \Doctrine\ORM\Exception\ORMException
{
    public static function onClassNotConfigured() : self
    {
    }
    /**
     * @param mixed[] $definition
     */
    public static function onMissingClass(array $definition) : self
    {
    }
}
