<?php

namespace Doctrine\ORM\Mapping\Builder;

/**
 * Embedded Builder
 *
 * @link        www.doctrine-project.com
 */
class EmbeddedBuilder
{
    /**
     * @param mixed[] $mapping
     */
    public function __construct(\Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder $builder, array $mapping)
    {
    }
    /**
     * Sets the column prefix for all of the embedded columns.
     *
     * @param string $columnPrefix
     *
     * @return $this
     */
    public function setColumnPrefix($columnPrefix)
    {
    }
    /**
     * Finalizes this embeddable and attach it to the ClassMetadata.
     *
     * Without this call an EmbeddedBuilder has no effect on the ClassMetadata.
     *
     * @return ClassMetadataBuilder
     */
    public function build()
    {
    }
}
