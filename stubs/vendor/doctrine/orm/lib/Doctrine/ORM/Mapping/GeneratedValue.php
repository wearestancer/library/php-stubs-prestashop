<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class GeneratedValue implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * The type of Id generator.
     *
     * @var string
     * @Enum({"AUTO", "SEQUENCE", "TABLE", "IDENTITY", "NONE", "UUID", "CUSTOM"})
     */
    public $strategy = 'AUTO';
    public function __construct(string $strategy = 'AUTO')
    {
    }
}
