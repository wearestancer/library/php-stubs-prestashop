<?php

namespace Doctrine\ORM\Mapping;

/**
 * A <tt>ClassMetadata</tt> instance holds all the object-relational mapping metadata
 * of an entity and its associations.
 *
 * Once populated, ClassMetadata instances are usually cached in a serialized form.
 *
 * <b>IMPORTANT NOTE:</b>
 *
 * The fields of this class are only public for 2 reasons:
 * 1) To allow fast READ access.
 * 2) To drastically reduce the size of a serialized instance (private/protected members
 *    get the whole class name, namespace inclusive, prepended to every property in
 *    the serialized representation).
 *
 * @template-covariant T of object
 * @template-implements ClassMetadata<T>
 * @psalm-type FieldMapping = array{
 *      type: string,
 *      fieldName: string,
 *      columnName: string,
 *      length?: int,
 *      id?: bool,
 *      nullable?: bool,
 *      notInsertable?: bool,
 *      notUpdatable?: bool,
 *      generated?: string,
 *      enumType?: class-string<BackedEnum>,
 *      columnDefinition?: string,
 *      precision?: int,
 *      scale?: int,
 *      unique?: string,
 *      inherited?: class-string,
 *      originalClass?: class-string,
 *      originalField?: string,
 *      quoted?: bool,
 *      requireSQLConversion?: bool,
 *      declared?: class-string,
 *      declaredField?: string,
 *      options?: array<string, mixed>
 * }
 */
class ClassMetadataInfo implements \Doctrine\Persistence\Mapping\ClassMetadata
{
    /* The inheritance mapping types */
    /**
     * NONE means the class does not participate in an inheritance hierarchy
     * and therefore does not need an inheritance mapping type.
     */
    public const INHERITANCE_TYPE_NONE = 1;
    /**
     * JOINED means the class will be persisted according to the rules of
     * <tt>Class Table Inheritance</tt>.
     */
    public const INHERITANCE_TYPE_JOINED = 2;
    /**
     * SINGLE_TABLE means the class will be persisted according to the rules of
     * <tt>Single Table Inheritance</tt>.
     */
    public const INHERITANCE_TYPE_SINGLE_TABLE = 3;
    /**
     * TABLE_PER_CLASS means the class will be persisted according to the rules
     * of <tt>Concrete Table Inheritance</tt>.
     */
    public const INHERITANCE_TYPE_TABLE_PER_CLASS = 4;
    /* The Id generator types. */
    /**
     * AUTO means the generator type will depend on what the used platform prefers.
     * Offers full portability.
     */
    public const GENERATOR_TYPE_AUTO = 1;
    /**
     * SEQUENCE means a separate sequence object will be used. Platforms that do
     * not have native sequence support may emulate it. Full portability is currently
     * not guaranteed.
     */
    public const GENERATOR_TYPE_SEQUENCE = 2;
    /**
     * TABLE means a separate table is used for id generation.
     * Offers full portability (in that it results in an exception being thrown
     * no matter the platform).
     *
     * @deprecated no replacement planned
     */
    public const GENERATOR_TYPE_TABLE = 3;
    /**
     * IDENTITY means an identity column is used for id generation. The database
     * will fill in the id column on insertion. Platforms that do not support
     * native identity columns may emulate them. Full portability is currently
     * not guaranteed.
     */
    public const GENERATOR_TYPE_IDENTITY = 4;
    /**
     * NONE means the class does not have a generated id. That means the class
     * must have a natural, manually assigned id.
     */
    public const GENERATOR_TYPE_NONE = 5;
    /**
     * UUID means that a UUID/GUID expression is used for id generation. Full
     * portability is currently not guaranteed.
     *
     * @deprecated use an application-side generator instead
     */
    public const GENERATOR_TYPE_UUID = 6;
    /**
     * CUSTOM means that customer will use own ID generator that supposedly work
     */
    public const GENERATOR_TYPE_CUSTOM = 7;
    /**
     * DEFERRED_IMPLICIT means that changes of entities are calculated at commit-time
     * by doing a property-by-property comparison with the original data. This will
     * be done for all entities that are in MANAGED state at commit-time.
     *
     * This is the default change tracking policy.
     */
    public const CHANGETRACKING_DEFERRED_IMPLICIT = 1;
    /**
     * DEFERRED_EXPLICIT means that changes of entities are calculated at commit-time
     * by doing a property-by-property comparison with the original data. This will
     * be done only for entities that were explicitly saved (through persist() or a cascade).
     */
    public const CHANGETRACKING_DEFERRED_EXPLICIT = 2;
    /**
     * NOTIFY means that Doctrine relies on the entities sending out notifications
     * when their properties change. Such entity classes must implement
     * the <tt>NotifyPropertyChanged</tt> interface.
     */
    public const CHANGETRACKING_NOTIFY = 3;
    /**
     * Specifies that an association is to be fetched when it is first accessed.
     */
    public const FETCH_LAZY = 2;
    /**
     * Specifies that an association is to be fetched when the owner of the
     * association is fetched.
     */
    public const FETCH_EAGER = 3;
    /**
     * Specifies that an association is to be fetched lazy (on first access) and that
     * commands such as Collection#count, Collection#slice are issued directly against
     * the database if the collection is not yet initialized.
     */
    public const FETCH_EXTRA_LAZY = 4;
    /**
     * Identifies a one-to-one association.
     */
    public const ONE_TO_ONE = 1;
    /**
     * Identifies a many-to-one association.
     */
    public const MANY_TO_ONE = 2;
    /**
     * Identifies a one-to-many association.
     */
    public const ONE_TO_MANY = 4;
    /**
     * Identifies a many-to-many association.
     */
    public const MANY_TO_MANY = 8;
    /**
     * Combined bitmask for to-one (single-valued) associations.
     */
    public const TO_ONE = 3;
    /**
     * Combined bitmask for to-many (collection-valued) associations.
     */
    public const TO_MANY = 12;
    /**
     * ReadOnly cache can do reads, inserts and deletes, cannot perform updates or employ any locks,
     */
    public const CACHE_USAGE_READ_ONLY = 1;
    /**
     * Nonstrict Read Write Cache doesn’t employ any locks but can do inserts, update and deletes.
     */
    public const CACHE_USAGE_NONSTRICT_READ_WRITE = 2;
    /**
     * Read Write Attempts to lock the entity before update/delete.
     */
    public const CACHE_USAGE_READ_WRITE = 3;
    /**
     * The value of this column is never generated by the database.
     */
    public const GENERATED_NEVER = 0;
    /**
     * The value of this column is generated by the database on INSERT, but not on UPDATE.
     */
    public const GENERATED_INSERT = 1;
    /**
     * The value of this column is generated by the database on both INSERT and UDPATE statements.
     */
    public const GENERATED_ALWAYS = 2;
    /**
     * READ-ONLY: The name of the entity class.
     *
     * @var string
     * @psalm-var class-string<T>
     */
    public $name;
    /**
     * READ-ONLY: The namespace the entity class is contained in.
     *
     * @var string
     * @todo Not really needed. Usage could be localized.
     */
    public $namespace;
    /**
     * READ-ONLY: The name of the entity class that is at the root of the mapped entity inheritance
     * hierarchy. If the entity is not part of a mapped inheritance hierarchy this is the same
     * as {@link $name}.
     *
     * @var string
     * @psalm-var class-string
     */
    public $rootEntityName;
    /**
     * READ-ONLY: The definition of custom generator. Only used for CUSTOM
     * generator type
     *
     * The definition has the following structure:
     * <code>
     * array(
     *     'class' => 'ClassName',
     * )
     * </code>
     *
     * @todo Merge with tableGeneratorDefinition into generic generatorDefinition
     * @var array<string, string>|null
     */
    public $customGeneratorDefinition;
    /**
     * The name of the custom repository class used for the entity class.
     * (Optional).
     *
     * @var string|null
     * @psalm-var ?class-string<EntityRepository>
     */
    public $customRepositoryClassName;
    /**
     * READ-ONLY: Whether this class describes the mapping of a mapped superclass.
     *
     * @var bool
     */
    public $isMappedSuperclass = false;
    /**
     * READ-ONLY: Whether this class describes the mapping of an embeddable class.
     *
     * @var bool
     */
    public $isEmbeddedClass = false;
    /**
     * READ-ONLY: The names of the parent classes (ancestors).
     *
     * @psalm-var list<class-string>
     */
    public $parentClasses = [];
    /**
     * READ-ONLY: The names of all subclasses (descendants).
     *
     * @psalm-var list<class-string>
     */
    public $subClasses = [];
    /**
     * READ-ONLY: The names of all embedded classes based on properties.
     *
     * @psalm-var array<string, mixed[]>
     */
    public $embeddedClasses = [];
    /**
     * READ-ONLY: The named queries allowed to be called directly from Repository.
     *
     * @psalm-var array<string, array<string, mixed>>
     */
    public $namedQueries = [];
    /**
     * READ-ONLY: The named native queries allowed to be called directly from Repository.
     *
     * A native SQL named query definition has the following structure:
     * <pre>
     * array(
     *     'name'               => <query name>,
     *     'query'              => <sql query>,
     *     'resultClass'        => <class of the result>,
     *     'resultSetMapping'   => <name of a SqlResultSetMapping>
     * )
     * </pre>
     *
     * @psalm-var array<string, array<string, mixed>>
     */
    public $namedNativeQueries = [];
    /**
     * READ-ONLY: The mappings of the results of native SQL queries.
     *
     * A native result mapping definition has the following structure:
     * <pre>
     * array(
     *     'name'               => <result name>,
     *     'entities'           => array(<entity result mapping>),
     *     'columns'            => array(<column result mapping>)
     * )
     * </pre>
     *
     * @psalm-var array<string, array{
     *                name: string,
     *                entities: mixed[],
     *                columns: mixed[]
     *            }>
     */
    public $sqlResultSetMappings = [];
    /**
     * READ-ONLY: The field names of all fields that are part of the identifier/primary key
     * of the mapped entity class.
     *
     * @psalm-var list<string>
     */
    public $identifier = [];
    /**
     * READ-ONLY: The inheritance mapping type used by the class.
     *
     * @var int
     * @psalm-var self::INHERITANCE_TYPE_*
     */
    public $inheritanceType = self::INHERITANCE_TYPE_NONE;
    /**
     * READ-ONLY: The Id generator type used by the class.
     *
     * @var int
     * @psalm-var self::GENERATOR_TYPE_*
     */
    public $generatorType = self::GENERATOR_TYPE_NONE;
    /**
     * READ-ONLY: The field mappings of the class.
     * Keys are field names and values are mapping definitions.
     *
     * The mapping definition array has the following values:
     *
     * - <b>fieldName</b> (string)
     * The name of the field in the Entity.
     *
     * - <b>type</b> (string)
     * The type name of the mapped field. Can be one of Doctrine's mapping types
     * or a custom mapping type.
     *
     * - <b>columnName</b> (string, optional)
     * The column name. Optional. Defaults to the field name.
     *
     * - <b>length</b> (integer, optional)
     * The database length of the column. Optional. Default value taken from
     * the type.
     *
     * - <b>id</b> (boolean, optional)
     * Marks the field as the primary key of the entity. Multiple fields of an
     * entity can have the id attribute, forming a composite key.
     *
     * - <b>nullable</b> (boolean, optional)
     * Whether the column is nullable. Defaults to FALSE.
     *
     * - <b>'notInsertable'</b> (boolean, optional)
     * Whether the column is not insertable. Optional. Is only set if value is TRUE.
     *
     * - <b>'notUpdatable'</b> (boolean, optional)
     * Whether the column is updatable. Optional. Is only set if value is TRUE.
     *
     * - <b>columnDefinition</b> (string, optional, schema-only)
     * The SQL fragment that is used when generating the DDL for the column.
     *
     * - <b>precision</b> (integer, optional, schema-only)
     * The precision of a decimal column. Only valid if the column type is decimal.
     *
     * - <b>scale</b> (integer, optional, schema-only)
     * The scale of a decimal column. Only valid if the column type is decimal.
     *
     * - <b>'unique'</b> (string, optional, schema-only)
     * Whether a unique constraint should be generated for the column.
     *
     * @var mixed[]
     * @psalm-var array<string, FieldMapping>
     */
    public $fieldMappings = [];
    /**
     * READ-ONLY: An array of field names. Used to look up field names from column names.
     * Keys are column names and values are field names.
     *
     * @psalm-var array<string, string>
     */
    public $fieldNames = [];
    /**
     * READ-ONLY: A map of field names to column names. Keys are field names and values column names.
     * Used to look up column names from field names.
     * This is the reverse lookup map of $_fieldNames.
     *
     * @deprecated 3.0 Remove this.
     *
     * @var mixed[]
     */
    public $columnNames = [];
    /**
     * READ-ONLY: The discriminator value of this class.
     *
     * <b>This does only apply to the JOINED and SINGLE_TABLE inheritance mapping strategies
     * where a discriminator column is used.</b>
     *
     * @see discriminatorColumn
     *
     * @var mixed
     */
    public $discriminatorValue;
    /**
     * READ-ONLY: The discriminator map of all mapped classes in the hierarchy.
     *
     * <b>This does only apply to the JOINED and SINGLE_TABLE inheritance mapping strategies
     * where a discriminator column is used.</b>
     *
     * @see discriminatorColumn
     *
     * @var mixed
     */
    public $discriminatorMap = [];
    /**
     * READ-ONLY: The definition of the discriminator column used in JOINED and SINGLE_TABLE
     * inheritance mappings.
     *
     * @psalm-var array<string, mixed>|null
     */
    public $discriminatorColumn;
    /**
     * READ-ONLY: The primary table definition. The definition is an array with the
     * following entries:
     *
     * name => <tableName>
     * schema => <schemaName>
     * indexes => array
     * uniqueConstraints => array
     *
     * @var mixed[]
     * @psalm-var array{
     *               name: string,
     *               schema: string,
     *               indexes: array,
     *               uniqueConstraints: array,
     *               options: array<string, mixed>,
     *               quoted?: bool
     *           }
     */
    public $table;
    /**
     * READ-ONLY: The registered lifecycle callbacks for entities of this class.
     *
     * @psalm-var array<string, list<string>>
     */
    public $lifecycleCallbacks = [];
    /**
     * READ-ONLY: The registered entity listeners.
     *
     * @psalm-var array<string, list<array{class: class-string, method: string}>>
     */
    public $entityListeners = [];
    /**
     * READ-ONLY: The association mappings of this class.
     *
     * The mapping definition array supports the following keys:
     *
     * - <b>fieldName</b> (string)
     * The name of the field in the entity the association is mapped to.
     *
     * - <b>targetEntity</b> (string)
     * The class name of the target entity. If it is fully-qualified it is used as is.
     * If it is a simple, unqualified class name the namespace is assumed to be the same
     * as the namespace of the source entity.
     *
     * - <b>mappedBy</b> (string, required for bidirectional associations)
     * The name of the field that completes the bidirectional association on the owning side.
     * This key must be specified on the inverse side of a bidirectional association.
     *
     * - <b>inversedBy</b> (string, required for bidirectional associations)
     * The name of the field that completes the bidirectional association on the inverse side.
     * This key must be specified on the owning side of a bidirectional association.
     *
     * - <b>cascade</b> (array, optional)
     * The names of persistence operations to cascade on the association. The set of possible
     * values are: "persist", "remove", "detach", "merge", "refresh", "all" (implies all others).
     *
     * - <b>orderBy</b> (array, one-to-many/many-to-many only)
     * A map of field names (of the target entity) to sorting directions (ASC/DESC).
     * Example: array('priority' => 'desc')
     *
     * - <b>fetch</b> (integer, optional)
     * The fetching strategy to use for the association, usually defaults to FETCH_LAZY.
     * Possible values are: ClassMetadata::FETCH_EAGER, ClassMetadata::FETCH_LAZY.
     *
     * - <b>joinTable</b> (array, optional, many-to-many only)
     * Specification of the join table and its join columns (foreign keys).
     * Only valid for many-to-many mappings. Note that one-to-many associations can be mapped
     * through a join table by simply mapping the association as many-to-many with a unique
     * constraint on the join table.
     *
     * - <b>indexBy</b> (string, optional, to-many only)
     * Specification of a field on target-entity that is used to index the collection by.
     * This field HAS to be either the primary key or a unique column. Otherwise the collection
     * does not contain all the entities that are actually related.
     *
     * A join table definition has the following structure:
     * <pre>
     * array(
     *     'name' => <join table name>,
     *      'joinColumns' => array(<join column mapping from join table to source table>),
     *      'inverseJoinColumns' => array(<join column mapping from join table to target table>)
     * )
     * </pre>
     *
     * @psalm-var array<string, array<string, mixed>>
     */
    public $associationMappings = [];
    /**
     * READ-ONLY: Flag indicating whether the identifier/primary key of the class is composite.
     *
     * @var bool
     */
    public $isIdentifierComposite = false;
    /**
     * READ-ONLY: Flag indicating whether the identifier/primary key contains at least one foreign key association.
     *
     * This flag is necessary because some code blocks require special treatment of this cases.
     *
     * @var bool
     */
    public $containsForeignIdentifier = false;
    /**
     * READ-ONLY: Flag indicating whether the identifier/primary key contains at least one ENUM type.
     *
     * This flag is necessary because some code blocks require special treatment of this cases.
     *
     * @var bool
     */
    public $containsEnumIdentifier = false;
    /**
     * READ-ONLY: The ID generator used for generating IDs for this class.
     *
     * @var AbstractIdGenerator
     * @todo Remove!
     */
    public $idGenerator;
    /**
     * READ-ONLY: The definition of the sequence generator of this class. Only used for the
     * SEQUENCE generation strategy.
     *
     * The definition has the following structure:
     * <code>
     * array(
     *     'sequenceName' => 'name',
     *     'allocationSize' => '20',
     *     'initialValue' => '1'
     * )
     * </code>
     *
     * @var array<string, mixed>
     * @psalm-var array{sequenceName: string, allocationSize: string, initialValue: string, quoted?: mixed}
     * @todo Merge with tableGeneratorDefinition into generic generatorDefinition
     */
    public $sequenceGeneratorDefinition;
    /**
     * READ-ONLY: The definition of the table generator of this class. Only used for the
     * TABLE generation strategy.
     *
     * @deprecated
     *
     * @var array<string, mixed>
     */
    public $tableGeneratorDefinition;
    /**
     * READ-ONLY: The policy used for change-tracking on entities of this class.
     *
     * @var int
     */
    public $changeTrackingPolicy = self::CHANGETRACKING_DEFERRED_IMPLICIT;
    /**
     * READ-ONLY: A Flag indicating whether one or more columns of this class
     * have to be reloaded after insert / update operations.
     *
     * @var bool
     */
    public $requiresFetchAfterChange = false;
    /**
     * READ-ONLY: A flag for whether or not instances of this class are to be versioned
     * with optimistic locking.
     *
     * @var bool
     */
    public $isVersioned = false;
    /**
     * READ-ONLY: The name of the field which is used for versioning in optimistic locking (if any).
     *
     * @var mixed
     */
    public $versionField;
    /** @var mixed[]|null */
    public $cache;
    /**
     * The ReflectionClass instance of the mapped class.
     *
     * @var ReflectionClass|null
     */
    public $reflClass;
    /**
     * Is this entity marked as "read-only"?
     *
     * That means it is never considered for change-tracking in the UnitOfWork. It is a very helpful performance
     * optimization for entities that are immutable, either in your domain or through the relation database
     * (coming from a view, or a history table for example).
     *
     * @var bool
     */
    public $isReadOnly = false;
    /**
     * NamingStrategy determining the default column and table names.
     *
     * @var NamingStrategy
     */
    protected $namingStrategy;
    /**
     * The ReflectionProperty instances of the mapped class.
     *
     * @var array<string, ReflectionProperty|null>
     */
    public $reflFields = [];
    /**
     * Initializes a new ClassMetadata instance that will hold the object-relational mapping
     * metadata of the class with the given name.
     *
     * @param string $entityName The name of the entity class the new instance is used for.
     * @psalm-param class-string<T> $entityName
     */
    public function __construct($entityName, ?\Doctrine\ORM\Mapping\NamingStrategy $namingStrategy = null)
    {
    }
    /**
     * Gets the ReflectionProperties of the mapped class.
     *
     * @return ReflectionProperty[]|null[] An array of ReflectionProperty instances.
     * @psalm-return array<ReflectionProperty|null>
     */
    public function getReflectionProperties()
    {
    }
    /**
     * Gets a ReflectionProperty for a specific field of the mapped class.
     *
     * @param string $name
     *
     * @return ReflectionProperty
     */
    public function getReflectionProperty($name)
    {
    }
    /**
     * Gets the ReflectionProperty for the single identifier field.
     *
     * @return ReflectionProperty
     *
     * @throws BadMethodCallException If the class has a composite identifier.
     */
    public function getSingleIdReflectionProperty()
    {
    }
    /**
     * Extracts the identifier values of an entity of this class.
     *
     * For composite identifiers, the identifier values are returned as an array
     * with the same order as the field order in {@link identifier}.
     *
     * @param object $entity
     *
     * @return array<string, mixed>
     */
    public function getIdentifierValues($entity)
    {
    }
    /**
     * Populates the entity identifier of an entity.
     *
     * @param object $entity
     * @psalm-param array<string, mixed> $id
     *
     * @return void
     *
     * @todo Rename to assignIdentifier()
     */
    public function setIdentifierValues($entity, array $id)
    {
    }
    /**
     * Sets the specified field to the specified value on the given entity.
     *
     * @param object $entity
     * @param string $field
     * @param mixed  $value
     *
     * @return void
     */
    public function setFieldValue($entity, $field, $value)
    {
    }
    /**
     * Gets the specified field's value off the given entity.
     *
     * @param object $entity
     * @param string $field
     *
     * @return mixed
     */
    public function getFieldValue($entity, $field)
    {
    }
    /**
     * Creates a string representation of this instance.
     *
     * @return string The string representation of this instance.
     *
     * @todo Construct meaningful string representation.
     */
    public function __toString()
    {
    }
    /**
     * Determines which fields get serialized.
     *
     * It is only serialized what is necessary for best unserialization performance.
     * That means any metadata properties that are not set or empty or simply have
     * their default value are NOT serialized.
     *
     * Parts that are also NOT serialized because they can not be properly unserialized:
     *      - reflClass (ReflectionClass)
     *      - reflFields (ReflectionProperty array)
     *
     * @return string[] The names of all the fields that should be serialized.
     */
    public function __sleep()
    {
    }
    /**
     * Creates a new instance of the mapped class, without invoking the constructor.
     *
     * @return object
     */
    public function newInstance()
    {
    }
    /**
     * Restores some state that can not be serialized/unserialized.
     *
     * @param ReflectionService $reflService
     *
     * @return void
     */
    public function wakeupReflection($reflService)
    {
    }
    /**
     * Initializes a new ClassMetadata instance that will hold the object-relational mapping
     * metadata of the class with the given name.
     *
     * @param ReflectionService $reflService The reflection service.
     *
     * @return void
     */
    public function initializeReflection($reflService)
    {
    }
    /**
     * Validates Identifier.
     *
     * @return void
     *
     * @throws MappingException
     */
    public function validateIdentifier()
    {
    }
    /**
     * Validates association targets actually exist.
     *
     * @return void
     *
     * @throws MappingException
     */
    public function validateAssociations()
    {
    }
    /**
     * Validates lifecycle callbacks.
     *
     * @param ReflectionService $reflService
     *
     * @return void
     *
     * @throws MappingException
     */
    public function validateLifecycleCallbacks($reflService)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getReflectionClass()
    {
    }
    /**
     * @psalm-param array{usage?: mixed, region?: mixed} $cache
     *
     * @return void
     */
    public function enableCache(array $cache)
    {
    }
    /**
     * @param string $fieldName
     * @psalm-param array{usage?: int, region?: string} $cache
     *
     * @return void
     */
    public function enableAssociationCache($fieldName, array $cache)
    {
    }
    /**
     * @param string $fieldName
     * @param array  $cache
     * @psalm-param array{usage?: int, region?: string|null} $cache
     *
     * @return int[]|string[]
     * @psalm-return array{usage: int, region: string|null}
     */
    public function getAssociationCacheDefaults($fieldName, array $cache)
    {
    }
    /**
     * Sets the change tracking policy used by this class.
     *
     * @param int $policy
     *
     * @return void
     */
    public function setChangeTrackingPolicy($policy)
    {
    }
    /**
     * Whether the change tracking policy of this class is "deferred explicit".
     *
     * @return bool
     */
    public function isChangeTrackingDeferredExplicit()
    {
    }
    /**
     * Whether the change tracking policy of this class is "deferred implicit".
     *
     * @return bool
     */
    public function isChangeTrackingDeferredImplicit()
    {
    }
    /**
     * Whether the change tracking policy of this class is "notify".
     *
     * @return bool
     */
    public function isChangeTrackingNotify()
    {
    }
    /**
     * Checks whether a field is part of the identifier/primary key field(s).
     *
     * @param string $fieldName The field name.
     *
     * @return bool TRUE if the field is part of the table identifier/primary key field(s),
     * FALSE otherwise.
     */
    public function isIdentifier($fieldName)
    {
    }
    /**
     * Checks if the field is unique.
     *
     * @param string $fieldName The field name.
     *
     * @return bool TRUE if the field is unique, FALSE otherwise.
     */
    public function isUniqueField($fieldName)
    {
    }
    /**
     * Checks if the field is not null.
     *
     * @param string $fieldName The field name.
     *
     * @return bool TRUE if the field is not null, FALSE otherwise.
     */
    public function isNullable($fieldName)
    {
    }
    /**
     * Gets a column name for a field name.
     * If the column name for the field cannot be found, the given field name
     * is returned.
     *
     * @param string $fieldName The field name.
     *
     * @return string The column name.
     */
    public function getColumnName($fieldName)
    {
    }
    /**
     * Gets the mapping of a (regular) field that holds some data but not a
     * reference to another object.
     *
     * @param string $fieldName The field name.
     *
     * @return mixed[] The field mapping.
     * @psalm-return FieldMapping
     *
     * @throws MappingException
     */
    public function getFieldMapping($fieldName)
    {
    }
    /**
     * Gets the mapping of an association.
     *
     * @see ClassMetadataInfo::$associationMappings
     *
     * @param string $fieldName The field name that represents the association in
     *                          the object model.
     *
     * @return mixed[] The mapping.
     * @psalm-return array<string, mixed>
     *
     * @throws MappingException
     */
    public function getAssociationMapping($fieldName)
    {
    }
    /**
     * Gets all association mappings of the class.
     *
     * @psalm-return array<string, array<string, mixed>>
     */
    public function getAssociationMappings()
    {
    }
    /**
     * Gets the field name for a column name.
     * If no field name can be found the column name is returned.
     *
     * @param string $columnName The column name.
     *
     * @return string The column alias.
     */
    public function getFieldName($columnName)
    {
    }
    /**
     * Gets the named query.
     *
     * @see ClassMetadataInfo::$namedQueries
     *
     * @param string $queryName The query name.
     *
     * @return string
     *
     * @throws MappingException
     */
    public function getNamedQuery($queryName)
    {
    }
    /**
     * Gets all named queries of the class.
     *
     * @return mixed[][]
     * @psalm-return array<string, array<string, mixed>>
     */
    public function getNamedQueries()
    {
    }
    /**
     * Gets the named native query.
     *
     * @see ClassMetadataInfo::$namedNativeQueries
     *
     * @param string $queryName The query name.
     *
     * @return mixed[]
     * @psalm-return array<string, mixed>
     *
     * @throws MappingException
     */
    public function getNamedNativeQuery($queryName)
    {
    }
    /**
     * Gets all named native queries of the class.
     *
     * @psalm-return array<string, array<string, mixed>>
     */
    public function getNamedNativeQueries()
    {
    }
    /**
     * Gets the result set mapping.
     *
     * @see ClassMetadataInfo::$sqlResultSetMappings
     *
     * @param string $name The result set mapping name.
     *
     * @return mixed[]
     * @psalm-return array{name: string, entities: array, columns: array}
     *
     * @throws MappingException
     */
    public function getSqlResultSetMapping($name)
    {
    }
    /**
     * Gets all sql result set mappings of the class.
     *
     * @return mixed[]
     * @psalm-return array<string, array{name: string, entities: array, columns: array}>
     */
    public function getSqlResultSetMappings()
    {
    }
    /**
     * Validates & completes the given field mapping.
     *
     * @psalm-param array<string, mixed> $mapping The field mapping to validate & complete.
     *
     * @return mixed[] The updated mapping.
     *
     * @throws MappingException
     */
    protected function validateAndCompleteFieldMapping(array $mapping) : array
    {
    }
    /**
     * Validates & completes the basic mapping information that is common to all
     * association mappings (one-to-one, many-ot-one, one-to-many, many-to-many).
     *
     * @psalm-param array<string, mixed> $mapping The mapping.
     *
     * @return mixed[] The updated mapping.
     * @psalm-return array{
     *                   mappedBy: mixed|null,
     *                   inversedBy: mixed|null,
     *                   isOwningSide: bool,
     *                   sourceEntity: class-string,
     *                   targetEntity: string,
     *                   fieldName: mixed,
     *                   fetch: mixed,
     *                   cascade: array<array-key,string>,
     *                   isCascadeRemove: bool,
     *                   isCascadePersist: bool,
     *                   isCascadeRefresh: bool,
     *                   isCascadeMerge: bool,
     *                   isCascadeDetach: bool,
     *                   type: int,
     *                   originalField: string,
     *                   originalClass: class-string,
     *                   ?orphanRemoval: bool
     *               }
     *
     * @throws MappingException If something is wrong with the mapping.
     */
    protected function _validateAndCompleteAssociationMapping(array $mapping)
    {
    }
    /**
     * Validates & completes a one-to-one association mapping.
     *
     * @psalm-param array<string, mixed> $mapping The mapping to validate & complete.
     * @psalm-param array<string, mixed> $mapping The mapping to validate & complete.
     *
     * @return mixed[] The validated & completed mapping.
     * @psalm-return array{isOwningSide: mixed, orphanRemoval: bool, isCascadeRemove: bool}
     * @psalm-return array{
     *      mappedBy: mixed|null,
     *      inversedBy: mixed|null,
     *      isOwningSide: bool,
     *      sourceEntity: class-string,
     *      targetEntity: string,
     *      fieldName: mixed,
     *      fetch: mixed,
     *      cascade: array<string>,
     *      isCascadeRemove: bool,
     *      isCascadePersist: bool,
     *      isCascadeRefresh: bool,
     *      isCascadeMerge: bool,
     *      isCascadeDetach: bool,
     *      type: int,
     *      originalField: string,
     *      originalClass: class-string,
     *      joinColumns?: array{0: array{name: string, referencedColumnName: string}}|mixed,
     *      id?: mixed,
     *      sourceToTargetKeyColumns?: array,
     *      joinColumnFieldNames?: array,
     *      targetToSourceKeyColumns?: array<array-key>,
     *      orphanRemoval: bool
     * }
     *
     * @throws RuntimeException
     * @throws MappingException
     */
    protected function _validateAndCompleteOneToOneMapping(array $mapping)
    {
    }
    /**
     * Validates & completes a one-to-many association mapping.
     *
     * @psalm-param array<string, mixed> $mapping The mapping to validate and complete.
     *
     * @return mixed[] The validated and completed mapping.
     * @psalm-return array{
     *                   mappedBy: mixed,
     *                   inversedBy: mixed,
     *                   isOwningSide: bool,
     *                   sourceEntity: string,
     *                   targetEntity: string,
     *                   fieldName: mixed,
     *                   fetch: int|mixed,
     *                   cascade: array<array-key,string>,
     *                   isCascadeRemove: bool,
     *                   isCascadePersist: bool,
     *                   isCascadeRefresh: bool,
     *                   isCascadeMerge: bool,
     *                   isCascadeDetach: bool,
     *                   orphanRemoval: bool
     *               }
     *
     * @throws MappingException
     * @throws InvalidArgumentException
     */
    protected function _validateAndCompleteOneToManyMapping(array $mapping)
    {
    }
    /**
     * Validates & completes a many-to-many association mapping.
     *
     * @psalm-param array<string, mixed> $mapping The mapping to validate & complete.
     * @psalm-param array<string, mixed> $mapping The mapping to validate & complete.
     *
     * @return mixed[] The validated & completed mapping.
     * @psalm-return array{
     *      mappedBy: mixed,
     *      inversedBy: mixed,
     *      isOwningSide: bool,
     *      sourceEntity: class-string,
     *      targetEntity: string,
     *      fieldName: mixed,
     *      fetch: mixed,
     *      cascade: array<string>,
     *      isCascadeRemove: bool,
     *      isCascadePersist: bool,
     *      isCascadeRefresh: bool,
     *      isCascadeMerge: bool,
     *      isCascadeDetach: bool,
     *      type: int,
     *      originalField: string,
     *      originalClass: class-string,
     *      joinTable?: array{inverseJoinColumns: mixed}|mixed,
     *      joinTableColumns?: list<mixed>,
     *      isOnDeleteCascade?: true,
     *      relationToSourceKeyColumns?: array,
     *      relationToTargetKeyColumns?: array,
     *      orphanRemoval: bool
     * }
     *
     * @throws InvalidArgumentException
     */
    protected function _validateAndCompleteManyToManyMapping(array $mapping)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIdentifierFieldNames()
    {
    }
    /**
     * Gets the name of the single id field. Note that this only works on
     * entity classes that have a single-field pk.
     *
     * @return string
     *
     * @throws MappingException If the class doesn't have an identifier or it has a composite primary key.
     */
    public function getSingleIdentifierFieldName()
    {
    }
    /**
     * Gets the column name of the single id column. Note that this only works on
     * entity classes that have a single-field pk.
     *
     * @return string
     *
     * @throws MappingException If the class doesn't have an identifier or it has a composite primary key.
     */
    public function getSingleIdentifierColumnName()
    {
    }
    /**
     * INTERNAL:
     * Sets the mapped identifier/primary key fields of this class.
     * Mainly used by the ClassMetadataFactory to assign inherited identifiers.
     *
     * @psalm-param list<mixed> $identifier
     *
     * @return void
     */
    public function setIdentifier(array $identifier)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getIdentifier()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasField($fieldName)
    {
    }
    /**
     * Gets an array containing all the column names.
     *
     * @psalm-param list<string>|null $fieldNames
     *
     * @return mixed[]
     * @psalm-return list<string>
     */
    public function getColumnNames(?array $fieldNames = null)
    {
    }
    /**
     * Returns an array with all the identifier column names.
     *
     * @psalm-return list<string>
     */
    public function getIdentifierColumnNames()
    {
    }
    /**
     * Sets the type of Id generator to use for the mapped class.
     *
     * @param int $generatorType
     * @psalm-param self::GENERATOR_TYPE_* $generatorType
     *
     * @return void
     */
    public function setIdGeneratorType($generatorType)
    {
    }
    /**
     * Checks whether the mapped class uses an Id generator.
     *
     * @return bool TRUE if the mapped class uses an Id generator, FALSE otherwise.
     */
    public function usesIdGenerator()
    {
    }
    /**
     * @return bool
     */
    public function isInheritanceTypeNone()
    {
    }
    /**
     * Checks whether the mapped class uses the JOINED inheritance mapping strategy.
     *
     * @return bool TRUE if the class participates in a JOINED inheritance mapping,
     * FALSE otherwise.
     */
    public function isInheritanceTypeJoined()
    {
    }
    /**
     * Checks whether the mapped class uses the SINGLE_TABLE inheritance mapping strategy.
     *
     * @return bool TRUE if the class participates in a SINGLE_TABLE inheritance mapping,
     * FALSE otherwise.
     */
    public function isInheritanceTypeSingleTable()
    {
    }
    /**
     * Checks whether the mapped class uses the TABLE_PER_CLASS inheritance mapping strategy.
     *
     * @return bool TRUE if the class participates in a TABLE_PER_CLASS inheritance mapping,
     * FALSE otherwise.
     */
    public function isInheritanceTypeTablePerClass()
    {
    }
    /**
     * Checks whether the class uses an identity column for the Id generation.
     *
     * @return bool TRUE if the class uses the IDENTITY generator, FALSE otherwise.
     */
    public function isIdGeneratorIdentity()
    {
    }
    /**
     * Checks whether the class uses a sequence for id generation.
     *
     * @return bool TRUE if the class uses the SEQUENCE generator, FALSE otherwise.
     */
    public function isIdGeneratorSequence()
    {
    }
    /**
     * Checks whether the class uses a table for id generation.
     *
     * @deprecated
     *
     * @return false
     */
    public function isIdGeneratorTable()
    {
    }
    /**
     * Checks whether the class has a natural identifier/pk (which means it does
     * not use any Id generator.
     *
     * @return bool
     */
    public function isIdentifierNatural()
    {
    }
    /**
     * Checks whether the class use a UUID for id generation.
     *
     * @deprecated
     *
     * @return bool
     */
    public function isIdentifierUuid()
    {
    }
    /**
     * Gets the type of a field.
     *
     * @param string $fieldName
     *
     * @return string|null
     *
     * @todo 3.0 Remove this. PersisterHelper should fix it somehow
     */
    public function getTypeOfField($fieldName)
    {
    }
    /**
     * Gets the type of a column.
     *
     * @deprecated 3.0 remove this. this method is bogus and unreliable, since it cannot resolve the type of a column
     *             that is derived by a referenced field on a different entity.
     *
     * @param string $columnName
     *
     * @return string|null
     */
    public function getTypeOfColumn($columnName)
    {
    }
    /**
     * Gets the name of the primary table.
     *
     * @return string
     */
    public function getTableName()
    {
    }
    /**
     * Gets primary table's schema name.
     *
     * @return string|null
     */
    public function getSchemaName()
    {
    }
    /**
     * Gets the table name to use for temporary identifier tables of this class.
     *
     * @return string
     */
    public function getTemporaryIdTableName()
    {
    }
    /**
     * Sets the mapped subclasses of this class.
     *
     * @psalm-param list<string> $subclasses The names of all mapped subclasses.
     *
     * @return void
     */
    public function setSubclasses(array $subclasses)
    {
    }
    /**
     * Sets the parent class names.
     * Assumes that the class names in the passed array are in the order:
     * directParent -> directParentParent -> directParentParentParent ... -> root.
     *
     * @psalm-param list<class-string> $classNames
     *
     * @return void
     */
    public function setParentClasses(array $classNames)
    {
    }
    /**
     * Sets the inheritance type used by the class and its subclasses.
     *
     * @param int $type
     * @psalm-param self::INHERITANCE_TYPE_* $type
     *
     * @return void
     *
     * @throws MappingException
     */
    public function setInheritanceType($type)
    {
    }
    /**
     * Sets the association to override association mapping of property for an entity relationship.
     *
     * @param string $fieldName
     * @psalm-param array<string, mixed> $overrideMapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function setAssociationOverride($fieldName, array $overrideMapping)
    {
    }
    /**
     * Sets the override for a mapped field.
     *
     * @param string $fieldName
     * @psalm-param array<string, mixed> $overrideMapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function setAttributeOverride($fieldName, array $overrideMapping)
    {
    }
    /**
     * Checks whether a mapped field is inherited from an entity superclass.
     *
     * @param string $fieldName
     *
     * @return bool TRUE if the field is inherited, FALSE otherwise.
     */
    public function isInheritedField($fieldName)
    {
    }
    /**
     * Checks if this entity is the root in any entity-inheritance-hierarchy.
     *
     * @return bool
     */
    public function isRootEntity()
    {
    }
    /**
     * Checks whether a mapped association field is inherited from a superclass.
     *
     * @param string $fieldName
     *
     * @return bool TRUE if the field is inherited, FALSE otherwise.
     */
    public function isInheritedAssociation($fieldName)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function isInheritedEmbeddedClass($fieldName)
    {
    }
    /**
     * Sets the name of the primary table the class is mapped to.
     *
     * @deprecated Use {@link setPrimaryTable}.
     *
     * @param string $tableName The table name.
     *
     * @return void
     */
    public function setTableName($tableName)
    {
    }
    /**
     * Sets the primary table definition. The provided array supports the
     * following structure:
     *
     * name => <tableName> (optional, defaults to class name)
     * indexes => array of indexes (optional)
     * uniqueConstraints => array of constraints (optional)
     *
     * If a key is omitted, the current value is kept.
     *
     * @psalm-param array<string, mixed> $table The table description.
     *
     * @return void
     */
    public function setPrimaryTable(array $table)
    {
    }
    /**
     * Adds a mapped field to the class.
     *
     * @psalm-param array<string, mixed> $mapping The field mapping.
     *
     * @return void
     *
     * @throws MappingException
     */
    public function mapField(array $mapping)
    {
    }
    /**
     * INTERNAL:
     * Adds an association mapping without completing/validating it.
     * This is mainly used to add inherited association mappings to derived classes.
     *
     * @psalm-param array<string, mixed> $mapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function addInheritedAssociationMapping(array $mapping)
    {
    }
    /**
     * INTERNAL:
     * Adds a field mapping without completing/validating it.
     * This is mainly used to add inherited field mappings to derived classes.
     *
     * @psalm-param array<string, mixed> $fieldMapping
     *
     * @return void
     */
    public function addInheritedFieldMapping(array $fieldMapping)
    {
    }
    /**
     * INTERNAL:
     * Adds a named query to this class.
     *
     * @deprecated
     *
     * @psalm-param array<string, mixed> $queryMapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function addNamedQuery(array $queryMapping)
    {
    }
    /**
     * INTERNAL:
     * Adds a named native query to this class.
     *
     * @deprecated
     *
     * @psalm-param array<string, mixed> $queryMapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function addNamedNativeQuery(array $queryMapping)
    {
    }
    /**
     * INTERNAL:
     * Adds a sql result set mapping to this class.
     *
     * @psalm-param array<string, mixed> $resultMapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function addSqlResultSetMapping(array $resultMapping)
    {
    }
    /**
     * Adds a one-to-one mapping.
     *
     * @param array<string, mixed> $mapping The mapping.
     *
     * @return void
     */
    public function mapOneToOne(array $mapping)
    {
    }
    /**
     * Adds a one-to-many mapping.
     *
     * @psalm-param array<string, mixed> $mapping The mapping.
     *
     * @return void
     */
    public function mapOneToMany(array $mapping)
    {
    }
    /**
     * Adds a many-to-one mapping.
     *
     * @psalm-param array<string, mixed> $mapping The mapping.
     *
     * @return void
     */
    public function mapManyToOne(array $mapping)
    {
    }
    /**
     * Adds a many-to-many mapping.
     *
     * @psalm-param array<string, mixed> $mapping The mapping.
     *
     * @return void
     */
    public function mapManyToMany(array $mapping)
    {
    }
    /**
     * Stores the association mapping.
     *
     * @psalm-param array<string, mixed> $assocMapping
     *
     * @return void
     *
     * @throws MappingException
     */
    protected function _storeAssociationMapping(array $assocMapping)
    {
    }
    /**
     * Registers a custom repository class for the entity class.
     *
     * @param string|null $repositoryClassName The class name of the custom mapper.
     * @psalm-param class-string<EntityRepository>|null $repositoryClassName
     *
     * @return void
     */
    public function setCustomRepositoryClass($repositoryClassName)
    {
    }
    /**
     * Dispatches the lifecycle event of the given entity to the registered
     * lifecycle callbacks and lifecycle listeners.
     *
     * @deprecated Deprecated since version 2.4 in favor of \Doctrine\ORM\Event\ListenersInvoker
     *
     * @param string $lifecycleEvent The lifecycle event.
     * @param object $entity         The Entity on which the event occurred.
     *
     * @return void
     */
    public function invokeLifecycleCallbacks($lifecycleEvent, $entity)
    {
    }
    /**
     * Whether the class has any attached lifecycle listeners or callbacks for a lifecycle event.
     *
     * @param string $lifecycleEvent
     *
     * @return bool
     */
    public function hasLifecycleCallbacks($lifecycleEvent)
    {
    }
    /**
     * Gets the registered lifecycle callbacks for an event.
     *
     * @param string $event
     *
     * @return string[]
     * @psalm-return list<string>
     */
    public function getLifecycleCallbacks($event)
    {
    }
    /**
     * Adds a lifecycle callback for entities of this class.
     *
     * @param string $callback
     * @param string $event
     *
     * @return void
     */
    public function addLifecycleCallback($callback, $event)
    {
    }
    /**
     * Sets the lifecycle callbacks for entities of this class.
     * Any previously registered callbacks are overwritten.
     *
     * @psalm-param array<string, list<string>> $callbacks
     *
     * @return void
     */
    public function setLifecycleCallbacks(array $callbacks)
    {
    }
    /**
     * Adds a entity listener for entities of this class.
     *
     * @param string $eventName The entity lifecycle event.
     * @param string $class     The listener class.
     * @param string $method    The listener callback method.
     *
     * @return void
     *
     * @throws MappingException
     */
    public function addEntityListener($eventName, $class, $method)
    {
    }
    /**
     * Sets the discriminator column definition.
     *
     * @see getDiscriminatorColumn()
     *
     * @param mixed[]|null $columnDef
     * @psalm-param array<string, mixed>|null $columnDef
     *
     * @return void
     *
     * @throws MappingException
     */
    public function setDiscriminatorColumn($columnDef)
    {
    }
    /**
     * @return array<string, mixed>
     */
    public final function getDiscriminatorColumn() : array
    {
    }
    /**
     * Sets the discriminator values used by this class.
     * Used for JOINED and SINGLE_TABLE inheritance mapping strategies.
     *
     * @psalm-param array<string, class-string> $map
     *
     * @return void
     */
    public function setDiscriminatorMap(array $map)
    {
    }
    /**
     * Adds one entry of the discriminator map with a new class and corresponding name.
     *
     * @param string $name
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return void
     *
     * @throws MappingException
     */
    public function addDiscriminatorMapClass($name, $className)
    {
    }
    /**
     * Checks whether the class has a named query with the given query name.
     *
     * @param string $queryName
     *
     * @return bool
     */
    public function hasNamedQuery($queryName)
    {
    }
    /**
     * Checks whether the class has a named native query with the given query name.
     *
     * @param string $queryName
     *
     * @return bool
     */
    public function hasNamedNativeQuery($queryName)
    {
    }
    /**
     * Checks whether the class has a named native query with the given query name.
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasSqlResultSetMapping($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasAssociation($fieldName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isSingleValuedAssociation($fieldName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isCollectionValuedAssociation($fieldName)
    {
    }
    /**
     * Is this an association that only has a single join column?
     *
     * @param string $fieldName
     *
     * @return bool
     */
    public function isAssociationWithSingleJoinColumn($fieldName)
    {
    }
    /**
     * Returns the single association join column (if any).
     *
     * @param string $fieldName
     *
     * @return string
     *
     * @throws MappingException
     */
    public function getSingleAssociationJoinColumnName($fieldName)
    {
    }
    /**
     * Returns the single association referenced join column name (if any).
     *
     * @param string $fieldName
     *
     * @return string
     *
     * @throws MappingException
     */
    public function getSingleAssociationReferencedJoinColumnName($fieldName)
    {
    }
    /**
     * Used to retrieve a fieldname for either field or association from a given column.
     *
     * This method is used in foreign-key as primary-key contexts.
     *
     * @param string $columnName
     *
     * @return string
     *
     * @throws MappingException
     */
    public function getFieldForColumn($columnName)
    {
    }
    /**
     * Sets the ID generator used to generate IDs for instances of this class.
     *
     * @param AbstractIdGenerator $generator
     *
     * @return void
     */
    public function setIdGenerator($generator)
    {
    }
    /**
     * Sets definition.
     *
     * @psalm-param array<string, string|null> $definition
     *
     * @return void
     */
    public function setCustomGeneratorDefinition(array $definition)
    {
    }
    /**
     * Sets the definition of the sequence ID generator for this class.
     *
     * The definition must have the following structure:
     * <code>
     * array(
     *     'sequenceName'   => 'name',
     *     'allocationSize' => 20,
     *     'initialValue'   => 1
     *     'quoted'         => 1
     * )
     * </code>
     *
     * @psalm-param array{sequenceName?: string, allocationSize?: int|string, initialValue?: int|string, quoted?: mixed} $definition
     *
     * @return void
     *
     * @throws MappingException
     */
    public function setSequenceGeneratorDefinition(array $definition)
    {
    }
    /**
     * Sets the version field mapping used for versioning. Sets the default
     * value to use depending on the column type.
     *
     * @psalm-param array<string, mixed> $mapping The version field mapping array.
     *
     * @return void
     *
     * @throws MappingException
     */
    public function setVersionMapping(array &$mapping)
    {
    }
    /**
     * Sets whether this class is to be versioned for optimistic locking.
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setVersioned($bool)
    {
    }
    /**
     * Sets the name of the field that is to be used for versioning if this class is
     * versioned for optimistic locking.
     *
     * @param string $versionField
     *
     * @return void
     */
    public function setVersionField($versionField)
    {
    }
    /**
     * Marks this class as read only, no change tracking is applied to it.
     *
     * @return void
     */
    public function markReadOnly()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getFieldNames()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAssociationNames()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @param string $assocName
     *
     * @return string
     * @psalm-return class-string
     *
     * @throws InvalidArgumentException
     */
    public function getAssociationTargetClass($assocName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
    }
    /**
     * Gets the (possibly quoted) identifier column names for safe use in an SQL statement.
     *
     * @deprecated Deprecated since version 2.3 in favor of \Doctrine\ORM\Mapping\QuoteStrategy
     *
     * @param AbstractPlatform $platform
     *
     * @return string[]
     * @psalm-return list<string>
     */
    public function getQuotedIdentifierColumnNames($platform)
    {
    }
    /**
     * Gets the (possibly quoted) column name of a mapped field for safe use  in an SQL statement.
     *
     * @deprecated Deprecated since version 2.3 in favor of \Doctrine\ORM\Mapping\QuoteStrategy
     *
     * @param string           $field
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getQuotedColumnName($field, $platform)
    {
    }
    /**
     * Gets the (possibly quoted) primary table name of this class for safe use in an SQL statement.
     *
     * @deprecated Deprecated since version 2.3 in favor of \Doctrine\ORM\Mapping\QuoteStrategy
     *
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getQuotedTableName($platform)
    {
    }
    /**
     * Gets the (possibly quoted) name of the join table.
     *
     * @deprecated Deprecated since version 2.3 in favor of \Doctrine\ORM\Mapping\QuoteStrategy
     *
     * @param mixed[]          $assoc
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getQuotedJoinTableName(array $assoc, $platform)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isAssociationInverseSide($fieldName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAssociationMappedByTargetField($fieldName)
    {
    }
    /**
     * @param string $targetClass
     *
     * @return mixed[][]
     * @psalm-return array<string, array<string, mixed>>
     */
    public function getAssociationsByTargetClass($targetClass)
    {
    }
    /**
     * @param string|null $className
     * @psalm-param string|class-string|null $className
     *
     * @return string|null null if the input value is null
     * @psalm-return class-string|null
     */
    public function fullyQualifiedClassName($className)
    {
    }
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getMetadataValue($name)
    {
    }
    /**
     * Map Embedded Class
     *
     * @psalm-param array<string, mixed> $mapping
     *
     * @return void
     *
     * @throws MappingException
     */
    public function mapEmbedded(array $mapping)
    {
    }
    /**
     * Inline the embeddable class
     *
     * @param string $property
     *
     * @return void
     */
    public function inlineEmbeddable($property, \Doctrine\ORM\Mapping\ClassMetadataInfo $embeddable)
    {
    }
    /**
     * Gets the sequence name based on class metadata.
     *
     * @return string
     *
     * @todo Sequence names should be computed in DBAL depending on the platform
     */
    public function getSequenceName(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * Gets the sequence name prefix based on class metadata.
     *
     * @return string
     *
     * @todo Sequence names should be computed in DBAL depending on the platform
     */
    public function getSequencePrefix(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
