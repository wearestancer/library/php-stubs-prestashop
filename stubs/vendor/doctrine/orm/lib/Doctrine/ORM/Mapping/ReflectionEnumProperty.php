<?php

namespace Doctrine\ORM\Mapping;

class ReflectionEnumProperty extends \ReflectionProperty
{
    /**
     * @param class-string<BackedEnum> $enumType
     */
    public function __construct(\ReflectionProperty $originalReflectionProperty, string $enumType)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @param object|null $object
     *
     * @return int|string|int[]|string[]|null
     */
    #[\ReturnTypeWillChange]
    public function getValue($object = null)
    {
    }
    /**
     * @param object                         $object
     * @param int|string|int[]|string[]|null $value
     */
    public function setValue($object, $value = null) : void
    {
    }
}
