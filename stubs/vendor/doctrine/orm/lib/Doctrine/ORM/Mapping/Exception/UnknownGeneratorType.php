<?php

namespace Doctrine\ORM\Mapping\Exception;

final class UnknownGeneratorType extends \Doctrine\ORM\Exception\ORMException
{
    public static function create(int $generatorType) : self
    {
    }
}
