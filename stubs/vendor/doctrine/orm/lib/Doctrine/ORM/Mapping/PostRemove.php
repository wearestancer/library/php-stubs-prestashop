<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @Target("METHOD")
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
final class PostRemove implements \Doctrine\ORM\Mapping\Annotation
{
}
