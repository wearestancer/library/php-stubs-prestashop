<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * XmlDriver is a metadata driver that enables mapping through XML files.
 *
 * @link        www.doctrine-project.org
 */
class XmlDriver extends \Doctrine\Persistence\Mapping\Driver\FileDriver
{
    public const DEFAULT_FILE_EXTENSION = '.dcm.xml';
    /**
     * {@inheritDoc}
     */
    public function __construct($locator, $fileExtension = self::DEFAULT_FILE_EXTENSION)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function loadMappingFile($file)
    {
    }
    /**
     * @param mixed $element
     *
     * @return bool
     */
    protected function evaluateBoolean($element)
    {
    }
}
