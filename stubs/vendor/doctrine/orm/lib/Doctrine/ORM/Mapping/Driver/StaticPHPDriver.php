<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * {@inheritDoc}
 *
 * @deprecated this driver will be removed. Use Doctrine\Persistence\Mapping\Driver\StaticPHPDriver instead
 */
class StaticPHPDriver extends \Doctrine\Persistence\Mapping\Driver\StaticPHPDriver
{
}
