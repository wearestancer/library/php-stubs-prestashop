<?php

namespace Doctrine\ORM\Mapping\Reflection;

/**
 * Utility class to retrieve all reflection instance properties of a given class, including
 * private inherited properties and transient properties.
 *
 * @private This API is for internal use only
 */
final class ReflectionPropertiesGetter
{
    public function __construct(\Doctrine\Persistence\Mapping\ReflectionService $reflectionService)
    {
    }
    /**
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return ReflectionProperty[] indexed by property internal name
     */
    public function getProperties($className) : array
    {
    }
}
