<?php

namespace Doctrine\ORM\Mapping\Builder;

/**
 * Builder for entity listeners.
 */
class EntityListenerBuilder
{
    /**
     * Lookup the entity class to find methods that match to event lifecycle names
     *
     * @param ClassMetadata $metadata  The entity metadata.
     * @param string        $className The listener class name.
     *
     * @return void
     *
     * @throws MappingException When the listener class not found.
     */
    public static function bindEntityListener(\Doctrine\ORM\Mapping\ClassMetadata $metadata, $className)
    {
    }
}
