<?php

namespace Doctrine\ORM\Mapping;

/**
 * The ClassMetadataFactory is used to create ClassMetadata objects that contain all the
 * metadata mapping information of a class which describes how a class should be mapped
 * to a relational database.
 *
 * @extends AbstractClassMetadataFactory<ClassMetadata>
 */
class ClassMetadataFactory extends \Doctrine\Persistence\Mapping\AbstractClassMetadataFactory
{
    /**
     * @return void
     */
    public function setEntityManager(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initialize()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function onNotFoundMetadata($className)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function doLoadMetadata($class, $parent, $rootEntityFound, array $nonSuperclassParents)
    {
    }
    /**
     * Validate runtime metadata is correctly defined.
     *
     * @param ClassMetadata               $class
     * @param ClassMetadataInterface|null $parent
     *
     * @return void
     *
     * @throws MappingException
     */
    protected function validateRuntimeMetadata($class, $parent)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function newClassMetadataInstance($className)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function wakeupReflection(\Doctrine\Persistence\Mapping\ClassMetadata $class, \Doctrine\Persistence\Mapping\ReflectionService $reflService)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function initializeReflection(\Doctrine\Persistence\Mapping\ClassMetadata $class, \Doctrine\Persistence\Mapping\ReflectionService $reflService)
    {
    }
    /**
     * @deprecated This method will be removed in ORM 3.0.
     *
     * @return class-string
     */
    protected function getFqcnFromAlias($namespaceAlias, $simpleClassName)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getDriver()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function isEntity(\Doctrine\Persistence\Mapping\ClassMetadata $class)
    {
    }
}
