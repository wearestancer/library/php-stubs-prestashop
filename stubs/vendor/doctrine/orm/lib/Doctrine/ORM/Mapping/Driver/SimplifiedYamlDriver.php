<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * YamlDriver that additionally looks for mapping information in a global file.
 *
 * @deprecated This class is being removed from the ORM and won't have any replacement
 */
class SimplifiedYamlDriver extends \Doctrine\ORM\Mapping\Driver\YamlDriver
{
    public const DEFAULT_FILE_EXTENSION = '.orm.yml';
    /**
     * {@inheritDoc}
     */
    public function __construct($prefixes, $fileExtension = self::DEFAULT_FILE_EXTENSION)
    {
    }
}
