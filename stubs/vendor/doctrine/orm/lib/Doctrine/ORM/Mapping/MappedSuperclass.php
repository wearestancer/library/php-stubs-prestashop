<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class MappedSuperclass implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * @var string|null
     * @psalm-var class-string<EntityRepository>|null
     */
    public $repositoryClass;
    /**
     * @psalm-param class-string<EntityRepository>|null $repositoryClass
     */
    public function __construct(?string $repositoryClass = null)
    {
    }
}
