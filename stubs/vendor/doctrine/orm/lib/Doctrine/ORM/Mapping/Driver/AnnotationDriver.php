<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * The AnnotationDriver reads the mapping metadata from docblock annotations.
 */
class AnnotationDriver extends \Doctrine\ORM\Mapping\Driver\CompatibilityAnnotationDriver
{
    use \Doctrine\Persistence\Mapping\Driver\ColocatedMappingDriver;
    /**
     * The annotation reader.
     *
     * @internal this property will be private in 3.0
     *
     * @var Reader
     */
    protected $reader;
    /**
     * @var int[]
     * @psalm-var array<class-string, int>
     */
    protected $entityAnnotationClasses = [\Doctrine\ORM\Mapping\Entity::class => 1, \Doctrine\ORM\Mapping\MappedSuperclass::class => 2];
    /**
     * Initializes a new AnnotationDriver that uses the given AnnotationReader for reading
     * docblock annotations.
     *
     * @param Reader               $reader The AnnotationReader to use
     * @param string|string[]|null $paths  One or multiple paths where mapping classes can be found.
     */
    public function __construct($reader, $paths = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * Retrieve the current annotation reader
     *
     * @return Reader
     */
    public function getReader()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isTransient($className)
    {
    }
    /**
     * Factory method for the Annotation Driver.
     *
     * @param mixed[]|string $paths
     *
     * @return AnnotationDriver
     */
    public static function create($paths = [], ?\Doctrine\Common\Annotations\AnnotationReader $reader = null)
    {
    }
}
