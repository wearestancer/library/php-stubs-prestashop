<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class SequenceGenerator implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string */
    public $sequenceName;
    /** @var int */
    public $allocationSize = 1;
    /** @var int */
    public $initialValue = 1;
    public function __construct(?string $sequenceName = null, int $allocationSize = 1, int $initialValue = 1)
    {
    }
}
