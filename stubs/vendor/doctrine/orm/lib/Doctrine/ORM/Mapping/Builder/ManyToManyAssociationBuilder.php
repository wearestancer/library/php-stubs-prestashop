<?php

namespace Doctrine\ORM\Mapping\Builder;

/**
 * ManyToMany Association Builder
 *
 * @link        www.doctrine-project.com
 */
class ManyToManyAssociationBuilder extends \Doctrine\ORM\Mapping\Builder\OneToManyAssociationBuilder
{
    /**
     * @param string $name
     *
     * @return $this
     */
    public function setJoinTable($name)
    {
    }
    /**
     * Adds Inverse Join Columns.
     *
     * @param string      $columnName
     * @param string      $referencedColumnName
     * @param bool        $nullable
     * @param bool        $unique
     * @param string|null $onDelete
     * @param string|null $columnDef
     *
     * @return $this
     */
    public function addInverseJoinColumn($columnName, $referencedColumnName, $nullable = true, $unique = false, $onDelete = null, $columnDef = null)
    {
    }
    /**
     * @return ClassMetadataBuilder
     */
    public function build()
    {
    }
}
