<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class DiscriminatorColumn implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $name;
    /** @var string|null */
    public $type;
    /** @var int|null */
    public $length;
    /**
     * Field name used in non-object hydration (array/scalar).
     *
     * @var mixed
     */
    public $fieldName;
    /** @var string */
    public $columnDefinition;
    public function __construct(?string $name = null, ?string $type = null, ?int $length = null, ?string $columnDefinition = null)
    {
    }
}
