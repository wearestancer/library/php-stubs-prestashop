<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class HasLifecycleCallbacks implements \Doctrine\ORM\Mapping\Annotation
{
}
