<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class Embedded implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $class;
    /** @var string|bool|null */
    public $columnPrefix;
    public function __construct(?string $class = null, $columnPrefix = null)
    {
    }
}
