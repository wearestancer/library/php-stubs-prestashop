<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class Entity implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * @var string|null
     * @psalm-var class-string<EntityRepository>|null
     */
    public $repositoryClass;
    /** @var bool */
    public $readOnly = false;
    /**
     * @psalm-param class-string<EntityRepository>|null $repositoryClass
     */
    public function __construct(?string $repositoryClass = null, bool $readOnly = false)
    {
    }
}
