<?php

namespace Doctrine\ORM\Mapping;

/**
 * This annotation is used to override the mapping of a entity property.
 *
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class AttributeOverrides implements \Doctrine\ORM\Mapping\Annotation
{
    /**
     * One or more field or property mapping overrides.
     *
     * @var array<AttributeOverride>
     */
    public $overrides = [];
    /**
     * @param array<AttributeOverride>|AttributeOverride $overrides
     */
    public function __construct($overrides)
    {
    }
}
