<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class Column implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $name;
    /** @var mixed */
    public $type;
    /** @var int|null */
    public $length;
    /**
     * The precision for a decimal (exact numeric) column (Applies only for decimal column).
     *
     * @var int|null
     */
    public $precision = 0;
    /**
     * The scale for a decimal (exact numeric) column (Applies only for decimal column).
     *
     * @var int|null
     */
    public $scale = 0;
    /** @var bool */
    public $unique = false;
    /** @var bool */
    public $nullable = false;
    /** @var bool */
    public $insertable = true;
    /** @var bool */
    public $updatable = true;
    /** @var class-string<\BackedEnum>|null */
    public $enumType = null;
    /** @var array<string,mixed> */
    public $options = [];
    /** @var string|null */
    public $columnDefinition;
    /**
     * @var string|null
     * @psalm-var 'NEVER'|'INSERT'|'ALWAYS'|null
     * @Enum({"NEVER", "INSERT", "ALWAYS"})
     */
    public $generated;
    /**
     * @param class-string<\BackedEnum>|null $enumType
     * @param array<string,mixed>            $options
     * @psalm-param 'NEVER'|'INSERT'|'ALWAYS'|null $generated
     */
    public function __construct(?string $name = null, ?string $type = null, ?int $length = null, ?int $precision = null, ?int $scale = null, bool $unique = false, bool $nullable = false, bool $insertable = true, bool $updatable = true, ?string $enumType = null, array $options = [], ?string $columnDefinition = null, ?string $generated = null)
    {
    }
}
