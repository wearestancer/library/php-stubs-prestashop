<?php

namespace Doctrine\ORM\Mapping;

/**
 * A MappingException indicates that something is wrong with the mapping setup.
 */
class MappingException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @return MappingException
     */
    public static function pathRequired()
    {
    }
    /**
     * @param string $entityName
     *
     * @return MappingException
     */
    public static function identifierRequired($entityName)
    {
    }
    /**
     * @param string $entityName
     * @param int    $type
     *
     * @return MappingException
     */
    public static function invalidInheritanceType($entityName, $type)
    {
    }
    /**
     * @return MappingException
     */
    public static function generatorNotAllowedWithCompositeId()
    {
    }
    /**
     * @param string $entity
     *
     * @return MappingException
     */
    public static function missingFieldName($entity)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function missingTargetEntity($fieldName)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function missingSourceEntity($fieldName)
    {
    }
    /**
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function missingEmbeddedClass($fieldName)
    {
    }
    /**
     * @param string $entityName
     * @param string $fileName
     *
     * @return MappingException
     */
    public static function mappingFileNotFound($entityName, $fileName)
    {
    }
    /**
     * Exception for invalid property name override.
     *
     * @param string $className The entity's name.
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function invalidOverrideFieldName($className, $fieldName)
    {
    }
    /**
     * Exception for invalid property type override.
     *
     * @param string $className The entity's name.
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function invalidOverrideFieldType($className, $fieldName)
    {
    }
    /**
     * @param string $className
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function mappingNotFound($className, $fieldName)
    {
    }
    /**
     * @param string $className
     * @param string $queryName
     *
     * @return MappingException
     */
    public static function queryNotFound($className, $queryName)
    {
    }
    /**
     * @param string $className
     * @param string $resultName
     *
     * @return MappingException
     */
    public static function resultMappingNotFound($className, $resultName)
    {
    }
    /**
     * @param string $entity
     * @param string $queryName
     *
     * @return MappingException
     */
    public static function emptyQueryMapping($entity, $queryName)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function nameIsMandatoryForQueryMapping($className)
    {
    }
    /**
     * @param string $entity
     * @param string $queryName
     *
     * @return MappingException
     */
    public static function missingQueryMapping($entity, $queryName)
    {
    }
    /**
     * @param string $entity
     * @param string $resultName
     *
     * @return MappingException
     */
    public static function missingResultSetMappingEntity($entity, $resultName)
    {
    }
    /**
     * @param string $entity
     * @param string $resultName
     *
     * @return MappingException
     */
    public static function missingResultSetMappingFieldName($entity, $resultName)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function nameIsMandatoryForSqlResultSetMapping($className)
    {
    }
    public static function oneToManyRequiresMappedBy(string $entityName, string $fieldName) : \Doctrine\ORM\Mapping\MappingException
    {
    }
    /**
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function joinTableRequired($fieldName)
    {
    }
    /**
     * Called if a required option was not found but is required
     *
     * @param string $field          Which field cannot be processed?
     * @param string $expectedOption Which option is required
     * @param string $hint           Can optionally be used to supply a tip for common mistakes,
     *                               e.g. "Did you think of the plural s?"
     *
     * @return MappingException
     */
    public static function missingRequiredOption($field, $expectedOption, $hint = '')
    {
    }
    /**
     * Generic exception for invalid mappings.
     *
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function invalidMapping($fieldName)
    {
    }
    /**
     * Exception for reflection exceptions - adds the entity name,
     * because there might be long classnames that will be shortened
     * within the stacktrace
     *
     * @param string $entity The entity's name
     *
     * @return MappingException
     */
    public static function reflectionFailure($entity, \ReflectionException $previousException)
    {
    }
    /**
     * @param string $className
     * @param string $joinColumn
     *
     * @return MappingException
     */
    public static function joinColumnMustPointToMappedField($className, $joinColumn)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function classIsNotAValidEntityOrMappedSuperClass($className)
    {
    }
    /**
     * @deprecated 2.9 no longer in use
     *
     * @param string $className
     * @param string $propertyName
     *
     * @return MappingException
     */
    public static function propertyTypeIsRequired($className, $propertyName)
    {
    }
    /**
     * @param string $entity    The entity's name.
     * @param string $fieldName The name of the field that was already declared.
     *
     * @return MappingException
     */
    public static function duplicateFieldMapping($entity, $fieldName)
    {
    }
    /**
     * @param string $entity
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function duplicateAssociationMapping($entity, $fieldName)
    {
    }
    /**
     * @param string $entity
     * @param string $queryName
     *
     * @return MappingException
     */
    public static function duplicateQueryMapping($entity, $queryName)
    {
    }
    /**
     * @param string $entity
     * @param string $resultName
     *
     * @return MappingException
     */
    public static function duplicateResultSetMapping($entity, $resultName)
    {
    }
    /**
     * @param string $entity
     *
     * @return MappingException
     */
    public static function singleIdNotAllowedOnCompositePrimaryKey($entity)
    {
    }
    /**
     * @param string $entity
     *
     * @return MappingException
     */
    public static function noIdDefined($entity)
    {
    }
    /**
     * @param string $entity
     * @param string $fieldName
     * @param string $unsupportedType
     *
     * @return MappingException
     */
    public static function unsupportedOptimisticLockingType($entity, $fieldName, $unsupportedType)
    {
    }
    /**
     * @param string|null $path
     *
     * @return MappingException
     */
    public static function fileMappingDriversRequireConfiguredDirectoryPath($path = null)
    {
    }
    /**
     * Returns an exception that indicates that a class used in a discriminator map does not exist.
     * An example would be an outdated (maybe renamed) classname.
     *
     * @param string $className   The class that could not be found
     * @param string $owningClass The class that declares the discriminator map.
     *
     * @return MappingException
     */
    public static function invalidClassInDiscriminatorMap($className, $owningClass)
    {
    }
    /**
     * @param string               $className
     * @param string[]             $entries
     * @param array<string,string> $map
     *
     * @return MappingException
     */
    public static function duplicateDiscriminatorEntry($className, array $entries, array $map)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function missingDiscriminatorMap($className)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function missingDiscriminatorColumn($className)
    {
    }
    /**
     * @param string $className
     * @param string $type
     *
     * @return MappingException
     */
    public static function invalidDiscriminatorColumnType($className, $type)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function nameIsMandatoryForDiscriminatorColumns($className)
    {
    }
    /**
     * @param string $className
     * @param string $fieldName
     *
     * @return MappingException
     */
    public static function cannotVersionIdField($className, $fieldName)
    {
    }
    /**
     * @param string $className
     * @param string $fieldName
     * @param string $type
     *
     * @return MappingException
     */
    public static function sqlConversionNotAllowedForIdentifiers($className, $fieldName, $type)
    {
    }
    /**
     * @param string $className
     * @param string $columnName
     *
     * @return MappingException
     */
    public static function duplicateColumnName($className, $columnName)
    {
    }
    /**
     * @param string $className
     * @param string $field
     *
     * @return MappingException
     */
    public static function illegalToManyAssociationOnMappedSuperclass($className, $field)
    {
    }
    /**
     * @param string $className
     * @param string $targetEntity
     * @param string $targetField
     *
     * @return MappingException
     */
    public static function cannotMapCompositePrimaryKeyEntitiesAsForeignId($className, $targetEntity, $targetField)
    {
    }
    /**
     * @param string $className
     * @param string $field
     *
     * @return MappingException
     */
    public static function noSingleAssociationJoinColumnFound($className, $field)
    {
    }
    /**
     * @param string $className
     * @param string $column
     *
     * @return MappingException
     */
    public static function noFieldNameFoundForColumn($className, $column)
    {
    }
    /**
     * @param string $className
     * @param string $field
     *
     * @return MappingException
     */
    public static function illegalOrphanRemovalOnIdentifierAssociation($className, $field)
    {
    }
    /**
     * @param string $className
     * @param string $field
     *
     * @return MappingException
     */
    public static function illegalOrphanRemoval($className, $field)
    {
    }
    /**
     * @param string $className
     * @param string $field
     *
     * @return MappingException
     */
    public static function illegalInverseIdentifierAssociation($className, $field)
    {
    }
    /**
     * @param string $className
     * @param string $field
     *
     * @return MappingException
     */
    public static function illegalToManyIdentifierAssociation($className, $field)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function noInheritanceOnMappedSuperClass($className)
    {
    }
    /**
     * @param string $className
     * @param string $rootClassName
     *
     * @return MappingException
     */
    public static function mappedClassNotPartOfDiscriminatorMap($className, $rootClassName)
    {
    }
    /**
     * @param string $className
     * @param string $methodName
     *
     * @return MappingException
     */
    public static function lifecycleCallbackMethodNotFound($className, $methodName)
    {
    }
    /**
     * @param string $listenerName
     * @param string $className
     *
     * @return MappingException
     */
    public static function entityListenerClassNotFound($listenerName, $className)
    {
    }
    /**
     * @param string $listenerName
     * @param string $methodName
     * @param string $className
     *
     * @return MappingException
     */
    public static function entityListenerMethodNotFound($listenerName, $methodName, $className)
    {
    }
    /**
     * @param string $listenerName
     * @param string $methodName
     * @param string $className
     *
     * @return MappingException
     */
    public static function duplicateEntityListener($listenerName, $methodName, $className)
    {
    }
    /**
     * @param string $className
     * @param string $annotation
     *
     * @return MappingException
     */
    public static function invalidFetchMode($className, $annotation)
    {
    }
    public static function invalidGeneratedMode(string $annotation) : \Doctrine\ORM\Mapping\MappingException
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function compositeKeyAssignedIdGeneratorRequired($className)
    {
    }
    /**
     * @param string $targetEntity
     * @param string $sourceEntity
     * @param string $associationName
     *
     * @return MappingException
     */
    public static function invalidTargetEntityClass($targetEntity, $sourceEntity, $associationName)
    {
    }
    /**
     * @param string[] $cascades
     * @param string   $className
     * @param string   $propertyName
     *
     * @return MappingException
     */
    public static function invalidCascadeOption(array $cascades, $className, $propertyName)
    {
    }
    /**
     * @param string $className
     *
     * @return MappingException
     */
    public static function missingSequenceName($className)
    {
    }
    /**
     * @param string $className
     * @param string $propertyName
     *
     * @return MappingException
     */
    public static function infiniteEmbeddableNesting($className, $propertyName)
    {
    }
    /**
     * @param string $className
     * @param string $propertyName
     *
     * @return self
     */
    public static function illegalOverrideOfInheritedProperty($className, $propertyName)
    {
    }
    /**
     * @return self
     */
    public static function invalidIndexConfiguration($className, $indexName)
    {
    }
    /**
     * @return self
     */
    public static function invalidUniqueConstraintConfiguration($className, $indexName)
    {
    }
    /**
     * @param mixed $givenValue
     */
    public static function invalidOverrideType(string $expectdType, $givenValue) : self
    {
    }
    public static function enumsRequirePhp81(string $className, string $fieldName) : self
    {
    }
    public static function nonEnumTypeMapped(string $className, string $fieldName, string $enumType) : self
    {
    }
    /**
     * @param class-string             $className
     * @param class-string<BackedEnum> $enumType
     */
    public static function invalidEnumValue(string $className, string $fieldName, string $value, string $enumType, \ValueError $previous) : self
    {
    }
}
