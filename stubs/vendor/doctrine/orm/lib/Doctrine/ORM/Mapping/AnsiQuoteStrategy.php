<?php

namespace Doctrine\ORM\Mapping;

/**
 * ANSI compliant quote strategy, this strategy does not apply any quote.
 * To use this strategy all mapped tables and columns should be ANSI compliant.
 */
class AnsiQuoteStrategy implements \Doctrine\ORM\Mapping\QuoteStrategy
{
    use \Doctrine\ORM\Internal\SQLResultCasing;
    /**
     * {@inheritdoc}
     */
    public function getColumnName($fieldName, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTableName(\Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSequenceName(array $definition, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getJoinColumnName(array $joinColumn, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getReferencedJoinColumnName(array $joinColumn, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getJoinTableName(array $association, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifierColumnNames(\Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getColumnAlias($columnName, $counter, \Doctrine\DBAL\Platforms\AbstractPlatform $platform, ?\Doctrine\ORM\Mapping\ClassMetadata $class = null)
    {
    }
}
