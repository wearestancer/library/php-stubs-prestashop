<?php

namespace Doctrine\ORM\Mapping;

/**
 * The default NamingStrategy
 *
 * @link    www.doctrine-project.org
 */
class DefaultNamingStrategy implements \Doctrine\ORM\Mapping\NamingStrategy
{
    /**
     * {@inheritdoc}
     */
    public function classToTableName($className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function propertyToColumnName($propertyName, $className = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function embeddedFieldToColumnName($propertyName, $embeddedColumnName, $className = null, $embeddedClassName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function referenceColumnName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function joinColumnName($propertyName, $className = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function joinTableName($sourceEntity, $targetEntity, $propertyName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function joinKeyColumnName($entityName, $referencedColumnName = null)
    {
    }
}
