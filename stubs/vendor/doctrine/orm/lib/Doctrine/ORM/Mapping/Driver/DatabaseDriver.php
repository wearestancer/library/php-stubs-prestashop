<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * The DatabaseDriver reverse engineers the mapping metadata from a database.
 *
 * @link    www.doctrine-project.org
 */
class DatabaseDriver implements \Doctrine\Persistence\Mapping\Driver\MappingDriver
{
    public function __construct(\Doctrine\DBAL\Schema\AbstractSchemaManager $schemaManager)
    {
    }
    /**
     * Set the namespace for the generated entities.
     *
     * @param string $namespace
     *
     * @return void
     */
    public function setNamespace($namespace)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isTransient($className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllClassNames()
    {
    }
    /**
     * Sets class name for a table.
     *
     * @param string $tableName
     * @param string $className
     *
     * @return void
     */
    public function setClassNameForTable($tableName, $className)
    {
    }
    /**
     * Sets field name for a column on a specific table.
     *
     * @param string $tableName
     * @param string $columnName
     * @param string $fieldName
     *
     * @return void
     */
    public function setFieldNameForColumn($tableName, $columnName, $fieldName)
    {
    }
    /**
     * Sets tables manually instead of relying on the reverse engineering capabilities of SchemaManager.
     *
     * @param Table[] $entityTables
     * @param Table[] $manyToManyTables
     * @psalm-param list<Table> $entityTables
     * @psalm-param list<Table> $manyToManyTables
     *
     * @return void
     */
    public function setTables($entityTables, $manyToManyTables)
    {
    }
    public function setInflector(\Doctrine\Inflector\Inflector $inflector) : void
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
}
