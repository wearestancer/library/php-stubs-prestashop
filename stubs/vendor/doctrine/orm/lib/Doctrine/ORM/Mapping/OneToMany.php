<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("PROPERTY")
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class OneToMany implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string */
    public $mappedBy;
    /** @var class-string|null */
    public $targetEntity;
    /** @var array<string> */
    public $cascade;
    /**
     * The fetching strategy to use for the association.
     *
     * @var string
     * @Enum({"LAZY", "EAGER", "EXTRA_LAZY"})
     */
    public $fetch = 'LAZY';
    /** @var bool */
    public $orphanRemoval = false;
    /** @var string */
    public $indexBy;
    /**
     * @param class-string|null $targetEntity
     * @param string[]|null     $cascade
     */
    public function __construct(?string $mappedBy = null, ?string $targetEntity = null, ?array $cascade = null, string $fetch = 'LAZY', bool $orphanRemoval = false, ?string $indexBy = null)
    {
    }
}
