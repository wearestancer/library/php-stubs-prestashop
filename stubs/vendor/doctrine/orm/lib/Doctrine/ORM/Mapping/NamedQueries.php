<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class NamedQueries implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var array<\Doctrine\ORM\Mapping\NamedQuery> */
    public $value;
}
