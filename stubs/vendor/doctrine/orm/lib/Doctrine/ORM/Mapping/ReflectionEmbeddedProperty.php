<?php

namespace Doctrine\ORM\Mapping;

/**
 * Acts as a proxy to a nested Property structure, making it look like
 * just a single scalar property.
 *
 * This way value objects "just work" without UnitOfWork, Persisters or Hydrators
 * needing any changes.
 *
 * TODO: Move this class into Common\Reflection
 */
class ReflectionEmbeddedProperty extends \ReflectionProperty
{
    /**
     * @param string $embeddedClass
     */
    public function __construct(\ReflectionProperty $parentProperty, \ReflectionProperty $childProperty, $embeddedClass)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function getValue($object = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function setValue($object, $value = null)
    {
    }
}
