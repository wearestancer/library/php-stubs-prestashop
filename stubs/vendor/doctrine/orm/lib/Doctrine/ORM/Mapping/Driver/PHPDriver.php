<?php

namespace Doctrine\ORM\Mapping\Driver;

/**
 * {@inheritDoc}
 *
 * @deprecated this driver will be removed, use StaticPHPDriver or other mapping drivers instead.
 */
class PHPDriver extends \Doctrine\Persistence\Mapping\Driver\PHPDriver
{
    /**
     * @param string|string[]|FileLocator $locator
     */
    public function __construct($locator)
    {
    }
}
