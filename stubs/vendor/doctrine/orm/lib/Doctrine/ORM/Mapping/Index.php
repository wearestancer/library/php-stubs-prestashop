<?php

namespace Doctrine\ORM\Mapping;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("ANNOTATION")
 */
#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::IS_REPEATABLE)]
final class Index implements \Doctrine\ORM\Mapping\Annotation
{
    /** @var string|null */
    public $name;
    /** @var array<string>|null */
    public $columns;
    /** @var array<string>|null */
    public $fields;
    /** @var array<string>|null */
    public $flags;
    /** @var array<string,mixed>|null */
    public $options;
    /**
     * @param array<string>|null       $columns
     * @param array<string>|null       $fields
     * @param array<string>|null       $flags
     * @param array<string,mixed>|null $options
     */
    public function __construct(?array $columns = null, ?array $fields = null, ?string $name = null, ?array $flags = null, ?array $options = null)
    {
    }
}
