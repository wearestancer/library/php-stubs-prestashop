<?php

namespace Doctrine\ORM;

/**
 * A Query object represents a DQL query.
 */
final class Query extends \Doctrine\ORM\AbstractQuery
{
    /**
     * A query object is in CLEAN state when it has NO unparsed/unprocessed DQL parts.
     */
    public const STATE_CLEAN = 1;
    /**
     * A query object is in state DIRTY when it has DQL parts that have not yet been
     * parsed/processed. This is automatically defined as DIRTY when addDqlQueryPart
     * is called.
     */
    public const STATE_DIRTY = 2;
    /* Query HINTS */
    /**
     * The refresh hint turns any query into a refresh query with the result that
     * any local changes in entities are overridden with the fetched values.
     */
    public const HINT_REFRESH = 'doctrine.refresh';
    public const HINT_CACHE_ENABLED = 'doctrine.cache.enabled';
    public const HINT_CACHE_EVICT = 'doctrine.cache.evict';
    /**
     * Internal hint: is set to the proxy entity that is currently triggered for loading
     */
    public const HINT_REFRESH_ENTITY = 'doctrine.refresh.entity';
    /**
     * The forcePartialLoad query hint forces a particular query to return
     * partial objects.
     *
     * @todo Rename: HINT_OPTIMIZE
     */
    public const HINT_FORCE_PARTIAL_LOAD = 'doctrine.forcePartialLoad';
    /**
     * The includeMetaColumns query hint causes meta columns like foreign keys and
     * discriminator columns to be selected and returned as part of the query result.
     *
     * This hint does only apply to non-object queries.
     */
    public const HINT_INCLUDE_META_COLUMNS = 'doctrine.includeMetaColumns';
    /**
     * An array of class names that implement \Doctrine\ORM\Query\TreeWalker and
     * are iterated and executed after the DQL has been parsed into an AST.
     */
    public const HINT_CUSTOM_TREE_WALKERS = 'doctrine.customTreeWalkers';
    /**
     * A string with a class name that implements \Doctrine\ORM\Query\TreeWalker
     * and is used for generating the target SQL from any DQL AST tree.
     */
    public const HINT_CUSTOM_OUTPUT_WALKER = 'doctrine.customOutputWalker';
    /**
     * Marks queries as creating only read only objects.
     *
     * If the object retrieved from the query is already in the identity map
     * then it does not get marked as read only if it wasn't already.
     */
    public const HINT_READ_ONLY = 'doctrine.readOnly';
    public const HINT_INTERNAL_ITERATION = 'doctrine.internal.iteration';
    public const HINT_LOCK_MODE = 'doctrine.lockMode';
    /**
     * Gets the SQL query/queries that correspond to this DQL query.
     *
     * @return mixed The built sql query or an array of all sql queries.
     *
     * @override
     */
    public function getSQL()
    {
    }
    /**
     * Returns the corresponding AST for this DQL query.
     *
     * @return SelectStatement|UpdateStatement|DeleteStatement
     */
    public function getAST()
    {
    }
    /**
     * Defines a cache driver to be used for caching queries.
     *
     * @deprecated Call {@see setQueryCache()} instead.
     *
     * @param Cache|null $queryCache Cache driver.
     *
     * @return $this
     */
    public function setQueryCacheDriver($queryCache) : self
    {
    }
    /**
     * Defines a cache driver to be used for caching queries.
     *
     * @return $this
     */
    public function setQueryCache(?\Psr\Cache\CacheItemPoolInterface $queryCache) : self
    {
    }
    /**
     * Defines whether the query should make use of a query cache, if available.
     *
     * @param bool $bool
     *
     * @return $this
     */
    public function useQueryCache($bool) : self
    {
    }
    /**
     * Returns the cache driver used for query caching.
     *
     * @deprecated
     *
     * @return Cache|null The cache driver used for query caching or NULL, if
     * this Query does not use query caching.
     */
    public function getQueryCacheDriver() : ?\Doctrine\Common\Cache\Cache
    {
    }
    /**
     * Defines how long the query cache will be active before expire.
     *
     * @param int $timeToLive How long the cache entry is valid.
     *
     * @return $this
     */
    public function setQueryCacheLifetime($timeToLive) : self
    {
    }
    /**
     * Retrieves the lifetime of resultset cache.
     */
    public function getQueryCacheLifetime() : ?int
    {
    }
    /**
     * Defines if the query cache is active or not.
     *
     * @param bool $expire Whether or not to force query cache expiration.
     *
     * @return $this
     */
    public function expireQueryCache($expire = true) : self
    {
    }
    /**
     * Retrieves if the query cache is active or not.
     */
    public function getExpireQueryCache() : bool
    {
    }
    /**
     * @override
     */
    public function free() : void
    {
    }
    /**
     * Sets a DQL query string.
     *
     * @param string $dqlQuery DQL Query.
     */
    public function setDQL($dqlQuery) : self
    {
    }
    /**
     * Returns the DQL query that is represented by this query object.
     */
    public function getDQL() : ?string
    {
    }
    /**
     * Returns the state of this query object
     * By default the type is Doctrine_ORM_Query_Abstract::STATE_CLEAN but if it appears any unprocessed DQL
     * part, it is switched to Doctrine_ORM_Query_Abstract::STATE_DIRTY.
     *
     * @see AbstractQuery::STATE_CLEAN
     * @see AbstractQuery::STATE_DIRTY
     *
     * @return int The query state.
     */
    public function getState() : int
    {
    }
    /**
     * Method to check if an arbitrary piece of DQL exists
     *
     * @param string $dql Arbitrary piece of DQL to check for.
     */
    public function contains($dql) : bool
    {
    }
    /**
     * Sets the position of the first result to retrieve (the "offset").
     *
     * @param int|null $firstResult The first result to return.
     *
     * @return $this
     */
    public function setFirstResult($firstResult) : self
    {
    }
    /**
     * Gets the position of the first result the query object was set to retrieve (the "offset").
     * Returns NULL if {@link setFirstResult} was not applied to this query.
     *
     * @return int|null The position of the first result.
     */
    public function getFirstResult() : ?int
    {
    }
    /**
     * Sets the maximum number of results to retrieve (the "limit").
     *
     * @param int|null $maxResults
     *
     * @return $this
     */
    public function setMaxResults($maxResults) : self
    {
    }
    /**
     * Gets the maximum number of results the query object was set to retrieve (the "limit").
     * Returns NULL if {@link setMaxResults} was not applied to this query.
     *
     * @return int|null Maximum number of results.
     */
    public function getMaxResults() : ?int
    {
    }
    /**
     * Executes the query and returns an IterableResult that can be used to incrementally
     * iterated over the result.
     *
     * @deprecated
     *
     * @param ArrayCollection|mixed[]|null $parameters    The query parameters.
     * @param string|int                   $hydrationMode The hydration mode to use.
     * @psalm-param ArrayCollection<int, Parameter>|array<string, mixed>|null $parameters
     * @psalm-param string|AbstractQuery::HYDRATE_*|null                      $hydrationMode
     */
    public function iterate($parameters = null, $hydrationMode = self::HYDRATE_OBJECT) : \Doctrine\ORM\Internal\Hydration\IterableResult
    {
    }
    /** {@inheritDoc} */
    public function toIterable(iterable $parameters = [], $hydrationMode = self::HYDRATE_OBJECT) : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setHint($name, $value) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setHydrationMode($hydrationMode) : self
    {
    }
    /**
     * Set the lock mode for this Query.
     *
     * @see \Doctrine\DBAL\LockMode
     *
     * @param int $lockMode
     * @psalm-param LockMode::* $lockMode
     *
     * @throws TransactionRequiredException
     */
    public function setLockMode($lockMode) : self
    {
    }
    /**
     * Get the current lock mode for this query.
     *
     * @return int|null The current lock mode of this query or NULL if no specific lock mode is set.
     */
    public function getLockMode() : ?int
    {
    }
    /**
     * Cleanup Query resource when clone is called.
     */
    public function __clone()
    {
    }
}
