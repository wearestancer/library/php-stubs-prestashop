<?php

namespace Doctrine\ORM;

/**
 * Configuration container for all configuration options of Doctrine.
 * It combines all configuration options from DBAL & ORM.
 *
 * Internal note: When adding a new configuration option just write a getter/setter pair.
 *
 * @psalm-import-type AutogenerateMode from ProxyFactory
 */
class Configuration extends \Doctrine\DBAL\Configuration
{
    /** @var mixed[] */
    protected $_attributes = [];
    /**
     * Sets the directory where Doctrine generates any necessary proxy class files.
     *
     * @param string $dir
     *
     * @return void
     */
    public function setProxyDir($dir)
    {
    }
    /**
     * Gets the directory where Doctrine generates any necessary proxy class files.
     *
     * @return string|null
     */
    public function getProxyDir()
    {
    }
    /**
     * Gets the strategy for automatically generating proxy classes.
     *
     * @return int Possible values are constants of Doctrine\Common\Proxy\AbstractProxyFactory.
     * @psalm-return AutogenerateMode
     */
    public function getAutoGenerateProxyClasses()
    {
    }
    /**
     * Sets the strategy for automatically generating proxy classes.
     *
     * @param bool|int $autoGenerate Possible values are constants of Doctrine\Common\Proxy\AbstractProxyFactory.
     * True is converted to AUTOGENERATE_ALWAYS, false to AUTOGENERATE_NEVER.
     *
     * @return void
     */
    public function setAutoGenerateProxyClasses($autoGenerate)
    {
    }
    /**
     * Gets the namespace where proxy classes reside.
     *
     * @return string|null
     */
    public function getProxyNamespace()
    {
    }
    /**
     * Sets the namespace where proxy classes reside.
     *
     * @param string $ns
     *
     * @return void
     */
    public function setProxyNamespace($ns)
    {
    }
    /**
     * Sets the cache driver implementation that is used for metadata caching.
     *
     * @return void
     *
     * @todo Force parameter to be a Closure to ensure lazy evaluation
     *       (as soon as a metadata cache is in effect, the driver never needs to initialize).
     */
    public function setMetadataDriverImpl(\Doctrine\Persistence\Mapping\Driver\MappingDriver $driverImpl)
    {
    }
    /**
     * Adds a new default annotation driver with a correctly configured annotation reader. If $useSimpleAnnotationReader
     * is true, the notation `@Entity` will work, otherwise, the notation `@ORM\Entity` will be supported.
     *
     * @deprecated Use {@see ORMSetup::createDefaultAnnotationDriver()} instead.
     *
     * @param string|string[] $paths
     * @param bool            $useSimpleAnnotationReader
     * @psalm-param string|list<string> $paths
     *
     * @return AnnotationDriver
     */
    public function newDefaultAnnotationDriver($paths = [], $useSimpleAnnotationReader = true)
    {
    }
    /**
     * @deprecated No replacement planned.
     *
     * Adds a namespace under a certain alias.
     *
     * @param string $alias
     * @param string $namespace
     *
     * @return void
     */
    public function addEntityNamespace($alias, $namespace)
    {
    }
    /**
     * Resolves a registered namespace alias to the full namespace.
     *
     * @param string $entityNamespaceAlias
     *
     * @return string
     *
     * @throws UnknownEntityNamespace
     */
    public function getEntityNamespace($entityNamespaceAlias)
    {
    }
    /**
     * Sets the entity alias map.
     *
     * @psalm-param array<string, string> $entityNamespaces
     *
     * @return void
     */
    public function setEntityNamespaces(array $entityNamespaces)
    {
    }
    /**
     * Retrieves the list of registered entity namespace aliases.
     *
     * @psalm-return array<string, string>
     */
    public function getEntityNamespaces()
    {
    }
    /**
     * Gets the cache driver implementation that is used for the mapping metadata.
     *
     * @return MappingDriver|null
     */
    public function getMetadataDriverImpl()
    {
    }
    /**
     * Gets the cache driver implementation that is used for query result caching.
     */
    public function getResultCache() : ?\Psr\Cache\CacheItemPoolInterface
    {
    }
    /**
     * Sets the cache driver implementation that is used for query result caching.
     */
    public function setResultCache(\Psr\Cache\CacheItemPoolInterface $cache) : void
    {
    }
    /**
     * Gets the cache driver implementation that is used for the query cache (SQL cache).
     *
     * @deprecated Call {@see getQueryCache()} instead.
     *
     * @return CacheDriver|null
     */
    public function getQueryCacheImpl()
    {
    }
    /**
     * Sets the cache driver implementation that is used for the query cache (SQL cache).
     *
     * @deprecated Call {@see setQueryCache()} instead.
     *
     * @return void
     */
    public function setQueryCacheImpl(\Doctrine\Common\Cache\Cache $cacheImpl)
    {
    }
    /**
     * Gets the cache driver implementation that is used for the query cache (SQL cache).
     */
    public function getQueryCache() : ?\Psr\Cache\CacheItemPoolInterface
    {
    }
    /**
     * Sets the cache driver implementation that is used for the query cache (SQL cache).
     */
    public function setQueryCache(\Psr\Cache\CacheItemPoolInterface $cache) : void
    {
    }
    /**
     * Gets the cache driver implementation that is used for the hydration cache (SQL cache).
     *
     * @deprecated Call {@see getHydrationCache()} instead.
     *
     * @return CacheDriver|null
     */
    public function getHydrationCacheImpl()
    {
    }
    /**
     * Sets the cache driver implementation that is used for the hydration cache (SQL cache).
     *
     * @deprecated Call {@see setHydrationCache()} instead.
     *
     * @return void
     */
    public function setHydrationCacheImpl(\Doctrine\Common\Cache\Cache $cacheImpl)
    {
    }
    public function getHydrationCache() : ?\Psr\Cache\CacheItemPoolInterface
    {
    }
    public function setHydrationCache(\Psr\Cache\CacheItemPoolInterface $cache) : void
    {
    }
    /**
     * Gets the cache driver implementation that is used for metadata caching.
     *
     * @deprecated Deprecated in favor of getMetadataCache
     *
     * @return CacheDriver|null
     */
    public function getMetadataCacheImpl()
    {
    }
    /**
     * Sets the cache driver implementation that is used for metadata caching.
     *
     * @deprecated Deprecated in favor of setMetadataCache
     *
     * @return void
     */
    public function setMetadataCacheImpl(\Doctrine\Common\Cache\Cache $cacheImpl)
    {
    }
    public function getMetadataCache() : ?\Psr\Cache\CacheItemPoolInterface
    {
    }
    public function setMetadataCache(\Psr\Cache\CacheItemPoolInterface $cache) : void
    {
    }
    /**
     * Adds a named DQL query to the configuration.
     *
     * @param string $name The name of the query.
     * @param string $dql  The DQL query string.
     *
     * @return void
     */
    public function addNamedQuery($name, $dql)
    {
    }
    /**
     * Gets a previously registered named DQL query.
     *
     * @param string $name The name of the query.
     *
     * @return string The DQL query.
     *
     * @throws NamedQueryNotFound
     */
    public function getNamedQuery($name)
    {
    }
    /**
     * Adds a named native query to the configuration.
     *
     * @param string                 $name The name of the query.
     * @param string                 $sql  The native SQL query string.
     * @param Query\ResultSetMapping $rsm  The ResultSetMapping used for the results of the SQL query.
     *
     * @return void
     */
    public function addNamedNativeQuery($name, $sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
    }
    /**
     * Gets the components of a previously registered named native query.
     *
     * @param string $name The name of the query.
     *
     * @return mixed[]
     * @psalm-return array{string, ResultSetMapping} A tuple with the first element being the SQL string and the second
     *                                               element being the ResultSetMapping.
     *
     * @throws NamedNativeQueryNotFound
     */
    public function getNamedNativeQuery($name)
    {
    }
    /**
     * Ensures that this Configuration instance contains settings that are
     * suitable for a production environment.
     *
     * @deprecated
     *
     * @return void
     *
     * @throws ProxyClassesAlwaysRegenerating
     * @throws CacheException If a configuration setting has a value that is not
     *                        suitable for a production environment.
     */
    public function ensureProductionSettings()
    {
    }
    /**
     * Registers a custom DQL function that produces a string value.
     * Such a function can then be used in any DQL statement in any place where string
     * functions are allowed.
     *
     * DQL function names are case-insensitive.
     *
     * @param string          $name      Function name.
     * @param string|callable $className Class name or a callable that returns the function.
     *
     * @return void
     */
    public function addCustomStringFunction($name, $className)
    {
    }
    /**
     * Gets the implementation class name of a registered custom string DQL function.
     *
     * @param string $name
     *
     * @return string|null
     * @psalm-return ?class-string
     */
    public function getCustomStringFunction($name)
    {
    }
    /**
     * Sets a map of custom DQL string functions.
     *
     * Keys must be function names and values the FQCN of the implementing class.
     * The function names will be case-insensitive in DQL.
     *
     * Any previously added string functions are discarded.
     *
     * @psalm-param array<string, class-string> $functions The map of custom
     *                                                     DQL string functions.
     *
     * @return void
     */
    public function setCustomStringFunctions(array $functions)
    {
    }
    /**
     * Registers a custom DQL function that produces a numeric value.
     * Such a function can then be used in any DQL statement in any place where numeric
     * functions are allowed.
     *
     * DQL function names are case-insensitive.
     *
     * @param string          $name      Function name.
     * @param string|callable $className Class name or a callable that returns the function.
     *
     * @return void
     */
    public function addCustomNumericFunction($name, $className)
    {
    }
    /**
     * Gets the implementation class name of a registered custom numeric DQL function.
     *
     * @param string $name
     *
     * @return string|null
     * @psalm-return ?class-string
     */
    public function getCustomNumericFunction($name)
    {
    }
    /**
     * Sets a map of custom DQL numeric functions.
     *
     * Keys must be function names and values the FQCN of the implementing class.
     * The function names will be case-insensitive in DQL.
     *
     * Any previously added numeric functions are discarded.
     *
     * @psalm-param array<string, class-string> $functions The map of custom
     *                                                     DQL numeric functions.
     *
     * @return void
     */
    public function setCustomNumericFunctions(array $functions)
    {
    }
    /**
     * Registers a custom DQL function that produces a date/time value.
     * Such a function can then be used in any DQL statement in any place where date/time
     * functions are allowed.
     *
     * DQL function names are case-insensitive.
     *
     * @param string          $name      Function name.
     * @param string|callable $className Class name or a callable that returns the function.
     * @psalm-param class-string|callable $className
     *
     * @return void
     */
    public function addCustomDatetimeFunction($name, $className)
    {
    }
    /**
     * Gets the implementation class name of a registered custom date/time DQL function.
     *
     * @param string $name
     *
     * @return string|null
     * @psalm-return ?class-string $name
     */
    public function getCustomDatetimeFunction($name)
    {
    }
    /**
     * Sets a map of custom DQL date/time functions.
     *
     * Keys must be function names and values the FQCN of the implementing class.
     * The function names will be case-insensitive in DQL.
     *
     * Any previously added date/time functions are discarded.
     *
     * @param array $functions The map of custom DQL date/time functions.
     * @psalm-param array<string, string> $functions
     *
     * @return void
     */
    public function setCustomDatetimeFunctions(array $functions)
    {
    }
    /**
     * Sets the custom hydrator modes in one pass.
     *
     * @param array<string, class-string<AbstractHydrator>> $modes An array of ($modeName => $hydrator).
     *
     * @return void
     */
    public function setCustomHydrationModes($modes)
    {
    }
    /**
     * Gets the hydrator class for the given hydration mode name.
     *
     * @param string $modeName The hydration mode name.
     *
     * @return string|null The hydrator class name.
     * @psalm-return class-string<AbstractHydrator>|null
     */
    public function getCustomHydrationMode($modeName)
    {
    }
    /**
     * Adds a custom hydration mode.
     *
     * @param string $modeName The hydration mode name.
     * @param string $hydrator The hydrator class name.
     * @psalm-param class-string<AbstractHydrator> $hydrator
     *
     * @return void
     */
    public function addCustomHydrationMode($modeName, $hydrator)
    {
    }
    /**
     * Sets a class metadata factory.
     *
     * @param string $cmfName
     * @psalm-param class-string $cmfName
     *
     * @return void
     */
    public function setClassMetadataFactoryName($cmfName)
    {
    }
    /**
     * @return string
     * @psalm-return class-string
     */
    public function getClassMetadataFactoryName()
    {
    }
    /**
     * Adds a filter to the list of possible filters.
     *
     * @param string $name      The name of the filter.
     * @param string $className The class name of the filter.
     * @psalm-param class-string<SQLFilter> $className
     *
     * @return void
     */
    public function addFilter($name, $className)
    {
    }
    /**
     * Gets the class name for a given filter name.
     *
     * @param string $name The name of the filter.
     *
     * @return string|null The class name of the filter, or null if it is not
     *  defined.
     * @psalm-return class-string<SQLFilter>|null
     */
    public function getFilterClassName($name)
    {
    }
    /**
     * Sets default repository class.
     *
     * @param string $className
     * @psalm-param class-string<EntityRepository> $className
     *
     * @return void
     *
     * @throws InvalidEntityRepository If $classname is not an ObjectRepository.
     */
    public function setDefaultRepositoryClassName($className)
    {
    }
    /**
     * Get default repository class.
     *
     * @return string
     * @psalm-return class-string<EntityRepository>
     */
    public function getDefaultRepositoryClassName()
    {
    }
    /**
     * Sets naming strategy.
     *
     * @return void
     */
    public function setNamingStrategy(\Doctrine\ORM\Mapping\NamingStrategy $namingStrategy)
    {
    }
    /**
     * Gets naming strategy..
     *
     * @return NamingStrategy
     */
    public function getNamingStrategy()
    {
    }
    /**
     * Sets quote strategy.
     *
     * @return void
     */
    public function setQuoteStrategy(\Doctrine\ORM\Mapping\QuoteStrategy $quoteStrategy)
    {
    }
    /**
     * Gets quote strategy.
     *
     * @return QuoteStrategy
     */
    public function getQuoteStrategy()
    {
    }
    /**
     * Set the entity listener resolver.
     *
     * @return void
     */
    public function setEntityListenerResolver(\Doctrine\ORM\Mapping\EntityListenerResolver $resolver)
    {
    }
    /**
     * Get the entity listener resolver.
     *
     * @return EntityListenerResolver
     */
    public function getEntityListenerResolver()
    {
    }
    /**
     * Set the entity repository factory.
     *
     * @return void
     */
    public function setRepositoryFactory(\Doctrine\ORM\Repository\RepositoryFactory $repositoryFactory)
    {
    }
    /**
     * Get the entity repository factory.
     *
     * @return RepositoryFactory
     */
    public function getRepositoryFactory()
    {
    }
    /**
     * @return bool
     */
    public function isSecondLevelCacheEnabled()
    {
    }
    /**
     * @param bool $flag
     *
     * @return void
     */
    public function setSecondLevelCacheEnabled($flag = true)
    {
    }
    /**
     * @return void
     */
    public function setSecondLevelCacheConfiguration(\Doctrine\ORM\Cache\CacheConfiguration $cacheConfig)
    {
    }
    /**
     * @return CacheConfiguration|null
     */
    public function getSecondLevelCacheConfiguration()
    {
    }
    /**
     * Returns query hints, which will be applied to every query in application
     *
     * @psalm-return array<string, mixed>
     */
    public function getDefaultQueryHints()
    {
    }
    /**
     * Sets array of query hints, which will be applied to every query in application
     *
     * @psalm-param array<string, mixed> $defaultQueryHints
     *
     * @return void
     */
    public function setDefaultQueryHints(array $defaultQueryHints)
    {
    }
    /**
     * Gets the value of a default query hint. If the hint name is not recognized, FALSE is returned.
     *
     * @param string $name The name of the hint.
     *
     * @return mixed The value of the hint or FALSE, if the hint name is not recognized.
     */
    public function getDefaultQueryHint($name)
    {
    }
    /**
     * Sets a default query hint. If the hint name is not recognized, it is silently ignored.
     *
     * @param string $name  The name of the hint.
     * @param mixed  $value The value of the hint.
     *
     * @return void
     */
    public function setDefaultQueryHint($name, $value)
    {
    }
    /**
     * Gets a list of entity class names to be ignored by the SchemaTool
     *
     * @return list<class-string>
     */
    public function getSchemaIgnoreClasses() : array
    {
    }
    /**
     * Sets a list of entity class names to be ignored by the SchemaTool
     *
     * @param list<class-string> $schemaIgnoreClasses List of entity class names
     */
    public function setSchemaIgnoreClasses(array $schemaIgnoreClasses) : void
    {
    }
}
