<?php

namespace Doctrine\ORM;

class PessimisticLockException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @return PessimisticLockException
     */
    public static function lockFailed()
    {
    }
}
