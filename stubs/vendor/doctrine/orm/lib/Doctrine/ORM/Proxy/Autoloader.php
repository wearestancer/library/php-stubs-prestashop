<?php

namespace Doctrine\ORM\Proxy;

/**
 * @deprecated use \Doctrine\Common\Proxy\Autoloader instead
 */
class Autoloader extends \Doctrine\Common\Proxy\Autoloader
{
}
