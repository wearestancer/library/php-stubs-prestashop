<?php

namespace Doctrine\ORM\Proxy;

/**
 * This factory is used to create proxy objects for entities at runtime.
 *
 * @psalm-type AutogenerateMode = AbstractProxyFactory::AUTOGENERATE_NEVER|AbstractProxyFactory::AUTOGENERATE_ALWAYS|AbstractProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS|AbstractProxyFactory::AUTOGENERATE_EVAL|AbstractProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS_OR_CHANGED
 */
class ProxyFactory extends \Doctrine\Common\Proxy\AbstractProxyFactory
{
    /**
     * Initializes a new instance of the <tt>ProxyFactory</tt> class that is
     * connected to the given <tt>EntityManager</tt>.
     *
     * @param EntityManagerInterface $em           The EntityManager the new factory works for.
     * @param string                 $proxyDir     The directory to use for the proxy classes. It must exist.
     * @param string                 $proxyNs      The namespace to use for the proxy classes.
     * @param bool|int               $autoGenerate The strategy for automatically generating proxy classes. Possible
     *                                             values are constants of {@see AbstractProxyFactory}.
     * @psalm-param bool|AutogenerateMode $autoGenerate
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, $proxyDir, $proxyNs, $autoGenerate = \Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_NEVER)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function skipClass(\Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function createProxyDefinition($className)
    {
    }
}
