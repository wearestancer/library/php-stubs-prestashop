<?php

namespace Doctrine\ORM\Proxy;

/**
 * Interface for proxy classes.
 */
interface Proxy extends \Doctrine\Common\Proxy\Proxy
{
}
