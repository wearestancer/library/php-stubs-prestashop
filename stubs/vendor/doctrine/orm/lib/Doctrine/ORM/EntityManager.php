<?php

namespace Doctrine\ORM;

/**
 * The EntityManager is the central access point to ORM functionality.
 *
 * It is a facade to all different ORM subsystems such as UnitOfWork,
 * Query Language and Repository API. Instantiation is done through
 * the static create() method. The quickest way to obtain a fully
 * configured EntityManager is:
 *
 *     use Doctrine\ORM\Tools\Setup;
 *     use Doctrine\ORM\EntityManager;
 *
 *     $paths = array('/path/to/entity/mapping/files');
 *
 *     $config = Setup::createAnnotationMetadataConfiguration($paths);
 *     $dbParams = array('driver' => 'pdo_sqlite', 'memory' => true);
 *     $entityManager = EntityManager::create($dbParams, $config);
 *
 * For more information see
 * {@link http://docs.doctrine-project.org/projects/doctrine-orm/en/stable/reference/configuration.html}
 *
 * You should never attempt to inherit from the EntityManager: Inheritance
 * is not a valid extension point for the EntityManager. Instead you
 * should take a look at the {@see \Doctrine\ORM\Decorator\EntityManagerDecorator}
 * and wrap your entity manager in a decorator.
 */
/* final */
class EntityManager implements \Doctrine\ORM\EntityManagerInterface
{
    /**
     * Creates a new EntityManager that operates on the given database connection
     * and uses the given Configuration and EventManager implementations.
     */
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
    }
    /**
     * Gets the metadata factory used to gather the metadata of classes.
     *
     * @return ClassMetadataFactory
     */
    public function getMetadataFactory()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function wrapInTransaction(callable $func)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function commit()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
    }
    /**
     * Returns the ORM metadata descriptor for a class.
     *
     * The class name must be the fully-qualified class name without a leading backslash
     * (as it is returned by get_class($obj)) or an aliased class name.
     *
     * Examples:
     * MyProject\Domain\User
     * sales:PriceRequest
     *
     * Internal note: Performance-sensitive method.
     *
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
    }
    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
    }
    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * If an entity is explicitly passed to this method only this entity and
     * the cascade-persist semantics + scheduled inserts/removals are synchronized.
     *
     * @param object|mixed[]|null $entity
     *
     * @return void
     *
     * @throws OptimisticLockException If a version check on an entity that
     * makes use of optimistic locking fails.
     * @throws ORMException
     */
    public function flush($entity = null)
    {
    }
    /**
     * Finds an Entity by its identifier.
     *
     * @param string   $className   The class name of the entity to find.
     * @param mixed    $id          The identity of the entity to find.
     * @param int|null $lockMode    One of the \Doctrine\DBAL\LockMode::* constants
     *    or NULL if no specific lock mode should be used
     *    during the search.
     * @param int|null $lockVersion The version of the entity to find when using
     * optimistic locking.
     * @psalm-param class-string<T> $className
     * @psalm-param LockMode::*|null $lockMode
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     * @psalm-return ?T
     *
     * @throws OptimisticLockException
     * @throws ORMInvalidArgumentException
     * @throws TransactionRequiredException
     * @throws ORMException
     *
     * @template T
     */
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
    }
    /**
     * Clears the EntityManager. All entities that are currently managed
     * by this EntityManager become detached.
     *
     * @param string|null $entityName if given, only entities of this type will get detached
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException If a non-null non-string value is given.
     * @throws MappingException            If a $entityName is given, but that entity is not
     *                                     found in the mappings.
     */
    public function clear($entityName = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function close()
    {
    }
    /**
     * Tells the EntityManager to make an instance managed and persistent.
     *
     * The entity will be entered into the database at or before transaction
     * commit or as a result of the flush operation.
     *
     * NOTE: The persist operation always considers entities that are not yet known to
     * this EntityManager as NEW. Do not pass detached entities to the persist operation.
     *
     * @param object $entity The instance to make managed and persistent.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function persist($entity)
    {
    }
    /**
     * Removes an entity instance.
     *
     * A removed entity will be removed from the database at or before transaction commit
     * or as a result of the flush operation.
     *
     * @param object $entity The entity instance to remove.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function remove($entity)
    {
    }
    /**
     * Refreshes the persistent state of an entity from the database,
     * overriding any local changes that have not yet been persisted.
     *
     * @param object $entity The entity to refresh.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function refresh($entity)
    {
    }
    /**
     * Detaches an entity from the EntityManager, causing a managed entity to
     * become detached.  Unflushed changes made to the entity if any
     * (including removal of the entity), will not be synchronized to the database.
     * Entities which previously referenced the detached entity will continue to
     * reference it.
     *
     * @param object $entity The entity to detach.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     */
    public function detach($entity)
    {
    }
    /**
     * Merges the state of a detached entity into the persistence context
     * of this EntityManager and returns the managed copy of the entity.
     * The entity passed to merge will not become associated/managed with this EntityManager.
     *
     * @deprecated 2.7 This method is being removed from the ORM and won't have any replacement
     *
     * @param object $entity The detached entity to merge into the persistence context.
     *
     * @return object The managed copy of the entity.
     *
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function merge($entity)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
    }
    /**
     * Gets the repository for an entity class.
     *
     * @param string $entityName The name of the entity.
     * @psalm-param class-string<T> $entityName
     *
     * @return ObjectRepository|EntityRepository The repository class.
     * @psalm-return EntityRepository<T>
     *
     * @template T of object
     */
    public function getRepository($entityName)
    {
    }
    /**
     * Determines whether an entity instance is managed in this EntityManager.
     *
     * @param object $entity
     *
     * @return bool TRUE if this EntityManager currently manages the given entity, FALSE otherwise.
     */
    public function contains($entity)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
    }
    /**
     * Factory method to create EntityManager instances.
     *
     * @param mixed[]|Connection $connection   An array with the connection parameters or an existing Connection instance.
     * @param Configuration      $config       The Configuration instance to use.
     * @param EventManager|null  $eventManager The EventManager instance to use.
     * @psalm-param array<string, mixed>|Connection $connection
     *
     * @return EntityManager The created EntityManager.
     *
     * @throws InvalidArgumentException
     * @throws ORMException
     */
    public static function create($connection, \Doctrine\ORM\Configuration $config, ?\Doctrine\Common\EventManager $eventManager = null)
    {
    }
    /**
     * Factory method to create Connection instances.
     *
     * @param mixed[]|Connection $connection   An array with the connection parameters or an existing Connection instance.
     * @param Configuration      $config       The Configuration instance to use.
     * @param EventManager|null  $eventManager The EventManager instance to use.
     * @psalm-param array<string, mixed>|Connection $connection
     *
     * @return Connection
     *
     * @throws InvalidArgumentException
     * @throws ORMException
     */
    protected static function createConnection($connection, \Doctrine\ORM\Configuration $config, ?\Doctrine\Common\EventManager $eventManager = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
    }
}
