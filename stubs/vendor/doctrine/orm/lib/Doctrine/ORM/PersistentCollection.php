<?php

namespace Doctrine\ORM;

/**
 * A PersistentCollection represents a collection of elements that have persistent state.
 *
 * Collections of entities represent only the associations (links) to those entities.
 * That means, if the collection is part of a many-many mapping and you remove
 * entities from the collection, only the links in the relation table are removed (on flush).
 * Similarly, if you remove entities from a collection that is part of a one-many
 * mapping this will only result in the nulling out of the foreign keys on flush.
 *
 * @psalm-template TKey of array-key
 * @psalm-template T
 * @template-implements Collection<TKey,T>
 */
final class PersistentCollection extends \Doctrine\Common\Collections\AbstractLazyCollection implements \Doctrine\Common\Collections\Selectable
{
    /**
     * Creates a new persistent collection.
     *
     * @param EntityManagerInterface $em    The EntityManager the collection will be associated with.
     * @param ClassMetadata          $class The class descriptor of the entity type of this collection.
     * @psalm-param Collection<TKey, T> $collection The collection elements.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, $class, \Doctrine\Common\Collections\Collection $collection)
    {
    }
    /**
     * INTERNAL:
     * Sets the collection's owning entity together with the AssociationMapping that
     * describes the association between the owner and the elements of the collection.
     *
     * @param object $entity
     * @psalm-param array<string, mixed> $assoc
     */
    public function setOwner($entity, array $assoc) : void
    {
    }
    /**
     * INTERNAL:
     * Gets the collection owner.
     *
     * @return object|null
     */
    public function getOwner()
    {
    }
    /**
     * @return Mapping\ClassMetadata
     */
    public function getTypeClass() : \Doctrine\ORM\Mapping\ClassMetadataInfo
    {
    }
    /**
     * INTERNAL:
     * Adds an element to a collection during hydration. This will automatically
     * complete bidirectional associations in the case of a one-to-many association.
     *
     * @param mixed $element The element to add.
     */
    public function hydrateAdd($element) : void
    {
    }
    /**
     * INTERNAL:
     * Sets a keyed element in the collection during hydration.
     *
     * @param mixed $key     The key to set.
     * @param mixed $element The element to set.
     */
    public function hydrateSet($key, $element) : void
    {
    }
    /**
     * Initializes the collection by loading its contents from the database
     * if the collection is not yet initialized.
     */
    public function initialize() : void
    {
    }
    /**
     * INTERNAL:
     * Tells this collection to take a snapshot of its current state.
     */
    public function takeSnapshot() : void
    {
    }
    /**
     * INTERNAL:
     * Returns the last snapshot of the elements in the collection.
     *
     * @psalm-return array<string|int, mixed> The last snapshot of the elements.
     */
    public function getSnapshot() : array
    {
    }
    /**
     * INTERNAL:
     * getDeleteDiff
     *
     * @return mixed[]
     */
    public function getDeleteDiff() : array
    {
    }
    /**
     * INTERNAL:
     * getInsertDiff
     *
     * @return mixed[]
     */
    public function getInsertDiff() : array
    {
    }
    /**
     * INTERNAL: Gets the association mapping of the collection.
     *
     * @psalm-return array<string, mixed>|null
     */
    public function getMapping() : ?array
    {
    }
    /**
     * Gets a boolean flag indicating whether this collection is dirty which means
     * its state needs to be synchronized with the database.
     *
     * @return bool TRUE if the collection is dirty, FALSE otherwise.
     */
    public function isDirty() : bool
    {
    }
    /**
     * Sets a boolean flag, indicating whether this collection is dirty.
     *
     * @param bool $dirty Whether the collection should be marked dirty or not.
     */
    public function setDirty($dirty) : void
    {
    }
    /**
     * Sets the initialized flag of the collection, forcing it into that state.
     *
     * @param bool $bool
     */
    public function setInitialized($bool) : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return object
     */
    public function remove($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeElement($element) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsKey($key) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains($element) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
    }
    public function count() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($key, $value) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add($value) : bool
    {
    }
    /* ArrayAccess implementation */
    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value) : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return object|null
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
    }
    public function isEmpty() : bool
    {
    }
    public function clear() : void
    {
    }
    /**
     * Called by PHP when this collection is serialized. Ensures that only the
     * elements are properly serialized.
     *
     * Internal note: Tried to implement Serializable first but that did not work well
     *                with circular references. This solution seems simpler and works well.
     *
     * @return string[]
     * @psalm-return array{0: string, 1: string}
     */
    public function __sleep() : array
    {
    }
    /**
     * Extracts a slice of $length elements starting at position $offset from the Collection.
     *
     * If $length is null it returns all elements from $offset to the end of the Collection.
     * Keys have to be preserved by this method. Calling this method will only return the
     * selected slice and NOT change the elements contained in the collection slice is called on.
     *
     * @param int      $offset
     * @param int|null $length
     *
     * @return mixed[]
     * @psalm-return array<TKey,T>
     */
    public function slice($offset, $length = null) : array
    {
    }
    /**
     * Cleans up internal state of cloned persistent collection.
     *
     * The following problems have to be prevented:
     * 1. Added entities are added to old PC
     * 2. New collection is not dirty, if reused on other entity nothing
     * changes.
     * 3. Snapshot leads to invalid diffs being generated.
     * 4. Lazy loading grabs entities from old owner object.
     * 5. New collection is connected to old owner and leads to duplicate keys.
     */
    public function __clone()
    {
    }
    /**
     * Selects all elements from a selectable that match the expression and
     * return a new collection containing these elements.
     *
     * @psalm-return Collection<TKey, T>
     *
     * @throws RuntimeException
     */
    public function matching(\Doctrine\Common\Collections\Criteria $criteria) : \Doctrine\Common\Collections\Collection
    {
    }
    /**
     * Retrieves the wrapped Collection instance.
     *
     * @return Collection<TKey, T>
     */
    public function unwrap() : \Doctrine\Common\Collections\Collection
    {
    }
}
