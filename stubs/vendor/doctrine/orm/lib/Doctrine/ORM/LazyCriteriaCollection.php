<?php

namespace Doctrine\ORM;

/**
 * A lazy collection that allows a fast count when using criteria object
 * Once count gets executed once without collection being initialized, result
 * is cached and returned on subsequent calls until collection gets loaded,
 * then returning the number of loaded results.
 *
 * @template TKey of array-key
 * @template TValue of object
 * @extends AbstractLazyCollection<TKey, TValue>
 * @implements Selectable<TKey, TValue>
 */
class LazyCriteriaCollection extends \Doctrine\Common\Collections\AbstractLazyCollection implements \Doctrine\Common\Collections\Selectable
{
    /** @var EntityPersister */
    protected $entityPersister;
    /** @var Criteria */
    protected $criteria;
    public function __construct(\Doctrine\ORM\Persisters\Entity\EntityPersister $entityPersister, \Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * Do an efficient count on the collection
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * check if collection is empty without loading it
     *
     * @return bool TRUE if the collection is empty, FALSE otherwise.
     */
    public function isEmpty()
    {
    }
    /**
     * Do an optimized search of an element
     *
     * @param object $element
     * @psalm-param TValue $element
     *
     * @return bool
     */
    public function contains($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function matching(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function doInitialize()
    {
    }
}
