<?php

namespace Doctrine\ORM\Utility;

/**
 * The PersisterHelper contains logic to infer binding types which is used in
 * several persisters.
 *
 * @link   www.doctrine-project.org
 */
class PersisterHelper
{
    /**
     * @param string $fieldName
     *
     * @return array<int, string>
     *
     * @throws QueryException
     */
    public static function getTypeOfField($fieldName, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * @param string $columnName
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public static function getTypeOfColumn($columnName, \Doctrine\ORM\Mapping\ClassMetadata $class, \Doctrine\ORM\EntityManagerInterface $em)
    {
    }
}
