<?php

namespace Doctrine\ORM\Utility;

/**
 * The IdentifierFlattener utility now houses some of the identifier manipulation logic from unit of work, so that it
 * can be re-used elsewhere.
 */
final class IdentifierFlattener
{
    /**
     * Initializes a new IdentifierFlattener instance, bound to the given EntityManager.
     */
    public function __construct(\Doctrine\ORM\UnitOfWork $unitOfWork, \Doctrine\Persistence\Mapping\ClassMetadataFactory $metadataFactory)
    {
    }
    /**
     * convert foreign identifiers into scalar foreign key values to avoid object to string conversion failures.
     *
     * @param mixed[] $id
     *
     * @return mixed[]
     * @psalm-return array<string, mixed>
     */
    public function flattenIdentifier(\Doctrine\ORM\Mapping\ClassMetadata $class, array $id) : array
    {
    }
}
