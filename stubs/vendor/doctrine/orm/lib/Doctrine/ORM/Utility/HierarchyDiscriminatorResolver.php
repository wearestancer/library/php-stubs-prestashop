<?php

namespace Doctrine\ORM\Utility;

/**
 * @internal This class exists only to avoid code duplication, do not reuse it externally
 */
final class HierarchyDiscriminatorResolver
{
    /**
     * This method is needed to make INSTANCEOF work correctly with inheritance: if the class at hand has inheritance,
     * it extracts all the discriminators from the child classes and returns them
     *
     * @return null[]
     * @psalm-return array<array-key, null>
     */
    public static function resolveDiscriminatorsForClass(\Doctrine\Persistence\Mapping\ClassMetadata $rootClassMetadata, \Doctrine\ORM\EntityManagerInterface $entityManager) : array
    {
    }
}
