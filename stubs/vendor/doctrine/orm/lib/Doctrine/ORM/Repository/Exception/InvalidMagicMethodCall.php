<?php

namespace Doctrine\ORM\Repository\Exception;

final class InvalidMagicMethodCall extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\RepositoryException
{
    public static function becauseFieldNotFoundIn(string $entityName, string $fieldName, string $method) : self
    {
    }
    public static function onMissingParameter(string $methodName) : self
    {
    }
}
