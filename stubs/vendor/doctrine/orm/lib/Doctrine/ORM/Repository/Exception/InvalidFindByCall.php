<?php

namespace Doctrine\ORM\Repository\Exception;

final class InvalidFindByCall extends \Doctrine\ORM\Exception\ORMException implements \Doctrine\ORM\Exception\RepositoryException
{
    public static function fromInverseSideUsage(string $entityName, string $associationFieldName) : self
    {
    }
}
