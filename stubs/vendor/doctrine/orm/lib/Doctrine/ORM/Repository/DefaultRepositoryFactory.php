<?php

namespace Doctrine\ORM\Repository;

/**
 * This factory is used to create default repository objects for entities at runtime.
 */
final class DefaultRepositoryFactory implements \Doctrine\ORM\Repository\RepositoryFactory
{
    /**
     * {@inheritdoc}
     */
    public function getRepository(\Doctrine\ORM\EntityManagerInterface $entityManager, $entityName) : \Doctrine\Persistence\ObjectRepository
    {
    }
}
