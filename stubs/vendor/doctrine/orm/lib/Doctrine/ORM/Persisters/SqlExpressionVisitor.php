<?php

namespace Doctrine\ORM\Persisters;

/**
 * Visit Expressions and generate SQL WHERE conditions from them.
 */
class SqlExpressionVisitor extends \Doctrine\Common\Collections\Expr\ExpressionVisitor
{
    public function __construct(\Doctrine\ORM\Persisters\Entity\BasicEntityPersister $persister, \Doctrine\ORM\Mapping\ClassMetadata $classMetadata)
    {
    }
    /**
     * Converts a comparison expression into the target query language output.
     *
     * @return mixed
     */
    public function walkComparison(\Doctrine\Common\Collections\Expr\Comparison $comparison)
    {
    }
    /**
     * Converts a composite expression into the target query language output.
     *
     * @return string
     *
     * @throws RuntimeException
     */
    public function walkCompositeExpression(\Doctrine\Common\Collections\Expr\CompositeExpression $expr)
    {
    }
    /**
     * Converts a value expression into the target query language part.
     *
     * @return string
     */
    public function walkValue(\Doctrine\Common\Collections\Expr\Value $value)
    {
    }
}
