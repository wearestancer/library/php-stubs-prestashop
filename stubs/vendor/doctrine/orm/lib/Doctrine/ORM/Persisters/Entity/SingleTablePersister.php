<?php

namespace Doctrine\ORM\Persisters\Entity;

/**
 * Persister for entities that participate in a hierarchy mapped with the
 * SINGLE_TABLE strategy.
 *
 * @link https://martinfowler.com/eaaCatalog/singleTableInheritance.html
 */
class SingleTablePersister extends \Doctrine\ORM\Persisters\Entity\AbstractEntityInheritancePersister
{
    use \Doctrine\ORM\Internal\SQLResultCasing;
    /**
     * {@inheritdoc}
     */
    protected function getDiscriminatorColumnTableName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getSelectColumnsSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getInsertColumnList()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getSQLTableAlias($className, $assocName = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getSelectConditionSQL(array $criteria, $assoc = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getSelectConditionCriteriaSQL(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * @return string
     */
    protected function getSelectConditionDiscriminatorValueSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateFilterConditionSQL(\Doctrine\ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias)
    {
    }
}
