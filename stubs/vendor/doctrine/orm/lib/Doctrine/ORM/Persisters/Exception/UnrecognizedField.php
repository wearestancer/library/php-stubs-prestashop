<?php

namespace Doctrine\ORM\Persisters\Exception;

final class UnrecognizedField extends \Doctrine\ORM\Exception\PersisterException
{
    public static function byName(string $field) : self
    {
    }
}
