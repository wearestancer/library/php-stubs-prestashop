<?php

namespace Doctrine\ORM\Persisters\Entity;

/**
 * The joined subclass persister maps a single entity instance to several tables in the
 * database as it is defined by the <tt>Class Table Inheritance</tt> strategy.
 *
 * @see https://martinfowler.com/eaaCatalog/classTableInheritance.html
 */
class JoinedSubclassPersister extends \Doctrine\ORM\Persisters\Entity\AbstractEntityInheritancePersister
{
    use \Doctrine\ORM\Internal\SQLResultCasing;
    /**
     * {@inheritdoc}
     */
    protected function getDiscriminatorColumnTableName()
    {
    }
    /**
     * Gets the name of the table that owns the column the given field is mapped to.
     *
     * @param string $fieldName
     *
     * @return string
     *
     * @override
     */
    public function getOwningTable($fieldName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function executeInserts()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSelectSQL($criteria, $assoc = null, $lockMode = null, $limit = null, $offset = null, ?array $orderBy = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCountSQL($criteria = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getLockTablesSql($lockMode)
    {
    }
    /**
     * Ensure this method is never called. This persister overrides getSelectEntitiesSQL directly.
     *
     * @return string
     */
    protected function getSelectColumnsSQL()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getInsertColumnList()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function assignDefaultVersionAndUpsertableValues($entity, array $id)
    {
    }
}
