<?php

namespace Doctrine\ORM\Persisters\Exception;

class InvalidOrientation extends \Doctrine\ORM\Exception\PersisterException
{
    public static function fromClassNameAndField(string $className, string $field) : self
    {
    }
}
