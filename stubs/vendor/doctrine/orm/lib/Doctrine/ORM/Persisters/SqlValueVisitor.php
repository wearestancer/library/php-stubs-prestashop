<?php

namespace Doctrine\ORM\Persisters;

/**
 * Extract the values from a criteria/expression
 */
class SqlValueVisitor extends \Doctrine\Common\Collections\Expr\ExpressionVisitor
{
    /**
     * Converts a comparison expression into the target query language output.
     *
     * @return void
     */
    public function walkComparison(\Doctrine\Common\Collections\Expr\Comparison $comparison)
    {
    }
    /**
     * Converts a composite expression into the target query language output.
     *
     * @return void
     */
    public function walkCompositeExpression(\Doctrine\Common\Collections\Expr\CompositeExpression $expr)
    {
    }
    /**
     * Converts a value expression into the target query language part.
     *
     * @return void
     */
    public function walkValue(\Doctrine\Common\Collections\Expr\Value $value)
    {
    }
    /**
     * Returns the Parameters and Types necessary for matching the last visited expression.
     *
     * @return mixed[][]
     * @psalm-return array{0: array, 1: array<array<mixed>>}
     */
    public function getParamsAndTypes()
    {
    }
    /**
     * Returns the value from a Comparison. In case of a CONTAINS comparison,
     * the value is wrapped in %-signs, because it will be used in a LIKE clause.
     *
     * @return mixed
     */
    protected function getValueFromComparison(\Doctrine\Common\Collections\Expr\Comparison $comparison)
    {
    }
}
