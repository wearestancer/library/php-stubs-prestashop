<?php

namespace Doctrine\ORM\Persisters\Collection;

/**
 * Base class for all collection persisters.
 */
abstract class AbstractCollectionPersister implements \Doctrine\ORM\Persisters\Collection\CollectionPersister
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var Connection */
    protected $conn;
    /** @var UnitOfWork */
    protected $uow;
    /**
     * The database platform.
     *
     * @var AbstractPlatform
     */
    protected $platform;
    /** @var QuoteStrategy */
    protected $quoteStrategy;
    /**
     * Initializes a new instance of a class derived from AbstractCollectionPersister.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Check if entity is in a valid state for operations.
     *
     * @param object $entity
     *
     * @return bool
     */
    protected function isValidEntityState($entity)
    {
    }
}
