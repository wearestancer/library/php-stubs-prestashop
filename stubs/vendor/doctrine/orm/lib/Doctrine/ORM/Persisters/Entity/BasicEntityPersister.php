<?php

namespace Doctrine\ORM\Persisters\Entity;

/**
 * A BasicEntityPersister maps an entity to a single table in a relational database.
 *
 * A persister is always responsible for a single entity type.
 *
 * EntityPersisters are used during a UnitOfWork to apply any changes to the persistent
 * state of entities onto a relational database when the UnitOfWork is committed,
 * as well as for basic querying of entities and their associations (not DQL).
 *
 * The persisting operations that are invoked during a commit of a UnitOfWork to
 * persist the persistent entity state are:
 *
 *   - {@link addInsert} : To schedule an entity for insertion.
 *   - {@link executeInserts} : To execute all scheduled insertions.
 *   - {@link update} : To update the persistent state of an entity.
 *   - {@link delete} : To delete the persistent state of an entity.
 *
 * As can be seen from the above list, insertions are batched and executed all at once
 * for increased efficiency.
 *
 * The querying operations invoked during a UnitOfWork, either through direct find
 * requests or lazy-loading, are the following:
 *
 *   - {@link load} : Loads (the state of) a single, managed entity.
 *   - {@link loadAll} : Loads multiple, managed entities.
 *   - {@link loadOneToOneEntity} : Loads a one/many-to-one entity association (lazy-loading).
 *   - {@link loadOneToManyCollection} : Loads a one-to-many entity association (lazy-loading).
 *   - {@link loadManyToManyCollection} : Loads a many-to-many entity association (lazy-loading).
 *
 * The BasicEntityPersister implementation provides the default behavior for
 * persisting and querying entities that are mapped to a single database table.
 *
 * Subclasses can be created to provide custom persisting and querying strategies,
 * i.e. spanning multiple tables.
 */
class BasicEntityPersister implements \Doctrine\ORM\Persisters\Entity\EntityPersister
{
    /**
     * Metadata object that describes the mapping of the mapped entity class.
     *
     * @var ClassMetadata
     */
    protected $class;
    /**
     * The underlying DBAL Connection of the used EntityManager.
     *
     * @var Connection $conn
     */
    protected $conn;
    /**
     * The database platform.
     *
     * @var AbstractPlatform
     */
    protected $platform;
    /**
     * The EntityManager instance.
     *
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * Queued inserts.
     *
     * @psalm-var array<int, object>
     */
    protected $queuedInserts = [];
    /**
     * The map of column names to DBAL mapping types of all prepared columns used
     * when INSERTing or UPDATEing an entity.
     *
     * @see prepareInsertData($entity)
     * @see prepareUpdateData($entity)
     *
     * @var mixed[]
     */
    protected $columnTypes = [];
    /**
     * The map of quoted column names.
     *
     * @see prepareInsertData($entity)
     * @see prepareUpdateData($entity)
     *
     * @var mixed[]
     */
    protected $quotedColumns = [];
    /**
     * The quote strategy.
     *
     * @var QuoteStrategy
     */
    protected $quoteStrategy;
    /** @var CachedPersisterContext */
    protected $currentPersisterContext;
    /**
     * Initializes a new <tt>BasicEntityPersister</tt> that uses the given EntityManager
     * and persists instances of the class described by the given ClassMetadata descriptor.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassMetadata()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResultSetMapping()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addInsert($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInserts()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function executeInserts()
    {
    }
    /**
     * Retrieves the default version value which was created
     * by the preceding INSERT statement and assigns it back in to the
     * entities version field if the given entity is versioned.
     * Also retrieves values of columns marked as 'non insertable' and / or
     * 'not updatable' and assigns them back to the entities corresponding fields.
     *
     * @param object  $entity
     * @param mixed[] $id
     *
     * @return void
     */
    protected function assignDefaultVersionAndUpsertableValues($entity, array $id)
    {
    }
    /**
     * Fetches the current version value of a versioned entity and / or the values of fields
     * marked as 'not insertable' and / or 'not updatable'.
     *
     * @param ClassMetadata $versionedClass
     * @param mixed[]       $id
     *
     * @return mixed
     */
    protected function fetchVersionAndNotUpsertableValues($versionedClass, array $id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($entity)
    {
    }
    /**
     * Performs an UPDATE statement for an entity on a specific table.
     * The UPDATE can optionally be versioned, which requires the entity to have a version field.
     *
     * @param object  $entity          The entity object being updated.
     * @param string  $quotedTableName The quoted name of the table to apply the UPDATE on.
     * @param mixed[] $updateData      The map of columns to update (column => value).
     * @param bool    $versioned       Whether the UPDATE should be versioned.
     *
     * @throws UnrecognizedField
     * @throws OptimisticLockException
     */
    protected final function updateTable($entity, $quotedTableName, array $updateData, $versioned = false) : void
    {
    }
    /**
     * @param array<mixed> $identifier
     * @param string[]     $types
     *
     * @todo Add check for platform if it supports foreign keys/cascading.
     */
    protected function deleteJoinTableRecords(array $identifier, array $types) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete($entity)
    {
    }
    /**
     * Prepares the changeset of an entity for database insertion (UPDATE).
     *
     * The changeset is obtained from the currently running UnitOfWork.
     *
     * During this preparation the array that is passed as the second parameter is filled with
     * <columnName> => <value> pairs, grouped by table name.
     *
     * Example:
     * <code>
     * array(
     *    'foo_table' => array('column1' => 'value1', 'column2' => 'value2', ...),
     *    'bar_table' => array('columnX' => 'valueX', 'columnY' => 'valueY', ...),
     *    ...
     * )
     * </code>
     *
     * @param object $entity   The entity for which to prepare the data.
     * @param bool   $isInsert Whether the data to be prepared refers to an insert statement.
     *
     * @return mixed[][] The prepared data.
     * @psalm-return array<string, array<array-key, mixed|null>>
     */
    protected function prepareUpdateData($entity, bool $isInsert = false)
    {
    }
    /**
     * Prepares the data changeset of a managed entity for database insertion (initial INSERT).
     * The changeset of the entity is obtained from the currently running UnitOfWork.
     *
     * The default insert data preparation is the same as for updates.
     *
     * @see prepareUpdateData
     *
     * @param object $entity The entity for which to prepare the data.
     *
     * @return mixed[][] The prepared data for the tables to update.
     * @psalm-return array<string, mixed[]>
     */
    protected function prepareInsertData($entity)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOwningTable($fieldName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(array $criteria, $entity = null, $assoc = null, array $hints = [], $lockMode = null, $limit = null, ?array $orderBy = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadById(array $identifier, $entity = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadOneToOneEntity(array $assoc, $sourceEntity, array $identifier = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refresh(array $id, $entity, $lockMode = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function count($criteria = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadCriteria(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function expandCriteriaParameters(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadAll(array $criteria = [], ?array $orderBy = null, $limit = null, $offset = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getManyToManyCollection(array $assoc, $sourceEntity, $offset = null, $limit = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadManyToManyCollection(array $assoc, $sourceEntity, \Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSelectSQL($criteria, $assoc = null, $lockMode = null, $limit = null, $offset = null, ?array $orderBy = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getCountSQL($criteria = [])
    {
    }
    /**
     * Gets the ORDER BY SQL snippet for ordered collections.
     *
     * @psalm-param array<string, string> $orderBy
     *
     * @throws InvalidOrientation
     * @throws InvalidFindByCall
     * @throws UnrecognizedField
     */
    protected final function getOrderBySQL(array $orderBy, string $baseTableAlias) : string
    {
    }
    /**
     * Gets the SQL fragment with the list of columns to select when querying for
     * an entity in this persister.
     *
     * Subclasses should override this method to alter or change the select column
     * list SQL fragment. Note that in the implementation of BasicEntityPersister
     * the resulting SQL fragment is generated only once and cached in {@link selectColumnListSql}.
     * Subclasses may or may not do the same.
     *
     * @return string The SQL fragment.
     */
    protected function getSelectColumnsSQL()
    {
    }
    /**
     * Gets the SQL join fragment used when selecting entities from an association.
     *
     * @param string  $field
     * @param mixed[] $assoc
     * @param string  $alias
     *
     * @return string
     */
    protected function getSelectColumnAssociationSQL($field, $assoc, \Doctrine\ORM\Mapping\ClassMetadata $class, $alias = 'r')
    {
    }
    /**
     * Gets the SQL join fragment used when selecting entities from a
     * many-to-many association.
     *
     * @psalm-param array<string, mixed> $manyToMany
     *
     * @return string
     */
    protected function getSelectManyToManyJoinSQL(array $manyToMany)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInsertSQL()
    {
    }
    /**
     * Gets the list of columns to put in the INSERT SQL statement.
     *
     * Subclasses should override this method to alter or change the list of
     * columns placed in the INSERT statements used by the persister.
     *
     * @return string[] The list of columns.
     * @psalm-return list<string>
     */
    protected function getInsertColumnList()
    {
    }
    /**
     * Gets the SQL snippet of a qualified column name for the given field name.
     *
     * @param string        $field The field name.
     * @param ClassMetadata $class The class that declares this field. The table this class is
     *                             mapped to must own the column for the given field.
     * @param string        $alias
     *
     * @return string
     */
    protected function getSelectColumnSQL($field, \Doctrine\ORM\Mapping\ClassMetadata $class, $alias = 'r')
    {
    }
    /**
     * Gets the SQL table alias for the given class name.
     *
     * @param string $className
     * @param string $assocName
     *
     * @return string The SQL table alias.
     *
     * @todo Reconsider. Binding table aliases to class names is not such a good idea.
     */
    protected function getSQLTableAlias($className, $assocName = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lock(array $criteria, $lockMode)
    {
    }
    /**
     * Gets the FROM and optionally JOIN conditions to lock the entity managed by this persister.
     *
     * @param int|null $lockMode One of the Doctrine\DBAL\LockMode::* constants.
     * @psalm-param LockMode::*|null $lockMode
     *
     * @return string
     */
    protected function getLockTablesSql($lockMode)
    {
    }
    /**
     * Gets the Select Where Condition from a Criteria object.
     *
     * @return string
     */
    protected function getSelectConditionCriteriaSQL(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSelectConditionStatementSQL($field, $value, $assoc = null, $comparison = null)
    {
    }
    /**
     * Gets the conditional SQL fragment used in the WHERE clause when selecting
     * entities in this persister.
     *
     * Subclasses are supposed to override this method if they intend to change
     * or alter the criteria by which entities are selected.
     *
     * @param mixed[]|null $assoc
     * @psalm-param array<string, mixed> $criteria
     * @psalm-param array<string, mixed>|null $assoc
     *
     * @return string
     */
    protected function getSelectConditionSQL(array $criteria, $assoc = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOneToManyCollection(array $assoc, $sourceEntity, $offset = null, $limit = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadOneToManyCollection(array $assoc, $sourceEntity, \Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function expandParameters($criteria)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists($entity, ?\Doctrine\Common\Collections\Criteria $extraConditions = null)
    {
    }
    /**
     * Generates the appropriate join SQL for the given join column.
     *
     * @param array[] $joinColumns The join columns definition of an association.
     * @psalm-param array<array<string, mixed>> $joinColumns
     *
     * @return string LEFT JOIN if one of the columns is nullable, INNER JOIN otherwise.
     */
    protected function getJoinSQLForJoinColumns($joinColumns)
    {
    }
    /**
     * @param string $columnName
     *
     * @return string
     */
    public function getSQLColumnAlias($columnName)
    {
    }
    /**
     * Generates the filter SQL for a given entity and table alias.
     *
     * @param ClassMetadata $targetEntity     Metadata of the target entity.
     * @param string        $targetTableAlias The table alias of the joined/selected table.
     *
     * @return string The SQL query part to add to a query.
     */
    protected function generateFilterConditionSQL(\Doctrine\ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias)
    {
    }
    /**
     * Switches persister context according to current query offset/limits
     *
     * This is due to the fact that to-many associations cannot be fetch-joined when a limit is involved
     *
     * @param int|null $offset
     * @param int|null $limit
     *
     * @return void
     */
    protected function switchPersisterContext($offset, $limit)
    {
    }
    /**
     * @return string[]
     * @psalm-return list<string>
     */
    protected function getClassIdentifiersTypes(\Doctrine\ORM\Mapping\ClassMetadata $class) : array
    {
    }
}
