<?php

namespace Doctrine\ORM\Persisters;

final class MatchingAssociationFieldRequiresObject extends \Doctrine\ORM\Exception\PersisterException
{
    public static function fromClassAndAssociation(string $class, string $associationName) : self
    {
    }
}
