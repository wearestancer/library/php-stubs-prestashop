<?php

namespace Doctrine\ORM\Persisters\Entity;

/**
 * Base class for entity persisters that implement a certain inheritance mapping strategy.
 * All these persisters are assumed to use a discriminator column to discriminate entity
 * types in the hierarchy.
 */
abstract class AbstractEntityInheritancePersister extends \Doctrine\ORM\Persisters\Entity\BasicEntityPersister
{
    /**
     * {@inheritdoc}
     */
    protected function prepareInsertData($entity)
    {
    }
    /**
     * Gets the name of the table that contains the discriminator column.
     *
     * @return string The table name.
     */
    protected abstract function getDiscriminatorColumnTableName();
    /**
     * {@inheritdoc}
     */
    protected function getSelectColumnSQL($field, \Doctrine\ORM\Mapping\ClassMetadata $class, $alias = 'r')
    {
    }
    /**
     * @param string $tableAlias
     * @param string $joinColumnName
     * @param string $quotedColumnName
     * @param string $type
     *
     * @return string
     */
    protected function getSelectJoinColumnSQL($tableAlias, $joinColumnName, $quotedColumnName, $type)
    {
    }
}
