<?php

namespace Doctrine\ORM\Persisters;

class PersisterException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @param string $class
     * @param string $associationName
     *
     * @return PersisterException
     */
    public static function matchingAssocationFieldRequiresObject($class, $associationName)
    {
    }
}
