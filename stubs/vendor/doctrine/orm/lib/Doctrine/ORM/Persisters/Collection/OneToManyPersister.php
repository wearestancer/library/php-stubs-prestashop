<?php

namespace Doctrine\ORM\Persisters\Collection;

/**
 * Persister for one-to-many collections.
 */
class OneToManyPersister extends \Doctrine\ORM\Persisters\Collection\AbstractCollectionPersister
{
    /**
     * {@inheritdoc}
     *
     * @return int|null
     */
    public function delete(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(\Doctrine\ORM\PersistentCollection $collection, $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function slice(\Doctrine\ORM\PersistentCollection $collection, $offset, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsKey(\Doctrine\ORM\PersistentCollection $collection, $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains(\Doctrine\ORM\PersistentCollection $collection, $element)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadCriteria(\Doctrine\ORM\PersistentCollection $collection, \Doctrine\Common\Collections\Criteria $criteria)
    {
    }
}
