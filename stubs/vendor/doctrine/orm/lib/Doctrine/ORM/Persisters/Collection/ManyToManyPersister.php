<?php

namespace Doctrine\ORM\Persisters\Collection;

/**
 * Persister for many-to-many collections.
 */
class ManyToManyPersister extends \Doctrine\ORM\Persisters\Collection\AbstractCollectionPersister
{
    /**
     * {@inheritdoc}
     */
    public function delete(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(\Doctrine\ORM\PersistentCollection $collection, $index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function slice(\Doctrine\ORM\PersistentCollection $collection, $offset, $length = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function containsKey(\Doctrine\ORM\PersistentCollection $collection, $key)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function contains(\Doctrine\ORM\PersistentCollection $collection, $element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadCriteria(\Doctrine\ORM\PersistentCollection $collection, \Doctrine\Common\Collections\Criteria $criteria)
    {
    }
    /**
     * Generates the filter SQL for a given mapping.
     *
     * This method is not used for actually grabbing the related entities
     * but when the extra-lazy collection methods are called on a filtered
     * association. This is why besides the many to many table we also
     * have to join in the actual entities table leading to additional
     * JOIN.
     *
     * @param mixed[] $mapping Array containing mapping information.
     * @psalm-param array<string, mixed> $mapping
     *
     * @return string[] ordered tuple:
     *                   - JOIN condition to add to the SQL
     *                   - WHERE condition to add to the SQL
     * @psalm-return array{0: string, 1: string}
     */
    public function getFilterSql($mapping)
    {
    }
    /**
     * Generates the filter SQL for a given entity and table alias.
     *
     * @param ClassMetadata $targetEntity     Metadata of the target entity.
     * @param string        $targetTableAlias The table alias of the joined/selected table.
     *
     * @return string The SQL query part to add to a query.
     */
    protected function generateFilterConditionSQL(\Doctrine\ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias)
    {
    }
    /**
     * Generate ON condition
     *
     * @param mixed[] $mapping
     * @psalm-param array<string, mixed> $mapping
     *
     * @return string[]
     * @psalm-return list<string>
     */
    protected function getOnConditionSQL($mapping)
    {
    }
    /**
     * @return string
     */
    protected function getDeleteSQL(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * Internal note: Order of the parameters must be the same as the order of the columns in getDeleteSql.
     *
     * @return list<mixed>
     */
    protected function getDeleteSQLParameters(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * Gets the SQL statement used for deleting a row from the collection.
     *
     * @return string[]|string[][] ordered tuple containing the SQL to be executed and an array
     *                             of types for bound parameters
     * @psalm-return array{0: string, 1: list<string>}
     */
    protected function getDeleteRowSQL(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * Gets the SQL parameters for the corresponding SQL statement to delete the given
     * element from the given collection.
     *
     * Internal note: Order of the parameters must be the same as the order of the columns in getDeleteRowSql.
     *
     * @param mixed $element
     *
     * @return mixed[]
     * @psalm-return list<mixed>
     */
    protected function getDeleteRowSQLParameters(\Doctrine\ORM\PersistentCollection $collection, $element)
    {
    }
    /**
     * Gets the SQL statement used for inserting a row in the collection.
     *
     * @return string[]|string[][] ordered tuple containing the SQL to be executed and an array
     *                             of types for bound parameters
     * @psalm-return array{0: string, 1: list<string>}
     */
    protected function getInsertRowSQL(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * Gets the SQL parameters for the corresponding SQL statement to insert the given
     * element of the given collection into the database.
     *
     * Internal note: Order of the parameters must be the same as the order of the columns in getInsertRowSql.
     *
     * @param object $element
     *
     * @return mixed[]
     * @psalm-return list<mixed>
     */
    protected function getInsertRowSQLParameters(\Doctrine\ORM\PersistentCollection $collection, $element)
    {
    }
}
