<?php

namespace Doctrine\ORM\Persisters\Collection;

/**
 * Define the behavior that should be implemented by all collection persisters.
 */
interface CollectionPersister
{
    /**
     * Deletes the persistent state represented by the given collection.
     *
     * @return void
     */
    public function delete(\Doctrine\ORM\PersistentCollection $collection);
    /**
     * Updates the given collection, synchronizing its state with the database
     * by inserting, updating and deleting individual elements.
     *
     * @return void
     */
    public function update(\Doctrine\ORM\PersistentCollection $collection);
    /**
     * Counts the size of this persistent collection.
     *
     * @return int
     */
    public function count(\Doctrine\ORM\PersistentCollection $collection);
    /**
     * Slices elements.
     *
     * @param int      $offset
     * @param int|null $length
     *
     * @return mixed[]
     */
    public function slice(\Doctrine\ORM\PersistentCollection $collection, $offset, $length = null);
    /**
     * Checks for existence of an element.
     *
     * @param object $element
     *
     * @return bool
     */
    public function contains(\Doctrine\ORM\PersistentCollection $collection, $element);
    /**
     * Checks for existence of a key.
     *
     * @param mixed $key
     *
     * @return bool
     */
    public function containsKey(\Doctrine\ORM\PersistentCollection $collection, $key);
    /**
     * Gets an element by key.
     *
     * @param mixed $index
     *
     * @return mixed
     */
    public function get(\Doctrine\ORM\PersistentCollection $collection, $index);
    /**
     * Loads association entities matching the given Criteria object.
     *
     * @return mixed[]
     */
    public function loadCriteria(\Doctrine\ORM\PersistentCollection $collection, \Doctrine\Common\Collections\Criteria $criteria);
}
