<?php

namespace Doctrine\ORM\Persisters\Exception;

class CantUseInOperatorOnCompositeKeys extends \Doctrine\ORM\Exception\PersisterException
{
    public static function create() : self
    {
    }
}
