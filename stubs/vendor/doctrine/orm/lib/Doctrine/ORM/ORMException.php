<?php

namespace Doctrine\ORM;

/**
 * Base exception class for all ORM exceptions.
 *
 * @deprecated Use Doctrine\ORM\Exception\ORMException for catch and instanceof
 */
class ORMException extends \Exception
{
    /**
     * @deprecated Use Doctrine\ORM\Exception\MissingMappingDriverImplementation
     *
     * @return ORMException
     */
    public static function missingMappingDriverImpl()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\NamedQueryNotFound
     *
     * @param string $queryName
     *
     * @return ORMException
     */
    public static function namedQueryNotFound($queryName)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\NamedQueryNotFound
     *
     * @param string $nativeQueryName
     *
     * @return ORMException
     */
    public static function namedNativeQueryNotFound($nativeQueryName)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Persisters\Exception\UnrecognizedField
     *
     * @param string $field
     *
     * @return ORMException
     */
    public static function unrecognizedField($field)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\UnexpectedAssociationValue
     *
     * @param string $class
     * @param string $association
     * @param string $given
     * @param string $expected
     *
     * @return ORMException
     */
    public static function unexpectedAssociationValue($class, $association, $given, $expected)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Persisters\Exception\InvalidOrientation
     *
     * @param string $className
     * @param string $field
     *
     * @return ORMException
     */
    public static function invalidOrientation($className, $field)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\EntityManagerClosed
     *
     * @return ORMException
     */
    public static function entityManagerClosed()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\InvalidHydrationMode
     *
     * @param string $mode
     *
     * @return ORMException
     */
    public static function invalidHydrationMode($mode)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\MismatchedEventManager
     *
     * @return ORMException
     */
    public static function mismatchedEventManager()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Repository\Exception\InvalidMagicMethodCall::onMissingParameter()
     *
     * @param string $methodName
     *
     * @return ORMException
     */
    public static function findByRequiresParameter($methodName)
    {
    }
    /**
     * @deprecated Doctrine\ORM\Repository\Exception\InvalidFindByCall
     *
     * @param string $entityName
     * @param string $fieldName
     * @param string $method
     *
     * @return ORMException
     */
    public static function invalidMagicCall($entityName, $fieldName, $method)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Repository\Exception\InvalidFindByCall::fromInverseSideUsage()
     *
     * @param string $entityName
     * @param string $associationFieldName
     *
     * @return ORMException
     */
    public static function invalidFindByInverseAssociation($entityName, $associationFieldName)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Cache\Exception\InvalidResultCacheDriver
     *
     * @return ORMException
     */
    public static function invalidResultCacheDriver()
    {
    }
    /**
     * @deprecated Doctrine\ORM\Tools\Exception\NotSupported
     *
     * @return ORMException
     */
    public static function notSupported()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Cache\Exception\QueryCacheNotConfigured
     *
     * @return ORMException
     */
    public static function queryCacheNotConfigured()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Cache\Exception\MetadataCacheNotConfigured
     *
     * @return ORMException
     */
    public static function metadataCacheNotConfigured()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Cache\Exception\QueryCacheUsesNonPersistentCache
     *
     * @return ORMException
     */
    public static function queryCacheUsesNonPersistentCache(\Doctrine\Common\Cache\Cache $cache)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Cache\Exception\MetadataCacheUsesNonPersistentCache
     *
     * @return ORMException
     */
    public static function metadataCacheUsesNonPersistentCache(\Doctrine\Common\Cache\Cache $cache)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\ProxyClassesAlwaysRegenerating
     *
     * @return ORMException
     */
    public static function proxyClassesAlwaysRegenerating()
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\UnknownEntityNamespace
     *
     * @param string $entityNamespaceAlias
     *
     * @return ORMException
     */
    public static function unknownEntityNamespace($entityNamespaceAlias)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\InvalidEntityRepository
     *
     * @param string $className
     *
     * @return ORMException
     */
    public static function invalidEntityRepository($className)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\MissingIdentifierField
     *
     * @param string $className
     * @param string $fieldName
     *
     * @return ORMException
     */
    public static function missingIdentifierField($className, $fieldName)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Exception\UnrecognizedIdentifierFields
     *
     * @param string   $className
     * @param string[] $fieldNames
     *
     * @return ORMException
     */
    public static function unrecognizedIdentifierFields($className, $fieldNames)
    {
    }
    /**
     * @deprecated Use Doctrine\ORM\Persisters\Exception\CantUseInOperatorOnCompositeKeys
     *
     * @return ORMException
     */
    public static function cantUseInOperatorOnCompositeKeys()
    {
    }
}
