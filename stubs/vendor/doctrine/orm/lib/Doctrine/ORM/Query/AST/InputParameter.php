<?php

namespace Doctrine\ORM\Query\AST;

class InputParameter extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $isNamed;
    /** @var string */
    public $name;
    /**
     * @param string $value
     *
     * @throws QueryException
     */
    public function __construct($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
