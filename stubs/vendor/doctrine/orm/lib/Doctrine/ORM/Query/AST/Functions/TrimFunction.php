<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "TRIM" "(" [["LEADING" | "TRAILING" | "BOTH"] [char] "FROM"] StringPrimary ")"
 *
 * @link    www.doctrine-project.org
 */
class TrimFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var bool */
    public $leading;
    /** @var bool */
    public $trailing;
    /** @var bool */
    public $both;
    /** @var string|false */
    public $trimChar = false;
    /** @var Node */
    public $stringPrimary;
    /**
     * {@inheritdoc}
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
