<?php

namespace Doctrine\ORM\Query;

/**
 * The SqlWalker is a TreeWalker that walks over a DQL AST and constructs
 * the corresponding SQL.
 *
 * @psalm-import-type QueryComponent from Parser
 * @psalm-consistent-constructor
 */
class SqlWalker implements \Doctrine\ORM\Query\TreeWalker
{
    public const HINT_DISTINCT = 'doctrine.distinct';
    /**
     * Used to mark a query as containing a PARTIAL expression, which needs to be known by SLC.
     */
    public const HINT_PARTIAL = 'doctrine.partial';
    /**
     * @param Query        $query        The parsed Query.
     * @param ParserResult $parserResult The result of the parsing process.
     * @psalm-param array<string, QueryComponent> $queryComponents The query components (symbol table).
     */
    public function __construct($query, $parserResult, array $queryComponents)
    {
    }
    /**
     * Gets the Query instance used by the walker.
     *
     * @return Query
     */
    public function getQuery()
    {
    }
    /**
     * Gets the Connection used by the walker.
     *
     * @return Connection
     */
    public function getConnection()
    {
    }
    /**
     * Gets the EntityManager used by the walker.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
    /**
     * Gets the information about a single query component.
     *
     * @param string $dqlAlias The DQL alias.
     *
     * @return mixed[]
     * @psalm-return QueryComponent
     */
    public function getQueryComponent($dqlAlias)
    {
    }
    public function getMetadataForDqlAlias(string $dqlAlias) : \Doctrine\ORM\Mapping\ClassMetadata
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getQueryComponents()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setQueryComponent($dqlAlias, array $queryComponent)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExecutor($AST)
    {
    }
    /**
     * Generates a unique, short SQL table alias.
     *
     * @param string $tableName Table name
     * @param string $dqlAlias  The DQL alias.
     *
     * @return string Generated table alias.
     */
    public function getSQLTableAlias($tableName, $dqlAlias = '')
    {
    }
    /**
     * Forces the SqlWalker to use a specific alias for a table name, rather than
     * generating an alias on its own.
     *
     * @param string $tableName
     * @param string $alias
     * @param string $dqlAlias
     *
     * @return string
     */
    public function setSQLTableAlias($tableName, $alias, $dqlAlias = '')
    {
    }
    /**
     * Gets an SQL column alias for a column name.
     *
     * @param string $columnName
     *
     * @return string
     */
    public function getSQLColumnAlias($columnName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSelectStatement(\Doctrine\ORM\Query\AST\SelectStatement $AST)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkUpdateStatement(\Doctrine\ORM\Query\AST\UpdateStatement $AST)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkDeleteStatement(\Doctrine\ORM\Query\AST\DeleteStatement $AST)
    {
    }
    /**
     * Walks down an IdentificationVariable AST node, thereby generating the appropriate SQL.
     * This one differs of ->walkIdentificationVariable() because it generates the entity identifiers.
     *
     * @param string $identVariable
     *
     * @return string
     */
    public function walkEntityIdentificationVariable($identVariable)
    {
    }
    /**
     * Walks down an IdentificationVariable (no AST node associated), thereby generating the SQL.
     *
     * @param string $identificationVariable
     * @param string $fieldName
     *
     * @return string The SQL.
     */
    public function walkIdentificationVariable($identificationVariable, $fieldName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkPathExpression($pathExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSelectClause($selectClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkFromClause($fromClause)
    {
    }
    /**
     * Walks down a IdentificationVariableDeclaration AST node, thereby generating the appropriate SQL.
     *
     * @param AST\IdentificationVariableDeclaration $identificationVariableDecl
     *
     * @return string
     */
    public function walkIdentificationVariableDeclaration($identificationVariableDecl)
    {
    }
    /**
     * Walks down a IndexBy AST node.
     *
     * @param AST\IndexBy $indexBy
     *
     * @return void
     */
    public function walkIndexBy($indexBy)
    {
    }
    /**
     * Walks down a RangeVariableDeclaration AST node, thereby generating the appropriate SQL.
     *
     * @param AST\RangeVariableDeclaration $rangeVariableDeclaration
     *
     * @return string
     */
    public function walkRangeVariableDeclaration($rangeVariableDeclaration)
    {
    }
    /**
     * Walks down a JoinAssociationDeclaration AST node, thereby generating the appropriate SQL.
     *
     * @param AST\JoinAssociationDeclaration $joinAssociationDeclaration
     * @param int                            $joinType
     * @param AST\ConditionalExpression      $condExpr
     * @psalm-param AST\Join::JOIN_TYPE_* $joinType
     *
     * @return string
     *
     * @throws QueryException
     */
    public function walkJoinAssociationDeclaration($joinAssociationDeclaration, $joinType = \Doctrine\ORM\Query\AST\Join::JOIN_TYPE_INNER, $condExpr = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkFunction($function)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkOrderByClause($orderByClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkOrderByItem($orderByItem)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkHavingClause($havingClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkJoin($join)
    {
    }
    /**
     * Walks down a CoalesceExpression AST node and generates the corresponding SQL.
     *
     * @param AST\CoalesceExpression $coalesceExpression
     *
     * @return string The SQL.
     */
    public function walkCoalesceExpression($coalesceExpression)
    {
    }
    /**
     * Walks down a NullIfExpression AST node and generates the corresponding SQL.
     *
     * @param AST\NullIfExpression $nullIfExpression
     *
     * @return string The SQL.
     */
    public function walkNullIfExpression($nullIfExpression)
    {
    }
    /**
     * Walks down a GeneralCaseExpression AST node and generates the corresponding SQL.
     *
     * @return string The SQL.
     */
    public function walkGeneralCaseExpression(\Doctrine\ORM\Query\AST\GeneralCaseExpression $generalCaseExpression)
    {
    }
    /**
     * Walks down a SimpleCaseExpression AST node and generates the corresponding SQL.
     *
     * @param AST\SimpleCaseExpression $simpleCaseExpression
     *
     * @return string The SQL.
     */
    public function walkSimpleCaseExpression($simpleCaseExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSelectExpression($selectExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkQuantifiedExpression($qExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSubselect($subselect)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSubselectFromClause($subselectFromClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSimpleSelectClause($simpleSelectClause)
    {
    }
    /**
     * @return string
     */
    public function walkParenthesisExpression(\Doctrine\ORM\Query\AST\ParenthesisExpression $parenthesisExpression)
    {
    }
    /**
     * @param AST\NewObjectExpression $newObjectExpression
     * @param string|null             $newObjectResultAlias
     *
     * @return string The SQL.
     */
    public function walkNewObject($newObjectExpression, $newObjectResultAlias = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSimpleSelectExpression($simpleSelectExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkAggregateExpression($aggExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkGroupByClause($groupByClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkGroupByItem($groupByItem)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkDeleteClause(\Doctrine\ORM\Query\AST\DeleteClause $deleteClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkUpdateClause($updateClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkUpdateItem($updateItem)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkWhereClause($whereClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkConditionalExpression($condExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkConditionalTerm($condTerm)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkConditionalFactor($factor)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkConditionalPrimary($primary)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkExistsExpression($existsExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkCollectionMemberExpression($collMemberExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkEmptyCollectionComparisonExpression($emptyCollCompExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkNullComparisonExpression($nullCompExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkInExpression($inExpr)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws QueryException
     */
    public function walkInstanceOfExpression($instanceOfExpr)
    {
    }
    /**
     * @param mixed $inParam
     *
     * @return string
     */
    public function walkInParameter($inParam)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkLiteral($literal)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkBetweenExpression($betweenExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkLikeExpression($likeExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkStateFieldPathExpression($stateFieldPathExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkComparisonExpression($compExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkInputParameter($inputParam)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkArithmeticExpression($arithmeticExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkSimpleArithmeticExpression($simpleArithmeticExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkArithmeticTerm($term)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkArithmeticFactor($factor)
    {
    }
    /**
     * Walks down an ArithmeticPrimary that represents an AST node, thereby generating the appropriate SQL.
     *
     * @param mixed $primary
     *
     * @return string The SQL.
     */
    public function walkArithmeticPrimary($primary)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkStringPrimary($stringPrimary)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function walkResultVariable($resultVariable)
    {
    }
}
