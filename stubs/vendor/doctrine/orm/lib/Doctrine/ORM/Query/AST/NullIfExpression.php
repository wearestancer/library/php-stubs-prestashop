<?php

namespace Doctrine\ORM\Query\AST;

/**
 * NullIfExpression ::= "NULLIF" "(" ScalarExpression "," ScalarExpression ")"
 *
 * @link    www.doctrine-project.org
 */
class NullIfExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed */
    public $firstExpression;
    /** @var mixed */
    public $secondExpression;
    /**
     * @param mixed $firstExpression
     * @param mixed $secondExpression
     */
    public function __construct($firstExpression, $secondExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
