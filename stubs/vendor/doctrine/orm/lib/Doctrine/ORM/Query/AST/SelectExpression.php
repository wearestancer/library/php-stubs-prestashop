<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SelectExpression ::= IdentificationVariable ["." "*"] | StateFieldPathExpression |
 *                      (AggregateExpression | "(" Subselect ")") [["AS"] ["HIDDEN"] FieldAliasIdentificationVariable]
 *
 * @link    www.doctrine-project.org
 */
class SelectExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed */
    public $expression;
    /** @var string|null */
    public $fieldIdentificationVariable;
    /** @var bool */
    public $hiddenAliasResultVariable;
    /**
     * @param mixed       $expression
     * @param string|null $fieldIdentificationVariable
     * @param bool        $hiddenAliasResultVariable
     */
    public function __construct($expression, $fieldIdentificationVariable, $hiddenAliasResultVariable = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
