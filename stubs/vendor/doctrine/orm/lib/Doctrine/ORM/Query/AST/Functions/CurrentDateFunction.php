<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "CURRENT_DATE"
 *
 * @link    www.doctrine-project.org
 */
class CurrentDateFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
