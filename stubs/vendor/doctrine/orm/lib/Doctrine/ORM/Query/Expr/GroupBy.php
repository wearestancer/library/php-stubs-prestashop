<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for building DQL Group By parts.
 *
 * @link    www.doctrine-project.org
 */
class GroupBy extends \Doctrine\ORM\Query\Expr\Base
{
    /** @var string */
    protected $preSeparator = '';
    /** @var string */
    protected $postSeparator = '';
    /** @psalm-var list<string> */
    protected $parts = [];
    /**
     * @psalm-return list<string>
     */
    public function getParts()
    {
    }
}
