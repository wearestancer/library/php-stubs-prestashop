<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "DATE_ADD" "(" ArithmeticPrimary "," ArithmeticPrimary "," StringPrimary ")"
 *
 * @link    www.doctrine-project.org
 */
class DateAddFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    public $firstDateExpression = null;
    /** @var Node */
    public $intervalExpression = null;
    /** @var Node */
    public $unit = null;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
