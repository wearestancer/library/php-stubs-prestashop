<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for generating DQL functions.
 *
 * @link    www.doctrine-project.org
 */
class Func
{
    /** @var string */
    protected $name;
    /** @var mixed[] */
    protected $arguments;
    /**
     * Creates a function, with the given argument.
     *
     * @param string        $name
     * @param mixed[]|mixed $arguments
     * @psalm-param list<mixed>|mixed $arguments
     */
    public function __construct($name, $arguments)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @psalm-return list<mixed>
     */
    public function getArguments()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
