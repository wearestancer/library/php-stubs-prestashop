<?php

namespace Doctrine\ORM\Query\AST;

/**
 * OrderByClause ::= "ORDER" "BY" OrderByItem {"," OrderByItem}*
 *
 * @link    www.doctrine-project.org
 */
class OrderByClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var OrderByItem[] */
    public $orderByItems = [];
    /**
     * @param OrderByItem[] $orderByItems
     */
    public function __construct(array $orderByItems)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
