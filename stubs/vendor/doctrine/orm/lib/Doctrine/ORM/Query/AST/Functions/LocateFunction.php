<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "LOCATE" "(" StringPrimary "," StringPrimary ["," SimpleArithmeticExpression]")"
 *
 * @link    www.doctrine-project.org
 */
class LocateFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    public $firstStringPrimary;
    /** @var Node */
    public $secondStringPrimary;
    /** @var SimpleArithmeticExpression|bool */
    public $simpleArithmeticExpression = false;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
