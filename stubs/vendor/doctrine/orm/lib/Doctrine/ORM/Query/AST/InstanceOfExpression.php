<?php

namespace Doctrine\ORM\Query\AST;

/**
 * InstanceOfExpression ::= IdentificationVariable ["NOT"] "INSTANCE" ["OF"] (InstanceOfParameter | "(" InstanceOfParameter {"," InstanceOfParameter}* ")")
 * InstanceOfParameter  ::= AbstractSchemaName | InputParameter
 *
 * @link    www.doctrine-project.org
 */
class InstanceOfExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $not;
    /** @var string */
    public $identificationVariable;
    /** @var mixed[] */
    public $value;
    /**
     * @param string $identVariable
     */
    public function __construct($identVariable)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
