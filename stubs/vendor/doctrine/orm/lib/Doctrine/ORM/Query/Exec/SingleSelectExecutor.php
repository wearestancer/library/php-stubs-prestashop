<?php

namespace Doctrine\ORM\Query\Exec;

/**
 * Executor that executes the SQL statement for simple DQL SELECT statements.
 *
 * @link        www.doctrine-project.org
 */
class SingleSelectExecutor extends \Doctrine\ORM\Query\Exec\AbstractSqlExecutor
{
    public function __construct(\Doctrine\ORM\Query\AST\SelectStatement $AST, \Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Result
     */
    public function execute(\Doctrine\DBAL\Connection $conn, array $params, array $types)
    {
    }
}
