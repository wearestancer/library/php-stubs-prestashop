<?php

namespace Doctrine\ORM\Query\AST;

/**
 * FromClause ::= "FROM" IdentificationVariableDeclaration {"," IdentificationVariableDeclaration}
 *
 * @link    www.doctrine-project.org
 */
class FromClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $identificationVariableDeclarations = [];
    /**
     * @param mixed[] $identificationVariableDeclarations
     */
    public function __construct(array $identificationVariableDeclarations)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
