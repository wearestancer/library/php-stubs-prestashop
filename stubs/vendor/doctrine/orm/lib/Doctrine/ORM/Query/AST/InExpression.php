<?php

namespace Doctrine\ORM\Query\AST;

/**
 * InExpression ::= ArithmeticExpression ["NOT"] "IN" "(" (Literal {"," Literal}* | Subselect) ")"
 *
 * @link    www.doctrine-project.org
 */
class InExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $not;
    /** @var ArithmeticExpression */
    public $expression;
    /** @var mixed[] */
    public $literals = [];
    /** @var Subselect|null */
    public $subselect;
    /**
     * @param ArithmeticExpression $expression
     */
    public function __construct($expression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
