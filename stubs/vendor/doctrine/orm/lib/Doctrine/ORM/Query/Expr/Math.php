<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for DQL math statements.
 *
 * @link    www.doctrine-project.org
 */
class Math
{
    /** @var mixed */
    protected $leftExpr;
    /** @var string */
    protected $operator;
    /** @var mixed */
    protected $rightExpr;
    /**
     * Creates a mathematical expression with the given arguments.
     *
     * @param mixed  $leftExpr
     * @param string $operator
     * @param mixed  $rightExpr
     */
    public function __construct($leftExpr, $operator, $rightExpr)
    {
    }
    /**
     * @return mixed
     */
    public function getLeftExpr()
    {
    }
    /**
     * @return string
     */
    public function getOperator()
    {
    }
    /**
     * @return mixed
     */
    public function getRightExpr()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
