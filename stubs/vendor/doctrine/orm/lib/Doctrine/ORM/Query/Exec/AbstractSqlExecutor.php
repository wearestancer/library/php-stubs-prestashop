<?php

namespace Doctrine\ORM\Query\Exec;

/**
 * Base class for SQL statement executors.
 *
 * @link        http://www.doctrine-project.org
 *
 * @todo Rename: AbstractSQLExecutor
 */
abstract class AbstractSqlExecutor
{
    /** @var mixed[]|string */
    protected $_sqlStatements;
    /** @var QueryCacheProfile */
    protected $queryCacheProfile;
    /**
     * Gets the SQL statements that are executed by the executor.
     *
     * @return mixed[]|string  All the SQL update statements.
     */
    public function getSqlStatements()
    {
    }
    /**
     * @return void
     */
    public function setQueryCacheProfile(\Doctrine\DBAL\Cache\QueryCacheProfile $qcp)
    {
    }
    /**
     * Do not use query cache
     *
     * @return void
     */
    public function removeQueryCacheProfile()
    {
    }
    /**
     * Executes all sql statements.
     *
     * @param Connection $conn The database connection that is used to execute the queries.
     * @psalm-param array<int, mixed>|array<string, mixed> $params The parameters.
     * @psalm-param array<int, int|string|Type|null>|
     *              array<string, int|string|Type|null> $types The parameter types.
     *
     * @return Result|int
     */
    public abstract function execute(\Doctrine\DBAL\Connection $conn, array $params, array $types);
}
