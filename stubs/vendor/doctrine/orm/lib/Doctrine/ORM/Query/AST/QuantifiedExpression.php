<?php

namespace Doctrine\ORM\Query\AST;

/**
 * QuantifiedExpression ::= ("ALL" | "ANY" | "SOME") "(" Subselect ")"
 *
 * @link    www.doctrine-project.org
 */
class QuantifiedExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $type;
    /** @var Subselect */
    public $subselect;
    /**
     * @param Subselect $subselect
     */
    public function __construct($subselect)
    {
    }
    /**
     * @return bool
     */
    public function isAll()
    {
    }
    /**
     * @return bool
     */
    public function isAny()
    {
    }
    /**
     * @return bool
     */
    public function isSome()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
