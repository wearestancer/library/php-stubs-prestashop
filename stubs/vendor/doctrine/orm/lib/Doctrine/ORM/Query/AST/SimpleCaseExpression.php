<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SimpleCaseExpression ::= "CASE" CaseOperand SimpleWhenClause {SimpleWhenClause}* "ELSE" ScalarExpression "END"
 *
 * @link    www.doctrine-project.org
 */
class SimpleCaseExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var PathExpression */
    public $caseOperand = null;
    /** @var mixed[] */
    public $simpleWhenClauses = [];
    /** @var mixed */
    public $elseScalarExpression = null;
    /**
     * @param PathExpression $caseOperand
     * @param mixed[]        $simpleWhenClauses
     * @param mixed          $elseScalarExpression
     */
    public function __construct($caseOperand, array $simpleWhenClauses, $elseScalarExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
