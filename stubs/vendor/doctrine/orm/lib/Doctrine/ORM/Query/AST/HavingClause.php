<?php

namespace Doctrine\ORM\Query\AST;

class HavingClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var ConditionalExpression */
    public $conditionalExpression;
    /**
     * @param ConditionalExpression $conditionalExpression
     */
    public function __construct($conditionalExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
