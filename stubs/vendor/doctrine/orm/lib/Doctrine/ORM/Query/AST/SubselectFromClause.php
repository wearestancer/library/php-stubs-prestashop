<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SubselectFromClause ::= "FROM" SubselectIdentificationVariableDeclaration {"," SubselectIdentificationVariableDeclaration}*
 *
 * @link    www.doctrine-project.org
 */
class SubselectFromClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $identificationVariableDeclarations = [];
    /**
     * @param mixed[] $identificationVariableDeclarations
     */
    public function __construct(array $identificationVariableDeclarations)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
