<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ConditionalTerm ::= ConditionalFactor {"AND" ConditionalFactor}*
 *
 * @link    www.doctrine-project.org
 */
class ConditionalTerm extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $conditionalFactors = [];
    /**
     * @param mixed[] $conditionalFactors
     */
    public function __construct(array $conditionalFactors)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
