<?php

namespace Doctrine\ORM\Query\AST;

/**
 * NullComparisonExpression ::= (SingleValuedPathExpression | InputParameter) "IS" ["NOT"] "NULL"
 *
 * @link    www.doctrine-project.org
 */
class NullComparisonExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $not;
    /** @var Node */
    public $expression;
    /**
     * @param Node $expression
     */
    public function __construct($expression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
