<?php

namespace Doctrine\ORM\Query\AST;

/**
 * OrderByItem ::= (ResultVariable | StateFieldPathExpression) ["ASC" | "DESC"]
 *
 * @link    www.doctrine-project.org
 */
class OrderByItem extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed */
    public $expression;
    /** @var string */
    public $type;
    /**
     * @param mixed $expression
     */
    public function __construct($expression)
    {
    }
    /**
     * @return bool
     */
    public function isAsc()
    {
    }
    /**
     * @return bool
     */
    public function isDesc()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
