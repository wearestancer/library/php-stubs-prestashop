<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * Abstract Function Node.
 *
 * @link    www.doctrine-project.org
 */
abstract class FunctionNode extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $name;
    /**
     * @param string $name
     */
    public function __construct($name)
    {
    }
    /**
     * @return string
     */
    public abstract function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker);
    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function dispatch($sqlWalker)
    {
    }
    /**
     * @return void
     */
    public abstract function parse(\Doctrine\ORM\Query\Parser $parser);
}
