<?php

namespace Doctrine\ORM\Query;

/**
 * Defines a Query Parameter.
 *
 * @link    www.doctrine-project.org
 */
class Parameter
{
    /**
     * Returns the internal representation of a parameter name.
     *
     * @param string|int $name The parameter name or position.
     *
     * @return string The normalized parameter name.
     */
    public static function normalizeName($name)
    {
    }
    /**
     * @param string|int $name  Parameter name
     * @param mixed      $value Parameter value
     * @param mixed      $type  Parameter type
     */
    public function __construct($name, $value, $type = null)
    {
    }
    /**
     * Retrieves the Parameter name.
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Retrieves the Parameter value.
     *
     * @return mixed
     */
    public function getValue()
    {
    }
    /**
     * Retrieves the Parameter type.
     *
     * @return mixed
     */
    public function getType()
    {
    }
    /**
     * Defines the Parameter value.
     *
     * @param mixed $value Parameter value.
     * @param mixed $type  Parameter type.
     *
     * @return void
     */
    public function setValue($value, $type = null)
    {
    }
    public function typeWasSpecified() : bool
    {
    }
}
