<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for building DQL Order By parts.
 *
 * @link    www.doctrine-project.org
 */
class OrderBy
{
    /** @var string */
    protected $preSeparator = '';
    /** @var string */
    protected $separator = ', ';
    /** @var string */
    protected $postSeparator = '';
    /** @var string[] */
    protected $allowedClasses = [];
    /** @psalm-var list<string> */
    protected $parts = [];
    /**
     * @param string|null $sort
     * @param string|null $order
     */
    public function __construct($sort = null, $order = null)
    {
    }
    /**
     * @param string      $sort
     * @param string|null $order
     *
     * @return void
     */
    public function add($sort, $order = null)
    {
    }
    /**
     * @return int
     * @psalm-return 0|positive-int
     */
    public function count()
    {
    }
    /**
     * @psalm-return list<string>
     */
    public function getParts()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
