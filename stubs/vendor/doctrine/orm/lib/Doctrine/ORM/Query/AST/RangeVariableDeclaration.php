<?php

namespace Doctrine\ORM\Query\AST;

/**
 * RangeVariableDeclaration ::= AbstractSchemaName ["AS"] AliasIdentificationVariable
 *
 * @link    www.doctrine-project.org
 */
class RangeVariableDeclaration extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $abstractSchemaName;
    /** @var string */
    public $aliasIdentificationVariable;
    /** @var bool */
    public $isRoot;
    /**
     * @param string $abstractSchemaName
     * @param string $aliasIdentificationVar
     * @param bool   $isRoot
     */
    public function __construct($abstractSchemaName, $aliasIdentificationVar, $isRoot = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
