<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "SUM" "(" ["DISTINCT"] StringPrimary ")"
 */
final class SumFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker) : string
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser) : void
    {
    }
}
