<?php

namespace Doctrine\ORM\Query\AST;

/**
 * LikeExpression ::= StringExpression ["NOT"] "LIKE" string ["ESCAPE" char]
 *
 * @link    www.doctrine-project.org
 */
class LikeExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $not = false;
    /** @var Node|string */
    public $stringExpression;
    /** @var InputParameter|FunctionNode|PathExpression|Literal */
    public $stringPattern;
    /** @var Literal|null */
    public $escapeChar;
    /**
     * @param Node|string                                        $stringExpression
     * @param InputParameter|FunctionNode|PathExpression|Literal $stringPattern
     * @param Literal|null                                       $escapeChar
     */
    public function __construct($stringExpression, $stringPattern, $escapeChar = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
