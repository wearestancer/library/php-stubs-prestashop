<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Abstract base Expr class for building DQL parts.
 *
 * @link    www.doctrine-project.org
 */
abstract class Base
{
    /** @var string */
    protected $preSeparator = '(';
    /** @var string */
    protected $separator = ', ';
    /** @var string */
    protected $postSeparator = ')';
    /** @psalm-var list<class-string> */
    protected $allowedClasses = [];
    /** @psalm-var list<string|object> */
    protected $parts = [];
    /**
     * @param mixed $args
     */
    public function __construct($args = [])
    {
    }
    /**
     * @param string[]|object[]|string|object $args
     * @psalm-param list<string|object>|string|object $args
     *
     * @return $this
     */
    public function addMultiple($args = [])
    {
    }
    /**
     * @param mixed $arg
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function add($arg)
    {
    }
    /**
     * @return int
     * @psalm-return 0|positive-int
     */
    public function count()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
