<?php

namespace Doctrine\ORM\Query\AST;

/**
 * UpdateItem ::= [IdentificationVariable "."] {StateField | SingleValuedAssociationField} "=" NewValue
 * NewValue ::= SimpleArithmeticExpression | StringPrimary | DatetimePrimary | BooleanPrimary |
 *              EnumPrimary | SimpleEntityExpression | "NULL"
 *
 * @link    www.doctrine-project.org
 */
class UpdateItem extends \Doctrine\ORM\Query\AST\Node
{
    /** @var PathExpression */
    public $pathExpression;
    /** @var InputParameter|ArithmeticExpression|null */
    public $newValue;
    /**
     * @param PathExpression                           $pathExpression
     * @param InputParameter|ArithmeticExpression|null $newValue
     */
    public function __construct($pathExpression, $newValue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
