<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "SIZE" "(" CollectionValuedPathExpression ")"
 *
 * @link    www.doctrine-project.org
 */
class SizeFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var PathExpression */
    public $collectionPathExpression;
    /**
     * @override
     * @inheritdoc
     * @todo If the collection being counted is already joined, the SQL can be simpler (more efficient).
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
