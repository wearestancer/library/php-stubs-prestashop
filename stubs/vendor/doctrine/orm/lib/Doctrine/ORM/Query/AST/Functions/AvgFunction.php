<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "AVG" "(" ["DISTINCT"] StringPrimary ")"
 */
final class AvgFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker) : string
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser) : void
    {
    }
}
