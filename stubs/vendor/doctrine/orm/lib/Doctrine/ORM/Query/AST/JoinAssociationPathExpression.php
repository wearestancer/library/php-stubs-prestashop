<?php

namespace Doctrine\ORM\Query\AST;

/**
 * JoinAssociationPathExpression ::= IdentificationVariable "." (SingleValuedAssociationField | CollectionValuedAssociationField)
 *
 * @link    www.doctrine-project.org
 */
class JoinAssociationPathExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $identificationVariable;
    /** @var string */
    public $associationField;
    /**
     * @param string $identificationVariable
     * @param string $associationField
     */
    public function __construct($identificationVariable, $associationField)
    {
    }
}
