<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "ABS" "(" SimpleArithmeticExpression ")"
 *
 * @link    www.doctrine-project.org
 */
class AbsFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var SimpleArithmeticExpression */
    public $simpleArithmeticExpression;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
