<?php

namespace Doctrine\ORM\Query;

/**
 * @deprecated This class will be removed in 3.0 without replacement.
 *
 * @template-implements Iterator<TreeWalker>
 * @template-implements ArrayAccess<int, TreeWalker>
 */
class TreeWalkerChainIterator implements \Iterator, \ArrayAccess
{
    /**
     * @param AbstractQuery $query
     * @param ParserResult  $parserResult
     */
    public function __construct(\Doctrine\ORM\Query\TreeWalkerChain $treeWalkerChain, $query, $parserResult)
    {
    }
    /**
     * @return string|false
     * @psalm-return class-string<TreeWalker>|false
     */
    #[\ReturnTypeWillChange]
    public function rewind()
    {
    }
    /**
     * @return TreeWalker|null
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
    }
    /**
     * @return TreeWalker|null
     */
    #[\ReturnTypeWillChange]
    public function next()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function valid()
    {
    }
    /**
     * @param mixed $offset
     * @psalm-param array-key|null $offset
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
    }
    /**
     * @param mixed $offset
     * @psalm-param array-key|null $offset
     *
     * @return TreeWalker|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $value
     * @psalm-param array-key|null $offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
    }
    /**
     * @param mixed $offset
     * @psalm-param array-key|null $offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
    }
}
