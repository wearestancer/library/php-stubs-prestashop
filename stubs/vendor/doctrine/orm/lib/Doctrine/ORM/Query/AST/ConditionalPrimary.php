<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ConditionalPrimary ::= SimpleConditionalExpression | "(" ConditionalExpression ")"
 *
 * @link    www.doctrine-project.org
 */
class ConditionalPrimary extends \Doctrine\ORM\Query\AST\Node
{
    /** @var Node|null */
    public $simpleConditionalExpression;
    /** @var ConditionalExpression|null */
    public $conditionalExpression;
    /**
     * @return bool
     */
    public function isSimpleConditionalExpression()
    {
    }
    /**
     * @return bool
     */
    public function isConditionalExpression()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
