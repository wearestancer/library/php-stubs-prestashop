<?php

namespace Doctrine\ORM\Query\AST;

/**
 * Base exception class for AST exceptions.
 */
class ASTException extends \Doctrine\ORM\Query\QueryException
{
    /**
     * @param Node $node
     *
     * @return ASTException
     */
    public static function noDispatchForNode($node)
    {
    }
}
