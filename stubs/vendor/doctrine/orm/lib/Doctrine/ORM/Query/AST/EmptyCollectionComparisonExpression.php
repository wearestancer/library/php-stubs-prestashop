<?php

namespace Doctrine\ORM\Query\AST;

/**
 * EmptyCollectionComparisonExpression ::= CollectionValuedPathExpression "IS" ["NOT"] "EMPTY"
 *
 * @link    www.doctrine-project.org
 */
class EmptyCollectionComparisonExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var PathExpression */
    public $expression;
    /** @var bool */
    public $not;
    /**
     * @param PathExpression $expression
     */
    public function __construct($expression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
