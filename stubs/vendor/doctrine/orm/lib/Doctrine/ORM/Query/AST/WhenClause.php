<?php

namespace Doctrine\ORM\Query\AST;

/**
 * WhenClause ::= "WHEN" ConditionalExpression "THEN" ScalarExpression
 *
 * @link    www.doctrine-project.org
 */
class WhenClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var ConditionalExpression */
    public $caseConditionExpression = null;
    /** @var mixed */
    public $thenScalarExpression = null;
    /**
     * @param ConditionalExpression $caseConditionExpression
     * @param mixed                 $thenScalarExpression
     */
    public function __construct($caseConditionExpression, $thenScalarExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
