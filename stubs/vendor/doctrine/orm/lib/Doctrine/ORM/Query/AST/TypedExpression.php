<?php

namespace Doctrine\ORM\Query\AST;

/**
 * Provides an API for resolving the type of a Node
 */
interface TypedExpression
{
    public function getReturnType() : \Doctrine\DBAL\Types\Type;
}
