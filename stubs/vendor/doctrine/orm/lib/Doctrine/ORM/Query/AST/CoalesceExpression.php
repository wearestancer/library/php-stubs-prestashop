<?php

namespace Doctrine\ORM\Query\AST;

/**
 * CoalesceExpression ::= "COALESCE" "(" ScalarExpression {"," ScalarExpression}* ")"
 *
 * @link    www.doctrine-project.org
 */
class CoalesceExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $scalarExpressions = [];
    /**
     * @param mixed[] $scalarExpressions
     */
    public function __construct(array $scalarExpressions)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
