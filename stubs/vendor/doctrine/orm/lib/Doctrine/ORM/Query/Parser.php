<?php

namespace Doctrine\ORM\Query;

/**
 * An LL(*) recursive-descent parser for the context-free grammar of the Doctrine Query Language.
 * Parses a DQL query, reports any errors in it, and generates an AST.
 *
 * @psalm-import-type Token from AbstractLexer
 * @psalm-type QueryComponent = array{
 *                 metadata?: ClassMetadata<object>,
 *                 parent?: string|null,
 *                 relation?: mixed[]|null,
 *                 map?: string|null,
 *                 resultVariable?: AST\Node|string,
 *                 nestingLevel: int,
 *                 token: Token,
 *             }
 */
class Parser
{
    /**
     * Creates a new query parser object.
     *
     * @param Query $query The Query to parse.
     */
    public function __construct(\Doctrine\ORM\Query $query)
    {
    }
    /**
     * Sets a custom tree walker that produces output.
     * This tree walker will be run last over the AST, after any other walkers.
     *
     * @param string $className
     * @psalm-param class-string<SqlWalker> $className
     *
     * @return void
     */
    public function setCustomOutputTreeWalker($className)
    {
    }
    /**
     * Adds a custom tree walker for modifying the AST.
     *
     * @param string $className
     * @psalm-param class-string<TreeWalker> $className
     *
     * @return void
     */
    public function addCustomTreeWalker($className)
    {
    }
    /**
     * Gets the lexer used by the parser.
     *
     * @return Lexer
     */
    public function getLexer()
    {
    }
    /**
     * Gets the ParserResult that is being filled with information during parsing.
     *
     * @return ParserResult
     */
    public function getParserResult()
    {
    }
    /**
     * Gets the EntityManager used by the parser.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
    /**
     * Parses and builds AST for the given Query.
     *
     * @return SelectStatement|UpdateStatement|DeleteStatement
     */
    public function getAST()
    {
    }
    /**
     * Attempts to match the given token with the current lookahead token.
     *
     * If they match, updates the lookahead token; otherwise raises a syntax
     * error.
     *
     * @param int $token The token type.
     *
     * @return void
     *
     * @throws QueryException If the tokens don't match.
     */
    public function match($token)
    {
    }
    /**
     * Frees this parser, enabling it to be reused.
     *
     * @param bool $deep     Whether to clean peek and reset errors.
     * @param int  $position Position to reset.
     *
     * @return void
     */
    public function free($deep = false, $position = 0)
    {
    }
    /**
     * Parses a query string.
     *
     * @return ParserResult
     */
    public function parse()
    {
    }
    /**
     * Generates a new syntax error.
     *
     * @param string       $expected Expected string.
     * @param mixed[]|null $token    Got token.
     * @psalm-param Token|null $token
     *
     * @return void
     * @psalm-return no-return
     *
     * @throws QueryException
     */
    public function syntaxError($expected = '', $token = null)
    {
    }
    /**
     * Generates a new semantical error.
     *
     * @param string       $message Optional message.
     * @param mixed[]|null $token   Optional token.
     * @psalm-param Token|null $token
     *
     * @return void
     * @psalm-return no-return
     *
     * @throws QueryException
     */
    public function semanticalError($message = '', $token = null)
    {
    }
    /**
     * QueryLanguage ::= SelectStatement | UpdateStatement | DeleteStatement
     *
     * @return SelectStatement|UpdateStatement|DeleteStatement
     */
    public function QueryLanguage()
    {
    }
    /**
     * SelectStatement ::= SelectClause FromClause [WhereClause] [GroupByClause] [HavingClause] [OrderByClause]
     *
     * @return SelectStatement
     */
    public function SelectStatement()
    {
    }
    /**
     * UpdateStatement ::= UpdateClause [WhereClause]
     *
     * @return UpdateStatement
     */
    public function UpdateStatement()
    {
    }
    /**
     * DeleteStatement ::= DeleteClause [WhereClause]
     *
     * @return DeleteStatement
     */
    public function DeleteStatement()
    {
    }
    /**
     * IdentificationVariable ::= identifier
     *
     * @return string
     */
    public function IdentificationVariable()
    {
    }
    /**
     * AliasIdentificationVariable = identifier
     *
     * @return string
     */
    public function AliasIdentificationVariable()
    {
    }
    /**
     * AbstractSchemaName ::= fully_qualified_name | aliased_name | identifier
     *
     * @return string
     */
    public function AbstractSchemaName()
    {
    }
    /**
     * AliasResultVariable ::= identifier
     *
     * @return string
     */
    public function AliasResultVariable()
    {
    }
    /**
     * ResultVariable ::= identifier
     *
     * @return string
     */
    public function ResultVariable()
    {
    }
    /**
     * JoinAssociationPathExpression ::= IdentificationVariable "." (CollectionValuedAssociationField | SingleValuedAssociationField)
     *
     * @return JoinAssociationPathExpression
     */
    public function JoinAssociationPathExpression()
    {
    }
    /**
     * Parses an arbitrary path expression and defers semantical validation
     * based on expected types.
     *
     * PathExpression ::= IdentificationVariable {"." identifier}*
     *
     * @param int $expectedTypes
     * @psalm-param int-mask-of<PathExpression::TYPE_*> $expectedTypes
     *
     * @return PathExpression
     */
    public function PathExpression($expectedTypes)
    {
    }
    /**
     * AssociationPathExpression ::= CollectionValuedPathExpression | SingleValuedAssociationPathExpression
     *
     * @return PathExpression
     */
    public function AssociationPathExpression()
    {
    }
    /**
     * SingleValuedPathExpression ::= StateFieldPathExpression | SingleValuedAssociationPathExpression
     *
     * @return PathExpression
     */
    public function SingleValuedPathExpression()
    {
    }
    /**
     * StateFieldPathExpression ::= IdentificationVariable "." StateField
     *
     * @return PathExpression
     */
    public function StateFieldPathExpression()
    {
    }
    /**
     * SingleValuedAssociationPathExpression ::= IdentificationVariable "." SingleValuedAssociationField
     *
     * @return PathExpression
     */
    public function SingleValuedAssociationPathExpression()
    {
    }
    /**
     * CollectionValuedPathExpression ::= IdentificationVariable "." CollectionValuedAssociationField
     *
     * @return PathExpression
     */
    public function CollectionValuedPathExpression()
    {
    }
    /**
     * SelectClause ::= "SELECT" ["DISTINCT"] SelectExpression {"," SelectExpression}
     *
     * @return SelectClause
     */
    public function SelectClause()
    {
    }
    /**
     * SimpleSelectClause ::= "SELECT" ["DISTINCT"] SimpleSelectExpression
     *
     * @return SimpleSelectClause
     */
    public function SimpleSelectClause()
    {
    }
    /**
     * UpdateClause ::= "UPDATE" AbstractSchemaName ["AS"] AliasIdentificationVariable "SET" UpdateItem {"," UpdateItem}*
     *
     * @return UpdateClause
     */
    public function UpdateClause()
    {
    }
    /**
     * DeleteClause ::= "DELETE" ["FROM"] AbstractSchemaName ["AS"] AliasIdentificationVariable
     *
     * @return DeleteClause
     */
    public function DeleteClause()
    {
    }
    /**
     * FromClause ::= "FROM" IdentificationVariableDeclaration {"," IdentificationVariableDeclaration}*
     *
     * @return FromClause
     */
    public function FromClause()
    {
    }
    /**
     * SubselectFromClause ::= "FROM" SubselectIdentificationVariableDeclaration {"," SubselectIdentificationVariableDeclaration}*
     *
     * @return SubselectFromClause
     */
    public function SubselectFromClause()
    {
    }
    /**
     * WhereClause ::= "WHERE" ConditionalExpression
     *
     * @return WhereClause
     */
    public function WhereClause()
    {
    }
    /**
     * HavingClause ::= "HAVING" ConditionalExpression
     *
     * @return HavingClause
     */
    public function HavingClause()
    {
    }
    /**
     * GroupByClause ::= "GROUP" "BY" GroupByItem {"," GroupByItem}*
     *
     * @return GroupByClause
     */
    public function GroupByClause()
    {
    }
    /**
     * OrderByClause ::= "ORDER" "BY" OrderByItem {"," OrderByItem}*
     *
     * @return OrderByClause
     */
    public function OrderByClause()
    {
    }
    /**
     * Subselect ::= SimpleSelectClause SubselectFromClause [WhereClause] [GroupByClause] [HavingClause] [OrderByClause]
     *
     * @return Subselect
     */
    public function Subselect()
    {
    }
    /**
     * UpdateItem ::= SingleValuedPathExpression "=" NewValue
     *
     * @return UpdateItem
     */
    public function UpdateItem()
    {
    }
    /**
     * GroupByItem ::= IdentificationVariable | ResultVariable | SingleValuedPathExpression
     *
     * @return string|PathExpression
     */
    public function GroupByItem()
    {
    }
    /**
     * OrderByItem ::= (
     *      SimpleArithmeticExpression | SingleValuedPathExpression | CaseExpression |
     *      ScalarExpression | ResultVariable | FunctionDeclaration
     * ) ["ASC" | "DESC"]
     *
     * @return OrderByItem
     */
    public function OrderByItem()
    {
    }
    /**
     * NewValue ::= SimpleArithmeticExpression | StringPrimary | DatetimePrimary | BooleanPrimary |
     *      EnumPrimary | SimpleEntityExpression | "NULL"
     *
     * NOTE: Since it is not possible to correctly recognize individual types, here is the full
     * grammar that needs to be supported:
     *
     * NewValue ::= SimpleArithmeticExpression | "NULL"
     *
     * SimpleArithmeticExpression covers all *Primary grammar rules and also SimpleEntityExpression
     *
     * @return AST\ArithmeticExpression|AST\InputParameter|null
     */
    public function NewValue()
    {
    }
    /**
     * IdentificationVariableDeclaration ::= RangeVariableDeclaration [IndexBy] {Join}*
     *
     * @return IdentificationVariableDeclaration
     */
    public function IdentificationVariableDeclaration()
    {
    }
    /**
     * SubselectIdentificationVariableDeclaration ::= IdentificationVariableDeclaration
     *
     * {Internal note: WARNING: Solution is harder than a bare implementation.
     * Desired EBNF support:
     *
     * SubselectIdentificationVariableDeclaration ::= IdentificationVariableDeclaration | (AssociationPathExpression ["AS"] AliasIdentificationVariable)
     *
     * It demands that entire SQL generation to become programmatical. This is
     * needed because association based subselect requires "WHERE" conditional
     * expressions to be injected, but there is no scope to do that. Only scope
     * accessible is "FROM", prohibiting an easy implementation without larger
     * changes.}
     *
     * @return IdentificationVariableDeclaration
     */
    public function SubselectIdentificationVariableDeclaration()
    {
    }
    /**
     * Join ::= ["LEFT" ["OUTER"] | "INNER"] "JOIN"
     *          (JoinAssociationDeclaration | RangeVariableDeclaration)
     *          ["WITH" ConditionalExpression]
     *
     * @return Join
     */
    public function Join()
    {
    }
    /**
     * RangeVariableDeclaration ::= AbstractSchemaName ["AS"] AliasIdentificationVariable
     *
     * @return RangeVariableDeclaration
     *
     * @throws QueryException
     */
    public function RangeVariableDeclaration()
    {
    }
    /**
     * JoinAssociationDeclaration ::= JoinAssociationPathExpression ["AS"] AliasIdentificationVariable [IndexBy]
     *
     * @return AST\JoinAssociationDeclaration
     */
    public function JoinAssociationDeclaration()
    {
    }
    /**
     * PartialObjectExpression ::= "PARTIAL" IdentificationVariable "." PartialFieldSet
     * PartialFieldSet ::= "{" SimpleStateField {"," SimpleStateField}* "}"
     *
     * @return PartialObjectExpression
     */
    public function PartialObjectExpression()
    {
    }
    /**
     * NewObjectExpression ::= "NEW" AbstractSchemaName "(" NewObjectArg {"," NewObjectArg}* ")"
     *
     * @return NewObjectExpression
     */
    public function NewObjectExpression()
    {
    }
    /**
     * NewObjectArg ::= ScalarExpression | "(" Subselect ")"
     *
     * @return mixed
     */
    public function NewObjectArg()
    {
    }
    /**
     * IndexBy ::= "INDEX" "BY" SingleValuedPathExpression
     *
     * @return IndexBy
     */
    public function IndexBy()
    {
    }
    /**
     * ScalarExpression ::= SimpleArithmeticExpression | StringPrimary | DateTimePrimary |
     *                      StateFieldPathExpression | BooleanPrimary | CaseExpression |
     *                      InstanceOfExpression
     *
     * @return mixed One of the possible expressions or subexpressions.
     */
    public function ScalarExpression()
    {
    }
    /**
     * CaseExpression ::= GeneralCaseExpression | SimpleCaseExpression | CoalesceExpression | NullifExpression
     * GeneralCaseExpression ::= "CASE" WhenClause {WhenClause}* "ELSE" ScalarExpression "END"
     * WhenClause ::= "WHEN" ConditionalExpression "THEN" ScalarExpression
     * SimpleCaseExpression ::= "CASE" CaseOperand SimpleWhenClause {SimpleWhenClause}* "ELSE" ScalarExpression "END"
     * CaseOperand ::= StateFieldPathExpression | TypeDiscriminator
     * SimpleWhenClause ::= "WHEN" ScalarExpression "THEN" ScalarExpression
     * CoalesceExpression ::= "COALESCE" "(" ScalarExpression {"," ScalarExpression}* ")"
     * NullifExpression ::= "NULLIF" "(" ScalarExpression "," ScalarExpression ")"
     *
     * @return mixed One of the possible expressions or subexpressions.
     */
    public function CaseExpression()
    {
    }
    /**
     * CoalesceExpression ::= "COALESCE" "(" ScalarExpression {"," ScalarExpression}* ")"
     *
     * @return CoalesceExpression
     */
    public function CoalesceExpression()
    {
    }
    /**
     * NullIfExpression ::= "NULLIF" "(" ScalarExpression "," ScalarExpression ")"
     *
     * @return NullIfExpression
     */
    public function NullIfExpression()
    {
    }
    /**
     * GeneralCaseExpression ::= "CASE" WhenClause {WhenClause}* "ELSE" ScalarExpression "END"
     *
     * @return GeneralCaseExpression
     */
    public function GeneralCaseExpression()
    {
    }
    /**
     * SimpleCaseExpression ::= "CASE" CaseOperand SimpleWhenClause {SimpleWhenClause}* "ELSE" ScalarExpression "END"
     * CaseOperand ::= StateFieldPathExpression | TypeDiscriminator
     *
     * @return AST\SimpleCaseExpression
     */
    public function SimpleCaseExpression()
    {
    }
    /**
     * WhenClause ::= "WHEN" ConditionalExpression "THEN" ScalarExpression
     *
     * @return WhenClause
     */
    public function WhenClause()
    {
    }
    /**
     * SimpleWhenClause ::= "WHEN" ScalarExpression "THEN" ScalarExpression
     *
     * @return SimpleWhenClause
     */
    public function SimpleWhenClause()
    {
    }
    /**
     * SelectExpression ::= (
     *     IdentificationVariable | ScalarExpression | AggregateExpression | FunctionDeclaration |
     *     PartialObjectExpression | "(" Subselect ")" | CaseExpression | NewObjectExpression
     * ) [["AS"] ["HIDDEN"] AliasResultVariable]
     *
     * @return SelectExpression
     */
    public function SelectExpression()
    {
    }
    /**
     * SimpleSelectExpression ::= (
     *      StateFieldPathExpression | IdentificationVariable | FunctionDeclaration |
     *      AggregateExpression | "(" Subselect ")" | ScalarExpression
     * ) [["AS"] AliasResultVariable]
     *
     * @return SimpleSelectExpression
     */
    public function SimpleSelectExpression()
    {
    }
    /**
     * ConditionalExpression ::= ConditionalTerm {"OR" ConditionalTerm}*
     *
     * @return AST\ConditionalExpression|AST\ConditionalFactor|AST\ConditionalPrimary|AST\ConditionalTerm
     */
    public function ConditionalExpression()
    {
    }
    /**
     * ConditionalTerm ::= ConditionalFactor {"AND" ConditionalFactor}*
     *
     * @return AST\ConditionalFactor|AST\ConditionalPrimary|AST\ConditionalTerm
     */
    public function ConditionalTerm()
    {
    }
    /**
     * ConditionalFactor ::= ["NOT"] ConditionalPrimary
     *
     * @return AST\ConditionalFactor|AST\ConditionalPrimary
     */
    public function ConditionalFactor()
    {
    }
    /**
     * ConditionalPrimary ::= SimpleConditionalExpression | "(" ConditionalExpression ")"
     *
     * @return ConditionalPrimary
     */
    public function ConditionalPrimary()
    {
    }
    /**
     * SimpleConditionalExpression ::=
     *      ComparisonExpression | BetweenExpression | LikeExpression |
     *      InExpression | NullComparisonExpression | ExistsExpression |
     *      EmptyCollectionComparisonExpression | CollectionMemberExpression |
     *      InstanceOfExpression
     *
     * @return AST\BetweenExpression|
     *         AST\CollectionMemberExpression|
     *         AST\ComparisonExpression|
     *         AST\EmptyCollectionComparisonExpression|
     *         AST\ExistsExpression|
     *         AST\InExpression|
     *         AST\InstanceOfExpression|
     *         AST\LikeExpression|
     *         AST\NullComparisonExpression
     */
    public function SimpleConditionalExpression()
    {
    }
    /**
     * EmptyCollectionComparisonExpression ::= CollectionValuedPathExpression "IS" ["NOT"] "EMPTY"
     *
     * @return EmptyCollectionComparisonExpression
     */
    public function EmptyCollectionComparisonExpression()
    {
    }
    /**
     * CollectionMemberExpression ::= EntityExpression ["NOT"] "MEMBER" ["OF"] CollectionValuedPathExpression
     *
     * EntityExpression ::= SingleValuedAssociationPathExpression | SimpleEntityExpression
     * SimpleEntityExpression ::= IdentificationVariable | InputParameter
     *
     * @return CollectionMemberExpression
     */
    public function CollectionMemberExpression()
    {
    }
    /**
     * Literal ::= string | char | integer | float | boolean
     *
     * @return Literal
     */
    public function Literal()
    {
    }
    /**
     * InParameter ::= ArithmeticExpression | InputParameter
     *
     * @return AST\InputParameter|AST\ArithmeticExpression
     */
    public function InParameter()
    {
    }
    /**
     * InputParameter ::= PositionalParameter | NamedParameter
     *
     * @return InputParameter
     */
    public function InputParameter()
    {
    }
    /**
     * ArithmeticExpression ::= SimpleArithmeticExpression | "(" Subselect ")"
     *
     * @return ArithmeticExpression
     */
    public function ArithmeticExpression()
    {
    }
    /**
     * SimpleArithmeticExpression ::= ArithmeticTerm {("+" | "-") ArithmeticTerm}*
     *
     * @return SimpleArithmeticExpression|ArithmeticTerm
     */
    public function SimpleArithmeticExpression()
    {
    }
    /**
     * ArithmeticTerm ::= ArithmeticFactor {("*" | "/") ArithmeticFactor}*
     *
     * @return ArithmeticTerm
     */
    public function ArithmeticTerm()
    {
    }
    /**
     * ArithmeticFactor ::= [("+" | "-")] ArithmeticPrimary
     *
     * @return ArithmeticFactor
     */
    public function ArithmeticFactor()
    {
    }
    /**
     * ArithmeticPrimary ::= SingleValuedPathExpression | Literal | ParenthesisExpression
     *          | FunctionsReturningNumerics | AggregateExpression | FunctionsReturningStrings
     *          | FunctionsReturningDatetime | IdentificationVariable | ResultVariable
     *          | InputParameter | CaseExpression
     *
     * @return Node|string
     */
    public function ArithmeticPrimary()
    {
    }
    /**
     * StringExpression ::= StringPrimary | ResultVariable | "(" Subselect ")"
     *
     * @return Subselect|Node|string
     */
    public function StringExpression()
    {
    }
    /**
     * StringPrimary ::= StateFieldPathExpression | string | InputParameter | FunctionsReturningStrings | AggregateExpression | CaseExpression
     *
     * @return Node
     */
    public function StringPrimary()
    {
    }
    /**
     * EntityExpression ::= SingleValuedAssociationPathExpression | SimpleEntityExpression
     *
     * @return AST\InputParameter|PathExpression
     */
    public function EntityExpression()
    {
    }
    /**
     * SimpleEntityExpression ::= IdentificationVariable | InputParameter
     *
     * @return AST\InputParameter|AST\PathExpression
     */
    public function SimpleEntityExpression()
    {
    }
    /**
     * AggregateExpression ::=
     *  ("AVG" | "MAX" | "MIN" | "SUM" | "COUNT") "(" ["DISTINCT"] SimpleArithmeticExpression ")"
     *
     * @return AggregateExpression
     */
    public function AggregateExpression()
    {
    }
    /**
     * QuantifiedExpression ::= ("ALL" | "ANY" | "SOME") "(" Subselect ")"
     *
     * @return QuantifiedExpression
     */
    public function QuantifiedExpression()
    {
    }
    /**
     * BetweenExpression ::= ArithmeticExpression ["NOT"] "BETWEEN" ArithmeticExpression "AND" ArithmeticExpression
     *
     * @return BetweenExpression
     */
    public function BetweenExpression()
    {
    }
    /**
     * ComparisonExpression ::= ArithmeticExpression ComparisonOperator ( QuantifiedExpression | ArithmeticExpression )
     *
     * @return ComparisonExpression
     */
    public function ComparisonExpression()
    {
    }
    /**
     * InExpression ::= SingleValuedPathExpression ["NOT"] "IN" "(" (InParameter {"," InParameter}* | Subselect) ")"
     *
     * @return InExpression
     */
    public function InExpression()
    {
    }
    /**
     * InstanceOfExpression ::= IdentificationVariable ["NOT"] "INSTANCE" ["OF"] (InstanceOfParameter | "(" InstanceOfParameter {"," InstanceOfParameter}* ")")
     *
     * @return InstanceOfExpression
     */
    public function InstanceOfExpression()
    {
    }
    /**
     * InstanceOfParameter ::= AbstractSchemaName | InputParameter
     *
     * @return mixed
     */
    public function InstanceOfParameter()
    {
    }
    /**
     * LikeExpression ::= StringExpression ["NOT"] "LIKE" StringPrimary ["ESCAPE" char]
     *
     * @return LikeExpression
     */
    public function LikeExpression()
    {
    }
    /**
     * NullComparisonExpression ::= (InputParameter | NullIfExpression | CoalesceExpression | AggregateExpression | FunctionDeclaration | IdentificationVariable | SingleValuedPathExpression | ResultVariable) "IS" ["NOT"] "NULL"
     *
     * @return NullComparisonExpression
     */
    public function NullComparisonExpression()
    {
    }
    /**
     * ExistsExpression ::= ["NOT"] "EXISTS" "(" Subselect ")"
     *
     * @return ExistsExpression
     */
    public function ExistsExpression()
    {
    }
    /**
     * ComparisonOperator ::= "=" | "<" | "<=" | "<>" | ">" | ">=" | "!="
     *
     * @return string
     */
    public function ComparisonOperator()
    {
    }
    /**
     * FunctionDeclaration ::= FunctionsReturningStrings | FunctionsReturningNumerics | FunctionsReturningDatetime
     *
     * @return FunctionNode
     */
    public function FunctionDeclaration()
    {
    }
    /**
     * FunctionsReturningNumerics ::=
     *      "LENGTH" "(" StringPrimary ")" |
     *      "LOCATE" "(" StringPrimary "," StringPrimary ["," SimpleArithmeticExpression]")" |
     *      "ABS" "(" SimpleArithmeticExpression ")" |
     *      "SQRT" "(" SimpleArithmeticExpression ")" |
     *      "MOD" "(" SimpleArithmeticExpression "," SimpleArithmeticExpression ")" |
     *      "SIZE" "(" CollectionValuedPathExpression ")" |
     *      "DATE_DIFF" "(" ArithmeticPrimary "," ArithmeticPrimary ")" |
     *      "BIT_AND" "(" ArithmeticPrimary "," ArithmeticPrimary ")" |
     *      "BIT_OR" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
     *
     * @return FunctionNode
     */
    public function FunctionsReturningNumerics()
    {
    }
    /**
     * @return FunctionNode
     */
    public function CustomFunctionsReturningNumerics()
    {
    }
    /**
     * FunctionsReturningDateTime ::=
     *     "CURRENT_DATE" |
     *     "CURRENT_TIME" |
     *     "CURRENT_TIMESTAMP" |
     *     "DATE_ADD" "(" ArithmeticPrimary "," ArithmeticPrimary "," StringPrimary ")" |
     *     "DATE_SUB" "(" ArithmeticPrimary "," ArithmeticPrimary "," StringPrimary ")"
     *
     * @return FunctionNode
     */
    public function FunctionsReturningDatetime()
    {
    }
    /**
     * @return FunctionNode
     */
    public function CustomFunctionsReturningDatetime()
    {
    }
    /**
     * FunctionsReturningStrings ::=
     *   "CONCAT" "(" StringPrimary "," StringPrimary {"," StringPrimary}* ")" |
     *   "SUBSTRING" "(" StringPrimary "," SimpleArithmeticExpression "," SimpleArithmeticExpression ")" |
     *   "TRIM" "(" [["LEADING" | "TRAILING" | "BOTH"] [char] "FROM"] StringPrimary ")" |
     *   "LOWER" "(" StringPrimary ")" |
     *   "UPPER" "(" StringPrimary ")" |
     *   "IDENTITY" "(" SingleValuedAssociationPathExpression {"," string} ")"
     *
     * @return FunctionNode
     */
    public function FunctionsReturningStrings()
    {
    }
    /**
     * @return FunctionNode
     */
    public function CustomFunctionsReturningStrings()
    {
    }
}
