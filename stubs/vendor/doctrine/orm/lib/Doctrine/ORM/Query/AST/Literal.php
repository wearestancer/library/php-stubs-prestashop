<?php

namespace Doctrine\ORM\Query\AST;

class Literal extends \Doctrine\ORM\Query\AST\Node
{
    public const STRING = 1;
    public const BOOLEAN = 2;
    public const NUMERIC = 3;
    /**
     * @var int
     * @psalm-var self::*
     */
    public $type;
    /** @var mixed */
    public $value;
    /**
     * @param int   $type
     * @param mixed $value
     * @psalm-param self::* $type
     */
    public function __construct($type, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
