<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "BIT_AND" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
 *
 * @link    www.doctrine-project.org
 */
class BitAndFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    public $firstArithmetic;
    /** @var Node */
    public $secondArithmetic;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
