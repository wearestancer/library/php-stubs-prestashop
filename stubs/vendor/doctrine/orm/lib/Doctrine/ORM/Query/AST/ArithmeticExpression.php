<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ArithmeticExpression ::= SimpleArithmeticExpression | "(" Subselect ")"
 *
 * @link    www.doctrine-project.org
 */
class ArithmeticExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var SimpleArithmeticExpression|null */
    public $simpleArithmeticExpression;
    /** @var Subselect|null */
    public $subselect;
    /**
     * @return bool
     */
    public function isSimpleArithmeticExpression()
    {
    }
    /**
     * @return bool
     */
    public function isSubselect()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
