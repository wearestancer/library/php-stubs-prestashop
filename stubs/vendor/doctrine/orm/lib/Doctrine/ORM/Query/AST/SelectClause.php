<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SelectClause = "SELECT" ["DISTINCT"] SelectExpression {"," SelectExpression}
 *
 * @link    www.doctrine-project.org
 */
class SelectClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $isDistinct;
    /** @var mixed[] */
    public $selectExpressions = [];
    /**
     * @param mixed[] $selectExpressions
     * @param bool    $isDistinct
     */
    public function __construct(array $selectExpressions, $isDistinct)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
