<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ConditionalExpression ::= ConditionalTerm {"OR" ConditionalTerm}*
 *
 * @link    www.doctrine-project.org
 */
class ConditionalExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $conditionalTerms = [];
    /**
     * @param mixed[] $conditionalTerms
     */
    public function __construct(array $conditionalTerms)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
