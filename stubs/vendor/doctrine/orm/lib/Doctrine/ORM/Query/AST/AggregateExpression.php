<?php

namespace Doctrine\ORM\Query\AST;

class AggregateExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $functionName;
    /** @var PathExpression|SimpleArithmeticExpression */
    public $pathExpression;
    /**
     * Some aggregate expressions support distinct, eg COUNT.
     *
     * @var bool
     */
    public $isDistinct = false;
    /**
     * @param string                                    $functionName
     * @param PathExpression|SimpleArithmeticExpression $pathExpression
     * @param bool                                      $isDistinct
     */
    public function __construct($functionName, $pathExpression, $isDistinct)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
