<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "DATE_SUB(date1, interval, unit)"
 *
 * @link    www.doctrine-project.org
 */
class DateSubFunction extends \Doctrine\ORM\Query\AST\Functions\DateAddFunction
{
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
