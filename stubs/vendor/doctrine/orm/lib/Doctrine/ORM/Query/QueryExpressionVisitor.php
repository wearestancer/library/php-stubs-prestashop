<?php

namespace Doctrine\ORM\Query;

/**
 * Converts Collection expressions to Query expressions.
 */
class QueryExpressionVisitor extends \Doctrine\Common\Collections\Expr\ExpressionVisitor
{
    /**
     * @param mixed[] $queryAliases
     */
    public function __construct($queryAliases)
    {
    }
    /**
     * Gets bound parameters.
     * Filled after {@link dispach()}.
     *
     * @return ArrayCollection<int, mixed>
     */
    public function getParameters()
    {
    }
    /**
     * Clears parameters.
     *
     * @return void
     */
    public function clearParameters()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function walkCompositeExpression(\Doctrine\Common\Collections\Expr\CompositeExpression $expr)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function walkComparison(\Doctrine\Common\Collections\Expr\Comparison $comparison)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function walkValue(\Doctrine\Common\Collections\Expr\Value $value)
    {
    }
}
