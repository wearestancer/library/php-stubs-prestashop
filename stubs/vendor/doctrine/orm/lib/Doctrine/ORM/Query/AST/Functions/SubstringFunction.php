<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "SUBSTRING" "(" StringPrimary "," SimpleArithmeticExpression "," SimpleArithmeticExpression ")"
 *
 * @link    www.doctrine-project.org
 */
class SubstringFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    public $stringPrimary;
    /** @var SimpleArithmeticExpression */
    public $firstSimpleArithmeticExpression;
    /** @var SimpleArithmeticExpression|null */
    public $secondSimpleArithmeticExpression = null;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
