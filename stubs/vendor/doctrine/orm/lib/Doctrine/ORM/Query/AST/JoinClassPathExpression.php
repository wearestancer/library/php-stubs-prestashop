<?php

namespace Doctrine\ORM\Query\AST;

/**
 * JoinClassPathExpression ::= AbstractSchemaName ["AS"] AliasIdentificationVariable
 *
 * @link    www.doctrine-project.org
 */
class JoinClassPathExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed */
    public $abstractSchemaName;
    /** @var mixed */
    public $aliasIdentificationVariable;
    /**
     * @param mixed $abstractSchemaName
     * @param mixed $aliasIdentificationVar
     */
    public function __construct($abstractSchemaName, $aliasIdentificationVar)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
