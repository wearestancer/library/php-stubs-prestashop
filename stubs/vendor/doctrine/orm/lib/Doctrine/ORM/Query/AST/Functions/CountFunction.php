<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "COUNT" "(" ["DISTINCT"] StringPrimary ")"
 */
final class CountFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode implements \Doctrine\ORM\Query\AST\TypedExpression
{
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker) : string
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser) : void
    {
    }
    public function getReturnType() : \Doctrine\DBAL\Types\Type
    {
    }
}
