<?php

namespace Doctrine\ORM\Query\AST;

/**
 * GeneralCaseExpression ::= "CASE" WhenClause {WhenClause}* "ELSE" ScalarExpression "END"
 *
 * @link    www.doctrine-project.org
 */
class GeneralCaseExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $whenClauses = [];
    /** @var mixed */
    public $elseScalarExpression = null;
    /**
     * @param mixed[] $whenClauses
     * @param mixed   $elseScalarExpression
     */
    public function __construct(array $whenClauses, $elseScalarExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
