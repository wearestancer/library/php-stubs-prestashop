<?php

namespace Doctrine\ORM\Query\AST;

/**
 * Subselect ::= SimpleSelectClause SubselectFromClause [WhereClause] [GroupByClause] [HavingClause] [OrderByClause]
 *
 * @link    www.doctrine-project.org
 */
class Subselect extends \Doctrine\ORM\Query\AST\Node
{
    /** @var SimpleSelectClause */
    public $simpleSelectClause;
    /** @var SubselectFromClause */
    public $subselectFromClause;
    /** @var WhereClause|null */
    public $whereClause;
    /** @var GroupByClause|null */
    public $groupByClause;
    /** @var HavingClause|null */
    public $havingClause;
    /** @var OrderByClause|null */
    public $orderByClause;
    /**
     * @param SimpleSelectClause  $simpleSelectClause
     * @param SubselectFromClause $subselectFromClause
     */
    public function __construct($simpleSelectClause, $subselectFromClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
