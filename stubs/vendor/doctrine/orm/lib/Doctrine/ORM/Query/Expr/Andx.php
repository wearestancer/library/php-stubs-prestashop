<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for building DQL and parts.
 *
 * @link    www.doctrine-project.org
 */
class Andx extends \Doctrine\ORM\Query\Expr\Composite
{
    /** @var string */
    protected $separator = ' AND ';
    /** @var string[] */
    protected $allowedClasses = [\Doctrine\ORM\Query\Expr\Comparison::class, \Doctrine\ORM\Query\Expr\Func::class, \Doctrine\ORM\Query\Expr\Orx::class, self::class];
    /** @psalm-var list<string|Comparison|Func|Orx|self> */
    protected $parts = [];
    /**
     * @psalm-return list<string|Comparison|Func|Orx|self>
     */
    public function getParts()
    {
    }
}
