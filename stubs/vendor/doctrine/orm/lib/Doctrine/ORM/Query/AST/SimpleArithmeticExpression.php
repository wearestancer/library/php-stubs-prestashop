<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SimpleArithmeticExpression ::= ArithmeticTerm {("+" | "-") ArithmeticTerm}*
 *
 * @link    www.doctrine-project.org
 */
class SimpleArithmeticExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $arithmeticTerms = [];
    /**
     * @param mixed[] $arithmeticTerms
     */
    public function __construct(array $arithmeticTerms)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
