<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ArithmeticTerm ::= ArithmeticFactor {("*" | "/") ArithmeticFactor}*
 *
 * @link    www.doctrine-project.org
 */
class ArithmeticTerm extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $arithmeticFactors;
    /**
     * @param mixed[] $arithmeticFactors
     */
    public function __construct(array $arithmeticFactors)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
