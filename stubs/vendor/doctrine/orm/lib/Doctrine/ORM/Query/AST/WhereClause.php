<?php

namespace Doctrine\ORM\Query\AST;

/**
 * WhereClause ::= "WHERE" ConditionalExpression
 *
 * @link    www.doctrine-project.org
 */
class WhereClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var ConditionalExpression|ConditionalTerm */
    public $conditionalExpression;
    /**
     * @param ConditionalExpression $conditionalExpression
     */
    public function __construct($conditionalExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
