<?php

namespace Doctrine\ORM\Query;

/**
 * A ResultSetMapping describes how a result set of an SQL query maps to a Doctrine result.
 *
 * IMPORTANT NOTE:
 * The properties of this class are only public for fast internal READ access and to (drastically)
 * reduce the size of serialized instances for more effective caching due to better (un-)serialization
 * performance.
 *
 * <b>Users should use the public methods.</b>
 *
 * @todo Think about whether the number of lookup maps can be reduced.
 */
class ResultSetMapping
{
    /**
     * Whether the result is mixed (contains scalar values together with field values).
     *
     * @ignore
     * @var bool
     */
    public $isMixed = false;
    /**
     * Whether the result is a select statement.
     *
     * @ignore
     * @var bool
     */
    public $isSelect = true;
    /**
     * Maps alias names to class names.
     *
     * @ignore
     * @psalm-var array<string, class-string>
     */
    public $aliasMap = [];
    /**
     * Maps alias names to related association field names.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $relationMap = [];
    /**
     * Maps alias names to parent alias names.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $parentAliasMap = [];
    /**
     * Maps column names in the result set to field names for each class.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $fieldMappings = [];
    /**
     * Maps column names in the result set to the alias/field name to use in the mapped result.
     *
     * @ignore
     * @psalm-var array<string, string|int>
     */
    public $scalarMappings = [];
    /**
     * Maps scalar columns to enums
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $enumMappings = [];
    /**
     * Maps column names in the result set to the alias/field type to use in the mapped result.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $typeMappings = [];
    /**
     * Maps entities in the result set to the alias name to use in the mapped result.
     *
     * @ignore
     * @psalm-var array<string, string|null>
     */
    public $entityMappings = [];
    /**
     * Maps column names of meta columns (foreign keys, discriminator columns, ...) to field names.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $metaMappings = [];
    /**
     * Maps column names in the result set to the alias they belong to.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $columnOwnerMap = [];
    /**
     * List of columns in the result set that are used as discriminator columns.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $discriminatorColumns = [];
    /**
     * Maps alias names to field names that should be used for indexing.
     *
     * @ignore
     * @psalm-var array<string, string>
     */
    public $indexByMap = [];
    /**
     * Map from column names to class names that declare the field the column is mapped to.
     *
     * @ignore
     * @psalm-var array<string, class-string>
     */
    public $declaringClasses = [];
    /**
     * This is necessary to hydrate derivate foreign keys correctly.
     *
     * @psalm-var array<string, array<string, bool>>
     */
    public $isIdentifierColumn = [];
    /**
     * Maps column names in the result set to field names for each new object expression.
     *
     * @psalm-var array<string, array<string, mixed>>
     */
    public $newObjectMappings = [];
    /**
     * Maps metadata parameter names to the metadata attribute.
     *
     * @psalm-var array<int|string, string>
     */
    public $metadataParameterMapping = [];
    /**
     * Contains query parameter names to be resolved as discriminator values
     *
     * @psalm-var array<string, string>
     */
    public $discriminatorParameters = [];
    /**
     * Adds an entity result to this ResultSetMapping.
     *
     * @param string      $class       The class name of the entity.
     * @param string      $alias       The alias for the class. The alias must be unique among all entity
     *                                 results or joined entity results within this ResultSetMapping.
     * @param string|null $resultAlias The result alias with which the entity result should be
     *                                 placed in the result structure.
     * @psalm-param class-string $class
     *
     * @return $this
     *
     * @todo Rename: addRootEntity
     */
    public function addEntityResult($class, $alias, $resultAlias = null)
    {
    }
    /**
     * Sets a discriminator column for an entity result or joined entity result.
     * The discriminator column will be used to determine the concrete class name to
     * instantiate.
     *
     * @param string $alias       The alias of the entity result or joined entity result the discriminator
     *                            column should be used for.
     * @param string $discrColumn The name of the discriminator column in the SQL result set.
     *
     * @return $this
     *
     * @todo Rename: addDiscriminatorColumn
     */
    public function setDiscriminatorColumn($alias, $discrColumn)
    {
    }
    /**
     * Sets a field to use for indexing an entity result or joined entity result.
     *
     * @param string $alias     The alias of an entity result or joined entity result.
     * @param string $fieldName The name of the field to use for indexing.
     *
     * @return $this
     */
    public function addIndexBy($alias, $fieldName)
    {
    }
    /**
     * Sets to index by a scalar result column name.
     *
     * @param string $resultColumnName
     *
     * @return $this
     */
    public function addIndexByScalar($resultColumnName)
    {
    }
    /**
     * Sets a column to use for indexing an entity or joined entity result by the given alias name.
     *
     * @param string $alias
     * @param string $resultColumnName
     *
     * @return $this
     */
    public function addIndexByColumn($alias, $resultColumnName)
    {
    }
    /**
     * Checks whether an entity result or joined entity result with a given alias has
     * a field set for indexing.
     *
     * @param string $alias
     *
     * @return bool
     *
     * @todo Rename: isIndexed($alias)
     */
    public function hasIndexBy($alias)
    {
    }
    /**
     * Checks whether the column with the given name is mapped as a field result
     * as part of an entity result or joined entity result.
     *
     * @param string $columnName The name of the column in the SQL result set.
     *
     * @return bool
     *
     * @todo Rename: isField
     */
    public function isFieldResult($columnName)
    {
    }
    /**
     * Adds a field to the result that belongs to an entity or joined entity.
     *
     * @param string      $alias          The alias of the root entity or joined entity to which the field belongs.
     * @param string      $columnName     The name of the column in the SQL result set.
     * @param string      $fieldName      The name of the field on the declaring class.
     * @param string|null $declaringClass The name of the class that declares/owns the specified field.
     *                                    When $alias refers to a superclass in a mapped hierarchy but
     *                                    the field $fieldName is defined on a subclass, specify that here.
     *                                    If not specified, the field is assumed to belong to the class
     *                                    designated by $alias.
     * @psalm-param class-string|null $declaringClass
     *
     * @return $this
     *
     * @todo Rename: addField
     */
    public function addFieldResult($alias, $columnName, $fieldName, $declaringClass = null)
    {
    }
    /**
     * Adds a joined entity result.
     *
     * @param string $class       The class name of the joined entity.
     * @param string $alias       The unique alias to use for the joined entity.
     * @param string $parentAlias The alias of the entity result that is the parent of this joined result.
     * @param string $relation    The association field that connects the parent entity result
     *                            with the joined entity result.
     * @psalm-param class-string $class
     *
     * @return $this
     *
     * @todo Rename: addJoinedEntity
     */
    public function addJoinedEntityResult($class, $alias, $parentAlias, $relation)
    {
    }
    /**
     * Adds a scalar result mapping.
     *
     * @param string     $columnName The name of the column in the SQL result set.
     * @param string|int $alias      The result alias with which the scalar result should be placed in the result structure.
     * @param string     $type       The column type
     *
     * @return $this
     *
     * @todo Rename: addScalar
     */
    public function addScalarResult($columnName, $alias, $type = 'string')
    {
    }
    /**
     * Adds a scalar result mapping.
     *
     * @param string $columnName The name of the column in the SQL result set.
     * @param string $enumType   The enum type
     *
     * @return $this
     */
    public function addEnumResult($columnName, $enumType)
    {
    }
    /**
     * Adds a metadata parameter mappings.
     *
     * @param string|int $parameter The parameter name in the SQL result set.
     * @param string     $attribute The metadata attribute.
     *
     * @return void
     */
    public function addMetadataParameterMapping($parameter, $attribute)
    {
    }
    /**
     * Checks whether a column with a given name is mapped as a scalar result.
     *
     * @param string $columnName The name of the column in the SQL result set.
     *
     * @return bool
     *
     * @todo Rename: isScalar
     */
    public function isScalarResult($columnName)
    {
    }
    /**
     * Gets the name of the class of an entity result or joined entity result,
     * identified by the given unique alias.
     *
     * @param string $alias
     *
     * @return class-string
     */
    public function getClassName($alias)
    {
    }
    /**
     * Gets the field alias for a column that is mapped as a scalar value.
     *
     * @param string $columnName The name of the column in the SQL result set.
     *
     * @return string|int
     */
    public function getScalarAlias($columnName)
    {
    }
    /**
     * Gets the name of the class that owns a field mapping for the specified column.
     *
     * @param string $columnName
     *
     * @return class-string
     */
    public function getDeclaringClass($columnName)
    {
    }
    /**
     * @param string $alias
     *
     * @return string
     */
    public function getRelation($alias)
    {
    }
    /**
     * @param string $alias
     *
     * @return bool
     */
    public function isRelation($alias)
    {
    }
    /**
     * Gets the alias of the class that owns a field mapping for the specified column.
     *
     * @param string $columnName
     *
     * @return string
     */
    public function getEntityAlias($columnName)
    {
    }
    /**
     * Gets the parent alias of the given alias.
     *
     * @param string $alias
     *
     * @return string
     */
    public function getParentAlias($alias)
    {
    }
    /**
     * Checks whether the given alias has a parent alias.
     *
     * @param string $alias
     *
     * @return bool
     */
    public function hasParentAlias($alias)
    {
    }
    /**
     * Gets the field name for a column name.
     *
     * @param string $columnName
     *
     * @return string
     */
    public function getFieldName($columnName)
    {
    }
    /**
     * @psalm-return array<string, class-string>
     */
    public function getAliasMap()
    {
    }
    /**
     * Gets the number of different entities that appear in the mapped result.
     *
     * @return int
     * @psalm-return 0|positive-int
     */
    public function getEntityResultCount()
    {
    }
    /**
     * Checks whether this ResultSetMapping defines a mixed result.
     *
     * Mixed results can only occur in object and array (graph) hydration. In such a
     * case a mixed result means that scalar values are mixed with objects/array in
     * the result.
     *
     * @return bool
     */
    public function isMixedResult()
    {
    }
    /**
     * Adds a meta column (foreign key or discriminator column) to the result set.
     *
     * @param string      $alias              The result alias with which the meta result should be placed in the result structure.
     * @param string      $columnName         The name of the column in the SQL result set.
     * @param string      $fieldName          The name of the field on the declaring class.
     * @param bool        $isIdentifierColumn
     * @param string|null $type               The column type
     *
     * @return $this
     *
     * @todo Make all methods of this class require all parameters and not infer anything
     */
    public function addMetaResult($alias, $columnName, $fieldName, $isIdentifierColumn = false, $type = null)
    {
    }
}
