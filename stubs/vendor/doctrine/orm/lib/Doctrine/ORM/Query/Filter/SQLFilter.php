<?php

namespace Doctrine\ORM\Query\Filter;

/**
 * The base class that user defined filters should extend.
 *
 * Handles the setting and escaping of parameters.
 *
 * @abstract
 */
abstract class SQLFilter
{
    /**
     * @param EntityManagerInterface $em The entity manager.
     */
    public final function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Sets a parameter list that can be used by the filter.
     *
     * @param string       $name   Name of the parameter.
     * @param array<mixed> $values List of parameter values.
     * @param string       $type   The parameter type. If specified, the given value will be run through
     *                             the type conversion of this type.
     *
     * @return $this
     */
    public final function setParameterList(string $name, array $values, string $type = \Doctrine\DBAL\Types\Types::STRING) : self
    {
    }
    /**
     * Sets a parameter that can be used by the filter.
     *
     * @param string      $name  Name of the parameter.
     * @param mixed       $value Value of the parameter.
     * @param string|null $type  The parameter type. If specified, the given value will be run through
     *                           the type conversion of this type. This is usually not needed for
     *                           strings and numeric types.
     *
     * @return $this
     */
    public final function setParameter($name, $value, $type = null) : self
    {
    }
    /**
     * Gets a parameter to use in a query.
     *
     * The function is responsible for the right output escaping to use the
     * value in a query.
     *
     * @param string $name Name of the parameter.
     *
     * @return string The SQL escaped parameter to use in a query.
     *
     * @throws InvalidArgumentException
     */
    public final function getParameter($name)
    {
    }
    /**
     * Gets a parameter to use in a query assuming it's a list of entries.
     *
     * The function is responsible for the right output escaping to use the
     * value in a query, separating each entry by comma to inline it into
     * an IN() query part.
     *
     * @param string $name Name of the parameter.
     *
     * @return string The SQL escaped parameter to use in a query.
     *
     * @throws InvalidArgumentException
     */
    public final function getParameterList(string $name) : string
    {
    }
    /**
     * Checks if a parameter was set for the filter.
     *
     * @param string $name Name of the parameter.
     *
     * @return bool
     */
    public final function hasParameter($name)
    {
    }
    /**
     * Returns as string representation of the SQLFilter parameters (the state).
     *
     * @return string String representation of the SQLFilter.
     */
    public final function __toString()
    {
    }
    /**
     * Returns the database connection used by the entity manager
     */
    protected final function getConnection() : \Doctrine\DBAL\Connection
    {
    }
    /**
     * Gets the SQL query part to add to a query.
     *
     * @param string $targetTableAlias
     * @psalm-param ClassMetadata<object> $targetEntity
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     */
    public abstract function addFilterConstraint(\Doctrine\ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias);
}
