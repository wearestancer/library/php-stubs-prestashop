<?php

namespace Doctrine\ORM\Query;

class QueryException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @param string $dql
     *
     * @return QueryException
     */
    public static function dqlError($dql)
    {
    }
    /**
     * @param string         $message
     * @param Exception|null $previous
     *
     * @return QueryException
     */
    public static function syntaxError($message, $previous = null)
    {
    }
    /**
     * @param string         $message
     * @param Exception|null $previous
     *
     * @return QueryException
     */
    public static function semanticalError($message, $previous = null)
    {
    }
    /**
     * @return QueryException
     */
    public static function invalidLockMode()
    {
    }
    /**
     * @param string $expected
     * @param string $received
     *
     * @return QueryException
     */
    public static function invalidParameterType($expected, $received)
    {
    }
    /**
     * @param string $pos
     *
     * @return QueryException
     */
    public static function invalidParameterPosition($pos)
    {
    }
    /**
     * @param int $expected
     * @param int $received
     *
     * @return QueryException
     */
    public static function tooManyParameters($expected, $received)
    {
    }
    /**
     * @param int $expected
     * @param int $received
     *
     * @return QueryException
     */
    public static function tooFewParameters($expected, $received)
    {
    }
    /**
     * @param string $value
     *
     * @return QueryException
     */
    public static function invalidParameterFormat($value)
    {
    }
    /**
     * @param string $key
     *
     * @return QueryException
     */
    public static function unknownParameter($key)
    {
    }
    /**
     * @return QueryException
     */
    public static function parameterTypeMismatch()
    {
    }
    /**
     * @param PathExpression $pathExpr
     *
     * @return QueryException
     */
    public static function invalidPathExpression($pathExpr)
    {
    }
    /**
     * @param string|Stringable $literal
     *
     * @return QueryException
     */
    public static function invalidLiteral($literal)
    {
    }
    /**
     * @param string[] $assoc
     * @psalm-param array<string, string> $assoc
     *
     * @return QueryException
     */
    public static function iterateWithFetchJoinCollectionNotAllowed($assoc)
    {
    }
    /**
     * @return QueryException
     */
    public static function partialObjectsAreDangerous()
    {
    }
    /**
     * @param string[] $assoc
     * @psalm-param array<string, string> $assoc
     *
     * @return QueryException
     */
    public static function overwritingJoinConditionsNotYetSupported($assoc)
    {
    }
    /**
     * @return QueryException
     */
    public static function associationPathInverseSideNotSupported(\Doctrine\ORM\Query\AST\PathExpression $pathExpr)
    {
    }
    /**
     * @param string[] $assoc
     * @psalm-param array<string, string> $assoc
     *
     * @return QueryException
     */
    public static function iterateWithFetchJoinNotAllowed($assoc)
    {
    }
    public static function iterateWithMixedResultNotAllowed() : \Doctrine\ORM\Query\QueryException
    {
    }
    /**
     * @return QueryException
     */
    public static function associationPathCompositeKeyNotSupported()
    {
    }
    /**
     * @param string $className
     * @param string $rootClass
     *
     * @return QueryException
     */
    public static function instanceOfUnrelatedClass($className, $rootClass)
    {
    }
    /**
     * @param string $dqlAlias
     *
     * @return QueryException
     */
    public static function invalidQueryComponent($dqlAlias)
    {
    }
}
