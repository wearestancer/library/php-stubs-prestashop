<?php

namespace Doctrine\ORM\Query\Exec;

/**
 * Executes the SQL statements for bulk DQL DELETE statements on classes in
 * Class Table Inheritance (JOINED).
 *
 * @link        http://www.doctrine-project.org
 */
class MultiTableDeleteExecutor extends \Doctrine\ORM\Query\Exec\AbstractSqlExecutor
{
    /**
     * Initializes a new <tt>MultiTableDeleteExecutor</tt>.
     *
     * Internal note: Any SQL construction and preparation takes place in the constructor for
     *                best performance. With a query cache the executor will be cached.
     *
     * @param DeleteStatement $AST       The root AST node of the DQL query.
     * @param SqlWalker       $sqlWalker The walker used for SQL generation from the AST.
     */
    public function __construct(\Doctrine\ORM\Query\AST\Node $AST, $sqlWalker)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return int
     */
    public function execute(\Doctrine\DBAL\Connection $conn, array $params, array $types)
    {
    }
}
