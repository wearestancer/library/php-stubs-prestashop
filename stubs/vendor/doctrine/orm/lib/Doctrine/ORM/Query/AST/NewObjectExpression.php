<?php

namespace Doctrine\ORM\Query\AST;

/**
 * NewObjectExpression ::= "NEW" IdentificationVariable "(" NewObjectArg {"," NewObjectArg}* ")"
 *
 * @link    www.doctrine-project.org
 */
class NewObjectExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $className;
    /** @var mixed[] */
    public $args;
    /**
     * @param string  $className
     * @param mixed[] $args
     */
    public function __construct($className, array $args)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
