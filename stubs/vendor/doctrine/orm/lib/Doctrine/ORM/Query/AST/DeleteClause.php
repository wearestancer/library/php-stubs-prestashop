<?php

namespace Doctrine\ORM\Query\AST;

/**
 * DeleteClause ::= "DELETE" ["FROM"] AbstractSchemaName [["AS"] AliasIdentificationVariable]
 *
 * @link    www.doctrine-project.org
 */
class DeleteClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $abstractSchemaName;
    /** @var string */
    public $aliasIdentificationVariable;
    /**
     * @param string $abstractSchemaName
     */
    public function __construct($abstractSchemaName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
