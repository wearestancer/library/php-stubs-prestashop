<?php

namespace Doctrine\ORM\Query\AST;

/**
 * IndexBy ::= "INDEX" "BY" SingleValuedPathExpression
 *
 * @link    www.doctrine-project.org
 */
class IndexBy extends \Doctrine\ORM\Query\AST\Node
{
    /** @var PathExpression */
    public $singleValuedPathExpression = null;
    /**
     * @deprecated
     *
     * @var PathExpression
     */
    public $simpleStateFieldPathExpression = null;
    public function __construct(\Doctrine\ORM\Query\AST\PathExpression $singleValuedPathExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
