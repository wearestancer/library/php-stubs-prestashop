<?php

namespace Doctrine\ORM\Query\AST;

/**
 * Abstract class of an AST node.
 *
 * @link    www.doctrine-project.org
 */
abstract class Node
{
    /**
     * Double-dispatch method, supposed to dispatch back to the walker.
     *
     * Implementation is not mandatory for all nodes.
     *
     * @param SqlWalker $walker
     *
     * @return string
     *
     * @throws ASTException
     */
    public function dispatch($walker)
    {
    }
    /**
     * Dumps the AST Node into a string representation for information purpose only.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * @param object $obj
     *
     * @return string
     */
    public function dump($obj)
    {
    }
}
