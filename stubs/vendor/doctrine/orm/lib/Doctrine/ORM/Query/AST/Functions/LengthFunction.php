<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "LENGTH" "(" StringPrimary ")"
 *
 * @link    www.doctrine-project.org
 */
class LengthFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode implements \Doctrine\ORM\Query\AST\TypedExpression
{
    /** @var Node */
    public $stringPrimary;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getReturnType() : \Doctrine\DBAL\Types\Type
    {
    }
}
