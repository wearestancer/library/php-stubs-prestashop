<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "UPPER" "(" StringPrimary ")"
 *
 * @link    www.doctrine-project.org
 */
class UpperFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    public $stringPrimary;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
