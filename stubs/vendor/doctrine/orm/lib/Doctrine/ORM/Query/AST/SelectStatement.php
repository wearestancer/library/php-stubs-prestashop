<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SelectStatement = SelectClause FromClause [WhereClause] [GroupByClause] [HavingClause] [OrderByClause]
 *
 * @link    www.doctrine-project.org
 */
class SelectStatement extends \Doctrine\ORM\Query\AST\Node
{
    /** @var SelectClause */
    public $selectClause;
    /** @var FromClause */
    public $fromClause;
    /** @var WhereClause|null */
    public $whereClause;
    /** @var GroupByClause|null */
    public $groupByClause;
    /** @var HavingClause|null */
    public $havingClause;
    /** @var OrderByClause|null */
    public $orderByClause;
    /**
     * @param SelectClause $selectClause
     * @param FromClause   $fromClause
     */
    public function __construct($selectClause, $fromClause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
