<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ParenthesisExpression ::= "(" ArithmeticPrimary ")"
 */
class ParenthesisExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var Node */
    public $expression;
    public function __construct(\Doctrine\ORM\Query\AST\Node $expression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
