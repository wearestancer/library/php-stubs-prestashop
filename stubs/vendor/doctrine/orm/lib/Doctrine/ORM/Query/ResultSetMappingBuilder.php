<?php

namespace Doctrine\ORM\Query;

/**
 * A ResultSetMappingBuilder uses the EntityManager to automatically populate entity fields.
 */
class ResultSetMappingBuilder extends \Doctrine\ORM\Query\ResultSetMapping
{
    use \Doctrine\ORM\Internal\SQLResultCasing;
    /**
     * Picking this rename mode will register entity columns as is,
     * as they are in the database. This can cause clashes when multiple
     * entities are fetched that have columns with the same name.
     */
    public const COLUMN_RENAMING_NONE = 1;
    /**
     * Picking custom renaming allows the user to define the renaming
     * of specific columns with a rename array that contains column names as
     * keys and result alias as values.
     */
    public const COLUMN_RENAMING_CUSTOM = 2;
    /**
     * Incremental renaming uses a result set mapping internal counter to add a
     * number to each column result, leading to uniqueness. This only works if
     * you use {@see generateSelectClause()} to generate the SELECT clause for
     * you.
     */
    public const COLUMN_RENAMING_INCREMENT = 3;
    /**
     * @param int $defaultRenameMode
     * @psalm-param self::COLUMN_RENAMING_* $defaultRenameMode
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, $defaultRenameMode = self::COLUMN_RENAMING_NONE)
    {
    }
    /**
     * Adds a root entity and all of its fields to the result set.
     *
     * @param string   $class          The class name of the root entity.
     * @param string   $alias          The unique alias to use for the root entity.
     * @param string[] $renamedColumns Columns that have been renamed (tableColumnName => queryColumnName).
     * @param int|null $renameMode     One of the COLUMN_RENAMING_* constants or array for BC reasons (CUSTOM).
     * @psalm-param class-string $class
     * @psalm-param array<string, string> $renamedColumns
     * @psalm-param self::COLUMN_RENAMING_*|null $renameMode
     *
     * @return void
     */
    public function addRootEntityFromClassMetadata($class, $alias, $renamedColumns = [], $renameMode = null)
    {
    }
    /**
     * Adds a joined entity and all of its fields to the result set.
     *
     * @param string   $class          The class name of the joined entity.
     * @param string   $alias          The unique alias to use for the joined entity.
     * @param string   $parentAlias    The alias of the entity result that is the parent of this joined result.
     * @param string   $relation       The association field that connects the parent entity result
     *                                 with the joined entity result.
     * @param string[] $renamedColumns Columns that have been renamed (tableColumnName => queryColumnName).
     * @param int|null $renameMode     One of the COLUMN_RENAMING_* constants or array for BC reasons (CUSTOM).
     * @psalm-param class-string $class
     * @psalm-param array<string, string> $renamedColumns
     * @psalm-param self::COLUMN_RENAMING_*|null $renameMode
     *
     * @return void
     */
    public function addJoinedEntityFromClassMetadata($class, $alias, $parentAlias, $relation, $renamedColumns = [], $renameMode = null)
    {
    }
    /**
     * Adds all fields of the given class to the result set mapping (columns and meta fields).
     *
     * @param string   $class
     * @param string   $alias
     * @param string[] $columnAliasMap
     * @psalm-param array<string, string> $columnAliasMap
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    protected function addAllClassFields($class, $alias, $columnAliasMap = [])
    {
    }
    /**
     * Adds the mappings of the results of native SQL queries to the result set.
     *
     * @param mixed[] $queryMapping
     *
     * @return ResultSetMappingBuilder
     */
    public function addNamedNativeQueryMapping(\Doctrine\ORM\Mapping\ClassMetadataInfo $class, array $queryMapping)
    {
    }
    /**
     * Adds the class mapping of the results of native SQL queries to the result set.
     *
     * @param string $resultClassName
     *
     * @return $this
     */
    public function addNamedNativeQueryResultClassMapping(\Doctrine\ORM\Mapping\ClassMetadataInfo $class, $resultClassName)
    {
    }
    /**
     * Adds the result set mapping of the results of native SQL queries to the result set.
     *
     * @param string $resultSetMappingName
     *
     * @return $this
     */
    public function addNamedNativeQueryResultSetMapping(\Doctrine\ORM\Mapping\ClassMetadataInfo $class, $resultSetMappingName)
    {
    }
    /**
     * Adds the entity result mapping of the results of native SQL queries to the result set.
     *
     * @param mixed[] $entityMapping
     * @param string  $alias
     *
     * @return $this
     *
     * @throws MappingException
     * @throws InvalidArgumentException
     */
    public function addNamedNativeQueryEntityResultMapping(\Doctrine\ORM\Mapping\ClassMetadataInfo $classMetadata, array $entityMapping, $alias)
    {
    }
    /**
     * Generates the Select clause from this ResultSetMappingBuilder.
     *
     * Works only for all the entity results. The select parts for scalar
     * expressions have to be written manually.
     *
     * @param string[] $tableAliases
     * @psalm-param array<string, string> $tableAliases
     *
     * @return string
     */
    public function generateSelectClause($tableAliases = [])
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
