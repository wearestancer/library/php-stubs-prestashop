<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ArithmeticFactor ::= [("+" | "-")] ArithmeticPrimary
 *
 * @link    www.doctrine-project.org
 */
class ArithmeticFactor extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed */
    public $arithmeticPrimary;
    /**
     * NULL represents no sign, TRUE means positive and FALSE means negative sign.
     *
     * @var bool|null
     */
    public $sign;
    /**
     * @param mixed     $arithmeticPrimary
     * @param bool|null $sign
     */
    public function __construct($arithmeticPrimary, $sign = null)
    {
    }
    /**
     * @return bool
     */
    public function isPositiveSigned()
    {
    }
    /**
     * @return bool
     */
    public function isNegativeSigned()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
