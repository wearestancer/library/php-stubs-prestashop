<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for building DQL and parts.
 *
 * @link    www.doctrine-project.org
 */
class Composite extends \Doctrine\ORM\Query\Expr\Base
{
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
