<?php

namespace Doctrine\ORM\Query\Exec;

/**
 * Executor that executes the SQL statements for DQL DELETE/UPDATE statements on classes
 * that are mapped to a single table.
 *
 * @link        www.doctrine-project.org
 *
 * @todo This is exactly the same as SingleSelectExecutor. Unify in SingleStatementExecutor.
 */
class SingleTableDeleteUpdateExecutor extends \Doctrine\ORM\Query\Exec\AbstractSqlExecutor
{
    /**
     * @param SqlWalker $sqlWalker
     */
    public function __construct(\Doctrine\ORM\Query\AST\Node $AST, $sqlWalker)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return int
     */
    public function execute(\Doctrine\DBAL\Connection $conn, array $params, array $types)
    {
    }
}
