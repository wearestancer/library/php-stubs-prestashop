<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for building DQL select statements.
 *
 * @link    www.doctrine-project.org
 */
class Select extends \Doctrine\ORM\Query\Expr\Base
{
    /** @var string */
    protected $preSeparator = '';
    /** @var string */
    protected $postSeparator = '';
    /** @var string[] */
    protected $allowedClasses = [\Doctrine\ORM\Query\Expr\Func::class];
    /** @psalm-var list<string|Func> */
    protected $parts = [];
    /**
     * @psalm-return list<string|Func>
     */
    public function getParts()
    {
    }
}
