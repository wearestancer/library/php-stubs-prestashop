<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SimpleWhenClause ::= "WHEN" ScalarExpression "THEN" ScalarExpression
 *
 * @link    www.doctrine-project.org
 */
class SimpleWhenClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed */
    public $caseScalarExpression = null;
    /** @var mixed */
    public $thenScalarExpression = null;
    /**
     * @param mixed $caseScalarExpression
     * @param mixed $thenScalarExpression
     */
    public function __construct($caseScalarExpression, $thenScalarExpression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
