<?php

namespace Doctrine\ORM\Query\AST;

class BetweenExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var ArithmeticExpression */
    public $expression;
    /** @var ArithmeticExpression */
    public $leftBetweenExpression;
    /** @var ArithmeticExpression */
    public $rightBetweenExpression;
    /** @var bool */
    public $not;
    /**
     * @param ArithmeticExpression $expr
     * @param ArithmeticExpression $leftExpr
     * @param ArithmeticExpression $rightExpr
     */
    public function __construct($expr, $leftExpr, $rightExpr)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
