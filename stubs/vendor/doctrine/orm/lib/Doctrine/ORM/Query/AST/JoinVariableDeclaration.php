<?php

namespace Doctrine\ORM\Query\AST;

/**
 * JoinVariableDeclaration ::= Join [IndexBy]
 *
 * @link    www.doctrine-project.org
 */
class JoinVariableDeclaration extends \Doctrine\ORM\Query\AST\Node
{
    /** @var Join */
    public $join;
    /** @var IndexBy|null */
    public $indexBy;
    /**
     * @param Join         $join
     * @param IndexBy|null $indexBy
     */
    public function __construct($join, $indexBy)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($walker)
    {
    }
}
