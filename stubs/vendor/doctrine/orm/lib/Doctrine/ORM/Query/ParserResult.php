<?php

namespace Doctrine\ORM\Query;

/**
 * Encapsulates the resulting components from a DQL query parsing process that
 * can be serialized.
 *
 * @link        http://www.doctrine-project.org
 */
class ParserResult
{
    /**
     * Initializes a new instance of the <tt>ParserResult</tt> class.
     * The new instance is initialized with an empty <tt>ResultSetMapping</tt>.
     */
    public function __construct()
    {
    }
    /**
     * Gets the ResultSetMapping for the parsed query.
     *
     * @return ResultSetMapping The result set mapping of the parsed query
     */
    public function getResultSetMapping()
    {
    }
    /**
     * Sets the ResultSetMapping of the parsed query.
     *
     * @return void
     */
    public function setResultSetMapping(\Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
    }
    /**
     * Sets the SQL executor that should be used for this ParserResult.
     *
     * @param AbstractSqlExecutor $executor
     *
     * @return void
     */
    public function setSqlExecutor($executor)
    {
    }
    /**
     * Gets the SQL executor used by this ParserResult.
     *
     * @return AbstractSqlExecutor
     */
    public function getSqlExecutor()
    {
    }
    /**
     * Adds a DQL to SQL parameter mapping. One DQL parameter name/position can map to
     * several SQL parameter positions.
     *
     * @param string|int $dqlPosition
     * @param int        $sqlPosition
     *
     * @return void
     */
    public function addParameterMapping($dqlPosition, $sqlPosition)
    {
    }
    /**
     * Gets all DQL to SQL parameter mappings.
     *
     * @psalm-return array<int|string, list<int>> The parameter mappings.
     */
    public function getParameterMappings()
    {
    }
    /**
     * Gets the SQL parameter positions for a DQL parameter name/position.
     *
     * @param string|int $dqlPosition The name or position of the DQL parameter.
     *
     * @return int[] The positions of the corresponding SQL parameters.
     * @psalm-return list<int>
     */
    public function getSqlParameterPositions($dqlPosition)
    {
    }
}
