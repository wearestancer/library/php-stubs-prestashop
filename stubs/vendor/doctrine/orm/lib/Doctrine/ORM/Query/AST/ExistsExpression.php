<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ExistsExpression ::= ["NOT"] "EXISTS" "(" Subselect ")"
 *
 * @link    www.doctrine-project.org
 */
class ExistsExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $not;
    /** @var Subselect */
    public $subselect;
    /**
     * @param Subselect $subselect
     */
    public function __construct($subselect)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
