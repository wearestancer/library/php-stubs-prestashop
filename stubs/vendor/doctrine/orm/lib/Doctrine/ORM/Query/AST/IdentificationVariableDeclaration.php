<?php

namespace Doctrine\ORM\Query\AST;

/**
 * IdentificationVariableDeclaration ::= RangeVariableDeclaration [IndexBy] {JoinVariableDeclaration}*
 *
 * @link    www.doctrine-project.org
 */
class IdentificationVariableDeclaration extends \Doctrine\ORM\Query\AST\Node
{
    /** @var RangeVariableDeclaration|null */
    public $rangeVariableDeclaration = null;
    /** @var IndexBy|null */
    public $indexBy = null;
    /** @var mixed[] */
    public $joins = [];
    /**
     * @param RangeVariableDeclaration|null $rangeVariableDecl
     * @param IndexBy|null                  $indexBy
     * @param mixed[]                       $joins
     */
    public function __construct($rangeVariableDecl, $indexBy, array $joins)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
