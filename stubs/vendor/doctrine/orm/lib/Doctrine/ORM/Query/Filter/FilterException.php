<?php

namespace Doctrine\ORM\Query\Filter;

class FilterException extends \Doctrine\ORM\Exception\ORMException
{
    public static function cannotConvertListParameterIntoSingleValue(string $name) : self
    {
    }
    public static function cannotConvertSingleParameterIntoListValue(string $name) : self
    {
    }
}
