<?php

namespace Doctrine\ORM\Query\AST;

class PartialObjectExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var string */
    public $identificationVariable;
    /** @var mixed[] */
    public $partialFieldSet;
    /**
     * @param string  $identificationVariable
     * @param mixed[] $partialFieldSet
     */
    public function __construct($identificationVariable, array $partialFieldSet)
    {
    }
}
