<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for DQL from.
 *
 * @link    www.doctrine-project.org
 */
class From
{
    /** @var string */
    protected $from;
    /** @var string */
    protected $alias;
    /** @var string|null */
    protected $indexBy;
    /**
     * @param string $from    The class name.
     * @param string $alias   The alias of the class.
     * @param string $indexBy The index for the from.
     */
    public function __construct($from, $alias, $indexBy = null)
    {
    }
    /**
     * @return string
     */
    public function getFrom()
    {
    }
    /**
     * @return string
     */
    public function getAlias()
    {
    }
    /**
     * @return string|null
     */
    public function getIndexBy()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
