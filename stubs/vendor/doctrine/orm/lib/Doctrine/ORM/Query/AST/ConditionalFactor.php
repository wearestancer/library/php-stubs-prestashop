<?php

namespace Doctrine\ORM\Query\AST;

/**
 * ConditionalFactor ::= ["NOT"] ConditionalPrimary
 *
 * @link    www.doctrine-project.org
 */
class ConditionalFactor extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $not = false;
    /** @var ConditionalPrimary */
    public $conditionalPrimary;
    /**
     * @param ConditionalPrimary $conditionalPrimary
     */
    public function __construct($conditionalPrimary)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
