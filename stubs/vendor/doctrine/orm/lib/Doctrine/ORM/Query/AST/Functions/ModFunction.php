<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "MOD" "(" SimpleArithmeticExpression "," SimpleArithmeticExpression ")"
 *
 * @link    www.doctrine-project.org
 */
class ModFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var SimpleArithmeticExpression */
    public $firstSimpleArithmeticExpression;
    /** @var SimpleArithmeticExpression */
    public $secondSimpleArithmeticExpression;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
