<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "IDENTITY" "(" SingleValuedAssociationPathExpression {"," string} ")"
 *
 * @link    www.doctrine-project.org
 */
class IdentityFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var PathExpression */
    public $pathExpression;
    /** @var string|null */
    public $fieldMapping;
    /**
     * {@inheritdoc}
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
