<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SimpleSelectClause  ::= "SELECT" ["DISTINCT"] SimpleSelectExpression
 *
 * @link    www.doctrine-project.org
 */
class SimpleSelectClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var bool */
    public $isDistinct = false;
    /** @var SimpleSelectExpression */
    public $simpleSelectExpression;
    /**
     * @param SimpleSelectExpression $simpleSelectExpression
     * @param bool                   $isDistinct
     */
    public function __construct($simpleSelectExpression, $isDistinct)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
