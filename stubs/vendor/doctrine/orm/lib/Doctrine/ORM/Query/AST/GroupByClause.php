<?php

namespace Doctrine\ORM\Query\AST;

class GroupByClause extends \Doctrine\ORM\Query\AST\Node
{
    /** @var mixed[] */
    public $groupByItems = [];
    /**
     * @param mixed[] $groupByItems
     */
    public function __construct(array $groupByItems)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
