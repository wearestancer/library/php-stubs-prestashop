<?php

namespace Doctrine\ORM\Query\AST\Functions;

/**
 * "BIT_OR" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
 *
 * @link    www.doctrine-project.org
 */
class BitOrFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    public $firstArithmetic;
    /** @var Node */
    public $secondArithmetic;
    /**
     * @override
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @override
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
