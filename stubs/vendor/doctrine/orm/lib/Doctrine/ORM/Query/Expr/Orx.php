<?php

namespace Doctrine\ORM\Query\Expr;

/**
 * Expression class for building DQL OR clauses.
 *
 * @link    www.doctrine-project.org
 */
class Orx extends \Doctrine\ORM\Query\Expr\Composite
{
    /** @var string */
    protected $separator = ' OR ';
    /** @var string[] */
    protected $allowedClasses = [\Doctrine\ORM\Query\Expr\Comparison::class, \Doctrine\ORM\Query\Expr\Func::class, \Doctrine\ORM\Query\Expr\Andx::class, self::class];
    /** @psalm-var list<string|Comparison|Func|Andx|self> */
    protected $parts = [];
    /**
     * @psalm-return list<string|Comparison|Func|Andx|self>
     */
    public function getParts()
    {
    }
}
