<?php

namespace Doctrine\ORM\Query\AST;

/**
 * SimpleSelectExpression ::= StateFieldPathExpression | IdentificationVariable
 *                          | (AggregateExpression [["AS"] FieldAliasIdentificationVariable])
 *
 * @link    www.doctrine-project.org
 */
class SimpleSelectExpression extends \Doctrine\ORM\Query\AST\Node
{
    /** @var Node|string */
    public $expression;
    /** @var string */
    public $fieldIdentificationVariable;
    /**
     * @param Node|string $expression
     */
    public function __construct($expression)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($sqlWalker)
    {
    }
}
