<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * The ArrayHydrator produces a nested array "graph" that is often (not always)
 * interchangeable with the corresponding object graph for read-only access.
 */
class ArrayHydrator extends \Doctrine\ORM\Internal\Hydration\AbstractHydrator
{
    /**
     * {@inheritdoc}
     */
    protected function prepare()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function hydrateRowData(array $row, array &$result)
    {
    }
}
