<?php

namespace Doctrine\ORM\Internal\Hydration;

class HydrationException extends \Doctrine\ORM\Exception\ORMException
{
    /**
     * @return HydrationException
     */
    public static function nonUniqueResult()
    {
    }
    /**
     * @param string $alias
     * @param string $parentAlias
     *
     * @return HydrationException
     */
    public static function parentObjectOfRelationNotFound($alias, $parentAlias)
    {
    }
    /**
     * @param string $dqlAlias
     *
     * @return HydrationException
     */
    public static function emptyDiscriminatorValue($dqlAlias)
    {
    }
    /**
     * @param string $entityName
     * @param string $discrColumnName
     * @param string $dqlAlias
     *
     * @return HydrationException
     */
    public static function missingDiscriminatorColumn($entityName, $discrColumnName, $dqlAlias)
    {
    }
    /**
     * @param string $entityName
     * @param string $discrColumnName
     * @param string $dqlAlias
     *
     * @return HydrationException
     */
    public static function missingDiscriminatorMetaMappingColumn($entityName, $discrColumnName, $dqlAlias)
    {
    }
    /**
     * @param string   $discrValue
     * @param string[] $discrMap
     * @psalm-param array<string, string> $discrMap
     *
     * @return HydrationException
     */
    public static function invalidDiscriminatorValue($discrValue, $discrMap)
    {
    }
}
