<?php

namespace Doctrine\ORM\Internal\Hydration;

class SimpleObjectHydrator extends \Doctrine\ORM\Internal\Hydration\AbstractHydrator
{
    use \Doctrine\ORM\Internal\SQLResultCasing;
    /**
     * {@inheritdoc}
     */
    protected function prepare()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function cleanup()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function hydrateRowData(array $row, array &$result)
    {
    }
}
