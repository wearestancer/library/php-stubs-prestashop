<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * Base class for all hydrators. A hydrator is a class that provides some form
 * of transformation of an SQL result set into another structure.
 */
abstract class AbstractHydrator
{
    /**
     * The ResultSetMapping.
     *
     * @var ResultSetMapping|null
     */
    protected $_rsm;
    /**
     * The EntityManager instance.
     *
     * @var EntityManagerInterface
     */
    protected $_em;
    /**
     * The dbms Platform instance.
     *
     * @var AbstractPlatform
     */
    protected $_platform;
    /**
     * The UnitOfWork of the associated EntityManager.
     *
     * @var UnitOfWork
     */
    protected $_uow;
    /**
     * Local ClassMetadata cache to avoid going to the EntityManager all the time.
     *
     * @var array<string, ClassMetadata<object>>
     */
    protected $_metadataCache = [];
    /**
     * The cache used during row-by-row hydration.
     *
     * @var array<string, mixed[]|null>
     */
    protected $_cache = [];
    /**
     * The statement that provides the data to hydrate.
     *
     * @var Result|null
     */
    protected $_stmt;
    /**
     * The query hints.
     *
     * @var array<string, mixed>
     */
    protected $_hints = [];
    /**
     * Initializes a new instance of a class derived from <tt>AbstractHydrator</tt>.
     *
     * @param EntityManagerInterface $em The EntityManager to use.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Initiates a row-by-row hydration.
     *
     * @deprecated
     *
     * @param Result|ResultStatement $stmt
     * @param ResultSetMapping       $resultSetMapping
     * @psalm-param array<string, mixed> $hints
     *
     * @return IterableResult
     */
    public function iterate($stmt, $resultSetMapping, array $hints = [])
    {
    }
    /**
     * Initiates a row-by-row hydration.
     *
     * @param Result|ResultStatement $stmt
     * @psalm-param array<string, mixed> $hints
     *
     * @return Generator<array-key, mixed>
     *
     * @final
     */
    public function toIterable($stmt, \Doctrine\ORM\Query\ResultSetMapping $resultSetMapping, array $hints = []) : iterable
    {
    }
    protected final function statement() : \Doctrine\DBAL\Result
    {
    }
    protected final function resultSetMapping() : \Doctrine\ORM\Query\ResultSetMapping
    {
    }
    /**
     * Hydrates all rows returned by the passed statement instance at once.
     *
     * @param Result|ResultStatement $stmt
     * @param ResultSetMapping       $resultSetMapping
     * @psalm-param array<string, string> $hints
     *
     * @return mixed[]
     */
    public function hydrateAll($stmt, $resultSetMapping, array $hints = [])
    {
    }
    /**
     * Hydrates a single row returned by the current statement instance during
     * row-by-row hydration with {@link iterate()} or {@link toIterable()}.
     *
     * @deprecated
     *
     * @return mixed[]|false
     */
    public function hydrateRow()
    {
    }
    /**
     * When executed in a hydrate() loop we have to clear internal state to
     * decrease memory consumption.
     *
     * @param mixed $eventArgs
     *
     * @return void
     */
    public function onClear($eventArgs)
    {
    }
    /**
     * Executes one-time preparation tasks, once each time hydration is started
     * through {@link hydrateAll} or {@link iterate()}.
     *
     * @return void
     */
    protected function prepare()
    {
    }
    /**
     * Executes one-time cleanup tasks at the end of a hydration that was initiated
     * through {@link hydrateAll} or {@link iterate()}.
     *
     * @return void
     */
    protected function cleanup()
    {
    }
    protected function cleanupAfterRowIteration() : void
    {
    }
    /**
     * Hydrates a single row from the current statement instance.
     *
     * Template method.
     *
     * @param mixed[] $row    The row data.
     * @param mixed[] $result The result to fill.
     *
     * @return void
     *
     * @throws HydrationException
     */
    protected function hydrateRowData(array $row, array &$result)
    {
    }
    /**
     * Hydrates all rows from the current statement instance at once.
     *
     * @return mixed[]
     */
    protected abstract function hydrateAllData();
    /**
     * Processes a row of the result set.
     *
     * Used for identity-based hydration (HYDRATE_OBJECT and HYDRATE_ARRAY).
     * Puts the elements of a result row into a new array, grouped by the dql alias
     * they belong to. The column names in the result set are mapped to their
     * field names during this procedure as well as any necessary conversions on
     * the values applied. Scalar values are kept in a specific key 'scalars'.
     *
     * @param mixed[] $data SQL Result Row.
     * @psalm-param array<string, string> $id                 Dql-Alias => ID-Hash.
     * @psalm-param array<string, bool>   $nonemptyComponents Does this DQL-Alias has at least one non NULL value?
     *
     * @return array<string, array<string, mixed>> An array with all the fields
     *                                             (name => value) of the data
     *                                             row, grouped by their
     *                                             component alias.
     * @psalm-return array{
     *                   data: array<array-key, array>,
     *                   newObjects?: array<array-key, array{
     *                       class: mixed,
     *                       args?: array
     *                   }>,
     *                   scalars?: array
     *               }
     */
    protected function gatherRowData(array $data, array &$id, array &$nonemptyComponents)
    {
    }
    /**
     * Processes a row of the result set.
     *
     * Used for HYDRATE_SCALAR. This is a variant of _gatherRowData() that
     * simply converts column names to field names and properly converts the
     * values according to their types. The resulting row has the same number
     * of elements as before.
     *
     * @param mixed[] $data
     * @psalm-param array<string, mixed> $data
     *
     * @return mixed[] The processed row.
     * @psalm-return array<string, mixed>
     */
    protected function gatherScalarRowData(&$data)
    {
    }
    /**
     * Retrieve column information from ResultSetMapping.
     *
     * @param string $key Column name
     *
     * @return mixed[]|null
     * @psalm-return array<string, mixed>|null
     */
    protected function hydrateColumnInfo($key)
    {
    }
    /**
     * Retrieve ClassMetadata associated to entity class name.
     *
     * @param string $className
     *
     * @return ClassMetadata
     */
    protected function getClassMetadata($className)
    {
    }
    /**
     * Register entity as managed in UnitOfWork.
     *
     * @param object  $entity
     * @param mixed[] $data
     *
     * @return void
     *
     * @todo The "$id" generation is the same of UnitOfWork#createEntity. Remove this duplication somehow
     */
    protected function registerManaged(\Doctrine\ORM\Mapping\ClassMetadata $class, $entity, array $data)
    {
    }
}
