<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * Hydrator that produces flat, rectangular results of scalar data.
 * The created result is almost the same as a regular SQL result set, except
 * that column names are mapped to field names and data type conversions take place.
 */
class ScalarHydrator extends \Doctrine\ORM\Internal\Hydration\AbstractHydrator
{
    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function hydrateRowData(array $row, array &$result)
    {
    }
}
