<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * Hydrator that produces one-dimensional array.
 */
final class ScalarColumnHydrator extends \Doctrine\ORM\Internal\Hydration\AbstractHydrator
{
}
