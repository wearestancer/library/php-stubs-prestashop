<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * Represents a result structure that can be iterated over, hydrating row-by-row
 * during the iteration. An IterableResult is obtained by AbstractHydrator#iterate().
 *
 * @deprecated
 */
class IterableResult implements \Iterator
{
    /**
     * @param AbstractHydrator $hydrator
     */
    public function __construct($hydrator)
    {
    }
    /**
     * @return void
     *
     * @throws HydrationException
     */
    #[\ReturnTypeWillChange]
    public function rewind()
    {
    }
    /**
     * Gets the next set of results.
     *
     * @return mixed[]|false
     */
    #[\ReturnTypeWillChange]
    public function next()
    {
    }
    /**
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function valid()
    {
    }
}
