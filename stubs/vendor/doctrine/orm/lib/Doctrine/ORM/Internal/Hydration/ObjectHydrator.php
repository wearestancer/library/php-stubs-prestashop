<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * The ObjectHydrator constructs an object graph out of an SQL result set.
 *
 * Internal note: Highly performance-sensitive code.
 */
class ObjectHydrator extends \Doctrine\ORM\Internal\Hydration\AbstractHydrator
{
    /**
     * {@inheritdoc}
     */
    protected function prepare()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function cleanup()
    {
    }
    protected function cleanupAfterRowIteration() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
    }
    /**
     * Hydrates a single row in an SQL result set.
     *
     * @internal
     * First, the data of the row is split into chunks where each chunk contains data
     * that belongs to a particular component/class. Afterwards, all these chunks
     * are processed, one after the other. For each chunk of class data only one of the
     * following code paths is executed:
     *
     * Path A: The data chunk belongs to a joined/associated object and the association
     *         is collection-valued.
     * Path B: The data chunk belongs to a joined/associated object and the association
     *         is single-valued.
     * Path C: The data chunk belongs to a root result element/object that appears in the topmost
     *         level of the hydrated result. A typical example are the objects of the type
     *         specified by the FROM clause in a DQL query.
     *
     * @param mixed[] $row    The data of the row to process.
     * @param mixed[] $result The result array to fill.
     *
     * @return void
     */
    protected function hydrateRowData(array $row, array &$result)
    {
    }
    /**
     * When executed in a hydrate() loop we may have to clear internal state to
     * decrease memory consumption.
     *
     * @param mixed $eventArgs
     *
     * @return void
     */
    public function onClear($eventArgs)
    {
    }
}
