<?php

namespace Doctrine\ORM\Internal;

/**
 * Class, which can handle completion of hydration cycle and produce some of tasks.
 * In current implementation triggers deferred postLoad event.
 */
final class HydrationCompleteHandler
{
    /**
     * Constructor for this object
     */
    public function __construct(\Doctrine\ORM\Event\ListenersInvoker $listenersInvoker, \Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Method schedules invoking of postLoad entity to the very end of current hydration cycle.
     *
     * @param object $entity
     */
    public function deferPostLoadInvoking(\Doctrine\ORM\Mapping\ClassMetadata $class, $entity) : void
    {
    }
    /**
     * This method should me called after any hydration cycle completed.
     *
     * Method fires all deferred invocations of postLoad events
     */
    public function hydrationComplete() : void
    {
    }
}
