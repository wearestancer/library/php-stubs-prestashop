<?php

namespace Doctrine\ORM\Internal\Hydration;

/**
 * Hydrator that hydrates a single scalar value from the result set.
 */
class SingleScalarHydrator extends \Doctrine\ORM\Internal\Hydration\AbstractHydrator
{
    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
    }
}
