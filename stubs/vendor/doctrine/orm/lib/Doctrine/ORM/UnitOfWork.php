<?php

namespace Doctrine\ORM;

/**
 * The UnitOfWork is responsible for tracking changes to objects during an
 * "object-level" transaction and for writing out changes to the database
 * in the correct order.
 *
 * Internal note: This class contains highly performance-sensitive code.
 */
class UnitOfWork implements \Doctrine\Persistence\PropertyChangedListener
{
    /**
     * An entity is in MANAGED state when its persistence is managed by an EntityManager.
     */
    public const STATE_MANAGED = 1;
    /**
     * An entity is new if it has just been instantiated (i.e. using the "new" operator)
     * and is not (yet) managed by an EntityManager.
     */
    public const STATE_NEW = 2;
    /**
     * A detached entity is an instance with persistent state and identity that is not
     * (or no longer) associated with an EntityManager (and a UnitOfWork).
     */
    public const STATE_DETACHED = 3;
    /**
     * A removed entity instance is an instance with a persistent identity,
     * associated with an EntityManager, whose persistent state will be deleted
     * on commit.
     */
    public const STATE_REMOVED = 4;
    /**
     * Hint used to collect all primary keys of associated entities during hydration
     * and execute it in a dedicated query afterwards
     *
     * @see https://www.doctrine-project.org/projects/doctrine-orm/en/stable/reference/dql-doctrine-query-language.html#temporarily-change-fetch-mode-in-dql
     */
    public const HINT_DEFEREAGERLOAD = 'deferEagerLoad';
    /** @var bool */
    protected $hasCache = false;
    /**
     * Initializes a new UnitOfWork instance, bound to the given EntityManager.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Commits the UnitOfWork, executing all operations that have been postponed
     * up to this point. The state of all managed entities will be synchronized with
     * the database.
     *
     * The operations are executed in the following order:
     *
     * 1) All entity insertions
     * 2) All entity updates
     * 3) All collection deletions
     * 4) All collection updates
     * 5) All entity deletions
     *
     * @param object|mixed[]|null $entity
     *
     * @return void
     *
     * @throws Exception
     */
    public function commit($entity = null)
    {
    }
    /**
     * Gets the changeset for an entity.
     *
     * @param object $entity
     *
     * @return mixed[][]
     * @psalm-return array<string, array{mixed, mixed}|PersistentCollection>
     */
    public function &getEntityChangeSet($entity)
    {
    }
    /**
     * Computes the changes that happened to a single entity.
     *
     * Modifies/populates the following properties:
     *
     * {@link _originalEntityData}
     * If the entity is NEW or MANAGED but not yet fully persisted (only has an id)
     * then it was not fetched from the database and therefore we have no original
     * entity data yet. All of the current entity data is stored as the original entity data.
     *
     * {@link _entityChangeSets}
     * The changes detected on all properties of the entity are stored there.
     * A change is a tuple array where the first entry is the old value and the second
     * entry is the new value of the property. Changesets are used by persisters
     * to INSERT/UPDATE the persistent entity state.
     *
     * {@link _entityUpdates}
     * If the entity is already fully MANAGED (has been fetched from the database before)
     * and any changes to its properties are detected, then a reference to the entity is stored
     * there to mark it for an update.
     *
     * {@link _collectionDeletions}
     * If a PersistentCollection has been de-referenced in a fully MANAGED entity,
     * then this collection is marked for deletion.
     *
     * @param ClassMetadata $class  The class descriptor of the entity.
     * @param object        $entity The entity for which to compute the changes.
     * @psalm-param ClassMetadata<T> $class
     * @psalm-param T $entity
     *
     * @return void
     *
     * @template T of object
     *
     * @ignore
     */
    public function computeChangeSet(\Doctrine\ORM\Mapping\ClassMetadata $class, $entity)
    {
    }
    /**
     * Computes all the changes that have been done to entities and collections
     * since the last commit and stores these changes in the _entityChangeSet map
     * temporarily for access by the persisters, until the UoW commit is finished.
     *
     * @return void
     */
    public function computeChangeSets()
    {
    }
    /**
     * INTERNAL:
     * Computes the changeset of an individual entity, independently of the
     * computeChangeSets() routine that is used at the beginning of a UnitOfWork#commit().
     *
     * The passed entity must be a managed entity. If the entity already has a change set
     * because this method is invoked during a commit cycle then the change sets are added.
     * whereby changes detected in this method prevail.
     *
     * @param ClassMetadata $class  The class descriptor of the entity.
     * @param object        $entity The entity for which to (re)calculate the change set.
     * @psalm-param ClassMetadata<T> $class
     * @psalm-param T $entity
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException If the passed entity is not MANAGED.
     *
     * @template T of object
     * @ignore
     */
    public function recomputeSingleEntityChangeSet(\Doctrine\ORM\Mapping\ClassMetadata $class, $entity)
    {
    }
    /**
     * Schedules an entity for insertion into the database.
     * If the entity already has an identifier, it will be added to the identity map.
     *
     * @param object $entity The entity to schedule for insertion.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function scheduleForInsert($entity)
    {
    }
    /**
     * Checks whether an entity is scheduled for insertion.
     *
     * @param object $entity
     *
     * @return bool
     */
    public function isScheduledForInsert($entity)
    {
    }
    /**
     * Schedules an entity for being updated.
     *
     * @param object $entity The entity to schedule for being updated.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     */
    public function scheduleForUpdate($entity)
    {
    }
    /**
     * INTERNAL:
     * Schedules an extra update that will be executed immediately after the
     * regular entity updates within the currently running commit cycle.
     *
     * Extra updates for entities are stored as (entity, changeset) tuples.
     *
     * @param object $entity The entity for which to schedule an extra update.
     * @psalm-param array<string, array{mixed, mixed}>  $changeset The changeset of the entity (what to update).
     *
     * @return void
     *
     * @ignore
     */
    public function scheduleExtraUpdate($entity, array $changeset)
    {
    }
    /**
     * Checks whether an entity is registered as dirty in the unit of work.
     * Note: Is not very useful currently as dirty entities are only registered
     * at commit time.
     *
     * @param object $entity
     *
     * @return bool
     */
    public function isScheduledForUpdate($entity)
    {
    }
    /**
     * Checks whether an entity is registered to be checked in the unit of work.
     *
     * @param object $entity
     *
     * @return bool
     */
    public function isScheduledForDirtyCheck($entity)
    {
    }
    /**
     * INTERNAL:
     * Schedules an entity for deletion.
     *
     * @param object $entity
     *
     * @return void
     */
    public function scheduleForDelete($entity)
    {
    }
    /**
     * Checks whether an entity is registered as removed/deleted with the unit
     * of work.
     *
     * @param object $entity
     *
     * @return bool
     */
    public function isScheduledForDelete($entity)
    {
    }
    /**
     * Checks whether an entity is scheduled for insertion, update or deletion.
     *
     * @param object $entity
     *
     * @return bool
     */
    public function isEntityScheduled($entity)
    {
    }
    /**
     * INTERNAL:
     * Registers an entity in the identity map.
     * Note that entities in a hierarchy are registered with the class name of
     * the root entity.
     *
     * @param object $entity The entity to register.
     *
     * @return bool TRUE if the registration was successful, FALSE if the identity of
     * the entity in question is already managed.
     *
     * @throws ORMInvalidArgumentException
     *
     * @ignore
     */
    public function addToIdentityMap($entity)
    {
    }
    /**
     * Gets the state of an entity with regard to the current unit of work.
     *
     * @param object   $entity
     * @param int|null $assume The state to assume if the state is not yet known (not MANAGED or REMOVED).
     *                         This parameter can be set to improve performance of entity state detection
     *                         by potentially avoiding a database lookup if the distinction between NEW and DETACHED
     *                         is either known or does not matter for the caller of the method.
     * @psalm-param self::STATE_*|null $assume
     *
     * @return int The entity state.
     * @psalm-return self::STATE_*
     */
    public function getEntityState($entity, $assume = null)
    {
    }
    /**
     * INTERNAL:
     * Removes an entity from the identity map. This effectively detaches the
     * entity from the persistence management of Doctrine.
     *
     * @param object $entity
     *
     * @return bool
     *
     * @throws ORMInvalidArgumentException
     *
     * @ignore
     */
    public function removeFromIdentityMap($entity)
    {
    }
    /**
     * INTERNAL:
     * Gets an entity in the identity map by its identifier hash.
     *
     * @param string $idHash
     * @param string $rootClassName
     *
     * @return object
     *
     * @ignore
     */
    public function getByIdHash($idHash, $rootClassName)
    {
    }
    /**
     * INTERNAL:
     * Tries to get an entity by its identifier hash. If no entity is found for
     * the given hash, FALSE is returned.
     *
     * @param mixed  $idHash        (must be possible to cast it to string)
     * @param string $rootClassName
     *
     * @return false|object The found entity or FALSE.
     *
     * @ignore
     */
    public function tryGetByIdHash($idHash, $rootClassName)
    {
    }
    /**
     * Checks whether an entity is registered in the identity map of this UnitOfWork.
     *
     * @param object $entity
     *
     * @return bool
     */
    public function isInIdentityMap($entity)
    {
    }
    /**
     * INTERNAL:
     * Checks whether an identifier hash exists in the identity map.
     *
     * @param string $idHash
     * @param string $rootClassName
     *
     * @return bool
     *
     * @ignore
     */
    public function containsIdHash($idHash, $rootClassName)
    {
    }
    /**
     * Persists an entity as part of the current unit of work.
     *
     * @param object $entity The entity to persist.
     *
     * @return void
     */
    public function persist($entity)
    {
    }
    /**
     * Deletes an entity as part of the current unit of work.
     *
     * @param object $entity The entity to remove.
     *
     * @return void
     */
    public function remove($entity)
    {
    }
    /**
     * Merges the state of the given detached entity into this UnitOfWork.
     *
     * @deprecated 2.7 This method is being removed from the ORM and won't have any replacement
     *
     * @param object $entity
     *
     * @return object The managed copy of the entity.
     *
     * @throws OptimisticLockException If the entity uses optimistic locking through a version
     *         attribute and the version check against the managed copy fails.
     */
    public function merge($entity)
    {
    }
    /**
     * Detaches an entity from the persistence management. It's persistence will
     * no longer be managed by Doctrine.
     *
     * @param object $entity The entity to detach.
     *
     * @return void
     */
    public function detach($entity)
    {
    }
    /**
     * Refreshes the state of the given entity from the database, overwriting
     * any local, unpersisted changes.
     *
     * @param object $entity The entity to refresh.
     *
     * @return void
     *
     * @throws InvalidArgumentException If the entity is not MANAGED.
     */
    public function refresh($entity)
    {
    }
    /**
     * Acquire a lock on the given entity.
     *
     * @param object                     $entity
     * @param int|DateTimeInterface|null $lockVersion
     * @psalm-param LockMode::* $lockMode
     *
     * @throws ORMInvalidArgumentException
     * @throws TransactionRequiredException
     * @throws OptimisticLockException
     */
    public function lock($entity, int $lockMode, $lockVersion = null) : void
    {
    }
    /**
     * Gets the CommitOrderCalculator used by the UnitOfWork to order commits.
     *
     * @return CommitOrderCalculator
     */
    public function getCommitOrderCalculator()
    {
    }
    /**
     * Clears the UnitOfWork.
     *
     * @param string|null $entityName if given, only entities of this type will get detached.
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException if an invalid entity name is given.
     */
    public function clear($entityName = null)
    {
    }
    /**
     * INTERNAL:
     * Schedules an orphaned entity for removal. The remove() operation will be
     * invoked on that entity at the beginning of the next commit of this
     * UnitOfWork.
     *
     * @param object $entity
     *
     * @return void
     *
     * @ignore
     */
    public function scheduleOrphanRemoval($entity)
    {
    }
    /**
     * INTERNAL:
     * Cancels a previously scheduled orphan removal.
     *
     * @param object $entity
     *
     * @return void
     *
     * @ignore
     */
    public function cancelOrphanRemoval($entity)
    {
    }
    /**
     * INTERNAL:
     * Schedules a complete collection for removal when this UnitOfWork commits.
     *
     * @return void
     */
    public function scheduleCollectionDeletion(\Doctrine\ORM\PersistentCollection $coll)
    {
    }
    /**
     * @return bool
     */
    public function isCollectionScheduledForDeletion(\Doctrine\ORM\PersistentCollection $coll)
    {
    }
    /**
     * INTERNAL:
     * Creates an entity. Used for reconstitution of persistent entities.
     *
     * Internal note: Highly performance-sensitive method.
     *
     * @param string  $className The name of the entity class.
     * @param mixed[] $data      The data for the entity.
     * @param mixed[] $hints     Any hints to account for during reconstitution/lookup of the entity.
     * @psalm-param class-string $className
     * @psalm-param array<string, mixed> $hints
     *
     * @return object The managed entity instance.
     *
     * @ignore
     * @todo Rename: getOrCreateEntity
     */
    public function createEntity($className, array $data, &$hints = [])
    {
    }
    /**
     * @return void
     */
    public function triggerEagerLoads()
    {
    }
    /**
     * Initializes (loads) an uninitialized persistent collection of an entity.
     *
     * @param PersistentCollection $collection The collection to initialize.
     *
     * @return void
     *
     * @todo Maybe later move to EntityManager#initialize($proxyOrCollection). See DDC-733.
     */
    public function loadCollection(\Doctrine\ORM\PersistentCollection $collection)
    {
    }
    /**
     * Gets the identity map of the UnitOfWork.
     *
     * @psalm-return array<class-string, array<string, object|null>>
     */
    public function getIdentityMap()
    {
    }
    /**
     * Gets the original data of an entity. The original data is the data that was
     * present at the time the entity was reconstituted from the database.
     *
     * @param object $entity
     *
     * @return mixed[]
     * @psalm-return array<string, mixed>
     */
    public function getOriginalEntityData($entity)
    {
    }
    /**
     * @param object  $entity
     * @param mixed[] $data
     *
     * @return void
     *
     * @ignore
     */
    public function setOriginalEntityData($entity, array $data)
    {
    }
    /**
     * INTERNAL:
     * Sets a property value of the original data array of an entity.
     *
     * @param int    $oid
     * @param string $property
     * @param mixed  $value
     *
     * @return void
     *
     * @ignore
     */
    public function setOriginalEntityProperty($oid, $property, $value)
    {
    }
    /**
     * Gets the identifier of an entity.
     * The returned value is always an array of identifier values. If the entity
     * has a composite identifier then the identifier values are in the same
     * order as the identifier field names as returned by ClassMetadata#getIdentifierFieldNames().
     *
     * @param object $entity
     *
     * @return mixed[] The identifier values.
     */
    public function getEntityIdentifier($entity)
    {
    }
    /**
     * Processes an entity instance to extract their identifier values.
     *
     * @param object $entity The entity instance.
     *
     * @return mixed A scalar value.
     *
     * @throws ORMInvalidArgumentException
     */
    public function getSingleIdentifierValue($entity)
    {
    }
    /**
     * Tries to find an entity with the given identifier in the identity map of
     * this UnitOfWork.
     *
     * @param mixed  $id            The entity identifier to look for.
     * @param string $rootClassName The name of the root class of the mapped entity hierarchy.
     * @psalm-param class-string $rootClassName
     *
     * @return object|false Returns the entity with the specified identifier if it exists in
     *                      this UnitOfWork, FALSE otherwise.
     */
    public function tryGetById($id, $rootClassName)
    {
    }
    /**
     * Schedules an entity for dirty-checking at commit-time.
     *
     * @param object $entity The entity to schedule for dirty-checking.
     *
     * @return void
     *
     * @todo Rename: scheduleForSynchronization
     */
    public function scheduleForDirtyCheck($entity)
    {
    }
    /**
     * Checks whether the UnitOfWork has any pending insertions.
     *
     * @return bool TRUE if this UnitOfWork has pending insertions, FALSE otherwise.
     */
    public function hasPendingInsertions()
    {
    }
    /**
     * Calculates the size of the UnitOfWork. The size of the UnitOfWork is the
     * number of entities in the identity map.
     *
     * @return int
     */
    public function size()
    {
    }
    /**
     * Gets the EntityPersister for an Entity.
     *
     * @param string $entityName The name of the Entity.
     * @psalm-param class-string $entityName
     *
     * @return EntityPersister
     */
    public function getEntityPersister($entityName)
    {
    }
    /**
     * Gets a collection persister for a collection-valued association.
     *
     * @psalm-param array<string, mixed> $association
     *
     * @return CollectionPersister
     */
    public function getCollectionPersister(array $association)
    {
    }
    /**
     * INTERNAL:
     * Registers an entity as managed.
     *
     * @param object  $entity The entity.
     * @param mixed[] $id     The identifier values.
     * @param mixed[] $data   The original entity data.
     *
     * @return void
     */
    public function registerManaged($entity, array $id, array $data)
    {
    }
    /**
     * INTERNAL:
     * Clears the property changeset of the entity with the given OID.
     *
     * @param int $oid The entity's OID.
     *
     * @return void
     */
    public function clearEntityChangeSet($oid)
    {
    }
    /* PropertyChangedListener implementation */
    /**
     * Notifies this UnitOfWork of a property change in an entity.
     *
     * @param object $sender       The entity that owns the property.
     * @param string $propertyName The name of the property that changed.
     * @param mixed  $oldValue     The old value of the property.
     * @param mixed  $newValue     The new value of the property.
     *
     * @return void
     */
    public function propertyChanged($sender, $propertyName, $oldValue, $newValue)
    {
    }
    /**
     * Gets the currently scheduled entity insertions in this UnitOfWork.
     *
     * @psalm-return array<int, object>
     */
    public function getScheduledEntityInsertions()
    {
    }
    /**
     * Gets the currently scheduled entity updates in this UnitOfWork.
     *
     * @psalm-return array<int, object>
     */
    public function getScheduledEntityUpdates()
    {
    }
    /**
     * Gets the currently scheduled entity deletions in this UnitOfWork.
     *
     * @psalm-return array<int, object>
     */
    public function getScheduledEntityDeletions()
    {
    }
    /**
     * Gets the currently scheduled complete collection deletions
     *
     * @psalm-return array<int, Collection<array-key, object>>
     */
    public function getScheduledCollectionDeletions()
    {
    }
    /**
     * Gets the currently scheduled collection inserts, updates and deletes.
     *
     * @psalm-return array<int, Collection<array-key, object>>
     */
    public function getScheduledCollectionUpdates()
    {
    }
    /**
     * Helper method to initialize a lazy loading proxy or persistent collection.
     *
     * @param object $obj
     *
     * @return void
     */
    public function initializeObject($obj)
    {
    }
    /**
     * Marks an entity as read-only so that it will not be considered for updates during UnitOfWork#commit().
     *
     * This operation cannot be undone as some parts of the UnitOfWork now keep gathering information
     * on this object that might be necessary to perform a correct update.
     *
     * @param object $object
     *
     * @return void
     *
     * @throws ORMInvalidArgumentException
     */
    public function markReadOnly($object)
    {
    }
    /**
     * Is this entity read only?
     *
     * @param object $object
     *
     * @return bool
     *
     * @throws ORMInvalidArgumentException
     */
    public function isReadOnly($object)
    {
    }
    /**
     * This method called by hydrators, and indicates that hydrator totally completed current hydration cycle.
     * Unit of work able to fire deferred events, related to loading events here.
     *
     * @internal should be called internally from object hydrators
     *
     * @return void
     */
    public function hydrationComplete()
    {
    }
}
