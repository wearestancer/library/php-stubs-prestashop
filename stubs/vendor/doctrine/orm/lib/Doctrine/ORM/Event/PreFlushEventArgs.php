<?php

namespace Doctrine\ORM\Event;

/**
 * Provides event arguments for the preFlush event.
 *
 * @link        www.doctrine-project.com
 */
class PreFlushEventArgs extends \Doctrine\Common\EventArgs
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
}
