<?php

namespace Doctrine\ORM\Event;

/**
 * Lifecycle Events are triggered by the UnitOfWork during lifecycle transitions
 * of entities.
 *
 * @extends BaseLifecycleEventArgs<EntityManagerInterface>
 */
class LifecycleEventArgs extends \Doctrine\Persistence\Event\LifecycleEventArgs
{
    /**
     * Retrieves associated Entity.
     *
     * @return object
     */
    public function getEntity()
    {
    }
    /**
     * Retrieves associated EntityManager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
}
