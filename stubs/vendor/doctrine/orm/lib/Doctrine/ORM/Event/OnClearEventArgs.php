<?php

namespace Doctrine\ORM\Event;

/**
 * Provides event arguments for the onClear event.
 *
 * @link        www.doctrine-project.org
 */
class OnClearEventArgs extends \Doctrine\Common\EventArgs
{
    /**
     * @param string|null $entityClass Optional entity class.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, $entityClass = null)
    {
    }
    /**
     * Retrieves associated EntityManager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
    /**
     * Name of the entity class that is cleared, or empty if all are cleared.
     *
     * @deprecated Clearing the entity manager partially is deprecated. This method will be removed in 3.0.
     *
     * @return string|null
     */
    public function getEntityClass()
    {
    }
    /**
     * Checks if event clears all entities.
     *
     * @deprecated Clearing the entity manager partially is deprecated. This method will be removed in 3.0.
     *
     * @return bool
     */
    public function clearsAllEntities()
    {
    }
}
