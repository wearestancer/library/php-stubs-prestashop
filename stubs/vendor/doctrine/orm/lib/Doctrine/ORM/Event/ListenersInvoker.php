<?php

namespace Doctrine\ORM\Event;

/**
 * A method invoker based on entity lifecycle.
 */
class ListenersInvoker
{
    public const INVOKE_NONE = 0;
    public const INVOKE_LISTENERS = 1;
    public const INVOKE_CALLBACKS = 2;
    public const INVOKE_MANAGER = 4;
    /**
     * Initializes a new ListenersInvoker instance.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Get the subscribed event systems
     *
     * @param ClassMetadata $metadata  The entity metadata.
     * @param string        $eventName The entity lifecycle event.
     *
     * @return int Bitmask of subscribed event systems.
     */
    public function getSubscribedSystems(\Doctrine\ORM\Mapping\ClassMetadata $metadata, $eventName)
    {
    }
    /**
     * Dispatches the lifecycle event of the given entity.
     *
     * @param ClassMetadata $metadata  The entity metadata.
     * @param string        $eventName The entity lifecycle event.
     * @param object        $entity    The Entity on which the event occurred.
     * @param EventArgs     $event     The Event args.
     * @param int           $invoke    Bitmask to invoke listeners.
     *
     * @return void
     */
    public function invoke(\Doctrine\ORM\Mapping\ClassMetadata $metadata, $eventName, $entity, \Doctrine\Common\EventArgs $event, $invoke)
    {
    }
}
