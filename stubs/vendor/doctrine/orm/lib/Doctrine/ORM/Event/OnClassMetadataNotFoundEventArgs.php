<?php

namespace Doctrine\ORM\Event;

/**
 * Class that holds event arguments for a `onClassMetadataNotFound` event.
 *
 * This object is mutable by design, allowing callbacks having access to it to set the
 * found metadata in it, and therefore "cancelling" a `onClassMetadataNotFound` event
 *
 * @extends ManagerEventArgs<EntityManagerInterface>
 */
class OnClassMetadataNotFoundEventArgs extends \Doctrine\Persistence\Event\ManagerEventArgs
{
    /**
     * @param string                 $className
     * @param EntityManagerInterface $objectManager
     */
    public function __construct($className, \Doctrine\Persistence\ObjectManager $objectManager)
    {
    }
    /**
     * @return void
     */
    public function setFoundMetadata(?\Doctrine\Persistence\Mapping\ClassMetadata $classMetadata = null)
    {
    }
    /**
     * @return ClassMetadata|null
     */
    public function getFoundMetadata()
    {
    }
    /**
     * Retrieve class name for which a failed metadata fetch attempt was executed
     *
     * @return string
     */
    public function getClassName()
    {
    }
}
