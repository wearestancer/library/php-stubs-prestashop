<?php

namespace Doctrine\ORM\Event;

/**
 * Provides event arguments for the postFlush event.
 *
 * @link        www.doctrine-project.org
 */
class PostFlushEventArgs extends \Doctrine\Common\EventArgs
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Retrieves associated EntityManager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
}
