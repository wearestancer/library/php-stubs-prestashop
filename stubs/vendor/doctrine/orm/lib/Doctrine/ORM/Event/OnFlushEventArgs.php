<?php

namespace Doctrine\ORM\Event;

/**
 * Provides event arguments for the preFlush event.
 *
 * @link        www.doctrine-project.org
 */
class OnFlushEventArgs extends \Doctrine\Common\EventArgs
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
    }
    /**
     * Retrieve associated EntityManager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
    }
}
