<?php

namespace Doctrine\ORM\Event;

/**
 * Class that holds event arguments for a preInsert/preUpdate event.
 */
class PreUpdateEventArgs extends \Doctrine\ORM\Event\LifecycleEventArgs
{
    /**
     * @param object    $entity
     * @param mixed[][] $changeSet
     * @psalm-param array<string, array{mixed, mixed}|PersistentCollection> $changeSet
     */
    public function __construct($entity, \Doctrine\ORM\EntityManagerInterface $em, array &$changeSet)
    {
    }
    /**
     * Retrieves entity changeset.
     *
     * @return mixed[][]
     * @psalm-return array<string, array{mixed, mixed}|PersistentCollection>
     */
    public function getEntityChangeSet()
    {
    }
    /**
     * Checks if field has a changeset.
     *
     * @param string $field
     *
     * @return bool
     */
    public function hasChangedField($field)
    {
    }
    /**
     * Gets the old value of the changeset of the changed field.
     *
     * @param string $field
     *
     * @return mixed
     */
    public function getOldValue($field)
    {
    }
    /**
     * Gets the new value of the changeset of the changed field.
     *
     * @param string $field
     *
     * @return mixed
     */
    public function getNewValue($field)
    {
    }
    /**
     * Sets the new value of this field.
     *
     * @param string $field
     * @param mixed  $value
     *
     * @return void
     */
    public function setNewValue($field, $value)
    {
    }
}
