<?php

namespace Doctrine\ORM;

/**
 * Exception thrown when an ORM query unexpectedly does not return any results.
 */
class NoResultException extends \Doctrine\ORM\UnexpectedResultException
{
    public function __construct()
    {
    }
}
