<?php

namespace Doctrine\Common\Annotations;

/**
 * Parses a file for namespaces/use/class declarations.
 */
final class PhpParser
{
    /**
     * Parses a class.
     *
     * @deprecated use parseUseStatements instead
     *
     * @param ReflectionClass $class A <code>ReflectionClass</code> object.
     *
     * @return array<string, class-string> A list with use statements in the form (Alias => FQN).
     */
    public function parseClass(\ReflectionClass $class)
    {
    }
    /**
     * Parse a class or function for use statements.
     *
     * @param ReflectionClass|ReflectionFunction $reflection
     *
     * @psalm-return array<string, string> a list with use statements in the form (Alias => FQN).
     */
    public function parseUseStatements($reflection) : array
    {
    }
}
