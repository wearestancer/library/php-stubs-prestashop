<?php

namespace Doctrine\Common\Annotations;

/**
 * Description of AnnotationException
 */
class AnnotationException extends \Exception
{
    /**
     * Creates a new AnnotationException describing a Syntax error.
     *
     * @param string $message Exception message
     *
     * @return AnnotationException
     */
    public static function syntaxError($message)
    {
    }
    /**
     * Creates a new AnnotationException describing a Semantical error.
     *
     * @param string $message Exception message
     *
     * @return AnnotationException
     */
    public static function semanticalError($message)
    {
    }
    /**
     * Creates a new AnnotationException describing an error which occurred during
     * the creation of the annotation.
     *
     * @param string $message
     *
     * @return AnnotationException
     */
    public static function creationError($message, ?\Throwable $previous = null)
    {
    }
    /**
     * Creates a new AnnotationException describing a type error.
     *
     * @param string $message
     *
     * @return AnnotationException
     */
    public static function typeError($message)
    {
    }
    /**
     * Creates a new AnnotationException describing a constant semantical error.
     *
     * @param string $identifier
     * @param string $context
     *
     * @return AnnotationException
     */
    public static function semanticalErrorConstants($identifier, $context = null)
    {
    }
    /**
     * Creates a new AnnotationException describing an type error of an attribute.
     *
     * @param string $attributeName
     * @param string $annotationName
     * @param string $context
     * @param string $expected
     * @param mixed  $actual
     *
     * @return AnnotationException
     */
    public static function attributeTypeError($attributeName, $annotationName, $context, $expected, $actual)
    {
    }
    /**
     * Creates a new AnnotationException describing an required error of an attribute.
     *
     * @param string $attributeName
     * @param string $annotationName
     * @param string $context
     * @param string $expected
     *
     * @return AnnotationException
     */
    public static function requiredError($attributeName, $annotationName, $context, $expected)
    {
    }
    /**
     * Creates a new AnnotationException describing a invalid enummerator.
     *
     * @param string $attributeName
     * @param string $annotationName
     * @param string $context
     * @param mixed  $given
     * @phpstan-param list<string>        $available
     *
     * @return AnnotationException
     */
    public static function enumeratorError($attributeName, $annotationName, $context, $available, $given)
    {
    }
    /** @return AnnotationException */
    public static function optimizerPlusSaveComments()
    {
    }
    /** @return AnnotationException */
    public static function optimizerPlusLoadComments()
    {
    }
}
