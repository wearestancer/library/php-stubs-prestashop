<?php

namespace Doctrine\Common\Annotations\Annotation;

/**
 * Annotation that can be used to signal to the parser
 * to check the annotation target during the parsing process.
 *
 * @Annotation
 */
final class Target
{
    public const TARGET_CLASS = 1;
    public const TARGET_METHOD = 2;
    public const TARGET_PROPERTY = 4;
    public const TARGET_ANNOTATION = 8;
    public const TARGET_FUNCTION = 16;
    public const TARGET_ALL = 31;
    /** @phpstan-var list<string> */
    public $value;
    /**
     * Targets as bitmask.
     *
     * @var int
     */
    public $targets;
    /**
     * Literal target declaration.
     *
     * @var string
     */
    public $literal;
    /**
     * @phpstan-param array{value?: string|list<string>} $values
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $values)
    {
    }
}
