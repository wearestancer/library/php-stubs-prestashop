<?php

namespace Doctrine\Common\Annotations;

/**
 * A parser for docblock annotations.
 *
 * It is strongly discouraged to change the default annotation parsing process.
 */
final class DocParser
{
    /**
     * Constructs a new DocParser.
     */
    public function __construct()
    {
    }
    /**
     * Sets the annotation names that are ignored during the parsing process.
     *
     * The names are supposed to be the raw names as used in the class, not the
     * fully qualified class names.
     *
     * @param bool[] $names indexed by annotation name
     *
     * @return void
     */
    public function setIgnoredAnnotationNames(array $names)
    {
    }
    /**
     * Sets the annotation namespaces that are ignored during the parsing process.
     *
     * @param bool[] $ignoredAnnotationNamespaces indexed by annotation namespace name
     *
     * @return void
     */
    public function setIgnoredAnnotationNamespaces($ignoredAnnotationNamespaces)
    {
    }
    /**
     * Sets ignore on not-imported annotations.
     *
     * @param bool $bool
     *
     * @return void
     */
    public function setIgnoreNotImportedAnnotations($bool)
    {
    }
    /**
     * Sets the default namespaces.
     *
     * @param string $namespace
     *
     * @return void
     *
     * @throws RuntimeException
     */
    public function addNamespace($namespace)
    {
    }
    /**
     * Sets the imports.
     *
     * @param array<string, class-string> $imports
     *
     * @return void
     *
     * @throws RuntimeException
     */
    public function setImports(array $imports)
    {
    }
    /**
     * Sets current target context as bitmask.
     *
     * @param int $target
     *
     * @return void
     */
    public function setTarget($target)
    {
    }
    /**
     * Parses the given docblock string for annotations.
     *
     * @param string $input   The docblock string to parse.
     * @param string $context The parsing context.
     *
     * @phpstan-return list<object> Array of annotations. If no annotations are found, an empty array is returned.
     *
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public function parse($input, $context = '')
    {
    }
}
