<?php

namespace Doctrine\Common\Annotations\Annotation;

/**
 * Annotation that can be used to signal to the parser
 * to check the available values during the parsing process.
 *
 * @Annotation
 * @Attributes({
 *    @Attribute("value",   required = true,  type = "array"),
 *    @Attribute("literal", required = false, type = "array")
 * })
 */
final class Enum
{
    /** @phpstan-var list<scalar> */
    public $value;
    /**
     * Literal target declaration.
     *
     * @var mixed[]
     */
    public $literal;
    /**
     * @phpstan-param array{literal?: mixed[], value: list<scalar>} $values
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $values)
    {
    }
}
