<?php

namespace Doctrine\Common\Annotations;

final class AnnotationRegistry
{
    public static function reset() : void
    {
    }
    /**
     * Registers file.
     *
     * @deprecated This method is deprecated and will be removed in
     *             doctrine/annotations 2.0. Annotations will be autoloaded in 2.0.
     */
    public static function registerFile(string $file) : void
    {
    }
    /**
     * Adds a namespace with one or many directories to look for files or null for the include path.
     *
     * Loading of this namespaces will be done with a PSR-0 namespace loading algorithm.
     *
     * @deprecated This method is deprecated and will be removed in
     *             doctrine/annotations 2.0. Annotations will be autoloaded in 2.0.
     *
     * @phpstan-param string|list<string>|null $dirs
     */
    public static function registerAutoloadNamespace(string $namespace, $dirs = null) : void
    {
    }
    /**
     * Registers multiple namespaces.
     *
     * Loading of this namespaces will be done with a PSR-0 namespace loading algorithm.
     *
     * @deprecated This method is deprecated and will be removed in
     *             doctrine/annotations 2.0. Annotations will be autoloaded in 2.0.
     *
     * @param string[][]|string[]|null[] $namespaces indexed by namespace name
     */
    public static function registerAutoloadNamespaces(array $namespaces) : void
    {
    }
    /**
     * Registers an autoloading callable for annotations, much like spl_autoload_register().
     *
     * NOTE: These class loaders HAVE to be silent when a class was not found!
     * IMPORTANT: Loaders have to return true if they loaded a class that could contain the searched annotation class.
     *
     * @deprecated This method is deprecated and will be removed in
     *             doctrine/annotations 2.0. Annotations will be autoloaded in 2.0.
     */
    public static function registerLoader(callable $callable) : void
    {
    }
    /**
     * Registers an autoloading callable for annotations, if it is not already registered
     *
     * @deprecated This method is deprecated and will be removed in
     *             doctrine/annotations 2.0. Annotations will be autoloaded in 2.0.
     */
    public static function registerUniqueLoader(callable $callable) : void
    {
    }
    /**
     * Autoloads an annotation class silently.
     */
    public static function loadAnnotationClass(string $class) : bool
    {
    }
}
