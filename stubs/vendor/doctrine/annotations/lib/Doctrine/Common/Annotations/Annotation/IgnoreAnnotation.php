<?php

namespace Doctrine\Common\Annotations\Annotation;

/**
 * Annotation that can be used to signal to the parser to ignore specific
 * annotations during the parsing process.
 *
 * @Annotation
 */
final class IgnoreAnnotation
{
    /** @phpstan-var list<string> */
    public $names;
    /**
     * @phpstan-param array{value: string|list<string>} $values
     *
     * @throws RuntimeException
     */
    public function __construct(array $values)
    {
    }
}
