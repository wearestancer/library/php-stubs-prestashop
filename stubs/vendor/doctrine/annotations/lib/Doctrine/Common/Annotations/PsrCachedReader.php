<?php

namespace Doctrine\Common\Annotations;

/**
 * A cache aware annotation reader.
 */
final class PsrCachedReader implements \Doctrine\Common\Annotations\Reader
{
    public function __construct(\Doctrine\Common\Annotations\Reader $reader, \Psr\Cache\CacheItemPoolInterface $cache, bool $debug = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassAnnotations(\ReflectionClass $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassAnnotation(\ReflectionClass $class, $annotationName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPropertyAnnotations(\ReflectionProperty $property)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPropertyAnnotation(\ReflectionProperty $property, $annotationName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getMethodAnnotations(\ReflectionMethod $method)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getMethodAnnotation(\ReflectionMethod $method, $annotationName)
    {
    }
    public function clearLoadedAnnotations() : void
    {
    }
}
