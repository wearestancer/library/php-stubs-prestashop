<?php

namespace Doctrine\Common\Annotations;

/**
 * File cache reader for annotations.
 *
 * @deprecated the FileCacheReader is deprecated and will be removed
 *             in version 2.0.0 of doctrine/annotations. Please use the
 *             {@see \Doctrine\Common\Annotations\PsrCachedReader} instead.
 */
class FileCacheReader implements \Doctrine\Common\Annotations\Reader
{
    /**
     * @param string $cacheDir
     * @param bool   $debug
     * @param int    $umask
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Doctrine\Common\Annotations\Reader $reader, $cacheDir, $debug = false, $umask = 02)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassAnnotations(\ReflectionClass $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPropertyAnnotations(\ReflectionProperty $property)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getMethodAnnotations(\ReflectionMethod $method)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassAnnotation(\ReflectionClass $class, $annotationName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getMethodAnnotation(\ReflectionMethod $method, $annotationName)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPropertyAnnotation(\ReflectionProperty $property, $annotationName)
    {
    }
    /**
     * Clears loaded annotations.
     *
     * @return void
     */
    public function clearLoadedAnnotations()
    {
    }
}
