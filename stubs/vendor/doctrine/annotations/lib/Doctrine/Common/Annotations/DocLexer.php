<?php

namespace Doctrine\Common\Annotations;

/**
 * Simple lexer for docblock annotations.
 *
 * @template-extends AbstractLexer<DocLexer::T_*>
 */
final class DocLexer extends \Doctrine\Common\Lexer\AbstractLexer
{
    public const T_NONE = 1;
    public const T_INTEGER = 2;
    public const T_STRING = 3;
    public const T_FLOAT = 4;
    // All tokens that are also identifiers should be >= 100
    public const T_IDENTIFIER = 100;
    public const T_AT = 101;
    public const T_CLOSE_CURLY_BRACES = 102;
    public const T_CLOSE_PARENTHESIS = 103;
    public const T_COMMA = 104;
    public const T_EQUALS = 105;
    public const T_FALSE = 106;
    public const T_NAMESPACE_SEPARATOR = 107;
    public const T_OPEN_CURLY_BRACES = 108;
    public const T_OPEN_PARENTHESIS = 109;
    public const T_TRUE = 110;
    public const T_NULL = 111;
    public const T_COLON = 112;
    public const T_MINUS = 113;
    /**
     * Whether the next token starts immediately, or if there were
     * non-captured symbols before that
     */
    public function nextTokenIsAdjacent() : bool
    {
    }
    /** @return array{value: int|string, type:self::T_*|null, position:int} */
    public function peek() : ?array
    {
    }
}
