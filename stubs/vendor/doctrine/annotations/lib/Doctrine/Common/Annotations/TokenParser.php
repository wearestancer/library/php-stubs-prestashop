<?php

namespace Doctrine\Common\Annotations;

/**
 * Parses a file for namespaces/use/class declarations.
 */
class TokenParser
{
    /** @param string $contents */
    public function __construct($contents)
    {
    }
    /**
     * Gets the next non whitespace and non comment token.
     *
     * @param bool $docCommentIsComment If TRUE then a doc comment is considered a comment and skipped.
     * If FALSE then only whitespace and normal comments are skipped.
     *
     * @return mixed[]|string|null The token if exists, null otherwise.
     */
    public function next($docCommentIsComment = true)
    {
    }
    /**
     * Parses a single use statement.
     *
     * @return array<string, string> A list with all found class names for a use statement.
     */
    public function parseUseStatement()
    {
    }
    /**
     * Gets all use statements.
     *
     * @param string $namespaceName The namespace name of the reflected class.
     *
     * @return array<string, string> A list with all found use statements.
     */
    public function parseUseStatements($namespaceName)
    {
    }
    /**
     * Gets the namespace.
     *
     * @return string The found namespace.
     */
    public function parseNamespace()
    {
    }
    /**
     * Gets the class name.
     *
     * @return string The found class name.
     */
    public function parseClass()
    {
    }
}
