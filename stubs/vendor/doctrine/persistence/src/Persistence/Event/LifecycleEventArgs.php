<?php

namespace Doctrine\Persistence\Event;

/**
 * Lifecycle Events are triggered by the UnitOfWork during lifecycle transitions
 * of entities.
 *
 * @template-covariant TObjectManager of ObjectManager
 */
class LifecycleEventArgs extends \Doctrine\Common\EventArgs
{
    /** @psalm-param TObjectManager $objectManager */
    public function __construct(object $object, \Doctrine\Persistence\ObjectManager $objectManager)
    {
    }
    /**
     * Retrieves the associated object.
     *
     * @return object
     */
    public function getObject()
    {
    }
    /**
     * Retrieves the associated ObjectManager.
     *
     * @return ObjectManager
     * @psalm-return TObjectManager
     */
    public function getObjectManager()
    {
    }
}
