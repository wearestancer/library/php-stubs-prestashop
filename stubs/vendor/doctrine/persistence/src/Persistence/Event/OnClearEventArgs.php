<?php

namespace Doctrine\Persistence\Event;

/**
 * Provides event arguments for the onClear event.
 *
 * @template-covariant TObjectManager of ObjectManager
 */
class OnClearEventArgs extends \Doctrine\Common\EventArgs
{
    /**
     * @param ObjectManager $objectManager The object manager.
     * @psalm-param TObjectManager $objectManager
     */
    public function __construct(\Doctrine\Persistence\ObjectManager $objectManager)
    {
    }
    /**
     * Retrieves the associated ObjectManager.
     *
     * @return ObjectManager
     * @psalm-return TObjectManager
     */
    public function getObjectManager()
    {
    }
}
