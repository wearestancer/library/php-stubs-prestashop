<?php

namespace Doctrine\Persistence\Event;

/**
 * Provides event arguments for the preFlush event.
 *
 * @template-covariant TObjectManager of ObjectManager
 */
class ManagerEventArgs extends \Doctrine\Common\EventArgs
{
    /** @psalm-param TObjectManager $objectManager */
    public function __construct(\Doctrine\Persistence\ObjectManager $objectManager)
    {
    }
    /**
     * Retrieves the associated ObjectManager.
     *
     * @return ObjectManager
     * @psalm-return TObjectManager
     */
    public function getObjectManager()
    {
    }
}
