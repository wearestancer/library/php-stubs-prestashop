<?php

namespace Doctrine\Persistence\Event;

/**
 * Class that holds event arguments for a preUpdate event.
 *
 * @template-covariant TObjectManager of ObjectManager
 * @extends LifecycleEventArgs<TObjectManager>
 */
class PreUpdateEventArgs extends \Doctrine\Persistence\Event\LifecycleEventArgs
{
    /**
     * @param array<string, array<int, mixed>> $changeSet
     * @psalm-param TObjectManager $objectManager
     */
    public function __construct(object $entity, \Doctrine\Persistence\ObjectManager $objectManager, array &$changeSet)
    {
    }
    /**
     * Retrieves the entity changeset.
     *
     * @return array<string, array<int, mixed>>
     */
    public function getEntityChangeSet()
    {
    }
    /**
     * Checks if field has a changeset.
     *
     * @return bool
     */
    public function hasChangedField(string $field)
    {
    }
    /**
     * Gets the old value of the changeset of the changed field.
     *
     * @return mixed
     */
    public function getOldValue(string $field)
    {
    }
    /**
     * Gets the new value of the changeset of the changed field.
     *
     * @return mixed
     */
    public function getNewValue(string $field)
    {
    }
    /**
     * Sets the new value of this field.
     *
     * @param mixed $value
     *
     * @return void
     */
    public function setNewValue(string $field, $value)
    {
    }
}
