<?php

namespace Doctrine\Persistence\Event;

/**
 * Class that holds event arguments for a loadMetadata event.
 *
 * @template-covariant TClassMetadata of ClassMetadata<object>
 * @template-covariant TObjectManager of ObjectManager
 */
class LoadClassMetadataEventArgs extends \Doctrine\Common\EventArgs
{
    /**
     * @psalm-param TClassMetadata $classMetadata
     * @psalm-param TObjectManager $objectManager
     */
    public function __construct(\Doctrine\Persistence\Mapping\ClassMetadata $classMetadata, \Doctrine\Persistence\ObjectManager $objectManager)
    {
    }
    /**
     * Retrieves the associated ClassMetadata.
     *
     * @return ClassMetadata
     * @psalm-return TClassMetadata
     */
    public function getClassMetadata()
    {
    }
    /**
     * Retrieves the associated ObjectManager.
     *
     * @return TObjectManager
     */
    public function getObjectManager()
    {
    }
}
