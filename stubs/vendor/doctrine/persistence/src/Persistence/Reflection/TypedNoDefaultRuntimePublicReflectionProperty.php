<?php

namespace Doctrine\Persistence\Reflection;

/**
 * PHP Typed No Default Runtime Public Reflection Property - special override for public typed properties without a default value.
 *
 * @deprecated since version 3.1, use TypedNoDefaultReflectionProperty instead.
 */
class TypedNoDefaultRuntimePublicReflectionProperty extends \Doctrine\Persistence\Reflection\RuntimePublicReflectionProperty
{
    use \Doctrine\Persistence\Reflection\TypedNoDefaultReflectionPropertyBase;
}
