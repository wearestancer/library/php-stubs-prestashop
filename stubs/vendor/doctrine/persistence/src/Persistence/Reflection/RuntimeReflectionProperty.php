<?php

namespace Doctrine\Persistence\Reflection;

/**
 * PHP Runtime Reflection Property.
 *
 * Avoids triggering lazy loading if the provided object
 * is a {@see \Doctrine\Persistence\Proxy}.
 */
class RuntimeReflectionProperty extends \ReflectionProperty
{
    public function __construct(string $class, string $name)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function getValue($object = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @param object|null $object
     * @param mixed       $value
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function setValue($object, $value = null)
    {
    }
}
