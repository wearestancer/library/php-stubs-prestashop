<?php

namespace Doctrine\Persistence\Reflection;

/**
 * PHP Runtime Reflection Public Property - special overrides for public properties.
 *
 * @deprecated since version 3.1, use RuntimeReflectionProperty instead.
 */
class RuntimePublicReflectionProperty extends \ReflectionProperty
{
    /**
     * {@inheritDoc}
     *
     * Returns the value of a public property without calling
     * `__get` on the provided $object if it exists.
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function getValue($object = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Avoids triggering lazy loading via `__set` if the provided object
     * is a {@see \Doctrine\Common\Proxy\Proxy}.
     *
     * @link https://bugs.php.net/bug.php?id=63463
     *
     * @param object|null $object
     * @param mixed       $value
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function setValue($object, $value = null)
    {
    }
}
