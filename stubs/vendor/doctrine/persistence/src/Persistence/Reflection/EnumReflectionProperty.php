<?php

namespace Doctrine\Persistence\Reflection;

/**
 * PHP Enum Reflection Property - special override for backed enums.
 */
class EnumReflectionProperty extends \ReflectionProperty
{
    /** @param class-string<BackedEnum> $enumType */
    public function __construct(\ReflectionProperty $originalReflectionProperty, string $enumType)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Converts enum instance to its value.
     *
     * @param object|null $object
     *
     * @return int|string|null
     */
    #[\ReturnTypeWillChange]
    public function getValue($object = null)
    {
    }
    /**
     * Converts enum value to enum instance.
     *
     * @param object $object
     * @param mixed  $value
     */
    public function setValue($object, $value = null) : void
    {
    }
}
