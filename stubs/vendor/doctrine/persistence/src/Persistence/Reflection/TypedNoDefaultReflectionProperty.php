<?php

namespace Doctrine\Persistence\Reflection;

/**
 * PHP Typed No Default Reflection Property - special override for typed properties without a default value.
 */
class TypedNoDefaultReflectionProperty extends \Doctrine\Persistence\Reflection\RuntimeReflectionProperty
{
    use \Doctrine\Persistence\Reflection\TypedNoDefaultReflectionPropertyBase;
}
