<?php

namespace Doctrine\Persistence;

/**
 * Abstract implementation of the ManagerRegistry contract.
 */
abstract class AbstractManagerRegistry implements \Doctrine\Persistence\ManagerRegistry
{
    /**
     * @param array<string, string> $connections
     * @param array<string, string> $managers
     * @psalm-param class-string $proxyInterfaceName
     */
    public function __construct(string $name, array $connections, array $managers, string $defaultConnection, string $defaultManager, string $proxyInterfaceName)
    {
    }
    /**
     * Fetches/creates the given services.
     *
     * A service in this context is connection or a manager instance.
     *
     * @param string $name The name of the service.
     *
     * @return ObjectManager The instance of the given service.
     */
    protected abstract function getService(string $name);
    /**
     * Resets the given services.
     *
     * A service in this context is connection or a manager instance.
     *
     * @param string $name The name of the service.
     *
     * @return void
     */
    protected abstract function resetService(string $name);
    /**
     * Gets the name of the registry.
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConnection(?string $name = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConnectionNames()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConnections()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultConnectionName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultManagerName()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function getManager(?string $name = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getManagerForClass(string $class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getManagerNames()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getManagers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRepository(string $persistentObject, ?string $persistentManagerName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetManager(?string $name = null)
    {
    }
}
