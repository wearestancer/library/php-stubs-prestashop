<?php

namespace Doctrine\Persistence;

/**
 * Base class to simplify ObjectManager decorators
 *
 * @template-covariant TObjectManager of ObjectManager
 */
abstract class ObjectManagerDecorator implements \Doctrine\Persistence\ObjectManager
{
    /** @var TObjectManager */
    protected $wrapped;
    /**
     * {@inheritdoc}
     */
    public function find(string $className, $id)
    {
    }
    public function persist(object $object)
    {
    }
    public function remove(object $object)
    {
    }
    public function clear() : void
    {
    }
    public function detach(object $object)
    {
    }
    public function refresh(object $object)
    {
    }
    public function flush()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRepository(string $className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassMetadata(string $className)
    {
    }
    /** @psalm-return ClassMetadataFactory<ClassMetadata<object>> */
    public function getMetadataFactory()
    {
    }
    public function initializeObject(object $obj)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function contains(object $object)
    {
    }
}
