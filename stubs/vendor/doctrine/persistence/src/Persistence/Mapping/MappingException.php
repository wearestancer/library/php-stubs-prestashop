<?php

namespace Doctrine\Persistence\Mapping;

/**
 * A MappingException indicates that something is wrong with the mapping setup.
 */
class MappingException extends \Exception
{
    /**
     * @param array<int, string> $namespaces
     *
     * @return self
     */
    public static function classNotFoundInNamespaces(string $className, array $namespaces)
    {
    }
    /** @param class-string $driverClassName */
    public static function pathRequiredForDriver(string $driverClassName) : self
    {
    }
    /** @return self */
    public static function fileMappingDriversRequireConfiguredDirectoryPath(?string $path = null)
    {
    }
    /** @return self */
    public static function mappingFileNotFound(string $entityName, string $fileName)
    {
    }
    /** @return self */
    public static function invalidMappingFile(string $entityName, string $fileName)
    {
    }
    /** @return self */
    public static function nonExistingClass(string $className)
    {
    }
    /** @param class-string $className */
    public static function classIsAnonymous(string $className) : self
    {
    }
}
