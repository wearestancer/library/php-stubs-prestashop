<?php

namespace Doctrine\Persistence\Mapping;

/**
 * PHP Runtime Reflection Service.
 */
class RuntimeReflectionService implements \Doctrine\Persistence\Mapping\ReflectionService
{
    public function __construct()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getParentClasses(string $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassShortName(string $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassNamespace(string $class)
    {
    }
    /**
     * @psalm-param class-string<T> $class
     *
     * @return ReflectionClass
     * @psalm-return ReflectionClass<T>
     *
     * @template T of object
     */
    public function getClass(string $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAccessibleProperty(string $class, string $property)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasPublicMethod(string $class, string $method)
    {
    }
}
