<?php

namespace Doctrine\Persistence\Mapping;

/**
 * The ClassMetadataFactory is used to create ClassMetadata objects that contain all the
 * metadata mapping informations of a class which describes how a class should be mapped
 * to a relational database.
 *
 * This class was abstracted from the ORM ClassMetadataFactory.
 *
 * @template CMTemplate of ClassMetadata
 * @template-implements ClassMetadataFactory<CMTemplate>
 */
abstract class AbstractClassMetadataFactory implements \Doctrine\Persistence\Mapping\ClassMetadataFactory
{
    /**
     * Salt used by specific Object Manager implementation.
     *
     * @var string
     */
    protected $cacheSalt = '__CLASSMETADATA__';
    /** @var bool */
    protected $initialized = false;
    public function setCache(\Psr\Cache\CacheItemPoolInterface $cache) : void
    {
    }
    protected final function getCache() : ?\Psr\Cache\CacheItemPoolInterface
    {
    }
    /**
     * Returns an array of all the loaded metadata currently in memory.
     *
     * @return ClassMetadata[]
     * @psalm-return CMTemplate[]
     */
    public function getLoadedMetadata()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllMetadata()
    {
    }
    public function setProxyClassNameResolver(\Doctrine\Persistence\Mapping\ProxyClassNameResolver $resolver) : void
    {
    }
    /**
     * Lazy initialization of this stuff, especially the metadata driver,
     * since these are not needed at all when a metadata cache is active.
     *
     * @return void
     */
    protected abstract function initialize();
    /**
     * Returns the mapping driver implementation.
     *
     * @return MappingDriver
     */
    protected abstract function getDriver();
    /**
     * Wakes up reflection after ClassMetadata gets unserialized from cache.
     *
     * @psalm-param CMTemplate $class
     *
     * @return void
     */
    protected abstract function wakeupReflection(\Doctrine\Persistence\Mapping\ClassMetadata $class, \Doctrine\Persistence\Mapping\ReflectionService $reflService);
    /**
     * Initializes Reflection after ClassMetadata was constructed.
     *
     * @psalm-param CMTemplate $class
     *
     * @return void
     */
    protected abstract function initializeReflection(\Doctrine\Persistence\Mapping\ClassMetadata $class, \Doctrine\Persistence\Mapping\ReflectionService $reflService);
    /**
     * Checks whether the class metadata is an entity.
     *
     * This method should return false for mapped superclasses or embedded classes.
     *
     * @psalm-param CMTemplate $class
     *
     * @return bool
     */
    protected abstract function isEntity(\Doctrine\Persistence\Mapping\ClassMetadata $class);
    /**
     * {@inheritDoc}
     *
     * @throws ReflectionException
     * @throws MappingException
     */
    public function getMetadataFor(string $className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasMetadataFor(string $className)
    {
    }
    /**
     * Sets the metadata descriptor for a specific class.
     *
     * NOTE: This is only useful in very special cases, like when generating proxy classes.
     *
     * @psalm-param class-string $className
     * @psalm-param CMTemplate $class
     *
     * @return void
     */
    public function setMetadataFor(string $className, \Doctrine\Persistence\Mapping\ClassMetadata $class)
    {
    }
    /**
     * Gets an array of parent classes for the given entity class.
     *
     * @psalm-param class-string $name
     *
     * @return string[]
     * @psalm-return list<class-string>
     */
    protected function getParentClasses(string $name)
    {
    }
    /**
     * Loads the metadata of the class in question and all it's ancestors whose metadata
     * is still not loaded.
     *
     * Important: The class $name does not necessarily exist at this point here.
     * Scenarios in a code-generation setup might have access to XML/YAML
     * Mapping files without the actual PHP code existing here. That is why the
     * {@see \Doctrine\Persistence\Mapping\ReflectionService} interface
     * should be used for reflection.
     *
     * @param string $name The name of the class for which the metadata should get loaded.
     * @psalm-param class-string $name
     *
     * @return array<int, string>
     * @psalm-return list<string>
     */
    protected function loadMetadata(string $name)
    {
    }
    /**
     * Provides a fallback hook for loading metadata when loading failed due to reflection/mapping exceptions
     *
     * Override this method to implement a fallback strategy for failed metadata loading
     *
     * @return ClassMetadata|null
     * @psalm-return CMTemplate|null
     */
    protected function onNotFoundMetadata(string $className)
    {
    }
    /**
     * Actually loads the metadata from the underlying metadata.
     *
     * @param string[] $nonSuperclassParents All parent class names that are
     *                                       not marked as mapped superclasses.
     * @psalm-param CMTemplate $class
     * @psalm-param CMTemplate|null $parent
     *
     * @return void
     */
    protected abstract function doLoadMetadata(\Doctrine\Persistence\Mapping\ClassMetadata $class, ?\Doctrine\Persistence\Mapping\ClassMetadata $parent, bool $rootEntityFound, array $nonSuperclassParents);
    /**
     * Creates a new ClassMetadata instance for the given class name.
     *
     * @psalm-param class-string<T> $className
     *
     * @return ClassMetadata<T>
     * @psalm-return CMTemplate
     *
     * @template T of object
     */
    protected abstract function newClassMetadataInstance(string $className);
    /**
     * {@inheritDoc}
     */
    public function isTransient(string $className)
    {
    }
    /**
     * Sets the reflectionService.
     *
     * @return void
     */
    public function setReflectionService(\Doctrine\Persistence\Mapping\ReflectionService $reflectionService)
    {
    }
    /**
     * Gets the reflection service associated with this metadata factory.
     *
     * @return ReflectionService
     */
    public function getReflectionService()
    {
    }
    protected function getCacheKey(string $realClassName) : string
    {
    }
}
