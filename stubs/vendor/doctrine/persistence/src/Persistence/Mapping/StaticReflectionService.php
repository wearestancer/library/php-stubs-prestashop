<?php

namespace Doctrine\Persistence\Mapping;

/**
 * PHP Runtime Reflection Service.
 */
class StaticReflectionService implements \Doctrine\Persistence\Mapping\ReflectionService
{
    /**
     * {@inheritDoc}
     */
    public function getParentClasses(string $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassShortName(string $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getClassNamespace(string $class)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return null
     */
    public function getClass(string $class)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAccessibleProperty(string $class, string $property)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function hasPublicMethod(string $class, string $method)
    {
    }
}
