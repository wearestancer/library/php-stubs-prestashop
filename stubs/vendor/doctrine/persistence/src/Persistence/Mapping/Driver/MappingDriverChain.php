<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * The DriverChain allows you to add multiple other mapping drivers for
 * certain namespaces.
 */
class MappingDriverChain implements \Doctrine\Persistence\Mapping\Driver\MappingDriver
{
    /**
     * Gets the default driver.
     *
     * @return MappingDriver|null
     */
    public function getDefaultDriver()
    {
    }
    /**
     * Set the default driver.
     *
     * @return void
     */
    public function setDefaultDriver(\Doctrine\Persistence\Mapping\Driver\MappingDriver $driver)
    {
    }
    /**
     * Adds a nested driver.
     *
     * @return void
     */
    public function addDriver(\Doctrine\Persistence\Mapping\Driver\MappingDriver $nestedDriver, string $namespace)
    {
    }
    /**
     * Gets the array of nested drivers.
     *
     * @return array<string, MappingDriver> $drivers
     */
    public function getDrivers()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass(string $className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllClassNames()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isTransient(string $className)
    {
    }
}
