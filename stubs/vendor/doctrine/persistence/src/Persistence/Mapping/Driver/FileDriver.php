<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * Base driver for file-based metadata drivers.
 *
 * A file driver operates in a mode where it loads the mapping files of individual
 * classes on demand. This requires the user to adhere to the convention of 1 mapping
 * file per class and the file names of the mapping files must correspond to the full
 * class name, including namespace, with the namespace delimiters '\', replaced by dots '.'.
 */
abstract class FileDriver implements \Doctrine\Persistence\Mapping\Driver\MappingDriver
{
    /** @var FileLocator */
    protected $locator;
    /**
     * @var ClassMetadata[]|null
     * @psalm-var array<class-string, ClassMetadata<object>>|null
     */
    protected $classCache;
    /** @var string */
    protected $globalBasename = '';
    /**
     * Initializes a new FileDriver that looks in the given path(s) for mapping
     * documents and operates in the specified operating mode.
     *
     * @param string|array<int, string>|FileLocator $locator A FileLocator or one/multiple paths
     *                                                       where mapping documents can be found.
     */
    public function __construct($locator, ?string $fileExtension = null)
    {
    }
    /**
     * Sets the global basename.
     *
     * @return void
     */
    public function setGlobalBasename(string $file)
    {
    }
    /**
     * Retrieves the global basename.
     *
     * @return string|null
     */
    public function getGlobalBasename()
    {
    }
    /**
     * Gets the element of schema meta data for the class from the mapping file.
     * This will lazily load the mapping file if it is not loaded yet.
     *
     * @psalm-param class-string $className
     *
     * @return ClassMetadata The element of schema meta data.
     * @psalm-return ClassMetadata<object>
     *
     * @throws MappingException
     */
    public function getElement(string $className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isTransient(string $className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllClassNames()
    {
    }
    /**
     * Loads a mapping file with the given name and returns a map
     * from class/entity names to their corresponding file driver elements.
     *
     * @param string $file The mapping file to load.
     *
     * @return ClassMetadata[]
     * @psalm-return array<class-string, ClassMetadata<object>>
     */
    protected abstract function loadMappingFile(string $file);
    /**
     * Initializes the class cache from all the global files.
     *
     * Using this feature adds a substantial performance hit to file drivers as
     * more metadata has to be loaded into memory than might actually be
     * necessary. This may not be relevant to scenarios where caching of
     * metadata is in place, however hits very hard in scenarios where no
     * caching is used.
     *
     * @return void
     */
    protected function initialize()
    {
    }
    /**
     * Retrieves the locator used to discover mapping files by className.
     *
     * @return FileLocator
     */
    public function getLocator()
    {
    }
    /**
     * Sets the locator used to discover mapping files by className.
     *
     * @return void
     */
    public function setLocator(\Doctrine\Persistence\Mapping\Driver\FileLocator $locator)
    {
    }
}
