<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * The ColocatedMappingDriver reads the mapping metadata located near the code.
 */
trait ColocatedMappingDriver
{
    /**
     * The paths where to look for mapping files.
     *
     * @var array<int, string>
     */
    protected $paths = [];
    /**
     * The paths excluded from path where to look for mapping files.
     *
     * @var array<int, string>
     */
    protected $excludePaths = [];
    /**
     * The file extension of mapping documents.
     *
     * @var string
     */
    protected $fileExtension = '.php';
    /**
     * Cache for getAllClassNames().
     *
     * @var array<int, string>|null
     * @psalm-var list<class-string>|null
     */
    protected $classNames;
    /**
     * Appends lookup paths to metadata driver.
     *
     * @param array<int, string> $paths
     *
     * @return void
     */
    public function addPaths(array $paths)
    {
    }
    /**
     * Retrieves the defined metadata lookup paths.
     *
     * @return array<int, string>
     */
    public function getPaths()
    {
    }
    /**
     * Append exclude lookup paths to metadata driver.
     *
     * @param string[] $paths
     *
     * @return void
     */
    public function addExcludePaths(array $paths)
    {
    }
    /**
     * Retrieve the defined metadata lookup exclude paths.
     *
     * @return array<int, string>
     */
    public function getExcludePaths()
    {
    }
    /**
     * Gets the file extension used to look for mapping files under.
     *
     * @return string
     */
    public function getFileExtension()
    {
    }
    /**
     * Sets the file extension used to look for mapping files under.
     *
     * @return void
     */
    public function setFileExtension(string $fileExtension)
    {
    }
    /**
     * {@inheritDoc}
     *
     * Returns whether the class with the specified name is transient. Only non-transient
     * classes, that is entities and mapped superclasses, should have their metadata loaded.
     *
     * @psalm-param class-string $className
     *
     * @return bool
     */
    public abstract function isTransient(string $className);
    /**
     * Gets the names of all mapped classes known to this driver.
     *
     * @return string[] The names of all mapped classes known to this driver.
     * @psalm-return list<class-string>
     */
    public function getAllClassNames()
    {
    }
}
