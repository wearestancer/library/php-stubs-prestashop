<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * The StaticPHPDriver calls a static loadMetadata() method on your entity
 * classes where you can manually populate the ClassMetadata instance.
 */
class StaticPHPDriver implements \Doctrine\Persistence\Mapping\Driver\MappingDriver
{
    /** @param array<int, string>|string $paths */
    public function __construct($paths)
    {
    }
    /**
     * @param array<int, string> $paths
     *
     * @return void
     */
    public function addPaths(array $paths)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadMetadataForClass(string $className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @todo Same code exists in ColocatedMappingDriver, should we re-use it
     * somehow or not worry about it?
     */
    public function getAllClassNames()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isTransient(string $className)
    {
    }
}
