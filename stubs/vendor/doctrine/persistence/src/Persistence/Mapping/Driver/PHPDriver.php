<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * The PHPDriver includes php files which just populate ClassMetadataInfo
 * instances with plain PHP code.
 */
class PHPDriver extends \Doctrine\Persistence\Mapping\Driver\FileDriver
{
    /**
     * @var ClassMetadata
     * @psalm-var ClassMetadata<object>
     */
    protected $metadata;
    /** @param string|array<int, string>|FileLocator $locator */
    public function __construct($locator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass(string $className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function loadMappingFile(string $file)
    {
    }
}
