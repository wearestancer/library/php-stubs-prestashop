<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * The Symfony File Locator makes a simplifying assumptions compared
 * to the DefaultFileLocator. By assuming paths only contain entities of a certain
 * namespace the mapping files consists of the short classname only.
 */
class SymfonyFileLocator implements \Doctrine\Persistence\Mapping\Driver\FileLocator
{
    /**
     * The paths where to look for mapping files.
     *
     * @var array<int, string>
     */
    protected $paths = [];
    /**
     * A map of mapping directory path to namespace prefix used to expand class shortnames.
     *
     * @var array<string, string>
     */
    protected $prefixes = [];
    /**
     * File extension that is searched for.
     *
     * @var string|null
     */
    protected $fileExtension;
    /**
     * @param array<string, string> $prefixes
     * @param string                $nsSeparator String which would be used when converting FQCN
     *                                           to filename and vice versa. Should not be empty
     */
    public function __construct(array $prefixes, string $fileExtension = '', string $nsSeparator = '.')
    {
    }
    /**
     * Adds Namespace Prefixes.
     *
     * @param array<string, string> $prefixes
     *
     * @return void
     */
    public function addNamespacePrefixes(array $prefixes)
    {
    }
    /**
     * Gets Namespace Prefixes.
     *
     * @return string[]
     */
    public function getNamespacePrefixes()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPaths()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getFileExtension()
    {
    }
    /**
     * Sets the file extension used to look for mapping files under.
     *
     * @param string $fileExtension The file extension to set.
     *
     * @return void
     */
    public function setFileExtension(string $fileExtension)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fileExists(string $className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllClassNames(?string $globalBasename = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function findMappingFile(string $className)
    {
    }
}
