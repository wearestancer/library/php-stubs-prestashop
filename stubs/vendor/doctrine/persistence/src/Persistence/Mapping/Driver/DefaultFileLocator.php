<?php

namespace Doctrine\Persistence\Mapping\Driver;

/**
 * Locates the file that contains the metadata information for a given class name.
 *
 * This behavior is independent of the actual content of the file. It just detects
 * the file which is responsible for the given class name.
 */
class DefaultFileLocator implements \Doctrine\Persistence\Mapping\Driver\FileLocator
{
    /**
     * The paths where to look for mapping files.
     *
     * @var array<int, string>
     */
    protected $paths = [];
    /**
     * The file extension of mapping documents.
     *
     * @var string|null
     */
    protected $fileExtension;
    /**
     * Initializes a new FileDriver that looks in the given path(s) for mapping
     * documents and operates in the specified operating mode.
     *
     * @param string|array<int, string> $paths         One or multiple paths where mapping documents
     *                                                 can be found.
     * @param string|null               $fileExtension The file extension of mapping documents,
     *                                                 usually prefixed with a dot.
     */
    public function __construct($paths, ?string $fileExtension = null)
    {
    }
    /**
     * Appends lookup paths to metadata driver.
     *
     * @param array<int, string> $paths
     *
     * @return void
     */
    public function addPaths(array $paths)
    {
    }
    /**
     * Retrieves the defined metadata lookup paths.
     *
     * @return array<int, string>
     */
    public function getPaths()
    {
    }
    /**
     * Gets the file extension used to look for mapping files under.
     *
     * @return string|null
     */
    public function getFileExtension()
    {
    }
    /**
     * Sets the file extension used to look for mapping files under.
     *
     * @param string|null $fileExtension The file extension to set.
     *
     * @return void
     */
    public function setFileExtension(?string $fileExtension)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function findMappingFile(string $className)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllClassNames(string $globalBasename)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function fileExists(string $className)
    {
    }
}
