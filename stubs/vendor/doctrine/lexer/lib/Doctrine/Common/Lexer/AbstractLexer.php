<?php

namespace Doctrine\Common\Lexer;

/**
 * Base class for writing simple lexers, i.e. for creating small DSLs.
 *
 * @psalm-type Token = array{value: int|string, type:string|int|null, position:int}
 */
abstract class AbstractLexer
{
    /**
     * The next token in the input.
     *
     * @var mixed[]|null
     * @psalm-var Token|null
     */
    public $lookahead;
    /**
     * The last matched/seen token.
     *
     * @var mixed[]|null
     * @psalm-var Token|null
     */
    public $token;
    /**
     * Sets the input data to be tokenized.
     *
     * The Lexer is immediately reset and the new input tokenized.
     * Any unprocessed tokens from any previous input are lost.
     *
     * @param string $input The input to be tokenized.
     *
     * @return void
     */
    public function setInput($input)
    {
    }
    /**
     * Resets the lexer.
     *
     * @return void
     */
    public function reset()
    {
    }
    /**
     * Resets the peek pointer to 0.
     *
     * @return void
     */
    public function resetPeek()
    {
    }
    /**
     * Resets the lexer position on the input to the given position.
     *
     * @param int $position Position to place the lexical scanner.
     *
     * @return void
     */
    public function resetPosition($position = 0)
    {
    }
    /**
     * Retrieve the original lexer's input until a given position.
     *
     * @param int $position
     *
     * @return string
     */
    public function getInputUntilPosition($position)
    {
    }
    /**
     * Checks whether a given token matches the current lookahead.
     *
     * @param int|string $type
     *
     * @return bool
     */
    public function isNextToken($type)
    {
    }
    /**
     * Checks whether any of the given tokens matches the current lookahead.
     *
     * @param list<int|string> $types
     *
     * @return bool
     */
    public function isNextTokenAny(array $types)
    {
    }
    /**
     * Moves to the next token in the input string.
     *
     * @return bool
     */
    public function moveNext()
    {
    }
    /**
     * Tells the lexer to skip input tokens until it sees a token with the given value.
     *
     * @param string $type The token type to skip until.
     *
     * @return void
     */
    public function skipUntil($type)
    {
    }
    /**
     * Checks if given value is identical to the given token.
     *
     * @param mixed      $value
     * @param int|string $token
     *
     * @return bool
     */
    public function isA($value, $token)
    {
    }
    /**
     * Moves the lookahead token forward.
     *
     * @return mixed[]|null The next token or NULL if there are no more tokens ahead.
     * @psalm-return Token|null
     */
    public function peek()
    {
    }
    /**
     * Peeks at the next token, returns it and immediately resets the peek.
     *
     * @return mixed[]|null The next token or NULL if there are no more tokens ahead.
     * @psalm-return Token|null
     */
    public function glimpse()
    {
    }
    /**
     * Scans the input string for tokens.
     *
     * @param string $input A query string.
     *
     * @return void
     */
    protected function scan($input)
    {
    }
    /**
     * Gets the literal for a given token.
     *
     * @param int|string $token
     *
     * @return int|string
     */
    public function getLiteral($token)
    {
    }
    /**
     * Regex modifiers
     *
     * @return string
     */
    protected function getModifiers()
    {
    }
    /**
     * Lexical catchable patterns.
     *
     * @return string[]
     */
    protected abstract function getCatchablePatterns();
    /**
     * Lexical non-catchable patterns.
     *
     * @return string[]
     */
    protected abstract function getNonCatchablePatterns();
    /**
     * Retrieve token type. Also processes the token value if necessary.
     *
     * @param string $value
     *
     * @return int|string|null
     */
    protected abstract function getType(&$value);
}
