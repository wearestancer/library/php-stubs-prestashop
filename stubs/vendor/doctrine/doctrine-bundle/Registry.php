<?php

namespace Doctrine\Bundle\DoctrineBundle;

/**
 * References all Doctrine connections and entity managers in a given Container.
 */
class Registry extends \Symfony\Bridge\Doctrine\ManagerRegistry implements \Symfony\Contracts\Service\ResetInterface
{
    /**
     * @param string[] $connections
     * @param string[] $entityManagers
     */
    public function __construct(\Psr\Container\ContainerInterface $container, array $connections, array $entityManagers, string $defaultConnection, string $defaultEntityManager)
    {
    }
    /**
     * Resolves a registered namespace alias to the full namespace.
     *
     * This method looks for the alias in all registered entity managers.
     *
     * @see Configuration::getEntityNamespace
     *
     * @param string $alias The alias
     *
     * @return string The full namespace
     */
    public function getAliasNamespace($alias)
    {
    }
    public function reset() : void
    {
    }
}
