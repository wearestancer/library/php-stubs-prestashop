<?php

namespace Doctrine\Bundle\DoctrineBundle\Mapping;

/** @final */
class ContainerEntityListenerResolver implements \Doctrine\Bundle\DoctrineBundle\Mapping\EntityListenerServiceResolver
{
    /** @param ContainerInterface $container a service locator for listeners */
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear($className = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function register($object)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function registerService($className, $serviceId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve($className)
    {
    }
}
