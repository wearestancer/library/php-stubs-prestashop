<?php

namespace Doctrine\Bundle\DoctrineBundle\Mapping;

/**
 * This class provides methods to access Doctrine entity class metadata for a
 * given bundle, namespace or entity class, for generation purposes
 */
class DisconnectedMetadataFactory
{
    /** @param ManagerRegistry $registry A ManagerRegistry instance */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
    }
    /**
     * Gets the metadata of all classes of a bundle.
     *
     * @param BundleInterface $bundle A BundleInterface instance
     *
     * @return ClassMetadataCollection A ClassMetadataCollection instance
     *
     * @throws RuntimeException When bundle does not contain mapped entities.
     */
    public function getBundleMetadata(\Symfony\Component\HttpKernel\Bundle\BundleInterface $bundle)
    {
    }
    /**
     * Gets the metadata of a class.
     *
     * @param string $class A class name
     * @param string $path  The path where the class is stored (if known)
     *
     * @return ClassMetadataCollection A ClassMetadataCollection instance
     *
     * @throws MappingException When class is not valid entity or mapped superclass.
     */
    public function getClassMetadata($class, $path = null)
    {
    }
    /**
     * Gets the metadata of all classes of a namespace.
     *
     * @param string $namespace A namespace name
     * @param string $path      The path where the class is stored (if known)
     *
     * @return ClassMetadataCollection A ClassMetadataCollection instance
     *
     * @throws RuntimeException When namespace not contain mapped entities.
     */
    public function getNamespaceMetadata($namespace, $path = null)
    {
    }
    /**
     * Find and configure path and namespace for the metadata collection.
     *
     * @param string|null $path
     *
     * @throws RuntimeException When unable to determine the path.
     */
    public function findNamespaceAndPathForMetadata(\Doctrine\Bundle\DoctrineBundle\Mapping\ClassMetadataCollection $metadata, $path = null)
    {
    }
}
