<?php

namespace Doctrine\Bundle\DoctrineBundle\Mapping;

class MappingDriver implements \Doctrine\Persistence\Mapping\Driver\MappingDriver
{
    public function __construct(\Doctrine\Persistence\Mapping\Driver\MappingDriver $driver, \Psr\Container\ContainerInterface $idGeneratorLocator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAllClassNames()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isTransient($className) : bool
    {
    }
    /**
     * {@inheritDoc}
     */
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata) : void
    {
    }
    /**
     * Returns the inner driver
     */
    public function getDriver() : \Doctrine\Persistence\Mapping\Driver\MappingDriver
    {
    }
}
