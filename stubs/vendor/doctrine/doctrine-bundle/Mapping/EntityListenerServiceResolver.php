<?php

namespace Doctrine\Bundle\DoctrineBundle\Mapping;

interface EntityListenerServiceResolver extends \Doctrine\ORM\Mapping\EntityListenerResolver
{
    /**
     * @param string $className
     * @param string $serviceId
     */
    // phpcs:ignore
    public function registerService($className, $serviceId);
}
