<?php

namespace Doctrine\Bundle\DoctrineBundle\Mapping;

class ClassMetadataCollection
{
    /** @param ClassMetadata[] $metadata */
    public function __construct(array $metadata)
    {
    }
    /** @return ClassMetadata[] */
    public function getMetadata()
    {
    }
    /** @param string $path */
    public function setPath($path)
    {
    }
    /** @return string */
    public function getPath()
    {
    }
    /** @param string $namespace */
    public function setNamespace($namespace)
    {
    }
    /** @return string */
    public function getNamespace()
    {
    }
}
