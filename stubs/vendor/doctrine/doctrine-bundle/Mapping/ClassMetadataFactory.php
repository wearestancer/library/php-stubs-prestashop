<?php

namespace Doctrine\Bundle\DoctrineBundle\Mapping;

class ClassMetadataFactory extends \Doctrine\ORM\Mapping\ClassMetadataFactory
{
    /**
     * {@inheritDoc}
     */
    protected function doLoadMetadata($class, $parent, $rootEntityFound, array $nonSuperclassParents) : void
    {
    }
}
