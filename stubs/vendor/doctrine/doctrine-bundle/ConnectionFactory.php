<?php

namespace Doctrine\Bundle\DoctrineBundle;

/** @psalm-import-type Params from DriverManager */
class ConnectionFactory
{
    /** @param mixed[][] $typesConfig */
    public function __construct(array $typesConfig)
    {
    }
    /**
     * Create a connection by name.
     *
     * @param mixed[]               $params
     * @param array<string, string> $mappingTypes
     * @psalm-param Params $params
     *
     * @return Connection
     */
    public function createConnection(array $params, ?\Doctrine\DBAL\Configuration $config = null, ?\Doctrine\Common\EventManager $eventManager = null, array $mappingTypes = [])
    {
    }
}
