<?php

namespace Doctrine\Bundle\DoctrineBundle;

class DoctrineBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function shutdown()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function registerCommands(\Symfony\Component\Console\Application $application)
    {
    }
}
