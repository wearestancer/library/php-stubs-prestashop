<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

final class ServiceRepositoryCompilerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public const REPOSITORY_SERVICE_TAG = 'doctrine.repository_service';
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
}
