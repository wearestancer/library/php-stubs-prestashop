<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

/** @internal */
final class RemoveLoggingMiddlewarePass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
