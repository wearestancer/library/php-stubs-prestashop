<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

final class IdGeneratorPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public const ID_GENERATOR_TAG = 'doctrine.id_generator';
    public const CONFIGURATION_TAG = 'doctrine.orm.configuration';
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
}
