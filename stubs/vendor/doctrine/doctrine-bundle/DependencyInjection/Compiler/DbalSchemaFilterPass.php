<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

/**
 * Processes the doctrine.dbal.schema_filter
 */
class DbalSchemaFilterPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
