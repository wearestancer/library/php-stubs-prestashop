<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

/**
 * Injects Doctrine DBAL and legacy PDO adapters into their schema subscribers.
 *
 * Must be run later after ResolveChildDefinitionsPass.
 */
class CacheSchemaSubscriberPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
