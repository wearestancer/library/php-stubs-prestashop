<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

/**
 * Class for Symfony bundles to register entity listeners
 */
class EntityListenerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    /**
     * {@inheritDoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
