<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler;

/**
 * Blacklist tables used by well-known Symfony classes.
 *
 * @deprecated Implement your own include/exclude mechanism
 */
class WellKnownSchemaFilterPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
