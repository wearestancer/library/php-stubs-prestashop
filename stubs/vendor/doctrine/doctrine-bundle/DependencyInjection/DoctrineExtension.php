<?php

namespace Doctrine\Bundle\DoctrineBundle\DependencyInjection;

/**
 * DoctrineExtension is an extension for the Doctrine DBAL and ORM library.
 */
class DoctrineExtension extends \Symfony\Bridge\Doctrine\DependencyInjection\AbstractDoctrineExtension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Loads the DBAL configuration.
     *
     * Usage example:
     *
     *      <doctrine:dbal id="myconn" dbname="sfweb" user="root" />
     *
     * @param array<string, mixed> $config    An array of configuration settings
     * @param ContainerBuilder     $container A ContainerBuilder instance
     */
    protected function dbalLoad(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Loads a configured DBAL connection.
     *
     * @param string               $name       The name of the connection
     * @param array<string, mixed> $connection A dbal connection configuration.
     * @param ContainerBuilder     $container  A ContainerBuilder instance
     */
    protected function loadDbalConnection($name, array $connection, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * @param array<string, mixed> $connection
     *
     * @return mixed[]
     */
    protected function getConnectionOptions(array $connection) : array
    {
    }
    /**
     * Loads the Doctrine ORM configuration.
     *
     * Usage example:
     *
     *     <doctrine:orm id="mydm" connection="myconn" />
     *
     * @param array<string, mixed> $config    An array of configuration settings
     * @param ContainerBuilder     $container A ContainerBuilder instance
     */
    protected function ormLoad(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Loads a configured ORM entity manager.
     *
     * @param array<string, mixed> $entityManager A configured ORM entity manager.
     * @param ContainerBuilder     $container     A ContainerBuilder instance
     */
    protected function loadOrmEntityManager(array $entityManager, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Loads an ORM entity managers bundle mapping information.
     *
     * There are two distinct configuration possibilities for mapping information:
     *
     * 1. Specify a bundle and optionally details where the entity and mapping information reside.
     * 2. Specify an arbitrary mapping location.
     *
     * @param array<string, mixed> $entityManager A configured ORM entity manager
     * @param Definition           $ormConfigDef  A Definition instance
     * @param ContainerBuilder     $container     A ContainerBuilder instance
     *
     * @example
     *
     *  doctrine.orm:
     *     mappings:
     *         MyBundle1: ~
     *         MyBundle2: yml
     *         MyBundle3: { type: annotation, dir: Entities/ }
     *         MyBundle4: { type: xml, dir: Resources/config/doctrine/mapping }
     *         MyBundle5: { type: attribute, dir: Entities/ }
     *         MyBundle6:
     *             type: yml
     *             dir: bundle-mappings/
     *             alias: BundleAlias
     *         arbitrary_key:
     *             type: xml
     *             dir: %kernel.project_dir%/src/vendor/DoctrineExtensions/lib/DoctrineExtensions/Entities
     *             prefix: DoctrineExtensions\Entities\
     *             alias: DExt
     *
     * In the case of bundles everything is really optional (which leads to autodetection for this bundle) but
     * in the mappings key everything except alias is a required argument.
     */
    protected function loadOrmEntityManagerMappingInformation(array $entityManager, \Symfony\Component\DependencyInjection\Definition $ormConfigDef, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Loads an ORM second level cache bundle mapping information.
     *
     * @param array<string, mixed> $entityManager A configured ORM entity manager
     * @param Definition           $ormConfigDef  A Definition instance
     * @param ContainerBuilder     $container     A ContainerBuilder instance
     *
     * @example
     *  entity_managers:
     *      default:
     *          second_level_cache:
     *              region_lifetime: 3600
     *              region_lock_lifetime: 60
     *              region_cache_driver: apc
     *              log_enabled: true
     *              regions:
     *                  my_service_region:
     *                      type: service
     *                      service : "my_service_region"
     *
     *                  my_query_region:
     *                      lifetime: 300
     *                      cache_driver: array
     *                      type: filelock
     *
     *                  my_entity_region:
     *                      lifetime: 600
     *                      cache_driver:
     *                          type: apc
     */
    protected function loadOrmSecondLevelCache(array $entityManager, \Symfony\Component\DependencyInjection\Definition $ormConfigDef, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getObjectManagerElementName($name) : string
    {
    }
    protected function getMappingObjectDefaultName() : string
    {
    }
    protected function getMappingResourceConfigDirectory(?string $bundleDir = null) : string
    {
    }
    protected function getMappingResourceExtension() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function loadCacheDriver($cacheName, $objectManagerName, array $cacheDriver, \Symfony\Component\DependencyInjection\ContainerBuilder $container) : string
    {
    }
    /**
     * Loads a configured entity managers cache drivers.
     *
     * @param array<string, mixed> $entityManager A configured ORM entity manager.
     */
    protected function loadOrmCacheDrivers(array $entityManager, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * @param array<string, mixed> $objectManager
     * @param string               $cacheName
     *
     * @psalm-suppress MoreSpecificImplementedParamType
     */
    public function loadObjectManagerCacheDriver(array $objectManager, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $cacheName)
    {
    }
    public function getXsdValidationBasePath() : string
    {
    }
    public function getNamespace() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getConfiguration(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container) : \Doctrine\Bundle\DoctrineBundle\DependencyInjection\Configuration
    {
    }
    protected function getMetadataDriverClass(string $driverType) : string
    {
    }
}
