<?php

namespace Doctrine\Bundle\DoctrineBundle;

/**
 * Configurator for an EntityManager
 */
class ManagerConfigurator
{
    /**
     * @param string[]                           $enabledFilters
     * @param array<string,array<string,string>> $filtersParameters
     */
    public function __construct(array $enabledFilters, array $filtersParameters)
    {
    }
    /**
     * Create a connection by name.
     */
    public function configure(\Doctrine\ORM\EntityManagerInterface $entityManager)
    {
    }
}
