<?php

namespace Doctrine\Bundle\DoctrineBundle\EventSubscriber;

interface EventSubscriberInterface extends \Doctrine\Common\EventSubscriber
{
}
