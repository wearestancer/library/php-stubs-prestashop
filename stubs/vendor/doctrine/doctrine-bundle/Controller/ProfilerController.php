<?php

namespace Doctrine\Bundle\DoctrineBundle\Controller;

/** @internal */
class ProfilerController
{
    public function __construct(\Twig\Environment $twig, \Doctrine\Bundle\DoctrineBundle\Registry $registry, \Symfony\Component\HttpKernel\Profiler\Profiler $profiler)
    {
    }
    /**
     * Renders the profiler panel for the given token.
     *
     * @param string $token          The profiler token
     * @param string $connectionName
     * @param int    $query
     *
     * @return Response A Response instance
     */
    public function explainAction($token, $connectionName, $query)
    {
    }
}
