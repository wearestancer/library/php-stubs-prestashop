<?php

namespace Doctrine\Bundle\DoctrineBundle\Command;

/**
 * Base class for Doctrine console commands to extend from.
 *
 * @internal
 */
abstract class DoctrineCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $doctrine)
    {
    }
    /**
     * get a doctrine entity generator
     *
     * @return EntityGenerator
     */
    protected function getEntityGenerator()
    {
    }
    /**
     * Get a doctrine entity manager by symfony name.
     *
     * @param string   $name
     * @param int|null $shardId
     *
     * @return EntityManager
     */
    protected function getEntityManager($name, $shardId = null)
    {
    }
    /**
     * Get a doctrine dbal connection by symfony name.
     *
     * @param string $name
     *
     * @return Connection
     */
    protected function getDoctrineConnection($name)
    {
    }
    /** @return ManagerRegistry */
    protected function getDoctrine()
    {
    }
}
