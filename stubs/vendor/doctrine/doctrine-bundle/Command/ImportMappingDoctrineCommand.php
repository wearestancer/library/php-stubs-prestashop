<?php

namespace Doctrine\Bundle\DoctrineBundle\Command;

/**
 * Import Doctrine ORM metadata mapping information from an existing database.
 *
 * @deprecated
 *
 * @final
 */
class ImportMappingDoctrineCommand extends \Doctrine\Bundle\DoctrineBundle\Command\DoctrineCommand
{
    /** @param string[] $bundles */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $doctrine, array $bundles)
    {
    }
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
