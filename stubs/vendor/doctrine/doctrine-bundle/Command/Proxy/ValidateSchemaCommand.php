<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to run Doctrine ValidateSchema() on the current mappings.
 */
class ValidateSchemaCommand extends \Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
