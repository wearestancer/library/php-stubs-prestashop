<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Ensure the Doctrine ORM is configured properly for a production environment.
 */
class EnsureProductionSettingsDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\EnsureProductionSettingsCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
