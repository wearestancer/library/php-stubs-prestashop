<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to clear a entity cache region.
 */
class EntityRegionCacheDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ClearCache\EntityRegionCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
