<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Convert Doctrine ORM metadata mapping information between the various supported
 * formats.
 */
class ConvertMappingDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ConvertMappingCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * @param string $toType
     * @param string $destPath
     *
     * @return AbstractExporter
     */
    protected function getExporter($toType, $destPath)
    {
    }
}
