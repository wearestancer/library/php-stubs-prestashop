<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Show information about mapped entities
 */
class InfoDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\InfoCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
