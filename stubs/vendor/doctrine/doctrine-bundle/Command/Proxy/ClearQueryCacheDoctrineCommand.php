<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to clear the query cache of the various cache drivers.
 */
class ClearQueryCacheDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
