<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Execute a SQL query and output the results.
 *
 * @deprecated use Doctrine\DBAL\Tools\Console\Command\RunSqlCommand instead
 */
class RunSqlDoctrineCommand extends \Doctrine\DBAL\Tools\Console\Command\RunSqlCommand
{
    public function __construct(?\Doctrine\DBAL\Tools\Console\ConnectionProvider $connectionProvider = null)
    {
    }
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
