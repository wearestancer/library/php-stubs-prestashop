<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to execute the SQL needed to generate the database schema for
 * a given entity manager.
 */
class CreateSchemaDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
