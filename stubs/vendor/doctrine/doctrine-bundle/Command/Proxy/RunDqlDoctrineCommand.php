<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Execute a Doctrine DQL query and output the results.
 */
class RunDqlDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\RunDqlCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
