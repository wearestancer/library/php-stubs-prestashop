<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to drop the database schema for a set of classes based on their mappings.
 */
class DropSchemaDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
