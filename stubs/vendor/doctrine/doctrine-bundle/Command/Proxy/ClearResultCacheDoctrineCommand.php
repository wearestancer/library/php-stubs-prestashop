<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to clear the result cache of the various cache drivers.
 */
class ClearResultCacheDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
