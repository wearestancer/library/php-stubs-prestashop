<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to clear the metadata cache of the various cache drivers.
 */
class ClearMetadataCacheDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
