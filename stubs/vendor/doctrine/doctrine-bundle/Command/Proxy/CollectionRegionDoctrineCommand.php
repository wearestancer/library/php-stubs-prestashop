<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to clear a collection cache region.
 */
class CollectionRegionDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ClearCache\CollectionRegionCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
