<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Loads an SQL file and executes it.
 *
 * @deprecated Use a database client application instead.
 */
class ImportDoctrineCommand extends \Doctrine\DBAL\Tools\Console\Command\ImportCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
