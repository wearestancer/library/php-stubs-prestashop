<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Command to clear a query cache region.
 */
class QueryRegionCacheDoctrineCommand extends \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryRegionCommand
{
    protected function configure() : void
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
