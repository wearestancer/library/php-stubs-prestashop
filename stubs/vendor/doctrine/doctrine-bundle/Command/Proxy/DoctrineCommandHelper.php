<?php

namespace Doctrine\Bundle\DoctrineBundle\Command\Proxy;

/**
 * Provides some helper and convenience methods to configure doctrine commands in the context of bundles
 * and multiple connections/entity managers.
 */
abstract class DoctrineCommandHelper
{
    /**
     * Convenience method to push the helper sets of a given entity manager into the application.
     *
     * @param string $emName
     */
    public static function setApplicationEntityManager(\Symfony\Bundle\FrameworkBundle\Console\Application $application, $emName)
    {
    }
    /**
     * Convenience method to push the helper sets of a given connection into the application.
     *
     * @param string $connName
     */
    public static function setApplicationConnection(\Symfony\Bundle\FrameworkBundle\Console\Application $application, $connName)
    {
    }
}
