<?php

namespace Doctrine\Bundle\DoctrineBundle\Command;

/**
 * Database tool allows you to easily drop your configured databases.
 *
 * @final
 */
class DropDatabaseDoctrineCommand extends \Doctrine\Bundle\DoctrineBundle\Command\DoctrineCommand
{
    public const RETURN_CODE_NOT_DROP = 1;
    public const RETURN_CODE_NO_FORCE = 2;
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
