<?php

namespace Doctrine\Bundle\DoctrineBundle\CacheWarmer;

class DoctrineMetadataCacheWarmer extends \Symfony\Bundle\FrameworkBundle\CacheWarmer\AbstractPhpFileCacheWarmer
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager, string $phpArrayFile)
    {
    }
    /**
     * It must not be optional because it should be called before ProxyCacheWarmer which is not optional.
     */
    public function isOptional() : bool
    {
    }
    /** @param string $cacheDir */
    protected function doWarmUp($cacheDir, \Symfony\Component\Cache\Adapter\ArrayAdapter $arrayAdapter) : bool
    {
    }
}
