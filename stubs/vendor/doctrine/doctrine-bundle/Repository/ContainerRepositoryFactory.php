<?php

namespace Doctrine\Bundle\DoctrineBundle\Repository;

/**
 * Fetches repositories from the container or falls back to normal creation.
 */
final class ContainerRepositoryFactory implements \Doctrine\ORM\Repository\RepositoryFactory
{
    /** @param ContainerInterface $container A service locator containing the repositories */
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRepository(\Doctrine\ORM\EntityManagerInterface $entityManager, $entityName) : \Doctrine\Persistence\ObjectRepository
    {
    }
}
