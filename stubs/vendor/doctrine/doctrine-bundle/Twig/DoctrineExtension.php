<?php

namespace Doctrine\Bundle\DoctrineBundle\Twig;

/**
 * This class contains the needed functions in order to do the query highlighting
 */
class DoctrineExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * Define our functions
     *
     * @return TwigFilter[]
     */
    public function getFilters()
    {
    }
    /**
     * Escape parameters of a SQL query
     * DON'T USE THIS FUNCTION OUTSIDE ITS INTENDED SCOPE
     *
     * @internal
     *
     * @param mixed $parameter
     *
     * @return string
     */
    public static function escapeFunction($parameter)
    {
    }
    /**
     * Return a query with the parameters replaced
     *
     * @param string       $query
     * @param mixed[]|Data $parameters
     *
     * @return string
     */
    public function replaceQueryParameters($query, $parameters)
    {
    }
    /**
     * Formats and/or highlights the given SQL statement.
     *
     * @param  string $sql
     * @param  bool   $highlightOnly If true the query is not formatted, just highlighted
     *
     * @return string
     */
    public function formatQuery($sql, $highlightOnly = false)
    {
    }
    public function prettifySql(string $sql) : string
    {
    }
    public function formatSql(string $sql, bool $highlight) : string
    {
    }
    /**
     * Get the name of the extension
     *
     * @return string
     */
    public function getName()
    {
    }
}
