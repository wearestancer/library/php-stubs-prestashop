<?php

namespace Doctrine\Bundle\DoctrineBundle\Dbal;

/** @deprecated Implement your own include/exclude mechanism */
class BlacklistSchemaAssetFilter
{
    /** @param string[] $blacklist */
    public function __construct(array $blacklist)
    {
    }
    /** @param string|AbstractAsset $assetName */
    public function __invoke($assetName) : bool
    {
    }
}
