<?php

namespace Doctrine\Bundle\DoctrineBundle\Dbal;

class ManagerRegistryAwareConnectionProvider implements \Doctrine\DBAL\Tools\Console\ConnectionProvider
{
    public function __construct(\Doctrine\Persistence\AbstractManagerRegistry $managerRegistry)
    {
    }
    public function getDefaultConnection() : \Doctrine\DBAL\Connection
    {
    }
    public function getConnection(string $name) : \Doctrine\DBAL\Connection
    {
    }
}
