<?php

namespace Doctrine\Bundle\DoctrineBundle\Dbal;

/**
 * Manages schema filters passed to Connection::setSchemaAssetsFilter()
 */
class SchemaAssetsFilterManager
{
    /** @param callable[] $schemaAssetFilters */
    public function __construct(array $schemaAssetFilters)
    {
    }
    /** @param string|AbstractAsset $assetName */
    public function __invoke($assetName) : bool
    {
    }
}
