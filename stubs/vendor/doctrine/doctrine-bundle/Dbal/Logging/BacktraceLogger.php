<?php

namespace Doctrine\Bundle\DoctrineBundle\Dbal\Logging;

final class BacktraceLogger extends \Doctrine\DBAL\Logging\DebugStack
{
    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null) : void
    {
    }
}
