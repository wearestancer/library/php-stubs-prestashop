<?php

namespace Doctrine\Bundle\DoctrineBundle\DataCollector;

/**
 * @psalm-type QueryType = array{
 *    executionMS: float,
 *    explainable: bool,
 *    sql: string,
 *    params: ?array<array-key, mixed>,
 *    runnable: bool,
 *    types: ?array<array-key, Type|int|string|null>,
 * }
 * @psalm-type DataType = array{
 *    caches: array{
 *       enabled: bool,
 *       counts: array<"puts"|"hits"|"misses", int>,
 *       log_enabled: bool,
 *       regions: array<"puts"|"hits"|"misses", array<string, int>>,
 *    },
 *    connections: list<string>,
 *    entities: array<string, array<class-string, class-string>>,
 *    errors: array<string, array<class-string, list<string>>>,
 *    managers: list<string>,
 *    queries: array<string, list<QueryType>>,
 * }
 * @psalm-property DataType $data
 */
class DoctrineDataCollector extends \Symfony\Bridge\Doctrine\DataCollector\DoctrineDataCollector
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry, bool $shouldValidateSchema = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, ?\Throwable $exception = null)
    {
    }
    /** @return array<string, array<string, string>> */
    public function getEntities()
    {
    }
    /** @return array<string, array<string, list<string>>> */
    public function getMappingErrors()
    {
    }
    /** @return int */
    public function getCacheHitsCount()
    {
    }
    /** @return int */
    public function getCachePutsCount()
    {
    }
    /** @return int */
    public function getCacheMissesCount()
    {
    }
    /** @return bool */
    public function getCacheEnabled()
    {
    }
    /**
     * @return array<string, array<string, int>>
     * @psalm-return array<"puts"|"hits"|"misses", array<string, int>>
     */
    public function getCacheRegions()
    {
    }
    /** @return array<string, int> */
    public function getCacheCounts()
    {
    }
    /** @return int */
    public function getInvalidEntityCount()
    {
    }
    /**
     * @return string[][]
     * @psalm-return array<string, list<QueryType&array{count: int, index: int, executionPercent: float}>>
     */
    public function getGroupedQueries()
    {
    }
    /** @return int */
    public function getGroupedQueryCount()
    {
    }
}
