<?php

namespace Doctrine\Instantiator\Exception;

/**
 * Exception for invalid arguments provided to the instantiator
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Doctrine\Instantiator\Exception\ExceptionInterface
{
    public static function fromNonExistingClass(string $className) : self
    {
    }
    /**
     * @phpstan-param ReflectionClass<T> $reflectionClass
     *
     * @template T of object
     */
    public static function fromAbstractClass(\ReflectionClass $reflectionClass) : self
    {
    }
    public static function fromEnum(string $className) : self
    {
    }
}
