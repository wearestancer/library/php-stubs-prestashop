<?php

namespace Doctrine\Instantiator\Exception;

/**
 * Exception for given parameters causing invalid/unexpected state on instantiation
 */
class UnexpectedValueException extends \UnexpectedValueException implements \Doctrine\Instantiator\Exception\ExceptionInterface
{
    /**
     * @phpstan-param ReflectionClass<T> $reflectionClass
     *
     * @template T of object
     */
    public static function fromSerializationTriggeredException(\ReflectionClass $reflectionClass, \Exception $exception) : self
    {
    }
    /**
     * @phpstan-param ReflectionClass<T> $reflectionClass
     *
     * @template T of object
     */
    public static function fromUncleanUnSerialization(\ReflectionClass $reflectionClass, string $errorString, int $errorCode, string $errorFile, int $errorLine) : self
    {
    }
}
