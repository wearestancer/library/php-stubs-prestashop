<?php

namespace Doctrine\Inflector\Rules;

final class Pattern
{
    public function __construct(string $pattern)
    {
    }
    public function getPattern() : string
    {
    }
    public function getRegex() : string
    {
    }
    public function matches(string $word) : bool
    {
    }
}
