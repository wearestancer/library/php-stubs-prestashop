<?php

namespace Doctrine\Inflector\Rules\NorwegianBokmal;

final class Uninflected
{
    /**
     * @return Pattern[]
     */
    public static function getSingular() : iterable
    {
    }
    /**
     * @return Pattern[]
     */
    public static function getPlural() : iterable
    {
    }
}
