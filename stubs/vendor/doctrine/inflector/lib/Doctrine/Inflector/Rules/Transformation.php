<?php

namespace Doctrine\Inflector\Rules;

final class Transformation implements \Doctrine\Inflector\WordInflector
{
    public function __construct(\Doctrine\Inflector\Rules\Pattern $pattern, string $replacement)
    {
    }
    public function getPattern() : \Doctrine\Inflector\Rules\Pattern
    {
    }
    public function getReplacement() : string
    {
    }
    public function inflect(string $word) : string
    {
    }
}
