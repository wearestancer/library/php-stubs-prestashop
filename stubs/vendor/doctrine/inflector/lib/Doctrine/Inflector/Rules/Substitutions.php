<?php

namespace Doctrine\Inflector\Rules;

class Substitutions implements \Doctrine\Inflector\WordInflector
{
    public function __construct(\Doctrine\Inflector\Rules\Substitution ...$substitutions)
    {
    }
    public function getFlippedSubstitutions() : \Doctrine\Inflector\Rules\Substitutions
    {
    }
    public function inflect(string $word) : string
    {
    }
}
