<?php

namespace Doctrine\Inflector\Rules;

class Ruleset
{
    public function __construct(\Doctrine\Inflector\Rules\Transformations $regular, \Doctrine\Inflector\Rules\Patterns $uninflected, \Doctrine\Inflector\Rules\Substitutions $irregular)
    {
    }
    public function getRegular() : \Doctrine\Inflector\Rules\Transformations
    {
    }
    public function getUninflected() : \Doctrine\Inflector\Rules\Patterns
    {
    }
    public function getIrregular() : \Doctrine\Inflector\Rules\Substitutions
    {
    }
}
