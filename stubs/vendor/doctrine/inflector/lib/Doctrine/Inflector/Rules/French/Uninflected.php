<?php

namespace Doctrine\Inflector\Rules\French;

final class Uninflected
{
    /**
     * @return Pattern[]
     */
    public static function getSingular() : iterable
    {
    }
    /**
     * @return Pattern[]
     */
    public static function getPlural() : iterable
    {
    }
}
