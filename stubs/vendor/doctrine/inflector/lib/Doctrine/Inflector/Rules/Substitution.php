<?php

namespace Doctrine\Inflector\Rules;

final class Substitution
{
    public function __construct(\Doctrine\Inflector\Rules\Word $from, \Doctrine\Inflector\Rules\Word $to)
    {
    }
    public function getFrom() : \Doctrine\Inflector\Rules\Word
    {
    }
    public function getTo() : \Doctrine\Inflector\Rules\Word
    {
    }
}
