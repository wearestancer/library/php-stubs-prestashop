<?php

namespace Doctrine\Inflector\Rules\English;

class Inflectible
{
    /**
     * @return Transformation[]
     */
    public static function getSingular() : iterable
    {
    }
    /**
     * @return Transformation[]
     */
    public static function getPlural() : iterable
    {
    }
    /**
     * @return Substitution[]
     */
    public static function getIrregular() : iterable
    {
    }
}
