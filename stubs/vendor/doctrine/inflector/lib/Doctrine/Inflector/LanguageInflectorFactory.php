<?php

namespace Doctrine\Inflector;

interface LanguageInflectorFactory
{
    /**
     * Applies custom rules for singularisation
     *
     * @param bool $reset If true, will unset default inflections for all new rules
     *
     * @return $this
     */
    public function withSingularRules(?\Doctrine\Inflector\Rules\Ruleset $singularRules, bool $reset = false) : self;
    /**
     * Applies custom rules for pluralisation
     *
     * @param bool $reset If true, will unset default inflections for all new rules
     *
     * @return $this
     */
    public function withPluralRules(?\Doctrine\Inflector\Rules\Ruleset $pluralRules, bool $reset = false) : self;
    /**
     * Builds the inflector instance with all applicable rules
     */
    public function build() : \Doctrine\Inflector\Inflector;
}
