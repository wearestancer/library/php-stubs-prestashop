<?php

namespace Doctrine\Inflector;

abstract class GenericLanguageInflectorFactory implements \Doctrine\Inflector\LanguageInflectorFactory
{
    public final function __construct()
    {
    }
    public final function build() : \Doctrine\Inflector\Inflector
    {
    }
    public final function withSingularRules(?\Doctrine\Inflector\Rules\Ruleset $singularRules, bool $reset = false) : \Doctrine\Inflector\LanguageInflectorFactory
    {
    }
    public final function withPluralRules(?\Doctrine\Inflector\Rules\Ruleset $pluralRules, bool $reset = false) : \Doctrine\Inflector\LanguageInflectorFactory
    {
    }
    protected abstract function getSingularRuleset() : \Doctrine\Inflector\Rules\Ruleset;
    protected abstract function getPluralRuleset() : \Doctrine\Inflector\Rules\Ruleset;
}
