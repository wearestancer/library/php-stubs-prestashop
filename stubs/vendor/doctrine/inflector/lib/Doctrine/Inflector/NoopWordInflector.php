<?php

namespace Doctrine\Inflector;

class NoopWordInflector implements \Doctrine\Inflector\WordInflector
{
    public function inflect(string $word) : string
    {
    }
}
