<?php

namespace Doctrine\Inflector;

final class InflectorFactory
{
    public static function create() : \Doctrine\Inflector\LanguageInflectorFactory
    {
    }
    public static function createForLanguage(string $language) : \Doctrine\Inflector\LanguageInflectorFactory
    {
    }
}
