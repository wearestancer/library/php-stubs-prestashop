<?php

namespace Doctrine\Inflector;

/**
 * Inflects based on multiple rulesets.
 *
 * Rules:
 * - If the word matches any uninflected word pattern, it is not inflected
 * - The first ruleset that returns a different value for an irregular word wins
 * - The first ruleset that returns a different value for a regular word wins
 * - If none of the above match, the word is left as-is
 */
class RulesetInflector implements \Doctrine\Inflector\WordInflector
{
    public function __construct(\Doctrine\Inflector\Rules\Ruleset $ruleset, \Doctrine\Inflector\Rules\Ruleset ...$rulesets)
    {
    }
    public function inflect(string $word) : string
    {
    }
}
