<?php

namespace Doctrine\Common\Util;

/**
 * Class and reflection related functionality for objects that
 * might or not be proxy objects at the moment.
 */
class ClassUtils
{
    /**
     * Gets the real class name of a class name that could be a proxy.
     *
     * @param string $className
     * @psalm-param class-string<Proxy<T>>|class-string<T> $className
     *
     * @return string
     * @psalm-return class-string<T>
     *
     * @template T of object
     */
    public static function getRealClass($className)
    {
    }
    /**
     * Gets the real class name of an object (even if its a proxy).
     *
     * @param object $object
     * @psalm-param Proxy<T>|T $object
     *
     * @return string
     * @psalm-return class-string<T>
     *
     * @template T of object
     */
    public static function getClass($object)
    {
    }
    /**
     * Gets the real parent class name of a class or object.
     *
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return string
     * @psalm-return class-string
     */
    public static function getParentClass($className)
    {
    }
    /**
     * Creates a new reflection class.
     *
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return ReflectionClass
     */
    public static function newReflectionClass($className)
    {
    }
    /**
     * Creates a new reflection object.
     *
     * @param object $object
     *
     * @return ReflectionClass
     */
    public static function newReflectionObject($object)
    {
    }
    /**
     * Given a class name and a proxy namespace returns the proxy name.
     *
     * @param string $className
     * @param string $proxyNamespace
     * @psalm-param class-string $className
     *
     * @return string
     * @psalm-return class-string
     */
    public static function generateProxyClassName($className, $proxyNamespace)
    {
    }
}
