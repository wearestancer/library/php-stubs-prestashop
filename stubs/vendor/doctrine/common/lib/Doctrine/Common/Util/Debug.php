<?php

namespace Doctrine\Common\Util;

/**
 * Static class containing most used debug methods.
 *
 * @deprecated The Debug class is deprecated, please use symfony/var-dumper instead.
 *
 * @link   www.doctrine-project.org
 */
final class Debug
{
    /**
     * Prints a dump of the public, protected and private properties of $var.
     *
     * @link https://xdebug.org/
     *
     * @param mixed $var       The variable to dump.
     * @param int   $maxDepth  The maximum nesting level for object properties.
     * @param bool  $stripTags Whether output should strip HTML tags.
     * @param bool  $echo      Send the dumped value to the output buffer
     *
     * @return string
     */
    public static function dump($var, $maxDepth = 2, $stripTags = true, $echo = true)
    {
    }
    /**
     * @param mixed $var
     * @param int   $maxDepth
     *
     * @return mixed
     */
    public static function export($var, $maxDepth)
    {
    }
    /**
     * Returns a string representation of an object.
     *
     * @param object $obj
     *
     * @return string
     */
    public static function toString($obj)
    {
    }
}
