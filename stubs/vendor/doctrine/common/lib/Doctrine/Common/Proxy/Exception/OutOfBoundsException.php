<?php

namespace Doctrine\Common\Proxy\Exception;

/**
 * Proxy Invalid Argument Exception.
 *
 * @link   www.doctrine-project.org
 */
class OutOfBoundsException extends \OutOfBoundsException implements \Doctrine\Common\Proxy\Exception\ProxyException
{
    /**
     * @param string $className
     * @param string $idField
     * @psalm-param class-string $className
     *
     * @return self
     */
    public static function missingPrimaryKeyValue($className, $idField)
    {
    }
}
