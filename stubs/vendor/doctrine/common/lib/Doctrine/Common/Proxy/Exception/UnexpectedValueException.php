<?php

namespace Doctrine\Common\Proxy\Exception;

/**
 * Proxy Unexpected Value Exception.
 *
 * @link   www.doctrine-project.org
 */
class UnexpectedValueException extends \UnexpectedValueException implements \Doctrine\Common\Proxy\Exception\ProxyException
{
    /**
     * @param string $proxyDirectory
     *
     * @return self
     */
    public static function proxyDirectoryNotWritable($proxyDirectory)
    {
    }
    /**
     * @param string $className
     * @param string $methodName
     * @param string $parameterName
     * @psalm-param class-string $className
     *
     * @return self
     */
    public static function invalidParameterTypeHint($className, $methodName, $parameterName, ?\Throwable $previous = null)
    {
    }
    /**
     * @param string $className
     * @param string $methodName
     * @psalm-param class-string $className
     *
     * @return self
     */
    public static function invalidReturnTypeHint($className, $methodName, ?\Throwable $previous = null)
    {
    }
}
