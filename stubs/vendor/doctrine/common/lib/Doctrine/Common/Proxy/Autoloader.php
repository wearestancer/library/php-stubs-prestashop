<?php

namespace Doctrine\Common\Proxy;

/**
 * Special Autoloader for Proxy classes, which are not PSR-0 compliant.
 *
 * @internal
 */
class Autoloader
{
    /**
     * Resolves proxy class name to a filename based on the following pattern.
     *
     * 1. Remove Proxy namespace from class name.
     * 2. Remove namespace separators from remaining class name.
     * 3. Return PHP filename from proxy-dir with the result from 2.
     *
     * @param string $proxyDir
     * @param string $proxyNamespace
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public static function resolveFile($proxyDir, $proxyNamespace, $className)
    {
    }
    /**
     * Registers and returns autoloader callback for the given proxy dir and namespace.
     *
     * @param string        $proxyDir
     * @param string        $proxyNamespace
     * @param callable|null $notFoundCallback Invoked when the proxy file is not found.
     *
     * @return Closure
     *
     * @throws InvalidArgumentException
     */
    public static function register($proxyDir, $proxyNamespace, $notFoundCallback = null)
    {
    }
}
