<?php

namespace Doctrine\Common\Proxy\Exception;

/**
 * Proxy Invalid Argument Exception.
 *
 * @link   www.doctrine-project.org
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Doctrine\Common\Proxy\Exception\ProxyException
{
    /**
     * @return self
     */
    public static function proxyDirectoryRequired()
    {
    }
    /**
     * @param string $className
     * @param string $proxyNamespace
     * @psalm-param class-string $className
     *
     * @return self
     */
    public static function notProxyClass($className, $proxyNamespace)
    {
    }
    /**
     * @param string $name
     *
     * @return self
     */
    public static function invalidPlaceholder($name)
    {
    }
    /**
     * @return self
     */
    public static function proxyNamespaceRequired()
    {
    }
    /**
     * @return self
     */
    public static function unitializedProxyExpected(\Doctrine\Persistence\Proxy $proxy)
    {
    }
    /**
     * @param mixed $callback
     *
     * @return self
     */
    public static function invalidClassNotFoundCallback($callback)
    {
    }
    /**
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return self
     */
    public static function classMustNotBeAbstract($className)
    {
    }
    /**
     * @param string $className
     * @psalm-param class-string $className
     *
     * @return self
     */
    public static function classMustNotBeFinal($className)
    {
    }
    /**
     * @param mixed $value
     */
    public static function invalidAutoGenerateMode($value) : self
    {
    }
}
