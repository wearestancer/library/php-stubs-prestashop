<?php

namespace Doctrine\Common;

/**
 * A <tt>ClassLoader</tt> is an autoloader for class files that can be
 * installed on the SPL autoload stack. It is a class loader that either loads only classes
 * of a specific namespace or all namespaces and it is suitable for working together
 * with other autoloaders in the SPL autoload stack.
 *
 * If no include path is configured through the constructor or {@link setIncludePath}, a ClassLoader
 * relies on the PHP <code>include_path</code>.
 *
 * @deprecated The ClassLoader is deprecated and will be removed in version 4.0 of doctrine/common.
 */
class ClassLoader
{
    /**
     * PHP file extension.
     *
     * @var string
     */
    protected $fileExtension = '.php';
    /**
     * Current namespace.
     *
     * @var string|null
     */
    protected $namespace;
    /**
     * Current include path.
     *
     * @var string|null
     */
    protected $includePath;
    /**
     * PHP namespace separator.
     *
     * @var string
     */
    protected $namespaceSeparator = '\\';
    /**
     * Creates a new <tt>ClassLoader</tt> that loads classes of the
     * specified namespace from the specified include path.
     *
     * If no include path is given, the ClassLoader relies on the PHP include_path.
     * If neither a namespace nor an include path is given, the ClassLoader will
     * be responsible for loading all classes, thereby relying on the PHP include_path.
     *
     * @param string|null $ns          The namespace of the classes to load.
     * @param string|null $includePath The base include path to use.
     */
    public function __construct($ns = null, $includePath = null)
    {
    }
    /**
     * Sets the namespace separator used by classes in the namespace of this ClassLoader.
     *
     * @param string $sep The separator to use.
     *
     * @return void
     */
    public function setNamespaceSeparator($sep)
    {
    }
    /**
     * Gets the namespace separator used by classes in the namespace of this ClassLoader.
     *
     * @return string
     */
    public function getNamespaceSeparator()
    {
    }
    /**
     * Sets the base include path for all class files in the namespace of this ClassLoader.
     *
     * @param string|null $includePath
     *
     * @return void
     */
    public function setIncludePath($includePath)
    {
    }
    /**
     * Gets the base include path for all class files in the namespace of this ClassLoader.
     *
     * @return string|null
     */
    public function getIncludePath()
    {
    }
    /**
     * Sets the file extension of class files in the namespace of this ClassLoader.
     *
     * @param string $fileExtension
     *
     * @return void
     */
    public function setFileExtension($fileExtension)
    {
    }
    /**
     * Gets the file extension of class files in the namespace of this ClassLoader.
     *
     * @return string
     */
    public function getFileExtension()
    {
    }
    /**
     * Registers this ClassLoader on the SPL autoload stack.
     *
     * @return void
     */
    public function register()
    {
    }
    /**
     * Removes this ClassLoader from the SPL autoload stack.
     *
     * @return void
     */
    public function unregister()
    {
    }
    /**
     * Loads the given class or interface.
     *
     * @param string $className The name of the class to load.
     * @psalm-param class-string $className
     *
     * @return bool TRUE if the class has been successfully loaded, FALSE otherwise.
     */
    public function loadClass($className)
    {
    }
    /**
     * Asks this ClassLoader whether it can potentially load the class (file) with
     * the given name.
     *
     * @param string $className The fully-qualified name of the class.
     * @psalm-param class-string $className
     *
     * @return bool TRUE if this ClassLoader can load the class, FALSE otherwise.
     */
    public function canLoadClass($className)
    {
    }
    /**
     * Checks whether a class with a given name exists. A class "exists" if it is either
     * already defined in the current request or if there is an autoloader on the SPL
     * autoload stack that is a) responsible for the class in question and b) is able to
     * load a class file in which the class definition resides.
     *
     * If the class is not already defined, each autoloader in the SPL autoload stack
     * is asked whether it is able to tell if the class exists. If the autoloader is
     * a <tt>ClassLoader</tt>, {@link canLoadClass} is used, otherwise the autoload
     * function of the autoloader is invoked and expected to return a value that
     * evaluates to TRUE if the class (file) exists. As soon as one autoloader reports
     * that the class exists, TRUE is returned.
     *
     * Note that, depending on what kinds of autoloaders are installed on the SPL
     * autoload stack, the class (file) might already be loaded as a result of checking
     * for its existence. This is not the case with a <tt>ClassLoader</tt>, who separates
     * these responsibilities.
     *
     * @param string $className The fully-qualified name of the class.
     * @psalm-param class-string $className
     *
     * @return bool TRUE if the class exists as per the definition given above, FALSE otherwise.
     */
    public static function classExists($className)
    {
    }
    /**
     * Gets the <tt>ClassLoader</tt> from the SPL autoload stack that is responsible
     * for (and is able to load) the class with the given name.
     *
     * @param string $className The name of the class.
     * @psalm-param class-string $className
     *
     * @return ClassLoader|null The <tt>ClassLoader</tt> for the class or NULL if no such <tt>ClassLoader</tt> exists.
     */
    public static function getClassLoader($className)
    {
    }
}
