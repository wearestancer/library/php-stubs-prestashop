<?php

namespace Doctrine\Common\Collections;

/**
 * An ArrayCollection is a Collection implementation that wraps a regular PHP array.
 *
 * Warning: Using (un-)serialize() on a collection is not a supported use-case
 * and may break when we change the internals in the future. If you need to
 * serialize a collection use {@link toArray()} and reconstruct the collection
 * manually.
 *
 * @psalm-template TKey of array-key
 * @psalm-template T
 * @template-implements Collection<TKey,T>
 * @template-implements Selectable<TKey,T>
 * @psalm-consistent-constructor
 */
class ArrayCollection implements \Doctrine\Common\Collections\Collection, \Doctrine\Common\Collections\Selectable
{
    /**
     * Initializes a new ArrayCollection.
     *
     * @param array $elements
     * @psalm-param array<TKey,T> $elements
     */
    public function __construct(array $elements = [])
    {
    }
    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function first()
    {
    }
    /**
     * Creates a new instance from the specified elements.
     *
     * This method is provided for derived classes to specify how a new
     * instance should be created when constructor semantics have changed.
     *
     * @param array $elements Elements.
     * @psalm-param array<K,V> $elements
     *
     * @return static
     * @psalm-return static<K,V>
     *
     * @psalm-template K of array-key
     * @psalm-template V
     */
    protected function createFrom(array $elements)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function last()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function key()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function next()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function current()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function remove($key)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function removeElement($element)
    {
    }
    /**
     * Required by interface ArrayAccess.
     *
     * @param TKey $offset
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
    }
    /**
     * Required by interface ArrayAccess.
     *
     * @param TKey $offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * Required by interface ArrayAccess.
     *
     * @param TKey|null $offset
     * @param T         $value
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
    }
    /**
     * Required by interface ArrayAccess.
     *
     * @param TKey $offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function containsKey($key)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @template TMaybeContained
     */
    public function contains($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function exists(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @psalm-param TMaybeContained $element
     *
     * @psalm-return (TMaybeContained is T ? TKey|false : false)
     *
     * @template TMaybeContained
     */
    public function indexOf($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function get($key)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getKeys()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getValues()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function set($key, $value)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @psalm-suppress InvalidPropertyAssignmentValue
     *
     * This breaks assumptions about the template type, but it would
     * be a backwards-incompatible change to remove this method
     */
    public function add($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isEmpty()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Traversable<int|string, mixed>
     * @psalm-return Traversable<TKey,T>
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @psalm-param Closure(T):U $func
     *
     * @return static
     * @psalm-return static<TKey, U>
     *
     * @psalm-template U
     */
    public function map(\Closure $func)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return static
     * @psalm-return static<TKey,T>
     */
    public function filter(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function forAll(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function partition(\Closure $p)
    {
    }
    /**
     * Returns a string representation of this object.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function slice($offset, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function matching(\Doctrine\Common\Collections\Criteria $criteria)
    {
    }
}
