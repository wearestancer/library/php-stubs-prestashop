<?php

namespace Doctrine\Common\Collections\Expr;

/**
 * An Expression visitor walks a graph of expressions and turns them into a
 * query for the underlying implementation.
 */
abstract class ExpressionVisitor
{
    /**
     * Converts a comparison expression into the target query language output.
     *
     * @return mixed
     */
    public abstract function walkComparison(\Doctrine\Common\Collections\Expr\Comparison $comparison);
    /**
     * Converts a value expression into the target query language part.
     *
     * @return mixed
     */
    public abstract function walkValue(\Doctrine\Common\Collections\Expr\Value $value);
    /**
     * Converts a composite expression into the target query language output.
     *
     * @return mixed
     */
    public abstract function walkCompositeExpression(\Doctrine\Common\Collections\Expr\CompositeExpression $expr);
    /**
     * Dispatches walking an expression to the appropriate handler.
     *
     * @return mixed
     *
     * @throws RuntimeException
     */
    public function dispatch(\Doctrine\Common\Collections\Expr\Expression $expr)
    {
    }
}
