<?php

namespace Doctrine\Common\Collections;

/**
 * Builder for Expressions in the {@link Selectable} interface.
 *
 * Important Notice for interoperable code: You have to use scalar
 * values only for comparisons, otherwise the behavior of the comparison
 * may be different between implementations (Array vs ORM vs ODM).
 */
class ExpressionBuilder
{
    /**
     * @param mixed ...$x
     *
     * @return CompositeExpression
     */
    public function andX($x = null)
    {
    }
    /**
     * @param mixed ...$x
     *
     * @return CompositeExpression
     */
    public function orX($x = null)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function eq($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function gt($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function lt($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function gte($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function lte($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function neq($field, $value)
    {
    }
    /**
     * @param string $field
     *
     * @return Comparison
     */
    public function isNull($field)
    {
    }
    /**
     * @param string  $field
     * @param mixed[] $values
     *
     * @return Comparison
     */
    public function in($field, array $values)
    {
    }
    /**
     * @param string  $field
     * @param mixed[] $values
     *
     * @return Comparison
     */
    public function notIn($field, array $values)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function contains($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function memberOf($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function startsWith($field, $value)
    {
    }
    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return Comparison
     */
    public function endsWith($field, $value)
    {
    }
}
