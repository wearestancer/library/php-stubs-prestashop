<?php

namespace Doctrine\Common\Collections;

/**
 * Lazy collection that is backed by a concrete collection
 *
 * @psalm-template TKey of array-key
 * @psalm-template T
 * @template-implements Collection<TKey,T>
 */
abstract class AbstractLazyCollection implements \Doctrine\Common\Collections\Collection
{
    /**
     * The backed collection to use
     *
     * @psalm-var Collection<TKey,T>|null
     * @var Collection<mixed>|null
     */
    protected $collection;
    /** @var bool */
    protected $initialized = false;
    /**
     * {@inheritDoc}
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function add($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritDoc}
     *
     * @template TMaybeContained
     */
    public function contains($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function isEmpty()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function remove($key)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function removeElement($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function containsKey($key)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function get($key)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getKeys()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getValues()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function set($key, $value)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function first()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function last()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function key()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function current()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function next()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function exists(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function filter(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function forAll(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function map(\Closure $func)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function partition(\Closure $p)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @template TMaybeContained
     */
    public function indexOf($element)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function slice($offset, $length = null)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return Traversable<int|string, mixed>
     * @psalm-return Traversable<TKey,T>
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * @param TKey $offset
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
    }
    /**
     * @param TKey $offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * @param TKey|null $offset
     * @param T         $value
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
    }
    /**
     * @param TKey $offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
    }
    /**
     * Is the lazy collection already initialized?
     *
     * @return bool
     *
     * @psalm-assert-if-true Collection<TKey,T> $this->collection
     */
    public function isInitialized()
    {
    }
    /**
     * Initialize the collection
     *
     * @return void
     *
     * @psalm-assert Collection<TKey,T> $this->collection
     */
    protected function initialize()
    {
    }
    /**
     * Do the initialization logic
     *
     * @return void
     */
    protected abstract function doInitialize();
}
