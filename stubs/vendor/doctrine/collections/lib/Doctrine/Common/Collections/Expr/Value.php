<?php

namespace Doctrine\Common\Collections\Expr;

class Value implements \Doctrine\Common\Collections\Expr\Expression
{
    /** @param mixed $value */
    public function __construct($value)
    {
    }
    /** @return mixed */
    public function getValue()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function visit(\Doctrine\Common\Collections\Expr\ExpressionVisitor $visitor)
    {
    }
}
