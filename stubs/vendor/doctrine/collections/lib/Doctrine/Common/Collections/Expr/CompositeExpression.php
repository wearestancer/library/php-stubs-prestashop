<?php

namespace Doctrine\Common\Collections\Expr;

/**
 * Expression of Expressions combined by AND or OR operation.
 */
class CompositeExpression implements \Doctrine\Common\Collections\Expr\Expression
{
    public const TYPE_AND = 'AND';
    public const TYPE_OR = 'OR';
    /**
     * @param string  $type
     * @param mixed[] $expressions
     *
     * @throws RuntimeException
     */
    public function __construct($type, array $expressions)
    {
    }
    /**
     * Returns the list of expressions nested in this composite.
     *
     * @return Expression[]
     */
    public function getExpressionList()
    {
    }
    /** @return string */
    public function getType()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function visit(\Doctrine\Common\Collections\Expr\ExpressionVisitor $visitor)
    {
    }
}
