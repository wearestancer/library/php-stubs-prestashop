<?php

namespace Doctrine\Common\Collections\Expr;

/**
 * Walks an expression graph and turns it into a PHP closure.
 *
 * This closure can be used with {@Collection#filter()} and is used internally
 * by {@ArrayCollection#select()}.
 */
class ClosureExpressionVisitor extends \Doctrine\Common\Collections\Expr\ExpressionVisitor
{
    /**
     * Accesses the field of a given object. This field has to be public
     * directly or indirectly (through an accessor get*, is*, or a magic
     * method, __get, __call).
     *
     * @param object|mixed[] $object
     * @param string         $field
     *
     * @return mixed
     */
    public static function getObjectFieldValue($object, $field)
    {
    }
    /**
     * Helper for sorting arrays of objects based on multiple fields + orientations.
     *
     * @param string $name
     * @param int    $orientation
     *
     * @return Closure
     */
    public static function sortByField($name, $orientation = 1, ?\Closure $next = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function walkComparison(\Doctrine\Common\Collections\Expr\Comparison $comparison)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function walkValue(\Doctrine\Common\Collections\Expr\Value $value)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function walkCompositeExpression(\Doctrine\Common\Collections\Expr\CompositeExpression $expr)
    {
    }
}
