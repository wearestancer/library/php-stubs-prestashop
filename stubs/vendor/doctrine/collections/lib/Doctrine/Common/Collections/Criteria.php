<?php

namespace Doctrine\Common\Collections;

/**
 * Criteria for filtering Selectable collections.
 *
 * @psalm-consistent-constructor
 */
class Criteria
{
    public const ASC = 'ASC';
    public const DESC = 'DESC';
    /**
     * Creates an instance of the class.
     *
     * @return Criteria
     */
    public static function create()
    {
    }
    /**
     * Returns the expression builder.
     *
     * @return ExpressionBuilder
     */
    public static function expr()
    {
    }
    /**
     * Construct a new Criteria.
     *
     * @param string[]|null $orderings
     * @param int|null      $firstResult
     * @param int|null      $maxResults
     */
    public function __construct(?\Doctrine\Common\Collections\Expr\Expression $expression = null, ?array $orderings = null, $firstResult = null, $maxResults = null)
    {
    }
    /**
     * Sets the where expression to evaluate when this Criteria is searched for.
     *
     * @return $this
     */
    public function where(\Doctrine\Common\Collections\Expr\Expression $expression)
    {
    }
    /**
     * Appends the where expression to evaluate when this Criteria is searched for
     * using an AND with previous expression.
     *
     * @return $this
     */
    public function andWhere(\Doctrine\Common\Collections\Expr\Expression $expression)
    {
    }
    /**
     * Appends the where expression to evaluate when this Criteria is searched for
     * using an OR with previous expression.
     *
     * @return $this
     */
    public function orWhere(\Doctrine\Common\Collections\Expr\Expression $expression)
    {
    }
    /**
     * Gets the expression attached to this Criteria.
     *
     * @return Expression|null
     */
    public function getWhereExpression()
    {
    }
    /**
     * Gets the current orderings of this Criteria.
     *
     * @return string[]
     */
    public function getOrderings()
    {
    }
    /**
     * Sets the ordering of the result of this Criteria.
     *
     * Keys are field and values are the order, being either ASC or DESC.
     *
     * @see Criteria::ASC
     * @see Criteria::DESC
     *
     * @param string[] $orderings
     *
     * @return $this
     */
    public function orderBy(array $orderings)
    {
    }
    /**
     * Gets the current first result option of this Criteria.
     *
     * @return int|null
     */
    public function getFirstResult()
    {
    }
    /**
     * Set the number of first result that this Criteria should return.
     *
     * @param int|null $firstResult The value to set.
     *
     * @return $this
     */
    public function setFirstResult($firstResult)
    {
    }
    /**
     * Gets maxResults.
     *
     * @return int|null
     */
    public function getMaxResults()
    {
    }
    /**
     * Sets maxResults.
     *
     * @param int|null $maxResults The value to set.
     *
     * @return $this
     */
    public function setMaxResults($maxResults)
    {
    }
}
