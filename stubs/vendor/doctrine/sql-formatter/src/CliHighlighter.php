<?php

namespace Doctrine\SqlFormatter;

final class CliHighlighter implements \Doctrine\SqlFormatter\Highlighter
{
    public const HIGHLIGHT_FUNCTIONS = 'functions';
    /**
     * @param array<string, string> $escapeSequences
     */
    public function __construct(array $escapeSequences = [])
    {
    }
    public function highlightToken(int $type, string $value) : string
    {
    }
    public function highlightError(string $value) : string
    {
    }
    public function highlightErrorMessage(string $value) : string
    {
    }
    public function output(string $string) : string
    {
    }
}
