<?php

namespace Doctrine\SqlFormatter;

final class HtmlHighlighter implements \Doctrine\SqlFormatter\Highlighter
{
    public const HIGHLIGHT_PRE = 'pre';
    /**
     * @param array<string, string> $htmlAttributes
     */
    public function __construct(array $htmlAttributes = [], bool $usePre = true)
    {
    }
    public function highlightToken(int $type, string $value) : string
    {
    }
    public function attributes(int $type) : ?string
    {
    }
    public function highlightError(string $value) : string
    {
    }
    public function highlightErrorMessage(string $value) : string
    {
    }
    public function output(string $string) : string
    {
    }
}
