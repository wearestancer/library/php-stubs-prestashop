<?php

namespace Doctrine\SqlFormatter;

final class Cursor
{
    /**
     * @param Token[] $tokens
     */
    public function __construct(array $tokens)
    {
    }
    public function next(?int $exceptTokenType = null) : ?\Doctrine\SqlFormatter\Token
    {
    }
    public function previous(?int $exceptTokenType = null) : ?\Doctrine\SqlFormatter\Token
    {
    }
    public function subCursor() : self
    {
    }
}
