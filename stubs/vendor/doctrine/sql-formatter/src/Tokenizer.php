<?php

namespace Doctrine\SqlFormatter;

/**
 * @internal
 */
final class Tokenizer
{
    /**
     * Stuff that only needs to be done once. Builds regular expressions and
     * sorts the reserved words.
     */
    public function __construct()
    {
    }
    /**
     * Takes a SQL string and breaks it into tokens.
     * Each token is an associative array with type and value.
     *
     * @param string $string The SQL string
     */
    public function tokenize(string $string) : \Doctrine\SqlFormatter\Cursor
    {
    }
}
