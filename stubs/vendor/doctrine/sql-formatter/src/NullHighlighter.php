<?php

namespace Doctrine\SqlFormatter;

final class NullHighlighter implements \Doctrine\SqlFormatter\Highlighter
{
    public function highlightToken(int $type, string $value) : string
    {
    }
    public function highlightError(string $value) : string
    {
    }
    public function highlightErrorMessage(string $value) : string
    {
    }
    public function output(string $string) : string
    {
    }
}
