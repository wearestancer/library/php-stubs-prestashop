<?php

namespace Doctrine\Deprecations\PHPUnit;

trait VerifyDeprecations
{
    /** @var array<string,int> */
    private $doctrineDeprecationsExpectations = [];
    /** @var array<string,int> */
    private $doctrineNoDeprecationsExpectations = [];
    public function expectDeprecationWithIdentifier(string $identifier) : void
    {
    }
    public function expectNoDeprecationWithIdentifier(string $identifier) : void
    {
    }
    /**
     * @before
     */
    public function enableDeprecationTracking() : void
    {
    }
    /**
     * @after
     */
    public function verifyDeprecationsAreTriggered() : void
    {
    }
}
