<?php

namespace DeprecationTests;

class Foo
{
    public static function triggerDependencyWithDeprecation() : void
    {
    }
    public static function triggerDependencyWithDeprecationFromInside() : void
    {
    }
}
