<?php

namespace Doctrine\Foo;

class Bar
{
    public function oldFunc() : void
    {
    }
    public function newFunc() : void
    {
    }
}
