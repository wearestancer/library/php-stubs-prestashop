<?php

namespace ProxyManager\Autoloader;

class Autoloader implements \ProxyManager\Autoloader\AutoloaderInterface
{
    protected $fileLocator;
    protected $classNameInflector;
    public function __construct(\ProxyManager\FileLocator\FileLocatorInterface $fileLocator, \ProxyManager\Inflector\ClassNameInflectorInterface $classNameInflector)
    {
    }
    public function __invoke(string $className) : bool
    {
    }
}
