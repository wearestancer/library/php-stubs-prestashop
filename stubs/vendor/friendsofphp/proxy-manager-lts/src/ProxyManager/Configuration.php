<?php

namespace ProxyManager;

/**
 * Base configuration class for the proxy manager - serves as micro disposable DIC/facade
 */
class Configuration
{
    public const DEFAULT_PROXY_NAMESPACE = 'ProxyManagerGeneratedProxy';
    protected $proxiesTargetDir;
    protected $proxiesNamespace = self::DEFAULT_PROXY_NAMESPACE;
    protected $generatorStrategy;
    protected $proxyAutoloader;
    protected $classNameInflector;
    protected $signatureGenerator;
    protected $signatureChecker;
    protected $classSignatureGenerator;
    public function setProxyAutoloader(\ProxyManager\Autoloader\AutoloaderInterface $proxyAutoloader) : void
    {
    }
    public function getProxyAutoloader() : \ProxyManager\Autoloader\AutoloaderInterface
    {
    }
    public function setProxiesNamespace(string $proxiesNamespace) : void
    {
    }
    public function getProxiesNamespace() : string
    {
    }
    public function setProxiesTargetDir(string $proxiesTargetDir) : void
    {
    }
    public function getProxiesTargetDir() : string
    {
    }
    public function setGeneratorStrategy(\ProxyManager\GeneratorStrategy\GeneratorStrategyInterface $generatorStrategy) : void
    {
    }
    public function getGeneratorStrategy() : \ProxyManager\GeneratorStrategy\GeneratorStrategyInterface
    {
    }
    public function setClassNameInflector(\ProxyManager\Inflector\ClassNameInflectorInterface $classNameInflector) : void
    {
    }
    public function getClassNameInflector() : \ProxyManager\Inflector\ClassNameInflectorInterface
    {
    }
    public function setSignatureGenerator(\ProxyManager\Signature\SignatureGeneratorInterface $signatureGenerator) : void
    {
    }
    public function getSignatureGenerator() : \ProxyManager\Signature\SignatureGeneratorInterface
    {
    }
    public function setSignatureChecker(\ProxyManager\Signature\SignatureCheckerInterface $signatureChecker) : void
    {
    }
    public function getSignatureChecker() : \ProxyManager\Signature\SignatureCheckerInterface
    {
    }
    public function setClassSignatureGenerator(\ProxyManager\Signature\ClassSignatureGeneratorInterface $classSignatureGenerator) : void
    {
    }
    public function getClassSignatureGenerator() : \ProxyManager\Signature\ClassSignatureGeneratorInterface
    {
    }
}
