<?php

namespace ProxyManager\Proxy;

/**
 * Null object marker
 */
interface NullObjectInterface extends \ProxyManager\Proxy\ProxyInterface
{
}
