<?php

namespace ProxyManager\Proxy;

/**
 * Base proxy marker
 *
 * @psalm-template DecoratedClassName of object
 */
interface ProxyInterface
{
}
