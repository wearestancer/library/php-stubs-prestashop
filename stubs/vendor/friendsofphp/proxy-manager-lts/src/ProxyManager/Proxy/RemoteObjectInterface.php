<?php

namespace ProxyManager\Proxy;

/**
 * Remote object marker
 */
interface RemoteObjectInterface extends \ProxyManager\Proxy\ProxyInterface
{
}
