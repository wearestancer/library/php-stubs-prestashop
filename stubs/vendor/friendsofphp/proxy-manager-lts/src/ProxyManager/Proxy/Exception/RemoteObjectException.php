<?php

namespace ProxyManager\Proxy\Exception;

/**
 * Remote object exception
 *
 * @deprecated this exception is not in use anymore, and should not be relied upon
 */
class RemoteObjectException extends \RuntimeException
{
}
