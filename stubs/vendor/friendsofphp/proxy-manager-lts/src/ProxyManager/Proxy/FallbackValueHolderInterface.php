<?php

namespace ProxyManager\Proxy;

/**
 * Fallback value holder object marker
 *
 * @deprecated this interface is not in use anymore, and should not be relied upon
 */
interface FallbackValueHolderInterface extends \ProxyManager\Proxy\ProxyInterface
{
}
