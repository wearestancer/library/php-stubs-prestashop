<?php

namespace ProxyManager\Proxy;

/**
 * Value holder marker
 *
 * @psalm-template WrappedValueHolderType of object
 */
interface ValueHolderInterface extends \ProxyManager\Proxy\ProxyInterface
{
    /**
     * @return object|null the wrapped value
     * @psalm-return WrappedValueHolderType|null
     */
    public function getWrappedValueHolderValue();
}
