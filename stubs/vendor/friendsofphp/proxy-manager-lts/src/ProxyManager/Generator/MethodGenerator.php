<?php

namespace ProxyManager\Generator;

/**
 * Method generator that fixes minor quirks in ZF2's method generator
 */
class MethodGenerator extends \Laminas\Code\Generator\MethodGenerator
{
    protected $hasTentativeReturnType = false;
    /**
     * @return static
     */
    public static function fromReflectionWithoutBodyAndDocBlock(\Laminas\Code\Reflection\MethodReflection $reflectionMethod) : self
    {
    }
    public function getDocBlock() : ?\Laminas\Code\Generator\DocBlockGenerator
    {
    }
    /**
     * {@inheritDoc} override needed to specify type in more detail
     */
    public function getSourceContent() : ?string
    {
    }
}
