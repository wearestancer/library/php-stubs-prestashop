<?php

namespace ProxyManager\Generator;

/**
 * @internal do not use this in your code: it is only here for internal use
 */
class ValueGenerator extends \Laminas\Code\Generator\ValueGenerator
{
    public function __construct($value, ?\ReflectionParameter $reflection = null)
    {
    }
    public function generate() : string
    {
    }
}
