<?php

namespace ProxyManager\Generator\Util;

/**
 * Utility class capable of generating
 * valid class/property/method identifiers
 * with a deterministic attached suffix,
 * in order to prevent property name collisions
 * and tampering from userland
 */
abstract class IdentifierSuffixer
{
    public const VALID_IDENTIFIER_FORMAT = '/^[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]+$/';
    public const DEFAULT_IDENTIFIER = 'g';
    /**
     * Generates a valid unique identifier from the given name,
     * with a suffix attached to it
     */
    public static function getIdentifier(string $name) : string
    {
    }
}
