<?php

namespace ProxyManager\Generator;

/**
 * Class generator that ensures that interfaces/classes that are implemented/extended are FQCNs
 *
 * @internal do not use this in your code: it is only here for internal use
 */
class ClassGenerator extends \Laminas\Code\Generator\ClassGenerator
{
    public function generate() : string
    {
    }
}
