<?php

namespace ProxyManager\Generator;

/**
 * Method generator for magic methods
 */
class MagicMethodGenerator extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * @param ParameterGenerator[]|array[]|string[] $parameters
     */
    public function __construct(\ReflectionClass $originalClass, string $name, array $parameters = [])
    {
    }
}
