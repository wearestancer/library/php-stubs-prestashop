<?php

namespace ProxyManager\GeneratorStrategy;

/**
 * Generator strategy that produces the code and evaluates it at runtime
 */
class EvaluatingGeneratorStrategy implements \ProxyManager\GeneratorStrategy\GeneratorStrategyInterface
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    /**
     * Evaluates the generated code before returning it
     *
     * {@inheritDoc}
     */
    public function generate(\Laminas\Code\Generator\ClassGenerator $classGenerator) : string
    {
    }
}
