<?php

namespace ProxyManager\GeneratorStrategy;

/**
 * Generator strategy that generates the class body
 */
class BaseGeneratorStrategy implements \ProxyManager\GeneratorStrategy\GeneratorStrategyInterface
{
    /**
     * {@inheritDoc}
     *
     * @psalm-suppress MixedInferredReturnType upstream has no declared type
     */
    public function generate(\Laminas\Code\Generator\ClassGenerator $classGenerator) : string
    {
    }
}
