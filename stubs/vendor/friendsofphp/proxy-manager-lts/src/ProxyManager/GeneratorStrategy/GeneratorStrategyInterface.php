<?php

namespace ProxyManager\GeneratorStrategy;

/**
 * Generator strategy interface - defines basic behavior of class generators
 */
interface GeneratorStrategyInterface
{
    /**
     * Generate the provided class
     */
    public function generate(\Laminas\Code\Generator\ClassGenerator $classGenerator) : string;
}
