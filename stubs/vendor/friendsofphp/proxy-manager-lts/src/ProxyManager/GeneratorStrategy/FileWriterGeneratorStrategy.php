<?php

namespace ProxyManager\GeneratorStrategy;

/**
 * Generator strategy that writes the generated classes to disk while generating them
 *
 * {@inheritDoc}
 */
class FileWriterGeneratorStrategy implements \ProxyManager\GeneratorStrategy\GeneratorStrategyInterface
{
    protected $fileLocator;
    public function __construct(\ProxyManager\FileLocator\FileLocatorInterface $fileLocator)
    {
    }
    /**
     * Write generated code to disk and return the class code
     *
     * {@inheritDoc}
     *
     * @throws FileNotWritableException
     */
    public function generate(\Laminas\Code\Generator\ClassGenerator $classGenerator) : string
    {
    }
}
