<?php

namespace ProxyManager\Signature;

/**
 * Applies a signature to a given class generator
 */
interface ClassSignatureGeneratorInterface
{
    /**
     * Applies a signature to a given class generator
     *
     * @param array<string, mixed> $parameters
     */
    public function addSignature(\Laminas\Code\Generator\ClassGenerator $classGenerator, array $parameters) : \Laminas\Code\Generator\ClassGenerator;
}
