<?php

namespace ProxyManager\Signature;

final class SignatureGenerator implements \ProxyManager\Signature\SignatureGeneratorInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritDoc}
     */
    public function generateSignature(array $parameters) : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function generateSignatureKey(array $parameters) : string
    {
    }
}
