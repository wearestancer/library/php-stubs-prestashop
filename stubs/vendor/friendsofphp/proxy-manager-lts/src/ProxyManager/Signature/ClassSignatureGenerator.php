<?php

namespace ProxyManager\Signature;

/**
 * Applies a signature to a given class generator
 */
final class ClassSignatureGenerator implements \ProxyManager\Signature\ClassSignatureGeneratorInterface
{
    public function __construct(\ProxyManager\Signature\SignatureGeneratorInterface $signatureGenerator)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException
     */
    public function addSignature(\Laminas\Code\Generator\ClassGenerator $classGenerator, array $parameters) : \Laminas\Code\Generator\ClassGenerator
    {
    }
}
