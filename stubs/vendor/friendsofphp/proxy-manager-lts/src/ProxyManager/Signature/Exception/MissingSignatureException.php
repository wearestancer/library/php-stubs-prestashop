<?php

namespace ProxyManager\Signature\Exception;

/**
 * Exception for no found signatures
 */
class MissingSignatureException extends \UnexpectedValueException implements \ProxyManager\Signature\Exception\ExceptionInterface
{
    /** @param mixed[] $parameters */
    public static function fromMissingSignature(\ReflectionClass $class, array $parameters, string $expected) : self
    {
    }
}
