<?php

namespace ProxyManager\Signature\Exception;

/**
 * Exception for invalid provided signatures
 */
class InvalidSignatureException extends \UnexpectedValueException implements \ProxyManager\Signature\Exception\ExceptionInterface
{
    /** @param mixed[] $parameters */
    public static function fromInvalidSignature(\ReflectionClass $class, array $parameters, string $signature, string $expected) : self
    {
    }
}
