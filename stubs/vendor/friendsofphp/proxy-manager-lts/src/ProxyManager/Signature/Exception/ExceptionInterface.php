<?php

namespace ProxyManager\Signature\Exception;

/**
 * Exception marker for exceptions from the signature sub-component
 */
interface ExceptionInterface
{
}
