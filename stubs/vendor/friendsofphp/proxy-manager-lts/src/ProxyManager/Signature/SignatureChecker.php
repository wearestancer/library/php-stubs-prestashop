<?php

namespace ProxyManager\Signature;

/**
 * Generator for signatures to be used to check the validity of generated code
 */
final class SignatureChecker implements \ProxyManager\Signature\SignatureCheckerInterface
{
    public function __construct(\ProxyManager\Signature\SignatureGeneratorInterface $signatureGenerator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function checkSignature(\ReflectionClass $class, array $parameters) : void
    {
    }
}
