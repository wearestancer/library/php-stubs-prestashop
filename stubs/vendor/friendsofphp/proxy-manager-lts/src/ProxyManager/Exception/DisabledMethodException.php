<?php

namespace ProxyManager\Exception;

/**
 * Exception for forcefully disabled methods
 */
class DisabledMethodException extends \BadMethodCallException implements \ProxyManager\Exception\ExceptionInterface
{
    public const NAME = self::class;
    public static function disabledMethod(string $method) : self
    {
    }
}
