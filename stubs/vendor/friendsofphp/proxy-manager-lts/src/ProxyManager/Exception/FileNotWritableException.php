<?php

namespace ProxyManager\Exception;

/**
 * Exception for non writable files
 */
class FileNotWritableException extends \UnexpectedValueException implements \ProxyManager\Exception\ExceptionInterface
{
    /**
     * @deprecated
     */
    public static function fromInvalidMoveOperation(string $fromPath, string $toPath) : self
    {
    }
    /**
     * @deprecated
     */
    public static function fromNotWritableDirectory(string $directory) : self
    {
    }
    public static function fromPrevious(\Throwable $previous) : self
    {
    }
}
