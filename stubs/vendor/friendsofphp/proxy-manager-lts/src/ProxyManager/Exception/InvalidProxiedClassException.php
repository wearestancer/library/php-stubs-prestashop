<?php

namespace ProxyManager\Exception;

/**
 * Exception for invalid proxied classes
 */
class InvalidProxiedClassException extends \InvalidArgumentException implements \ProxyManager\Exception\ExceptionInterface
{
    public static function interfaceNotSupported(\ReflectionClass $reflection) : self
    {
    }
    public static function finalClassNotSupported(\ReflectionClass $reflection) : self
    {
    }
    public static function abstractProtectedMethodsNotSupported(\ReflectionClass $reflection) : self
    {
    }
}
