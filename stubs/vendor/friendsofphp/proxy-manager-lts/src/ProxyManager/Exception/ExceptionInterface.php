<?php

namespace ProxyManager\Exception;

/**
 * Base exception class for the proxy manager
 */
interface ExceptionInterface extends \Throwable
{
}
