<?php

namespace ProxyManager\Exception;

/**
 * Exception for invalid proxied classes
 */
class UnsupportedProxiedClassException extends \LogicException implements \ProxyManager\Exception\ExceptionInterface
{
    public static function unsupportedLocalizedReflectionProperty(\ReflectionProperty $property) : self
    {
    }
    public static function nonReferenceableLocalizedReflectionProperties(\ReflectionClass $class, \ProxyManager\ProxyGenerator\Util\Properties $properties) : self
    {
    }
}
