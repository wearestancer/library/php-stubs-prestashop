<?php

namespace ProxyManager\Exception;

/**
 * Exception for invalid directories
 */
class InvalidProxyDirectoryException extends \InvalidArgumentException implements \ProxyManager\Exception\ExceptionInterface
{
    public static function proxyDirectoryNotFound(string $directory) : self
    {
    }
}
