<?php

namespace ProxyManager\Stub;

/**
 * Just an empty instantiable class
 */
final class EmptyClassStub
{
}
