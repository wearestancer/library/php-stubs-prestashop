<?php

namespace ProxyManager\ProxyGenerator\ValueHolder\MethodGenerator;

/**
 * The `__construct` implementation for lazy loading proxies
 */
class Constructor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public static function generateMethod(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $valueHolder) : self
    {
    }
}
