<?php

namespace ProxyManager\ProxyGenerator\ValueHolder\MethodGenerator;

/**
 * Magic `__sleep` for value holder objects
 */
class MagicSleep extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty)
    {
    }
}
