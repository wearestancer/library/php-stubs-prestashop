<?php

namespace ProxyManager\ProxyGenerator\ValueHolder\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\ValueHolderInterface::getWrappedValueHolderValue}
 * for lazy loading value holder objects
 */
class GetWrappedValueHolderValue extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $valueHolderProperty)
    {
    }
}
