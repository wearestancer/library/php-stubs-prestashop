<?php

namespace ProxyManager\ProxyGenerator;

/**
 * Generator for proxies implementing {@see \ProxyManager\Proxy\ValueHolderInterface}
 * and localizing scope of the proxied object at instantiation
 *
 * {@inheritDoc}
 */
class AccessInterceptorScopeLocalizerGenerator implements \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
{
    /**
     * {@inheritDoc}
     *
     * @return void
     *
     * @throws InvalidArgumentException
     * @throws InvalidProxiedClassException
     */
    public function generate(\ReflectionClass $originalClass, \Laminas\Code\Generator\ClassGenerator $classGenerator)
    {
    }
}
