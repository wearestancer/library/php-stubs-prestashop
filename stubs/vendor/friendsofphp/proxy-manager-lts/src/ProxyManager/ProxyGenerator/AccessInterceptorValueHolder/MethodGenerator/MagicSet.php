<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorValueHolder\MethodGenerator;

/**
 * Magic `__set` for method interceptor value holder objects
 */
class MagicSet extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $valueHolder, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors, \ProxyManager\ProxyGenerator\PropertyGenerator\PublicPropertiesMap $publicProperties)
    {
    }
}
