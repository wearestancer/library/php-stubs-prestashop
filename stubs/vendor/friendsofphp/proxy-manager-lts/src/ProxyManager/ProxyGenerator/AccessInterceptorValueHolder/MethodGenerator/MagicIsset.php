<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorValueHolder\MethodGenerator;

/**
 * Magic `__isset` for method interceptor value holder objects
 */
class MagicIsset extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $valueHolder, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors, \ProxyManager\ProxyGenerator\PropertyGenerator\PublicPropertiesMap $publicProperties)
    {
    }
}
