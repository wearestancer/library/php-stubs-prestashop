<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorValueHolder\MethodGenerator\Util;

/**
 * Utility to create pre- and post- method interceptors around a given method body
 *
 * @private - this class is just here as a small utility for this component, don't use it in your own code
 */
class InterceptorGenerator
{
    /**
     * @param string $methodBody the body of the previously generated code.
     *                           It MUST assign the return value to a variable
     *                           `$returnValue` instead of directly returning
     */
    public static function createInterceptedMethodBody(string $methodBody, \ProxyManager\Generator\MethodGenerator $method, \Laminas\Code\Generator\PropertyGenerator $valueHolder, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors, ?\ReflectionMethod $originalMethod) : string
    {
    }
}
