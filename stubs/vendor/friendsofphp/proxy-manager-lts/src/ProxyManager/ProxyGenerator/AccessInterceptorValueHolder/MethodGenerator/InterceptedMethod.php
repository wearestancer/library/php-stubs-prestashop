<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorValueHolder\MethodGenerator;

/**
 * Method with additional pre- and post- interceptor logic in the body
 */
class InterceptedMethod extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public static function generateMethod(\Laminas\Code\Reflection\MethodReflection $originalMethod, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors) : self
    {
    }
}
