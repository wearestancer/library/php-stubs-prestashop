<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorValueHolder\MethodGenerator;

/**
 * Magic `__clone` for lazy loading value holder objects
 */
class MagicClone extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors)
    {
    }
}
