<?php

namespace ProxyManager\ProxyGenerator\PropertyGenerator;

/**
 * Map of public properties that exist in the class being proxied
 */
class PublicPropertiesMap extends \Laminas\Code\Generator\PropertyGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(\ProxyManager\ProxyGenerator\Util\Properties $properties, bool $skipReadOnlyProperties = false)
    {
    }
    public function isEmpty() : bool
    {
    }
}
