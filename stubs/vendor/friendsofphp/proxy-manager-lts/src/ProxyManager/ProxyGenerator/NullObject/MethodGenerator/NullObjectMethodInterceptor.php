<?php

namespace ProxyManager\ProxyGenerator\NullObject\MethodGenerator;

/**
 * Method decorator for null objects
 */
class NullObjectMethodInterceptor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * @return static
     */
    public static function generateMethod(\Laminas\Code\Reflection\MethodReflection $originalMethod) : self
    {
    }
}
