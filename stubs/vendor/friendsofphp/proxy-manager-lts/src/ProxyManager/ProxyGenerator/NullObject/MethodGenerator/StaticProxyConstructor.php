<?php

namespace ProxyManager\ProxyGenerator\NullObject\MethodGenerator;

/**
 * The `staticProxyConstructor` implementation for null object proxies
 */
class StaticProxyConstructor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @param ReflectionClass $originalClass Reflection of the class to proxy
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass)
    {
    }
}
