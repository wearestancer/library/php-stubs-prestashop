<?php

namespace ProxyManager\ProxyGenerator\RemoteObject\MethodGenerator;

/**
 * Magic `__unset` method for remote objects
 */
class MagicUnset extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $adapterProperty)
    {
    }
}
