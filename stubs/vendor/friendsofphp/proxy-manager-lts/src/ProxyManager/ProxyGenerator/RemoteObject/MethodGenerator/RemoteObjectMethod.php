<?php

namespace ProxyManager\ProxyGenerator\RemoteObject\MethodGenerator;

/**
 * Method decorator for remote objects
 */
class RemoteObjectMethod extends \ProxyManager\Generator\MethodGenerator
{
    /** @return static */
    public static function generateMethod(\Laminas\Code\Reflection\MethodReflection $originalMethod, \Laminas\Code\Generator\PropertyGenerator $adapterProperty, \ReflectionClass $originalClass) : self
    {
    }
}
