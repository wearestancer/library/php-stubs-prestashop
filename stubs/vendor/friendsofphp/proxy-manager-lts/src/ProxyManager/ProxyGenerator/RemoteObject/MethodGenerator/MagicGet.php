<?php

namespace ProxyManager\ProxyGenerator\RemoteObject\MethodGenerator;

/**
 * Magic `__get` for remote objects
 */
class MagicGet extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $adapterProperty)
    {
    }
}
