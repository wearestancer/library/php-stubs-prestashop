<?php

namespace ProxyManager\ProxyGenerator\RemoteObject\PropertyGenerator;

/**
 * Property that contains the remote object adapter
 */
class AdapterProperty extends \Laminas\Code\Generator\PropertyGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
    }
}
