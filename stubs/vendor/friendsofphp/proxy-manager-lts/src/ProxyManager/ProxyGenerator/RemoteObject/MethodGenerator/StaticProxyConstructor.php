<?php

namespace ProxyManager\ProxyGenerator\RemoteObject\MethodGenerator;

/**
 * The `staticProxyConstructor` implementation for remote object proxies
 */
class StaticProxyConstructor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @param ReflectionClass   $originalClass Reflection of the class to proxy
     * @param PropertyGenerator $adapter       Adapter property
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $adapter)
    {
    }
}
