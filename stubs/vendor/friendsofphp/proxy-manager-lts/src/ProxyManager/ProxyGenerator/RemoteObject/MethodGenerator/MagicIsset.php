<?php

namespace ProxyManager\ProxyGenerator\RemoteObject\MethodGenerator;

/**
 * Magic `__isset` method for remote objects
 */
class MagicIsset extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $adapterProperty)
    {
    }
}
