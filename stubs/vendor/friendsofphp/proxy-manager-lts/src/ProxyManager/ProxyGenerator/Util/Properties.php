<?php

namespace ProxyManager\ProxyGenerator\Util;

/**
 * DTO containing the list of all non-static proxy properties and utility methods to access them
 * in various formats/collections
 */
final class Properties
{
    public static function fromReflectionClass(\ReflectionClass $reflection) : self
    {
    }
    /**
     * @param array<int, string> $excludedProperties
     */
    public function filter(array $excludedProperties) : self
    {
    }
    public function onlyNonReferenceableProperties() : self
    {
    }
    /** @deprecated Since PHP 7.4.1, all properties can be unset, regardless if typed or not */
    public function onlyPropertiesThatCanBeUnset() : self
    {
    }
    /**
     * Properties that cannot be referenced are non-nullable typed properties that aren't initialised
     */
    public function withoutNonReferenceableProperties() : self
    {
    }
    public function onlyNullableProperties() : self
    {
    }
    public function onlyInstanceProperties() : self
    {
    }
    public function empty() : bool
    {
    }
    /**
     * @return array<string, ReflectionProperty> indexed by the property internal visibility-aware name
     */
    public function getPublicProperties() : array
    {
    }
    /**
     * @return array<string, ReflectionProperty> indexed by the property internal visibility-aware name (\0*\0propertyName)
     */
    public function getProtectedProperties() : array
    {
    }
    /**
     * @return array<string, ReflectionProperty> indexed by the property internal visibility-aware name (\0ClassName\0propertyName)
     */
    public function getPrivateProperties() : array
    {
    }
    /**
     * @return array<string, ReflectionProperty> indexed by the property internal visibility-aware name (\0*\0propertyName)
     */
    public function getAccessibleProperties() : array
    {
    }
    /**
     * @return array<class-string, array<string, ReflectionProperty>> indexed by class name and property name
     */
    public function getGroupedPrivateProperties() : array
    {
    }
    /**
     * @return array<string, ReflectionProperty> indexed by the property internal visibility-aware name (\0*\0propertyName)
     */
    public function getInstanceProperties() : array
    {
    }
}
