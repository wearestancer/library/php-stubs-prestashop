<?php

namespace ProxyManager\ProxyGenerator\Util;

/**
 * Utility class used to filter methods that can be proxied
 */
final class ProxiedMethodsFilter
{
    /**
     * @internal
     */
    public const DEFAULT_EXCLUDED = ['__get', '__set', '__isset', '__unset', '__clone', '__sleep', '__wakeup'];
    /**
     * @param ReflectionClass    $class    reflection class from which methods should be extracted
     * @param array<int, string> $excluded methods to be ignored
     *
     * @return ReflectionMethod[]
     */
    public static function getProxiedMethods(\ReflectionClass $class, ?array $excluded = null) : array
    {
    }
    /**
     * @param ReflectionClass    $class    reflection class from which methods should be extracted
     * @param array<int, string> $excluded methods to be ignored
     *
     * @return ReflectionMethod[]
     */
    public static function getAbstractProxiedMethods(\ReflectionClass $class, ?array $excluded = null) : array
    {
    }
}
