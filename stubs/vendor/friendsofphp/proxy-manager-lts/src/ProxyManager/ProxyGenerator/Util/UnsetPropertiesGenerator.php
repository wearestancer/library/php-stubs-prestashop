<?php

namespace ProxyManager\ProxyGenerator\Util;

/**
 * Generates code necessary to unset all the given properties from a particular given instance string name
 */
final class UnsetPropertiesGenerator
{
    public static function generateSnippet(\ProxyManager\ProxyGenerator\Util\Properties $properties, string $instanceName) : string
    {
    }
}
