<?php

namespace ProxyManager\ProxyGenerator\Util;

/**
 * Generates code necessary to simulate a fatal error in case of unauthorized
 * access to class members in magic methods even when in child classes and dealing
 * with protected members.
 */
class PublicScopeSimulator
{
    public const OPERATION_SET = 'set';
    public const OPERATION_GET = 'get';
    public const OPERATION_ISSET = 'isset';
    public const OPERATION_UNSET = 'unset';
    /**
     * Generates code for simulating access to a property from the scope that is accessing a proxy.
     * This is done by introspecting `debug_backtrace()` and then binding a closure to the scope
     * of the parent caller.
     *
     * @param string            $operationType      operation to execute: one of 'get', 'set', 'isset' or 'unset'
     * @param string            $nameParameter      name of the `name` parameter of the magic method
     * @param string|null       $valueParameter     name of the `value` parameter of the magic method, only to be
     *                                              used with $operationType 'set'
     * @param PropertyGenerator $valueHolder        name of the property containing the target object from which
     *                                              to read the property. `$this` if none provided
     * @param string|null       $returnPropertyName name of the property to which we want to assign the result of
     *                                              the operation. Return directly if none provided
     * @param string|null       $interfaceName      name of the proxified interface if any
     * @psalm-param $operationType self::OPERATION_*
     *
     * @throws InvalidArgumentException
     */
    public static function getPublicAccessSimulationCode(string $operationType, string $nameParameter, ?string $valueParameter = null, ?\Laminas\Code\Generator\PropertyGenerator $valueHolder = null, ?string $returnPropertyName = null, ?\ReflectionClass $originalClass = null) : string
    {
    }
}
