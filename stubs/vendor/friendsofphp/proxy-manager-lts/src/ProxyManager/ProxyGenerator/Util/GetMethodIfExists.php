<?php

namespace ProxyManager\ProxyGenerator\Util;

/**
 * Internal utility class - allows fetching a method from a given class, if it exists
 */
final class GetMethodIfExists
{
    public static function get(\ReflectionClass $class, string $method) : ?\ReflectionMethod
    {
    }
}
