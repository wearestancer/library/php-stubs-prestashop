<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\MethodGenerator;

/**
 * Magic `__isset` method for lazy loading ghost objects
 */
class MagicIsset extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\MethodGenerator $callInitializer, \ProxyManager\ProxyGenerator\PropertyGenerator\PublicPropertiesMap $publicProperties, \ProxyManager\ProxyGenerator\LazyLoadingGhost\PropertyGenerator\ProtectedPropertiesMap $protectedProperties, \ProxyManager\ProxyGenerator\LazyLoadingGhost\PropertyGenerator\PrivatePropertiesMap $privateProperties)
    {
    }
}
