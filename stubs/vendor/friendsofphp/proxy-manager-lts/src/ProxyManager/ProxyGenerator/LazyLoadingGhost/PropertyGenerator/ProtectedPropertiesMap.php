<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\PropertyGenerator;

/**
 * Property that contains the protected instance lazy-loadable properties of an object
 */
class ProtectedPropertiesMap extends \Laminas\Code\Generator\PropertyGenerator
{
    public const KEY_DEFAULT_VALUE = 'defaultValue';
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ProxyManager\ProxyGenerator\Util\Properties $properties)
    {
    }
}
