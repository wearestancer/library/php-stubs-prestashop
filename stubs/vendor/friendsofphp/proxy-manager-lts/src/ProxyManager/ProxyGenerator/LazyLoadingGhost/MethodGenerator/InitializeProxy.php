<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\LazyLoadingInterface::initializeProxy}
 * for lazy loading ghost objects
 */
class InitializeProxy extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\MethodGenerator $callInitializer)
    {
    }
}
