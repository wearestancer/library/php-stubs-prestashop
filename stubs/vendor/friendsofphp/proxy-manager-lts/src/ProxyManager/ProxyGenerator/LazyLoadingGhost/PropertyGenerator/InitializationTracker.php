<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\PropertyGenerator;

/**
 * Property that contains the initializer for a lazy object
 */
class InitializationTracker extends \Laminas\Code\Generator\PropertyGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
    }
}
