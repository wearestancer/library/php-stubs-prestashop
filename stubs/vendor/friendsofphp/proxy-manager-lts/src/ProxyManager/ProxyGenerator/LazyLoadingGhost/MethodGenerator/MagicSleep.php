<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\MethodGenerator;

/**
 * Magic `__sleep` for lazy loading ghost objects
 */
class MagicSleep extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\MethodGenerator $callInitializer)
    {
    }
}
