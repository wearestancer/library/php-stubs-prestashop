<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\LazyLoadingInterface::isProxyInitialized}
 * for lazy loading value holder objects
 */
class CallInitializer extends \ProxyManager\Generator\MethodGenerator
{
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\PropertyGenerator $initTracker, \ProxyManager\ProxyGenerator\Util\Properties $properties)
    {
    }
}
