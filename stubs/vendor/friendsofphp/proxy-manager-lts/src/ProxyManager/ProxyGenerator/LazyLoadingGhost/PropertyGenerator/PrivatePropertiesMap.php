<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\PropertyGenerator;

/**
 * Property that contains the initializer for a lazy object
 */
class PrivatePropertiesMap extends \Laminas\Code\Generator\PropertyGenerator
{
    public const KEY_DEFAULT_VALUE = 'defaultValue';
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ProxyManager\ProxyGenerator\Util\Properties $properties)
    {
    }
    /**
     * @return list<string>
     */
    public function getReadOnlyPropertyNames() : array
    {
    }
}
