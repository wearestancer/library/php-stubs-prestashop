<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingGhost\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\LazyLoadingInterface::setProxyInitializer}
 * for lazy loading value holder objects
 */
class SetProxyInitializer extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $initializerProperty)
    {
    }
}
