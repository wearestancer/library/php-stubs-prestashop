<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptor\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\AccessInterceptorInterface::setMethodSuffixInterceptor}
 * for access interceptor objects
 */
class SetMethodSuffixInterceptor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $suffixInterceptor)
    {
    }
}
