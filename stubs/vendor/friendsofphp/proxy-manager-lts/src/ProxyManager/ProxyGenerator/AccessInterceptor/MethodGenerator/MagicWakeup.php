<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptor\MethodGenerator;

/**
 * Magic `__wakeup` for lazy loading value holder objects
 */
class MagicWakeup extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\ReflectionClass $originalClass)
    {
    }
}
