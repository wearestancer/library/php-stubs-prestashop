<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptor\PropertyGenerator;

/**
 * Property that contains the interceptor for operations to be executed after method execution
 */
class MethodSuffixInterceptors extends \Laminas\Code\Generator\PropertyGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
    }
}
