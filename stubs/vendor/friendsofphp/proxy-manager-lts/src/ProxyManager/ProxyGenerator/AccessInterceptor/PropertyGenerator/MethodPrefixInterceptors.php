<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptor\PropertyGenerator;

/**
 * Property that contains the interceptor for operations to be executed before method execution
 */
class MethodPrefixInterceptors extends \Laminas\Code\Generator\PropertyGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
    }
}
