<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingValueHolder\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\LazyLoadingInterface::initializeProxy}
 * for lazy loading value holder objects
 */
class InitializeProxy extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty)
    {
    }
}
