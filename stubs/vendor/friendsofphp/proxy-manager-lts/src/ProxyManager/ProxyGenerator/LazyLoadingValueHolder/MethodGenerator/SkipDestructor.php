<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingValueHolder\MethodGenerator;

/**
 * Destructor that skips the original destructor when the proxy is not initialized.
 */
class SkipDestructor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty)
    {
    }
}
