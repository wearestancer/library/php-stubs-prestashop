<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingValueHolder\MethodGenerator;

/**
 * Implementation for {@see \ProxyManager\Proxy\LazyLoadingInterface::isProxyInitialized}
 * for lazy loading value holder objects
 */
class IsProxyInitialized extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $valueHolderProperty)
    {
    }
}
