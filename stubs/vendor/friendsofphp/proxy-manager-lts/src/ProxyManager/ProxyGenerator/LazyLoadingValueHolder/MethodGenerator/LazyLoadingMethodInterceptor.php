<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingValueHolder\MethodGenerator;

/**
 * Method decorator for lazy loading value holder objects
 */
class LazyLoadingMethodInterceptor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public static function generateMethod(\Laminas\Code\Reflection\MethodReflection $originalMethod, \Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty) : self
    {
    }
}
