<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingValueHolder\PropertyGenerator;

/**
 * Property that contains the wrapped value of a lazy loading proxy
 */
class ValueHolderProperty extends \Laminas\Code\Generator\PropertyGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $type)
    {
    }
}
