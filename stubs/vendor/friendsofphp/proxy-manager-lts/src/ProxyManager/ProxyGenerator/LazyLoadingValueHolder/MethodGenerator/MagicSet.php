<?php

namespace ProxyManager\ProxyGenerator\LazyLoadingValueHolder\MethodGenerator;

/**
 * Magic `__set` for lazy loading value holder objects
 */
class MagicSet extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $initializerProperty, \Laminas\Code\Generator\PropertyGenerator $valueHolderProperty, \ProxyManager\ProxyGenerator\PropertyGenerator\PublicPropertiesMap $publicProperties)
    {
    }
}
