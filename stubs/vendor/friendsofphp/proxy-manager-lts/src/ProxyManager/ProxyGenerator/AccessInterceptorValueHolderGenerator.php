<?php

namespace ProxyManager\ProxyGenerator;

/**
 * Generator for proxies implementing {@see \ProxyManager\Proxy\ValueHolderInterface}
 * and {@see \ProxyManager\Proxy\AccessInterceptorInterface}
 *
 * {@inheritDoc}
 */
class AccessInterceptorValueHolderGenerator implements \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
{
    /**
     * {@inheritDoc}
     *
     * @return void
     *
     * @throws InvalidArgumentException
     * @throws InvalidProxiedClassException
     */
    public function generate(\ReflectionClass $originalClass, \Laminas\Code\Generator\ClassGenerator $classGenerator)
    {
    }
}
