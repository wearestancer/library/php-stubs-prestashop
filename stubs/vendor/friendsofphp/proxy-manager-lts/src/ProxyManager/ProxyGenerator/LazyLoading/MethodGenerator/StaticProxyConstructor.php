<?php

namespace ProxyManager\ProxyGenerator\LazyLoading\MethodGenerator;

/**
 * The `staticProxyConstructor` implementation for lazy loading proxies
 */
class StaticProxyConstructor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Static constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Laminas\Code\Generator\PropertyGenerator $initializerProperty, \ProxyManager\ProxyGenerator\Util\Properties $properties)
    {
    }
}
