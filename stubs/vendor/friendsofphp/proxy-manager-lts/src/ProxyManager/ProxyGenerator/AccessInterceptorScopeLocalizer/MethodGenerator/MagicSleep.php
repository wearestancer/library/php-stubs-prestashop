<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorScopeLocalizer\MethodGenerator;

/**
 * Magic `__sleep` for lazy loading ghost objects
 */
class MagicSleep extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors)
    {
    }
}
