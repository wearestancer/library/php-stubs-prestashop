<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorScopeLocalizer\MethodGenerator;

/**
 * The `bindProxyProperties` method implementation for access interceptor scope localizers
 */
class BindProxyProperties extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors)
    {
    }
}
