<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorScopeLocalizer\MethodGenerator;

/**
 * Magic `__isset` method for lazy loading ghost objects
 */
class MagicIsset extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors)
    {
    }
}
