<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorScopeLocalizer\MethodGenerator;

/**
 * Magic `__set` for lazy loading ghost objects
 */
class MagicSet extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors)
    {
    }
}
