<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorScopeLocalizer\MethodGenerator;

/**
 * The `staticProxyConstructor` implementation for an access interceptor scope localizer proxy
 */
class StaticProxyConstructor extends \ProxyManager\Generator\MethodGenerator
{
    /**
     * Constructor
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass)
    {
    }
}
