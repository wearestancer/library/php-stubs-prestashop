<?php

namespace ProxyManager\ProxyGenerator\AccessInterceptorScopeLocalizer\MethodGenerator;

/**
 * Magic `__unset` method for lazy loading ghost objects
 */
class MagicUnset extends \ProxyManager\Generator\MagicMethodGenerator
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(\ReflectionClass $originalClass, \Laminas\Code\Generator\PropertyGenerator $prefixInterceptors, \Laminas\Code\Generator\PropertyGenerator $suffixInterceptors)
    {
    }
}
