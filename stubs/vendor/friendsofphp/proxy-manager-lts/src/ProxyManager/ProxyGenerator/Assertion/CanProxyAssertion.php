<?php

namespace ProxyManager\ProxyGenerator\Assertion;

/**
 * Assertion that verifies that a class can be proxied
 */
final class CanProxyAssertion
{
    /**
     * Disabled constructor: not meant to be instantiated
     *
     * @throws BadMethodCallException
     */
    public function __construct()
    {
    }
    /**
     * @throws InvalidProxiedClassException
     */
    public static function assertClassCanBeProxied(\ReflectionClass $originalClass, bool $allowInterfaces = true) : void
    {
    }
}
