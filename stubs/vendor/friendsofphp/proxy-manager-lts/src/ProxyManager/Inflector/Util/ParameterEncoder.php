<?php

namespace ProxyManager\Inflector\Util;

/**
 * Encodes parameters into a class-name safe string
 */
class ParameterEncoder
{
    /**
     * Converts the given parameters into a set of characters that are safe to
     * use in a class name
     *
     * @param mixed[] $parameters
     */
    public function encodeParameters(array $parameters) : string
    {
    }
}
