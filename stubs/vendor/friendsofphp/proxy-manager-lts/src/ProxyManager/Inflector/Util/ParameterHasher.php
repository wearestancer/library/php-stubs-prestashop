<?php

namespace ProxyManager\Inflector\Util;

/**
 * Converts given parameters into a likely unique hash
 */
class ParameterHasher
{
    /**
     * Converts the given parameters into a likely-unique hash
     *
     * @param mixed[] $parameters
     */
    public function hashParameters(array $parameters) : string
    {
    }
}
