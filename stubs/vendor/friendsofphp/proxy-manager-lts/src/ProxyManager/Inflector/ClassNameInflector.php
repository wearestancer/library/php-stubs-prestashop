<?php

namespace ProxyManager\Inflector;

final class ClassNameInflector implements \ProxyManager\Inflector\ClassNameInflectorInterface
{
    public function __construct(string $proxyNamespace)
    {
    }
    /**
     * {@inheritDoc}
     *
     * @psalm-suppress MoreSpecificReturnType we ignore these issues because classes may not have been loaded yet
     */
    public function getUserClassName(string $className) : string
    {
    }
    /**
     * {@inheritDoc}
     *
     * @psalm-suppress MoreSpecificReturnType we ignore these issues because classes may not have been loaded yet
     */
    public function getProxyClassName(string $className, array $options = []) : string
    {
    }
    public function isProxyClassName(string $className) : bool
    {
    }
}
