<?php

namespace ProxyManager\Factory;

/**
 * Factory responsible of producing proxy objects
 */
class AccessInterceptorValueHolderFactory extends \ProxyManager\Factory\AbstractBaseFactory
{
    public function __construct(?\ProxyManager\Configuration $configuration = null)
    {
    }
    /**
     * @param object                 $instance           the object to be wrapped within the value holder
     * @param array<string, Closure> $prefixInterceptors an array (indexed by method name) of interceptor closures to be called
     *                                       before method logic is executed
     * @param array<string, Closure> $suffixInterceptors an array (indexed by method name) of interceptor closures to be called
     *                                       after method logic is executed
     * @psalm-param RealObjectType $instance
     * @psalm-param array<string, callable(
     *   RealObjectType&AccessInterceptorInterface<RealObjectType>=,
     *   RealObjectType=,
     *   string=,
     *   array<string, mixed>=,
     *   bool=
     * ) : mixed> $prefixInterceptors
     * @psalm-param array<string, callable(
     *   RealObjectType&AccessInterceptorInterface<RealObjectType>=,
     *   RealObjectType=,
     *   string=,
     *   array<string, mixed>=,
     *   mixed=,
     *   bool=
     * ) : mixed> $suffixInterceptors
     *
     * @psalm-return RealObjectType&AccessInterceptorInterface<RealObjectType>&ValueHolderInterface<RealObjectType>&AccessInterceptorValueHolderInterface<RealObjectType>
     *
     * @throws InvalidSignatureException
     * @throws MissingSignatureException
     * @throws OutOfBoundsException
     *
     * @psalm-template RealObjectType of object
     * @psalm-suppress MixedInferredReturnType We ignore type checks here, since `staticProxyConstructor` is not
     *                                         interfaced (by design)
     */
    public function createProxy($instance, array $prefixInterceptors = [], array $suffixInterceptors = []) : \ProxyManager\Proxy\AccessInterceptorValueHolderInterface
    {
    }
    protected function getGenerator() : \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
    {
    }
}
