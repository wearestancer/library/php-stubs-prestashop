<?php

namespace ProxyManager\Factory;

/**
 * Factory responsible of producing virtual proxy instances
 */
class LazyLoadingValueHolderFactory extends \ProxyManager\Factory\AbstractBaseFactory
{
    public function __construct(?\ProxyManager\Configuration $configuration = null)
    {
    }
    /**
     * @param array<string, mixed> $proxyOptions
     * @psalm-param class-string<RealObjectType> $className
     * @psalm-param Closure(
     *   RealObjectType|null=,
     *   RealObjectType&ValueHolderInterface<RealObjectType>&VirtualProxyInterface=,
     *   string=,
     *   array<string, mixed>=,
     *   ?Closure=
     * ) : bool $initializer
     * @psalm-param array{skipDestructor?: bool, fluentSafe?: bool} $proxyOptions
     *
     * @psalm-return RealObjectType&ValueHolderInterface<RealObjectType>&VirtualProxyInterface
     *
     * @psalm-template RealObjectType of object
     * @psalm-suppress MixedInferredReturnType We ignore type checks here, since `staticProxyConstructor` is not
     *                                         interfaced (by design)
     */
    public function createProxy(string $className, \Closure $initializer, array $proxyOptions = []) : \ProxyManager\Proxy\VirtualProxyInterface
    {
    }
    protected function getGenerator() : \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
    {
    }
}
