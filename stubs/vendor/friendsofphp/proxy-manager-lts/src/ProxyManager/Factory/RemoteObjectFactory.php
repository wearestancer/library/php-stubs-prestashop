<?php

namespace ProxyManager\Factory;

/**
 * Factory responsible of producing remote proxy objects
 */
class RemoteObjectFactory extends \ProxyManager\Factory\AbstractBaseFactory
{
    protected $adapter;
    /**
     * {@inheritDoc}
     *
     * @param AdapterInterface $adapter
     * @param Configuration    $configuration
     */
    public function __construct(\ProxyManager\Factory\RemoteObject\AdapterInterface $adapter, ?\ProxyManager\Configuration $configuration = null)
    {
    }
    /**
     * @psalm-param RealObjectType|class-string<RealObjectType> $instanceOrClassName
     *
     * @psalm-return RealObjectType&RemoteObjectInterface
     *
     * @throws InvalidSignatureException
     * @throws MissingSignatureException
     * @throws OutOfBoundsException
     *
     * @psalm-template RealObjectType of object
     * @psalm-suppress MixedInferredReturnType We ignore type checks here, since `staticProxyConstructor` is not
     *                                         interfaced (by design)
     */
    public function createProxy($instanceOrClassName) : \ProxyManager\Proxy\RemoteObjectInterface
    {
    }
    protected function getGenerator() : \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
    {
    }
}
