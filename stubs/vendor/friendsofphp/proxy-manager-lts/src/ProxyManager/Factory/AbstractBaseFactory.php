<?php

namespace ProxyManager\Factory;

/**
 * Base factory common logic
 */
abstract class AbstractBaseFactory
{
    protected $configuration;
    public function __construct(?\ProxyManager\Configuration $configuration = null)
    {
    }
    /**
     * Generate a proxy from a class name
     *
     * @param array<string, mixed> $proxyOptions
     * @psalm-param class-string<RealObjectType> $className
     *
     * @psalm-return class-string<RealObjectType>
     *
     * @throws InvalidSignatureException
     * @throws MissingSignatureException
     * @throws OutOfBoundsException
     *
     * @psalm-template RealObjectType of object
     */
    protected function generateProxy(string $className, array $proxyOptions = []) : string
    {
    }
    protected abstract function getGenerator() : \ProxyManager\ProxyGenerator\ProxyGeneratorInterface;
}
