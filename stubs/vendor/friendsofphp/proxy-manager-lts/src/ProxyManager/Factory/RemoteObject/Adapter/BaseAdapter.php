<?php

namespace ProxyManager\Factory\RemoteObject\Adapter;

/**
 * Remote Object base adapter
 */
abstract class BaseAdapter implements \ProxyManager\Factory\RemoteObject\AdapterInterface
{
    protected $client;
    /**
     * Service name mapping
     *
     * @var array<string, string>
     */
    protected $map = [];
    /**
     * Constructor
     *
     * @param array<string, string> $map map of service names to their aliases
     */
    public function __construct(\Laminas\Server\Client $client, array $map = [])
    {
    }
    /**
     * {@inheritDoc}
     */
    public function call(string $wrappedClass, string $method, array $params = [])
    {
    }
    /**
     * Get the service name will be used by the adapter
     */
    protected abstract function getServiceName(string $wrappedClass, string $method) : string;
}
