<?php

namespace ProxyManager\Factory;

/**
 * Factory responsible of producing proxy objects
 */
class NullObjectFactory extends \ProxyManager\Factory\AbstractBaseFactory
{
    public function __construct(?\ProxyManager\Configuration $configuration = null)
    {
    }
    /**
     * @param object|string $instanceOrClassName the object to be wrapped or interface to transform to null object
     * @psalm-param RealObjectType|class-string<RealObjectType> $instanceOrClassName
     *
     * @psalm-return RealObjectType&NullObjectInterface
     *
     * @throws InvalidSignatureException
     * @throws MissingSignatureException
     * @throws OutOfBoundsException
     *
     * @psalm-template RealObjectType of object
     * @psalm-suppress MixedInferredReturnType We ignore type checks here, since `staticProxyConstructor` is not
     *                                         interfaced (by design)
     */
    public function createProxy($instanceOrClassName) : \ProxyManager\Proxy\NullObjectInterface
    {
    }
    protected function getGenerator() : \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
    {
    }
}
