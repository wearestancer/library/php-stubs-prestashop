<?php

namespace ProxyManager\FileLocator;

class FileLocator implements \ProxyManager\FileLocator\FileLocatorInterface
{
    protected $proxiesDirectory;
    /**
     * @throws InvalidProxyDirectoryException
     */
    public function __construct(string $proxiesDirectory)
    {
    }
    public function getProxyFileName(string $className) : string
    {
    }
}
