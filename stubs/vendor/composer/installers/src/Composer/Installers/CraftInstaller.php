<?php

namespace Composer\Installers;

/**
 * Installer for Craft Plugins
 */
class CraftInstaller extends \Composer\Installers\BaseInstaller
{
    const NAME_PREFIX = 'craft';
    const NAME_SUFFIX = 'plugin';
    protected $locations = array('plugin' => 'craft/plugins/{$name}/');
    /**
     * Strip `craft-` prefix and/or `-plugin` suffix from package names
     *
     * @param  array $vars
     *
     * @return array
     */
    public final function inflectPackageVars($vars)
    {
    }
}
