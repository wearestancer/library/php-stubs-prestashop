<?php

namespace Composer\Installers;

class OxidInstaller extends \Composer\Installers\BaseInstaller
{
    const VENDOR_PATTERN = '/^modules\\/(?P<vendor>.+)\\/.+/';
    protected $locations = array('module' => 'modules/{$name}/', 'theme' => 'application/views/{$name}/', 'out' => 'out/{$name}/');
    /**
     * getInstallPath
     *
     * @param PackageInterface $package
     * @param string $frameworkType
     * @return string
     */
    public function getInstallPath(\Composer\Package\PackageInterface $package, $frameworkType = '')
    {
    }
    /**
     * prepareVendorDirectory
     *
     * Makes sure there is a vendormetadata.php file inside
     * the vendor folder if there is a vendor folder.
     *
     * @param string $installPath
     * @return void
     */
    protected function prepareVendorDirectory($installPath)
    {
    }
}
