<?php

namespace Composer\Installers;

class TastyIgniterInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('extension' => 'extensions/{$vendor}/{$name}/', 'theme' => 'themes/{$name}/');
    /**
     * Format package name.
     *
     * Cut off leading 'ti-ext-' or 'ti-theme-' if present.
     * Strip vendor name of characters that is not alphanumeric or an underscore
     *
     */
    public function inflectPackageVars($vars)
    {
    }
}
