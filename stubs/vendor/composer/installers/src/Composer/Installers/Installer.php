<?php

namespace Composer\Installers;

class Installer extends \Composer\Installer\LibraryInstaller
{
    /**
     * Installer constructor.
     *
     * Disables installers specified in main composer extra installer-disable
     * list
     *
     * @param IOInterface          $io
     * @param Composer             $composer
     * @param string               $type
     * @param Filesystem|null      $filesystem
     * @param BinaryInstaller|null $binaryInstaller
     */
    public function __construct(\Composer\IO\IOInterface $io, \Composer\Composer $composer, $type = 'library', \Composer\Util\Filesystem $filesystem = null, \Composer\Installer\BinaryInstaller $binaryInstaller = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(\Composer\Package\PackageInterface $package)
    {
    }
    public function uninstall(\Composer\Repository\InstalledRepositoryInterface $repo, \Composer\Package\PackageInterface $package)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
    }
    /**
     * Finds a supported framework type if it exists and returns it
     *
     * @param  string       $type
     * @return string|false
     */
    protected function findFrameworkType($type)
    {
    }
    /**
     * Get the second part of the regular expression to check for support of a
     * package type
     *
     * @param  string $frameworkType
     * @return string
     */
    protected function getLocationPattern($frameworkType)
    {
    }
    /**
     * Look for installers set to be disabled in composer's extra config and
     * remove them from the list of supported installers.
     *
     * Globals:
     *  - true, "all", and "*" - disable all installers.
     *  - false - enable all installers (useful with
     *     wikimedia/composer-merge-plugin or similar)
     *
     * @return void
     */
    protected function removeDisabledInstallers()
    {
    }
}
