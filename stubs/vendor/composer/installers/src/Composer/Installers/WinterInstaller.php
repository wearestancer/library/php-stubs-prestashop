<?php

namespace Composer\Installers;

class WinterInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'modules/{$name}/', 'plugin' => 'plugins/{$vendor}/{$name}/', 'theme' => 'themes/{$name}/');
    /**
     * Format package name.
     *
     * For package type winter-plugin, cut off a trailing '-plugin' if present.
     *
     * For package type winter-theme, cut off a trailing '-theme' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectModuleVars($vars)
    {
    }
    protected function inflectPluginVars($vars)
    {
    }
    protected function inflectThemeVars($vars)
    {
    }
}
