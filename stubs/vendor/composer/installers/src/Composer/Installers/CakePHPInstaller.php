<?php

namespace Composer\Installers;

class CakePHPInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('plugin' => 'Plugin/{$name}/');
    /**
     * Format package name to CamelCase
     */
    public function inflectPackageVars($vars)
    {
    }
    /**
     * Change the default plugin location when cakephp >= 3.0
     */
    public function getLocations()
    {
    }
    /**
     * Check if CakePHP version matches against a version
     *
     * @param string $matcher
     * @param string $version
     * @return bool
     * @phpstan-param Constraint::STR_OP_* $matcher
     */
    protected function matchesCakeVersion($matcher, $version)
    {
    }
}
