<?php

namespace Composer\Installers;

/**
 * Installer for Bitrix Framework. Supported types of extensions:
 * - `bitrix-d7-module` — copy the module to directory `bitrix/modules/<vendor>.<name>`.
 * - `bitrix-d7-component` — copy the component to directory `bitrix/components/<vendor>/<name>`.
 * - `bitrix-d7-template` — copy the template to directory `bitrix/templates/<vendor>_<name>`.
 *
 * You can set custom path to directory with Bitrix kernel in `composer.json`:
 *
 * ```json
 * {
 *      "extra": {
 *          "bitrix-dir": "s1/bitrix"
 *      }
 * }
 * ```
 *
 * @author Nik Samokhvalov <nik@samokhvalov.info>
 * @author Denis Kulichkin <onexhovia@gmail.com>
 */
class BitrixInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array(
        'module' => '{$bitrix_dir}/modules/{$name}/',
        // deprecated, remove on the major release (Backward compatibility will be broken)
        'component' => '{$bitrix_dir}/components/{$name}/',
        // deprecated, remove on the major release (Backward compatibility will be broken)
        'theme' => '{$bitrix_dir}/templates/{$name}/',
        // deprecated, remove on the major release (Backward compatibility will be broken)
        'd7-module' => '{$bitrix_dir}/modules/{$vendor}.{$name}/',
        'd7-component' => '{$bitrix_dir}/components/{$vendor}/{$name}/',
        'd7-template' => '{$bitrix_dir}/templates/{$vendor}_{$name}/',
    );
    /**
     * {@inheritdoc}
     */
    public function inflectPackageVars($vars)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function templatePath($path, array $vars = array())
    {
    }
    /**
     * Duplicates search packages.
     *
     * @param string $path
     * @param array $vars
     */
    protected function checkDuplicates($path, array $vars = array())
    {
    }
}
