<?php

namespace Composer\Installers;

class SiteDirectInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'modules/{$vendor}/{$name}/', 'plugin' => 'plugins/{$vendor}/{$name}/');
    public function inflectPackageVars($vars)
    {
    }
    protected function parseVars($vars)
    {
    }
}
