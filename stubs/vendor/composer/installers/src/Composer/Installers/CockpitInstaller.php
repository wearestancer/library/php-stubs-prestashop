<?php

namespace Composer\Installers;

class CockpitInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'cockpit/modules/addons/{$name}/');
    /**
     * Format module name.
     *
     * Strip `module-` prefix from package name.
     *
     * {@inheritDoc}
     */
    public function inflectPackageVars($vars)
    {
    }
    public function inflectModuleVars($vars)
    {
    }
}
