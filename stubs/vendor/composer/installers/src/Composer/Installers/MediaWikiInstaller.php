<?php

namespace Composer\Installers;

class MediaWikiInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('core' => 'core/', 'extension' => 'extensions/{$name}/', 'skin' => 'skins/{$name}/');
    /**
     * Format package name.
     *
     * For package type mediawiki-extension, cut off a trailing '-extension' if present and transform
     * to CamelCase keeping existing uppercase chars.
     *
     * For package type mediawiki-skin, cut off a trailing '-skin' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectExtensionVars($vars)
    {
    }
    protected function inflectSkinVars($vars)
    {
    }
}
