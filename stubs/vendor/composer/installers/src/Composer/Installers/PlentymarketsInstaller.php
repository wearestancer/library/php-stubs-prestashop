<?php

namespace Composer\Installers;

class PlentymarketsInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('plugin' => '{$name}/');
    /**
     * Remove hyphen, "plugin" and format to camelcase
     * @param array $vars
     *
     * @return array
     */
    public function inflectPackageVars($vars)
    {
    }
}
