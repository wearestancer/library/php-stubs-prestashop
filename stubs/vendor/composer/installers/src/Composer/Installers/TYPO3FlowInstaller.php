<?php

namespace Composer\Installers;

/**
 * An installer to handle TYPO3 Flow specifics when installing packages.
 */
class TYPO3FlowInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('package' => 'Packages/Application/{$name}/', 'framework' => 'Packages/Framework/{$name}/', 'plugin' => 'Packages/Plugins/{$name}/', 'site' => 'Packages/Sites/{$name}/', 'boilerplate' => 'Packages/Boilerplates/{$name}/', 'build' => 'Build/{$name}/');
    /**
     * Modify the package name to be a TYPO3 Flow style key.
     *
     * @param  array $vars
     * @return array
     */
    public function inflectPackageVars($vars)
    {
    }
}
