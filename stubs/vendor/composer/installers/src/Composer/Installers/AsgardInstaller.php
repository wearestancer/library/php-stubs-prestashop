<?php

namespace Composer\Installers;

class AsgardInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'Modules/{$name}/', 'theme' => 'Themes/{$name}/');
    /**
     * Format package name.
     *
     * For package type asgard-module, cut off a trailing '-plugin' if present.
     *
     * For package type asgard-theme, cut off a trailing '-theme' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectPluginVars($vars)
    {
    }
    protected function inflectThemeVars($vars)
    {
    }
}
