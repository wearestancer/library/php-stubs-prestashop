<?php

namespace Composer\Installers;

/**
 * Plugin/theme installer for shopware
 * @author Benjamin Boit
 */
class ShopwareInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('backend-plugin' => 'engine/Shopware/Plugins/Local/Backend/{$name}/', 'core-plugin' => 'engine/Shopware/Plugins/Local/Core/{$name}/', 'frontend-plugin' => 'engine/Shopware/Plugins/Local/Frontend/{$name}/', 'theme' => 'templates/{$name}/', 'plugin' => 'custom/plugins/{$name}/', 'frontend-theme' => 'themes/Frontend/{$name}/');
    /**
     * Transforms the names
     * @param  array $vars
     * @return array
     */
    public function inflectPackageVars($vars)
    {
    }
}
