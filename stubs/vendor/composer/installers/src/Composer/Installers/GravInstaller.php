<?php

namespace Composer\Installers;

class GravInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('plugin' => 'user/plugins/{$name}/', 'theme' => 'user/themes/{$name}/');
    /**
     * Format package name
     *
     * @param array $vars
     *
     * @return array
     */
    public function inflectPackageVars($vars)
    {
    }
}
