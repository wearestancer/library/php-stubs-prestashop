<?php

namespace Composer\Installers;

/**
 * An installer to handle TAO extensions.
 */
class TaoInstaller extends \Composer\Installers\BaseInstaller
{
    const EXTRA_TAO_EXTENSION_NAME = 'tao-extension-name';
    protected $locations = array('extension' => '{$name}');
    public function inflectPackageVars($vars)
    {
    }
}
