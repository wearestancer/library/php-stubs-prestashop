<?php

namespace Composer\Installers;

class VgmcpInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('bundle' => 'src/{$vendor}/{$name}/', 'theme' => 'themes/{$name}/');
    /**
     * Format package name.
     *
     * For package type vgmcp-bundle, cut off a trailing '-bundle' if present.
     *
     * For package type vgmcp-theme, cut off a trailing '-theme' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectPluginVars($vars)
    {
    }
    protected function inflectThemeVars($vars)
    {
    }
}
