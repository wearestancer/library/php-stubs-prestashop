<?php

namespace Composer\Installers;

class CroogoInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('plugin' => 'Plugin/{$name}/', 'theme' => 'View/Themed/{$name}/');
    /**
     * Format package name to CamelCase
     */
    public function inflectPackageVars($vars)
    {
    }
}
