<?php

namespace Composer\Installers;

class MayaInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'modules/{$name}/');
    /**
     * Format package name.
     *
     * For package type maya-module, cut off a trailing '-module' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectModuleVars($vars)
    {
    }
}
