<?php

namespace Composer\Installers;

class SyDESInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'app/modules/{$name}/', 'theme' => 'themes/{$name}/');
    /**
     * Format module name.
     *
     * Strip `sydes-` prefix and a trailing '-theme' or '-module' from package name if present.
     *
     * {@inerhitDoc}
     */
    public function inflectPackageVars($vars)
    {
    }
    public function inflectModuleVars($vars)
    {
    }
    protected function inflectThemeVars($vars)
    {
    }
}
