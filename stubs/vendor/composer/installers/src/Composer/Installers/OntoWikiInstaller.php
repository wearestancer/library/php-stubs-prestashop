<?php

namespace Composer\Installers;

class OntoWikiInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('extension' => 'extensions/{$name}/', 'theme' => 'extensions/themes/{$name}/', 'translation' => 'extensions/translations/{$name}/');
    /**
     * Format package name to lower case and remove ".ontowiki" suffix
     */
    public function inflectPackageVars($vars)
    {
    }
}
