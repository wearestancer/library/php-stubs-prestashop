<?php

namespace Composer\Installers;

class DokuWikiInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('plugin' => 'lib/plugins/{$name}/', 'template' => 'lib/tpl/{$name}/');
    /**
     * Format package name.
     *
     * For package type dokuwiki-plugin, cut off a trailing '-plugin',
     * or leading dokuwiki_ if present.
     *
     * For package type dokuwiki-template, cut off a trailing '-template' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectPluginVars($vars)
    {
    }
    protected function inflectTemplateVars($vars)
    {
    }
}
