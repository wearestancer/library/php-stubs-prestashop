<?php

namespace Composer\Installers;

class PxcmsInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'app/Modules/{$name}/', 'theme' => 'themes/{$name}/');
    /**
     * Format package name.
     *
     * @param array $vars
     *
     * @return array
     */
    public function inflectPackageVars($vars)
    {
    }
    /**
     * For package type pxcms-module, cut off a trailing '-plugin' if present.
     *
     * return string
     */
    protected function inflectModuleVars($vars)
    {
    }
    /**
     * For package type pxcms-module, cut off a trailing '-plugin' if present.
     *
     * return string
     */
    protected function inflectThemeVars($vars)
    {
    }
}
