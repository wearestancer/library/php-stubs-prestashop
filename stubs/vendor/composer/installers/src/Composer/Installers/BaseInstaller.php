<?php

namespace Composer\Installers;

abstract class BaseInstaller
{
    protected $locations = array();
    protected $composer;
    protected $package;
    protected $io;
    /**
     * Initializes base installer.
     *
     * @param PackageInterface $package
     * @param Composer         $composer
     * @param IOInterface      $io
     */
    public function __construct(\Composer\Package\PackageInterface $package = null, \Composer\Composer $composer = null, \Composer\IO\IOInterface $io = null)
    {
    }
    /**
     * Return the install path based on package type.
     *
     * @param  PackageInterface $package
     * @param  string           $frameworkType
     * @return string
     */
    public function getInstallPath(\Composer\Package\PackageInterface $package, $frameworkType = '')
    {
    }
    /**
     * For an installer to override to modify the vars per installer.
     *
     * @param  array<string, string> $vars This will normally receive array{name: string, vendor: string, type: string}
     * @return array<string, string>
     */
    public function inflectPackageVars($vars)
    {
    }
    /**
     * Gets the installer's locations
     *
     * @return array<string, string> map of package types => install path
     */
    public function getLocations()
    {
    }
    /**
     * Replace vars in a path
     *
     * @param  string                $path
     * @param  array<string, string> $vars
     * @return string
     */
    protected function templatePath($path, array $vars = array())
    {
    }
    /**
     * Search through a passed paths array for a custom install path.
     *
     * @param  array  $paths
     * @param  string $name
     * @param  string $type
     * @param  string $vendor = NULL
     * @return string|false
     */
    protected function mapCustomInstallPaths(array $paths, $name, $type, $vendor = NULL)
    {
    }
}
