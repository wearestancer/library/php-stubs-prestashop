<?php

namespace Composer\Installers;

class SilverStripeInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => '{$name}/', 'theme' => 'themes/{$name}/');
    /**
     * Return the install path based on package type.
     *
     * Relies on built-in BaseInstaller behaviour with one exception: silverstripe/framework
     * must be installed to 'sapphire' and not 'framework' if the version is <3.0.0
     *
     * @param  PackageInterface $package
     * @param  string           $frameworkType
     * @return string
     */
    public function getInstallPath(\Composer\Package\PackageInterface $package, $frameworkType = '')
    {
    }
}
