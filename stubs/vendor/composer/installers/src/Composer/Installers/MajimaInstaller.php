<?php

namespace Composer\Installers;

/**
 * Plugin/theme installer for majima
 * @author David Neustadt
 */
class MajimaInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('plugin' => 'plugins/{$name}/');
    /**
     * Transforms the names
     * @param  array $vars
     * @return array
     */
    public function inflectPackageVars($vars)
    {
    }
}
