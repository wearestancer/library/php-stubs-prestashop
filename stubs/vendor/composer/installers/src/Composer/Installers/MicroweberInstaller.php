<?php

namespace Composer\Installers;

class MicroweberInstaller extends \Composer\Installers\BaseInstaller
{
    protected $locations = array('module' => 'userfiles/modules/{$install_item_dir}/', 'module-skin' => 'userfiles/modules/{$install_item_dir}/templates/', 'template' => 'userfiles/templates/{$install_item_dir}/', 'element' => 'userfiles/elements/{$install_item_dir}/', 'vendor' => 'vendor/{$install_item_dir}/', 'components' => 'components/{$install_item_dir}/');
    /**
     * Format package name.
     *
     * For package type microweber-module, cut off a trailing '-module' if present
     *
     * For package type microweber-template, cut off a trailing '-template' if present.
     *
     */
    public function inflectPackageVars($vars)
    {
    }
    protected function inflectTemplateVars($vars)
    {
    }
    protected function inflectTemplatesVars($vars)
    {
    }
    protected function inflectCoreVars($vars)
    {
    }
    protected function inflectModuleVars($vars)
    {
    }
    protected function inflectModulesVars($vars)
    {
    }
    protected function inflectSkinVars($vars)
    {
    }
    protected function inflectElementVars($vars)
    {
    }
}
