<?php

namespace League\Event;

trait EmitterAwareTrait
{
    /**
     * The emitter instance.
     *
     * @var EmitterInterface|null
     */
    protected $emitter;
    /**
     * Set the Emitter.
     *
     * @param EmitterInterface|null $emitter
     *
     * @return $this
     */
    public function setEmitter(\League\Event\EmitterInterface $emitter = null)
    {
    }
    /**
     * Get the Emitter.
     *
     * @return EmitterInterface
     */
    public function getEmitter()
    {
    }
}
