<?php

namespace League\Event;

trait GeneratorTrait
{
    /**
     * The registered events.
     *
     * @var EventInterface[]
     */
    protected $events = [];
    /**
     * Add an event.
     *
     * @param EventInterface $event
     *
     * @return $this
     */
    protected function addEvent(\League\Event\EventInterface $event)
    {
    }
    /**
     * Release all the added events.
     *
     * @return EventInterface[]
     */
    public function releaseEvents()
    {
    }
}
