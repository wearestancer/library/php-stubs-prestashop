<?php

namespace League\Event;

class CallbackListener implements \League\Event\ListenerInterface
{
    /**
     * The callback.
     *
     * @var callable
     */
    protected $callback;
    /**
     * Create a new callback listener instance.
     *
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
    }
    /**
     * Get the callback.
     *
     * @return callable
     */
    public function getCallback()
    {
    }
    /**
     * @inheritdoc
     */
    public function handle(\League\Event\EventInterface $event)
    {
    }
    /**
     * @inheritdoc
     */
    public function isListener($listener)
    {
    }
    /**
     * Named constructor
     *
     * @param callable $callable
     *
     * @return static
     */
    public static function fromCallable(callable $callable)
    {
    }
}
