<?php

namespace League\Event;

class Emitter implements \League\Event\EmitterInterface
{
    /**
     * The registered listeners.
     *
     * @var array
     */
    protected $listeners = [];
    /**
     * The sorted listeners
     *
     * Listeners will get sorted and stored for re-use.
     *
     * @var ListenerInterface[]
     */
    protected $sortedListeners = [];
    /**
     * @inheritdoc
     */
    public function addListener($event, $listener, $priority = self::P_NORMAL)
    {
    }
    /**
     * @inheritdoc
     */
    public function addOneTimeListener($event, $listener, $priority = self::P_NORMAL)
    {
    }
    /**
     * @inheritdoc
     */
    public function useListenerProvider(\League\Event\ListenerProviderInterface $provider)
    {
    }
    /**
     * @inheritdoc
     */
    public function removeListener($event, $listener)
    {
    }
    /**
     * @inheritdoc
     */
    public function removeAllListeners($event)
    {
    }
    /**
     * Ensure the input is a listener.
     *
     * @param ListenerInterface|callable $listener
     *
     * @throws InvalidArgumentException
     *
     * @return ListenerInterface
     */
    protected function ensureListener($listener)
    {
    }
    /**
     * @inheritdoc
     */
    public function hasListeners($event)
    {
    }
    /**
     * @inheritdoc
     */
    public function getListeners($event)
    {
    }
    /**
     * Get the listeners sorted by priority for a given event.
     *
     * @param string $event
     *
     * @return ListenerInterface[]
     */
    protected function getSortedListeners($event)
    {
    }
    /**
     * @inheritdoc
     */
    public function emit($event)
    {
    }
    /**
     * @inheritdoc
     */
    public function emitBatch(array $events)
    {
    }
    /**
     * @inheritdoc
     */
    public function emitGeneratedEvents(\League\Event\GeneratorInterface $generator)
    {
    }
    /**
     * Invoke the listeners for an event.
     *
     * @param string         $name
     * @param EventInterface $event
     * @param array          $arguments
     *
     * @return void
     */
    protected function invokeListeners($name, \League\Event\EventInterface $event, array $arguments)
    {
    }
    /**
     * Prepare an event for emitting.
     *
     * @param string|EventInterface $event
     *
     * @return array
     */
    protected function prepareEvent($event)
    {
    }
    /**
     * Ensure event input is of type EventInterface or convert it.
     *
     * @param string|EventInterface $event
     *
     * @throws InvalidArgumentException
     *
     * @return EventInterface
     */
    protected function ensureEvent($event)
    {
    }
    /**
     * Clear the sorted listeners for an event
     *
     * @param $event
     */
    protected function clearSortedListeners($event)
    {
    }
}
