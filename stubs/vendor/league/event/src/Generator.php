<?php

namespace League\Event;

class Generator implements \League\Event\GeneratorInterface
{
    use \League\Event\GeneratorTrait {
        addEvent as public;
    }
}
