<?php

namespace League\Event;

class ListenerAcceptor implements \League\Event\ListenerAcceptorInterface
{
    /**
     * The emitter instance.
     *
     * @var EmitterInterface|null
     */
    protected $emitter;
    /**
     * Constructor
     *
     * @param EmitterInterface $emitter
     */
    public function __construct(\League\Event\EmitterInterface $emitter)
    {
    }
    /**
     * @inheritdoc
     */
    public function addListener($event, $listener, $priority = self::P_NORMAL)
    {
    }
    /**
     * @inheritdoc
     */
    public function addOneTimeListener($event, $listener, $priority = self::P_NORMAL)
    {
    }
}
