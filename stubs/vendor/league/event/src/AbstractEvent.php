<?php

namespace League\Event;

abstract class AbstractEvent implements \League\Event\EventInterface
{
    /**
     * Has propagation stopped?
     *
     * @var bool
     */
    protected $propagationStopped = false;
    /**
     * The emitter instance.
     *
     * @var EmitterInterface|null
     */
    protected $emitter;
    /**
     * @inheritdoc
     */
    public function setEmitter(\League\Event\EmitterInterface $emitter)
    {
    }
    /**
     * @inheritdoc
     */
    public function getEmitter()
    {
    }
    /**
     * @inheritdoc
     */
    public function stopPropagation()
    {
    }
    /**
     * @inheritdoc
     */
    public function isPropagationStopped()
    {
    }
    /**
     * @inheritdoc
     */
    public function getName()
    {
    }
}
