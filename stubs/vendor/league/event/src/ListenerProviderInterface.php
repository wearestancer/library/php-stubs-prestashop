<?php

namespace League\Event;

interface ListenerProviderInterface
{
    /**
     * Provide event
     *
     * @param ListenerAcceptorInterface $listenerAcceptor
     *
     * @return $this
     */
    public function provideListeners(\League\Event\ListenerAcceptorInterface $listenerAcceptor);
}
