<?php

namespace League\Event;

class OneTimeListener implements \League\Event\ListenerInterface
{
    /**
     * The listener instance.
     *
     * @var ListenerInterface
     */
    protected $listener;
    /**
     * Create a new one time listener instance.
     *
     * @param ListenerInterface $listener
     */
    public function __construct(\League\Event\ListenerInterface $listener)
    {
    }
    /**
     * Get the wrapped listener.
     *
     * @return ListenerInterface
     */
    public function getWrappedListener()
    {
    }
    /**
     * @inheritdoc
     */
    public function handle(\League\Event\EventInterface $event)
    {
    }
    /**
     * @inheritdoc
     */
    public function isListener($listener)
    {
    }
}
