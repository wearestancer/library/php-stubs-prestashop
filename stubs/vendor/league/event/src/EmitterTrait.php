<?php

namespace League\Event;

trait EmitterTrait
{
    use \League\Event\EmitterAwareTrait;
    /**
     * Add a listener for an event.
     *
     * The first parameter should be the event name, and the second should be
     * the event listener. It may implement the League\Event\ListenerInterface
     * or simply be "callable".
     *
     * @param string                     $event
     * @param ListenerInterface|callable $listener
     * @param int                        $priority
     *
     * @return $this
     */
    public function addListener($event, $listener, $priority = \League\Event\ListenerAcceptorInterface::P_NORMAL)
    {
    }
    /**
     * Add a one time listener for an event.
     *
     * The first parameter should be the event name, and the second should be
     * the event listener. It may implement the League\Event\ListenerInterface
     * or simply be "callable".
     *
     * @param string                     $event
     * @param ListenerInterface|callable $listener
     * @param int                        $priority
     *
     * @return $this
     */
    public function addOneTimeListener($event, $listener, $priority = \League\Event\ListenerAcceptorInterface::P_NORMAL)
    {
    }
    /**
     * Remove a specific listener for an event.
     *
     * The first parameter should be the event name, and the second should be
     * the event listener. It may implement the League\Event\ListenerInterface
     * or simply be "callable".
     *
     * @param string                     $event
     * @param ListenerInterface|callable $listener
     *
     * @return $this
     */
    public function removeListener($event, $listener)
    {
    }
    /**
     * Remove all listeners for an event.
     *
     * The first parameter should be the event name. All event listeners will
     * be removed.
     *
     * @param string $event
     *
     * @return $this
     */
    public function removeAllListeners($event)
    {
    }
    /**
     * Add listeners from a provider.
     *
     * @param ListenerProviderInterface $provider
     *
     * @return $this
     */
    public function useListenerProvider(\League\Event\ListenerProviderInterface $provider)
    {
    }
    /**
     * Emit an event.
     *
     * @param string|EventInterface $event
     *
     * @return EventInterface
     */
    public function emit($event)
    {
    }
}
