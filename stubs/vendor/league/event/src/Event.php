<?php

namespace League\Event;

class Event extends \League\Event\AbstractEvent
{
    /**
     * The event name.
     *
     * @var string
     */
    protected $name;
    /**
     * Create a new event instance.
     *
     * @param string $name
     */
    public function __construct($name)
    {
    }
    /**
     * @inheritdoc
     */
    public function getName()
    {
    }
    /**
     * Create a new event instance.
     *
     * @param string $name
     *
     * @return static
     */
    public static function named($name)
    {
    }
}
