<?php

namespace League\Event;

class BufferedEmitter extends \League\Event\Emitter
{
    /**
     * @var EventInterface[]
     */
    protected $bufferedEvents = [];
    /**
     * @inheritdoc
     */
    public function emit($event)
    {
    }
    /**
     * @inheritdoc
     */
    public function emitBatch(array $events)
    {
    }
    /**
     * Emit the buffered events.
     *
     * @return array
     */
    public function emitBufferedEvents()
    {
    }
}
