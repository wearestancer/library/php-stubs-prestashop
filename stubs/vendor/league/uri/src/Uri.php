<?php

namespace League\Uri;

final class Uri implements \League\Uri\Contracts\UriInterface
{
    /**
     * {@inheritDoc}
     */
    public static function __set_state(array $components) : self
    {
    }
    /**
     * Create a new instance from a URI and a Base URI.
     *
     * The returned URI must be absolute.
     *
     * @param mixed      $uri      the input URI to create
     * @param null|mixed $base_uri the base URI used for reference
     */
    public static function createFromBaseUri($uri, $base_uri = null) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * Create a new instance from a string.
     *
     * @param string|mixed $uri
     */
    public static function createFromString($uri = '') : self
    {
    }
    /**
     * Create a new instance from a hash of parse_url parts.
     *
     * Create an new instance from a hash representation of the URI similar
     * to PHP parse_url function result
     *
     * @param array<string, mixed> $components
     */
    public static function createFromComponents(array $components = []) : self
    {
    }
    /**
     * Create a new instance from a data file path.
     *
     * @param resource|null $context
     *
     * @throws FileinfoSupportMissing If ext/fileinfo is not installed
     * @throws SyntaxError            If the file does not exist or is not readable
     */
    public static function createFromDataPath(string $path, $context = null) : self
    {
    }
    /**
     * Create a new instance from a Unix path string.
     */
    public static function createFromUnixPath(string $uri = '') : self
    {
    }
    /**
     * Create a new instance from a local Windows path string.
     */
    public static function createFromWindowsPath(string $uri = '') : self
    {
    }
    /**
     * Create a new instance from a URI object.
     *
     * @param Psr7UriInterface|UriInterface $uri the input URI to create
     */
    public static function createFromUri($uri) : self
    {
    }
    /**
     * Create a new instance from the environment.
     */
    public static function createFromServer(array $server) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function __toString() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function jsonSerialize() : string
    {
    }
    /**
     * {@inheritDoc}
     *
     * @return array{scheme:?string, user_info:?string, host:?string, port:?int, path:string, query:?string, fragment:?string}
     */
    public function __debugInfo() : array
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getScheme() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAuthority() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getUserInfo() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getHost() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPort() : ?int
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPath() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getQuery() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getFragment() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withScheme($scheme) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withUserInfo($user, $password = null) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withHost($host) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withPort($port) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withPath($path) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withQuery($query) : \League\Uri\Contracts\UriInterface
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withFragment($fragment) : \League\Uri\Contracts\UriInterface
    {
    }
}
