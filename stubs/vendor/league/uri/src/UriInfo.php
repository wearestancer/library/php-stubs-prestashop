<?php

namespace League\Uri;

final class UriInfo
{
    /**
     * Tell whether the URI represents an absolute URI.
     *
     * @param Psr7UriInterface|UriInterface $uri
     */
    public static function isAbsolute($uri) : bool
    {
    }
    /**
     * Tell whether the URI represents a network path.
     *
     * @param Psr7UriInterface|UriInterface $uri
     */
    public static function isNetworkPath($uri) : bool
    {
    }
    /**
     * Tell whether the URI represents an absolute path.
     *
     * @param Psr7UriInterface|UriInterface $uri
     */
    public static function isAbsolutePath($uri) : bool
    {
    }
    /**
     * Tell whether the URI represents a relative path.
     *
     * @param Psr7UriInterface|UriInterface $uri
     */
    public static function isRelativePath($uri) : bool
    {
    }
    /**
     * Tell whether both URI refers to the same document.
     *
     * @param Psr7UriInterface|UriInterface $uri
     * @param Psr7UriInterface|UriInterface $base_uri
     */
    public static function isSameDocument($uri, $base_uri) : bool
    {
    }
    /**
     * Returns the URI origin property as defined by WHATWG URL living standard.
     *
     * {@see https://url.spec.whatwg.org/#origin}
     *
     * For URI without a special scheme the method returns null
     * For URI with the file scheme the method will return null (as this is left to the implementation decision)
     * For URI with a special scheme the method returns the scheme followed by its authority (without the userinfo part)
     *
     * @param Psr7UriInterface|UriInterface $uri
     */
    public static function getOrigin($uri) : ?string
    {
    }
}
