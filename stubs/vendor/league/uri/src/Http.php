<?php

namespace League\Uri;

final class Http implements \Psr\Http\Message\UriInterface, \JsonSerializable
{
    /**
     * Static method called by PHP's var export.
     */
    public static function __set_state(array $components) : self
    {
    }
    /**
     * Create a new instance from a string.
     *
     * @param string|mixed $uri
     */
    public static function createFromString($uri = '') : self
    {
    }
    /**
     * Create a new instance from a hash of parse_url parts.
     *
     * @param array $components a hash representation of the URI similar
     *                          to PHP parse_url function result
     */
    public static function createFromComponents(array $components) : self
    {
    }
    /**
     * Create a new instance from the environment.
     */
    public static function createFromServer(array $server) : self
    {
    }
    /**
     * Create a new instance from a URI and a Base URI.
     *
     * The returned URI must be absolute.
     *
     * @param mixed $uri      the input URI to create
     * @param mixed $base_uri the base URI used for reference
     */
    public static function createFromBaseUri($uri, $base_uri = null) : self
    {
    }
    /**
     * Create a new instance from a URI object.
     *
     * @param Psr7UriInterface|UriInterface $uri the input URI to create
     */
    public static function createFromUri($uri) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getScheme() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getAuthority() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getUserInfo() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getHost() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPort() : ?int
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getPath() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getQuery() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getFragment() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withScheme($scheme) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withUserInfo($user, $password = null) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withHost($host) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withPort($port) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withPath($path) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withQuery($query) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function withFragment($fragment) : self
    {
    }
    /**
     * {@inheritDoc}
     */
    public function __toString() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function jsonSerialize() : string
    {
    }
}
