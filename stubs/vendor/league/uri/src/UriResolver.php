<?php

namespace League\Uri;

final class UriResolver
{
    /**
     * @var array<string,int>
     */
    const DOT_SEGMENTS = ['.' => 1, '..' => 1];
    /**
     * Resolve an URI against a base URI using RFC3986 rules.
     *
     * If the first argument is a UriInterface the method returns a UriInterface object
     * If the first argument is a Psr7UriInterface the method returns a Psr7UriInterface object
     *
     * @param Psr7UriInterface|UriInterface $uri
     * @param Psr7UriInterface|UriInterface $base_uri
     *
     * @return Psr7UriInterface|UriInterface
     */
    public static function resolve($uri, $base_uri)
    {
    }
    /**
     * Relativize an URI according to a base URI.
     *
     * This method MUST retain the state of the submitted URI instance, and return
     * an URI instance of the same type that contains the applied modifications.
     *
     * This method MUST be transparent when dealing with error and exceptions.
     * It MUST not alter of silence them apart from validating its own parameters.
     *
     * @param Psr7UriInterface|UriInterface $uri
     * @param Psr7UriInterface|UriInterface $base_uri
     *
     * @return Psr7UriInterface|UriInterface
     */
    public static function relativize($uri, $base_uri)
    {
    }
}
