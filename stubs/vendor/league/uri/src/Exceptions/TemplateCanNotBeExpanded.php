<?php

namespace League\Uri\Exceptions;

class TemplateCanNotBeExpanded extends \InvalidArgumentException implements \League\Uri\Contracts\UriException
{
    public static function dueToUnableToProcessValueListWithPrefix(string $variableName) : self
    {
    }
    public static function dueToNestedListOfValue(string $variableName) : self
    {
    }
}
