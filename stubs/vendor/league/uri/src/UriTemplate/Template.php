<?php

namespace League\Uri\UriTemplate;

final class Template
{
    /**
     * {@inheritDoc}
     */
    public static function __set_state(array $properties) : self
    {
    }
    /**
     * @param object|string $template a string or an object with the __toString method
     *
     * @throws \TypeError  if the template is not a string or an object with the __toString method
     * @throws SyntaxError if the template contains invalid expressions
     * @throws SyntaxError if the template contains invalid variable specification
     */
    public static function createFromString($template) : self
    {
    }
    public function toString() : string
    {
    }
    /**
     * @return array<string>
     */
    public function variableNames() : array
    {
    }
    /**
     * @throws TemplateCanNotBeExpanded if the variables is an array and a ":" modifier needs to be applied
     * @throws TemplateCanNotBeExpanded if the variables contains nested array values
     */
    public function expand(\League\Uri\UriTemplate\VariableBag $variables) : string
    {
    }
}
