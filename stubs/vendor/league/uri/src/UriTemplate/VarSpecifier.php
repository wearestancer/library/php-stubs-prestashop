<?php

namespace League\Uri\UriTemplate;

final class VarSpecifier
{
    /**
     * {@inheritDoc}
     */
    public static function __set_state(array $properties) : self
    {
    }
    public static function createFromString(string $specification) : self
    {
    }
    public function toString() : string
    {
    }
    public function name() : string
    {
    }
    public function modifier() : string
    {
    }
    public function position() : int
    {
    }
}
