<?php

namespace League\Uri\UriTemplate;

final class VariableBag
{
    /**
     * @param iterable<string,mixed> $variables
     */
    public function __construct(iterable $variables = [])
    {
    }
    public static function __set_state(array $properties) : self
    {
    }
    /**
     * @return array<string,string|array<string>>
     */
    public function all() : array
    {
    }
    /**
     * Fetches the variable value if none found returns null.
     *
     * @return null|string|array<string>
     */
    public function fetch(string $name)
    {
    }
    /**
     * @param string|array<string> $value
     */
    public function assign(string $name, $value) : void
    {
    }
    /**
     * Replaces elements from passed variables into the current instance.
     */
    public function replace(\League\Uri\UriTemplate\VariableBag $variables) : self
    {
    }
}
