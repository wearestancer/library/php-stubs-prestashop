<?php

namespace League\Uri\UriTemplate;

final class Expression
{
    /**
     * {@inheritDoc}
     */
    public static function __set_state(array $properties) : self
    {
    }
    /**
     * @throws SyntaxError if the expression is invalid
     * @throws SyntaxError if the operator used in the expression is invalid
     * @throws SyntaxError if the variable specifiers is invalid
     */
    public static function createFromString(string $expression) : self
    {
    }
    /**
     * Returns the expression string representation.
     *
     */
    public function toString() : string
    {
    }
    /**
     * @return array<string>
     */
    public function variableNames() : array
    {
    }
    public function expand(\League\Uri\UriTemplate\VariableBag $variables) : string
    {
    }
}
