<?php

namespace League\Uri\Idna;

/**
 * @see https://unicode-org.github.io/icu-docs/apidoc/released/icu4c/uidna_8h.html
 */
final class IdnaInfo
{
    /**
     * @param array{result:string, isTransitionalDifferent:bool, errors:int} $infos
     */
    public static function fromIntl(array $infos) : self
    {
    }
    /**
     * @param array{result:string, isTransitionalDifferent:bool, errors:int} $properties
     */
    public static function __set_state(array $properties) : self
    {
    }
    public function result() : string
    {
    }
    public function isTransitionalDifferent() : bool
    {
    }
    public function errors() : int
    {
    }
    public function error(int $error) : ?string
    {
    }
    /**
     * @return array<int, string>
     */
    public function errorList() : array
    {
    }
}
