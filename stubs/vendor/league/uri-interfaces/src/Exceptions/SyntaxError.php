<?php

namespace League\Uri\Exceptions;

class SyntaxError extends \InvalidArgumentException implements \League\Uri\Contracts\UriException
{
}
