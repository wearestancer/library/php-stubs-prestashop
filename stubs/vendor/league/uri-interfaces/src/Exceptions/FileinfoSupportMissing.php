<?php

namespace League\Uri\Exceptions;

class FileinfoSupportMissing extends \RuntimeException implements \League\Uri\Contracts\UriException
{
}
