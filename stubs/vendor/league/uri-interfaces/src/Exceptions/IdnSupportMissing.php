<?php

namespace League\Uri\Exceptions;

class IdnSupportMissing extends \RuntimeException implements \League\Uri\Contracts\UriException
{
}
