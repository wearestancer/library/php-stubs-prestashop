<?php

namespace League\Uri\Exceptions;

final class IdnaConversionFailed extends \League\Uri\Exceptions\SyntaxError
{
    public static function dueToIDNAError(string $domain, \League\Uri\Idna\IdnaInfo $idnaInfo) : self
    {
    }
    public static function dueToInvalidHost(string $domain) : self
    {
    }
    public function idnaInfo() : ?\League\Uri\Idna\IdnaInfo
    {
    }
}
