<?php

namespace League\Uri\Contracts;

interface FragmentInterface extends \League\Uri\Contracts\UriComponentInterface
{
    /**
     * Returns the decoded fragment.
     */
    public function decoded() : ?string;
}
