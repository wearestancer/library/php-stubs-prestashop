<?php

namespace League\Uri\Contracts;

interface PortInterface extends \League\Uri\Contracts\UriComponentInterface
{
    /**
     * Returns the integer representation of the Port.
     */
    public function toInt() : ?int;
}
