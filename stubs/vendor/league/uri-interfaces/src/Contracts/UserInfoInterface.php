<?php

namespace League\Uri\Contracts;

interface UserInfoInterface extends \League\Uri\Contracts\UriComponentInterface
{
    /**
     * Returns the user component part.
     */
    public function getUser() : ?string;
    /**
     * Returns the pass component part.
     */
    public function getPass() : ?string;
    /**
     * Returns an instance with the specified user and/or pass.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified user.
     *
     * An empty user is equivalent to removing the user information.
     *
     * @param ?string $user
     * @param ?string $pass
     */
    public function withUserInfo(?string $user, ?string $pass = null) : self;
}
