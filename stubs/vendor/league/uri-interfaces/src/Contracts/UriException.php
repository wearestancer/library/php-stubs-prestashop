<?php

namespace League\Uri\Contracts;

interface UriException extends \Throwable
{
}
