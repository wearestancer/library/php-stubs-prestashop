<?php

namespace League\Tactician\Logger;

/**
 * Add support for writing a message to the log whenever a command is received,
 * handled or failed.
 */
class LoggerMiddleware implements \League\Tactician\Middleware
{
    /**
     * @param Formatter $formatter
     * @param LoggerInterface $logger
     */
    public function __construct(\League\Tactician\Logger\Formatter\Formatter $formatter, \Psr\Log\LoggerInterface $logger)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function execute($command, callable $next)
    {
    }
}
