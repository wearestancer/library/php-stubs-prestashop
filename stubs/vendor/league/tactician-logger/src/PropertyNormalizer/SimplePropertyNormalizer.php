<?php

namespace League\Tactician\Logger\PropertyNormalizer;

/**
 * Quick'n'dirty property normalizer that logs the first level properties
 *
 * Does not recurse into sub-objects or arrays.
 *
 * This is done in an extremely inefficient manner, so please never use this in
 * a production context, only for local debugging.
 */
class SimplePropertyNormalizer implements \League\Tactician\Logger\PropertyNormalizer\PropertyNormalizer
{
    /**
     * @param object $command
     * @return array
     */
    public function normalize($command)
    {
    }
    /**
     * Return the given (property) value as a descriptive string
     *
     * @param mixed $value Can be literally anything
     * @return string
     */
    protected function formatValue($value)
    {
    }
}
