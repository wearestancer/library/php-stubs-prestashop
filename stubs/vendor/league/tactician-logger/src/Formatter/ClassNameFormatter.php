<?php

namespace League\Tactician\Logger\Formatter;

/**
 * Returns log messages only dump the Command & Exception's class names.
 */
class ClassNameFormatter implements \League\Tactician\Logger\Formatter\Formatter
{
    /**
     * @param string $commandReceivedLevel
     * @param string $commandSucceededLevel
     * @param string $commandFailedLevel
     */
    public function __construct($commandReceivedLevel = \Psr\Log\LogLevel::DEBUG, $commandSucceededLevel = \Psr\Log\LogLevel::DEBUG, $commandFailedLevel = \Psr\Log\LogLevel::ERROR)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function logCommandReceived(\Psr\Log\LoggerInterface $logger, $command)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function logCommandSucceeded(\Psr\Log\LoggerInterface $logger, $command, $returnValue)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function logCommandFailed(\Psr\Log\LoggerInterface $logger, $command, \Exception $e)
    {
    }
}
