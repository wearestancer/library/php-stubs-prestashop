<?php

namespace League\Tactician\Logger\Formatter;

/**
 * Formatter that includes the Command's name and properties for more detail
 */
class ClassPropertiesFormatter implements \League\Tactician\Logger\Formatter\Formatter
{
    /**
     * @param PropertyNormalizer $normalizer
     * @param string $commandReceivedLevel
     * @param string $commandSucceededLevel
     * @param string $commandFailedLevel
     */
    public function __construct(\League\Tactician\Logger\PropertyNormalizer\PropertyNormalizer $normalizer = null, $commandReceivedLevel = \Psr\Log\LogLevel::DEBUG, $commandSucceededLevel = \Psr\Log\LogLevel::DEBUG, $commandFailedLevel = \Psr\Log\LogLevel::ERROR)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function logCommandReceived(\Psr\Log\LoggerInterface $logger, $command)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function logCommandSucceeded(\Psr\Log\LoggerInterface $logger, $command, $returnValue)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function logCommandFailed(\Psr\Log\LoggerInterface $logger, $command, \Exception $e)
    {
    }
}
