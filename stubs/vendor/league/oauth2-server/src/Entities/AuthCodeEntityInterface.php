<?php

namespace League\OAuth2\Server\Entities;

interface AuthCodeEntityInterface extends \League\OAuth2\Server\Entities\TokenInterface
{
    /**
     * @return string|null
     */
    public function getRedirectUri();
    /**
     * @param string $uri
     */
    public function setRedirectUri($uri);
}
