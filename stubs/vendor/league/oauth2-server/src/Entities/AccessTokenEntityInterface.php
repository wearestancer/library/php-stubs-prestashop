<?php

namespace League\OAuth2\Server\Entities;

interface AccessTokenEntityInterface extends \League\OAuth2\Server\Entities\TokenInterface
{
    /**
     * Set a private key used to encrypt the access token.
     */
    public function setPrivateKey(\League\OAuth2\Server\CryptKey $privateKey);
    /**
     * Generate a string representation of the access token.
     */
    public function __toString();
}
