<?php

namespace League\OAuth2\Server\Entities\Traits;

trait ScopeTrait
{
    /**
     * Serialize the object to the scopes string identifier when using json_encode().
     *
     * @return string
     */
    public function jsonSerialize()
    {
    }
    /**
     * @return string
     */
    public abstract function getIdentifier();
}
