<?php

namespace League\OAuth2\Server\Entities\Traits;

trait EntityTrait
{
    /**
     * @var string
     */
    protected $identifier;
    /**
     * @return mixed
     */
    public function getIdentifier()
    {
    }
    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier)
    {
    }
}
