<?php

namespace League\OAuth2\Server\Entities\Traits;

trait AccessTokenTrait
{
    /**
     * @var CryptKey
     */
    private $privateKey;
    /**
     * @var Configuration
     */
    private $jwtConfiguration;
    /**
     * Set the private key used to encrypt this access token.
     */
    public function setPrivateKey(\League\OAuth2\Server\CryptKey $privateKey)
    {
    }
    /**
     * Initialise the JWT Configuration.
     */
    public function initJwtConfiguration()
    {
    }
    /**
     * Generate a JWT from the access token
     *
     * @return Token
     */
    private function convertToJWT()
    {
    }
    /**
     * Generate a string representation from the access token
     */
    public function __toString()
    {
    }
    /**
     * @return ClientEntityInterface
     */
    public abstract function getClient();
    /**
     * @return DateTimeImmutable
     */
    public abstract function getExpiryDateTime();
    /**
     * @return string|int
     */
    public abstract function getUserIdentifier();
    /**
     * @return ScopeEntityInterface[]
     */
    public abstract function getScopes();
    /**
     * @return string
     */
    public abstract function getIdentifier();
}
