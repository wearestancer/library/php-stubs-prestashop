<?php

namespace League\OAuth2\Server\Entities\Traits;

trait TokenEntityTrait
{
    /**
     * @var ScopeEntityInterface[]
     */
    protected $scopes = [];
    /**
     * @var DateTimeImmutable
     */
    protected $expiryDateTime;
    /**
     * @var string|int|null
     */
    protected $userIdentifier;
    /**
     * @var ClientEntityInterface
     */
    protected $client;
    /**
     * Associate a scope with the token.
     *
     * @param ScopeEntityInterface $scope
     */
    public function addScope(\League\OAuth2\Server\Entities\ScopeEntityInterface $scope)
    {
    }
    /**
     * Return an array of scopes associated with the token.
     *
     * @return ScopeEntityInterface[]
     */
    public function getScopes()
    {
    }
    /**
     * Get the token's expiry date time.
     *
     * @return DateTimeImmutable
     */
    public function getExpiryDateTime()
    {
    }
    /**
     * Set the date time when the token expires.
     *
     * @param DateTimeImmutable $dateTime
     */
    public function setExpiryDateTime(\DateTimeImmutable $dateTime)
    {
    }
    /**
     * Set the identifier of the user associated with the token.
     *
     * @param string|int|null $identifier The identifier of the user
     */
    public function setUserIdentifier($identifier)
    {
    }
    /**
     * Get the token user's identifier.
     *
     * @return string|int|null
     */
    public function getUserIdentifier()
    {
    }
    /**
     * Get the client that the token was issued to.
     *
     * @return ClientEntityInterface
     */
    public function getClient()
    {
    }
    /**
     * Set the client that the token was issued to.
     *
     * @param ClientEntityInterface $client
     */
    public function setClient(\League\OAuth2\Server\Entities\ClientEntityInterface $client)
    {
    }
}
