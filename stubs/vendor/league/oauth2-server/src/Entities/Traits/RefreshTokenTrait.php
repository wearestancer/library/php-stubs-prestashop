<?php

namespace League\OAuth2\Server\Entities\Traits;

trait RefreshTokenTrait
{
    /**
     * @var AccessTokenEntityInterface
     */
    protected $accessToken;
    /**
     * @var DateTimeImmutable
     */
    protected $expiryDateTime;
    /**
     * {@inheritdoc}
     */
    public function setAccessToken(\League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAccessToken()
    {
    }
    /**
     * Get the token's expiry date time.
     *
     * @return DateTimeImmutable
     */
    public function getExpiryDateTime()
    {
    }
    /**
     * Set the date time when the token expires.
     *
     * @param DateTimeImmutable $dateTime
     */
    public function setExpiryDateTime(\DateTimeImmutable $dateTime)
    {
    }
}
