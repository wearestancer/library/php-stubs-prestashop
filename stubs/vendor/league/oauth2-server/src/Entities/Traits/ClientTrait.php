<?php

namespace League\OAuth2\Server\Entities\Traits;

trait ClientTrait
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string|string[]
     */
    protected $redirectUri;
    /**
     * @var bool
     */
    protected $isConfidential = false;
    /**
     * Get the client's name.
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getName()
    {
    }
    /**
     * Returns the registered redirect URI (as a string).
     *
     * Alternatively return an indexed array of redirect URIs.
     *
     * @return string|string[]
     */
    public function getRedirectUri()
    {
    }
    /**
     * Returns true if the client is confidential.
     *
     * @return bool
     */
    public function isConfidential()
    {
    }
}
