<?php

namespace League\OAuth2\Server\Entities\Traits;

trait AuthCodeTrait
{
    /**
     * @var null|string
     */
    protected $redirectUri;
    /**
     * @return string|null
     */
    public function getRedirectUri()
    {
    }
    /**
     * @param string $uri
     */
    public function setRedirectUri($uri)
    {
    }
}
