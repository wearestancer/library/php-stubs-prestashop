<?php

namespace League\OAuth2\Server;

class CryptKey
{
    /** @deprecated left for backward compatibility check */
    const RSA_KEY_PATTERN = '/^(-----BEGIN (RSA )?(PUBLIC|PRIVATE) KEY-----)\\R.*(-----END (RSA )?(PUBLIC|PRIVATE) KEY-----)\\R?$/s';
    /**
     * @var string Key contents
     */
    protected $keyContents;
    /**
     * @var string
     */
    protected $keyPath;
    /**
     * @var null|string
     */
    protected $passPhrase;
    /**
     * @param string      $keyPath
     * @param null|string $passPhrase
     * @param bool        $keyPermissionsCheck
     */
    public function __construct($keyPath, $passPhrase = null, $keyPermissionsCheck = true)
    {
    }
    /**
     * Get key contents
     *
     * @return string Key contents
     */
    public function getKeyContents() : string
    {
    }
    /**
     * Retrieve key path.
     *
     * @return string
     */
    public function getKeyPath()
    {
    }
    /**
     * Retrieve key pass phrase.
     *
     * @return null|string
     */
    public function getPassPhrase()
    {
    }
}
