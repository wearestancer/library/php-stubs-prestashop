<?php

namespace League\OAuth2\Server;

class RequestRefreshTokenEvent extends \League\OAuth2\Server\RequestEvent
{
    /**
     * @param string                 $name
     * @param ServerRequestInterface $request
     */
    public function __construct($name, \Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\Entities\RefreshTokenEntityInterface $refreshToken)
    {
    }
    /**
     * @return RefreshTokenEntityInterface
     * @codeCoverageIgnore
     */
    public function getRefreshToken()
    {
    }
}
