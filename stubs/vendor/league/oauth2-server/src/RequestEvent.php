<?php

namespace League\OAuth2\Server;

class RequestEvent extends \League\Event\Event
{
    const CLIENT_AUTHENTICATION_FAILED = 'client.authentication.failed';
    const USER_AUTHENTICATION_FAILED = 'user.authentication.failed';
    const REFRESH_TOKEN_CLIENT_FAILED = 'refresh_token.client.failed';
    const REFRESH_TOKEN_ISSUED = 'refresh_token.issued';
    const ACCESS_TOKEN_ISSUED = 'access_token.issued';
    /**
     * RequestEvent constructor.
     *
     * @param string                 $name
     * @param ServerRequestInterface $request
     */
    public function __construct($name, \Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * @return ServerRequestInterface
     * @codeCoverageIgnore
     */
    public function getRequest()
    {
    }
}
