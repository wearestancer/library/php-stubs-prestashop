<?php

namespace League\OAuth2\Server;

class AuthorizationServer implements \League\Event\EmitterAwareInterface
{
    use \League\Event\EmitterAwareTrait;
    /**
     * @var GrantTypeInterface[]
     */
    protected $enabledGrantTypes = [];
    /**
     * @var DateInterval[]
     */
    protected $grantTypeAccessTokenTTL = [];
    /**
     * @var CryptKey
     */
    protected $privateKey;
    /**
     * @var CryptKey
     */
    protected $publicKey;
    /**
     * @var ResponseTypeInterface
     */
    protected $responseType;
    /**
     * New server instance.
     *
     * @param ClientRepositoryInterface      $clientRepository
     * @param AccessTokenRepositoryInterface $accessTokenRepository
     * @param ScopeRepositoryInterface       $scopeRepository
     * @param CryptKey|string                $privateKey
     * @param string|Key                     $encryptionKey
     * @param null|ResponseTypeInterface     $responseType
     */
    public function __construct(\League\OAuth2\Server\Repositories\ClientRepositoryInterface $clientRepository, \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface $accessTokenRepository, \League\OAuth2\Server\Repositories\ScopeRepositoryInterface $scopeRepository, $privateKey, $encryptionKey, \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType = null)
    {
    }
    /**
     * Enable a grant type on the server.
     *
     * @param GrantTypeInterface $grantType
     * @param null|DateInterval  $accessTokenTTL
     */
    public function enableGrantType(\League\OAuth2\Server\Grant\GrantTypeInterface $grantType, \DateInterval $accessTokenTTL = null)
    {
    }
    /**
     * Validate an authorization request
     *
     * @param ServerRequestInterface $request
     *
     * @throws OAuthServerException
     *
     * @return AuthorizationRequest
     */
    public function validateAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Complete an authorization request
     *
     * @param AuthorizationRequest $authRequest
     * @param ResponseInterface    $response
     *
     * @return ResponseInterface
     */
    public function completeAuthorizationRequest(\League\OAuth2\Server\RequestTypes\AuthorizationRequest $authRequest, \Psr\Http\Message\ResponseInterface $response)
    {
    }
    /**
     * Return an access token response.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @throws OAuthServerException
     *
     * @return ResponseInterface
     */
    public function respondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request, \Psr\Http\Message\ResponseInterface $response)
    {
    }
    /**
     * Get the token type that grants will return in the HTTP response.
     *
     * @return ResponseTypeInterface
     */
    protected function getResponseType()
    {
    }
    /**
     * Set the default scope for the authorization server.
     *
     * @param string $defaultScope
     */
    public function setDefaultScope($defaultScope)
    {
    }
    /**
     * Sets whether to revoke refresh tokens or not (for all grant types).
     *
     * @param bool $revokeRefreshTokens
     */
    public function revokeRefreshTokens(bool $revokeRefreshTokens) : void
    {
    }
}
