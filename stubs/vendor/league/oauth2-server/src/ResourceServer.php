<?php

namespace League\OAuth2\Server;

class ResourceServer
{
    /**
     * New server instance.
     *
     * @param AccessTokenRepositoryInterface       $accessTokenRepository
     * @param CryptKey|string                      $publicKey
     * @param null|AuthorizationValidatorInterface $authorizationValidator
     */
    public function __construct(\League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface $accessTokenRepository, $publicKey, \League\OAuth2\Server\AuthorizationValidators\AuthorizationValidatorInterface $authorizationValidator = null)
    {
    }
    /**
     * @return AuthorizationValidatorInterface
     */
    protected function getAuthorizationValidator()
    {
    }
    /**
     * Determine the access token validity.
     *
     * @param ServerRequestInterface $request
     *
     * @throws OAuthServerException
     *
     * @return ServerRequestInterface
     */
    public function validateAuthenticatedRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
}
