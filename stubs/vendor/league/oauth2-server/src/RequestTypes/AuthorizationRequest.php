<?php

namespace League\OAuth2\Server\RequestTypes;

class AuthorizationRequest
{
    /**
     * The grant type identifier
     *
     * @var string
     */
    protected $grantTypeId;
    /**
     * The client identifier
     *
     * @var ClientEntityInterface
     */
    protected $client;
    /**
     * The user identifier
     *
     * @var UserEntityInterface
     */
    protected $user;
    /**
     * An array of scope identifiers
     *
     * @var ScopeEntityInterface[]
     */
    protected $scopes = [];
    /**
     * Has the user authorized the authorization request
     *
     * @var bool
     */
    protected $authorizationApproved = false;
    /**
     * The redirect URI used in the request
     *
     * @var string|null
     */
    protected $redirectUri;
    /**
     * The state parameter on the authorization request
     *
     * @var string|null
     */
    protected $state;
    /**
     * The code challenge (if provided)
     *
     * @var string
     */
    protected $codeChallenge;
    /**
     * The code challenge method (if provided)
     *
     * @var string
     */
    protected $codeChallengeMethod;
    /**
     * @return string
     */
    public function getGrantTypeId()
    {
    }
    /**
     * @param string $grantTypeId
     */
    public function setGrantTypeId($grantTypeId)
    {
    }
    /**
     * @return ClientEntityInterface
     */
    public function getClient()
    {
    }
    /**
     * @param ClientEntityInterface $client
     */
    public function setClient(\League\OAuth2\Server\Entities\ClientEntityInterface $client)
    {
    }
    /**
     * @return UserEntityInterface|null
     */
    public function getUser()
    {
    }
    /**
     * @param UserEntityInterface $user
     */
    public function setUser(\League\OAuth2\Server\Entities\UserEntityInterface $user)
    {
    }
    /**
     * @return ScopeEntityInterface[]
     */
    public function getScopes()
    {
    }
    /**
     * @param ScopeEntityInterface[] $scopes
     */
    public function setScopes(array $scopes)
    {
    }
    /**
     * @return bool
     */
    public function isAuthorizationApproved()
    {
    }
    /**
     * @param bool $authorizationApproved
     */
    public function setAuthorizationApproved($authorizationApproved)
    {
    }
    /**
     * @return string|null
     */
    public function getRedirectUri()
    {
    }
    /**
     * @param string|null $redirectUri
     */
    public function setRedirectUri($redirectUri)
    {
    }
    /**
     * @return string|null
     */
    public function getState()
    {
    }
    /**
     * @param string $state
     */
    public function setState($state)
    {
    }
    /**
     * @return string
     */
    public function getCodeChallenge()
    {
    }
    /**
     * @param string $codeChallenge
     */
    public function setCodeChallenge($codeChallenge)
    {
    }
    /**
     * @return string
     */
    public function getCodeChallengeMethod()
    {
    }
    /**
     * @param string $codeChallengeMethod
     */
    public function setCodeChallengeMethod($codeChallengeMethod)
    {
    }
}
