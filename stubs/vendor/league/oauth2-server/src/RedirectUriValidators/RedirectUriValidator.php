<?php

namespace League\OAuth2\Server\RedirectUriValidators;

class RedirectUriValidator implements \League\OAuth2\Server\RedirectUriValidators\RedirectUriValidatorInterface
{
    /**
     * New validator instance for the given uri
     *
     * @param string|array $allowedRedirectUris
     */
    public function __construct($allowedRedirectUri)
    {
    }
    /**
     * Validates the redirect uri.
     *
     * @param string $redirectUri
     *
     * @return bool Return true if valid, false otherwise
     */
    public function validateRedirectUri($redirectUri)
    {
    }
}
