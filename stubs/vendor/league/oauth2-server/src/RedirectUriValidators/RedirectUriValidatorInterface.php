<?php

namespace League\OAuth2\Server\RedirectUriValidators;

interface RedirectUriValidatorInterface
{
    /**
     * Validates the redirect uri.
     *
     * @param string $redirectUri
     *
     * @return bool Return true if valid, false otherwise
     */
    public function validateRedirectUri($redirectUri);
}
