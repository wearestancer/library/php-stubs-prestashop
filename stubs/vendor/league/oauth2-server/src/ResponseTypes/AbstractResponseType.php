<?php

namespace League\OAuth2\Server\ResponseTypes;

abstract class AbstractResponseType implements \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface
{
    use \League\OAuth2\Server\CryptTrait;
    /**
     * @var AccessTokenEntityInterface
     */
    protected $accessToken;
    /**
     * @var RefreshTokenEntityInterface
     */
    protected $refreshToken;
    /**
     * @var CryptKey
     */
    protected $privateKey;
    /**
     * {@inheritdoc}
     */
    public function setAccessToken(\League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setRefreshToken(\League\OAuth2\Server\Entities\RefreshTokenEntityInterface $refreshToken)
    {
    }
    /**
     * Set the private key
     *
     * @param CryptKey $key
     */
    public function setPrivateKey(\League\OAuth2\Server\CryptKey $key)
    {
    }
}
