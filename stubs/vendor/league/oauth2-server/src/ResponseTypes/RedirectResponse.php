<?php

namespace League\OAuth2\Server\ResponseTypes;

class RedirectResponse extends \League\OAuth2\Server\ResponseTypes\AbstractResponseType
{
    /**
     * @param string $redirectUri
     */
    public function setRedirectUri($redirectUri)
    {
    }
    /**
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function generateHttpResponse(\Psr\Http\Message\ResponseInterface $response)
    {
    }
}
