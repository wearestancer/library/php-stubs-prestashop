<?php

namespace League\OAuth2\Server\ResponseTypes;

class BearerTokenResponse extends \League\OAuth2\Server\ResponseTypes\AbstractResponseType
{
    /**
     * {@inheritdoc}
     */
    public function generateHttpResponse(\Psr\Http\Message\ResponseInterface $response)
    {
    }
    /**
     * Add custom fields to your Bearer Token response here, then override
     * AuthorizationServer::getResponseType() to pull in your version of
     * this class rather than the default.
     *
     * @param AccessTokenEntityInterface $accessToken
     *
     * @return array
     */
    protected function getExtraParams(\League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken)
    {
    }
}
