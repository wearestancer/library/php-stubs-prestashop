<?php

namespace League\OAuth2\Server\ResponseTypes;

interface ResponseTypeInterface
{
    /**
     * @param AccessTokenEntityInterface $accessToken
     */
    public function setAccessToken(\League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken);
    /**
     * @param RefreshTokenEntityInterface $refreshToken
     */
    public function setRefreshToken(\League\OAuth2\Server\Entities\RefreshTokenEntityInterface $refreshToken);
    /**
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function generateHttpResponse(\Psr\Http\Message\ResponseInterface $response);
    /**
     * Set the encryption key
     *
     * @param string|Key|null $key
     */
    public function setEncryptionKey($key = null);
}
