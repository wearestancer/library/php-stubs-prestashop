<?php

namespace League\OAuth2\Server;

class RequestAccessTokenEvent extends \League\OAuth2\Server\RequestEvent
{
    /**
     * @param string                 $name
     * @param ServerRequestInterface $request
     */
    public function __construct($name, \Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken)
    {
    }
    /**
     * @return AccessTokenEntityInterface
     * @codeCoverageIgnore
     */
    public function getAccessToken()
    {
    }
}
