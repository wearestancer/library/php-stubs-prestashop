<?php

namespace League\OAuth2\Server\Grant;

abstract class AbstractAuthorizeGrant extends \League\OAuth2\Server\Grant\AbstractGrant
{
    /**
     * @param string $uri
     * @param array  $params
     * @param string $queryDelimiter
     *
     * @return string
     */
    public function makeRedirectUri($uri, $params = [], $queryDelimiter = '?')
    {
    }
}
