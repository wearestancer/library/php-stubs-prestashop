<?php

namespace League\OAuth2\Server\Grant;

class ImplicitGrant extends \League\OAuth2\Server\Grant\AbstractAuthorizeGrant
{
    /**
     * @param DateInterval $accessTokenTTL
     * @param string       $queryDelimiter
     */
    public function __construct(\DateInterval $accessTokenTTL, $queryDelimiter = '#')
    {
    }
    /**
     * @param DateInterval $refreshTokenTTL
     *
     * @throw LogicException
     */
    public function setRefreshTokenTTL(\DateInterval $refreshTokenTTL)
    {
    }
    /**
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     *
     * @throw LogicException
     */
    public function setRefreshTokenRepository(\League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canRespondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Return the grant identifier that can be used in matching up requests.
     *
     * @return string
     */
    public function getIdentifier()
    {
    }
    /**
     * Respond to an incoming request.
     *
     * @param ServerRequestInterface $request
     * @param ResponseTypeInterface  $responseType
     * @param DateInterval           $accessTokenTTL
     *
     * @return ResponseTypeInterface
     */
    public function respondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType, \DateInterval $accessTokenTTL)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canRespondToAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function completeAuthorizationRequest(\League\OAuth2\Server\RequestTypes\AuthorizationRequest $authorizationRequest)
    {
    }
}
