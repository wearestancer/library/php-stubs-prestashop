<?php

namespace League\OAuth2\Server\Grant;

/**
 * Refresh token grant.
 */
class RefreshTokenGrant extends \League\OAuth2\Server\Grant\AbstractGrant
{
    /**
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function __construct(\League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType, \DateInterval $accessTokenTTL)
    {
    }
    /**
     * @param ServerRequestInterface $request
     * @param string                 $clientId
     *
     * @throws OAuthServerException
     *
     * @return array
     */
    protected function validateOldRefreshToken(\Psr\Http\Message\ServerRequestInterface $request, $clientId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
    }
}
