<?php

namespace League\OAuth2\Server\Grant;

/**
 * Client credentials grant class.
 */
class ClientCredentialsGrant extends \League\OAuth2\Server\Grant\AbstractGrant
{
    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType, \DateInterval $accessTokenTTL)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
    }
}
