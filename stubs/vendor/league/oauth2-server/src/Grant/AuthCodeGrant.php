<?php

namespace League\OAuth2\Server\Grant;

class AuthCodeGrant extends \League\OAuth2\Server\Grant\AbstractAuthorizeGrant
{
    /**
     * @param AuthCodeRepositoryInterface     $authCodeRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     * @param DateInterval                    $authCodeTTL
     *
     * @throws Exception
     */
    public function __construct(\League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface $authCodeRepository, \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository, \DateInterval $authCodeTTL)
    {
    }
    /**
     * Disable the requirement for a code challenge for public clients.
     */
    public function disableRequireCodeChallengeForPublicClients()
    {
    }
    /**
     * Respond to an access token request.
     *
     * @param ServerRequestInterface $request
     * @param ResponseTypeInterface  $responseType
     * @param DateInterval           $accessTokenTTL
     *
     * @throws OAuthServerException
     *
     * @return ResponseTypeInterface
     */
    public function respondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType, \DateInterval $accessTokenTTL)
    {
    }
    /**
     * Return the grant identifier that can be used in matching up requests.
     *
     * @return string
     */
    public function getIdentifier()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canRespondToAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function completeAuthorizationRequest(\League\OAuth2\Server\RequestTypes\AuthorizationRequest $authorizationRequest)
    {
    }
}
