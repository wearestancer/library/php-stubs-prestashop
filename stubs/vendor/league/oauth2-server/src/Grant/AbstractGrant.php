<?php

namespace League\OAuth2\Server\Grant;

/**
 * Abstract grant class.
 */
abstract class AbstractGrant implements \League\OAuth2\Server\Grant\GrantTypeInterface
{
    use \League\Event\EmitterAwareTrait, \League\OAuth2\Server\CryptTrait;
    const SCOPE_DELIMITER_STRING = ' ';
    const MAX_RANDOM_TOKEN_GENERATION_ATTEMPTS = 10;
    /**
     * @var ClientRepositoryInterface
     */
    protected $clientRepository;
    /**
     * @var AccessTokenRepositoryInterface
     */
    protected $accessTokenRepository;
    /**
     * @var ScopeRepositoryInterface
     */
    protected $scopeRepository;
    /**
     * @var AuthCodeRepositoryInterface
     */
    protected $authCodeRepository;
    /**
     * @var RefreshTokenRepositoryInterface
     */
    protected $refreshTokenRepository;
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;
    /**
     * @var DateInterval
     */
    protected $refreshTokenTTL;
    /**
     * @var CryptKey
     */
    protected $privateKey;
    /**
     * @var string
     */
    protected $defaultScope;
    /**
     * @var bool
     */
    protected $revokeRefreshTokens;
    /**
     * @param ClientRepositoryInterface $clientRepository
     */
    public function setClientRepository(\League\OAuth2\Server\Repositories\ClientRepositoryInterface $clientRepository)
    {
    }
    /**
     * @param AccessTokenRepositoryInterface $accessTokenRepository
     */
    public function setAccessTokenRepository(\League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface $accessTokenRepository)
    {
    }
    /**
     * @param ScopeRepositoryInterface $scopeRepository
     */
    public function setScopeRepository(\League\OAuth2\Server\Repositories\ScopeRepositoryInterface $scopeRepository)
    {
    }
    /**
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function setRefreshTokenRepository(\League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository)
    {
    }
    /**
     * @param AuthCodeRepositoryInterface $authCodeRepository
     */
    public function setAuthCodeRepository(\League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface $authCodeRepository)
    {
    }
    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function setUserRepository(\League\OAuth2\Server\Repositories\UserRepositoryInterface $userRepository)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setRefreshTokenTTL(\DateInterval $refreshTokenTTL)
    {
    }
    /**
     * Set the private key
     *
     * @param CryptKey $key
     */
    public function setPrivateKey(\League\OAuth2\Server\CryptKey $key)
    {
    }
    /**
     * @param string $scope
     */
    public function setDefaultScope($scope)
    {
    }
    /**
     * @param bool $revokeRefreshTokens
     */
    public function revokeRefreshTokens(bool $revokeRefreshTokens)
    {
    }
    /**
     * Validate the client.
     *
     * @param ServerRequestInterface $request
     *
     * @throws OAuthServerException
     *
     * @return ClientEntityInterface
     */
    protected function validateClient(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Wrapper around ClientRepository::getClientEntity() that ensures we emit
     * an event and throw an exception if the repo doesn't return a client
     * entity.
     *
     * This is a bit of defensive coding because the interface contract
     * doesn't actually enforce non-null returns/exception-on-no-client so
     * getClientEntity might return null. By contrast, this method will
     * always either return a ClientEntityInterface or throw.
     *
     * @param string                 $clientId
     * @param ServerRequestInterface $request
     *
     * @return ClientEntityInterface
     */
    protected function getClientEntityOrFail($clientId, \Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Gets the client credentials from the request from the request body or
     * the Http Basic Authorization header
     *
     * @param ServerRequestInterface $request
     *
     * @return array
     */
    protected function getClientCredentials(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Validate redirectUri from the request.
     * If a redirect URI is provided ensure it matches what is pre-registered
     *
     * @param string                 $redirectUri
     * @param ClientEntityInterface  $client
     * @param ServerRequestInterface $request
     *
     * @throws OAuthServerException
     */
    protected function validateRedirectUri(string $redirectUri, \League\OAuth2\Server\Entities\ClientEntityInterface $client, \Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Validate scopes in the request.
     *
     * @param string|array $scopes
     * @param string       $redirectUri
     *
     * @throws OAuthServerException
     *
     * @return ScopeEntityInterface[]
     */
    public function validateScopes($scopes, $redirectUri = null)
    {
    }
    /**
     * Retrieve request parameter.
     *
     * @param string                 $parameter
     * @param ServerRequestInterface $request
     * @param mixed                  $default
     *
     * @return null|string
     */
    protected function getRequestParameter($parameter, \Psr\Http\Message\ServerRequestInterface $request, $default = null)
    {
    }
    /**
     * Retrieve HTTP Basic Auth credentials with the Authorization header
     * of a request. First index of the returned array is the username,
     * second is the password (so list() will work). If the header does
     * not exist, or is otherwise an invalid HTTP Basic header, return
     * [null, null].
     *
     * @param ServerRequestInterface $request
     *
     * @return string[]|null[]
     */
    protected function getBasicAuthCredentials(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * Retrieve query string parameter.
     *
     * @param string                 $parameter
     * @param ServerRequestInterface $request
     * @param mixed                  $default
     *
     * @return null|string
     */
    protected function getQueryStringParameter($parameter, \Psr\Http\Message\ServerRequestInterface $request, $default = null)
    {
    }
    /**
     * Retrieve cookie parameter.
     *
     * @param string                 $parameter
     * @param ServerRequestInterface $request
     * @param mixed                  $default
     *
     * @return null|string
     */
    protected function getCookieParameter($parameter, \Psr\Http\Message\ServerRequestInterface $request, $default = null)
    {
    }
    /**
     * Retrieve server parameter.
     *
     * @param string                 $parameter
     * @param ServerRequestInterface $request
     * @param mixed                  $default
     *
     * @return null|string
     */
    protected function getServerParameter($parameter, \Psr\Http\Message\ServerRequestInterface $request, $default = null)
    {
    }
    /**
     * Issue an access token.
     *
     * @param DateInterval           $accessTokenTTL
     * @param ClientEntityInterface  $client
     * @param string|null            $userIdentifier
     * @param ScopeEntityInterface[] $scopes
     *
     * @throws OAuthServerException
     * @throws UniqueTokenIdentifierConstraintViolationException
     *
     * @return AccessTokenEntityInterface
     */
    protected function issueAccessToken(\DateInterval $accessTokenTTL, \League\OAuth2\Server\Entities\ClientEntityInterface $client, $userIdentifier, array $scopes = [])
    {
    }
    /**
     * Issue an auth code.
     *
     * @param DateInterval           $authCodeTTL
     * @param ClientEntityInterface  $client
     * @param string                 $userIdentifier
     * @param string|null            $redirectUri
     * @param ScopeEntityInterface[] $scopes
     *
     * @throws OAuthServerException
     * @throws UniqueTokenIdentifierConstraintViolationException
     *
     * @return AuthCodeEntityInterface
     */
    protected function issueAuthCode(\DateInterval $authCodeTTL, \League\OAuth2\Server\Entities\ClientEntityInterface $client, $userIdentifier, $redirectUri, array $scopes = [])
    {
    }
    /**
     * @param AccessTokenEntityInterface $accessToken
     *
     * @throws OAuthServerException
     * @throws UniqueTokenIdentifierConstraintViolationException
     *
     * @return RefreshTokenEntityInterface|null
     */
    protected function issueRefreshToken(\League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken)
    {
    }
    /**
     * Generate a new unique identifier.
     *
     * @param int $length
     *
     * @throws OAuthServerException
     *
     * @return string
     */
    protected function generateUniqueIdentifier($length = 40)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canRespondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canRespondToAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthorizationRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function completeAuthorizationRequest(\League\OAuth2\Server\RequestTypes\AuthorizationRequest $authorizationRequest)
    {
    }
}
