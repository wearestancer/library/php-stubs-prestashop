<?php

namespace League\OAuth2\Server\Grant;

/**
 * Password grant class.
 */
class PasswordGrant extends \League\OAuth2\Server\Grant\AbstractGrant
{
    /**
     * @param UserRepositoryInterface         $userRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function __construct(\League\OAuth2\Server\Repositories\UserRepositoryInterface $userRepository, \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(\Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType, \DateInterval $accessTokenTTL)
    {
    }
    /**
     * @param ServerRequestInterface $request
     * @param ClientEntityInterface  $client
     *
     * @throws OAuthServerException
     *
     * @return UserEntityInterface
     */
    protected function validateUser(\Psr\Http\Message\ServerRequestInterface $request, \League\OAuth2\Server\Entities\ClientEntityInterface $client)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
    }
}
