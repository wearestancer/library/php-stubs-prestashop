<?php

namespace League\OAuth2\Server;

trait CryptTrait
{
    /**
     * @var string|Key|null
     */
    protected $encryptionKey;
    /**
     * Encrypt data with encryptionKey.
     *
     * @param string $unencryptedData
     *
     * @throws LogicException
     *
     * @return string
     */
    protected function encrypt($unencryptedData)
    {
    }
    /**
     * Decrypt data with encryptionKey.
     *
     * @param string $encryptedData
     *
     * @throws LogicException
     *
     * @return string
     */
    protected function decrypt($encryptedData)
    {
    }
    /**
     * Set the encryption key
     *
     * @param string|Key $key
     */
    public function setEncryptionKey($key = null)
    {
    }
}
