<?php

namespace League\OAuth2\Server\CodeChallengeVerifiers;

class S256Verifier implements \League\OAuth2\Server\CodeChallengeVerifiers\CodeChallengeVerifierInterface
{
    /**
     * Return code challenge method.
     *
     * @return string
     */
    public function getMethod()
    {
    }
    /**
     * Verify the code challenge.
     *
     * @param string $codeVerifier
     * @param string $codeChallenge
     *
     * @return bool
     */
    public function verifyCodeChallenge($codeVerifier, $codeChallenge)
    {
    }
}
