<?php

namespace League\OAuth2\Server\Middleware;

class AuthorizationServerMiddleware
{
    /**
     * @param AuthorizationServer $server
     */
    public function __construct(\League\OAuth2\Server\AuthorizationServer $server)
    {
    }
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface
     */
    public function __invoke(\Psr\Http\Message\ServerRequestInterface $request, \Psr\Http\Message\ResponseInterface $response, callable $next)
    {
    }
}
