<?php

namespace League\OAuth2\Server\Middleware;

class ResourceServerMiddleware
{
    /**
     * @param ResourceServer $server
     */
    public function __construct(\League\OAuth2\Server\ResourceServer $server)
    {
    }
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface
     */
    public function __invoke(\Psr\Http\Message\ServerRequestInterface $request, \Psr\Http\Message\ResponseInterface $response, callable $next)
    {
    }
}
