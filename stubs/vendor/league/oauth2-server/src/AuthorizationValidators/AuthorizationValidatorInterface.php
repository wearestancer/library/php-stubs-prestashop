<?php

namespace League\OAuth2\Server\AuthorizationValidators;

interface AuthorizationValidatorInterface
{
    /**
     * Determine the access token in the authorization header and append OAUth properties to the request
     *  as attributes.
     *
     * @param ServerRequestInterface $request
     *
     * @return ServerRequestInterface
     */
    public function validateAuthorization(\Psr\Http\Message\ServerRequestInterface $request);
}
