<?php

namespace League\OAuth2\Server\AuthorizationValidators;

class BearerTokenValidator implements \League\OAuth2\Server\AuthorizationValidators\AuthorizationValidatorInterface
{
    use \League\OAuth2\Server\CryptTrait;
    /**
     * @var CryptKey
     */
    protected $publicKey;
    /**
     * @param AccessTokenRepositoryInterface $accessTokenRepository
     */
    public function __construct(\League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface $accessTokenRepository)
    {
    }
    /**
     * Set the public key
     *
     * @param CryptKey $key
     */
    public function setPublicKey(\League\OAuth2\Server\CryptKey $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthorization(\Psr\Http\Message\ServerRequestInterface $request)
    {
    }
}
