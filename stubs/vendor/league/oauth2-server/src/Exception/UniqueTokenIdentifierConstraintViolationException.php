<?php

namespace League\OAuth2\Server\Exception;

class UniqueTokenIdentifierConstraintViolationException extends \League\OAuth2\Server\Exception\OAuthServerException
{
    /**
     * @return UniqueTokenIdentifierConstraintViolationException
     */
    public static function create()
    {
    }
}
