<?php

namespace League\OAuth2\Server\Exception;

class OAuthServerException extends \Exception
{
    /**
     * Throw a new exception.
     *
     * @param string      $message        Error message
     * @param int         $code           Error code
     * @param string      $errorType      Error type
     * @param int         $httpStatusCode HTTP status code to send (default = 400)
     * @param null|string $hint           A helper hint
     * @param null|string $redirectUri    A HTTP URI to redirect the user back to
     * @param Throwable   $previous       Previous exception
     */
    public function __construct($message, $code, $errorType, $httpStatusCode = 400, $hint = null, $redirectUri = null, \Throwable $previous = null)
    {
    }
    /**
     * Returns the current payload.
     *
     * @return array
     */
    public function getPayload()
    {
    }
    /**
     * Updates the current payload.
     *
     * @param array $payload
     */
    public function setPayload(array $payload)
    {
    }
    /**
     * Set the server request that is responsible for generating the exception
     *
     * @param ServerRequestInterface $serverRequest
     */
    public function setServerRequest(\Psr\Http\Message\ServerRequestInterface $serverRequest)
    {
    }
    /**
     * Unsupported grant type error.
     *
     * @return static
     */
    public static function unsupportedGrantType()
    {
    }
    /**
     * Invalid request error.
     *
     * @param string      $parameter The invalid parameter
     * @param null|string $hint
     * @param Throwable   $previous  Previous exception
     *
     * @return static
     */
    public static function invalidRequest($parameter, $hint = null, \Throwable $previous = null)
    {
    }
    /**
     * Invalid client error.
     *
     * @param ServerRequestInterface $serverRequest
     *
     * @return static
     */
    public static function invalidClient(\Psr\Http\Message\ServerRequestInterface $serverRequest)
    {
    }
    /**
     * Invalid scope error.
     *
     * @param string      $scope       The bad scope
     * @param null|string $redirectUri A HTTP URI to redirect the user back to
     *
     * @return static
     */
    public static function invalidScope($scope, $redirectUri = null)
    {
    }
    /**
     * Invalid credentials error.
     *
     * @return static
     */
    public static function invalidCredentials()
    {
    }
    /**
     * Server error.
     *
     * @param string    $hint
     * @param Throwable $previous
     *
     * @return static
     *
     * @codeCoverageIgnore
     */
    public static function serverError($hint, \Throwable $previous = null)
    {
    }
    /**
     * Invalid refresh token.
     *
     * @param null|string $hint
     * @param Throwable   $previous
     *
     * @return static
     */
    public static function invalidRefreshToken($hint = null, \Throwable $previous = null)
    {
    }
    /**
     * Access denied.
     *
     * @param null|string $hint
     * @param null|string $redirectUri
     * @param Throwable   $previous
     *
     * @return static
     */
    public static function accessDenied($hint = null, $redirectUri = null, \Throwable $previous = null)
    {
    }
    /**
     * Invalid grant.
     *
     * @param string $hint
     *
     * @return static
     */
    public static function invalidGrant($hint = '')
    {
    }
    /**
     * @return string
     */
    public function getErrorType()
    {
    }
    /**
     * Generate a HTTP response.
     *
     * @param ResponseInterface $response
     * @param bool              $useFragment True if errors should be in the URI fragment instead of query string
     * @param int               $jsonOptions options passed to json_encode
     *
     * @return ResponseInterface
     */
    public function generateHttpResponse(\Psr\Http\Message\ResponseInterface $response, $useFragment = false, $jsonOptions = 0)
    {
    }
    /**
     * Get all headers that have to be send with the error response.
     *
     * @return array Array with header values
     */
    public function getHttpHeaders()
    {
    }
    /**
     * Check if the exception has an associated redirect URI.
     *
     * Returns whether the exception includes a redirect, since
     * getHttpStatusCode() doesn't return a 302 when there's a
     * redirect enabled. This helps when you want to override local
     * error pages but want to let redirects through.
     *
     * @return bool
     */
    public function hasRedirect()
    {
    }
    /**
     * Returns the Redirect URI used for redirecting.
     *
     * @return string|null
     */
    public function getRedirectUri()
    {
    }
    /**
     * Returns the HTTP status code to send when the exceptions is output.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
    }
    /**
     * @return null|string
     */
    public function getHint()
    {
    }
}
