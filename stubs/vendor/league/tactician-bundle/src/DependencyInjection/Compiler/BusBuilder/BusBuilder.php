<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler\BusBuilder;

final class BusBuilder
{
    public function __construct(string $busId, string $methodInflector, array $middlewareIds)
    {
    }
    public function id() : string
    {
    }
    public function serviceId() : string
    {
    }
    public function locatorServiceId()
    {
    }
    public function commandHandlerMiddlewareId() : string
    {
    }
    public function registerInContainer(\Symfony\Component\DependencyInjection\ContainerBuilder $container, array $commandsToAccept)
    {
    }
}
