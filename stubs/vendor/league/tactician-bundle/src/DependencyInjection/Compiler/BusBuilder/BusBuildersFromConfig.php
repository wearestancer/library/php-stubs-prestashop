<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler\BusBuilder;

final class BusBuildersFromConfig
{
    const DEFAULT_METHOD_INFLECTOR = 'tactician.handler.method_name_inflector.handle';
    const DEFAULT_BUS_ID = 'default';
    public static function convert(array $config) : \League\Tactician\Bundle\DependencyInjection\Compiler\BusBuilder\BusBuilders
    {
    }
}
