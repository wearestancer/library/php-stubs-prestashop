<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler\BusBuilder;

final class BusBuilders implements \IteratorAggregate
{
    public function __construct(array $busBuilders, string $defaultBusId)
    {
    }
    public function createBlankRouting() : \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing
    {
    }
    public function defaultBus() : \League\Tactician\Bundle\DependencyInjection\Compiler\BusBuilder\BusBuilder
    {
    }
    /**
     * @return ArrayIterator|BusBuilder[]
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
