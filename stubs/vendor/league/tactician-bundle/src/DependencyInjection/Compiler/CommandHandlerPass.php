<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler;

/**
 * This compiler pass maps Handler DI tags to specific commands.
 */
class CommandHandlerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(\League\Tactician\Bundle\DependencyInjection\HandlerMapping\HandlerMapping $mappingStrategy)
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
