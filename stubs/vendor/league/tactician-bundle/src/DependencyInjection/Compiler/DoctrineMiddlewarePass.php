<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler;

/**
 * This compiler pass registers doctrine entity manager middleware
 */
class DoctrineMiddlewarePass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
