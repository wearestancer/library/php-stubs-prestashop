<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler;

/**
 * This compiler pass registers validator middleware if possible
 */
class ValidatorMiddlewarePass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    const SERVICE_ID = 'tactician.middleware.validator';
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
