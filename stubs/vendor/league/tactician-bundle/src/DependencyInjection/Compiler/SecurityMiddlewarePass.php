<?php

namespace League\Tactician\Bundle\DependencyInjection\Compiler;

/**
 * This compiler pass registers security middleware if possible
 */
class SecurityMiddlewarePass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    const SERVICE_ID = 'tactician.middleware.security';
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
