<?php

namespace League\Tactician\Bundle\DependencyInjection\HandlerMapping;

final class Routing
{
    public function __construct(array $validBusIds)
    {
    }
    public function routeToBus($busId, $commandClassName, $serviceId)
    {
    }
    public function routeToAllBuses($commandClassName, $serviceId)
    {
    }
    public function commandToServiceMapping(string $busId) : array
    {
    }
}
