<?php

namespace League\Tactician\Bundle\DependencyInjection\HandlerMapping;

final class CompositeMapping implements \League\Tactician\Bundle\DependencyInjection\HandlerMapping\HandlerMapping
{
    public function __construct(\League\Tactician\Bundle\DependencyInjection\HandlerMapping\HandlerMapping ...$strategies)
    {
    }
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing $routing) : \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing
    {
    }
}
