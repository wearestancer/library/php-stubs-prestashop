<?php

namespace League\Tactician\Bundle\DependencyInjection\HandlerMapping;

/**
 * Routes commands based on typehints in the handler.
 *
 * If your handler has a public method with a single, non-scalar, no-interface type hinted
 * parameter, we'll assume that typehint is a command and route it to this
 * service definition as the handler.
 *
 * So, a class like this:
 *
 * class MyHandler
 * {
 *     public function handle(RegisterUser $command) {...}
 *     private function foobar(SomeObject $obj) {...}
 *     public function checkThings(OtherObject $obj, WhatObject $obj2)
 *     public function setADependency(ManagerInterface $interface) {...}
 * }
 *
 * would have RegisterUser routed to it, but not SomeObject (because it's
 * used in a private method), not OtherObject or WhatObject (because they
 * don't appear as the only parameter) and not setADependency (because it
 * has an interface type hinted parameter).
 */
final class TypeHintMapping extends \League\Tactician\Bundle\DependencyInjection\HandlerMapping\TagBasedMapping
{
}
