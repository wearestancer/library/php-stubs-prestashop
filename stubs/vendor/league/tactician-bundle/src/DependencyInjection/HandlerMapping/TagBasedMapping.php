<?php

namespace League\Tactician\Bundle\DependencyInjection\HandlerMapping;

abstract class TagBasedMapping implements \League\Tactician\Bundle\DependencyInjection\HandlerMapping\HandlerMapping
{
    const TAG_NAME = 'tactician.handler';
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing $routing) : \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing
    {
    }
    protected abstract function isSupported(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \Symfony\Component\DependencyInjection\Definition $definition, array $tagAttributes) : bool;
    protected abstract function findCommandsForService(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \Symfony\Component\DependencyInjection\Definition $definition, array $tagAttributes) : array;
}
