<?php

namespace League\Tactician\Bundle\DependencyInjection\HandlerMapping;

interface HandlerMapping
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing $routing) : \League\Tactician\Bundle\DependencyInjection\HandlerMapping\Routing;
}
