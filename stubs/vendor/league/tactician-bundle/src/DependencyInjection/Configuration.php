<?php

namespace League\Tactician\Bundle\DependencyInjection;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements \Symfony\Component\Config\Definition\ConfigurationInterface
{
    /**
     * Create a rootnode tree for configuration that can be injected into the DI container.
     *
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
    }
}
