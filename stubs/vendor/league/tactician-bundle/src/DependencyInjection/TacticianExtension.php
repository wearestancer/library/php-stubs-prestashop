<?php

namespace League\Tactician\Bundle\DependencyInjection;

class TacticianExtension extends \Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension
{
    /**
     * Configures the passed container according to the merged configuration.
     *
     * @param array $mergedConfig
     * @param ContainerBuilder $container
     */
    protected function loadInternal(array $mergedConfig, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function getAlias() : string
    {
    }
}
