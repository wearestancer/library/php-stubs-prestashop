<?php

namespace League\Tactician\Bundle\Security\Voter;

/**
 * Voter for security checks on handling commands.
 *
 * @author Ron Rademaker
 */
class HandleCommandVoter extends \Symfony\Component\Security\Core\Authorization\Voter\Voter
{
    /**
     * Create a new HandleCommandVoter.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     * @param array                          $commandRoleMapping
     */
    public function __construct(\Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface $decisionManager, array $commandRoleMapping = [])
    {
    }
    /**
     * The voter supports checking handle commands
     *
     * @param string $attribute
     * @param object $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject) : bool
    {
    }
    /**
     * Checks if the currently logged on user may handle $subject.
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token) : bool
    {
    }
}
