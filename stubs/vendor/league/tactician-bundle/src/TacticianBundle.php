<?php

namespace League\Tactician\Bundle;

class TacticianBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function __construct(\League\Tactician\Bundle\DependencyInjection\HandlerMapping\HandlerMapping $handlerMapping = null)
    {
    }
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function getContainerExtension() : \Symfony\Component\DependencyInjection\Extension\ExtensionInterface
    {
    }
    public static function defaultMappingStrategy() : \League\Tactician\Bundle\DependencyInjection\HandlerMapping\HandlerMapping
    {
    }
}
