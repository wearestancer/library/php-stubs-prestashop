<?php

namespace League\Tactician\Bundle\Handler;

/**
 * Lazily loads Command Handlers from the Symfony DI container
 */
class ContainerBasedHandlerLocator implements \League\Tactician\Handler\Locator\HandlerLocator
{
    /**
     * @param ContainerInterface $container
     * @param array $commandToServiceIdMapping
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, array $commandToServiceIdMapping)
    {
    }
    /**
     * Retrieves the handler for a specified command
     *
     * @param string $commandName
     * @return object
     *
     * @throws MissingHandlerException
     */
    public function getHandlerForCommand($commandName)
    {
    }
}
