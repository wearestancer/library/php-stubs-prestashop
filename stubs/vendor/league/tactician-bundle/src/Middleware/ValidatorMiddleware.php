<?php

namespace League\Tactician\Bundle\Middleware;

class ValidatorMiddleware implements \League\Tactician\Middleware
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;
    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator)
    {
    }
    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     *
     * @throws InvalidCommandException
     */
    public function execute($command, callable $next)
    {
    }
}
