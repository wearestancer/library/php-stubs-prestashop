<?php

namespace League\Tactician\Bundle\Middleware;

class InvalidCommandException extends \Exception implements \League\Tactician\Exception\Exception
{
    /**
     * @var object
     */
    protected $command;
    /**
     * @var ConstraintViolationListInterface
     */
    protected $violations;
    /**
     * @param object $command
     * @param ConstraintViolationListInterface $violations
     *
     * @return static
     */
    public static function onCommand($command, \Symfony\Component\Validator\ConstraintViolationListInterface $violations)
    {
    }
    /**
     * @return object
     */
    public function getCommand()
    {
    }
    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolations()
    {
    }
}
