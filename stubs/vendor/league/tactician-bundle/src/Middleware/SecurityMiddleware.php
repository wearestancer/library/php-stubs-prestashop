<?php

namespace League\Tactician\Bundle\Middleware;

class SecurityMiddleware implements \League\Tactician\Middleware
{
    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(\Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker)
    {
    }
    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
    }
}
