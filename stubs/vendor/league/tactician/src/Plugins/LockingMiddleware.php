<?php

namespace League\Tactician\Plugins;

/**
 * If another command is already being executed, locks the command bus and
 * queues the new incoming commands until the first has completed.
 */
class LockingMiddleware implements \League\Tactician\Middleware
{
    /**
     * Execute the given command... after other running commands are complete.
     *
     * @param object   $command
     * @param callable $next
     *
     * @throws \Exception
     *
     * @return mixed|void
     */
    public function execute($command, callable $next)
    {
    }
    /**
     * Process any pending commands in the queue. If multiple, jobs are in the
     * queue, only the first return value is given back.
     *
     * @return mixed
     */
    protected function executeQueuedJobs()
    {
    }
}
