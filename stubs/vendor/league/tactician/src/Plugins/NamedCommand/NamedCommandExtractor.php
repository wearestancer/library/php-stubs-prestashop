<?php

namespace League\Tactician\Plugins\NamedCommand;

/**
 * Extract the name from a NamedCommand
 */
class NamedCommandExtractor implements \League\Tactician\Handler\CommandNameExtractor\CommandNameExtractor
{
    /**
     * {@inheritdoc}
     */
    public function extract($command)
    {
    }
}
