<?php

namespace League\Tactician\Exception;

/**
 * Thrown when the CommandBus was instantiated with an invalid middleware object
 */
class InvalidMiddlewareException extends \InvalidArgumentException implements \League\Tactician\Exception\Exception
{
    public static function forMiddleware($middleware)
    {
    }
}
