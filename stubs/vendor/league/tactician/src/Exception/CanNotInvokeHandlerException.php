<?php

namespace League\Tactician\Exception;

/**
 * Thrown when a specific handler object can not be used on a command object.
 *
 * The most common reason is the receiving method is missing or incorrectly
 * named.
 */
class CanNotInvokeHandlerException extends \BadMethodCallException implements \League\Tactician\Exception\Exception
{
    /**
     * @param mixed $command
     * @param string $reason
     *
     * @return static
     */
    public static function forCommand($command, $reason)
    {
    }
    /**
     * Returns the command that could not be invoked
     *
     * @return mixed
     */
    public function getCommand()
    {
    }
}
