<?php

namespace League\Tactician\Exception;

/**
 * No handler could be found for the given command.
 */
class MissingHandlerException extends \OutOfBoundsException implements \League\Tactician\Exception\Exception
{
    /**
     * @param string $commandName
     *
     * @return static
     */
    public static function forCommand($commandName)
    {
    }
    /**
     * @return string
     */
    public function getCommandName()
    {
    }
}
