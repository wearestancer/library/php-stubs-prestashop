<?php

namespace League\Tactician\Exception;

/**
 * Thrown when the command bus is given an non-object to use as a command.
 */
class InvalidCommandException extends \RuntimeException implements \League\Tactician\Exception\Exception
{
    /**
     * @param mixed $invalidCommand
     *
     * @return static
     */
    public static function forUnknownValue($invalidCommand)
    {
    }
    /**
     * @return mixed
     */
    public function getInvalidCommand()
    {
    }
}
