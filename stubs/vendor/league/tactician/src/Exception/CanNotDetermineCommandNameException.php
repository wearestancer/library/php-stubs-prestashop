<?php

namespace League\Tactician\Exception;

/**
 * Thrown when a CommandNameExtractor cannot determine the command's name
 */
class CanNotDetermineCommandNameException extends \RuntimeException implements \League\Tactician\Exception\Exception
{
    /**
     * @param mixed $command
     *
     * @return static
     */
    public static function forCommand($command)
    {
    }
    /**
     * Returns the command that could not be invoked
     *
     * @return mixed
     */
    public function getCommand()
    {
    }
}
