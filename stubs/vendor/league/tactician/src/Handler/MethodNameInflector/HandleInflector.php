<?php

namespace League\Tactician\Handler\MethodNameInflector;

/**
 * Handle command by calling the "handle" method.
 */
class HandleInflector implements \League\Tactician\Handler\MethodNameInflector\MethodNameInflector
{
    /**
     * {@inheritdoc}
     */
    public function inflect($command, $commandHandler)
    {
    }
}
