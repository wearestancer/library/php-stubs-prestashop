<?php

namespace League\Tactician\Handler\MethodNameInflector;

/**
 * Returns a method name that is handle + the last portion of the class name
 * but also without a given suffix, typically "Command". This allows you to
 * handle multiple commands on a single object but with slightly less annoying
 * method names.
 *
 * The string removal is case sensitive.
 *
 * Examples:
 *  - \CompleteTaskCommand     => $handler->handleCompleteTask()
 *  - \My\App\DoThingCommand   => $handler->handleDoThing()
 */
class HandleClassNameWithoutSuffixInflector extends \League\Tactician\Handler\MethodNameInflector\HandleClassNameInflector
{
    /**
     * @param string $suffix The string to remove from end of each class name
     */
    public function __construct($suffix = 'Command')
    {
    }
    /**
     * @param object $command
     * @param object $commandHandler
     * @return string
     */
    public function inflect($command, $commandHandler)
    {
    }
}
