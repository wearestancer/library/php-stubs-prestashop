<?php

namespace League\Tactician\Handler\MethodNameInflector;

/**
 * Assumes the method is only the last portion of the class name.
 *
 * Examples:
 *  - \MyGlobalCommand    => $handler->myGlobalCommand()
 *  - \My\App\CreateUser  => $handler->createUser()
 */
class ClassNameInflector implements \League\Tactician\Handler\MethodNameInflector\MethodNameInflector
{
    /**
     * {@inheritdoc}
     */
    public function inflect($command, $commandHandler)
    {
    }
}
