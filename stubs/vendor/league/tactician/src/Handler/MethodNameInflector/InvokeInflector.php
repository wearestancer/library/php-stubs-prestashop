<?php

namespace League\Tactician\Handler\MethodNameInflector;

/**
 * Handle command by calling the __invoke magic method. Handy for single
 * use classes or closures.
 */
class InvokeInflector implements \League\Tactician\Handler\MethodNameInflector\MethodNameInflector
{
    /**
     * {@inheritdoc}
     */
    public function inflect($command, $commandHandler)
    {
    }
}
