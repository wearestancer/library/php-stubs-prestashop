<?php

namespace League\Tactician\Handler;

/**
 * The "core" CommandBus. Locates the appropriate handler and executes command.
 */
class CommandHandlerMiddleware implements \League\Tactician\Middleware
{
    /**
     * @param CommandNameExtractor $commandNameExtractor
     * @param HandlerLocator       $handlerLocator
     * @param MethodNameInflector  $methodNameInflector
     */
    public function __construct(\League\Tactician\Handler\CommandNameExtractor\CommandNameExtractor $commandNameExtractor, \League\Tactician\Handler\Locator\HandlerLocator $handlerLocator, \League\Tactician\Handler\MethodNameInflector\MethodNameInflector $methodNameInflector)
    {
    }
    /**
     * Executes a command and optionally returns a value
     *
     * @param object   $command
     * @param callable $next
     *
     * @return mixed
     *
     * @throws CanNotInvokeHandlerException
     */
    public function execute($command, callable $next)
    {
    }
}
