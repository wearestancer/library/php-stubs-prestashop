<?php

namespace League\Tactician\Handler\CommandNameExtractor;

/**
 * Extract the name from the class
 */
class ClassNameExtractor implements \League\Tactician\Handler\CommandNameExtractor\CommandNameExtractor
{
    /**
     * {@inheritdoc}
     */
    public function extract($command)
    {
    }
}
