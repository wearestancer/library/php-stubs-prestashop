<?php

namespace League\Tactician;

/**
 * Receives a command and sends it through a chain of middleware for processing.
 *
 * @final
 */
class CommandBus
{
    /**
     * @param Middleware[] $middleware
     */
    public function __construct(array $middleware)
    {
    }
    /**
     * Executes the given command and optionally returns a value
     *
     * @param object $command
     *
     * @return mixed
     */
    public function handle($command)
    {
    }
}
