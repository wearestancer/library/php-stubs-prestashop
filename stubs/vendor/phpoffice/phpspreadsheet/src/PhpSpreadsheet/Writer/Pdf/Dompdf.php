<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Pdf;

class Dompdf extends \PhpOffice\PhpSpreadsheet\Writer\Pdf
{
    /**
     * Gets the implementation of external PDF library that should be used.
     *
     * @return \Dompdf\Dompdf implementation
     */
    protected function createExternalWriterInstance()
    {
    }
    /**
     * Save Spreadsheet to file.
     *
     * @param string $filename Name of the file to save as
     */
    public function save($filename, int $flags = 0) : void
    {
    }
}
