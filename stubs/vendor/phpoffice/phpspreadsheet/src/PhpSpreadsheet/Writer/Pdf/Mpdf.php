<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Pdf;

class Mpdf extends \PhpOffice\PhpSpreadsheet\Writer\Pdf
{
    /**
     * Gets the implementation of external PDF library that should be used.
     *
     * @param array $config Configuration array
     *
     * @return \Mpdf\Mpdf implementation
     */
    protected function createExternalWriterInstance($config)
    {
    }
    /**
     * Save Spreadsheet to file.
     *
     * @param string $filename Name of the file to save as
     */
    public function save($filename, int $flags = 0) : void
    {
    }
}
