<?php

namespace PhpOffice\PhpSpreadsheet\Writer;

class Html extends \PhpOffice\PhpSpreadsheet\Writer\BaseWriter
{
    /**
     * Spreadsheet object.
     *
     * @var Spreadsheet
     */
    protected $spreadsheet;
    /**
     * Is the current writer creating PDF?
     *
     * @var bool
     */
    protected $isPdf = false;
    /**
     * Create a new HTML.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Save Spreadsheet to file.
     *
     * @param resource|string $filename
     */
    public function save($filename, int $flags = 0) : void
    {
    }
    /**
     * Save Spreadsheet as html to variable.
     *
     * @return string
     */
    public function generateHtmlAll()
    {
    }
    /**
     * Set a callback to edit the entire HTML.
     *
     * The callback must accept the HTML as string as first parameter,
     * and it must return the edited HTML as string.
     */
    public function setEditHtmlCallback(?callable $callback) : void
    {
    }
    const VALIGN_ARR = [\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM => 'bottom', \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP => 'top', \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER => 'middle', \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_JUSTIFY => 'middle'];
    const HALIGN_ARR = [\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT => 'left', \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT => 'right', \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER => 'center', \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS => 'center', \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY => 'justify'];
    const BORDER_ARR = [\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE => 'none', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOT => '1px dashed', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOTDOT => '1px dotted', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHED => '1px dashed', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED => '1px dotted', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE => '3px double', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR => '1px solid', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM => '2px solid', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOT => '2px dashed', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOTDOT => '2px dotted', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_SLANTDASHDOT => '2px dashed', \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK => '3px solid'];
    /**
     * Get sheet index.
     *
     * @return int
     */
    public function getSheetIndex()
    {
    }
    /**
     * Set sheet index.
     *
     * @param int $pValue Sheet index
     *
     * @return $this
     */
    public function setSheetIndex($pValue)
    {
    }
    /**
     * Get sheet index.
     *
     * @return bool
     */
    public function getGenerateSheetNavigationBlock()
    {
    }
    /**
     * Set sheet index.
     *
     * @param bool $pValue Flag indicating whether the sheet navigation block should be generated or not
     *
     * @return $this
     */
    public function setGenerateSheetNavigationBlock($pValue)
    {
    }
    /**
     * Write all sheets (resets sheetIndex to NULL).
     *
     * @return $this
     */
    public function writeAllSheets()
    {
    }
    /**
     * Generate HTML header.
     *
     * @param bool $pIncludeStyles Include styles?
     *
     * @return string
     */
    public function generateHTMLHeader($pIncludeStyles = false)
    {
    }
    /**
     * Generate sheet data.
     *
     * @return string
     */
    public function generateSheetData()
    {
    }
    /**
     * Generate sheet tabs.
     *
     * @return string
     */
    public function generateNavigation()
    {
    }
    /**
     * Convert Windows file name to file protocol URL.
     *
     * @param string $filename file name on local system
     *
     * @return string
     */
    public static function winFileToUrl($filename)
    {
    }
    /**
     * Generate CSS styles.
     *
     * @param bool $generateSurroundingHTML Generate surrounding HTML tags? (&lt;style&gt; and &lt;/style&gt;)
     *
     * @return string
     */
    public function generateStyles($generateSurroundingHTML = true)
    {
    }
    /**
     * Build CSS styles.
     *
     * @param bool $generateSurroundingHTML Generate surrounding HTML style? (html { })
     *
     * @return array
     */
    public function buildCSS($generateSurroundingHTML = true)
    {
    }
    /**
     * Generate HTML footer.
     */
    public function generateHTMLFooter()
    {
    }
    /**
     * Get images root.
     *
     * @return string
     */
    public function getImagesRoot()
    {
    }
    /**
     * Set images root.
     *
     * @param string $pValue
     *
     * @return $this
     */
    public function setImagesRoot($pValue)
    {
    }
    /**
     * Get embed images.
     *
     * @return bool
     */
    public function getEmbedImages()
    {
    }
    /**
     * Set embed images.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setEmbedImages($pValue)
    {
    }
    /**
     * Get use inline CSS?
     *
     * @return bool
     */
    public function getUseInlineCss()
    {
    }
    /**
     * Set use inline CSS?
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setUseInlineCss($pValue)
    {
    }
    /**
     * Get use embedded CSS?
     *
     * @return bool
     *
     * @codeCoverageIgnore
     *
     * @deprecated no longer used
     */
    public function getUseEmbeddedCSS()
    {
    }
    /**
     * Set use embedded CSS?
     *
     * @param bool $pValue
     *
     * @return $this
     *
     * @codeCoverageIgnore
     *
     * @deprecated no longer used
     */
    public function setUseEmbeddedCSS($pValue)
    {
    }
    /**
     * Add color to formatted string as inline style.
     *
     * @param string $pValue Plain formatted value without color
     * @param string $pFormat Format code
     *
     * @return string
     */
    public function formatColor($pValue, $pFormat)
    {
    }
}
