<?php

namespace PhpOffice\PhpSpreadsheet\Writer;

abstract class BaseWriter implements \PhpOffice\PhpSpreadsheet\Writer\IWriter
{
    /**
     * Write charts that are defined in the workbook?
     * Identifies whether the Writer should write definitions for any charts that exist in the PhpSpreadsheet object.
     *
     * @var bool
     */
    protected $includeCharts = false;
    /**
     * Pre-calculate formulas
     * Forces PhpSpreadsheet to recalculate all formulae in a workbook when saving, so that the pre-calculated values are
     * immediately available to MS Excel or other office spreadsheet viewer when opening the file.
     *
     * @var bool
     */
    protected $preCalculateFormulas = true;
    /**
     * @var resource
     */
    protected $fileHandle;
    public function getIncludeCharts()
    {
    }
    public function setIncludeCharts($includeCharts)
    {
    }
    public function getPreCalculateFormulas()
    {
    }
    public function setPreCalculateFormulas($precalculateFormulas)
    {
    }
    public function getUseDiskCaching()
    {
    }
    public function setUseDiskCaching($useDiskCache, $cacheDirectory = null)
    {
    }
    public function getDiskCachingDirectory()
    {
    }
    protected function processFlags(int $flags) : void
    {
    }
    /**
     * Open file handle.
     *
     * @param resource|string $filename
     */
    public function openFileHandle($filename) : void
    {
    }
    /**
     * Close file handle only if we opened it ourselves.
     */
    protected function maybeCloseFileHandle() : void
    {
    }
}
