<?php

namespace PhpOffice\PhpSpreadsheet\Writer;

class Ods extends \PhpOffice\PhpSpreadsheet\Writer\BaseWriter
{
    /**
     * Create a new Ods.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    public function getWriterPartContent() : \PhpOffice\PhpSpreadsheet\Writer\Ods\Content
    {
    }
    public function getWriterPartMeta() : \PhpOffice\PhpSpreadsheet\Writer\Ods\Meta
    {
    }
    public function getWriterPartMetaInf() : \PhpOffice\PhpSpreadsheet\Writer\Ods\MetaInf
    {
    }
    public function getWriterPartMimetype() : \PhpOffice\PhpSpreadsheet\Writer\Ods\Mimetype
    {
    }
    public function getWriterPartSettings() : \PhpOffice\PhpSpreadsheet\Writer\Ods\Settings
    {
    }
    public function getWriterPartStyles() : \PhpOffice\PhpSpreadsheet\Writer\Ods\Styles
    {
    }
    public function getWriterPartThumbnails() : \PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails
    {
    }
    /**
     * Save PhpSpreadsheet to file.
     *
     * @param resource|string $filename
     */
    public function save($filename, int $flags = 0) : void
    {
    }
    /**
     * Get Spreadsheet object.
     *
     * @return Spreadsheet
     */
    public function getSpreadsheet()
    {
    }
    /**
     * Set Spreadsheet object.
     *
     * @param Spreadsheet $spreadsheet PhpSpreadsheet object
     *
     * @return $this
     */
    public function setSpreadsheet(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
}
