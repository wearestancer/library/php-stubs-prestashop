<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RelsRibbon extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write relationships for additional objects of custom UI (ribbon).
     *
     * @return string XML Output
     */
    public function writeRibbonRelationships(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
}
