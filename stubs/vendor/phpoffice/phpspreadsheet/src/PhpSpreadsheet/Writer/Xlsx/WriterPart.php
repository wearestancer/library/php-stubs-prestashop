<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

abstract class WriterPart
{
    /**
     * Get parent Xlsx object.
     *
     * @return Xlsx
     */
    public function getParentWriter()
    {
    }
    /**
     * Set parent Xlsx object.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Writer\Xlsx $pWriter)
    {
    }
}
