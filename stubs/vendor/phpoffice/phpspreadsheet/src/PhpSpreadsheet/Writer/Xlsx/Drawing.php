<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Drawing extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write drawings to XML format.
     *
     * @param bool $includeCharts Flag indicating if we should include drawing details for charts
     *
     * @return string XML Output
     */
    public function writeDrawings(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $pWorksheet, $includeCharts = false)
    {
    }
    /**
     * Write drawings to XML format.
     *
     * @param XMLWriter $objWriter XML Writer
     * @param int $pRelationId
     */
    public function writeChart(\PhpOffice\PhpSpreadsheet\Shared\XMLWriter $objWriter, \PhpOffice\PhpSpreadsheet\Chart\Chart $pChart, $pRelationId = -1) : void
    {
    }
    /**
     * Write drawings to XML format.
     *
     * @param XMLWriter $objWriter XML Writer
     * @param int $pRelationId
     * @param null|int $hlinkClickId
     */
    public function writeDrawing(\PhpOffice\PhpSpreadsheet\Shared\XMLWriter $objWriter, \PhpOffice\PhpSpreadsheet\Worksheet\BaseDrawing $pDrawing, $pRelationId = -1, $hlinkClickId = null) : void
    {
    }
    /**
     * Write VML header/footer images to XML format.
     *
     * @return string XML Output
     */
    public function writeVMLHeaderFooterImages(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $pWorksheet)
    {
    }
    /**
     * Get an array of all drawings.
     *
     * @return \PhpOffice\PhpSpreadsheet\Worksheet\Drawing[] All drawings in PhpSpreadsheet
     */
    public function allDrawings(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
}
