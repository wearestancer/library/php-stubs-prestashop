<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Theme extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write theme to XML format.
     *
     * @return string XML Output
     */
    public function writeTheme(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
}
