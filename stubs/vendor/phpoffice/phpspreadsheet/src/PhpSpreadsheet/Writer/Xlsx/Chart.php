<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Chart extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    protected $calculateCellValues;
    /**
     * Write charts to XML format.
     *
     * @param mixed $calculateCellValues
     *
     * @return string XML Output
     */
    public function writeChart(\PhpOffice\PhpSpreadsheet\Chart\Chart $pChart, $calculateCellValues = true)
    {
    }
}
