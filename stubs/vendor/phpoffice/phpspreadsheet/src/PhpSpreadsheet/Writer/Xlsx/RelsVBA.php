<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RelsVBA extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write relationships for a signed VBA Project.
     *
     * @return string XML Output
     */
    public function writeVBARelationships(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
}
