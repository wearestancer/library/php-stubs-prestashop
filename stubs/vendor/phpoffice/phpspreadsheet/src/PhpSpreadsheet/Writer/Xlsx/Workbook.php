<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Workbook extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write workbook to XML format.
     *
     * @param bool $recalcRequired Indicate whether formulas should be recalculated before writing
     *
     * @return string XML Output
     */
    public function writeWorkbook(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet, $recalcRequired = false)
    {
    }
}
