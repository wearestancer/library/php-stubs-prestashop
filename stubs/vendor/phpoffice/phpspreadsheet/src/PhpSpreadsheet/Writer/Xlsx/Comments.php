<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Comments extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write comments to XML format.
     *
     * @return string XML Output
     */
    public function writeComments(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $pWorksheet)
    {
    }
    /**
     * Write VML comments to XML format.
     *
     * @return string XML Output
     */
    public function writeVMLComments(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $pWorksheet)
    {
    }
}
