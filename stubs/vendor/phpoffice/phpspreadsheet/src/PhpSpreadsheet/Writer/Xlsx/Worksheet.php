<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Worksheet extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write worksheet to XML format.
     *
     * @param string[] $pStringTable
     * @param bool $includeCharts Flag indicating if we should write charts
     *
     * @return string XML Output
     */
    public function writeWorksheet(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet, $pStringTable = null, $includeCharts = false)
    {
    }
}
