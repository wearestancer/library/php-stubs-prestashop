<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Style extends \PhpOffice\PhpSpreadsheet\Writer\Xlsx\WriterPart
{
    /**
     * Write styles to XML format.
     *
     * @return string XML Output
     */
    public function writeStyles(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get an array of all styles.
     *
     * @return \PhpOffice\PhpSpreadsheet\Style\Style[] All styles in PhpSpreadsheet
     */
    public function allStyles(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get an array of all conditional styles.
     *
     * @return Conditional[] All conditional styles in PhpSpreadsheet
     */
    public function allConditionalStyles(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get an array of all fills.
     *
     * @return Fill[] All fills in PhpSpreadsheet
     */
    public function allFills(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get an array of all fonts.
     *
     * @return Font[] All fonts in PhpSpreadsheet
     */
    public function allFonts(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get an array of all borders.
     *
     * @return Borders[] All borders in PhpSpreadsheet
     */
    public function allBorders(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get an array of all number formats.
     *
     * @return NumberFormat[] All number formats in PhpSpreadsheet
     */
    public function allNumberFormats(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
}
