<?php

namespace PhpOffice\PhpSpreadsheet\Writer;

class Xls extends \PhpOffice\PhpSpreadsheet\Writer\BaseWriter
{
    /**
     * Create a new Xls Writer.
     *
     * @param Spreadsheet $spreadsheet PhpSpreadsheet object
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Save Spreadsheet to file.
     *
     * @param resource|string $filename
     */
    public function save($filename, int $flags = 0) : void
    {
    }
}
