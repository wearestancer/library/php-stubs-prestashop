<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods\Cell;

class Style
{
    public const CELL_STYLE_PREFIX = 'ce';
    public function __construct(\PhpOffice\PhpSpreadsheet\Shared\XMLWriter $writer)
    {
    }
    protected function mapUnderlineStyle(\PhpOffice\PhpSpreadsheet\Style\Font $font) : string
    {
    }
    protected function writeTextProperties(\PhpOffice\PhpSpreadsheet\Style\Style $style) : void
    {
    }
    public function write(\PhpOffice\PhpSpreadsheet\Style\Style $style) : void
    {
    }
}
