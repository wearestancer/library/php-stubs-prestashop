<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

class MetaInf extends \PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart
{
    /**
     * Write META-INF/manifest.xml to XML format.
     *
     * @return string XML Output
     */
    public function write() : string
    {
    }
}
