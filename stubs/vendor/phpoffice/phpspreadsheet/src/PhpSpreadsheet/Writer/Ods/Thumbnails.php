<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

class Thumbnails extends \PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart
{
    /**
     * Write Thumbnails/thumbnail.png to PNG format.
     *
     * @return string XML Output
     */
    public function write() : string
    {
    }
}
