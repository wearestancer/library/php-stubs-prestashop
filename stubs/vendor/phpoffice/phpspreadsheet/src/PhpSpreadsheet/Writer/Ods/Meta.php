<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

class Meta extends \PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart
{
    /**
     * Write meta.xml to XML format.
     *
     * @return string XML Output
     */
    public function write() : string
    {
    }
}
