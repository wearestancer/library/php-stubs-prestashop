<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

class Settings extends \PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart
{
    /**
     * Write settings.xml to XML format.
     *
     * @return string XML Output
     */
    public function write() : string
    {
    }
}
