<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

/**
 * @author     Alexander Pervakov <frost-nzcr4@jagmort.com>
 */
class Content extends \PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart
{
    const NUMBER_COLS_REPEATED_MAX = 1024;
    const NUMBER_ROWS_REPEATED_MAX = 1048576;
    /**
     * Set parent Ods writer.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Writer\Ods $writer)
    {
    }
    /**
     * Write content.xml to XML format.
     *
     * @return string XML Output
     */
    public function write() : string
    {
    }
}
