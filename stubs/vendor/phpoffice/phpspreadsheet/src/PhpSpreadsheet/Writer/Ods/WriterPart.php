<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

abstract class WriterPart
{
    /**
     * Get Ods writer.
     *
     * @return Ods
     */
    public function getParentWriter()
    {
    }
    /**
     * Set parent Ods writer.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Writer\Ods $writer)
    {
    }
    public abstract function write() : string;
}
