<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods\Cell;

/**
 * @author     Alexander Pervakov <frost-nzcr4@jagmort.com>
 */
class Comment
{
    public static function write(\PhpOffice\PhpSpreadsheet\Shared\XMLWriter $objWriter, \PhpOffice\PhpSpreadsheet\Cell\Cell $cell) : void
    {
    }
}
