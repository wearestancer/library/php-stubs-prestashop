<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Ods;

class Mimetype extends \PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart
{
    /**
     * Write mimetype to plain text format.
     *
     * @return string XML Output
     */
    public function write() : string
    {
    }
}
