<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xls;

class Font
{
    /**
     * Constructor.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Style\Font $font)
    {
    }
    /**
     * Set the color index.
     *
     * @param int $colorIndex
     */
    public function setColorIndex($colorIndex) : void
    {
    }
    /**
     * Get font record data.
     *
     * @return string
     */
    public function writeFont()
    {
    }
}
