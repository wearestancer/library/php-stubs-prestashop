<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xls;

class Escher
{
    /**
     * Constructor.
     *
     * @param mixed $object
     */
    public function __construct($object)
    {
    }
    /**
     * Process the object to be written.
     *
     * @return string
     */
    public function close()
    {
    }
    /**
     * Gets the shape offsets.
     *
     * @return array
     */
    public function getSpOffsets()
    {
    }
    /**
     * Gets the shape types.
     *
     * @return array
     */
    public function getSpTypes()
    {
    }
}
