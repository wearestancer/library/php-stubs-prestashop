<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xls;

// Original file header of PEAR::Spreadsheet_Excel_Writer_Parser (used as the base for this class):
// -----------------------------------------------------------------------------------------
// *  Class for parsing Excel formulas
// *
// *  License Information:
// *
// *    Spreadsheet_Excel_Writer:  A library for generating Excel Spreadsheets
// *    Copyright (c) 2002-2003 Xavier Noguer xnoguer@rezebra.com
// *
// *    This library is free software; you can redistribute it and/or
// *    modify it under the terms of the GNU Lesser General Public
// *    License as published by the Free Software Foundation; either
// *    version 2.1 of the License, or (at your option) any later version.
// *
// *    This library is distributed in the hope that it will be useful,
// *    but WITHOUT ANY WARRANTY; without even the implied warranty of
// *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// *    Lesser General Public License for more details.
// *
// *    You should have received a copy of the GNU Lesser General Public
// *    License along with this library; if not, write to the Free Software
// *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// */
class Parser
{
    /**    Constants                */
    // Sheet title in unquoted form
    // Invalid sheet title characters cannot occur in the sheet title:
    //         *:/\?[]
    // Moreover, there are valid sheet title characters that cannot occur in unquoted form (there may be more?)
    // +-% '^&<>=,;#()"{}
    const REGEX_SHEET_TITLE_UNQUOTED = '[^\\*\\:\\/\\\\\\?\\[\\]\\+\\-\\% \\\'\\^\\&\<\>\\=\\,\\;\\#\\(\\)\\"\\{\\}]+';
    // Sheet title in quoted form (without surrounding quotes)
    // Invalid sheet title characters cannot occur in the sheet title:
    // *:/\?[]                    (usual invalid sheet title characters)
    // Single quote is represented as a pair ''
    const REGEX_SHEET_TITLE_QUOTED = '(([^\\*\\:\\/\\\\\\?\\[\\]\\\'])+|(\\\'\\\')+)+';
    /**
     * The index of the character we are currently looking at.
     *
     * @var int
     */
    public $currentCharacter;
    /**
     * The token we are working on.
     *
     * @var string
     */
    public $currentToken;
    /**
     * The character ahead of the current char.
     *
     * @var string
     */
    public $lookAhead;
    /**
     * The parse tree to be generated.
     *
     * @var string
     */
    public $parseTree;
    /**
     * Array of sheet references in the form of REF structures.
     *
     * @var array
     */
    public $references;
    /**
     * The class constructor.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * This method is used to update the array of sheet names. It is
     * called by the addWorksheet() method of the
     * \PhpOffice\PhpSpreadsheet\Writer\Xls\Workbook class.
     *
     * @param string $name The name of the worksheet being added
     * @param int $index The index of the worksheet being added
     *
     * @see \PhpOffice\PhpSpreadsheet\Writer\Xls\Workbook::addWorksheet()
     */
    public function setExtSheet($name, $index) : void
    {
    }
    /**
     * The parsing method. It parses a formula.
     *
     * @param string $formula the formula to parse, without the initial equal
     *                        sign (=)
     *
     * @return mixed true on success
     */
    public function parse($formula)
    {
    }
    /**
     * Builds a string containing the tree in reverse polish notation (What you
     * would use in a HP calculator stack).
     * The following tree:.
     *
     *    +
     *   / \
     *  2   3
     *
     * produces: "23+"
     *
     * The following tree:
     *
     *    +
     *   / \
     *  3   *
     *     / \
     *    6   A1
     *
     * produces: "36A1*+"
     *
     * In fact all operands, functions, references, etc... are written as ptg's
     *
     * @param array $tree the optional tree to convert
     *
     * @return string The tree in reverse polish notation
     */
    public function toReversePolish($tree = [])
    {
    }
}
