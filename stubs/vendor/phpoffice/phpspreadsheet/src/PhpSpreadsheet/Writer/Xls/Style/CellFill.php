<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xls\Style;

class CellFill
{
    /**
     * @var array<string, int>
     */
    protected static $fillStyleMap = [
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE => 0x0,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID => 0x1,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_MEDIUMGRAY => 0x2,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY => 0x3,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTGRAY => 0x4,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKHORIZONTAL => 0x5,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKVERTICAL => 0x6,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKDOWN => 0x7,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKUP => 0x8,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRID => 0x9,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKTRELLIS => 0xa,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTHORIZONTAL => 0xb,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTVERTICAL => 0xc,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTDOWN => 0xd,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTUP => 0xe,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTGRID => 0xf,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTTRELLIS => 0x10,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_GRAY125 => 0x11,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_GRAY0625 => 0x12,
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR => 0x0,
        // does not exist in BIFF8
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_PATH => 0x0,
    ];
    public static function style(\PhpOffice\PhpSpreadsheet\Style\Fill $fill) : int
    {
    }
}
