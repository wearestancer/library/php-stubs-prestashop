<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xls;

class CellDataValidation
{
    /**
     * @var array<string, int>
     */
    protected static $validationTypeMap = [\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_NONE => 0x0, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE => 0x1, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_DECIMAL => 0x2, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST => 0x3, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_DATE => 0x4, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_TIME => 0x5, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_TEXTLENGTH => 0x6, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_CUSTOM => 0x7];
    /**
     * @var array<string, int>
     */
    protected static $errorStyleMap = [\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP => 0x0, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_WARNING => 0x1, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION => 0x2];
    /**
     * @var array<string, int>
     */
    protected static $operatorMap = [\PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_BETWEEN => 0x0, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_NOTBETWEEN => 0x1, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_EQUAL => 0x2, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_NOTEQUAL => 0x3, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_GREATERTHAN => 0x4, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_LESSTHAN => 0x5, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_GREATERTHANOREQUAL => 0x6, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::OPERATOR_LESSTHANOREQUAL => 0x7];
    public static function type(\PhpOffice\PhpSpreadsheet\Cell\DataValidation $dataValidation) : int
    {
    }
    public static function errorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation $dataValidation) : int
    {
    }
    public static function operator(\PhpOffice\PhpSpreadsheet\Cell\DataValidation $dataValidation) : int
    {
    }
}
