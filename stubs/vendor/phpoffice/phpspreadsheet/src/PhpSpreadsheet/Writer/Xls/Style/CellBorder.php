<?php

namespace PhpOffice\PhpSpreadsheet\Writer\Xls\Style;

class CellBorder
{
    /**
     * @var array<string, int>
     */
    protected static $styleMap = [\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE => 0x0, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN => 0x1, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM => 0x2, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHED => 0x3, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED => 0x4, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK => 0x5, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE => 0x6, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR => 0x7, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHED => 0x8, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOT => 0x9, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOT => 0xa, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOTDOT => 0xb, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOTDOT => 0xc, \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_SLANTDASHDOT => 0xd];
    public static function style(\PhpOffice\PhpSpreadsheet\Style\Border $border) : int
    {
    }
}
