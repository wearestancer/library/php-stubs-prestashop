<?php

namespace PhpOffice\PhpSpreadsheet\Writer;

class Csv extends \PhpOffice\PhpSpreadsheet\Writer\BaseWriter
{
    /**
     * Create a new CSV.
     *
     * @param Spreadsheet $spreadsheet Spreadsheet object
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Save PhpSpreadsheet to file.
     *
     * @param resource|string $filename
     */
    public function save($filename, int $flags = 0) : void
    {
    }
    /**
     * Get delimiter.
     *
     * @return string
     */
    public function getDelimiter()
    {
    }
    /**
     * Set delimiter.
     *
     * @param string $pValue Delimiter, defaults to ','
     *
     * @return $this
     */
    public function setDelimiter($pValue)
    {
    }
    /**
     * Get enclosure.
     *
     * @return string
     */
    public function getEnclosure()
    {
    }
    /**
     * Set enclosure.
     *
     * @param string $pValue Enclosure, defaults to "
     *
     * @return $this
     */
    public function setEnclosure($pValue = '"')
    {
    }
    /**
     * Get line ending.
     *
     * @return string
     */
    public function getLineEnding()
    {
    }
    /**
     * Set line ending.
     *
     * @param string $pValue Line ending, defaults to OS line ending (PHP_EOL)
     *
     * @return $this
     */
    public function setLineEnding($pValue)
    {
    }
    /**
     * Get whether BOM should be used.
     *
     * @return bool
     */
    public function getUseBOM()
    {
    }
    /**
     * Set whether BOM should be used.
     *
     * @param bool $pValue Use UTF-8 byte-order mark? Defaults to false
     *
     * @return $this
     */
    public function setUseBOM($pValue)
    {
    }
    /**
     * Get whether a separator line should be included.
     *
     * @return bool
     */
    public function getIncludeSeparatorLine()
    {
    }
    /**
     * Set whether a separator line should be included as the first line of the file.
     *
     * @param bool $pValue Use separator line? Defaults to false
     *
     * @return $this
     */
    public function setIncludeSeparatorLine($pValue)
    {
    }
    /**
     * Get whether the file should be saved with full Excel Compatibility.
     *
     * @return bool
     */
    public function getExcelCompatibility()
    {
    }
    /**
     * Set whether the file should be saved with full Excel Compatibility.
     *
     * @param bool $pValue Set the file to be written as a fully Excel compatible csv file
     *                                Note that this overrides other settings such as useBOM, enclosure and delimiter
     *
     * @return $this
     */
    public function setExcelCompatibility($pValue)
    {
    }
    /**
     * Get sheet index.
     *
     * @return int
     */
    public function getSheetIndex()
    {
    }
    /**
     * Set sheet index.
     *
     * @param int $pValue Sheet index
     *
     * @return $this
     */
    public function setSheetIndex($pValue)
    {
    }
    /**
     * Get output encoding.
     *
     * @return string
     */
    public function getOutputEncoding()
    {
    }
    /**
     * Set output encoding.
     *
     * @param string $pValue Output encoding
     *
     * @return $this
     */
    public function setOutputEncoding($pValue)
    {
    }
    public function setEnclosureRequired(bool $value) : self
    {
    }
    public function getEnclosureRequired() : bool
    {
    }
}
