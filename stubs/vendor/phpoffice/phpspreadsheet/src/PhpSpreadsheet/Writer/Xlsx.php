<?php

namespace PhpOffice\PhpSpreadsheet\Writer;

class Xlsx extends \PhpOffice\PhpSpreadsheet\Writer\BaseWriter
{
    /**
     * Create a new Xlsx Writer.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    public function getWriterPartChart() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Chart
    {
    }
    public function getWriterPartComments() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Comments
    {
    }
    public function getWriterPartContentTypes() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\ContentTypes
    {
    }
    public function getWriterPartDocProps() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\DocProps
    {
    }
    public function getWriterPartDrawing() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Drawing
    {
    }
    public function getWriterPartRels() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels
    {
    }
    public function getWriterPartRelsRibbon() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\RelsRibbon
    {
    }
    public function getWriterPartRelsVBA() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\RelsVBA
    {
    }
    public function getWriterPartStringTable() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\StringTable
    {
    }
    public function getWriterPartStyle() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Style
    {
    }
    public function getWriterPartTheme() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Theme
    {
    }
    public function getWriterPartWorkbook() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Workbook
    {
    }
    public function getWriterPartWorksheet() : \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet
    {
    }
    /**
     * Save PhpSpreadsheet to file.
     *
     * @param resource|string $filename
     */
    public function save($filename, int $flags = 0) : void
    {
    }
    /**
     * Get Spreadsheet object.
     *
     * @return Spreadsheet
     */
    public function getSpreadsheet()
    {
    }
    /**
     * Set Spreadsheet object.
     *
     * @param Spreadsheet $spreadsheet PhpSpreadsheet object
     *
     * @return $this
     */
    public function setSpreadsheet(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get string table.
     *
     * @return string[]
     */
    public function getStringTable()
    {
    }
    /**
     * Get Style HashTable.
     *
     * @return HashTable<\PhpOffice\PhpSpreadsheet\Style\Style>
     */
    public function getStyleHashTable()
    {
    }
    /**
     * Get Conditional HashTable.
     *
     * @return HashTable<Conditional>
     */
    public function getStylesConditionalHashTable()
    {
    }
    /**
     * Get Fill HashTable.
     *
     * @return HashTable<Fill>
     */
    public function getFillHashTable()
    {
    }
    /**
     * Get \PhpOffice\PhpSpreadsheet\Style\Font HashTable.
     *
     * @return HashTable<Font>
     */
    public function getFontHashTable()
    {
    }
    /**
     * Get Borders HashTable.
     *
     * @return HashTable<Borders>
     */
    public function getBordersHashTable()
    {
    }
    /**
     * Get NumberFormat HashTable.
     *
     * @return HashTable<NumberFormat>
     */
    public function getNumFmtHashTable()
    {
    }
    /**
     * Get \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet\BaseDrawing HashTable.
     *
     * @return HashTable<BaseDrawing>
     */
    public function getDrawingHashTable()
    {
    }
    /**
     * Get Office2003 compatibility.
     *
     * @return bool
     */
    public function getOffice2003Compatibility()
    {
    }
    /**
     * Set Office2003 compatibility.
     *
     * @param bool $pValue Office2003 compatibility?
     *
     * @return $this
     */
    public function setOffice2003Compatibility($pValue)
    {
    }
}
