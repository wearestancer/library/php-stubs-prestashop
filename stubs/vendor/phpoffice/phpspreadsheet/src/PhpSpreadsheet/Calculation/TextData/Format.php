<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Format
{
    /**
     * DOLLAR.
     *
     * This function converts a number to text using currency format, with the decimals rounded to the specified place.
     * The format used is $#,##0.00_);($#,##0.00)..
     *
     * @param mixed $value The value to format
     * @param mixed $decimals The number of digits to display to the right of the decimal point (as an integer).
     *                            If decimals is negative, number is rounded to the left of the decimal point.
     *                            If you omit decimals, it is assumed to be 2
     */
    public static function DOLLAR($value = 0, $decimals = 2) : string
    {
    }
    /**
     * FIXED.
     *
     * @param mixed $value The value to format
     * @param mixed $decimals Integer value for the number of decimal places that should be formatted
     * @param mixed $noCommas Boolean value indicating whether the value should have thousands separators or not
     */
    public static function FIXEDFORMAT($value, $decimals = 2, $noCommas = false) : string
    {
    }
    /**
     * TEXT.
     *
     * @param mixed $value The value to format
     * @param mixed $format A string with the Format mask that should be used
     */
    public static function TEXTFORMAT($value, $format) : string
    {
    }
    /**
     * VALUE.
     *
     * @param mixed $value Value to check
     *
     * @return DateTimeInterface|float|int|string A string if arguments are invalid
     */
    public static function VALUE($value = '')
    {
    }
    /**
     * NUMBERVALUE.
     *
     * @param mixed $value The value to format
     * @param mixed $decimalSeparator A string with the decimal separator to use, defaults to locale defined value
     * @param mixed $groupSeparator A string with the group/thousands separator to use, defaults to locale defined value
     *
     * @return float|string
     */
    public static function NUMBERVALUE($value = '', $decimalSeparator = null, $groupSeparator = null)
    {
    }
}
