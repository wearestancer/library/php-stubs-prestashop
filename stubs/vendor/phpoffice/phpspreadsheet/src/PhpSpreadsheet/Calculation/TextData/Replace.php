<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Replace
{
    /**
     * REPLACE.
     *
     * @param mixed $oldText The text string value to modify
     * @param mixed $start Integer offset for start character of the replacement
     * @param mixed $chars Integer number of characters to replace from the start offset
     * @param mixed $newText String to replace in the defined position
     */
    public static function replace($oldText, $start, $chars, $newText) : string
    {
    }
    /**
     * SUBSTITUTE.
     *
     * @param mixed $text The text string value to modify
     * @param mixed $fromText The string value that we want to replace in $text
     * @param mixed $toText The string value that we want to replace with in $text
     * @param mixed $instance Integer instance Number for the occurrence of frmText to change
     */
    public static function substitute($text = '', $fromText = '', $toText = '', $instance = null) : string
    {
    }
}
