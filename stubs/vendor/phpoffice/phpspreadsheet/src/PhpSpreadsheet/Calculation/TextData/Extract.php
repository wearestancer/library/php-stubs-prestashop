<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Extract
{
    /**
     * LEFT.
     *
     * @param mixed $value String value from which to extract characters
     * @param mixed $chars The number of characters to extract (as an integer)
     */
    public static function left($value, $chars = 1) : string
    {
    }
    /**
     * MID.
     *
     * @param mixed $value String value from which to extract characters
     * @param mixed $start Integer offset of the first character that we want to extract
     * @param mixed $chars The number of characters to extract (as an integer)
     */
    public static function mid($value, $start, $chars) : string
    {
    }
    /**
     * RIGHT.
     *
     * @param mixed $value String value from which to extract characters
     * @param mixed $chars The number of characters to extract (as an integer)
     */
    public static function right($value, $chars = 1) : string
    {
    }
}
