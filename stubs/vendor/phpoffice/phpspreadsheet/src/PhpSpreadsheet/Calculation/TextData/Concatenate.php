<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Concatenate
{
    /**
     * CONCATENATE.
     *
     * @param array $args
     */
    public static function CONCATENATE(...$args) : string
    {
    }
    /**
     * TEXTJOIN.
     *
     * @param mixed $delimiter
     * @param mixed $ignoreEmpty
     * @param mixed $args
     */
    public static function TEXTJOIN($delimiter, $ignoreEmpty, ...$args) : string
    {
    }
    /**
     * REPT.
     *
     * Returns the result of builtin function round after validating args.
     *
     * @param mixed $stringValue The value to repeat
     * @param mixed $repeatCount The number of times the string value should be repeated
     */
    public static function builtinREPT($stringValue, $repeatCount) : string
    {
    }
}
