<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class CharacterConvert
{
    /**
     * CHAR.
     *
     * @param mixed $character Integer Value to convert to its character representation
     */
    public static function character($character) : string
    {
    }
    /**
     * CODE.
     *
     * @param mixed $characters String character to convert to its ASCII value
     *
     * @return int|string A string if arguments are invalid
     */
    public static function code($characters)
    {
    }
}
