<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Text
{
    /**
     * LEN.
     *
     * @param mixed $value String Value
     */
    public static function length($value = '') : int
    {
    }
    /**
     * Compares two text strings and returns TRUE if they are exactly the same, FALSE otherwise.
     * EXACT is case-sensitive but ignores formatting differences.
     * Use EXACT to test text being entered into a document.
     *
     * @param mixed $value1 String Value
     * @param mixed $value2 String Value
     */
    public static function exact($value1, $value2) : bool
    {
    }
    /**
     * RETURNSTRING.
     *
     * @param mixed $testValue Value to check
     *
     * @return null|string
     */
    public static function test($testValue = '')
    {
    }
}
