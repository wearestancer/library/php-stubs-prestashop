<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Helpers
{
    public static function convertBooleanValue(bool $value) : string
    {
    }
    /**
     * @param mixed $value String value from which to extract characters
     */
    public static function extractString($value) : string
    {
    }
    /**
     * @param mixed $value
     */
    public static function extractInt($value, int $minValue, int $gnumericNull = 0, bool $ooBoolOk = false) : int
    {
    }
    /**
     * @param mixed $value
     */
    public static function extractFloat($value) : float
    {
    }
    /**
     * @param mixed $value
     */
    public static function validateInt($value) : int
    {
    }
}
