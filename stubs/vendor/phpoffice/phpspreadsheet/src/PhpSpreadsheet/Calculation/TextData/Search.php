<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\TextData;

class Search
{
    /**
     * FIND (case sensitive search).
     *
     * @param mixed $needle The string to look for
     * @param mixed $haystack The string in which to look
     * @param mixed $offset Integer offset within $haystack to start searching from
     *
     * @return int|string
     */
    public static function sensitive($needle, $haystack, $offset = 1)
    {
    }
    /**
     * SEARCH (case insensitive search).
     *
     * @param mixed $needle The string to look for
     * @param mixed $haystack The string in which to look
     * @param mixed $offset Integer offset within $haystack to start searching from
     *
     * @return int|string
     */
    public static function insensitive($needle, $haystack, $offset = 1)
    {
    }
}
