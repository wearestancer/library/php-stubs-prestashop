<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Factorial
{
    /**
     * FACT.
     *
     * Returns the factorial of a number.
     * The factorial of a number is equal to 1*2*3*...* number.
     *
     * Excel Function:
     *        FACT(factVal)
     *
     * @param float $factVal Factorial Value
     *
     * @return float|int|string Factorial, or a string containing an error
     */
    public static function fact($factVal)
    {
    }
    /**
     * FACTDOUBLE.
     *
     * Returns the double factorial of a number.
     *
     * Excel Function:
     *        FACTDOUBLE(factVal)
     *
     * @param float $factVal Factorial Value
     *
     * @return float|int|string Double Factorial, or a string containing an error
     */
    public static function factDouble($factVal)
    {
    }
    /**
     * MULTINOMIAL.
     *
     * Returns the ratio of the factorial of a sum of values to the product of factorials.
     *
     * @param mixed[] $args An array of mixed values for the Data Series
     *
     * @return float|string The result, or a string containing an error
     */
    public static function multinomial(...$args)
    {
    }
}
