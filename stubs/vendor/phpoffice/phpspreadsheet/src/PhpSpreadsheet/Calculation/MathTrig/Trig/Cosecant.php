<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Trig;

class Cosecant
{
    /**
     * CSC.
     *
     * Returns the cosecant of an angle.
     *
     * @param float $angle Number
     *
     * @return float|string The cosecant of the angle
     */
    public static function csc($angle)
    {
    }
    /**
     * CSCH.
     *
     * Returns the hyperbolic cosecant of an angle.
     *
     * @param float $angle Number
     *
     * @return float|string The hyperbolic cosecant of the angle
     */
    public static function csch($angle)
    {
    }
}
