<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class SumSquares
{
    /**
     * SUMSQ.
     *
     * SUMSQ returns the sum of the squares of the arguments
     *
     * Excel Function:
     *        SUMSQ(value1[,value2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float|string
     */
    public static function sumSquare(...$args)
    {
    }
    /**
     * SUMX2MY2.
     *
     * @param mixed[] $matrixData1 Matrix #1
     * @param mixed[] $matrixData2 Matrix #2
     *
     * @return float|string
     */
    public static function sumXSquaredMinusYSquared($matrixData1, $matrixData2)
    {
    }
    /**
     * SUMX2PY2.
     *
     * @param mixed[] $matrixData1 Matrix #1
     * @param mixed[] $matrixData2 Matrix #2
     *
     * @return float|string
     */
    public static function sumXSquaredPlusYSquared($matrixData1, $matrixData2)
    {
    }
    /**
     * SUMXMY2.
     *
     * @param mixed[] $matrixData1 Matrix #1
     * @param mixed[] $matrixData2 Matrix #2
     *
     * @return float|string
     */
    public static function sumXMinusYSquared($matrixData1, $matrixData2)
    {
    }
}
