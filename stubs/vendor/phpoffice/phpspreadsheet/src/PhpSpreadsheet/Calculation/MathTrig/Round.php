<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Round
{
    /**
     * ROUND.
     *
     * Returns the result of builtin function round after validating args.
     *
     * @param mixed $number Should be numeric
     * @param mixed $precision Should be int
     *
     * @return float|string Rounded number
     */
    public static function round($number, $precision)
    {
    }
    /**
     * ROUNDUP.
     *
     * Rounds a number up to a specified number of decimal places
     *
     * @param float $number Number to round
     * @param int $digits Number of digits to which you want to round $number
     *
     * @return float|string Rounded Number, or a string containing an error
     */
    public static function up($number, $digits)
    {
    }
    /**
     * ROUNDDOWN.
     *
     * Rounds a number down to a specified number of decimal places
     *
     * @param float $number Number to round
     * @param int $digits Number of digits to which you want to round $number
     *
     * @return float|string Rounded Number, or a string containing an error
     */
    public static function down($number, $digits)
    {
    }
    /**
     * MROUND.
     *
     * Rounds a number to the nearest multiple of a specified value
     *
     * @param mixed $number Expect float. Number to round.
     * @param mixed $multiple Expect int. Multiple to which you want to round.
     *
     * @return float|string Rounded Number, or a string containing an error
     */
    public static function multiple($number, $multiple)
    {
    }
    /**
     * EVEN.
     *
     * Returns number rounded up to the nearest even integer.
     * You can use this function for processing items that come in twos. For example,
     *        a packing crate accepts rows of one or two items. The crate is full when
     *        the number of items, rounded up to the nearest two, matches the crate's
     *        capacity.
     *
     * Excel Function:
     *        EVEN(number)
     *
     * @param float $number Number to round
     *
     * @return float|string Rounded Number, or a string containing an error
     */
    public static function even($number)
    {
    }
    /**
     * ODD.
     *
     * Returns number rounded up to the nearest odd integer.
     *
     * @param float $number Number to round
     *
     * @return float|string Rounded Number, or a string containing an error
     */
    public static function odd($number)
    {
    }
}
