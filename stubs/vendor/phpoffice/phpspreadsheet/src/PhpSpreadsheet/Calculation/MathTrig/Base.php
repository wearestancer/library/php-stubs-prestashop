<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Base
{
    /**
     * BASE.
     *
     * Converts a number into a text representation with the given radix (base).
     *
     * Excel Function:
     *        BASE(Number, Radix [Min_length])
     *
     * @param mixed $number expect float
     * @param mixed $radix expect float
     * @param mixed $minLength expect int or null
     *
     * @return string the text representation with the given radix (base)
     */
    public static function evaluate($number, $radix, $minLength = null)
    {
    }
}
