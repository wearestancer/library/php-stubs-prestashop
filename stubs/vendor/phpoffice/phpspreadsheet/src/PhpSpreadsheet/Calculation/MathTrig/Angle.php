<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Angle
{
    /**
     * DEGREES.
     *
     * Returns the result of builtin function rad2deg after validating args.
     *
     * @param mixed $number Should be numeric
     *
     * @return float|string Rounded number
     */
    public static function toDegrees($number)
    {
    }
    /**
     * RADIANS.
     *
     * Returns the result of builtin function deg2rad after validating args.
     *
     * @param mixed $number Should be numeric
     *
     * @return float|string Rounded number
     */
    public static function toRadians($number)
    {
    }
}
