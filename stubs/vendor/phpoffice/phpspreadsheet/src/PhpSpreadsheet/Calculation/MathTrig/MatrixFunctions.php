<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class MatrixFunctions
{
    /**
     * MDETERM.
     *
     * Returns the matrix determinant of an array.
     *
     * Excel Function:
     *        MDETERM(array)
     *
     * @param mixed $matrixValues A matrix of values
     *
     * @return float|string The result, or a string containing an error
     */
    public static function determinant($matrixValues)
    {
    }
    /**
     * MINVERSE.
     *
     * Returns the inverse matrix for the matrix stored in an array.
     *
     * Excel Function:
     *        MINVERSE(array)
     *
     * @param mixed $matrixValues A matrix of values
     *
     * @return array|string The result, or a string containing an error
     */
    public static function inverse($matrixValues)
    {
    }
    /**
     * MMULT.
     *
     * @param mixed $matrixData1 A matrix of values
     * @param mixed $matrixData2 A matrix of values
     *
     * @return array|string The result, or a string containing an error
     */
    public static function multiply($matrixData1, $matrixData2)
    {
    }
    /**
     * MUnit.
     *
     * @param mixed $dimension Number of rows and columns
     *
     * @return array|string The result, or a string containing an error
     */
    public static function identity($dimension)
    {
    }
}
