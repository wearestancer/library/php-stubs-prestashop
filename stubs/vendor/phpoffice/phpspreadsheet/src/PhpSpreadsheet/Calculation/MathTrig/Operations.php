<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Operations
{
    /**
     * MOD.
     *
     * @param mixed $dividend Dividend
     * @param mixed $divisor Divisor
     *
     * @return float|int|string Remainder, or a string containing an error
     */
    public static function mod($dividend, $divisor)
    {
    }
    /**
     * POWER.
     *
     * Computes x raised to the power y.
     *
     * @param float|int $x
     * @param float|int $y
     *
     * @return float|int|string The result, or a string containing an error
     */
    public static function power($x, $y)
    {
    }
    /**
     * PRODUCT.
     *
     * PRODUCT returns the product of all the values and cells referenced in the argument list.
     *
     * Excel Function:
     *        PRODUCT(value1[,value2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float|string
     */
    public static function product(...$args)
    {
    }
    /**
     * QUOTIENT.
     *
     * QUOTIENT function returns the integer portion of a division. Numerator is the divided number
     *        and denominator is the divisor.
     *
     * Excel Function:
     *        QUOTIENT(value1,value2)
     *
     * @param mixed $numerator Expect float|int
     * @param mixed $denominator Expect float|int
     *
     * @return int|string
     */
    public static function quotient($numerator, $denominator)
    {
    }
}
