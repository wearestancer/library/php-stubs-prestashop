<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Gcd
{
    /**
     * GCD.
     *
     * Returns the greatest common divisor of a series of numbers.
     * The greatest common divisor is the largest integer that divides both
     *        number1 and number2 without a remainder.
     *
     * Excel Function:
     *        GCD(number1[,number2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float|int|string Greatest Common Divisor, or a string containing an error
     */
    public static function evaluate(...$args)
    {
    }
}
