<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Roman
{
    const MAX_ROMAN_VALUE = 3999;
    const MAX_ROMAN_STYLE = 4;
    public static function calculateRoman(int $aValue, int $style) : string
    {
    }
    /**
     * ROMAN.
     *
     * Converts a number to Roman numeral
     *
     * @param mixed $aValue Number to convert
     * @param mixed $style Number indicating one of five possible forms
     *
     * @return string Roman numeral, or a string containing an error
     */
    public static function evaluate($aValue, $style = 0)
    {
    }
}
