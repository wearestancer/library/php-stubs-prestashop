<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Arabic
{
    /**
     * ARABIC.
     *
     * Converts a Roman numeral to an Arabic numeral.
     *
     * Excel Function:
     *        ARABIC(text)
     *
     * @param string $roman
     *
     * @return int|string the arabic numberal contrived from the roman numeral
     */
    public static function evaluate($roman)
    {
    }
}
