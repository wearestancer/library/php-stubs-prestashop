<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Exp
{
    /**
     * EXP.
     *
     * Returns the result of builtin function exp after validating args.
     *
     * @param mixed $number Should be numeric
     *
     * @return float|string Rounded number
     */
    public static function evaluate($number)
    {
    }
}
