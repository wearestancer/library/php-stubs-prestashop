<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Subtotal
{
    /**
     * @param mixed $cellReference
     * @param mixed $args
     */
    protected static function filterHiddenArgs($cellReference, $args) : array
    {
    }
    /**
     * @param mixed $cellReference
     * @param mixed $args
     */
    protected static function filterFormulaArgs($cellReference, $args) : array
    {
    }
    /**
     * SUBTOTAL.
     *
     * Returns a subtotal in a list or database.
     *
     * @param mixed $functionType
     *            A number 1 to 11 that specifies which function to
     *                    use in calculating subtotals within a range
     *                    list
     *            Numbers 101 to 111 shadow the functions of 1 to 11
     *                    but ignore any values in the range that are
     *                    in hidden rows
     * @param mixed[] $args A mixed data series of values
     *
     * @return float|string
     */
    public static function evaluate($functionType, ...$args)
    {
    }
}
