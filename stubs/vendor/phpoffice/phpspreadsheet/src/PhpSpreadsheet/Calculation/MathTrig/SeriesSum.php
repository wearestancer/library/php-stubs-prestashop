<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class SeriesSum
{
    /**
     * SERIESSUM.
     *
     * Returns the sum of a power series
     *
     * @param mixed $x Input value
     * @param mixed $n Initial power
     * @param mixed $m Step
     * @param mixed[] $args An array of coefficients for the Data Series
     *
     * @return float|string The result, or a string containing an error
     */
    public static function evaluate($x, $n, $m, ...$args)
    {
    }
}
