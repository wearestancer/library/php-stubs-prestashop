<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Trunc
{
    /**
     * TRUNC.
     *
     * Truncates value to the number of fractional digits by number_digits.
     *
     * @param float $value
     * @param int $digits
     *
     * @return float|string Truncated value, or a string containing an error
     */
    public static function evaluate($value = 0, $digits = 0)
    {
    }
}
