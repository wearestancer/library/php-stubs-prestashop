<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Absolute
{
    /**
     * ABS.
     *
     * Returns the result of builtin function abs after validating args.
     *
     * @param mixed $number Should be numeric
     *
     * @return float|int|string Rounded number
     */
    public static function evaluate($number)
    {
    }
}
