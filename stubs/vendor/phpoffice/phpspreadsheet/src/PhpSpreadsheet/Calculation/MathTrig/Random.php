<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Random
{
    /**
     * RAND.
     *
     * @return float Random number
     */
    public static function rand()
    {
    }
    /**
     * RANDBETWEEN.
     *
     * @param mixed $min Minimal value
     * @param mixed $max Maximal value
     *
     * @return float|int|string Random number
     */
    public static function randBetween($min, $max)
    {
    }
}
