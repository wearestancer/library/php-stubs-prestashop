<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Combinations
{
    /**
     * COMBIN.
     *
     * Returns the number of combinations for a given number of items. Use COMBIN to
     *        determine the total possible number of groups for a given number of items.
     *
     * Excel Function:
     *        COMBIN(numObjs,numInSet)
     *
     * @param mixed $numObjs Number of different objects
     * @param mixed $numInSet Number of objects in each combination
     *
     * @return float|int|string Number of combinations, or a string containing an error
     */
    public static function withoutRepetition($numObjs, $numInSet)
    {
    }
    /**
     * COMBIN.
     *
     * Returns the number of combinations for a given number of items. Use COMBIN to
     *        determine the total possible number of groups for a given number of items.
     *
     * Excel Function:
     *        COMBIN(numObjs,numInSet)
     *
     * @param mixed $numObjs Number of different objects
     * @param mixed $numInSet Number of objects in each combination
     *
     * @return float|int|string Number of combinations, or a string containing an error
     */
    public static function withRepetition($numObjs, $numInSet)
    {
    }
}
