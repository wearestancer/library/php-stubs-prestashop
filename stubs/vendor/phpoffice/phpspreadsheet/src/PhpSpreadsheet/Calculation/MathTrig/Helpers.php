<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

class Helpers
{
    /**
     * Many functions accept null/false/true argument treated as 0/0/1.
     *
     * @return float|string quotient or DIV0 if denominator is too small
     */
    public static function verySmallDenominator(float $numerator, float $denominator)
    {
    }
    /**
     * Many functions accept null/false/true argument treated as 0/0/1.
     *
     * @param mixed $number
     *
     * @return float|int
     */
    public static function validateNumericNullBool($number)
    {
    }
    /**
     * Validate numeric, but allow substitute for null.
     *
     * @param mixed $number
     * @param null|float|int $substitute
     *
     * @return float|int
     */
    public static function validateNumericNullSubstitution($number, $substitute)
    {
    }
    /**
     * Confirm number >= 0.
     *
     * @param float|int $number
     */
    public static function validateNotNegative($number, ?string $except = null) : void
    {
    }
    /**
     * Confirm number > 0.
     *
     * @param float|int $number
     */
    public static function validatePositive($number, ?string $except = null) : void
    {
    }
    /**
     * Confirm number != 0.
     *
     * @param float|int $number
     */
    public static function validateNotZero($number) : void
    {
    }
    public static function returnSign(float $number) : int
    {
    }
    public static function getEven(float $number) : float
    {
    }
    /**
     * Return NAN or value depending on argument.
     *
     * @param float $result Number
     *
     * @return float|string
     */
    public static function numberOrNan($result)
    {
    }
}
