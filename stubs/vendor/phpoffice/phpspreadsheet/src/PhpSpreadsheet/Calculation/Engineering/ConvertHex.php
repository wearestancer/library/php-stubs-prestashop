<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class ConvertHex extends \PhpOffice\PhpSpreadsheet\Calculation\Engineering\ConvertBase
{
    /**
     * toBinary.
     *
     * Return a hex value as binary.
     *
     * Excel Function:
     *        HEX2BIN(x[,places])
     *
     * @param string $value The hexadecimal number you want to convert.
     *                      Number cannot contain more than 10 characters.
     *                      The most significant bit of number is the sign bit (40th bit from the right).
     *                      The remaining 9 bits are magnitude bits.
     *                      Negative numbers are represented using two's-complement notation.
     *                      If number is negative, HEX2BIN ignores places and returns a 10-character binary number.
     *                      If number is negative, it cannot be less than FFFFFFFE00,
     *                          and if number is positive, it cannot be greater than 1FF.
     *                      If number is not a valid hexadecimal number, HEX2BIN returns the #NUM! error value.
     *                      If HEX2BIN requires more than places characters, it returns the #NUM! error value.
     * @param int $places The number of characters to use. If places is omitted,
     *                          HEX2BIN uses the minimum number of characters necessary. Places
     *                          is useful for padding the return value with leading 0s (zeros).
     *                      If places is not an integer, it is truncated.
     *                      If places is nonnumeric, HEX2BIN returns the #VALUE! error value.
     *                      If places is negative, HEX2BIN returns the #NUM! error value.
     */
    public static function toBinary($value, $places = null) : string
    {
    }
    /**
     * toDecimal.
     *
     * Return a hex value as decimal.
     *
     * Excel Function:
     *        HEX2DEC(x)
     *
     * @param string $value The hexadecimal number you want to convert. This number cannot
     *                          contain more than 10 characters (40 bits). The most significant
     *                          bit of number is the sign bit. The remaining 39 bits are magnitude
     *                          bits. Negative numbers are represented using two's-complement
     *                          notation.
     *                      If number is not a valid hexadecimal number, HEX2DEC returns the
     *                          #NUM! error value.
     */
    public static function toDecimal($value) : string
    {
    }
    /**
     * toOctal.
     *
     * Return a hex value as octal.
     *
     * Excel Function:
     *        HEX2OCT(x[,places])
     *
     * @param string $value The hexadecimal number you want to convert. Number cannot
     *                                    contain more than 10 characters. The most significant bit of
     *                                    number is the sign bit. The remaining 39 bits are magnitude
     *                                    bits. Negative numbers are represented using two's-complement
     *                                    notation.
     *                                    If number is negative, HEX2OCT ignores places and returns a
     *                                    10-character octal number.
     *                                    If number is negative, it cannot be less than FFE0000000, and
     *                                    if number is positive, it cannot be greater than 1FFFFFFF.
     *                                    If number is not a valid hexadecimal number, HEX2OCT returns
     *                                    the #NUM! error value.
     *                                    If HEX2OCT requires more than places characters, it returns
     *                                    the #NUM! error value.
     * @param int $places The number of characters to use. If places is omitted, HEX2OCT
     *                                    uses the minimum number of characters necessary. Places is
     *                                    useful for padding the return value with leading 0s (zeros).
     *                                    If places is not an integer, it is truncated.
     *                                    If places is nonnumeric, HEX2OCT returns the #VALUE! error
     *                                    value.
     *                                    If places is negative, HEX2OCT returns the #NUM! error value.
     */
    public static function toOctal($value, $places = null) : string
    {
    }
    protected static function validateHex(string $value) : string
    {
    }
}
