<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class ErfC
{
    /**
     * ERFC.
     *
     *    Returns the complementary ERF function integrated between x and infinity
     *
     *    Note: In Excel 2007 or earlier, if you input a negative value for the lower bound argument,
     *        the function would return a #NUM! error. However, in Excel 2010, the function algorithm was
     *        improved, so that it can now calculate the function for both positive and negative x values.
     *            PhpSpreadsheet follows Excel 2010 behaviour, and accepts nagative arguments.
     *
     *    Excel Function:
     *        ERFC(x)
     *
     * @param mixed $value The float lower bound for integrating ERFC
     *
     * @return float|string
     */
    public static function ERFC($value)
    {
    }
}
