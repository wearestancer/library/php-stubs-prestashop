<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class ConvertBase
{
    protected static function validateValue($value) : string
    {
    }
    protected static function validatePlaces($places = null) : ?int
    {
    }
    /**
     * Formats a number base string value with leading zeroes.
     *
     * @param string $value The "number" to pad
     * @param ?int $places The length that we want to pad this value
     *
     * @return string The padded "number"
     */
    protected static function nbrConversionFormat(string $value, ?int $places) : string
    {
    }
}
