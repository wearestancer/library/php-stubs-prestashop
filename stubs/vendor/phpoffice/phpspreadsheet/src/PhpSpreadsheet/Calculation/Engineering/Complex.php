<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class Complex
{
    /**
     * COMPLEX.
     *
     * Converts real and imaginary coefficients into a complex number of the form x +/- yi or x +/- yj.
     *
     * Excel Function:
     *        COMPLEX(realNumber,imaginary[,suffix])
     *
     * @param mixed $realNumber the real float coefficient of the complex number
     * @param mixed $imaginary the imaginary float coefficient of the complex number
     * @param mixed $suffix The character suffix for the imaginary component of the complex number.
     *                          If omitted, the suffix is assumed to be "i".
     *
     * @return string
     */
    public static function COMPLEX($realNumber = 0.0, $imaginary = 0.0, $suffix = 'i')
    {
    }
    /**
     * IMAGINARY.
     *
     * Returns the imaginary coefficient of a complex number in x + yi or x + yj text format.
     *
     * Excel Function:
     *        IMAGINARY(complexNumber)
     *
     * @param string $complexNumber the complex number for which you want the imaginary
     *                                         coefficient
     *
     * @return float|string
     */
    public static function IMAGINARY($complexNumber)
    {
    }
    /**
     * IMREAL.
     *
     * Returns the real coefficient of a complex number in x + yi or x + yj text format.
     *
     * Excel Function:
     *        IMREAL(complexNumber)
     *
     * @param string $complexNumber the complex number for which you want the real coefficient
     *
     * @return float|string
     */
    public static function IMREAL($complexNumber)
    {
    }
}
