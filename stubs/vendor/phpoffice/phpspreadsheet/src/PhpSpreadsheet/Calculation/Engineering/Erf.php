<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class Erf
{
    /**
     * ERF.
     *
     * Returns the error function integrated between the lower and upper bound arguments.
     *
     *    Note: In Excel 2007 or earlier, if you input a negative value for the upper or lower bound arguments,
     *            the function would return a #NUM! error. However, in Excel 2010, the function algorithm was
     *            improved, so that it can now calculate the function for both positive and negative ranges.
     *            PhpSpreadsheet follows Excel 2010 behaviour, and accepts negative arguments.
     *
     *    Excel Function:
     *        ERF(lower[,upper])
     *
     * @param mixed $lower Lower bound float for integrating ERF
     * @param mixed $upper Upper bound float for integrating ERF.
     *                           If omitted, ERF integrates between zero and lower_limit
     *
     * @return float|string
     */
    public static function ERF($lower, $upper = null)
    {
    }
    /**
     * ERFPRECISE.
     *
     * Returns the error function integrated between the lower and upper bound arguments.
     *
     *    Excel Function:
     *        ERF.PRECISE(limit)
     *
     * @param mixed $limit Float bound for integrating ERF, other bound is zero
     *
     * @return float|string
     */
    public static function ERFPRECISE($limit)
    {
    }
    //
    //    Private method to calculate the erf value
    //
    public static function erfValue($value)
    {
    }
}
