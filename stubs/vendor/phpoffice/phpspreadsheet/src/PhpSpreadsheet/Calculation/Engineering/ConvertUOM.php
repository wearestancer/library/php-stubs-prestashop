<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class ConvertUOM
{
    public const CATEGORY_WEIGHT_AND_MASS = 'Weight and Mass';
    public const CATEGORY_DISTANCE = 'Distance';
    public const CATEGORY_TIME = 'Time';
    public const CATEGORY_PRESSURE = 'Pressure';
    public const CATEGORY_FORCE = 'Force';
    public const CATEGORY_ENERGY = 'Energy';
    public const CATEGORY_POWER = 'Power';
    public const CATEGORY_MAGNETISM = 'Magnetism';
    public const CATEGORY_TEMPERATURE = 'Temperature';
    public const CATEGORY_VOLUME = 'Volume and Liquid Measure';
    public const CATEGORY_AREA = 'Area';
    public const CATEGORY_INFORMATION = 'Information';
    public const CATEGORY_SPEED = 'Speed';
    /**
     *    getConversionGroups
     * Returns a list of the different conversion groups for UOM conversions.
     *
     * @return array
     */
    public static function getConversionCategories()
    {
    }
    /**
     *    getConversionGroupUnits
     * Returns an array of units of measure, for a specified conversion group, or for all groups.
     *
     * @param string $category The group whose units of measure you want to retrieve
     *
     * @return array
     */
    public static function getConversionCategoryUnits($category = null)
    {
    }
    /**
     * getConversionGroupUnitDetails.
     *
     * @param string $category The group whose units of measure you want to retrieve
     *
     * @return array
     */
    public static function getConversionCategoryUnitDetails($category = null)
    {
    }
    /**
     *    getConversionMultipliers
     * Returns an array of the Multiplier prefixes that can be used with Units of Measure in CONVERTUOM().
     *
     * @return mixed[]
     */
    public static function getConversionMultipliers()
    {
    }
    /**
     *    getBinaryConversionMultipliers
     * Returns an array of the additional Multiplier prefixes that can be used with Information Units of Measure in CONVERTUOM().
     *
     * @return mixed[]
     */
    public static function getBinaryConversionMultipliers()
    {
    }
    /**
     * CONVERT.
     *
     * Converts a number from one measurement system to another.
     *    For example, CONVERT can translate a table of distances in miles to a table of distances
     * in kilometers.
     *
     *    Excel Function:
     *        CONVERT(value,fromUOM,toUOM)
     *
     * @param float|int $value the value in fromUOM to convert
     * @param string $fromUOM the units for value
     * @param string $toUOM the units for the result
     *
     * @return float|string
     */
    public static function CONVERT($value, $fromUOM, $toUOM)
    {
    }
    /**
     * @param float|int $value
     *
     * @return float|int
     */
    protected static function convertTemperature(string $fromUOM, string $toUOM, $value)
    {
    }
}
