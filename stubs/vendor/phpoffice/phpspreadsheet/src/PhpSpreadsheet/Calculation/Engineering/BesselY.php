<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engineering;

class BesselY
{
    /**
     * BESSELY.
     *
     * Returns the Bessel function, which is also called the Weber function or the Neumann function.
     *
     *    Excel Function:
     *        BESSELY(x,ord)
     *
     * @param mixed $x A float value at which to evaluate the function.
     *                   If x is nonnumeric, BESSELY returns the #VALUE! error value.
     * @param mixed $ord The integer order of the Bessel function.
     *                       If ord is not an integer, it is truncated.
     *                       If $ord is nonnumeric, BESSELY returns the #VALUE! error value.
     *                       If $ord < 0, BESSELY returns the #NUM! error value.
     *
     * @return float|string Result, or a string containing an error
     */
    public static function BESSELY($x, $ord)
    {
    }
}
