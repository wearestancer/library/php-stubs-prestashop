<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\LookupRef;

abstract class LookupBase
{
    protected static function validateIndexLookup($lookup_array, $index_number)
    {
    }
    protected static function checkMatch(bool $bothNumeric, bool $bothNotNumeric, $notExactMatch, int $rowKey, string $cellDataLower, string $lookupLower, ?int $rowNumber) : ?int
    {
    }
}
