<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\LookupRef;

class Hyperlink
{
    /**
     * HYPERLINK.
     *
     * Excel Function:
     *        =HYPERLINK(linkURL, [displayName])
     *
     * @param mixed $linkURL Expect string. Value to check, is also the value returned when no error
     * @param mixed $displayName Expect string. Value to return when testValue is an error condition
     * @param Cell $pCell The cell to set the hyperlink in
     *
     * @return mixed The value of $displayName (or $linkURL if $displayName was blank)
     */
    public static function set($linkURL = '', $displayName = null, ?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null)
    {
    }
}
