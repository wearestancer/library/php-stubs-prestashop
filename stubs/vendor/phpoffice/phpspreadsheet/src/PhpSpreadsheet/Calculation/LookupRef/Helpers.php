<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\LookupRef;

class Helpers
{
    public const CELLADDRESS_USE_A1 = true;
    public const CELLADDRESS_USE_R1C1 = false;
    public static function extractCellAddresses(string $cellAddress, bool $a1, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet, string $sheetName = '') : array
    {
    }
    public static function extractWorksheet(string $cellAddress, \PhpOffice\PhpSpreadsheet\Cell\Cell $pCell) : array
    {
    }
}
