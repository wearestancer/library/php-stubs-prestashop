<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\LookupRef;

class Matrix
{
    /**
     * TRANSPOSE.
     *
     * @param array|mixed $matrixData A matrix of values
     *
     * @return array
     */
    public static function transpose($matrixData)
    {
    }
    /**
     * INDEX.
     *
     * Uses an index to choose a value from a reference or array
     *
     * Excel Function:
     *        =INDEX(range_array, row_num, [column_num], [area_num])
     *
     * @param mixed $matrix A range of cells or an array constant
     * @param mixed $rowNum The row in the array or range from which to return a value.
     *                          If row_num is omitted, column_num is required.
     * @param mixed $columnNum The column in the array or range from which to return a value.
     *                          If column_num is omitted, row_num is required.
     *
     * TODO Provide support for area_num, currently not supported
     *
     * @return mixed the value of a specified cell or array of cells
     */
    public static function index($matrix, $rowNum = 0, $columnNum = 0)
    {
    }
}
