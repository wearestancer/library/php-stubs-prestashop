<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\LookupRef;

class Address
{
    public const ADDRESS_ABSOLUTE = 1;
    public const ADDRESS_COLUMN_RELATIVE = 2;
    public const ADDRESS_ROW_RELATIVE = 3;
    public const ADDRESS_RELATIVE = 4;
    public const REFERENCE_STYLE_A1 = true;
    public const REFERENCE_STYLE_R1C1 = false;
    /**
     * ADDRESS.
     *
     * Creates a cell address as text, given specified row and column numbers.
     *
     * Excel Function:
     *        =ADDRESS(row, column, [relativity], [referenceStyle], [sheetText])
     *
     * @param mixed $row Row number (integer) to use in the cell reference
     * @param mixed $column Column number (integer) to use in the cell reference
     * @param mixed $relativity Integer flag indicating the type of reference to return
     *                             1 or omitted    Absolute
     *                             2               Absolute row; relative column
     *                             3               Relative row; absolute column
     *                             4               Relative
     * @param mixed $referenceStyle A logical (boolean) value that specifies the A1 or R1C1 reference style.
     *                                TRUE or omitted    ADDRESS returns an A1-style reference
     *                                FALSE              ADDRESS returns an R1C1-style reference
     * @param mixed $sheetName Optional Name of worksheet to use
     *
     * @return string
     */
    public static function cell($row, $column, $relativity = 1, $referenceStyle = true, $sheetName = '')
    {
    }
}
