<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\LookupRef;

class Formula
{
    /**
     * FORMULATEXT.
     *
     * @param mixed $cellReference The cell to check
     * @param Cell $pCell The current cell (containing this formula)
     *
     * @return string
     */
    public static function text($cellReference = '', ?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null)
    {
    }
}
