<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class Fisher
{
    /**
     * FISHER.
     *
     * Returns the Fisher transformation at x. This transformation produces a function that
     *        is normally distributed rather than skewed. Use this function to perform hypothesis
     *        testing on the correlation coefficient.
     *
     * @param mixed $value Float value for which we want the probability
     *
     * @return float|string
     */
    public static function distribution($value)
    {
    }
    /**
     * FISHERINV.
     *
     * Returns the inverse of the Fisher transformation. Use this transformation when
     *        analyzing correlations between ranges or arrays of data. If y = FISHER(x), then
     *        FISHERINV(y) = x.
     *
     * @param mixed $probability Float probability at which you want to evaluate the distribution
     *
     * @return float|string
     */
    public static function inverse($probability)
    {
    }
}
