<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical;

class StatisticalValidations
{
    /**
     * @param mixed $value
     */
    public static function validateFloat($value) : float
    {
    }
    /**
     * @param mixed $value
     */
    public static function validateInt($value) : int
    {
    }
    /**
     * @param mixed $value
     */
    public static function validateBool($value) : bool
    {
    }
}
