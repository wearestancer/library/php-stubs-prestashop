<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical;

abstract class AggregateBase
{
    /**
     * MS Excel does not count Booleans if passed as cell values, but they are counted if passed as literals.
     * OpenOffice Calc always counts Booleans.
     * Gnumeric never counts Booleans.
     *
     * @param mixed $arg
     * @param mixed $k
     *
     * @return int|mixed
     */
    protected static function testAcceptedBoolean($arg, $k)
    {
    }
    /**
     * @param mixed $arg
     * @param mixed $k
     *
     * @return bool
     */
    protected static function isAcceptedCountable($arg, $k)
    {
    }
}
