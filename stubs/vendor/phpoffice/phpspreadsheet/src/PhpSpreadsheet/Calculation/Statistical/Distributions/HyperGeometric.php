<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class HyperGeometric
{
    /**
     * HYPGEOMDIST.
     *
     * Returns the hypergeometric distribution. HYPGEOMDIST returns the probability of a given number of
     * sample successes, given the sample size, population successes, and population size.
     *
     * @param mixed $sampleSuccesses Integer number of successes in the sample
     * @param mixed $sampleNumber Integer size of the sample
     * @param mixed $populationSuccesses Integer number of successes in the population
     * @param mixed $populationNumber Integer population size
     *
     * @return float|string
     */
    public static function distribution($sampleSuccesses, $sampleNumber, $populationSuccesses, $populationNumber)
    {
    }
}
