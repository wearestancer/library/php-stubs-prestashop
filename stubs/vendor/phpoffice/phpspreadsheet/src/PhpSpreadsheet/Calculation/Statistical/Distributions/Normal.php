<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class Normal
{
    public const SQRT2PI = 2.5066282746310007;
    /**
     * NORMDIST.
     *
     * Returns the normal distribution for the specified mean and standard deviation. This
     * function has a very wide range of applications in statistics, including hypothesis
     * testing.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $mean Mean value as a float
     * @param mixed $stdDev Standard Deviation as a float
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string The result, or a string containing an error
     */
    public static function distribution($value, $mean, $stdDev, $cumulative)
    {
    }
    /**
     * NORMINV.
     *
     * Returns the inverse of the normal cumulative distribution for the specified mean and standard deviation.
     *
     * @param mixed $probability Float probability for which we want the value
     * @param mixed $mean Mean Value as a float
     * @param mixed $stdDev Standard Deviation as a float
     *
     * @return float|string The result, or a string containing an error
     */
    public static function inverse($probability, $mean, $stdDev)
    {
    }
}
