<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical;

class Maximum extends \PhpOffice\PhpSpreadsheet\Calculation\Statistical\MaxMinBase
{
    /**
     * MAX.
     *
     * MAX returns the value of the element of the values passed that has the highest value,
     *        with negative numbers considered smaller than positive numbers.
     *
     * Excel Function:
     *        MAX(value1[,value2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float
     */
    public static function max(...$args)
    {
    }
    /**
     * MAXA.
     *
     * Returns the greatest value in a list of arguments, including numbers, text, and logical values
     *
     * Excel Function:
     *        MAXA(value1[,value2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float
     */
    public static function maxA(...$args)
    {
    }
}
