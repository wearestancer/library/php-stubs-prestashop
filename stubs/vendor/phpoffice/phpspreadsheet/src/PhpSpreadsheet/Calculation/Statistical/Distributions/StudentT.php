<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class StudentT
{
    /**
     * TDIST.
     *
     * Returns the probability of Student's T distribution.
     *
     * @param mixed $value Float value for the distribution
     * @param mixed $degrees Integer value for degrees of freedom
     * @param mixed $tails Integer value for the number of tails (1 or 2)
     *
     * @return float|string The result, or a string containing an error
     */
    public static function distribution($value, $degrees, $tails)
    {
    }
    /**
     * TINV.
     *
     * Returns the one-tailed probability of the chi-squared distribution.
     *
     * @param mixed $probability Float probability for the function
     * @param mixed $degrees Integer value for degrees of freedom
     *
     * @return float|string The result, or a string containing an error
     */
    public static function inverse($probability, $degrees)
    {
    }
}
