<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical;

class Confidence
{
    /**
     * CONFIDENCE.
     *
     * Returns the confidence interval for a population mean
     *
     * @param mixed $alpha As a float
     * @param mixed $stdDev Standard Deviation as a float
     * @param mixed $size As an integer
     *
     * @return float|string
     */
    public static function CONFIDENCE($alpha, $stdDev, $size)
    {
    }
}
