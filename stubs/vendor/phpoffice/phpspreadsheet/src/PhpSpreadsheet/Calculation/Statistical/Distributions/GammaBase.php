<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

abstract class GammaBase
{
    protected static function calculateDistribution(float $value, float $a, float $b, bool $cumulative)
    {
    }
    protected static function calculateInverse(float $probability, float $alpha, float $beta)
    {
    }
    //
    //    Implementation of the incomplete Gamma function
    //
    public static function incompleteGamma(float $a, float $x) : float
    {
    }
    //
    //    Implementation of the Gamma function
    //
    public static function gammaValue(float $value) : float
    {
    }
    public static function logGamma(float $x) : float
    {
    }
    protected static function logGamma3(float $y)
    {
    }
    protected static function logGamma4(float $y)
    {
    }
}
