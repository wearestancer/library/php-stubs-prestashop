<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class Exponential
{
    /**
     * EXPONDIST.
     *
     *    Returns the exponential distribution. Use EXPONDIST to model the time between events,
     *        such as how long an automated bank teller takes to deliver cash. For example, you can
     *        use EXPONDIST to determine the probability that the process takes at most 1 minute.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $lambda The parameter value as a float
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string
     */
    public static function distribution($value, $lambda, $cumulative)
    {
    }
}
