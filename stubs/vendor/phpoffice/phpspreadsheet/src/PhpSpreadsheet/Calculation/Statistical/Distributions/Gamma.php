<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class Gamma extends \PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions\GammaBase
{
    /**
     * GAMMA.
     *
     * Return the gamma function value.
     *
     * @param mixed $value Float value for which we want the probability
     *
     * @return float|string The result, or a string containing an error
     */
    public static function gamma($value)
    {
    }
    /**
     * GAMMADIST.
     *
     * Returns the gamma distribution.
     *
     * @param mixed $value Float Value at which you want to evaluate the distribution
     * @param mixed $a Parameter to the distribution as a float
     * @param mixed $b Parameter to the distribution as a float
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string
     */
    public static function distribution($value, $a, $b, $cumulative)
    {
    }
    /**
     * GAMMAINV.
     *
     * Returns the inverse of the Gamma distribution.
     *
     * @param mixed $probability Float probability at which you want to evaluate the distribution
     * @param mixed $alpha Parameter to the distribution as a float
     * @param mixed $beta Parameter to the distribution as a float
     *
     * @return float|string
     */
    public static function inverse($probability, $alpha, $beta)
    {
    }
    /**
     * GAMMALN.
     *
     * Returns the natural logarithm of the gamma function.
     *
     * @param mixed $value Float Value at which you want to evaluate the distribution
     *
     * @return float|string
     */
    public static function ln($value)
    {
    }
}
