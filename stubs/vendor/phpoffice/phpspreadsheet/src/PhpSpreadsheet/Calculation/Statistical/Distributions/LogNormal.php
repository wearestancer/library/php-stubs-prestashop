<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class LogNormal
{
    /**
     * LOGNORMDIST.
     *
     * Returns the cumulative lognormal distribution of x, where ln(x) is normally distributed
     * with parameters mean and standard_dev.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $mean Mean value as a float
     * @param mixed $stdDev Standard Deviation as a float
     *
     * @return float|string The result, or a string containing an error
     */
    public static function cumulative($value, $mean, $stdDev)
    {
    }
    /**
     * LOGNORM.DIST.
     *
     * Returns the lognormal distribution of x, where ln(x) is normally distributed
     * with parameters mean and standard_dev.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $mean Mean value as a float
     * @param mixed $stdDev Standard Deviation as a float
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string The result, or a string containing an error
     */
    public static function distribution($value, $mean, $stdDev, $cumulative = false)
    {
    }
    /**
     * LOGINV.
     *
     * Returns the inverse of the lognormal cumulative distribution
     *
     * @param mixed $probability Float probability for which we want the value
     * @param mixed $mean Mean Value as a float
     * @param mixed $stdDev Standard Deviation as a float
     *
     * @return float|string The result, or a string containing an error
     *
     * @TODO    Try implementing P J Acklam's refinement algorithm for greater
     *            accuracy if I can get my head round the mathematics
     *            (as described at) http://home.online.no/~pjacklam/notes/invnorm/
     */
    public static function inverse($probability, $mean, $stdDev)
    {
    }
}
