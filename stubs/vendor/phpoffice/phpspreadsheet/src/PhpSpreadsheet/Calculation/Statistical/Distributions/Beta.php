<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class Beta
{
    /**
     * BETADIST.
     *
     * Returns the beta distribution.
     *
     * @param mixed $value Float value at which you want to evaluate the distribution
     * @param mixed $alpha Parameter to the distribution as a float
     * @param mixed $beta Parameter to the distribution as a float
     * @param mixed $rMin as an float
     * @param mixed $rMax as an float
     *
     * @return float|string
     */
    public static function distribution($value, $alpha, $beta, $rMin = 0.0, $rMax = 1.0)
    {
    }
    /**
     * BETAINV.
     *
     * Returns the inverse of the Beta distribution.
     *
     * @param mixed $probability Float probability at which you want to evaluate the distribution
     * @param mixed $alpha Parameter to the distribution as a float
     * @param mixed $beta Parameter to the distribution as a float
     * @param mixed $rMin Minimum value as a float
     * @param mixed $rMax Maximum value as a float
     *
     * @return float|string
     */
    public static function inverse($probability, $alpha, $beta, $rMin = 0.0, $rMax = 1.0)
    {
    }
    /**
     * Incomplete beta function.
     *
     * @author Jaco van Kooten
     * @author Paul Meagher
     *
     * The computation is based on formulas from Numerical Recipes, Chapter 6.4 (W.H. Press et al, 1992).
     *
     * @param float $x require 0<=x<=1
     * @param float $p require p>0
     * @param float $q require q>0
     *
     * @return float 0 if x<0, p<=0, q<=0 or p+q>2.55E305 and 1 if x>1 to avoid errors and over/underflow
     */
    public static function incompleteBeta(float $x, float $p, float $q) : float
    {
    }
}
