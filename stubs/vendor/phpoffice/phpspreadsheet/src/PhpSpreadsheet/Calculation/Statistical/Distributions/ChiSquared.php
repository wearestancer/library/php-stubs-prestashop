<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class ChiSquared
{
    /**
     * CHIDIST.
     *
     * Returns the one-tailed probability of the chi-squared distribution.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $degrees Integer degrees of freedom
     *
     * @return float|string
     */
    public static function distributionRightTail($value, $degrees)
    {
    }
    /**
     * CHIDIST.
     *
     * Returns the one-tailed probability of the chi-squared distribution.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $degrees Integer degrees of freedom
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string
     */
    public static function distributionLeftTail($value, $degrees, $cumulative)
    {
    }
    /**
     * CHIINV.
     *
     * Returns the inverse of the right-tailed probability of the chi-squared distribution.
     *
     * @param mixed $probability Float probability at which you want to evaluate the distribution
     * @param mixed $degrees Integer degrees of freedom
     *
     * @return float|string
     */
    public static function inverseRightTail($probability, $degrees)
    {
    }
    /**
     * CHIINV.
     *
     * Returns the inverse of the left-tailed probability of the chi-squared distribution.
     *
     * @param mixed $probability Float probability at which you want to evaluate the distribution
     * @param mixed $degrees Integer degrees of freedom
     *
     * @return float|string
     */
    public static function inverseLeftTail($probability, $degrees)
    {
    }
    /**
     * CHITEST.
     *
     * Uses the chi-square test to calculate the probability that the differences between two supplied data sets
     *      (of observed and expected frequencies), are likely to be simply due to sampling error,
     *      or if they are likely to be real.
     *
     * @param mixed $actual an array of observed frequencies
     * @param mixed $expected an array of expected frequencies
     *
     * @return float|string
     */
    public static function test($actual, $expected)
    {
    }
    protected static function degrees(int $rows, int $columns) : int
    {
    }
}
