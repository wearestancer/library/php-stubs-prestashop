<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class Binomial
{
    /**
     * BINOMDIST.
     *
     * Returns the individual term binomial distribution probability. Use BINOMDIST in problems with
     *        a fixed number of tests or trials, when the outcomes of any trial are only success or failure,
     *        when trials are independent, and when the probability of success is constant throughout the
     *        experiment. For example, BINOMDIST can calculate the probability that two of the next three
     *        babies born are male.
     *
     * @param mixed $value Integer number of successes in trials
     * @param mixed $trials Integer umber of trials
     * @param mixed $probability Probability of success on each trial as a float
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string
     */
    public static function distribution($value, $trials, $probability, $cumulative)
    {
    }
    /**
     * BINOM.DIST.RANGE.
     *
     * Returns returns the Binomial Distribution probability for the number of successes from a specified number
     *     of trials falling into a specified range.
     *
     * @param mixed $trials Integer number of trials
     * @param mixed $probability Probability of success on each trial as a float
     * @param mixed $successes The integer number of successes in trials
     * @param mixed $limit Upper limit for successes in trials as null, or an integer
     *                           If null, then this will indicate the same as the number of Successes
     *
     * @return float|string
     */
    public static function range($trials, $probability, $successes, $limit = null)
    {
    }
    /**
     * NEGBINOMDIST.
     *
     * Returns the negative binomial distribution. NEGBINOMDIST returns the probability that
     *        there will be number_f failures before the number_s-th success, when the constant
     *        probability of a success is probability_s. This function is similar to the binomial
     *        distribution, except that the number of successes is fixed, and the number of trials is
     *        variable. Like the binomial, trials are assumed to be independent.
     *
     * @param mixed $failures Number of Failures as an integer
     * @param mixed $successes Threshold number of Successes as an integer
     * @param mixed $probability Probability of success on each trial as a float
     *
     * @return float|string The result, or a string containing an error
     *
     * TODO Add support for the cumulative flag not present for NEGBINOMDIST, but introduced for NEGBINOM.DIST
     *      The cumulative default should be false to reflect the behaviour of NEGBINOMDIST
     */
    public static function negative($failures, $successes, $probability)
    {
    }
    /**
     * CRITBINOM.
     *
     * Returns the smallest value for which the cumulative binomial distribution is greater
     *        than or equal to a criterion value
     *
     * @param mixed $trials number of Bernoulli trials as an integer
     * @param mixed $probability probability of a success on each trial as a float
     * @param mixed $alpha criterion value as a float
     *
     * @return int|string
     */
    public static function inverse($trials, $probability, $alpha)
    {
    }
}
