<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical;

class Minimum extends \PhpOffice\PhpSpreadsheet\Calculation\Statistical\MaxMinBase
{
    /**
     * MIN.
     *
     * MIN returns the value of the element of the values passed that has the smallest value,
     *        with negative numbers considered smaller than positive numbers.
     *
     * Excel Function:
     *        MIN(value1[,value2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float
     */
    public static function min(...$args)
    {
    }
    /**
     * MINA.
     *
     * Returns the smallest value in a list of arguments, including numbers, text, and logical values
     *
     * Excel Function:
     *        MINA(value1[,value2[, ...]])
     *
     * @param mixed ...$args Data values
     *
     * @return float
     */
    public static function minA(...$args)
    {
    }
}
