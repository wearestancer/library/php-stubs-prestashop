<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions;

class F
{
    /**
     * F.DIST.
     *
     *    Returns the F probability distribution.
     *    You can use this function to determine whether two data sets have different degrees of diversity.
     *    For example, you can examine the test scores of men and women entering high school, and determine
     *        if the variability in the females is different from that found in the males.
     *
     * @param mixed $value Float value for which we want the probability
     * @param mixed $u The numerator degrees of freedom as an integer
     * @param mixed $v The denominator degrees of freedom as an integer
     * @param mixed $cumulative Boolean value indicating if we want the cdf (true) or the pdf (false)
     *
     * @return float|string
     */
    public static function distribution($value, $u, $v, $cumulative)
    {
    }
}
