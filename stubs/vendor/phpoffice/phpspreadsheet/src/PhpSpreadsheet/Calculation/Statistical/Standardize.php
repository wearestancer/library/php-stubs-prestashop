<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Statistical;

class Standardize extends \PhpOffice\PhpSpreadsheet\Calculation\Statistical\StatisticalValidations
{
    /**
     * STANDARDIZE.
     *
     * Returns a normalized value from a distribution characterized by mean and standard_dev.
     *
     * @param float $value Value to normalize
     * @param float $mean Mean Value
     * @param float $stdDev Standard Deviation
     *
     * @return float|string Standardized value, or a string containing an error
     */
    public static function execute($value, $mean, $stdDev)
    {
    }
}
