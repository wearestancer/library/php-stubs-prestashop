<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Financial\CashFlow\Constant\Periodic;

class InterestAndPrincipal
{
    protected $interest;
    protected $principal;
    public function __construct(float $rate = 0.0, int $period = 0, int $numberOfPeriods = 0, float $presentValue = 0, float $futureValue = 0, int $type = \PhpOffice\PhpSpreadsheet\Calculation\Financial\Constants::PAYMENT_END_OF_PERIOD)
    {
    }
    public function interest() : float
    {
    }
    public function principal() : float
    {
    }
}
