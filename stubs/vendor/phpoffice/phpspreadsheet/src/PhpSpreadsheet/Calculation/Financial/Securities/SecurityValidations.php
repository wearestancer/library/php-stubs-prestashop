<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Financial\Securities;

class SecurityValidations extends \PhpOffice\PhpSpreadsheet\Calculation\Financial\FinancialValidations
{
    /**
     * @param mixed $issue
     */
    public static function validateIssueDate($issue) : float
    {
    }
    /**
     * @param mixed $settlement
     * @param mixed $maturity
     */
    public static function validateSecurityPeriod($settlement, $maturity) : void
    {
    }
    /**
     * @param mixed $redemption
     */
    public static function validateRedemption($redemption) : float
    {
    }
}
