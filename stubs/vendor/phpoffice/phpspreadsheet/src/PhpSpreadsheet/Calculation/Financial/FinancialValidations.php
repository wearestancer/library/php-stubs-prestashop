<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Financial;

class FinancialValidations
{
    /**
     * @param mixed $date
     */
    public static function validateDate($date) : float
    {
    }
    /**
     * @param mixed $settlement
     */
    public static function validateSettlementDate($settlement) : float
    {
    }
    /**
     * @param mixed $maturity
     */
    public static function validateMaturityDate($maturity) : float
    {
    }
    /**
     * @param mixed $value
     */
    public static function validateFloat($value) : float
    {
    }
    /**
     * @param mixed $value
     */
    public static function validateInt($value) : int
    {
    }
    /**
     * @param mixed $rate
     */
    public static function validateRate($rate) : float
    {
    }
    /**
     * @param mixed $frequency
     */
    public static function validateFrequency($frequency) : int
    {
    }
    /**
     * @param mixed $basis
     */
    public static function validateBasis($basis) : int
    {
    }
    /**
     * @param mixed $price
     */
    public static function validatePrice($price) : float
    {
    }
    /**
     * @param mixed $parValue
     */
    public static function validateParValue($parValue) : float
    {
    }
    /**
     * @param mixed $yield
     */
    public static function validateYield($yield) : float
    {
    }
    /**
     * @param mixed $discount
     */
    public static function validateDiscount($discount) : float
    {
    }
}
