<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Financial;

class Amortization
{
    /**
     * AMORDEGRC.
     *
     * Returns the depreciation for each accounting period.
     * This function is provided for the French accounting system. If an asset is purchased in
     * the middle of the accounting period, the prorated depreciation is taken into account.
     * The function is similar to AMORLINC, except that a depreciation coefficient is applied in
     * the calculation depending on the life of the assets.
     * This function will return the depreciation until the last period of the life of the assets
     * or until the cumulated value of depreciation is greater than the cost of the assets minus
     * the salvage value.
     *
     * Excel Function:
     *        AMORDEGRC(cost,purchased,firstPeriod,salvage,period,rate[,basis])
     *
     * @param mixed $cost The float cost of the asset
     * @param mixed $purchased Date of the purchase of the asset
     * @param mixed $firstPeriod Date of the end of the first period
     * @param mixed $salvage The salvage value at the end of the life of the asset
     * @param mixed $period the period (float)
     * @param mixed $rate rate of depreciation (float)
     * @param mixed $basis The type of day count to use (int).
     *                         0 or omitted    US (NASD) 30/360
     *                         1               Actual/actual
     *                         2               Actual/360
     *                         3               Actual/365
     *                         4               European 30/360
     *
     * @return float|string (string containing the error type if there is an error)
     */
    public static function AMORDEGRC($cost, $purchased, $firstPeriod, $salvage, $period, $rate, $basis = \PhpOffice\PhpSpreadsheet\Calculation\Financial\Constants::BASIS_DAYS_PER_YEAR_NASD)
    {
    }
    /**
     * AMORLINC.
     *
     * Returns the depreciation for each accounting period.
     * This function is provided for the French accounting system. If an asset is purchased in
     * the middle of the accounting period, the prorated depreciation is taken into account.
     *
     * Excel Function:
     *        AMORLINC(cost,purchased,firstPeriod,salvage,period,rate[,basis])
     *
     * @param mixed $cost The cost of the asset as a float
     * @param mixed $purchased Date of the purchase of the asset
     * @param mixed $firstPeriod Date of the end of the first period
     * @param mixed $salvage The salvage value at the end of the life of the asset
     * @param mixed $period The period as a float
     * @param mixed $rate Rate of depreciation as  float
     * @param mixed $basis Integer indicating the type of day count to use.
     *                             0 or omitted    US (NASD) 30/360
     *                             1               Actual/actual
     *                             2               Actual/360
     *                             3               Actual/365
     *                             4               European 30/360
     *
     * @return float|string (string containing the error type if there is an error)
     */
    public static function AMORLINC($cost, $purchased, $firstPeriod, $salvage, $period, $rate, $basis = \PhpOffice\PhpSpreadsheet\Calculation\Financial\Constants::BASIS_DAYS_PER_YEAR_NASD)
    {
    }
}
