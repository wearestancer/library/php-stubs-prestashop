<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Financial\CashFlow;

class CashFlowValidations extends \PhpOffice\PhpSpreadsheet\Calculation\Financial\FinancialValidations
{
    /**
     * @param mixed $rate
     */
    public static function validateRate($rate) : float
    {
    }
    /**
     * @param mixed $type
     */
    public static function validatePeriodType($type) : int
    {
    }
    /**
     * @param mixed $presentValue
     */
    public static function validatePresentValue($presentValue) : float
    {
    }
    /**
     * @param mixed $futureValue
     */
    public static function validateFutureValue($futureValue) : float
    {
    }
}
