<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Financial;

class Dollar
{
    /**
     * DOLLAR.
     *
     * This function converts a number to text using currency format, with the decimals rounded to the specified place.
     * The format used is $#,##0.00_);($#,##0.00)..
     *
     * @param mixed $number The value to format
     * @param mixed $precision The number of digits to display to the right of the decimal point (as an integer).
     *                            If precision is negative, number is rounded to the left of the decimal point.
     *                            If you omit precision, it is assumed to be 2
     */
    public static function format($number, $precision = 2) : string
    {
    }
    /**
     * DOLLARDE.
     *
     * Converts a dollar price expressed as an integer part and a fraction
     *        part into a dollar price expressed as a decimal number.
     * Fractional dollar numbers are sometimes used for security prices.
     *
     * Excel Function:
     *        DOLLARDE(fractional_dollar,fraction)
     *
     * @param mixed $fractionalDollar Fractional Dollar
     * @param mixed $fraction Fraction
     *
     * @return float|string
     */
    public static function decimal($fractionalDollar = null, $fraction = 0)
    {
    }
    /**
     * DOLLARFR.
     *
     * Converts a dollar price expressed as a decimal number into a dollar price
     *        expressed as a fraction.
     * Fractional dollar numbers are sometimes used for security prices.
     *
     * Excel Function:
     *        DOLLARFR(decimal_dollar,fraction)
     *
     * @param mixed $decimalDollar Decimal Dollar
     * @param mixed $fraction Fraction
     *
     * @return float|string
     */
    public static function fractional($decimalDollar = null, $fraction = 0)
    {
    }
}
