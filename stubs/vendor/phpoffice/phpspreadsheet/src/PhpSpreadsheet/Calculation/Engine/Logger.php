<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\Engine;

class Logger
{
    /**
     * Instantiate a Calculation engine logger.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Calculation\Engine\CyclicReferenceStack $stack)
    {
    }
    /**
     * Enable/Disable Calculation engine logging.
     *
     * @param bool $pValue
     */
    public function setWriteDebugLog($pValue) : void
    {
    }
    /**
     * Return whether calculation engine logging is enabled or disabled.
     *
     * @return bool
     */
    public function getWriteDebugLog()
    {
    }
    /**
     * Enable/Disable echoing of debug log information.
     *
     * @param bool $pValue
     */
    public function setEchoDebugLog($pValue) : void
    {
    }
    /**
     * Return whether echoing of debug log information is enabled or disabled.
     *
     * @return bool
     */
    public function getEchoDebugLog()
    {
    }
    /**
     * Write an entry to the calculation engine debug log.
     */
    public function writeDebugLog(...$args) : void
    {
    }
    /**
     * Write a series of entries to the calculation engine debug log.
     *
     * @param string[] $args
     */
    public function mergeDebugLog(array $args) : void
    {
    }
    /**
     * Clear the calculation engine debug log.
     */
    public function clearLog() : void
    {
    }
    /**
     * Return the calculation engine debug log.
     *
     * @return string[]
     */
    public function getLog()
    {
    }
}
