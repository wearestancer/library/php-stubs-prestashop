<?php

namespace PhpOffice\PhpSpreadsheet\Calculation;

class Calculation
{
    /** Constants                */
    /** Regular Expressions        */
    //    Numeric operand
    const CALCULATION_REGEXP_NUMBER = '[-+]?\\d*\\.?\\d+(e[-+]?\\d+)?';
    //    String operand
    const CALCULATION_REGEXP_STRING = '"(?:[^"]|"")*"';
    //    Opening bracket
    const CALCULATION_REGEXP_OPENBRACE = '\\(';
    //    Function (allow for the old @ symbol that could be used to prefix a function, but we'll ignore it)
    const CALCULATION_REGEXP_FUNCTION = '@?(?:_xlfn\\.)?([\\p{L}][\\p{L}\\p{N}\\.]*)[\\s]*\\(';
    //    Cell reference (cell or range of cells, with or without a sheet reference)
    const CALCULATION_REGEXP_CELLREF = '((([^\\s,!&%^\\/\\*\\+<>=-]*)|(\'[^\']*\')|(\\"[^\\"]*\\"))!)?\\$?\\b([a-z]{1,3})\\$?(\\d{1,7})(?![\\w.])';
    //    Cell reference (with or without a sheet reference) ensuring absolute/relative
    const CALCULATION_REGEXP_CELLREF_RELATIVE = '((([^\\s\\(,!&%^\\/\\*\\+<>=-]*)|(\'[^\']*\')|(\\"[^\\"]*\\"))!)?(\\$?\\b[a-z]{1,3})(\\$?\\d{1,7})(?![\\w.])';
    const CALCULATION_REGEXP_COLUMN_RANGE = '(((([^\\s\\(,!&%^\\/\\*\\+<>=-]*)|(\'[^\']*\')|(\\"[^\\"]*\\"))!)?(\\$?[a-z]{1,3})):(?![.*])';
    const CALCULATION_REGEXP_ROW_RANGE = '(((([^\\s\\(,!&%^\\/\\*\\+<>=-]*)|(\'[^\']*\')|(\\"[^\\"]*\\"))!)?(\\$?[1-9][0-9]{0,6})):(?![.*])';
    //    Cell reference (with or without a sheet reference) ensuring absolute/relative
    //    Cell ranges ensuring absolute/relative
    const CALCULATION_REGEXP_COLUMNRANGE_RELATIVE = '(\\$?[a-z]{1,3}):(\\$?[a-z]{1,3})';
    const CALCULATION_REGEXP_ROWRANGE_RELATIVE = '(\\$?\\d{1,7}):(\\$?\\d{1,7})';
    //    Defined Names: Named Range of cells, or Named Formulae
    const CALCULATION_REGEXP_DEFINEDNAME = '((([^\\s,!&%^\\/\\*\\+<>=-]*)|(\'[^\']*\')|(\\"[^\\"]*\\"))!)?([_\\p{L}][_\\p{L}\\p{N}\\.]*)';
    //    Error
    const CALCULATION_REGEXP_ERROR = '\\#[A-Z][A-Z0_\\/]*[!\\?]?';
    /** constants */
    const RETURN_ARRAY_AS_ERROR = 'error';
    const RETURN_ARRAY_AS_VALUE = 'value';
    const RETURN_ARRAY_AS_ARRAY = 'array';
    const FORMULA_OPEN_FUNCTION_BRACE = '{';
    const FORMULA_CLOSE_FUNCTION_BRACE = '}';
    const FORMULA_STRING_QUOTE = '"';
    /**
     * Flag to determine how formula errors should be handled
     *        If true, then a user error will be triggered
     *        If false, then an exception will be thrown.
     *
     * @var bool
     */
    public $suppressFormulaErrors = false;
    /**
     * Error message for any error that was raised/thrown by the calculation engine.
     *
     * @var null|string
     */
    public $formulaError;
    /**
     * Number of iterations for cyclic formulae.
     *
     * @var int
     */
    public $cyclicFormulaCount = 1;
    /**
     * Locale-specific translations for Excel constants (True, False and Null).
     *
     * @var array<string, string>
     */
    public static $localeBoolean = ['TRUE' => 'TRUE', 'FALSE' => 'FALSE', 'NULL' => 'NULL'];
    public function __construct(?\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet = null)
    {
    }
    /**
     * Get an instance of this class.
     *
     * @param ?Spreadsheet $spreadsheet Injected spreadsheet for working with a PhpSpreadsheet Spreadsheet object,
     *                                    or NULL to create a standalone calculation engine
     */
    public static function getInstance(?\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet = null) : self
    {
    }
    /**
     * Flush the calculation cache for any existing instance of this class
     *        but only if a Calculation instance exists.
     */
    public function flushInstance() : void
    {
    }
    /**
     * Get the Logger for this calculation engine instance.
     *
     * @return Logger
     */
    public function getDebugLog()
    {
    }
    /**
     * __clone implementation. Cloning should not be allowed in a Singleton!
     */
    public final function __clone()
    {
    }
    /**
     * Return the locale-specific translation of TRUE.
     *
     * @return string locale-specific translation of TRUE
     */
    public static function getTRUE() : string
    {
    }
    /**
     * Return the locale-specific translation of FALSE.
     *
     * @return string locale-specific translation of FALSE
     */
    public static function getFALSE() : string
    {
    }
    /**
     * Set the Array Return Type (Array or Value of first element in the array).
     *
     * @param string $returnType Array return type
     *
     * @return bool Success or failure
     */
    public static function setArrayReturnType($returnType)
    {
    }
    /**
     * Return the Array Return Type (Array or Value of first element in the array).
     *
     * @return string $returnType Array return type
     */
    public static function getArrayReturnType()
    {
    }
    /**
     * Is calculation caching enabled?
     *
     * @return bool
     */
    public function getCalculationCacheEnabled()
    {
    }
    /**
     * Enable/disable calculation cache.
     *
     * @param bool $pValue
     */
    public function setCalculationCacheEnabled($pValue) : void
    {
    }
    /**
     * Enable calculation cache.
     */
    public function enableCalculationCache() : void
    {
    }
    /**
     * Disable calculation cache.
     */
    public function disableCalculationCache() : void
    {
    }
    /**
     * Clear calculation cache.
     */
    public function clearCalculationCache() : void
    {
    }
    /**
     * Clear calculation cache for a specified worksheet.
     *
     * @param string $worksheetName
     */
    public function clearCalculationCacheForWorksheet($worksheetName) : void
    {
    }
    /**
     * Rename calculation cache for a specified worksheet.
     *
     * @param string $fromWorksheetName
     * @param string $toWorksheetName
     */
    public function renameCalculationCacheForWorksheet($fromWorksheetName, $toWorksheetName) : void
    {
    }
    /**
     * Enable/disable calculation cache.
     *
     * @param mixed $enabled
     */
    public function setBranchPruningEnabled($enabled) : void
    {
    }
    public function enableBranchPruning() : void
    {
    }
    public function disableBranchPruning() : void
    {
    }
    public function clearBranchStore() : void
    {
    }
    /**
     * Get the currently defined locale code.
     *
     * @return string
     */
    public function getLocale()
    {
    }
    /**
     * Set the locale code.
     *
     * @param string $locale The locale to use for formula translation, eg: 'en_us'
     *
     * @return bool
     */
    public function setLocale(string $locale)
    {
    }
    /**
     * @param string $fromSeparator
     * @param string $toSeparator
     * @param string $formula
     * @param bool $inBraces
     *
     * @return string
     */
    public static function translateSeparator($fromSeparator, $toSeparator, $formula, &$inBraces)
    {
    }
    public function _translateFormulaToLocale($formula)
    {
    }
    public function _translateFormulaToEnglish($formula)
    {
    }
    public static function localeFunc($function)
    {
    }
    /**
     * Wrap string values in quotes.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public static function wrapResult($value)
    {
    }
    /**
     * Remove quotes used as a wrapper to identify string values.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public static function unwrapResult($value)
    {
    }
    /**
     * Calculate cell value (using formula from a cell ID)
     * Retained for backward compatibility.
     *
     * @param Cell $pCell Cell to calculate
     *
     * @return mixed
     */
    public function calculate(?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null)
    {
    }
    /**
     * Calculate the value of a cell formula.
     *
     * @param Cell $pCell Cell to calculate
     * @param bool $resetLog Flag indicating whether the debug log should be reset or not
     *
     * @return mixed
     */
    public function calculateCellValue(?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null, $resetLog = true)
    {
    }
    /**
     * Validate and parse a formula string.
     *
     * @param string $formula Formula to parse
     *
     * @return array|bool
     */
    public function parseFormula($formula)
    {
    }
    /**
     * Calculate the value of a formula.
     *
     * @param string $formula Formula to parse
     * @param string $cellID Address of the cell to calculate
     * @param Cell $pCell Cell to calculate
     *
     * @return mixed
     */
    public function calculateFormula($formula, $cellID = null, ?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null)
    {
    }
    /**
     * @param mixed $cellValue
     */
    public function getValueFromCache(string $cellReference, &$cellValue) : bool
    {
    }
    /**
     * @param string $cellReference
     * @param mixed $cellValue
     */
    public function saveValueToCache($cellReference, $cellValue) : void
    {
    }
    /**
     * Parse a cell formula and calculate its value.
     *
     * @param string $formula The formula to parse and calculate
     * @param string $cellID The ID (e.g. A3) of the cell that we are calculating
     * @param Cell $pCell Cell to calculate
     *
     * @return mixed
     */
    public function _calculateFormulaValue($formula, $cellID = null, ?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null)
    {
    }
    /**
     * Read the dimensions of a matrix, and re-index it with straight numeric keys starting from row 0, column 0.
     *
     * @param array $matrix matrix operand
     *
     * @return int[] An array comprising the number of rows, and number of columns
     */
    public static function getMatrixDimensions(array &$matrix)
    {
    }
    // trigger an error, but nicely, if need be
    protected function raiseFormulaError($errorMessage)
    {
    }
    /**
     * Extract range values.
     *
     * @param string $pRange String based range representation
     * @param Worksheet $worksheet Worksheet
     * @param bool $resetLog Flag indicating whether calculation log should be reset or not
     *
     * @return mixed Array of values in range if range contains more than one element. Otherwise, a single value is returned.
     */
    public function extractCellRange(&$pRange = 'A1', ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null, $resetLog = true)
    {
    }
    /**
     * Extract range values.
     *
     * @param string $range String based range representation
     * @param null|Worksheet $worksheet Worksheet
     * @param bool $resetLog Flag indicating whether calculation log should be reset or not
     *
     * @return mixed Array of values in range if range contains more than one element. Otherwise, a single value is returned.
     */
    public function extractNamedRange(string &$range = 'A1', ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null, $resetLog = true)
    {
    }
    /**
     * Is a specific function implemented?
     *
     * @param string $pFunction Function Name
     *
     * @return bool
     */
    public function isImplemented($pFunction)
    {
    }
    /**
     * Get a list of all implemented functions as an array of function objects.
     */
    public function getFunctions() : array
    {
    }
    /**
     * Get a list of implemented Excel function names.
     *
     * @return array
     */
    public function getImplementedFunctionNames()
    {
    }
}
