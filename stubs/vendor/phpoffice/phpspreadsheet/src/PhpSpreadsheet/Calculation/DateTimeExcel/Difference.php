<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel;

class Difference
{
    /**
     * DATEDIF.
     *
     * @param mixed $startDate Excel date serial value, PHP date/time stamp, PHP DateTime object
     *                                    or a standard date string
     * @param mixed $endDate Excel date serial value, PHP date/time stamp, PHP DateTime object
     *                                    or a standard date string
     * @param string $unit
     *
     * @return int|string Interval between the dates
     */
    public static function interval($startDate, $endDate, $unit = 'D')
    {
    }
}
