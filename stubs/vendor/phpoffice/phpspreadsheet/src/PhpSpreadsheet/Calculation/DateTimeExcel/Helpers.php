<?php

namespace PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel;

class Helpers
{
    /**
     * Identify if a year is a leap year or not.
     *
     * @param int|string $year The year to test
     *
     * @return bool TRUE if the year is a leap year, otherwise FALSE
     */
    public static function isLeapYear($year) : bool
    {
    }
    /**
     * getDateValue.
     *
     * @param mixed $dateValue
     *
     * @return float Excel date/time serial value
     */
    public static function getDateValue($dateValue, bool $allowBool = true) : float
    {
    }
    /**
     * getTimeValue.
     *
     * @param string $timeValue
     *
     * @return mixed Excel date/time serial value, or string if error
     */
    public static function getTimeValue($timeValue)
    {
    }
    /**
     * Adjust date by given months.
     *
     * @param mixed $dateValue
     */
    public static function adjustDateByMonths($dateValue = 0, float $adjustmentMonths = 0) : \DateTime
    {
    }
    /**
     * Help reduce perceived complexity of some tests.
     *
     * @param mixed $value
     * @param mixed $altValue
     */
    public static function replaceIfEmpty(&$value, $altValue) : void
    {
    }
    /**
     * Adjust year in ambiguous situations.
     */
    public static function adjustYear(string $testVal1, string $testVal2, string &$testVal3) : void
    {
    }
    /**
     * Return result in one of three formats.
     *
     * @return mixed
     */
    public static function returnIn3FormatsArray(array $dateArray, bool $noFrac = false)
    {
    }
    /**
     * Return result in one of three formats.
     *
     * @return mixed
     */
    public static function returnIn3FormatsFloat(float $excelDateValue)
    {
    }
    /**
     * Return result in one of three formats.
     *
     * @return mixed
     */
    public static function returnIn3FormatsObject(\DateTime $PHPDateObject)
    {
    }
    /**
     * Many functions accept null/false/true argument treated as 0/0/1.
     *
     * @param mixed $number
     */
    public static function nullFalseTrueToNumber(&$number, bool $allowBool = true) : void
    {
    }
    /**
     * Many functions accept null argument treated as 0.
     *
     * @param mixed $number
     *
     * @return float|int
     */
    public static function validateNumericNull($number)
    {
    }
    /**
     * Many functions accept null/false/true argument treated as 0/0/1.
     *
     * @param mixed $number
     *
     * @return float
     */
    public static function validateNotNegative($number)
    {
    }
    public static function silly1900(\DateTime $PHPDateObject, string $mod = '-1 day') : void
    {
    }
}
