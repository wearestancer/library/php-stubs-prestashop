<?php

namespace PhpOffice\PhpSpreadsheet\Shared;

class CodePage
{
    public const DEFAULT_CODE_PAGE = 'CP1252';
    public static function validate(string $codePage) : bool
    {
    }
    /**
     * Convert Microsoft Code Page Identifier to Code Page Name which iconv
     * and mbstring understands.
     *
     * @param int $codePage Microsoft Code Page Indentifier
     *
     * @return string Code Page Name
     */
    public static function numberToName(int $codePage) : string
    {
    }
    public static function getEncodings() : array
    {
    }
}
