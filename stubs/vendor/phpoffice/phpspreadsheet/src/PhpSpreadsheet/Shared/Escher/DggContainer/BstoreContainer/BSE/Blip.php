<?php

namespace PhpOffice\PhpSpreadsheet\Shared\Escher\DggContainer\BstoreContainer\BSE;

class Blip
{
    /**
     * Get the raw image data.
     *
     * @return string
     */
    public function getData()
    {
    }
    /**
     * Set the raw image data.
     *
     * @param string $data
     */
    public function setData($data) : void
    {
    }
    /**
     * Set parent BSE.
     *
     * @param \PhpOffice\PhpSpreadsheet\Shared\Escher\DggContainer\BstoreContainer\BSE $parent
     */
    public function setParent($parent) : void
    {
    }
    /**
     * Get parent BSE.
     *
     * @return \PhpOffice\PhpSpreadsheet\Shared\Escher\DggContainer\BstoreContainer\BSE $parent
     */
    public function getParent()
    {
    }
}
