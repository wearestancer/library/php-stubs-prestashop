<?php

namespace PhpOffice\PhpSpreadsheet\Shared\Escher\DgContainer\SpgrContainer;

class SpContainer
{
    /**
     * Set parent Shape Group Container.
     *
     * @param SpgrContainer $parent
     */
    public function setParent($parent) : void
    {
    }
    /**
     * Get the parent Shape Group Container.
     *
     * @return SpgrContainer
     */
    public function getParent()
    {
    }
    /**
     * Set whether this is a group shape.
     *
     * @param bool $value
     */
    public function setSpgr($value) : void
    {
    }
    /**
     * Get whether this is a group shape.
     *
     * @return bool
     */
    public function getSpgr()
    {
    }
    /**
     * Set the shape type.
     *
     * @param int $value
     */
    public function setSpType($value) : void
    {
    }
    /**
     * Get the shape type.
     *
     * @return int
     */
    public function getSpType()
    {
    }
    /**
     * Set the shape flag.
     *
     * @param int $value
     */
    public function setSpFlag($value) : void
    {
    }
    /**
     * Get the shape flag.
     *
     * @return int
     */
    public function getSpFlag()
    {
    }
    /**
     * Set the shape index.
     *
     * @param int $value
     */
    public function setSpId($value) : void
    {
    }
    /**
     * Get the shape index.
     *
     * @return int
     */
    public function getSpId()
    {
    }
    /**
     * Set an option for the Shape Group Container.
     *
     * @param int $property The number specifies the option
     * @param mixed $value
     */
    public function setOPT($property, $value) : void
    {
    }
    /**
     * Get an option for the Shape Group Container.
     *
     * @param int $property The number specifies the option
     *
     * @return mixed
     */
    public function getOPT($property)
    {
    }
    /**
     * Get the collection of options.
     *
     * @return array
     */
    public function getOPTCollection()
    {
    }
    /**
     * Set cell coordinates of upper-left corner of shape.
     *
     * @param string $value eg: 'A1'
     */
    public function setStartCoordinates($value) : void
    {
    }
    /**
     * Get cell coordinates of upper-left corner of shape.
     *
     * @return string
     */
    public function getStartCoordinates()
    {
    }
    /**
     * Set offset in x-direction of upper-left corner of shape measured in 1/1024 of column width.
     *
     * @param int $startOffsetX
     */
    public function setStartOffsetX($startOffsetX) : void
    {
    }
    /**
     * Get offset in x-direction of upper-left corner of shape measured in 1/1024 of column width.
     *
     * @return int
     */
    public function getStartOffsetX()
    {
    }
    /**
     * Set offset in y-direction of upper-left corner of shape measured in 1/256 of row height.
     *
     * @param int $startOffsetY
     */
    public function setStartOffsetY($startOffsetY) : void
    {
    }
    /**
     * Get offset in y-direction of upper-left corner of shape measured in 1/256 of row height.
     *
     * @return int
     */
    public function getStartOffsetY()
    {
    }
    /**
     * Set cell coordinates of bottom-right corner of shape.
     *
     * @param string $value eg: 'A1'
     */
    public function setEndCoordinates($value) : void
    {
    }
    /**
     * Get cell coordinates of bottom-right corner of shape.
     *
     * @return string
     */
    public function getEndCoordinates()
    {
    }
    /**
     * Set offset in x-direction of bottom-right corner of shape measured in 1/1024 of column width.
     *
     * @param int $endOffsetX
     */
    public function setEndOffsetX($endOffsetX) : void
    {
    }
    /**
     * Get offset in x-direction of bottom-right corner of shape measured in 1/1024 of column width.
     *
     * @return int
     */
    public function getEndOffsetX()
    {
    }
    /**
     * Set offset in y-direction of bottom-right corner of shape measured in 1/256 of row height.
     *
     * @param int $endOffsetY
     */
    public function setEndOffsetY($endOffsetY) : void
    {
    }
    /**
     * Get offset in y-direction of bottom-right corner of shape measured in 1/256 of row height.
     *
     * @return int
     */
    public function getEndOffsetY()
    {
    }
    /**
     * Get the nesting level of this spContainer. This is the number of spgrContainers between this spContainer and
     * the dgContainer. A value of 1 = immediately within first spgrContainer
     * Higher nesting level occurs if and only if spContainer is part of a shape group.
     *
     * @return int Nesting level
     */
    public function getNestingLevel()
    {
    }
}
