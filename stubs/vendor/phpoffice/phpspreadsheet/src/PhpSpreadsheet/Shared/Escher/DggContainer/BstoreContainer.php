<?php

namespace PhpOffice\PhpSpreadsheet\Shared\Escher\DggContainer;

class BstoreContainer
{
    /**
     * Add a BLIP Store Entry.
     *
     * @param BstoreContainer\BSE $BSE
     */
    public function addBSE($BSE) : void
    {
    }
    /**
     * Get the collection of BLIP Store Entries.
     *
     * @return BstoreContainer\BSE[]
     */
    public function getBSECollection()
    {
    }
}
