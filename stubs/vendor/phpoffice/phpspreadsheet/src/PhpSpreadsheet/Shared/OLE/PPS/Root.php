<?php

namespace PhpOffice\PhpSpreadsheet\Shared\OLE\PPS;

/**
 * Class for creating Root PPS's for OLE containers.
 *
 * @author   Xavier Noguer <xnoguer@php.net>
 */
class Root extends \PhpOffice\PhpSpreadsheet\Shared\OLE\PPS
{
    /**
     * @param null|float|int $time_1st A timestamp
     * @param null|float|int $time_2nd A timestamp
     * @param File[] $raChild
     */
    public function __construct($time_1st, $time_2nd, $raChild)
    {
    }
    /**
     * Method for saving the whole OLE container (including files).
     * In fact, if called with an empty argument (or '-'), it saves to a
     * temporary file and then outputs it's contents to stdout.
     * If a resource pointer to a stream created by fopen() is passed
     * it will be used, but you have to close such stream by yourself.
     *
     * @param resource $fileHandle the name of the file or stream where to save the OLE container
     *
     * @return bool true on success
     */
    public function save($fileHandle)
    {
    }
}
