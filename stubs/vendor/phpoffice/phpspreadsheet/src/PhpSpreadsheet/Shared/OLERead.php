<?php

namespace PhpOffice\PhpSpreadsheet\Shared;

class OLERead
{
    // Size of a sector = 512 bytes
    const BIG_BLOCK_SIZE = 0x200;
    // Size of a short sector = 64 bytes
    const SMALL_BLOCK_SIZE = 0x40;
    // Size of a directory entry always = 128 bytes
    const PROPERTY_STORAGE_BLOCK_SIZE = 0x80;
    // Minimum size of a standard stream = 4096 bytes, streams smaller than this are stored as short streams
    const SMALL_BLOCK_THRESHOLD = 0x1000;
    // header offsets
    const NUM_BIG_BLOCK_DEPOT_BLOCKS_POS = 0x2c;
    const ROOT_START_BLOCK_POS = 0x30;
    const SMALL_BLOCK_DEPOT_BLOCK_POS = 0x3c;
    const EXTENSION_BLOCK_POS = 0x44;
    const NUM_EXTENSION_BLOCK_POS = 0x48;
    const BIG_BLOCK_DEPOT_BLOCKS_POS = 0x4c;
    // property storage offsets (directory offsets)
    const SIZE_OF_NAME_POS = 0x40;
    const TYPE_POS = 0x42;
    const START_BLOCK_POS = 0x74;
    const SIZE_POS = 0x78;
    public $wrkbook;
    public $summaryInformation;
    public $documentSummaryInformation;
    /**
     * Read the file.
     */
    public function read(string $filename) : void
    {
    }
    /**
     * Extract binary stream data.
     *
     * @param int $stream
     *
     * @return null|string
     */
    public function getStream($stream)
    {
    }
}
