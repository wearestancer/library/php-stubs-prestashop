<?php

namespace PhpOffice\PhpSpreadsheet\Shared;

class File
{
    /**
     * Use Temp or File Upload Temp for temporary files.
     *
     * @var bool
     */
    protected static $useUploadTempDirectory = false;
    /**
     * Set the flag indicating whether the File Upload Temp directory should be used for temporary files.
     */
    public static function setUseUploadTempDirectory(bool $useUploadTempDir) : void
    {
    }
    /**
     * Get the flag indicating whether the File Upload Temp directory should be used for temporary files.
     */
    public static function getUseUploadTempDirectory() : bool
    {
    }
    /**
     * Verify if a file exists.
     */
    public static function fileExists(string $filename) : bool
    {
    }
    /**
     * Returns canonicalized absolute pathname, also for ZIP archives.
     */
    public static function realpath(string $filename) : string
    {
    }
    /**
     * Get the systems temporary directory.
     */
    public static function sysGetTempDir() : string
    {
    }
    public static function temporaryFilename() : string
    {
    }
    /**
     * Assert that given path is an existing file and is readable, otherwise throw exception.
     */
    public static function assertFile(string $filename, string $zipMember = '') : void
    {
    }
    /**
     * Same as assertFile, except return true/false and don't throw Exception.
     */
    public static function testFileNoThrow(string $filename, string $zipMember = '') : bool
    {
    }
}
