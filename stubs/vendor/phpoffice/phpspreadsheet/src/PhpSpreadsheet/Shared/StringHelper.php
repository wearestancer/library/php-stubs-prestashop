<?php

namespace PhpOffice\PhpSpreadsheet\Shared;

class StringHelper
{
    /**    Constants                */
    /**    Regular Expressions        */
    //    Fraction
    const STRING_REGEXP_FRACTION = '(-?)(\\d+)\\s+(\\d+\\/\\d+)';
    /**
     * Get whether iconv extension is available.
     *
     * @return bool
     */
    public static function getIsIconvEnabled()
    {
    }
    /**
     * Convert from OpenXML escaped control character to PHP control character.
     *
     * Excel 2007 team:
     * ----------------
     * That's correct, control characters are stored directly in the shared-strings table.
     * We do encode characters that cannot be represented in XML using the following escape sequence:
     * _xHHHH_ where H represents a hexadecimal character in the character's value...
     * So you could end up with something like _x0008_ in a string (either in a cell value (<v>)
     * element or in the shared string <t> element.
     *
     * @param string $textValue Value to unescape
     *
     * @return string
     */
    public static function controlCharacterOOXML2PHP($textValue)
    {
    }
    /**
     * Convert from PHP control character to OpenXML escaped control character.
     *
     * Excel 2007 team:
     * ----------------
     * That's correct, control characters are stored directly in the shared-strings table.
     * We do encode characters that cannot be represented in XML using the following escape sequence:
     * _xHHHH_ where H represents a hexadecimal character in the character's value...
     * So you could end up with something like _x0008_ in a string (either in a cell value (<v>)
     * element or in the shared string <t> element.
     *
     * @param string $textValue Value to escape
     *
     * @return string
     */
    public static function controlCharacterPHP2OOXML($textValue)
    {
    }
    /**
     * Try to sanitize UTF8, stripping invalid byte sequences. Not perfect. Does not surrogate characters.
     *
     * @param string $textValue
     *
     * @return string
     */
    public static function sanitizeUTF8($textValue)
    {
    }
    /**
     * Check if a string contains UTF8 data.
     *
     * @param string $textValue
     *
     * @return bool
     */
    public static function isUTF8($textValue)
    {
    }
    /**
     * Formats a numeric value as a string for output in various output writers forcing
     * point as decimal separator in case locale is other than English.
     *
     * @param mixed $numericValue
     *
     * @return string
     */
    public static function formatNumber($numericValue)
    {
    }
    /**
     * Converts a UTF-8 string into BIFF8 Unicode string data (8-bit string length)
     * Writes the string using uncompressed notation, no rich text, no Asian phonetics
     * If mbstring extension is not available, ASCII is assumed, and compressed notation is used
     * although this will give wrong results for non-ASCII strings
     * see OpenOffice.org's Documentation of the Microsoft Excel File Format, sect. 2.5.3.
     *
     * @param string $textValue UTF-8 encoded string
     * @param mixed[] $arrcRuns Details of rich text runs in $value
     *
     * @return string
     */
    public static function UTF8toBIFF8UnicodeShort($textValue, $arrcRuns = [])
    {
    }
    /**
     * Converts a UTF-8 string into BIFF8 Unicode string data (16-bit string length)
     * Writes the string using uncompressed notation, no rich text, no Asian phonetics
     * If mbstring extension is not available, ASCII is assumed, and compressed notation is used
     * although this will give wrong results for non-ASCII strings
     * see OpenOffice.org's Documentation of the Microsoft Excel File Format, sect. 2.5.3.
     *
     * @param string $textValue UTF-8 encoded string
     *
     * @return string
     */
    public static function UTF8toBIFF8UnicodeLong($textValue)
    {
    }
    /**
     * Convert string from one encoding to another.
     *
     * @param string $textValue
     * @param string $to Encoding to convert to, e.g. 'UTF-8'
     * @param string $from Encoding to convert from, e.g. 'UTF-16LE'
     *
     * @return string
     */
    public static function convertEncoding($textValue, $to, $from)
    {
    }
    /**
     * Get character count.
     *
     * @param string $textValue
     * @param string $encoding Encoding
     *
     * @return int Character count
     */
    public static function countCharacters($textValue, $encoding = 'UTF-8')
    {
    }
    /**
     * Get a substring of a UTF-8 encoded string.
     *
     * @param string $textValue UTF-8 encoded string
     * @param int $offset Start offset
     * @param int $length Maximum number of characters in substring
     *
     * @return string
     */
    public static function substring($textValue, $offset, $length = 0)
    {
    }
    /**
     * Convert a UTF-8 encoded string to upper case.
     *
     * @param string $textValue UTF-8 encoded string
     *
     * @return string
     */
    public static function strToUpper($textValue)
    {
    }
    /**
     * Convert a UTF-8 encoded string to lower case.
     *
     * @param string $textValue UTF-8 encoded string
     *
     * @return string
     */
    public static function strToLower($textValue)
    {
    }
    /**
     * Convert a UTF-8 encoded string to title/proper case
     * (uppercase every first character in each word, lower case all other characters).
     *
     * @param string $textValue UTF-8 encoded string
     *
     * @return string
     */
    public static function strToTitle($textValue)
    {
    }
    public static function mbIsUpper($character)
    {
    }
    public static function mbStrSplit($string)
    {
    }
    /**
     * Reverse the case of a string, so that all uppercase characters become lowercase
     * and all lowercase characters become uppercase.
     *
     * @param string $textValue UTF-8 encoded string
     *
     * @return string
     */
    public static function strCaseReverse($textValue)
    {
    }
    /**
     * Identify whether a string contains a fractional numeric value,
     * and convert it to a numeric if it is.
     *
     * @param string $operand string value to test
     *
     * @return bool
     */
    public static function convertToNumberIfFraction(&$operand)
    {
    }
    //    function convertToNumberIfFraction()
    /**
     * Get the decimal separator. If it has not yet been set explicitly, try to obtain number
     * formatting information from locale.
     *
     * @return string
     */
    public static function getDecimalSeparator()
    {
    }
    /**
     * Set the decimal separator. Only used by NumberFormat::toFormattedString()
     * to format output by \PhpOffice\PhpSpreadsheet\Writer\Html and \PhpOffice\PhpSpreadsheet\Writer\Pdf.
     *
     * @param string $separator Character for decimal separator
     */
    public static function setDecimalSeparator($separator) : void
    {
    }
    /**
     * Get the thousands separator. If it has not yet been set explicitly, try to obtain number
     * formatting information from locale.
     *
     * @return string
     */
    public static function getThousandsSeparator()
    {
    }
    /**
     * Set the thousands separator. Only used by NumberFormat::toFormattedString()
     * to format output by \PhpOffice\PhpSpreadsheet\Writer\Html and \PhpOffice\PhpSpreadsheet\Writer\Pdf.
     *
     * @param string $separator Character for thousands separator
     */
    public static function setThousandsSeparator($separator) : void
    {
    }
    /**
     *    Get the currency code. If it has not yet been set explicitly, try to obtain the
     *        symbol information from locale.
     *
     * @return string
     */
    public static function getCurrencyCode()
    {
    }
    /**
     * Set the currency code. Only used by NumberFormat::toFormattedString()
     *        to format output by \PhpOffice\PhpSpreadsheet\Writer\Html and \PhpOffice\PhpSpreadsheet\Writer\Pdf.
     *
     * @param string $currencyCode Character for currency code
     */
    public static function setCurrencyCode($currencyCode) : void
    {
    }
    /**
     * Convert SYLK encoded string to UTF-8.
     *
     * @param string $textValue
     *
     * @return string UTF-8 encoded string
     */
    public static function SYLKtoUTF8($textValue)
    {
    }
    /**
     * Retrieve any leading numeric part of a string, or return the full string if no leading numeric
     * (handles basic integer or float, but not exponent or non decimal).
     *
     * @param string $textValue
     *
     * @return mixed string or only the leading numeric part of the string
     */
    public static function testStringAsNumeric($textValue)
    {
    }
}
