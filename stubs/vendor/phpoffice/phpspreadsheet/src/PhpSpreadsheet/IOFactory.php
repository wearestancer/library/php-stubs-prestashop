<?php

namespace PhpOffice\PhpSpreadsheet;

/**
 * Factory to create readers and writers easily.
 *
 * It is not required to use this class, but it should make it easier to read and write files.
 * Especially for reading files with an unknown format.
 */
abstract class IOFactory
{
    /**
     * Create Writer\IWriter.
     */
    public static function createWriter(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet, string $writerType) : \PhpOffice\PhpSpreadsheet\Writer\IWriter
    {
    }
    /**
     * Create IReader.
     */
    public static function createReader(string $readerType) : \PhpOffice\PhpSpreadsheet\Reader\IReader
    {
    }
    /**
     * Loads Spreadsheet from file using automatic Reader\IReader resolution.
     *
     * @param string $filename The name of the spreadsheet file
     */
    public static function load(string $filename, int $flags = 0) : \PhpOffice\PhpSpreadsheet\Spreadsheet
    {
    }
    /**
     * Identify file type using automatic IReader resolution.
     */
    public static function identify(string $filename) : string
    {
    }
    /**
     * Create Reader\IReader for file using automatic IReader resolution.
     */
    public static function createReaderForFile(string $filename) : \PhpOffice\PhpSpreadsheet\Reader\IReader
    {
    }
    /**
     * Register a writer with its type and class name.
     */
    public static function registerWriter(string $writerType, string $writerClass) : void
    {
    }
    /**
     * Register a reader with its type and class name.
     */
    public static function registerReader(string $readerType, string $readerClass) : void
    {
    }
}
