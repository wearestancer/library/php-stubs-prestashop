<?php

namespace PhpOffice\PhpSpreadsheet;

abstract class DefinedName
{
    protected const REGEXP_IDENTIFY_FORMULA = '[^_\\p{N}\\p{L}:, \\$\'!]';
    /**
     * Name.
     *
     * @var string
     */
    protected $name;
    /**
     * Worksheet on which the defined name can be resolved.
     *
     * @var Worksheet
     */
    protected $worksheet;
    /**
     * Value of the named object.
     *
     * @var string
     */
    protected $value;
    /**
     * Is the defined named local? (i.e. can only be used on $this->worksheet).
     *
     * @var bool
     */
    protected $localOnly;
    /**
     * Scope.
     *
     * @var Worksheet
     */
    protected $scope;
    /**
     * Whether this is a named range or a named formula.
     *
     * @var bool
     */
    protected $isFormula;
    /**
     * Create a new Defined Name.
     */
    public function __construct(string $name, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null, ?string $value = null, bool $localOnly = false, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $scope = null)
    {
    }
    /**
     * Create a new defined name, either a range or a formula.
     */
    public static function createInstance(string $name, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null, ?string $value = null, bool $localOnly = false, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $scope = null) : self
    {
    }
    public static function testIfFormula(string $value) : bool
    {
    }
    /**
     * Get name.
     */
    public function getName() : string
    {
    }
    /**
     * Set name.
     */
    public function setName(string $name) : self
    {
    }
    /**
     * Get worksheet.
     */
    public function getWorksheet() : ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
    {
    }
    /**
     * Set worksheet.
     */
    public function setWorksheet(?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet) : self
    {
    }
    /**
     * Get range or formula value.
     */
    public function getValue() : string
    {
    }
    /**
     * Set range or formula  value.
     */
    public function setValue(string $value) : self
    {
    }
    /**
     * Get localOnly.
     */
    public function getLocalOnly() : bool
    {
    }
    /**
     * Set localOnly.
     */
    public function setLocalOnly(bool $localScope) : self
    {
    }
    /**
     * Get scope.
     */
    public function getScope() : ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
    {
    }
    /**
     * Set scope.
     */
    public function setScope(?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet) : self
    {
    }
    /**
     * Identify whether this is a named range or a named formula.
     */
    public function isFormula() : bool
    {
    }
    /**
     * Resolve a named range to a regular cell range or formula.
     */
    public static function resolveName(string $definedName, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet, string $sheetName = '') : ?self
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
}
