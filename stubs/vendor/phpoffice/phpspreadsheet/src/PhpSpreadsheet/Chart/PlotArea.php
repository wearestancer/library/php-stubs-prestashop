<?php

namespace PhpOffice\PhpSpreadsheet\Chart;

class PlotArea
{
    /**
     * Create a new PlotArea.
     *
     * @param DataSeries[] $plotSeries
     */
    public function __construct(?\PhpOffice\PhpSpreadsheet\Chart\Layout $layout = null, array $plotSeries = [])
    {
    }
    /**
     * Get Layout.
     *
     * @return Layout
     */
    public function getLayout()
    {
    }
    /**
     * Get Number of Plot Groups.
     */
    public function getPlotGroupCount() : int
    {
    }
    /**
     * Get Number of Plot Series.
     *
     * @return int
     */
    public function getPlotSeriesCount()
    {
    }
    /**
     * Get Plot Series.
     *
     * @return DataSeries[]
     */
    public function getPlotGroup()
    {
    }
    /**
     * Get Plot Series by Index.
     *
     * @param mixed $index
     *
     * @return DataSeries
     */
    public function getPlotGroupByIndex($index)
    {
    }
    /**
     * Set Plot Series.
     *
     * @param DataSeries[] $plotSeries
     *
     * @return $this
     */
    public function setPlotSeries(array $plotSeries)
    {
    }
    public function refresh(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet) : void
    {
    }
}
