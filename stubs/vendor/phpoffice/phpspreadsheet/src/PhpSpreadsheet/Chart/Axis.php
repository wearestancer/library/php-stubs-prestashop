<?php

namespace PhpOffice\PhpSpreadsheet\Chart;

/**
 * Created by PhpStorm.
 * User: Wiktor Trzonkowski
 * Date: 6/17/14
 * Time: 12:11 PM.
 */
class Axis extends \PhpOffice\PhpSpreadsheet\Chart\Properties
{
    /**
     * Get Series Data Type.
     *
     * @param mixed $format_code
     */
    public function setAxisNumberProperties($format_code) : void
    {
    }
    /**
     * Get Axis Number Format Data Type.
     *
     * @return string
     */
    public function getAxisNumberFormat()
    {
    }
    /**
     * Get Axis Number Source Linked.
     *
     * @return string
     */
    public function getAxisNumberSourceLinked()
    {
    }
    /**
     * Set Axis Options Properties.
     *
     * @param string $axisLabels
     * @param string $horizontalCrossesValue
     * @param string $horizontalCrosses
     * @param string $axisOrientation
     * @param string $majorTmt
     * @param string $minorTmt
     * @param string $minimum
     * @param string $maximum
     * @param string $majorUnit
     * @param string $minorUnit
     */
    public function setAxisOptionsProperties($axisLabels, $horizontalCrossesValue = null, $horizontalCrosses = null, $axisOrientation = null, $majorTmt = null, $minorTmt = null, $minimum = null, $maximum = null, $majorUnit = null, $minorUnit = null) : void
    {
    }
    /**
     * Get Axis Options Property.
     *
     * @param string $property
     *
     * @return string
     */
    public function getAxisOptionsProperty($property)
    {
    }
    /**
     * Set Axis Orientation Property.
     *
     * @param string $orientation
     */
    public function setAxisOrientation($orientation) : void
    {
    }
    /**
     * Set Fill Property.
     *
     * @param string $color
     * @param int $alpha
     * @param string $AlphaType
     */
    public function setFillParameters($color, $alpha = 0, $AlphaType = self::EXCEL_COLOR_TYPE_ARGB) : void
    {
    }
    /**
     * Set Line Property.
     *
     * @param string $color
     * @param int $alpha
     * @param string $alphaType
     */
    public function setLineParameters($color, $alpha = 0, $alphaType = self::EXCEL_COLOR_TYPE_ARGB) : void
    {
    }
    /**
     * Get Fill Property.
     *
     * @param string $property
     *
     * @return string
     */
    public function getFillProperty($property)
    {
    }
    /**
     * Get Line Property.
     *
     * @param string $property
     *
     * @return string
     */
    public function getLineProperty($property)
    {
    }
    /**
     * Set Line Style Properties.
     *
     * @param float $lineWidth
     * @param string $compoundType
     * @param string $dashType
     * @param string $capType
     * @param string $joinType
     * @param string $headArrowType
     * @param string $headArrowSize
     * @param string $endArrowType
     * @param string $endArrowSize
     */
    public function setLineStyleProperties($lineWidth = null, $compoundType = null, $dashType = null, $capType = null, $joinType = null, $headArrowType = null, $headArrowSize = null, $endArrowType = null, $endArrowSize = null) : void
    {
    }
    /**
     * Get Line Style Property.
     *
     * @param array|string $elements
     *
     * @return string
     */
    public function getLineStyleProperty($elements)
    {
    }
    /**
     * Get Line Style Arrow Excel Width.
     *
     * @param string $arrow
     *
     * @return string
     */
    public function getLineStyleArrowWidth($arrow)
    {
    }
    /**
     * Get Line Style Arrow Excel Length.
     *
     * @param string $arrow
     *
     * @return string
     */
    public function getLineStyleArrowLength($arrow)
    {
    }
    /**
     * Set Shadow Properties.
     *
     * @param int $shadowPresets
     * @param string $colorValue
     * @param string $colorType
     * @param string $colorAlpha
     * @param float $blur
     * @param int $angle
     * @param float $distance
     */
    public function setShadowProperties($shadowPresets, $colorValue = null, $colorType = null, $colorAlpha = null, $blur = null, $angle = null, $distance = null) : void
    {
    }
    /**
     * Get Shadow Property.
     *
     * @param string|string[] $elements
     *
     * @return null|array|int|string
     */
    public function getShadowProperty($elements)
    {
    }
    /**
     * Set Glow Properties.
     *
     * @param float $size
     * @param string $colorValue
     * @param int $colorAlpha
     * @param string $colorType
     */
    public function setGlowProperties($size, $colorValue = null, $colorAlpha = null, $colorType = null) : void
    {
    }
    /**
     * Get Glow Property.
     *
     * @param array|string $property
     *
     * @return string
     */
    public function getGlowProperty($property)
    {
    }
    /**
     * Set Soft Edges Size.
     *
     * @param float $size
     */
    public function setSoftEdges($size) : void
    {
    }
    /**
     * Get Soft Edges Size.
     *
     * @return string
     */
    public function getSoftEdgesSize()
    {
    }
}
