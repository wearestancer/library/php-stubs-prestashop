<?php

namespace PhpOffice\PhpSpreadsheet\Chart;

class Chart
{
    /**
     * Create a new Chart.
     *
     * @param mixed $name
     * @param mixed $plotVisibleOnly
     * @param string $displayBlanksAs
     */
    public function __construct($name, ?\PhpOffice\PhpSpreadsheet\Chart\Title $title = null, ?\PhpOffice\PhpSpreadsheet\Chart\Legend $legend = null, ?\PhpOffice\PhpSpreadsheet\Chart\PlotArea $plotArea = null, $plotVisibleOnly = true, $displayBlanksAs = \PhpOffice\PhpSpreadsheet\Chart\DataSeries::EMPTY_AS_GAP, ?\PhpOffice\PhpSpreadsheet\Chart\Title $xAxisLabel = null, ?\PhpOffice\PhpSpreadsheet\Chart\Title $yAxisLabel = null, ?\PhpOffice\PhpSpreadsheet\Chart\Axis $xAxis = null, ?\PhpOffice\PhpSpreadsheet\Chart\Axis $yAxis = null, ?\PhpOffice\PhpSpreadsheet\Chart\GridLines $majorGridlines = null, ?\PhpOffice\PhpSpreadsheet\Chart\GridLines $minorGridlines = null)
    {
    }
    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Get Worksheet.
     *
     * @return Worksheet
     */
    public function getWorksheet()
    {
    }
    /**
     * Set Worksheet.
     *
     * @param Worksheet $worksheet
     *
     * @return $this
     */
    public function setWorksheet(?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null)
    {
    }
    /**
     * Get Title.
     *
     * @return Title
     */
    public function getTitle()
    {
    }
    /**
     * Set Title.
     *
     * @return $this
     */
    public function setTitle(\PhpOffice\PhpSpreadsheet\Chart\Title $title)
    {
    }
    /**
     * Get Legend.
     *
     * @return Legend
     */
    public function getLegend()
    {
    }
    /**
     * Set Legend.
     *
     * @return $this
     */
    public function setLegend(\PhpOffice\PhpSpreadsheet\Chart\Legend $legend)
    {
    }
    /**
     * Get X-Axis Label.
     *
     * @return Title
     */
    public function getXAxisLabel()
    {
    }
    /**
     * Set X-Axis Label.
     *
     * @return $this
     */
    public function setXAxisLabel(\PhpOffice\PhpSpreadsheet\Chart\Title $label)
    {
    }
    /**
     * Get Y-Axis Label.
     *
     * @return Title
     */
    public function getYAxisLabel()
    {
    }
    /**
     * Set Y-Axis Label.
     *
     * @return $this
     */
    public function setYAxisLabel(\PhpOffice\PhpSpreadsheet\Chart\Title $label)
    {
    }
    /**
     * Get Plot Area.
     *
     * @return PlotArea
     */
    public function getPlotArea()
    {
    }
    /**
     * Get Plot Visible Only.
     *
     * @return bool
     */
    public function getPlotVisibleOnly()
    {
    }
    /**
     * Set Plot Visible Only.
     *
     * @param bool $plotVisibleOnly
     *
     * @return $this
     */
    public function setPlotVisibleOnly($plotVisibleOnly)
    {
    }
    /**
     * Get Display Blanks as.
     *
     * @return string
     */
    public function getDisplayBlanksAs()
    {
    }
    /**
     * Set Display Blanks as.
     *
     * @param string $displayBlanksAs
     *
     * @return $this
     */
    public function setDisplayBlanksAs($displayBlanksAs)
    {
    }
    /**
     * Get yAxis.
     *
     * @return Axis
     */
    public function getChartAxisY()
    {
    }
    /**
     * Get xAxis.
     *
     * @return Axis
     */
    public function getChartAxisX()
    {
    }
    /**
     * Get Major Gridlines.
     *
     * @return GridLines
     */
    public function getMajorGridlines()
    {
    }
    /**
     * Get Minor Gridlines.
     *
     * @return GridLines
     */
    public function getMinorGridlines()
    {
    }
    /**
     * Set the Top Left position for the chart.
     *
     * @param string $cell
     * @param int $xOffset
     * @param int $yOffset
     *
     * @return $this
     */
    public function setTopLeftPosition($cell, $xOffset = null, $yOffset = null)
    {
    }
    /**
     * Get the top left position of the chart.
     *
     * @return array{cell: string, xOffset: int, yOffset: int} an associative array containing the cell address, X-Offset and Y-Offset from the top left of that cell
     */
    public function getTopLeftPosition()
    {
    }
    /**
     * Get the cell address where the top left of the chart is fixed.
     *
     * @return string
     */
    public function getTopLeftCell()
    {
    }
    /**
     * Set the Top Left cell position for the chart.
     *
     * @param string $cell
     *
     * @return $this
     */
    public function setTopLeftCell($cell)
    {
    }
    /**
     * Set the offset position within the Top Left cell for the chart.
     *
     * @param int $xOffset
     * @param int $yOffset
     *
     * @return $this
     */
    public function setTopLeftOffset($xOffset, $yOffset)
    {
    }
    /**
     * Get the offset position within the Top Left cell for the chart.
     *
     * @return int[]
     */
    public function getTopLeftOffset()
    {
    }
    public function setTopLeftXOffset($xOffset)
    {
    }
    public function getTopLeftXOffset()
    {
    }
    public function setTopLeftYOffset($yOffset)
    {
    }
    public function getTopLeftYOffset()
    {
    }
    /**
     * Set the Bottom Right position of the chart.
     *
     * @param string $cell
     * @param int $xOffset
     * @param int $yOffset
     *
     * @return $this
     */
    public function setBottomRightPosition($cell, $xOffset = null, $yOffset = null)
    {
    }
    /**
     * Get the bottom right position of the chart.
     *
     * @return array an associative array containing the cell address, X-Offset and Y-Offset from the top left of that cell
     */
    public function getBottomRightPosition()
    {
    }
    public function setBottomRightCell($cell)
    {
    }
    /**
     * Get the cell address where the bottom right of the chart is fixed.
     *
     * @return string
     */
    public function getBottomRightCell()
    {
    }
    /**
     * Set the offset position within the Bottom Right cell for the chart.
     *
     * @param int $xOffset
     * @param int $yOffset
     *
     * @return $this
     */
    public function setBottomRightOffset($xOffset, $yOffset)
    {
    }
    /**
     * Get the offset position within the Bottom Right cell for the chart.
     *
     * @return int[]
     */
    public function getBottomRightOffset()
    {
    }
    public function setBottomRightXOffset($xOffset)
    {
    }
    public function getBottomRightXOffset()
    {
    }
    public function setBottomRightYOffset($yOffset)
    {
    }
    public function getBottomRightYOffset()
    {
    }
    public function refresh() : void
    {
    }
    /**
     * Render the chart to given file (or stream).
     *
     * @param string $outputDestination Name of the file render to
     *
     * @return bool true on success
     */
    public function render($outputDestination = null)
    {
    }
}
