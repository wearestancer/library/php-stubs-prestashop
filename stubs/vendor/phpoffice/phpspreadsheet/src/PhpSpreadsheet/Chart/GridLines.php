<?php

namespace PhpOffice\PhpSpreadsheet\Chart;

/**
 * Created by PhpStorm.
 * User: Wiktor Trzonkowski
 * Date: 7/2/14
 * Time: 2:36 PM.
 */
class GridLines extends \PhpOffice\PhpSpreadsheet\Chart\Properties
{
    /**
     * Get Object State.
     *
     * @return bool
     */
    public function getObjectState()
    {
    }
    /**
     * Set Line Color Properties.
     *
     * @param string $value
     * @param int $alpha
     * @param string $colorType
     */
    public function setLineColorProperties($value, $alpha = 0, $colorType = self::EXCEL_COLOR_TYPE_STANDARD) : void
    {
    }
    /**
     * Set Line Color Properties.
     *
     * @param float $lineWidth
     * @param string $compoundType
     * @param string $dashType
     * @param string $capType
     * @param string $joinType
     * @param string $headArrowType
     * @param string $headArrowSize
     * @param string $endArrowType
     * @param string $endArrowSize
     */
    public function setLineStyleProperties($lineWidth = null, $compoundType = null, $dashType = null, $capType = null, $joinType = null, $headArrowType = null, $headArrowSize = null, $endArrowType = null, $endArrowSize = null) : void
    {
    }
    /**
     * Get Line Color Property.
     *
     * @param string $propertyName
     *
     * @return string
     */
    public function getLineColorProperty($propertyName)
    {
    }
    /**
     * Get Line Style Property.
     *
     * @param array|string $elements
     *
     * @return string
     */
    public function getLineStyleProperty($elements)
    {
    }
    /**
     * Set Glow Properties.
     *
     * @param float $size
     * @param string $colorValue
     * @param int $colorAlpha
     * @param string $colorType
     */
    public function setGlowProperties($size, $colorValue = null, $colorAlpha = null, $colorType = null) : void
    {
    }
    /**
     * Get Glow Color Property.
     *
     * @param string $propertyName
     *
     * @return string
     */
    public function getGlowColor($propertyName)
    {
    }
    /**
     * Get Glow Size.
     *
     * @return string
     */
    public function getGlowSize()
    {
    }
    /**
     * Get Line Style Arrow Parameters.
     *
     * @param string $arrowSelector
     * @param string $propertySelector
     *
     * @return string
     */
    public function getLineStyleArrowParameters($arrowSelector, $propertySelector)
    {
    }
    /**
     * Set Shadow Properties.
     *
     * @param int $presets
     * @param string $colorValue
     * @param string $colorType
     * @param string $colorAlpha
     * @param string $blur
     * @param int $angle
     * @param float $distance
     */
    public function setShadowProperties($presets, $colorValue = null, $colorType = null, $colorAlpha = null, $blur = null, $angle = null, $distance = null) : void
    {
    }
    /**
     * Get Shadow Property.
     *
     * @param string|string[] $elements
     *
     * @return string
     */
    public function getShadowProperty($elements)
    {
    }
    /**
     * Set Soft Edges Size.
     *
     * @param float $size
     */
    public function setSoftEdgesSize($size) : void
    {
    }
    /**
     * Get Soft Edges Size.
     *
     * @return string
     */
    public function getSoftEdgesSize()
    {
    }
}
