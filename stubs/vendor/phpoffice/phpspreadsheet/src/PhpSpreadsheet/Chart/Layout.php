<?php

namespace PhpOffice\PhpSpreadsheet\Chart;

class Layout
{
    /**
     * Create a new Layout.
     */
    public function __construct(array $layout = [])
    {
    }
    /**
     * Get Layout Target.
     *
     * @return string
     */
    public function getLayoutTarget()
    {
    }
    /**
     * Set Layout Target.
     *
     * @param string $target
     *
     * @return $this
     */
    public function setLayoutTarget($target)
    {
    }
    /**
     * Get X-Mode.
     *
     * @return string
     */
    public function getXMode()
    {
    }
    /**
     * Set X-Mode.
     *
     * @param string $mode
     *
     * @return $this
     */
    public function setXMode($mode)
    {
    }
    /**
     * Get Y-Mode.
     *
     * @return string
     */
    public function getYMode()
    {
    }
    /**
     * Set Y-Mode.
     *
     * @param string $mode
     *
     * @return $this
     */
    public function setYMode($mode)
    {
    }
    /**
     * Get X-Position.
     *
     * @return number
     */
    public function getXPosition()
    {
    }
    /**
     * Set X-Position.
     *
     * @param float $position
     *
     * @return $this
     */
    public function setXPosition($position)
    {
    }
    /**
     * Get Y-Position.
     *
     * @return number
     */
    public function getYPosition()
    {
    }
    /**
     * Set Y-Position.
     *
     * @param float $position
     *
     * @return $this
     */
    public function setYPosition($position)
    {
    }
    /**
     * Get Width.
     *
     * @return number
     */
    public function getWidth()
    {
    }
    /**
     * Set Width.
     *
     * @param float $width
     *
     * @return $this
     */
    public function setWidth($width)
    {
    }
    /**
     * Get Height.
     *
     * @return number
     */
    public function getHeight()
    {
    }
    /**
     * Set Height.
     *
     * @param float $height
     *
     * @return $this
     */
    public function setHeight($height)
    {
    }
    /**
     * Get show legend key.
     *
     * @return bool
     */
    public function getShowLegendKey()
    {
    }
    /**
     * Set show legend key
     * Specifies that legend keys should be shown in data labels.
     *
     * @param bool $showLegendKey Show legend key
     *
     * @return $this
     */
    public function setShowLegendKey($showLegendKey)
    {
    }
    /**
     * Get show value.
     *
     * @return bool
     */
    public function getShowVal()
    {
    }
    /**
     * Set show val
     * Specifies that the value should be shown in data labels.
     *
     * @param bool $showDataLabelValues Show val
     *
     * @return $this
     */
    public function setShowVal($showDataLabelValues)
    {
    }
    /**
     * Get show category name.
     *
     * @return bool
     */
    public function getShowCatName()
    {
    }
    /**
     * Set show cat name
     * Specifies that the category name should be shown in data labels.
     *
     * @param bool $showCategoryName Show cat name
     *
     * @return $this
     */
    public function setShowCatName($showCategoryName)
    {
    }
    /**
     * Get show data series name.
     *
     * @return bool
     */
    public function getShowSerName()
    {
    }
    /**
     * Set show ser name
     * Specifies that the series name should be shown in data labels.
     *
     * @param bool $showSeriesName Show series name
     *
     * @return $this
     */
    public function setShowSerName($showSeriesName)
    {
    }
    /**
     * Get show percentage.
     *
     * @return bool
     */
    public function getShowPercent()
    {
    }
    /**
     * Set show percentage
     * Specifies that the percentage should be shown in data labels.
     *
     * @param bool $showPercentage Show percentage
     *
     * @return $this
     */
    public function setShowPercent($showPercentage)
    {
    }
    /**
     * Get show bubble size.
     *
     * @return bool
     */
    public function getShowBubbleSize()
    {
    }
    /**
     * Set show bubble size
     * Specifies that the bubble size should be shown in data labels.
     *
     * @param bool $showBubbleSize Show bubble size
     *
     * @return $this
     */
    public function setShowBubbleSize($showBubbleSize)
    {
    }
    /**
     * Get show leader lines.
     *
     * @return bool
     */
    public function getShowLeaderLines()
    {
    }
    /**
     * Set show leader lines
     * Specifies that leader lines should be shown in data labels.
     *
     * @param bool $showLeaderLines Show leader lines
     *
     * @return $this
     */
    public function setShowLeaderLines($showLeaderLines)
    {
    }
}
