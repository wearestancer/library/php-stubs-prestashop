<?php

namespace PhpOffice\PhpSpreadsheet\Chart;

class Title
{
    /**
     * Create a new Title.
     *
     * @param array|RichText|string $caption
     */
    public function __construct($caption = '', ?\PhpOffice\PhpSpreadsheet\Chart\Layout $layout = null)
    {
    }
    /**
     * Get caption.
     *
     * @return array|RichText|string
     */
    public function getCaption()
    {
    }
    public function getCaptionText() : string
    {
    }
    /**
     * Set caption.
     *
     * @param array|RichText|string $caption
     *
     * @return $this
     */
    public function setCaption($caption)
    {
    }
    /**
     * Get Layout.
     *
     * @return Layout
     */
    public function getLayout()
    {
    }
}
