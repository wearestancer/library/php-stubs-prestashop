<?php

namespace PhpOffice\PhpSpreadsheet;

class Spreadsheet
{
    // Allowable values for workbook window visilbity
    const VISIBILITY_VISIBLE = 'visible';
    const VISIBILITY_HIDDEN = 'hidden';
    const VISIBILITY_VERY_HIDDEN = 'veryHidden';
    /**
     * The workbook has macros ?
     *
     * @return bool
     */
    public function hasMacros()
    {
    }
    /**
     * Define if a workbook has macros.
     *
     * @param bool $hasMacros true|false
     */
    public function setHasMacros($hasMacros) : void
    {
    }
    /**
     * Set the macros code.
     *
     * @param string $macroCode string|null
     */
    public function setMacrosCode($macroCode) : void
    {
    }
    /**
     * Return the macros code.
     *
     * @return null|string
     */
    public function getMacrosCode()
    {
    }
    /**
     * Set the macros certificate.
     *
     * @param null|string $certificate
     */
    public function setMacrosCertificate($certificate) : void
    {
    }
    /**
     * Is the project signed ?
     *
     * @return bool true|false
     */
    public function hasMacrosCertificate()
    {
    }
    /**
     * Return the macros certificate.
     *
     * @return null|string
     */
    public function getMacrosCertificate()
    {
    }
    /**
     * Remove all macros, certificate from spreadsheet.
     */
    public function discardMacros() : void
    {
    }
    /**
     * set ribbon XML data.
     *
     * @param null|mixed $target
     * @param null|mixed $xmlData
     */
    public function setRibbonXMLData($target, $xmlData) : void
    {
    }
    /**
     * retrieve ribbon XML Data.
     *
     * @param string $what
     *
     * @return null|array|string
     */
    public function getRibbonXMLData($what = 'all')
    {
    }
    /**
     * store binaries ribbon objects (pictures).
     *
     * @param null|mixed $BinObjectsNames
     * @param null|mixed $BinObjectsData
     */
    public function setRibbonBinObjects($BinObjectsNames, $BinObjectsData) : void
    {
    }
    /**
     * List of unparsed loaded data for export to same format with better compatibility.
     * It has to be minimized when the library start to support currently unparsed data.
     *
     * @internal
     *
     * @return array
     */
    public function getUnparsedLoadedData()
    {
    }
    /**
     * List of unparsed loaded data for export to same format with better compatibility.
     * It has to be minimized when the library start to support currently unparsed data.
     *
     * @internal
     */
    public function setUnparsedLoadedData(array $unparsedLoadedData) : void
    {
    }
    /**
     * retrieve Binaries Ribbon Objects.
     *
     * @param string $what
     *
     * @return null|array
     */
    public function getRibbonBinObjects($what = 'all')
    {
    }
    /**
     * This workbook have a custom UI ?
     *
     * @return bool
     */
    public function hasRibbon()
    {
    }
    /**
     * This workbook have additionnal object for the ribbon ?
     *
     * @return bool
     */
    public function hasRibbonBinObjects()
    {
    }
    /**
     * Check if a sheet with a specified code name already exists.
     *
     * @param string $codeName Name of the worksheet to check
     *
     * @return bool
     */
    public function sheetCodeNameExists($codeName)
    {
    }
    /**
     * Get sheet by code name. Warning : sheet don't have always a code name !
     *
     * @param string $codeName Sheet name
     *
     * @return null|Worksheet
     */
    public function getSheetByCodeName($codeName)
    {
    }
    /**
     * Create a new PhpSpreadsheet with one Worksheet.
     */
    public function __construct()
    {
    }
    /**
     * Code to execute when this worksheet is unset().
     */
    public function __destruct()
    {
    }
    /**
     * Disconnect all worksheets from this PhpSpreadsheet workbook object,
     * typically so that the PhpSpreadsheet object can be unset.
     */
    public function disconnectWorksheets() : void
    {
    }
    /**
     * Return the calculation engine for this worksheet.
     *
     * @return null|Calculation
     */
    public function getCalculationEngine()
    {
    }
    /**
     * Get properties.
     *
     * @return Document\Properties
     */
    public function getProperties()
    {
    }
    /**
     * Set properties.
     */
    public function setProperties(\PhpOffice\PhpSpreadsheet\Document\Properties $documentProperties) : void
    {
    }
    /**
     * Get security.
     *
     * @return Document\Security
     */
    public function getSecurity()
    {
    }
    /**
     * Set security.
     */
    public function setSecurity(\PhpOffice\PhpSpreadsheet\Document\Security $documentSecurity) : void
    {
    }
    /**
     * Get active sheet.
     *
     * @return Worksheet
     */
    public function getActiveSheet()
    {
    }
    /**
     * Create sheet and add it to this workbook.
     *
     * @param null|int $sheetIndex Index where sheet should go (0,1,..., or null for last)
     *
     * @return Worksheet
     */
    public function createSheet($sheetIndex = null)
    {
    }
    /**
     * Check if a sheet with a specified name already exists.
     *
     * @param string $worksheetName Name of the worksheet to check
     *
     * @return bool
     */
    public function sheetNameExists($worksheetName)
    {
    }
    /**
     * Add sheet.
     *
     * @param Worksheet $worksheet The worskeet to add
     * @param null|int $sheetIndex Index where sheet should go (0,1,..., or null for last)
     *
     * @return Worksheet
     */
    public function addSheet(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet, $sheetIndex = null)
    {
    }
    /**
     * Remove sheet by index.
     *
     * @param int $sheetIndex Index position of the worksheet to remove
     */
    public function removeSheetByIndex($sheetIndex) : void
    {
    }
    /**
     * Get sheet by index.
     *
     * @param int $sheetIndex Sheet index
     *
     * @return Worksheet
     */
    public function getSheet($sheetIndex)
    {
    }
    /**
     * Get all sheets.
     *
     * @return Worksheet[]
     */
    public function getAllSheets()
    {
    }
    /**
     * Get sheet by name.
     *
     * @param string $worksheetName Sheet name
     *
     * @return null|Worksheet
     */
    public function getSheetByName($worksheetName)
    {
    }
    /**
     * Get index for sheet.
     *
     * @return int index
     */
    public function getIndex(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet)
    {
    }
    /**
     * Set index for sheet by sheet name.
     *
     * @param string $worksheetName Sheet name to modify index for
     * @param int $newIndexPosition New index for the sheet
     *
     * @return int New sheet index
     */
    public function setIndexByName($worksheetName, $newIndexPosition)
    {
    }
    /**
     * Get sheet count.
     *
     * @return int
     */
    public function getSheetCount()
    {
    }
    /**
     * Get active sheet index.
     *
     * @return int Active sheet index
     */
    public function getActiveSheetIndex()
    {
    }
    /**
     * Set active sheet index.
     *
     * @param int $worksheetIndex Active sheet index
     *
     * @return Worksheet
     */
    public function setActiveSheetIndex($worksheetIndex)
    {
    }
    /**
     * Set active sheet index by name.
     *
     * @param string $worksheetName Sheet title
     *
     * @return Worksheet
     */
    public function setActiveSheetIndexByName($worksheetName)
    {
    }
    /**
     * Get sheet names.
     *
     * @return string[]
     */
    public function getSheetNames()
    {
    }
    /**
     * Add external sheet.
     *
     * @param Worksheet $worksheet External sheet to add
     * @param null|int $sheetIndex Index where sheet should go (0,1,..., or null for last)
     *
     * @return Worksheet
     */
    public function addExternalSheet(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet, $sheetIndex = null)
    {
    }
    /**
     * Get an array of all Named Ranges.
     *
     * @return DefinedName[]
     */
    public function getNamedRanges() : array
    {
    }
    /**
     * Get an array of all Named Formulae.
     *
     * @return DefinedName[]
     */
    public function getNamedFormulae() : array
    {
    }
    /**
     * Get an array of all Defined Names (both named ranges and named formulae).
     *
     * @return DefinedName[]
     */
    public function getDefinedNames() : array
    {
    }
    /**
     * Add a named range.
     * If a named range with this name already exists, then this will replace the existing value.
     */
    public function addNamedRange(\PhpOffice\PhpSpreadsheet\NamedRange $namedRange) : void
    {
    }
    /**
     * Add a named formula.
     * If a named formula with this name already exists, then this will replace the existing value.
     */
    public function addNamedFormula(\PhpOffice\PhpSpreadsheet\NamedFormula $namedFormula) : void
    {
    }
    /**
     * Add a defined name (either a named range or a named formula).
     * If a defined named with this name already exists, then this will replace the existing value.
     */
    public function addDefinedName(\PhpOffice\PhpSpreadsheet\DefinedName $definedName) : void
    {
    }
    /**
     * Get named range.
     *
     * @param null|Worksheet $worksheet Scope. Use null for global scope
     */
    public function getNamedRange(string $namedRange, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null) : ?\PhpOffice\PhpSpreadsheet\NamedRange
    {
    }
    /**
     * Get named formula.
     *
     * @param null|Worksheet $worksheet Scope. Use null for global scope
     */
    public function getNamedFormula(string $namedFormula, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null) : ?\PhpOffice\PhpSpreadsheet\NamedFormula
    {
    }
    /**
     * Get named range.
     *
     * @param null|Worksheet $worksheet Scope. Use null for global scope
     */
    public function getDefinedName(string $definedName, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null) : ?\PhpOffice\PhpSpreadsheet\DefinedName
    {
    }
    /**
     * Remove named range.
     *
     * @param null|Worksheet $worksheet scope: use null for global scope
     *
     * @return $this
     */
    public function removeNamedRange(string $namedRange, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null) : self
    {
    }
    /**
     * Remove named formula.
     *
     * @param null|Worksheet $worksheet scope: use null for global scope
     *
     * @return $this
     */
    public function removeNamedFormula(string $namedFormula, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null) : self
    {
    }
    /**
     * Remove defined name.
     *
     * @param null|Worksheet $worksheet scope: use null for global scope
     *
     * @return $this
     */
    public function removeDefinedName(string $definedName, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null) : self
    {
    }
    /**
     * Get worksheet iterator.
     *
     * @return Iterator
     */
    public function getWorksheetIterator()
    {
    }
    /**
     * Copy workbook (!= clone!).
     *
     * @return Spreadsheet
     */
    public function copy()
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
    /**
     * Get the workbook collection of cellXfs.
     *
     * @return Style[]
     */
    public function getCellXfCollection()
    {
    }
    /**
     * Get cellXf by index.
     *
     * @param int $cellStyleIndex
     *
     * @return Style
     */
    public function getCellXfByIndex($cellStyleIndex)
    {
    }
    /**
     * Get cellXf by hash code.
     *
     * @param string $hashcode
     *
     * @return false|Style
     */
    public function getCellXfByHashCode($hashcode)
    {
    }
    /**
     * Check if style exists in style collection.
     *
     * @param Style $cellStyleIndex
     *
     * @return bool
     */
    public function cellXfExists($cellStyleIndex)
    {
    }
    /**
     * Get default style.
     *
     * @return Style
     */
    public function getDefaultStyle()
    {
    }
    /**
     * Add a cellXf to the workbook.
     */
    public function addCellXf(\PhpOffice\PhpSpreadsheet\Style\Style $style) : void
    {
    }
    /**
     * Remove cellXf by index. It is ensured that all cells get their xf index updated.
     *
     * @param int $cellStyleIndex Index to cellXf
     */
    public function removeCellXfByIndex($cellStyleIndex) : void
    {
    }
    /**
     * Get the cellXf supervisor.
     *
     * @return Style
     */
    public function getCellXfSupervisor()
    {
    }
    /**
     * Get the workbook collection of cellStyleXfs.
     *
     * @return Style[]
     */
    public function getCellStyleXfCollection()
    {
    }
    /**
     * Get cellStyleXf by index.
     *
     * @param int $cellStyleIndex Index to cellXf
     *
     * @return Style
     */
    public function getCellStyleXfByIndex($cellStyleIndex)
    {
    }
    /**
     * Get cellStyleXf by hash code.
     *
     * @param string $hashcode
     *
     * @return false|Style
     */
    public function getCellStyleXfByHashCode($hashcode)
    {
    }
    /**
     * Add a cellStyleXf to the workbook.
     */
    public function addCellStyleXf(\PhpOffice\PhpSpreadsheet\Style\Style $style) : void
    {
    }
    /**
     * Remove cellStyleXf by index.
     *
     * @param int $cellStyleIndex Index to cellXf
     */
    public function removeCellStyleXfByIndex($cellStyleIndex) : void
    {
    }
    /**
     * Eliminate all unneeded cellXf and afterwards update the xfIndex for all cells
     * and columns in the workbook.
     */
    public function garbageCollect() : void
    {
    }
    /**
     * Return the unique ID value assigned to this spreadsheet workbook.
     *
     * @return string
     */
    public function getID()
    {
    }
    /**
     * Get the visibility of the horizonal scroll bar in the application.
     *
     * @return bool True if horizonal scroll bar is visible
     */
    public function getShowHorizontalScroll()
    {
    }
    /**
     * Set the visibility of the horizonal scroll bar in the application.
     *
     * @param bool $showHorizontalScroll True if horizonal scroll bar is visible
     */
    public function setShowHorizontalScroll($showHorizontalScroll) : void
    {
    }
    /**
     * Get the visibility of the vertical scroll bar in the application.
     *
     * @return bool True if vertical scroll bar is visible
     */
    public function getShowVerticalScroll()
    {
    }
    /**
     * Set the visibility of the vertical scroll bar in the application.
     *
     * @param bool $showVerticalScroll True if vertical scroll bar is visible
     */
    public function setShowVerticalScroll($showVerticalScroll) : void
    {
    }
    /**
     * Get the visibility of the sheet tabs in the application.
     *
     * @return bool True if the sheet tabs are visible
     */
    public function getShowSheetTabs()
    {
    }
    /**
     * Set the visibility of the sheet tabs  in the application.
     *
     * @param bool $showSheetTabs True if sheet tabs are visible
     */
    public function setShowSheetTabs($showSheetTabs) : void
    {
    }
    /**
     * Return whether the workbook window is minimized.
     *
     * @return bool true if workbook window is minimized
     */
    public function getMinimized()
    {
    }
    /**
     * Set whether the workbook window is minimized.
     *
     * @param bool $minimized true if workbook window is minimized
     */
    public function setMinimized($minimized) : void
    {
    }
    /**
     * Return whether to group dates when presenting the user with
     * filtering optiomd in the user interface.
     *
     * @return bool true if workbook window is minimized
     */
    public function getAutoFilterDateGrouping()
    {
    }
    /**
     * Set whether to group dates when presenting the user with
     * filtering optiomd in the user interface.
     *
     * @param bool $autoFilterDateGrouping true if workbook window is minimized
     */
    public function setAutoFilterDateGrouping($autoFilterDateGrouping) : void
    {
    }
    /**
     * Return the first sheet in the book view.
     *
     * @return int First sheet in book view
     */
    public function getFirstSheetIndex()
    {
    }
    /**
     * Set the first sheet in the book view.
     *
     * @param int $firstSheetIndex First sheet in book view
     */
    public function setFirstSheetIndex($firstSheetIndex) : void
    {
    }
    /**
     * Return the visibility status of the workbook.
     *
     * This may be one of the following three values:
     * - visibile
     *
     * @return string Visible status
     */
    public function getVisibility()
    {
    }
    /**
     * Set the visibility status of the workbook.
     *
     * Valid values are:
     *  - 'visible' (self::VISIBILITY_VISIBLE):
     *       Workbook window is visible
     *  - 'hidden' (self::VISIBILITY_HIDDEN):
     *       Workbook window is hidden, but can be shown by the user
     *       via the user interface
     *  - 'veryHidden' (self::VISIBILITY_VERY_HIDDEN):
     *       Workbook window is hidden and cannot be shown in the
     *       user interface.
     *
     * @param string $visibility visibility status of the workbook
     */
    public function setVisibility($visibility) : void
    {
    }
    /**
     * Get the ratio between the workbook tabs bar and the horizontal scroll bar.
     * TabRatio is assumed to be out of 1000 of the horizontal window width.
     *
     * @return int Ratio between the workbook tabs bar and the horizontal scroll bar
     */
    public function getTabRatio()
    {
    }
    /**
     * Set the ratio between the workbook tabs bar and the horizontal scroll bar
     * TabRatio is assumed to be out of 1000 of the horizontal window width.
     *
     * @param int $tabRatio Ratio between the tabs bar and the horizontal scroll bar
     */
    public function setTabRatio($tabRatio) : void
    {
    }
}
