<?php

namespace PhpOffice\PhpSpreadsheet\RichText;

class RichText implements \PhpOffice\PhpSpreadsheet\IComparable
{
    /**
     * Create a new RichText instance.
     *
     * @param Cell $pCell
     */
    public function __construct(?\PhpOffice\PhpSpreadsheet\Cell\Cell $pCell = null)
    {
    }
    /**
     * Add text.
     *
     * @param ITextElement $text Rich text element
     *
     * @return $this
     */
    public function addText(\PhpOffice\PhpSpreadsheet\RichText\ITextElement $text)
    {
    }
    /**
     * Create text.
     *
     * @param string $text Text
     *
     * @return TextElement
     */
    public function createText($text)
    {
    }
    /**
     * Create text run.
     *
     * @param string $text Text
     *
     * @return Run
     */
    public function createTextRun($text)
    {
    }
    /**
     * Get plain text.
     *
     * @return string
     */
    public function getPlainText()
    {
    }
    /**
     * Convert to string.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Get Rich Text elements.
     *
     * @return ITextElement[]
     */
    public function getRichTextElements()
    {
    }
    /**
     * Set Rich Text elements.
     *
     * @param ITextElement[] $textElements Array of elements
     *
     * @return $this
     */
    public function setRichTextElements(array $textElements)
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
}
