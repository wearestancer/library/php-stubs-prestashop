<?php

namespace PhpOffice\PhpSpreadsheet\RichText;

class Run extends \PhpOffice\PhpSpreadsheet\RichText\TextElement implements \PhpOffice\PhpSpreadsheet\RichText\ITextElement
{
    /**
     * Create a new Run instance.
     *
     * @param string $text Text
     */
    public function __construct($text = '')
    {
    }
    /**
     * Get font.
     *
     * @return null|\PhpOffice\PhpSpreadsheet\Style\Font
     */
    public function getFont()
    {
    }
    /**
     * Set font.
     *
     * @param Font $font Font
     *
     * @return $this
     */
    public function setFont(?\PhpOffice\PhpSpreadsheet\Style\Font $font = null)
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
}
