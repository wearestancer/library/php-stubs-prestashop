<?php

namespace PhpOffice\PhpSpreadsheet;

class NamedFormula extends \PhpOffice\PhpSpreadsheet\DefinedName
{
    /**
     * Create a new Named Formula.
     */
    public function __construct(string $name, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null, ?string $formula = null, bool $localOnly = false, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $scope = null)
    {
    }
    /**
     * Get the formula value.
     */
    public function getFormula() : string
    {
    }
    /**
     * Set the formula value.
     */
    public function setFormula(string $formula) : self
    {
    }
}
