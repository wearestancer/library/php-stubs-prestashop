<?php

namespace PhpOffice\PhpSpreadsheet\Cell;

class Hyperlink
{
    /**
     * Create a new Hyperlink.
     *
     * @param string $url Url to link the cell to
     * @param string $tooltip Tooltip to display on the hyperlink
     */
    public function __construct($url = '', $tooltip = '')
    {
    }
    /**
     * Get URL.
     *
     * @return string
     */
    public function getUrl()
    {
    }
    /**
     * Set URL.
     *
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
    }
    /**
     * Get tooltip.
     *
     * @return string
     */
    public function getTooltip()
    {
    }
    /**
     * Set tooltip.
     *
     * @param string $tooltip
     *
     * @return $this
     */
    public function setTooltip($tooltip)
    {
    }
    /**
     * Is this hyperlink internal? (to another worksheet).
     *
     * @return bool
     */
    public function isInternal()
    {
    }
    /**
     * @return string
     */
    public function getTypeHyperlink()
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
}
