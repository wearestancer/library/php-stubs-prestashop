<?php

namespace PhpOffice\PhpSpreadsheet\Cell;

class StringValueBinder implements \PhpOffice\PhpSpreadsheet\Cell\IValueBinder
{
    /**
     * @var bool
     */
    protected $convertNull = true;
    /**
     * @var bool
     */
    protected $convertBoolean = true;
    /**
     * @var bool
     */
    protected $convertNumeric = true;
    /**
     * @var bool
     */
    protected $convertFormula = true;
    public function setNullConversion(bool $suppressConversion = false) : self
    {
    }
    public function setBooleanConversion(bool $suppressConversion = false) : self
    {
    }
    public function setNumericConversion(bool $suppressConversion = false) : self
    {
    }
    public function setFormulaConversion(bool $suppressConversion = false) : self
    {
    }
    public function setConversionForAllValueTypes(bool $suppressConversion = false) : self
    {
    }
    /**
     * Bind value to a cell.
     *
     * @param Cell $cell Cell to bind value to
     * @param mixed $value Value to bind in cell
     */
    public function bindValue(\PhpOffice\PhpSpreadsheet\Cell\Cell $cell, $value)
    {
    }
    protected function bindObjectValue(\PhpOffice\PhpSpreadsheet\Cell\Cell $cell, object $value) : bool
    {
    }
}
