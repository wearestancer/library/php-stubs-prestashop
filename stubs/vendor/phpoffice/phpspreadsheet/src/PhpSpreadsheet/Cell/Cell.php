<?php

namespace PhpOffice\PhpSpreadsheet\Cell;

class Cell
{
    /**
     * Update the cell into the cell collection.
     *
     * @return $this
     */
    public function updateInCollection()
    {
    }
    public function detach() : void
    {
    }
    public function attach(\PhpOffice\PhpSpreadsheet\Collection\Cells $parent) : void
    {
    }
    /**
     * Create a new Cell.
     *
     * @param mixed $value
     * @param string $dataType
     */
    public function __construct($value, $dataType, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet)
    {
    }
    /**
     * Get cell coordinate column.
     *
     * @return string
     */
    public function getColumn()
    {
    }
    /**
     * Get cell coordinate row.
     *
     * @return int
     */
    public function getRow()
    {
    }
    /**
     * Get cell coordinate.
     *
     * @return string
     */
    public function getCoordinate()
    {
    }
    /**
     * Get cell value.
     *
     * @return mixed
     */
    public function getValue()
    {
    }
    /**
     * Get cell value with formatting.
     *
     * @return string
     */
    public function getFormattedValue()
    {
    }
    /**
     * Set cell value.
     *
     *    Sets the value for a cell, automatically determining the datatype using the value binder
     *
     * @param mixed $value Value
     *
     * @return $this
     */
    public function setValue($value)
    {
    }
    /**
     * Set the value for a cell, with the explicit data type passed to the method (bypassing any use of the value binder).
     *
     * @param mixed $value Value
     * @param string $dataType Explicit data type, see DataType::TYPE_*
     *
     * @return Cell
     */
    public function setValueExplicit($value, $dataType)
    {
    }
    /**
     * Get calculated cell value.
     *
     * @param bool $resetLog Whether the calculation engine logger should be reset or not
     *
     * @return mixed
     */
    public function getCalculatedValue($resetLog = true)
    {
    }
    /**
     * Set old calculated value (cached).
     *
     * @param mixed $originalValue Value
     *
     * @return Cell
     */
    public function setCalculatedValue($originalValue)
    {
    }
    /**
     *    Get old calculated value (cached)
     *    This returns the value last calculated by MS Excel or whichever spreadsheet program was used to
     *        create the original spreadsheet file.
     *    Note that this value is not guaranteed to reflect the actual calculated value because it is
     *        possible that auto-calculation was disabled in the original spreadsheet, and underlying data
     *        values used by the formula have changed since it was last calculated.
     *
     * @return mixed
     */
    public function getOldCalculatedValue()
    {
    }
    /**
     * Get cell data type.
     *
     * @return string
     */
    public function getDataType()
    {
    }
    /**
     * Set cell data type.
     *
     * @param string $dataType see DataType::TYPE_*
     *
     * @return Cell
     */
    public function setDataType($dataType)
    {
    }
    /**
     * Identify if the cell contains a formula.
     *
     * @return bool
     */
    public function isFormula()
    {
    }
    /**
     *    Does this cell contain Data validation rules?
     *
     * @return bool
     */
    public function hasDataValidation()
    {
    }
    /**
     * Get Data validation rules.
     *
     * @return DataValidation
     */
    public function getDataValidation()
    {
    }
    /**
     * Set Data validation rules.
     *
     * @param DataValidation $dataValidation
     *
     * @return Cell
     */
    public function setDataValidation(?\PhpOffice\PhpSpreadsheet\Cell\DataValidation $dataValidation = null)
    {
    }
    /**
     * Does this cell contain valid value?
     *
     * @return bool
     */
    public function hasValidValue()
    {
    }
    /**
     * Does this cell contain a Hyperlink?
     *
     * @return bool
     */
    public function hasHyperlink()
    {
    }
    /**
     * Get Hyperlink.
     *
     * @return Hyperlink
     */
    public function getHyperlink()
    {
    }
    /**
     * Set Hyperlink.
     *
     * @param Hyperlink $hyperlink
     *
     * @return Cell
     */
    public function setHyperlink(?\PhpOffice\PhpSpreadsheet\Cell\Hyperlink $hyperlink = null)
    {
    }
    /**
     * Get cell collection.
     *
     * @return Cells
     */
    public function getParent()
    {
    }
    /**
     * Get parent worksheet.
     *
     * @return Worksheet
     */
    public function getWorksheet()
    {
    }
    /**
     * Is this cell in a merge range.
     *
     * @return bool
     */
    public function isInMergeRange()
    {
    }
    /**
     * Is this cell the master (top left cell) in a merge range (that holds the actual data value).
     *
     * @return bool
     */
    public function isMergeRangeValueCell()
    {
    }
    /**
     * If this cell is in a merge range, then return the range.
     *
     * @return false|string
     */
    public function getMergeRange()
    {
    }
    /**
     * Get cell style.
     *
     * @return Style
     */
    public function getStyle()
    {
    }
    /**
     * Re-bind parent.
     *
     * @return Cell
     */
    public function rebindParent(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $parent)
    {
    }
    /**
     *    Is cell in a specific range?
     *
     * @param string $range Cell range (e.g. A1:A1)
     *
     * @return bool
     */
    public function isInRange($range)
    {
    }
    /**
     * Compare 2 cells.
     *
     * @param Cell $a Cell a
     * @param Cell $b Cell b
     *
     * @return int Result of comparison (always -1 or 1, never zero!)
     */
    public static function compareCells(self $a, self $b)
    {
    }
    /**
     * Get value binder to use.
     *
     * @return IValueBinder
     */
    public static function getValueBinder()
    {
    }
    /**
     * Set value binder to use.
     */
    public static function setValueBinder(\PhpOffice\PhpSpreadsheet\Cell\IValueBinder $binder) : void
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
    /**
     * Get index to cellXf.
     *
     * @return int
     */
    public function getXfIndex()
    {
    }
    /**
     * Set index to cellXf.
     *
     * @param int $indexValue
     *
     * @return Cell
     */
    public function setXfIndex($indexValue)
    {
    }
    /**
     * Set the formula attributes.
     *
     * @param mixed $attributes
     *
     * @return $this
     */
    public function setFormulaAttributes($attributes)
    {
    }
    /**
     * Get the formula attributes.
     */
    public function getFormulaAttributes()
    {
    }
    /**
     * Convert to string.
     *
     * @return string
     */
    public function __toString()
    {
    }
}
