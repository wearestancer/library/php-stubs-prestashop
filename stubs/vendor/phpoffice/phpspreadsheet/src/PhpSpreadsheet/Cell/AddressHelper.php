<?php

namespace PhpOffice\PhpSpreadsheet\Cell;

class AddressHelper
{
    public const R1C1_COORDINATE_REGEX = '/(R((?:\\[-?\\d*\\])|(?:\\d*))?)(C((?:\\[-?\\d*\\])|(?:\\d*))?)/i';
    /**
     * Converts an R1C1 format cell address to an A1 format cell address.
     */
    public static function convertToA1(string $address, int $currentRowNumber = 1, int $currentColumnNumber = 1) : string
    {
    }
    protected static function convertSpreadsheetMLFormula(string $formula) : string
    {
    }
    /**
     * Converts a formula that uses R1C1/SpreadsheetXML format cell address to an A1 format cell address.
     */
    public static function convertFormulaToA1(string $formula, int $currentRowNumber = 1, int $currentColumnNumber = 1) : string
    {
    }
    /**
     * Converts an A1 format cell address to an R1C1 format cell address.
     * If $currentRowNumber or $currentColumnNumber are provided, then the R1C1 address will be formatted as a relative address.
     */
    public static function convertToR1C1(string $address, ?int $currentRowNumber = null, ?int $currentColumnNumber = null) : string
    {
    }
}
