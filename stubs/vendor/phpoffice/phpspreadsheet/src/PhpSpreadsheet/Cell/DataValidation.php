<?php

namespace PhpOffice\PhpSpreadsheet\Cell;

class DataValidation
{
    // Data validation types
    const TYPE_NONE = 'none';
    const TYPE_CUSTOM = 'custom';
    const TYPE_DATE = 'date';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_LIST = 'list';
    const TYPE_TEXTLENGTH = 'textLength';
    const TYPE_TIME = 'time';
    const TYPE_WHOLE = 'whole';
    // Data validation error styles
    const STYLE_STOP = 'stop';
    const STYLE_WARNING = 'warning';
    const STYLE_INFORMATION = 'information';
    // Data validation operators
    const OPERATOR_BETWEEN = 'between';
    const OPERATOR_EQUAL = 'equal';
    const OPERATOR_GREATERTHAN = 'greaterThan';
    const OPERATOR_GREATERTHANOREQUAL = 'greaterThanOrEqual';
    const OPERATOR_LESSTHAN = 'lessThan';
    const OPERATOR_LESSTHANOREQUAL = 'lessThanOrEqual';
    const OPERATOR_NOTBETWEEN = 'notBetween';
    const OPERATOR_NOTEQUAL = 'notEqual';
    /**
     * Create a new DataValidation.
     */
    public function __construct()
    {
    }
    /**
     * Get Formula 1.
     *
     * @return string
     */
    public function getFormula1()
    {
    }
    /**
     * Set Formula 1.
     *
     * @param string $formula
     *
     * @return $this
     */
    public function setFormula1($formula)
    {
    }
    /**
     * Get Formula 2.
     *
     * @return string
     */
    public function getFormula2()
    {
    }
    /**
     * Set Formula 2.
     *
     * @param string $formula
     *
     * @return $this
     */
    public function setFormula2($formula)
    {
    }
    /**
     * Get Type.
     *
     * @return string
     */
    public function getType()
    {
    }
    /**
     * Set Type.
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
    }
    /**
     * Get Error style.
     *
     * @return string
     */
    public function getErrorStyle()
    {
    }
    /**
     * Set Error style.
     *
     * @param string $errorStyle see self::STYLE_*
     *
     * @return $this
     */
    public function setErrorStyle($errorStyle)
    {
    }
    /**
     * Get Operator.
     *
     * @return string
     */
    public function getOperator()
    {
    }
    /**
     * Set Operator.
     *
     * @param string $operator
     *
     * @return $this
     */
    public function setOperator($operator)
    {
    }
    /**
     * Get Allow Blank.
     *
     * @return bool
     */
    public function getAllowBlank()
    {
    }
    /**
     * Set Allow Blank.
     *
     * @param bool $allowBlank
     *
     * @return $this
     */
    public function setAllowBlank($allowBlank)
    {
    }
    /**
     * Get Show DropDown.
     *
     * @return bool
     */
    public function getShowDropDown()
    {
    }
    /**
     * Set Show DropDown.
     *
     * @param bool $showDropDown
     *
     * @return $this
     */
    public function setShowDropDown($showDropDown)
    {
    }
    /**
     * Get Show InputMessage.
     *
     * @return bool
     */
    public function getShowInputMessage()
    {
    }
    /**
     * Set Show InputMessage.
     *
     * @param bool $showInputMessage
     *
     * @return $this
     */
    public function setShowInputMessage($showInputMessage)
    {
    }
    /**
     * Get Show ErrorMessage.
     *
     * @return bool
     */
    public function getShowErrorMessage()
    {
    }
    /**
     * Set Show ErrorMessage.
     *
     * @param bool $showErrorMessage
     *
     * @return $this
     */
    public function setShowErrorMessage($showErrorMessage)
    {
    }
    /**
     * Get Error title.
     *
     * @return string
     */
    public function getErrorTitle()
    {
    }
    /**
     * Set Error title.
     *
     * @param string $errorTitle
     *
     * @return $this
     */
    public function setErrorTitle($errorTitle)
    {
    }
    /**
     * Get Error.
     *
     * @return string
     */
    public function getError()
    {
    }
    /**
     * Set Error.
     *
     * @param string $error
     *
     * @return $this
     */
    public function setError($error)
    {
    }
    /**
     * Get Prompt title.
     *
     * @return string
     */
    public function getPromptTitle()
    {
    }
    /**
     * Set Prompt title.
     *
     * @param string $promptTitle
     *
     * @return $this
     */
    public function setPromptTitle($promptTitle)
    {
    }
    /**
     * Get Prompt.
     *
     * @return string
     */
    public function getPrompt()
    {
    }
    /**
     * Set Prompt.
     *
     * @param string $prompt
     *
     * @return $this
     */
    public function setPrompt($prompt)
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
    public function getSqref() : ?string
    {
    }
    public function setSqref(?string $str) : self
    {
    }
}
