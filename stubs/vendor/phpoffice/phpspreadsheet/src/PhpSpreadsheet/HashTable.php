<?php

namespace PhpOffice\PhpSpreadsheet;

/**
 * @template T of IComparable
 */
class HashTable
{
    /**
     * HashTable elements.
     *
     * @var array<string, T>
     */
    protected $items = [];
    /**
     * HashTable key map.
     *
     * @var array<int, string>
     */
    protected $keyMap = [];
    /**
     * Create a new HashTable.
     *
     * @param T[] $source Optional source array to create HashTable from
     */
    public function __construct($source = null)
    {
    }
    /**
     * Add HashTable items from source.
     *
     * @param T[] $source Source array to create HashTable from
     */
    public function addFromSource(?array $source = null) : void
    {
    }
    /**
     * Add HashTable item.
     *
     * @param T $source Item to add
     */
    public function add(\PhpOffice\PhpSpreadsheet\IComparable $source) : void
    {
    }
    /**
     * Remove HashTable item.
     *
     * @param T $source Item to remove
     */
    public function remove(\PhpOffice\PhpSpreadsheet\IComparable $source) : void
    {
    }
    /**
     * Clear HashTable.
     */
    public function clear() : void
    {
    }
    /**
     * Count.
     *
     * @return int
     */
    public function count()
    {
    }
    /**
     * Get index for hash code.
     *
     * @return false|int Index
     */
    public function getIndexForHashCode(string $hashCode)
    {
    }
    /**
     * Get by index.
     *
     * @return null|T
     */
    public function getByIndex(int $index)
    {
    }
    /**
     * Get by hashcode.
     *
     * @return null|T
     */
    public function getByHashCode(string $hashCode)
    {
    }
    /**
     * HashTable to array.
     *
     * @return T[]
     */
    public function toArray()
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
}
