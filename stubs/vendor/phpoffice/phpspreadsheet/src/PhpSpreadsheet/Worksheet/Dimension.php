<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

abstract class Dimension
{
    /**
     * Create a new Dimension.
     *
     * @param int $initialValue Numeric row index
     */
    public function __construct($initialValue = null)
    {
    }
    /**
     * Get Visible.
     */
    public function getVisible() : bool
    {
    }
    /**
     * Set Visible.
     *
     * @return $this
     */
    public function setVisible(bool $visible)
    {
    }
    /**
     * Get Outline Level.
     */
    public function getOutlineLevel() : int
    {
    }
    /**
     * Set Outline Level.
     * Value must be between 0 and 7.
     *
     * @return $this
     */
    public function setOutlineLevel(int $level)
    {
    }
    /**
     * Get Collapsed.
     */
    public function getCollapsed() : bool
    {
    }
    /**
     * Set Collapsed.
     *
     * @return $this
     */
    public function setCollapsed(bool $collapsed)
    {
    }
    /**
     * Get index to cellXf.
     *
     * @return int
     */
    public function getXfIndex() : ?int
    {
    }
    /**
     * Set index to cellXf.
     *
     * @return $this
     */
    public function setXfIndex(int $pValue)
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
}
