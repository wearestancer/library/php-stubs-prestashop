<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class Protection
{
    const ALGORITHM_MD2 = 'MD2';
    const ALGORITHM_MD4 = 'MD4';
    const ALGORITHM_MD5 = 'MD5';
    const ALGORITHM_SHA_1 = 'SHA-1';
    const ALGORITHM_SHA_256 = 'SHA-256';
    const ALGORITHM_SHA_384 = 'SHA-384';
    const ALGORITHM_SHA_512 = 'SHA-512';
    const ALGORITHM_RIPEMD_128 = 'RIPEMD-128';
    const ALGORITHM_RIPEMD_160 = 'RIPEMD-160';
    const ALGORITHM_WHIRLPOOL = 'WHIRLPOOL';
    /**
     * Create a new Protection.
     */
    public function __construct()
    {
    }
    /**
     * Is some sort of protection enabled?
     *
     * @return bool
     */
    public function isProtectionEnabled()
    {
    }
    /**
     * Get Sheet.
     *
     * @return bool
     */
    public function getSheet()
    {
    }
    /**
     * Set Sheet.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setSheet($pValue)
    {
    }
    /**
     * Get Objects.
     *
     * @return bool
     */
    public function getObjects()
    {
    }
    /**
     * Set Objects.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setObjects($pValue)
    {
    }
    /**
     * Get Scenarios.
     *
     * @return bool
     */
    public function getScenarios()
    {
    }
    /**
     * Set Scenarios.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setScenarios($pValue)
    {
    }
    /**
     * Get FormatCells.
     *
     * @return bool
     */
    public function getFormatCells()
    {
    }
    /**
     * Set FormatCells.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setFormatCells($pValue)
    {
    }
    /**
     * Get FormatColumns.
     *
     * @return bool
     */
    public function getFormatColumns()
    {
    }
    /**
     * Set FormatColumns.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setFormatColumns($pValue)
    {
    }
    /**
     * Get FormatRows.
     *
     * @return bool
     */
    public function getFormatRows()
    {
    }
    /**
     * Set FormatRows.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setFormatRows($pValue)
    {
    }
    /**
     * Get InsertColumns.
     *
     * @return bool
     */
    public function getInsertColumns()
    {
    }
    /**
     * Set InsertColumns.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setInsertColumns($pValue)
    {
    }
    /**
     * Get InsertRows.
     *
     * @return bool
     */
    public function getInsertRows()
    {
    }
    /**
     * Set InsertRows.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setInsertRows($pValue)
    {
    }
    /**
     * Get InsertHyperlinks.
     *
     * @return bool
     */
    public function getInsertHyperlinks()
    {
    }
    /**
     * Set InsertHyperlinks.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setInsertHyperlinks($pValue)
    {
    }
    /**
     * Get DeleteColumns.
     *
     * @return bool
     */
    public function getDeleteColumns()
    {
    }
    /**
     * Set DeleteColumns.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setDeleteColumns($pValue)
    {
    }
    /**
     * Get DeleteRows.
     *
     * @return bool
     */
    public function getDeleteRows()
    {
    }
    /**
     * Set DeleteRows.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setDeleteRows($pValue)
    {
    }
    /**
     * Get SelectLockedCells.
     *
     * @return bool
     */
    public function getSelectLockedCells()
    {
    }
    /**
     * Set SelectLockedCells.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setSelectLockedCells($pValue)
    {
    }
    /**
     * Get Sort.
     *
     * @return bool
     */
    public function getSort()
    {
    }
    /**
     * Set Sort.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setSort($pValue)
    {
    }
    /**
     * Get AutoFilter.
     *
     * @return bool
     */
    public function getAutoFilter()
    {
    }
    /**
     * Set AutoFilter.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setAutoFilter($pValue)
    {
    }
    /**
     * Get PivotTables.
     *
     * @return bool
     */
    public function getPivotTables()
    {
    }
    /**
     * Set PivotTables.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setPivotTables($pValue)
    {
    }
    /**
     * Get SelectUnlockedCells.
     *
     * @return bool
     */
    public function getSelectUnlockedCells()
    {
    }
    /**
     * Set SelectUnlockedCells.
     *
     * @param bool $pValue
     *
     * @return $this
     */
    public function setSelectUnlockedCells($pValue)
    {
    }
    /**
     * Get hashed password.
     *
     * @return string
     */
    public function getPassword()
    {
    }
    /**
     * Set Password.
     *
     * @param string $pValue
     * @param bool $pAlreadyHashed If the password has already been hashed, set this to true
     *
     * @return $this
     */
    public function setPassword($pValue, $pAlreadyHashed = false)
    {
    }
    /**
     * Get algorithm name.
     */
    public function getAlgorithm() : string
    {
    }
    /**
     * Set algorithm name.
     */
    public function setAlgorithm(string $algorithm) : void
    {
    }
    /**
     * Get salt value.
     */
    public function getSalt() : string
    {
    }
    /**
     * Set salt value.
     */
    public function setSalt(string $salt) : void
    {
    }
    /**
     * Get spin count.
     */
    public function getSpinCount() : int
    {
    }
    /**
     * Set spin count.
     */
    public function setSpinCount(int $spinCount) : void
    {
    }
    /**
     * Verify that the given non-hashed password can "unlock" the protection.
     */
    public function verify(string $password) : bool
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
}
