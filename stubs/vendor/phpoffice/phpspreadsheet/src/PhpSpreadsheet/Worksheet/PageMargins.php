<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class PageMargins
{
    /**
     * Create a new PageMargins.
     */
    public function __construct()
    {
    }
    /**
     * Get Left.
     *
     * @return float
     */
    public function getLeft()
    {
    }
    /**
     * Set Left.
     *
     * @param float $pValue
     *
     * @return $this
     */
    public function setLeft($pValue)
    {
    }
    /**
     * Get Right.
     *
     * @return float
     */
    public function getRight()
    {
    }
    /**
     * Set Right.
     *
     * @param float $pValue
     *
     * @return $this
     */
    public function setRight($pValue)
    {
    }
    /**
     * Get Top.
     *
     * @return float
     */
    public function getTop()
    {
    }
    /**
     * Set Top.
     *
     * @param float $pValue
     *
     * @return $this
     */
    public function setTop($pValue)
    {
    }
    /**
     * Get Bottom.
     *
     * @return float
     */
    public function getBottom()
    {
    }
    /**
     * Set Bottom.
     *
     * @param float $pValue
     *
     * @return $this
     */
    public function setBottom($pValue)
    {
    }
    /**
     * Get Header.
     *
     * @return float
     */
    public function getHeader()
    {
    }
    /**
     * Set Header.
     *
     * @param float $pValue
     *
     * @return $this
     */
    public function setHeader($pValue)
    {
    }
    /**
     * Get Footer.
     *
     * @return float
     */
    public function getFooter()
    {
    }
    /**
     * Set Footer.
     *
     * @param float $pValue
     *
     * @return $this
     */
    public function setFooter($pValue)
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
    public static function fromCentimeters(float $value) : float
    {
    }
    public static function toCentimeters(float $value) : float
    {
    }
    public static function fromMillimeters(float $value) : float
    {
    }
    public static function toMillimeters(float $value) : float
    {
    }
    public static function fromPoints(float $value) : float
    {
    }
    public static function toPoints(float $value) : float
    {
    }
}
