<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class RowDimension extends \PhpOffice\PhpSpreadsheet\Worksheet\Dimension
{
    /**
     * Create a new RowDimension.
     *
     * @param int $pIndex Numeric row index
     */
    public function __construct($pIndex = 0)
    {
    }
    /**
     * Get Row Index.
     */
    public function getRowIndex() : int
    {
    }
    /**
     * Set Row Index.
     *
     * @return $this
     */
    public function setRowIndex(int $index)
    {
    }
    /**
     * Get Row Height.
     * By default, this will be in points; but this method accepts a unit of measure
     *    argument, and will convert the value to the specified UoM.
     *
     * @return float
     */
    public function getRowHeight(?string $unitOfMeasure = null)
    {
    }
    /**
     * Set Row Height.
     *
     * @param float $height in points
     * By default, this will be the passed argument value; but this method accepts a unit of measure
     *    argument, and will convert the passed argument value to points from the specified UoM
     *
     * @return $this
     */
    public function setRowHeight($height, ?string $unitOfMeasure = null)
    {
    }
    /**
     * Get ZeroHeight.
     */
    public function getZeroHeight() : bool
    {
    }
    /**
     * Set ZeroHeight.
     *
     * @return $this
     */
    public function setZeroHeight(bool $pValue)
    {
    }
}
