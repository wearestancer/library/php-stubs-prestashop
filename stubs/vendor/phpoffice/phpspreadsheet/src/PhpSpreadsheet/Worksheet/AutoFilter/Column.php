<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter;

class Column
{
    const AUTOFILTER_FILTERTYPE_FILTER = 'filters';
    const AUTOFILTER_FILTERTYPE_CUSTOMFILTER = 'customFilters';
    //    Supports no more than 2 rules, with an And/Or join criteria
    //        if more than 1 rule is defined
    const AUTOFILTER_FILTERTYPE_DYNAMICFILTER = 'dynamicFilter';
    //    Even though the filter rule is constant, the filtered data can vary
    //        e.g. filtered by date = TODAY
    const AUTOFILTER_FILTERTYPE_TOPTENFILTER = 'top10';
    // Multiple Rule Connections
    const AUTOFILTER_COLUMN_JOIN_AND = 'and';
    const AUTOFILTER_COLUMN_JOIN_OR = 'or';
    /**
     * Create a new Column.
     *
     * @param string $pColumn Column (e.g. A)
     * @param AutoFilter $pParent Autofilter for this column
     */
    public function __construct($pColumn, ?\PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter $pParent = null)
    {
    }
    /**
     * Get AutoFilter column index as string eg: 'A'.
     *
     * @return string
     */
    public function getColumnIndex()
    {
    }
    /**
     * Set AutoFilter column index as string eg: 'A'.
     *
     * @param string $pColumn Column (e.g. A)
     *
     * @return $this
     */
    public function setColumnIndex($pColumn)
    {
    }
    /**
     * Get this Column's AutoFilter Parent.
     *
     * @return null|AutoFilter
     */
    public function getParent()
    {
    }
    /**
     * Set this Column's AutoFilter Parent.
     *
     * @param AutoFilter $pParent
     *
     * @return $this
     */
    public function setParent(?\PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter $pParent = null)
    {
    }
    /**
     * Get AutoFilter Type.
     *
     * @return string
     */
    public function getFilterType()
    {
    }
    /**
     * Set AutoFilter Type.
     *
     * @param string $pFilterType
     *
     * @return $this
     */
    public function setFilterType($pFilterType)
    {
    }
    /**
     * Get AutoFilter Multiple Rules And/Or Join.
     *
     * @return string
     */
    public function getJoin()
    {
    }
    /**
     * Set AutoFilter Multiple Rules And/Or.
     *
     * @param string $pJoin And/Or
     *
     * @return $this
     */
    public function setJoin($pJoin)
    {
    }
    /**
     * Set AutoFilter Attributes.
     *
     * @param mixed[] $attributes
     *
     * @return $this
     */
    public function setAttributes($attributes)
    {
    }
    /**
     * Set An AutoFilter Attribute.
     *
     * @param string $pName Attribute Name
     * @param string $pValue Attribute Value
     *
     * @return $this
     */
    public function setAttribute($pName, $pValue)
    {
    }
    /**
     * Get AutoFilter Column Attributes.
     *
     * @return int[]|string[]
     */
    public function getAttributes()
    {
    }
    /**
     * Get specific AutoFilter Column Attribute.
     *
     * @param string $pName Attribute Name
     *
     * @return null|int|string
     */
    public function getAttribute($pName)
    {
    }
    public function ruleCount() : int
    {
    }
    /**
     * Get all AutoFilter Column Rules.
     *
     * @return Column\Rule[]
     */
    public function getRules()
    {
    }
    /**
     * Get a specified AutoFilter Column Rule.
     *
     * @param int $pIndex Rule index in the ruleset array
     *
     * @return Column\Rule
     */
    public function getRule($pIndex)
    {
    }
    /**
     * Create a new AutoFilter Column Rule in the ruleset.
     *
     * @return Column\Rule
     */
    public function createRule()
    {
    }
    /**
     * Add a new AutoFilter Column Rule to the ruleset.
     *
     * @return $this
     */
    public function addRule(\PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule $pRule)
    {
    }
    /**
     * Delete a specified AutoFilter Column Rule
     * If the number of rules is reduced to 1, then we reset And/Or logic to Or.
     *
     * @param int $pIndex Rule index in the ruleset array
     *
     * @return $this
     */
    public function deleteRule($pIndex)
    {
    }
    /**
     * Delete all AutoFilter Column Rules.
     *
     * @return $this
     */
    public function clearRules()
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
}
