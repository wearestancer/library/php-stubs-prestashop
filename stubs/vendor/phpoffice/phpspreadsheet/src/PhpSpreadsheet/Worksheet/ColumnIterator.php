<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class ColumnIterator implements \Iterator
{
    /**
     * Create a new column iterator.
     *
     * @param Worksheet $worksheet The worksheet to iterate over
     * @param string $startColumn The column address at which to start iterating
     * @param string $endColumn Optionally, the column address at which to stop iterating
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet, $startColumn = 'A', $endColumn = null)
    {
    }
    /**
     * Destructor.
     */
    public function __destruct()
    {
    }
    /**
     * (Re)Set the start column and the current column pointer.
     *
     * @param string $startColumn The column address at which to start iterating
     *
     * @return $this
     */
    public function resetStart(string $startColumn = 'A')
    {
    }
    /**
     * (Re)Set the end column.
     *
     * @param string $endColumn The column address at which to stop iterating
     *
     * @return $this
     */
    public function resetEnd($endColumn = null)
    {
    }
    /**
     * Set the column pointer to the selected column.
     *
     * @param string $column The column address to set the current pointer at
     *
     * @return $this
     */
    public function seek(string $column = 'A')
    {
    }
    /**
     * Rewind the iterator to the starting column.
     */
    public function rewind() : void
    {
    }
    /**
     * Return the current column in this worksheet.
     */
    public function current() : \PhpOffice\PhpSpreadsheet\Worksheet\Column
    {
    }
    /**
     * Return the current iterator key.
     */
    public function key() : string
    {
    }
    /**
     * Set the iterator to its next value.
     */
    public function next() : void
    {
    }
    /**
     * Set the iterator to its previous value.
     */
    public function prev() : void
    {
    }
    /**
     * Indicate if more columns exist in the worksheet range of columns that we're iterating.
     */
    public function valid() : bool
    {
    }
}
