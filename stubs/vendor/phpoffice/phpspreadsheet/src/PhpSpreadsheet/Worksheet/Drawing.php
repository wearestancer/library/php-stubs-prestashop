<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class Drawing extends \PhpOffice\PhpSpreadsheet\Worksheet\BaseDrawing
{
    /**
     * Create a new Drawing.
     */
    public function __construct()
    {
    }
    /**
     * Get Filename.
     *
     * @return string
     */
    public function getFilename()
    {
    }
    /**
     * Get indexed filename (using image index).
     *
     * @return string
     */
    public function getIndexedFilename()
    {
    }
    /**
     * Get Extension.
     *
     * @return string
     */
    public function getExtension()
    {
    }
    /**
     * Get Path.
     *
     * @return string
     */
    public function getPath()
    {
    }
    /**
     * Set Path.
     *
     * @param string $pValue File path
     * @param bool $pVerifyFile Verify file
     *
     * @return $this
     */
    public function setPath($pValue, $pVerifyFile = true)
    {
    }
    /**
     * Get isURL.
     */
    public function getIsURL() : bool
    {
    }
    /**
     * Set isURL.
     *
     * @return $this
     */
    public function setIsURL(bool $isUrl) : self
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
}
