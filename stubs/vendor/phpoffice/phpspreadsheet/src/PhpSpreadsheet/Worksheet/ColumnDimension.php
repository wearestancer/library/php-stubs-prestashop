<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class ColumnDimension extends \PhpOffice\PhpSpreadsheet\Worksheet\Dimension
{
    /**
     * Create a new ColumnDimension.
     *
     * @param string $pIndex Character column index
     */
    public function __construct($pIndex = 'A')
    {
    }
    /**
     * Get column index as string eg: 'A'.
     */
    public function getColumnIndex() : string
    {
    }
    /**
     * Set column index as string eg: 'A'.
     *
     * @return $this
     */
    public function setColumnIndex(string $index)
    {
    }
    /**
     * Get Width.
     *
     * Each unit of column width is equal to the width of one character in the default font size.
     * By default, this will be the return value; but this method also accepts a unit of measure argument and will
     *     return the value converted to the specified UoM using an approximation method.
     */
    public function getWidth(?string $unitOfMeasure = null) : float
    {
    }
    /**
     * Set Width.
     *
     * Each unit of column width is equal to the width of one character in the default font size.
     * By default, this will be the unit of measure for the passed value; but this method accepts a unit of measure
     *    argument, and will convert the value from the specified UoM using an approximation method.
     *
     * @return $this
     */
    public function setWidth(float $width, ?string $unitOfMeasure = null)
    {
    }
    /**
     * Get Auto Size.
     */
    public function getAutoSize() : bool
    {
    }
    /**
     * Set Auto Size.
     *
     * @return $this
     */
    public function setAutoSize(bool $autosizeEnabled)
    {
    }
}
