<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class Iterator implements \Iterator
{
    /**
     * Create a new worksheet iterator.
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $subject)
    {
    }
    /**
     * Rewind iterator.
     */
    public function rewind() : void
    {
    }
    /**
     * Current Worksheet.
     */
    public function current() : \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
    {
    }
    /**
     * Current key.
     */
    public function key() : int
    {
    }
    /**
     * Next value.
     */
    public function next() : void
    {
    }
    /**
     * Are there more Worksheet instances available?
     */
    public function valid() : bool
    {
    }
}
