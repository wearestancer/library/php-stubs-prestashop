<?php

namespace PhpOffice\PhpSpreadsheet\Worksheet;

class RowIterator implements \Iterator
{
    /**
     * Create a new row iterator.
     *
     * @param Worksheet $subject The worksheet to iterate over
     * @param int $startRow The row number at which to start iterating
     * @param int $endRow Optionally, the row number at which to stop iterating
     */
    public function __construct(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $subject, $startRow = 1, $endRow = null)
    {
    }
    /**
     * (Re)Set the start row and the current row pointer.
     *
     * @param int $startRow The row number at which to start iterating
     *
     * @return $this
     */
    public function resetStart(int $startRow = 1)
    {
    }
    /**
     * (Re)Set the end row.
     *
     * @param int $endRow The row number at which to stop iterating
     *
     * @return $this
     */
    public function resetEnd($endRow = null)
    {
    }
    /**
     * Set the row pointer to the selected row.
     *
     * @param int $row The row number to set the current pointer at
     *
     * @return $this
     */
    public function seek(int $row = 1)
    {
    }
    /**
     * Rewind the iterator to the starting row.
     */
    public function rewind() : void
    {
    }
    /**
     * Return the current row in this worksheet.
     */
    public function current() : \PhpOffice\PhpSpreadsheet\Worksheet\Row
    {
    }
    /**
     * Return the current iterator key.
     */
    public function key() : int
    {
    }
    /**
     * Set the iterator to its next value.
     */
    public function next() : void
    {
    }
    /**
     * Set the iterator to its previous value.
     */
    public function prev() : void
    {
    }
    /**
     * Indicate if more rows exist in the worksheet range of rows that we're iterating.
     */
    public function valid() : bool
    {
    }
}
