<?php

namespace PhpOffice\PhpSpreadsheet\Style;

class Color extends \PhpOffice\PhpSpreadsheet\Style\Supervisor
{
    const NAMED_COLORS = ['Black', 'White', 'Red', 'Green', 'Blue', 'Yellow', 'Magenta', 'Cyan'];
    // Colors
    const COLOR_BLACK = 'FF000000';
    const COLOR_WHITE = 'FFFFFFFF';
    const COLOR_RED = 'FFFF0000';
    const COLOR_DARKRED = 'FF800000';
    const COLOR_BLUE = 'FF0000FF';
    const COLOR_DARKBLUE = 'FF000080';
    const COLOR_GREEN = 'FF00FF00';
    const COLOR_DARKGREEN = 'FF008000';
    const COLOR_YELLOW = 'FFFFFF00';
    const COLOR_DARKYELLOW = 'FF808000';
    const VALIDATE_ARGB_SIZE = 8;
    const VALIDATE_RGB_SIZE = 6;
    const VALIDATE_COLOR_VALUE = '/^[A-F0-9]{%d}$/i';
    /**
     * Indexed colors array.
     *
     * @var array
     */
    protected static $indexedColors;
    /**
     * ARGB - Alpha RGB.
     *
     * @var null|string
     */
    protected $argb;
    /**
     * Create a new Color.
     *
     * @param string $colorValue ARGB value for the colour, or named colour
     * @param bool $isSupervisor Flag indicating if this is a supervisor or not
     *                                    Leave this value at default unless you understand exactly what
     *                                        its ramifications are
     * @param bool $isConditional Flag indicating if this is a conditional style or not
     *                                    Leave this value at default unless you understand exactly what
     *                                        its ramifications are
     */
    public function __construct($colorValue = self::COLOR_BLACK, $isSupervisor = false, $isConditional = false)
    {
    }
    /**
     * Get the shared style component for the currently active cell in currently active sheet.
     * Only used for style supervisor.
     *
     * @return Color
     */
    public function getSharedComponent()
    {
    }
    /**
     * Build style array from subcomponents.
     *
     * @param array $array
     *
     * @return array
     */
    public function getStyleArray($array)
    {
    }
    /**
     * Apply styles from array.
     *
     * <code>
     * $spreadsheet->getActiveSheet()->getStyle('B2')->getFont()->getColor()->applyFromArray(['rgb' => '808080']);
     * </code>
     *
     * @param array $styleArray Array containing style information
     *
     * @return $this
     */
    public function applyFromArray(array $styleArray)
    {
    }
    /**
     * Get ARGB.
     */
    public function getARGB() : ?string
    {
    }
    /**
     * Set ARGB.
     *
     * @param string $colorValue  ARGB value, or a named color
     *
     * @return $this
     */
    public function setARGB(?string $colorValue = self::COLOR_BLACK)
    {
    }
    /**
     * Get RGB.
     */
    public function getRGB() : string
    {
    }
    /**
     * Set RGB.
     *
     * @param string $colorValue RGB value, or a named color
     *
     * @return $this
     */
    public function setRGB(?string $colorValue = self::COLOR_BLACK)
    {
    }
    /**
     * Get the red colour component of an RGB value.
     *
     * @param string $rgbValue The colour as an RGB value (e.g. FF00CCCC or CCDDEE
     * @param bool $hex Flag indicating whether the component should be returned as a hex or a
     *                                    decimal value
     *
     * @return int|string The red colour component
     */
    public static function getRed($rgbValue, $hex = true)
    {
    }
    /**
     * Get the green colour component of an RGB value.
     *
     * @param string $rgbValue The colour as an RGB value (e.g. FF00CCCC or CCDDEE
     * @param bool $hex Flag indicating whether the component should be returned as a hex or a
     *                                    decimal value
     *
     * @return int|string The green colour component
     */
    public static function getGreen($rgbValue, $hex = true)
    {
    }
    /**
     * Get the blue colour component of an RGB value.
     *
     * @param string $rgbValue The colour as an RGB value (e.g. FF00CCCC or CCDDEE
     * @param bool $hex Flag indicating whether the component should be returned as a hex or a
     *                                    decimal value
     *
     * @return int|string The blue colour component
     */
    public static function getBlue($rgbValue, $hex = true)
    {
    }
    /**
     * Adjust the brightness of a color.
     *
     * @param string $hexColourValue The colour as an RGBA or RGB value (e.g. FF00CCCC or CCDDEE)
     * @param float $adjustPercentage The percentage by which to adjust the colour as a float from -1 to 1
     *
     * @return string The adjusted colour as an RGBA or RGB value (e.g. FF00CCCC or CCDDEE)
     */
    public static function changeBrightness($hexColourValue, $adjustPercentage)
    {
    }
    /**
     * Get indexed color.
     *
     * @param int $colorIndex Index entry point into the colour array
     * @param bool $background Flag to indicate whether default background or foreground colour
     *                                            should be returned if the indexed colour doesn't exist
     *
     * @return Color
     */
    public static function indexedColor($colorIndex, $background = false) : self
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode() : string
    {
    }
    protected function exportArray1() : array
    {
    }
}
