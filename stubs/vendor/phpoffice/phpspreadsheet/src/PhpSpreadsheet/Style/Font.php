<?php

namespace PhpOffice\PhpSpreadsheet\Style;

class Font extends \PhpOffice\PhpSpreadsheet\Style\Supervisor
{
    // Underline types
    const UNDERLINE_NONE = 'none';
    const UNDERLINE_DOUBLE = 'double';
    const UNDERLINE_DOUBLEACCOUNTING = 'doubleAccounting';
    const UNDERLINE_SINGLE = 'single';
    const UNDERLINE_SINGLEACCOUNTING = 'singleAccounting';
    /**
     * Font Name.
     *
     * @var null|string
     */
    protected $name = 'Calibri';
    /**
     * Font Size.
     *
     * @var null|float
     */
    protected $size = 11;
    /**
     * Bold.
     *
     * @var null|bool
     */
    protected $bold = false;
    /**
     * Italic.
     *
     * @var null|bool
     */
    protected $italic = false;
    /**
     * Superscript.
     *
     * @var null|bool
     */
    protected $superscript = false;
    /**
     * Subscript.
     *
     * @var null|bool
     */
    protected $subscript = false;
    /**
     * Underline.
     *
     * @var null|string
     */
    protected $underline = self::UNDERLINE_NONE;
    /**
     * Strikethrough.
     *
     * @var null|bool
     */
    protected $strikethrough = false;
    /**
     * Foreground color.
     *
     * @var Color
     */
    protected $color;
    /**
     * @var null|int
     */
    public $colorIndex;
    /**
     * Create a new Font.
     *
     * @param bool $isSupervisor Flag indicating if this is a supervisor or not
     *                                    Leave this value at default unless you understand exactly what
     *                                        its ramifications are
     * @param bool $isConditional Flag indicating if this is a conditional style or not
     *                                    Leave this value at default unless you understand exactly what
     *                                        its ramifications are
     */
    public function __construct($isSupervisor = false, $isConditional = false)
    {
    }
    /**
     * Get the shared style component for the currently active cell in currently active sheet.
     * Only used for style supervisor.
     *
     * @return Font
     */
    public function getSharedComponent()
    {
    }
    /**
     * Build style array from subcomponents.
     *
     * @param array $array
     *
     * @return array
     */
    public function getStyleArray($array)
    {
    }
    /**
     * Apply styles from array.
     *
     * <code>
     * $spreadsheet->getActiveSheet()->getStyle('B2')->getFont()->applyFromArray(
     *     [
     *         'name' => 'Arial',
     *         'bold' => TRUE,
     *         'italic' => FALSE,
     *         'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE,
     *         'strikethrough' => FALSE,
     *         'color' => [
     *             'rgb' => '808080'
     *         ]
     *     ]
     * );
     * </code>
     *
     * @param array $styleArray Array containing style information
     *
     * @return $this
     */
    public function applyFromArray(array $styleArray)
    {
    }
    /**
     * Get Name.
     *
     * @return null|string
     */
    public function getName()
    {
    }
    /**
     * Set Name.
     *
     * @param string $fontname
     *
     * @return $this
     */
    public function setName($fontname)
    {
    }
    /**
     * Get Size.
     *
     * @return null|float
     */
    public function getSize()
    {
    }
    /**
     * Set Size.
     *
     * @param mixed $sizeInPoints A float representing the value of a positive measurement in points (1/72 of an inch)
     *
     * @return $this
     */
    public function setSize($sizeInPoints)
    {
    }
    /**
     * Get Bold.
     *
     * @return null|bool
     */
    public function getBold()
    {
    }
    /**
     * Set Bold.
     *
     * @param bool $bold
     *
     * @return $this
     */
    public function setBold($bold)
    {
    }
    /**
     * Get Italic.
     *
     * @return null|bool
     */
    public function getItalic()
    {
    }
    /**
     * Set Italic.
     *
     * @param bool $italic
     *
     * @return $this
     */
    public function setItalic($italic)
    {
    }
    /**
     * Get Superscript.
     *
     * @return null|bool
     */
    public function getSuperscript()
    {
    }
    /**
     * Set Superscript.
     *
     * @return $this
     */
    public function setSuperscript(bool $superscript)
    {
    }
    /**
     * Get Subscript.
     *
     * @return null|bool
     */
    public function getSubscript()
    {
    }
    /**
     * Set Subscript.
     *
     * @return $this
     */
    public function setSubscript(bool $subscript)
    {
    }
    /**
     * Get Underline.
     *
     * @return null|string
     */
    public function getUnderline()
    {
    }
    /**
     * Set Underline.
     *
     * @param bool|string $underlineStyle \PhpOffice\PhpSpreadsheet\Style\Font underline type
     *                                    If a boolean is passed, then TRUE equates to UNDERLINE_SINGLE,
     *                                        false equates to UNDERLINE_NONE
     *
     * @return $this
     */
    public function setUnderline($underlineStyle)
    {
    }
    /**
     * Get Strikethrough.
     *
     * @return null|bool
     */
    public function getStrikethrough()
    {
    }
    /**
     * Set Strikethrough.
     *
     * @param bool $strikethru
     *
     * @return $this
     */
    public function setStrikethrough($strikethru)
    {
    }
    /**
     * Get Color.
     *
     * @return Color
     */
    public function getColor()
    {
    }
    /**
     * Set Color.
     *
     * @return $this
     */
    public function setColor(\PhpOffice\PhpSpreadsheet\Style\Color $color)
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
    protected function exportArray1() : array
    {
    }
}
