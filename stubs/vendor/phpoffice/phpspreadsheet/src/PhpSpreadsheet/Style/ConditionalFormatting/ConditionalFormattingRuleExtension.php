<?php

namespace PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting;

class ConditionalFormattingRuleExtension
{
    const CONDITION_EXTENSION_DATABAR = 'dataBar';
    /**
     * ConditionalFormattingRuleExtension constructor.
     */
    public function __construct($id = null, string $cfRule = self::CONDITION_EXTENSION_DATABAR)
    {
    }
    public static function parseExtLstXml($extLstXml)
    {
    }
    /**
     * @return mixed
     */
    public function getId()
    {
    }
    /**
     * @param mixed $id
     */
    public function setId($id) : self
    {
    }
    public function getCfRule() : string
    {
    }
    public function setCfRule(string $cfRule) : self
    {
    }
    public function getDataBarExt() : \PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalDataBarExtension
    {
    }
    public function setDataBarExt(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalDataBarExtension $dataBar) : self
    {
    }
    public function getSqref() : string
    {
    }
    public function setSqref(string $sqref) : self
    {
    }
}
