<?php

namespace PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting;

class ConditionalDataBarExtension
{
    public function getXmlAttributes()
    {
    }
    public function getXmlElements()
    {
    }
    /**
     * @return int
     */
    public function getMinLength()
    {
    }
    public function setMinLength(int $minLength) : self
    {
    }
    /**
     * @return int
     */
    public function getMaxLength()
    {
    }
    public function setMaxLength(int $maxLength) : self
    {
    }
    /**
     * @return null|bool
     */
    public function getBorder()
    {
    }
    public function setBorder(bool $border) : self
    {
    }
    /**
     * @return null|bool
     */
    public function getGradient()
    {
    }
    public function setGradient(bool $gradient) : self
    {
    }
    /**
     * @return string
     */
    public function getDirection()
    {
    }
    public function setDirection(string $direction) : self
    {
    }
    /**
     * @return null|bool
     */
    public function getNegativeBarBorderColorSameAsPositive()
    {
    }
    public function setNegativeBarBorderColorSameAsPositive(bool $negativeBarBorderColorSameAsPositive) : self
    {
    }
    /**
     * @return string
     */
    public function getAxisPosition()
    {
    }
    public function setAxisPosition(string $axisPosition) : self
    {
    }
    /**
     * @return ConditionalFormatValueObject
     */
    public function getMaximumConditionalFormatValueObject()
    {
    }
    public function setMaximumConditionalFormatValueObject(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalFormatValueObject $maximumConditionalFormatValueObject)
    {
    }
    /**
     * @return ConditionalFormatValueObject
     */
    public function getMinimumConditionalFormatValueObject()
    {
    }
    public function setMinimumConditionalFormatValueObject(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalFormatValueObject $minimumConditionalFormatValueObject)
    {
    }
    /**
     * @return string
     */
    public function getBorderColor()
    {
    }
    public function setBorderColor(string $borderColor) : self
    {
    }
    /**
     * @return string
     */
    public function getNegativeFillColor()
    {
    }
    public function setNegativeFillColor(string $negativeFillColor) : self
    {
    }
    /**
     * @return string
     */
    public function getNegativeBorderColor()
    {
    }
    public function setNegativeBorderColor(string $negativeBorderColor) : self
    {
    }
    public function getAxisColor() : array
    {
    }
    /**
     * @param mixed $rgb
     * @param null|mixed $theme
     * @param null|mixed $tint
     */
    public function setAxisColor($rgb, $theme = null, $tint = null) : self
    {
    }
}
