<?php

namespace PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting;

class ConditionalFormatValueObject
{
    /**
     * ConditionalFormatValueObject constructor.
     *
     * @param null|mixed $cellFormula
     */
    public function __construct($type, $value = null, $cellFormula = null)
    {
    }
    /**
     * @return mixed
     */
    public function getType()
    {
    }
    /**
     * @param mixed $type
     */
    public function setType($type)
    {
    }
    /**
     * @return mixed
     */
    public function getValue()
    {
    }
    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
    }
    /**
     * @return mixed
     */
    public function getCellFormula()
    {
    }
    /**
     * @param mixed $cellFormula
     */
    public function setCellFormula($cellFormula)
    {
    }
}
