<?php

namespace PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting;

class ConditionalDataBar
{
    /**
     * @return null|bool
     */
    public function getShowValue()
    {
    }
    /**
     * @param bool $showValue
     */
    public function setShowValue($showValue)
    {
    }
    /**
     * @return ConditionalFormatValueObject
     */
    public function getMinimumConditionalFormatValueObject()
    {
    }
    public function setMinimumConditionalFormatValueObject(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalFormatValueObject $minimumConditionalFormatValueObject)
    {
    }
    /**
     * @return ConditionalFormatValueObject
     */
    public function getMaximumConditionalFormatValueObject()
    {
    }
    public function setMaximumConditionalFormatValueObject(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalFormatValueObject $maximumConditionalFormatValueObject)
    {
    }
    public function getColor() : string
    {
    }
    public function setColor(string $color) : self
    {
    }
    /**
     * @return ConditionalFormattingRuleExtension
     */
    public function getConditionalFormattingRuleExt()
    {
    }
    public function setConditionalFormattingRuleExt(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalFormattingRuleExtension $conditionalFormattingRuleExt)
    {
    }
}
