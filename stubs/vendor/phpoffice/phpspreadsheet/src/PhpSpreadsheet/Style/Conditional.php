<?php

namespace PhpOffice\PhpSpreadsheet\Style;

class Conditional implements \PhpOffice\PhpSpreadsheet\IComparable
{
    // Condition types
    const CONDITION_NONE = 'none';
    const CONDITION_CELLIS = 'cellIs';
    const CONDITION_CONTAINSTEXT = 'containsText';
    const CONDITION_EXPRESSION = 'expression';
    const CONDITION_CONTAINSBLANKS = 'containsBlanks';
    const CONDITION_NOTCONTAINSBLANKS = 'notContainsBlanks';
    const CONDITION_DATABAR = 'dataBar';
    const CONDITION_NOTCONTAINSTEXT = 'notContainsText';
    // Operator types
    const OPERATOR_NONE = '';
    const OPERATOR_BEGINSWITH = 'beginsWith';
    const OPERATOR_ENDSWITH = 'endsWith';
    const OPERATOR_EQUAL = 'equal';
    const OPERATOR_GREATERTHAN = 'greaterThan';
    const OPERATOR_GREATERTHANOREQUAL = 'greaterThanOrEqual';
    const OPERATOR_LESSTHAN = 'lessThan';
    const OPERATOR_LESSTHANOREQUAL = 'lessThanOrEqual';
    const OPERATOR_NOTEQUAL = 'notEqual';
    const OPERATOR_CONTAINSTEXT = 'containsText';
    const OPERATOR_NOTCONTAINS = 'notContains';
    const OPERATOR_BETWEEN = 'between';
    const OPERATOR_NOTBETWEEN = 'notBetween';
    /**
     * Create a new Conditional.
     */
    public function __construct()
    {
    }
    /**
     * Get Condition type.
     *
     * @return string
     */
    public function getConditionType()
    {
    }
    /**
     * Set Condition type.
     *
     * @param string $type Condition type, see self::CONDITION_*
     *
     * @return $this
     */
    public function setConditionType($type)
    {
    }
    /**
     * Get Operator type.
     *
     * @return string
     */
    public function getOperatorType()
    {
    }
    /**
     * Set Operator type.
     *
     * @param string $type Conditional operator type, see self::OPERATOR_*
     *
     * @return $this
     */
    public function setOperatorType($type)
    {
    }
    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
    }
    /**
     * Set text.
     *
     * @param string $text
     *
     * @return $this
     */
    public function setText($text)
    {
    }
    /**
     * Get StopIfTrue.
     *
     * @return bool
     */
    public function getStopIfTrue()
    {
    }
    /**
     * Set StopIfTrue.
     *
     * @param bool $stopIfTrue
     *
     * @return $this
     */
    public function setStopIfTrue($stopIfTrue)
    {
    }
    /**
     * Get Conditions.
     *
     * @return string[]
     */
    public function getConditions()
    {
    }
    /**
     * Set Conditions.
     *
     * @param bool|float|int|string|string[] $conditions Condition
     *
     * @return $this
     */
    public function setConditions($conditions)
    {
    }
    /**
     * Add Condition.
     *
     * @param string $condition Condition
     *
     * @return $this
     */
    public function addCondition($condition)
    {
    }
    /**
     * Get Style.
     *
     * @return Style
     */
    public function getStyle()
    {
    }
    /**
     * Set Style.
     *
     * @param Style $style
     *
     * @return $this
     */
    public function setStyle(?\PhpOffice\PhpSpreadsheet\Style\Style $style = null)
    {
    }
    /**
     * get DataBar.
     *
     * @return null|ConditionalDataBar
     */
    public function getDataBar()
    {
    }
    /**
     * set DataBar.
     *
     * @return $this
     */
    public function setDataBar(\PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\ConditionalDataBar $dataBar)
    {
    }
    /**
     * Get hash code.
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
    /**
     * Verify if param is valid condition type.
     */
    public static function isValidConditionType(string $type) : bool
    {
    }
}
