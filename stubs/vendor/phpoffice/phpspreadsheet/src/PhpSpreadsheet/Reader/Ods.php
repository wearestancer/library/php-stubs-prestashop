<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

class Ods extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    const INITIAL_FILE = 'content.xml';
    /**
     * Create a new Ods Reader instance.
     */
    public function __construct()
    {
    }
    /**
     * Can the current IReader read the file?
     */
    public function canRead(string $filename) : bool
    {
    }
    /**
     * Reads names of the worksheets from a file, without parsing the whole file to a PhpSpreadsheet object.
     *
     * @param string $pFilename
     *
     * @return string[]
     */
    public function listWorksheetNames($pFilename)
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     *
     * @param string $pFilename
     *
     * @return array
     */
    public function listWorksheetInfo($pFilename)
    {
    }
    /**
     * Loads PhpSpreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Loads PhpSpreadsheet from file into PhpSpreadsheet instance.
     *
     * @param string $pFilename
     *
     * @return Spreadsheet
     */
    public function loadIntoExisting($pFilename, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Recursively scan element.
     *
     * @return string
     */
    protected function scanElementForText(\DOMNode $element)
    {
    }
}
