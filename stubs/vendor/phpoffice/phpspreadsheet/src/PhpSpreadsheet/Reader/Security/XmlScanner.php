<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Security;

class XmlScanner
{
    public function __construct($pattern = '<!DOCTYPE')
    {
    }
    public static function getInstance(\PhpOffice\PhpSpreadsheet\Reader\IReader $reader)
    {
    }
    public static function threadSafeLibxmlDisableEntityLoaderAvailability()
    {
    }
    public static function shutdown() : void
    {
    }
    public function __destruct()
    {
    }
    public function setAdditionalCallback(callable $callback) : void
    {
    }
    /**
     * Scan the XML for use of <!ENTITY to prevent XXE/XEE attacks.
     *
     * @param mixed $xml
     *
     * @return string
     */
    public function scan($xml)
    {
    }
    /**
     * Scan theXML for use of <!ENTITY to prevent XXE/XEE attacks.
     *
     * @param string $filestream
     *
     * @return string
     */
    public function scanFile($filestream)
    {
    }
}
