<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

class Slk extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    /**
     * Create a new SYLK Reader instance.
     */
    public function __construct()
    {
    }
    /**
     * Validate that the current file is a SYLK file.
     */
    public function canRead(string $filename) : bool
    {
    }
    /**
     * Set input encoding.
     *
     * @deprecated no use is made of this property
     *
     * @param string $pValue Input encoding, eg: 'ANSI'
     *
     * @return $this
     *
     * @codeCoverageIgnore
     */
    public function setInputEncoding($pValue)
    {
    }
    /**
     * Get input encoding.
     *
     * @deprecated no use is made of this property
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function getInputEncoding()
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     *
     * @param string $pFilename
     *
     * @return array
     */
    public function listWorksheetInfo($pFilename)
    {
    }
    /**
     * Loads PhpSpreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Loads PhpSpreadsheet from file into PhpSpreadsheet instance.
     *
     * @param string $pFilename
     *
     * @return Spreadsheet
     */
    public function loadIntoExisting($pFilename, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Get sheet index.
     *
     * @return int
     */
    public function getSheetIndex()
    {
    }
    /**
     * Set sheet index.
     *
     * @param int $pValue Sheet index
     *
     * @return $this
     */
    public function setSheetIndex($pValue)
    {
    }
}
