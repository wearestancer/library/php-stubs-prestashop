<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Gnumeric;

class PageSetup
{
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    public function printInformation(\SimpleXMLElement $sheet) : self
    {
    }
    public function sheetMargins(\SimpleXMLElement $sheet) : self
    {
    }
}
