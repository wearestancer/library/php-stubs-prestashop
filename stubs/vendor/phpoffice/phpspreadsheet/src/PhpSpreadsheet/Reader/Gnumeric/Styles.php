<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Gnumeric;

class Styles
{
    /**
     * @var bool
     */
    protected $readDataOnly = false;
    /** @var array */
    public static $mappings = ['borderStyle' => ['0' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE, '1' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, '2' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM, '3' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_SLANTDASHDOT, '4' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHED, '5' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK, '6' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE, '7' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED, '8' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHED, '9' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOT, '10' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOT, '11' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOTDOT, '12' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOTDOT, '13' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOTDOT], 'fillType' => [
        '1' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
        '2' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY,
        '3' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_MEDIUMGRAY,
        '4' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTGRAY,
        '5' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_GRAY125,
        '6' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_GRAY0625,
        '7' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKHORIZONTAL,
        // horizontal stripe
        '8' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKVERTICAL,
        // vertical stripe
        '9' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKDOWN,
        // diagonal stripe
        '10' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKUP,
        // reverse diagonal stripe
        '11' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRID,
        // diagoanl crosshatch
        '12' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKTRELLIS,
        // thick diagonal crosshatch
        '13' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTHORIZONTAL,
        '14' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTVERTICAL,
        '15' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTUP,
        '16' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTDOWN,
        '17' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTGRID,
        // thin horizontal crosshatch
        '18' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTTRELLIS,
    ], 'horizontal' => ['1' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_GENERAL, '2' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, '4' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT, '8' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, '16' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS, '32' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY, '64' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS], 'underline' => ['1' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLE, '2' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE, '3' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLEACCOUNTING, '4' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLEACCOUNTING], 'vertical' => ['1' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP, '2' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM, '4' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, '8' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_JUSTIFY]];
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet, bool $readDataOnly)
    {
    }
    public function read(\SimpleXMLElement $sheet, int $maxRow, int $maxCol) : void
    {
    }
}
