<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

class Csv extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    const DEFAULT_FALLBACK_ENCODING = 'CP1252';
    const GUESS_ENCODING = 'guess';
    const UTF8_BOM = "﻿";
    const UTF8_BOM_LEN = 3;
    const UTF16BE_BOM = "\xfe\xff";
    const UTF16BE_BOM_LEN = 2;
    const UTF16BE_LF = "\x00\n";
    const UTF16LE_BOM = "\xff\xfe";
    const UTF16LE_BOM_LEN = 2;
    const UTF16LE_LF = "\n\x00";
    const UTF32BE_BOM = "\x00\x00\xfe\xff";
    const UTF32BE_BOM_LEN = 4;
    const UTF32BE_LF = "\x00\x00\x00\n";
    const UTF32LE_BOM = "\xff\xfe\x00\x00";
    const UTF32LE_BOM_LEN = 4;
    const UTF32LE_LF = "\n\x00\x00\x00";
    /**
     * Create a new CSV Reader instance.
     */
    public function __construct()
    {
    }
    /**
     * Set a callback to change the defaults.
     *
     * The callback must accept the Csv Reader object as the first parameter,
     * and it should return void.
     */
    public static function setConstructorCallback(?callable $callback) : void
    {
    }
    public static function getConstructorCallback() : ?callable
    {
    }
    public function setInputEncoding(string $encoding) : self
    {
    }
    public function getInputEncoding() : string
    {
    }
    public function setFallbackEncoding(string $pValue) : self
    {
    }
    public function getFallbackEncoding() : string
    {
    }
    /**
     * Move filepointer past any BOM marker.
     */
    protected function skipBOM() : void
    {
    }
    /**
     * Identify any separator that is explicitly set in the file.
     */
    protected function checkSeparator() : void
    {
    }
    /**
     * Infer the separator if it isn't explicitly set in the file or specified by the user.
     */
    protected function inferSeparator() : void
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     */
    public function listWorksheetInfo(string $filename) : array
    {
    }
    /**
     * Loads Spreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Loads PhpSpreadsheet from file into PhpSpreadsheet instance.
     */
    public function loadIntoExisting(string $filename, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet) : \PhpOffice\PhpSpreadsheet\Spreadsheet
    {
    }
    public function getDelimiter() : ?string
    {
    }
    public function setDelimiter(?string $delimiter) : self
    {
    }
    public function getEnclosure() : string
    {
    }
    public function setEnclosure(string $enclosure) : self
    {
    }
    public function getSheetIndex() : int
    {
    }
    public function setSheetIndex(int $indexValue) : self
    {
    }
    public function setContiguous(bool $contiguous) : self
    {
    }
    public function getContiguous() : bool
    {
    }
    public function setEscapeCharacter(string $escapeCharacter) : self
    {
    }
    public function getEscapeCharacter() : string
    {
    }
    /**
     * Can the current IReader read the file?
     */
    public function canRead(string $filename) : bool
    {
    }
    public static function guessEncoding(string $filename, string $dflt = self::DEFAULT_FALLBACK_ENCODING) : string
    {
    }
}
