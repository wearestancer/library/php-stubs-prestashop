<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

class Xlsx extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    const INITIAL_FILE = '_rels/.rels';
    /**
     * Create a new Xlsx Reader instance.
     */
    public function __construct()
    {
    }
    /**
     * Can the current IReader read the file?
     */
    public function canRead(string $filename) : bool
    {
    }
    /**
     * @param mixed $value
     */
    public static function testSimpleXml($value) : \SimpleXMLElement
    {
    }
    public static function getAttributes(?\SimpleXMLElement $value, string $ns = '') : \SimpleXMLElement
    {
    }
    /**
     * @param mixed $value
     */
    public static function falseToArray($value) : array
    {
    }
    /**
     * Reads names of the worksheets from a file, without parsing the whole file to a Spreadsheet object.
     *
     * @param string $filename
     *
     * @return array
     */
    public function listWorksheetNames($filename)
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     *
     * @param string $filename
     *
     * @return array
     */
    public function listWorksheetInfo($filename)
    {
    }
    /**
     * Loads Spreadsheet from file.
     */
    public function load(string $filename, int $flags = 0) : \PhpOffice\PhpSpreadsheet\Spreadsheet
    {
    }
    public static function stripWhiteSpaceFromStyleString($string)
    {
    }
}
