<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Ods;

abstract class BaseReader
{
    /**
     * @var Spreadsheet
     */
    protected $spreadsheet;
    /**
     * @var string
     */
    protected $tableNs;
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet, string $tableNs)
    {
    }
    public abstract function read(\DOMElement $workbookData) : void;
    protected function convertToExcelAddressValue(string $openOfficeAddress) : string
    {
    }
    protected function convertToExcelFormulaValue(string $openOfficeFormula) : string
    {
    }
}
