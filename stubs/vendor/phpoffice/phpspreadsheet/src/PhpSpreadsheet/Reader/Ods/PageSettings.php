<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Ods;

class PageSettings
{
    public function __construct(\DOMDocument $styleDom)
    {
    }
    public function readStyleCrossReferences(\DOMDocument $contentDom) : void
    {
    }
    public function setPrintSettingsForWorksheet(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet, string $styleName) : void
    {
    }
}
