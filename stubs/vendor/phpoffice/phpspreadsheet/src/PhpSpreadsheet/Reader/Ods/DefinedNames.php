<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Ods;

class DefinedNames extends \PhpOffice\PhpSpreadsheet\Reader\Ods\BaseReader
{
    public function read(\DOMElement $workbookData) : void
    {
    }
    /**
     * Read any Named Ranges that are defined in this spreadsheet.
     */
    protected function readDefinedRanges(\DOMElement $workbookData) : void
    {
    }
    /**
     * Read any Named Formulae that are defined in this spreadsheet.
     */
    protected function readDefinedExpressions(\DOMElement $workbookData) : void
    {
    }
}
