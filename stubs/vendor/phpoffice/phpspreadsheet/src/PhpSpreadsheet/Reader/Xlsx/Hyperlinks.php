<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Hyperlinks
{
    public function __construct(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $workSheet)
    {
    }
    public function readHyperlinks(\SimpleXMLElement $relsWorksheet) : void
    {
    }
    public function setHyperlinks(\SimpleXMLElement $worksheetXml) : void
    {
    }
}
