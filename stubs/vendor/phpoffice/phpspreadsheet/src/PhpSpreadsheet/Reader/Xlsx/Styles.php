<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Styles extends \PhpOffice\PhpSpreadsheet\Reader\Xlsx\BaseParserClass
{
    public function __construct(\SimpleXMLElement $styleXml)
    {
    }
    public function setStyleBaseData(?\PhpOffice\PhpSpreadsheet\Reader\Xlsx\Theme $theme = null, $styles = [], $cellStyles = []) : void
    {
    }
    public static function readFontStyle(\PhpOffice\PhpSpreadsheet\Style\Font $fontStyle, \SimpleXMLElement $fontStyleXml) : void
    {
    }
    public static function readFillStyle(\PhpOffice\PhpSpreadsheet\Style\Fill $fillStyle, \SimpleXMLElement $fillStyleXml) : void
    {
    }
    public static function readBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Borders $borderStyle, \SimpleXMLElement $borderStyleXml) : void
    {
    }
    public static function readAlignmentStyle(\PhpOffice\PhpSpreadsheet\Style\Alignment $alignment, \SimpleXMLElement $alignmentXml) : void
    {
    }
    public static function readProtectionLocked(\PhpOffice\PhpSpreadsheet\Style\Style $docStyle, $style) : void
    {
    }
    public static function readProtectionHidden(\PhpOffice\PhpSpreadsheet\Style\Style $docStyle, $style) : void
    {
    }
    public static function readColor($color, $background = false)
    {
    }
    public function dxfs($readDataOnly = false)
    {
    }
    public function styles()
    {
    }
}
