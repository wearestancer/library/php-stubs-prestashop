<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ColumnAndRowAttributes extends \PhpOffice\PhpSpreadsheet\Reader\Xlsx\BaseParserClass
{
    public function __construct(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $workSheet, ?\SimpleXMLElement $worksheetXml = null)
    {
    }
    /**
     * @param IReadFilter $readFilter
     * @param bool $readDataOnly
     */
    public function load(?\PhpOffice\PhpSpreadsheet\Reader\IReadFilter $readFilter = null, $readDataOnly = false) : void
    {
    }
}
