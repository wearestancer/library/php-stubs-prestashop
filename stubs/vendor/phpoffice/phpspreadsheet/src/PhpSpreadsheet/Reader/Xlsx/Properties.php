<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Properties
{
    public function __construct(\PhpOffice\PhpSpreadsheet\Reader\Security\XmlScanner $securityScanner, \PhpOffice\PhpSpreadsheet\Document\Properties $docProps)
    {
    }
    public function readCoreProperties(string $propertyData) : void
    {
    }
    public function readExtendedProperties(string $propertyData) : void
    {
    }
    public function readCustomProperties(string $propertyData) : void
    {
    }
}
