<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class SheetViewOptions extends \PhpOffice\PhpSpreadsheet\Reader\Xlsx\BaseParserClass
{
    public function __construct(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $workSheet, ?\SimpleXMLElement $worksheetXml = null)
    {
    }
    /**
     * @param bool $readDataOnly
     */
    public function load($readDataOnly = false) : void
    {
    }
}
