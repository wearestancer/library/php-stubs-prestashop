<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class PageSetup extends \PhpOffice\PhpSpreadsheet\Reader\Xlsx\BaseParserClass
{
    public function __construct(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $workSheet, ?\SimpleXMLElement $worksheetXml = null)
    {
    }
    public function load(array $unparsedLoadedData)
    {
    }
}
