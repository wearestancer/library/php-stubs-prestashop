<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

class Gnumeric extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    const NAMESPACE_GNM = 'http://www.gnumeric.org/v10.dtd';
    // gmr in old sheets
    const NAMESPACE_XSI = 'http://www.w3.org/2001/XMLSchema-instance';
    const NAMESPACE_OFFICE = 'urn:oasis:names:tc:opendocument:xmlns:office:1.0';
    const NAMESPACE_XLINK = 'http://www.w3.org/1999/xlink';
    const NAMESPACE_DC = 'http://purl.org/dc/elements/1.1/';
    const NAMESPACE_META = 'urn:oasis:names:tc:opendocument:xmlns:meta:1.0';
    const NAMESPACE_OOO = 'http://openoffice.org/2004/office';
    /** @var array */
    public static $mappings = ['dataType' => [
        '10' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NULL,
        '20' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_BOOL,
        '30' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC,
        // Integer doesn't exist in Excel
        '40' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC,
        // Float
        '50' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_ERROR,
        '60' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING,
    ]];
    /**
     * Create a new Gnumeric.
     */
    public function __construct()
    {
    }
    /**
     * Can the current IReader read the file?
     */
    public function canRead(string $filename) : bool
    {
    }
    /**
     * Reads names of the worksheets from a file, without parsing the whole file to a Spreadsheet object.
     *
     * @param string $pFilename
     *
     * @return array
     */
    public function listWorksheetNames($pFilename)
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     *
     * @param string $pFilename
     *
     * @return array
     */
    public function listWorksheetInfo($pFilename)
    {
    }
    public static function gnumericMappings() : array
    {
    }
    /**
     * Loads Spreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Loads from file into Spreadsheet instance.
     */
    public function loadIntoExisting(string $pFilename, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet) : \PhpOffice\PhpSpreadsheet\Spreadsheet
    {
    }
}
