<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Csv;

class Delimiter
{
    protected const POTENTIAL_DELIMETERS = [',', ';', "\t", '|', ':', ' ', '~'];
    /** @var resource */
    protected $fileHandle;
    /** @var string */
    protected $escapeCharacter;
    /** @var string */
    protected $enclosure;
    /** @var array */
    protected $counts = [];
    /** @var int */
    protected $numberLines = 0;
    /** @var ?string */
    protected $delimiter;
    /**
     * @param resource $fileHandle
     */
    public function __construct($fileHandle, string $escapeCharacter, string $enclosure)
    {
    }
    public function getDefaultDelimiter() : string
    {
    }
    public function linesCounted() : int
    {
    }
    protected function countPotentialDelimiters() : void
    {
    }
    protected function countDelimiterValues(string $line, array $delimiterKeys) : void
    {
    }
    public function infer() : ?string
    {
    }
    /**
     * Get the next full line from the file.
     *
     * @return false|string
     */
    public function getNextLine()
    {
    }
}
