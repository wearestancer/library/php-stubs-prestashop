<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xml\Style;

abstract class StyleBase
{
    protected static function identifyFixedStyleValue(array $styleList, string &$styleAttributeValue) : bool
    {
    }
    protected static function getAttributes(?\SimpleXMLElement $simple, string $node) : \SimpleXMLElement
    {
    }
}
