<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xml\Style;

class Border extends \PhpOffice\PhpSpreadsheet\Reader\Xml\Style\StyleBase
{
    protected const BORDER_POSITIONS = ['top', 'left', 'bottom', 'right'];
    /**
     * @var array
     */
    public const BORDER_MAPPINGS = ['borderStyle' => ['1continuous' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, '1dash' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHED, '1dashdot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOT, '1dashdotdot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOTDOT, '1dot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED, '1double' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE, '2continuous' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM, '2dash' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHED, '2dashdot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOT, '2dashdotdot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOTDOT, '2dot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED, '2double' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE, '3continuous' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK, '3dash' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHED, '3dashdot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOT, '3dashdotdot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHDOTDOT, '3dot' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED, '3double' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE]];
    public function parseStyle(\SimpleXMLElement $styleData, array $namespaces) : array
    {
    }
    protected function parsePosition(string $borderStyleValue, string $diagonalDirection) : array
    {
    }
}
