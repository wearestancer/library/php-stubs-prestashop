<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xml\Style;

class Font extends \PhpOffice\PhpSpreadsheet\Reader\Xml\Style\StyleBase
{
    protected const UNDERLINE_STYLES = [\PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE, \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLEACCOUNTING, \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLE, \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLEACCOUNTING];
    protected function parseUnderline(array $style, string $styleAttributeValue) : array
    {
    }
    protected function parseVerticalAlign(array $style, string $styleAttributeValue) : array
    {
    }
    public function parseStyle(\SimpleXMLElement $styleAttributes) : array
    {
    }
}
