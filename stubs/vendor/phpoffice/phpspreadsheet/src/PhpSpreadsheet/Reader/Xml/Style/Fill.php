<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xml\Style;

class Fill extends \PhpOffice\PhpSpreadsheet\Reader\Xml\Style\StyleBase
{
    /**
     * @var array
     */
    public const FILL_MAPPINGS = ['fillType' => [
        'solid' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
        'gray75' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY,
        'gray50' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_MEDIUMGRAY,
        'gray25' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTGRAY,
        'gray125' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_GRAY125,
        'gray0625' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_GRAY0625,
        'horzstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKHORIZONTAL,
        // horizontal stripe
        'vertstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKVERTICAL,
        // vertical stripe
        'reversediagstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKUP,
        // reverse diagonal stripe
        'diagstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKDOWN,
        // diagonal stripe
        'diagcross' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRID,
        // diagoanl crosshatch
        'thickdiagcross' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKTRELLIS,
        // thick diagonal crosshatch
        'thinhorzstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTHORIZONTAL,
        'thinvertstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTVERTICAL,
        'thinreversediagstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTUP,
        'thindiagstripe' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTDOWN,
        'thinhorzcross' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTGRID,
        // thin horizontal crosshatch
        'thindiagcross' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_LIGHTTRELLIS,
    ]];
    public function parseStyle(\SimpleXMLElement $styleAttributes) : array
    {
    }
}
