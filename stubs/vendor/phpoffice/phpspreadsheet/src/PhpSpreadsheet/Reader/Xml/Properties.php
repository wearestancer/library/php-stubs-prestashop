<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xml;

class Properties
{
    /**
     * @var Spreadsheet
     */
    protected $spreadsheet;
    public function __construct(\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    public function readProperties(\SimpleXMLElement $xml, array $namespaces) : void
    {
    }
    protected function readStandardProperties(\SimpleXMLElement $xml) : void
    {
    }
    protected function readCustomProperties(\SimpleXMLElement $xml, array $namespaces) : void
    {
    }
    protected function processStandardProperty(\PhpOffice\PhpSpreadsheet\Document\Properties $docProps, string $propertyName, string $stringValue) : void
    {
    }
    protected function processCustomProperty(\PhpOffice\PhpSpreadsheet\Document\Properties $docProps, string $propertyName, ?\SimpleXMLElement $propertyValue, \SimpleXMLElement $propertyAttributes) : void
    {
    }
    protected function hex2str(array $hex) : string
    {
    }
}
