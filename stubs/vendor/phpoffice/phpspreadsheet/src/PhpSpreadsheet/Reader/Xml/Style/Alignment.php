<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xml\Style;

class Alignment extends \PhpOffice\PhpSpreadsheet\Reader\Xml\Style\StyleBase
{
    protected const VERTICAL_ALIGNMENT_STYLES = [\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM, \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP, \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_JUSTIFY];
    protected const HORIZONTAL_ALIGNMENT_STYLES = [\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_GENERAL, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY];
    public function parseStyle(\SimpleXMLElement $styleAttributes) : array
    {
    }
}
