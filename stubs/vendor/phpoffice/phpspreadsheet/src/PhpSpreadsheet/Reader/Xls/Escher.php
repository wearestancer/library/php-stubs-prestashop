<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xls;

class Escher
{
    const DGGCONTAINER = 0xf000;
    const BSTORECONTAINER = 0xf001;
    const DGCONTAINER = 0xf002;
    const SPGRCONTAINER = 0xf003;
    const SPCONTAINER = 0xf004;
    const DGG = 0xf006;
    const BSE = 0xf007;
    const DG = 0xf008;
    const SPGR = 0xf009;
    const SP = 0xf00a;
    const OPT = 0xf00b;
    const CLIENTTEXTBOX = 0xf00d;
    const CLIENTANCHOR = 0xf010;
    const CLIENTDATA = 0xf011;
    const BLIPJPEG = 0xf01d;
    const BLIPPNG = 0xf01e;
    const SPLITMENUCOLORS = 0xf11e;
    const TERTIARYOPT = 0xf122;
    /**
     * Create a new Escher instance.
     *
     * @param mixed $object
     */
    public function __construct($object)
    {
    }
    /**
     * Load Escher stream data. May be a partial Escher stream.
     *
     * @param string $data
     *
     * @return BSE|BstoreContainer|DgContainer|DggContainer|\PhpOffice\PhpSpreadsheet\Shared\Escher|SpContainer|SpgrContainer
     */
    public function load($data)
    {
    }
}
