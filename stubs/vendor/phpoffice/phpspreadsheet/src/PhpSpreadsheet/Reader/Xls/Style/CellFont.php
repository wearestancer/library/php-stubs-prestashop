<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xls\Style;

class CellFont
{
    public static function escapement(\PhpOffice\PhpSpreadsheet\Style\Font $font, int $escapement) : void
    {
    }
    /**
     * @var array<int, string>
     */
    protected static $underlineMap = [0x1 => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLE, 0x2 => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE, 0x21 => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLEACCOUNTING, 0x22 => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLEACCOUNTING];
    public static function underline(\PhpOffice\PhpSpreadsheet\Style\Font $font, int $underline) : void
    {
    }
}
