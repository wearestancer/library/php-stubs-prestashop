<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xls;

class MD5
{
    /**
     * MD5 stream constructor.
     */
    public function __construct()
    {
    }
    /**
     * Reset the MD5 stream context.
     */
    public function reset() : void
    {
    }
    /**
     * Get MD5 stream context.
     *
     * @return string
     */
    public function getContext()
    {
    }
    /**
     * Add data to context.
     *
     * @param string $data Data to add
     */
    public function add(string $data) : void
    {
    }
}
