<?php

namespace PhpOffice\PhpSpreadsheet\Reader\Xls\Style;

class CellAlignment
{
    /**
     * @var array<int, string>
     */
    protected static $horizontalAlignmentMap = [0 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_GENERAL, 1 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 2 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 3 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT, 4 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_FILL, 5 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY, 6 => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER_CONTINUOUS];
    /**
     * @var array<int, string>
     */
    protected static $verticalAlignmentMap = [0 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP, 1 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 2 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM, 3 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_JUSTIFY];
    public static function horizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment $alignment, int $horizontal) : void
    {
    }
    public static function vertical(\PhpOffice\PhpSpreadsheet\Style\Alignment $alignment, int $vertical) : void
    {
    }
    public static function wrap(\PhpOffice\PhpSpreadsheet\Style\Alignment $alignment, int $wrap) : void
    {
    }
}
