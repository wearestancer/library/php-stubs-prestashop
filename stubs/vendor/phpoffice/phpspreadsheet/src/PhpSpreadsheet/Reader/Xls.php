<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

// Original file header of ParseXL (used as the base for this class):
// --------------------------------------------------------------------------------
// Adapted from Excel_Spreadsheet_Reader developed by users bizon153,
// trex005, and mmp11 (SourceForge.net)
// https://sourceforge.net/projects/phpexcelreader/
// Primary changes made by canyoncasa (dvc) for ParseXL 1.00 ...
//     Modelled moreso after Perl Excel Parse/Write modules
//     Added Parse_Excel_Spreadsheet object
//         Reads a whole worksheet or tab as row,column array or as
//         associated hash of indexed rows and named column fields
//     Added variables for worksheet (tab) indexes and names
//     Added an object call for loading individual woorksheets
//     Changed default indexing defaults to 0 based arrays
//     Fixed date/time and percent formats
//     Includes patches found at SourceForge...
//         unicode patch by nobody
//         unpack("d") machine depedency patch by matchy
//         boundsheet utf16 patch by bjaenichen
//     Renamed functions for shorter names
//     General code cleanup and rigor, including <80 column width
//     Included a testcase Excel file and PHP example calls
//     Code works for PHP 5.x
// Primary changes made by canyoncasa (dvc) for ParseXL 1.10 ...
// http://sourceforge.net/tracker/index.php?func=detail&aid=1466964&group_id=99160&atid=623334
//     Decoding of formula conditions, results, and tokens.
//     Support for user-defined named cells added as an array "namedcells"
//         Patch code for user-defined named cells supports single cells only.
//         NOTE: this patch only works for BIFF8 as BIFF5-7 use a different
//         external sheet reference structure
class Xls extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    // ParseXL definitions
    const XLS_BIFF8 = 0x600;
    const XLS_BIFF7 = 0x500;
    const XLS_WORKBOOKGLOBALS = 0x5;
    const XLS_WORKSHEET = 0x10;
    // record identifiers
    const XLS_TYPE_FORMULA = 0x6;
    const XLS_TYPE_EOF = 0xa;
    const XLS_TYPE_PROTECT = 0x12;
    const XLS_TYPE_OBJECTPROTECT = 0x63;
    const XLS_TYPE_SCENPROTECT = 0xdd;
    const XLS_TYPE_PASSWORD = 0x13;
    const XLS_TYPE_HEADER = 0x14;
    const XLS_TYPE_FOOTER = 0x15;
    const XLS_TYPE_EXTERNSHEET = 0x17;
    const XLS_TYPE_DEFINEDNAME = 0x18;
    const XLS_TYPE_VERTICALPAGEBREAKS = 0x1a;
    const XLS_TYPE_HORIZONTALPAGEBREAKS = 0x1b;
    const XLS_TYPE_NOTE = 0x1c;
    const XLS_TYPE_SELECTION = 0x1d;
    const XLS_TYPE_DATEMODE = 0x22;
    const XLS_TYPE_EXTERNNAME = 0x23;
    const XLS_TYPE_LEFTMARGIN = 0x26;
    const XLS_TYPE_RIGHTMARGIN = 0x27;
    const XLS_TYPE_TOPMARGIN = 0x28;
    const XLS_TYPE_BOTTOMMARGIN = 0x29;
    const XLS_TYPE_PRINTGRIDLINES = 0x2b;
    const XLS_TYPE_FILEPASS = 0x2f;
    const XLS_TYPE_FONT = 0x31;
    const XLS_TYPE_CONTINUE = 0x3c;
    const XLS_TYPE_PANE = 0x41;
    const XLS_TYPE_CODEPAGE = 0x42;
    const XLS_TYPE_DEFCOLWIDTH = 0x55;
    const XLS_TYPE_OBJ = 0x5d;
    const XLS_TYPE_COLINFO = 0x7d;
    const XLS_TYPE_IMDATA = 0x7f;
    const XLS_TYPE_SHEETPR = 0x81;
    const XLS_TYPE_HCENTER = 0x83;
    const XLS_TYPE_VCENTER = 0x84;
    const XLS_TYPE_SHEET = 0x85;
    const XLS_TYPE_PALETTE = 0x92;
    const XLS_TYPE_SCL = 0xa0;
    const XLS_TYPE_PAGESETUP = 0xa1;
    const XLS_TYPE_MULRK = 0xbd;
    const XLS_TYPE_MULBLANK = 0xbe;
    const XLS_TYPE_DBCELL = 0xd7;
    const XLS_TYPE_XF = 0xe0;
    const XLS_TYPE_MERGEDCELLS = 0xe5;
    const XLS_TYPE_MSODRAWINGGROUP = 0xeb;
    const XLS_TYPE_MSODRAWING = 0xec;
    const XLS_TYPE_SST = 0xfc;
    const XLS_TYPE_LABELSST = 0xfd;
    const XLS_TYPE_EXTSST = 0xff;
    const XLS_TYPE_EXTERNALBOOK = 0x1ae;
    const XLS_TYPE_DATAVALIDATIONS = 0x1b2;
    const XLS_TYPE_TXO = 0x1b6;
    const XLS_TYPE_HYPERLINK = 0x1b8;
    const XLS_TYPE_DATAVALIDATION = 0x1be;
    const XLS_TYPE_DIMENSION = 0x200;
    const XLS_TYPE_BLANK = 0x201;
    const XLS_TYPE_NUMBER = 0x203;
    const XLS_TYPE_LABEL = 0x204;
    const XLS_TYPE_BOOLERR = 0x205;
    const XLS_TYPE_STRING = 0x207;
    const XLS_TYPE_ROW = 0x208;
    const XLS_TYPE_INDEX = 0x20b;
    const XLS_TYPE_ARRAY = 0x221;
    const XLS_TYPE_DEFAULTROWHEIGHT = 0x225;
    const XLS_TYPE_WINDOW2 = 0x23e;
    const XLS_TYPE_RK = 0x27e;
    const XLS_TYPE_STYLE = 0x293;
    const XLS_TYPE_FORMAT = 0x41e;
    const XLS_TYPE_SHAREDFMLA = 0x4bc;
    const XLS_TYPE_BOF = 0x809;
    const XLS_TYPE_SHEETPROTECTION = 0x867;
    const XLS_TYPE_RANGEPROTECTION = 0x868;
    const XLS_TYPE_SHEETLAYOUT = 0x862;
    const XLS_TYPE_XFEXT = 0x87d;
    const XLS_TYPE_PAGELAYOUTVIEW = 0x88b;
    const XLS_TYPE_UNKNOWN = 0xffff;
    // Encryption type
    const MS_BIFF_CRYPTO_NONE = 0;
    const MS_BIFF_CRYPTO_XOR = 1;
    const MS_BIFF_CRYPTO_RC4 = 2;
    // Size of stream blocks when using RC4 encryption
    const REKEY_BLOCK = 0x400;
    /**
     * Create a new Xls Reader instance.
     */
    public function __construct()
    {
    }
    /**
     * Can the current IReader read the file?
     */
    public function canRead(string $filename) : bool
    {
    }
    public function setCodepage(string $codepage) : void
    {
    }
    /**
     * Reads names of the worksheets from a file, without parsing the whole file to a PhpSpreadsheet object.
     *
     * @param string $pFilename
     *
     * @return array
     */
    public function listWorksheetNames($pFilename)
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     *
     * @param string $pFilename
     *
     * @return array
     */
    public function listWorksheetInfo($pFilename)
    {
    }
    /**
     * Loads PhpSpreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Read 16-bit unsigned integer.
     *
     * @param string $data
     * @param int $pos
     *
     * @return int
     */
    public static function getUInt2d($data, $pos)
    {
    }
    /**
     * Read 16-bit signed integer.
     *
     * @param string $data
     * @param int $pos
     *
     * @return int
     */
    public static function getInt2d($data, $pos)
    {
    }
    /**
     * Read 32-bit signed integer.
     *
     * @param string $data
     * @param int $pos
     *
     * @return int
     */
    public static function getInt4d($data, $pos)
    {
    }
}
