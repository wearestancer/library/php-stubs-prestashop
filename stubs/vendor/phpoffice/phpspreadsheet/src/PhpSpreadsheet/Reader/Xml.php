<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

/**
 * Reader for SpreadsheetML, the XML schema for Microsoft Office Excel 2003.
 */
class Xml extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    /**
     * Formats.
     *
     * @var array
     */
    protected $styles = [];
    /**
     * Create a new Excel2003XML Reader instance.
     */
    public function __construct()
    {
    }
    public static function xmlMappings() : array
    {
    }
    /**
     * Can the current IReader read the file?
     */
    public function canRead(string $filename) : bool
    {
    }
    /**
     * Check if the file is a valid SimpleXML.
     *
     * @param string $pFilename
     *
     * @return false|SimpleXMLElement
     */
    public function trySimpleXMLLoadString($pFilename)
    {
    }
    /**
     * Reads names of the worksheets from a file, without parsing the whole file to a Spreadsheet object.
     *
     * @param string $filename
     *
     * @return array
     */
    public function listWorksheetNames($filename)
    {
    }
    /**
     * Return worksheet info (Name, Last Column Letter, Last Column Index, Total Rows, Total Columns).
     *
     * @param string $filename
     *
     * @return array
     */
    public function listWorksheetInfo($filename)
    {
    }
    /**
     * Loads Spreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Loads from file into Spreadsheet instance.
     *
     * @param string $filename
     *
     * @return Spreadsheet
     */
    public function loadIntoExisting($filename, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    protected function parseCellComment(\SimpleXMLElement $comment, array $namespaces, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet, string $columnID, int $rowID) : void
    {
    }
    protected function parseRichText(string $annotation) : \PhpOffice\PhpSpreadsheet\RichText\RichText
    {
    }
}
