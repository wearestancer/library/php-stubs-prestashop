<?php

namespace PhpOffice\PhpSpreadsheet\Reader;

/** PhpSpreadsheet root directory */
class Html extends \PhpOffice\PhpSpreadsheet\Reader\BaseReader
{
    /**
     * Sample size to read to determine if it's HTML or not.
     */
    const TEST_SAMPLE_SIZE = 2048;
    /**
     * Input encoding.
     *
     * @var string
     */
    protected $inputEncoding = 'ANSI';
    /**
     * Sheet index to read.
     *
     * @var int
     */
    protected $sheetIndex = 0;
    /**
     * Formats.
     *
     * @var array
     */
    protected $formats = [
        'h1' => ['font' => ['bold' => true, 'size' => 24]],
        //    Bold, 24pt
        'h2' => ['font' => ['bold' => true, 'size' => 18]],
        //    Bold, 18pt
        'h3' => ['font' => ['bold' => true, 'size' => 13.5]],
        //    Bold, 13.5pt
        'h4' => ['font' => ['bold' => true, 'size' => 12]],
        //    Bold, 12pt
        'h5' => ['font' => ['bold' => true, 'size' => 10]],
        //    Bold, 10pt
        'h6' => ['font' => ['bold' => true, 'size' => 7.5]],
        //    Bold, 7.5pt
        'a' => ['font' => ['underline' => true, 'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE]]],
        //    Blue underlined
        'hr' => ['borders' => ['bottom' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => [\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK]]]],
        //    Bottom border
        'strong' => ['font' => ['bold' => true]],
        //    Bold
        'b' => ['font' => ['bold' => true]],
        //    Bold
        'i' => ['font' => ['italic' => true]],
        //    Italic
        'em' => ['font' => ['italic' => true]],
    ];
    /** @var array */
    protected $rowspan = [];
    /**
     * Create a new HTML Reader instance.
     */
    public function __construct()
    {
    }
    /**
     * Validate that the current file is an HTML file.
     */
    public function canRead(string $filename) : bool
    {
    }
    /**
     * Loads Spreadsheet from file.
     *
     * @return Spreadsheet
     */
    public function load(string $filename, int $flags = 0)
    {
    }
    /**
     * Set input encoding.
     *
     * @param string $pValue Input encoding, eg: 'ANSI'
     *
     * @return $this
     *
     * @codeCoverageIgnore
     *
     * @deprecated no use is made of this property
     */
    public function setInputEncoding($pValue)
    {
    }
    /**
     * Get input encoding.
     *
     * @return string
     *
     * @codeCoverageIgnore
     *
     * @deprecated no use is made of this property
     */
    public function getInputEncoding()
    {
    }
    //    Data Array used for testing only, should write to Spreadsheet object on completion of tests
    /** @var array */
    protected $dataArray = [];
    /** @var int */
    protected $tableLevel = 0;
    /** @var array */
    protected $nestedColumn = ['A'];
    protected function setTableStartColumn(string $column) : string
    {
    }
    protected function getTableStartColumn() : string
    {
    }
    protected function releaseTableStartColumn() : string
    {
    }
    /**
     * Flush cell.
     *
     * @param string $column
     * @param int|string $row
     * @param mixed $cellContent
     */
    protected function flushCell(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet, $column, $row, &$cellContent) : void
    {
    }
    protected function processDomElement(\DOMNode $element, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet, int &$row, string &$column, string &$cellContent) : void
    {
    }
    /**
     * Loads PhpSpreadsheet from file into PhpSpreadsheet instance.
     *
     * @param string $pFilename
     *
     * @return Spreadsheet
     */
    public function loadIntoExisting($pFilename, \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet)
    {
    }
    /**
     * Spreadsheet from content.
     *
     * @param string $content
     */
    public function loadFromString($content, ?\PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet = null) : \PhpOffice\PhpSpreadsheet\Spreadsheet
    {
    }
    /**
     * Get sheet index.
     *
     * @return int
     */
    public function getSheetIndex()
    {
    }
    /**
     * Set sheet index.
     *
     * @param int $pValue Sheet index
     *
     * @return $this
     */
    public function setSheetIndex($pValue)
    {
    }
    /**
     * Check if has #, so we can get clean hex.
     *
     * @param mixed $value
     *
     * @return null|string
     */
    public function getStyleColor($value)
    {
    }
    public static function getBorderMappings() : array
    {
    }
    /**
     * Map html border style to PhpSpreadsheet border style.
     *
     * @param  string $style
     *
     * @return null|string
     */
    public function getBorderStyle($style)
    {
    }
}
