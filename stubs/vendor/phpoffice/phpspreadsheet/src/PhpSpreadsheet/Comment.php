<?php

namespace PhpOffice\PhpSpreadsheet;

class Comment implements \PhpOffice\PhpSpreadsheet\IComparable
{
    /**
     * Create a new Comment.
     */
    public function __construct()
    {
    }
    /**
     * Get Author.
     */
    public function getAuthor() : string
    {
    }
    /**
     * Set Author.
     */
    public function setAuthor(string $author) : self
    {
    }
    /**
     * Get Rich text comment.
     */
    public function getText() : \PhpOffice\PhpSpreadsheet\RichText\RichText
    {
    }
    /**
     * Set Rich text comment.
     */
    public function setText(\PhpOffice\PhpSpreadsheet\RichText\RichText $text) : self
    {
    }
    /**
     * Get comment width (CSS style, i.e. XXpx or YYpt).
     */
    public function getWidth() : string
    {
    }
    /**
     * Set comment width (CSS style, i.e. XXpx or YYpt). Default unit is pt.
     */
    public function setWidth(string $width) : self
    {
    }
    /**
     * Get comment height (CSS style, i.e. XXpx or YYpt).
     */
    public function getHeight() : string
    {
    }
    /**
     * Set comment height (CSS style, i.e. XXpx or YYpt). Default unit is pt.
     */
    public function setHeight(string $height) : self
    {
    }
    /**
     * Get left margin (CSS style, i.e. XXpx or YYpt).
     */
    public function getMarginLeft() : string
    {
    }
    /**
     * Set left margin (CSS style, i.e. XXpx or YYpt). Default unit is pt.
     */
    public function setMarginLeft(string $margin) : self
    {
    }
    /**
     * Get top margin (CSS style, i.e. XXpx or YYpt).
     */
    public function getMarginTop() : string
    {
    }
    /**
     * Set top margin (CSS style, i.e. XXpx or YYpt). Default unit is pt.
     */
    public function setMarginTop(string $margin) : self
    {
    }
    /**
     * Is the comment visible by default?
     */
    public function getVisible() : bool
    {
    }
    /**
     * Set comment default visibility.
     */
    public function setVisible(bool $visibility) : self
    {
    }
    /**
     * Set fill color.
     */
    public function setFillColor(\PhpOffice\PhpSpreadsheet\Style\Color $color) : self
    {
    }
    /**
     * Get fill color.
     */
    public function getFillColor() : \PhpOffice\PhpSpreadsheet\Style\Color
    {
    }
    /**
     * Set Alignment.
     */
    public function setAlignment(string $alignment) : self
    {
    }
    /**
     * Get Alignment.
     */
    public function getAlignment() : string
    {
    }
    /**
     * Get hash code.
     */
    public function getHashCode() : string
    {
    }
    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
    }
    /**
     * Convert to string.
     */
    public function __toString() : string
    {
    }
}
