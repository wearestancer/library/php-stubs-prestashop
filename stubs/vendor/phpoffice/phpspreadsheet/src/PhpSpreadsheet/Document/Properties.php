<?php

namespace PhpOffice\PhpSpreadsheet\Document;

class Properties
{
    /** constants */
    public const PROPERTY_TYPE_BOOLEAN = 'b';
    public const PROPERTY_TYPE_INTEGER = 'i';
    public const PROPERTY_TYPE_FLOAT = 'f';
    public const PROPERTY_TYPE_DATE = 'd';
    public const PROPERTY_TYPE_STRING = 's';
    public const PROPERTY_TYPE_UNKNOWN = 'u';
    /**
     * Create a new Document Properties instance.
     */
    public function __construct()
    {
    }
    /**
     * Get Creator.
     */
    public function getCreator() : string
    {
    }
    /**
     * Set Creator.
     *
     * @return $this
     */
    public function setCreator(string $creator) : self
    {
    }
    /**
     * Get Last Modified By.
     */
    public function getLastModifiedBy() : string
    {
    }
    /**
     * Set Last Modified By.
     *
     * @return $this
     */
    public function setLastModifiedBy(string $modifiedBy) : self
    {
    }
    /**
     * Get Created.
     *
     * @return float|int
     */
    public function getCreated()
    {
    }
    /**
     * Set Created.
     *
     * @param null|float|int|string $timestamp
     *
     * @return $this
     */
    public function setCreated($timestamp) : self
    {
    }
    /**
     * Get Modified.
     *
     * @return float|int
     */
    public function getModified()
    {
    }
    /**
     * Set Modified.
     *
     * @param null|float|int|string $timestamp
     *
     * @return $this
     */
    public function setModified($timestamp) : self
    {
    }
    /**
     * Get Title.
     */
    public function getTitle() : string
    {
    }
    /**
     * Set Title.
     *
     * @return $this
     */
    public function setTitle(string $title) : self
    {
    }
    /**
     * Get Description.
     */
    public function getDescription() : string
    {
    }
    /**
     * Set Description.
     *
     * @return $this
     */
    public function setDescription(string $description) : self
    {
    }
    /**
     * Get Subject.
     */
    public function getSubject() : string
    {
    }
    /**
     * Set Subject.
     *
     * @return $this
     */
    public function setSubject(string $subject) : self
    {
    }
    /**
     * Get Keywords.
     */
    public function getKeywords() : string
    {
    }
    /**
     * Set Keywords.
     *
     * @return $this
     */
    public function setKeywords(string $keywords) : self
    {
    }
    /**
     * Get Category.
     */
    public function getCategory() : string
    {
    }
    /**
     * Set Category.
     *
     * @return $this
     */
    public function setCategory(string $category) : self
    {
    }
    /**
     * Get Company.
     */
    public function getCompany() : string
    {
    }
    /**
     * Set Company.
     *
     * @return $this
     */
    public function setCompany(string $company) : self
    {
    }
    /**
     * Get Manager.
     */
    public function getManager() : string
    {
    }
    /**
     * Set Manager.
     *
     * @return $this
     */
    public function setManager(string $manager) : self
    {
    }
    /**
     * Get a List of Custom Property Names.
     *
     * @return string[]
     */
    public function getCustomProperties() : array
    {
    }
    /**
     * Check if a Custom Property is defined.
     */
    public function isCustomPropertySet(string $propertyName) : bool
    {
    }
    /**
     * Get a Custom Property Value.
     *
     * @return mixed
     */
    public function getCustomPropertyValue(string $propertyName)
    {
    }
    /**
     * Get a Custom Property Type.
     *
     * @return null|string
     */
    public function getCustomPropertyType(string $propertyName)
    {
    }
    /**
     * Set a Custom Property.
     *
     * @param mixed $propertyValue
     * @param string $propertyType
     *      'i'    : Integer
     *   'f' : Floating Point
     *   's' : String
     *   'd' : Date/Time
     *   'b' : Boolean
     *
     * @return $this
     */
    public function setCustomProperty(string $propertyName, $propertyValue = '', $propertyType = null) : self
    {
    }
    /**
     * Convert property to form desired by Excel.
     *
     * @param mixed $propertyValue
     *
     * @return mixed
     */
    public static function convertProperty($propertyValue, string $propertyType)
    {
    }
    public static function convertPropertyType(string $propertyType) : string
    {
    }
}
