<?php

namespace PhpOffice\PhpSpreadsheet\Document;

class Security
{
    /**
     * Create a new Document Security instance.
     */
    public function __construct()
    {
    }
    /**
     * Is some sort of document security enabled?
     */
    public function isSecurityEnabled() : bool
    {
    }
    public function getLockRevision() : bool
    {
    }
    public function setLockRevision(?bool $locked) : self
    {
    }
    public function getLockStructure() : bool
    {
    }
    public function setLockStructure(?bool $locked) : self
    {
    }
    public function getLockWindows() : bool
    {
    }
    public function setLockWindows(?bool $locked) : self
    {
    }
    public function getRevisionsPassword() : string
    {
    }
    /**
     * Set RevisionsPassword.
     *
     * @param string $password
     * @param bool $alreadyHashed If the password has already been hashed, set this to true
     *
     * @return $this
     */
    public function setRevisionsPassword(?string $password, bool $alreadyHashed = false)
    {
    }
    public function getWorkbookPassword() : string
    {
    }
    /**
     * Set WorkbookPassword.
     *
     * @param string $password
     * @param bool $alreadyHashed If the password has already been hashed, set this to true
     *
     * @return $this
     */
    public function setWorkbookPassword(?string $password, bool $alreadyHashed = false)
    {
    }
}
