<?php

namespace PhpOffice\PhpSpreadsheet\Helper;

class Dimension
{
    public const UOM_CENTIMETERS = 'cm';
    public const UOM_MILLIMETERS = 'mm';
    public const UOM_INCHES = 'in';
    public const UOM_PIXELS = 'px';
    public const UOM_POINTS = 'pt';
    public const UOM_PICA = 'pc';
    /**
     * Based on 96 dpi.
     */
    const ABSOLUTE_UNITS = [self::UOM_CENTIMETERS => 96.0 / 2.54, self::UOM_MILLIMETERS => 96.0 / 25.4, self::UOM_INCHES => 96.0, self::UOM_PIXELS => 1.0, self::UOM_POINTS => 96.0 / 72, self::UOM_PICA => 96.0 * 12 / 72];
    /**
     * Based on a standard column width of 8.54 units in MS Excel.
     */
    const RELATIVE_UNITS = ['em' => 10.0 / 8.539999999999999, 'ex' => 10.0 / 8.539999999999999, 'ch' => 10.0 / 8.539999999999999, 'rem' => 10.0 / 8.539999999999999, 'vw' => 8.539999999999999, 'vh' => 8.539999999999999, 'vmin' => 8.539999999999999, 'vmax' => 8.539999999999999, '%' => 8.539999999999999 / 100];
    /**
     * @var float|int If this is a width, then size is measured in pixels (if is set)
     *                   or in Excel's default column width units if $unit is null.
     *                If this is a height, then size is measured in pixels ()
     *                   or in points () if $unit is null.
     */
    protected $size;
    /**
     * @var null|string
     */
    protected $unit;
    public function __construct(string $dimension)
    {
    }
    public function width() : float
    {
    }
    public function height() : float
    {
    }
    public function toUnit(string $unitOfMeasure) : float
    {
    }
}
