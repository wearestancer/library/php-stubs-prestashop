<?php

namespace PhpOffice\PhpSpreadsheet;

class Settings
{
    /**
     * Set the locale code to use for formula translations and any special formatting.
     *
     * @param string $locale The locale code to use (e.g. "fr" or "pt_br" or "en_uk")
     *
     * @return bool Success or failure
     */
    public static function setLocale(string $locale)
    {
    }
    public static function getLocale() : string
    {
    }
    /**
     * Identify to PhpSpreadsheet the external library to use for rendering charts.
     *
     * @param string $rendererClassName Class name of the chart renderer
     *    eg: PhpOffice\PhpSpreadsheet\Chart\Renderer\JpGraph
     */
    public static function setChartRenderer(string $rendererClassName) : void
    {
    }
    /**
     * Return the Chart Rendering Library that PhpSpreadsheet is currently configured to use.
     *
     * @return null|string Class name of the chart renderer
     *    eg: PhpOffice\PhpSpreadsheet\Chart\Renderer\JpGraph
     */
    public static function getChartRenderer() : ?string
    {
    }
    public static function htmlEntityFlags() : int
    {
    }
    /**
     * Set default options for libxml loader.
     *
     * @param int $options Default options for libxml loader
     */
    public static function setLibXmlLoaderOptions($options) : void
    {
    }
    /**
     * Get default options for libxml loader.
     * Defaults to LIBXML_DTDLOAD | LIBXML_DTDATTR when not set explicitly.
     *
     * @return int Default options for libxml loader
     */
    public static function getLibXmlLoaderOptions() : int
    {
    }
    /**
     * Enable/Disable the entity loader for libxml loader.
     * Allow/disallow libxml_disable_entity_loader() call when not thread safe.
     * Default behaviour is to do the check, but if you're running PHP versions
     *      7.2 < 7.2.1
     * then you may need to disable this check to prevent unwanted behaviour in other threads
     * SECURITY WARNING: Changing this flag to false is not recommended.
     *
     * @param bool $state
     */
    public static function setLibXmlDisableEntityLoader($state) : void
    {
    }
    /**
     * Return the state of the entity loader (disabled/enabled) for libxml loader.
     *
     * @return bool $state
     */
    public static function getLibXmlDisableEntityLoader() : bool
    {
    }
    /**
     * Sets the implementation of cache that should be used for cell collection.
     */
    public static function setCache(\Psr\SimpleCache\CacheInterface $cache) : void
    {
    }
    /**
     * Gets the implementation of cache that is being used for cell collection.
     */
    public static function getCache() : \Psr\SimpleCache\CacheInterface
    {
    }
    /**
     * Set the HTTP client implementation to be used for network request.
     */
    public static function setHttpClient(\Psr\Http\Client\ClientInterface $httpClient, \Psr\Http\Message\RequestFactoryInterface $requestFactory) : void
    {
    }
    /**
     * Unset the HTTP client configuration.
     */
    public static function unsetHttpClient() : void
    {
    }
    /**
     * Get the HTTP client implementation to be used for network request.
     */
    public static function getHttpClient() : \Psr\Http\Client\ClientInterface
    {
    }
    /**
     * Get the HTTP request factory.
     */
    public static function getRequestFactory() : \Psr\Http\Message\RequestFactoryInterface
    {
    }
}
