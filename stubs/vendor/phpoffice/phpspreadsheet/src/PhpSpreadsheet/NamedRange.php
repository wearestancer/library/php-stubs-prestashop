<?php

namespace PhpOffice\PhpSpreadsheet;

class NamedRange extends \PhpOffice\PhpSpreadsheet\DefinedName
{
    /**
     * Create a new Named Range.
     */
    public function __construct(string $name, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet = null, string $range = 'A1', bool $localOnly = false, ?\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $scope = null)
    {
    }
    /**
     * Get the range value.
     */
    public function getRange() : string
    {
    }
    /**
     * Set the range value.
     */
    public function setRange(string $range) : self
    {
    }
    public function getCellsInRange() : array
    {
    }
}
