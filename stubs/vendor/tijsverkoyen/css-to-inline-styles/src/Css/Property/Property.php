<?php

namespace TijsVerkoyen\CssToInlineStyles\Css\Property;

final class Property
{
    /**
     * Property constructor.
     * @param string           $name
     * @param string           $value
     * @param Specificity|null $specificity
     */
    public function __construct($name, $value, \Symfony\Component\CssSelector\Node\Specificity $specificity = null)
    {
    }
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
    }
    /**
     * Get originalSpecificity
     *
     * @return Specificity
     */
    public function getOriginalSpecificity()
    {
    }
    /**
     * Is this property important?
     *
     * @return bool
     */
    public function isImportant()
    {
    }
    /**
     * Get the textual representation of the property
     *
     * @return string
     */
    public function toString()
    {
    }
}
