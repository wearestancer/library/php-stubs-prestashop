<?php

namespace TijsVerkoyen\CssToInlineStyles\Css\Property;

class Processor
{
    /**
     * Split a string into separate properties
     *
     * @param string $propertiesString
     *
     * @return string[]
     */
    public function splitIntoSeparateProperties($propertiesString)
    {
    }
    /**
     * Converts a property-string into an object
     *
     * @param string $property
     *
     * @return Property|null
     */
    public function convertToObject($property, \Symfony\Component\CssSelector\Node\Specificity $specificity = null)
    {
    }
    /**
     * Converts an array of property-strings into objects
     *
     * @param string[] $properties
     *
     * @return Property[]
     */
    public function convertArrayToObjects(array $properties, \Symfony\Component\CssSelector\Node\Specificity $specificity = null)
    {
    }
    /**
     * Build the property-string for multiple properties
     *
     * @param Property[] $properties
     *
     * @return string
     */
    public function buildPropertiesString(array $properties)
    {
    }
}
