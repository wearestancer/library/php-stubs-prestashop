<?php

namespace TijsVerkoyen\CssToInlineStyles\Css\Rule;

final class Rule
{
    /**
     * Rule constructor.
     *
     * @param string      $selector
     * @param Property[]  $properties
     * @param Specificity $specificity
     * @param int         $order
     */
    public function __construct($selector, array $properties, \Symfony\Component\CssSelector\Node\Specificity $specificity, $order)
    {
    }
    /**
     * Get selector
     *
     * @return string
     */
    public function getSelector()
    {
    }
    /**
     * Get properties
     *
     * @return Property[]
     */
    public function getProperties()
    {
    }
    /**
     * Get specificity
     *
     * @return Specificity
     */
    public function getSpecificity()
    {
    }
    /**
     * Get order
     *
     * @return int
     */
    public function getOrder()
    {
    }
}
