<?php

namespace Negotiation;

abstract class AbstractNegotiator
{
    /**
     * @param string $header     A string containing an `Accept|Accept-*` header.
     * @param array  $priorities A set of server priorities.
     *
     * @return AcceptHeader|null best matching type
     */
    public function getBest($header, array $priorities, $strict = false)
    {
    }
    /**
     * @param string $header A string containing an `Accept|Accept-*` header.
     *
     * @return AcceptHeader[] An ordered list of accept header elements
     */
    public function getOrderedElements($header)
    {
    }
    /**
     * @param string $header accept header part or server priority
     *
     * @return AcceptHeader Parsed header object
     */
    protected abstract function acceptFactory($header);
    /**
     * @param AcceptHeader $header
     * @param AcceptHeader $priority
     * @param integer      $index
     *
     * @return AcceptMatch|null Headers matched
     */
    protected function match(\Negotiation\AcceptHeader $header, \Negotiation\AcceptHeader $priority, $index)
    {
    }
}
