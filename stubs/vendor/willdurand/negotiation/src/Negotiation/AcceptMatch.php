<?php

namespace Negotiation;

final class AcceptMatch
{
    /**
     * @var float
     */
    public $quality;
    /**
     * @var int
     */
    public $score;
    /**
     * @var int
     */
    public $index;
    public function __construct($quality, $score, $index)
    {
    }
    /**
     * @param AcceptMatch $a
     * @param AcceptMatch $b
     *
     * @return int
     */
    public static function compare(\Negotiation\AcceptMatch $a, \Negotiation\AcceptMatch $b)
    {
    }
    /**
     * @param array   $carry reduced array
     * @param AcceptMatch $match match to be reduced
     *
     * @return AcceptMatch[]
     */
    public static function reduce(array $carry, \Negotiation\AcceptMatch $match)
    {
    }
}
