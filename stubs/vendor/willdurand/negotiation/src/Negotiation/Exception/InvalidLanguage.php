<?php

namespace Negotiation\Exception;

class InvalidLanguage extends \RuntimeException implements \Negotiation\Exception\Exception
{
}
