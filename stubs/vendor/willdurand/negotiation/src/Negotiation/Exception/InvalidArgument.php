<?php

namespace Negotiation\Exception;

class InvalidArgument extends \InvalidArgumentException implements \Negotiation\Exception\Exception
{
}
