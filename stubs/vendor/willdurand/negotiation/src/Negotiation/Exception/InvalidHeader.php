<?php

namespace Negotiation\Exception;

class InvalidHeader extends \RuntimeException implements \Negotiation\Exception\Exception
{
}
