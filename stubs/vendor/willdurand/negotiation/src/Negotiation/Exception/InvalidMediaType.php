<?php

namespace Negotiation\Exception;

class InvalidMediaType extends \RuntimeException implements \Negotiation\Exception\Exception
{
}
