<?php

namespace Negotiation;

abstract class BaseAccept
{
    /**
     * @var string
     */
    protected $type;
    /**
     * @param string $value
     */
    public function __construct($value)
    {
    }
    /**
     * @return string
     */
    public function getNormalizedValue()
    {
    }
    /**
     * @return string
     */
    public function getValue()
    {
    }
    /**
     * @return string
     */
    public function getType()
    {
    }
    /**
     * @return float
     */
    public function getQuality()
    {
    }
    /**
     * @return array
     */
    public function getParameters()
    {
    }
    /**
     * @param string $key
     * @param mixed  $default
     *
     * @return string|null
     */
    public function getParameter($key, $default = null)
    {
    }
    /**
     * @param string $key
     *
     * @return boolean
     */
    public function hasParameter($key)
    {
    }
}
