<?php

namespace Negotiation;

final class AcceptLanguage extends \Negotiation\BaseAccept implements \Negotiation\AcceptHeader
{
    public function __construct($value)
    {
    }
    /**
     * @return string
     */
    public function getSubPart()
    {
    }
    /**
     * @return string
     */
    public function getBasePart()
    {
    }
}
