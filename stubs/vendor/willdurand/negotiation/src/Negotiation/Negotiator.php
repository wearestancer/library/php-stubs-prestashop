<?php

namespace Negotiation;

class Negotiator extends \Negotiation\AbstractNegotiator
{
    /**
     * {@inheritdoc}
     */
    protected function acceptFactory($accept)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function match(\Negotiation\AcceptHeader $accept, \Negotiation\AcceptHeader $priority, $index)
    {
    }
    /**
     * Split a subpart into the subpart and "plus" part.
     *
     * For media-types of the form "application/vnd.example+json", matching
     * should allow wildcards for either the portion before the "+" or
     * after. This method splits the subpart to allow such matching.
     */
    protected function splitSubPart($subPart)
    {
    }
}
