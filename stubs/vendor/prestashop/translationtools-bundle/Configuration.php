<?php

namespace PrestaShop\TranslationToolsBundle;

class Configuration
{
    /**
     * @param array $arr
     */
    public static function fromArray(array $arr)
    {
    }
    /**
     * @param string $yamlFile
     */
    public static function fromYamlFile($yamlFile)
    {
    }
    /**
     * @return array
     */
    public static function getPaths()
    {
    }
    /**
     * @return array
     */
    public static function getExcludeFiles()
    {
    }
    /**
     * @return string
     */
    public static function getProjectDirectory()
    {
    }
    /**
     * @param string $path
     * @param string|bool $rootDir
     *
     * @return string
     */
    public static function getRelativePath($path, $rootDir = false)
    {
    }
    /**
     * @return string
     */
    public static function getCacheDir()
    {
    }
}
