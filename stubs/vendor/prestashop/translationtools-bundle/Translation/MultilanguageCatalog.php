<?php

namespace PrestaShop\TranslationToolsBundle\Translation;

class MultilanguageCatalog
{
    /**
     * @param string|int $key
     * @param string|int $locale
     *
     * @return bool
     */
    public function has($key, $locale = null)
    {
    }
    /**
     * @param string|int $key
     * @param string|int $locale
     *
     * @return mixed
     */
    public function get($key, $locale = null)
    {
    }
    /**
     * @param string|int $key
     * @param string|int $locale
     * @param mixed $translation
     */
    public function set($key, $locale, $translation)
    {
    }
}
