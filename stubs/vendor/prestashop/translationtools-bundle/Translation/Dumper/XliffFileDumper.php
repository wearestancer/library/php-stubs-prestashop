<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Dumper;

class XliffFileDumper extends \Symfony\Component\Translation\Dumper\XliffFileDumper
{
    protected $relativePathTemplate = '%locale%/%domain%.%extension%';
    /**
     * {@inheritdoc}
     */
    public function dump(\Symfony\Component\Translation\MessageCatalogue $messages, $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function formatCatalogue(\Symfony\Component\Translation\MessageCatalogue $messages, $domain, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExtension()
    {
    }
}
