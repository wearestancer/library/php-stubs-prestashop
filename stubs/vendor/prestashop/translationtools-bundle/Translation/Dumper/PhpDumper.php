<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Dumper;

class PhpDumper extends \Symfony\Component\Translation\Dumper\FileDumper
{
    /**
     * {@inheritdoc}
     */
    public function dump(\Symfony\Component\Translation\MessageCatalogue $messages, $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function formatCatalogue(\Symfony\Component\Translation\MessageCatalogue $messages, $domain, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExtension()
    {
    }
}
