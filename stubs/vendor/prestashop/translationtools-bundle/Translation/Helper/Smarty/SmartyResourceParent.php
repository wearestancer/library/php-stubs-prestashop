<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Helper\Smarty;

/**
 * Stub to support "parent" as a resource in smarty files
 */
class SmartyResourceParent extends \Smarty_Resource_Custom
{
    /**
     * Fetch a template.
     *
     * @param string $name template name
     * @param string $source template source
     * @param int $mtime template modification timestamp (epoch)
     */
    protected function fetch($name, &$source, &$mtime)
    {
    }
}
