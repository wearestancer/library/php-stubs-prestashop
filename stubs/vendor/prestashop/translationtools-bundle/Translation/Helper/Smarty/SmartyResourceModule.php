<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Helper\Smarty;

/**
 * Stub to support "module" as a resource in smarty files
 */
class SmartyResourceModule extends \Smarty_Resource_Custom
{
    /**
     * Fetch a template.
     *
     * @param string $name template name
     * @param string $source template source
     * @param int $mtime template modification timestamp (epoch)
     */
    protected function fetch($name, &$source, &$mtime)
    {
    }
}
