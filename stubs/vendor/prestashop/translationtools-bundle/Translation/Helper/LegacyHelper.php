<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Helper;

class LegacyHelper
{
    /**
     * @param string $inputFilename NB: Remove the working directory (eg: controllers/foo|themes/bar)
     *
     * @return array|null
     */
    public static function getOutputInfo($inputFilename)
    {
    }
    /**
     * @param string $string
     *
     * @return string
     */
    public static function getKey($string)
    {
    }
    /**
     * @param string $file
     *
     * @return string|null
     */
    public static function getKeyPrefix($file)
    {
    }
}
