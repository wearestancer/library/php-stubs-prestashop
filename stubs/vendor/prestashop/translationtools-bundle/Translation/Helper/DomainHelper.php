<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Helper;

class DomainHelper
{
    /**
     * @param string $localPath
     *
     * @return string
     */
    public static function getCrowdinPath($localPath)
    {
    }
    /**
     * @param string $domain
     *
     * @return string
     */
    public static function getExportPath($domain)
    {
    }
    /**
     * @param string $exportPath
     *
     * @return string
     */
    public static function getDomain($exportPath)
    {
    }
    /**
     * Builds a module domain name for the legacy system
     *
     * @param string $moduleName Name of the module (eg. ps_themecusto)
     * @param string $sourceFileName Filename where the wording was found (eg. someFile.tpl)
     *
     * @return string The domain name (eg. Modules.Psthemecusto.somefile)
     */
    public static function buildModuleDomainFromLegacySource($moduleName, $sourceFileName)
    {
    }
    /**
     * Returns the base domain for the provided module name
     *
     * @param string $moduleName
     * @param bool $withDots True to use separating dots
     *
     * @return string
     */
    public static function buildModuleBaseDomain($moduleName, $withDots = false)
    {
    }
}
