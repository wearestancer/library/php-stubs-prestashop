<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Builder;

class XliffBuilder
{
    /**
     * @var DOMDocument
     */
    protected $dom;
    /**
     * @var string
     */
    protected $version;
    /**
     * @var array
     */
    protected $originalFiles = [];
    /**
     * @var array
     */
    protected $transUnits = [];
    public function __construct()
    {
    }
    /**
     * @return DOMDocument
     */
    public function build()
    {
    }
    /**
     * @param string $filename
     * @param string $sourceLanguage
     * @param string $targetLanguage
     *
     * @return \PrestaShop\TranslationToolsBundle\Translation\Builder\XliffBuilder
     */
    public function addFile($filename, $sourceLanguage, $targetLanguage)
    {
    }
    /**
     * @param string $filename
     * @param string $source
     * @param string $target
     * @param string $note
     *
     * @return \PrestaShop\TranslationToolsBundle\Translation\Builder\XliffBuilder
     */
    public function addTransUnit($filename, $source, $target, $note)
    {
    }
    /**
     * @param string $version
     *
     * @return \PrestaShop\TranslationToolsBundle\Translation\Builder\XliffBuilder
     */
    public function setVersion($version)
    {
    }
}
