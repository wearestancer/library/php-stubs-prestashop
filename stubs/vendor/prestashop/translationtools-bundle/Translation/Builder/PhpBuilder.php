<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Builder;

class PhpBuilder
{
    public const POS_NEWLINE = 0;
    public const POS_VAR = 1;
    public const POS_ARRAY_KEY = 2;
    public const POS_ASSIGN = 3;
    public const POS_VALUE = 4;
    protected $fileName;
    protected $output;
    protected $pos = self::POS_NEWLINE;
    public function __construct()
    {
    }
    /**
     * @return string
     */
    public function build()
    {
    }
    /**
     * @param string $varName
     */
    public function appendGlobalDeclaration($varName)
    {
    }
    /**
     * @param string $varName
     * @param string $key
     * @param string $value
     *
     * @return \PrestaShop\TranslationToolsBundle\Translation\Builder\PhpBuilder
     *
     * @throws Exception
     */
    public function appendStringLine($varName, $key, $value)
    {
    }
    /**
     * @return PhpBuilder
     */
    protected function open()
    {
    }
    /**
     * @param string $varName
     *
     * @return PhpBuilder
     */
    protected function appendVar($varName)
    {
    }
    /**
     * @param string $key
     *
     * @return PhpBuilder
     */
    protected function appendKey($key)
    {
    }
    /**
     * @return PhpBuilder
     */
    protected function appendVarAssignation()
    {
    }
    /**
     * @param string $value
     *
     * @return PhpBuilder
     */
    protected function appendValue($value)
    {
    }
    /**
     * @return PhpBuilder
     */
    protected function appendEndOfLine()
    {
    }
}
