<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor;

class ChainExtractor extends \Symfony\Component\Translation\Extractor\ChainExtractor
{
    /**
     * @param string $format
     *
     * @return self
     */
    public function addExtractor($format, \Symfony\Component\Translation\Extractor\ExtractorInterface $extractor)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($directory, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
}
