<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor;

class CrowdinPhpExtractor extends \Symfony\Component\Translation\Extractor\AbstractFileExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    use \PrestaShop\TranslationToolsBundle\Translation\Extractor\TraitExtractor;
    public function __construct(\PrestaShop\TranslationToolsBundle\Translation\Parser\CrowdinPhpParser $crodwinPhpParser, \PrestaShop\TranslationToolsBundle\Translation\Manager\OriginalStringManager $originalStringManager)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($resource, \Symfony\Component\Translation\MessageCatalogue $catalog)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
    }
    /**
     * @param string $file
     *
     * @throws \InvalidArgumentException
     *
     * @return bool
     */
    protected function canBeExtracted($file)
    {
    }
    /**
     * @param string|array $directory
     *
     * @return array
     */
    protected function extractFromDirectory($directory)
    {
    }
}
