<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor;

trait TraitExtractor
{
    protected $defaultDomain = 'messages';
    /**
     * @var Finder
     */
    protected $finder;
    /**
     * Directories ignored when scanning files for catalogue extraction.
     *
     * @var array
     */
    protected $excludedDirectories = [];
    public function getExcludedDirectories() : array
    {
    }
    public function setExcludedDirectories(array $excludedDirectories) : self
    {
    }
    protected function resolveDomain(?string $domainName) : string
    {
    }
    /**
     * Retrieves comments on the same line as translation string
     */
    public function getEntryComment(array $comments, string $file, int $line) : ?string
    {
    }
    public function setFinder(\Symfony\Component\Finder\Finder $finder) : self
    {
    }
    public function getFinder() : \Symfony\Component\Finder\Finder
    {
    }
}
