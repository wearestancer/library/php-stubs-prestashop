<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor;

class TwigExtractor extends \Symfony\Bridge\Twig\Translation\TwigExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    use \PrestaShop\TranslationToolsBundle\Translation\Extractor\TraitExtractor;
    /**
     * The twig environment.
     *
     * @var Environment
     */
    public function __construct(\Twig\Environment $twig)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($resource, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractTemplateFile($file, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    /**
     * @param string $directory
     */
    protected function extractFromDirectory($directory) : \Symfony\Component\Finder\Finder
    {
    }
}
