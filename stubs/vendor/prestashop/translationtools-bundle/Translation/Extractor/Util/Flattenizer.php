<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Util;

/**
 * This class have only one thing to do, transform:
 *
 * en-US
 *   ├── Admin
 *   │   └── Catalog
 *   │        ├── Feature.xlf
 *   │        ├── Help.xlf
 *   │        └── Notification.xlf
 *   └── Shop
 *        └── PDF.xlf
 *
 *
 * Into:
 *
 * ├── AdminCatalogFeature.en-US.xlf
 * ├── AdminCatalogHelp.en-US.xlf
 * ├── AdminCatalogNotification.en-US.xlf
 * └── ShopPDF.en-US.xlf
 */
class Flattenizer
{
    public static $finder = null;
    public static $filesystem = null;
    /**
     * @input string $inputPath Path of directory to flattenize
     * @input string $outputPath Location of flattenized files newly created
     * @input string $locale Selected locale for theses files.
     * @input boolean $cleanPath Clean input path after flatten.
     */
    public static function flatten($inputPath, $outputPath, $locale, $cleanPath = true)
    {
    }
    /**
     * @param SplFileInfo $files List of files to flattenize
     * @param string $outputPath Location of flattenized files newly created
     * @param string $locale Selected locale for theses files
     * @param Filesystem $filesystem Instance of Filesystem
     * @param bool $addLocale Should add the locale to filename
     *
     * @return bool
     */
    public static function flattenFiles($files, $outputPath, $locale, $filesystem, $addLocale = true)
    {
    }
}
