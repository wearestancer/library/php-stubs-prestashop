<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor;

class SmartyExtractor extends \Symfony\Component\Translation\Extractor\AbstractFileExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    use \PrestaShop\TranslationToolsBundle\Translation\Extractor\TraitExtractor;
    public const INCLUDE_EXTERNAL_MODULES = true;
    public const EXCLUDE_EXTERNAL_MODULES = false;
    /**
     * @param bool $includeExternalWordings Set to SmartyCompiler::INCLUDE_EXTERNAL_MODULES to include wordings signed with 'mod' (external modules)
     */
    public function __construct(\PrestaShop\TranslationToolsBundle\Translation\Compiler\Smarty\TranslationTemplateCompiler $smartyCompiler, $includeExternalWordings = self::EXCLUDE_EXTERNAL_MODULES)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($resource, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    protected function extractFromFile(\SplFileInfo $resource, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function canBeExtracted($file)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractFromDirectory($directory)
    {
    }
}
