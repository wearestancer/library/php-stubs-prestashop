<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor;

class PhpExtractor extends \Symfony\Component\Translation\Extractor\AbstractFileExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    use \PrestaShop\TranslationToolsBundle\Translation\Extractor\TraitExtractor;
    /**
     * @var array
     */
    protected $visitors = [];
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($resource, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
    }
    /**
     * @param $file
     *
     * @throws \Exception
     */
    protected function parseFileTokens($file, \Symfony\Component\Translation\MessageCatalogue $catalog)
    {
    }
    /**
     * @param string $file
     *
     * @throws \InvalidArgumentException
     *
     * @return bool
     */
    protected function canBeExtracted($file)
    {
    }
    /**
     * @param string|array $directory
     *
     * @return array
     */
    protected function extractFromDirectory($directory)
    {
    }
}
