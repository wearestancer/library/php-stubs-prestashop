<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor;

/**
 * Extracts comment information
 */
class CommentsNodeVisitor extends \PhpParser\NodeVisitorAbstract
{
    protected $file;
    /**
     * @var array
     */
    protected $comments = [];
    /**
     * TranslationNodeVisitor constructor.
     *
     * @param $file
     */
    public function __construct($file)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function leaveNode(\PhpParser\Node $node)
    {
    }
    /**
     * @return array
     */
    public function getComments()
    {
    }
}
