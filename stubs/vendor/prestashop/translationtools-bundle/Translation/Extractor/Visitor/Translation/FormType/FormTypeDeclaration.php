<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\FormType;

/**
 * Extracts wordings from FormType classes
 *
 * Only supports wordings in ChoiceType for now
 */
class FormTypeDeclaration extends \PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\AbstractTranslationNodeVisitor
{
    /**
     * FQCN to symfony form AbstractType, separated into parts
     */
    public const SUPPORTED_FORM_TYPES = [['Symfony', 'Component', 'Form', 'AbstractType'], ['PrestaShopBundle', 'Form', 'Admin', 'Type', 'TranslatorAwareType']];
    /**
     * Types of nodes that we are interested in inspecting
     */
    public const INTERESTING_NODE_TYPES = [\PhpParser\Node\Stmt\Use_::class, \PhpParser\Node\Stmt\Class_::class, \PhpParser\Node\Stmt\ClassMethod::class, \PhpParser\Node\Expr\MethodCall::class];
    public function __construct(\PrestaShop\TranslationToolsBundle\Translation\Extractor\Util\TranslationCollection $collection)
    {
    }
    public function enterNode(\PhpParser\Node $node)
    {
    }
    public function afterTraverse(array $nodes)
    {
    }
}
