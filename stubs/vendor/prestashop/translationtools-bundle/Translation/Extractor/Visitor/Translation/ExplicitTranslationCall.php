<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation;

/**
 * Looks up for a translation call using l(), trans() or t()
 */
class ExplicitTranslationCall extends \PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\AbstractTranslationNodeVisitor
{
    public const SUPPORTED_METHODS = ['l', 'trans', 't'];
    public function leaveNode(\PhpParser\Node $node)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extractFrom(\PhpParser\Node $node)
    {
    }
}
