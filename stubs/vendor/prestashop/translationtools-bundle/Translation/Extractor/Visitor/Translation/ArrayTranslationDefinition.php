<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation;

/**
 * This class looks for arrays like this:
 *
 * ```php
 * [
 *     'key' => 'This text is lonely',
 *     'parameters' => [],
 *     'domain' => 'Admin.Notifications.Error',
 * ]
 *
 * [
 *     'key' => 'This text is lonely',
 *     'domain' => 'Admin.Notifications.Error',
 * ]
 * ```
 *
 * Parameters can be in any order
 */
class ArrayTranslationDefinition extends \PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\AbstractTranslationNodeVisitor
{
    public function leaveNode(\PhpParser\Node $node)
    {
    }
    /**
     * @return array Array of translations to add
     */
    public function extractFrom(\PhpParser\Node $node)
    {
    }
}
