<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\FormType;

/**
 * Extracts choices from a choice declaration.
 *
 * A choice declaration looks like this:
 *
 * ```php
 * ->add('default_order_way', ChoiceType::class, [
 *     'choices' => [
 *         'Ascending' => 0,
 *         'Descending' => 1,
 *     ],
 *     'required' => true,
 *     'choice_translation_domain' => 'Admin.Global',
 * ]);
 * ```
 */
class ChoiceExtractor
{
    public const METHOD_NAME = 'add';
    public const EXPECTED_ARG_COUNT = 3;
    public const CLASS_ARG_INDEX = 1;
    public const OPTIONS_ARG_INDEX = 2;
    public const OPTION_NAME_CHOICES = 'choices';
    public const OPTION_NAME_TRANSLATION_DOMAIN = 'choice_translation_domain';
    public const CHOICE_CLASS_NAME = 'ChoiceType';
    public function __construct(\PhpParser\Node $node, $defaultTranslationDomain = '')
    {
    }
    /**
     * @return bool
     */
    public function isChoiceDeclaration()
    {
    }
    /**
     * Returns the list of choices as translation items.
     *
     * Note: the translation domain is only
     *
     * @return array[] Translation items
     */
    public function getChoiceWordings()
    {
    }
}
