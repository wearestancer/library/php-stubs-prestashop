<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\FormType;

/**
 * This class looks for a default translation domain declaration.
 *
 * It looks like this:
 *
 * ```php
 * public function configureOptions(OptionsResolver $resolver)
 * {
 *     $resolver->setDefaults([
 *         'translation_domain' => 'Admin.Shopparameters.Feature',
 *     ]);
 * }
 * ```
 */
class DefaultTranslationDomainExtractor
{
    public const CONFIGURE_OPTIONS = 'configureOptions';
    /**
     * Types of nodes that we are interested in inspecting
     */
    public const INTERESTING_NODE_TYPES = [\PhpParser\Node\Stmt\ClassMethod::class, \PhpParser\Node\Expr\MethodCall::class];
    /**
     * Name of the method that sets default settings
     */
    public const SET_DEFAULTS_DECLARATION_METHOD_NAME = 'setDefaults';
    /**
     * Index of the OptionsResolver parameter in the configureOptions method declaration
     */
    public const OPTIONS_RESOLVER_PARAM_INDEX = 0;
    /**
     * @return bool
     */
    public function lookForDefaultTranslationDomain(\PhpParser\Node $node)
    {
    }
    /**
     * @return bool
     */
    public function defaultDomainHasBeenFound()
    {
    }
    /**
     * @return string
     */
    public function getDefaultTranslationDomain()
    {
    }
}
