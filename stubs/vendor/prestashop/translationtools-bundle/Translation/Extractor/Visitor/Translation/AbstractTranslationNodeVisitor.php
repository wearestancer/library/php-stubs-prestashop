<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation;

abstract class AbstractTranslationNodeVisitor extends \PhpParser\NodeVisitorAbstract implements \PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation\TranslationVisitorInterface
{
    /**
     * @var TranslationCollection
     */
    protected $translations;
    public function __construct(\PrestaShop\TranslationToolsBundle\Translation\Extractor\Util\TranslationCollection $collection)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getTranslationCollection()
    {
    }
}
