<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Extractor\Visitor\Translation;

interface TranslationVisitorInterface
{
    /**
     * @return TranslationCollection
     */
    public function getTranslationCollection();
}
