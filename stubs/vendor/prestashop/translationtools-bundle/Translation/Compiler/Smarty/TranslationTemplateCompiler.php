<?php

namespace PrestaShop\TranslationToolsBundle\Translation\Compiler\Smarty;

class TranslationTemplateCompiler extends \Smarty_Internal_SmartyTemplateCompiler
{
    /**
     * @var bool
     */
    public $nocache = false;
    /**
     * @var bool
     */
    public $tag_nocache = false;
    /**
     * @return array
     */
    public function getTranslationTags()
    {
    }
    /**
     * @param string $templateFile
     */
    public function setTemplateFile($templateFile)
    {
    }
}
