<?php

namespace PrestaShop\TranslationToolsBundle\Twig\Extension;

class AppExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * AppExtension constructor.
     */
    public function __construct(\Symfony\Component\Translation\TranslatorInterface $translation)
    {
    }
    /**
     * We need to define and reset each twig function as the definition
     * of theses function is stored in PrestaShop codebase.
     */
    public function getFunctions()
    {
    }
    /**
     * @param $string
     *
     * @return string
     */
    public function transChoice($string)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
