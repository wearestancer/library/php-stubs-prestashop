<?php

namespace PrestaShop\TranslationToolsBundle\Twig\Extension;

class TranslationExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * @var BaseTranslationExtension
     */
    protected $baseTranslationExtension;
    /**
     * @var TranslationNodeVisitor
     */
    protected $nodeVisitors;
    public function __construct()
    {
    }
    public function getTokenParsers()
    {
    }
    public function getNodeVisitors()
    {
    }
    public function getTranslationNodeVisitor()
    {
    }
    public function getFilters()
    {
    }
    public function getTests()
    {
    }
    public function getFunctions()
    {
    }
    public function getOperators()
    {
    }
}
