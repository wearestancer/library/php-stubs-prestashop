<?php

namespace PrestaShop\TranslationToolsBundle\Twig\NodeVisitor;

class TranslationNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
{
    public function __construct()
    {
    }
    public function enable()
    {
    }
    public function disable()
    {
    }
    public function getMessages()
    {
    }
    protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : ?\Twig\Node\Node
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPriority() : int
    {
    }
}
