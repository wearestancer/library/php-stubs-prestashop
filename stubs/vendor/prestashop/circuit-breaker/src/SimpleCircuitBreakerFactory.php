<?php

namespace PrestaShop\CircuitBreaker;

/**
 * Main implementation of Circuit Breaker Factory
 * Used to create a SimpleCircuitBreaker instance.
 */
final class SimpleCircuitBreakerFactory implements \PrestaShop\CircuitBreaker\Contract\FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(\PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settings) : \PrestaShop\CircuitBreaker\Contract\CircuitBreakerInterface
    {
    }
}
