<?php

namespace PrestaShop\CircuitBreaker;

/**
 * Main implementation of Circuit Breaker.
 */
final class SimpleCircuitBreaker extends \PrestaShop\CircuitBreaker\PartialCircuitBreaker
{
    public function __construct(\PrestaShop\CircuitBreaker\Contract\PlaceInterface $openPlace, \PrestaShop\CircuitBreaker\Contract\PlaceInterface $halfOpenPlace, \PrestaShop\CircuitBreaker\Contract\PlaceInterface $closedPlace, \PrestaShop\CircuitBreaker\Contract\ClientInterface $client)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function call(string $service, array $serviceParameters = [], callable $fallback = null) : string
    {
    }
}
