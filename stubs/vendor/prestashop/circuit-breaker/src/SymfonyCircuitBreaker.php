<?php

namespace PrestaShop\CircuitBreaker;

/**
 * Symfony implementation of Circuit Breaker.
 */
final class SymfonyCircuitBreaker extends \PrestaShop\CircuitBreaker\AdvancedCircuitBreaker
{
    public function __construct(\PrestaShop\CircuitBreaker\Contract\SystemInterface $system, \PrestaShop\CircuitBreaker\Contract\ClientInterface $client, \PrestaShop\CircuitBreaker\Contract\StorageInterface $storage, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
    }
}
