<?php

namespace PrestaShop\CircuitBreaker;

/**
 * This implementation of the CircuitBreaker is a bit more advanced than the SimpleCircuitBreaker,
 * it allows you to setup your client, system, storage and dispatcher.
 */
class AdvancedCircuitBreaker extends \PrestaShop\CircuitBreaker\PartialCircuitBreaker
{
    /** @var TransitionDispatcherInterface */
    protected $dispatcher;
    /** @var callable|null */
    protected $defaultFallback;
    public function __construct(\PrestaShop\CircuitBreaker\Contract\SystemInterface $system, \PrestaShop\CircuitBreaker\Contract\ClientInterface $client, \PrestaShop\CircuitBreaker\Contract\StorageInterface $storage, \PrestaShop\CircuitBreaker\Contract\TransitionDispatcherInterface $dispatcher)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function call($service, array $serviceParameters = [], callable $fallback = null) : string
    {
    }
    public function getDefaultFallback() : ?callable
    {
    }
    public function setDefaultFallback(?callable $defaultFallback = null) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function callFallback(callable $fallback = null) : string
    {
    }
    protected function dispatchTransition(string $transition, string $service, array $serviceParameters) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initTransaction(string $service) : \PrestaShop\CircuitBreaker\Contract\TransactionInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function request(string $service, array $parameters = []) : string
    {
    }
}
