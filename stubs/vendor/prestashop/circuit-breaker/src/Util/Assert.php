<?php

namespace PrestaShop\CircuitBreaker\Util;

/**
 * Util class to handle object validation
 * Should be deprecated for most parts once
 * the library will drop PHP5 support.
 */
final class Assert
{
    /**
     * @param mixed $value the value to evaluate
     */
    public static function isPositiveValue($value) : bool
    {
    }
    /**
     * @param mixed $value the value to evaluate
     */
    public static function isPositiveInteger($value) : bool
    {
    }
    /**
     * @param mixed $value the value to evaluate
     */
    public static function isURI($value) : bool
    {
    }
    /**
     * @param mixed $value the value to evaluate
     */
    public static function isString($value) : bool
    {
    }
}
