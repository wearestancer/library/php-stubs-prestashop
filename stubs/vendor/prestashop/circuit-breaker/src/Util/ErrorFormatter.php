<?php

namespace PrestaShop\CircuitBreaker\Util;

/**
 * Helper to provide complete and easy to read
 * error messages.
 * Mostly used to build Exception messages.
 */
final class ErrorFormatter
{
    /**
     * Format error message.
     *
     * @param string $parameter the parameter to evaluate
     * @param mixed $value the value to format
     * @param string $function the validation function
     * @param string $expectedType the expected type
     */
    public static function format(string $parameter, $value, string $function, string $expectedType) : string
    {
    }
}
