<?php

namespace PrestaShop\CircuitBreaker\Contract;

/**
 * This interface is used for a circuit breaker to dispatch its state transitions.
 */
interface TransitionDispatcherInterface
{
    /**
     * @param string $transition the transition name
     * @param string $service the Service URI
     * @param array $serviceParameters the Service parameters
     */
    public function dispatchTransition(string $transition, string $service, array $serviceParameters) : void;
}
