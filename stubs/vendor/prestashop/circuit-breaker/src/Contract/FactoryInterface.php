<?php

namespace PrestaShop\CircuitBreaker\Contract;

/**
 * Ease the creation of the Circuit Breaker.
 */
interface FactoryInterface
{
    /**
     * @param FactorySettingsInterface $settings the settings for the Place
     */
    public function create(\PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settings) : \PrestaShop\CircuitBreaker\Contract\CircuitBreakerInterface;
}
