<?php

namespace PrestaShop\CircuitBreaker\Contract;

/**
 * Store the transaction between the Circuit Breaker
 * and the tiers service.
 */
interface StorageInterface
{
    /**
     * Save the CircuitBreaker transaction.
     *
     * @param string $service The service name
     * @param TransactionInterface $transaction the transaction
     */
    public function saveTransaction(string $service, \PrestaShop\CircuitBreaker\Contract\TransactionInterface $transaction) : bool;
    /**
     * Retrieve the CircuitBreaker transaction for a specific service.
     *
     * @param string $service the service name
     *
     * @throws TransactionNotFoundException
     */
    public function getTransaction(string $service) : \PrestaShop\CircuitBreaker\Contract\TransactionInterface;
    /**
     * Checks if the transaction exists.
     *
     * @param string $service the service name
     */
    public function hasTransaction(string $service) : bool;
    /**
     * Clear the Circuit Breaker storage.
     */
    public function clear() : bool;
}
