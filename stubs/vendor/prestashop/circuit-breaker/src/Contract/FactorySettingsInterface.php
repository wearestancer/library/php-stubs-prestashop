<?php

namespace PrestaShop\CircuitBreaker\Contract;

/**
 * Interface FactorySettingsInterface contains the settings used by the Factory
 */
interface FactorySettingsInterface
{
    public static function merge(\PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settingsA, \PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settingsB) : \PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface;
    public function getFailures() : int;
    public function getTimeout() : float;
    public function getThreshold() : int;
    public function getStrippedTimeout() : float;
    public function getStrippedFailures() : int;
    public function getStorage() : ?\PrestaShop\CircuitBreaker\Contract\StorageInterface;
    public function getDispatcher() : ?\PrestaShop\CircuitBreaker\Contract\TransitionDispatcherInterface;
    public function getClientOptions() : array;
    public function getClient() : ?\PrestaShop\CircuitBreaker\Contract\ClientInterface;
    public function getDefaultFallback() : ?callable;
}
