<?php

namespace PrestaShop\CircuitBreaker\Contract;

/**
 * A circuit breaker can be in 3 places:
 * closed, half open or open. Each place have its
 * own properties and behaviors.
 */
interface PlaceInterface
{
    /**
     * Return the current state of the Circuit Breaker.
     */
    public function getState() : string;
    /**
     * @return int the number of failures
     */
    public function getFailures() : int;
    /**
     * @return int the allowed number of trials
     */
    public function getThreshold() : int;
    /**
     * @return float the allowed timeout
     */
    public function getTimeout() : float;
}
