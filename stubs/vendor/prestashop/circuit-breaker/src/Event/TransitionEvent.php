<?php

namespace PrestaShop\CircuitBreaker\Event;

class TransitionEvent extends \Symfony\Component\EventDispatcher\Event
{
    /**
     * @param string $eventName the transition name
     * @param string $service the Service URI
     * @param array $parameters the Service parameters
     */
    public function __construct(string $eventName, string $service, array $parameters)
    {
    }
    /**
     * @return string the Transition name
     */
    public function getEvent() : string
    {
    }
    /**
     * @return string the Service URI
     */
    public function getService() : string
    {
    }
    /**
     * @return array the Service parameters
     */
    public function getParameters() : array
    {
    }
}
