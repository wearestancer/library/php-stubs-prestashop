<?php

namespace PrestaShop\CircuitBreaker;

/**
 * Advanced implementation of Circuit Breaker Factory
 * Used to create an AdvancedCircuitBreaker instance.
 */
final class AdvancedCircuitBreakerFactory implements \PrestaShop\CircuitBreaker\Contract\FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(\PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settings) : \PrestaShop\CircuitBreaker\Contract\CircuitBreakerInterface
    {
    }
}
