<?php

namespace PrestaShop\CircuitBreaker;

abstract class PartialCircuitBreaker implements \PrestaShop\CircuitBreaker\Contract\CircuitBreakerInterface
{
    public function __construct(\PrestaShop\CircuitBreaker\Contract\SystemInterface $system, \PrestaShop\CircuitBreaker\Contract\ClientInterface $client, \PrestaShop\CircuitBreaker\Contract\StorageInterface $storage)
    {
    }
    /**
     * @var ClientInterface the Client that consumes the service URI
     */
    protected $client;
    /**
     * @var PlaceInterface the current Place of the Circuit Breaker
     */
    protected $currentPlace;
    /**
     * @var PlaceInterface[] the Circuit Breaker places
     */
    protected $places = [];
    /**
     * @var StorageInterface the Circuit Breaker storage
     */
    protected $storage;
    /**
     * {@inheritdoc}
     */
    public abstract function call(string $service, array $serviceParameters = [], callable $fallback = null) : string;
    /**
     * {@inheritdoc}
     */
    public function getState() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isOpened() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isHalfOpened() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isClosed() : bool
    {
    }
    protected function callFallback(callable $fallback = null) : string
    {
    }
    /**
     * @param string $state the Place state
     * @param string $service the service URI
     */
    protected function moveStateTo(string $state, string $service) : bool
    {
    }
    /**
     * @param string $service the service URI
     */
    protected function initTransaction(string $service) : \PrestaShop\CircuitBreaker\Contract\TransactionInterface
    {
    }
    /**
     * @param TransactionInterface $transaction the Transaction
     */
    protected function isAllowedToRetry(\PrestaShop\CircuitBreaker\Contract\TransactionInterface $transaction) : bool
    {
    }
    /**
     * @param TransactionInterface $transaction the Transaction
     */
    protected function canAccessService(\PrestaShop\CircuitBreaker\Contract\TransactionInterface $transaction) : bool
    {
    }
    /**
     * Calls the client with the right information.
     *
     * @param string $service the service URI
     * @param array $parameters the service URI parameters
     */
    protected function request(string $service, array $parameters = []) : string
    {
    }
}
