<?php

namespace PrestaShop\CircuitBreaker\Exception;

final class TransactionNotFoundException extends \PrestaShop\CircuitBreaker\Exception\CircuitBreakerException
{
}
