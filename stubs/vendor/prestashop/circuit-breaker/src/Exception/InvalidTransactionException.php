<?php

namespace PrestaShop\CircuitBreaker\Exception;

final class InvalidTransactionException extends \PrestaShop\CircuitBreaker\Exception\CircuitBreakerException
{
    /**
     * @param mixed $service the service URI
     * @param mixed $failures the failures
     * @param mixed $state the Circuit Breaker
     * @param mixed $threshold the threshold
     */
    public static function invalidParameters($service, $failures, $state, $threshold) : self
    {
    }
}
