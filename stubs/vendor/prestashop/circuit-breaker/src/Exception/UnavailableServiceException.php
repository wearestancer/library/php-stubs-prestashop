<?php

namespace PrestaShop\CircuitBreaker\Exception;

final class UnavailableServiceException extends \PrestaShop\CircuitBreaker\Exception\CircuitBreakerException
{
}
