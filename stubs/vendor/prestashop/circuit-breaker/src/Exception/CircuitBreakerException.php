<?php

namespace PrestaShop\CircuitBreaker\Exception;

/**
 * Base exception for Circuit Breaker exceptions
 */
class CircuitBreakerException extends \Exception
{
}
