<?php

namespace PrestaShop\CircuitBreaker\Exception;

/**
 * Used when trying to use an unsupported HTTP method
 */
class UnsupportedMethodException extends \PrestaShop\CircuitBreaker\Exception\CircuitBreakerException
{
    public static function unsupportedMethod(string $methodName) : self
    {
    }
}
