<?php

namespace PrestaShop\CircuitBreaker\Exception;

final class InvalidPlaceException extends \PrestaShop\CircuitBreaker\Exception\CircuitBreakerException
{
    /**
     * @param mixed $failures the failures
     * @param mixed $timeout the timeout
     * @param mixed $threshold the threshold
     */
    public static function invalidSettings($failures, $timeout, $threshold) : self
    {
    }
}
