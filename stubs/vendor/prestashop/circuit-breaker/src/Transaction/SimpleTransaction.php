<?php

namespace PrestaShop\CircuitBreaker\Transaction;

/**
 * Main implementation of Circuit Breaker transaction.
 */
final class SimpleTransaction implements \PrestaShop\CircuitBreaker\Contract\TransactionInterface
{
    /**
     * @param string $service the service URI
     * @param int $failures the allowed failures
     * @param string $state the circuit breaker state/place
     * @param int $threshold the place threshold
     */
    public function __construct(string $service, int $failures, string $state, int $threshold)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getService() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFailures() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getState() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getThresholdDateTime() : \DateTime
    {
    }
    /**
     * {@inheritdoc}
     */
    public function incrementFailures() : bool
    {
    }
    /**
     * Helper to create a transaction from the Place.
     *
     * @param PlaceInterface $place the Circuit Breaker place
     * @param string $service the service URI
     */
    public static function createFromPlace(\PrestaShop\CircuitBreaker\Contract\PlaceInterface $place, string $service) : self
    {
    }
}
