<?php

namespace PrestaShop\CircuitBreaker\Storage;

/**
 * Very simple implementation of Storage using a simple PHP array.
 */
final class SimpleArray implements \PrestaShop\CircuitBreaker\Contract\StorageInterface
{
    /**
     * @var array the circuit breaker transactions
     */
    public static $transactions = [];
    /**
     * {@inheritdoc}
     */
    public function saveTransaction(string $service, \PrestaShop\CircuitBreaker\Contract\TransactionInterface $transaction) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTransaction(string $service) : \PrestaShop\CircuitBreaker\Contract\TransactionInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasTransaction(string $service) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear() : bool
    {
    }
}
