<?php

namespace PrestaShop\CircuitBreaker\Storage;

/**
 * Implementation of Storage using the Doctrine Cache.
 */
class DoctrineCache implements \PrestaShop\CircuitBreaker\Contract\StorageInterface
{
    public function __construct(\Doctrine\Common\Cache\CacheProvider $cacheProvider)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function saveTransaction(string $service, \PrestaShop\CircuitBreaker\Contract\TransactionInterface $transaction) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTransaction(string $service) : \PrestaShop\CircuitBreaker\Contract\TransactionInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasTransaction(string $service) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear() : bool
    {
    }
}
