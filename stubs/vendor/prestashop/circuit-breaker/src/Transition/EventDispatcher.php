<?php

namespace PrestaShop\CircuitBreaker\Transition;

/**
 * Class EventDispatcher implements the TransitionDispatcher using the Symfony EventDispatcherInterface
 */
class EventDispatcher implements \PrestaShop\CircuitBreaker\Contract\TransitionDispatcherInterface
{
    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatchTransition(string $transition, string $service, array $serviceParameters) : void
    {
    }
}
