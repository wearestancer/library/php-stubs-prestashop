<?php

namespace PrestaShop\CircuitBreaker\Transition;

/**
 * Class NullDispatcher is used when you have no TransitionDispatcher to inject
 * because you don't need it.
 */
class NullDispatcher implements \PrestaShop\CircuitBreaker\Contract\TransitionDispatcherInterface
{
    /**
     * {@inheritdoc}
     */
    public function dispatchTransition(string $transition, string $service, array $serviceParameters) : void
    {
    }
}
