<?php

namespace PrestaShop\CircuitBreaker\System;

/**
 * Implement the system described by the documentation.
 * The main system is built with 3 places:
 * - A Closed place
 * - A Half Open Place
 * - An Open Place
 */
final class MainSystem implements \PrestaShop\CircuitBreaker\Contract\SystemInterface
{
    public function __construct(\PrestaShop\CircuitBreaker\Contract\PlaceInterface $closedPlace, \PrestaShop\CircuitBreaker\Contract\PlaceInterface $halfOpenPlace, \PrestaShop\CircuitBreaker\Contract\PlaceInterface $openPlace)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInitialPlace() : \PrestaShop\CircuitBreaker\Contract\PlaceInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPlaces() : array
    {
    }
}
