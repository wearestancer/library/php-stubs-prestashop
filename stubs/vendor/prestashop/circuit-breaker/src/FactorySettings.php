<?php

namespace PrestaShop\CircuitBreaker;

/**
 * Class FactorySettings is a simple implementation of FactorySettingsInterface, it is mainly
 * a settings container and can be used with any Factory class.
 */
class FactorySettings implements \PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface
{
    public function __construct(int $failures, float $timeout, int $threshold)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function merge(\PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settingsA, \PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface $settingsB) : \PrestaShop\CircuitBreaker\Contract\FactorySettingsInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFailures() : int
    {
    }
    public function setFailures(int $failures) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTimeout() : float
    {
    }
    public function setTimeout(float $timeout) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getThreshold() : int
    {
    }
    public function setThreshold(int $threshold) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStrippedTimeout() : float
    {
    }
    public function setStrippedTimeout(float $strippedTimeout) : self
    {
    }
    public function getStrippedFailures() : int
    {
    }
    public function setStrippedFailures(int $strippedFailures) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStorage() : ?\PrestaShop\CircuitBreaker\Contract\StorageInterface
    {
    }
    public function setStorage(\PrestaShop\CircuitBreaker\Contract\StorageInterface $storage) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDispatcher() : ?\PrestaShop\CircuitBreaker\Contract\TransitionDispatcherInterface
    {
    }
    public function setDispatcher(\PrestaShop\CircuitBreaker\Contract\TransitionDispatcherInterface $dispatcher) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClientOptions() : array
    {
    }
    public function setClientOptions(array $clientOptions) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClient() : ?\PrestaShop\CircuitBreaker\Contract\ClientInterface
    {
    }
    public function setClient(?\PrestaShop\CircuitBreaker\Contract\ClientInterface $client = null) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultFallback() : ?callable
    {
    }
    public function setDefaultFallback(callable $defaultFallback) : self
    {
    }
}
