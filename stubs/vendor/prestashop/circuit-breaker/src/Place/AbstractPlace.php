<?php

namespace PrestaShop\CircuitBreaker\Place;

abstract class AbstractPlace implements \PrestaShop\CircuitBreaker\Contract\PlaceInterface
{
    /**
     * @param int $failures the Place failures
     * @param float $timeout the Place timeout
     * @param int $threshold the Place threshold
     *
     * @throws InvalidPlaceException
     */
    public function __construct(int $failures, float $timeout, int $threshold)
    {
    }
    /**
     * {@inheritdoc}
     */
    public abstract function getstate() : string;
    /**
     * {@inheritdoc}
     */
    public function getfailures() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function gettimeout() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getthreshold() : int
    {
    }
}
