<?php

namespace PrestaShop\CircuitBreaker\Place;

final class ClosedPlace extends \PrestaShop\CircuitBreaker\Place\AbstractPlace
{
    /**
     * {@inheritdoc}
     */
    public function getState() : string
    {
    }
}
