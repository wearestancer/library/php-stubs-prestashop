<?php

namespace PrestaShop\CircuitBreaker\Client;

/**
 * Guzzle implementation of client.
 * The possibility of extending this client is intended.
 */
class GuzzleClient implements \PrestaShop\CircuitBreaker\Contract\ClientInterface
{
    /**
     * @var string by default, calls are sent using GET method
     */
    const DEFAULT_METHOD = 'GET';
    /**
     * Supported HTTP methods
     */
    const SUPPORTED_METHODS = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS'];
    public function __construct(array $defaultOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws UnavailableServiceException
     */
    public function request(string $resource, array $options) : string
    {
    }
}
