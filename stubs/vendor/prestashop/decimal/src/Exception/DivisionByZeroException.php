<?php

namespace PrestaShop\Decimal\Exception;

/**
 * Thrown when attempting to divide by zero
 */
class DivisionByZeroException extends \Exception
{
}
