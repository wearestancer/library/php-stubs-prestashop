<?php

namespace PrestaShop\Decimal;

/**
 * Builds Number instances
 */
class Builder
{
    /**
     * Pattern for most numbers
     */
    public const NUMBER_PATTERN = "/^(?<sign>[-+])?(?<integerPart>\\d+)?(?:\\.(?<fractionalPart>\\d+)(?<exponentPart>[eE](?<exponentSign>[-+])(?<exponent>\\d+))?)?\$/";
    /**
     * Pattern for integer numbers in scientific notation (rare but supported by spec)
     */
    public const INT_EXPONENTIAL_PATTERN = "/^(?<sign>[-+])?(?<integerPart>\\d+)(?<exponentPart>[eE](?<exponentSign>[-+])(?<exponent>\\d+))\$/";
    /**
     * Builds a Number from a string
     *
     * @param string $number
     *
     * @return DecimalNumber
     */
    public static function parseNumber($number)
    {
    }
}
