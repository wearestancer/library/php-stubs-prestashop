<?php

namespace PrestaShop\Decimal;

/**
 * Retrocompatible name for DecimalNumber
 *
 * @deprecated use DecimalNumber instead
 */
class Number extends \PrestaShop\Decimal\DecimalNumber
{
    /**
     * {@inheritdoc}
     */
    public function __construct($number, $exponent = null)
    {
    }
}
