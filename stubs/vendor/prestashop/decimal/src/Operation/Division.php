<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Computes the division between two decimal numbers.
 */
class Division
{
    public const DEFAULT_PRECISION = 6;
    /**
     * Performs the division.
     *
     * A target maximum precision is required in order to handle potential infinite number of decimals
     * (e.g. 1/3 = 0.3333333...).
     *
     * If the division yields more decimal positions than the requested precision,
     * the remaining decimals are truncated, with **no rounding**.
     *
     * @param DecimalNumber $a Dividend
     * @param DecimalNumber $b Divisor
     * @param int $precision Maximum decimal precision
     *
     * @return DecimalNumber Result of the division
     *
     * @throws DivisionByZeroException
     */
    public function compute(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b, $precision = self::DEFAULT_PRECISION)
    {
    }
    /**
     * Performs the division using BC Math
     *
     * @param DecimalNumber $a Dividend
     * @param DecimalNumber $b Divisor
     * @param int $precision Maximum decimal precision
     *
     * @return DecimalNumber Result of the division
     *
     * @throws DivisionByZeroException
     */
    public function computeUsingBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b, $precision = self::DEFAULT_PRECISION)
    {
    }
    /**
     * Performs the division without BC Math
     *
     * @param DecimalNumber $a Dividend
     * @param DecimalNumber $b Divisor
     * @param int $precision Maximum decimal precision
     *
     * @return DecimalNumber Result of the division
     *
     * @throws DivisionByZeroException
     */
    public function computeWithoutBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b, $precision = self::DEFAULT_PRECISION)
    {
    }
}
