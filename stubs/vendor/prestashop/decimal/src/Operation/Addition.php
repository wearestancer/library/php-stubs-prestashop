<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Computes the addition of two decimal numbers
 */
class Addition
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    /**
     * Performs the addition
     *
     * @param DecimalNumber $a Base number
     * @param DecimalNumber $b Addend
     *
     * @return DecimalNumber Result of the addition
     */
    public function compute(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Performs the addition using BC Math
     *
     * @param DecimalNumber $a Base number
     * @param DecimalNumber $b Addend
     *
     * @return DecimalNumber Result of the addition
     */
    public function computeUsingBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Performs the addition without BC Math
     *
     * @param DecimalNumber $a Base number
     * @param DecimalNumber $b Addend
     *
     * @return DecimalNumber Result of the addition
     */
    public function computeWithoutBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
}
