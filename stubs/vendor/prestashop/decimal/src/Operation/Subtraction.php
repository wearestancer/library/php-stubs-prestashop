<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Computes the subtraction of two decimal numbers
 */
class Subtraction
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    /**
     * Performs the subtraction
     *
     * @param DecimalNumber $a Minuend
     * @param DecimalNumber $b Subtrahend
     *
     * @return DecimalNumber Result of the subtraction
     */
    public function compute(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Performs the subtraction using BC Math
     *
     * @param DecimalNumber $a Minuend
     * @param DecimalNumber $b Subtrahend
     *
     * @return DecimalNumber Result of the subtraction
     */
    public function computeUsingBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Performs the subtraction without using BC Math
     *
     * @param DecimalNumber $a Minuend
     * @param DecimalNumber $b Subtrahend
     *
     * @return DecimalNumber Result of the subtraction
     */
    public function computeWithoutBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
}
