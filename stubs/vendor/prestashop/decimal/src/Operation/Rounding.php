<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Allows transforming a decimal number's precision
 */
class Rounding
{
    public const ROUND_TRUNCATE = 'truncate';
    public const ROUND_CEIL = 'ceil';
    public const ROUND_FLOOR = 'floor';
    public const ROUND_HALF_UP = 'up';
    public const ROUND_HALF_DOWN = 'down';
    public const ROUND_HALF_EVEN = 'even';
    /**
     * Rounds a decimal number to a specified precision
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     * @param string $roundingMode Rounding algorithm
     *
     * @return DecimalNumber
     */
    public function compute(\PrestaShop\Decimal\DecimalNumber $number, $precision, $roundingMode)
    {
    }
    /**
     * Truncates a number to a target number of decimal digits.
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     *
     * @return DecimalNumber
     */
    public function truncate(\PrestaShop\Decimal\DecimalNumber $number, $precision)
    {
    }
    /**
     * Rounds a number up if its precision is greater than the target one.
     *
     * Ceil always rounds towards positive infinity.
     *
     * Examples:
     *
     * ```
     * $n = new Decimal\Number('123.456');
     * $this->ceil($n, 0); // '124'
     * $this->ceil($n, 1); // '123.5'
     * $this->ceil($n, 2); // '123.46'
     *
     * $n = new Decimal\Number('-123.456');
     * $this->ceil($n, 0); // '-122'
     * $this->ceil($n, 1); // '-123.3'
     * $this->ceil($n, 2); // '-123.44'
     * ```
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     *
     * @return DecimalNumber
     */
    public function ceil(\PrestaShop\Decimal\DecimalNumber $number, $precision)
    {
    }
    /**
     * Rounds a number down if its precision is greater than the target one.
     *
     * Floor always rounds towards negative infinity.
     *
     * Examples:
     *
     * ```
     * $n = new Decimal\Number('123.456');
     * $this->floor($n, 0); // '123'
     * $this->floor($n, 1); // '123.4'
     * $this->floor($n, 2); // '123.45'
     *
     * $n = new Decimal\Number('-123.456');
     * $this->floor($n, 0); // '-124'
     * $this->floor($n, 1); // '-123.5'
     * $this->floor($n, 2); // '-123.46'
     * ```
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     *
     * @return DecimalNumber
     */
    public function floor(\PrestaShop\Decimal\DecimalNumber $number, $precision)
    {
    }
    /**
     * Rounds the number according to the digit D located at precision P.
     * - It rounds away from zero if D >= 5
     * - It rounds towards zero if D < 5
     *
     * Examples:
     *
     * ```
     * $n = new Decimal\Number('123.456');
     * $this->roundHalfUp($n, 0); // '123'
     * $this->roundHalfUp($n, 1); // '123.5'
     * $this->roundHalfUp($n, 2); // '123.46'
     *
     * $n = new Decimal\Number('-123.456');
     * $this->roundHalfUp($n, 0); // '-123'
     * $this->roundHalfUp($n, 1); // '-123.5'
     * $this->roundHalfUp($n, 2); // '-123.46'
     * ```
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     *
     * @return DecimalNumber
     */
    public function roundHalfUp(\PrestaShop\Decimal\DecimalNumber $number, $precision)
    {
    }
    /**
     * Rounds the number according to the digit D located at precision P.
     * - It rounds away from zero if D > 5
     * - It rounds towards zero if D <= 5
     *
     * Examples:
     *
     * ```
     * $n = new Decimal\Number('123.456');
     * $this->roundHalfUp($n, 0); // '123'
     * $this->roundHalfUp($n, 1); // '123.4'
     * $this->roundHalfUp($n, 2); // '123.46'
     *
     * $n = new Decimal\Number('-123.456');
     * $this->roundHalfUp($n, 0); // '-123'
     * $this->roundHalfUp($n, 1); // '-123.4'
     * $this->roundHalfUp($n, 2); // '-123.46'
     * ```
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     *
     * @return DecimalNumber
     */
    public function roundHalfDown(\PrestaShop\Decimal\DecimalNumber $number, $precision)
    {
    }
    /**
     * Rounds a number according to "banker's rounding".
     *
     * The number is rounded according to the digit D located at precision P.
     * - Away from zero if D > 5
     * - Towards zero if D < 5
     * - if D = 5, then
     *     - If the last significant digit is even, the number is rounded away from zero
     *     - If the last significant digit is odd, the number is rounded towards zero.
     *
     * Examples:
     *
     * ```
     * $n = new Decimal\Number('123.456');
     * $this->roundHalfUp($n, 0); // '123'
     * $this->roundHalfUp($n, 1); // '123.4'
     * $this->roundHalfUp($n, 2); // '123.46'
     *
     * $n = new Decimal\Number('-123.456');
     * $this->roundHalfUp($n, 0); // '-123'
     * $this->roundHalfUp($n, 1); // '-123.4'
     * $this->roundHalfUp($n, 2); // '-123.46'
     *
     * $n = new Decimal\Number('1.1525354556575859505');
     * $this->roundHalfEven($n, 0);  // '1'
     * $this->roundHalfEven($n, 1);  // '1.2'
     * $this->roundHalfEven($n, 2);  // '1.15'
     * $this->roundHalfEven($n, 3);  // '1.152'
     * $this->roundHalfEven($n, 4);  // '1.1525'
     * $this->roundHalfEven($n, 5);  // '1.15255'
     * $this->roundHalfEven($n, 6);  // '1.152535'
     * $this->roundHalfEven($n, 7);  // '1.1525354'
     * $this->roundHalfEven($n, 8);  // '1.15253546'
     * $this->roundHalfEven($n, 9);  // '1.152535456'
     * $this->roundHalfEven($n, 10); // '1.1525354556'
     * ```
     *
     * @param DecimalNumber $number Number to round
     * @param int $precision Maximum number of decimals
     *
     * @return DecimalNumber
     */
    public function roundHalfEven(\PrestaShop\Decimal\DecimalNumber $number, $precision)
    {
    }
}
