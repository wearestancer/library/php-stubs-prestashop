<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Computes relative magnitude changes on a decimal number
 */
class MagnitudeChange
{
    /**
     * Multiplies a number by 10^$exponent.
     *
     * Examples:
     * ```php
     * $n = new Decimal\Number('123.45678');
     * $o = new Decimal\Operation\MagnitudeChange();
     * $o->compute($n, 2);  // 12345.678
     * $o->compute($n, 6);  // 123456780
     * $o->compute($n, -2); // 1.2345678
     * $o->compute($n, -6); // 0.00012345678
     * ```
     *
     * @param DecimalNumber $number
     * @param int $exponent
     *
     * @return DecimalNumber
     */
    public function compute(\PrestaShop\Decimal\DecimalNumber $number, $exponent)
    {
    }
}
