<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Computes the multiplication between two decimal numbers
 */
class Multiplication
{
    /**
     * Performs the multiplication
     *
     * @param DecimalNumber $a Left operand
     * @param DecimalNumber $b Right operand
     *
     * @return DecimalNumber Result of the multiplication
     */
    public function compute(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Performs the multiplication using BC Math
     *
     * @param DecimalNumber $a Left operand
     * @param DecimalNumber $b Right operand
     *
     * @return DecimalNumber Result of the multiplication
     */
    public function computeUsingBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Performs the multiplication without BC Math
     *
     * @param DecimalNumber $a Left operand
     * @param DecimalNumber $b Right operand
     *
     * @return DecimalNumber Result of the multiplication
     */
    public function computeWithoutBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
}
