<?php

namespace PrestaShop\Decimal\Operation;

/**
 * Compares two decimal numbers
 */
class Comparison
{
    /**
     * Compares two decimal numbers.
     *
     * @param DecimalNumber $a
     * @param DecimalNumber $b
     *
     * @return int returns 1 if $a > $b, -1 if $a < $b, and 0 if they are equal
     */
    public function compare(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Compares two decimal numbers using BC Math
     *
     * @param DecimalNumber $a
     * @param DecimalNumber $b
     *
     * @return int returns 1 if $a > $b, -1 if $a < $b, and 0 if they are equal
     */
    public function compareUsingBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
    /**
     * Compares two decimal numbers without using BC Math
     *
     * @param DecimalNumber $a
     * @param DecimalNumber $b
     *
     * @return int returns 1 if $a > $b, -1 if $a < $b, and 0 if they are equal
     */
    public function compareWithoutBcMath(\PrestaShop\Decimal\DecimalNumber $a, \PrestaShop\Decimal\DecimalNumber $b)
    {
    }
}
