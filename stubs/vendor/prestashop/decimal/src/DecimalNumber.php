<?php

namespace PrestaShop\Decimal;

/**
 * Decimal number.
 *
 * Allows for arbitrary precision math operations.
 */
class DecimalNumber
{
    /**
     * Number constructor.
     *
     * This constructor can be used in two ways:
     *
     * 1) With a number string:
     *
     * ```php
     * (string) new Number('0.123456'); // -> '0.123456'
     * ```
     *
     * 2) With an integer string as coefficient and an exponent
     *
     * ```php
     * // 123456 * 10^(-6)
     * (string) new Number('123456', 6); // -> '0.123456'
     * ```
     *
     * Note: decimal positions must always be a positive number.
     *
     * @param string $number Number or coefficient
     * @param int|null $exponent [default=null] If provided, the number can be considered as the negative
     *                           exponent of the scientific notation, or the number of fractional digits
     */
    public function __construct($number, $exponent = null)
    {
    }
    /**
     * Returns the integer part of the number.
     * Note that this does NOT include the sign.
     *
     * @return string
     */
    public function getIntegerPart()
    {
    }
    /**
     * Returns the fractional part of the number.
     * Note that this does NOT include the sign.
     *
     * @return string
     */
    public function getFractionalPart()
    {
    }
    /**
     * Returns the number of digits in the fractional part.
     *
     * @see self::getExponent() This method is an alias of getExponent().
     *
     * @return int
     */
    public function getPrecision()
    {
    }
    /**
     * Returns the number's sign.
     * Note that this method will return an empty string if the number is positive!
     *
     * @return string '-' if negative, empty string if positive
     */
    public function getSign()
    {
    }
    /**
     * Returns the exponent of this number. For practical reasons, this exponent is always >= 0.
     *
     * This value can also be interpreted as the number of significant digits on the fractional part.
     *
     * @return int
     */
    public function getExponent()
    {
    }
    /**
     * Returns the raw number as stored internally. This coefficient is always an integer.
     *
     * It can be transformed to float by computing:
     * ```
     * getCoefficient() * 10^(-getExponent())
     * ```
     *
     * @return string
     */
    public function getCoefficient()
    {
    }
    /**
     * Returns a string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Returns the number as a string, with exactly $precision decimals
     *
     * Example:
     * ```
     * $n = new Number('123.4560');
     * (string) $n->round(1); // '123.4'
     * (string) $n->round(2); // '123.45'
     * (string) $n->round(3); // '123.456'
     * (string) $n->round(4); // '123.4560' (trailing zeroes are added)
     * (string) $n->round(5); // '123.45600' (trailing zeroes are added)
     * ```
     *
     * @param int $precision Exact number of desired decimals
     * @param string $roundingMode [default=Rounding::ROUND_TRUNCATE] Rounding algorithm
     *
     * @return string
     */
    public function toPrecision($precision, $roundingMode = \PrestaShop\Decimal\Operation\Rounding::ROUND_TRUNCATE)
    {
    }
    /**
     * Returns the number as a string, with up to $maxDecimals significant digits.
     *
     * Example:
     * ```
     * $n = new Number('123.4560');
     * (string) $n->round(1); // '123.4'
     * (string) $n->round(2); // '123.45'
     * (string) $n->round(3); // '123.456'
     * (string) $n->round(4); // '123.456' (does not add trailing zeroes)
     * (string) $n->round(5); // '123.456' (does not add trailing zeroes)
     * ```
     *
     * @param int $maxDecimals Maximum number of decimals
     * @param string $roundingMode [default=Rounding::ROUND_TRUNCATE] Rounding algorithm
     *
     * @return string
     */
    public function round($maxDecimals, $roundingMode = \PrestaShop\Decimal\Operation\Rounding::ROUND_TRUNCATE)
    {
    }
    /**
     * Returns this number as a positive number
     *
     * @return self
     */
    public function toPositive()
    {
    }
    /**
     * Returns this number as a negative number
     *
     * @return self
     */
    public function toNegative()
    {
    }
    /**
     * Returns the computed result of adding another number to this one
     *
     * @param self $addend Number to add
     *
     * @return self
     */
    public function plus(self $addend)
    {
    }
    /**
     * Returns the computed result of subtracting another number to this one
     *
     * @param self $subtrahend Number to subtract
     *
     * @return self
     */
    public function minus(self $subtrahend)
    {
    }
    /**
     * Returns the computed result of multiplying this number with another one
     *
     * @param self $factor
     *
     * @return self
     */
    public function times(self $factor)
    {
    }
    /**
     * Returns the computed result of dividing this number by another one, with up to $precision number of decimals.
     *
     * A target maximum precision is required in order to handle potential infinite number of decimals
     * (e.g. 1/3 = 0.3333333...).
     *
     * If the division yields more decimal positions than the requested precision,
     * the remaining decimals are truncated, with **no rounding**.
     *
     * @param self $divisor
     * @param int $precision [optional] By default, up to Operation\Division::DEFAULT_PRECISION number of decimals
     *
     * @return self
     *
     * @throws Exception\DivisionByZeroException
     */
    public function dividedBy(self $divisor, $precision = \PrestaShop\Decimal\Operation\Division::DEFAULT_PRECISION)
    {
    }
    /**
     * Indicates if this number equals zero
     *
     * @return bool
     */
    public function equalsZero()
    {
    }
    /**
     * Indicates if this number is greater than the provided one
     *
     * @param self $number
     *
     * @return bool
     */
    public function isGreaterThan(self $number)
    {
    }
    /**
     * Indicates if this number is greater than zero
     *
     * @return bool
     */
    public function isGreaterThanZero()
    {
    }
    /**
     * Indicates if this number is greater or equal than zero
     *
     * @return bool
     */
    public function isGreaterOrEqualThanZero()
    {
    }
    /**
     * Indicates if this number is greater or equal compared to the provided one
     *
     * @param self $number
     *
     * @return bool
     */
    public function isGreaterOrEqualThan(self $number)
    {
    }
    /**
     * Indicates if this number is lower than zero
     *
     * @return bool
     */
    public function isLowerThanZero()
    {
    }
    /**
     * Indicates if this number is lower or equal than zero
     *
     * @return bool
     */
    public function isLowerOrEqualThanZero()
    {
    }
    /**
     * Indicates if this number is greater than the provided one
     *
     * @param self $number
     *
     * @return bool
     */
    public function isLowerThan(self $number)
    {
    }
    /**
     * Indicates if this number is lower or equal compared to the provided one
     *
     * @param self $number
     *
     * @return bool
     */
    public function isLowerOrEqualThan(self $number)
    {
    }
    /**
     * Indicates if this number is positive
     *
     * @return bool
     */
    public function isPositive()
    {
    }
    /**
     * Indicates if this number is negative
     *
     * @return bool
     */
    public function isNegative()
    {
    }
    /**
     * Indicates if this number equals another one
     *
     * @param self $number
     *
     * @return bool
     */
    public function equals(self $number)
    {
    }
    /**
     * Returns the additive inverse of this number (that is, N * -1).
     *
     * @return static
     */
    public function invert()
    {
    }
    /**
     * Creates a new copy of this number multiplied by 10^$exponent
     *
     * @param int $exponent
     *
     * @return static
     */
    public function toMagnitude($exponent)
    {
    }
}
