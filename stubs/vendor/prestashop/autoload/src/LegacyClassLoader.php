<?php

namespace PrestaShop\Autoload;

/**
 * Parses all the core classes to build indexes file used by the Prestashop Autoload.
 */
final class LegacyClassLoader
{
    public const CORE_SUFFIX = 'Core';
    public const TYPE_CLASS = 'class';
    public const TYPE_ABSTRACT_CLASS = 'abstract class';
    public const TYPE_INTERFACE = 'interface';
    public function __construct(string $directory, string $cacheDirectory)
    {
    }
    /**
     * @return array<int|string, array<string, string|null>>
     */
    public function buildClassIndex(bool $enableOverrides) : array
    {
    }
    /**
     * Get Class index cache file.
     */
    public function getClassIndexFilepath() : string
    {
    }
    public function getCacheDirectory() : string
    {
    }
    /**
     * @return array<int|string, array<string, string|null>>
     */
    public function loadClassCache() : array
    {
    }
}
