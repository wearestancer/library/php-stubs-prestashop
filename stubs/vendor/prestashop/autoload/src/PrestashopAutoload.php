<?php

namespace PrestaShop\Autoload;

final class PrestashopAutoload
{
    public function __construct(string $rootDirectory, string $cacheDirectory)
    {
    }
    public function generateIndex() : void
    {
    }
    public function getClassPath(string $className) : ?string
    {
    }
    public function register() : self
    {
    }
    public function disableOverrides() : self
    {
    }
    public static function create(string $rootDirectory, string $cacheDirectory) : self
    {
    }
    public static function getInstance() : self
    {
    }
}
