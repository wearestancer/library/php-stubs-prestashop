<?php

namespace PrestaShop\Autoload;

final class Autoloader
{
    public function __construct(string $directory)
    {
    }
    public function register() : void
    {
    }
    public function load(string $className) : void
    {
    }
    /**
     * @param array<int|string, array<string, string|null>> $classIndex
     */
    public function setClassIndex(array $classIndex) : void
    {
    }
    public function getClassPath(string $className) : ?string
    {
    }
    public function setInitializationCallBack(\Closure $closure) : self
    {
    }
}
