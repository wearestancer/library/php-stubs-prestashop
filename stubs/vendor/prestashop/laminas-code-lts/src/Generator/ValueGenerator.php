<?php

namespace Laminas\Code\Generator;

class ValueGenerator extends \Laminas\Code\Generator\AbstractGenerator
{
    /**#@+
     * Constant values
     */
    public const TYPE_AUTO = 'auto';
    public const TYPE_BOOLEAN = 'boolean';
    public const TYPE_BOOL = 'bool';
    public const TYPE_NUMBER = 'number';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_INT = 'int';
    public const TYPE_FLOAT = 'float';
    public const TYPE_DOUBLE = 'double';
    public const TYPE_STRING = 'string';
    public const TYPE_ARRAY = 'array';
    public const TYPE_ARRAY_SHORT = 'array_short';
    public const TYPE_ARRAY_LONG = 'array_long';
    public const TYPE_CONSTANT = 'constant';
    public const TYPE_NULL = 'null';
    public const TYPE_OBJECT = 'object';
    public const TYPE_OTHER = 'other';
    /**#@-*/
    public const OUTPUT_MULTIPLE_LINE = 'multipleLine';
    public const OUTPUT_SINGLE_LINE = 'singleLine';
    /** @var mixed */
    protected $value;
    /** @var string */
    protected $type = self::TYPE_AUTO;
    /** @var int */
    protected $arrayDepth = 0;
    /** @var string */
    protected $outputMode = self::OUTPUT_MULTIPLE_LINE;
    /** @var array */
    protected $allowedTypes = [];
    /**
     * Autodetectable constants
     *
     * @var SplArrayObject|StdlibArrayObject
     */
    protected $constants;
    /**
     * @param null|mixed                            $value
     * @param string                                $type
     * @param string                                $outputMode
     * @param null|SplArrayObject|StdlibArrayObject $constants
     */
    public function __construct($value = null, $type = self::TYPE_AUTO, $outputMode = self::OUTPUT_MULTIPLE_LINE, $constants = null)
    {
    }
    /**
     * Init constant list by defined and magic constants
     */
    public function initEnvironmentConstants()
    {
    }
    /**
     * Add constant to list
     *
     * @param string $constant
     * @return $this
     */
    public function addConstant($constant)
    {
    }
    /**
     * Delete constant from constant list
     *
     * @param string $constant
     * @return bool
     */
    public function deleteConstant($constant)
    {
    }
    /**
     * Return constant list
     *
     * @return SplArrayObject|StdlibArrayObject
     */
    public function getConstants()
    {
    }
    /**
     * @return bool
     */
    public function isValidConstantType()
    {
    }
    /**
     * @param  mixed $value
     * @return $this
     */
    public function setValue($value)
    {
    }
    /**
     * @return mixed
     */
    public function getValue()
    {
    }
    /**
     * @param  string $type
     * @return $this
     */
    public function setType($type)
    {
    }
    /**
     * @return string
     */
    public function getType()
    {
    }
    /**
     * @param  int $arrayDepth
     * @return $this
     */
    public function setArrayDepth($arrayDepth)
    {
    }
    /**
     * @return int
     */
    public function getArrayDepth()
    {
    }
    /**
     * @param  string $type
     * @return string
     */
    protected function getValidatedType($type)
    {
    }
    /**
     * @param  mixed $value
     * @return string
     */
    public function getAutoDeterminedType($value)
    {
    }
    /**
     * @throws Exception\RuntimeException
     * @return string
     */
    public function generate()
    {
    }
    /**
     * Quotes value for PHP code.
     *
     * @param  string $input Raw string.
     * @param  bool   $quote Whether add surrounding quotes or not.
     * @return string PHP-ready code.
     */
    public static function escape($input, $quote = true)
    {
    }
    /**
     * @param  string $outputMode
     * @return $this
     */
    public function setOutputMode($outputMode)
    {
    }
    /**
     * @return string
     */
    public function getOutputMode()
    {
    }
    /** @return string */
    public function __toString()
    {
    }
}
