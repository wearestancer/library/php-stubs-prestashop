<?php

namespace Laminas\Code\Generator;

abstract class AbstractGenerator implements \Laminas\Code\Generator\GeneratorInterface
{
    /**
     * Line feed to use in place of EOL
     */
    public const LINE_FEED = "\n";
    /** @var bool */
    protected $isSourceDirty = true;
    /** @var string 4 spaces by default */
    protected $indentation = '    ';
    /**
     * @var string|null
     * TODO: Type should be changed to "string" in the next major version. Nullable for BC
     */
    protected $sourceContent;
    /**
     * @param  array $options
     */
    public function __construct($options = [])
    {
    }
    /**
     * @param  bool $isSourceDirty
     * @return $this
     */
    public function setSourceDirty($isSourceDirty = true)
    {
    }
    /**
     * @return bool
     */
    public function isSourceDirty()
    {
    }
    /**
     * @param  string $indentation
     * @return $this
     */
    public function setIndentation($indentation)
    {
    }
    /**
     * @return string
     */
    public function getIndentation()
    {
    }
    /**
     * @param  ?string $sourceContent
     * @return $this
     */
    public function setSourceContent($sourceContent)
    {
    }
    /**
     * @return ?string
     */
    public function getSourceContent()
    {
    }
    /**
     * @param  array|Traversable $options
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setOptions($options)
    {
    }
}
