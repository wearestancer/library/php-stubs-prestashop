<?php

namespace Laminas\Code\Generator;

class FileGenerator extends \Laminas\Code\Generator\AbstractGenerator
{
    /** @var string */
    protected $filename = '';
    /** @var DocBlockGenerator|null */
    protected $docBlock;
    /** @var string[] */
    protected $requiredFiles = [];
    /** @var string */
    protected $namespace = '';
    /**
     * @psalm-var list<array{string, string|null}>
     * @var array
     */
    protected $uses = [];
    /**
     * @var ClassGenerator[]
     * @psalm-var array<string, ClassGenerator>
     */
    protected $classes = [];
    /** @var string */
    protected $body = '';
    /**
     * @var DeclareStatement[]
     * @psalm-var array<string, DeclareStatement>
     */
    protected $declares = [];
    /**
     * Passes $options to {@link setOptions()}.
     *
     * @param array|Traversable|null $options
     */
    public function __construct($options = null)
    {
    }
    /**
     * @return FileGenerator
     */
    public static function fromArray(array $values)
    {
    }
    /**
     * @param  DocBlockGenerator|array|string $docBlock
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setDocBlock($docBlock)
    {
    }
    /**
     * @return ?DocBlockGenerator
     */
    public function getDocBlock()
    {
    }
    /**
     * @param  string[] $requiredFiles
     * @return $this
     */
    public function setRequiredFiles(array $requiredFiles)
    {
    }
    /**
     * @return string[]
     */
    public function getRequiredFiles()
    {
    }
    /**
     * @return string
     */
    public function getNamespace()
    {
    }
    /**
     * @param  string $namespace
     * @return $this
     */
    public function setNamespace($namespace)
    {
    }
    /**
     * Returns an array with the first element the use statement, second is the as part.
     * If $withResolvedAs is set to true, there will be a third element that is the
     * "resolved" as statement, as the second part is not required in use statements
     *
     * @param  bool $withResolvedAs
     * @return array
     * @psalm-return array<int, array{string, null|string, false|null|string}>
     */
    public function getUses($withResolvedAs = false)
    {
    }
    /**
     * @return $this
     */
    public function setUses(array $uses)
    {
    }
    /**
     * @param  string      $use
     * @param  null|string $as
     * @return $this
     */
    public function setUse($use, $as = null)
    {
    }
    /**
     * @param  array[]|string[]|ClassGenerator[] $classes
     * @return $this
     */
    public function setClasses(array $classes)
    {
    }
    /**
     * @param string|null $name
     * @return ClassGenerator
     * @throws ClassNotFoundException
     */
    public function getClass($name = null)
    {
    }
    /**
     * @param  array|string|ClassGenerator $class
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setClass($class)
    {
    }
    /**
     * @param  string $filename
     * @return $this
     */
    public function setFilename($filename)
    {
    }
    /**
     * @return string
     */
    public function getFilename()
    {
    }
    /**
     * @return ClassGenerator[]
     */
    public function getClasses()
    {
    }
    /**
     * @param  string $body
     * @return $this
     */
    public function setBody($body)
    {
    }
    /**
     * @return string
     */
    public function getBody()
    {
    }
    /**
     * @param DeclareStatement[] $declares
     * @return static
     */
    public function setDeclares(array $declares)
    {
    }
    /**
     * @return bool
     */
    public function isSourceDirty()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
    /**
     * @return $this
     * @throws Exception\RuntimeException
     */
    public function write()
    {
    }
}
