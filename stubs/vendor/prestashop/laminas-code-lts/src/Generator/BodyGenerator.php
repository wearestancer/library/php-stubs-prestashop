<?php

namespace Laminas\Code\Generator;

class BodyGenerator extends \Laminas\Code\Generator\AbstractGenerator
{
    /** @var string */
    protected $content = '';
    /**
     * @param  string $content
     * @return $this
     */
    public function setContent($content)
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
