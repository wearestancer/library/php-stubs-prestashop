<?php

namespace Laminas\Code\Generator;

class DocBlockGenerator extends \Laminas\Code\Generator\AbstractGenerator
{
    /** @var string */
    protected $shortDescription = '';
    /** @var string */
    protected $longDescription = '';
    /** @var array */
    protected $tags = [];
    /** @var string */
    protected $indentation = '';
    /** @var bool */
    protected $wordwrap = true;
    /** @var TagManager|null */
    protected static $tagManager;
    /**
     * Build a DocBlock generator object from a reflection object
     *
     * @return DocBlockGenerator
     */
    public static function fromReflection(\Laminas\Code\Reflection\DocBlockReflection $reflectionDocBlock)
    {
    }
    /**
     * Generate from array
     *
     * @configkey shortdescription string The short description for this doc block
     * @configkey longdescription  string The long description for this doc block
     * @configkey tags             array
     * @throws Exception\InvalidArgumentException
     * @return DocBlockGenerator
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * @return TagManager
     */
    protected static function getTagManager()
    {
    }
    /**
     * @param ?string                $shortDescription
     * @param ?string                $longDescription
     * @param array[]|TagInterface[] $tags
     */
    public function __construct($shortDescription = null, $longDescription = null, array $tags = [])
    {
    }
    /**
     * @param  string $shortDescription
     * @return $this
     */
    public function setShortDescription($shortDescription)
    {
    }
    /**
     * @return string
     */
    public function getShortDescription()
    {
    }
    /**
     * @param  string $longDescription
     * @return $this
     */
    public function setLongDescription($longDescription)
    {
    }
    /**
     * @return string
     */
    public function getLongDescription()
    {
    }
    /**
     * @param  array[]|TagInterface[] $tags
     * @return $this
     */
    public function setTags(array $tags)
    {
    }
    /**
     * @param array|TagInterface $tag
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setTag($tag)
    {
    }
    /**
     * @return TagInterface[]
     */
    public function getTags()
    {
    }
    /**
     * @param bool $value
     * @return $this
     */
    public function setWordWrap($value)
    {
    }
    /**
     * @return bool
     */
    public function getWordWrap()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
    /**
     * @param  string $content
     * @return string
     */
    protected function docCommentize($content)
    {
    }
}
