<?php

namespace Laminas\Code\Generator;

class InterfaceGenerator extends \Laminas\Code\Generator\ClassGenerator
{
    public const OBJECT_TYPE = 'interface';
    public const IMPLEMENTS_KEYWORD = 'extends';
    /**
     * Build a Code Generation Php Object from a Class Reflection
     *
     * @return static
     */
    public static function fromReflection(\Laminas\Code\Reflection\ClassReflection $classReflection)
    {
    }
    /**
     * Generate from array
     *
     * @configkey name           string        [required] Class Name
     * @configkey filegenerator  FileGenerator File generator that holds this class
     * @configkey namespacename  string        The namespace for this class
     * @configkey docblock       string        The docblock information
     * @configkey constants
     * @configkey methods
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function addPropertyFromGenerator(\Laminas\Code\Generator\PropertyGenerator $property)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function addMethodFromGenerator(\Laminas\Code\Generator\MethodGenerator $method)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function setExtendedClass($extendedClass)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function setAbstract($isAbstract)
    {
    }
}
