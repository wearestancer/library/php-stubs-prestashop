<?php

namespace Laminas\Code\Generator\TypeGenerator;

/**
 * Represents a single/indivisible (atomic) type, as supported by PHP.
 * This means that this object can be composed into more complex union, intersection
 * and nullable types.
 *
 * @internal the {@see AtomicType} is an implementation detail of the type generator,
 *
 * @psalm-immutable
 */
final class AtomicType
{
    /**
     * @psalm-var value-of<AtomicType::BUILT_IN_TYPES_PRECEDENCE>|0
     * @var int
     */
    public $sortIndex;
    /**
     * @psalm-var non-empty-string
     * @var string
     */
    public $type;
    /**
     * @psalm-pure
     * @throws InvalidArgumentException
     */
    public static function fromString(string $type) : self
    {
    }
    /** @psalm-pure */
    public static function null() : self
    {
    }
    /** @psalm-return non-empty-string */
    public function fullyQualifiedName() : string
    {
    }
    /**
     * @psalm-param non-empty-array<self> $others
     * @throws InvalidArgumentException
     */
    public function assertCanUnionWith(array $others) : void
    {
    }
    /**
     * @psalm-param non-empty-array<self> $others
     * @throws InvalidArgumentException
     */
    public function assertCanIntersectWith(array $others) : void
    {
    }
    /** @throws InvalidArgumentException */
    public function assertCanBeAStandaloneType() : void
    {
    }
    /** @throws InvalidArgumentException */
    public function assertCanBeStandaloneNullable() : void
    {
    }
}
