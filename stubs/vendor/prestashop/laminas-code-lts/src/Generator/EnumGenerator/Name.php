<?php

namespace Laminas\Code\Generator\EnumGenerator;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class Name
{
    public function getName() : string
    {
    }
    public function getNamespace() : ?string
    {
    }
    public static function fromFullyQualifiedClassName(string $name) : self
    {
    }
}
