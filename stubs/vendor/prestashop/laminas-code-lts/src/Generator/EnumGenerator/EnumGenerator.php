<?php

namespace Laminas\Code\Generator\EnumGenerator;

/** @psalm-immutable */
final class EnumGenerator
{
    public function generate() : string
    {
    }
    /**
     * @psalm-param array{
     *      name: non-empty-string,
     *      pureCases: list<non-empty-string>,
     * }|array{
     *      name: non-empty-string,
     *      backedCases: array{
     *          type: 'int'|'string',
     *          cases: array<non-empty-string, int|non-empty-string>,
     *      },
     * } $options
     */
    public static function withConfig(array $options) : self
    {
    }
    public static function fromReflection(\ReflectionEnum $enum) : self
    {
    }
}
