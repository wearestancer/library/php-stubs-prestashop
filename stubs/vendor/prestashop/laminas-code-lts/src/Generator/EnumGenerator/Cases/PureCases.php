<?php

namespace Laminas\Code\Generator\EnumGenerator\Cases;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class PureCases
{
    /**
     * @return list<string>
     */
    public function getCases() : array
    {
    }
    /**
     * @param list<non-empty-string> $pureCases
     */
    public static function fromCases(array $pureCases) : self
    {
    }
}
