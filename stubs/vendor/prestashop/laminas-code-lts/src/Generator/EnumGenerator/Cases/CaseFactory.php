<?php

namespace Laminas\Code\Generator\EnumGenerator\Cases;

/** @internal */
final class CaseFactory
{
    /**
     * @psalm-param array{
     *      name: non-empty-string,
     *      pureCases: list<non-empty-string>,
     * }|array{
     *      name: non-empty-string,
     *      backedCases: array{
     *          type: 'int',
     *          cases: array<non-empty-string, int>,
     *      }|array{
     *          type: 'string',
     *          cases: array<non-empty-string, non-empty-string>,
     *      },
     * } $options
     * @return BackedCases|PureCases
     */
    public static function fromOptions(array $options)
    {
    }
    /**
     * @return BackedCases|PureCases
     */
    public static function fromReflectionCases(\ReflectionEnum $enum)
    {
    }
}
