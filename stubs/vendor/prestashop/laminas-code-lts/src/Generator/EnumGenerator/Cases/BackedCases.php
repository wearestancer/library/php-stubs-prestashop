<?php

namespace Laminas\Code\Generator\EnumGenerator\Cases;

/**
 * @internal
 *
 * @psalm-immutable
 */
final class BackedCases
{
    public function getBackedType() : string
    {
    }
    /**
     * @return list<string>
     */
    public function getCases() : array
    {
    }
    /**
     * @param array<non-empty-string, int>|array<non-empty-string, non-empty-string> $backedCases
     */
    public static function fromCasesWithType(array $backedCases, string $type) : self
    {
    }
}
