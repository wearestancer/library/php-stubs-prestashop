<?php

namespace Laminas\Code\Generator;

class ParameterGenerator extends \Laminas\Code\Generator\AbstractGenerator
{
    /** @var string */
    protected $name = '';
    /** @var TypeGenerator|null */
    protected $type;
    /** @var ValueGenerator|null */
    protected $defaultValue;
    /** @var int */
    protected $position = 0;
    /** @var bool */
    protected $passedByReference = false;
    /**
     * @return ParameterGenerator
     */
    public static function fromReflection(\Laminas\Code\Reflection\ParameterReflection $reflectionParameter)
    {
    }
    /**
     * Generate from array
     *
     * @configkey name                  string                                          [required] Class Name
     * @configkey type                  string
     * @configkey defaultvalue          null|bool|string|int|float|array|ValueGenerator
     * @configkey passedbyreference     bool
     * @configkey position              int
     * @configkey sourcedirty           bool
     * @configkey indentation           string
     * @configkey sourcecontent         string
     * @configkey omitdefaultvalue      bool
     * @throws Exception\InvalidArgumentException
     * @return ParameterGenerator
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * @param  ?string $name
     * @param  ?string $type
     * @param  ?mixed  $defaultValue
     * @param  ?int    $position
     * @param  bool    $passByReference
     */
    public function __construct($name = null, $type = null, $defaultValue = null, $position = null, $passByReference = false)
    {
    }
    /**
     * @param  string $type
     * @return $this
     */
    public function setType($type)
    {
    }
    /**
     * @return string
     */
    public function getType()
    {
    }
    /**
     * @param  string $name
     * @return $this
     */
    public function setName($name)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Set the default value of the parameter.
     *
     * Certain variables are difficult to express
     *
     * @param  null|bool|string|int|float|array|ValueGenerator $defaultValue
     * @return $this
     */
    public function setDefaultValue($defaultValue)
    {
    }
    /**
     * @return ?ValueGenerator
     */
    public function getDefaultValue()
    {
    }
    /**
     * @param  int $position
     * @return $this
     */
    public function setPosition($position)
    {
    }
    /**
     * @return int
     */
    public function getPosition()
    {
    }
    /**
     * @return bool
     */
    public function getPassedByReference()
    {
    }
    /**
     * @param  bool $passedByReference
     * @return $this
     */
    public function setPassedByReference($passedByReference)
    {
    }
    /**
     * @param bool $variadic
     * @return $this
     */
    public function setVariadic($variadic)
    {
    }
    /**
     * @return bool
     */
    public function getVariadic()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
    /**
     * @return $this
     */
    public function omitDefaultValue(bool $omit = true)
    {
    }
}
