<?php

namespace Laminas\Code\Generator;

class PropertyGenerator extends \Laminas\Code\Generator\AbstractMemberGenerator
{
    public const FLAG_CONSTANT = 0x8;
    public const FLAG_READONLY = 0x80;
    /** @var bool */
    protected $isConst = false;
    /** @var PropertyValueGenerator|null */
    protected $defaultValue;
    /** @return static */
    public static function fromReflection(\Laminas\Code\Reflection\PropertyReflection $reflectionProperty)
    {
    }
    /**
     * Generate from array
     *
     * @configkey name               string                                          [required] Class Name
     * @configkey const              bool
     * @configkey defaultvalue       null|bool|string|int|float|array|ValueGenerator
     * @configkey flags              int
     * @configkey abstract           bool
     * @configkey final              bool
     * @configkey static             bool
     * @configkey visibility         string
     * @configkey omitdefaultvalue   bool
     * @configkey readonly           bool
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * @param PropertyValueGenerator|string|array|null $defaultValue
     * @param int|int[]                                $flags
     */
    public function __construct(?string $name = null, $defaultValue = null, $flags = self::FLAG_PUBLIC)
    {
    }
    /**
     * @param  bool $const
     * @return $this
     */
    public function setConst($const)
    {
    }
    /**
     * @return bool
     */
    public function isConst()
    {
    }
    public function setReadonly(bool $readonly) : self
    {
    }
    public function isReadonly() : bool
    {
    }
    /**
     * {@inheritDoc}
     */
    public function setFlags($flags)
    {
    }
    /**
     * @param PropertyValueGenerator|mixed $defaultValue
     * @param string                       $defaultValueType
     * @param string                       $defaultValueOutputMode
     * @return static
     */
    public function setDefaultValue($defaultValue, $defaultValueType = \Laminas\Code\Generator\PropertyValueGenerator::TYPE_AUTO, $defaultValueOutputMode = \Laminas\Code\Generator\PropertyValueGenerator::OUTPUT_MULTIPLE_LINE)
    {
    }
    /**
     * @return ?PropertyValueGenerator
     */
    public function getDefaultValue()
    {
    }
    /**
     * @throws Exception\RuntimeException
     * @return string
     * @psalm-return non-empty-string
     */
    public function generate()
    {
    }
    /**
     * @return $this
     */
    public function omitDefaultValue(bool $omit = true)
    {
    }
}
