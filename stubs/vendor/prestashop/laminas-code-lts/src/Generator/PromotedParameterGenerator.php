<?php

namespace Laminas\Code\Generator;

final class PromotedParameterGenerator extends \Laminas\Code\Generator\ParameterGenerator
{
    public const VISIBILITY_PUBLIC = 'public';
    public const VISIBILITY_PROTECTED = 'protected';
    public const VISIBILITY_PRIVATE = 'private';
    /**
     * @psalm-param non-empty-string $name
     * @psalm-param ?non-empty-string $type
     * @psalm-param PromotedParameterGenerator::VISIBILITY_* $visibility
     */
    public function __construct(string $name, ?string $type = null, string $visibility = self::VISIBILITY_PUBLIC, ?int $position = null, bool $passByReference = false)
    {
    }
    /** @psalm-return non-empty-string */
    public function generate() : string
    {
    }
    public static function fromReflection(\Laminas\Code\Reflection\ParameterReflection $reflectionParameter) : self
    {
    }
    /** @psalm-param PromotedParameterGenerator::VISIBILITY_* $visibility */
    public static function fromParameterGeneratorWithVisibility(parent $generator, string $visibility) : self
    {
    }
}
