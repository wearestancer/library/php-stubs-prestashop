<?php

namespace Laminas\Code\Generator;

class TraitGenerator extends \Laminas\Code\Generator\ClassGenerator
{
    public const OBJECT_TYPE = 'trait';
    /**
     * Build a Code Generation Php Object from a Class Reflection
     *
     * @return static
     */
    public static function fromReflection(\Laminas\Code\Reflection\ClassReflection $classReflection)
    {
    }
    /**
     * Generate from array
     *
     * @configkey name           string        [required] Class Name
     * @configkey filegenerator  FileGenerator File generator that holds this class
     * @configkey namespacename  string        The namespace for this class
     * @configkey docblock       string        The docblock information
     * @configkey properties
     * @configkey methods
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * @inheritDoc
     * @param int[]|int $flags
     */
    public function setFlags($flags)
    {
    }
    /**
     * @param int $flag
     * @return static
     */
    public function addFlag($flag)
    {
    }
    /**
     * @param int $flag
     * @return static
     */
    public function removeFlag($flag)
    {
    }
    /**
     * @inheritDoc
     */
    public function setFinal($isFinal)
    {
    }
    /**
     * @param ?string $extendedClass
     * @return static
     */
    public function setExtendedClass($extendedClass)
    {
    }
    /**
     * @inheritDoc
     */
    public function setImplementedInterfaces(array $implementedInterfaces)
    {
    }
    /**
     * @inheritDoc
     */
    public function setAbstract($isAbstract)
    {
    }
}
