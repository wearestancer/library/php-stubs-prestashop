<?php

namespace Laminas\Code\Generator\Exception;

class ClassNotFoundException extends \Laminas\Code\Generator\Exception\RuntimeException implements \Laminas\Code\Generator\Exception\ExceptionInterface
{
}
