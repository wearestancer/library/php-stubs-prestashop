<?php

namespace Laminas\Code\Generator\Exception;

class InvalidArgumentException extends \Laminas\Code\Exception\InvalidArgumentException implements \Laminas\Code\Generator\Exception\ExceptionInterface
{
}
