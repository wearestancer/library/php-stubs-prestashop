<?php

namespace Laminas\Code\Generator\Exception;

interface ExceptionInterface extends \Laminas\Code\Exception\ExceptionInterface
{
}
