<?php

namespace Laminas\Code\Generator\Exception;

class RuntimeException extends \Laminas\Code\Exception\RuntimeException implements \Laminas\Code\Generator\Exception\ExceptionInterface
{
}
