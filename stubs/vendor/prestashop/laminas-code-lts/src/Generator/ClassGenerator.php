<?php

namespace Laminas\Code\Generator;

class ClassGenerator extends \Laminas\Code\Generator\AbstractGenerator implements \Laminas\Code\Generator\TraitUsageInterface
{
    public const OBJECT_TYPE = 'class';
    public const IMPLEMENTS_KEYWORD = 'implements';
    public const FLAG_ABSTRACT = 0x1;
    public const FLAG_FINAL = 0x2;
    /** @var FileGenerator|null */
    protected $containingFileGenerator;
    /** @var string|null */
    protected $namespaceName;
    /** @var DocBlockGenerator|null */
    protected $docBlock;
    /** @var string */
    protected $name = '';
    /** @var int */
    protected $flags = 0x0;
    /**
     * @psalm-var ?class-string
     * @var string|null
     */
    protected $extendedClass;
    /**
     * Array of implemented interface names
     *
     * @var string[]
     * @psalm-var array<class-string>
     */
    protected $implementedInterfaces = [];
    /** @var PropertyGenerator[] */
    protected $properties = [];
    /** @var PropertyGenerator[] */
    protected $constants = [];
    /** @var MethodGenerator[] */
    protected $methods = [];
    /** @var TraitUsageGenerator Object to encapsulate trait usage logic */
    protected $traitUsageGenerator;
    /**
     * Build a Code Generation Php Object from a Class Reflection
     *
     * @return static
     */
    public static function fromReflection(\Laminas\Code\Reflection\ClassReflection $classReflection)
    {
    }
    /**
     * Generate from array
     *
     * @configkey name           string        [required] Class Name
     * @configkey filegenerator  FileGenerator File generator that holds this class
     * @configkey namespacename  string        The namespace for this class
     * @configkey docblock       string        The docblock information
     * @configkey flags          int           Flags, one of ClassGenerator::FLAG_ABSTRACT ClassGenerator::FLAG_FINAL
     * @configkey extendedclass  string        Class which this class is extending
     * @configkey implementedinterfaces
     * @configkey properties
     * @configkey methods
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * @param null|string                          $name
     * @param null|string                          $namespaceName
     * @param int|int[]|null                       $flags
     * @param class-string|null                    $extends
     * @param string[]                             $interfaces
     * @psalm-param array<class-string>            $interfaces
     * @param PropertyGenerator[]|string[]|array[] $properties
     * @param MethodGenerator[]|string[]|array[]   $methods
     * @param null|DocBlockGenerator               $docBlock
     */
    public function __construct($name = null, $namespaceName = null, $flags = null, $extends = null, array $interfaces = [], array $properties = [], array $methods = [], $docBlock = null)
    {
    }
    /**
     * @param  string $name
     * @return static
     */
    public function setName($name)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param  ?string $namespaceName
     * @return static
     */
    public function setNamespaceName($namespaceName)
    {
    }
    /**
     * @return ?string
     */
    public function getNamespaceName()
    {
    }
    /**
     * @return static
     */
    public function setContainingFileGenerator(\Laminas\Code\Generator\FileGenerator $fileGenerator)
    {
    }
    /**
     * @return ?FileGenerator
     */
    public function getContainingFileGenerator()
    {
    }
    /**
     * @return static
     */
    public function setDocBlock(\Laminas\Code\Generator\DocBlockGenerator $docBlock)
    {
    }
    /**
     * @return ?DocBlockGenerator
     */
    public function getDocBlock()
    {
    }
    /**
     * @param  int[]|int $flags
     * @return static
     */
    public function setFlags($flags)
    {
    }
    /**
     * @param  int $flag
     * @return static
     */
    public function addFlag($flag)
    {
    }
    /**
     * @param  int $flag
     * @return static
     */
    public function removeFlag($flag)
    {
    }
    /**
     * @param  bool $isAbstract
     * @return static
     */
    public function setAbstract($isAbstract)
    {
    }
    /**
     * @return bool
     */
    public function isAbstract()
    {
    }
    /**
     * @param  bool $isFinal
     * @return static
     */
    public function setFinal($isFinal)
    {
    }
    /**
     * @return bool
     */
    public function isFinal()
    {
    }
    /**
     * @param  ?string $extendedClass
     * @psalm-param ?class-string $extendedClass
     * @return static
     */
    public function setExtendedClass($extendedClass)
    {
    }
    /**
     * @return ?string
     * @psalm-return ?class-string
     */
    public function getExtendedClass()
    {
    }
    /**
     * @return bool
     */
    public function hasExtentedClass()
    {
    }
    /**
     * @return static
     */
    public function removeExtentedClass()
    {
    }
    /**
     * @param string[] $implementedInterfaces
     * @psalm-param array<class-string> $implementedInterfaces
     * @return static
     */
    public function setImplementedInterfaces(array $implementedInterfaces)
    {
    }
    /**
     * @return string[]
     * @psalm-return array<class-string>
     */
    public function getImplementedInterfaces()
    {
    }
    /**
     * @param string $implementedInterface
     * @psalm-param class-string $implementedInterface
     * @return bool
     */
    public function hasImplementedInterface($implementedInterface)
    {
    }
    /**
     * @param string $implementedInterface
     * @psalm-param class-string $implementedInterface
     * @return static
     */
    public function removeImplementedInterface($implementedInterface)
    {
    }
    /**
     * @param  string $constantName
     * @return PropertyGenerator|false
     */
    public function getConstant($constantName)
    {
    }
    /**
     * @return PropertyGenerator[] indexed by constant name
     */
    public function getConstants()
    {
    }
    /**
     * @param  string $constantName
     * @return static
     */
    public function removeConstant($constantName)
    {
    }
    /**
     * @param  string $constantName
     * @return bool
     */
    public function hasConstant($constantName)
    {
    }
    /**
     * Add constant from PropertyGenerator
     *
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public function addConstantFromGenerator(\Laminas\Code\Generator\PropertyGenerator $constant)
    {
    }
    /**
     * Add Constant
     *
     * @param  string                      $name Non-empty string
     * @param  string|int|null|float|array $value Scalar
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public function addConstant($name, $value, bool $isFinal = false)
    {
    }
    /**
     * @param  PropertyGenerator[]|array[] $constants
     * @return static
     */
    public function addConstants(array $constants)
    {
    }
    /**
     * @param  PropertyGenerator[]|string[]|array[] $properties
     * @return static
     */
    public function addProperties(array $properties)
    {
    }
    /**
     * Add Property from scalars
     *
     * @param  string            $name
     * @param  null|string|array $defaultValue
     * @param  int               $flags
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public function addProperty($name, $defaultValue = null, $flags = \Laminas\Code\Generator\PropertyGenerator::FLAG_PUBLIC)
    {
    }
    /**
     * Add property from PropertyGenerator
     *
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public function addPropertyFromGenerator(\Laminas\Code\Generator\PropertyGenerator $property)
    {
    }
    /**
     * @return PropertyGenerator[]
     */
    public function getProperties()
    {
    }
    /**
     * @param  string $propertyName
     * @return PropertyGenerator|false
     */
    public function getProperty($propertyName)
    {
    }
    /**
     * Add a class to "use" classes
     *
     * @param  string      $use
     * @param  string|null $useAlias
     * @return static
     */
    public function addUse($use, $useAlias = null)
    {
    }
    /**
     * @param string $use
     * @return bool
     */
    public function hasUse($use)
    {
    }
    /**
     * @param  string $use
     * @return static
     */
    public function removeUse($use)
    {
    }
    /**
     * @param string $use
     * @return bool
     */
    public function hasUseAlias($use)
    {
    }
    /**
     * @param string $use
     * @return static
     */
    public function removeUseAlias($use)
    {
    }
    /**
     * Returns the "use" classes
     *
     * @return array
     */
    public function getUses()
    {
    }
    /**
     * @param  string $propertyName
     * @return static
     */
    public function removeProperty($propertyName)
    {
    }
    /**
     * @param  string $propertyName
     * @return bool
     */
    public function hasProperty($propertyName)
    {
    }
    /**
     * @param  MethodGenerator[]|string[]|array[] $methods
     * @return static
     */
    public function addMethods(array $methods)
    {
    }
    /**
     * Add Method from scalars
     *
     * @param  string                                $name
     * @param  ParameterGenerator[]|array[]|string[] $parameters
     * @param  int                                   $flags
     * @param  null|string                           $body
     * @param  null|string                           $docBlock
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public function addMethod($name, array $parameters = [], $flags = \Laminas\Code\Generator\MethodGenerator::FLAG_PUBLIC, $body = null, $docBlock = null)
    {
    }
    /**
     * Add Method from MethodGenerator
     *
     * @throws Exception\InvalidArgumentException
     * @return static
     */
    public function addMethodFromGenerator(\Laminas\Code\Generator\MethodGenerator $method)
    {
    }
    /**
     * @return MethodGenerator[]
     */
    public function getMethods()
    {
    }
    /**
     * @param  string $methodName
     * @return MethodGenerator|false
     */
    public function getMethod($methodName)
    {
    }
    /**
     * @param  string $methodName
     * @return static
     */
    public function removeMethod($methodName)
    {
    }
    /**
     * @param  string $methodName
     * @return bool
     */
    public function hasMethod($methodName)
    {
    }
    /**
     * @inheritDoc
     */
    public function addTrait($trait)
    {
    }
    /**
     * @inheritDoc
     */
    public function addTraits(array $traits)
    {
    }
    /**
     * @inheritDoc
     */
    public function hasTrait($traitName)
    {
    }
    /**
     * @inheritDoc
     */
    public function getTraits()
    {
    }
    /**
     * @inheritDoc
     */
    public function removeTrait($traitName)
    {
    }
    /**
     * @inheritDoc
     */
    public function addTraitAlias($method, $alias, $visibility = null)
    {
    }
    /**
     * @inheritDoc
     */
    public function getTraitAliases()
    {
    }
    /**
     * @inheritDoc
     */
    public function addTraitOverride($method, $traitsToReplace)
    {
    }
    /**
     * @inheritDoc
     */
    public function removeTraitOverride($method, $overridesToRemove = null)
    {
    }
    /**
     * @inheritDoc
     */
    public function getTraitOverrides()
    {
    }
    /**
     * @return bool
     */
    public function isSourceDirty()
    {
    }
    /**
     * @inheritDoc
     */
    public function generate()
    {
    }
}
