<?php

namespace Laminas\Code\Generator;

class MethodGenerator extends \Laminas\Code\Generator\AbstractMemberGenerator
{
    /** @var DocBlockGenerator|null */
    protected $docBlock;
    /** @var ParameterGenerator[] */
    protected $parameters = [];
    /** @var string */
    protected $body = '';
    /**
     * @return MethodGenerator
     */
    public static function fromReflection(\Laminas\Code\Reflection\MethodReflection $reflectionMethod)
    {
    }
    /**
     * Returns a MethodGenerator based on a MethodReflection with only the signature copied.
     *
     * This is similar to fromReflection() but without the method body and phpdoc as this is quite heavy to copy.
     * It's for example useful when creating proxies where you normally change the method body anyway.
     */
    public static function copyMethodSignature(\Laminas\Code\Reflection\MethodReflection $reflectionMethod) : self
    {
    }
    /**
     * Identify the space indention from the first line and remove this indention
     * from all lines
     *
     * @param string $body
     * @return string
     */
    protected static function clearBodyIndention($body)
    {
    }
    /**
     * Generate from array
     *
     * @configkey name             string        [required] Class Name
     * @configkey docblock         string        The DocBlock information
     * @configkey flags            int           Flags, one of self::FLAG_ABSTRACT, self::FLAG_FINAL
     * @configkey parameters       string        Class which this class is extending
     * @configkey body             string
     * @configkey returntype       string
     * @configkey returnsreference bool
     * @configkey abstract         bool
     * @configkey final            bool
     * @configkey static           bool
     * @configkey visibility       string
     * @throws Exception\InvalidArgumentException
     * @return MethodGenerator
     */
    public static function fromArray(array $array)
    {
    }
    /**
     * @param  ?string                               $name
     * @param ParameterGenerator[]|array[]|string[] $parameters
     * @param int|int[]                             $flags
     * @param  ?string                               $body
     * @param DocBlockGenerator|string|null         $docBlock
     */
    public function __construct($name = null, array $parameters = [], $flags = self::FLAG_PUBLIC, $body = null, $docBlock = null)
    {
    }
    /**
     * @param  ParameterGenerator[]|array[]|string[] $parameters
     * @return $this
     */
    public function setParameters(array $parameters)
    {
    }
    /**
     * @param  ParameterGenerator|array|string $parameter
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setParameter($parameter)
    {
    }
    /**
     * @return ParameterGenerator[]
     */
    public function getParameters()
    {
    }
    /**
     * @param  string $body
     * @return $this
     */
    public function setBody($body)
    {
    }
    /**
     * @return string
     */
    public function getBody()
    {
    }
    /**
     * @param string|null $returnType
     * @return $this
     */
    public function setReturnType($returnType = null)
    {
    }
    /**
     * @return TypeGenerator|null
     */
    public function getReturnType()
    {
    }
    /**
     * @param bool $returnsReference
     * @return $this
     */
    public function setReturnsReference($returnsReference)
    {
    }
    public function returnsReference() : bool
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
    /** @return string */
    public function __toString()
    {
    }
}
