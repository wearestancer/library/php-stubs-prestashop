<?php

namespace Laminas\Code\Generator;

class TraitUsageGenerator extends \Laminas\Code\Generator\AbstractGenerator implements \Laminas\Code\Generator\TraitUsageInterface
{
    /** @var ClassGenerator */
    protected $classGenerator;
    /** @psalm-var array<int, string> Array of trait names */
    protected $traits = [];
    /** @var array Array of trait aliases */
    protected $traitAliases = [];
    /** @var array Array of trait overrides */
    protected $traitOverrides = [];
    /** @var array Array of string names */
    protected $uses = [];
    public function __construct(\Laminas\Code\Generator\ClassGenerator $classGenerator)
    {
    }
    /**
     * @inheritDoc
     */
    public function addUse($use, $useAlias = null)
    {
    }
    /**
     * @inheritDoc
     */
    public function getUses()
    {
    }
    /**
     * @param string $use
     * @return bool
     */
    public function hasUse($use)
    {
    }
    /**
     * @param string $use
     * @return bool
     */
    public function hasUseAlias($use)
    {
    }
    /**
     * Returns the alias of the provided FQCN
     */
    public function getUseAlias(string $use) : ?string
    {
    }
    /**
     * Returns true if the alias is defined in the use list
     */
    public function isUseAlias(string $alias) : bool
    {
    }
    /**
     * @param string $use
     * @return $this
     */
    public function removeUse($use)
    {
    }
    /**
     * @param string $use
     * @return $this
     */
    public function removeUseAlias($use)
    {
    }
    /**
     * @inheritDoc
     */
    public function addTrait($trait)
    {
    }
    /**
     * @inheritDoc
     */
    public function addTraits(array $traits)
    {
    }
    /**
     * @inheritDoc
     */
    public function hasTrait($traitName)
    {
    }
    /**
     * @inheritDoc
     */
    public function getTraits()
    {
    }
    /**
     * @inheritDoc
     */
    public function removeTrait($traitName)
    {
    }
    /**
     * @inheritDoc
     */
    public function addTraitAlias($method, $alias, $visibility = null)
    {
    }
    /**
     * @inheritDoc
     */
    public function getTraitAliases()
    {
    }
    /**
     * @inheritDoc
     */
    public function addTraitOverride($method, $traitsToReplace)
    {
    }
    /**
     * @inheritDoc
     */
    public function removeTraitOverride($method, $overridesToRemove = null)
    {
    }
    /**
     * @inheritDoc
     */
    public function getTraitOverrides()
    {
    }
    /**
     * @inheritDoc
     */
    public function generate()
    {
    }
}
