<?php

namespace Laminas\Code\Generator;

abstract class AbstractMemberGenerator extends \Laminas\Code\Generator\AbstractGenerator
{
    public const FLAG_ABSTRACT = 0x1;
    public const FLAG_FINAL = 0x2;
    public const FLAG_STATIC = 0x4;
    public const FLAG_INTERFACE = 0x8;
    public const FLAG_PUBLIC = 0x10;
    public const FLAG_PROTECTED = 0x20;
    public const FLAG_PRIVATE = 0x40;
    public const VISIBILITY_PUBLIC = 'public';
    public const VISIBILITY_PROTECTED = 'protected';
    public const VISIBILITY_PRIVATE = 'private';
    /** @var DocBlockGenerator|null */
    protected $docBlock;
    /** @var string */
    protected $name = '';
    /** @var int */
    protected $flags = self::FLAG_PUBLIC;
    /**
     * @param  int|int[] $flags
     * @return $this
     */
    public function setFlags($flags)
    {
    }
    /**
     * @param  int $flag
     * @return $this
     */
    public function addFlag($flag)
    {
    }
    /**
     * @param  int $flag
     * @return $this
     */
    public function removeFlag($flag)
    {
    }
    /**
     * @param  bool $isAbstract
     * @return AbstractMemberGenerator
     */
    public function setAbstract($isAbstract)
    {
    }
    /**
     * @return bool
     */
    public function isAbstract()
    {
    }
    /**
     * @param  bool $isInterface
     * @return AbstractMemberGenerator
     */
    public function setInterface($isInterface)
    {
    }
    /**
     * @return bool
     */
    public function isInterface()
    {
    }
    /**
     * @param  bool $isFinal
     * @return AbstractMemberGenerator
     */
    public function setFinal($isFinal)
    {
    }
    /**
     * @return bool
     */
    public function isFinal()
    {
    }
    /**
     * @param  bool $isStatic
     * @return AbstractMemberGenerator
     */
    public function setStatic($isStatic)
    {
    }
    /**
     * @return bool
     */
    public function isStatic()
    {
    }
    /**
     * @param  string $visibility
     * @return $this
     */
    public function setVisibility($visibility)
    {
    }
    /**
     * @psalm-return static::VISIBILITY_*
     */
    public function getVisibility()
    {
    }
    /**
     * @param  string $name
     * @return $this
     */
    public function setName($name)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param  DocBlockGenerator|string $docBlock
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setDocBlock($docBlock)
    {
    }
    public function removeDocBlock() : void
    {
    }
    /**
     * @return DocBlockGenerator|null
     */
    public function getDocBlock()
    {
    }
}
