<?php

namespace Laminas\Code\Generator\DocBlock;

/**
 * This class is used in DocBlockGenerator and creates the needed
 * Tag classes depending on the tag. So for example an @author tag
 * will trigger the creation of an AuthorTag class.
 *
 * If none of the classes is applicable, the GenericTag class will be
 * created
 */
class TagManager extends \Laminas\Code\Generic\Prototype\PrototypeClassFactory
{
    public function initializeDefaultTags() : void
    {
    }
    /**
     * @return TagInterface
     */
    public function createTagFromReflection(\Laminas\Code\Reflection\DocBlock\Tag\TagInterface $reflectionTag)
    {
    }
}
