<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class AuthorTag extends \Laminas\Code\Generator\AbstractGenerator implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $authorName;
    /** @var string */
    protected $authorEmail;
    /**
     * @param null|string $authorName
     * @param null|string $authorEmail
     */
    public function __construct($authorName = null, $authorEmail = null)
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use TagManager::createTagFromReflection() instead
     *
     * @return AuthorTag
     */
    public static function fromReflection(\Laminas\Code\Reflection\DocBlock\Tag\TagInterface $reflectionTag)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param string $authorEmail
     * @return $this
     */
    public function setAuthorEmail($authorEmail)
    {
    }
    /**
     * @return string
     */
    public function getAuthorEmail()
    {
    }
    /**
     * @param string $authorName
     * @return $this
     */
    public function setAuthorName($authorName)
    {
    }
    /**
     * @return string
     */
    public function getAuthorName()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
