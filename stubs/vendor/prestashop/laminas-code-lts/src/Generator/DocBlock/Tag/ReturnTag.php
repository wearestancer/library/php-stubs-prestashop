<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class ReturnTag extends \Laminas\Code\Generator\DocBlock\Tag\AbstractTypeableTag implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /**
     * @deprecated Deprecated in 2.3. Use TagManager::createTagFromReflection() instead
     *
     * @return ReturnTag
     */
    public static function fromReflection(\Laminas\Code\Reflection\DocBlock\Tag\TagInterface $reflectionTag)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use setTypes() instead
     *
     * @param string $datatype
     * @return ReturnTag
     */
    public function setDatatype($datatype)
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use getTypes() or getTypesAsString() instead
     *
     * @return string
     */
    public function getDatatype()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
