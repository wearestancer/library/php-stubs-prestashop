<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class ParamTag extends \Laminas\Code\Generator\DocBlock\Tag\AbstractTypeableTag implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $variableName;
    /**
     * @param null|string $variableName
     * @param array       $types
     * @param null|string $description
     */
    public function __construct($variableName = null, $types = [], $description = null)
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use TagManager::createTagFromReflection() instead
     *
     * @return ParamTag
     */
    public static function fromReflection(\Laminas\Code\Reflection\DocBlock\Tag\TagInterface $reflectionTag)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param string $variableName
     * @return $this
     */
    public function setVariableName($variableName)
    {
    }
    /**
     * @return string
     */
    public function getVariableName()
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use setTypes() instead
     *
     * @param string $datatype
     * @return ParamTag
     */
    public function setDatatype($datatype)
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use getTypes() or getTypesAsString() instead
     *
     * @return string
     */
    public function getDatatype()
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use setVariableName() instead
     *
     * @param  string $paramName
     * @return ParamTag
     */
    public function setParamName($paramName)
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use getVariableName() instead
     *
     * @return string
     */
    public function getParamName()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
