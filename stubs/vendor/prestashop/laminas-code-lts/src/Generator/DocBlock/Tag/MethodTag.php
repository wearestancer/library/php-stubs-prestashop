<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class MethodTag extends \Laminas\Code\Generator\DocBlock\Tag\AbstractTypeableTag implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $methodName;
    /** @var bool */
    protected $isStatic = false;
    /**
     * @param null|string $methodName
     * @param array       $types
     * @param null|string $description
     * @param bool        $isStatic
     */
    public function __construct($methodName = null, $types = [], $description = null, $isStatic = false)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param bool $isStatic
     * @return $this
     */
    public function setIsStatic($isStatic)
    {
    }
    /**
     * @return bool
     */
    public function isStatic()
    {
    }
    /**
     * @param string $methodName
     * @return $this
     */
    public function setMethodName($methodName)
    {
    }
    /**
     * @return string
     */
    public function getMethodName()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
