<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

/**
 * This abstract class can be used as parent for all tags
 * that use a type part in their content.
 *
 * @see http://www.phpdoc.org/docs/latest/for-users/phpdoc/types.html
 */
abstract class AbstractTypeableTag extends \Laminas\Code\Generator\AbstractGenerator
{
    /** @var string */
    protected $description;
    /** @var array */
    protected $types = [];
    /**
     * @param string|string[] $types
     * @param null|string     $description
     */
    public function __construct($types = [], $description = null)
    {
    }
    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
    }
    /**
     * @return string
     */
    public function getDescription()
    {
    }
    /**
     * Array of types or string with types delimited by pipe (|)
     * e.g. array('int', 'null') or "int|null"
     *
     * @param array|string $types
     * @return $this
     */
    public function setTypes($types)
    {
    }
    /**
     * @return array
     */
    public function getTypes()
    {
    }
    /**
     * @param string $delimiter
     * @return string
     */
    public function getTypesAsString($delimiter = '|')
    {
    }
}
