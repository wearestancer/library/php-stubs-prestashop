<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class VarTag extends \Laminas\Code\Generator\DocBlock\Tag\AbstractTypeableTag implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /**
     * @param string|string[] $types
     */
    public function __construct(?string $variableName = null, $types = [], ?string $description = null)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function getName() : string
    {
    }
    /**
     * @internal this code is only public for compatibility with the
     *
     * @see \Laminas\Code\Generator\DocBlock\TagManager, which
     *           uses setters
     */
    public function setVariableName(?string $variableName) : void
    {
    }
    public function getVariableName() : ?string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function generate() : string
    {
    }
}
