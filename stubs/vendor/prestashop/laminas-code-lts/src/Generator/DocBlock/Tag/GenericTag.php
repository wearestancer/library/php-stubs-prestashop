<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class GenericTag extends \Laminas\Code\Generator\AbstractGenerator implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface, \Laminas\Code\Generic\Prototype\PrototypeGenericInterface
{
    /** @var string */
    protected $name;
    /** @var string */
    protected $content;
    /**
     * @param null|string $name
     * @param null|string $content
     */
    public function __construct($name = null, $content = null)
    {
    }
    /**
     * @param  string $name
     * @return $this
     */
    public function setName($name)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
