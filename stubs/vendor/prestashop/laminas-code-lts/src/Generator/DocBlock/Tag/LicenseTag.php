<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class LicenseTag extends \Laminas\Code\Generator\AbstractGenerator implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $url;
    /** @var string */
    protected $licenseName;
    /**
     * @param null|string $url
     * @param null|string $licenseName
     */
    public function __construct($url = null, $licenseName = null)
    {
    }
    /**
     * @deprecated Deprecated in 2.3. Use TagManager::createTagFromReflection() instead
     *
     * @return ReturnTag
     */
    public static function fromReflection(\Laminas\Code\Reflection\DocBlock\Tag\TagInterface $reflectionTag)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
    }
    /**
     * @return string
     */
    public function getUrl()
    {
    }
    /**
     * @param  string $name
     * @return $this
     */
    public function setLicenseName($name)
    {
    }
    /**
     * @return string
     */
    public function getLicenseName()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
