<?php

namespace Laminas\Code\Generator\DocBlock\Tag;

class PropertyTag extends \Laminas\Code\Generator\DocBlock\Tag\AbstractTypeableTag implements \Laminas\Code\Generator\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $propertyName;
    /**
     * @param null|string $propertyName
     * @param array       $types
     * @param null|string $description
     */
    public function __construct($propertyName = null, $types = [], $description = null)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param string $propertyName
     * @return $this
     */
    public function setPropertyName($propertyName)
    {
    }
    /**
     * @return string
     */
    public function getPropertyName()
    {
    }
    /**
     * @return string
     */
    public function generate()
    {
    }
}
