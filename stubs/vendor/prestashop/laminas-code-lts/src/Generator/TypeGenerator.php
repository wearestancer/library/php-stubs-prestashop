<?php

namespace Laminas\Code\Generator;

/** @psalm-immutable */
final class TypeGenerator implements \Laminas\Code\Generator\GeneratorInterface
{
    /**
     * @internal
     *
     * @psalm-pure
     */
    public static function fromReflectionType(?\ReflectionType $type, ?\ReflectionClass $currentClass) : ?self
    {
    }
    /**
     * @throws InvalidArgumentException
     * @psalm-pure
     */
    public static function fromTypeString(string $type) : self
    {
    }
    /**
     * {@inheritDoc}
     *
     * Generates the type string, including FQCN "\\" prefix, so that
     * it can directly be used within any code snippet, regardless of
     * imports.
     *
     * @psalm-return non-empty-string
     */
    public function generate() : string
    {
    }
    public function equals(self $otherType) : bool
    {
    }
    /**
     * @return string the cleaned type string. Please note that this value is not suitable for code generation,
     *                since the returned value does not include any root namespace prefixes, when applicable,
     *                and therefore the values cannot be used as FQCN in generated code.
     */
    public function __toString() : string
    {
    }
}
