<?php

namespace Laminas\Code\Scanner;

/** @internal this class is not part of the public API of this package */
class DocBlockScanner
{
    /** @var bool */
    protected $isScanned = false;
    /** @var string */
    protected $docComment;
    /** @var string */
    protected $shortDescription = '';
    /** @var string */
    protected $longDescription = '';
    /** @var array */
    protected $tags = [];
    /**
     * @param  string $docComment
     */
    public function __construct($docComment)
    {
    }
    /**
     * @return string
     */
    public function getShortDescription()
    {
    }
    /**
     * @return string
     */
    public function getLongDescription()
    {
    }
    /**
     * @return array
     */
    public function getTags()
    {
    }
    protected function scan() : void
    {
    }
    /**
     * @phpcs:disable Generic.Formatting.MultipleStatementAlignment.NotSame
     * @return array
     */
    protected function tokenize()
    {
    }
}
