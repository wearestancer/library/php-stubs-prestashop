<?php

namespace Laminas\Code\Exception;

class BadMethodCallException extends \BadMethodCallException implements \Laminas\Code\Exception\ExceptionInterface
{
}
