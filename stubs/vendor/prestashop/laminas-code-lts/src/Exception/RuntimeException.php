<?php

namespace Laminas\Code\Exception;

class RuntimeException extends \RuntimeException implements \Laminas\Code\Exception\ExceptionInterface
{
}
