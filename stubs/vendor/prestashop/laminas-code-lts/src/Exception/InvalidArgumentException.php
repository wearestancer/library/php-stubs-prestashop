<?php

namespace Laminas\Code\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements \Laminas\Code\Exception\ExceptionInterface
{
}
