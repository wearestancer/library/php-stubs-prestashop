<?php

namespace Laminas\Code\Reflection;

class MethodReflection extends \ReflectionMethod implements \Laminas\Code\Reflection\ReflectionInterface
{
    /**
     * Constant use in @MethodReflection to display prototype as an array
     */
    public const PROTOTYPE_AS_ARRAY = 'prototype_as_array';
    /**
     * Constant use in @MethodReflection to display prototype as a string
     */
    public const PROTOTYPE_AS_STRING = 'prototype_as_string';
    /**
     * Retrieve method DocBlock reflection
     *
     * @return DocBlockReflection|false
     */
    public function getDocBlock()
    {
    }
    /**
     * Get start line (position) of method
     *
     * @param  bool $includeDocComment
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function getStartLine($includeDocComment = false)
    {
    }
    /**
     * Get reflection of declaring class
     *
     * @return ClassReflection
     */
    #[\ReturnTypeWillChange]
    public function getDeclaringClass()
    {
    }
    /**
     * Get method prototype
     *
     * @param string $format
     * @return array|string
     */
    #[\ReturnTypeWillChange]
    public function getPrototype($format = self::PROTOTYPE_AS_ARRAY)
    {
    }
    /**
     * Get all method parameter reflection objects
     *
     * @return ParameterReflection[]
     */
    #[\ReturnTypeWillChange]
    public function getParameters()
    {
    }
    /**
     * Get method contents
     *
     * @param  bool $includeDocBlock
     * @return string
     */
    public function getContents($includeDocBlock = true)
    {
    }
    /**
     * Get method body
     *
     * @return string
     */
    public function getBody()
    {
    }
    /**
     * Tokenize method string and return concatenated body
     *
     * @param bool $bodyOnly
     * @return string
     */
    protected function extractMethodContents($bodyOnly = false)
    {
    }
    /**
     * Take current position and find any whitespace
     *
     * @param array $haystack
     * @param int   $position
     * @return string
     */
    protected function extractPrefixedWhitespace($haystack, $position)
    {
    }
    /**
     * Test for ending brace
     *
     * @param array $haystack
     * @param int   $position
     * @return bool
     */
    protected function isEndingBrace($haystack, $position)
    {
    }
    /**
     * Test to see if current position is valid function or
     * closure.  Returns true if it's a function and NOT a closure
     *
     * @param array       $haystack
     * @param int         $position
     * @param null|string $functionName
     * @return bool
     */
    protected function isValidFunction($haystack, $position, $functionName = null)
    {
    }
    /**
     * @return string
     */
    public function toString()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
