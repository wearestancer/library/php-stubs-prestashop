<?php

namespace Laminas\Code\Reflection;

/** @internal this class is not part of the public API of this package */
interface ReflectionInterface extends \Reflector
{
    /**
     * @return string
     */
    public function toString();
}
