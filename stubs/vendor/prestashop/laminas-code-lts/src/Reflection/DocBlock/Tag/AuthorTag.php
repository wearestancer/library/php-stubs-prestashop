<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class AuthorTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $authorName;
    /** @var string */
    protected $authorEmail;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Initializer
     *
     * @param  string $tagDocblockLine
     */
    public function initialize($tagDocblockLine) : void
    {
    }
    /**
     * @return null|string
     */
    public function getAuthorName()
    {
    }
    /**
     * @return null|string
     */
    public function getAuthorEmail()
    {
    }
    /**
     * @return string
     * @psalm-return non-empty-string
     */
    public function __toString()
    {
    }
}
