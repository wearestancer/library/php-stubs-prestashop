<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class MethodTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Reflection\DocBlock\Tag\PhpDocTypedTagInterface
{
    /**
     * Return value type
     *
     * @var string[]
     * @psalm-var list<string>
     */
    protected $types = [];
    /** @var string */
    protected $methodName;
    /** @var string */
    protected $description;
    /**
     * Is static method
     *
     * @var bool
     */
    protected $isStatic = false;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Initializer
     *
     * @param  string $tagDocblockLine
     */
    public function initialize($tagDocblockLine) : void
    {
    }
    /**
     * Get return value type
     *
     * @deprecated 2.0.4 use getTypes instead
     *
     * @return null|string
     */
    public function getReturnType()
    {
    }
    /** {@inheritDoc} */
    public function getTypes()
    {
    }
    /**
     * @return string
     */
    public function getMethodName()
    {
    }
    /**
     * @return null|string
     */
    public function getDescription()
    {
    }
    /**
     * @return bool
     */
    public function isStatic()
    {
    }
    /**
     * @return string
     * @psalm-return non-empty-string
     */
    public function __toString()
    {
    }
}
