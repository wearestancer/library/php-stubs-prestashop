<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class PropertyTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Reflection\DocBlock\Tag\PhpDocTypedTagInterface
{
    /**
     * @var string[]
     * @psalm-var list<string>
     */
    protected $types = [];
    /** @var string */
    protected $propertyName;
    /** @var string */
    protected $description;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Initializer
     *
     * @param  string $tagDocblockLine
     */
    public function initialize($tagDocblockLine) : void
    {
    }
    /**
     * @deprecated 2.0.4 use getTypes instead
     *
     * @return null|string
     */
    public function getType()
    {
    }
    /** {@inheritDoc} */
    public function getTypes()
    {
    }
    /**
     * @return null|string
     */
    public function getPropertyName()
    {
    }
    /**
     * @return null|string
     */
    public function getDescription()
    {
    }
    /**
     * @return string
     * @psalm-return non-empty-string
     */
    public function __toString()
    {
    }
}
