<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class ThrowsTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Reflection\DocBlock\Tag\PhpDocTypedTagInterface
{
    /**
     * @var string[]
     * @psalm-var list<string>
     */
    protected $types = [];
    /** @var string */
    protected $description;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param  string $tagDocBlockLine
     */
    public function initialize($tagDocBlockLine) : void
    {
    }
    /**
     * Get return variable type
     *
     * @deprecated 2.0.4 use getTypes instead
     *
     * @return string
     */
    public function getType()
    {
    }
    /** {@inheritDoc} */
    public function getTypes()
    {
    }
    /**
     * @return string
     */
    public function getDescription()
    {
    }
}
