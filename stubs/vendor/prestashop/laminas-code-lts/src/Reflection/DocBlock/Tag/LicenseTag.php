<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class LicenseTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface
{
    /** @var string */
    protected $url;
    /** @var string */
    protected $licenseName;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Initializer
     *
     * @param  string $tagDocblockLine
     */
    public function initialize($tagDocblockLine) : void
    {
    }
    /**
     * @return null|string
     */
    public function getUrl()
    {
    }
    /**
     * @return null|string
     */
    public function getLicenseName()
    {
    }
    /**
     * @return string
     * @psalm-return non-empty-string
     */
    public function __toString()
    {
    }
}
