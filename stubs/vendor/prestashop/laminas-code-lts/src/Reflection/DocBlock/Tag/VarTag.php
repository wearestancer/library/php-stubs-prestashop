<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class VarTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Reflection\DocBlock\Tag\PhpDocTypedTagInterface
{
    /**
     * {@inheritDoc}
     */
    public function getName() : string
    {
    }
    /**
     * {@inheritDoc}
     */
    public function initialize($tagDocblockLine) : void
    {
    }
    /** {@inheritDoc} */
    public function getTypes() : array
    {
    }
    public function getVariableName() : ?string
    {
    }
    public function getDescription() : ?string
    {
    }
    /**
     * @psalm-return non-empty-string
     */
    public function __toString() : string
    {
    }
}
