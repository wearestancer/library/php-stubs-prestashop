<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

interface TagInterface extends \Laminas\Code\Generic\Prototype\PrototypeInterface
{
    /**
     * @param  string $content
     */
    public function initialize($content) : void;
}
