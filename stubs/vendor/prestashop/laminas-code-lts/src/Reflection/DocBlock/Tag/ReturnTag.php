<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class ReturnTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Reflection\DocBlock\Tag\PhpDocTypedTagInterface
{
    /** @var array */
    protected $types = [];
    /** @var string */
    protected $description;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param  string $tagDocBlockLine
     */
    public function initialize($tagDocBlockLine) : void
    {
    }
    /**
     * @deprecated 2.0.4 use getTypes instead
     *
     * @return string
     */
    public function getType()
    {
    }
    /** {@inheritDoc} */
    public function getTypes()
    {
    }
    /**
     * @return string
     */
    public function getDescription()
    {
    }
}
