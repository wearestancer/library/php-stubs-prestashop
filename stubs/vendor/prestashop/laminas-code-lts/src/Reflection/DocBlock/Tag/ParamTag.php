<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class ParamTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Reflection\DocBlock\Tag\PhpDocTypedTagInterface
{
    /**
     * @var string[]
     * @psalm-return list<string>
     */
    protected $types = [];
    /** @var string */
    protected $variableName;
    /** @var string */
    protected $description;
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Initializer
     *
     * @param  string $tagDocBlockLine
     */
    public function initialize($tagDocBlockLine) : void
    {
    }
    /**
     * Get parameter variable type
     *
     * @deprecated 2.0.4 use getTypes instead
     *
     * @return string
     */
    public function getType()
    {
    }
    /** {@inheritDoc} */
    public function getTypes()
    {
    }
    /**
     * Get parameter name
     *
     * @return string
     */
    public function getVariableName()
    {
    }
    /**
     * @return string
     */
    public function getDescription()
    {
    }
}
