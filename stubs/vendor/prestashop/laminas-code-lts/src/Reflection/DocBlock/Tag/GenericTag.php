<?php

namespace Laminas\Code\Reflection\DocBlock\Tag;

class GenericTag implements \Laminas\Code\Reflection\DocBlock\Tag\TagInterface, \Laminas\Code\Generic\Prototype\PrototypeGenericInterface
{
    /** @var string */
    protected $name;
    /** @var string */
    protected $content;
    /** @var null|string */
    protected $contentSplitCharacter;
    /** @var array */
    protected $values = [];
    /**
     * @param  string $contentSplitCharacter
     */
    public function __construct($contentSplitCharacter = ' ')
    {
    }
    /**
     * @param  string $tagDocBlockLine
     */
    public function initialize($tagDocBlockLine) : void
    {
    }
    /**
     * Get annotation tag name
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @param  string $name
     */
    public function setName($name)
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @param  int $position
     * @return string
     */
    public function returnValue($position)
    {
    }
    /**
     * Serialize to string
     *
     * Required by Reflector
     *
     * @todo   What should this do?
     * @return string
     * @psalm-return non-empty-string
     */
    public function __toString()
    {
    }
    /**
     * @param  string $docBlockLine
     */
    protected function parse($docBlockLine)
    {
    }
}
