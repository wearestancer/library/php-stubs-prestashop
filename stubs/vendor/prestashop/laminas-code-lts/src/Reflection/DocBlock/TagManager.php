<?php

namespace Laminas\Code\Reflection\DocBlock;

class TagManager extends \Laminas\Code\Generic\Prototype\PrototypeClassFactory
{
    public function initializeDefaultTags() : void
    {
    }
    /**
     * @param string      $tagName
     * @param null|string $content
     * @return TagInterface
     */
    public function createTag($tagName, $content = null)
    {
    }
}
