<?php

namespace Laminas\Code\Reflection\Exception;

class RuntimeException extends \Laminas\Code\Exception\RuntimeException implements \Laminas\Code\Reflection\Exception\ExceptionInterface
{
}
