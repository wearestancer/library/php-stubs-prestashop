<?php

namespace Laminas\Code\Reflection\Exception;

interface ExceptionInterface extends \Laminas\Code\Exception\ExceptionInterface
{
}
