<?php

namespace Laminas\Code\Reflection\Exception;

class InvalidArgumentException extends \Laminas\Code\Exception\InvalidArgumentException implements \Laminas\Code\Reflection\Exception\ExceptionInterface
{
}
