<?php

namespace Laminas\Code\Reflection\Exception;

class BadMethodCallException extends \Laminas\Code\Exception\BadMethodCallException implements \Laminas\Code\Reflection\Exception\ExceptionInterface
{
}
