<?php

namespace Laminas\Code\Reflection;

class FunctionReflection extends \ReflectionFunction implements \Laminas\Code\Reflection\ReflectionInterface
{
    /**
     * Constant use in @MethodReflection to display prototype as an array
     */
    public const PROTOTYPE_AS_ARRAY = 'prototype_as_array';
    /**
     * Constant use in @MethodReflection to display prototype as a string
     */
    public const PROTOTYPE_AS_STRING = 'prototype_as_string';
    /**
     * Get function DocBlock
     *
     * @throws Exception\InvalidArgumentException
     * @return DocBlockReflection
     */
    public function getDocBlock()
    {
    }
    /**
     * Get start line (position) of function
     *
     * @param  bool $includeDocComment
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function getStartLine($includeDocComment = false)
    {
    }
    /**
     * Get contents of function
     *
     * @param  bool $includeDocBlock
     * @return string
     */
    public function getContents($includeDocBlock = true)
    {
    }
    /**
     * Get method prototype
     *
     * @param string $format
     * @return array|string
     */
    public function getPrototype($format = self::PROTOTYPE_AS_ARRAY)
    {
    }
    /**
     * Get function parameters
     *
     * @return ParameterReflection[]
     */
    #[\ReturnTypeWillChange]
    public function getParameters()
    {
    }
    /**
     * Get return type tag
     *
     * @throws Exception\InvalidArgumentException
     * @return DocBlockReflection
     */
    public function getReturn()
    {
    }
    /**
     * Get method body
     *
     * @return string|false
     */
    public function getBody()
    {
    }
    /**
     * @return string
     */
    public function toString()
    {
    }
    /**
     * Required due to bug in php
     *
     * @return string
     */
    public function __toString()
    {
    }
}
