<?php

namespace Laminas\Code\Reflection;

/**
 * @todo       implement line numbers
 */
class PropertyReflection extends \ReflectionProperty implements \Laminas\Code\Reflection\ReflectionInterface
{
    /**
     * Get declaring class reflection object
     *
     * @return ClassReflection
     */
    #[\ReturnTypeWillChange]
    public function getDeclaringClass()
    {
    }
    /**
     * Get DocBlock comment
     *
     * @return string|false False if no DocBlock defined
     */
    #[\ReturnTypeWillChange]
    public function getDocComment()
    {
    }
    /**
     * @return false|DocBlockReflection
     */
    public function getDocBlock()
    {
    }
    /**
     * @return string
     */
    public function toString()
    {
    }
}
