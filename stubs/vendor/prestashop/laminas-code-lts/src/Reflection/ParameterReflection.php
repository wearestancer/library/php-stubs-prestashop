<?php

namespace Laminas\Code\Reflection;

class ParameterReflection extends \ReflectionParameter implements \Laminas\Code\Reflection\ReflectionInterface
{
    /** @var bool */
    protected $isFromMethod = false;
    /**
     * Get declaring class reflection object
     *
     * @return ClassReflection
     */
    #[\ReturnTypeWillChange]
    public function getDeclaringClass()
    {
    }
    /**
     * Get class reflection object
     *
     * @return null|ClassReflection
     */
    #[\ReturnTypeWillChange]
    public function getClass()
    {
    }
    /**
     * Get declaring function reflection object
     *
     * @return FunctionReflection|MethodReflection
     */
    #[\ReturnTypeWillChange]
    public function getDeclaringFunction()
    {
    }
    /**
     * Get parameter type
     *
     * @return string|null
     */
    public function detectType()
    {
    }
    /**
     * @return string
     */
    public function toString()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
    /** @psalm-pure */
    public function isPromoted() : bool
    {
    }
    public function isPublicPromoted() : bool
    {
    }
    public function isProtectedPromoted() : bool
    {
    }
    public function isPrivatePromoted() : bool
    {
    }
}
