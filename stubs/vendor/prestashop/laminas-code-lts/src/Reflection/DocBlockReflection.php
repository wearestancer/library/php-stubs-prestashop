<?php

namespace Laminas\Code\Reflection;

class DocBlockReflection implements \Laminas\Code\Reflection\ReflectionInterface
{
    /** @var Reflector */
    protected $reflector;
    /** @var string */
    protected $docComment;
    /** @var DocBlockTagManager */
    protected $tagManager;
    /** @var int */
    protected $startLine;
    /** @var int */
    protected $endLine;
    /** @var string */
    protected $cleanDocComment;
    /** @var string */
    protected $longDescription;
    /** @var string */
    protected $shortDescription;
    /** @var array */
    protected $tags = [];
    /** @var bool */
    protected $isReflected = false;
    /**
     * Export reflection
     *
     * Required by the Reflector interface.
     *
     * @todo   What should this do?
     */
    public static function export() : void
    {
    }
    /**
     * @param  Reflector|string $commentOrReflector
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($commentOrReflector, ?\Laminas\Code\Reflection\DocBlock\TagManager $tagManager = null)
    {
    }
    /**
     * Retrieve contents of DocBlock
     *
     * @return string
     */
    public function getContents()
    {
    }
    /**
     * Get start line (position) of DocBlock
     *
     * @return int
     */
    public function getStartLine()
    {
    }
    /**
     * Get last line (position) of DocBlock
     *
     * @return int
     */
    public function getEndLine()
    {
    }
    /**
     * Get DocBlock short description
     *
     * @return string
     */
    public function getShortDescription()
    {
    }
    /**
     * Get DocBlock long description
     *
     * @return string
     */
    public function getLongDescription()
    {
    }
    /**
     * Does the DocBlock contain the given annotation tag?
     *
     * @param  string $name
     * @return bool
     */
    public function hasTag($name)
    {
    }
    /**
     * Retrieve the given DocBlock tag
     *
     * @param  string $name
     * @return DocBlockTagInterface|false
     */
    public function getTag($name)
    {
    }
    /**
     * Get all DocBlock annotation tags
     *
     * @param  null|string $filter
     * @return DocBlockTagInterface[]
     */
    public function getTags($filter = null)
    {
    }
    /**
     * Parse the DocBlock
     */
    protected function reflect() : void
    {
    }
    /**
     * @return string
     */
    public function toString()
    {
    }
    /**
     * Serialize to string
     *
     * Required by the Reflector interface
     *
     * @return string
     */
    public function __toString()
    {
    }
}
