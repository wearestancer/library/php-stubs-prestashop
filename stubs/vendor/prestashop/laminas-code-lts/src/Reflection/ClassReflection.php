<?php

namespace Laminas\Code\Reflection;

class ClassReflection extends \ReflectionClass implements \Laminas\Code\Reflection\ReflectionInterface
{
    /** @var DocBlockReflection|null */
    protected $docBlock;
    /**
     * Return the classes DocBlock reflection object
     *
     * @return DocBlockReflection|false
     * @throws Exception\ExceptionInterface When missing DocBock or invalid reflection class.
     */
    public function getDocBlock()
    {
    }
    /**
     * Return the start line of the class
     *
     * @param  bool $includeDocComment
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function getStartLine($includeDocComment = false)
    {
    }
    /**
     * Return the contents of the class
     *
     * @param  bool $includeDocBlock
     * @return string
     */
    public function getContents($includeDocBlock = true)
    {
    }
    /**
     * Get all reflection objects of implemented interfaces
     *
     * @return ClassReflection[]
     */
    #[\ReturnTypeWillChange]
    public function getInterfaces()
    {
    }
    /**
     * Return method reflection by name
     *
     * @param  string $name
     * @return MethodReflection
     */
    #[\ReturnTypeWillChange]
    public function getMethod($name)
    {
    }
    /**
     * Get reflection objects of all methods
     *
     * @param  int $filter
     * @return MethodReflection[]
     */
    #[\ReturnTypeWillChange]
    public function getMethods($filter = -1)
    {
    }
    /**
     * Returns an array of reflection classes of traits used by this class.
     *
     * @return null|array
     */
    #[\ReturnTypeWillChange]
    public function getTraits()
    {
    }
    /**
     * Get parent reflection class of reflected class
     *
     * @return ClassReflection|bool
     */
    #[\ReturnTypeWillChange]
    public function getParentClass()
    {
    }
    /**
     * Return reflection property of this class by name
     *
     * @param  string $name
     * @return PropertyReflection
     */
    #[\ReturnTypeWillChange]
    public function getProperty($name)
    {
    }
    /**
     * Return reflection properties of this class
     *
     * @param  int $filter
     * @return PropertyReflection[]
     */
    #[\ReturnTypeWillChange]
    public function getProperties($filter = -1)
    {
    }
    /**
     * @return string
     */
    public function toString()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
