<?php

namespace Laminas\Code\Generic\Prototype;

/** @internal this class is not part of the public API of this package */
interface PrototypeGenericInterface extends \Laminas\Code\Generic\Prototype\PrototypeInterface
{
    /**
     * @param string $name
     */
    public function setName($name);
}
