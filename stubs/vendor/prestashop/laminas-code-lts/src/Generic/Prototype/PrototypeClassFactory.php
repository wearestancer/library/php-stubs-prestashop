<?php

namespace Laminas\Code\Generic\Prototype;

/**
 * This is a factory for classes which are identified by name.
 *
 * All classes that this factory can supply need to
 * be registered before (prototypes). This prototypes need to implement
 * an interface which ensures every prototype has a name.
 *
 * If the factory can not supply the class someone is asking for
 * it tries to fallback on a generic default prototype, which would
 * have need to be set before.
 *
 * @internal this class is not part of the public API of this package
 */
class PrototypeClassFactory
{
    /** @var array */
    protected $prototypes = [];
    /** @var PrototypeGenericInterface|null */
    protected $genericPrototype;
    /**
     * @param PrototypeInterface[] $prototypes
     */
    public function __construct($prototypes = [], ?\Laminas\Code\Generic\Prototype\PrototypeGenericInterface $genericPrototype = null)
    {
    }
    /**
     * @throws Exception\InvalidArgumentException
     */
    public function addPrototype(\Laminas\Code\Generic\Prototype\PrototypeInterface $prototype)
    {
    }
    /**
     * @throws Exception\InvalidArgumentException
     */
    public function setGenericPrototype(\Laminas\Code\Generic\Prototype\PrototypeGenericInterface $prototype)
    {
    }
    /**
     * @param string $name
     * @return string
     */
    protected function normalizeName($name)
    {
    }
    /**
     * @param string $name
     * @return bool
     */
    public function hasPrototype($name)
    {
    }
    /**
     * @param  string $prototypeName
     * @return PrototypeInterface
     * @throws Exception\RuntimeException
     */
    public function getClonedPrototype($prototypeName)
    {
    }
}
