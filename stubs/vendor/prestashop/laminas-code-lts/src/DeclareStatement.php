<?php

namespace Laminas\Code;

class DeclareStatement
{
    public const TICKS = 'ticks';
    public const STRICT_TYPES = 'strict_types';
    public const ENCODING = 'encoding';
    /** @var string */
    protected $directive;
    /** @var int|string */
    protected $value;
    public function getDirective() : string
    {
    }
    /**
     * @return int|string
     */
    public function getValue()
    {
    }
    public static function ticks(int $value) : self
    {
    }
    public static function strictTypes(int $value) : self
    {
    }
    public static function encoding(string $value) : self
    {
    }
    public static function fromArray(array $config) : self
    {
    }
    public function getStatement() : string
    {
    }
}
