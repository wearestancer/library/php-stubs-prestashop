<?php

namespace Http\Message;

/**
 * Factory for PSR-7 Request and Response.
 *
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
interface MessageFactory extends \Http\Message\RequestFactory, \Http\Message\ResponseFactory
{
}
