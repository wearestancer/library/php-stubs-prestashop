<?php

/**
 * PSR-4 autoloader implementation for the MaxMind\DB namespace.
 * First we define the 'mmdb_autoload' function, and then we register
 * it with 'spl_autoload_register' so that PHP knows to use it.
 *
 * @param mixed $class
 */
/**
 * Automatically include the file that defines <code>class</code>.
 *
 * @param string $class
 *                      the name of the class to load
 */
function mmdb_autoload($class) : void
{
}
