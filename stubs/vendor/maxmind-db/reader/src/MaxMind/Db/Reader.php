<?php

namespace MaxMind\Db;

/**
 * Instances of this class provide a reader for the MaxMind DB format. IP
 * addresses can be looked up using the get method.
 */
class Reader
{
    /**
     * Constructs a Reader for the MaxMind DB format. The file passed to it must
     * be a valid MaxMind DB file such as a GeoIp2 database file.
     *
     * @param string $database
     *                         the MaxMind DB file to use
     *
     * @throws InvalidArgumentException for invalid database path or unknown arguments
     * @throws InvalidDatabaseException
     *                                  if the database is invalid or there is an error reading
     *                                  from it
     */
    public function __construct(string $database)
    {
    }
    /**
     * Retrieves the record for the IP address.
     *
     * @param string $ipAddress
     *                          the IP address to look up
     *
     * @throws BadMethodCallException   if this method is called on a closed database
     * @throws InvalidArgumentException if something other than a single IP address is passed to the method
     * @throws InvalidDatabaseException
     *                                  if the database is invalid or there is an error reading
     *                                  from it
     *
     * @return mixed the record for the IP address
     */
    public function get(string $ipAddress)
    {
    }
    /**
     * Retrieves the record for the IP address and its associated network prefix length.
     *
     * @param string $ipAddress
     *                          the IP address to look up
     *
     * @throws BadMethodCallException   if this method is called on a closed database
     * @throws InvalidArgumentException if something other than a single IP address is passed to the method
     * @throws InvalidDatabaseException
     *                                  if the database is invalid or there is an error reading
     *                                  from it
     *
     * @return array an array where the first element is the record and the
     *               second the network prefix length for the record
     */
    public function getWithPrefixLen(string $ipAddress) : array
    {
    }
    /**
     * @throws InvalidArgumentException if arguments are passed to the method
     * @throws BadMethodCallException   if the database has been closed
     *
     * @return Metadata object for the database
     */
    public function metadata() : \MaxMind\Db\Reader\Metadata
    {
    }
    /**
     * Closes the MaxMind DB and returns resources to the system.
     *
     * @throws Exception
     *                   if an I/O error occurs
     */
    public function close() : void
    {
    }
}
