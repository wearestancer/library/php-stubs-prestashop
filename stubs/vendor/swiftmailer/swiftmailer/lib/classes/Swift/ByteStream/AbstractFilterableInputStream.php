<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Provides the base functionality for an InputStream supporting filters.
 *
 * @author Chris Corbyn
 */
abstract class Swift_ByteStream_AbstractFilterableInputStream implements \Swift_InputByteStream, \Swift_Filterable
{
    /**
     * Write sequence.
     */
    protected $sequence = 0;
    /**
     * Commit the given bytes to the storage medium immediately.
     *
     * @param string $bytes
     */
    protected abstract function doCommit($bytes);
    /**
     * Flush any buffers/content with immediate effect.
     */
    protected abstract function flush();
    /**
     * Add a StreamFilter to this InputByteStream.
     *
     * @param string $key
     */
    public function addFilter(\Swift_StreamFilter $filter, $key)
    {
    }
    /**
     * Remove an already present StreamFilter based on its $key.
     *
     * @param string $key
     */
    public function removeFilter($key)
    {
    }
    /**
     * Writes $bytes to the end of the stream.
     *
     * @param string $bytes
     *
     * @throws Swift_IoException
     *
     * @return int
     */
    public function write($bytes)
    {
    }
    /**
     * For any bytes that are currently buffered inside the stream, force them
     * off the buffer.
     *
     * @throws Swift_IoException
     */
    public function commit()
    {
    }
    /**
     * Attach $is to this stream.
     *
     * The stream acts as an observer, receiving all data that is written.
     * All {@link write()} and {@link flushBuffers()} operations will be mirrored.
     */
    public function bind(\Swift_InputByteStream $is)
    {
    }
    /**
     * Remove an already bound stream.
     *
     * If $is is not bound, no errors will be raised.
     * If the stream currently has any buffered data it will be written to $is
     * before unbinding occurs.
     */
    public function unbind(\Swift_InputByteStream $is)
    {
    }
    /**
     * Flush the contents of the stream (empty it) and set the internal pointer
     * to the beginning.
     *
     * @throws Swift_IoException
     */
    public function flushBuffers()
    {
    }
}
