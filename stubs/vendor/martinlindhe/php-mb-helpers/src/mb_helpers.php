<?php

/**
 * @param string $str
 * @param string $encoding
 * @return string Uc Words
 */
function mb_ucwords($str, $encoding = 'UTF-8')
{
}
/**
 * @param string $str
 * @param string $encoding
 * @return string Uc first
 */
function mb_ucfirst($str, $encoding = 'UTF-8')
{
}
/**
 * @param string $str
 * @param string $encoding
 * @return string
 */
function mb_strrev($str, $encoding = 'UTF-8')
{
}
/**
 * @param string $string
 * @param int $mode only mode 1 and 3 is available
 * @param string $encoding
 * @return array|string
 * @throws \Exception
 */
function mb_count_chars($string, $mode, $encoding = 'UTF-8')
{
}
