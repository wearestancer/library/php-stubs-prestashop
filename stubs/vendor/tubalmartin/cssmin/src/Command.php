<?php

namespace tubalmartin\CssMin;

class Command
{
    const SUCCESS_EXIT = 0;
    const FAILURE_EXIT = 1;
    protected $stats = array();
    public static function main()
    {
    }
    public function run()
    {
    }
    protected function getOpt($opts, $options)
    {
    }
    protected function setStat($statName, $statValue)
    {
    }
    protected function formatBytes($size, $precision = 2)
    {
    }
    protected function formatMicroSeconds($microSecs, $precision = 2)
    {
    }
    protected function showStats()
    {
    }
    protected function showHelp()
    {
    }
}
