<?php

namespace GuzzleHttp\Promise;

final class Is
{
    /**
     * Returns true if a promise is pending.
     *
     * @return bool
     */
    public static function pending(\GuzzleHttp\Promise\PromiseInterface $promise)
    {
    }
    /**
     * Returns true if a promise is fulfilled or rejected.
     *
     * @return bool
     */
    public static function settled(\GuzzleHttp\Promise\PromiseInterface $promise)
    {
    }
    /**
     * Returns true if a promise is fulfilled.
     *
     * @return bool
     */
    public static function fulfilled(\GuzzleHttp\Promise\PromiseInterface $promise)
    {
    }
    /**
     * Returns true if a promise is rejected.
     *
     * @return bool
     */
    public static function rejected(\GuzzleHttp\Promise\PromiseInterface $promise)
    {
    }
}
