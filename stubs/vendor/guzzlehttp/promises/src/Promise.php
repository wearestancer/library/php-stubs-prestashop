<?php

namespace GuzzleHttp\Promise;

/**
 * Promises/A+ implementation that avoids recursion when possible.
 *
 * @link https://promisesaplus.com/
 */
class Promise implements \GuzzleHttp\Promise\PromiseInterface
{
    /**
     * @param callable $waitFn   Fn that when invoked resolves the promise.
     * @param callable $cancelFn Fn that when invoked cancels the promise.
     */
    public function __construct(callable $waitFn = null, callable $cancelFn = null)
    {
    }
    public function then(callable $onFulfilled = null, callable $onRejected = null)
    {
    }
    public function otherwise(callable $onRejected)
    {
    }
    public function wait($unwrap = true)
    {
    }
    public function getState()
    {
    }
    public function cancel()
    {
    }
    public function resolve($value)
    {
    }
    public function reject($reason)
    {
    }
}
