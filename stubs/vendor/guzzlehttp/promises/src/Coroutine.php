<?php

namespace GuzzleHttp\Promise;

/**
 * Creates a promise that is resolved using a generator that yields values or
 * promises (somewhat similar to C#'s async keyword).
 *
 * When called, the Coroutine::of method will start an instance of the generator
 * and returns a promise that is fulfilled with its final yielded value.
 *
 * Control is returned back to the generator when the yielded promise settles.
 * This can lead to less verbose code when doing lots of sequential async calls
 * with minimal processing in between.
 *
 *     use GuzzleHttp\Promise;
 *
 *     function createPromise($value) {
 *         return new Promise\FulfilledPromise($value);
 *     }
 *
 *     $promise = Promise\Coroutine::of(function () {
 *         $value = (yield createPromise('a'));
 *         try {
 *             $value = (yield createPromise($value . 'b'));
 *         } catch (\Exception $e) {
 *             // The promise was rejected.
 *         }
 *         yield $value . 'c';
 *     });
 *
 *     // Outputs "abc"
 *     $promise->then(function ($v) { echo $v; });
 *
 * @param callable $generatorFn Generator function to wrap into a promise.
 *
 * @return Promise
 *
 * @link https://github.com/petkaantonov/bluebird/blob/master/API.md#generators inspiration
 */
final class Coroutine implements \GuzzleHttp\Promise\PromiseInterface
{
    public function __construct(callable $generatorFn)
    {
    }
    /**
     * Create a new coroutine.
     *
     * @return self
     */
    public static function of(callable $generatorFn)
    {
    }
    public function then(callable $onFulfilled = null, callable $onRejected = null)
    {
    }
    public function otherwise(callable $onRejected)
    {
    }
    public function wait($unwrap = true)
    {
    }
    public function getState()
    {
    }
    public function resolve($value)
    {
    }
    public function reject($reason)
    {
    }
    public function cancel()
    {
    }
    /**
     * @internal
     */
    public function _handleSuccess($value)
    {
    }
    /**
     * @internal
     */
    public function _handleFailure($reason)
    {
    }
}
