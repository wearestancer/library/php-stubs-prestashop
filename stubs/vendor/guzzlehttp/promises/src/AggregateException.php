<?php

namespace GuzzleHttp\Promise;

/**
 * Exception thrown when too many errors occur in the some() or any() methods.
 */
class AggregateException extends \GuzzleHttp\Promise\RejectionException
{
    public function __construct($msg, array $reasons)
    {
    }
}
