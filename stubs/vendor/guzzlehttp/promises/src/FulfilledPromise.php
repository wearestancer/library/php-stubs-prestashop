<?php

namespace GuzzleHttp\Promise;

/**
 * A promise that has been fulfilled.
 *
 * Thenning off of this promise will invoke the onFulfilled callback
 * immediately and ignore other callbacks.
 */
class FulfilledPromise implements \GuzzleHttp\Promise\PromiseInterface
{
    public function __construct($value)
    {
    }
    public function then(callable $onFulfilled = null, callable $onRejected = null)
    {
    }
    public function otherwise(callable $onRejected)
    {
    }
    public function wait($unwrap = true, $defaultDelivery = null)
    {
    }
    public function getState()
    {
    }
    public function resolve($value)
    {
    }
    public function reject($reason)
    {
    }
    public function cancel()
    {
    }
}
