<?php

namespace GuzzleHttp\Promise;

/**
 * A promise that has been rejected.
 *
 * Thenning off of this promise will invoke the onRejected callback
 * immediately and ignore other callbacks.
 */
class RejectedPromise implements \GuzzleHttp\Promise\PromiseInterface
{
    public function __construct($reason)
    {
    }
    public function then(callable $onFulfilled = null, callable $onRejected = null)
    {
    }
    public function otherwise(callable $onRejected)
    {
    }
    public function wait($unwrap = true, $defaultDelivery = null)
    {
    }
    public function getState()
    {
    }
    public function resolve($value)
    {
    }
    public function reject($reason)
    {
    }
    public function cancel()
    {
    }
}
