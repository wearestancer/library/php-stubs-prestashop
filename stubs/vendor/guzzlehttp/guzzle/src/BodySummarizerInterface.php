<?php

namespace GuzzleHttp;

interface BodySummarizerInterface
{
    /**
     * Returns a summarized message body.
     */
    public function summarize(\Psr\Http\Message\MessageInterface $message) : ?string;
}
