<?php

namespace GuzzleHttp\Handler;

/**
 * Creates curl resources from a request
 *
 * @final
 */
class CurlFactory implements \GuzzleHttp\Handler\CurlFactoryInterface
{
    public const CURL_VERSION_STR = 'curl_version';
    /**
     * @deprecated
     */
    public const LOW_CURL_VERSION_NUMBER = '7.21.2';
    /**
     * @param int $maxHandles Maximum number of idle handles.
     */
    public function __construct(int $maxHandles)
    {
    }
    public function create(\Psr\Http\Message\RequestInterface $request, array $options) : \GuzzleHttp\Handler\EasyHandle
    {
    }
    public function release(\GuzzleHttp\Handler\EasyHandle $easy) : void
    {
    }
    /**
     * Completes a cURL transaction, either returning a response promise or a
     * rejected promise.
     *
     * @param callable(RequestInterface, array): PromiseInterface $handler
     * @param CurlFactoryInterface                                $factory Dictates how the handle is released
     */
    public static function finish(callable $handler, \GuzzleHttp\Handler\EasyHandle $easy, \GuzzleHttp\Handler\CurlFactoryInterface $factory) : \GuzzleHttp\Promise\PromiseInterface
    {
    }
}
