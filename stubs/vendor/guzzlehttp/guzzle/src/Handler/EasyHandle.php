<?php

namespace GuzzleHttp\Handler;

/**
 * Represents a cURL easy handle and the data it populates.
 *
 * @internal
 */
final class EasyHandle
{
    /**
     * @var resource|\CurlHandle cURL resource
     */
    public $handle;
    /**
     * @var StreamInterface Where data is being written
     */
    public $sink;
    /**
     * @var array Received HTTP headers so far
     */
    public $headers = [];
    /**
     * @var ResponseInterface|null Received response (if any)
     */
    public $response;
    /**
     * @var RequestInterface Request being sent
     */
    public $request;
    /**
     * @var array Request options
     */
    public $options = [];
    /**
     * @var int cURL error number (if any)
     */
    public $errno = 0;
    /**
     * @var \Throwable|null Exception during on_headers (if any)
     */
    public $onHeadersException;
    /**
     * @var \Exception|null Exception during createResponse (if any)
     */
    public $createResponseException;
    /**
     * Attach a response to the easy handle based on the received headers.
     *
     * @throws \RuntimeException if no headers have been received or the first
     *                           header line is invalid.
     */
    public function createResponse() : void
    {
    }
    /**
     * @param string $name
     *
     * @return void
     *
     * @throws \BadMethodCallException
     */
    public function __get($name)
    {
    }
}
