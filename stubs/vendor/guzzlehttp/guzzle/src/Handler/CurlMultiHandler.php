<?php

namespace GuzzleHttp\Handler;

/**
 * Returns an asynchronous response using curl_multi_* functions.
 *
 * When using the CurlMultiHandler, custom curl options can be specified as an
 * associative array of curl option constants mapping to values in the
 * **curl** key of the provided request options.
 *
 * @property resource|\CurlMultiHandle $_mh Internal use only. Lazy loaded multi-handle.
 *
 * @final
 */
#[\AllowDynamicProperties]
class CurlMultiHandler
{
    /**
     * This handler accepts the following options:
     *
     * - handle_factory: An optional factory  used to create curl handles
     * - select_timeout: Optional timeout (in seconds) to block before timing
     *   out while selecting curl handles. Defaults to 1 second.
     * - options: An associative array of CURLMOPT_* options and
     *   corresponding values for curl_multi_setopt()
     */
    public function __construct(array $options = [])
    {
    }
    /**
     * @param string $name
     *
     * @return resource|\CurlMultiHandle
     *
     * @throws \BadMethodCallException when another field as `_mh` will be gotten
     * @throws \RuntimeException       when curl can not initialize a multi handle
     */
    public function __get($name)
    {
    }
    public function __destruct()
    {
    }
    public function __invoke(\Psr\Http\Message\RequestInterface $request, array $options) : \GuzzleHttp\Promise\PromiseInterface
    {
    }
    /**
     * Ticks the curl event loop.
     */
    public function tick() : void
    {
    }
    /**
     * Runs until all outstanding connections have completed.
     */
    public function execute() : void
    {
    }
}
