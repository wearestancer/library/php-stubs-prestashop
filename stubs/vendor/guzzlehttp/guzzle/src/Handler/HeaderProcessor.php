<?php

namespace GuzzleHttp\Handler;

/**
 * @internal
 */
final class HeaderProcessor
{
    /**
     * Returns the HTTP version, status code, reason phrase, and headers.
     *
     * @param string[] $headers
     *
     * @throws \RuntimeException
     *
     * @return array{0:string, 1:int, 2:?string, 3:array}
     */
    public static function parseHeaders(array $headers) : array
    {
    }
}
