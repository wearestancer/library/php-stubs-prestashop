<?php

namespace GuzzleHttp;

/**
 * Creates a composed Guzzle handler function by stacking middlewares on top of
 * an HTTP handler function.
 *
 * @final
 */
class HandlerStack
{
    /**
     * Creates a default handler stack that can be used by clients.
     *
     * The returned handler will wrap the provided handler or use the most
     * appropriate default handler for your system. The returned HandlerStack has
     * support for cookies, redirects, HTTP error exceptions, and preparing a body
     * before sending.
     *
     * The returned handler stack can be passed to a client in the "handler"
     * option.
     *
     * @param (callable(RequestInterface, array): PromiseInterface)|null $handler HTTP handler function to use with the stack. If no
     *                                                                            handler is provided, the best handler for your
     *                                                                            system will be utilized.
     */
    public static function create(?callable $handler = null) : self
    {
    }
    /**
     * @param (callable(RequestInterface, array): PromiseInterface)|null $handler Underlying HTTP handler.
     */
    public function __construct(callable $handler = null)
    {
    }
    /**
     * Invokes the handler stack as a composed handler
     *
     * @return ResponseInterface|PromiseInterface
     */
    public function __invoke(\Psr\Http\Message\RequestInterface $request, array $options)
    {
    }
    /**
     * Dumps a string representation of the stack.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Set the HTTP handler that actually returns a promise.
     *
     * @param callable(RequestInterface, array): PromiseInterface $handler Accepts a request and array of options and
     *                                                                     returns a Promise.
     */
    public function setHandler(callable $handler) : void
    {
    }
    /**
     * Returns true if the builder has a handler.
     */
    public function hasHandler() : bool
    {
    }
    /**
     * Unshift a middleware to the bottom of the stack.
     *
     * @param callable(callable): callable $middleware Middleware function
     * @param string                       $name       Name to register for this middleware.
     */
    public function unshift(callable $middleware, ?string $name = null) : void
    {
    }
    /**
     * Push a middleware to the top of the stack.
     *
     * @param callable(callable): callable $middleware Middleware function
     * @param string                       $name       Name to register for this middleware.
     */
    public function push(callable $middleware, string $name = '') : void
    {
    }
    /**
     * Add a middleware before another middleware by name.
     *
     * @param string                       $findName   Middleware to find
     * @param callable(callable): callable $middleware Middleware function
     * @param string                       $withName   Name to register for this middleware.
     */
    public function before(string $findName, callable $middleware, string $withName = '') : void
    {
    }
    /**
     * Add a middleware after another middleware by name.
     *
     * @param string                       $findName   Middleware to find
     * @param callable(callable): callable $middleware Middleware function
     * @param string                       $withName   Name to register for this middleware.
     */
    public function after(string $findName, callable $middleware, string $withName = '') : void
    {
    }
    /**
     * Remove a middleware by instance or name from the stack.
     *
     * @param callable|string $remove Middleware to remove by instance or name.
     */
    public function remove($remove) : void
    {
    }
    /**
     * Compose the middleware and handler into a single callable function.
     *
     * @return callable(RequestInterface, array): PromiseInterface
     */
    public function resolve() : callable
    {
    }
}
