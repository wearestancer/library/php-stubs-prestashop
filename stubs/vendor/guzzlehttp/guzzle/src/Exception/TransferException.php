<?php

namespace GuzzleHttp\Exception;

class TransferException extends \RuntimeException implements \GuzzleHttp\Exception\GuzzleException
{
}
