<?php

namespace GuzzleHttp\Exception;

class TooManyRedirectsException extends \GuzzleHttp\Exception\RequestException
{
}
