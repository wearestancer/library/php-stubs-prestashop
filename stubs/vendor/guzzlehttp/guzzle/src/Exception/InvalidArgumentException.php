<?php

namespace GuzzleHttp\Exception;

final class InvalidArgumentException extends \InvalidArgumentException implements \GuzzleHttp\Exception\GuzzleException
{
}
