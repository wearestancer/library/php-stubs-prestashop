<?php

namespace GuzzleHttp\Exception;

/**
 * Exception when an HTTP error occurs (4xx or 5xx error)
 */
class BadResponseException extends \GuzzleHttp\Exception\RequestException
{
    public function __construct(string $message, \Psr\Http\Message\RequestInterface $request, \Psr\Http\Message\ResponseInterface $response, \Throwable $previous = null, array $handlerContext = [])
    {
    }
    /**
     * Current exception and the ones that extend it will always have a response.
     */
    public function hasResponse() : bool
    {
    }
    /**
     * This function narrows the return type from the parent class and does not allow it to be nullable.
     */
    public function getResponse() : \Psr\Http\Message\ResponseInterface
    {
    }
}
