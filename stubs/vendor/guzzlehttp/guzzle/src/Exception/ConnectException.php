<?php

namespace GuzzleHttp\Exception;

/**
 * Exception thrown when a connection cannot be established.
 *
 * Note that no response is present for a ConnectException
 */
class ConnectException extends \GuzzleHttp\Exception\TransferException implements \Psr\Http\Client\NetworkExceptionInterface
{
    public function __construct(string $message, \Psr\Http\Message\RequestInterface $request, \Throwable $previous = null, array $handlerContext = [])
    {
    }
    /**
     * Get the request that caused the exception
     */
    public function getRequest() : \Psr\Http\Message\RequestInterface
    {
    }
    /**
     * Get contextual information about the error from the underlying handler.
     *
     * The contents of this array will vary depending on which handler you are
     * using. It may also be just an empty array. Relying on this data will
     * couple you to a specific handler, but can give more debug information
     * when needed.
     */
    public function getHandlerContext() : array
    {
    }
}
