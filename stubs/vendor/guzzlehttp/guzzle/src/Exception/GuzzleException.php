<?php

namespace GuzzleHttp\Exception;

interface GuzzleException extends \Psr\Http\Client\ClientExceptionInterface
{
}
