<?php

namespace GuzzleHttp\Exception;

/**
 * HTTP Request exception
 */
class RequestException extends \GuzzleHttp\Exception\TransferException implements \Psr\Http\Client\RequestExceptionInterface
{
    public function __construct(string $message, \Psr\Http\Message\RequestInterface $request, \Psr\Http\Message\ResponseInterface $response = null, \Throwable $previous = null, array $handlerContext = [])
    {
    }
    /**
     * Wrap non-RequestExceptions with a RequestException
     */
    public static function wrapException(\Psr\Http\Message\RequestInterface $request, \Throwable $e) : \GuzzleHttp\Exception\RequestException
    {
    }
    /**
     * Factory method to create a new exception with a normalized error message
     *
     * @param RequestInterface             $request        Request sent
     * @param ResponseInterface            $response       Response received
     * @param \Throwable|null              $previous       Previous exception
     * @param array                        $handlerContext Optional handler context
     * @param BodySummarizerInterface|null $bodySummarizer Optional body summarizer
     */
    public static function create(\Psr\Http\Message\RequestInterface $request, \Psr\Http\Message\ResponseInterface $response = null, \Throwable $previous = null, array $handlerContext = [], \GuzzleHttp\BodySummarizerInterface $bodySummarizer = null) : self
    {
    }
    /**
     * Get the request that caused the exception
     */
    public function getRequest() : \Psr\Http\Message\RequestInterface
    {
    }
    /**
     * Get the associated response
     */
    public function getResponse() : ?\Psr\Http\Message\ResponseInterface
    {
    }
    /**
     * Check if a response was received
     */
    public function hasResponse() : bool
    {
    }
    /**
     * Get contextual information about the error from the underlying handler.
     *
     * The contents of this array will vary depending on which handler you are
     * using. It may also be just an empty array. Relying on this data will
     * couple you to a specific handler, but can give more debug information
     * when needed.
     */
    public function getHandlerContext() : array
    {
    }
}
