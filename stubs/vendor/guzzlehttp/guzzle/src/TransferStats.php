<?php

namespace GuzzleHttp;

/**
 * Represents data at the point after it was transferred either successfully
 * or after a network error.
 */
final class TransferStats
{
    /**
     * @param RequestInterface       $request          Request that was sent.
     * @param ResponseInterface|null $response         Response received (if any)
     * @param float|null             $transferTime     Total handler transfer time.
     * @param mixed                  $handlerErrorData Handler error data.
     * @param array                  $handlerStats     Handler specific stats.
     */
    public function __construct(\Psr\Http\Message\RequestInterface $request, ?\Psr\Http\Message\ResponseInterface $response = null, ?float $transferTime = null, $handlerErrorData = null, array $handlerStats = [])
    {
    }
    public function getRequest() : \Psr\Http\Message\RequestInterface
    {
    }
    /**
     * Returns the response that was received (if any).
     */
    public function getResponse() : ?\Psr\Http\Message\ResponseInterface
    {
    }
    /**
     * Returns true if a response was received.
     */
    public function hasResponse() : bool
    {
    }
    /**
     * Gets handler specific error data.
     *
     * This might be an exception, a integer representing an error code, or
     * anything else. Relying on this value assumes that you know what handler
     * you are using.
     *
     * @return mixed
     */
    public function getHandlerErrorData()
    {
    }
    /**
     * Get the effective URI the request was sent to.
     */
    public function getEffectiveUri() : \Psr\Http\Message\UriInterface
    {
    }
    /**
     * Get the estimated time the request was being transferred by the handler.
     *
     * @return float|null Time in seconds.
     */
    public function getTransferTime() : ?float
    {
    }
    /**
     * Gets an array of all of the handler specific transfer data.
     */
    public function getHandlerStats() : array
    {
    }
    /**
     * Get a specific handler statistic from the handler by name.
     *
     * @param string $stat Handler specific transfer stat to retrieve.
     *
     * @return mixed|null
     */
    public function getHandlerStat(string $stat)
    {
    }
}
