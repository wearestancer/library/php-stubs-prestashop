<?php

namespace GuzzleHttp;

/**
 * Prepares requests that contain a body, adding the Content-Length,
 * Content-Type, and Expect headers.
 *
 * @final
 */
class PrepareBodyMiddleware
{
    /**
     * @param callable(RequestInterface, array): PromiseInterface $nextHandler Next handler to invoke.
     */
    public function __construct(callable $nextHandler)
    {
    }
    public function __invoke(\Psr\Http\Message\RequestInterface $request, array $options) : \GuzzleHttp\Promise\PromiseInterface
    {
    }
}
