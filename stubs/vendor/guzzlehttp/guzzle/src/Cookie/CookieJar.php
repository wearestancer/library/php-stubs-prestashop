<?php

namespace GuzzleHttp\Cookie;

/**
 * Cookie jar that stores cookies as an array
 */
class CookieJar implements \GuzzleHttp\Cookie\CookieJarInterface
{
    /**
     * @param bool  $strictMode  Set to true to throw exceptions when invalid
     *                           cookies are added to the cookie jar.
     * @param array $cookieArray Array of SetCookie objects or a hash of
     *                           arrays that can be used with the SetCookie
     *                           constructor
     */
    public function __construct(bool $strictMode = false, array $cookieArray = [])
    {
    }
    /**
     * Create a new Cookie jar from an associative array and domain.
     *
     * @param array  $cookies Cookies to create the jar from
     * @param string $domain  Domain to set the cookies to
     */
    public static function fromArray(array $cookies, string $domain) : self
    {
    }
    /**
     * Evaluate if this cookie should be persisted to storage
     * that survives between requests.
     *
     * @param SetCookie $cookie              Being evaluated.
     * @param bool      $allowSessionCookies If we should persist session cookies
     */
    public static function shouldPersist(\GuzzleHttp\Cookie\SetCookie $cookie, bool $allowSessionCookies = false) : bool
    {
    }
    /**
     * Finds and returns the cookie based on the name
     *
     * @param string $name cookie name to search for
     *
     * @return SetCookie|null cookie that was found or null if not found
     */
    public function getCookieByName(string $name) : ?\GuzzleHttp\Cookie\SetCookie
    {
    }
    /**
     * @inheritDoc
     */
    public function toArray() : array
    {
    }
    /**
     * @inheritDoc
     */
    public function clear(?string $domain = null, ?string $path = null, ?string $name = null) : void
    {
    }
    /**
     * @inheritDoc
     */
    public function clearSessionCookies() : void
    {
    }
    /**
     * @inheritDoc
     */
    public function setCookie(\GuzzleHttp\Cookie\SetCookie $cookie) : bool
    {
    }
    public function count() : int
    {
    }
    /**
     * @return \ArrayIterator<int, SetCookie>
     */
    public function getIterator() : \ArrayIterator
    {
    }
    public function extractCookies(\Psr\Http\Message\RequestInterface $request, \Psr\Http\Message\ResponseInterface $response) : void
    {
    }
    public function withCookieHeader(\Psr\Http\Message\RequestInterface $request) : \Psr\Http\Message\RequestInterface
    {
    }
}
