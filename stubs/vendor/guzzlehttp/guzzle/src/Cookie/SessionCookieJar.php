<?php

namespace GuzzleHttp\Cookie;

/**
 * Persists cookies in the client session
 */
class SessionCookieJar extends \GuzzleHttp\Cookie\CookieJar
{
    /**
     * Create a new SessionCookieJar object
     *
     * @param string $sessionKey          Session key name to store the cookie
     *                                    data in session
     * @param bool   $storeSessionCookies Set to true to store session cookies
     *                                    in the cookie jar.
     */
    public function __construct(string $sessionKey, bool $storeSessionCookies = false)
    {
    }
    /**
     * Saves cookies to session when shutting down
     */
    public function __destruct()
    {
    }
    /**
     * Save cookies to the client session
     */
    public function save() : void
    {
    }
    /**
     * Load the contents of the client session into the data array
     */
    protected function load() : void
    {
    }
}
