<?php

namespace GuzzleHttp;

/**
 * Request redirect middleware.
 *
 * Apply this middleware like other middleware using
 * {@see \GuzzleHttp\Middleware::redirect()}.
 *
 * @final
 */
class RedirectMiddleware
{
    public const HISTORY_HEADER = 'X-Guzzle-Redirect-History';
    public const STATUS_HISTORY_HEADER = 'X-Guzzle-Redirect-Status-History';
    /**
     * @var array
     */
    public static $defaultSettings = ['max' => 5, 'protocols' => ['http', 'https'], 'strict' => false, 'referer' => false, 'track_redirects' => false];
    /**
     * @param callable(RequestInterface, array): PromiseInterface $nextHandler Next handler to invoke.
     */
    public function __construct(callable $nextHandler)
    {
    }
    public function __invoke(\Psr\Http\Message\RequestInterface $request, array $options) : \GuzzleHttp\Promise\PromiseInterface
    {
    }
    /**
     * @return ResponseInterface|PromiseInterface
     */
    public function checkRedirect(\Psr\Http\Message\RequestInterface $request, array $options, \Psr\Http\Message\ResponseInterface $response)
    {
    }
    public function modifyRequest(\Psr\Http\Message\RequestInterface $request, array $options, \Psr\Http\Message\ResponseInterface $response) : \Psr\Http\Message\RequestInterface
    {
    }
}
