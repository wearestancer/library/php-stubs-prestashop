<?php

namespace GuzzleHttp;

final class BodySummarizer implements \GuzzleHttp\BodySummarizerInterface
{
    public function __construct(int $truncateAt = null)
    {
    }
    /**
     * Returns a summarized message body.
     */
    public function summarize(\Psr\Http\Message\MessageInterface $message) : ?string
    {
    }
}
