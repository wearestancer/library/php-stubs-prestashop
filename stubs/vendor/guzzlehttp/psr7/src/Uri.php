<?php

namespace GuzzleHttp\Psr7;

/**
 * PSR-7 URI implementation.
 *
 * @author Michael Dowling
 * @author Tobias Schultze
 * @author Matthew Weier O'Phinney
 */
class Uri implements \Psr\Http\Message\UriInterface, \JsonSerializable
{
    public function __construct(string $uri = '')
    {
    }
    public function __toString() : string
    {
    }
    /**
     * Composes a URI reference string from its various components.
     *
     * Usually this method does not need to be called manually but instead is used indirectly via
     * `Psr\Http\Message\UriInterface::__toString`.
     *
     * PSR-7 UriInterface treats an empty component the same as a missing component as
     * getQuery(), getFragment() etc. always return a string. This explains the slight
     * difference to RFC 3986 Section 5.3.
     *
     * Another adjustment is that the authority separator is added even when the authority is missing/empty
     * for the "file" scheme. This is because PHP stream functions like `file_get_contents` only work with
     * `file:///myfile` but not with `file:/myfile` although they are equivalent according to RFC 3986. But
     * `file:///` is the more common syntax for the file scheme anyway (Chrome for example redirects to
     * that format).
     *
     * @link https://tools.ietf.org/html/rfc3986#section-5.3
     */
    public static function composeComponents(?string $scheme, ?string $authority, string $path, ?string $query, ?string $fragment) : string
    {
    }
    /**
     * Whether the URI has the default port of the current scheme.
     *
     * `Psr\Http\Message\UriInterface::getPort` may return null or the standard port. This method can be used
     * independently of the implementation.
     */
    public static function isDefaultPort(\Psr\Http\Message\UriInterface $uri) : bool
    {
    }
    /**
     * Whether the URI is absolute, i.e. it has a scheme.
     *
     * An instance of UriInterface can either be an absolute URI or a relative reference. This method returns true
     * if it is the former. An absolute URI has a scheme. A relative reference is used to express a URI relative
     * to another URI, the base URI. Relative references can be divided into several forms:
     * - network-path references, e.g. '//example.com/path'
     * - absolute-path references, e.g. '/path'
     * - relative-path references, e.g. 'subpath'
     *
     * @see Uri::isNetworkPathReference
     * @see Uri::isAbsolutePathReference
     * @see Uri::isRelativePathReference
     * @link https://tools.ietf.org/html/rfc3986#section-4
     */
    public static function isAbsolute(\Psr\Http\Message\UriInterface $uri) : bool
    {
    }
    /**
     * Whether the URI is a network-path reference.
     *
     * A relative reference that begins with two slash characters is termed an network-path reference.
     *
     * @link https://tools.ietf.org/html/rfc3986#section-4.2
     */
    public static function isNetworkPathReference(\Psr\Http\Message\UriInterface $uri) : bool
    {
    }
    /**
     * Whether the URI is a absolute-path reference.
     *
     * A relative reference that begins with a single slash character is termed an absolute-path reference.
     *
     * @link https://tools.ietf.org/html/rfc3986#section-4.2
     */
    public static function isAbsolutePathReference(\Psr\Http\Message\UriInterface $uri) : bool
    {
    }
    /**
     * Whether the URI is a relative-path reference.
     *
     * A relative reference that does not begin with a slash character is termed a relative-path reference.
     *
     * @link https://tools.ietf.org/html/rfc3986#section-4.2
     */
    public static function isRelativePathReference(\Psr\Http\Message\UriInterface $uri) : bool
    {
    }
    /**
     * Whether the URI is a same-document reference.
     *
     * A same-document reference refers to a URI that is, aside from its fragment
     * component, identical to the base URI. When no base URI is given, only an empty
     * URI reference (apart from its fragment) is considered a same-document reference.
     *
     * @param UriInterface      $uri  The URI to check
     * @param UriInterface|null $base An optional base URI to compare against
     *
     * @link https://tools.ietf.org/html/rfc3986#section-4.4
     */
    public static function isSameDocumentReference(\Psr\Http\Message\UriInterface $uri, \Psr\Http\Message\UriInterface $base = null) : bool
    {
    }
    /**
     * Creates a new URI with a specific query string value removed.
     *
     * Any existing query string values that exactly match the provided key are
     * removed.
     *
     * @param UriInterface $uri URI to use as a base.
     * @param string       $key Query string key to remove.
     */
    public static function withoutQueryValue(\Psr\Http\Message\UriInterface $uri, string $key) : \Psr\Http\Message\UriInterface
    {
    }
    /**
     * Creates a new URI with a specific query string value.
     *
     * Any existing query string values that exactly match the provided key are
     * removed and replaced with the given key value pair.
     *
     * A value of null will set the query string key without a value, e.g. "key"
     * instead of "key=value".
     *
     * @param UriInterface $uri   URI to use as a base.
     * @param string       $key   Key to set.
     * @param string|null  $value Value to set
     */
    public static function withQueryValue(\Psr\Http\Message\UriInterface $uri, string $key, ?string $value) : \Psr\Http\Message\UriInterface
    {
    }
    /**
     * Creates a new URI with multiple specific query string values.
     *
     * It has the same behavior as withQueryValue() but for an associative array of key => value.
     *
     * @param UriInterface               $uri           URI to use as a base.
     * @param array<string, string|null> $keyValueArray Associative array of key and values
     */
    public static function withQueryValues(\Psr\Http\Message\UriInterface $uri, array $keyValueArray) : \Psr\Http\Message\UriInterface
    {
    }
    /**
     * Creates a URI from a hash of `parse_url` components.
     *
     * @link http://php.net/manual/en/function.parse-url.php
     *
     * @throws MalformedUriException If the components do not form a valid URI.
     */
    public static function fromParts(array $parts) : \Psr\Http\Message\UriInterface
    {
    }
    public function getScheme() : string
    {
    }
    public function getAuthority() : string
    {
    }
    public function getUserInfo() : string
    {
    }
    public function getHost() : string
    {
    }
    public function getPort() : ?int
    {
    }
    public function getPath() : string
    {
    }
    public function getQuery() : string
    {
    }
    public function getFragment() : string
    {
    }
    public function withScheme($scheme) : \Psr\Http\Message\UriInterface
    {
    }
    public function withUserInfo($user, $password = null) : \Psr\Http\Message\UriInterface
    {
    }
    public function withHost($host) : \Psr\Http\Message\UriInterface
    {
    }
    public function withPort($port) : \Psr\Http\Message\UriInterface
    {
    }
    public function withPath($path) : \Psr\Http\Message\UriInterface
    {
    }
    public function withQuery($query) : \Psr\Http\Message\UriInterface
    {
    }
    public function withFragment($fragment) : \Psr\Http\Message\UriInterface
    {
    }
    public function jsonSerialize() : string
    {
    }
}
