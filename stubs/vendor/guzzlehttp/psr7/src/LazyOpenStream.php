<?php

namespace GuzzleHttp\Psr7;

/**
 * Lazily reads or writes to a file that is opened only after an IO operation
 * take place on the stream.
 */
final class LazyOpenStream implements \Psr\Http\Message\StreamInterface
{
    use \GuzzleHttp\Psr7\StreamDecoratorTrait;
    /**
     * @param string $filename File to lazily open
     * @param string $mode     fopen mode to use when opening the stream
     */
    public function __construct(string $filename, string $mode)
    {
    }
}
