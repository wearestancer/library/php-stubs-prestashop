<?php

namespace GuzzleHttp\Psr7;

/**
 * PSR-7 response implementation.
 */
class Response implements \Psr\Http\Message\ResponseInterface
{
    use \GuzzleHttp\Psr7\MessageTrait;
    /**
     * @param int                                  $status  Status code
     * @param array<string, string|string[]>       $headers Response headers
     * @param string|resource|StreamInterface|null $body    Response body
     * @param string                               $version Protocol version
     * @param string|null                          $reason  Reason phrase (when empty a default will be used based on the status code)
     */
    public function __construct(int $status = 200, array $headers = [], $body = null, string $version = '1.1', string $reason = null)
    {
    }
    public function getStatusCode() : int
    {
    }
    public function getReasonPhrase() : string
    {
    }
    public function withStatus($code, $reasonPhrase = '') : \Psr\Http\Message\ResponseInterface
    {
    }
}
