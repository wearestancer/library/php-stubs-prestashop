<?php

namespace GuzzleHttp\Psr7;

/**
 * PHP stream implementation.
 */
class Stream implements \Psr\Http\Message\StreamInterface
{
    /**
     * This constructor accepts an associative array of options.
     *
     * - size: (int) If a read stream would otherwise have an indeterminate
     *   size, but the size is known due to foreknowledge, then you can
     *   provide that size, in bytes.
     * - metadata: (array) Any additional metadata to return when the metadata
     *   of the stream is accessed.
     *
     * @param resource                            $stream  Stream resource to wrap.
     * @param array{size?: int, metadata?: array} $options Associative array of options.
     *
     * @throws \InvalidArgumentException if the stream is not a stream resource
     */
    public function __construct($stream, array $options = [])
    {
    }
    /**
     * Closes the stream when the destructed
     */
    public function __destruct()
    {
    }
    public function __toString() : string
    {
    }
    public function getContents() : string
    {
    }
    public function close() : void
    {
    }
    public function detach()
    {
    }
    public function getSize() : ?int
    {
    }
    public function isReadable() : bool
    {
    }
    public function isWritable() : bool
    {
    }
    public function isSeekable() : bool
    {
    }
    public function eof() : bool
    {
    }
    public function tell() : int
    {
    }
    public function rewind() : void
    {
    }
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    public function read($length) : string
    {
    }
    public function write($string) : int
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getMetadata($key = null)
    {
    }
}
