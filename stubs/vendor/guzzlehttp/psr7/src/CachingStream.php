<?php

namespace GuzzleHttp\Psr7;

/**
 * Stream decorator that can cache previously read bytes from a sequentially
 * read stream.
 */
final class CachingStream implements \Psr\Http\Message\StreamInterface
{
    use \GuzzleHttp\Psr7\StreamDecoratorTrait;
    /**
     * We will treat the buffer object as the body of the stream
     *
     * @param StreamInterface $stream Stream to cache. The cursor is assumed to be at the beginning of the stream.
     * @param StreamInterface $target Optionally specify where data is cached
     */
    public function __construct(\Psr\Http\Message\StreamInterface $stream, \Psr\Http\Message\StreamInterface $target = null)
    {
    }
    public function getSize() : ?int
    {
    }
    public function rewind() : void
    {
    }
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    public function read($length) : string
    {
    }
    public function write($string) : int
    {
    }
    public function eof() : bool
    {
    }
    /**
     * Close both the remote stream and buffer stream
     */
    public function close() : void
    {
    }
}
