<?php

namespace GuzzleHttp\Psr7;

/**
 * Stream decorator that begins dropping data once the size of the underlying
 * stream becomes too full.
 */
final class DroppingStream implements \Psr\Http\Message\StreamInterface
{
    use \GuzzleHttp\Psr7\StreamDecoratorTrait;
    /**
     * @param StreamInterface $stream    Underlying stream to decorate.
     * @param int             $maxLength Maximum size before dropping data.
     */
    public function __construct(\Psr\Http\Message\StreamInterface $stream, int $maxLength)
    {
    }
    public function write($string) : int
    {
    }
}
