<?php

namespace GuzzleHttp\Psr7;

/**
 * Provides methods to determine if a modified URL should be considered cross-origin.
 *
 * @author Graham Campbell
 */
final class UriComparator
{
    /**
     * Determines if a modified URL should be considered cross-origin with
     * respect to an original URL.
     */
    public static function isCrossOrigin(\Psr\Http\Message\UriInterface $original, \Psr\Http\Message\UriInterface $modified) : bool
    {
    }
}
