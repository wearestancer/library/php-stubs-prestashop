<?php

namespace GuzzleHttp\Psr7;

/**
 * Reads from multiple streams, one after the other.
 *
 * This is a read-only stream decorator.
 */
final class AppendStream implements \Psr\Http\Message\StreamInterface
{
    /**
     * @param StreamInterface[] $streams Streams to decorate. Each stream must
     *                                   be readable.
     */
    public function __construct(array $streams = [])
    {
    }
    public function __toString() : string
    {
    }
    /**
     * Add a stream to the AppendStream
     *
     * @param StreamInterface $stream Stream to append. Must be readable.
     *
     * @throws \InvalidArgumentException if the stream is not readable
     */
    public function addStream(\Psr\Http\Message\StreamInterface $stream) : void
    {
    }
    public function getContents() : string
    {
    }
    /**
     * Closes each attached stream.
     */
    public function close() : void
    {
    }
    /**
     * Detaches each attached stream.
     *
     * Returns null as it's not clear which underlying stream resource to return.
     */
    public function detach()
    {
    }
    public function tell() : int
    {
    }
    /**
     * Tries to calculate the size by adding the size of each stream.
     *
     * If any of the streams do not return a valid number, then the size of the
     * append stream cannot be determined and null is returned.
     */
    public function getSize() : ?int
    {
    }
    public function eof() : bool
    {
    }
    public function rewind() : void
    {
    }
    /**
     * Attempts to seek to the given position. Only supports SEEK_SET.
     */
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    /**
     * Reads from all of the appended streams until the length is met or EOF.
     */
    public function read($length) : string
    {
    }
    public function isReadable() : bool
    {
    }
    public function isWritable() : bool
    {
    }
    public function isSeekable() : bool
    {
    }
    public function write($string) : int
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getMetadata($key = null)
    {
    }
}
