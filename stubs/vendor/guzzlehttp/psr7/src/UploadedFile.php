<?php

namespace GuzzleHttp\Psr7;

class UploadedFile implements \Psr\Http\Message\UploadedFileInterface
{
    /**
     * @param StreamInterface|string|resource $streamOrFile
     */
    public function __construct($streamOrFile, ?int $size, int $errorStatus, string $clientFilename = null, string $clientMediaType = null)
    {
    }
    public function isMoved() : bool
    {
    }
    public function getStream() : \Psr\Http\Message\StreamInterface
    {
    }
    public function moveTo($targetPath) : void
    {
    }
    public function getSize() : ?int
    {
    }
    public function getError() : int
    {
    }
    public function getClientFilename() : ?string
    {
    }
    public function getClientMediaType() : ?string
    {
    }
}
