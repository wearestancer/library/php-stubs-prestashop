<?php

namespace GuzzleHttp\Psr7;

/**
 * Trait implementing functionality common to requests and responses.
 */
trait MessageTrait
{
    /** @var array<string, string[]> Map of all registered headers, as original name => array of values */
    private $headers = [];
    /** @var array<string, string> Map of lowercase header name => original name at registration */
    private $headerNames = [];
    /** @var string */
    private $protocol = '1.1';
    /** @var StreamInterface|null */
    private $stream;
    public function getProtocolVersion() : string
    {
    }
    public function withProtocolVersion($version) : \Psr\Http\Message\MessageInterface
    {
    }
    public function getHeaders() : array
    {
    }
    public function hasHeader($header) : bool
    {
    }
    public function getHeader($header) : array
    {
    }
    public function getHeaderLine($header) : string
    {
    }
    public function withHeader($header, $value) : \Psr\Http\Message\MessageInterface
    {
    }
    public function withAddedHeader($header, $value) : \Psr\Http\Message\MessageInterface
    {
    }
    public function withoutHeader($header) : \Psr\Http\Message\MessageInterface
    {
    }
    public function getBody() : \Psr\Http\Message\StreamInterface
    {
    }
    public function withBody(\Psr\Http\Message\StreamInterface $body) : \Psr\Http\Message\MessageInterface
    {
    }
    /**
     * @param array<string|int, string|string[]> $headers
     */
    private function setHeaders(array $headers) : void
    {
    }
    /**
     * @param mixed $value
     *
     * @return string[]
     */
    private function normalizeHeaderValue($value) : array
    {
    }
    /**
     * Trims whitespace from the header values.
     *
     * Spaces and tabs ought to be excluded by parsers when extracting the field value from a header field.
     *
     * header-field = field-name ":" OWS field-value OWS
     * OWS          = *( SP / HTAB )
     *
     * @param mixed[] $values Header values
     *
     * @return string[] Trimmed header values
     *
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     */
    private function trimAndValidateHeaderValues(array $values) : array
    {
    }
    /**
     * @see https://tools.ietf.org/html/rfc7230#section-3.2
     *
     * @param mixed $header
     */
    private function assertHeader($header) : void
    {
    }
    /**
     * @see https://tools.ietf.org/html/rfc7230#section-3.2
     *
     * field-value    = *( field-content / obs-fold )
     * field-content  = field-vchar [ 1*( SP / HTAB ) field-vchar ]
     * field-vchar    = VCHAR / obs-text
     * VCHAR          = %x21-7E
     * obs-text       = %x80-FF
     * obs-fold       = CRLF 1*( SP / HTAB )
     */
    private function assertValue(string $value) : void
    {
    }
}
