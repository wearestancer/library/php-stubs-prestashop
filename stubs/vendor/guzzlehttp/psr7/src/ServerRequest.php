<?php

namespace GuzzleHttp\Psr7;

/**
 * Server-side HTTP request
 *
 * Extends the Request definition to add methods for accessing incoming data,
 * specifically server parameters, cookies, matched path parameters, query
 * string arguments, body parameters, and upload file information.
 *
 * "Attributes" are discovered via decomposing the request (and usually
 * specifically the URI path), and typically will be injected by the application.
 *
 * Requests are considered immutable; all methods that might change state are
 * implemented such that they retain the internal state of the current
 * message and return a new instance that contains the changed state.
 */
class ServerRequest extends \GuzzleHttp\Psr7\Request implements \Psr\Http\Message\ServerRequestInterface
{
    /**
     * @param string                               $method       HTTP method
     * @param string|UriInterface                  $uri          URI
     * @param array<string, string|string[]>       $headers      Request headers
     * @param string|resource|StreamInterface|null $body         Request body
     * @param string                               $version      Protocol version
     * @param array                                $serverParams Typically the $_SERVER superglobal
     */
    public function __construct(string $method, $uri, array $headers = [], $body = null, string $version = '1.1', array $serverParams = [])
    {
    }
    /**
     * Return an UploadedFile instance array.
     *
     * @param array $files An array which respect $_FILES structure
     *
     * @throws InvalidArgumentException for unrecognized values
     */
    public static function normalizeFiles(array $files) : array
    {
    }
    /**
     * Return a ServerRequest populated with superglobals:
     * $_GET
     * $_POST
     * $_COOKIE
     * $_FILES
     * $_SERVER
     */
    public static function fromGlobals() : \Psr\Http\Message\ServerRequestInterface
    {
    }
    /**
     * Get a Uri populated with values from $_SERVER.
     */
    public static function getUriFromGlobals() : \Psr\Http\Message\UriInterface
    {
    }
    public function getServerParams() : array
    {
    }
    public function getUploadedFiles() : array
    {
    }
    public function withUploadedFiles(array $uploadedFiles) : \Psr\Http\Message\ServerRequestInterface
    {
    }
    public function getCookieParams() : array
    {
    }
    public function withCookieParams(array $cookies) : \Psr\Http\Message\ServerRequestInterface
    {
    }
    public function getQueryParams() : array
    {
    }
    public function withQueryParams(array $query) : \Psr\Http\Message\ServerRequestInterface
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|object|null
     */
    public function getParsedBody()
    {
    }
    public function withParsedBody($data) : \Psr\Http\Message\ServerRequestInterface
    {
    }
    public function getAttributes() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getAttribute($attribute, $default = null)
    {
    }
    public function withAttribute($attribute, $value) : \Psr\Http\Message\ServerRequestInterface
    {
    }
    public function withoutAttribute($attribute) : \Psr\Http\Message\ServerRequestInterface
    {
    }
}
