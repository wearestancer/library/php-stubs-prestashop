<?php

namespace GuzzleHttp\Psr7;

/**
 * Stream that when read returns bytes for a streaming multipart or
 * multipart/form-data stream.
 */
final class MultipartStream implements \Psr\Http\Message\StreamInterface
{
    use \GuzzleHttp\Psr7\StreamDecoratorTrait;
    /**
     * @param array  $elements Array of associative arrays, each containing a
     *                         required "name" key mapping to the form field,
     *                         name, a required "contents" key mapping to a
     *                         StreamInterface/resource/string, an optional
     *                         "headers" associative array of custom headers,
     *                         and an optional "filename" key mapping to a
     *                         string to send as the filename in the part.
     * @param string $boundary You can optionally provide a specific boundary
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $elements = [], string $boundary = null)
    {
    }
    public function getBoundary() : string
    {
    }
    public function isWritable() : bool
    {
    }
}
