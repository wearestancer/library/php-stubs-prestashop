<?php

namespace GuzzleHttp\Psr7;

/**
 * PSR-7 request implementation.
 */
class Request implements \Psr\Http\Message\RequestInterface
{
    use \GuzzleHttp\Psr7\MessageTrait;
    /**
     * @param string                               $method  HTTP method
     * @param string|UriInterface                  $uri     URI
     * @param array<string, string|string[]>       $headers Request headers
     * @param string|resource|StreamInterface|null $body    Request body
     * @param string                               $version Protocol version
     */
    public function __construct(string $method, $uri, array $headers = [], $body = null, string $version = '1.1')
    {
    }
    public function getRequestTarget() : string
    {
    }
    public function withRequestTarget($requestTarget) : \Psr\Http\Message\RequestInterface
    {
    }
    public function getMethod() : string
    {
    }
    public function withMethod($method) : \Psr\Http\Message\RequestInterface
    {
    }
    public function getUri() : \Psr\Http\Message\UriInterface
    {
    }
    public function withUri(\Psr\Http\Message\UriInterface $uri, $preserveHost = false) : \Psr\Http\Message\RequestInterface
    {
    }
}
