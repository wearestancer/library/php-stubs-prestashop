<?php

namespace GuzzleHttp\Psr7;

/**
 * Stream decorator that prevents a stream from being seeked.
 */
final class NoSeekStream implements \Psr\Http\Message\StreamInterface
{
    use \GuzzleHttp\Psr7\StreamDecoratorTrait;
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    public function isSeekable() : bool
    {
    }
}
