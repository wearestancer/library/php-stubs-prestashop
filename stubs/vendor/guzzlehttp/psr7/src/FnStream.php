<?php

namespace GuzzleHttp\Psr7;

/**
 * Compose stream implementations based on a hash of functions.
 *
 * Allows for easy testing and extension of a provided stream without needing
 * to create a concrete class for a simple extension point.
 */
#[\AllowDynamicProperties]
final class FnStream implements \Psr\Http\Message\StreamInterface
{
    /**
     * @param array<string, callable> $methods Hash of method name to a callable.
     */
    public function __construct(array $methods)
    {
    }
    /**
     * Lazily determine which methods are not implemented.
     *
     * @throws \BadMethodCallException
     */
    public function __get(string $name) : void
    {
    }
    /**
     * The close method is called on the underlying stream only if possible.
     */
    public function __destruct()
    {
    }
    /**
     * An unserialize would allow the __destruct to run when the unserialized value goes out of scope.
     *
     * @throws \LogicException
     */
    public function __wakeup() : void
    {
    }
    /**
     * Adds custom functionality to an underlying stream by intercepting
     * specific method calls.
     *
     * @param StreamInterface         $stream  Stream to decorate
     * @param array<string, callable> $methods Hash of method name to a closure
     *
     * @return FnStream
     */
    public static function decorate(\Psr\Http\Message\StreamInterface $stream, array $methods)
    {
    }
    public function __toString() : string
    {
    }
    public function close() : void
    {
    }
    public function detach()
    {
    }
    public function getSize() : ?int
    {
    }
    public function tell() : int
    {
    }
    public function eof() : bool
    {
    }
    public function isSeekable() : bool
    {
    }
    public function rewind() : void
    {
    }
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    public function isWritable() : bool
    {
    }
    public function write($string) : int
    {
    }
    public function isReadable() : bool
    {
    }
    public function read($length) : string
    {
    }
    public function getContents() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getMetadata($key = null)
    {
    }
}
