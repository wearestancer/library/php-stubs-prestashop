<?php

namespace GuzzleHttp\Psr7;

/**
 * Converts Guzzle streams into PHP stream resources.
 *
 * @see https://www.php.net/streamwrapper
 */
final class StreamWrapper
{
    /** @var resource */
    public $context;
    /**
     * Returns a resource representing the stream.
     *
     * @param StreamInterface $stream The stream to get a resource for
     *
     * @return resource
     *
     * @throws \InvalidArgumentException if stream is not readable or writable
     */
    public static function getResource(\Psr\Http\Message\StreamInterface $stream)
    {
    }
    /**
     * Creates a stream context that can be used to open a stream as a php stream resource.
     *
     * @return resource
     */
    public static function createStreamContext(\Psr\Http\Message\StreamInterface $stream)
    {
    }
    /**
     * Registers the stream wrapper if needed
     */
    public static function register() : void
    {
    }
    public function stream_open(string $path, string $mode, int $options, string &$opened_path = null) : bool
    {
    }
    public function stream_read(int $count) : string
    {
    }
    public function stream_write(string $data) : int
    {
    }
    public function stream_tell() : int
    {
    }
    public function stream_eof() : bool
    {
    }
    public function stream_seek(int $offset, int $whence) : bool
    {
    }
    /**
     * @return resource|false
     */
    public function stream_cast(int $cast_as)
    {
    }
    /**
     * @return array<int|string, int>
     */
    public function stream_stat() : array
    {
    }
    /**
     * @return array<int|string, int>
     */
    public function url_stat(string $path, int $flags) : array
    {
    }
}
