<?php

namespace GuzzleHttp\Psr7;

/**
 * Provides a read only stream that pumps data from a PHP callable.
 *
 * When invoking the provided callable, the PumpStream will pass the amount of
 * data requested to read to the callable. The callable can choose to ignore
 * this value and return fewer or more bytes than requested. Any extra data
 * returned by the provided callable is buffered internally until drained using
 * the read() function of the PumpStream. The provided callable MUST return
 * false when there is no more data to read.
 */
final class PumpStream implements \Psr\Http\Message\StreamInterface
{
    /**
     * @param callable(int): (string|null|false)  $source  Source of the stream data. The callable MAY
     *                                                     accept an integer argument used to control the
     *                                                     amount of data to return. The callable MUST
     *                                                     return a string when called, or false|null on error
     *                                                     or EOF.
     * @param array{size?: int, metadata?: array} $options Stream options:
     *                                                     - metadata: Hash of metadata to use with stream.
     *                                                     - size: Size of the stream, if known.
     */
    public function __construct(callable $source, array $options = [])
    {
    }
    public function __toString() : string
    {
    }
    public function close() : void
    {
    }
    public function detach()
    {
    }
    public function getSize() : ?int
    {
    }
    public function tell() : int
    {
    }
    public function eof() : bool
    {
    }
    public function isSeekable() : bool
    {
    }
    public function rewind() : void
    {
    }
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    public function isWritable() : bool
    {
    }
    public function write($string) : int
    {
    }
    public function isReadable() : bool
    {
    }
    public function read($length) : string
    {
    }
    public function getContents() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getMetadata($key = null)
    {
    }
}
