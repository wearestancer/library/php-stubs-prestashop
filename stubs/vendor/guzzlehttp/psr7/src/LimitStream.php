<?php

namespace GuzzleHttp\Psr7;

/**
 * Decorator used to return only a subset of a stream.
 */
final class LimitStream implements \Psr\Http\Message\StreamInterface
{
    use \GuzzleHttp\Psr7\StreamDecoratorTrait;
    /**
     * @param StreamInterface $stream Stream to wrap
     * @param int             $limit  Total number of bytes to allow to be read
     *                                from the stream. Pass -1 for no limit.
     * @param int             $offset Position to seek to before reading (only
     *                                works on seekable streams).
     */
    public function __construct(\Psr\Http\Message\StreamInterface $stream, int $limit = -1, int $offset = 0)
    {
    }
    public function eof() : bool
    {
    }
    /**
     * Returns the size of the limited subset of data
     */
    public function getSize() : ?int
    {
    }
    /**
     * Allow for a bounded seek on the read limited stream
     */
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    /**
     * Give a relative tell()
     */
    public function tell() : int
    {
    }
    /**
     * Set the offset to start limiting from
     *
     * @param int $offset Offset to seek to and begin byte limiting from
     *
     * @throws \RuntimeException if the stream cannot be seeked.
     */
    public function setOffset(int $offset) : void
    {
    }
    /**
     * Set the limit of bytes that the decorator allows to be read from the
     * stream.
     *
     * @param int $limit Number of bytes to allow to be read from the stream.
     *                   Use -1 for no limit.
     */
    public function setLimit(int $limit) : void
    {
    }
    public function read($length) : string
    {
    }
}
