<?php

namespace GuzzleHttp\Psr7;

/**
 * Implements all of the PSR-17 interfaces.
 *
 * Note: in consuming code it is recommended to require the implemented interfaces
 * and inject the instance of this class multiple times.
 */
final class HttpFactory implements \Psr\Http\Message\RequestFactoryInterface, \Psr\Http\Message\ResponseFactoryInterface, \Psr\Http\Message\ServerRequestFactoryInterface, \Psr\Http\Message\StreamFactoryInterface, \Psr\Http\Message\UploadedFileFactoryInterface, \Psr\Http\Message\UriFactoryInterface
{
    public function createUploadedFile(\Psr\Http\Message\StreamInterface $stream, int $size = null, int $error = \UPLOAD_ERR_OK, string $clientFilename = null, string $clientMediaType = null) : \Psr\Http\Message\UploadedFileInterface
    {
    }
    public function createStream(string $content = '') : \Psr\Http\Message\StreamInterface
    {
    }
    public function createStreamFromFile(string $file, string $mode = 'r') : \Psr\Http\Message\StreamInterface
    {
    }
    public function createStreamFromResource($resource) : \Psr\Http\Message\StreamInterface
    {
    }
    public function createServerRequest(string $method, $uri, array $serverParams = []) : \Psr\Http\Message\ServerRequestInterface
    {
    }
    public function createResponse(int $code = 200, string $reasonPhrase = '') : \Psr\Http\Message\ResponseInterface
    {
    }
    public function createRequest(string $method, $uri) : \Psr\Http\Message\RequestInterface
    {
    }
    public function createUri(string $uri = '') : \Psr\Http\Message\UriInterface
    {
    }
}
