<?php

namespace GuzzleHttp\Psr7;

/**
 * Stream decorator trait
 *
 * @property StreamInterface $stream
 */
trait StreamDecoratorTrait
{
    /**
     * @param StreamInterface $stream Stream to decorate
     */
    public function __construct(\Psr\Http\Message\StreamInterface $stream)
    {
    }
    /**
     * Magic method used to create a new stream if streams are not added in
     * the constructor of a decorator (e.g., LazyOpenStream).
     *
     * @return StreamInterface
     */
    public function __get(string $name)
    {
    }
    public function __toString() : string
    {
    }
    public function getContents() : string
    {
    }
    /**
     * Allow decorators to implement custom methods
     *
     * @return mixed
     */
    public function __call(string $method, array $args)
    {
    }
    public function close() : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getMetadata($key = null)
    {
    }
    public function detach()
    {
    }
    public function getSize() : ?int
    {
    }
    public function eof() : bool
    {
    }
    public function tell() : int
    {
    }
    public function isReadable() : bool
    {
    }
    public function isWritable() : bool
    {
    }
    public function isSeekable() : bool
    {
    }
    public function rewind() : void
    {
    }
    public function seek($offset, $whence = SEEK_SET) : void
    {
    }
    public function read($length) : string
    {
    }
    public function write($string) : int
    {
    }
    /**
     * Implement in subclasses to dynamically create streams when requested.
     *
     * @throws \BadMethodCallException
     */
    protected function createStream() : \Psr\Http\Message\StreamInterface
    {
    }
}
