<?php

namespace RandomLib\Mixer;

/**
 * mcrypt mixer using the Rijndael cipher with 128 bit block size
 *
 * @category   PHPCryptLib
 * @package    Random
 * @subpackage Mixer
 *
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 * @author     Chris Smith <chris@cs278.org>
 */
class McryptRijndael128 extends \RandomLib\AbstractMcryptMixer
{
    /**
     * {@inheritdoc}
     */
    public static function getStrength()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getCipher()
    {
    }
}
