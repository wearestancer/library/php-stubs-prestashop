<?php

namespace RandomLib;

/**
 * The Random Number Generator Class
 *
 * Use this factory to generate cryptographic quality random numbers (strings)
 *
 * @category   PHPPasswordLib
 * @package    Random
 *
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 * @author     Timo Hamina
 */
class Generator
{
    /**
     * @const Flag for uppercase letters
     */
    const CHAR_UPPER = 1;
    /**
     * @const Flag for lowercase letters
     */
    const CHAR_LOWER = 2;
    /**
     * @const Flag for alpha characters (combines UPPER + LOWER)
     */
    const CHAR_ALPHA = 3;
    // CHAR_UPPER | CHAR_LOWER
    /**
     * @const Flag for digits
     */
    const CHAR_DIGITS = 4;
    /**
     * @const Flag for alpha numeric characters
     */
    const CHAR_ALNUM = 7;
    // CHAR_ALPHA | CHAR_DIGITS
    /**
     * @const Flag for uppercase hexadecimal symbols
     */
    const CHAR_UPPER_HEX = 12;
    // 8 | CHAR_DIGITS
    /**
     * @const Flag for lowercase hexidecimal symbols
     */
    const CHAR_LOWER_HEX = 20;
    // 16 | CHAR_DIGITS
    /**
     * @const Flag for base64 symbols
     */
    const CHAR_BASE64 = 39;
    // 32 | CHAR_ALNUM
    /**
     * @const Flag for additional symbols accessible via the keyboard
     */
    const CHAR_SYMBOLS = 64;
    /**
     * @const Flag for brackets
     */
    const CHAR_BRACKETS = 128;
    /**
     * @const Flag for punctuation marks
     */
    const CHAR_PUNCT = 256;
    /**
     * @const Flag for upper/lower-case and digits but without "B8G6I1l|0OQDS5Z2"
     */
    const EASY_TO_READ = 512;
    /**
     * @var Mixer The mixing strategy to use for this generator instance
     */
    protected $mixer = null;
    /**
     * @var array An array of random number sources to use for this generator
     */
    protected $sources = array();
    /**
     * @var array The different characters, by Flag
     */
    protected $charArrays = array(self::CHAR_UPPER => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', self::CHAR_LOWER => 'abcdefghijklmnopqrstuvwxyz', self::CHAR_DIGITS => '0123456789', self::CHAR_UPPER_HEX => 'ABCDEF', self::CHAR_LOWER_HEX => 'abcdef', self::CHAR_BASE64 => '+/', self::CHAR_SYMBOLS => '!"#$%&\'()* +,-./:;<=>?@[\\]^_`{|}~', self::CHAR_BRACKETS => '()[]{}<>', self::CHAR_PUNCT => ',.;:');
    /**
     * @internal
     * @private
     * @const string Ambiguous characters for "Easy To Read" sets
     */
    const AMBIGUOUS_CHARS = 'B8G6I1l|0OQDS5Z2()[]{}:;,.';
    /**
     * Build a new instance of the generator
     *
     * @param array $sources An array of random data sources to use
     * @param Mixer $mixer   The mixing strategy to use for this generator
     */
    public function __construct(array $sources, \RandomLib\Mixer $mixer)
    {
    }
    /**
     * Add a random number source to the generator
     *
     * @param Source $source The random number source to add
     *
     * @return Generator $this The current generator instance
     */
    public function addSource(\RandomLib\Source $source)
    {
    }
    /**
     * Generate a random number (string) of the requested size
     *
     * @param int $size The size of the requested random number
     *
     * @return string The generated random number (string)
     */
    public function generate($size)
    {
    }
    /**
     * Generate a random integer with the given range
     *
     * @param int $min The lower bound of the range to generate
     * @param int $max The upper bound of the range to generate
     *
     * @return int The generated random number within the range
     */
    public function generateInt($min = 0, $max = PHP_INT_MAX)
    {
    }
    /**
     * Generate a random string of specified length.
     *
     * This uses the supplied character list for generating the new result
     * string.
     *
     * @param int   $length     The length of the generated string
     * @param mixed $characters String: An optional list of characters to use
     *                          Integer: Character flags
     *
     * @return string The generated random string
     */
    public function generateString($length, $characters = '')
    {
    }
    /**
     * Get the Mixer used for this instance
     *
     * @return Mixer the current mixer
     */
    public function getMixer()
    {
    }
    /**
     * Get the Sources used for this instance
     *
     * @return Source[] the current mixer
     */
    public function getSources()
    {
    }
    /**
     * Count the minimum number of bits to represent the provided number
     *
     * This is basically floor(log($number, 2))
     * But avoids float precision issues
     *
     * @param int $number The number to count
     *
     * @return int The number of bits
     */
    protected function countBits($number)
    {
    }
    /**
     * Expand a character set bitwise spec into a string character set
     *
     * This will also replace EASY_TO_READ characters if the flag is set
     *
     * @param int $spec The spec to expand (bitwise combination of flags)
     *
     * @return string The expanded string
     */
    protected function expandCharacterSets($spec)
    {
    }
}
