<?php

namespace RandomLib;

/**
 * The mcrypt abstract mixer class
 *
 * @category   PHPCryptLib
 * @package    Random
 * @subpackage Mixer
 *
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 * @author     Chris Smith <chris@cs278.org>
 */
abstract class AbstractMcryptMixer extends \RandomLib\AbstractMixer
{
    /**
     * {@inheritdoc}
     */
    public static function test()
    {
    }
    /**
     * Construct mcrypt mixer
     */
    public function __construct()
    {
    }
    /**
     * Performs cleanup
     */
    public function __destruct()
    {
    }
    /**
     * Fetch the cipher for mcrypt.
     *
     * @return string
     */
    protected abstract function getCipher();
    /**
     * {@inheritdoc}
     */
    protected function getPartSize()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function mixParts1($part1, $part2)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function mixParts2($part1, $part2)
    {
    }
}
