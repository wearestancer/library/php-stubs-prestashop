<?php

namespace RandomLib;

/**
 * An abstract mixer to implement a common mixing strategy
 *
 * @category PHPSecurityLib
 * @package  Random
 */
abstract class AbstractSource implements \RandomLib\Source
{
    /**
     * Return an instance of Strength indicating the strength of the source
     *
     * @return \SecurityLib\Strength An instance of one of the strength classes
     */
    public static function getStrength()
    {
    }
    /**
     * If the source is currently available.
     * Reasons might be because the library is not installed
     *
     * @return bool
     */
    public static function isSupported()
    {
    }
    /**
     * Returns a string of zeroes, useful when no entropy is available.
     *
     * @param int $size The size of the requested random string
     *
     * @return string A string of the requested size
     */
    protected static function emptyValue($size)
    {
    }
}
