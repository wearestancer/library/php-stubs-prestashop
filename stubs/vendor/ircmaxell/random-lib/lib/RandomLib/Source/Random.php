<?php

namespace RandomLib\Source;

/**
 * The Random Random Number Source
 *
 * This uses the *nix /dev/random device to generate high strength numbers
 *
 * @category   PHPCryptLib
 * @package    Random
 * @subpackage Source
 *
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 * @codeCoverageIgnore
 */
class Random extends \RandomLib\Source\URandom
{
    /**
     * @var string The file to read from
     */
    protected static $file = '/dev/random';
    /**
     * Return an instance of Strength indicating the strength of the source
     *
     * @return \SecurityLib\Strength An instance of one of the strength classes
     */
    public static function getStrength()
    {
    }
}
