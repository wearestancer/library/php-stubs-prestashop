<?php

namespace RandomLib\Source;

/**
 * The libsodium Random Number Source
 *
 * This uses the libsodium secure generator to generate high strength numbers
 *
 * @category   PHPCryptLib
 * @package    Random
 * @subpackage Source
 *
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 * @author     Ben Ramsey <ben@benramsey.com>
 */
class Sodium extends \RandomLib\AbstractSource
{
    /**
     * Constructs a libsodium Random Number Source
     *
     * @param bool $useLibsodium May be set to `false` to disable libsodium for
     *                           testing purposes
     */
    public function __construct($useLibsodium = true)
    {
    }
    /**
     * If the source is currently available.
     * Reasons might be because the library is not installed
     *
     * @return bool
     */
    public static function isSupported()
    {
    }
    /**
     * Return an instance of Strength indicating the strength of the source
     *
     * @return Strength An instance of one of the strength classes
     */
    public static function getStrength()
    {
    }
    /**
     * Generate a random string of the specified size
     *
     * @param int $size The size of the requested random string
     *
     * @return string A string of the requested size
     */
    public function generate($size)
    {
    }
}
