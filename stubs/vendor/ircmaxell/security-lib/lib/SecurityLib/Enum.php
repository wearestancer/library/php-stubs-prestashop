<?php

namespace SecurityLib;

/**
 * The Enum base class for Enum functionality
 *
 * This is based off of the SplEnum class implementation (which is only available
 * as a PECL extension in 5.3)
 *
 * @see        http://www.php.net/manual/en/class.splenum.php
 * @category   PHPPasswordLib
 * @package    Core
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 */
abstract class Enum
{
    /**
     * A default value of null is provided.  Override this to set your own default
     */
    const __DEFAULT = null;
    /**
     * @var string The name of the constant this instance is using
     */
    protected $name = '';
    /**
     * @var scalar The value of the constant this instance is using.
     */
    protected $value = '';
    /**
     * Creates a new value of the Enum type
     *
     * @param mixed   $value  The value this instance represents
     * @param boolean $strict Not Implemented at this time
     *
     * @return void
     * @throws UnexpectedValueException If the value is not a constant
     */
    public function __construct($value = null, $strict = false)
    {
    }
    /**
     * Cast the current object to a string and return its value
     *
     * @return mixed the current value of the instance
     */
    public function __toString()
    {
    }
    /**
     * Compare two enums using numeric comparison
     *
     * @param Enum $arg The enum to compare this instance to
     *
     * @return int 0 if same, 1 if the argument is greater, -1 else
     */
    public function compare(\SecurityLib\Enum $arg)
    {
    }
    /**
     * Returns all constants (including values) as an associative array
     *
     * @param boolean $include_default Include the __default magic value?
     *
     * @return array All of the constants found against this instance
     */
    public function getConstList($include_default = false)
    {
    }
}
