<?php

namespace SecurityLib\BigMath;

/**
 * A class for arbitrary precision math functions implemented in PHP
 *
 * @category   PHPPasswordLib
 * @package    Core
 * @subpackage BigMath
 */
class PHPMath extends \SecurityLib\BigMath
{
    /**
     * Add two numbers together
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return A base-10 string of the sum of the two arguments
     */
    public function add($left, $right)
    {
    }
    /**
     * Subtract two numbers
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return A base-10 string of the difference of the two arguments
     */
    public function subtract($left, $right)
    {
    }
    /**
     * Add two binary strings together
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return string The binary result
     */
    protected function addBinary($left, $right)
    {
    }
    /**
     * Subtract two binary strings using 256's compliment
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return string The binary result
     */
    protected function subtractBinary($left, $right)
    {
    }
    /**
     * Take the 256 base compliment
     *
     * @param string $string The binary string to compliment
     *
     * @return string The complimented string
     */
    protected function compliment($string)
    {
    }
    /**
     * Transform a string number into a binary string using base autodetection
     *
     * @param string $string The string to transform
     *
     * @return string The binary transformed number
     */
    protected function normalize($string)
    {
    }
}
