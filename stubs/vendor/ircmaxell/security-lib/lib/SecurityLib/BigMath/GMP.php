<?php

namespace SecurityLib\BigMath;

/**
 * A class for arbitrary precision math functions implemented using GMP
 *
 * @category   PHPPasswordLib
 * @package    Core
 * @subpackage BigMath
 */
class GMP extends \SecurityLib\BigMath
{
    /**
     * Add two numbers together
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return A base-10 string of the sum of the two arguments
     */
    public function add($left, $right)
    {
    }
    /**
     * Subtract two numbers
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return A base-10 string of the difference of the two arguments
     */
    public function subtract($left, $right)
    {
    }
}
