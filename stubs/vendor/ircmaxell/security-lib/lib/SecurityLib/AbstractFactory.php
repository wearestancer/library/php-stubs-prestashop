<?php

namespace SecurityLib;

/**
 * The base abstract factory used by all PasswordLib factories
 *
 * @category PHPPasswordLib
 * @package  Core
 * @author   Anthony Ferrara <ircmaxell@ircmaxell.com>
 */
abstract class AbstractFactory
{
    /**
     * Register a type with the factory by name
     *
     * This is an internal method to check if a provided class name implements
     * an interface, and if it does to append that class to an internal array
     * by name.
     *
     * @param string  $type        The name of the variable to store the class
     * @param string  $implements  The interface to validate against
     * @param string  $name        The name of this particular class
     * @param string  $class       The fully qualified class name
     * @param boolean $instantiate Should the class be stored instantiated
     *
     * @return void
     * @throws InvalidArgumentException If class does not implement interface
     */
    protected function registerType($type, $implements, $name, $class, $instantiate = false)
    {
    }
    /**
     * Load a set of classes from a directory into the factory
     *
     * @param string $directory The directory to search for classes in
     * @param string $namespace The namespace prefix for any found classes
     * @param string $callback  The callback with which to register the class
     *
     * @return void
     */
    protected function loadFiles($directory, $namespace, $callback)
    {
    }
}
