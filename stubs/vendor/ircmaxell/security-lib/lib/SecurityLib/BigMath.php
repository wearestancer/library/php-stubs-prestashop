<?php

namespace SecurityLib;

/**
 * A class for arbitrary precision math functions
 *
 * @category   PHPPasswordLib
 * @package    Core
 * @author     Anthony Ferrara <ircmaxell@ircmaxell.com>
 */
abstract class BigMath
{
    /**
     * Get an instance of the big math class
     *
     * This is NOT a singleton.  It simply loads the proper strategy
     * given the current server configuration
     *
     * @return \PasswordLib\Core\BigMath A big math instance
     */
    public static function createFromServerConfiguration()
    {
    }
    /**
     * Add two numbers together
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return A base-10 string of the sum of the two arguments
     */
    public abstract function add($left, $right);
    /**
     * Subtract two numbers
     *
     * @param string $left  The left argument
     * @param string $right The right argument
     *
     * @return A base-10 string of the difference of the two arguments
     */
    public abstract function subtract($left, $right);
}
