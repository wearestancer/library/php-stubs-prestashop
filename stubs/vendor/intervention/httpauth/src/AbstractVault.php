<?php

namespace Intervention\HttpAuth;

abstract class AbstractVault
{
    /**
     * Environment
     *
     * @var Environment
     */
    protected $environment;
    /**
     * Name of realm for vault
     *
     * @var string
     */
    protected $realm;
    /**
     * Username for vault
     * @var string
     */
    protected $username;
    /**
     * Password for vault
     *
     * @var string
     */
    protected $password;
    /**
     * Build directive for current vault
     *
     * @return Directive
     */
    public abstract function getDirective() : \Intervention\HttpAuth\Directive;
    /**
     * Determine if vault is accessible by given key
     *
     * @param  Key    $key
     * @return bool
     */
    public abstract function unlocksWithKey(\Intervention\HttpAuth\Key $key) : bool;
    /**
     * Create new instance
     *
     * @param mixed $realm
     * @param mixed $username
     * @param mixed $password
     */
    public function __construct($realm, $username, $password)
    {
    }
    /**
     * Return key from current token
     *
     * @return Key
     */
    public function getKey() : \Intervention\HttpAuth\Key
    {
    }
    /**
     * Denies access for non-authenticated users
     *
     * @return void
     */
    public function secure() : void
    {
    }
    /**
     * Set name of realm
     *
     * @param string $realm
     * @return AbstractVault
     */
    public function setRealm($realm) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Alias for setRealm()
     *
     * @param string $realm
     * @return AbstractVault
     */
    public function realm($realm) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Return current realm name
     *
     * @return string
     */
    public function getRealm()
    {
    }
    /**
     * Set username for current vault
     *
     * @param string $username
     */
    public function setUsername($username) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Alias for setUsername()
     *
     * @param string $username
     */
    public function username($username) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Return current username
     *
     * @return string
     */
    public function getUsername()
    {
    }
    /**
     * Set password for current vault
     *
     * @param string $password
     * @return AbstractVault
     */
    public function setPassword($password) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Alias for setPassword()
     *
     * @param  string $password
     * @return AbstractVault
     */
    public function password($password) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Return current password
     *
     * @return string
     */
    public function getPassword()
    {
    }
    /**
     * Set username and password at once
     *
     * @param  string $username
     * @param  string $password
     * @return AbstractVault
     */
    public function credentials($username, $password) : \Intervention\HttpAuth\AbstractVault
    {
    }
    /**
     * Sends HTTP 401 Header
     *
     * @return void
     */
    protected function denyAccess() : void
    {
    }
}
