<?php

namespace Intervention\HttpAuth;

class Environment
{
    /**
     * Available auth tokens
     *
     * @var array
     */
    protected $tokenClassnames = [\Intervention\HttpAuth\Token\PhpAuthUser::class, \Intervention\HttpAuth\Token\HttpAuthorization::class, \Intervention\HttpAuth\Token\RedirectHttpAuthorization::class, \Intervention\HttpAuth\Token\PhpAuthDigest::class, \Intervention\HttpAuth\Token\HttpAuthorization::class];
    /**
     * Get first active auth token from all available tokens
     *
     * @return TokenInterface
     */
    public function getToken() : \Intervention\HttpAuth\TokenInterface
    {
    }
}
