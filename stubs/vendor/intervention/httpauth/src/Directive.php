<?php

namespace Intervention\HttpAuth;

class Directive
{
    /**
     * Type of directive (basic|digest)
     *
     * @var string
     */
    protected $type;
    /**
     * Array of parameters
     *
     * @var array
     */
    protected $parameters = [];
    /**
     * Create new instance
     *
     * @param string $type
     * @param array  $parameters
     */
    public function __construct($type, $parameters = [])
    {
    }
    /**
     * Format current instance
     *
     * @return string
     */
    public function format() : string
    {
    }
    /**
     * Return current type
     *
     * @return string
     */
    public function getType()
    {
    }
    /**
     * Return value of given key from all parameters, if existing
     *
     * @param  mixed $key
     * @return mixed
     */
    public function getParameter($key)
    {
    }
    /**
     * Cast object to string
     *
     * @return string
     */
    public function __toString() : string
    {
    }
}
