<?php

namespace Intervention\HttpAuth;

class HttpAuth
{
    /**
     * Authentication type
     *
     * @var string
     */
    protected static $type = 'basic';
    /**
     * Name of authentication realm
     *
     * @var string
     */
    protected static $realm = 'Secured Resource';
    /**
     * Username
     *
     * @var string
     */
    protected static $username = 'admin';
    /**
     * Password
     *
     * @var string
     */
    protected static $password = 'secret';
    /**
     * Static factory method
     *
     * @param  array  $config
     * @return HttpAuth
     */
    public static function make(array $config = []) : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Create vault by current parameters and secure it
     *
     * @return void
     */
    public function secure() : void
    {
    }
    /**
     * Create HTTP basic auth instance
     *
     * @return HttpAuth
     */
    public function basic() : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Create HTTP digest auth instance
     *
     * @return HttpAuth
     */
    public function digest() : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Set type of configured vault
     *
     * @param  string $value
     * @return HttpAuth
     */
    public function type($value) : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Set realm name of configured vault
     *
     * @param  string $value
     * @return HttpAuth
     */
    public function realm($value) : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Set username of configured vault
     *
     * @param  string $value
     * @return HttpAuth
     */
    public function username($value) : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Set password of configured vault
     *
     * @param  string $value
     * @return HttpAuth
     */
    public function password($value) : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Set credentials for configured vault
     *
     * @param  string $username
     * @param  string $password
     * @return HttpAuth
     */
    public function credentials($username, $password) : \Intervention\HttpAuth\HttpAuth
    {
    }
    /**
     * Get type of current instance
     *
     * @return mixed
     */
    public function getType()
    {
    }
    /**
     * Get realm of current instance
     *
     * @return mixed
     */
    public function getRealm()
    {
    }
    /**
     * Get username of current instance
     *
     * @return mixed
     */
    public function getUsername()
    {
    }
    /**
     * Get password of current instance
     *
     * @return mixed
     */
    public function getPassword()
    {
    }
    /**
     * Return ready configured vault
     *
     * @return AbstractVault
     */
    protected function getVault() : \Intervention\HttpAuth\AbstractVault
    {
    }
}
