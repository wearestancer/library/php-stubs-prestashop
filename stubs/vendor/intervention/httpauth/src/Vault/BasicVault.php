<?php

namespace Intervention\HttpAuth\Vault;

class BasicVault extends \Intervention\HttpAuth\AbstractVault
{
    /**
     * Determine if given key is able to unlock (access) vault.
     *
     * @param  Key    $key
     * @return bool
     */
    public function unlocksWithKey(\Intervention\HttpAuth\Key $key) : bool
    {
    }
    /**
     * Return auth directive
     *
     * @return Directive
     */
    public function getDirective() : \Intervention\HttpAuth\Directive
    {
    }
}
