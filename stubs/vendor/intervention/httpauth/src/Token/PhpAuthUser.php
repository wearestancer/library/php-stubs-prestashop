<?php

namespace Intervention\HttpAuth\Token;

class PhpAuthUser extends \Intervention\HttpAuth\Token\NullToken
{
    /**
     * Parsed authentification username
     *
     * @var string
     */
    protected $username;
    /**
     * Parsed authentification password
     *
     * @var string
     */
    protected $password;
    /**
     * Transform current instance to key object
     *
     * @return Key
     */
    public function toKey() : \Intervention\HttpAuth\Key
    {
    }
    /**
     * Parse environment variables and store value in object
     *
     * @return bool "true" if value was found or "false"
     */
    protected function parse() : bool
    {
    }
}
