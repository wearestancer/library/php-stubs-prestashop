<?php

namespace Intervention\HttpAuth\Token;

class HttpAuthentification extends \Intervention\HttpAuth\Token\NullToken
{
    /**
     * Parsed authentification value
     *
     * @var string
     */
    protected $value;
    /**
     * Transform current instance to key object
     *
     * @return Key
     */
    public function toKey() : \Intervention\HttpAuth\Key
    {
    }
    /**
     * Parse environment variables and store value in object
     *
     * @return bool "true" if value was found or "false"
     */
    protected function parse() : bool
    {
    }
}
