<?php

namespace Intervention\HttpAuth\Token;

class NullToken implements \Intervention\HttpAuth\TokenInterface
{
    /**
     * Create new instance
     */
    public function __construct()
    {
    }
    /**
     * Transform current instance to key object
     *
     * @return Key
     */
    public function toKey() : \Intervention\HttpAuth\Key
    {
    }
    /**
     * Parse environment variables and store value in object
     *
     * @return bool "true" if value was found or "false"
     */
    protected function parse() : bool
    {
    }
    /**
     * Return the value of given key in given array data.
     * Returns null if key doesn't exists
     *
     * @param  array  $data
     * @param  mixed  $key
     * @return mixed
     */
    protected function getArrayValue($data, $key)
    {
    }
}
