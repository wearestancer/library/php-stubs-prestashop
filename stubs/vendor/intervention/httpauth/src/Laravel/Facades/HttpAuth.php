<?php

namespace Intervention\HttpAuth\Laravel\Facades;

class HttpAuth extends \Illuminate\Support\Facades\Facade
{
    /**
     * Return facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
    }
}
