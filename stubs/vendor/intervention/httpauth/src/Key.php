<?php

namespace Intervention\HttpAuth;

class Key
{
    public function getRealm()
    {
    }
    /**
     * Return current username
     *
     * @return string
     */
    public function getUsername()
    {
    }
    /**
     * Return current password
     *
     * @return string
     */
    public function getPassword()
    {
    }
    /**
     * Return current qop
     *
     * @return string
     */
    public function getQop()
    {
    }
    /**
     * Return current nonce
     *
     * @return string
     */
    public function getNonce()
    {
    }
    /**
     * Return current opaque
     *
     * @return string
     */
    public function getOpaque()
    {
    }
    /**
     * Return current uri
     *
     * @return string
     */
    public function getUri()
    {
    }
    /**
     * Return current nc
     *
     * @return string
     */
    public function getNc()
    {
    }
    /**
     * Return current cnonce
     *
     * @return string
     */
    public function getCnonce()
    {
    }
    /**
     * Return current response
     *
     * @return string
     */
    public function getResponse()
    {
    }
    /**
     * Set property to given value on current instance
     *
     * @param string $name
     * @param mixed  $value
     * @return Key
     */
    public function setProperty($name, $value) : \Intervention\HttpAuth\Key
    {
    }
}
