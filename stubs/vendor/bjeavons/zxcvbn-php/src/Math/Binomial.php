<?php

namespace ZxcvbnPhp\Math;

class Binomial
{
    /**
     * Calculate binomial coefficient (n choose k).
     *
     * @param int $n
     * @param int $k
     * @return float
     */
    public static function binom(int $n, int $k) : float
    {
    }
    public static function getProvider() : \ZxcvbnPhp\Math\BinomialProvider
    {
    }
    /**
     * @return string[]
     */
    public static function getUsableProviderClasses() : array
    {
    }
}
