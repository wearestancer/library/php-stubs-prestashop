<?php

namespace ZxcvbnPhp\Math\Impl;

abstract class AbstractBinomialProvider implements \ZxcvbnPhp\Math\BinomialProvider
{
    public function binom(int $n, int $k) : float
    {
    }
    protected abstract function calculate(int $n, int $k) : float;
}
