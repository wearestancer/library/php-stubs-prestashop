<?php

namespace ZxcvbnPhp\Math\Impl;

class BinomialProviderPhp73Gmp extends \ZxcvbnPhp\Math\Impl\AbstractBinomialProvider
{
    /**
     * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
     * @noinspection PhpComposerExtensionStubsInspection
     */
    protected function calculate(int $n, int $k) : float
    {
    }
}
