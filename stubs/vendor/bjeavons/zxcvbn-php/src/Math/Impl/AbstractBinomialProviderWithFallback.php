<?php

namespace ZxcvbnPhp\Math\Impl;

abstract class AbstractBinomialProviderWithFallback extends \ZxcvbnPhp\Math\Impl\AbstractBinomialProvider
{
    protected function calculate(int $n, int $k) : float
    {
    }
    protected abstract function tryCalculate(int $n, int $k) : ?float;
    protected abstract function initFallbackProvider() : \ZxcvbnPhp\Math\Impl\AbstractBinomialProvider;
    protected function getFallbackProvider() : \ZxcvbnPhp\Math\Impl\AbstractBinomialProvider
    {
    }
}
