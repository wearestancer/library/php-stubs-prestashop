<?php

namespace ZxcvbnPhp\Math\Impl;

class BinomialProviderInt64 extends \ZxcvbnPhp\Math\Impl\AbstractBinomialProviderWithFallback
{
    protected function initFallbackProvider() : \ZxcvbnPhp\Math\Impl\AbstractBinomialProvider
    {
    }
    protected function tryCalculate(int $n, int $k) : ?float
    {
    }
}
