<?php

namespace ZxcvbnPhp;

/**
 * Feedback - gives some user guidance based on the strength
 * of a password
 *
 * @see zxcvbn/src/time_estimates.coffee
 */
class TimeEstimator
{
    /**
     * @param int|float $guesses
     * @return array
     */
    public function estimateAttackTimes(float $guesses) : array
    {
    }
    protected function guessesToScore(float $guesses) : int
    {
    }
    protected function displayTime(float $seconds) : string
    {
    }
}
