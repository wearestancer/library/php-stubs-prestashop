<?php

namespace ZxcvbnPhp;

class Matcher
{
    /**
     * Get matches for a password.
     *
     * @param string $password  Password string to match
     * @param array $userInputs Array of values related to the user (optional)
     * @code array('Alice Smith')
     * @endcode
     *
     * @return MatchInterface[] Array of Match objects.
     *
     * @see  zxcvbn/src/matching.coffee::omnimatch
     */
    public function getMatches(string $password, array $userInputs = []) : array
    {
    }
    public function addMatcher(string $className) : self
    {
    }
    /**
     * A stable implementation of usort().
     *
     * Whether or not the sort() function in JavaScript is stable or not is implementation-defined.
     * This means it's impossible for us to match all browsers exactly, but since most browsers implement sort() using
     * a stable sorting algorithm, we'll get the highest rate of accuracy by using a stable sort in our code as well.
     *
     * This function taken from https://github.com/vanderlee/PHP-stable-sort-functions
     * Copyright © 2015-2018 Martijn van der Lee (http://martijn.vanderlee.com). MIT License applies.
     *
     * @param array $array
     * @param callable $value_compare_func
     * @return bool
     */
    public static function usortStable(array &$array, callable $value_compare_func) : bool
    {
    }
    public static function compareMatches(\ZxcvbnPhp\Matchers\BaseMatch $a, \ZxcvbnPhp\Matchers\BaseMatch $b) : int
    {
    }
    /**
     * Load available Match objects to match against a password.
     *
     * @return array Array of classes implementing MatchInterface
     */
    protected function getMatchers() : array
    {
    }
}
