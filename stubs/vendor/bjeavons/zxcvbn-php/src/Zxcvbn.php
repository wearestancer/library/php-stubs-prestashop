<?php

namespace ZxcvbnPhp;

/**
 * The main entry point.
 *
 * @see  zxcvbn/src/main.coffee
 */
class Zxcvbn
{
    /**
     * @var
     */
    protected $matcher;
    /**
     * @var
     */
    protected $scorer;
    /**
     * @var
     */
    protected $timeEstimator;
    /**
     * @var
     */
    protected $feedback;
    public function __construct()
    {
    }
    public function addMatcher(string $className) : self
    {
    }
    /**
     * Calculate password strength via non-overlapping minimum entropy patterns.
     *
     * @param string $password   Password to measure
     * @param array  $userInputs Optional user inputs
     *
     * @return array Strength result array with keys:
     *               password
     *               entropy
     *               match_sequence
     *               score
     */
    public function passwordStrength(string $password, array $userInputs = []) : array
    {
    }
}
