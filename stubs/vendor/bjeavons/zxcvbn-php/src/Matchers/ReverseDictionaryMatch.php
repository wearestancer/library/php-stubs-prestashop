<?php

namespace ZxcvbnPhp\Matchers;

class ReverseDictionaryMatch extends \ZxcvbnPhp\Matchers\DictionaryMatch
{
    /** @var bool Whether or not the matched word was reversed in the token. */
    public $reversed = true;
    /**
     * Match occurences of reversed dictionary words in password.
     *
     * @param $password
     * @param array $userInputs
     * @param array $rankedDictionaries
     * @return ReverseDictionaryMatch[]
     */
    public static function match(string $password, array $userInputs = [], array $rankedDictionaries = []) : array
    {
    }
    protected function getRawGuesses() : float
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    public static function mbStrRev(string $string, string $encoding = null) : string
    {
    }
}
