<?php

namespace ZxcvbnPhp\Matchers;

/**
 * Class Bruteforce
 * @package ZxcvbnPhp\Matchers
 *
 * Intentionally not named with Match suffix to prevent autoloading from Matcher.
 */
class Bruteforce extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public const BRUTEFORCE_CARDINALITY = 10;
    public $pattern = 'bruteforce';
    /**
     * @param string $password
     * @param array $userInputs
     * @return Bruteforce[]
     */
    public static function match(string $password, array $userInputs = []) : array
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    public function getRawGuesses() : float
    {
    }
}
