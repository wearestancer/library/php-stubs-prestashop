<?php

namespace ZxcvbnPhp\Matchers;

abstract class BaseMatch implements \ZxcvbnPhp\Matchers\MatchInterface
{
    /**
     * @var
     */
    public $password;
    /**
     * @var
     */
    public $begin;
    /**
     * @var
     */
    public $end;
    /**
     * @var
     */
    public $token;
    /**
     * @var
     */
    public $pattern;
    public function __construct(string $password, int $begin, int $end, string $token)
    {
    }
    /**
     * Get feedback to a user based on the match.
     *
     * @param  bool $isSoleMatch
     *   Whether this is the only match in the password
     * @return array
     *   Associative array with warning (string) and suggestions (array of strings)
     */
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public abstract function getFeedback(bool $isSoleMatch) : array;
    /**
     * Find all occurrences of regular expression in a string.
     *
     * @param string $string
     *   String to search.
     * @param string $regex
     *   Regular expression with captures.
     * @param int $offset
     * @return array
     *   Array of capture groups. Captures in a group have named indexes: 'begin', 'end', 'token'.
     *     e.g. fishfish /(fish)/
     *     array(
     *       array(
     *         array('begin' => 0, 'end' => 3, 'token' => 'fish'),
     *         array('begin' => 0, 'end' => 3, 'token' => 'fish')
     *       ),
     *       array(
     *         array('begin' => 4, 'end' => 7, 'token' => 'fish'),
     *         array('begin' => 4, 'end' => 7, 'token' => 'fish')
     *       )
     *     )
     */
    public static function findAll(string $string, string $regex, int $offset = 0) : array
    {
    }
    /**
     * Calculate binomial coefficient (n choose k).
     *
     * @param int $n
     * @param int $k
     * @return float
     * @deprecated Use {@see Binomial::binom()} instead
     */
    public static function binom(int $n, int $k) : float
    {
    }
    protected abstract function getRawGuesses() : float;
    public function getGuesses() : float
    {
    }
    protected function getMinimumGuesses() : float
    {
    }
    public function getGuessesLog10() : float
    {
    }
}
