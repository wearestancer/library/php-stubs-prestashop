<?php

namespace ZxcvbnPhp\Matchers;

class SequenceMatch extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public const MAX_DELTA = 5;
    public $pattern = 'sequence';
    /** @var string The name of the detected sequence. */
    public $sequenceName;
    /** @var int The number of characters in the complete sequence space. */
    public $sequenceSpace;
    /** @var bool True if the sequence is ascending, and false if it is descending. */
    public $ascending;
    /**
     * Match sequences of three or more characters.
     *
     * @param string $password
     * @param array $userInputs
     * @return SequenceMatch[]
     */
    public static function match(string $password, array $userInputs = []) : array
    {
    }
    public static function findSequenceMatch(string $password, int $begin, int $end, int $delta, array &$matches)
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    /**
     * @param string $password
     * @param int $begin
     * @param int $end
     * @param string $token
     * @param array $params An array with keys: [sequenceName, sequenceSpace, ascending].
     */
    public function __construct(string $password, int $begin, int $end, string $token, array $params = [])
    {
    }
    protected function getRawGuesses() : float
    {
    }
}
