<?php

namespace ZxcvbnPhp\Matchers;

class YearMatch extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public const NUM_YEARS = 119;
    public $pattern = 'regex';
    public $regexName = 'recent_year';
    /**
     * Match occurrences of years in a password
     *
     * @param string $password
     * @param array $userInputs
     * @return YearMatch[]
     */
    public static function match(string $password, array $userInputs = []) : array
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    protected function getRawGuesses() : float
    {
    }
}
