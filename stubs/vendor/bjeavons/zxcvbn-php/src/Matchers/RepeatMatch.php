<?php

namespace ZxcvbnPhp\Matchers;

class RepeatMatch extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public const GREEDY_MATCH = '/(.+)\\1+/u';
    public const LAZY_MATCH = '/(.+?)\\1+/u';
    public const ANCHORED_LAZY_MATCH = '/^(.+?)\\1+$/u';
    public $pattern = 'repeat';
    /** @var MatchInterface[] An array of matches for the repeated section itself. */
    public $baseMatches = [];
    /** @var int The number of guesses required for the repeated section itself. */
    public $baseGuesses;
    /** @var int The number of times the repeated section is repeated. */
    public $repeatCount;
    /** @var string The string that was repeated in the token. */
    public $repeatedChar;
    /**
     * Match 3 or more repeated characters.
     *
     * @param string $password
     * @param array $userInputs
     * @return RepeatMatch[]
     */
    public static function match(string $password, array $userInputs = []) : array
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    /**
     * @param string $password
     * @param int $begin
     * @param int $end
     * @param string $token
     * @param array $params An array with keys: [repeated_char, base_guesses, base_matches, repeat_count].
     */
    public function __construct(string $password, int $begin, int $end, string $token, array $params = [])
    {
    }
    protected function getRawGuesses() : float
    {
    }
}
