<?php

namespace ZxcvbnPhp\Matchers;

class DateMatch extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public const NUM_YEARS = 119;
    // Years match against 1900 - 2019
    public const NUM_MONTHS = 12;
    public const NUM_DAYS = 31;
    public const MIN_YEAR = 1000;
    public const MAX_YEAR = 2050;
    public const MIN_YEAR_SPACE = 20;
    public $pattern = 'date';
    protected const DATE_NO_SEPARATOR = '/^\\d{4,8}$/u';
    /**
     * (\d{1,4})        # day, month, year
     * ([\s\/\\\\_.-])  # separator
     * (\d{1,2})        # day, month
     * \2               # same separator
     * (\d{1,4})        # day, month, year
     */
    protected const DATE_WITH_SEPARATOR = '/^(\\d{1,4})([\\s\\/\\\\_.-])(\\d{1,2})\\2(\\d{1,4})$/u';
    /** @var int The day portion of the date in the token. */
    public $day;
    /** @var int The month portion of the date in the token. */
    public $month;
    /** @var int The year portion of the date in the token. */
    public $year;
    /** @var string The separator used for the date in the token. */
    public $separator;
    /**
     * Match occurences of dates in a password
     *
     * @param string $password
     * @param array $userInputs
     * @return DateMatch[]
     */
    public static function match(string $password, array $userInputs = []) : array
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    /**
     * @param string $password
     * @param int $begin
     * @param int $end
     * @param string $token
     * @param array $params An array with keys: [day, month, year, separator].
     */
    public function __construct(string $password, int $begin, int $end, string $token, array $params)
    {
    }
    /**
     * Find dates with separators in a password.
     *
     * @param string $password
     *
     * @return array
     */
    protected static function datesWithSeparators(string $password) : array
    {
    }
    /**
     * Find dates without separators in a password.
     *
     * @param string $password
     *
     * @return array
     */
    protected static function datesWithoutSeparators(string $password) : array
    {
    }
    /**
     * @param array $candidate
     * @return int Returns the number of years between the detected year and the current year for a candidate.
     */
    protected static function getDistanceForMatchCandidate(array $candidate) : int
    {
    }
    public static function getReferenceYear() : int
    {
    }
    /**
     * @param int[] $ints Three numbers in an array representing day, month and year (not necessarily in that order).
     * @return array|bool Returns an associative array containing 'day', 'month' and 'year' keys, or false if the
     *                    provided date array is invalid.
     */
    protected static function checkDate(array $ints)
    {
    }
    /**
     * @param int[] $ints Two numbers in an array representing day and month (not necessarily in that order).
     * @return array|bool Returns an associative array containing 'day' and 'month' keys, or false if any combination
     *                    of the two numbers does not match a day and month.
     */
    protected static function mapIntsToDayMonth(array $ints)
    {
    }
    /**
     * @param int $year A two digit number representing a year.
     * @return int Returns the most likely four digit year for the provided number.
     */
    protected static function twoToFourDigitYear(int $year) : int
    {
    }
    /**
     * Removes date matches that are strict substrings of others.
     *
     * This is helpful because the match function will contain matches for all valid date strings in a way that is
     * tricky to capture with regexes only. While thorough, it will contain some unintuitive noise:
     *
     *   '2015_06_04', in addition to matching 2015_06_04, will also contain
     *   5(!) other date matches: 15_06_04, 5_06_04, ..., even 2015 (matched as 5/1/2020)
     *
     * @param array $matches An array of matches (not Match objects)
     * @return array The provided array of matches, but with matches that are strict substrings of others removed.
     */
    protected static function removeRedundantMatches(array $matches) : array
    {
    }
    protected function getRawGuesses() : float
    {
    }
}
