<?php

namespace ZxcvbnPhp\Matchers;

class DictionaryMatch extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public $pattern = 'dictionary';
    /** @var string The name of the dictionary that the token was found in. */
    public $dictionaryName;
    /** @var int The rank of the token in the dictionary. */
    public $rank;
    /** @var string The word that was matched from the dictionary. */
    public $matchedWord;
    /** @var bool Whether or not the matched word was reversed in the token. */
    public $reversed = false;
    /** @var bool Whether or not the token contained l33t substitutions. */
    public $l33t = false;
    /** @var array A cache of the frequency_lists json file */
    protected static $rankedDictionaries = [];
    protected const START_UPPER = "/^[A-Z][^A-Z]+\$/u";
    protected const END_UPPER = "/^[^A-Z]+[A-Z]\$/u";
    protected const ALL_UPPER = "/^[^a-z]+\$/u";
    protected const ALL_LOWER = "/^[^A-Z]+\$/u";
    /**
     * Match occurrences of dictionary words in password.
     *
     * @param string $password
     * @param array $userInputs
     * @param array $rankedDictionaries
     * @return DictionaryMatch[]
     */
    public static function match(string $password, array $userInputs = [], array $rankedDictionaries = []) : array
    {
    }
    /**
     * @param string $password
     * @param int $begin
     * @param int $end
     * @param string $token
     * @param array $params An array with keys: [dictionary_name, matched_word, rank].
     */
    public function __construct(string $password, int $begin, int $end, string $token, array $params = [])
    {
    }
    /**
     * @param bool $isSoleMatch
     * @return array
     */
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    public function getFeedbackWarning(bool $isSoleMatch) : string
    {
    }
    /**
     * Attempts to find the provided password (as well as all possible substrings) in a dictionary.
     *
     * @param string $password
     * @param array $dict
     * @return array
     */
    protected static function dictionaryMatch(string $password, array $dict) : array
    {
    }
    /**
     * Load ranked frequency dictionaries.
     *
     * @return array
     */
    protected static function getRankedDictionaries() : array
    {
    }
    protected function getRawGuesses() : float
    {
    }
    protected function getUppercaseVariations() : float
    {
    }
}
