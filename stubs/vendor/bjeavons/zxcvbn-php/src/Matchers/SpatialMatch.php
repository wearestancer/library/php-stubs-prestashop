<?php

namespace ZxcvbnPhp\Matchers;

class SpatialMatch extends \ZxcvbnPhp\Matchers\BaseMatch
{
    public const SHIFTED_CHARACTERS = '~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:"ZXCVBNM<>?';
    // Preset properties since adjacency graph is constant for qwerty keyboard and keypad.
    public const KEYBOARD_STARTING_POSITION = 94;
    public const KEYPAD_STARTING_POSITION = 15;
    public const KEYBOARD_AVERAGE_DEGREES = 4.5957446809;
    // 432 / 94
    public const KEYPAD_AVERAGE_DEGREES = 5.0666666667;
    // 76 / 15
    public $pattern = 'spatial';
    /** @var int The number of characters the shift key was held for in the token. */
    public $shiftedCount;
    /** @var int The number of turns on the keyboard required to complete the token. */
    public $turns;
    /** @var string The keyboard layout that the token is a spatial match on. */
    public $graph;
    /** @var array A cache of the adjacency_graphs json file */
    protected static $adjacencyGraphs = [];
    /**
     * Match spatial patterns based on keyboard layouts (e.g. qwerty, dvorak, keypad).
     *
     * @param string $password
     * @param array $userInputs
     * @param array $graphs
     * @return SpatialMatch[]
     */
    public static function match(string $password, array $userInputs = [], array $graphs = []) : array
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    /**
     * @param string $password
     * @param int $begin
     * @param int $end
     * @param string $token
     * @param array $params An array with keys: [graph (required), shifted_count, turns].
     */
    public function __construct(string $password, int $begin, int $end, string $token, array $params = [])
    {
    }
    /**
     * Match spatial patterns in a adjacency graph.
     * @param string $password
     * @param array  $graph
     * @param string $graphName
     * @return array
     */
    protected static function graphMatch(string $password, array $graph, string $graphName) : array
    {
    }
    /**
     * Get the index of a string a character first
     *
     * @param string $string
     * @param string $char
     *
     * @return int
     */
    protected static function indexOf(string $string, string $char) : int
    {
    }
    /**
     * Load adjacency graphs.
     *
     * @return array
     */
    public static function getAdjacencyGraphs() : array
    {
    }
    protected function getRawGuesses() : float
    {
    }
}
