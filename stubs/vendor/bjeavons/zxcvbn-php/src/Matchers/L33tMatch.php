<?php

namespace ZxcvbnPhp\Matchers;

/**
 * Class L33tMatch extends DictionaryMatch to translate l33t into dictionary words for matching.
 * @package ZxcvbnPhp\Matchers
 */
class L33tMatch extends \ZxcvbnPhp\Matchers\DictionaryMatch
{
    /** @var array An array of substitutions made to get from the token to the dictionary word. */
    public $sub = [];
    /** @var string A user-readable string that shows which substitutions were detected. */
    public $subDisplay;
    /** @var bool Whether or not the token contained l33t substitutions. */
    public $l33t = true;
    /**
     * Match occurences of l33t words in password to dictionary words.
     *
     * @param string $password
     * @param array $userInputs
     * @param array $rankedDictionaries
     * @return L33tMatch[]
     */
    public static function match(string $password, array $userInputs = [], array $rankedDictionaries = []) : array
    {
    }
    /**
     * @param string $password
     * @param int $begin
     * @param int $end
     * @param string $token
     * @param array $params An array with keys: [sub, sub_display].
     */
    public function __construct(string $password, int $begin, int $end, string $token, array $params = [])
    {
    }
    #[\JetBrains\PhpStorm\ArrayShape(['warning' => 'string', 'suggestions' => 'string[]'])]
    public function getFeedback(bool $isSoleMatch) : array
    {
    }
    /**
     * @param string $string
     * @param array  $map
     * @return string
     */
    protected static function translate(string $string, array $map) : string
    {
    }
    protected static function getL33tTable() : array
    {
    }
    protected static function getL33tSubtable(string $password) : array
    {
    }
    protected static function getL33tSubstitutions(array $subtable) : array
    {
    }
    protected static function substitutionTableHelper(array $table, array $keys, array $subs) : array
    {
    }
    protected function getRawGuesses() : float
    {
    }
    protected function getL33tVariations() : float
    {
    }
}
