<?php

namespace ZxcvbnPhp;

/**
 * Feedback - gives some user guidance based on the strength
 * of a password
 *
 * @see zxcvbn/src/feedback.coffee
 */
class Feedback
{
    /**
     * @param int $score
     * @param MatchInterface[] $sequence
     * @return array
     */
    public function getFeedback(int $score, array $sequence) : array
    {
    }
}
