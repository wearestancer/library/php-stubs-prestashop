<?php

namespace ZxcvbnPhp;

/**
 * scorer - takes a list of potential matches, ranks and evaluates them,
 * and figures out how many guesses it would take to crack the password
 *
 * @see zxcvbn/src/scoring.coffee
 */
class Scorer
{
    public const MIN_GUESSES_BEFORE_GROWING_SEQUENCE = 10000;
    public const MIN_SUBMATCH_GUESSES_SINGLE_CHAR = 10;
    public const MIN_SUBMATCH_GUESSES_MULTI_CHAR = 50;
    protected $password;
    protected $excludeAdditive;
    protected $optimal = [];
    /**
     * ------------------------------------------------------------------------------
     * search --- most guessable match sequence -------------------------------------
     * ------------------------------------------------------------------------------
     *
     * takes a sequence of overlapping matches, returns the non-overlapping sequence with
     * minimum guesses. the following is a O(l_max * (n + m)) dynamic programming algorithm
     * for a length-n password with m candidate matches. l_max is the maximum optimal
     * sequence length spanning each prefix of the password. In practice it rarely exceeds 5 and the
     * search terminates rapidly.
     *
     * the optimal "minimum guesses" sequence is here defined to be the sequence that
     * minimizes the following function:
     *
     *    g = l! * Product(m.guesses for m in sequence) + D^(l - 1)
     *
     * where l is the length of the sequence.
     *
     * the factorial term is the number of ways to order l patterns.
     *
     * the D^(l-1) term is another length penalty, roughly capturing the idea that an
     * attacker will try lower-length sequences first before trying length-l sequences.
     *
     * for example, consider a sequence that is date-repeat-dictionary.
     *  - an attacker would need to try other date-repeat-dictionary combinations,
     *    hence the product term.
     *  - an attacker would need to try repeat-date-dictionary, dictionary-repeat-date,
     *    ..., hence the factorial term.
     *  - an attacker would also likely try length-1 (dictionary) and length-2 (dictionary-date)
     *    sequences before length-3. assuming at minimum D guesses per pattern type,
     *    D^(l-1) approximates Sum(D^i for i in [1..l-1]
     *
     * @param string $password
     * @param MatchInterface[] $matches
     * @param bool $excludeAdditive
     * @return array Returns an array with these keys: [password, guesses, guesses_log10, sequence]
     */
    public function getMostGuessableMatchSequence(string $password, array $matches, bool $excludeAdditive = false) : array
    {
    }
    /**
     * helper: considers whether a length-l sequence ending at match m is better (fewer guesses)
     * than previously encountered sequences, updating state if so.
     * @param BaseMatch $match
     * @param int $length
     */
    protected function update(\ZxcvbnPhp\Matchers\BaseMatch $match, int $length) : void
    {
    }
    /**
     * helper: evaluate bruteforce matches ending at k
     * @param int $end
     */
    protected function bruteforceUpdate(int $end) : void
    {
    }
    /**
     * helper: make bruteforce match objects spanning i to j, inclusive.
     * @param int $begin
     * @param int $end
     * @return Bruteforce
     */
    protected function makeBruteforceMatch(int $begin, int $end) : \ZxcvbnPhp\Matchers\Bruteforce
    {
    }
    /**
     * helper: step backwards through optimal.m starting at the end, constructing the final optimal match sequence.
     * @param int $n
     * @return MatchInterface[]
     */
    protected function unwind(int $n) : array
    {
    }
    /**
     * unoptimized, called only on small n
     * @param int $n
     * @return int
     */
    protected function factorial(int $n) : int
    {
    }
}
