<?php

namespace Nyholm\Psr7;

/**
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class UploadedFile implements \Psr\Http\Message\UploadedFileInterface
{
    /**
     * @param StreamInterface|string|resource $streamOrFile
     * @param int $size
     * @param int $errorStatus
     * @param string|null $clientFilename
     * @param string|null $clientMediaType
     */
    public function __construct($streamOrFile, $size, $errorStatus, $clientFilename = null, $clientMediaType = null)
    {
    }
    public function getStream() : \Psr\Http\Message\StreamInterface
    {
    }
    public function moveTo($targetPath) : void
    {
    }
    public function getSize() : int
    {
    }
    public function getError() : int
    {
    }
    public function getClientFilename() : ?string
    {
    }
    public function getClientMediaType() : ?string
    {
    }
}
