<?php

namespace Nyholm\Psr7;

/**
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class Response implements \Psr\Http\Message\ResponseInterface
{
    use \Nyholm\Psr7\MessageTrait;
    /**
     * @param int $status Status code
     * @param array $headers Response headers
     * @param string|resource|StreamInterface|null $body Response body
     * @param string $version Protocol version
     * @param string|null $reason Reason phrase (when empty a default will be used based on the status code)
     */
    public function __construct(int $status = 200, array $headers = [], $body = null, string $version = '1.1', string $reason = null)
    {
    }
    public function getStatusCode() : int
    {
    }
    public function getReasonPhrase() : string
    {
    }
    public function withStatus($code, $reasonPhrase = '') : self
    {
    }
}
