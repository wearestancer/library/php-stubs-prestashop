<?php

namespace Nyholm\Psr7;

/**
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class ServerRequest implements \Psr\Http\Message\ServerRequestInterface
{
    use \Nyholm\Psr7\MessageTrait;
    use \Nyholm\Psr7\RequestTrait;
    /**
     * @param string $method HTTP method
     * @param string|UriInterface $uri URI
     * @param array $headers Request headers
     * @param string|resource|StreamInterface|null $body Request body
     * @param string $version Protocol version
     * @param array $serverParams Typically the $_SERVER superglobal
     */
    public function __construct(string $method, $uri, array $headers = [], $body = null, string $version = '1.1', array $serverParams = [])
    {
    }
    public function getServerParams() : array
    {
    }
    public function getUploadedFiles() : array
    {
    }
    /**
     * @return static
     */
    public function withUploadedFiles(array $uploadedFiles)
    {
    }
    public function getCookieParams() : array
    {
    }
    /**
     * @return static
     */
    public function withCookieParams(array $cookies)
    {
    }
    public function getQueryParams() : array
    {
    }
    /**
     * @return static
     */
    public function withQueryParams(array $query)
    {
    }
    /**
     * @return array|object|null
     */
    public function getParsedBody()
    {
    }
    /**
     * @return static
     */
    public function withParsedBody($data)
    {
    }
    public function getAttributes() : array
    {
    }
    /**
     * @return mixed
     */
    public function getAttribute($attribute, $default = null)
    {
    }
    public function withAttribute($attribute, $value) : self
    {
    }
    public function withoutAttribute($attribute) : self
    {
    }
}
