<?php

namespace Nyholm\Psr7\Factory;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class Psr17Factory implements \Psr\Http\Message\RequestFactoryInterface, \Psr\Http\Message\ResponseFactoryInterface, \Psr\Http\Message\ServerRequestFactoryInterface, \Psr\Http\Message\StreamFactoryInterface, \Psr\Http\Message\UploadedFileFactoryInterface, \Psr\Http\Message\UriFactoryInterface
{
    public function createRequest(string $method, $uri) : \Psr\Http\Message\RequestInterface
    {
    }
    public function createResponse(int $code = 200, string $reasonPhrase = '') : \Psr\Http\Message\ResponseInterface
    {
    }
    public function createStream(string $content = '') : \Psr\Http\Message\StreamInterface
    {
    }
    public function createStreamFromFile(string $filename, string $mode = 'r') : \Psr\Http\Message\StreamInterface
    {
    }
    public function createStreamFromResource($resource) : \Psr\Http\Message\StreamInterface
    {
    }
    public function createUploadedFile(\Psr\Http\Message\StreamInterface $stream, int $size = null, int $error = \UPLOAD_ERR_OK, string $clientFilename = null, string $clientMediaType = null) : \Psr\Http\Message\UploadedFileInterface
    {
    }
    public function createUri(string $uri = '') : \Psr\Http\Message\UriInterface
    {
    }
    public function createServerRequest(string $method, $uri, array $serverParams = []) : \Psr\Http\Message\ServerRequestInterface
    {
    }
}
