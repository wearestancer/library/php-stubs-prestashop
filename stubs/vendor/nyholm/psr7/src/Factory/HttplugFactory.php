<?php

namespace Nyholm\Psr7\Factory;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class HttplugFactory implements \Http\Message\MessageFactory, \Http\Message\StreamFactory, \Http\Message\UriFactory
{
    public function createRequest($method, $uri, array $headers = [], $body = null, $protocolVersion = '1.1') : \Psr\Http\Message\RequestInterface
    {
    }
    public function createResponse($statusCode = 200, $reasonPhrase = null, array $headers = [], $body = null, $version = '1.1') : \Psr\Http\Message\ResponseInterface
    {
    }
    public function createStream($body = null) : \Psr\Http\Message\StreamInterface
    {
    }
    public function createUri($uri = '') : \Psr\Http\Message\UriInterface
    {
    }
}
