<?php

namespace Nyholm\Psr7;

/**
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @internal should not be used outside of Nyholm/Psr7 as it does not fall under our BC promise
 */
trait RequestTrait
{
    /** @var string */
    private $method;
    /** @var string|null */
    private $requestTarget;
    /** @var UriInterface|null */
    private $uri;
    public function getRequestTarget() : string
    {
    }
    public function withRequestTarget($requestTarget) : self
    {
    }
    public function getMethod() : string
    {
    }
    public function withMethod($method) : self
    {
    }
    public function getUri() : \Psr\Http\Message\UriInterface
    {
    }
    public function withUri(\Psr\Http\Message\UriInterface $uri, $preserveHost = false) : self
    {
    }
    private function updateHostFromUri() : void
    {
    }
}
