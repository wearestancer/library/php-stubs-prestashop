<?php

namespace Nyholm\Psr7;

/**
 * PSR-7 URI implementation.
 *
 * @author Michael Dowling
 * @author Tobias Schultze
 * @author Matthew Weier O'Phinney
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class Uri implements \Psr\Http\Message\UriInterface
{
    public function __construct(string $uri = '')
    {
    }
    public function __toString() : string
    {
    }
    public function getScheme() : string
    {
    }
    public function getAuthority() : string
    {
    }
    public function getUserInfo() : string
    {
    }
    public function getHost() : string
    {
    }
    public function getPort() : ?int
    {
    }
    public function getPath() : string
    {
    }
    public function getQuery() : string
    {
    }
    public function getFragment() : string
    {
    }
    public function withScheme($scheme) : self
    {
    }
    public function withUserInfo($user, $password = null) : self
    {
    }
    public function withHost($host) : self
    {
    }
    public function withPort($port) : self
    {
    }
    public function withPath($path) : self
    {
    }
    public function withQuery($query) : self
    {
    }
    public function withFragment($fragment) : self
    {
    }
}
