<?php

namespace Nyholm\Psr7;

/**
 * Trait implementing functionality common to requests and responses.
 *
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @internal should not be used outside of Nyholm/Psr7 as it does not fall under our BC promise
 */
trait MessageTrait
{
    /** @var array Map of all registered headers, as original name => array of values */
    private $headers = [];
    /** @var array Map of lowercase header name => original name at registration */
    private $headerNames = [];
    /** @var string */
    private $protocol = '1.1';
    /** @var StreamInterface|null */
    private $stream;
    public function getProtocolVersion() : string
    {
    }
    public function withProtocolVersion($version) : self
    {
    }
    public function getHeaders() : array
    {
    }
    public function hasHeader($header) : bool
    {
    }
    public function getHeader($header) : array
    {
    }
    public function getHeaderLine($header) : string
    {
    }
    public function withHeader($header, $value) : self
    {
    }
    public function withAddedHeader($header, $value) : self
    {
    }
    public function withoutHeader($header) : self
    {
    }
    public function getBody() : \Psr\Http\Message\StreamInterface
    {
    }
    public function withBody(\Psr\Http\Message\StreamInterface $body) : self
    {
    }
    private function setHeaders(array $headers) : void
    {
    }
    /**
     * Make sure the header complies with RFC 7230.
     *
     * Header names must be a non-empty string consisting of token characters.
     *
     * Header values must be strings consisting of visible characters with all optional
     * leading and trailing whitespace stripped. This method will always strip such
     * optional whitespace. Note that the method does not allow folding whitespace within
     * the values as this was deprecated for almost all instances by the RFC.
     *
     * header-field = field-name ":" OWS field-value OWS
     * field-name   = 1*( "!" / "#" / "$" / "%" / "&" / "'" / "*" / "+" / "-" / "." / "^"
     *              / "_" / "`" / "|" / "~" / %x30-39 / ( %x41-5A / %x61-7A ) )
     * OWS          = *( SP / HTAB )
     * field-value  = *( ( %x21-7E / %x80-FF ) [ 1*( SP / HTAB ) ( %x21-7E / %x80-FF ) ] )
     *
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     */
    private function validateAndTrimHeader($header, $values) : array
    {
    }
}
