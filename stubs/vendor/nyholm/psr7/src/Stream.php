<?php

namespace Nyholm\Psr7;

/**
 * @author Michael Dowling and contributors to guzzlehttp/psr7
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Martijn van der Ven <martijn@vanderven.se>
 *
 * @final This class should never be extended. See https://github.com/Nyholm/psr7/blob/master/doc/final.md
 */
class Stream implements \Psr\Http\Message\StreamInterface
{
    /**
     * @param resource $body
     */
    public function __construct($body)
    {
    }
    /**
     * Creates a new PSR-7 stream.
     *
     * @param string|resource|StreamInterface $body
     *
     * @throws \InvalidArgumentException
     */
    public static function create($body = '') : \Psr\Http\Message\StreamInterface
    {
    }
    /**
     * Closes the stream when the destructed.
     */
    public function __destruct()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
    public function close() : void
    {
    }
    public function detach()
    {
    }
    public function getSize() : ?int
    {
    }
    public function tell() : int
    {
    }
    public function eof() : bool
    {
    }
    public function isSeekable() : bool
    {
    }
    public function seek($offset, $whence = \SEEK_SET) : void
    {
    }
    public function rewind() : void
    {
    }
    public function isWritable() : bool
    {
    }
    public function write($string) : int
    {
    }
    public function isReadable() : bool
    {
    }
    public function read($length) : string
    {
    }
    public function getContents() : string
    {
    }
    /**
     * @return mixed
     */
    public function getMetadata($key = null)
    {
    }
}
