<?php

/**
 * This is the main Smarty class
 *
 * @package Smarty
 *
 * The following methods will be dynamically loaded by the extension handler when they are called.
 * They are located in a corresponding Smarty_Internal_Method_xxxx class
 *
 * @method int clearAllCache(int $exp_time = null, string $type = null)
 * @method int clearCache(string $template_name, string $cache_id = null, string $compile_id = null, int $exp_time = null, string $type = null)
 * @method int compileAllTemplates(string $extension = '.tpl', bool $force_compile = false, int $time_limit = 0, $max_errors = null)
 * @method int compileAllConfig(string $extension = '.conf', bool $force_compile = false, int $time_limit = 0, $max_errors = null)
 * @method int clearCompiledTemplate($resource_name = null, $compile_id = null, $exp_time = null)
 */
class Smarty extends \Smarty_Internal_TemplateBase
{
    /**
     * smarty version
     */
    const SMARTY_VERSION = '4.3.4';
    /**
     * define variable scopes
     */
    const SCOPE_LOCAL = 1;
    const SCOPE_PARENT = 2;
    const SCOPE_TPL_ROOT = 4;
    const SCOPE_ROOT = 8;
    const SCOPE_SMARTY = 16;
    const SCOPE_GLOBAL = 32;
    /**
     * define caching modes
     */
    const CACHING_OFF = 0;
    const CACHING_LIFETIME_CURRENT = 1;
    const CACHING_LIFETIME_SAVED = 2;
    /**
     * define constant for clearing cache files be saved expiration dates
     */
    const CLEAR_EXPIRED = -1;
    /**
     * define compile check modes
     */
    const COMPILECHECK_OFF = 0;
    const COMPILECHECK_ON = 1;
    const COMPILECHECK_CACHEMISS = 2;
    /**
     * define debug modes
     */
    const DEBUG_OFF = 0;
    const DEBUG_ON = 1;
    const DEBUG_INDIVIDUAL = 2;
    /**
     * filter types
     */
    const FILTER_POST = 'post';
    const FILTER_PRE = 'pre';
    const FILTER_OUTPUT = 'output';
    const FILTER_VARIABLE = 'variable';
    /**
     * plugin types
     */
    const PLUGIN_FUNCTION = 'function';
    const PLUGIN_BLOCK = 'block';
    const PLUGIN_COMPILER = 'compiler';
    const PLUGIN_MODIFIER = 'modifier';
    const PLUGIN_MODIFIERCOMPILER = 'modifiercompiler';
    /**
     * assigned global tpl vars
     */
    public static $global_tpl_vars = array();
    /**
     * Flag denoting if Multibyte String functions are available
     */
    public static $_MBSTRING = \SMARTY_MBSTRING;
    /**
     * The character set to adhere to (e.g. "UTF-8")
     */
    public static $_CHARSET = \SMARTY_MBSTRING ? 'UTF-8' : 'ISO-8859-1';
    /**
     * The date format to be used internally
     * (accepts date() and strftime())
     */
    public static $_DATE_FORMAT = '%b %e, %Y';
    /**
     * Flag denoting if PCRE should run in UTF-8 mode
     */
    public static $_UTF8_MODIFIER = 'u';
    /**
     * Flag denoting if operating system is windows
     */
    public static $_IS_WINDOWS = \false;
    /**
     * auto literal on delimiters with whitespace
     *
     * @var boolean
     */
    public $auto_literal = \true;
    /**
     * display error on not assigned variables
     *
     * @var boolean
     */
    public $error_unassigned = \false;
    /**
     * look up relative file path in include_path
     *
     * @var boolean
     */
    public $use_include_path = \false;
    /**
     * flag if template_dir is normalized
     *
     * @var bool
     */
    public $_templateDirNormalized = \false;
    /**
     * joined template directory string used in cache keys
     *
     * @var string
     */
    public $_joined_template_dir = \null;
    /**
     * flag if config_dir is normalized
     *
     * @var bool
     */
    public $_configDirNormalized = \false;
    /**
     * joined config directory string used in cache keys
     *
     * @var string
     */
    public $_joined_config_dir = \null;
    /**
     * default template handler
     *
     * @var callable
     */
    public $default_template_handler_func = \null;
    /**
     * default config handler
     *
     * @var callable
     */
    public $default_config_handler_func = \null;
    /**
     * default plugin handler
     *
     * @var callable
     */
    public $default_plugin_handler_func = \null;
    /**
     * flag if template_dir is normalized
     *
     * @var bool
     */
    public $_compileDirNormalized = \false;
    /**
     * flag if plugins_dir is normalized
     *
     * @var bool
     */
    public $_pluginsDirNormalized = \false;
    /**
     * flag if template_dir is normalized
     *
     * @var bool
     */
    public $_cacheDirNormalized = \false;
    /**
     * force template compiling?
     *
     * @var boolean
     */
    public $force_compile = \false;
    /**
     * use sub dirs for compiled/cached files?
     *
     * @var boolean
     */
    public $use_sub_dirs = \false;
    /**
     * allow ambiguous resources (that are made unique by the resource handler)
     *
     * @var boolean
     */
    public $allow_ambiguous_resources = \false;
    /**
     * merge compiled includes
     *
     * @var boolean
     */
    public $merge_compiled_includes = \false;
    /*
     * flag for behaviour when extends: resource  and {extends} tag are used simultaneous
     *   if false disable execution of {extends} in templates called by extends resource.
     *   (behaviour as versions < 3.1.28)
     *
     * @var boolean
     */
    public $extends_recursion = \true;
    /**
     * force cache file creation
     *
     * @var boolean
     */
    public $force_cache = \false;
    /**
     * template left-delimiter
     *
     * @var string
     */
    public $left_delimiter = "{";
    /**
     * template right-delimiter
     *
     * @var string
     */
    public $right_delimiter = "}";
    /**
     * array of strings which shall be treated as literal by compiler
     *
     * @var array string
     */
    public $literals = array();
    /**
     * class name
     * This should be instance of Smarty_Security.
     *
     * @var string
     * @see Smarty_Security
     */
    public $security_class = 'Smarty_Security';
    /**
     * implementation of security class
     *
     * @var Smarty_Security
     */
    public $security_policy = \null;
    /**
     * controls if the php template file resource is allowed
     *
     * @var bool
     */
    public $allow_php_templates = \false;
    /**
     * debug mode
     * Setting this to true enables the debug-console.
     *
     * @var boolean
     */
    public $debugging = \false;
    /**
     * This determines if debugging is enable-able from the browser.
     * <ul>
     *  <li>NONE => no debugging control allowed</li>
     *  <li>URL => enable debugging when SMARTY_DEBUG is found in the URL.</li>
     * </ul>
     *
     * @var string
     */
    public $debugging_ctrl = 'NONE';
    /**
     * Name of debugging URL-param.
     * Only used when $debugging_ctrl is set to 'URL'.
     * The name of the URL-parameter that activates debugging.
     *
     * @var string
     */
    public $smarty_debug_id = 'SMARTY_DEBUG';
    /**
     * Path of debug template.
     *
     * @var string
     */
    public $debug_tpl = \null;
    /**
     * When set, smarty uses this value as error_reporting-level.
     *
     * @var int
     */
    public $error_reporting = \null;
    /**
     * Controls whether variables with the same name overwrite each other.
     *
     * @var boolean
     */
    public $config_overwrite = \true;
    /**
     * Controls whether config values of on/true/yes and off/false/no get converted to boolean.
     *
     * @var boolean
     */
    public $config_booleanize = \true;
    /**
     * Controls whether hidden config sections/vars are read from the file.
     *
     * @var boolean
     */
    public $config_read_hidden = \false;
    /**
     * locking concurrent compiles
     *
     * @var boolean
     */
    public $compile_locking = \true;
    /**
     * Controls whether cache resources should use locking mechanism
     *
     * @var boolean
     */
    public $cache_locking = \false;
    /**
     * seconds to wait for acquiring a lock before ignoring the write lock
     *
     * @var float
     */
    public $locking_timeout = 10;
    /**
     * resource type used if none given
     * Must be an valid key of $registered_resources.
     *
     * @var string
     */
    public $default_resource_type = 'file';
    /**
     * caching type
     * Must be an element of $cache_resource_types.
     *
     * @var string
     */
    public $caching_type = 'file';
    /**
     * config type
     *
     * @var string
     */
    public $default_config_type = 'file';
    /**
     * check If-Modified-Since headers
     *
     * @var boolean
     */
    public $cache_modified_check = \false;
    /**
     * registered plugins
     *
     * @var array
     */
    public $registered_plugins = array();
    /**
     * registered objects
     *
     * @var array
     */
    public $registered_objects = array();
    /**
     * registered classes
     *
     * @var array
     */
    public $registered_classes = array();
    /**
     * registered filters
     *
     * @var array
     */
    public $registered_filters = array();
    /**
     * registered resources
     *
     * @var array
     */
    public $registered_resources = array();
    /**
     * registered cache resources
     *
     * @var array
     */
    public $registered_cache_resources = array();
    /**
     * autoload filter
     *
     * @var array
     */
    public $autoload_filters = array();
    /**
     * default modifier
     *
     * @var array
     */
    public $default_modifiers = array();
    /**
     * autoescape variable output
     *
     * @var boolean
     */
    public $escape_html = \false;
    /**
     * start time for execution time calculation
     *
     * @var int
     */
    public $start_time = 0;
    /**
     * required by the compiler for BC
     *
     * @var string
     */
    public $_current_file = \null;
    /**
     * internal flag to enable parser debugging
     *
     * @var bool
     */
    public $_parserdebug = \false;
    /**
     * This object type (Smarty = 1, template = 2, data = 4)
     *
     * @var int
     */
    public $_objType = 1;
    /**
     * Debug object
     *
     * @var Smarty_Internal_Debug
     */
    public $_debug = \null;
    /**
     * template directory
     *
     * @var array
     */
    protected $template_dir = array('./templates/');
    /**
     * flags for normalized template directory entries
     *
     * @var array
     */
    protected $_processedTemplateDir = array();
    /**
     * config directory
     *
     * @var array
     */
    protected $config_dir = array('./configs/');
    /**
     * flags for normalized template directory entries
     *
     * @var array
     */
    protected $_processedConfigDir = array();
    /**
     * compile directory
     *
     * @var string
     */
    protected $compile_dir = './templates_c/';
    /**
     * plugins directory
     *
     * @var array
     */
    protected $plugins_dir = array();
    /**
     * cache directory
     *
     * @var string
     */
    protected $cache_dir = './cache/';
    /**
     * removed properties
     *
     * @var string[]
     */
    protected $obsoleteProperties = array('resource_caching', 'template_resource_caching', 'direct_access_security', '_dir_perms', '_file_perms', 'plugin_search_order', 'inheritance_merge_compiled_includes', 'resource_cache_mode');
    /**
     * List of private properties which will call getter/setter on a direct access
     *
     * @var string[]
     */
    protected $accessMap = array('template_dir' => 'TemplateDir', 'config_dir' => 'ConfigDir', 'plugins_dir' => 'PluginsDir', 'compile_dir' => 'CompileDir', 'cache_dir' => 'CacheDir');
    /**
     * Initialize new Smarty object
     */
    public function __construct()
    {
    }
    /**
     * Check if a template resource exists
     *
     * @param string $resource_name template name
     *
     * @return bool status
     * @throws \SmartyException
     */
    public function templateExists($resource_name)
    {
    }
    /**
     * Loads security class and enables security
     *
     * @param string|Smarty_Security $security_class if a string is used, it must be class-name
     *
     * @return Smarty                 current Smarty instance for chaining
     * @throws \SmartyException
     */
    public function enableSecurity($security_class = \null)
    {
    }
    /**
     * Disable security
     *
     * @return Smarty current Smarty instance for chaining
     */
    public function disableSecurity()
    {
    }
    /**
     * Add template directory(s)
     *
     * @param string|array $template_dir directory(s) of template sources
     * @param string       $key          of the array element to assign the template dir to
     * @param bool         $isConfig     true for config_dir
     *
     * @return Smarty          current Smarty instance for chaining
     */
    public function addTemplateDir($template_dir, $key = \null, $isConfig = \false)
    {
    }
    /**
     * Get template directories
     *
     * @param mixed $index    index of directory to get, null to get all
     * @param bool  $isConfig true for config_dir
     *
     * @return array|string list of template directories, or directory of $index
     */
    public function getTemplateDir($index = \null, $isConfig = \false)
    {
    }
    /**
     * Set template directory
     *
     * @param string|array $template_dir directory(s) of template sources
     * @param bool         $isConfig     true for config_dir
     *
     * @return \Smarty current Smarty instance for chaining
     */
    public function setTemplateDir($template_dir, $isConfig = \false)
    {
    }
    /**
     * Add config directory(s)
     *
     * @param string|array $config_dir directory(s) of config sources
     * @param mixed        $key        key of the array element to assign the config dir to
     *
     * @return Smarty current Smarty instance for chaining
     */
    public function addConfigDir($config_dir, $key = \null)
    {
    }
    /**
     * Get config directory
     *
     * @param mixed $index index of directory to get, null to get all
     *
     * @return array configuration directory
     */
    public function getConfigDir($index = \null)
    {
    }
    /**
     * Set config directory
     *
     * @param $config_dir
     *
     * @return Smarty       current Smarty instance for chaining
     */
    public function setConfigDir($config_dir)
    {
    }
    /**
     * Adds directory of plugin files
     *
     * @param null|array|string $plugins_dir
     *
     * @return Smarty current Smarty instance for chaining
     */
    public function addPluginsDir($plugins_dir)
    {
    }
    /**
     * Get plugin directories
     *
     * @return array list of plugin directories
     */
    public function getPluginsDir()
    {
    }
    /**
     * Set plugins directory
     *
     * @param string|array $plugins_dir directory(s) of plugins
     *
     * @return Smarty       current Smarty instance for chaining
     */
    public function setPluginsDir($plugins_dir)
    {
    }
    /**
     * Get compiled directory
     *
     * @return string path to compiled templates
     */
    public function getCompileDir()
    {
    }
    /**
     *
     * @param  string $compile_dir directory to store compiled templates in
     *
     * @return Smarty current Smarty instance for chaining
     */
    public function setCompileDir($compile_dir)
    {
    }
    /**
     * Get cache directory
     *
     * @return string path of cache directory
     */
    public function getCacheDir()
    {
    }
    /**
     * Set cache directory
     *
     * @param string $cache_dir directory to store cached templates in
     *
     * @return Smarty current Smarty instance for chaining
     */
    public function setCacheDir($cache_dir)
    {
    }
    /**
     * creates a template object
     *
     * @param string  $template   the resource handle of the template file
     * @param mixed   $cache_id   cache id to be used with this template
     * @param mixed   $compile_id compile id to be used with this template
     * @param object  $parent     next higher level of Smarty variables
     * @param boolean $do_clone   flag is Smarty object shall be cloned
     *
     * @return \Smarty_Internal_Template template object
     * @throws \SmartyException
     */
    public function createTemplate($template, $cache_id = \null, $compile_id = \null, $parent = \null, $do_clone = \true)
    {
    }
    /**
     * Takes unknown classes and loads plugin files for them
     * class name format: Smarty_PluginType_PluginName
     * plugin filename format: plugintype.pluginname.php
     *
     * @param string $plugin_name class plugin name to load
     * @param bool   $check       check if already loaded
     *
     * @return string |boolean filepath of loaded file or false
     * @throws \SmartyException
     */
    public function loadPlugin($plugin_name, $check = \true)
    {
    }
    /**
     * Get unique template id
     *
     * @param string                    $template_name
     * @param null|mixed                $cache_id
     * @param null|mixed                $compile_id
     * @param null                      $caching
     * @param \Smarty_Internal_Template $template
     *
     * @return string
     * @throws \SmartyException
     */
    public function _getTemplateId($template_name, $cache_id = \null, $compile_id = \null, $caching = \null, \Smarty_Internal_Template $template = \null)
    {
    }
    /**
     * Normalize path
     *  - remove /./ and /../
     *  - make it absolute if required
     *
     * @param string $path     file path
     * @param bool   $realpath if true - convert to absolute
     *                         false - convert to relative
     *                         null - keep as it is but
     *                         remove /./ /../
     *
     * @return string
     */
    public function _realpath($path, $realpath = \null)
    {
    }
    /**
     * Empty template objects cache
     */
    public function _clearTemplateCache()
    {
    }
    /**
     * @param boolean $use_sub_dirs
     */
    public function setUseSubDirs($use_sub_dirs)
    {
    }
    /**
     * @param int $error_reporting
     */
    public function setErrorReporting($error_reporting)
    {
    }
    /**
     * @param boolean $escape_html
     */
    public function setEscapeHtml($escape_html)
    {
    }
    /**
     * Return auto_literal flag
     *
     * @return boolean
     */
    public function getAutoLiteral()
    {
    }
    /**
     * Set auto_literal flag
     *
     * @param boolean $auto_literal
     */
    public function setAutoLiteral($auto_literal = \true)
    {
    }
    /**
     * @param boolean $force_compile
     */
    public function setForceCompile($force_compile)
    {
    }
    /**
     * @param boolean $merge_compiled_includes
     */
    public function setMergeCompiledIncludes($merge_compiled_includes)
    {
    }
    /**
     * Get left delimiter
     *
     * @return string
     */
    public function getLeftDelimiter()
    {
    }
    /**
     * Set left delimiter
     *
     * @param string $left_delimiter
     */
    public function setLeftDelimiter($left_delimiter)
    {
    }
    /**
     * Get right delimiter
     *
     * @return string $right_delimiter
     */
    public function getRightDelimiter()
    {
    }
    /**
     * Set right delimiter
     *
     * @param string
     */
    public function setRightDelimiter($right_delimiter)
    {
    }
    /**
     * @param boolean $debugging
     */
    public function setDebugging($debugging)
    {
    }
    /**
     * @param boolean $config_overwrite
     */
    public function setConfigOverwrite($config_overwrite)
    {
    }
    /**
     * @param boolean $config_booleanize
     */
    public function setConfigBooleanize($config_booleanize)
    {
    }
    /**
     * @param boolean $config_read_hidden
     */
    public function setConfigReadHidden($config_read_hidden)
    {
    }
    /**
     * @param boolean $compile_locking
     */
    public function setCompileLocking($compile_locking)
    {
    }
    /**
     * @param string $default_resource_type
     */
    public function setDefaultResourceType($default_resource_type)
    {
    }
    /**
     * @param string $caching_type
     */
    public function setCachingType($caching_type)
    {
    }
    /**
     * Test install
     *
     * @param null $errors
     */
    public function testInstall(&$errors = \null)
    {
    }
    /**
     * Get Smarty object
     *
     * @return Smarty
     */
    public function _getSmartyObj()
    {
    }
    /**
     * <<magic>> Generic getter.
     * Calls the appropriate getter function.
     * Issues an E_USER_NOTICE if no valid getter is found.
     *
     * @param string $name property name
     *
     * @return mixed
     */
    public function __get($name)
    {
    }
    /**
     * <<magic>> Generic setter.
     * Calls the appropriate setter function.
     * Issues an E_USER_NOTICE if no valid setter is found.
     *
     * @param string $name  property name
     * @param mixed  $value parameter passed to setter
     *
     */
    public function __set($name, $value)
    {
    }
    /**
     * Mutes errors for "undefined index", "undefined array key" and "trying to read property of null".
     *
     * @void
     */
    public function muteUndefinedOrNullWarnings() : void
    {
    }
    /**
     * Indicates if Smarty will mute errors for "undefined index", "undefined array key" and "trying to read property of null".
     * @bool
     */
    public function isMutingUndefinedOrNullWarnings() : bool
    {
    }
}
/**
 *
 */
\define('SMARTY_DIR', __DIR__ . \DIRECTORY_SEPARATOR);
/**
 *
 */
\define('SMARTY_SYSPLUGINS_DIR', \SMARTY_DIR . 'sysplugins' . \DIRECTORY_SEPARATOR);
/**
 *
 */
\define('SMARTY_PLUGINS_DIR', \SMARTY_DIR . 'plugins' . \DIRECTORY_SEPARATOR);
/**
 *
 */
\define('SMARTY_MBSTRING', \function_exists('mb_get_info'));
