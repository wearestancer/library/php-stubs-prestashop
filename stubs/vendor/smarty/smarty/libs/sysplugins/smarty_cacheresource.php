<?php

/**
 * Smarty Internal Plugin
 *
 * @package    Smarty
 * @subpackage Cacher
 */
/**
 * Cache Handler API
 *
 * @package    Smarty
 * @subpackage Cacher
 * @author     Rodney Rehm
 */
abstract class Smarty_CacheResource
{
    /**
     * resource types provided by the core
     *
     * @var array
     */
    protected static $sysplugins = array('file' => 'smarty_internal_cacheresource_file.php');
    /**
     * populate Cached Object with meta data from Resource
     *
     * @param \Smarty_Template_Cached  $cached    cached object
     * @param Smarty_Internal_Template $_template template object
     *
     * @return void
     */
    public abstract function populate(\Smarty_Template_Cached $cached, \Smarty_Internal_Template $_template);
    /**
     * populate Cached Object with timestamp and exists from Resource
     *
     * @param Smarty_Template_Cached $cached
     *
     * @return void
     */
    public abstract function populateTimestamp(\Smarty_Template_Cached $cached);
    /**
     * Read the cached template and process header
     *
     * @param Smarty_Internal_Template $_template template object
     * @param Smarty_Template_Cached   $cached    cached object
     * @param boolean                  $update    flag if called because cache update
     *
     * @return boolean true or false if the cached content does not exist
     */
    public abstract function process(\Smarty_Internal_Template $_template, \Smarty_Template_Cached $cached = \null, $update = \false);
    /**
     * Write the rendered template output to cache
     *
     * @param Smarty_Internal_Template $_template template object
     * @param string                   $content   content to cache
     *
     * @return boolean success
     */
    public abstract function writeCachedContent(\Smarty_Internal_Template $_template, $content);
    /**
     * Read cached template from cache
     *
     * @param Smarty_Internal_Template $_template template object
     *
     * @return string  content
     */
    public abstract function readCachedContent(\Smarty_Internal_Template $_template);
    /**
     * Return cached content
     *
     * @param Smarty_Internal_Template $_template template object
     *
     * @return null|string
     */
    public function getCachedContent(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Empty cache
     *
     * @param Smarty  $smarty   Smarty object
     * @param integer $exp_time expiration time (number of seconds, not timestamp)
     *
     * @return integer number of cache files deleted
     */
    public abstract function clearAll(\Smarty $smarty, $exp_time = \null);
    /**
     * Empty cache for a specific template
     *
     * @param Smarty  $smarty        Smarty object
     * @param string  $resource_name template name
     * @param string  $cache_id      cache id
     * @param string  $compile_id    compile id
     * @param integer $exp_time      expiration time (number of seconds, not timestamp)
     *
     * @return integer number of cache files deleted
     */
    public abstract function clear(\Smarty $smarty, $resource_name, $cache_id, $compile_id, $exp_time);
    /**
     * @param Smarty                 $smarty
     * @param Smarty_Template_Cached $cached
     *
     * @return bool|null
     */
    public function locked(\Smarty $smarty, \Smarty_Template_Cached $cached)
    {
    }
    /**
     * Check is cache is locked for this template
     *
     * @param Smarty                 $smarty
     * @param Smarty_Template_Cached $cached
     *
     * @return bool
     */
    public function hasLock(\Smarty $smarty, \Smarty_Template_Cached $cached)
    {
    }
    /**
     * Lock cache for this template
     *
     * @param Smarty                 $smarty
     * @param Smarty_Template_Cached $cached
     *
     * @return bool
     */
    public function acquireLock(\Smarty $smarty, \Smarty_Template_Cached $cached)
    {
    }
    /**
     * Unlock cache for this template
     *
     * @param Smarty                 $smarty
     * @param Smarty_Template_Cached $cached
     *
     * @return bool
     */
    public function releaseLock(\Smarty $smarty, \Smarty_Template_Cached $cached)
    {
    }
    /**
     * Load Cache Resource Handler
     *
     * @param Smarty $smarty Smarty object
     * @param string $type   name of the cache resource
     *
     * @throws SmartyException
     * @return Smarty_CacheResource Cache Resource Handler
     */
    public static function load(\Smarty $smarty, $type = \null)
    {
    }
}
