<?php

/**
 * Inline Runtime Methods render, setSourceByUid, setupSubTemplate
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 **/
class Smarty_Internal_Runtime_UpdateCache
{
    /**
     * check client side cache
     *
     * @param \Smarty_Template_Cached  $cached
     * @param Smarty_Internal_Template $_template
     * @param string                   $content
     */
    public function cacheModifiedCheck(\Smarty_Template_Cached $cached, \Smarty_Internal_Template $_template, $content)
    {
    }
    /**
     * Cache was invalid , so render from compiled and write to cache
     *
     * @param \Smarty_Template_Cached   $cached
     * @param \Smarty_Internal_Template $_template
     * @param                           $no_output_filter
     *
     * @throws \Exception
     */
    public function updateCache(\Smarty_Template_Cached $cached, \Smarty_Internal_Template $_template, $no_output_filter)
    {
    }
    /**
     * Sanitize content and write it to cache resource
     *
     * @param \Smarty_Template_Cached  $cached
     * @param Smarty_Internal_Template $_template
     * @param bool                     $no_output_filter
     *
     * @throws \SmartyException
     */
    public function removeNoCacheHash(\Smarty_Template_Cached $cached, \Smarty_Internal_Template $_template, $no_output_filter)
    {
    }
    /**
     * Writes the content to cache resource
     *
     * @param Smarty_Internal_Template $_template
     * @param string                   $content
     *
     * @return bool
     */
    public function writeCachedContent(\Smarty_Internal_Template $_template, $content)
    {
    }
    /**
     * Write this cache object to handler
     *
     * @param Smarty_Internal_Template $_template template object
     * @param string                   $content   content to cache
     *
     * @return bool success
     */
    public function write(\Smarty_Internal_Template $_template, $content)
    {
    }
}
