<?php

/*
 * This file is part of Smarty.
 *
 * (c) 2015 Uwe Tews
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Smarty_Internal_Templatelexer
 * This is the template file lexer.
 * It is generated from the smarty_internal_templatelexer.plex file
 *
 *
 * @author Uwe Tews <uwe.tews@googlemail.com>
 */
class Smarty_Internal_Templatelexer
{
    /**
     * Source
     *
     * @var string
     */
    public $data;
    /**
     * Source length
     *
     * @var int
     */
    public $dataLength = \null;
    /**
     * byte counter
     *
     * @var int
     */
    public $counter;
    /**
     * token number
     *
     * @var int
     */
    public $token;
    /**
     * token value
     *
     * @var string
     */
    public $value;
    /**
     * current line
     *
     * @var int
     */
    public $line;
    /**
     * tag start line
     *
     * @var
     */
    public $taglineno;
    /**
     * php code type
     *
     * @var string
     */
    public $phpType = '';
    /**
     * state number
     *
     * @var int
     */
    public $state = 1;
    /**
     * Smarty object
     *
     * @var Smarty
     */
    public $smarty = \null;
    /**
     * compiler object
     *
     * @var Smarty_Internal_TemplateCompilerBase
     */
    public $compiler = \null;
    /**
     * trace file
     *
     * @var resource
     */
    public $yyTraceFILE;
    /**
     * trace prompt
     *
     * @var string
     */
    public $yyTracePrompt;
    /**
     * XML flag true while processing xml
     *
     * @var bool
     */
    public $is_xml = \false;
    /**
     * state names
     *
     * @var array
     */
    public $state_name = array(1 => 'TEXT', 2 => 'TAG', 3 => 'TAGBODY', 4 => 'LITERAL', 5 => 'DOUBLEQUOTEDSTRING');
    /**
     * token names
     *
     * @var array
     */
    public $smarty_token_names = array(
        // Text for parser error messages
        'NOT' => '(!,not)',
        'OPENP' => '(',
        'CLOSEP' => ')',
        'OPENB' => '[',
        'CLOSEB' => ']',
        'PTR' => '->',
        'APTR' => '=>',
        'EQUAL' => '=',
        'NUMBER' => 'number',
        'UNIMATH' => '+" , "-',
        'MATH' => '*" , "/" , "%',
        'INCDEC' => '++" , "--',
        'SPACE' => ' ',
        'DOLLAR' => '$',
        'SEMICOLON' => ';',
        'COLON' => ':',
        'DOUBLECOLON' => '::',
        'AT' => '@',
        'HATCH' => '#',
        'QUOTE' => '"',
        'BACKTICK' => '`',
        'VERT' => '"|" modifier',
        'DOT' => '.',
        'COMMA' => '","',
        'QMARK' => '"?"',
        'ID' => 'id, name',
        'TEXT' => 'text',
        'LDELSLASH' => '{/..} closing tag',
        'LDEL' => '{...} Smarty tag',
        'COMMENT' => 'comment',
        'AS' => 'as',
        'TO' => 'to',
        'PHP' => '"<?php", "<%", "{php}" tag',
        'LOGOP' => '"<", "==" ... logical operator',
        'TLOGOP' => '"lt", "eq" ... logical operator; "is div by" ... if condition',
        'SCOND' => '"is even" ... if condition',
    );
    /**
     * constructor
     *
     * @param   string                             $source template source
     * @param Smarty_Internal_TemplateCompilerBase $compiler
     */
    public function __construct($source, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
    /**
     * open lexer/parser trace file
     *
     */
    public function PrintTrace()
    {
    }
    /**
     * replace placeholders with runtime preg  code
     *
     * @param string $preg
     *
     * @return string
     */
    public function replace($preg)
    {
    }
    /**
     * check if current value is an autoliteral left delimiter
     *
     * @return bool
     */
    public function isAutoLiteral()
    {
    }
    public function yylex()
    {
    }
    public function yypushstate($state)
    {
    }
    public function yypopstate()
    {
    }
    public function yybegin($state)
    {
    }
    public function yylex1()
    {
    }
    // end function
    const TEXT = 1;
    public function yy_r1_1()
    {
    }
    public function yy_r1_2()
    {
    }
    public function yy_r1_4()
    {
    }
    public function yy_r1_6()
    {
    }
    public function yy_r1_8()
    {
    }
    public function yy_r1_10()
    {
    }
    public function yy_r1_12()
    {
    }
    public function yylex2()
    {
    }
    // end function
    const TAG = 2;
    public function yy_r2_1()
    {
    }
    public function yy_r2_4()
    {
    }
    public function yy_r2_6()
    {
    }
    public function yy_r2_8()
    {
    }
    public function yy_r2_10()
    {
    }
    public function yy_r2_12()
    {
    }
    public function yy_r2_15()
    {
    }
    public function yy_r2_18()
    {
    }
    public function yy_r2_20()
    {
    }
    public function yy_r2_23()
    {
    }
    public function yy_r2_25()
    {
    }
    public function yylex3()
    {
    }
    // end function
    const TAGBODY = 3;
    public function yy_r3_1()
    {
    }
    public function yy_r3_2()
    {
    }
    public function yy_r3_4()
    {
    }
    public function yy_r3_5()
    {
    }
    public function yy_r3_6()
    {
    }
    public function yy_r3_7()
    {
    }
    public function yy_r3_8()
    {
    }
    public function yy_r3_9()
    {
    }
    public function yy_r3_10()
    {
    }
    public function yy_r3_11()
    {
    }
    public function yy_r3_12()
    {
    }
    public function yy_r3_13()
    {
    }
    public function yy_r3_15()
    {
    }
    public function yy_r3_17()
    {
    }
    public function yy_r3_20()
    {
    }
    public function yy_r3_23()
    {
    }
    public function yy_r3_24()
    {
    }
    public function yy_r3_28()
    {
    }
    public function yy_r3_29()
    {
    }
    public function yy_r3_30()
    {
    }
    public function yy_r3_31()
    {
    }
    public function yy_r3_32()
    {
    }
    public function yy_r3_33()
    {
    }
    public function yy_r3_34()
    {
    }
    public function yy_r3_35()
    {
    }
    public function yy_r3_37()
    {
    }
    public function yy_r3_39()
    {
    }
    public function yy_r3_41()
    {
    }
    public function yy_r3_42()
    {
    }
    public function yy_r3_43()
    {
    }
    public function yy_r3_44()
    {
    }
    public function yy_r3_45()
    {
    }
    public function yy_r3_48()
    {
    }
    public function yy_r3_49()
    {
    }
    public function yy_r3_50()
    {
    }
    public function yy_r3_51()
    {
    }
    public function yy_r3_52()
    {
    }
    public function yy_r3_53()
    {
    }
    public function yy_r3_54()
    {
    }
    public function yy_r3_55()
    {
    }
    public function yy_r3_56()
    {
    }
    public function yy_r3_57()
    {
    }
    public function yy_r3_58()
    {
    }
    public function yy_r3_59()
    {
    }
    public function yy_r3_60()
    {
    }
    public function yylex4()
    {
    }
    // end function
    const LITERAL = 4;
    public function yy_r4_1()
    {
    }
    public function yy_r4_3()
    {
    }
    public function yy_r4_5()
    {
    }
    public function yylex5()
    {
    }
    // end function
    const DOUBLEQUOTEDSTRING = 5;
    public function yy_r5_1()
    {
    }
    public function yy_r5_3()
    {
    }
    public function yy_r5_5()
    {
    }
    public function yy_r5_7()
    {
    }
    public function yy_r5_9()
    {
    }
    public function yy_r5_11()
    {
    }
    public function yy_r5_13()
    {
    }
    public function yy_r5_14()
    {
    }
    public function yy_r5_15()
    {
    }
    public function yy_r5_16()
    {
    }
    public function yy_r5_17()
    {
    }
    public function yy_r5_22()
    {
    }
}
