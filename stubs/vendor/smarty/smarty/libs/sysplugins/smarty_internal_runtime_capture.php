<?php

/**
 * Runtime Extension Capture
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Runtime_Capture
{
    /**
     * Flag that this instance  will not be cached
     *
     * @var bool
     */
    public $isPrivateExtension = \true;
    /**
     * Open capture section
     *
     * @param \Smarty_Internal_Template $_template
     * @param string                    $buffer capture name
     * @param string                    $assign variable name
     * @param string                    $append variable name
     */
    public function open(\Smarty_Internal_Template $_template, $buffer, $assign, $append)
    {
    }
    /**
     * Start render callback
     *
     * @param \Smarty_Internal_Template $_template
     */
    public function startRender(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Close capture section
     *
     * @param \Smarty_Internal_Template $_template
     *
     * @throws \SmartyException
     */
    public function close(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Error exception on not matching {capture}{/capture}
     *
     * @param \Smarty_Internal_Template $_template
     *
     * @throws \SmartyException
     */
    public function error(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Return content of named capture buffer by key or as array
     *
     * @param \Smarty_Internal_Template $_template
     * @param string|null               $name
     *
     * @return string|string[]|null
     */
    public function getBuffer(\Smarty_Internal_Template $_template, $name = \null)
    {
    }
    /**
     * End render callback
     *
     * @param \Smarty_Internal_Template $_template
     *
     * @throws \SmartyException
     */
    public function endRender(\Smarty_Internal_Template $_template)
    {
    }
}
