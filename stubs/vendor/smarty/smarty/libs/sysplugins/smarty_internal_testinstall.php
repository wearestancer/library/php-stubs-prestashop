<?php

/**
 * Smarty Internal TestInstall
 * Test Smarty installation
 *
 * @package    Smarty
 * @subpackage Utilities
 * @author     Uwe Tews
 */
/**
 * TestInstall class
 *
 * @package    Smarty
 * @subpackage Utilities
 */
class Smarty_Internal_TestInstall
{
    /**
     * diagnose Smarty setup
     * If $errors is secified, the diagnostic report will be appended to the array, rather than being output.
     *
     * @param \Smarty $smarty
     * @param array   $errors array to push results into rather than outputting them
     *
     * @return bool status, true if everything is fine, false else
     */
    public static function testInstall(\Smarty $smarty, &$errors = \null)
    {
    }
}
