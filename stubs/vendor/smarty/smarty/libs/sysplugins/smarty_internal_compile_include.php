<?php

/**
 * Smarty Internal Plugin Compile Include
 * Compiles the {include} tag
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile Include Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Include extends \Smarty_Internal_CompileBase
{
    /**
     * caching mode to create nocache code but no cache file
     */
    const CACHING_NOCACHE_CODE = 9999;
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $required_attributes = array('file');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $shorttag_order = array('file');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $option_flags = array('nocache', 'inline', 'caching');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $optional_attributes = array('_any');
    /**
     * Valid scope names
     *
     * @var array
     */
    public $valid_scopes = array('parent' => \Smarty::SCOPE_PARENT, 'root' => \Smarty::SCOPE_ROOT, 'global' => \Smarty::SCOPE_GLOBAL, 'tpl_root' => \Smarty::SCOPE_TPL_ROOT, 'smarty' => \Smarty::SCOPE_SMARTY);
    /**
     * Compiles code for the {include} tag
     *
     * @param array                                  $args     array with attributes from parser
     * @param Smarty_Internal_SmartyTemplateCompiler $compiler compiler object
     *
     * @return string
     * @throws \Exception
     * @throws \SmartyCompilerException
     * @throws \SmartyException
     */
    public function compile($args, \Smarty_Internal_SmartyTemplateCompiler $compiler)
    {
    }
    /**
     * Compile inline sub template
     *
     * @param \Smarty_Internal_SmartyTemplateCompiler $compiler
     * @param \Smarty_Internal_Template               $tpl
     * @param string                                  $t_hash
     *
     * @return bool
     * @throws \Exception
     * @throws \SmartyException
     */
    public function compileInlineTemplate(\Smarty_Internal_SmartyTemplateCompiler $compiler, \Smarty_Internal_Template $tpl, $t_hash)
    {
    }
}
