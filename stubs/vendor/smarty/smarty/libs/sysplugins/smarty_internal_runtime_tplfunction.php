<?php

/**
 * TplFunction Runtime Methods callTemplateFunction
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 **/
class Smarty_Internal_Runtime_TplFunction
{
    /**
     * Call template function
     *
     * @param \Smarty_Internal_Template $tpl     template object
     * @param string                    $name    template function name
     * @param array                     $params  parameter array
     * @param bool                      $nocache true if called nocache
     *
     * @throws \SmartyException
     */
    public function callTemplateFunction(\Smarty_Internal_Template $tpl, $name, $params, $nocache)
    {
    }
    /**
     * Register template functions defined by template
     *
     * @param \Smarty|\Smarty_Internal_Template|\Smarty_Internal_TemplateBase $obj
     * @param array                                                           $tplFunctions source information array of
     *                                                                                      template functions defined
     *                                                                                      in template
     * @param bool                                                            $override     if true replace existing
     *                                                                                      functions with same name
     */
    public function registerTplFunctions(\Smarty_Internal_TemplateBase $obj, $tplFunctions, $override = \true)
    {
    }
    /**
     * Return source parameter array for single or all template functions
     *
     * @param \Smarty_Internal_Template $tpl  template object
     * @param null|string               $name template function name
     *
     * @return array|bool|mixed
     */
    public function getTplFunction(\Smarty_Internal_Template $tpl, $name = \null)
    {
    }
    /**
     * Add template function to cache file for nocache calls
     *
     * @param Smarty_Internal_Template $tpl
     * @param string                   $_name     template function name
     * @param string                   $_function PHP function name
     *
     * @return bool
     */
    public function addTplFuncToCache(\Smarty_Internal_Template $tpl, $_name, $_function)
    {
    }
    /**
     * Save current template variables on stack
     *
     * @param \Smarty_Internal_Template $tpl
     * @param string                    $name stack name
     */
    public function saveTemplateVariables(\Smarty_Internal_Template $tpl, $name)
    {
    }
    /**
     * Restore saved variables into template objects
     *
     * @param \Smarty_Internal_Template $tpl
     * @param string                    $name stack name
     */
    public function restoreTemplateVariables(\Smarty_Internal_Template $tpl, $name)
    {
    }
}
