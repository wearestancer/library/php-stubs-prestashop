<?php

/**
 * Smarty Resource Data Object
 * Meta Data Container for Template Files
 *
 * @package    Smarty
 * @subpackage TemplateResources
 * @author     Rodney Rehm
 * @property   string $content compiled content
 */
class Smarty_Template_Compiled extends \Smarty_Template_Resource_Base
{
    /**
     * nocache hash
     *
     * @var string|null
     */
    public $nocache_hash = \null;
    /**
     * get a Compiled Object of this source
     *
     * @param Smarty_Internal_Template $_template template object
     *
     * @return Smarty_Template_Compiled compiled object
     */
    public static function load($_template)
    {
    }
    /**
     * populate Compiled Object with compiled filepath
     *
     * @param Smarty_Internal_Template $_template template object
     **/
    public function populateCompiledFilepath(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * render compiled template code
     *
     * @param Smarty_Internal_Template $_template
     *
     * @return void
     * @throws Exception
     */
    public function render(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * load compiled template or compile from source
     *
     * @param Smarty_Internal_Template $_smarty_tpl do not change variable name, is used by compiled template
     *
     * @throws Exception
     */
    public function process(\Smarty_Internal_Template $_smarty_tpl)
    {
    }
    /**
     * compile template from source
     *
     * @param Smarty_Internal_Template $_template
     *
     * @throws Exception
     */
    public function compileTemplateSource(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Write compiled code by handler
     *
     * @param Smarty_Internal_Template $_template template object
     * @param string                   $code      compiled code
     *
     * @return bool success
     * @throws \SmartyException
     */
    public function write(\Smarty_Internal_Template $_template, $code)
    {
    }
    /**
     * Read compiled content from handler
     *
     * @param Smarty_Internal_Template $_template template object
     *
     * @return string content
     */
    public function read(\Smarty_Internal_Template $_template)
    {
    }
}
