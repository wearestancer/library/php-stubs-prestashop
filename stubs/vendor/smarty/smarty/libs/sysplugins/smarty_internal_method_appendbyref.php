<?php

/**
 * Smarty Method AppendByRef
 *
 * Smarty::appendByRef() method
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Method_AppendByRef
{
    /**
     * appends values to template variables by reference
     *
     * @api  Smarty::appendByRef()
     * @link https://www.smarty.net/docs/en/api.append.by.ref.tpl
     *
     * @param \Smarty_Internal_Data|\Smarty_Internal_Template|\Smarty $data
     * @param string                                                  $tpl_var the template variable name
     * @param mixed                                                   &$value  the referenced value to append
     * @param bool                                                    $merge   flag if array elements shall be merged
     *
     * @return \Smarty_Internal_Data|\Smarty_Internal_Template|\Smarty
     */
    public static function appendByRef(\Smarty_Internal_Data $data, $tpl_var, &$value, $merge = \false)
    {
    }
}
