<?php

/**
 * Smarty read include path plugin
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Monte Ohrt
 */
/**
 * Smarty Internal Read Include Path Class
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 */
class Smarty_Internal_Runtime_GetIncludePath
{
    /**
     * include path cache
     *
     * @var string
     */
    public $_include_path = '';
    /**
     * include path directory cache
     *
     * @var array
     */
    public $_include_dirs = array();
    /**
     * include path directory cache
     *
     * @var array
     */
    public $_user_dirs = array();
    /**
     * stream cache
     *
     * @var string[][]
     */
    public $isFile = array();
    /**
     * stream cache
     *
     * @var string[]
     */
    public $isPath = array();
    /**
     * stream cache
     *
     * @var int[]
     */
    public $number = array();
    /**
     * status cache
     *
     * @var bool
     */
    public $_has_stream_include = \null;
    /**
     * Number for array index
     *
     * @var int
     */
    public $counter = 0;
    /**
     * Check if include path was updated
     *
     * @param \Smarty $smarty
     *
     * @return bool
     */
    public function isNewIncludePath(\Smarty $smarty)
    {
    }
    /**
     * return array with include path directories
     *
     * @param \Smarty $smarty
     *
     * @return array
     */
    public function getIncludePathDirs(\Smarty $smarty)
    {
    }
    /**
     * Return full file path from PHP include_path
     *
     * @param string[] $dirs
     * @param string   $file
     * @param \Smarty  $smarty
     *
     * @return bool|string full filepath or false
     */
    public function getIncludePath($dirs, $file, \Smarty $smarty)
    {
    }
}
