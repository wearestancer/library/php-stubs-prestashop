<?php

/**
 * Created by PhpStorm.
 * User: Uwe Tews
 * Date: 04.12.2014
 * Time: 06:08
 */
/**
 * Smarty Resource Data Object
 * Cache Data Container for Template Files
 *
 * @package    Smarty
 * @subpackage TemplateResources
 * @author     Rodney Rehm
 */
class Smarty_Template_Cached extends \Smarty_Template_Resource_Base
{
    /**
     * Cache Is Valid
     *
     * @var boolean
     */
    public $valid = \null;
    /**
     * CacheResource Handler
     *
     * @var Smarty_CacheResource
     */
    public $handler = \null;
    /**
     * Template Cache Id (Smarty_Internal_Template::$cache_id)
     *
     * @var string
     */
    public $cache_id = \null;
    /**
     * saved cache lifetime in seconds
     *
     * @var integer
     */
    public $cache_lifetime = 0;
    /**
     * Id for cache locking
     *
     * @var string
     */
    public $lock_id = \null;
    /**
     * flag that cache is locked by this instance
     *
     * @var bool
     */
    public $is_locked = \false;
    /**
     * Source Object
     *
     * @var Smarty_Template_Source
     */
    public $source = \null;
    /**
     * Nocache hash codes of processed compiled templates
     *
     * @var array
     */
    public $hashes = array();
    /**
     * Flag if this is a cache resource
     *
     * @var bool
     */
    public $isCache = \true;
    /**
     * create Cached Object container
     *
     * @param Smarty_Internal_Template $_template template object
     *
     * @throws \SmartyException
     */
    public function __construct(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * @param Smarty_Internal_Template $_template
     *
     * @return Smarty_Template_Cached
     */
    public static function load(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Render cache template
     *
     * @param \Smarty_Internal_Template $_template
     * @param bool                      $no_output_filter
     *
     * @throws \Exception
     */
    public function render(\Smarty_Internal_Template $_template, $no_output_filter = \true)
    {
    }
    /**
     * Check if cache is valid, lock cache if required
     *
     * @param \Smarty_Internal_Template $_template
     *
     * @return bool flag true if cache is valid
     */
    public function isCached(\Smarty_Internal_Template $_template)
    {
    }
    /**
     * Process cached template
     *
     * @param Smarty_Internal_Template $_template template object
     * @param bool                     $update    flag if called because cache update
     */
    public function process(\Smarty_Internal_Template $_template, $update = \false)
    {
    }
    /**
     * Read cache content from handler
     *
     * @param Smarty_Internal_Template $_template template object
     *
     * @return string|false content
     */
    public function read(\Smarty_Internal_Template $_template)
    {
    }
}
