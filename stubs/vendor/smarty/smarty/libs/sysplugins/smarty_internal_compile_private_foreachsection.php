<?php

/**
 * Smarty Internal Plugin Compile ForeachSection
 * Shared methods for {foreach} {section} tags
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile ForeachSection Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Private_ForeachSection extends \Smarty_Internal_CompileBase
{
    /**
     * Name of this tag
     *
     * @var string
     */
    public $tagName = '';
    /**
     * Valid properties of $smarty.xxx variable
     *
     * @var array
     */
    public $nameProperties = array();
    /**
     * {section} tag has no item properties
     *
     * @var array
     */
    public $itemProperties = \null;
    /**
     * {section} tag has always name attribute
     *
     * @var bool
     */
    public $isNamed = \true;
    /**
     * @var array
     */
    public $matchResults = array();
    /**
     * Scan sources for used tag attributes
     *
     * @param array                                 $attributes
     * @param \Smarty_Internal_TemplateCompilerBase $compiler
     *
     * @throws \SmartyException
     */
    public function scanForProperties($attributes, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
    /**
     * Build property preg string
     *
     * @param bool  $named
     * @param array $attributes
     */
    public function buildPropertyPreg($named, $attributes)
    {
    }
    /**
     * Find matches in source string
     *
     * @param string $source
     */
    public function matchProperty($source)
    {
    }
    /**
     * Find matches in template source
     *
     * @param \Smarty_Internal_TemplateCompilerBase $compiler
     */
    public function matchTemplateSource(\Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
    /**
     * Find matches in all parent template source
     *
     * @param \Smarty_Internal_TemplateCompilerBase $compiler
     *
     * @throws \SmartyException
     */
    public function matchParentTemplateSource(\Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
    /**
     * Find matches in {block} tag source
     *
     * @param \Smarty_Internal_TemplateCompilerBase $compiler
     */
    public function matchBlockSource(\Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
    /**
     * Compiles code for the {$smarty.foreach.xxx} or {$smarty.section.xxx}tag
     *
     * @param array                                 $args      array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler  compiler object
     * @param array                                 $parameter array with compilation parameter
     *
     * @return string compiled code
     * @throws \SmartyCompilerException
     */
    public function compileSpecialVariable($args, \Smarty_Internal_TemplateCompilerBase $compiler, $parameter)
    {
    }
}
