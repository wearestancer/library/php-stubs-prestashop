<?php

/**
 * Smarty Internal Plugin Compile Function
 * Compiles the {function} {/function} tags
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile Function Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Function extends \Smarty_Internal_CompileBase
{
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $required_attributes = array('name');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $shorttag_order = array('name');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $optional_attributes = array('_any');
    /**
     * Compiles code for the {function} tag
     *
     * @param array                                 $args     array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler compiler object
     *
     * @return bool true
     * @throws \SmartyCompilerException
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
}
/**
 * Smarty Internal Plugin Compile Functionclose Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Functionclose extends \Smarty_Internal_CompileBase
{
    /**
     * Compiles code for the {/function} tag
     *
     * @param array                                        $args     array with attributes from parser
     * @param object|\Smarty_Internal_TemplateCompilerBase $compiler compiler object
     *
     * @return bool true
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
    /**
     * Remove nocache code
     *
     * @param $match
     *
     * @return string
     */
    public function removeNocache($match)
    {
    }
}
