<?php

/**
 * Smarty Internal Plugin Debug
 * Class to collect data for the Smarty Debugging Console
 *
 * @package    Smarty
 * @subpackage Debug
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Debug Class
 *
 * @package    Smarty
 * @subpackage Debug
 */
class Smarty_Internal_Debug extends \Smarty_Internal_Data
{
    /**
     * template data
     *
     * @var array
     */
    public $template_data = array();
    /**
     * List of uid's which shall be ignored
     *
     * @var array
     */
    public $ignore_uid = array();
    /**
     * Index of display() and fetch() calls
     *
     * @var int
     */
    public $index = 0;
    /**
     * Counter for window offset
     *
     * @var int
     */
    public $offset = 0;
    /**
     * Start logging template
     *
     * @param \Smarty_Internal_Template $template template
     * @param null                      $mode     true: display   false: fetch  null: subtemplate
     */
    public function start_template(\Smarty_Internal_Template $template, $mode = \null)
    {
    }
    /**
     * End logging of cache time
     *
     * @param \Smarty_Internal_Template $template cached template
     */
    public function end_template(\Smarty_Internal_Template $template)
    {
    }
    /**
     * Start logging of compile time
     *
     * @param \Smarty_Internal_Template $template
     */
    public function start_compile(\Smarty_Internal_Template $template)
    {
    }
    /**
     * End logging of compile time
     *
     * @param \Smarty_Internal_Template $template
     */
    public function end_compile(\Smarty_Internal_Template $template)
    {
    }
    /**
     * Start logging of render time
     *
     * @param \Smarty_Internal_Template $template
     */
    public function start_render(\Smarty_Internal_Template $template)
    {
    }
    /**
     * End logging of compile time
     *
     * @param \Smarty_Internal_Template $template
     */
    public function end_render(\Smarty_Internal_Template $template)
    {
    }
    /**
     * Start logging of cache time
     *
     * @param \Smarty_Internal_Template $template cached template
     */
    public function start_cache(\Smarty_Internal_Template $template)
    {
    }
    /**
     * End logging of cache time
     *
     * @param \Smarty_Internal_Template $template cached template
     */
    public function end_cache(\Smarty_Internal_Template $template)
    {
    }
    /**
     * Register template object
     *
     * @param \Smarty_Internal_Template $template cached template
     */
    public function register_template(\Smarty_Internal_Template $template)
    {
    }
    /**
     * Register data object
     *
     * @param \Smarty_Data $data data object
     */
    public static function register_data(\Smarty_Data $data)
    {
    }
    /**
     * Opens a window for the Smarty Debugging Console and display the data
     *
     * @param Smarty_Internal_Template|Smarty $obj object to debug
     * @param bool                            $full
     *
     * @throws \Exception
     * @throws \SmartyException
     */
    public function display_debug($obj, $full = \false)
    {
    }
    /**
     * Recursively gets variables from all template/data scopes
     *
     * @param Smarty_Internal_Template|Smarty_Data $obj object to debug
     *
     * @return StdClass
     */
    public function get_debug_vars($obj)
    {
    }
    /**
     * Ignore template
     *
     * @param \Smarty_Internal_Template $template
     */
    public function ignore(\Smarty_Internal_Template $template)
    {
    }
    /**
     * handle 'URL' debugging mode
     *
     * @param Smarty $smarty
     */
    public function debugUrl(\Smarty $smarty)
    {
    }
}
