<?php

/**
 * Smarty Extension handler
 *
 * Load extensions dynamically
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 *
 * Runtime extensions
 * @property   Smarty_Internal_Runtime_CacheModify       $_cacheModify
 * @property   Smarty_Internal_Runtime_CacheResourceFile $_cacheResourceFile
 * @property   Smarty_Internal_Runtime_Capture           $_capture
 * @property   Smarty_Internal_Runtime_CodeFrame         $_codeFrame
 * @property   Smarty_Internal_Runtime_FilterHandler     $_filterHandler
 * @property   Smarty_Internal_Runtime_Foreach           $_foreach
 * @property   Smarty_Internal_Runtime_GetIncludePath    $_getIncludePath
 * @property   Smarty_Internal_Runtime_Make_Nocache      $_make_nocache
 * @property   Smarty_Internal_Runtime_UpdateCache       $_updateCache
 * @property   Smarty_Internal_Runtime_UpdateScope       $_updateScope
 * @property   Smarty_Internal_Runtime_TplFunction       $_tplFunction
 * @property   Smarty_Internal_Runtime_WriteFile         $_writeFile
 *
 * Method extensions
 * @property   Smarty_Internal_Method_GetTemplateVars    $getTemplateVars
 * @property   Smarty_Internal_Method_Append             $append
 * @property   Smarty_Internal_Method_AppendByRef        $appendByRef
 * @property   Smarty_Internal_Method_AssignGlobal       $assignGlobal
 * @property   Smarty_Internal_Method_AssignByRef        $assignByRef
 * @property   Smarty_Internal_Method_LoadFilter         $loadFilter
 * @property   Smarty_Internal_Method_LoadPlugin         $loadPlugin
 * @property   Smarty_Internal_Method_RegisterFilter     $registerFilter
 * @property   Smarty_Internal_Method_RegisterObject     $registerObject
 * @property   Smarty_Internal_Method_RegisterPlugin     $registerPlugin
 * @property   mixed|\Smarty_Template_Cached             configLoad
 */
#[\AllowDynamicProperties]
class Smarty_Internal_Extension_Handler
{
    public $objType = \null;
    /**
     * Call external Method
     *
     * @param \Smarty_Internal_Data $data
     * @param string                $name external method names
     * @param array                 $args argument array
     *
     * @return mixed
     */
    public function _callExternalMethod(\Smarty_Internal_Data $data, $name, $args)
    {
    }
    /**
     * Make first character of name parts upper case
     *
     * @param string $name
     *
     * @return string
     */
    public function upperCase($name)
    {
    }
    /**
     * get extension object
     *
     * @param string $property_name property name
     *
     * @return mixed|Smarty_Template_Cached
     */
    public function __get($property_name)
    {
    }
    /**
     * set extension property
     *
     * @param string $property_name property name
     * @param mixed  $value         value
     *
     */
    public function __set($property_name, $value)
    {
    }
    /**
     * Call error handler for undefined method
     *
     * @param string $name unknown method-name
     * @param array  $args argument array
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
    }
}
