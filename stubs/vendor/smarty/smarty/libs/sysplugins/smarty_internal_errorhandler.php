<?php

/**
 * Smarty error handler to fix new error levels in PHP8 for backwards compatibility
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Simon Wisselink
 *
 */
class Smarty_Internal_ErrorHandler
{
    /**
     * Allows {$foo} where foo is unset.
     * @var bool
     */
    public $allowUndefinedVars = \true;
    /**
     * Allows {$foo->propName} where propName is undefined.
     * @var bool
     */
    public $allowUndefinedProperties = \true;
    /**
     * Allows {$foo.bar} where bar is unset and {$foo.bar1.bar2} where either bar1 or bar2 is unset.
     * @var bool
     */
    public $allowUndefinedArrayKeys = \true;
    /**
     * Allows {$foo->bar} where bar is not an object (e.g. null or false).
     * @var bool
     */
    public $allowDereferencingNonObjects = \true;
    /**
     * Enable error handler to intercept errors
     */
    public function activate()
    {
    }
    /**
     * Disable error handler
     */
    public function deactivate()
    {
    }
    /**
     * Error Handler to mute expected messages
     *
     * @link https://php.net/set_error_handler
     *
     * @param integer $errno Error level
     * @param         $errstr
     * @param         $errfile
     * @param         $errline
     * @param         $errcontext
     *
     * @return bool
     */
    public function handleError($errno, $errstr, $errfile, $errline, $errcontext = [])
    {
    }
}
