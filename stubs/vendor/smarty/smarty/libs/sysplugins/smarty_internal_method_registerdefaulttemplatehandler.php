<?php

/**
 * Smarty Method RegisterDefaultTemplateHandler
 *
 * Smarty::registerDefaultTemplateHandler() method
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Method_RegisterDefaultTemplateHandler
{
    /**
     * Valid for Smarty and template object
     *
     * @var int
     */
    public $objMap = 3;
    /**
     * Register template default handler
     *
     * @api Smarty::registerDefaultTemplateHandler()
     *
     * @param \Smarty_Internal_TemplateBase|\Smarty_Internal_Template|\Smarty $obj
     * @param callable                                                        $callback class/method name
     *
     * @return \Smarty|\Smarty_Internal_Template
     * @throws SmartyException              if $callback is not callable
     */
    public function registerDefaultTemplateHandler(\Smarty_Internal_TemplateBase $obj, $callback)
    {
    }
    /**
     * get default content from template or config resource handler
     *
     * @param Smarty_Template_Source $source
     *
     * @throws \SmartyException
     */
    public static function _getDefaultTemplate(\Smarty_Template_Source $source)
    {
    }
}
