<?php

/**
 * Smarty Internal Plugin Compile Append
 * Compiles the {append} tag
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile Append Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Append extends \Smarty_Internal_Compile_Assign
{
    /**
     * Compiles code for the {append} tag
     *
     * @param array                                 $args      array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler  compiler object
     * @param array                                 $parameter array with compilation parameter
     *
     * @return string compiled code
     * @throws \SmartyCompilerException
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler, $parameter)
    {
    }
}
