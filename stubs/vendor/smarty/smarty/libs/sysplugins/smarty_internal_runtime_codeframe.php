<?php

/**
 * Smarty Internal Extension
 * This file contains the Smarty template extension to create a code frame
 *
 * @package    Smarty
 * @subpackage Template
 * @author     Uwe Tews
 */
/**
 * Class Smarty_Internal_Extension_CodeFrame
 * Create code frame for compiled and cached templates
 */
class Smarty_Internal_Runtime_CodeFrame
{
    /**
     * Create code frame for compiled and cached templates
     *
     * @param Smarty_Internal_Template              $_template
     * @param string                                $content   optional template content
     * @param string                                $functions compiled template function and block code
     * @param bool                                  $cache     flag for cache file
     * @param \Smarty_Internal_TemplateCompilerBase $compiler
     *
     * @return string
     */
    public function create(\Smarty_Internal_Template $_template, $content = '', $functions = '', $cache = \false, \Smarty_Internal_TemplateCompilerBase $compiler = \null)
    {
    }
}
