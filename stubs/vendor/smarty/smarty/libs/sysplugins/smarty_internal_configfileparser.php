<?php

class TPC_yyStackEntry
{
    public $stateno;
    /* The state-number */
    public $major;
    /* The major token value.  This is the code
     ** number for the token at this stack level */
    public $minor;
    /* The user-supplied minor token value.  This
     ** is the value of the token  */
}
// line 12 "../smarty/lexer/smarty_internal_configfileparser.y"
/**
 * Smarty Internal Plugin Configfileparse
 *
 * This is the config file parser.
 * It is generated from the smarty_internal_configfileparser.y file
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
class Smarty_Internal_Configfileparser
{
    // line 25 "../smarty/lexer/smarty_internal_configfileparser.y"
    const TPC_OPENB = 1;
    const TPC_SECTION = 2;
    const TPC_CLOSEB = 3;
    const TPC_DOT = 4;
    const TPC_ID = 5;
    const TPC_EQUAL = 6;
    const TPC_FLOAT = 7;
    const TPC_INT = 8;
    const TPC_BOOL = 9;
    const TPC_SINGLE_QUOTED_STRING = 10;
    const TPC_DOUBLE_QUOTED_STRING = 11;
    const TPC_TRIPPLE_QUOTES = 12;
    const TPC_TRIPPLE_TEXT = 13;
    const TPC_TRIPPLE_QUOTES_END = 14;
    const TPC_NAKED_STRING = 15;
    const TPC_OTHER = 16;
    const TPC_NEWLINE = 17;
    const TPC_COMMENTSTART = 18;
    const YY_NO_ACTION = 60;
    const YY_ACCEPT_ACTION = 59;
    const YY_ERROR_ACTION = 58;
    const YY_SZ_ACTTAB = 38;
    const YY_SHIFT_USE_DFLT = -8;
    const YY_SHIFT_MAX = 19;
    const YY_REDUCE_USE_DFLT = -17;
    const YY_REDUCE_MAX = 10;
    const YYNOCODE = 29;
    const YYSTACKDEPTH = 100;
    const YYNSTATE = 36;
    const YYNRULE = 22;
    const YYERRORSYMBOL = 19;
    const YYERRSYMDT = 'yy0';
    const YYFALLBACK = 0;
    public static $yy_action = array(32, 31, 30, 29, 35, 13, 19, 3, 24, 26, 59, 9, 14, 1, 16, 25, 11, 28, 25, 11, 17, 27, 34, 20, 18, 15, 23, 5, 6, 22, 10, 8, 4, 12, 2, 33, 7, 21);
    public static $yy_lookahead = array(7, 8, 9, 10, 11, 12, 5, 23, 15, 16, 20, 21, 2, 23, 4, 17, 18, 14, 17, 18, 13, 14, 25, 26, 15, 2, 17, 3, 3, 17, 25, 25, 6, 1, 23, 27, 22, 24);
    public static $yy_shift_ofst = array(-8, 1, 1, 1, -7, -2, -2, 32, -8, -8, -8, 9, 10, 7, 25, 24, 23, 3, 12, 26);
    public static $yy_reduce_ofst = array(-10, -3, -3, -3, 8, 6, 5, 13, 11, 14, -16);
    public static $yyExpectedTokens = array(array(), array(5, 17, 18), array(5, 17, 18), array(5, 17, 18), array(7, 8, 9, 10, 11, 12, 15, 16), array(17, 18), array(17, 18), array(1), array(), array(), array(), array(15, 17), array(2, 4), array(13, 14), array(3), array(3), array(2), array(14), array(17), array(6), array(), array(), array(), array(), array(), array(), array(), array(), array(), array(), array(), array(), array(), array(), array(), array());
    public static $yy_default = array(44, 37, 41, 40, 58, 58, 58, 36, 44, 39, 44, 58, 58, 58, 58, 58, 58, 58, 58, 58, 43, 38, 57, 56, 53, 55, 54, 52, 51, 49, 48, 47, 46, 45, 42, 50);
    public static $yyFallback = array();
    public static $yyRuleName = array('start ::= global_vars sections', 'global_vars ::= var_list', 'sections ::= sections section', 'sections ::=', 'section ::= OPENB SECTION CLOSEB newline var_list', 'section ::= OPENB DOT SECTION CLOSEB newline var_list', 'var_list ::= var_list newline', 'var_list ::= var_list var', 'var_list ::=', 'var ::= ID EQUAL value', 'value ::= FLOAT', 'value ::= INT', 'value ::= BOOL', 'value ::= SINGLE_QUOTED_STRING', 'value ::= DOUBLE_QUOTED_STRING', 'value ::= TRIPPLE_QUOTES TRIPPLE_TEXT TRIPPLE_QUOTES_END', 'value ::= TRIPPLE_QUOTES TRIPPLE_QUOTES_END', 'value ::= NAKED_STRING', 'value ::= OTHER', 'newline ::= NEWLINE', 'newline ::= COMMENTSTART NEWLINE', 'newline ::= COMMENTSTART NAKED_STRING NEWLINE');
    public static $yyRuleInfo = array(array(0 => 20, 1 => 2), array(0 => 21, 1 => 1), array(0 => 22, 1 => 2), array(0 => 22, 1 => 0), array(0 => 24, 1 => 5), array(0 => 24, 1 => 6), array(0 => 23, 1 => 2), array(0 => 23, 1 => 2), array(0 => 23, 1 => 0), array(0 => 26, 1 => 3), array(0 => 27, 1 => 1), array(0 => 27, 1 => 1), array(0 => 27, 1 => 1), array(0 => 27, 1 => 1), array(0 => 27, 1 => 1), array(0 => 27, 1 => 3), array(0 => 27, 1 => 2), array(0 => 27, 1 => 1), array(0 => 27, 1 => 1), array(0 => 25, 1 => 1), array(0 => 25, 1 => 2), array(0 => 25, 1 => 3));
    public static $yyReduceMap = array(0 => 0, 2 => 0, 3 => 0, 19 => 0, 20 => 0, 21 => 0, 1 => 1, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 17);
    /**
     * result status
     *
     * @var bool
     */
    public $successful = \true;
    /**
     * return value
     *
     * @var mixed
     */
    public $retvalue = 0;
    /**
     * @var
     */
    public $yymajor;
    /**
     * compiler object
     *
     * @var Smarty_Internal_Config_File_Compiler
     */
    public $compiler = \null;
    /**
     * smarty object
     *
     * @var Smarty
     */
    public $smarty = \null;
    public $yyTraceFILE;
    public $yyTracePrompt;
    public $yyidx;
    public $yyerrcnt;
    public $yystack = array();
    public $yyTokenName = array('$', 'OPENB', 'SECTION', 'CLOSEB', 'DOT', 'ID', 'EQUAL', 'FLOAT', 'INT', 'BOOL', 'SINGLE_QUOTED_STRING', 'DOUBLE_QUOTED_STRING', 'TRIPPLE_QUOTES', 'TRIPPLE_TEXT', 'TRIPPLE_QUOTES_END', 'NAKED_STRING', 'OTHER', 'NEWLINE', 'COMMENTSTART', 'error', 'start', 'global_vars', 'sections', 'var_list', 'section', 'newline', 'var', 'value');
    /**
     * constructor
     *
     * @param Smarty_Internal_Configfilelexer      $lex
     * @param Smarty_Internal_Config_File_Compiler $compiler
     */
    public function __construct(\Smarty_Internal_Configfilelexer $lex, \Smarty_Internal_Config_File_Compiler $compiler)
    {
    }
    public static function yy_destructor($yymajor, $yypminor)
    {
    }
    /* The parser's stack */
    public function Trace($TraceFILE, $zTracePrompt)
    {
    }
    public function PrintTrace()
    {
    }
    public function tokenName($tokenType)
    {
    }
    public function yy_pop_parser_stack()
    {
    }
    public function __destruct()
    {
    }
    public function yy_get_expected_tokens($token)
    {
    }
    public function yy_is_expected_token($token)
    {
    }
    public function yy_find_shift_action($iLookAhead)
    {
    }
    public function yy_find_reduce_action($stateno, $iLookAhead)
    {
    }
    public function yy_shift($yyNewState, $yyMajor, $yypMinor)
    {
    }
    public function yy_r0()
    {
    }
    public function yy_r1()
    {
    }
    public function yy_r4()
    {
    }
    // line 245 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r5()
    {
    }
    // line 250 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r6()
    {
    }
    // line 264 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r7()
    {
    }
    // line 269 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r8()
    {
    }
    // line 277 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r9()
    {
    }
    // line 281 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r10()
    {
    }
    // line 285 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r11()
    {
    }
    // line 291 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r12()
    {
    }
    // line 296 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r13()
    {
    }
    // line 300 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r14()
    {
    }
    // line 304 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r15()
    {
    }
    // line 308 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r16()
    {
    }
    // line 312 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_r17()
    {
    }
    // line 316 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_reduce($yyruleno)
    {
    }
    // line 320 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_parse_failed()
    {
    }
    // line 324 "../smarty/lexer/smarty_internal_configfileparser.y"
    public function yy_syntax_error($yymajor, $TOKEN)
    {
    }
    public function yy_accept()
    {
    }
    public function doParse($yymajor, $yytokenvalue)
    {
    }
}
