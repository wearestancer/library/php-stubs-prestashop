<?php

/**
 * Smarty Method RegisterFilter
 *
 * Smarty::registerFilter() method
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Method_RegisterFilter
{
    /**
     * Valid for Smarty and template object
     *
     * @var int
     */
    public $objMap = 3;
    /**
     * Registers a filter function
     *
     * @api  Smarty::registerFilter()
     *
     * @link https://www.smarty.net/docs/en/api.register.filter.tpl
     *
     * @param \Smarty_Internal_TemplateBase|\Smarty_Internal_Template|\Smarty $obj
     * @param string                                                          $type filter type
     * @param callback                                                        $callback
     * @param string|null                                                     $name optional filter name
     *
     * @return \Smarty|\Smarty_Internal_Template
     * @throws \SmartyException
     */
    public function registerFilter(\Smarty_Internal_TemplateBase $obj, $type, $callback, $name = \null)
    {
    }
    /**
     * Return internal filter name
     *
     * @param callback $function_name
     *
     * @return string   internal filter name
     */
    public function _getFilterName($function_name)
    {
    }
    /**
     * Check if filter type is valid
     *
     * @param string $type
     *
     * @throws \SmartyException
     */
    public function _checkFilterType($type)
    {
    }
}
