<?php

/**
 * Smarty Internal Plugin Compile Section
 * Compiles the {section} {sectionelse} {/section} tags
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile Section Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Section extends \Smarty_Internal_Compile_Private_ForeachSection
{
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $required_attributes = array('name', 'loop');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $shorttag_order = array('name', 'loop');
    /**
     * Attribute definition: Overwrites base class.
     *
     * @var array
     * @see Smarty_Internal_CompileBase
     */
    public $optional_attributes = array('start', 'step', 'max', 'show', 'properties');
    /**
     * counter
     *
     * @var int
     */
    public $counter = 0;
    /**
     * Name of this tag
     *
     * @var string
     */
    public $tagName = 'section';
    /**
     * Valid properties of $smarty.section.name.xxx variable
     *
     * @var array
     */
    public $nameProperties = array('first', 'last', 'index', 'iteration', 'show', 'total', 'rownum', 'index_prev', 'index_next', 'loop');
    /**
     * {section} tag has no item properties
     *
     * @var array
     */
    public $itemProperties = \null;
    /**
     * {section} tag has always name attribute
     *
     * @var bool
     */
    public $isNamed = \true;
    /**
     * Compiles code for the {section} tag
     *
     * @param array                                 $args     array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler compiler object
     *
     * @return string compiled code
     * @throws \SmartyCompilerException
     * @throws \SmartyException
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
}
/**
 * Smarty Internal Plugin Compile Sectionelse Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Sectionelse extends \Smarty_Internal_CompileBase
{
    /**
     * Compiles code for the {sectionelse} tag
     *
     * @param array                                 $args     array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler compiler object
     *
     * @return string compiled code
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
}
/**
 * Smarty Internal Plugin Compile Sectionclose Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Sectionclose extends \Smarty_Internal_CompileBase
{
    /**
     * Compiles code for the {/section} tag
     *
     * @param array                                 $args     array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler compiler object
     *
     * @return string compiled code
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler)
    {
    }
}
