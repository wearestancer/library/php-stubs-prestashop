<?php

/**
 * Smarty Internal Plugin Templateparser Parse Tree
 * These are classes to build parse tree in the template parser
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Thue Kristensen
 * @author     Uwe Tews
 */
/**
 * Template element
 *
 * @package    Smarty
 * @subpackage Compiler
 * @ignore
 */
class Smarty_Internal_ParseTree_Template extends \Smarty_Internal_ParseTree
{
    /**
     * Array of template elements
     *
     * @var array
     */
    public $subtrees = array();
    /**
     * Create root of parse tree for template elements
     */
    public function __construct()
    {
    }
    /**
     * Append buffer to subtree
     *
     * @param \Smarty_Internal_Templateparser $parser
     * @param Smarty_Internal_ParseTree       $subtree
     */
    public function append_subtree(\Smarty_Internal_Templateparser $parser, \Smarty_Internal_ParseTree $subtree)
    {
    }
    /**
     * Append array to subtree
     *
     * @param \Smarty_Internal_Templateparser $parser
     * @param \Smarty_Internal_ParseTree[]    $array
     */
    public function append_array(\Smarty_Internal_Templateparser $parser, $array = array())
    {
    }
    /**
     * Prepend array to subtree
     *
     * @param \Smarty_Internal_Templateparser $parser
     * @param \Smarty_Internal_ParseTree[]    $array
     */
    public function prepend_array(\Smarty_Internal_Templateparser $parser, $array = array())
    {
    }
    /**
     * Sanitize and merge subtree buffers together
     *
     * @param \Smarty_Internal_Templateparser $parser
     *
     * @return string template code content
     */
    public function to_smarty_php(\Smarty_Internal_Templateparser $parser)
    {
    }
}
