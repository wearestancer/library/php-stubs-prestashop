<?php

/**
 * Smarty Internal Plugin Compile Registered Block
 * Compiles code for the execution of a registered block function
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile Registered Block Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Private_Registered_Block extends \Smarty_Internal_Compile_Private_Block_Plugin
{
    /**
     * Setup callback, parameter array and nocache mode
     *
     * @param \Smarty_Internal_TemplateCompilerBase $compiler
     * @param array                                 $_attr attributes
     * @param string                                $tag
     * @param null                                  $function
     *
     * @return array
     */
    public function setup(\Smarty_Internal_TemplateCompilerBase $compiler, $_attr, $tag, $function)
    {
    }
}
