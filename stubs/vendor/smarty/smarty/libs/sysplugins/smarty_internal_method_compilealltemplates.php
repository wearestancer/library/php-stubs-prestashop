<?php

/**
 * Smarty Method CompileAllTemplates
 *
 * Smarty::compileAllTemplates() method
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Method_CompileAllTemplates
{
    /**
     * Valid for Smarty object
     *
     * @var int
     */
    public $objMap = 1;
    /**
     * Compile all template files
     *
     * @api Smarty::compileAllTemplates()
     *
     * @param \Smarty $smarty        passed smarty object
     * @param string  $extension     file extension
     * @param bool    $force_compile force all to recompile
     * @param int     $time_limit
     * @param int     $max_errors
     *
     * @return integer number of template files recompiled
     */
    public function compileAllTemplates(\Smarty $smarty, $extension = '.tpl', $force_compile = \false, $time_limit = 0, $max_errors = \null)
    {
    }
    /**
     * Compile all template or config files
     *
     * @param \Smarty $smarty
     * @param string  $extension     template file name extension
     * @param bool    $force_compile force all to recompile
     * @param int     $time_limit    set maximum execution time
     * @param int     $max_errors    set maximum allowed errors
     * @param bool    $isConfig      flag true if called for config files
     *
     * @return int number of template files compiled
     */
    protected function compileAll(\Smarty $smarty, $extension, $force_compile, $time_limit, $max_errors, $isConfig = \false)
    {
    }
}
