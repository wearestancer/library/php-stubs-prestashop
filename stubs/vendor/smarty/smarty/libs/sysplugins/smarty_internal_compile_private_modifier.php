<?php

/**
 * Smarty Internal Plugin Compile Modifier
 * Compiles code for modifier execution
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Smarty Internal Plugin Compile Modifier Class
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_Compile_Private_Modifier extends \Smarty_Internal_CompileBase
{
    /**
     * Compiles code for modifier execution
     *
     * @param array                                 $args      array with attributes from parser
     * @param \Smarty_Internal_TemplateCompilerBase $compiler  compiler object
     * @param array                                 $parameter array with compilation parameter
     *
     * @return string compiled code
     * @throws \SmartyCompilerException
     * @throws \SmartyException
     */
    public function compile($args, \Smarty_Internal_TemplateCompilerBase $compiler, $parameter)
    {
    }
}
