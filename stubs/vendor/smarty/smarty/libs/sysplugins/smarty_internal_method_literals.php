<?php

/**
 * Smarty Method GetLiterals
 *
 * Smarty::getLiterals() method
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Method_Literals
{
    /**
     * Valid for Smarty and template object
     *
     * @var int
     */
    public $objMap = 3;
    /**
     * Get literals
     *
     * @api Smarty::getLiterals()
     *
     * @param \Smarty_Internal_TemplateBase|\Smarty_Internal_Template|\Smarty $obj
     *
     * @return array list of literals
     */
    public function getLiterals(\Smarty_Internal_TemplateBase $obj)
    {
    }
    /**
     * Add literals
     *
     * @api Smarty::addLiterals()
     *
     * @param \Smarty_Internal_TemplateBase|\Smarty_Internal_Template|\Smarty $obj
     * @param array|string                                                    $literals literal or list of literals
     *                                                                                  to addto add
     *
     * @return \Smarty|\Smarty_Internal_Template
     * @throws \SmartyException
     */
    public function addLiterals(\Smarty_Internal_TemplateBase $obj, $literals = \null)
    {
    }
    /**
     * Set literals
     *
     * @api Smarty::setLiterals()
     *
     * @param \Smarty_Internal_TemplateBase|\Smarty_Internal_Template|\Smarty $obj
     * @param array|string                                                    $literals literal or list of literals
     *                                                                                  to setto set
     *
     * @return \Smarty|\Smarty_Internal_Template
     * @throws \SmartyException
     */
    public function setLiterals(\Smarty_Internal_TemplateBase $obj, $literals = \null)
    {
    }
}
