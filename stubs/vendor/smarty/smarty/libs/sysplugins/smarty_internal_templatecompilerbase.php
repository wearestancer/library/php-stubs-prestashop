<?php

/**
 * Smarty Internal Plugin Smarty Template Compiler Base
 * This file contains the basic classes and methods for compiling Smarty templates with lexer/parser
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Main abstract compiler class
 *
 * @package    Smarty
 * @subpackage Compiler
 *
 * @property Smarty_Internal_SmartyTemplateCompiler $prefixCompiledCode  = ''
 * @property Smarty_Internal_SmartyTemplateCompiler $postfixCompiledCode = ''
 * @method   registerPostCompileCallback($callback, $parameter = array(), $key = null, $replace = false)
 * @method   unregisterPostCompileCallback($key)
 */
abstract class Smarty_Internal_TemplateCompilerBase
{
    /**
     * compile tag objects cache
     *
     * @var array
     */
    public static $_tag_objects = array();
    /**
     * counter for prefix variable number
     *
     * @var int
     */
    public static $prefixVariableNumber = 0;
    /**
     * Smarty object
     *
     * @var Smarty
     */
    public $smarty = \null;
    /**
     * Parser object
     *
     * @var Smarty_Internal_Templateparser
     */
    public $parser = \null;
    /**
     * hash for nocache sections
     *
     * @var mixed
     */
    public $nocache_hash = \null;
    /**
     * suppress generation of nocache code
     *
     * @var bool
     */
    public $suppressNocacheProcessing = \false;
    /**
     * caching enabled (copied from template object)
     *
     * @var int
     */
    public $caching = 0;
    /**
     * tag stack
     *
     * @var array
     */
    public $_tag_stack = array();
    /**
     * tag stack count
     *
     * @var array
     */
    public $_tag_stack_count = array();
    /**
     * Plugins used by template
     *
     * @var array
     */
    public $required_plugins = array('compiled' => array(), 'nocache' => array());
    /**
     * Required plugins stack
     *
     * @var array
     */
    public $required_plugins_stack = array();
    /**
     * current template
     *
     * @var Smarty_Internal_Template
     */
    public $template = \null;
    /**
     * merged included sub template data
     *
     * @var array
     */
    public $mergedSubTemplatesData = array();
    /**
     * merged sub template code
     *
     * @var array
     */
    public $mergedSubTemplatesCode = array();
    /**
     * collected template properties during compilation
     *
     * @var array
     */
    public $templateProperties = array();
    /**
     * source line offset for error messages
     *
     * @var int
     */
    public $trace_line_offset = 0;
    /**
     * trace uid
     *
     * @var string
     */
    public $trace_uid = '';
    /**
     * trace file path
     *
     * @var string
     */
    public $trace_filepath = '';
    /**
     * stack for tracing file and line of nested {block} tags
     *
     * @var array
     */
    public $trace_stack = array();
    /**
     * plugins loaded by default plugin handler
     *
     * @var array
     */
    public $default_handler_plugins = array();
    /**
     * saved preprocessed modifier list
     *
     * @var mixed
     */
    public $default_modifier_list = \null;
    /**
     * force compilation of complete template as nocache
     *
     * @var boolean
     */
    public $forceNocache = \false;
    /**
     * flag if compiled template file shall we written
     *
     * @var bool
     */
    public $write_compiled_code = \true;
    /**
     * Template functions
     *
     * @var array
     */
    public $tpl_function = array();
    /**
     * called sub functions from template function
     *
     * @var array
     */
    public $called_functions = array();
    /**
     * compiled template or block function code
     *
     * @var string
     */
    public $blockOrFunctionCode = '';
    /**
     * flags for used modifier plugins
     *
     * @var array
     */
    public $modifier_plugins = array();
    /**
     * type of already compiled modifier
     *
     * @var array
     */
    public $known_modifier_type = array();
    /**
     * parent compiler object for merged subtemplates and template functions
     *
     * @var Smarty_Internal_TemplateCompilerBase
     */
    public $parent_compiler = \null;
    /**
     * Flag true when compiling nocache section
     *
     * @var bool
     */
    public $nocache = \false;
    /**
     * Flag true when tag is compiled as nocache
     *
     * @var bool
     */
    public $tag_nocache = \false;
    /**
     * Compiled tag prefix code
     *
     * @var array
     */
    public $prefix_code = array();
    /**
     * used prefix variables by current compiled tag
     *
     * @var array
     */
    public $usedPrefixVariables = array();
    /**
     * Prefix code  stack
     *
     * @var array
     */
    public $prefixCodeStack = array();
    /**
     * Tag has compiled code
     *
     * @var bool
     */
    public $has_code = \false;
    /**
     * A variable string was compiled
     *
     * @var bool
     */
    public $has_variable_string = \false;
    /**
     * Stack for {setfilter} {/setfilter}
     *
     * @var array
     */
    public $variable_filter_stack = array();
    /**
     * variable filters for {setfilter} {/setfilter}
     *
     * @var array
     */
    public $variable_filters = array();
    /**
     * Nesting count of looping tags like {foreach}, {for}, {section}, {while}
     *
     * @var int
     */
    public $loopNesting = 0;
    /**
     * Strip preg pattern
     *
     * @var string
     */
    public $stripRegEx = '![\\t ]*[\\r\\n]+[\\t ]*!';
    /**
     * plugin search order
     *
     * @var array
     */
    public $plugin_search_order = array('function', 'block', 'compiler', 'class');
    /**
     * General storage area for tag compiler plugins
     *
     * @var array
     */
    public $_cache = array();
    /**
     * Initialize compiler
     *
     * @param Smarty $smarty global instance
     */
    public function __construct(\Smarty $smarty)
    {
    }
    /**
     * Method to compile a Smarty template
     *
     * @param Smarty_Internal_Template                  $template template object to compile
     * @param bool                                      $nocache  true is shall be compiled in nocache mode
     * @param null|Smarty_Internal_TemplateCompilerBase $parent_compiler
     *
     * @return bool true if compiling succeeded, false if it failed
     * @throws \Exception
     */
    public function compileTemplate(\Smarty_Internal_Template $template, $nocache = \null, \Smarty_Internal_TemplateCompilerBase $parent_compiler = \null)
    {
    }
    /**
     * Compile template source and run optional post filter
     *
     * @param \Smarty_Internal_Template             $template
     * @param null|bool                             $nocache flag if template must be compiled in nocache mode
     * @param \Smarty_Internal_TemplateCompilerBase $parent_compiler
     *
     * @return string
     * @throws \Exception
     */
    public function compileTemplateSource(\Smarty_Internal_Template $template, $nocache = \null, \Smarty_Internal_TemplateCompilerBase $parent_compiler = \null)
    {
    }
    /**
     * Optionally process compiled code by post filter
     *
     * @param string $code compiled code
     *
     * @return string
     * @throws \SmartyException
     */
    public function postFilter($code)
    {
    }
    /**
     * Run optional prefilter
     *
     * @param string $_content template source
     *
     * @return string
     * @throws \SmartyException
     */
    public function preFilter($_content)
    {
    }
    /**
     * Compile Tag
     * This is a call back from the lexer/parser
     *
     * Save current prefix code
     * Compile tag
     * Merge tag prefix code with saved one
     * (required nested tags in attributes)
     *
     * @param string $tag       tag name
     * @param array  $args      array with tag attributes
     * @param array  $parameter array with compilation parameter
     *
     * @throws SmartyCompilerException
     * @throws SmartyException
     * @return string compiled code
     */
    public function compileTag($tag, $args, $parameter = array())
    {
    }
    /**
     * compile variable
     *
     * @param string $variable
     *
     * @return string
     */
    public function compileVariable($variable)
    {
    }
    /**
     * compile config variable
     *
     * @param string $variable
     *
     * @return string
     */
    public function compileConfigVariable($variable)
    {
    }
    /**
     * compile PHP function call
     *
     * @param string $name
     * @param array  $parameter
     *
     * @return string
     * @throws \SmartyCompilerException
     */
    public function compilePHPFunctionCall($name, $parameter)
    {
    }
    /**
     * This method is called from parser to process a text content section if strip is enabled
     * - remove text from inheritance child templates as they may generate output
     *
     * @param string $text
     *
     * @return string
     */
    public function processText($text)
    {
    }
    /**
     * lazy loads internal compile plugin for tag and calls the compile method
     * compile objects cached for reuse.
     * class name format:  Smarty_Internal_Compile_TagName
     * plugin filename format: Smarty_Internal_TagName.php
     *
     * @param string $tag    tag name
     * @param array  $args   list of tag attributes
     * @param mixed  $param1 optional parameter
     * @param mixed  $param2 optional parameter
     * @param mixed  $param3 optional parameter
     *
     * @return bool|string compiled code or false
     * @throws \SmartyCompilerException
     */
    public function callTagCompiler($tag, $args, $param1 = \null, $param2 = \null, $param3 = \null)
    {
    }
    /**
     * lazy loads internal compile plugin for tag compile objects cached for reuse.
     *
     * class name format:  Smarty_Internal_Compile_TagName
     * plugin filename format: Smarty_Internal_TagName.php
     *
     * @param string $tag tag name
     *
     * @return bool|\Smarty_Internal_CompileBase tag compiler object or false if not found
     */
    public function getTagCompiler($tag)
    {
    }
    /**
     * Check for plugins and return function name
     *
     * @param        $plugin_name
     * @param string $plugin_type type of plugin
     *
     * @return string call name of function
     * @throws \SmartyException
     */
    public function getPlugin($plugin_name, $plugin_type)
    {
    }
    /**
     * Check for plugins by default plugin handler
     *
     * @param string $tag         name of tag
     * @param string $plugin_type type of plugin
     *
     * @return bool true if found
     * @throws \SmartyCompilerException
     */
    public function getPluginFromDefaultHandler($tag, $plugin_type)
    {
    }
    /**
     * Append code segments and remove unneeded ?> <?php transitions
     *
     * @param string $left
     * @param string $right
     *
     * @return string
     */
    public function appendCode($left, $right)
    {
    }
    /**
     * Inject inline code for nocache template sections
     * This method gets the content of each template element from the parser.
     * If the content is compiled code and it should be not cached the code is injected
     * into the rendered output.
     *
     * @param string  $content content of template element
     * @param boolean $is_code true if content is compiled code
     *
     * @return string  content
     */
    public function processNocacheCode($content, $is_code)
    {
    }
    /**
     * Get Id
     *
     * @param string $input
     *
     * @return bool|string
     */
    public function getId($input)
    {
    }
    /**
     * Get variable name from string
     *
     * @param string $input
     *
     * @return bool|string
     */
    public function getVariableName($input)
    {
    }
    /**
     * Set nocache flag in variable or create new variable
     *
     * @param string $varName
     */
    public function setNocacheInVariable($varName)
    {
    }
    /**
     * @param array $_attr tag attributes
     * @param array $validScopes
     *
     * @return int|string
     * @throws \SmartyCompilerException
     */
    public function convertScope($_attr, $validScopes)
    {
    }
    /**
     * Generate nocache code string
     *
     * @param string $code PHP code
     *
     * @return string
     */
    public function makeNocacheCode($code)
    {
    }
    /**
     * display compiler error messages without dying
     * If parameter $args is empty it is a parser detected syntax error.
     * In this case the parser is called to obtain information about expected tokens.
     * If parameter $args contains a string this is used as error message
     *
     * @param string    $args    individual error message or null
     * @param string    $line    line-number
     * @param null|bool $tagline if true the line number of last tag
     *
     * @throws \SmartyCompilerException when an unexpected token is found
     */
    public function trigger_template_error($args = \null, $line = \null, $tagline = \null)
    {
    }
    /**
     * Return var_export() value with all white spaces removed
     *
     * @param mixed $value
     *
     * @return string
     */
    public function getVarExport($value)
    {
    }
    /**
     *  enter double quoted string
     *  - save tag stack count
     */
    public function enterDoubleQuote()
    {
    }
    /**
     * Return tag stack count
     *
     * @return int
     */
    public function getTagStackCount()
    {
    }
    /**
     * @param $lexerPreg
     *
     * @return mixed
     */
    public function replaceDelimiter($lexerPreg)
    {
    }
    /**
     * Build lexer regular expressions for left and right delimiter and user defined literals
     */
    public function initDelimiterPreg()
    {
    }
    /**
     *  leave double quoted string
     *  - throw exception if block in string was not closed
     *
     * @throws \SmartyCompilerException
     */
    public function leaveDoubleQuote()
    {
    }
    /**
     * Get left delimiter preg
     *
     * @return string
     */
    public function getLdelPreg()
    {
    }
    /**
     * Get right delimiter preg
     *
     * @return string
     */
    public function getRdelPreg()
    {
    }
    /**
     * Get length of left delimiter
     *
     * @return int
     */
    public function getLdelLength()
    {
    }
    /**
     * Get length of right delimiter
     *
     * @return int
     */
    public function getRdelLength()
    {
    }
    /**
     * Get name of current open block tag
     *
     * @return string|boolean
     */
    public function getOpenBlockTag()
    {
    }
    /**
     * Check if $value contains variable elements
     *
     * @param mixed $value
     *
     * @return bool|int
     */
    public function isVariable($value)
    {
    }
    /**
     * Get new prefix variable name
     *
     * @return string
     */
    public function getNewPrefixVariable()
    {
    }
    /**
     * Get current prefix variable name
     *
     * @return string
     */
    public function getPrefixVariable()
    {
    }
    /**
     * append  code to prefix buffer
     *
     * @param string $code
     */
    public function appendPrefixCode($code)
    {
    }
    /**
     * get prefix code string
     *
     * @return string
     */
    public function getPrefixCode()
    {
    }
    /**
     * Save current required plugins
     *
     * @param bool $init if true init required plugins
     */
    public function saveRequiredPlugins($init = \false)
    {
    }
    /**
     * Restore required plugins
     */
    public function restoreRequiredPlugins()
    {
    }
    /**
     * Compile code to call Smarty_Internal_Template::_checkPlugins()
     * for required plugins
     *
     * @return string
     */
    public function compileRequiredPlugins()
    {
    }
    /**
     * Compile code to call Smarty_Internal_Template::_checkPlugins
     *   - checks if plugin is callable require otherwise
     *
     * @param $requiredPlugins
     *
     * @return string
     */
    public function compileCheckPlugins($requiredPlugins)
    {
    }
    /**
     * method to compile a Smarty template
     *
     * @param mixed $_content template source
     * @param bool  $isTemplateSource
     *
     * @return bool true if compiling succeeded, false if it failed
     */
    protected abstract function doCompile($_content, $isTemplateSource = \false);
    public function cStyleComment($string)
    {
    }
}
