<?php

/**
 * Smarty Internal Plugin Template
 * This file contains the Smarty template engine
 *
 * @package    Smarty
 * @subpackage Template
 * @author     Uwe Tews
 */
/**
 * Main class with template data structures and methods
 *
 * @package    Smarty
 * @subpackage Template
 *
 * @property Smarty_Template_Compiled             $compiled
 * @property Smarty_Template_Cached               $cached
 * @property Smarty_Internal_TemplateCompilerBase $compiler
 * @property mixed|\Smarty_Template_Cached        registered_plugins
 *
 * The following methods will be dynamically loaded by the extension handler when they are called.
 * They are located in a corresponding Smarty_Internal_Method_xxxx class
 *
 * @method bool mustCompile()
 */
#[\AllowDynamicProperties]
class Smarty_Internal_Template extends \Smarty_Internal_TemplateBase
{
    /**
     * Template object cache
     *
     * @var Smarty_Internal_Template[]
     */
    public static $tplObjCache = array();
    /**
     * Template object cache for Smarty::isCached() === true
     *
     * @var Smarty_Internal_Template[]
     */
    public static $isCacheTplObj = array();
    /**
     * Sub template Info Cache
     * - index name
     * - value use count
     *
     * @var int[]
     */
    public static $subTplInfo = array();
    /**
     * This object type (Smarty = 1, template = 2, data = 4)
     *
     * @var int
     */
    public $_objType = 2;
    /**
     * Global smarty instance
     *
     * @var Smarty
     */
    public $smarty = \null;
    /**
     * Source instance
     *
     * @var Smarty_Template_Source|Smarty_Template_Config
     */
    public $source = \null;
    /**
     * Inheritance runtime extension
     *
     * @var Smarty_Internal_Runtime_Inheritance
     */
    public $inheritance = \null;
    /**
     * Template resource
     *
     * @var string
     */
    public $template_resource = \null;
    /**
     * flag if compiled template is invalid and must be (re)compiled
     *
     * @var bool
     */
    public $mustCompile = \null;
    /**
     * Template Id
     *
     * @var null|string
     */
    public $templateId = \null;
    /**
     * Scope in which variables shall be assigned
     *
     * @var int
     */
    public $scope = 0;
    /**
     * Flag which is set while rending a cache file
     *
     * @var bool
     */
    public $isRenderingCache = \false;
    /**
     * Callbacks called before rendering template
     *
     * @var callback[]
     */
    public $startRenderCallbacks = array();
    /**
     * Callbacks called after rendering template
     *
     * @var callback[]
     */
    public $endRenderCallbacks = array();
    /**
     * Create template data object
     * Some of the global Smarty settings copied to template scope
     * It load the required template resources and caching plugins
     *
     * @param string                                                       $template_resource template resource string
     * @param Smarty                                                       $smarty            Smarty instance
     * @param null|\Smarty_Internal_Template|\Smarty|\Smarty_Internal_Data $_parent           back pointer to parent
     *                                                                                        object with variables or
     *                                                                                        null
     * @param mixed                                                        $_cache_id         cache   id or null
     * @param mixed                                                        $_compile_id       compile id or null
     * @param bool|int|null                                                $_caching          use caching?
     * @param int|null                                                     $_cache_lifetime   cache life-time in
     *                                                                                        seconds
     * @param bool                                                         $_isConfig
     *
     * @throws \SmartyException
     */
    public function __construct($template_resource, \Smarty $smarty, \Smarty_Internal_Data $_parent = \null, $_cache_id = \null, $_compile_id = \null, $_caching = \null, $_cache_lifetime = \null, $_isConfig = \false)
    {
    }
    /**
     * render template
     *
     * @param bool      $no_output_filter if true do not run output filter
     * @param null|bool $display          true: display, false: fetch null: sub-template
     *
     * @return string
     * @throws \Exception
     * @throws \SmartyException
     */
    public function render($no_output_filter = \true, $display = \null)
    {
    }
    /**
     * Runtime function to render sub-template
     *
     * @param string  $template       template name
     * @param mixed   $cache_id       cache id
     * @param mixed   $compile_id     compile id
     * @param integer $caching        cache mode
     * @param integer $cache_lifetime life time of cache data
     * @param array   $data           passed parameter template variables
     * @param int     $scope          scope in which {include} should execute
     * @param bool    $forceTplCache  cache template object
     * @param string  $uid            file dependency uid
     * @param string  $content_func   function name
     *
     * @throws \Exception
     * @throws \SmartyException
     */
    public function _subTemplateRender($template, $cache_id, $compile_id, $caching, $cache_lifetime, $data, $scope, $forceTplCache, $uid = \null, $content_func = \null)
    {
    }
    /**
     * Get called sub-templates and save call count
     */
    public function _subTemplateRegister()
    {
    }
    /**
     * Check if this is a sub template
     *
     * @return bool true is sub template
     */
    public function _isSubTpl()
    {
    }
    /**
     * Assign variable in scope
     *
     * @param string $varName variable name
     * @param mixed  $value   value
     * @param bool   $nocache nocache flag
     * @param int    $scope   scope into which variable shall be assigned
     */
    public function _assignInScope($varName, $value, $nocache = \false, $scope = 0)
    {
    }
    /**
     * Check if plugins are callable require file otherwise
     *
     * @param array $plugins required plugins
     *
     * @throws \SmartyException
     */
    public function _checkPlugins($plugins)
    {
    }
    /**
     * This function is executed automatically when a compiled or cached template file is included
     * - Decode saved properties from compiled template and cache files
     * - Check if compiled or cache file is valid
     *
     * @param \Smarty_Internal_Template $tpl
     * @param array                     $properties special template properties
     * @param bool                      $cache      flag if called from cache file
     *
     * @return bool flag if compiled or cache file is valid
     * @throws \SmartyException
     */
    public function _decodeProperties(\Smarty_Internal_Template $tpl, $properties, $cache = \false)
    {
    }
    /**
     * Compiles the template
     * If the template is not evaluated the compiled template is saved on disk
     *
     * @throws \Exception
     */
    public function compileTemplateSource()
    {
    }
    /**
     * Writes the content to cache resource
     *
     * @param string $content
     *
     * @return bool
     */
    public function writeCachedContent($content)
    {
    }
    /**
     * Get unique template id
     *
     * @return string
     * @throws \SmartyException
     */
    public function _getTemplateId()
    {
    }
    /**
     * runtime error not matching capture tags
     *
     * @throws \SmartyException
     */
    public function capture_error()
    {
    }
    /**
     * Load compiled object
     *
     * @param bool $force force new compiled object
     */
    public function loadCompiled($force = \false)
    {
    }
    /**
     * Load cached object
     *
     * @param bool $force force new cached object
     */
    public function loadCached($force = \false)
    {
    }
    /**
     * Load inheritance object
     */
    public function _loadInheritance()
    {
    }
    /**
     * Unload inheritance object
     */
    public function _cleanUp()
    {
    }
    /**
     * Load compiler object
     *
     * @throws \SmartyException
     */
    public function loadCompiler()
    {
    }
    /**
     * Handle unknown class methods
     *
     * @param string $name unknown method-name
     * @param array  $args argument array
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
    }
    /**
     * get Smarty property in template context
     *
     * @param string $property_name property name
     *
     * @return mixed|Smarty_Template_Cached
     * @throws SmartyException
     */
    public function __get($property_name)
    {
    }
    /**
     * set Smarty property in template context
     *
     * @param string $property_name property name
     * @param mixed  $value         value
     *
     * @throws SmartyException
     */
    public function __set($property_name, $value)
    {
    }
    /**
     * Template data object destructor
     */
    public function __destruct()
    {
    }
}
