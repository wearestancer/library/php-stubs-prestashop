<?php

/**
 * Smarty Internal Plugin Configfilelexer
 *
 * This is the lexer to break the config file source into tokens
 *
 * @package    Smarty
 * @subpackage Config
 * @author     Uwe Tews
 */
/**
 * Smarty_Internal_Configfilelexer
 *
 * This is the config file lexer.
 * It is generated from the smarty_internal_configfilelexer.plex file
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
class Smarty_Internal_Configfilelexer
{
    const START = 1;
    const VALUE = 2;
    const NAKED_STRING_VALUE = 3;
    const COMMENT = 4;
    const SECTION = 5;
    const TRIPPLE = 6;
    /**
     * Source
     *
     * @var string
     */
    public $data;
    /**
     * Source length
     *
     * @var int
     */
    public $dataLength = \null;
    /**
     * byte counter
     *
     * @var int
     */
    public $counter;
    /**
     * token number
     *
     * @var int
     */
    public $token;
    /**
     * token value
     *
     * @var string
     */
    public $value;
    /**
     * current line
     *
     * @var int
     */
    public $line;
    /**
     * state number
     *
     * @var int
     */
    public $state = 1;
    /**
     * Smarty object
     *
     * @var Smarty
     */
    public $smarty = \null;
    /**
     * trace file
     *
     * @var resource
     */
    public $yyTraceFILE;
    /**
     * trace prompt
     *
     * @var string
     */
    public $yyTracePrompt;
    /**
     * state names
     *
     * @var array
     */
    public $state_name = array(1 => 'START', 2 => 'VALUE', 3 => 'NAKED_STRING_VALUE', 4 => 'COMMENT', 5 => 'SECTION', 6 => 'TRIPPLE');
    /**
     * token names
     *
     * @var array
     */
    public $smarty_token_names = array();
    /**
     * constructor
     *
     * @param   string                             $data template source
     * @param Smarty_Internal_Config_File_Compiler $compiler
     */
    public function __construct($data, \Smarty_Internal_Config_File_Compiler $compiler)
    {
    }
    public function replace($input)
    {
    }
    // end function
    public function PrintTrace()
    {
    }
    public function yylex()
    {
    }
    public function yypushstate($state)
    {
    }
    public function yypopstate()
    {
    }
    public function yybegin($state)
    {
    }
    public function yylex1()
    {
    }
    public function yy_r1_1()
    {
    }
    public function yy_r1_2()
    {
    }
    public function yy_r1_3()
    {
    }
    public function yy_r1_4()
    {
    }
    // end function
    public function yy_r1_5()
    {
    }
    public function yy_r1_6()
    {
    }
    public function yy_r1_7()
    {
    }
    public function yy_r1_8()
    {
    }
    public function yylex2()
    {
    }
    public function yy_r2_1()
    {
    }
    public function yy_r2_2()
    {
    }
    public function yy_r2_3()
    {
    }
    public function yy_r2_4()
    {
    }
    public function yy_r2_5()
    {
    }
    public function yy_r2_6()
    {
    }
    // end function
    public function yy_r2_7()
    {
    }
    public function yy_r2_8()
    {
    }
    public function yy_r2_9()
    {
    }
    // end function
    public function yylex3()
    {
    }
    public function yy_r3_1()
    {
    }
    public function yylex4()
    {
    }
    public function yy_r4_1()
    {
    }
    public function yy_r4_2()
    {
    }
    // end function
    public function yy_r4_3()
    {
    }
    public function yylex5()
    {
    }
    public function yy_r5_1()
    {
    }
    public function yy_r5_2()
    {
    }
    // end function
    public function yylex6()
    {
    }
    public function yy_r6_1()
    {
    }
    public function yy_r6_2()
    {
    }
}
