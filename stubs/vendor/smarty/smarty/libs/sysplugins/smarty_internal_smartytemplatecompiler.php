<?php

/**
 * Smarty Internal Plugin Smarty Template Compiler Base
 * This file contains the basic classes and methods for compiling Smarty templates with lexer/parser
 *
 * @package    Smarty
 * @subpackage Compiler
 * @author     Uwe Tews
 */
/**
 * Class SmartyTemplateCompiler
 *
 * @package    Smarty
 * @subpackage Compiler
 */
class Smarty_Internal_SmartyTemplateCompiler extends \Smarty_Internal_TemplateCompilerBase
{
    /**
     * Lexer class name
     *
     * @var string
     */
    public $lexer_class;
    /**
     * Parser class name
     *
     * @var string
     */
    public $parser_class;
    /**
     * array of vars which can be compiled in local scope
     *
     * @var array
     */
    public $local_var = array();
    /**
     * array of callbacks called when the normal compile process of template is finished
     *
     * @var array
     */
    public $postCompileCallbacks = array();
    /**
     * prefix code
     *
     * @var string
     */
    public $prefixCompiledCode = '';
    /**
     * postfix code
     *
     * @var string
     */
    public $postfixCompiledCode = '';
    /**
     * Initialize compiler
     *
     * @param string $lexer_class  class name
     * @param string $parser_class class name
     * @param Smarty $smarty       global instance
     */
    public function __construct($lexer_class, $parser_class, \Smarty $smarty)
    {
    }
    /**
     * method to compile a Smarty template
     *
     * @param mixed $_content template source
     * @param bool  $isTemplateSource
     *
     * @return bool true if compiling succeeded, false if it failed
     * @throws \SmartyCompilerException
     */
    protected function doCompile($_content, $isTemplateSource = \false)
    {
    }
    /**
     * Register a post compile callback
     * - when the callback is called after template compiling the compiler object will be inserted as first parameter
     *
     * @param callback $callback
     * @param array    $parameter optional parameter array
     * @param string   $key       optional key for callback
     * @param bool     $replace   if true replace existing keyed callback
     */
    public function registerPostCompileCallback($callback, $parameter = array(), $key = \null, $replace = \false)
    {
    }
    /**
     * Remove a post compile callback
     *
     * @param string $key callback key
     */
    public function unregisterPostCompileCallback($key)
    {
    }
}
