<?php

/**
 * Smarty Method UnregisterCacheResource
 *
 * Smarty::unregisterCacheResource() method
 *
 * @package    Smarty
 * @subpackage PluginsInternal
 * @author     Uwe Tews
 */
class Smarty_Internal_Method_UnregisterCacheResource
{
    /**
     * Valid for Smarty and template object
     *
     * @var int
     */
    public $objMap = 3;
    /**
     * Registers a resource to fetch a template
     *
     * @api  Smarty::unregisterCacheResource()
     * @link https://www.smarty.net/docs/en/api.unregister.cacheresource.tpl
     *
     * @param \Smarty_Internal_TemplateBase|\Smarty_Internal_Template|\Smarty $obj
     * @param                                                                 $name
     *
     * @return \Smarty|\Smarty_Internal_Template
     */
    public function unregisterCacheResource(\Smarty_Internal_TemplateBase $obj, $name)
    {
    }
}
