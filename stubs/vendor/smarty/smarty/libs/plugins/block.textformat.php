<?php

/**
 * Smarty plugin to format text blocks
 *
 * @package    Smarty
 * @subpackage PluginsBlock
 */
/**
 * Smarty {textformat}{/textformat} block plugin
 * Type:     block function
 * Name:     textformat
 * Purpose:  format text a certain way with preset styles
 *           or custom wrap/indent settings
 * Params:
 *
 * - style         - string (email)
 * - indent        - integer (0)
 * - wrap          - integer (80)
 * - wrap_char     - string ("\n")
 * - indent_char   - string (" ")
 * - wrap_boundary - boolean (true)
 *
 * @link   https://www.smarty.net/manual/en/language.function.textformat.php {textformat}
 *         (Smarty online manual)
 *
 * @param array                    $params   parameters
 * @param string                   $content  contents of the block
 * @param Smarty_Internal_Template $template template object
 * @param boolean                  &$repeat  repeat flag
 *
 * @return string content re-formatted
 * @author Monte Ohrt <monte at ohrt dot com>
 * @throws \SmartyException
 */
function smarty_block_textformat($params, $content, \Smarty_Internal_Template $template, &$repeat)
{
}
