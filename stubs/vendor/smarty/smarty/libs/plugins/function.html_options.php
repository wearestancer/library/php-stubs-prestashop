<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */
/**
 * Smarty {html_options} function plugin
 * Type:     function
 * Name:     html_options
 * Purpose:  Prints the list of <option> tags generated from
 *           the passed parameters
 * Params:
 *
 * - name       (optional) - string default "select"
 * - values     (required) - if no options supplied) - array
 * - options    (required) - if no values supplied) - associative array
 * - selected   (optional) - string default not set
 * - output     (required) - if not options supplied) - array
 * - id         (optional) - string default not set
 * - class      (optional) - string default not set
 *
 * @link   https://www.smarty.net/manual/en/language.function.html.options.php {html_image}
 *           (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 * @author Ralf Strehle (minor optimization) <ralf dot strehle at yahoo dot de>
 *
 * @param array                     $params parameters
 *
 * @param \Smarty_Internal_Template $template
 *
 * @return string
 * @uses   smarty_function_escape_special_chars()
 * @throws \SmartyException
 */
function smarty_function_html_options($params, \Smarty_Internal_Template $template)
{
}
/**
 * @param $key
 * @param $value
 * @param $selected
 * @param $id
 * @param $class
 * @param $idx
 *
 * @return string
 */
function smarty_function_html_options_optoutput($key, $value, $selected, $id, $class, &$idx)
{
}
/**
 * @param $key
 * @param $values
 * @param $selected
 * @param $id
 * @param $class
 * @param $idx
 *
 * @return string
 */
function smarty_function_html_options_optgroup($key, $values, $selected, $id, $class, &$idx)
{
}
