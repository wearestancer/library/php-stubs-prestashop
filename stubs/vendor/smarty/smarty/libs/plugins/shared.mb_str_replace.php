<?php

/**
 * Multibyte string replace
 *
 * @param string|string[] $search  the string to be searched
 * @param string|string[] $replace the replacement string
 * @param string          $subject the source string
 * @param int             &$count  number of matches found
 *
 * @return string replaced string
 * @author Rodney Rehm
 */
function smarty_mb_str_replace($search, $replace, $subject, &$count = 0)
{
}
