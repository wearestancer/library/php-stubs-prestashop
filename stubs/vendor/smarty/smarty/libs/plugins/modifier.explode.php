<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty explode modifier plugin
 * Type:     modifier
 * Name:     explode
 * Purpose:  split a string by a string
 *
 * @param string   $separator
 * @param string   $string
 * @param int|null $limit
 *
 * @return array
 */
function smarty_modifier_explode($separator, $string, ?int $limit = \null)
{
}
