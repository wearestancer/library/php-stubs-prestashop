<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */
/**
 * Smarty {html_select_time} function plugin
 * Type:     function
 * Name:     html_select_time
 * Purpose:  Prints the dropdowns for time selection
 *
 * @link   https://www.smarty.net/manual/en/language.function.html.select.time.php {html_select_time}
 *           (Smarty online manual)
 * @author Roberto Berto <roberto@berto.net>
 * @author Monte Ohrt <monte AT ohrt DOT com>
 *
 * @param array                     $params parameters
 *
 * @param \Smarty_Internal_Template $template
 *
 * @return string
 * @uses   smarty_make_timestamp()
 * @throws \SmartyException
 */
function smarty_function_html_select_time($params, \Smarty_Internal_Template $template)
{
}
