<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty count modifier plugin
 * Type:     modifier
 * Name:     count
 * Purpose:  counts all elements in an array or in a Countable object
 * Input:
 *          - Countable|array: array or object to count
 *          - mode: int defaults to 0 for normal count mode, if set to 1 counts recursive
 *
 * @param mixed $arrayOrObject  input array/object
 * @param int $mode       count mode
 *
 * @return int
 */
function smarty_modifier_count($arrayOrObject, $mode = 0)
{
}
