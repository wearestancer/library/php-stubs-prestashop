<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */
/**
 * Smarty {html_radios} function plugin
 * File:       function.html_radios.php
 * Type:       function
 * Name:       html_radios
 * Date:       24.Feb.2003
 * Purpose:    Prints out a list of radio input types
 * Params:
 *
 * - name       (optional) - string default "radio"
 * - values     (required) - array
 * - options    (required) - associative array
 * - checked    (optional) - array default not set
 * - separator  (optional) - ie <br> or &nbsp;
 * - output     (optional) - the output next to each radio button
 * - assign     (optional) - assign the output as an array to this variable
 * - escape     (optional) - escape the content (not value), defaults to true
 *
 * Examples:
 *
 * {html_radios values=$ids output=$names}
 * {html_radios values=$ids name='box' separator='<br>' output=$names}
 * {html_radios values=$ids checked=$checked separator='<br>' output=$names}
 *
 * @link    https://www.smarty.net/manual/en/language.function.html.radios.php {html_radios}
 *          (Smarty online manual)
 * @author  Christopher Kvarme <christopher.kvarme@flashjab.com>
 * @author  credits to Monte Ohrt <monte at ohrt dot com>
 * @version 1.0
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 *
 * @return string
 * @uses    smarty_function_escape_special_chars()
 * @throws \SmartyException
 */
function smarty_function_html_radios($params, \Smarty_Internal_Template $template)
{
}
/**
 * @param $name
 * @param $value
 * @param $output
 * @param $selected
 * @param $extra
 * @param $separator
 * @param $labels
 * @param $label_ids
 * @param $escape
 *
 * @return string
 */
function smarty_function_html_radios_output($name, $value, $output, $selected, $extra, $separator, $labels, $label_ids, $escape)
{
}
