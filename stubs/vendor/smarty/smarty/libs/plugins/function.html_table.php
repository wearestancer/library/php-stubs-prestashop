<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */
/**
 * Smarty {html_table} function plugin
 * Type:     function
 * Name:     html_table
 * Date:     Feb 17, 2003
 * Purpose:  make an html table from an array of data
 * Params:
 *
 * - loop       - array to loop through
 * - cols       - number of columns, comma separated list of column names
 *                or array of column names
 * - rows       - number of rows
 * - table_attr - table attributes
 * - th_attr    - table heading attributes (arrays are cycled)
 * - tr_attr    - table row attributes (arrays are cycled)
 * - td_attr    - table cell attributes (arrays are cycled)
 * - trailpad   - value to pad trailing cells with
 * - caption    - text for caption element
 * - vdir       - vertical direction (default: "down", means top-to-bottom)
 * - hdir       - horizontal direction (default: "right", means left-to-right)
 * - inner      - inner loop (default "cols": print $loop line by line,
 *                $loop will be printed column by column otherwise)
 *
 * Examples:
 *
 * {table loop=$data}
 * {table loop=$data cols=4 tr_attr='"bgcolor=red"'}
 * {table loop=$data cols="first,second,third" tr_attr=$colors}
 *
 * @author  Monte Ohrt <monte at ohrt dot com>
 * @author  credit to Messju Mohr <messju at lammfellpuschen dot de>
 * @author  credit to boots <boots dot smarty at yahoo dot com>
 * @version 1.1
 * @link    https://www.smarty.net/manual/en/language.function.html.table.php {html_table}
 *           (Smarty online manual)
 *
 * @param array $params parameters
 *
 * @return string
 */
function smarty_function_html_table($params)
{
}
/**
 * @param $name
 * @param $var
 * @param $no
 *
 * @return string
 */
function smarty_function_html_table_cycle($name, $var, $no)
{
}
