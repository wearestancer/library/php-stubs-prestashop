<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty capitalize modifier plugin
 * Type:     modifier
 * Name:     capitalize
 * Purpose:  capitalize words in the string
 * {@internal {$string|capitalize:true:true} is the fastest option for MBString enabled systems }}
 *
 * @param string  $string    string to capitalize
 * @param boolean $uc_digits also capitalize "x123" to "X123"
 * @param boolean $lc_rest   capitalize first letters, lowercase all following letters "aAa" to "Aaa"
 *
 * @return string capitalized string
 * @author Monte Ohrt <monte at ohrt dot com>
 * @author Rodney Rehm
 */
function smarty_modifier_capitalize($string, $uc_digits = \false, $lc_rest = \false)
{
}
/**
 *
 * Bug: create_function() use exhausts memory when used in long loops
 * Fix: use declared functions for callbacks instead of using create_function()
 * Note: This can be fixed using anonymous functions instead, but that requires PHP >= 5.3
 *
 * @author Kyle Renfrow
 */
/**
 * @param $matches
 *
 * @return string
 */
function smarty_mod_cap_mbconvert_cb($matches)
{
}
/**
 * @param $matches
 *
 * @return string
 */
function smarty_mod_cap_mbconvert2_cb($matches)
{
}
/**
 * @param $matches
 *
 * @return string
 */
function smarty_mod_cap_ucfirst_cb($matches)
{
}
/**
 * @param $matches
 *
 * @return string
 */
function smarty_mod_cap_ucfirst2_cb($matches)
{
}
