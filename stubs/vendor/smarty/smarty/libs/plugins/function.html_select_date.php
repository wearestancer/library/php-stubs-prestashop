<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */
/**
 * Smarty {html_select_date} plugin
 * Type:     function
 * Name:     html_select_date
 * Purpose:  Prints the dropdowns for date selection.
 * ChangeLog:
 *
 *            - 1.0 initial release
 *            - 1.1 added support for +/- N syntax for begin
 *              and end year values. (Monte)
 *            - 1.2 added support for yyyy-mm-dd syntax for
 *              time value. (Jan Rosier)
 *            - 1.3 added support for choosing format for
 *              month values (Gary Loescher)
 *            - 1.3.1 added support for choosing format for
 *              day values (Marcus Bointon)
 *            - 1.3.2 support negative timestamps, force year
 *              dropdown to include given date unless explicitly set (Monte)
 *            - 1.3.4 fix behaviour of 0000-00-00 00:00:00 dates to match that
 *              of 0000-00-00 dates (cybot, boots)
 *            - 2.0 complete rewrite for performance,
 *              added attributes month_names, *_id
 *
 * @link    https://www.smarty.net/manual/en/language.function.html.select.date.php {html_select_date}
 *           (Smarty online manual)
 * @version 2.0
 * @author  Andrei Zmievski
 * @author  Monte Ohrt <monte at ohrt dot com>
 * @author  Rodney Rehm
 *
 * @param array                     $params parameters
 *
 * @param \Smarty_Internal_Template $template
 *
 * @return string
 * @throws \SmartyException
 */
function smarty_function_html_select_date($params, \Smarty_Internal_Template $template)
{
}
