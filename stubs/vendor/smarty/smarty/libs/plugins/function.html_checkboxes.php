<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */
/**
 * Smarty {html_checkboxes} function plugin
 * File:       function.html_checkboxes.php
 * Type:       function
 * Name:       html_checkboxes
 * Date:       24.Feb.2003
 * Purpose:    Prints out a list of checkbox input types
 * Examples:
 *
 * {html_checkboxes values=$ids output=$names}
 * {html_checkboxes values=$ids name='box' separator='<br>' output=$names}
 * {html_checkboxes values=$ids checked=$checked separator='<br>' output=$names}
 *
 * Params:
 *
 * - name       (optional) - string default "checkbox"
 * - values     (required) - array
 * - options    (optional) - associative array
 * - checked    (optional) - array default not set
 * - separator  (optional) - ie <br> or &nbsp;
 * - output     (optional) - the output next to each checkbox
 * - assign     (optional) - assign the output as an array to this variable
 * - escape     (optional) - escape the content (not value), defaults to true
 *
 * @link    https://www.smarty.net/manual/en/language.function.html.checkboxes.php {html_checkboxes}
 *             (Smarty online manual)
 * @author  Christopher Kvarme <christopher.kvarme@flashjab.com>
 * @author  credits to Monte Ohrt <monte at ohrt dot com>
 * @version 1.0
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 *
 * @return string
 * @uses    smarty_function_escape_special_chars()
 * @throws \SmartyException
 */
function smarty_function_html_checkboxes($params, \Smarty_Internal_Template $template)
{
}
/**
 * @param      $name
 * @param      $value
 * @param      $output
 * @param      $selected
 * @param      $extra
 * @param      $separator
 * @param      $labels
 * @param      $label_ids
 * @param bool $escape
 *
 * @return string
 */
function smarty_function_html_checkboxes_output($name, $value, $output, $selected, $extra, $separator, $labels, $label_ids, $escape = \true)
{
}
