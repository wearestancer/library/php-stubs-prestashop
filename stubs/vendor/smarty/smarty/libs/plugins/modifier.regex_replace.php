<?php

/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty regex_replace modifier plugin
 * Type:     modifier
 * Name:     regex_replace
 * Purpose:  regular expression search/replace
 *
 * @link   https://www.smarty.net/manual/en/language.modifier.regex.replace.php
 *          regex_replace (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 *
 * @param string       $string  input string
 * @param string|array $search  regular expression(s) to search for
 * @param string|array $replace string(s) that should be replaced
 * @param int          $limit   the maximum number of replacements
 *
 * @return string
 */
function smarty_modifier_regex_replace($string, $search, $replace, $limit = -1)
{
}
/**
 * @param  string $search string(s) that should be replaced
 *
 * @return string
 * @ignore
 */
function _smarty_regex_replace_check($search)
{
}
