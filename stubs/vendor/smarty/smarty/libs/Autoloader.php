<?php

/**
 * Smarty Autoloader
 *
 * @package Smarty
 * @author  Uwe Tews
 *             Usage:
 *                  require_once '...path/Autoloader.php';
 *                  Smarty_Autoloader::register();
 *             or
 *                  include '...path/bootstrap.php';
 *
 *                  $smarty = new Smarty();
 */
class Smarty_Autoloader
{
    /**
     * Filepath to Smarty root
     *
     * @var string
     */
    public static $SMARTY_DIR = \null;
    /**
     * Filepath to Smarty internal plugins
     *
     * @var string
     */
    public static $SMARTY_SYSPLUGINS_DIR = \null;
    /**
     * Array with Smarty core classes and their filename
     *
     * @var array
     */
    public static $rootClasses = array('smarty' => 'Smarty.class.php');
    /**
     * Registers Smarty_Autoloader backward compatible to older installations.
     *
     * @param bool $prepend Whether to prepend the autoloader or not.
     */
    public static function registerBC($prepend = \false)
    {
    }
    /**
     * Registers Smarty_Autoloader as an SPL autoloader.
     *
     * @param bool $prepend Whether to prepend the autoloader or not.
     */
    public static function register($prepend = \false)
    {
    }
    /**
     * Handles auto loading of classes.
     *
     * @param string $class A class name.
     */
    public static function autoload($class)
    {
    }
}
