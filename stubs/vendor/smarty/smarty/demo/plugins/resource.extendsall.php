<?php

/**
 * Extends All Resource
 * Resource Implementation modifying the extends-Resource to walk
 * through the template_dirs and inherit all templates of the same name
 *
 * @package Resource-examples
 * @author  Rodney Rehm
 */
class Smarty_Resource_Extendsall extends \Smarty_Internal_Resource_Extends
{
    /**
     * populate Source Object with meta data from Resource
     *
     * @param Smarty_Template_Source   $source    source object
     * @param Smarty_Internal_Template $_template template object
     *
     * @return void
     */
    public function populate(\Smarty_Template_Source $source, \Smarty_Internal_Template $_template = \null)
    {
    }
    /**
     * Disable timestamp checks for extendsall resource.
     * The individual source components will be checked.
     *
     * @return bool false
     */
    public function checkTimestamps()
    {
    }
}
