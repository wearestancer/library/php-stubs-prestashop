<?php

namespace Minify\StaticService;

/**
 * Build a URI for the static cache
 *
 * @param string $static_uri E.g. "/min/static"
 * @param string $query E.g. "b=scripts&f=1.js,2.js"
 * @param string $type "css" or "js"
 * @return string
 */
function build_uri($static_uri, $query, $type)
{
}
/**
 * Get the name of the current cache directory within static/. E.g. "1467089473"
 *
 * @param bool $auto_create Automatically create the directory if missing?
 * @return null|string null if missing or can't create
 */
function get_cache_time($auto_create = true)
{
}
function flush_cache()
{
}
function remove_tree($dir)
{
}
