<?php

/**
 * Minify - Combines, minifies, and caches JavaScript and CSS files on demand.
 *
 * See README for usage instructions (for now).
 *
 * This library was inspired by {@link mailto:flashkot@mail.ru jscsscomp by Maxim Martynyuk}
 * and by the article {@link http://www.hunlock.com/blogs/Supercharged_Javascript "Supercharged JavaScript" by Patrick Hunlock}.
 *
 * @package Minify
 * @author Ryan Grove <ryan@wonko.com>
 * @author Stephen Clay <steve@mrclay.org>
 * @copyright 2008 Ryan Grove, Stephen Clay. All rights reserved.
 * @license http://opensource.org/licenses/bsd-license.php  New BSD License
 * @link https://github.com/mrclay/minify
 */
class Minify
{
    /**
     * API version
     *
     * This is only bumped when API breaks are done and should follow the major version of the library
     *
     * @var int
     */
    const VERSION = 3;
    const TYPE_CSS = 'text/css';
    const TYPE_HTML = 'text/html';
    // there is some debate over the ideal JS Content-Type, but this is the
    // Apache default and what Yahoo! uses..
    const TYPE_JS = 'application/x-javascript';
    const URL_DEBUG = 'https://github.com/mrclay/minify/blob/master/docs/Debugging.wiki.md';
    /**
     * Active controller for current request
     *
     * @var Minify_Controller_Base
     */
    protected $controller;
    /**
     * @var Minify_Env
     */
    protected $env;
    /**
     * @var Minify_SourceInterface[]
     */
    protected $sources;
    /**
     * @var string
     */
    protected $selectionId;
    /**
     * Options for current request
     *
     * @var array
     */
    protected $options;
    /**
     * @var LoggerInterface|null
     */
    protected $logger;
    /**
     * @param Minify_CacheInterface $cache
     * @param LoggerInterface       $logger
     */
    public function __construct(\Minify_CacheInterface $cache, \Psr\Log\LoggerInterface $logger = \null)
    {
    }
    /**
     * Get default Minify options.
     *
     * @return array options for Minify
     */
    public function getDefaultOptions()
    {
    }
    /**
     * Serve a request for a minified file.
     *
     * Here are the available options and defaults:
     *
     * 'isPublic' : send "public" instead of "private" in Cache-Control
     * headers, allowing shared caches to cache the output. (default true)
     *
     * 'quiet' : set to true to have serve() return an array rather than sending
     * any headers/output (default false)
     *
     * 'encodeOutput' : set to false to disable content encoding, and not send
     * the Vary header (default true)
     *
     * 'encodeMethod' : generally you should let this be determined by
     * HTTP_Encoder (leave null), but you can force a particular encoding
     * to be returned, by setting this to 'gzip' or '' (no encoding)
     *
     * 'encodeLevel' : level of encoding compression (0 to 9, default 9)
     *
     * 'contentTypeCharset' : appended to the Content-Type header sent. Set to a falsey
     * value to remove. (default 'utf-8')
     *
     * 'maxAge' : set this to the number of seconds the client should use its cache
     * before revalidating with the server. This sets Cache-Control: max-age and the
     * Expires header. Unlike the old 'setExpires' setting, this setting will NOT
     * prevent conditional GETs. Note this has nothing to do with server-side caching.
     *
     * 'rewriteCssUris' : If true, serve() will automatically set the 'currentDir'
     * minifier option to enable URI rewriting in CSS files (default true)
     *
     * 'bubbleCssImports' : If true, all @import declarations in combined CSS
     * files will be move to the top. Note this may alter effective CSS values
     * due to a change in order. (default false)
     *
     * 'debug' : set to true to minify all sources with the 'Lines' controller, which
     * eases the debugging of combined files. This also prevents 304 responses.
     * @see Minify_Lines::minify()
     *
     * 'concatOnly' : set to true to disable minification and simply concatenate the files.
     * For JS, no minifier will be used. For CSS, only URI rewriting is still performed.
     *
     * 'minifiers' : to override Minify's default choice of minifier function for
     * a particular content-type, specify your callback under the key of the
     * content-type:
     * <code>
     * // call customCssMinifier($css) for all CSS minification
     * $options['minifiers'][Minify::TYPE_CSS] = 'customCssMinifier';
     *
     * // don't minify Javascript at all
     * $options['minifiers'][Minify::TYPE_JS] = 'Minify::nullMinifier';
     * </code>
     *
     * 'minifierOptions' : to send options to the minifier function, specify your options
     * under the key of the content-type. E.g. To send the CSS minifier an option:
     * <code>
     * // give CSS minifier array('optionName' => 'optionValue') as 2nd argument
     * $options['minifierOptions'][Minify::TYPE_CSS]['optionName'] = 'optionValue';
     * </code>
     *
     * 'contentType' : (optional) this is only needed if your file extension is not
     * js/css/html. The given content-type will be sent regardless of source file
     * extension, so this should not be used in a Groups config with other
     * Javascript/CSS files.
     *
     * 'importWarning' : serve() will check CSS files for @import declarations that
     * appear too late in the combined stylesheet. If found, serve() will prepend
     * the output with this warning. To disable this, set this option to empty string.
     *
     * Any controller options are documented in that controller's createConfiguration() method.
     *
     * @param Minify_ControllerInterface $controller instance of subclass of Minify_Controller_Base
     *
     * @param array                      $options    controller/serve options
     *
     * @return null|array if the 'quiet' option is set to true, an array
     * with keys "success" (bool), "statusCode" (int), "content" (string), and
     * "headers" (array).
     *
     * @throws Exception
     */
    public function serve(\Minify_ControllerInterface $controller, $options = array())
    {
    }
    /**
     * Return combined minified content for a set of sources
     *
     * No internal caching will be used and the content will not be HTTP encoded.
     *
     * @param array $sources array of filepaths and/or Minify_Source objects
     *
     * @param array $options (optional) array of options for serve.
     *
     * @return string
     */
    public function combine($sources, $options = array())
    {
    }
    /**
     * Show an error page
     *
     * @param string $header  Full header. E.g. 'HTTP/1.0 500 Internal Server Error'
     * @param string $url     URL to direct the user to
     * @param string $msgHtml HTML message for the client
     *
     * @return void
     * @internal This is not part of the public API and is subject to change
     * @access private
     */
    public function errorExit($header, $url = '', $msgHtml = '')
    {
    }
    /**
     * Default minifier for .min or -min JS files.
     *
     * @param string $content
     * @return string
     */
    public static function nullMinifier($content)
    {
    }
    /**
     * Setup CSS sources for URI rewriting
     */
    protected function setupUriRewrites()
    {
    }
    /**
     * Set up sources to use Minify_Lines
     */
    protected function setupDebug()
    {
    }
    /**
     * Combines sources and minifies the result.
     *
     * @return string
     *
     * @throws Exception
     */
    protected function combineMinify()
    {
    }
    /**
     * Make a unique cache id for for this request.
     *
     * Any settings that could affect output are taken into consideration
     *
     * @param string $prefix
     *
     * @return string
     */
    protected function _getCacheId($prefix = 'minify')
    {
    }
    /**
     * Bubble CSS @imports to the top or prepend a warning if an import is detected not at the top.
     *
     * @param string $css
     *
     * @return string
     */
    protected function handleCssImports($css)
    {
    }
    /**
     * Analyze sources (if there are any) and set $options 'contentType'
     * and 'lastModifiedTime' if they already aren't.
     *
     * @param array $options options for Minify
     *
     * @return array options for Minify
     */
    protected function analyzeSources($options = array())
    {
    }
}
