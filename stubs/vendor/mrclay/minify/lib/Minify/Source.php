<?php

/**
 * Class Minify_Source
 * @package Minify
 */
/**
 * A content source to be minified by Minify.
 *
 * This allows per-source minification options and the mixing of files with
 * content from other sources.
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_Source implements \Minify_SourceInterface
{
    /**
     * @var int time of last modification
     */
    protected $lastModified;
    /**
     * @var callback minifier function specifically for this source.
     */
    protected $minifier;
    /**
     * @var array minification options specific to this source.
     */
    protected $minifyOptions = array();
    /**
     * @var string full path of file
     */
    protected $filepath;
    /**
     * @var string HTTP Content Type (Minify requires one of the constants Minify::TYPE_*)
     */
    protected $contentType;
    /**
     * @var string
     */
    protected $content;
    /**
     * @var callable
     */
    protected $getContentFunc;
    /**
     * @var string
     */
    protected $id;
    /**
     * Create a Minify_Source
     *
     * In the $spec array(), you can either provide a 'filepath' to an existing
     * file (existence will not be checked!) or give 'id' (unique string for
     * the content), 'content' (the string content) and 'lastModified'
     * (unixtime of last update).
     *
     * @param array $spec options
     */
    public function __construct($spec)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLastModified()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMinifier()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMinifier($minifier = \null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMinifierOptions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMinifierOptions(array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContentType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilePath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setupUriRewrites()
    {
    }
}
