<?php

class Minify_LessCssSource extends \Minify_Source
{
    /**
     * @inheritdoc
     */
    public function __construct(array $spec, \Minify_CacheInterface $cache)
    {
    }
    /**
     * Get last modified of all parsed files
     *
     * @return int
     */
    public function getLastModified()
    {
    }
    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
    }
}
