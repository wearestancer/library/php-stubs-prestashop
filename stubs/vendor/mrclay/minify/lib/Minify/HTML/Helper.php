<?php

/**
 * Class Minify_HTML_Helper
 * @package Minify
 */
/**
 * Helpers for writing Minify URIs into HTML
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_HTML_Helper
{
    public $rewriteWorks = \true;
    public $minAppUri = '/min';
    public $groupsConfigFile = '';
    /**
     * Get an HTML-escaped Minify URI for a group or set of files
     *
     * @param string|array $keyOrFiles a group key or array of filepaths/URIs
     * @param array $opts options:
     *   'farExpires' : (default true) append a modified timestamp for cache revving
     *   'debug' : (default false) append debug flag
     *   'charset' : (default 'UTF-8') for htmlspecialchars
     *   'minAppUri' : (default '/min') URI of min directory
     *   'rewriteWorks' : (default true) does mod_rewrite work in min app?
     *   'groupsConfigFile' : specify if different
     * @return string
     */
    public static function getUri($keyOrFiles, $opts = array())
    {
    }
    /**
     * Get non-HTML-escaped URI to minify the specified files
     *
     * @param bool $farExpires
     * @param bool $debug
     * @return string
     */
    public function getRawUri($farExpires = \true, $debug = \false)
    {
    }
    /**
     * Set the files that will comprise the URI we're building
     *
     * @param array $files
     * @param bool $checkLastModified
     */
    public function setFiles($files, $checkLastModified = \true)
    {
    }
    /**
     * Set the group of files that will comprise the URI we're building
     *
     * @param string $key
     * @param bool $checkLastModified
     */
    public function setGroup($key, $checkLastModified = \true)
    {
    }
    /**
     * Get the max(lastModified) of all files
     *
     * @param array|string $sources
     * @param int $lastModified
     * @return int
     */
    public static function getLastModified($sources, $lastModified = 0)
    {
    }
    /**
     * @param \Minify\App $app
     * @return \Minify\App
     * @internal
     */
    public static function app(\Minify\App $app = \null)
    {
    }
    protected $_groupKey = \null;
    // if present, URI will be like g=...
    protected $_filePaths = array();
    protected $_lastModified = \null;
    /**
     * In a given array of strings, find the character they all have at
     * a particular index
     *
     * @param array $arr array of strings
     * @param int $pos index to check
     * @return mixed a common char or '' if any do not match
     */
    protected static function _getCommonCharAtPos($arr, $pos)
    {
    }
    /**
     * Get the shortest URI to minify the set of source files
     *
     * @param array $paths root-relative URIs of files
     * @param string $minRoot root-relative URI of the "min" application
     * @return string
     */
    protected static function _getShortestUri($paths, $minRoot = '/min/')
    {
    }
}
