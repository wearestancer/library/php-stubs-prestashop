<?php

/**
 * Class Minify_YUICompressor
 * @package Minify
 */
/**
 * Compress Javascript/CSS using the YUI Compressor
 *
 * You must set $jarFile and $tempDir before calling the minify functions.
 * Also, depending on your shell's environment, you may need to specify
 * the full path to java in $javaExecutable or use putenv() to setup the
 * Java environment.
 *
 * <code>
 * Minify_YUICompressor::$jarFile = '/path/to/yuicompressor-2.4.6.jar';
 * Minify_YUICompressor::$tempDir = '/tmp';
 * $code = Minify_YUICompressor::minifyJs(
 *   $code
 *   ,array('nomunge' => true, 'line-break' => 1000)
 * );
 * </code>
 *
 * Note: In case you run out stack (default is 512k), you may increase stack size in $options:
 *   array('stack-size' => '2048k')
 *
 * @todo unit tests, $options docs
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_YUICompressor
{
    /**
     * Filepath of the YUI Compressor jar file. This must be set before
     * calling minifyJs() or minifyCss().
     *
     * @var string
     */
    public static $jarFile;
    /**
     * Writable temp directory. This must be set before calling minifyJs()
     * or minifyCss().
     *
     * @var string
     */
    public static $tempDir;
    /**
     * Filepath of "java" executable (may be needed if not in shell's PATH)
     *
     * @var string
     */
    public static $javaExecutable = 'java';
    /**
     * Minify a Javascript string
     *
     * @param string $js
     *
     * @param array $options (verbose is ignored)
     *
     * @see http://www.julienlecomte.net/yuicompressor/README
     *
     * @return string
     */
    public static function minifyJs($js, $options = array())
    {
    }
    /**
     * Minify a CSS string
     *
     * @param string $css
     *
     * @param array $options (verbose is ignored)
     *
     * @see http://www.julienlecomte.net/yuicompressor/README
     *
     * @return string
     */
    public static function minifyCss($css, $options = array())
    {
    }
}
