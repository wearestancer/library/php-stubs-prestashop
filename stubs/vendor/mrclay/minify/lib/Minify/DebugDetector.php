<?php

/**
 * Detect whether request should be debugged
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_DebugDetector
{
    public static function shouldDebugRequest(\Minify_Env $env)
    {
    }
}
