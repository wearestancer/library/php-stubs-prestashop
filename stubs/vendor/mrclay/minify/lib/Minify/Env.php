<?php

class Minify_Env
{
    /**
     * @return string
     */
    public function getDocRoot()
    {
    }
    /**
     * @return string
     */
    public function getRequestUri()
    {
    }
    public function __construct($options = array())
    {
    }
    public function server($key = \null)
    {
    }
    public function cookie($key = \null, $default = \null)
    {
    }
    public function get($key = \null, $default = \null)
    {
    }
    public function post($key = \null, $default = \null)
    {
    }
    /**
     * turn windows-style slashes into unix-style,
     * remove trailing slash
     * and lowercase drive letter
     *
     * @param string $path absolute path
     *
     * @return string
     */
    public function normalizePath($path)
    {
    }
    protected $server;
    protected $get;
    protected $post;
    protected $cookie;
    /**
     * Compute $_SERVER['DOCUMENT_ROOT'] for IIS using SCRIPT_FILENAME and SCRIPT_NAME.
     *
     * @param array $server
     * @return string
     */
    protected function computeDocRoot(array $server)
    {
    }
}
