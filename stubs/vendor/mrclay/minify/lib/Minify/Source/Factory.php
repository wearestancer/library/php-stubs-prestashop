<?php

class Minify_Source_Factory
{
    /**
     * @var array
     */
    protected $options;
    /**
     * @var callable[]
     */
    protected $handlers = array();
    /**
     * @var Minify_Env
     */
    protected $env;
    /**
     * @param Minify_Env            $env
     * @param array                 $options
     *
     *   noMinPattern        : Pattern matched against basename of the filepath (if present). If the pattern
     *                         matches, Minify will try to avoid re-compressing the resource.
     *
     *   fileChecker         : Callable responsible for verifying the existence of the file.
     *
     *   resolveDocRoot      : If true, a leading "//" will be replaced with the document root.
     *
     *   checkAllowDirs      : If true, the filepath will be verified to be within one of the directories
     *                         specified by allowDirs.
     *
     *   allowDirs           : Directory paths in which sources can be served.
     *
     *   uploaderHoursBehind : How many hours behind are the file modification times of uploaded files?
     *                         If you upload files from Windows to a non-Windows server, Windows may report
     *                         incorrect mtimes for the files. Immediately after modifying and uploading a
     *                         file, use the touch command to update the mtime on the server. If the mtime
     *                         jumps ahead by a number of hours, set this variable to that number. If the mtime
     *                         moves back, this should not be needed.
     *
     * @param Minify_CacheInterface $cache Optional cache for handling .less files.
     *
     */
    public function __construct(\Minify_Env $env, array $options = array(), \Minify_CacheInterface $cache = \null)
    {
    }
    /**
     * @param string   $basenamePattern A pattern tested against basename. E.g. "~\.css$~"
     * @param callable $handler         Function that recieves a $spec array and returns a Minify_SourceInterface
     */
    public function setHandler($basenamePattern, $handler)
    {
    }
    /**
     * @param string $file
     * @return string
     *
     * @throws Minify_Source_FactoryException
     */
    public function checkIsFile($file)
    {
    }
    /**
     * @param mixed $spec
     *
     * @return Minify_SourceInterface
     *
     * @throws Minify_Source_FactoryException
     */
    public function makeSource($spec)
    {
    }
}
