<?php

/**
 * Class Minify_CSS_Compressor
 * @package Minify
 */
/**
 * Compress CSS
 *
 * This is a heavy regex-based removal of whitespace, unnecessary
 * comments and tokens, and some CSS value minimization, where practical.
 * Many steps have been taken to avoid breaking comment-based hacks,
 * including the ie5/mac filter (and its inversion), but expect tricky
 * hacks involving comment tokens in 'content' value strings to break
 * minimization badly. A test suite is available.
 *
 * Note: This replaces a lot of spaces with line breaks. It's rumored
 * (https://github.com/yui/yuicompressor/blob/master/README.md#global-options)
 * that some source control tools and old browsers don't like very long lines.
 * Compressed files with shorter lines are also easier to diff. If this is
 * unacceptable please use CSSmin instead.
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 * @author http://code.google.com/u/1stvamp/ (Issue 64 patch)
 *
 * @deprecated Use CSSmin (tubalmartin/cssmin)
 */
class Minify_CSS_Compressor
{
    /**
     * Minify a CSS string
     *
     * @param string $css
     *
     * @param array $options (currently ignored)
     *
     * @return string
     */
    public static function process($css, $options = array())
    {
    }
    /**
     * @var array
     */
    protected $_options = \null;
    /**
     * Are we "in" a hack? I.e. are some browsers targetted until the next comment?
     *
     * @var bool
     */
    protected $_inHack = \false;
    /**
     * Minify a CSS string
     *
     * @param string $css
     *
     * @return string
     */
    protected function _process($css)
    {
    }
    /**
     * Replace what looks like a set of selectors
     *
     * @param array $m regex matches
     *
     * @return string
     */
    protected function _selectorsCB($m)
    {
    }
    /**
     * Process a comment and return a replacement
     *
     * @param array $m regex matches
     *
     * @return string
     */
    protected function _commentCB($m)
    {
    }
    /**
     * Process a font-family listing and return a replacement
     *
     * @param array $m regex matches
     *
     * @return string
     */
    protected function _fontFamilyCB($m)
    {
    }
}
