<?php

/**
 * Class Minify_HTML
 * @package Minify
 */
/**
 * Compress HTML
 *
 * This is a heavy regex-based removal of whitespace, unnecessary comments and
 * tokens. IE conditional comments are preserved. There are also options to have
 * STYLE and SCRIPT blocks compressed by callback functions.
 *
 * A test suite is available.
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_HTML
{
    /**
     * @var boolean
     */
    protected $_jsCleanComments = \true;
    /**
     * "Minify" an HTML page
     *
     * @param string $html
     *
     * @param array $options
     *
     * 'cssMinifier' : (optional) callback function to process content of STYLE
     * elements.
     *
     * 'jsMinifier' : (optional) callback function to process content of SCRIPT
     * elements. Note: the type attribute is ignored.
     *
     * 'xhtml' : (optional boolean) should content be treated as XHTML1.0? If
     * unset, minify will sniff for an XHTML doctype.
     *
     * @return string
     */
    public static function minify($html, $options = array())
    {
    }
    /**
     * Create a minifier object
     *
     * @param string $html
     *
     * @param array $options
     *
     * 'cssMinifier' : (optional) callback function to process content of STYLE
     * elements.
     *
     * 'jsMinifier' : (optional) callback function to process content of SCRIPT
     * elements. Note: the type attribute is ignored.
     *
     * 'jsCleanComments' : (optional) whether to remove HTML comments beginning and end of script block
     *
     * 'xhtml' : (optional boolean) should content be treated as XHTML1.0? If
     * unset, minify will sniff for an XHTML doctype.
     */
    public function __construct($html, $options = array())
    {
    }
    /**
     * Minify the markeup given in the constructor
     *
     * @return string
     */
    public function process()
    {
    }
    protected function _commentCB($m)
    {
    }
    protected function _reservePlace($content)
    {
    }
    protected $_isXhtml;
    protected $_replacementHash;
    protected $_placeholders = array();
    protected $_cssMinifier;
    protected $_jsMinifier;
    protected function _removePreCB($m)
    {
    }
    protected function _removeTextareaCB($m)
    {
    }
    protected function _removeStyleCB($m)
    {
    }
    protected function _removeScriptCB($m)
    {
    }
    protected function _removeCdata($str)
    {
    }
    protected function _needsCdata($str)
    {
    }
}
