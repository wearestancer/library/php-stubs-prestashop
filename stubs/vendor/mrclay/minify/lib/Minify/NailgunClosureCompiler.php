<?php

/**
 * Class Minify_ClosureCompiler
 * @package Minify
 */
/**
 * Run Closure Compiler via NailGun
 *
 * @package Minify
 * @author Elan Ruusamäe <glen@delfi.ee>
 * @link https://github.com/martylamb/nailgun
 */
class Minify_NailgunClosureCompiler extends \Minify_ClosureCompiler
{
    const NG_SERVER = 'com.martiansoftware.nailgun.NGServer';
    const CC_MAIN = 'com.google.javascript.jscomp.CommandLineRunner';
    /**
     * Filepath of "ng" executable (from Nailgun package)
     *
     * @var string
     */
    public static $ngExecutable = 'ng';
    /**
     * Filepath of the Nailgun jar file.
     *
     * @var string
     */
    public static $ngJarFile;
    /**
     * Get command to launch NailGun server.
     *
     * @return array
     */
    protected function getServerCommandLine()
    {
    }
    /**
     * @return array
     * @throws Minify_ClosureCompiler_Exception
     */
    protected function getCompilerCommandLine()
    {
    }
    /**
     * @param string $tmpFile
     * @param array $options
     * @return string
     * @throws Minify_ClosureCompiler_Exception
     */
    protected function compile($tmpFile, $options)
    {
    }
}
