<?php

/**
 * Class Minify_CommentPreserver
 * @package Minify
 */
/**
 * Process a string in pieces preserving C-style comments that begin with "/*!"
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_CommentPreserver
{
    /**
     * String to be prepended to each preserved comment
     *
     * @var string
     */
    public static $prepend = "\n";
    /**
     * String to be appended to each preserved comment
     *
     * @var string
     */
    public static $append = "\n";
    /**
     * Process a string outside of C-style comments that begin with "/*!"
     *
     * On each non-empty string outside these comments, the given processor
     * function will be called. The comments will be surrounded by
     * Minify_CommentPreserver::$preprend and Minify_CommentPreserver::$append.
     *
     * @param string $content
     * @param callback $processor function
     * @param array $args array of extra arguments to pass to the processor
     * function (default = array())
     * @return string
     */
    public static function process($content, $processor, $args = array())
    {
    }
}
