<?php

namespace Minify;

/**
 * @property Minify_CacheInterface            $cache
 * @property Config                           $config
 * @property string                           $configPath
 * @property Minify_ControllerInterface      $controller
 * @property string                           $dir
 * @property string                           $docRoot
 * @property Minify_Env                      $env
 * @property Monolog\Handler\ErrorLogHandler $errorLogHandler
 * @property array                            $groupsConfig
 * @property string                           $groupsConfigPath
 * @property LoggerInterface         $logger
 * @property \Minify                          $minify
 * @property array                            $serveOptions
 * @property Minify_Source_Factory           $sourceFactory
 * @property array                            $sourceFactoryOptions
 */
class App extends \Props\Container
{
    /**
     * Constructor
     *
     * @param string $dir Directory containing config files
     */
    public function __construct($dir)
    {
    }
    public function runServer()
    {
    }
}
