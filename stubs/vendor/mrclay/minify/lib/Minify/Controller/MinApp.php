<?php

/**
 * Class Minify_Controller_MinApp
 * @package Minify
 */
/**
 * Controller class for requests to /min/index.php
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_Controller_MinApp extends \Minify_Controller_Base
{
    /**
     * Set up groups of files as sources
     *
     * @param array $options controller and Minify options
     *
     * @return array Minify options
     */
    public function createConfiguration(array $options)
    {
    }
}
