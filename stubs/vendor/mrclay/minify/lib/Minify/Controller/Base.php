<?php

/**
 * Base class for Minify controller
 *
 * The controller class validates a request and uses it to create a configuration for Minify::serve().
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
abstract class Minify_Controller_Base implements \Minify_ControllerInterface
{
    /**
     * @var Minify_Env
     */
    protected $env;
    /**
     * @var Minify_Source_Factory
     */
    protected $sourceFactory;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @param Minify_Env            $env
     * @param Minify_Source_Factory $sourceFactory
     * @param LoggerInterface       $logger
     */
    public function __construct(\Minify_Env $env, \Minify_Source_Factory $sourceFactory, \Psr\Log\LoggerInterface $logger = \null)
    {
    }
    /**
     * Create controller sources and options for Minify::serve()
     *
     * @param array $options controller and Minify options
     *
     * @return Minify_ServeConfiguration
     */
    public abstract function createConfiguration(array $options);
    /**
     * Send message to the Minify logger
     *
     * @param string $msg
     *
     * @return null
     * @deprecated use $this->logger
     */
    public function log($msg)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEnv()
    {
    }
}
