<?php

/**
 * Class Minify_Controller_Groups
 * @package Minify
 */
/**
 * Controller class for serving predetermined groups of minimized sets, selected
 * by PATH_INFO
 *
 * <code>
 * Minify::serve('Groups', array(
 *     'groups' => array(
 *         'css' => array('//css/type.css', '//css/layout.css')
 *        ,'js' => array('//js/jquery.js', '//js/site.js')
 *     )
 * ));
 * </code>
 *
 * If the above code were placed in /serve.php, it would enable the URLs
 * /serve.php/js and /serve.php/css
 *
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */
class Minify_Controller_Groups extends \Minify_Controller_Files
{
    /**
     * Set up groups of files as sources
     *
     * @param array $options controller and Minify options
     *
     * 'groups': (required) array mapping PATH_INFO strings to arrays
     * of complete file paths. @see Minify_Controller_Groups
     *
     * @return array Minify options
     */
    public function createConfiguration(array $options)
    {
    }
}
