<?php

/**
 * Class Minify_Cache_Wincache
 * @package Minify
 */
/**
 * WinCache-based cache class for Minify
 *
 * <code>
 * Minify::setCache(new Minify_Cache_WinCache());
 * </code>
 *
 * @package Minify
 * @author Matthias Fax
 **/
class Minify_Cache_WinCache implements \Minify_CacheInterface
{
    /**
     * Create a Minify_Cache_Wincache object, to be passed to
     * Minify::setCache().
     *
     * @param int $expire seconds until expiration (default = 0
     * meaning the item will not get an expiration date)
     *
     * @throws Exception
     */
    public function __construct($expire = 0)
    {
    }
    /**
     * Write data to cache.
     *
     * @param string $id cache id
     *
     * @param string $data
     *
     * @return bool success
     */
    public function store($id, $data)
    {
    }
    /**
     * Get the size of a cache entry
     *
     * @param string $id cache id
     *
     * @return int size in bytes
     */
    public function getSize($id)
    {
    }
    /**
     * Does a valid cache entry exist?
     *
     * @param string $id cache id
     *
     * @param int $srcMtime mtime of the original source file(s)
     *
     * @return bool exists
     */
    public function isValid($id, $srcMtime)
    {
    }
    /**
     * Send the cached content to output
     *
     * @param string $id cache id
     */
    public function display($id)
    {
    }
    /**
     * Fetch the cached content
     *
     * @param string $id cache id
     *
     * @return string
     */
    public function fetch($id)
    {
    }
}
