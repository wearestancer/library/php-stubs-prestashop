<?php

/**
 * Class Minify_Cache_ZendPlatform
 * @package Minify
 */
/**
 * ZendPlatform-based cache class for Minify
 *
 * Based on Minify_Cache_APC, uses output_cache_get/put (currently deprecated)
 *
 * <code>
 * Minify::setCache(new Minify_Cache_ZendPlatform());
 * </code>
 *
 * @package Minify
 * @author Patrick van Dissel
 */
class Minify_Cache_ZendPlatform implements \Minify_CacheInterface
{
    /**
     * Create a Minify_Cache_ZendPlatform object, to be passed to
     * Minify::setCache().
     *
     * @param int $expire seconds until expiration (default = 0
     * meaning the item will not get an expiration date)
     *
     */
    public function __construct($expire = 0)
    {
    }
    /**
     * Write data to cache.
     *
     * @param string $id cache id
     *
     * @param string $data
     *
     * @return bool success
     */
    public function store($id, $data)
    {
    }
    /**
     * Get the size of a cache entry
     *
     * @param string $id cache id
     *
     * @return int size in bytes
     */
    public function getSize($id)
    {
    }
    /**
     * Does a valid cache entry exist?
     *
     * @param string $id cache id
     *
     * @param int $srcMtime mtime of the original source file(s)
     *
     * @return bool exists
     */
    public function isValid($id, $srcMtime)
    {
    }
    /**
     * Send the cached content to output
     *
     * @param string $id cache id
     */
    public function display($id)
    {
    }
    /**
     * Fetch the cached content
     *
     * @param string $id cache id
     *
     * @return string
     */
    public function fetch($id)
    {
    }
}
