<?php

class Minify_Cache_File implements \Minify_CacheInterface
{
    /**
     * @param string          $path
     * @param bool            $fileLocking
     * @param LoggerInterface $logger
     */
    public function __construct($path = '', $fileLocking = \false, \Psr\Log\LoggerInterface $logger = \null)
    {
    }
    /**
     * Write data to cache.
     *
     * @param string $id cache id (e.g. a filename)
     *
     * @param string $data
     *
     * @return bool success
     */
    public function store($id, $data)
    {
    }
    /**
     * Get the size of a cache entry
     *
     * @param string $id cache id (e.g. a filename)
     *
     * @return int size in bytes
     */
    public function getSize($id)
    {
    }
    /**
     * Does a valid cache entry exist?
     *
     * @param string $id cache id (e.g. a filename)
     *
     * @param int $srcMtime mtime of the original source file(s)
     *
     * @return bool exists
     */
    public function isValid($id, $srcMtime)
    {
    }
    /**
     * Send the cached content to output
     *
     * @param string $id cache id (e.g. a filename)
     */
    public function display($id)
    {
    }
    /**
     * Fetch the cached content
     *
     * @param string $id cache id (e.g. a filename)
     *
     * @return string
     */
    public function fetch($id)
    {
    }
    /**
     * Fetch the cache path used
     *
     * @return string
     */
    public function getPath()
    {
    }
    /**
     * Get a usable temp directory
     *
     * @return string
     * @deprecated
     */
    public static function tmp()
    {
    }
    /**
     * Send message to the Minify logger
     * @param string $msg
     * @return null
     * @deprecated Use $this->logger
     */
    protected function _log($msg)
    {
    }
}
