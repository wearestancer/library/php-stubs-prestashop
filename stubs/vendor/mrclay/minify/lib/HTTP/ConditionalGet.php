<?php

/**
 * Class HTTP_ConditionalGet
 * @package Minify
 * @subpackage HTTP
 */
/**
 * Implement conditional GET via a timestamp or hash of content
 *
 * E.g. Content from DB with update time:
 * <code>
 * list($updateTime, $content) = getDbUpdateAndContent();
 * $cg = new HTTP_ConditionalGet(array(
 *     'lastModifiedTime' => $updateTime
 *     ,'isPublic' => true
 * ));
 * $cg->sendHeaders();
 * if ($cg->cacheIsValid) {
 *     exit();
 * }
 * echo $content;
 * </code>
 *
 * E.g. Shortcut for the above
 * <code>
 * HTTP_ConditionalGet::check($updateTime, true); // exits if client has cache
 * echo $content;
 * </code>
 *
 * E.g. Content from DB with no update time:
 * <code>
 * $content = getContentFromDB();
 * $cg = new HTTP_ConditionalGet(array(
 *     'contentHash' => md5($content)
 * ));
 * $cg->sendHeaders();
 * if ($cg->cacheIsValid) {
 *     exit();
 * }
 * echo $content;
 * </code>
 *
 * E.g. Static content with some static includes:
 * <code>
 * // before content
 * $cg = new HTTP_ConditionalGet(array(
 *     'lastUpdateTime' => max(
 *         filemtime(__FILE__)
 *         ,filemtime('/path/to/header.inc')
 *         ,filemtime('/path/to/footer.inc')
 *     )
 * ));
 * $cg->sendHeaders();
 * if ($cg->cacheIsValid) {
 *     exit();
 * }
 * </code>
 * @package Minify
 * @subpackage HTTP
 * @author Stephen Clay <steve@mrclay.org>
 */
class HTTP_ConditionalGet
{
    /**
     * Does the client have a valid copy of the requested resource?
     *
     * You'll want to check this after instantiating the object. If true, do
     * not send content, just call sendHeaders() if you haven't already.
     *
     * @var bool
     */
    public $cacheIsValid = \null;
    /**
     * @param array $spec options
     *
     * 'isPublic': (bool) if false, the Cache-Control header will contain
     * "private", allowing only browser caching. (default false)
     *
     * 'lastModifiedTime': (int) if given, both ETag AND Last-Modified headers
     * will be sent with content. This is recommended.
     *
     * 'encoding': (string) if set, the header "Vary: Accept-Encoding" will
     * always be sent and a truncated version of the encoding will be appended
     * to the ETag. E.g. "pub123456;gz". This will also trigger a more lenient
     * checking of the client's If-None-Match header, as the encoding portion of
     * the ETag will be stripped before comparison.
     *
     * 'contentHash': (string) if given, only the ETag header can be sent with
     * content (only HTTP1.1 clients can conditionally GET). The given string
     * should be short with no quote characters and always change when the
     * resource changes (recommend md5()). This is not needed/used if
     * lastModifiedTime is given.
     *
     * 'eTag': (string) if given, this will be used as the ETag header rather
     * than values based on lastModifiedTime or contentHash. Also the encoding
     * string will not be appended to the given value as described above.
     *
     * 'invalidate': (bool) if true, the client cache will be considered invalid
     * without testing. Effectively this disables conditional GET.
     * (default false)
     *
     * 'maxAge': (int) if given, this will set the Cache-Control max-age in
     * seconds, and also set the Expires header to the equivalent GMT date.
     * After the max-age period has passed, the browser will again send a
     * conditional GET to revalidate its cache.
     */
    public function __construct($spec)
    {
    }
    /**
     * Get array of output headers to be sent
     *
     * In the case of 304 responses, this array will only contain the response
     * code header: array('_responseCode' => 'HTTP/1.0 304 Not Modified')
     *
     * Otherwise something like:
     * <code>
     * array(
     *     'Cache-Control' => 'max-age=0, public'
     *     ,'ETag' => '"foobar"'
     * )
     * </code>
     *
     * @return array
     */
    public function getHeaders()
    {
    }
    /**
     * Set the Content-Length header in bytes
     *
     * With most PHP configs, as long as you don't flush() output, this method
     * is not needed and PHP will buffer all output and set Content-Length for
     * you. Otherwise you'll want to call this to let the client know up front.
     *
     * @param int $bytes
     *
     * @return int copy of input $bytes
     */
    public function setContentLength($bytes)
    {
    }
    /**
     * Send headers
     *
     * @see getHeaders()
     *
     * Note this doesn't "clear" the headers. Calling sendHeaders() will
     * call header() again (but probably have not effect) and getHeaders() will
     * still return the headers.
     *
     * @return null
     */
    public function sendHeaders()
    {
    }
    /**
     * Exit if the client's cache is valid for this resource
     *
     * This is a convenience method for common use of the class
     *
     * @param int $lastModifiedTime if given, both ETag AND Last-Modified headers
     * will be sent with content. This is recommended.
     *
     * @param bool $isPublic (default false) if true, the Cache-Control header
     * will contain "public", allowing proxies to cache the content. Otherwise
     * "private" will be sent, allowing only browser caching.
     *
     * @param array $options (default empty) additional options for constructor
     */
    public static function check($lastModifiedTime = \null, $isPublic = \false, $options = array())
    {
    }
    /**
     * Get a GMT formatted date for use in HTTP headers
     *
     * <code>
     * header('Expires: ' . HTTP_ConditionalGet::gmtdate($time));
     * </code>
     *
     * @param int $time unix timestamp
     *
     * @return string
     */
    public static function gmtDate($time)
    {
    }
    protected $_headers = array();
    protected $_lmTime = \null;
    protected $_etag = \null;
    protected $_stripEtag = \false;
    /**
     * @param string $hash
     *
     * @param string $scope
     */
    protected function _setEtag($hash, $scope)
    {
    }
    /**
     * @param int $time
     */
    protected function _setLastModified($time)
    {
    }
    /**
     * Determine validity of client cache and queue 304 header if valid
     *
     * @return bool
     */
    protected function _isCacheValid()
    {
    }
    /**
     * @return bool
     */
    protected function resourceMatchedEtag()
    {
    }
    /**
     * @param string $etag
     *
     * @return string
     */
    protected function normalizeEtag($etag)
    {
    }
    /**
     * @return bool
     */
    protected function resourceNotModified()
    {
    }
}
