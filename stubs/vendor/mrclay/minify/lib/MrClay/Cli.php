<?php

namespace MrClay;

/**
 * Forms a front controller for a console app, handling and validating arguments (options)
 *
 * Instantiate, add arguments, then call validate(). Afterwards, the user's valid arguments
 * and their values will be available in $cli->values.
 *
 * You may also specify that some arguments be used to provide input/output. By communicating
 * solely through the file pointers provided by openInput()/openOutput(), you can make your
 * app more flexible to end users.
 *
 * @author Steve Clay <steve@mrclay.org>
 * @license http://www.opensource.org/licenses/mit-license.php  MIT License
 */
class Cli
{
    /**
     * @var array validation errors
     */
    public $errors = array();
    /**
     * @var array option values available after validation.
     *
     * E.g. array(
     *      'a' => false              // option was missing
     *     ,'b' => true               // option was present
     *     ,'c' => "Hello"            // option had value
     *     ,'f' => "/home/user/file"  // file path from root
     *     ,'f.raw' => "~/file"       // file path as given to option
     * )
     */
    public $values = array();
    /**
     * @var array
     */
    public $moreArgs = array();
    /**
     * @var array
     */
    public $debug = array();
    /**
     * @var bool The user wants help info
     */
    public $isHelpRequest = false;
    /**
     * @var Arg[]
     */
    protected $_args = array();
    /**
     * @var resource
     */
    protected $_stdin = null;
    /**
     * @var resource
     */
    protected $_stdout = null;
    /**
     * @param bool $exitIfNoStdin (default true) Exit() if STDIN is not defined
     */
    public function __construct($exitIfNoStdin = true)
    {
    }
    /**
     * @param Arg|string $letter
     * @return Arg
     */
    public function addOptionalArg($letter)
    {
    }
    /**
     * @param Arg|string $letter
     * @return Arg
     */
    public function addRequiredArg($letter)
    {
    }
    /**
     * @param string $letter
     * @param bool $required
     * @param Arg|null $arg
     * @return Arg
     * @throws InvalidArgumentException
     */
    public function addArgument($letter, $required, \MrClay\Cli\Arg $arg = null)
    {
    }
    /**
     * @param string $letter
     * @return Arg|null
     */
    public function getArgument($letter)
    {
    }
    /*
     * Read and validate options
     *
     * @return bool true if all options are valid
     */
    public function validate()
    {
    }
    /**
     * Get the full paths of file(s) passed in as unspecified arguments
     *
     * @return array
     */
    public function getPathArgs()
    {
    }
    /**
     * Get a short list of errors with options
     *
     * @return string
     */
    public function getErrorReport()
    {
    }
    /**
     * @return string
     */
    public function getArgumentsListing()
    {
    }
    /**
     * Get resource of open input stream. May be STDIN or a file pointer
     * to the file specified by an option with 'STDIN'.
     *
     * @return resource
     */
    public function openInput()
    {
    }
    public function closeInput()
    {
    }
    /**
     * Get resource of open output stream. May be STDOUT or a file pointer
     * to the file specified by an option with 'STDOUT'. The file will be
     * truncated to 0 bytes on opening.
     *
     * @return resource
     */
    public function openOutput()
    {
    }
    public function closeOutput()
    {
    }
    /**
     * @param string $letter
     * @param string $msg
     * @param string $value
     */
    protected function addError($letter, $msg, $value = null)
    {
    }
}
