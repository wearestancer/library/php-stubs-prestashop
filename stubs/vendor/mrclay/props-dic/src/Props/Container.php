<?php

namespace Props;

/**
 * Container holding values which can be resolved upon reading and optionally stored and shared
 * across reads.
 *
 * Values are read/set as properties.
 *
 * @note see scripts/example.php
 */
class Container implements \Psr\Container\ContainerInterface
{
    /**
     * Fetch a value.
     *
     * @param string $name
     * @return mixed
     * @throws FactoryUncallableException|ValueUnresolvableException|NotFoundException
     */
    public function __get($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * Set a value.
     *
     * @param string $name
     * @param mixed $value
     * @throws \InvalidArgumentException
     */
    public function __set($name, $value)
    {
    }
    /**
     * Set a value to be later returned as is. You only need to use this if you wish to store
     * a Closure.
     *
     * @param string $name
     * @param mixed $value
     * @throws \InvalidArgumentException
     */
    public function setValue($name, $value)
    {
    }
    /**
     * @param string $name
     */
    public function __unset($name)
    {
    }
    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
    }
    /**
     * Fetch a freshly-resolved value.
     *
     * @param string $method method name must start with "new_"
     * @param array $args
     * @return mixed
     * @throws BadMethodCallException
     */
    public function __call($method, $args)
    {
    }
    /**
     * Can we fetch a new value via new_$name()?
     *
     * @param string $name
     * @return bool
     */
    public function hasFactory($name)
    {
    }
    /**
     * Set a factory to generate a value when the container is read.
     *
     * @param string   $name    The name of the value
     * @param callable $factory Factory for the value
     * @throws FactoryUncallableException
     */
    public function setFactory($name, $factory)
    {
    }
    /**
     * Get an already-set factory callable (Closure, invokable, or callback)
     *
     * @param string $name The name of the value
     * @return callable
     * @throws NotFoundException
     */
    public function getFactory($name)
    {
    }
    /**
     * Add a function that gets applied to the return value of an existing factory
     *
     * @note A cached value (from a previous property read) will thrown away. The next property read
     *       (and all new_NAME() calls) will call the original factory.
     *
     * @param string   $name     The name of the value
     * @param callable $extender Function that is applied to extend the returned value
     * @return \Closure
     * @throws FactoryUncallableException|NotFoundException
     */
    public function extend($name, $extender)
    {
    }
    /**
     * Get all keys available
     *
     * @return string[]
     */
    public function getKeys()
    {
    }
}
