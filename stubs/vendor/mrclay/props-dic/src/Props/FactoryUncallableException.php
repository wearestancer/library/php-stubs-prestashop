<?php

namespace Props;

class FactoryUncallableException extends \Exception implements \Psr\Container\ContainerExceptionInterface
{
}
