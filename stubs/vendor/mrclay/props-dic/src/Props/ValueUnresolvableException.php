<?php

namespace Props;

class ValueUnresolvableException extends \Exception implements \Psr\Container\ContainerExceptionInterface
{
}
