<?php

namespace Props;

class BadMethodCallException extends \Exception implements \Psr\Container\ContainerExceptionInterface
{
}
