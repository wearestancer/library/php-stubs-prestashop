<?php

namespace Props;

class NotFoundException extends \Exception implements \Psr\Container\ContainerExceptionInterface, \Psr\Container\NotFoundExceptionInterface
{
}
