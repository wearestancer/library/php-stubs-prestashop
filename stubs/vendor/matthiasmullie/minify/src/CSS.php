<?php

namespace MatthiasMullie\Minify;

/**
 * CSS minifier
 *
 * Please report bugs on https://github.com/matthiasmullie/minify/issues
 *
 * @package Minify
 * @author Matthias Mullie <minify@mullie.eu>
 * @author Tijs Verkoyen <minify@verkoyen.eu>
 * @copyright Copyright (c) 2012, Matthias Mullie. All rights reserved
 * @license MIT License
 */
class CSS extends \MatthiasMullie\Minify\Minify
{
    /**
     * @var int maximum inport size in kB
     */
    protected $maxImportSize = 5;
    /**
     * @var string[] valid import extensions
     */
    protected $importExtensions = array('gif' => 'data:image/gif', 'png' => 'data:image/png', 'jpe' => 'data:image/jpeg', 'jpg' => 'data:image/jpeg', 'jpeg' => 'data:image/jpeg', 'svg' => 'data:image/svg+xml', 'woff' => 'data:application/x-font-woff', 'woff2' => 'data:application/x-font-woff2', 'avif' => 'data:image/avif', 'apng' => 'data:image/apng', 'webp' => 'data:image/webp', 'tif' => 'image/tiff', 'tiff' => 'image/tiff', 'xbm' => 'image/x-xbitmap');
    /**
     * Set the maximum size if files to be imported.
     *
     * Files larger than this size (in kB) will not be imported into the CSS.
     * Importing files into the CSS as data-uri will save you some connections,
     * but we should only import relatively small decorative images so that our
     * CSS file doesn't get too bulky.
     *
     * @param int $size Size in kB
     */
    public function setMaxImportSize($size)
    {
    }
    /**
     * Set the type of extensions to be imported into the CSS (to save network
     * connections).
     * Keys of the array should be the file extensions & respective values
     * should be the data type.
     *
     * @param string[] $extensions Array of file extensions
     */
    public function setImportExtensions(array $extensions)
    {
    }
    /**
     * Move any import statements to the top.
     *
     * @param string $content Nearly finished CSS content
     *
     * @return string
     */
    protected function moveImportsToTop($content)
    {
    }
    /**
     * Combine CSS from import statements.
     *
     * @import's will be loaded and their content merged into the original file,
     * to save HTTP requests.
     *
     * @param string   $source  The file to combine imports for
     * @param string   $content The CSS content to combine imports for
     * @param string[] $parents Parent paths, for circular reference checks
     *
     * @return string
     *
     * @throws FileImportException
     */
    protected function combineImports($source, $content, $parents)
    {
    }
    /**
     * Import files into the CSS, base64-ized.
     *
     * @url(image.jpg) images will be loaded and their content merged into the
     * original file, to save HTTP requests.
     *
     * @param string $source  The file to import files for
     * @param string $content The CSS content to import files for
     *
     * @return string
     */
    protected function importFiles($source, $content)
    {
    }
    /**
     * Minify the data.
     * Perform CSS optimizations.
     *
     * @param string[optional] $path    Path to write the data to
     * @param string[]         $parents Parent paths, for circular reference checks
     *
     * @return string The minified data
     */
    public function execute($path = null, $parents = array())
    {
    }
    /**
     * Moving a css file should update all relative urls.
     * Relative references (e.g. ../images/image.gif) in a certain css file,
     * will have to be updated when a file is being saved at another location
     * (e.g. ../../images/image.gif, if the new CSS file is 1 folder deeper).
     *
     * @param ConverterInterface $converter Relative path converter
     * @param string             $content   The CSS content to update relative urls for
     *
     * @return string
     */
    protected function move(\MatthiasMullie\PathConverter\ConverterInterface $converter, $content)
    {
    }
    /**
     * Shorthand hex color codes.
     * #FF0000 -> #F00.
     *
     * @param string $content The CSS content to shorten the hex color codes for
     *
     * @return string
     */
    protected function shortenColors($content)
    {
    }
    /**
     * Shorten CSS font weights.
     *
     * @param string $content The CSS content to shorten the font weights for
     *
     * @return string
     */
    protected function shortenFontWeights($content)
    {
    }
    /**
     * Shorthand 0 values to plain 0, instead of e.g. -0em.
     *
     * @param string $content The CSS content to shorten the zero values for
     *
     * @return string
     */
    protected function shortenZeroes($content)
    {
    }
    /**
     * Strip empty tags from source code.
     *
     * @param string $content
     *
     * @return string
     */
    protected function stripEmptyTags($content)
    {
    }
    /**
     * Strip comments from source code.
     */
    protected function stripComments()
    {
    }
    /**
     * Strip whitespace.
     *
     * @param string $content The CSS content to strip the whitespace for
     *
     * @return string
     */
    protected function stripWhitespace($content)
    {
    }
    /**
     * Replace all occurrences of functions that may contain math, where
     * whitespace around operators needs to be preserved (e.g. calc, clamp)
     */
    protected function extractMath()
    {
    }
    /**
     * Replace custom properties, whose values may be used in scenarios where
     * we wouldn't want them to be minified (e.g. inside calc)
     */
    protected function extractCustomProperties()
    {
    }
    /**
     * Check if file is small enough to be imported.
     *
     * @param string $path The path to the file
     *
     * @return bool
     */
    protected function canImportBySize($path)
    {
    }
    /**
     * Check if file a file can be imported, going by the path.
     *
     * @param string $path
     *
     * @return bool
     */
    protected function canImportByPath($path)
    {
    }
    /**
     * Return a converter to update relative paths to be relative to the new
     * destination.
     *
     * @param string $source
     * @param string $target
     *
     * @return ConverterInterface
     */
    protected function getPathConverter($source, $target)
    {
    }
}
