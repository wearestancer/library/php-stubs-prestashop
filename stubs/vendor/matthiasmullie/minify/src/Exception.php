<?php

namespace MatthiasMullie\Minify;

/**
 * Base Exception Class
 * @deprecated Use Exceptions\BasicException instead
 *
 * @package Minify
 * @author Matthias Mullie <minify@mullie.eu>
 */
abstract class Exception extends \Exception
{
}
