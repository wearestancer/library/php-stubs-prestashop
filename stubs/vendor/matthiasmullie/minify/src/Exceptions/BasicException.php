<?php

namespace MatthiasMullie\Minify\Exceptions;

/**
 * Basic Exception Class
 *
 * @package Minify\Exception
 * @author Matthias Mullie <minify@mullie.eu>
 */
abstract class BasicException extends \MatthiasMullie\Minify\Exception
{
}
