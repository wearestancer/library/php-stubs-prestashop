<?php

namespace MatthiasMullie\Minify\Exceptions;

/**
 * IO Exception Class
 *
 * @package Minify\Exception
 * @author Matthias Mullie <minify@mullie.eu>
 */
class IOException extends \MatthiasMullie\Minify\Exceptions\BasicException
{
}
