<?php

namespace MatthiasMullie\Minify\Exceptions;

/**
 * File Import Exception Class
 *
 * @package Minify\Exception
 * @author Matthias Mullie <minify@mullie.eu>
 */
class FileImportException extends \MatthiasMullie\Minify\Exceptions\BasicException
{
}
