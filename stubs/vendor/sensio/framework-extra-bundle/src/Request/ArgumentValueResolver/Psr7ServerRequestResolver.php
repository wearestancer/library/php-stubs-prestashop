<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Request\ArgumentValueResolver;

/**
 * Injects the RequestInterface, MessageInterface or ServerRequestInterface when requested.
 *
 * @author Iltar van der Berg <kjarli@gmail.com>
 */
final class Psr7ServerRequestResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface
{
    public function __construct(\Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface $httpMessageFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument)
    {
    }
}
