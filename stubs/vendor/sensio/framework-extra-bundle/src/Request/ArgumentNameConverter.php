<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Request;

/**
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class ArgumentNameConverter
{
    public function __construct(\Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactoryInterface $argumentMetadataFactory)
    {
    }
    /**
     * Returns an associative array of the controller arguments for the event.
     *
     * @return array
     */
    public function getControllerArguments(\Symfony\Component\HttpKernel\Event\ControllerArgumentsEvent $event)
    {
    }
}
