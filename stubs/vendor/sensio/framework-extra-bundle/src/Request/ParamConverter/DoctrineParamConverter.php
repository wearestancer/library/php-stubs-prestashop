<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter;

/**
 * DoctrineParamConverter.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DoctrineParamConverter implements \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry = null, \Symfony\Component\ExpressionLanguage\ExpressionLanguage $expressionLanguage = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws \LogicException       When unable to guess how to get a Doctrine instance from the request information
     * @throws NotFoundHttpException When object not found
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, \Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter $configuration)
    {
    }
}
