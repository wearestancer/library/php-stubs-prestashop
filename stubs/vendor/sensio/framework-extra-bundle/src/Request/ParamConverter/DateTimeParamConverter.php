<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter;

/**
 * Convert DateTime instances from request attribute variable.
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 */
class DateTimeParamConverter implements \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws NotFoundHttpException When invalid date given
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, \Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter $configuration)
    {
    }
}
