<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter;

/**
 * Managers converters.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Henrik Bjornskov <henrik@bjrnskov.dk>
 */
class ParamConverterManager
{
    /**
     * Applies all converters to the passed configurations and stops when a
     * converter is applied it will move on to the next configuration and so on.
     *
     * @param array|object $configurations
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, $configurations)
    {
    }
    /**
     * Adds a parameter converter.
     *
     * Converters match either explicitly via $name or by iteration over all
     * converters with a $priority. If you pass a $priority = null then the
     * added converter will not be part of the iteration chain and can only
     * be invoked explicitly.
     *
     * @param int    $priority the priority (between -10 and 10)
     * @param string $name     name of the converter
     */
    public function add(\Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface $converter, $priority = 0, $name = null)
    {
    }
    /**
     * Returns all registered param converters.
     *
     * @return array An array of param converters
     */
    public function all()
    {
    }
}
