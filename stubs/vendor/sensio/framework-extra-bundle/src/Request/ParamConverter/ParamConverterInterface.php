<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter;

/**
 * Converts request parameters to objects and stores them as request
 * attributes, so they can be injected as controller method arguments.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface ParamConverterInterface
{
    /**
     * Stores the object in the request.
     *
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return bool True if the object has been successfully set, else false
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, \Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter $configuration);
    /**
     * Checks if the object is supported.
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(\Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter $configuration);
}
