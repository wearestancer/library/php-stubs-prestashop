<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Routing;

/**
 * AnnotatedRouteControllerLoader is an implementation of AnnotationClassLoader
 * that sets the '_controller' default based on the class and method names.
 *
 * It also parse the @Method annotation.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 5.2
 */
class AnnotatedRouteControllerLoader extends \Symfony\Component\Routing\Loader\AnnotationClassLoader
{
    /**
     * Configures the _controller default parameter and eventually the HTTP method
     * requirement of a given Route instance.
     *
     * @param mixed $annot The annotation class instance
     *
     * @throws \LogicException When the service option is specified on a method
     */
    protected function configureRoute(\Symfony\Component\Routing\Route $route, \ReflectionClass $class, \ReflectionMethod $method, $annot)
    {
    }
    protected function getGlobals(\ReflectionClass $class)
    {
    }
    /**
     * Makes the default route name more sane by removing common keywords.
     *
     * @return string The default route name
     */
    protected function getDefaultRouteName(\ReflectionClass $class, \ReflectionMethod $method)
    {
    }
}
