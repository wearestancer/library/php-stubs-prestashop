<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * The ParamConverter class handles the ParamConverter annotation parts.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @Annotation
 */
class ParamConverter extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation
{
    /**
     * Returns the parameter name.
     *
     * @return string
     */
    public function getName()
    {
    }
    /**
     * Sets the parameter name.
     *
     * @param string $name The parameter name
     */
    public function setValue($name)
    {
    }
    /**
     * Sets the parameter name.
     *
     * @param string $name The parameter name
     */
    public function setName($name)
    {
    }
    /**
     * Returns the parameter class name.
     *
     * @return string $name
     */
    public function getClass()
    {
    }
    /**
     * Sets the parameter class name.
     *
     * @param string $class The parameter class name
     */
    public function setClass($class)
    {
    }
    /**
     * Returns an array of options.
     *
     * @return array
     */
    public function getOptions()
    {
    }
    /**
     * Sets an array of options.
     *
     * @param array $options An array of options
     */
    public function setOptions($options)
    {
    }
    /**
     * Sets whether or not the parameter is optional.
     *
     * @param bool $optional Whether the parameter is optional
     */
    public function setIsOptional($optional)
    {
    }
    /**
     * Returns whether or not the parameter is optional.
     *
     * @return bool
     */
    public function isOptional()
    {
    }
    /**
     * Get explicit converter name.
     *
     * @return string
     */
    public function getConverter()
    {
    }
    /**
     * Set explicit converter name.
     *
     * @param string $converter
     */
    public function setConverter($converter)
    {
    }
    /**
     * Returns the annotation alias name.
     *
     * @return string
     *
     * @see ConfigurationInterface
     */
    public function getAliasName()
    {
    }
    /**
     * Multiple ParamConverters are allowed.
     *
     * @return bool
     *
     * @see ConfigurationInterface
     */
    public function allowArray()
    {
    }
}
