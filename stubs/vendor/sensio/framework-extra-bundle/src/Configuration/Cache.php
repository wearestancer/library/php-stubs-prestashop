<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * The Cache class handles the Cache annotation parts.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @Annotation
 */
class Cache extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation
{
    /**
     * Returns the expiration date for the Expires header field.
     *
     * @return string
     */
    public function getExpires()
    {
    }
    /**
     * Sets the expiration date for the Expires header field.
     *
     * @param string $expires A valid php date
     */
    public function setExpires($expires)
    {
    }
    /**
     * Sets the number of seconds for the max-age cache-control header field.
     *
     * @param int $maxage A number of seconds
     */
    public function setMaxAge($maxage)
    {
    }
    /**
     * Returns the number of seconds the response is considered fresh by a
     * private cache.
     *
     * @return int
     */
    public function getMaxAge()
    {
    }
    /**
     * Sets the number of seconds for the s-maxage cache-control header field.
     *
     * @param int $smaxage A number of seconds
     */
    public function setSMaxAge($smaxage)
    {
    }
    /**
     * Returns the number of seconds the response is considered fresh by a
     * public cache.
     *
     * @return int
     */
    public function getSMaxAge()
    {
    }
    /**
     * Returns whether or not a response is public.
     *
     * @return bool
     */
    public function isPublic()
    {
    }
    /**
     * @return bool
     */
    public function mustRevalidate()
    {
    }
    /**
     * Forces a response to be revalidated.
     *
     * @param bool $mustRevalidate
     */
    public function setMustRevalidate($mustRevalidate)
    {
    }
    /**
     * Returns whether or not a response is private.
     *
     * @return bool
     */
    public function isPrivate()
    {
    }
    /**
     * Sets a response public.
     *
     * @param bool $public A boolean value
     */
    public function setPublic($public)
    {
    }
    /**
     * Returns the custom "Vary"-headers.
     *
     * @return array
     */
    public function getVary()
    {
    }
    /**
     * Add additional "Vary:"-headers.
     *
     * @param array $vary
     */
    public function setVary($vary)
    {
    }
    /**
     * Sets the "Last-Modified"-header expression.
     *
     * @param string $expression
     */
    public function setLastModified($expression)
    {
    }
    /**
     * Returns the "Last-Modified"-header expression.
     *
     * @return string
     */
    public function getLastModified()
    {
    }
    /**
     * Sets the "ETag"-header expression.
     *
     * @param string $expression
     */
    public function setEtag($expression)
    {
    }
    /**
     * Returns the "ETag"-header expression.
     *
     * @return string
     */
    public function getEtag()
    {
    }
    /**
     * @return int|string
     */
    public function getMaxStale()
    {
    }
    /**
     * Sets the number of seconds for the max-stale cache-control header field.
     *
     * @param int|string $maxStale A number of seconds
     */
    public function setMaxStale($maxStale)
    {
    }
    /**
     * @return int|string
     */
    public function getStaleWhileRevalidate()
    {
    }
    /**
     * @param int|string $staleWhileRevalidate
     *
     * @return self
     */
    public function setStaleWhileRevalidate($staleWhileRevalidate)
    {
    }
    /**
     * @return int|string
     */
    public function getStaleIfError()
    {
    }
    /**
     * @param int|string $staleIfError
     *
     * @return self
     */
    public function setStaleIfError($staleIfError)
    {
    }
    /**
     * Returns the annotation alias name.
     *
     * @return string
     *
     * @see ConfigurationInterface
     */
    public function getAliasName()
    {
    }
    /**
     * Only one cache directive is allowed.
     *
     * @return bool
     *
     * @see ConfigurationInterface
     */
    public function allowArray()
    {
    }
}
