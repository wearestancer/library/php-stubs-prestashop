<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * The Security class handles the Security annotation.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 * @Annotation
 */
class IsGranted extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation
{
    public function setAttributes($attributes)
    {
    }
    public function getAttributes()
    {
    }
    public function setSubject($subject)
    {
    }
    public function getSubject()
    {
    }
    public function getMessage()
    {
    }
    public function setMessage($message)
    {
    }
    public function getStatusCode()
    {
    }
    public function setStatusCode($statusCode)
    {
    }
    public function setValue($value)
    {
    }
    public function getAliasName()
    {
    }
    public function allowArray()
    {
    }
}
