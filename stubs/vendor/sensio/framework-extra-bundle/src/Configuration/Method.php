<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * The Method class handles the Method annotation parts.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @Annotation
 *
 * @deprecated since version 5.2
 */
class Method extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation
{
    /**
     * Returns the array of HTTP methods.
     *
     * @return array
     */
    public function getMethods()
    {
    }
    /**
     * Sets the HTTP methods.
     *
     * @param array|string $methods An HTTP method or an array of HTTP methods
     */
    public function setMethods($methods)
    {
    }
    /**
     * Sets the HTTP methods.
     *
     * @param array|string $methods An HTTP method or an array of HTTP methods
     */
    public function setValue($methods)
    {
    }
    /**
     * Returns the annotation alias name.
     *
     * @return string
     *
     * @see ConfigurationInterface
     */
    public function getAliasName()
    {
    }
    /**
     * Only one method directive is allowed.
     *
     * @return bool
     *
     * @see ConfigurationInterface
     */
    public function allowArray()
    {
    }
}
