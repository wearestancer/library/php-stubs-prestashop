<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * @author Kris Wallsmith <kris@symfony.com>
 * @Annotation
 *
 * @deprecated since version 5.2
 */
class Route extends \Symfony\Component\Routing\Annotation\Route
{
    public function setService($service)
    {
    }
    public function getService()
    {
    }
    /**
     * Multiple route annotations are allowed.
     *
     * @return bool
     *
     * @see ConfigurationInterface
     */
    public function allowArray()
    {
    }
}
