<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * Base configuration annotation.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
abstract class ConfigurationAnnotation implements \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface
{
    public function __construct(array $values)
    {
    }
}
