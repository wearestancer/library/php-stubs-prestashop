<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * The Security class handles the Security annotation.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @Annotation
 */
class Security extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation
{
    /**
     * If set, will throw Symfony\Component\HttpKernel\Exception\HttpException
     * with the given $statusCode.
     * If null, Symfony\Component\Security\Core\Exception\AccessDeniedException.
     * will be used.
     *
     * @var int|null
     */
    protected $statusCode;
    /**
     * The message of the exception.
     *
     * @var string
     */
    protected $message = 'Access denied.';
    public function getExpression()
    {
    }
    public function setExpression($expression)
    {
    }
    public function getStatusCode()
    {
    }
    public function setStatusCode($statusCode)
    {
    }
    public function getMessage()
    {
    }
    public function setMessage($message)
    {
    }
    public function setValue($expression)
    {
    }
    public function getAliasName()
    {
    }
    public function allowArray()
    {
    }
}
