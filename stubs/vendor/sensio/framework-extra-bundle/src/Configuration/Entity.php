<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * Doctrine-specific ParamConverter with an easier syntax.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 * @Annotation
 */
class Entity extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter
{
    public function setExpr($expr)
    {
    }
}
