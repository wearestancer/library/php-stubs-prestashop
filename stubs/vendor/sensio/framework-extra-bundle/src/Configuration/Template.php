<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * The Template class handles the Template annotation parts.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @Annotation
 */
class Template extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation
{
    /**
     * The template.
     *
     * @var string
     */
    protected $template;
    /**
     * Returns the array of templates variables.
     *
     * @return array
     */
    public function getVars()
    {
    }
    /**
     * @param bool $streamable
     */
    public function setIsStreamable($streamable)
    {
    }
    /**
     * @return bool
     */
    public function isStreamable()
    {
    }
    /**
     * Sets the template variables.
     *
     * @param array $vars The template variables
     */
    public function setVars($vars)
    {
    }
    /**
     * Sets the template logic name.
     *
     * @param string $template The template logic name
     */
    public function setValue($template)
    {
    }
    /**
     * Returns the template.
     *
     * @return string
     */
    public function getTemplate()
    {
    }
    /**
     * Sets the template.
     *
     * @param string $template The template
     */
    public function setTemplate($template)
    {
    }
    /**
     * Returns the annotation alias name.
     *
     * @return string
     *
     * @see ConfigurationInterface
     */
    public function getAliasName()
    {
    }
    /**
     * Only one template directive is allowed.
     *
     * @return bool
     *
     * @see ConfigurationInterface
     */
    public function allowArray()
    {
    }
    public function setOwner(array $owner)
    {
    }
    /**
     * The controller (+action) this annotation is attached to.
     *
     * @return array
     */
    public function getOwner()
    {
    }
}
