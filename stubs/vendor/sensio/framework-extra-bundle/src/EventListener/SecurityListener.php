<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * SecurityListener handles security restrictions on controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SecurityListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Sensio\Bundle\FrameworkExtraBundle\Request\ArgumentNameConverter $argumentNameConverter, \Sensio\Bundle\FrameworkExtraBundle\Security\ExpressionLanguage $language = null, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $trustResolver = null, \Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy = null, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage = null, \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authChecker = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onKernelControllerArguments(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
