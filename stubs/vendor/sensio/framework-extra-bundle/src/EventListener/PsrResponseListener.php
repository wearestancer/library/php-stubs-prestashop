<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * Converts PSR-7 Response to HttpFoundation Response using the bridge.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class PsrResponseListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Bridge\PsrHttpMessage\HttpFoundationFactoryInterface $httpFoundationFactory)
    {
    }
    /**
     * Do the conversion if applicable and update the response of the event.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
