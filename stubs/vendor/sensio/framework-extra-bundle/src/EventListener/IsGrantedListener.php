<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * Handles the IsGranted annotation on controllers.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class IsGrantedListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Sensio\Bundle\FrameworkExtraBundle\Request\ArgumentNameConverter $argumentNameConverter, \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authChecker = null)
    {
    }
    public function onKernelControllerArguments(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
