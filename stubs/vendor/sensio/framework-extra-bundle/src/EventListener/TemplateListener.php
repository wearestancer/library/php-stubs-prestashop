<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * Handles the Template annotation for actions.
 *
 * Depends on pre-processing of the ControllerListener.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TemplateListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface, \Symfony\Contracts\Service\ServiceSubscriberInterface
{
    public function __construct(\Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser $templateGuesser, \Twig\Environment $twig = null)
    {
    }
    public function setContainer(\Psr\Container\ContainerInterface $container) : void
    {
    }
    /**
     * Guesses the template name to render and its variables and adds them to
     * the request object.
     */
    public function onKernelController(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * Renders the template and initializes a new response object with the
     * rendered template content.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
    public static function getSubscribedServices() : array
    {
    }
}
