<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * The ParamConverterListener handles the ParamConverter annotation.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ParamConverterListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param bool $autoConvert Auto convert non-configured objects
     */
    public function __construct(\Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager $manager, $autoConvert = true)
    {
    }
    /**
     * Modifies the ParamConverterManager instance.
     */
    public function onKernelController(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
