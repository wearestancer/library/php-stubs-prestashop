<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * The ControllerListener class parses annotation blocks located in
 * controller classes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ControllerListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Doctrine\Common\Annotations\Reader $reader)
    {
    }
    /**
     * Modifies the Request object to apply configuration information found in
     * controllers annotations like the template to render or HTTP caching
     * configuration.
     */
    public function onKernelController(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
