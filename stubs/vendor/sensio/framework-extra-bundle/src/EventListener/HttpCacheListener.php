<?php

namespace Sensio\Bundle\FrameworkExtraBundle\EventListener;

/**
 * HttpCacheListener handles HTTP cache headers.
 *
 * It can be configured via the Cache annotation.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpCacheListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct()
    {
    }
    /**
     * Handles HTTP validation headers.
     */
    public function onKernelController(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    /**
     * Modifies the response to apply HTTP cache headers when needed.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
