<?php

namespace Sensio\Bundle\FrameworkExtraBundle\Templating;

/**
 * The TemplateGuesser class handles the guessing of template name based on controller.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TemplateGuesser
{
    /**
     * @param string[] $controllerPatterns Regexps extracting the controller name from its FQN
     */
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, array $controllerPatterns = [])
    {
    }
    /**
     * Guesses and returns the template name to render based on the controller
     * and action names.
     *
     * @param callable $controller An array storing the controller object and action method
     *
     * @return string The template name
     *
     * @throws \InvalidArgumentException
     */
    public function guessTemplateName($controller, \Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
