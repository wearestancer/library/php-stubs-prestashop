<?php

namespace Sensio\Bundle\FrameworkExtraBundle\DependencyInjection\Compiler;

/**
 * Adds tagged request.param_converter services to converter.manager service.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AddParamConverterPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
