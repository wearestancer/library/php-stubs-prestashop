<?php

namespace Sensio\Bundle\FrameworkExtraBundle\DependencyInjection\Compiler;

/**
 * Optimizes the container by removing unneeded listeners.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class OptimizerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
