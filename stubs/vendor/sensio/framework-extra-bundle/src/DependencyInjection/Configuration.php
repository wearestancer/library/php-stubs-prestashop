<?php

namespace Sensio\Bundle\FrameworkExtraBundle\DependencyInjection;

/**
 * FrameworkExtraBundle configuration structure.
 *
 * @author Henrik Bjornskov <hb@peytz.dk>
 */
class Configuration implements \Symfony\Component\Config\Definition\ConfigurationInterface
{
    /**
     * Generates the configuration tree.
     *
     * @return NodeInterface
     */
    public function getConfigTreeBuilder()
    {
    }
}
