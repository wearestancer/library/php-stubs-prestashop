<?php

namespace Sensio\Bundle\FrameworkExtraBundle;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SensioFrameworkExtraBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
