<?php

namespace ApiPlatform\Hydra\EventListener;

/**
 * Adds the HTTP Link header pointing to the Hydra documentation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AddLinkHeaderListener
{
    use \ApiPlatform\Util\CorsTrait;
    public function __construct(\ApiPlatform\Api\UrlGeneratorInterface $urlGenerator)
    {
    }
    /**
     * Sends the Hydra header on each response.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\ResponseEvent $event) : void
    {
    }
}
