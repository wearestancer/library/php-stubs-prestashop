<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * Creates a machine readable Hydra API documentation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class DocumentationNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT = 'jsonld';
    public function __construct($resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, $propertyMetadataFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\Core\Api\OperationMethodResolverInterface $operationMethodResolver = null, \ApiPlatform\Api\UrlGeneratorInterface $urlGenerator, \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $subresourceOperationFactory = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
