<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * Converts {@see \Symfony\Component\Validator\ConstraintViolationListInterface} to a Hydra error representation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ConstraintViolationListNormalizer extends \ApiPlatform\Serializer\AbstractConstraintViolationListNormalizer
{
    public const FORMAT = 'jsonld';
    public function __construct(\ApiPlatform\Api\UrlGeneratorInterface $urlGenerator, array $serializePayloadFields = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
}
