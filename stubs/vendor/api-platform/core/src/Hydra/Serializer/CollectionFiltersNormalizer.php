<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * Enhances the result of collection by adding the filters applied on collection.
 *
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class CollectionFiltersNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Core\Api\FilterLocatorTrait;
    /**
     * @param ContainerInterface|FilterCollection                                 $filterLocator           The new filter locator or the deprecated filter collection
     * @param mixed                                                               $resourceMetadataFactory
     * @param ResourceClassResolverInterface|LegacyResourceClassResolverInterface $resourceClassResolver
     */
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $collectionNormalizer, $resourceMetadataFactory, $resourceClassResolver, $filterLocator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setNormalizer(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer)
    {
    }
}
