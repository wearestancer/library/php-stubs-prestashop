<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * Normalizes the API entrypoint.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class EntrypointNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT = 'jsonld';
    public function __construct($resourceMetadataFactory, $iriConverter, \ApiPlatform\Api\UrlGeneratorInterface $urlGenerator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
