<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * Adds a view key to the result of a paginated Hydra collection.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class PartialCollectionViewNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $collectionNormalizer, string $pageParameterName = 'page', string $enabledParameterName = 'pagination', $resourceMetadataFactory = null, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setNormalizer(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer)
    {
    }
}
