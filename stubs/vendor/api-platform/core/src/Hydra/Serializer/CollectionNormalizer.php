<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * This normalizer handles collections.
 *
 * @author Kevin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class CollectionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Serializer\ContextTrait;
    use \ApiPlatform\JsonLd\Serializer\JsonLdContextTrait;
    use \Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
    public const FORMAT = 'jsonld';
    public const IRI_ONLY = 'iri_only';
    public function __construct(\ApiPlatform\JsonLd\ContextBuilderInterface $contextBuilder, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, $iriConverter, ?\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param iterable $object
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
