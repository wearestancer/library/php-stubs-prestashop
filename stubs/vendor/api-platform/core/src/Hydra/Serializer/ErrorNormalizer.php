<?php

namespace ApiPlatform\Hydra\Serializer;

/**
 * Converts {@see \Exception} or {@see FlattenException} or {@see LegacyFlattenException} to a Hydra error representation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class ErrorNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Problem\Serializer\ErrorNormalizerTrait;
    public const FORMAT = 'jsonld';
    public const TITLE = 'title';
    public function __construct(\ApiPlatform\Api\UrlGeneratorInterface $urlGenerator, bool $debug = false, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
