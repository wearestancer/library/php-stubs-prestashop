<?php

namespace ApiPlatform\JsonLd\Serializer;

/**
 * Converts between objects and array including JSON-LD and Hydra metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ItemNormalizer extends \ApiPlatform\Serializer\AbstractItemNormalizer
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Serializer\ContextTrait;
    use \ApiPlatform\JsonLd\Serializer\JsonLdContextTrait;
    public const FORMAT = 'jsonld';
    public function __construct($resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, $propertyMetadataFactory, $iriConverter, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\JsonLd\ContextBuilderInterface $contextBuilder, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, array $defaultContext = [], iterable $dataTransformers = [], \ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws NotNormalizableValueException
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
}
