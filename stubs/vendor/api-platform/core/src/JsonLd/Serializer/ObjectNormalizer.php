<?php

namespace ApiPlatform\JsonLd\Serializer;

/**
 * Decorates the output with JSON-LD metadata when appropriate, but otherwise just
 * passes through to the decorated normalizer.
 */
final class ObjectNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\JsonLd\Serializer\JsonLdContextTrait;
    public const FORMAT = 'jsonld';
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $decorated, $iriConverter, \ApiPlatform\JsonLd\AnonymousContextBuilderInterface $anonymousContextBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
}
