<?php

namespace ApiPlatform\JsonLd\Serializer;

/**
 * Creates and manipulates the Serializer context.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @internal
 */
trait JsonLdContextTrait
{
    /**
     * Updates the given JSON-LD document to add its @context key.
     */
    private function addJsonLdContext(\ApiPlatform\JsonLd\ContextBuilderInterface $contextBuilder, string $resourceClass, array &$context, array $data = []) : array
    {
    }
    private function createJsonLdContext(\ApiPlatform\JsonLd\AnonymousContextBuilderInterface $contextBuilder, $object, array &$context, array $data = []) : array
    {
    }
}
