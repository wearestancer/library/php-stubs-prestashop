<?php

namespace ApiPlatform\JsonLd\Action;

/**
 * Generates JSON-LD contexts.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ContextAction
{
    public const RESERVED_SHORT_NAMES = ['ConstraintViolationList' => true, 'Error' => true];
    public function __construct(\ApiPlatform\JsonLd\ContextBuilderInterface $contextBuilder, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, $resourceMetadataFactory)
    {
    }
    /**
     * Generates a context according to the type requested.
     *
     * @throws NotFoundHttpException
     */
    public function __invoke(string $shortName) : array
    {
    }
}
