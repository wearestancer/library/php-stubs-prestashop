<?php

namespace ApiPlatform\JsonLd;

/**
 * {@inheritdoc}
 * TODO: 3.0 simplify or remove the class.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ContextBuilder implements \ApiPlatform\JsonLd\AnonymousContextBuilderInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public const FORMAT = 'jsonld';
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, $resourceMetadataFactory, $propertyNameCollectionFactory, $propertyMetadataFactory, \ApiPlatform\Api\UrlGeneratorInterface $urlGenerator, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \ApiPlatform\Api\IriConverterInterface $iriConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBaseContext(int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_URL) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEntrypointContext(int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceContext(string $resourceClass, int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceContextUri(string $resourceClass, int $referenceType = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAnonymousResourceContext($object, array $context = [], int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH) : array
    {
    }
}
