<?php

namespace ApiPlatform\JsonLd;

/**
 * JSON-LD context builder with Input Output DTO support interface.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface AnonymousContextBuilderInterface extends \ApiPlatform\JsonLd\ContextBuilderInterface
{
    /**
     * Creates a JSON-LD context based on the given object.
     * Usually this is used with an Input or Output DTO object.
     *
     * @param mixed $object
     */
    public function getAnonymousResourceContext($object, array $context = [], int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH) : array;
}
