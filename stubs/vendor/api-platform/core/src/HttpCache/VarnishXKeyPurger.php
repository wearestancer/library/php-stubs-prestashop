<?php

namespace ApiPlatform\HttpCache;

/**
 * Purges Varnish XKey.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class VarnishXKeyPurger implements \ApiPlatform\HttpCache\PurgerInterface
{
    /**
     * @param HttpClientInterface[] $clients
     */
    public function __construct(array $clients, int $maxHeaderLength = self::VARNISH_MAX_HEADER_LENGTH, string $xkeyGlue = ' ')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function purge(array $iris)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResponseHeaders(array $iris) : array
    {
    }
}
