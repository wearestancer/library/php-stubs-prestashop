<?php

namespace ApiPlatform\HttpCache;

/**
 * Purges resources from the cache.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface PurgerInterface
{
    /**
     * Purges all responses containing the given resources from the cache.
     *
     * @param string[] $iris
     */
    public function purge(array $iris);
    /**
     * Get the response header containing purged tags.
     *
     * @param string[] $iris
     */
    public function getResponseHeaders(array $iris) : array;
}
