<?php

namespace ApiPlatform\HttpCache\EventListener;

/**
 * Configures cache HTTP headers for the current response.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AddHeadersListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    /**
     * @param ResourceMetadataFactoryInterface|ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory
     */
    public function __construct(bool $etag = false, int $maxAge = null, int $sharedMaxAge = null, array $vary = null, bool $public = null, $resourceMetadataFactory = null, int $staleWhileRevalidate = null, int $staleIfError = null)
    {
    }
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\ResponseEvent $event) : void
    {
    }
}
