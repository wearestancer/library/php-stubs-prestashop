<?php

namespace ApiPlatform\HttpCache\EventListener;

/**
 * Sets the list of resources' IRIs included in this response in the "Cache-Tags" and/or "xkey" HTTP headers.
 *
 * The "Cache-Tags" is used because it is supported by CloudFlare.
 *
 * @see https://support.cloudflare.com/hc/en-us/articles/206596608-How-to-Purge-Cache-Using-Cache-Tags-Enterprise-only-
 *
 * The "xkey" is used because it is supported by Varnish.
 * @see https://docs.varnish-software.com/varnish-cache-plus/vmods/ykey/
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AddTagsListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\State\UriVariablesResolverTrait;
    /**
     * @param LegacyPurgerInterface|PurgerInterface|null        $purger
     * @param LegacyIriConverterInterface|IriConverterInterface $iriConverter
     * @param mixed|null                                        $purger
     */
    public function __construct($iriConverter, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory = null, $purger = null)
    {
    }
    /**
     * Adds the "Cache-Tags" and "xkey" headers.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\ResponseEvent $event) : void
    {
    }
}
