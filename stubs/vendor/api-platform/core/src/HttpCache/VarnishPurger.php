<?php

namespace ApiPlatform\HttpCache;

/**
 * Purges Varnish.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class VarnishPurger implements \ApiPlatform\HttpCache\PurgerInterface
{
    /**
     * @param HttpClientInterface[] $clients
     */
    public function __construct(array $clients, int $maxHeaderLength = self::DEFAULT_VARNISH_MAX_HEADER_LENGTH)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function purge(array $iris)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResponseHeaders(array $iris) : array
    {
    }
}
