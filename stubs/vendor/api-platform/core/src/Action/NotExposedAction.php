<?php

namespace ApiPlatform\Action;

/**
 * An action which always returns HTTP 404 Not Found with an explanation for why the operation is not exposed.
 */
final class NotExposedAction
{
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request) : never
    {
    }
}
