<?php

namespace ApiPlatform\Action;

/**
 * Generates the API entrypoint.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class EntrypointAction
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory)
    {
    }
    public function __invoke() : \ApiPlatform\Api\Entrypoint
    {
    }
}
