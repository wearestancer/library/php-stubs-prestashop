<?php

namespace ApiPlatform\Action;

/**
 * Placeholder returning the data passed in parameter.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PlaceholderAction
{
    /**
     * @param object $data
     *
     * @return object
     */
    public function __invoke($data)
    {
    }
}
