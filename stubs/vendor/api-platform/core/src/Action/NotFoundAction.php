<?php

namespace ApiPlatform\Action;

/**
 * An action which always returns HTTP 404 Not Found. Useful for disabling an operation.
 */
final class NotFoundAction
{
    public function __invoke()
    {
    }
}
