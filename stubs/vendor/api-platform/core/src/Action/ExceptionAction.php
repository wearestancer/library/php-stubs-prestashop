<?php

namespace ApiPlatform\Action;

/**
 * Renders a normalized exception for a given {@see FlattenException} or {@see LegacyFlattenException}.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ExceptionAction
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    /**
     * @param array      $errorFormats            A list of enabled error formats
     * @param array      $exceptionToStatus       A list of exceptions mapped to their HTTP status code
     * @param mixed|null $resourceMetadataFactory
     */
    public function __construct(\Symfony\Component\Serializer\SerializerInterface $serializer, array $errorFormats, array $exceptionToStatus = [], $resourceMetadataFactory = null)
    {
    }
    /**
     * Converts an exception to a JSON response.
     *
     * @param FlattenException|LegacyFlattenException $exception
     */
    public function __invoke($exception, \Symfony\Component\HttpFoundation\Request $request) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
