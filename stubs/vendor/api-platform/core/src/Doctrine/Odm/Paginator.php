<?php

namespace ApiPlatform\Doctrine\Odm;

/**
 * Decorates the Doctrine MongoDB ODM paginator.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class Paginator implements \IteratorAggregate, \ApiPlatform\State\Pagination\PaginatorInterface
{
    public const LIMIT_ZERO_MARKER_FIELD = '___';
    public const LIMIT_ZERO_MARKER = 'limit0';
    public function __construct(\Doctrine\ODM\MongoDB\Iterator\Iterator $mongoDbOdmIterator, \Doctrine\ODM\MongoDB\UnitOfWork $unitOfWork, string $resourceClass, array $pipeline)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLastPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTotalItems() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
}
