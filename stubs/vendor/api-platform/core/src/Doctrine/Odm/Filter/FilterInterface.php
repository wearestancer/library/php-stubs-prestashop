<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * Doctrine MongoDB ODM filter interface.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface FilterInterface extends \ApiPlatform\Api\FilterInterface
{
    /**
     * Applies the filter.
     */
    public function apply(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []);
}
