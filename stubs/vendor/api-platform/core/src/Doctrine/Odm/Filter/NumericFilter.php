<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * Filters the collection by numeric values.
 *
 * Filters collection by equality of numeric properties.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not numeric, the property is ignored.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class NumericFilter extends \ApiPlatform\Doctrine\Odm\Filter\AbstractFilter
{
    use \ApiPlatform\Doctrine\Common\Filter\NumericFilterTrait;
    /**
     * Type of numeric in Doctrine.
     */
    public const DOCTRINE_NUMERIC_TYPES = [\Doctrine\ODM\MongoDB\Types\Type::INT => true, \Doctrine\ODM\MongoDB\Types\Type::INTEGER => true, \Doctrine\ODM\MongoDB\Types\Type::FLOAT => true];
}
