<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * Filters the collection by range.
 *
 * @author Lee Siong Chan <ahlee2326@me.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class RangeFilter extends \ApiPlatform\Doctrine\Odm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\RangeFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\RangeFilterTrait;
}
