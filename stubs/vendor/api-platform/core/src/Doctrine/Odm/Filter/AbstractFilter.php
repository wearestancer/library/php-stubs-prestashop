<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * {@inheritdoc}
 *
 * Abstract class for easing the implementation of a filter.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
abstract class AbstractFilter implements \ApiPlatform\Doctrine\Odm\Filter\FilterInterface
{
    use \ApiPlatform\Doctrine\Odm\PropertyHelperTrait;
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    /** @var ManagerRegistry */
    protected $managerRegistry;
    /** @var LoggerInterface */
    protected $logger;
    /** @var array|null */
    protected $properties;
    /** @var NameConverterInterface|null */
    protected $nameConverter;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = [])
    {
    }
    /**
     * Passes a property through the filter.
     *
     * @param mixed $value
     */
    protected abstract function filterProperty(string $property, $value, \Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []);
    protected function getManagerRegistry() : \Doctrine\Persistence\ManagerRegistry
    {
    }
    protected function getProperties() : ?array
    {
    }
    protected function getLogger() : \Psr\Log\LoggerInterface
    {
    }
    /**
     * Determines whether the given property is enabled.
     */
    protected function isPropertyEnabled(string $property, string $resourceClass) : bool
    {
    }
    protected function denormalizePropertyName($property) : string
    {
    }
    protected function normalizePropertyName($property) : string
    {
    }
}
