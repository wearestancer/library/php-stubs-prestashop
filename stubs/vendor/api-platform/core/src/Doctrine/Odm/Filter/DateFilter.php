<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * Filters the collection by date intervals.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class DateFilter extends \ApiPlatform\Doctrine\Odm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\DateFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\DateFilterTrait;
    public const DOCTRINE_DATE_TYPES = [\Doctrine\ODM\MongoDB\Types\Type::DATE => true, \Doctrine\ODM\MongoDB\Types\Type::DATE_IMMUTABLE => true];
}
