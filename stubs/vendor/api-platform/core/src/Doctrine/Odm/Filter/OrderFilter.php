<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * Order the collection by given properties.
 *
 * The ordering is done in the same sequence as they are specified in the query,
 * and for each property a direction value can be specified.
 *
 * For each property passed, if the resource does not have such property or if the
 * direction value is different from "asc" or "desc" (case insensitive), the property
 * is ignored.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class OrderFilter extends \ApiPlatform\Doctrine\Odm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\OrderFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\OrderFilterTrait;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, string $orderParameterName = 'order', \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []) : void
    {
    }
}
