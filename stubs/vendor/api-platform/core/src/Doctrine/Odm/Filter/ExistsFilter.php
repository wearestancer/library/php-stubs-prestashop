<?php

namespace ApiPlatform\Doctrine\Odm\Filter;

/**
 * Filters the collection by whether a property value exists or not.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not one of ( "true" | "false" | "1" | "0" ) the property is ignored.
 *
 * A query parameter with key but no value is treated as `true`, e.g.:
 * Request: GET /products?exists[brand]
 * Interpretation: filter products which have a brand
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ExistsFilter extends \ApiPlatform\Doctrine\Odm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\ExistsFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\ExistsFilterTrait;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \Psr\Log\LoggerInterface $logger = null, array $properties = null, string $existsParameterName = self::QUERY_PARAMETER_KEY, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []) : void
    {
    }
}
