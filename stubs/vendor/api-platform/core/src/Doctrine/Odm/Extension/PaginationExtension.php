<?php

namespace ApiPlatform\Doctrine\Odm\Extension;

/**
 * Applies pagination on the Doctrine aggregation for resource collection when enabled.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class PaginationExtension implements \ApiPlatform\Doctrine\Odm\Extension\AggregationResultCollectionExtensionInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\State\Pagination\Pagination $pagination)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function applyToCollection(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsResult(string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function getResult(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : iterable
    {
    }
}
