<?php

namespace ApiPlatform\Doctrine\Odm\Extension;

/**
 * Applies filters on a resource aggregation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class FilterExtension implements \ApiPlatform\Doctrine\Odm\Extension\AggregationCollectionExtensionInterface
{
    public function __construct(\Psr\Container\ContainerInterface $filterLocator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []) : void
    {
    }
}
