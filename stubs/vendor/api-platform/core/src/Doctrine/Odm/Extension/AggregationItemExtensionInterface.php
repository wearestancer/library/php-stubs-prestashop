<?php

namespace ApiPlatform\Doctrine\Odm\Extension;

/**
 * Interface of Doctrine MongoDB ODM aggregation extensions for item aggregations.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface AggregationItemExtensionInterface
{
    public function applyToItem(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, array $identifiers, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []);
}
