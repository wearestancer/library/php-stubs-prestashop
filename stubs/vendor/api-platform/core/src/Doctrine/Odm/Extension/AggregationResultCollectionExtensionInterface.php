<?php

namespace ApiPlatform\Doctrine\Odm\Extension;

/**
 * Interface of Doctrine MongoDB ODM aggregation extensions that supports result production
 * for specific cases such as pagination.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface AggregationResultCollectionExtensionInterface extends \ApiPlatform\Doctrine\Odm\Extension\AggregationCollectionExtensionInterface
{
    public function supportsResult(string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool;
    public function getResult(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []);
}
