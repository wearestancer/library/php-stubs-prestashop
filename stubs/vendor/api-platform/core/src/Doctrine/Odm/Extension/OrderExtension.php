<?php

namespace ApiPlatform\Doctrine\Odm\Extension;

/**
 * Applies selected ordering while querying resource collection.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class OrderExtension implements \ApiPlatform\Doctrine\Odm\Extension\AggregationCollectionExtensionInterface
{
    use \ApiPlatform\Doctrine\Odm\PropertyHelperTrait;
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    public function __construct(string $order = null, \Doctrine\Persistence\ManagerRegistry $managerRegistry = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []) : void
    {
    }
}
