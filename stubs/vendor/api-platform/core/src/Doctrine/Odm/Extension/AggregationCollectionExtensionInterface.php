<?php

namespace ApiPlatform\Doctrine\Odm\Extension;

/**
 * Interface of Doctrine MongoDB ODM aggregation extensions for collection aggregations.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface AggregationCollectionExtensionInterface
{
    public function applyToCollection(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array &$context = []);
}
