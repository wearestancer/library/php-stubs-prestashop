<?php

namespace ApiPlatform\Doctrine\Odm\PropertyInfo;

/**
 * Extracts data using Doctrine MongoDB ODM metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class DoctrineExtractor implements \Symfony\Component\PropertyInfo\PropertyListExtractorInterface, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface, \Symfony\Component\PropertyInfo\PropertyAccessExtractorInterface
{
    public function __construct(\Doctrine\Persistence\ObjectManager $objectManager)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string[]|null
     */
    public function getProperties($class, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Type[]|null
     */
    public function getTypes($class, $property, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isReadable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isWritable($class, $property, array $context = []) : ?bool
    {
    }
}
