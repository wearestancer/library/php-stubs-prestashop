<?php

namespace ApiPlatform\Doctrine\Odm\State;

trait LinksHandlerTrait
{
    use \ApiPlatform\Doctrine\Common\State\LinksHandlerTrait;
    private function handleLinks(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, array $identifiers, array $context, string $resourceClass, \ApiPlatform\Metadata\Operation $operation) : void
    {
    }
    /**
     * @throws RuntimeException
     */
    private function buildAggregation(string $toClass, array $links, array $identifiers, array $context, array $executeOptions, string $previousAggregationClass, \Doctrine\ODM\MongoDB\Aggregation\Builder $previousAggregationBuilder) : \Doctrine\ODM\MongoDB\Aggregation\Builder
    {
    }
}
