<?php

namespace ApiPlatform\Doctrine\Odm\State;

/**
 * Collection state provider using the Doctrine ODM.
 */
final class CollectionProvider implements \ApiPlatform\State\ProviderInterface
{
    use \ApiPlatform\Doctrine\Odm\State\LinksHandlerTrait;
    /**
     * @param AggregationCollectionExtensionInterface[] $collectionExtensions
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \Doctrine\Persistence\ManagerRegistry $managerRegistry, iterable $collectionExtensions = [])
    {
    }
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
