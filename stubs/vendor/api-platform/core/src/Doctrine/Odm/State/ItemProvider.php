<?php

namespace ApiPlatform\Doctrine\Odm\State;

/**
 * Item state provider using the Doctrine ODM.
 *
 * @author Kévin Dunglas <kevin@dunglas.fr>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class ItemProvider implements \ApiPlatform\State\ProviderInterface
{
    use \ApiPlatform\Doctrine\Odm\State\LinksHandlerTrait;
    /**
     * @param AggregationItemExtensionInterface[] $itemExtensions
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \Doctrine\Persistence\ManagerRegistry $managerRegistry, iterable $itemExtensions = [])
    {
    }
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
