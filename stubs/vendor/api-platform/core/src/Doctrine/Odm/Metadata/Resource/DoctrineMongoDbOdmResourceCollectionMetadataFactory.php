<?php

namespace ApiPlatform\Doctrine\Odm\Metadata\Resource;

final class DoctrineMongoDbOdmResourceCollectionMetadataFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
