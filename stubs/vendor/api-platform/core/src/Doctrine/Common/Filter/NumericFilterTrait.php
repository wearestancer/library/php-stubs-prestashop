<?php

namespace ApiPlatform\Doctrine\Common\Filter;

/**
 * Trait for filtering the collection by numeric values.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait NumericFilterTrait
{
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    /**
     * Gets the PHP type corresponding to this Doctrine type.
     */
    protected abstract function getType(string $doctrineType = null) : string;
    protected abstract function getProperties() : ?array;
    protected abstract function getLogger() : \Psr\Log\LoggerInterface;
    protected abstract function normalizePropertyName($property) : string;
    /**
     * Determines whether the given property refers to a numeric field.
     */
    protected function isNumericField(string $property, string $resourceClass) : bool
    {
    }
    protected function normalizeValues($value, string $property) : ?array
    {
    }
    protected function isNumericArray(array $values) : bool
    {
    }
}
