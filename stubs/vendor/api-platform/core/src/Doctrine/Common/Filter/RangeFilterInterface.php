<?php

namespace ApiPlatform\Doctrine\Common\Filter;

/**
 * Interface for filtering the collection by range.
 *
 * @author Lee Siong Chan <ahlee2326@me.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface RangeFilterInterface
{
    public const PARAMETER_BETWEEN = 'between';
    public const PARAMETER_GREATER_THAN = 'gt';
    public const PARAMETER_GREATER_THAN_OR_EQUAL = 'gte';
    public const PARAMETER_LESS_THAN = 'lt';
    public const PARAMETER_LESS_THAN_OR_EQUAL = 'lte';
}
