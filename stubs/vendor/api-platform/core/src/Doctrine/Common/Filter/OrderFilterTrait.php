<?php

namespace ApiPlatform\Doctrine\Common\Filter;

/**
 * Trait for ordering the collection by given properties.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait OrderFilterTrait
{
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    /**
     * @var string Keyword used to retrieve the value
     */
    protected $orderParameterName;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    protected abstract function getProperties() : ?array;
    protected abstract function normalizePropertyName($property) : string;
    private function normalizeValue($value, string $property) : ?string
    {
    }
}
