<?php

namespace ApiPlatform\Doctrine\Common\Filter;

/**
 * Trait for filtering the collection by whether a property value exists or not.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait ExistsFilterTrait
{
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    /**
     * @var string Keyword used to retrieve the value
     */
    private $existsParameterName;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    /**
     * Determines whether the given property refers to a nullable field.
     */
    protected abstract function isNullableField(string $property, string $resourceClass) : bool;
    protected abstract function getProperties() : ?array;
    protected abstract function getLogger() : \Psr\Log\LoggerInterface;
    protected abstract function normalizePropertyName($property) : string;
    private function normalizeValue($value, string $property) : ?bool
    {
    }
}
