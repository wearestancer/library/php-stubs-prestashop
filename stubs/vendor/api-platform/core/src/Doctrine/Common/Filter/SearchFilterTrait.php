<?php

namespace ApiPlatform\Doctrine\Common\Filter;

/**
 * Trait for filtering the collection by given properties.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait SearchFilterTrait
{
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    protected $iriConverter;
    protected $propertyAccessor;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    /**
     * Converts a Doctrine type in PHP type.
     */
    protected abstract function getType(string $doctrineType) : string;
    protected abstract function getProperties() : ?array;
    protected abstract function getLogger() : \Psr\Log\LoggerInterface;
    protected abstract function getIriConverter() : \ApiPlatform\Api\IriConverterInterface;
    protected abstract function getPropertyAccessor() : \Symfony\Component\PropertyAccess\PropertyAccessorInterface;
    protected abstract function normalizePropertyName($property) : string;
    /**
     * Gets the ID from an IRI or a raw ID.
     */
    protected function getIdFromValue(string $value)
    {
    }
    /**
     * Normalize the values array.
     */
    protected function normalizeValues(array $values, string $property) : ?array
    {
    }
    /**
     * When the field should be an integer, check that the given value is a valid one.
     *
     * @param mixed|null $type
     */
    protected function hasValidValues(array $values, $type = null) : bool
    {
    }
}
