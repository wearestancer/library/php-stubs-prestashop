<?php

namespace ApiPlatform\Doctrine\Common\State;

final class PersistProcessor implements \ApiPlatform\State\ProcessorInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    public function process($data, \ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
