<?php

namespace ApiPlatform\Doctrine\Common\State;

trait LinksHandlerTrait
{
    /**
     * @return Link[]
     */
    private function getLinks(string $resourceClass, \ApiPlatform\Metadata\Operation $operation, array $context) : array
    {
    }
    private function getIdentifierValue(array &$identifiers, string $name = null)
    {
    }
    private function getOperationLinks(?\ApiPlatform\Metadata\Operation $operation = null) : array
    {
    }
}
