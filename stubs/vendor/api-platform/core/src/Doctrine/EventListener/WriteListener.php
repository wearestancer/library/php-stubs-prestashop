<?php

namespace ApiPlatform\Doctrine\EventListener;

/**
 * Bridges Doctrine and the API system.
 *
 * @deprecated
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class WriteListener
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    /**
     * Persists, updates or delete data return by the controller if applicable.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
