<?php

namespace ApiPlatform\Doctrine\EventListener;

/**
 * Purges responses containing modified entities from the proxy cache.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PurgeHttpCacheListener
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct(\ApiPlatform\HttpCache\PurgerInterface $purger, $iriConverter, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * Collects tags from the previous and the current version of the updated entities to purge related documents.
     */
    public function preUpdate(\Doctrine\ORM\Event\PreUpdateEventArgs $eventArgs) : void
    {
    }
    /**
     * Collects tags from inserted and deleted entities, including relations.
     */
    public function onFlush(\Doctrine\ORM\Event\OnFlushEventArgs $eventArgs) : void
    {
    }
    /**
     * Purges tags collected during this request, and clears the tag list.
     */
    public function postFlush() : void
    {
    }
}
