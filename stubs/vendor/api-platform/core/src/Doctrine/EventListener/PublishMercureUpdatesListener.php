<?php

namespace ApiPlatform\Doctrine\EventListener;

/**
 * Publishes resources updates to the Mercure hub.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PublishMercureUpdatesListener
{
    use \ApiPlatform\Symfony\Messenger\DispatchTrait;
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    /**
     * @param array<string, string[]|string> $formats
     * @param HubRegistry|callable           $hubRegistry
     */
    public function __construct(\ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\Api\IriConverterInterface $iriConverter, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory, \Symfony\Component\Serializer\SerializerInterface $serializer, array $formats, \Symfony\Component\Messenger\MessageBusInterface $messageBus = null, $hubRegistry = null, ?\ApiPlatform\GraphQl\Subscription\SubscriptionManagerInterface $graphQlSubscriptionManager = null, ?\ApiPlatform\GraphQl\Subscription\MercureSubscriptionIriGeneratorInterface $graphQlMercureSubscriptionIriGenerator = null, \Symfony\Component\ExpressionLanguage\ExpressionLanguage $expressionLanguage = null)
    {
    }
    /**
     * Collects created, updated and deleted objects.
     */
    public function onFlush(\Doctrine\Common\EventArgs $eventArgs) : void
    {
    }
    /**
     * Publishes updates for changes collected on flush, and resets the store.
     */
    public function postFlush() : void
    {
    }
}
