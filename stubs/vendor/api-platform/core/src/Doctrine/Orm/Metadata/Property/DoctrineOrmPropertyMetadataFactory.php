<?php

namespace ApiPlatform\Doctrine\Orm\Metadata\Property;

/**
 * Use Doctrine metadata to populate the identifier property.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class DoctrineOrmPropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
