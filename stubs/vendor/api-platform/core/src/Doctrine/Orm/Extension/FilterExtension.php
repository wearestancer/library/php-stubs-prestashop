<?php

namespace ApiPlatform\Doctrine\Orm\Extension;

/**
 * Applies filters on a resource query.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class FilterExtension implements \ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface
{
    public function __construct(\Psr\Container\ContainerInterface $filterLocator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass = null, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : void
    {
    }
}
