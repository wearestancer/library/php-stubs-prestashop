<?php

namespace ApiPlatform\Doctrine\Orm\Extension;

/**
 * Applies selected ordering while querying resource collection.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class OrderExtension implements \ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface
{
    public function __construct(string $order = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass = null, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : void
    {
    }
}
