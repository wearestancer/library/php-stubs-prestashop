<?php

namespace ApiPlatform\Doctrine\Orm\Extension;

/**
 * Interface of Doctrine ORM query extensions that supports result production
 * for specific cases such as pagination.
 *
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @template T of object
 */
interface QueryResultCollectionExtensionInterface extends \ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface
{
    public function supportsResult(string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool;
    /**
     * @return iterable<T>
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder, string $resourceClass = null, \ApiPlatform\Metadata\Operation $operation = null, array $context = []);
}
