<?php

namespace ApiPlatform\Doctrine\Orm\Extension;

/**
 * Fixes filters on OneToMany associations
 * https://github.com/api-platform/core/issues/944.
 */
final class FilterEagerLoadingExtension implements \ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface
{
    public function __construct(bool $forceEager = true, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass = null, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : void
    {
    }
}
