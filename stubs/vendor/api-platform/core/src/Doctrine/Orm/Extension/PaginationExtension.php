<?php

namespace ApiPlatform\Doctrine\Orm\Extension;

/**
 * Applies pagination on the Doctrine query for resource collection when enabled.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class PaginationExtension implements \ApiPlatform\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\State\Pagination\Pagination $pagination)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsResult(string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder, string $resourceClass = null, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : iterable
    {
    }
}
