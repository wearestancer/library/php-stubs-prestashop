<?php

namespace ApiPlatform\Doctrine\Orm\Extension;

/**
 * Interface of Doctrine ORM query extensions that supports result production
 * for specific cases such as Query alteration.
 *
 * @author Antoine BLUCHET <soyuka@gmail.com>
 *
 * @template T of object
 */
interface QueryResultItemExtensionInterface extends \ApiPlatform\Doctrine\Orm\Extension\QueryItemExtensionInterface
{
    public function supportsResult(string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool;
    /**
     * @return T|null
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder, string $resourceClass = null, \ApiPlatform\Metadata\Operation $operation = null, array $context = []);
}
