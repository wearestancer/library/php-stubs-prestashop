<?php

namespace ApiPlatform\Doctrine\Orm;

/**
 * Decorates the Doctrine ORM paginator.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class Paginator extends \ApiPlatform\Doctrine\Orm\AbstractPaginator implements \ApiPlatform\State\Pagination\PaginatorInterface, \ApiPlatform\Doctrine\Orm\QueryAwareInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLastPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTotalItems() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getQuery() : \Doctrine\ORM\Query
    {
    }
}
