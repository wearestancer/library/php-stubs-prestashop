<?php

namespace ApiPlatform\Doctrine\Orm\State;

trait LinksHandlerTrait
{
    use \ApiPlatform\Doctrine\Common\State\LinksHandlerTrait;
    private function handleLinks(\Doctrine\ORM\QueryBuilder $queryBuilder, array $identifiers, \ApiPlatform\Doctrine\Orm\Util\QueryNameGenerator $queryNameGenerator, array $context, string $resourceClass, \ApiPlatform\Metadata\Operation $operation) : void
    {
    }
}
