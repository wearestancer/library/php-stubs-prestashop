<?php

namespace ApiPlatform\Doctrine\Orm\State;

/**
 * Collection state provider using the Doctrine ORM.
 *
 * @author Kévin Dunglas <kevin@dunglas.fr>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class CollectionProvider implements \ApiPlatform\State\ProviderInterface
{
    use \ApiPlatform\Doctrine\Orm\State\LinksHandlerTrait;
    /**
     * @param QueryCollectionExtensionInterface[] $collectionExtensions
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \Doctrine\Persistence\ManagerRegistry $managerRegistry, iterable $collectionExtensions = [])
    {
    }
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
