<?php

namespace ApiPlatform\Doctrine\Orm\State;

/**
 * Item state provider using the Doctrine ORM.
 *
 * @author Kévin Dunglas <kevin@dunglas.fr>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class ItemProvider implements \ApiPlatform\State\ProviderInterface
{
    use \ApiPlatform\Doctrine\Orm\State\LinksHandlerTrait;
    /**
     * @param QueryItemExtensionInterface[] $itemExtensions
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \Doctrine\Persistence\ManagerRegistry $managerRegistry, iterable $itemExtensions = [])
    {
    }
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
