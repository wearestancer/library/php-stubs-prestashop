<?php

namespace ApiPlatform\Doctrine\Orm;

abstract class AbstractPaginator implements \IteratorAggregate, \ApiPlatform\State\Pagination\PartialPaginatorInterface
{
    protected $paginator;
    protected $iterator;
    protected $firstResult;
    protected $maxResults;
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(\Doctrine\ORM\Tools\Pagination\Paginator $paginator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
}
