<?php

namespace ApiPlatform\Doctrine\Orm;

interface QueryAwareInterface
{
    /**
     * Gets the Query object that will actually be executed.
     *
     * This should allow configuring options which could only be set on the Query
     * object itself.
     */
    public function getQuery() : \Doctrine\ORM\Query;
}
