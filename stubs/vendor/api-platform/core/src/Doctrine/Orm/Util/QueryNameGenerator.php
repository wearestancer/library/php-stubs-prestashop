<?php

namespace ApiPlatform\Doctrine\Orm\Util;

/**
 * Utility functions for working with Doctrine ORM query.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
final class QueryNameGenerator implements \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function generateJoinAlias(string $association) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function generateParameterName(string $name) : string
    {
    }
}
