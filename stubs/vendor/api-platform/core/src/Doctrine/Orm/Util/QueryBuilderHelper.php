<?php

namespace ApiPlatform\Doctrine\Orm\Util;

/**
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 *
 * @internal
 */
final class QueryBuilderHelper
{
    /**
     * Adds a join to the QueryBuilder if none exists.
     */
    public static function addJoinOnce(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $association, string $joinType = null, string $conditionType = null, string $condition = null, string $originAlias = null, string $newAlias = null) : string
    {
    }
    /**
     * Gets the entity class name by an alias used in the QueryBuilder.
     */
    public static function getEntityClassByAlias(string $alias, \Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : string
    {
    }
    /**
     * Finds the root alias for an alias used in the QueryBuilder.
     */
    public static function findRootAlias(string $alias, \Doctrine\ORM\QueryBuilder $queryBuilder) : string
    {
    }
    /**
     * Traverses through the joins for an alias used in the QueryBuilder.
     *
     * @return \Generator<string, array>
     */
    public static function traverseJoins(string $alias, \Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : \Generator
    {
    }
    /**
     * Gets the existing join from QueryBuilder DQL parts.
     */
    public static function getExistingJoin(\Doctrine\ORM\QueryBuilder $queryBuilder, string $alias, string $association, string $originAlias = null) : ?\Doctrine\ORM\Query\Expr\Join
    {
    }
}
