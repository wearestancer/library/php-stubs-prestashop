<?php

namespace ApiPlatform\Doctrine\Orm\Util;

/**
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
interface QueryNameGeneratorInterface
{
    /**
     * Generates a cacheable alias for DQL join.
     */
    public function generateJoinAlias(string $association) : string;
    /**
     * Generates a cacheable parameter name for DQL query.
     */
    public function generateParameterName(string $name) : string;
}
