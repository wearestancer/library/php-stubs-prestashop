<?php

namespace ApiPlatform\Doctrine\Orm\Filter;

abstract class AbstractFilter implements \ApiPlatform\Doctrine\Orm\Filter\FilterInterface
{
    use \ApiPlatform\Doctrine\Orm\PropertyHelperTrait;
    use \ApiPlatform\Doctrine\Common\PropertyHelperTrait;
    /** @var ManagerRegistry */
    protected $managerRegistry;
    /** @var LoggerInterface */
    protected $logger;
    /** @var array|null */
    protected $properties;
    /** @var NameConverterInterface|null */
    protected $nameConverter;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = [])
    {
    }
    /**
     * Passes a property through the filter.
     *
     * @param mixed $value
     */
    protected abstract function filterProperty(string $property, $value, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []);
    protected function getManagerRegistry() : \Doctrine\Persistence\ManagerRegistry
    {
    }
    protected function getProperties() : ?array
    {
    }
    protected function getLogger() : \Psr\Log\LoggerInterface
    {
    }
    /**
     * Determines whether the given property is enabled.
     */
    protected function isPropertyEnabled(string $property, string $resourceClass) : bool
    {
    }
    protected function denormalizePropertyName($property) : string
    {
    }
    protected function normalizePropertyName($property) : string
    {
    }
}
