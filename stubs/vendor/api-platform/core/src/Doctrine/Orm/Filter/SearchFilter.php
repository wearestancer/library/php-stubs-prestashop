<?php

namespace ApiPlatform\Doctrine\Orm\Filter;

/**
 * Filter the collection by given properties.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SearchFilter extends \ApiPlatform\Doctrine\Orm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\SearchFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\SearchFilterTrait;
    public const DOCTRINE_INTEGER_TYPE = \Doctrine\DBAL\Types\Types::INTEGER;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\Api\IriConverterInterface $iriConverter, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
}
