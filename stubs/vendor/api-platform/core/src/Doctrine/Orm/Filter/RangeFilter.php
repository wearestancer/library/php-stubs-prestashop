<?php

namespace ApiPlatform\Doctrine\Orm\Filter;

/**
 * Filters the collection by range.
 *
 * @author Lee Siong Chan <ahlee2326@me.com>
 */
final class RangeFilter extends \ApiPlatform\Doctrine\Orm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\RangeFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\RangeFilterTrait;
}
