<?php

namespace ApiPlatform\Doctrine\Orm\Filter;

/**
 * Doctrine ORM filter interface.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface FilterInterface extends \ApiPlatform\Api\FilterInterface
{
    /**
     * Applies the filter.
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, \ApiPlatform\Metadata\Operation $operation = null, array $context = []);
}
