<?php

namespace ApiPlatform\Doctrine\Orm\Filter;

/**
 * Filters the collection by boolean values.
 *
 * Filters collection on equality of boolean properties. The value is specified
 * as one of ( "true" | "false" | "1" | "0" ) in the query.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not one of ( "true" | "false" | "1" | "0" ) the property is ignored.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class BooleanFilter extends \ApiPlatform\Doctrine\Orm\Filter\AbstractFilter
{
    use \ApiPlatform\Doctrine\Common\Filter\BooleanFilterTrait;
    public const DOCTRINE_BOOLEAN_TYPES = [\Doctrine\DBAL\Types\Types::BOOLEAN => true];
}
