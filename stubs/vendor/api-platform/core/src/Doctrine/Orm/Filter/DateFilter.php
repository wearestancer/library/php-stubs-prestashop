<?php

namespace ApiPlatform\Doctrine\Orm\Filter;

/**
 * Filters the collection by date intervals.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 */
final class DateFilter extends \ApiPlatform\Doctrine\Orm\Filter\AbstractFilter implements \ApiPlatform\Doctrine\Common\Filter\DateFilterInterface
{
    use \ApiPlatform\Doctrine\Common\Filter\DateFilterTrait;
    public const DOCTRINE_DATE_TYPES = [\Doctrine\DBAL\Types\Types::DATE_MUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIMETZ_MUTABLE => true, \Doctrine\DBAL\Types\Types::TIME_MUTABLE => true, \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIMETZ_IMMUTABLE => true, \Doctrine\DBAL\Types\Types::TIME_IMMUTABLE => true];
}
