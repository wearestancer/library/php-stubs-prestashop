<?php

namespace ApiPlatform\Symfony\Routing;

/**
 * {@inheritdoc}
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class IriConverter implements \ApiPlatform\Api\IriConverterInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    use \ApiPlatform\State\UriVariablesResolverTrait;
    public function __construct(\ApiPlatform\State\ProviderInterface $provider, \Symfony\Component\Routing\RouterInterface $router, \ApiPlatform\Api\IdentifiersExtractorInterface $identifiersExtractor, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\Api\UriVariablesConverterInterface $uriVariablesConverter = null, \ApiPlatform\Api\IriConverterInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceFromIri(string $iri, array $context = [], ?\ApiPlatform\Metadata\Operation $operation = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIriFromResource($item, int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : ?string
    {
    }
}
