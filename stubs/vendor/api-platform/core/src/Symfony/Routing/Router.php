<?php

namespace ApiPlatform\Symfony\Routing;

/**
 * Symfony router decorator.
 *
 * Kévin Dunglas <dunglas@gmail.com>
 */
final class Router implements \Symfony\Component\Routing\RouterInterface, \ApiPlatform\Api\UrlGeneratorInterface
{
    public const CONST_MAP = [\ApiPlatform\Api\UrlGeneratorInterface::ABS_URL => \Symfony\Component\Routing\RouterInterface::ABSOLUTE_URL, \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH => \Symfony\Component\Routing\RouterInterface::ABSOLUTE_PATH, \ApiPlatform\Api\UrlGeneratorInterface::REL_PATH => \Symfony\Component\Routing\RouterInterface::RELATIVE_PATH, \ApiPlatform\Api\UrlGeneratorInterface::NET_PATH => \Symfony\Component\Routing\RouterInterface::NETWORK_PATH];
    public function __construct(\Symfony\Component\Routing\RouterInterface $router, int $urlGenerationStrategy = self::ABS_PATH)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setContext(\Symfony\Component\Routing\RequestContext $context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContext() : \Symfony\Component\Routing\RequestContext
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRouteCollection() : \Symfony\Component\Routing\RouteCollection
    {
    }
    /**
     * {@inheritdoc}
     */
    public function match($pathInfo) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = [], $referenceType = null) : string
    {
    }
}
