<?php

namespace ApiPlatform\Symfony\Routing;

/**
 * Loads Resources.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ApiLoader extends \Symfony\Component\Config\Loader\Loader
{
    /**
     * @deprecated since version 2.1, to be removed in 3.0. Use {@see RouteNameGenerator::ROUTE_NAME_PREFIX} instead.
     */
    public const ROUTE_NAME_PREFIX = 'api_';
    public const DEFAULT_ACTION_PATTERN = 'api_platform.action.';
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, $resourceMetadataFactory, \ApiPlatform\PathResolver\OperationPathResolverInterface $operationPathResolver, \Symfony\Component\DependencyInjection\ContainerInterface $container, array $formats, array $resourceClassDirectories = [], \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $subresourceOperationFactory = null, bool $graphqlEnabled = false, bool $entrypointEnabled = true, bool $docsEnabled = true, bool $graphiQlEnabled = false, bool $graphQlPlaygroundEnabled = false, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load($data, $type = null) : \Symfony\Component\Routing\RouteCollection
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null) : bool
    {
    }
}
