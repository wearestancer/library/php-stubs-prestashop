<?php

namespace ApiPlatform\Symfony\Routing;

/**
 * {@inheritdoc}
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class SkolemIriConverter implements \ApiPlatform\Api\IriConverterInterface
{
    public static $skolemUriTemplate = '/.well-known/genid/{id}';
    public function __construct(\Symfony\Component\Routing\RouterInterface $router)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceFromIri(string $iri, array $context = [], ?\ApiPlatform\Metadata\Operation $operation = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIriFromResource($item, int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : ?string
    {
    }
}
