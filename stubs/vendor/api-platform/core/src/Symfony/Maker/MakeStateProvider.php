<?php

namespace ApiPlatform\Symfony\Maker;

final class MakeStateProvider extends \Symfony\Bundle\MakerBundle\Maker\AbstractMaker
{
    /**
     * {@inheritdoc}
     */
    public static function getCommandName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getCommandDescription() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureCommand(\Symfony\Component\Console\Command\Command $command, \Symfony\Bundle\MakerBundle\InputConfiguration $inputConfig)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureDependencies(\Symfony\Bundle\MakerBundle\DependencyBuilder $dependencies)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function generate(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Bundle\MakerBundle\ConsoleStyle $io, \Symfony\Bundle\MakerBundle\Generator $generator)
    {
    }
}
