<?php

namespace ApiPlatform\Symfony\UriVariableTransformer;

/**
 * Transforms an UUID string to an instance of Symfony\Component\Uid\Uuid.
 */
final class UuidUriVariableTransformer implements \ApiPlatform\Api\UriVariableTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value, array $types, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($value, array $types, array $context = []) : bool
    {
    }
}
