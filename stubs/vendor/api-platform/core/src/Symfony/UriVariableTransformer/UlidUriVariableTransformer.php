<?php

namespace ApiPlatform\Symfony\UriVariableTransformer;

/**
 * Transforms an ULID string to an instance of Symfony\Component\Uid\Ulid.
 */
final class UlidUriVariableTransformer implements \ApiPlatform\Api\UriVariableTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value, array $types, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($value, array $types, array $context = []) : bool
    {
    }
}
