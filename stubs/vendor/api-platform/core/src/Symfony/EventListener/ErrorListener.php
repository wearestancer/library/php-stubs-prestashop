<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * This error listener extends the Symfony one in order to add
 * the `_api_operation` attribute when the request is duplicated.
 * It will later be used to retrieve the exceptionToStatus from the operation ({@see ExceptionAction}).
 */
final class ErrorListener extends \Symfony\Component\HttpKernel\EventListener\ErrorListener
{
}
