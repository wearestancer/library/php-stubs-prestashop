<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Retrieves data from the applicable data provider and sets it as a request parameter called data.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ReadListener
{
    use \ApiPlatform\Util\CloneTrait;
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\State\UriVariablesResolverTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'read';
    public function __construct(\ApiPlatform\State\ProviderInterface $provider, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\Serializer\SerializerContextBuilderInterface $serializerContextBuilder = null, \ApiPlatform\Api\UriVariablesConverterInterface $uriVariablesConverter = null)
    {
    }
    /**
     * Calls the data provider and sets the data attribute.
     *
     * @throws NotFoundHttpException
     */
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
}
