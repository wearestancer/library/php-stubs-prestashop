<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Bridges persistence and the API system.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class WriteListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    use \ApiPlatform\State\UriVariablesResolverTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'write';
    public function __construct(\ApiPlatform\State\ProcessorInterface $processor, \ApiPlatform\Api\IriConverterInterface $iriConverter, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, ?\ApiPlatform\Api\UriVariablesConverterInterface $uriVariablesConverter = null)
    {
    }
    /**
     * Persists, updates or delete data return by the controller if applicable.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
