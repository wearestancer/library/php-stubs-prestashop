<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Denies access to the current resource if the logged user doesn't have sufficient permissions.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class DenyAccessListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    public function __construct(
        $resourceMetadataFactory,
        /* ResourceAccessCheckerInterface */
        $resourceAccessCheckerOrExpressionLanguage = null,
        \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $authenticationTrustResolver = null,
        \Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy = null,
        \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage = null,
        \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker = null
    )
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
    public function onSecurity(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
    public function onSecurityPostDenormalize(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
    public function onSecurityPostValidation(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
