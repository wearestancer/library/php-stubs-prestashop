<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Builds the response object.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class RespondListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    public const METHOD_TO_CODE = ['POST' => \Symfony\Component\HttpFoundation\Response::HTTP_CREATED, 'DELETE' => \Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT];
    public function __construct($resourceMetadataFactory = null, \ApiPlatform\Api\IriConverterInterface $iriConverter = null)
    {
    }
    /**
     * Creates a Response to send to the client according to the requested format.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
