<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Updates the entity retrieved by the data provider with data contained in the request body.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class DeserializeListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\Core\Metadata\Resource\ToggleableOperationAttributeTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'deserialize';
    /**
     * @param ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface|FormatsProviderInterface|array $resourceMetadataFactory
     */
    public function __construct(\Symfony\Component\Serializer\SerializerInterface $serializer, \ApiPlatform\Serializer\SerializerContextBuilderInterface $serializerContextBuilder, $resourceMetadataFactory)
    {
    }
    /**
     * Deserializes the data sent in the requested format.
     *
     * @throws UnsupportedMediaTypeHttpException
     */
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
}
