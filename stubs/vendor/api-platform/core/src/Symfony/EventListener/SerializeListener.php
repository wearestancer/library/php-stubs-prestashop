<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Serializes data.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SerializeListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\Core\Metadata\Resource\ToggleableOperationAttributeTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'serialize';
    public function __construct(\Symfony\Component\Serializer\SerializerInterface $serializer, \ApiPlatform\Serializer\SerializerContextBuilderInterface $serializerContextBuilder, $resourceMetadataFactory = null)
    {
    }
    /**
     * Serializes the data to the requested format.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
