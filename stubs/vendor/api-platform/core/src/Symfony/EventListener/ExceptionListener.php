<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Handles requests errors.
 *
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ExceptionListener
{
    public function __construct($controller, \Psr\Log\LoggerInterface $logger = null, $debug = false, \Symfony\Component\HttpKernel\EventListener\ErrorListener $errorListener = null)
    {
    }
    public function onKernelException(\Symfony\Component\HttpKernel\Event\ExceptionEvent $event) : void
    {
    }
}
