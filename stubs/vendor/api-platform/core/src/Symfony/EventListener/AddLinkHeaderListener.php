<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Adds the HTTP Link header pointing to the Mercure hub for resources having their updates dispatched.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AddLinkHeaderListener
{
    use \ApiPlatform\Util\CorsTrait;
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    /**
     * @param Discovery|string $discovery
     * @param mixed            $resourceMetadataFactory
     */
    public function __construct($resourceMetadataFactory, $discovery)
    {
    }
    /**
     * Sends the Mercure header on each response.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\ResponseEvent $event) : void
    {
    }
}
