<?php

namespace ApiPlatform\Symfony\EventListener;

/**
 * Validates query parameters depending on filter description.
 *
 * @author Julien Deniau <julien.deniau@mapado.com>
 */
final class QueryParameterValidateListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\Core\Metadata\Resource\ToggleableOperationAttributeTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'query_parameter_validate';
    /**
     * @param ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface $resourceMetadataFactory
     * @param QueryParameterValidator|LegacyQueryParameterValidator                       $queryParameterValidator
     */
    public function __construct($resourceMetadataFactory, $queryParameterValidator, bool $enabled = true)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
}
