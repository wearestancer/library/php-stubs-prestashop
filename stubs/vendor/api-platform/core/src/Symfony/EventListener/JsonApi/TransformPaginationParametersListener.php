<?php

namespace ApiPlatform\Symfony\EventListener\JsonApi;

/**
 * @see http://jsonapi.org/format/#fetching-pagination
 * @see https://api-platform.com/docs/core/pagination
 *
 * @author Héctor Hurtarte <hectorh30@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class TransformPaginationParametersListener
{
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
}
