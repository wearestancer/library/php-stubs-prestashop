<?php

namespace ApiPlatform\Symfony\EventListener\JsonApi;

/**
 * @see http://jsonapi.org/format/#fetching-filtering
 * @see http://jsonapi.org/recommendations/#filtering
 *
 * @author Héctor Hurtarte <hectorh30@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class TransformFilteringParametersListener
{
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
}
