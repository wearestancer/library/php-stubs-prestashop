<?php

namespace ApiPlatform\Symfony\Messenger;

final class Processor implements \ApiPlatform\State\ProcessorInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Symfony\Messenger\DispatchTrait;
    public function __construct(\Symfony\Component\Messenger\MessageBusInterface $messageBus)
    {
    }
    public function process($data, \ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
