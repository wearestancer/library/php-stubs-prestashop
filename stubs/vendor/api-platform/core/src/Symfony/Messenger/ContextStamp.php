<?php

namespace ApiPlatform\Symfony\Messenger;

/**
 * An envelope stamp with context which related to a message.
 *
 * @author Sergii Pavlenko <sergii.pavlenko.v@gmail.com>
 */
final class ContextStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    public function __construct(array $context = [])
    {
    }
    /**
     * Get the context related to a message.
     */
    public function getContext() : array
    {
    }
}
