<?php

namespace ApiPlatform\Symfony\Messenger;

/**
 * Hints that the resource in the envelope must be removed.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class RemoveStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
}
