<?php

namespace ApiPlatform\Symfony\Messenger;

/**
 * @internal
 */
trait DispatchTrait
{
    /**
     * @var MessageBusInterface|null
     */
    private $messageBus;
    /**
     * @param object|Envelope $message
     */
    private function dispatch($message)
    {
    }
}
