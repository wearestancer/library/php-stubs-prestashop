<?php

namespace ApiPlatform\Symfony\Validator\Exception;

/**
 * Thrown when a validation error occurs.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ValidationException extends \ApiPlatform\Validator\Exception\ValidationException implements \ApiPlatform\Symfony\Validator\Exception\ConstraintViolationListAwareExceptionInterface
{
    public function __construct(\Symfony\Component\Validator\ConstraintViolationListInterface $constraintViolationList, string $message = '', int $code = 0, \Exception $previous = null)
    {
    }
    public function getConstraintViolationList() : \Symfony\Component\Validator\ConstraintViolationListInterface
    {
    }
    public function __toString() : string
    {
    }
}
