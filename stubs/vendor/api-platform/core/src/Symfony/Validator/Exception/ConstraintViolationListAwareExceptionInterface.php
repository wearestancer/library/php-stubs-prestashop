<?php

namespace ApiPlatform\Symfony\Validator\Exception;

/**
 * An exception which has a constraint violation list.
 */
interface ConstraintViolationListAwareExceptionInterface extends \ApiPlatform\Exception\ExceptionInterface
{
    /**
     * Gets constraint violations related to this exception.
     */
    public function getConstraintViolationList() : \Symfony\Component\Validator\ConstraintViolationListInterface;
}
