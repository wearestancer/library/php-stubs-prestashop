<?php

namespace ApiPlatform\Symfony\Validator;

/**
 * Generates validation groups for an object.
 *
 * @author Tomas Norkūnas <norkunas.tom@gmail.com>
 */
interface ValidationGroupsGeneratorInterface
{
    /**
     * @param object $object
     *
     * @return GroupSequence|string[]
     */
    public function __invoke($object);
}
