<?php

namespace ApiPlatform\Symfony\Validator\Metadata\Property\Restriction;

/**
 * Class PropertySchemaRegexRestriction.
 *
 * @author Andrii Penchuk penja7@gmail.com
 */
class PropertySchemaRegexRestriction implements \ApiPlatform\Symfony\Validator\Metadata\Property\Restriction\PropertySchemaRestrictionMetadataInterface
{
    /**
     * {@inheritdoc}
     *
     * @param Regex $constraint
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : bool
    {
    }
}
