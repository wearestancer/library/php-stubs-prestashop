<?php

namespace ApiPlatform\Symfony\Validator\Metadata\Property\Restriction;

/**
 * @author Tomas Norkūnas <norkunas.tom@gmail.com>
 */
final class PropertySchemaGreaterThanRestriction implements \ApiPlatform\Symfony\Validator\Metadata\Property\Restriction\PropertySchemaRestrictionMetadataInterface
{
    /**
     * {@inheritdoc}
     *
     * @param GreaterThan $constraint
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : bool
    {
    }
}
