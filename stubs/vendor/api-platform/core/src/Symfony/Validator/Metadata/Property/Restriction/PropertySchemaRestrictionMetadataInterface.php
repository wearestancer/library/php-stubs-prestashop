<?php

namespace ApiPlatform\Symfony\Validator\Metadata\Property\Restriction;

/**
 * Interface PropertySchemaRestrictionsInterface.
 *
 * @author Andrii Penchuk penja7@gmail.com
 */
interface PropertySchemaRestrictionMetadataInterface
{
    /**
     * Creates json schema restrictions based on the validation constraints.
     *
     * @param Constraint  $constraint       The validation constraint
     * @param ApiProperty $propertyMetadata The property metadata
     *
     * @return array The array of restrictions
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : array;
    /**
     * Is the constraint supported by the schema restriction?
     *
     * @param Constraint  $constraint       The validation constraint
     * @param ApiProperty $propertyMetadata The property metadata
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : bool;
}
