<?php

namespace ApiPlatform\Symfony\Validator\Metadata\Property\Restriction;

/**
 * Class PropertySchemaFormat.
 *
 * @author Andrii Penchuk penja7@gmail.com
 */
class PropertySchemaFormat implements \ApiPlatform\Symfony\Validator\Metadata\Property\Restriction\PropertySchemaRestrictionMetadataInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : bool
    {
    }
}
