<?php

namespace ApiPlatform\Symfony\Validator\Metadata\Property\Restriction;

/**
 * @author Tomas Norkūnas <norkunas.tom@gmail.com>
 */
final class PropertySchemaCollectionRestriction implements \ApiPlatform\Symfony\Validator\Metadata\Property\Restriction\PropertySchemaRestrictionMetadataInterface
{
    /**
     * @param iterable<PropertySchemaRestrictionMetadataInterface> $restrictionsMetadata
     */
    public function __construct(iterable $restrictionsMetadata = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Collection $constraint
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Metadata\ApiProperty $propertyMetadata) : bool
    {
    }
}
