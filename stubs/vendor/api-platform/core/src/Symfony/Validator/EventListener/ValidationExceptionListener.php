<?php

namespace ApiPlatform\Symfony\Validator\EventListener;

/**
 * Handles validation errors.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ValidationExceptionListener
{
    public function __construct(\Symfony\Component\Serializer\SerializerInterface $serializer, array $errorFormats, array $exceptionToStatus = [])
    {
    }
    /**
     * Returns a list of violations normalized in the Hydra format.
     */
    public function onKernelException(\Symfony\Component\HttpKernel\Event\ExceptionEvent $event) : void
    {
    }
}
