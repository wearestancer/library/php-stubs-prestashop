<?php

namespace ApiPlatform\Symfony\Validator;

/**
 * Validates an item using the Symfony validator component.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class Validator implements \ApiPlatform\Validator\ValidatorInterface
{
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator, \Psr\Container\ContainerInterface $container = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($data, array $context = [])
    {
    }
}
