<?php

namespace ApiPlatform\Symfony\Security;

/**
 * Checks if the logged user has sufficient permissions to access the given resource.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ResourceAccessChecker implements \ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface
{
    public function __construct(\Symfony\Component\ExpressionLanguage\ExpressionLanguage $expressionLanguage = null, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $authenticationTrustResolver = null, \Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy = null, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage = null, \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker = null, bool $exceptionOnNoToken = true)
    {
    }
    public function isGranted(string $resourceClass, string $expression, array $extraVariables = []) : bool
    {
    }
}
