<?php

namespace ApiPlatform\Symfony\Security\Core\Authorization;

/**
 * Registers API Platform's Expression Language functions.
 *
 * @author Yanick Witschi <yanick.witschi@terminal42.ch>
 */
final class ExpressionLanguageProvider implements \Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface
{
    public function getFunctions() : array
    {
    }
}
