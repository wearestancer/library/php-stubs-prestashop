<?php

namespace ApiPlatform\Symfony\Security;

/**
 * Checks if the logged user has sufficient permissions to access the given resource.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ResourceAccessCheckerInterface
{
    /**
     * Checks if the given item can be accessed by the current user.
     */
    public function isGranted(string $resourceClass, string $expression, array $extraVariables = []) : bool;
}
