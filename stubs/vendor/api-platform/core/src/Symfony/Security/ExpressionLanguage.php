<?php

namespace ApiPlatform\Symfony\Security;

/**
 * Adds some function to the default Symfony Security ExpressionLanguage.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @copyright Fabien Potencier <fabien@symfony.com>
 *
 * @see https://github.com/sensiolabs/SensioFrameworkExtraBundle/blob/master/Security/ExpressionLanguage.php
 */
class ExpressionLanguage extends \Symfony\Component\Security\Core\Authorization\ExpressionLanguage
{
    /**
     * {@inheritdoc}
     */
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cache = null, array $providers = [])
    {
    }
    protected function registerFunctions()
    {
    }
}
