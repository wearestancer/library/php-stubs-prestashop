<?php

namespace ApiPlatform\Symfony\Bundle\ArgumentResolver;

final class PayloadArgumentResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\Serializer\SerializerContextBuilderInterface $serializationContextBuilder)
    {
    }
    public function supports(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : bool
    {
    }
    public function resolve(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : iterable
    {
    }
}
