<?php

namespace ApiPlatform\Symfony\Bundle\Command;

final class DebugResourceCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \Symfony\Component\VarDumper\Cloner\ClonerInterface $cloner, $dumper)
    {
    }
    public static function getDefaultName() : string
    {
    }
}
