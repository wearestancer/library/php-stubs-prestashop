<?php

namespace ApiPlatform\Symfony\Bundle\Command;

/**
 * Dumps Open API documentation.
 */
final class OpenApiCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @param LegacyOpenApiFactoryInterface|OpenApiFactoryInterface $openApiFactory
     */
    public function __construct($openApiFactory, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer)
    {
    }
    public static function getDefaultName() : string
    {
    }
}
