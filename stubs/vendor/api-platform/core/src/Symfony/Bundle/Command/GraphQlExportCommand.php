<?php

namespace ApiPlatform\Symfony\Bundle\Command;

/**
 * Export the GraphQL schema in Schema Definition Language (SDL).
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
class GraphQlExportCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct($schemaBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    public static function getDefaultName() : string
    {
    }
}
