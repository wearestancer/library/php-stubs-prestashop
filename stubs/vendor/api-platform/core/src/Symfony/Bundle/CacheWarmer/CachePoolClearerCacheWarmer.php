<?php

namespace ApiPlatform\Symfony\Bundle\CacheWarmer;

/**
 * Clears the cache pools when warming up the cache.
 *
 * Do not use in production!
 *
 * @internal
 */
final class CachePoolClearerCacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface
{
    public function __construct(\Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer $poolClearer, array $pools = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string[]
     */
    public function warmUp($cacheDirectory) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isOptional() : bool
    {
    }
}
