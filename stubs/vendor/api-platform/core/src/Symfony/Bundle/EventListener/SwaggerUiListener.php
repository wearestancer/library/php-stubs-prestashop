<?php

namespace ApiPlatform\Symfony\Bundle\EventListener;

final class SwaggerUiListener
{
    /**
     * Sets SwaggerUiAction as controller if the requested format is HTML.
     */
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
}
