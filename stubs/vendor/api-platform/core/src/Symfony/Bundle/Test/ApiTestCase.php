<?php

namespace ApiPlatform\Symfony\Bundle\Test;

/**
 * Base class for functional API tests.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class ApiTestCase extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    use \ApiPlatform\Symfony\Bundle\Test\ApiTestAssertionsTrait;
    /**
     * {@inheritdoc}
     */
    protected function tearDown() : void
    {
    }
    /**
     * Creates a Client.
     *
     * @param array $kernelOptions  Options to pass to the createKernel method
     * @param array $defaultOptions Default options for the requests
     */
    protected static function createClient(array $kernelOptions = [], array $defaultOptions = []) : \ApiPlatform\Symfony\Bundle\Test\Client
    {
    }
    /**
     * Finds the IRI of a resource item matching the resource class and the specified criteria.
     */
    protected function findIriBy(string $resourceClass, array $criteria) : ?string
    {
    }
}
