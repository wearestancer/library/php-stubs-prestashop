<?php

namespace ApiPlatform\Symfony\Bundle\Test;

/**
 * HTTP Response.
 *
 * @internal
 *
 * Partially copied from \Symfony\Component\HttpClient\Response\ResponseTrait
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class Response implements \Symfony\Contracts\HttpClient\ResponseInterface
{
    use \ApiPlatform\Util\ResponseTrait;
    public function __construct(\Symfony\Component\HttpFoundation\Response $httpFoundationResponse, \Symfony\Component\BrowserKit\Response $browserKitResponse, array $info)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContent(bool $throw = true) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStatusCode() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getHeaders(bool $throw = true) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toArray(bool $throw = true) : array
    {
    }
    /**
     * Returns the internal HttpKernel response.
     */
    public function getKernelResponse() : \Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Returns the internal BrowserKit response.
     */
    public function getBrowserKitResponse() : \Symfony\Component\BrowserKit\Response
    {
    }
    /**
     * {@inheritdoc}.
     */
    public function cancel() : void
    {
    }
}
