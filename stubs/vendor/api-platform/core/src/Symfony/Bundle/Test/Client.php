<?php

namespace ApiPlatform\Symfony\Bundle\Test;

/**
 * Convenient test client that makes requests to a Kernel object.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class Client implements \Symfony\Contracts\HttpClient\HttpClientInterface
{
    use \ApiPlatform\Util\ClientTrait, \Symfony\Component\HttpClient\HttpClientTrait {
        \ApiPlatform\Util\ClientTrait::withOptions insteadof \Symfony\Component\HttpClient\HttpClientTrait;
    }
    /**
     * @see HttpClientInterface::OPTIONS_DEFAULTS
     */
    public const API_OPTIONS_DEFAULTS = ['auth_basic' => null, 'auth_bearer' => null, 'query' => [], 'headers' => ['accept' => ['application/ld+json']], 'body' => '', 'json' => null, 'base_uri' => 'http://localhost', 'extra' => []];
    /**
     * @param array $defaultOptions Default options for the requests
     *
     * @see HttpClientInterface::OPTIONS_DEFAULTS for available options
     */
    public function __construct(\Symfony\Bundle\FrameworkBundle\KernelBrowser $kernelBrowser, array $defaultOptions = [])
    {
    }
    /**
     * Sets the default options for the requests.
     *
     * @see HttpClientInterface::OPTIONS_DEFAULTS for available options
     */
    public function setDefaultOptions(array $defaultOptions) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null) : \Symfony\Contracts\HttpClient\ResponseStreamInterface
    {
    }
    /**
     * Gets the latest response.
     *
     * @internal
     */
    public function getResponse() : ?\ApiPlatform\Symfony\Bundle\Test\Response
    {
    }
    /**
     * Gets the underlying test client.
     *
     * @internal
     */
    public function getKernelBrowser() : \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
    }
    // The following methods are proxy methods for KernelBrowser's ones
    /**
     * Returns the container.
     *
     * @return ContainerInterface|null Returns null when the Kernel has been shutdown or not started yet
     */
    public function getContainer() : ?\Symfony\Component\DependencyInjection\ContainerInterface
    {
    }
    /**
     * Returns the CookieJar instance.
     */
    public function getCookieJar() : \Symfony\Component\BrowserKit\CookieJar
    {
    }
    /**
     * Returns the kernel.
     */
    public function getKernel() : \Symfony\Component\HttpKernel\KernelInterface
    {
    }
    /**
     * Gets the profile associated with the current Response.
     *
     * @return Profile|false A Profile instance
     */
    public function getProfile()
    {
    }
    /**
     * Enables the profiler for the very next request.
     *
     * If the profiler is not enabled, the call to this method does nothing.
     */
    public function enableProfiler() : void
    {
    }
    /**
     * Disables kernel reboot between requests.
     *
     * By default, the Client reboots the Kernel for each request. This method
     * allows to keep the same kernel across requests.
     */
    public function disableReboot() : void
    {
    }
    /**
     * Enables kernel reboot between requests.
     */
    public function enableReboot() : void
    {
    }
    public function loginUser(\Symfony\Component\Security\Core\User\UserInterface $user, string $firewallContext = 'main') : self
    {
    }
}
