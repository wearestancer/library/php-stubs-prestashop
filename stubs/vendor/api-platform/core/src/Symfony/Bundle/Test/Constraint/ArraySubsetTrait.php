<?php

namespace ApiPlatform\Symfony\Bundle\Test\Constraint;

/**
 * Constraint that asserts that the array it is evaluated for has a specified subset.
 *
 * Uses array_replace_recursive() to check if a key value subset is part of the
 * subject array.
 *
 * Imported from dms/phpunit-arraysubset-asserts, because the original constraint has been deprecated.
 *
 * @copyright Sebastian Bergmann <sebastian@phpunit.de>
 * @copyright Rafael Dohms <rdohms@gmail.com>
 *
 * @see https://github.com/sebastianbergmann/phpunit/issues/3494
 */
trait ArraySubsetTrait
{
    private $subset;
    private $strict;
    public function __construct(iterable $subset, bool $strict = false)
    {
    }
    private function _evaluate($other, string $description = '', bool $returnResult = false) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function failureDescription($other) : string
    {
    }
    private function toArray(iterable $other) : array
    {
    }
}
