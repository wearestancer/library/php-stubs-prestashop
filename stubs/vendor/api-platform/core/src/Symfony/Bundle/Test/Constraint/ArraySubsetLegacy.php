<?php

namespace ApiPlatform\Symfony\Bundle\Test\Constraint;

/**
 * Is used for phpunit < 8.
 *
 * @internal
 */
final class ArraySubsetLegacy extends \PHPUnit\Framework\Constraint\Constraint
{
    use \ApiPlatform\Symfony\Bundle\Test\Constraint\ArraySubsetTrait;
    /**
     * {@inheritdoc}
     */
    public function evaluate($other, $description = '', $returnResult = false) : ?bool
    {
    }
}
