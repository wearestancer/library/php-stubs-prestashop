<?php

namespace ApiPlatform\Symfony\Bundle\Test;

/**
 * @see \Symfony\Bundle\FrameworkBundle\Test\WebTestAssertionsTrait
 */
trait ApiTestAssertionsTrait
{
    use \Symfony\Bundle\FrameworkBundle\Test\BrowserKitAssertionsTrait;
    /**
     * Asserts that the retrieved JSON contains the specified subset.
     *
     * This method delegates to static::assertArraySubset().
     *
     * @param array|string $subset
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public static function assertJsonContains($subset, bool $checkForObjectIdentity = true, string $message = '') : void
    {
    }
    /**
     * Asserts that the retrieved JSON is equal to $json.
     *
     * Both values are canonicalized before the comparison.
     *
     * @param array|string $json
     */
    public static function assertJsonEquals($json, string $message = '') : void
    {
    }
    /**
     * Asserts that an array has a specified subset.
     *
     * Imported from dms/phpunit-arraysubset, because the original constraint has been deprecated.
     *
     * @copyright Sebastian Bergmann <sebastian@phpunit.de>
     * @copyright Rafael Dohms <rdohms@gmail.com>
     *
     * @see https://github.com/sebastianbergmann/phpunit/issues/3494
     *
     * @param iterable $subset
     * @param iterable $array
     *
     * @throws ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     * @throws \Exception
     */
    public static function assertArraySubset($subset, $array, bool $checkForObjectIdentity = false, string $message = '') : void
    {
    }
    /**
     * @param object|array|string $jsonSchema
     */
    public static function assertMatchesJsonSchema($jsonSchema, ?int $checkMode = null, string $message = '') : void
    {
    }
    public static function assertMatchesResourceCollectionJsonSchema(string $resourceClass, ?string $operationName = null, string $format = 'jsonld') : void
    {
    }
    public static function assertMatchesResourceItemJsonSchema(string $resourceClass, ?string $operationName = null, string $format = 'jsonld') : void
    {
    }
    private static function getHttpClient(\ApiPlatform\Symfony\Bundle\Test\Client $newClient = null) : ?\ApiPlatform\Symfony\Bundle\Test\Client
    {
    }
    private static function getHttpResponse() : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * @return SchemaFactoryInterface|LegacySchemaFactoryInterface
     */
    private static function getSchemaFactory()
    {
    }
    /**
     * @return ResourceMetadataCollectionFactoryInterface|null
     */
    private static function getResourceMetadataCollectionFactory()
    {
    }
}
