<?php

namespace ApiPlatform\Symfony\Bundle\SwaggerUi;

final class SwaggerUiContext
{
    public function __construct(bool $swaggerUiEnabled = false, bool $showWebby = true, bool $reDocEnabled = false, bool $graphQlEnabled = false, bool $graphiQlEnabled = false, bool $graphQlPlaygroundEnabled = false, $assetPackage = null, array $extraConfiguration = [])
    {
    }
    public function isSwaggerUiEnabled() : bool
    {
    }
    public function isWebbyShown() : bool
    {
    }
    public function isRedocEnabled() : bool
    {
    }
    public function isGraphQlEnabled() : bool
    {
    }
    public function isGraphiQlEnabled() : bool
    {
    }
    public function isGraphQlPlaygroundEnabled() : bool
    {
    }
    public function getAssetPackage() : ?string
    {
    }
    public function getExtraConfiguration() : array
    {
    }
}
