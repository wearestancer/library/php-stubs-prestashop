<?php

namespace ApiPlatform\Symfony\Bundle\SwaggerUi;

/**
 * Displays the swaggerui interface.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class SwaggerUiAction
{
    public function __construct($resourceMetadataFactory, ?\Twig\Environment $twig, \Symfony\Component\Routing\Generator\UrlGeneratorInterface $urlGenerator, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer, \ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface $openApiFactory, \ApiPlatform\OpenApi\Options $openApiOptions, \ApiPlatform\Symfony\Bundle\SwaggerUi\SwaggerUiContext $swaggerUiContext, array $formats = [], string $oauthClientId = null, string $oauthClientSecret = null, bool $oauthPkce = false)
    {
    }
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
