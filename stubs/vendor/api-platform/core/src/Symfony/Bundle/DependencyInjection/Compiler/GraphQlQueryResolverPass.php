<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection\Compiler;

/**
 * Injects GraphQL resolvers.
 *
 * @internal
 *
 * @author Lukas Lücke <lukas@luecke.me>
 */
final class GraphQlQueryResolverPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
