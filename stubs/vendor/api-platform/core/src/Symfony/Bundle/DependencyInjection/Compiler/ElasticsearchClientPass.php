<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection\Compiler;

/**
 * Creates the Elasticsearch client.
 *
 * @author Baptiste Meyer <baptiste@les-tilleuls.coop>
 */
final class ElasticsearchClientPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
