<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection\Compiler;

/**
 * Checks if the new authenticator manager exists.
 *
 * @internal
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class AuthenticatorManagerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
}
