<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection;

/**
 * The extension of this bundle.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ApiPlatformExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension implements \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function prepend(\Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
}
