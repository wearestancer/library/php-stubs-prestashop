<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection\Compiler;

/**
 * Handles Mercure Publisher depreciation.
 *
 * @internal calls `setDeprecated` method with valid arguments
 *  depending which version of symfony/dependency-injection is used
 */
final class DeprecateMercurePublisherPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
