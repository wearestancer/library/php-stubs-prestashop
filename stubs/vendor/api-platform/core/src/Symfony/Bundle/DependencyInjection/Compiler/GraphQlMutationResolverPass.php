<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection\Compiler;

/**
 * Injects GraphQL Mutation resolvers.
 *
 * @internal
 *
 * @author Raoul Clais <raoul.clais@gmail.com>
 */
final class GraphQlMutationResolverPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
