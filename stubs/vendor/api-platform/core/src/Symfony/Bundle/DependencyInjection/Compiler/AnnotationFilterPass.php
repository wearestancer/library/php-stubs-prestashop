<?php

namespace ApiPlatform\Symfony\Bundle\DependencyInjection\Compiler;

/**
 * Registers filter services from {@see ApiFilter} annotations.
 *
 * @internal
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class AnnotationFilterPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \ApiPlatform\Util\AnnotationFilterExtractorTrait;
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
}
