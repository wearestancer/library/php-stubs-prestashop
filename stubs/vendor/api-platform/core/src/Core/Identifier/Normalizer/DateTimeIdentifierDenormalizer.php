<?php

namespace ApiPlatform\Core\Identifier\Normalizer;

final class DateTimeIdentifierDenormalizer extends \Symfony\Component\Serializer\Normalizer\DateTimeNormalizer
{
    /**
     * @param mixed|null $format
     * @param mixed      $data
     * @param mixed      $class
     */
    public function denormalize($data, $class, $format = null, array $context = []) : \DateTimeInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
