<?php

namespace ApiPlatform\Core\Identifier;

/**
 * Identifier converter that chains identifier denormalizers.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class IdentifierConverter implements \ApiPlatform\Core\Identifier\ContextAwareIdentifierConverterInterface
{
    /**
     * TODO: rename identifierDenormalizers to identifierTransformers in 3.0 and change their interfaces to a IdentifierTransformerInterface.
     *
     * @param iterable<DenormalizerInterface> $identifierDenormalizers
     */
    public function __construct(\ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, iterable $identifierDenormalizers, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function convert($data, string $class, array $context = []) : array
    {
    }
}
