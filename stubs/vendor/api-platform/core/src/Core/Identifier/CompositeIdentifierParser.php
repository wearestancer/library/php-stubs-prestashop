<?php

namespace ApiPlatform\Core\Identifier;

/**
 * Normalizes a composite identifier.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class CompositeIdentifierParser
{
    public const COMPOSITE_IDENTIFIER_REGEXP = '/(\\w+)=(?<=\\w=)(.*?)(?=;\\w+=)|(\\w+)=([^;]*);?$/';
    /*
     * Normalize takes a string and gives back an array of identifiers.
     *
     * For example: foo=0;bar=2 returns ['foo' => 0, 'bar' => 2].
     */
    public static function parse(string $identifier) : array
    {
    }
    /**
     * Renders composite identifiers to string using: key=value;key2=value2.
     */
    public static function stringify(array $identifiers) : string
    {
    }
}
