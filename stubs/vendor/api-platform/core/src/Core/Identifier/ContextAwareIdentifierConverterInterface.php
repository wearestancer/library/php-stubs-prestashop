<?php

namespace ApiPlatform\Core\Identifier;

/**
 * Gives access to the context in the IdentifierConverter.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface ContextAwareIdentifierConverterInterface extends \ApiPlatform\Core\Identifier\IdentifierConverterInterface
{
    /**
     * {@inheritdoc}
     */
    public function convert($data, string $class, array $context = []) : array;
}
