<?php

namespace ApiPlatform\Core\GraphQl\Subscription;

/**
 * Manages all the queried subscriptions by creating their ID
 * and saving to a cache the information needed to publish updated data.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SubscriptionManager implements \ApiPlatform\Core\GraphQl\Subscription\SubscriptionManagerInterface
{
    use \ApiPlatform\Core\GraphQl\Resolver\Util\IdentifierTrait;
    use \ApiPlatform\Core\Util\ResourceClassInfoTrait;
    use \ApiPlatform\Core\Util\SortTrait;
    public function __construct(\Psr\Cache\CacheItemPoolInterface $subscriptionsCache, \ApiPlatform\Core\GraphQl\Subscription\SubscriptionIdentifierGeneratorInterface $subscriptionIdentifierGenerator, \ApiPlatform\Core\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter)
    {
    }
    public function retrieveSubscriptionId(array $context, ?array $result) : ?string
    {
    }
    /**
     * @param object $object
     */
    public function getPushPayloads($object) : array
    {
    }
}
