<?php

namespace ApiPlatform\Core\GraphQl\Subscription;

/**
 * Generates Mercure-related IRIs from a subscription ID.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface MercureSubscriptionIriGeneratorInterface
{
    public function generateTopicIri(string $subscriptionId) : string;
    public function generateMercureUrl(string $subscriptionId, ?string $hub = null) : string;
}
