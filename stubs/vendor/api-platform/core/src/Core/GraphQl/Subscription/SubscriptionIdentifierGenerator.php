<?php

namespace ApiPlatform\Core\GraphQl\Subscription;

/**
 * Generates an identifier used to identify a subscription.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SubscriptionIdentifierGenerator implements \ApiPlatform\Core\GraphQl\Subscription\SubscriptionIdentifierGeneratorInterface
{
    public function generateSubscriptionIdentifier(array $fields) : string
    {
    }
}
