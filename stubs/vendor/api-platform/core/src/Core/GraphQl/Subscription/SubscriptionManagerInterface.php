<?php

namespace ApiPlatform\Core\GraphQl\Subscription;

/**
 * Manages all the queried subscriptions and creates their ID.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SubscriptionManagerInterface
{
    public function retrieveSubscriptionId(array $context, ?array $result) : ?string;
    public function getPushPayloads($object) : array;
}
