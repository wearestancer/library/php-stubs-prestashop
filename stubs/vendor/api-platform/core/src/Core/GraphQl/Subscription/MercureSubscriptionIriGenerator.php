<?php

namespace ApiPlatform\Core\GraphQl\Subscription;

/**
 * Generates Mercure-related IRIs from a subscription ID.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class MercureSubscriptionIriGenerator implements \ApiPlatform\Core\GraphQl\Subscription\MercureSubscriptionIriGeneratorInterface
{
    /**
     * @param HubRegistry|string $registry
     */
    public function __construct(\Symfony\Component\Routing\RequestContext $requestContext, $registry)
    {
    }
    public function generateTopicIri(string $subscriptionId) : string
    {
    }
    public function generateMercureUrl(string $subscriptionId, ?string $hub = null) : string
    {
    }
}
