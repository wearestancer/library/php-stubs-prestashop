<?php

namespace ApiPlatform\Core\GraphQl\Subscription;

/**
 * Generates an identifier used to identify a subscription.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SubscriptionIdentifierGeneratorInterface
{
    public function generateSubscriptionIdentifier(array $fields) : string;
}
