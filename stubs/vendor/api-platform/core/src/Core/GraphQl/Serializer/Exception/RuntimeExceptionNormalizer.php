<?php

namespace ApiPlatform\Core\GraphQl\Serializer\Exception;

/**
 * Normalize runtime exceptions to have the right message in production mode.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class RuntimeExceptionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
}
