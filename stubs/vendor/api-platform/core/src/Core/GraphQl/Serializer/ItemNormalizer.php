<?php

namespace ApiPlatform\Core\GraphQl\Serializer;

/**
 * GraphQL normalizer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ItemNormalizer extends \ApiPlatform\Core\Serializer\ItemNormalizer
{
    use \ApiPlatform\Core\Util\ClassInfoTrait;
    public const FORMAT = 'graphql';
    public const ITEM_RESOURCE_CLASS_KEY = '#itemResourceClass';
    public const ITEM_IDENTIFIERS_KEY = '#itemIdentifiers';
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider = null, bool $allowPlainIdentifiers = false, \Psr\Log\LoggerInterface $logger = null, iterable $dataTransformers = [], \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws UnexpectedValueException
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
}
