<?php

namespace ApiPlatform\Core\GraphQl\Serializer;

/**
 * Builds the context used by the Symfony Serializer.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SerializerContextBuilder implements \ApiPlatform\Core\GraphQl\Serializer\SerializerContextBuilderInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter)
    {
    }
    public function create(?string $resourceClass, string $operationName, array $resolverContext, bool $normalization) : array
    {
    }
}
