<?php

namespace ApiPlatform\Core\GraphQl\Serializer;

/**
 * Builds the context used by the Symfony Serializer.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SerializerContextBuilderInterface
{
    public function create(string $resourceClass, string $operationName, array $resolverContext, bool $normalization) : array;
}
