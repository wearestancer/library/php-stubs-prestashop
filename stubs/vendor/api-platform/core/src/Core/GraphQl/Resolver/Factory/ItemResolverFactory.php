<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Factory;

/**
 * Creates a function retrieving an item to resolve a GraphQL query.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ItemResolverFactory implements \ApiPlatform\Core\GraphQl\Resolver\Factory\ResolverFactoryInterface
{
    use \ApiPlatform\Core\Util\ClassInfoTrait;
    use \ApiPlatform\Core\Util\CloneTrait;
    public function __construct(\ApiPlatform\Core\GraphQl\Resolver\Stage\ReadStageInterface $readStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface $securityStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityPostDenormalizeStageInterface $securityPostDenormalizeStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \Psr\Container\ContainerInterface $queryResolverLocator, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory)
    {
    }
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?string $operationName = null) : callable
    {
    }
}
