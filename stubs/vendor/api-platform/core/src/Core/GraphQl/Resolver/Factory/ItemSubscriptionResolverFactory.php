<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Factory;

/**
 * Creates a function resolving a GraphQL subscription of an item.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ItemSubscriptionResolverFactory implements \ApiPlatform\Core\GraphQl\Resolver\Factory\ResolverFactoryInterface
{
    use \ApiPlatform\Core\Util\ClassInfoTrait;
    use \ApiPlatform\Core\Util\CloneTrait;
    public function __construct(\ApiPlatform\Core\GraphQl\Resolver\Stage\ReadStageInterface $readStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface $securityStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\GraphQl\Subscription\SubscriptionManagerInterface $subscriptionManager, ?\ApiPlatform\Core\GraphQl\Subscription\MercureSubscriptionIriGeneratorInterface $mercureSubscriptionIriGenerator)
    {
    }
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?string $operationName = null) : callable
    {
    }
}
