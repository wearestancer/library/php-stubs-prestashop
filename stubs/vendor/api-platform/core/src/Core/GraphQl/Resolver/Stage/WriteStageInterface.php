<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Write stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface WriteStageInterface
{
    /**
     * @param object|null $data
     *
     * @return object|null
     */
    public function __invoke($data, string $resourceClass, string $operationName, array $context);
}
