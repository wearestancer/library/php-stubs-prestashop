<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Validate stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface ValidateStageInterface
{
    /**
     * @param object $object
     *
     * @throws Error
     */
    public function __invoke($object, string $resourceClass, string $operationName, array $context) : void;
}
