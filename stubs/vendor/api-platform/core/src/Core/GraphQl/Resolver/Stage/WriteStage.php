<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Write stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class WriteStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\WriteStageInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface $dataPersister, \ApiPlatform\Core\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($data, string $resourceClass, string $operationName, array $context)
    {
    }
}
