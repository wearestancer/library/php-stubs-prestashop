<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Deserialize stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class DeserializeStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\DeserializeStageInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer, \ApiPlatform\Core\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($objectToPopulate, string $resourceClass, string $operationName, array $context)
    {
    }
}
