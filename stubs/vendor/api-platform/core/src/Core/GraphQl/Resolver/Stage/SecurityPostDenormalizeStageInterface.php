<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Security post deserialization stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface SecurityPostDenormalizeStageInterface
{
    /**
     * @throws Error
     */
    public function __invoke(string $resourceClass, string $operationName, array $context) : void;
}
