<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Serialize stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SerializeStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\SerializeStageInterface
{
    use \ApiPlatform\Core\GraphQl\Resolver\Util\IdentifierTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer, \ApiPlatform\Core\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder, \ApiPlatform\Core\DataProvider\Pagination $pagination)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($itemOrCollection, string $resourceClass, string $operationName, array $context) : ?array
    {
    }
}
