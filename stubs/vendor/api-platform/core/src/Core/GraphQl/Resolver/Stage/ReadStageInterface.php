<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Read stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface ReadStageInterface
{
    /**
     * @return object|iterable|null
     */
    public function __invoke(?string $resourceClass, ?string $rootClass, string $operationName, array $context);
}
