<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Factory;

/**
 * Creates a function resolving a GraphQL mutation of an item.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ItemMutationResolverFactory implements \ApiPlatform\Core\GraphQl\Resolver\Factory\ResolverFactoryInterface
{
    use \ApiPlatform\Core\Util\ClassInfoTrait;
    use \ApiPlatform\Core\Util\CloneTrait;
    public function __construct(\ApiPlatform\Core\GraphQl\Resolver\Stage\ReadStageInterface $readStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface $securityStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityPostDenormalizeStageInterface $securityPostDenormalizeStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\DeserializeStageInterface $deserializeStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\WriteStageInterface $writeStage, \ApiPlatform\Core\GraphQl\Resolver\Stage\ValidateStageInterface $validateStage, \Psr\Container\ContainerInterface $mutationResolverLocator, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory)
    {
    }
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?string $operationName = null) : callable
    {
    }
}
