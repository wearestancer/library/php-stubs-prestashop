<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Security stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface SecurityStageInterface
{
    /**
     * @throws Error
     */
    public function __invoke(string $resourceClass, string $operationName, array $context) : void;
}
