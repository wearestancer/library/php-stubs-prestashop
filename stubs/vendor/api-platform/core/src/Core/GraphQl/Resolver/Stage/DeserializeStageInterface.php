<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Deserialize stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface DeserializeStageInterface
{
    /**
     * @param object|null $objectToPopulate
     *
     * @return object|null
     */
    public function __invoke($objectToPopulate, string $resourceClass, string $operationName, array $context);
}
