<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Read stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ReadStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\ReadStageInterface
{
    use \ApiPlatform\Core\Util\ArrayTrait;
    use \ApiPlatform\Core\Util\ClassInfoTrait;
    use \ApiPlatform\Core\GraphQl\Resolver\Util\IdentifierTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter, \ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface $collectionDataProvider, \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface $subresourceDataProvider, \ApiPlatform\Core\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder, string $nestingSeparator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(?string $resourceClass, ?string $rootClass, string $operationName, array $context)
    {
    }
}
