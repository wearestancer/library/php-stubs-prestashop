<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Factory;

/**
 * Builds a GraphQL resolver.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ResolverFactoryInterface
{
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?string $operationName = null) : callable;
}
