<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Security post denormalize stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class SecurityPostDenormalizeStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityPostDenormalizeStageInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, ?\ApiPlatform\Core\Security\ResourceAccessCheckerInterface $resourceAccessChecker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(string $resourceClass, string $operationName, array $context) : void
    {
    }
}
