<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Validate stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ValidateStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\ValidateStageInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Validator\ValidatorInterface $validator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($object, string $resourceClass, string $operationName, array $context) : void
    {
    }
}
