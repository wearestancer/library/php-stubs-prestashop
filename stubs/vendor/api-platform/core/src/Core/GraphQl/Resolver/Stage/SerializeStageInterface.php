<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Serialize stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SerializeStageInterface
{
    /**
     * @param object|iterable|null $itemOrCollection
     */
    public function __invoke($itemOrCollection, string $resourceClass, string $operationName, array $context) : ?array;
}
