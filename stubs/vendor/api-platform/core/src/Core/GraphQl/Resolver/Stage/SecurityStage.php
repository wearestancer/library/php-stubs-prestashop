<?php

namespace ApiPlatform\Core\GraphQl\Resolver\Stage;

/**
 * Security stage of GraphQL resolvers.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SecurityStage implements \ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, ?\ApiPlatform\Core\Security\ResourceAccessCheckerInterface $resourceAccessChecker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(string $resourceClass, string $operationName, array $context) : void
    {
    }
}
