<?php

namespace ApiPlatform\Core\GraphQl\Type;

/**
 * Builds the GraphQL types.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypeBuilder implements \ApiPlatform\Core\GraphQl\Type\TypeBuilderInterface
{
    public function __construct($typesContainer, callable $defaultFieldResolver, \Psr\Container\ContainerInterface $fieldsBuilderLocator, \ApiPlatform\Core\DataProvider\Pagination $pagination)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceObjectType(?string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, bool $input, ?string $queryName, ?string $mutationName, ?string $subscriptionName, bool $wrapped = false, int $depth = 0) : \GraphQL\Type\Definition\Type
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNodeInterface() : \GraphQL\Type\Definition\InterfaceType
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourcePaginatedCollectionType(\GraphQL\Type\Definition\Type $resourceType, string $resourceClass, string $operationName) : \GraphQL\Type\Definition\Type
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isCollection(\Symfony\Component\PropertyInfo\Type $type) : bool
    {
    }
}
