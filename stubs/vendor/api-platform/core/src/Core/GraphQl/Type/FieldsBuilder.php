<?php

namespace ApiPlatform\Core\GraphQl\Type;

/**
 * Builds the GraphQL fields.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class FieldsBuilder implements \ApiPlatform\Core\GraphQl\Type\FieldsBuilderInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, $typesContainer, \ApiPlatform\Core\GraphQl\Type\TypeBuilderInterface $typeBuilder, $typeConverter, $itemResolverFactory, $collectionResolverFactory, $itemMutationResolverFactory, $itemSubscriptionResolverFactory, \Psr\Container\ContainerInterface $filterLocator, \ApiPlatform\Core\DataProvider\Pagination $pagination, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter, string $nestingSeparator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNodeQueryFields() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemQueryFields(string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, string $queryName, array $configuration) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollectionQueryFields(string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, string $queryName, array $configuration) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMutationFields(string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, string $mutationName) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubscriptionFields(string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, string $subscriptionName) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceObjectTypeFields(?string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, bool $input, ?string $queryName, ?string $mutationName, ?string $subscriptionName, int $depth = 0, ?array $ioMetadata = null) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveResourceArgs(array $args, string $operationName, string $shortName) : array
    {
    }
}
