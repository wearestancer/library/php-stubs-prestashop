<?php

namespace ApiPlatform\Core\GraphQl\Type;

final class TypeNotFoundException extends \ApiPlatform\GraphQl\Type\TypeNotFoundException
{
}
