<?php

namespace ApiPlatform\Core\GraphQl\Type;

/**
 * Builds the GraphQL schema.
 *
 * @author Raoul Clais <raoul.clais@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SchemaBuilder implements \ApiPlatform\Core\GraphQl\Type\SchemaBuilderInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, $typesFactory, $typesContainer, \ApiPlatform\Core\GraphQl\Type\FieldsBuilderInterface $fieldsBuilder)
    {
    }
    public function getSchema() : \GraphQL\Type\Schema
    {
    }
}
