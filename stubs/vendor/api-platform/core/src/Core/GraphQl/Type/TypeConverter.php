<?php

namespace ApiPlatform\Core\GraphQl\Type;

/**
 * Converts a type to its GraphQL equivalent.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypeConverter implements \ApiPlatform\Core\GraphQl\Type\TypeConverterInterface
{
    public function __construct(\ApiPlatform\Core\GraphQl\Type\TypeBuilderInterface $typeBuilder, $typesContainer, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, $propertyMetadataFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function convertType(\Symfony\Component\PropertyInfo\Type $type, bool $input, ?string $queryName, ?string $mutationName, ?string $subscriptionName, string $resourceClass, string $rootResource, ?string $property, int $depth)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveType(string $type) : ?\GraphQL\Type\Definition\Type
    {
    }
}
