<?php

namespace ApiPlatform\Core\GraphQl\Type;

/**
 * Interface implemented to build a GraphQL type.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface TypeBuilderInterface
{
    /**
     * Gets the object type of the given resource.
     *
     * @return ObjectType|NonNull the object type, possibly wrapped by NonNull
     */
    public function getResourceObjectType(?string $resourceClass, \ApiPlatform\Core\Metadata\Resource\ResourceMetadata $resourceMetadata, bool $input, ?string $queryName, ?string $mutationName, ?string $subscriptionName, bool $wrapped, int $depth) : \GraphQL\Type\Definition\Type;
    /**
     * Get the interface type of a node.
     */
    public function getNodeInterface() : \GraphQL\Type\Definition\InterfaceType;
    /**
     * Gets the type of a paginated collection of the given resource type.
     */
    public function getResourcePaginatedCollectionType(\GraphQL\Type\Definition\Type $resourceType, string $resourceClass, string $operationName) : \GraphQL\Type\Definition\Type;
    /**
     * Returns true if a type is a collection.
     */
    public function isCollection(\Symfony\Component\PropertyInfo\Type $type) : bool;
}
