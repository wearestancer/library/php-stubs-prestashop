<?php

namespace ApiPlatform\Core\GraphQl\Type;

/**
 * Builds a GraphQL schema.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SchemaBuilderInterface
{
    public function getSchema() : \GraphQL\Type\Schema;
}
