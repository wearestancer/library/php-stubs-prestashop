<?php

namespace ApiPlatform\Core\Hydra\JsonSchema;

/**
 * Decorator factory which adds Hydra properties to the JSON Schema document.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SchemaFactory implements \ApiPlatform\Core\JsonSchema\SchemaFactoryInterface
{
    public function __construct(\ApiPlatform\Core\JsonSchema\SchemaFactoryInterface $schemaFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildSchema(string $className, string $format = 'json', string $type = \ApiPlatform\Core\JsonSchema\Schema::TYPE_OUTPUT, ?string $operationType = null, ?string $operationName = null, ?\ApiPlatform\Core\JsonSchema\Schema $schema = null, ?array $serializerContext = null, bool $forceCollection = false) : \ApiPlatform\Core\JsonSchema\Schema
    {
    }
    public function addDistinctFormat(string $format) : void
    {
    }
}
