<?php

namespace ApiPlatform\Core\Api;

/**
 * Extracts identifiers for a given Resource according to the retrieved Metadata.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface IdentifiersExtractorInterface
{
    /**
     * Finds identifiers from a Resource class.
     */
    public function getIdentifiersFromResourceClass(string $resourceClass) : array;
    /**
     * Finds identifiers from an Item (object).
     *
     * @param object $item
     *
     * @throws RuntimeException
     */
    public function getIdentifiersFromItem($item) : array;
}
