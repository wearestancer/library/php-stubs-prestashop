<?php

namespace ApiPlatform\Core\Api;

/**
 * Extracts formats for a given operation according to the retrieved Metadata.
 *
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 *
 * @deprecated since API Platform 2.5, use the "formats" attribute instead
 */
interface OperationAwareFormatsProviderInterface extends \ApiPlatform\Core\Api\FormatsProviderInterface
{
    /**
     * Finds formats for an operation.
     */
    public function getFormatsFromOperation(string $resourceClass, string $operationName, string $operationType) : array;
}
