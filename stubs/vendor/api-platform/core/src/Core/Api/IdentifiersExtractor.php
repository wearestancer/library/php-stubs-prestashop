<?php

namespace ApiPlatform\Core\Api;

/**
 * {@inheritdoc}
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class IdentifiersExtractor implements \ApiPlatform\Core\Api\IdentifiersExtractorInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver = null, bool $metadataBackwardCompatibilityLayer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifiersFromResourceClass(string $resourceClass) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifiersFromItem($item) : array
    {
    }
}
