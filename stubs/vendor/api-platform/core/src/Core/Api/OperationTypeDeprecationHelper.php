<?php

namespace ApiPlatform\Core\Api;

/**
 * Class OperationTypeDeprecationHelper
 * Before API Platform 2.1, the operation type was one of:
 * - "collection" (true)
 * - "item" (false).
 *
 * Because we introduced a third type in API Platform 2.1, we're using a string with OperationType constants:
 * - OperationType::ITEM
 * - OperationType::COLLECTION
 * - OperationType::SUBRESOURCE
 *
 * @internal
 */
final class OperationTypeDeprecationHelper
{
    /**
     * @param string|bool $operationType
     */
    public static function getOperationType($operationType) : string
    {
    }
}
