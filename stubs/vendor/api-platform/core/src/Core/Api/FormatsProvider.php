<?php

namespace ApiPlatform\Core\Api;

/**
 * {@inheritdoc}
 *
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 *
 * @deprecated since API Platform 2.5, use the "formats" attribute instead
 */
final class FormatsProvider implements \ApiPlatform\Core\Api\FormatsProviderInterface, \ApiPlatform\Core\Api\OperationAwareFormatsProviderInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, array $configuredFormats)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function getFormatsFromAttributes(array $attributes) : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function getFormatsFromOperation(string $resourceClass, string $operationName, string $operationType) : array
    {
    }
}
