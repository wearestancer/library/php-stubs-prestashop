<?php

namespace ApiPlatform\Core\Api;

/**
 * {@inheritdoc}
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class CachedIdentifiersExtractor implements \ApiPlatform\Core\Api\IdentifiersExtractorInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public const CACHE_KEY_PREFIX = 'iri_identifiers';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $decorated, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifiersFromResourceClass(string $resourceClass) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifiersFromItem($item) : array
    {
    }
}
