<?php

namespace ApiPlatform\Core\Api;

/**
 * Manipulates filters with a backward compatibility between the new filter locator and the deprecated filter collection.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 *
 * @internal
 */
trait FilterLocatorTrait
{
    private $filterLocator;
    /**
     * Sets a filter locator with a backward compatibility.
     *
     * @param ContainerInterface|FilterCollection|null $filterLocator
     */
    private function setFilterLocator($filterLocator, bool $allowNull = false) : void
    {
    }
    /**
     * Gets a filter with a backward compatibility.
     */
    private function getFilter(string $filterId) : ?\ApiPlatform\Core\Api\FilterInterface
    {
    }
}
