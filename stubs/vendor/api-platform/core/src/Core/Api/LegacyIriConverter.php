<?php

namespace ApiPlatform\Core\Api;

/**
 * This IRI converter calls the legacy IriConverter on legacy resources.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @internal
 */
final class LegacyIriConverter implements \ApiPlatform\Api\IriConverterInterface
{
    public function __construct(\ApiPlatform\Core\Api\IriConverterInterface $legacyIriConverter, \ApiPlatform\Api\IriConverterInterface $iriConverter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceFromIri(string $iri, array $context = [], ?\ApiPlatform\Metadata\Operation $operation = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIriFromResource($item, int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : ?string
    {
    }
}
