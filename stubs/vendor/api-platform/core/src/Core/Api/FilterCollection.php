<?php

namespace ApiPlatform\Core\Api;

/**
 * A list of filters.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @deprecated since version 2.1, to be removed in 3.0. Use a service locator {@see \Psr\Container\ContainerInterface}.
 */
final class FilterCollection extends \ArrayObject
{
    public function __construct($input = [], $flags = 0, $iterator_class = 'ArrayIterator')
    {
    }
}
