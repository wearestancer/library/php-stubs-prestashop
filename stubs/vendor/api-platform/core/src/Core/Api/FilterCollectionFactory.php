<?php

namespace ApiPlatform\Core\Api;

/**
 * Filter collection factory.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 *
 * @deprecated see FilterCollection
 *
 * @internal
 */
class FilterCollectionFactory
{
    /**
     * @param string[] $filtersIds
     */
    public function __construct(array $filtersIds)
    {
    }
    /**
     * Creates a filter collection from a filter locator.
     */
    public function createFilterCollectionFromLocator(\Psr\Container\ContainerInterface $filterLocator) : \ApiPlatform\Core\Api\FilterCollection
    {
    }
}
