<?php

namespace ApiPlatform\Core\Api;

/**
 * Resolves the uppercased HTTP method associated with an operation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @deprecated since API Platform 2.5, use the "method" attribute instead
 */
interface OperationMethodResolverInterface
{
    /**
     * Resolves the uppercased HTTP method associated with a collection operation.
     *
     * @throws RuntimeException
     */
    public function getCollectionOperationMethod(string $resourceClass, string $operationName) : string;
    /**
     * Resolves the uppercased HTTP method associated with an item operation.
     *
     * @throws RuntimeException
     */
    public function getItemOperationMethod(string $resourceClass, string $operationName) : string;
}
