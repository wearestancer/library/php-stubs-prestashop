<?php

namespace ApiPlatform\Core\HttpCache;

/**
 * Purges resources from the cache.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @experimental
 */
interface PurgerInterface
{
    /**
     * Purges all responses containing the given resources from the cache.
     *
     * @param string[] $iris
     */
    public function purge(array $iris) : void;
}
