<?php

namespace ApiPlatform\Core\EventListener;

/**
 * Retrieves data from the applicable data provider and sets it as a request parameter called data.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @deprecated
 */
final class ReadListener
{
    use \ApiPlatform\Util\CloneTrait;
    use \ApiPlatform\Core\DataProvider\OperationDataProviderTrait;
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\Core\Metadata\Resource\ToggleableOperationAttributeTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'read';
    public function __construct(\ApiPlatform\Core\DataProvider\CollectionDataProviderInterface $collectionDataProvider, \ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider, \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface $subresourceDataProvider = null, \ApiPlatform\Serializer\SerializerContextBuilderInterface $serializerContextBuilder = null, \ApiPlatform\Core\Identifier\IdentifierConverterInterface $identifierConverter = null, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory = null, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory = null, bool $metadataBackwardCompatibilityLayer = null)
    {
    }
    /**
     * Calls the data provider and sets the data attribute.
     *
     * @throws NotFoundHttpException
     */
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
}
