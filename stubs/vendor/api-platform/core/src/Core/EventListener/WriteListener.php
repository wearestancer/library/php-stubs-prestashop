<?php

namespace ApiPlatform\Core\EventListener;

/**
 * Bridges persistence and the API system.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 *
 * @deprecated
 */
final class WriteListener
{
    use \ApiPlatform\Util\OperationRequestInitiatorTrait;
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    use \ApiPlatform\Core\Metadata\Resource\ToggleableOperationAttributeTrait;
    public const OPERATION_ATTRIBUTE_KEY = 'write';
    public function __construct(\ApiPlatform\Core\DataPersister\DataPersisterInterface $dataPersister, $iriConverter = null, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory = null, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver = null, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory = null, ?bool $metadataBackwardCompatibilityLayer = null)
    {
    }
    /**
     * Persists, updates or delete data return by the controller if applicable.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
