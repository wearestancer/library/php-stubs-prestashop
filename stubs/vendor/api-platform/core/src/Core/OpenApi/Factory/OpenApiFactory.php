<?php

namespace ApiPlatform\Core\OpenApi\Factory;

/**
 * Generates an Open API v3 specification.
 */
final class OpenApiFactory implements \ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface
{
    use \ApiPlatform\Core\Api\FilterLocatorTrait;
    public const BASE_URL = 'base_url';
    public const OPENAPI_DEFINITION_NAME = 'openapi_definition_name';
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\JsonSchema\SchemaFactoryInterface $jsonSchemaFactory, \ApiPlatform\JsonSchema\TypeFactoryInterface $jsonSchemaTypeFactory, \ApiPlatform\PathResolver\OperationPathResolverInterface $operationPathResolver, \Psr\Container\ContainerInterface $filterLocator, \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $subresourceOperationFactory, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor = null, array $formats = [], \ApiPlatform\OpenApi\Options $openApiOptions = null, \ApiPlatform\State\Pagination\PaginationOptions $paginationOptions = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(array $context = []) : \ApiPlatform\OpenApi\OpenApi
    {
    }
}
