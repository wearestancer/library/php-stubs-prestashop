<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Injects serializer in data providers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
trait SerializerAwareDataProviderTrait
{
    /**
     * @internal
     *
     * @var ContainerInterface
     */
    private $serializerLocator;
    public function setSerializerLocator(\Psr\Container\ContainerInterface $serializerLocator) : void
    {
    }
    private function getSerializer() : \Symfony\Component\Serializer\SerializerInterface
    {
    }
}
