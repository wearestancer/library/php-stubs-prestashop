<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Injects serializer in data providers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface SerializerAwareDataProviderInterface
{
    public function setSerializerLocator(\Psr\Container\ContainerInterface $serializerLocator);
}
