<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Retrieves items from a persistence layer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ItemDataProviderInterface
{
    /**
     * Retrieves an item.
     *
     * @param array|int|object|string $id
     *
     * @throws ResourceClassNotSupportedException
     *
     * @return object|null
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []);
}
