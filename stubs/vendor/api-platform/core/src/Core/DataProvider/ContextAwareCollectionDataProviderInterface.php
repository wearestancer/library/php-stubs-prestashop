<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Retrieves items from a persistence layer and allow to pass a context to it.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareCollectionDataProviderInterface extends \ApiPlatform\Core\DataProvider\CollectionDataProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []);
}
