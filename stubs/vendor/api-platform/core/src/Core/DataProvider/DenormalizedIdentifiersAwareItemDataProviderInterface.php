<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Marks data providers able to deal with complex identifiers denormalized as an array.
 *
 * @author Anthony GRASSIOT <antograssiot@free.Fr>
 */
interface DenormalizedIdentifiersAwareItemDataProviderInterface extends \ApiPlatform\Core\DataProvider\ItemDataProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getItem(
        string $resourceClass,
        /* array */
        $id,
        string $operationName = null,
        array $context = []
    );
}
