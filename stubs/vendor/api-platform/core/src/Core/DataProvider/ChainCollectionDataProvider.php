<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Tries each configured data provider and returns the result of the first able to handle the resource class.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ChainCollectionDataProvider implements \ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    use \ApiPlatform\Core\DataProvider\RestrictDataProviderTrait;
    /**
     * @param CollectionDataProviderInterface[] $dataProviders
     */
    public function __construct(iterable $dataProviders)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []) : iterable
    {
    }
}
