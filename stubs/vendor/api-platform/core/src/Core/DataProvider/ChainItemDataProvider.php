<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Tries each configured data provider and returns the result of the first able to handle the resource class.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ChainItemDataProvider implements \ApiPlatform\Core\DataProvider\ItemDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    use \ApiPlatform\Core\DataProvider\RestrictDataProviderTrait;
    /**
     * @param ItemDataProviderInterface[] $dataProviders
     */
    public function __construct(iterable $dataProviders)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
    }
}
