<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * @internal
 */
trait OperationDataProviderTrait
{
    /**
     * @var CollectionDataProviderInterface
     */
    private $collectionDataProvider;
    /**
     * @var ItemDataProviderInterface
     */
    private $itemDataProvider;
    /**
     * @var SubresourceDataProviderInterface|null
     */
    private $subresourceDataProvider;
    /**
     * @var IdentifierConverterInterface|null
     */
    private $identifierConverter;
    /**
     * Retrieves data for a collection operation.
     */
    private function getCollectionData(array $attributes, array $context) : iterable
    {
    }
    /**
     * Gets data for an item operation.
     *
     * @param mixed $identifiers
     *
     * @return object|null
     */
    private function getItemData($identifiers, array $attributes, array $context)
    {
    }
    /**
     * Gets data for a nested operation.
     *
     * @param mixed $identifiers
     *
     * @throws RuntimeException
     *
     * @return array|object|null
     */
    private function getSubresourceData($identifiers, array $attributes, array $context)
    {
    }
    /**
     * @param array $parameters - usually comes from $request->attributes->all()
     *
     * @throws InvalidIdentifierException
     */
    private function extractIdentifiers(array $parameters, array $attributes)
    {
    }
}
