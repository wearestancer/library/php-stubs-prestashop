<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Tries each configured data provider and returns the result of the first able to handle the resource class.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class ChainSubresourceDataProvider implements \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface
{
    /**
     * @var iterable<SubresourceDataProviderInterface>
     *
     * @internal
     */
    public $dataProviders;
    /**
     * @param SubresourceDataProviderInterface[] $dataProviders
     */
    public function __construct(iterable $dataProviders)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
    }
}
