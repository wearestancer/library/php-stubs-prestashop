<?php

namespace ApiPlatform\Core\DataProvider;

/**
 * Pagination configuration.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class Pagination
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, array $options = [], array $graphQlOptions = [])
    {
    }
    /**
     * Gets the current page.
     *
     * @throws InvalidArgumentException
     */
    public function getPage(array $context = []) : int
    {
    }
    /**
     * Gets the current offset.
     */
    public function getOffset(string $resourceClass = null, string $operationName = null, array $context = []) : int
    {
    }
    /**
     * Gets the current limit.
     *
     * @throws InvalidArgumentException
     */
    public function getLimit(string $resourceClass = null, string $operationName = null, array $context = []) : int
    {
    }
    /**
     * Gets info about the pagination.
     *
     * Returns an array with the following info as values:
     *   - the page {@see Pagination::getPage()}
     *   - the offset {@see Pagination::getOffset()}
     *   - the limit {@see Pagination::getLimit()}
     *
     * @throws InvalidArgumentException
     */
    public function getPagination(string $resourceClass = null, string $operationName = null, array $context = []) : array
    {
    }
    /**
     * Is the pagination enabled?
     */
    public function isEnabled(string $resourceClass = null, string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * Is the pagination enabled for GraphQL?
     */
    public function isGraphQlEnabled(?string $resourceClass = null, ?string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * Is the partial pagination enabled?
     */
    public function isPartialEnabled(string $resourceClass = null, string $operationName = null, array $context = []) : bool
    {
    }
    public function getOptions() : array
    {
    }
    public function getGraphQlPaginationType(string $resourceClass, string $operationName) : string
    {
    }
}
