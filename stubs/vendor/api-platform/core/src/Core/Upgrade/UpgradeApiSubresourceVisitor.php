<?php

namespace ApiPlatform\Core\Upgrade;

final class UpgradeApiSubresourceVisitor extends \PhpParser\NodeVisitorAbstract
{
    use \ApiPlatform\Core\Upgrade\RemoveAnnotationTrait;
    public function __construct($subresourceMetadata, $referenceType)
    {
    }
    /**
     * @return int|Node|null
     */
    public function enterNode(\PhpParser\Node $node)
    {
    }
}
