<?php

namespace ApiPlatform\Core\Upgrade;

/**
 * Inspired by @see https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/master/src/Differ/DiffConsoleFormatter.php to be
 * used as standalone class, without need to require whole package by Dariusz Rumiński <dariusz.ruminski@gmail.com>
 * Forked by @soyuka from Symplify\PackageBuilder\Console\Formatter to remove Nette\Utils\Strings dependency to be even more standalone.
 *
 * @see \Symplify\PackageBuilder\Tests\Console\Formatter\ColorConsoleDiffFormatterTest
 *
 * @internal
 */
final class ColorConsoleDiffFormatter
{
    public function __construct()
    {
    }
    public function format(string $diff) : string
    {
    }
}
