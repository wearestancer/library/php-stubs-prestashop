<?php

namespace ApiPlatform\Core\Upgrade;

final class UpgradeApiFilterVisitor extends \PhpParser\NodeVisitorAbstract
{
    use \ApiPlatform\Metadata\Resource\DeprecationMetadataTrait;
    use \ApiPlatform\Core\Upgrade\RemoveAnnotationTrait;
    public function __construct(?\Doctrine\Common\Annotations\AnnotationReader $reader, string $resourceClass)
    {
    }
    /**
     * @return int|Node|null
     */
    public function enterNode(\PhpParser\Node $node)
    {
    }
}
