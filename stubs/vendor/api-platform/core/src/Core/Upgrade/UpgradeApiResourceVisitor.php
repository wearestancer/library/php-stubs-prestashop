<?php

namespace ApiPlatform\Core\Upgrade;

final class UpgradeApiResourceVisitor extends \PhpParser\NodeVisitorAbstract
{
    use \ApiPlatform\Metadata\Resource\DeprecationMetadataTrait;
    use \ApiPlatform\Core\Upgrade\RemoveAnnotationTrait;
    public function __construct(\ApiPlatform\Core\Annotation\ApiResource $resourceAnnotation, bool $isAnnotation, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor, string $resourceClass)
    {
    }
    /**
     * @return int|Node|null
     */
    public function enterNode(\PhpParser\Node $node)
    {
    }
}
