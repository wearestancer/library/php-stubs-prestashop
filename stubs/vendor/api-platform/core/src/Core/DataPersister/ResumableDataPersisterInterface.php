<?php

namespace ApiPlatform\Core\DataPersister;

/**
 * Control the resumability of the data persister chain.
 */
interface ResumableDataPersisterInterface
{
    /**
     * Should we continue calling the next DataPersister or stop after this one?
     * Defaults to stop the ChainDatapersister if this interface is not implemented.
     */
    public function resumable(array $context = []) : bool;
}
