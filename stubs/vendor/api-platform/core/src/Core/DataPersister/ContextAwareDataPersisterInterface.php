<?php

namespace ApiPlatform\Core\DataPersister;

/**
 * Manages data persistence.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface ContextAwareDataPersisterInterface extends \ApiPlatform\Core\DataPersister\DataPersisterInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []) : bool;
    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = []);
    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = []);
}
