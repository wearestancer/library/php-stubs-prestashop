<?php

namespace ApiPlatform\Core\DataPersister;

/**
 * Manages data persistence.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface DataPersisterInterface
{
    /**
     * Is the data supported by the persister?
     *
     * @param mixed $data
     */
    public function supports($data) : bool;
    /**
     * Persists the data.
     *
     * @param mixed $data
     *
     * @return object|void Void will not be supported in API Platform 3, an object should always be returned
     */
    public function persist($data);
    /**
     * Removes the data.
     *
     * @param mixed $data
     */
    public function remove($data);
}
