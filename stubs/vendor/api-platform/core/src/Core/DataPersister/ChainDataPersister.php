<?php

namespace ApiPlatform\Core\DataPersister;

/**
 * Chained data persisters.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ChainDataPersister implements \ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface
{
    /**
     * @var iterable<DataPersisterInterface>
     *
     * @internal
     */
    public $persisters;
    /**
     * @param DataPersisterInterface[] $persisters
     */
    public function __construct(iterable $persisters)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
    }
}
