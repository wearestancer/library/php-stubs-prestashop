<?php

namespace ApiPlatform\Core\Swagger\Serializer;

/**
 * Generates an OpenAPI specification (formerly known as Swagger). OpenAPI v2 and v3 are supported.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 */
final class DocumentationNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Core\Metadata\Resource\ApiResourceToLegacyResourceMetadataTrait;
    use \ApiPlatform\Core\Api\FilterLocatorTrait;
    public const FORMAT = 'json';
    public const BASE_URL = 'base_url';
    public const SPEC_VERSION = 'spec_version';
    public const OPENAPI_VERSION = '3.0.2';
    public const SWAGGER_DEFINITION_NAME = 'swagger_definition_name';
    public const SWAGGER_VERSION = '2.0';
    /**
     * @deprecated
     */
    public const ATTRIBUTE_NAME = 'swagger_context';
    /**
     * @param LegacySchemaFactoryInterface|SchemaFactoryInterface|ResourceClassResolverInterface|null $jsonSchemaFactory
     * @param ContainerInterface|FilterCollection|null                                                $filterLocator
     * @param array|OperationAwareFormatsProviderInterface                                            $formats
     * @param mixed|null                                                                              $jsonSchemaTypeFactory
     * @param int[]                                                                                   $swaggerVersions
     * @param mixed                                                                                   $resourceMetadataFactory
     */
    public function __construct($resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, $jsonSchemaFactory = null, $jsonSchemaTypeFactory = null, \ApiPlatform\PathResolver\OperationPathResolverInterface $operationPathResolver = null, \ApiPlatform\Core\Api\UrlGeneratorInterface $urlGenerator = null, $filterLocator = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, bool $oauthEnabled = false, string $oauthType = '', string $oauthFlow = '', string $oauthTokenUrl = '', string $oauthAuthorizationUrl = '', array $oauthScopes = [], array $apiKeys = [], \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $subresourceOperationFactory = null, bool $paginationEnabled = true, string $paginationPageParameterName = 'page', bool $clientItemsPerPage = false, string $itemsPerPageParameterName = 'itemsPerPage', $formats = [], bool $paginationClientEnabled = false, string $paginationClientEnabledParameterName = 'pagination', array $defaultContext = [], array $swaggerVersions = [2, 3], \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor = null, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $openApiNormalizer = null, bool $legacyMode = false)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
