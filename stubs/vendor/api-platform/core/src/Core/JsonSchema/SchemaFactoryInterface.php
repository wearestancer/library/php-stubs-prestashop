<?php

namespace ApiPlatform\Core\JsonSchema;

/**
 * Factory for creating the JSON Schema document corresponding to a PHP class.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface SchemaFactoryInterface
{
    /**
     * Builds the JSON Schema document corresponding to the given PHP class.
     */
    public function buildSchema(string $className, string $format = 'json', string $type = \ApiPlatform\JsonSchema\Schema::TYPE_OUTPUT, ?string $operationType = null, ?string $operationName = null, ?\ApiPlatform\JsonSchema\Schema $schema = null, ?array $serializerContext = null, bool $forceCollection = false) : \ApiPlatform\JsonSchema\Schema;
}
