<?php

namespace ApiPlatform\Core\JsonSchema;

/**
 * {@inheritdoc}
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SchemaFactory implements \ApiPlatform\Core\JsonSchema\SchemaFactoryInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    /**
     * @param TypeFactoryInterface $typeFactory
     * @param mixed                $resourceMetadataFactory
     * @param mixed                $propertyNameCollectionFactory
     * @param mixed                $propertyMetadataFactory
     */
    public function __construct($typeFactory, $resourceMetadataFactory, $propertyNameCollectionFactory, $propertyMetadataFactory, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * When added to the list, the given format will lead to the creation of a new definition.
     *
     * @internal
     */
    public function addDistinctFormat(string $format) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildSchema(string $className, string $format = 'json', string $type = \ApiPlatform\Core\JsonSchema\Schema::TYPE_OUTPUT, ?string $operationType = null, ?string $operationName = null, ?\ApiPlatform\Core\JsonSchema\Schema $schema = null, ?array $serializerContext = null, bool $forceCollection = false) : \ApiPlatform\Core\JsonSchema\Schema
    {
    }
}
