<?php

namespace ApiPlatform\Core\Annotation;

/**
 * Hydrates attributes from annotation's parameters.
 *
 * @internal
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait AttributesHydratorTrait
{
    private static $configMetadata;
    /**
     * @internal
     */
    public static function getConfigMetadata() : array
    {
    }
    /**
     * @var array
     */
    public $attributes = null;
    /**
     * @throws InvalidArgumentException
     */
    private function hydrateAttributes(array $values) : void
    {
    }
}
