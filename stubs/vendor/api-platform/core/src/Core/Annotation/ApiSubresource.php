<?php

namespace ApiPlatform\Core\Annotation;

/**
 * ApiSubresource annotation.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @Annotation
 *
 * @Target({"METHOD", "PROPERTY"})
 *
 * @Attributes(
 *
 *     @Attribute("maxDepth", type="int"),
 * )
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD)]
final class ApiSubresource
{
    /**
     * @var int
     */
    public $maxDepth;
    /**
     * @param int $maxDepth
     */
    public function __construct($maxDepth = null)
    {
    }
}
