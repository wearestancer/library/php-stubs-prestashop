<?php

namespace ApiPlatform\Core\PathResolver;

/**
 * Generates a path with words separated by underscores.
 *
 * @author Paul Le Corre <paul@lecorre.me>
 *
 * @deprecated since version 2.1, to be removed in 3.0. Use {@see \ApiPlatform\Core\Operation\UnderscorePathSegmentNameGenerator} instead.
 */
final class UnderscoreOperationPathResolver implements \ApiPlatform\PathResolver\OperationPathResolverInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveOperationPath(string $resourceShortName, array $operation, $operationType) : string
    {
    }
}
