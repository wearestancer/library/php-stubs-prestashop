<?php

namespace ApiPlatform\Core\Operation\Factory;

/**
 * @internal
 */
final class SubresourceOperationFactory implements \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface
{
    public const SUBRESOURCE_SUFFIX = '_subresource';
    public const FORMAT_SUFFIX = '.{_format}';
    public const ROUTE_OPTIONS = ['defaults' => [], 'requirements' => [], 'options' => [], 'host' => '', 'schemes' => [], 'condition' => '', 'controller' => null, 'stateless' => null];
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Operation\PathSegmentNameGeneratorInterface $pathSegmentNameGenerator, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : array
    {
    }
}
