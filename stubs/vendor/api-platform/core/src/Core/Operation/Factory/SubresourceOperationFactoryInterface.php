<?php

namespace ApiPlatform\Core\Operation\Factory;

/**
 * Computes subresource operation for a given resource.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface SubresourceOperationFactoryInterface
{
    /**
     * Creates subresource operations.
     */
    public function create(string $resourceClass) : array;
}
