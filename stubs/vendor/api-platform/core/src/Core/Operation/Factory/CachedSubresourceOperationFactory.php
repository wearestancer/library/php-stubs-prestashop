<?php

namespace ApiPlatform\Core\Operation\Factory;

/**
 * @internal
 */
final class CachedSubresourceOperationFactory implements \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface
{
    use \ApiPlatform\Util\CachedTrait;
    public const CACHE_KEY_PREFIX = 'subresource_operations_';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : array
    {
    }
}
