<?php

namespace ApiPlatform\Core\Validator\Exception;

class ValidationException extends \ApiPlatform\Validator\Exception\ValidationException
{
}
