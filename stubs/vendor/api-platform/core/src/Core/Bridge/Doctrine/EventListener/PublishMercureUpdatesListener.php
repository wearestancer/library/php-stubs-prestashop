<?php

namespace ApiPlatform\Core\Bridge\Doctrine\EventListener;

/**
 * Publishes resources updates to the Mercure hub.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PublishMercureUpdatesListener
{
    use \ApiPlatform\Core\Bridge\Symfony\Messenger\DispatchTrait;
    use \ApiPlatform\Core\Util\ResourceClassInfoTrait;
    /**
     * @param array<string, string[]|string> $formats
     * @param HubRegistry|callable           $hubRegistry
     */
    public function __construct(\ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \Symfony\Component\Serializer\SerializerInterface $serializer, array $formats, \Symfony\Component\Messenger\MessageBusInterface $messageBus = null, $hubRegistry = null, ?\ApiPlatform\Core\GraphQl\Subscription\SubscriptionManagerInterface $graphQlSubscriptionManager = null, ?\ApiPlatform\Core\GraphQl\Subscription\MercureSubscriptionIriGeneratorInterface $graphQlMercureSubscriptionIriGenerator = null, \Symfony\Component\ExpressionLanguage\ExpressionLanguage $expressionLanguage = null)
    {
    }
    /**
     * Collects created, updated and deleted objects.
     */
    public function onFlush(\Doctrine\Common\EventArgs $eventArgs) : void
    {
    }
    /**
     * Publishes updates for changes collected on flush, and resets the store.
     */
    public function postFlush() : void
    {
    }
}
