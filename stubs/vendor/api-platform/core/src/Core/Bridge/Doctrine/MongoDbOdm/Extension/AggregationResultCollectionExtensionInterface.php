<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension;

/**
 * Interface of Doctrine MongoDB ODM aggregation extensions that supports result production
 * for specific cases such as pagination.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface AggregationResultCollectionExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension\AggregationCollectionExtensionInterface
{
    public function supportsResult(string $resourceClass, string $operationName = null, array $context = []) : bool;
    public function getResult(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array $context = []);
}
