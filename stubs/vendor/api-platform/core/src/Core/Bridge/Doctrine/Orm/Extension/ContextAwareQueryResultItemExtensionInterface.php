<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Extension;

/**
 * Context aware extension.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareQueryResultItemExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultItemExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function supportsResult(string $resourceClass, string $operationName = null, array $context = []) : bool;
    /**
     * {@inheritdoc}
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder, string $resourceClass = null, string $operationName = null, array $context = []);
}
