<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm;

/**
 * Helper trait regarding a property in an entity using the resource metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 */
trait PropertyHelperTrait
{
    /**
     * Splits the given property into parts.
     */
    protected abstract function splitPropertyParts(string $property) : array;
    /**
     * Adds the necessary joins for a nested property.
     *
     * @throws InvalidArgumentException If property is not nested
     *
     * @return array An array where the first element is the join $alias of the leaf entity,
     *               the second element is the $field name
     *               the third element is the $associations array
     */
    protected function addJoinsForNestedProperty(string $property, string $rootAlias, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator) : array
    {
    }
}
