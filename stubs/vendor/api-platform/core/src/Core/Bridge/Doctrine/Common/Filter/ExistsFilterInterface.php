<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common\Filter;

/**
 * Interface for filtering the collection by whether a property value exists or not.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface ExistsFilterInterface
{
    public const QUERY_PARAMETER_KEY = 'exists';
}
