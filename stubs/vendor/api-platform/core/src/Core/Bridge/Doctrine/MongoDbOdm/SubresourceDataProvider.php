<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm;

/**
 * Subresource data provider for the Doctrine MongoDB ODM.
 *
 * @experimental
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SubresourceDataProvider implements \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Util\IdentifierManagerTrait;
    /**
     * @param LegacyAggregationCollectionExtensionInterface[]|AggregationCollectionExtensionInterface[] $collectionExtensions
     * @param LegacyAggregationItemExtensionInterface[]|AggregationItemExtensionInterface[]             $itemExtensions
     * @param ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface               $resourceMetadataFactory
     */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, $resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, iterable $collectionExtensions = [], iterable $itemExtensions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
    }
}
