<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm;

/**
 * Helper trait regarding a property in a MongoDB document using the resource metadata.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait PropertyHelperTrait
{
    /**
     * Splits the given property into parts.
     */
    protected abstract function splitPropertyParts(string $property) : array;
    /**
     * Gets class metadata for the given resource.
     */
    protected abstract function getClassMetadata(string $resourceClass) : \Doctrine\Persistence\Mapping\ClassMetadata;
    /**
     * Adds the necessary lookups for a nested property.
     *
     * @throws InvalidArgumentException If property is not nested
     * @throws MappingException
     *
     * @return array An array where the first element is the $alias of the lookup,
     *               the second element is the $field name
     *               the third element is the $associations array
     */
    protected function addLookupsForNestedProperty(string $property, \Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass) : array
    {
    }
}
