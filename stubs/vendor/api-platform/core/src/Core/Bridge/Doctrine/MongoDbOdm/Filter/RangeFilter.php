<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter;

/**
 * Filters the collection by range.
 *
 * @experimental
 *
 * @author Lee Siong Chan <ahlee2326@me.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class RangeFilter extends \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\AbstractFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\RangeFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\RangeFilterTrait;
}
