<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * {@inheritdoc}
 *
 * Abstract class with helpers for easing the implementation of a filter.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 */
abstract class AbstractFilter implements \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\FilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Orm\PropertyHelperTrait;
    use \ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
    protected $managerRegistry;
    protected $requestStack;
    protected $logger;
    protected $properties;
    protected $nameConverter;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, ?\Symfony\Component\HttpFoundation\RequestStack $requestStack = null, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * Passes a property through the filter.
     *
     * @param mixed $value
     */
    protected abstract function filterProperty(string $property, $value, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null);
    protected function getManagerRegistry() : \Doctrine\Persistence\ManagerRegistry
    {
    }
    protected function getProperties() : ?array
    {
    }
    protected function getLogger() : \Psr\Log\LoggerInterface
    {
    }
    /**
     * Determines whether the given property is enabled.
     */
    protected function isPropertyEnabled(string $property) : bool
    {
    }
    /**
     * Extracts properties to filter from the request.
     */
    protected function extractProperties(\Symfony\Component\HttpFoundation\Request $request) : array
    {
    }
    protected function denormalizePropertyName($property)
    {
    }
    protected function normalizePropertyName($property)
    {
    }
}
