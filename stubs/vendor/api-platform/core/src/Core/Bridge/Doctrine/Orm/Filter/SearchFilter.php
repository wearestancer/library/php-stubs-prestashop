<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Filter the collection by given properties.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class SearchFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterTrait;
    public const DOCTRINE_INTEGER_TYPE = \Doctrine\DBAL\Types\Types::INTEGER;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, ?\Symfony\Component\HttpFoundation\RequestStack $requestStack, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    protected function getIriConverter() : \ApiPlatform\Core\Api\IriConverterInterface
    {
    }
    protected function getPropertyAccessor() : \Symfony\Component\PropertyAccess\PropertyAccessorInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * Adds where clause according to the strategy.
     *
     * @param mixed $values
     *
     * @throws InvalidArgumentException If strategy does not exist
     */
    protected function addWhereByStrategy(string $strategy, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, $values, bool $caseSensitive)
    {
    }
    /**
     * Creates a function that will wrap a Doctrine expression according to the
     * specified case sensitivity.
     *
     * For example, "o.name" will get wrapped into "LOWER(o.name)" when $caseSensitive
     * is false.
     */
    protected function createWrapCase(bool $caseSensitive) : \Closure
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getType(string $doctrineType) : string
    {
    }
}
