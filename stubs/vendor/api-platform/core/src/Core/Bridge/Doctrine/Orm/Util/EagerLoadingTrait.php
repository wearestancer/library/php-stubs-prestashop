<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Util;

/**
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @internal
 */
trait EagerLoadingTrait
{
    private $forceEager;
    private $fetchPartial;
    private $resourceMetadataFactory;
    /**
     * Checks if an operation has a `force_eager` attribute.
     */
    private function shouldOperationForceEager(string $resourceClass, array $options) : bool
    {
    }
    /**
     * Checks if an operation has a `fetch_partial` attribute.
     */
    private function shouldOperationFetchPartial(string $resourceClass, array $options) : bool
    {
    }
    /**
     * Get the boolean attribute of an operation or the resource metadata.
     */
    private function getBooleanOperationAttribute(string $resourceClass, array $options, string $attributeName, bool $default) : bool
    {
    }
    /**
     * Checks if the class has an associationMapping with FETCH=EAGER.
     *
     * @param array $checked array cache of tested metadata classes
     */
    private function hasFetchEagerAssociation(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadataInfo $classMetadata, array &$checked = []) : bool
    {
    }
}
