<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common;

/**
 * Data persister for Doctrine.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 *
 * @deprecated
 */
final class DataPersister implements \ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
    }
}
