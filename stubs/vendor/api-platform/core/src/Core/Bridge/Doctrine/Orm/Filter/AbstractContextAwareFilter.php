<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

abstract class AbstractContextAwareFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter implements \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ContextAwareFilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
    }
}
