<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm;

/**
 * Collection data provider for the Doctrine MongoDB ODM.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class CollectionDataProvider implements \ApiPlatform\Core\DataProvider\CollectionDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    /**
     * @param LegacyAggregationCollectionExtensionInterface[]|AggregationCollectionExtensionInterface[] $collectionExtensions
     * @param ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface               $resourceMetadataFactory
     */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, $resourceMetadataFactory, iterable $collectionExtensions = [])
    {
    }
    public function supports(string $resourceClass, string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []) : iterable
    {
    }
}
