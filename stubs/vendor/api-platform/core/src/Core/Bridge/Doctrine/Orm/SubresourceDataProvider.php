<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm;

/**
 * Subresource data provider for the Doctrine ORM.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class SubresourceDataProvider implements \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Util\IdentifierManagerTrait;
    /**
     * @param LegacyQueryCollectionExtensionInterface[]|QueryCollectionExtensionInterface[] $collectionExtensions
     * @param LegacyQueryItemExtensionInterface[]|QueryItemExtensionInterface[]             $itemExtensions
     */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, iterable $collectionExtensions = [], iterable $itemExtensions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
    }
}
