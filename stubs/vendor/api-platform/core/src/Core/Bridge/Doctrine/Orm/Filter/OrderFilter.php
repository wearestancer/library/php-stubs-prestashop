<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Order the collection by given properties.
 *
 * The ordering is done in the same sequence as they are specified in the query,
 * and for each property a direction value can be specified.
 *
 * For each property passed, if the resource does not have such property or if the
 * direction value is different from "asc" or "desc" (case insensitive), the property
 * is ignored.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 */
class OrderFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\OrderFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\OrderFilterTrait;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, ?\Symfony\Component\HttpFoundation\RequestStack $requestStack = null, string $orderParameterName = 'order', \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $direction, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractProperties(\Symfony\Component\HttpFoundation\Request $request) : array
    {
    }
}
