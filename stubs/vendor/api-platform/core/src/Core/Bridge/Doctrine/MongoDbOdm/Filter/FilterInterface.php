<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter;

/**
 * Doctrine MongoDB ODM filter interface.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface FilterInterface extends \ApiPlatform\Core\Api\FilterInterface
{
    /**
     * Applies the filter.
     */
    public function apply(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array &$context = []);
}
