<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Doctrine ORM filter interface.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface FilterInterface extends \ApiPlatform\Core\Api\FilterInterface
{
    /**
     * Applies the filter.
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null);
}
