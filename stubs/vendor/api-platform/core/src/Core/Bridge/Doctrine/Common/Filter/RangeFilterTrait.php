<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common\Filter;

/**
 * Trait for filtering the collection by range.
 *
 * @author Lee Siong Chan <ahlee2326@me.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait RangeFilterTrait
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    protected abstract function getProperties() : ?array;
    protected abstract function getLogger() : \Psr\Log\LoggerInterface;
    protected abstract function normalizePropertyName($property);
    /**
     * Gets filter description.
     */
    protected function getFilterDescription(string $fieldName, string $operator) : array
    {
    }
    private function normalizeValues(array $values, string $property) : ?array
    {
    }
    /**
     * Normalize the values array for between operator.
     */
    private function normalizeBetweenValues(array $values) : ?array
    {
    }
    /**
     * Normalize the value.
     *
     * @return int|float|null
     */
    private function normalizeValue(string $value, string $operator)
    {
    }
}
