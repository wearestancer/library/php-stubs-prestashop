<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension;

/**
 * Applies selected ordering while querying resource collection.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class OrderExtension implements \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension\AggregationCollectionExtensionInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\PropertyHelperTrait;
    use \ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
    public function __construct(string $order = null, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory = null, \Doctrine\Persistence\ManagerRegistry $managerRegistry = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array &$context = [])
    {
    }
}
