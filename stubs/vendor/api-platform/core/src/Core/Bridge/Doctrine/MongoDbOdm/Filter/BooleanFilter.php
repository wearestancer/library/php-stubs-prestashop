<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter;

/**
 * Filters the collection by boolean values.
 *
 * Filters collection on equality of boolean properties. The value is specified
 * as one of ( "true" | "false" | "1" | "0" ) in the query.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not one of ( "true" | "false" | "1" | "0" ) the property is ignored.
 *
 * @experimental
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class BooleanFilter extends \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\AbstractFilter
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\BooleanFilterTrait;
    public const DOCTRINE_BOOLEAN_TYPES = [\Doctrine\ODM\MongoDB\Types\Type::BOOL => true, \Doctrine\ODM\MongoDB\Types\Type::BOOLEAN => true];
}
