<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension;

/**
 * Interface of Doctrine MongoDB ODM aggregation extensions for item aggregations.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface AggregationItemExtensionInterface
{
    public function applyToItem(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, array $identifiers, string $operationName = null, array &$context = []);
}
