<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common\Util;

/**
 * @internal
 */
trait IdentifierManagerTrait
{
    private $propertyNameCollectionFactory;
    private $propertyMetadataFactory;
    private $resourceMetadataFactory;
    /**
     * Transform and check the identifier, composite or not.
     *
     * @param int|string $id
     *
     * @throws PropertyNotFoundException
     * @throws InvalidIdentifierException
     */
    private function normalizeIdentifiers($id, \Doctrine\Persistence\ObjectManager $manager, string $resourceClass) : array
    {
    }
}
