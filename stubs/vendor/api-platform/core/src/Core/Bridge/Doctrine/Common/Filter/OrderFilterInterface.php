<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common\Filter;

/**
 * Interface for ordering the collection by given properties.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface OrderFilterInterface
{
    public const DIRECTION_ASC = 'ASC';
    public const DIRECTION_DESC = 'DESC';
    public const NULLS_SMALLEST = 'nulls_smallest';
    public const NULLS_LARGEST = 'nulls_largest';
    public const NULLS_DIRECTION_MAP = [self::NULLS_SMALLEST => ['ASC' => 'ASC', 'DESC' => 'DESC'], self::NULLS_LARGEST => ['ASC' => 'DESC', 'DESC' => 'ASC']];
}
