<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Filters the collection by whether a property value exists or not.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not one of ( "true" | "false" | "1" | "0" ) the property is ignored.
 *
 * A query parameter with key but no value is treated as `true`, e.g.:
 * Request: GET /products?exists[brand]
 * Interpretation: filter products which have a brand
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
class ExistsFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\ExistsFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\ExistsFilterTrait;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, ?\Symfony\Component\HttpFoundation\RequestStack $requestStack = null, \Psr\Log\LoggerInterface $logger = null, array $properties = null, string $existsParameterName = self::QUERY_PARAMETER_KEY, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function isNullableField(string $property, string $resourceClass) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractProperties(\Symfony\Component\HttpFoundation\Request $request) : array
    {
    }
}
