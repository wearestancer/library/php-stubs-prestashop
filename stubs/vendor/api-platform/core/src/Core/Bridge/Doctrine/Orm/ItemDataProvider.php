<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm;

/**
 * Item data provider for the Doctrine ORM.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 *
 * @final
 */
class ItemDataProvider implements \ApiPlatform\Core\DataProvider\DenormalizedIdentifiersAwareItemDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Util\IdentifierManagerTrait;
    /**
     * @param LegacyQueryItemExtensionInterface[]|QueryItemExtensionInterface[] $itemExtensions
     * @param ResourceMetadataCollectionFactoryInterface|null                   $resourceMetadataFactory
     */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, iterable $itemExtensions = [], $resourceMetadataFactory = null)
    {
    }
    public function supports(string $resourceClass, string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * The context may contain a `fetch_data` key representing whether the value should be fetched by Doctrine or if we should return a reference.
     *
     * @throws RuntimeException
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
    }
}
