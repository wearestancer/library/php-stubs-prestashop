<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Extension;

/**
 * Context aware extension.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareQueryCollectionExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface
{
    /**
     * {@inheritdoc}
     *
     * @param LegacyQueryNameGeneratorInterface|QueryNameGeneratorInterface $queryNameGenerator
     */
    public function applyToCollection(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = []);
}
