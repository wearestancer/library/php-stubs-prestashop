<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Context aware filter.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareFilterInterface extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\FilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = []);
}
