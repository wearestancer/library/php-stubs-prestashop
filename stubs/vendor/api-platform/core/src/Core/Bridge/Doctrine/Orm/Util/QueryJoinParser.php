<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Util;

/**
 * Utility functions for working with Doctrine ORM query.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 *
 * @internal
 *
 * @deprecated
 */
final class QueryJoinParser
{
    /**
     * Gets the class metadata from a given join alias.
     *
     * @deprecated
     */
    public static function getClassMetadataFromJoinAlias(string $alias, \Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : \Doctrine\Persistence\Mapping\ClassMetadata
    {
    }
    /**
     * Gets the relationship from a Join expression.
     *
     * @deprecated
     */
    public static function getJoinRelationship(\Doctrine\ORM\Query\Expr\Join $join) : string
    {
    }
    /**
     * Gets the alias from a Join expression.
     *
     * @deprecated
     */
    public static function getJoinAlias(\Doctrine\ORM\Query\Expr\Join $join) : string
    {
    }
    /**
     * Gets the parts from an OrderBy expression.
     *
     * @return string[]
     *
     * @deprecated
     */
    public static function getOrderByParts(\Doctrine\ORM\Query\Expr\OrderBy $orderBy) : array
    {
    }
}
