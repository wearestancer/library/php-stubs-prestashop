<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common\Filter;

/**
 * Trait for filtering the collection by boolean values.
 *
 * Filters collection on equality of boolean properties. The value is specified
 * as one of ( "true" | "false" | "1" | "0" ) in the query.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not one of ( "true" | "false" | "1" | "0" ) the property is ignored.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait BooleanFilterTrait
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    protected abstract function getProperties() : ?array;
    protected abstract function getLogger() : \Psr\Log\LoggerInterface;
    protected abstract function normalizePropertyName($property);
    /**
     * Determines whether the given property refers to a boolean field.
     */
    protected function isBooleanField(string $property, string $resourceClass) : bool
    {
    }
    private function normalizeValue($value, string $property) : ?bool
    {
    }
}
