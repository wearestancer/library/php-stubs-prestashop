<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Extension;

/**
 * Context aware extension.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareQueryResultCollectionExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function supportsResult(string $resourceClass, string $operationName = null, array $context = []) : bool;
    /**
     * {@inheritdoc}
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder, string $resourceClass = null, string $operationName = null, array $context = []);
}
