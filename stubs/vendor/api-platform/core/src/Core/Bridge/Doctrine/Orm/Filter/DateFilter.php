<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Filters the collection by date intervals.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 */
class DateFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\DateFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\DateFilterTrait;
    public const DOCTRINE_DATE_TYPES = [\Doctrine\DBAL\Types\Types::DATE_MUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIMETZ_MUTABLE => true, \Doctrine\DBAL\Types\Types::TIME_MUTABLE => true, \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE => true, \Doctrine\DBAL\Types\Types::DATETIMETZ_IMMUTABLE => true, \Doctrine\DBAL\Types\Types::TIME_IMMUTABLE => true];
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $values, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * Adds the where clause according to the chosen null management.
     *
     * @param string|DBALType $type
     */
    protected function addWhere(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, string $operator, string $value, string $nullManagement = null, $type = null)
    {
    }
}
