<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Filters the collection by range.
 *
 * @author Lee Siong Chan <ahlee2326@me.com>
 */
class RangeFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\RangeFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\RangeFilterTrait;
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $values, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * Adds the where clause according to the operator.
     *
     * @param string $alias
     * @param string $field
     * @param string $operator
     * @param string $value
     */
    protected function addWhere(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, $alias, $field, $operator, $value)
    {
    }
}
