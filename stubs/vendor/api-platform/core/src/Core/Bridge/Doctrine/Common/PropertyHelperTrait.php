<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common;

/**
 * Helper trait for getting information regarding a property using the resource metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait PropertyHelperTrait
{
    protected abstract function getManagerRegistry() : \Doctrine\Persistence\ManagerRegistry;
    /**
     * Determines whether the given property is mapped.
     */
    protected function isPropertyMapped(string $property, string $resourceClass, bool $allowAssociation = false) : bool
    {
    }
    /**
     * Determines whether the given property is nested.
     */
    protected function isPropertyNested(string $property) : bool
    {
    }
    /**
     * Determines whether the given property is embedded.
     */
    protected function isPropertyEmbedded(string $property, string $resourceClass) : bool
    {
    }
    /**
     * Splits the given property into parts.
     *
     * Returns an array with the following keys:
     *   - associations: array of associations according to nesting order
     *   - field: string holding the actual field (leaf node)
     */
    protected function splitPropertyParts(string $property) : array
    {
    }
    /**
     * Gets the Doctrine Type of a given property/resourceClass.
     *
     * @return string|null
     */
    protected function getDoctrineFieldType(string $property, string $resourceClass)
    {
    }
    /**
     * Gets nested class metadata for the given resource.
     *
     * @param string[] $associations
     */
    protected function getNestedMetadata(string $resourceClass, array $associations) : \Doctrine\Persistence\Mapping\ClassMetadata
    {
    }
    /**
     * Gets class metadata for the given resource.
     */
    protected function getClassMetadata(string $resourceClass) : \Doctrine\Persistence\Mapping\ClassMetadata
    {
    }
}
