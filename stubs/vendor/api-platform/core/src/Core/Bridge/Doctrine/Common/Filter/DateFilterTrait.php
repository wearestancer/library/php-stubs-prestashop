<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Common\Filter;

/**
 * Trait for filtering the collection by date intervals.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait DateFilterTrait
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    protected abstract function getProperties() : ?array;
    protected abstract function normalizePropertyName($property);
    /**
     * Determines whether the given property refers to a date field.
     */
    protected function isDateField(string $property, string $resourceClass) : bool
    {
    }
    /**
     * Gets filter description.
     */
    protected function getFilterDescription(string $property, string $period) : array
    {
    }
}
