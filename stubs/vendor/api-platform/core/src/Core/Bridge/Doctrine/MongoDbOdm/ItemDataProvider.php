<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm;

/**
 * Item data provider for the Doctrine MongoDB ODM.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ItemDataProvider implements \ApiPlatform\Core\DataProvider\DenormalizedIdentifiersAwareItemDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Util\IdentifierManagerTrait;
    /**
     * @param LegacyAggregationItemExtensionInterface[]|AggregationItemExtensionInterface[] $itemExtensions
     * @param ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface   $resourceMetadataFactory
     */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, $resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, iterable $itemExtensions = [])
    {
    }
    public function supports(string $resourceClass, string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
    }
}
