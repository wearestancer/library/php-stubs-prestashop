<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Filter;

/**
 * Filters the collection by numeric values.
 *
 * Filters collection by equality of numeric properties.
 *
 * For each property passed, if the resource does not have such property or if
 * the value is not numeric, the property is ignored.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
class NumericFilter extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\NumericFilterTrait;
    /**
     * Type of numeric in Doctrine.
     *
     * @see http://doctrine-orm.readthedocs.org/projects/doctrine-dbal/en/latest/reference/types.html
     */
    public const DOCTRINE_NUMERIC_TYPES = [\Doctrine\DBAL\Types\Types::BIGINT => true, \Doctrine\DBAL\Types\Types::DECIMAL => true, \Doctrine\DBAL\Types\Types::FLOAT => true, \Doctrine\DBAL\Types\Types::INTEGER => true, \Doctrine\DBAL\Types\Types::SMALLINT => true];
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, \Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getType(string $doctrineType = null) : string
    {
    }
}
