<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Extension;

/**
 * Interface of Doctrine ORM query extensions that supports result production
 * for specific cases such as pagination.
 *
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface QueryResultCollectionExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface
{
    public function supportsResult(string $resourceClass, string $operationName = null) : bool;
    /**
     * @return iterable
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder);
}
