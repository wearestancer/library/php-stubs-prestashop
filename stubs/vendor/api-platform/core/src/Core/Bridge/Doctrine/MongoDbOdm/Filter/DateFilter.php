<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter;

/**
 * Filters the collection by date intervals.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
class DateFilter extends \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\AbstractFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\DateFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\DateFilterTrait;
    public const DOCTRINE_DATE_TYPES = [\Doctrine\ODM\MongoDB\Types\Type::DATE => true, \Doctrine\ODM\MongoDB\Types\Type::DATE_IMMUTABLE => true];
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $values, \Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array &$context = [])
    {
    }
}
