<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Extension;

/**
 * Applies pagination on the Doctrine query for resource collection when enabled.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class PaginationExtension implements \ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\ContextAwareQueryResultCollectionExtensionInterface
{
    /**
     * @param ResourceMetadataFactoryInterface|RequestStack $resourceMetadataFactory
     * @param Pagination|ResourceMetadataFactoryInterface   $pagination
     */
    public function __construct(
        \Doctrine\Persistence\ManagerRegistry $managerRegistry,
        /* ResourceMetadataFactoryInterface */
        $resourceMetadataFactory,
        /* Pagination */
        $pagination
    )
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(\Doctrine\ORM\QueryBuilder $queryBuilder, \ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsResult(string $resourceClass, string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder, string $resourceClass = null, string $operationName = null, array $context = []) : iterable
    {
    }
}
