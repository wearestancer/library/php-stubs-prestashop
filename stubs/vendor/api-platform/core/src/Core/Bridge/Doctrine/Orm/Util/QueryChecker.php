<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Util;

/**
 * Utility functions for working with Doctrine ORM query.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 *
 * @internal
 */
final class QueryChecker
{
    /**
     * Determines whether the QueryBuilder uses a HAVING clause.
     */
    public static function hasHavingClause(\Doctrine\ORM\QueryBuilder $queryBuilder) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder has any root entity with foreign key identifier.
     */
    public static function hasRootEntityWithForeignKeyIdentifier(\Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder has any composite identifier.
     */
    public static function hasRootEntityWithCompositeIdentifier(\Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder has a limit on the maximum number of results.
     */
    public static function hasMaxResults(\Doctrine\ORM\QueryBuilder $queryBuilder) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder has ORDER BY on a column from a fetch joined to-many association.
     */
    public static function hasOrderByOnFetchJoinedToManyAssociation(\Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder has ORDER BY on a column from a fetch joined to-many association.
     *
     * @deprecated
     */
    public static function hasOrderByOnToManyJoin(\Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder already has a left join.
     */
    public static function hasLeftJoin(\Doctrine\ORM\QueryBuilder $queryBuilder) : bool
    {
    }
    /**
     * Determines whether the QueryBuilder has a joined to-many association.
     */
    public static function hasJoinedToManyAssociation(\Doctrine\ORM\QueryBuilder $queryBuilder, \Doctrine\Persistence\ManagerRegistry $managerRegistry) : bool
    {
    }
}
