<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter;

/**
 * Filter the collection by given properties.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SearchFilter extends \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\AbstractFilter implements \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterTrait;
    public const DOCTRINE_INTEGER_TYPE = [\Doctrine\ODM\MongoDB\Types\Type::INTEGER, \Doctrine\ODM\MongoDB\Types\Type::INT];
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
}
