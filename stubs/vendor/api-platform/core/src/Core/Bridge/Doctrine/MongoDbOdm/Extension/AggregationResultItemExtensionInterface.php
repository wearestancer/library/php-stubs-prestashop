<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension;

/**
 * Interface of Doctrine MongoDB ODM aggregation extensions that supports result production
 * for specific cases such as Aggregation alteration.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface AggregationResultItemExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Extension\AggregationItemExtensionInterface
{
    public function supportsResult(string $resourceClass, string $operationName = null, array $context = []) : bool;
    public function getResult(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array $context = []);
}
