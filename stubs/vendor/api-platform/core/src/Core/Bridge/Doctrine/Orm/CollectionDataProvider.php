<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm;

/**
 * Collection data provider for the Doctrine ORM.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 *
 * @final
 */
class CollectionDataProvider implements \ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    /**
     * @param LegacyQueryCollectionExtensionInterface[]|QueryCollectionExtensionInterface[] $collectionExtensions
     */
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, iterable $collectionExtensions = [])
    {
    }
    public function supports(string $resourceClass, string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws RuntimeException
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []) : iterable
    {
    }
}
