<?php

namespace ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter;

/**
 * {@inheritdoc}
 *
 * Abstract class for easing the implementation of a filter.
 *
 * @experimental
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
abstract class AbstractFilter implements \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\FilterInterface
{
    use \ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\PropertyHelperTrait;
    use \ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
    protected $managerRegistry;
    protected $logger;
    protected $properties;
    protected $nameConverter;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, \Psr\Log\LoggerInterface $logger = null, array $properties = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array &$context = [])
    {
    }
    /**
     * Passes a property through the filter.
     *
     * @param mixed $value
     */
    protected abstract function filterProperty(string $property, $value, \Doctrine\ODM\MongoDB\Aggregation\Builder $aggregationBuilder, string $resourceClass, string $operationName = null, array &$context = []);
    protected function getManagerRegistry() : \Doctrine\Persistence\ManagerRegistry
    {
    }
    protected function getProperties() : ?array
    {
    }
    protected function getLogger() : \Psr\Log\LoggerInterface
    {
    }
    /**
     * Determines whether the given property is enabled.
     */
    protected function isPropertyEnabled(string $property, string $resourceClass) : bool
    {
    }
    protected function denormalizePropertyName($property)
    {
    }
    protected function normalizePropertyName($property)
    {
    }
}
