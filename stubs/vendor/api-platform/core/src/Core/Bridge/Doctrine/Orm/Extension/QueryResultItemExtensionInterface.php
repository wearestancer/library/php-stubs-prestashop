<?php

namespace ApiPlatform\Core\Bridge\Doctrine\Orm\Extension;

/**
 * Interface of Doctrine ORM query extensions that supports result production
 * for specific cases such as Query alteration.
 *
 * @author Antoine BLUCHET <soyuka@gmail.com>
 */
interface QueryResultItemExtensionInterface extends \ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface
{
    public function supportsResult(string $resourceClass, string $operationName = null) : bool;
    /**
     * @return object|null
     */
    public function getResult(\Doctrine\ORM\QueryBuilder $queryBuilder);
}
