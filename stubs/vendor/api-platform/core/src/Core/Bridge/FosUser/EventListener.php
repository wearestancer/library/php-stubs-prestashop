<?php

namespace ApiPlatform\Core\Bridge\FosUser;

/**
 * Bridges between FOSUserBundle and API Platform Core.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Théo FIDRY <theo.fidry@gmail.com>
 */
final class EventListener
{
    public function __construct(\FOS\UserBundle\Model\UserManagerInterface $userManager)
    {
    }
    /**
     * Persists, updates or delete data return by the controller if applicable.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
