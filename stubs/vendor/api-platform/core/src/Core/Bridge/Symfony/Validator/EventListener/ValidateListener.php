<?php

namespace ApiPlatform\Core\Bridge\Symfony\Validator\EventListener;

/**
 * Validates data.
 *
 * @deprecated
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ValidateListener
{
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \Psr\Container\ContainerInterface $container = null)
    {
    }
    /**
     * Validates data returned by the controller if applicable.
     *
     * @throws ValidationException
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
}
