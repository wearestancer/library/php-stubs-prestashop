<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\Command;

/**
 * Console command to dump Swagger API documentations.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
final class SwaggerCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @param int[] $swaggerVersions
     */
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollection, string $apiTitle, string $apiDescription, string $apiVersion, array $apiFormats = null, array $swaggerVersions = [2, 3], bool $legacyMode = false)
    {
    }
    public static function getDefaultName() : string
    {
    }
}
