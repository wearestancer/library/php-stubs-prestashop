<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Constraint;

/**
 * Is used for phpunit >= 9.
 *
 * @internal
 */
final class ArraySubsetV9 extends \PHPUnit\Framework\Constraint\Constraint
{
    use \ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Constraint\ArraySubsetTrait;
    /**
     * {@inheritdoc}
     */
    public function evaluate($other, string $description = '', bool $returnResult = false) : ?bool
    {
    }
}
