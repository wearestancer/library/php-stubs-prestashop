<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class IriConverter implements \ApiPlatform\Core\Api\IriConverterInterface
{
    use \ApiPlatform\Core\DataProvider\OperationDataProviderTrait;
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider, \ApiPlatform\Core\Bridge\Symfony\Routing\RouteNameResolverInterface $routeNameResolver, \Symfony\Component\Routing\RouterInterface $router, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor = null, \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface $subresourceDataProvider = null, \ApiPlatform\Core\Identifier\IdentifierConverterInterface $identifierConverter = null, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver = null, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return object
     */
    public function getItemFromIri(string $iri, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIriFromItem($item, int $referenceType = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIriFromResourceClass(string $resourceClass, int $referenceType = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemIriFromResourceClass(string $resourceClass, array $identifiers, int $referenceType = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubresourceIriFromResourceClass(string $resourceClass, array $context, int $referenceType = null) : string
    {
    }
}
