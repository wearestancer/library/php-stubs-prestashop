<?php

namespace ApiPlatform\Core\Bridge\Symfony\Messenger;

/**
 * Dispatches the given resource using the message bus of Symfony Messenger.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class DataPersister implements \ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Symfony\Messenger\DispatchTrait;
    public function __construct($resourceMetadataFactory, \Symfony\Component\Messenger\MessageBusInterface $messageBus)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
    }
}
