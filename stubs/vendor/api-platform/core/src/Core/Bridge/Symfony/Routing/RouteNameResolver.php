<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class RouteNameResolver implements \ApiPlatform\Core\Bridge\Symfony\Routing\RouteNameResolverInterface
{
    public function __construct(\Symfony\Component\Routing\RouterInterface $router)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRouteName(string $resourceClass, $operationType) : string
    {
    }
}
