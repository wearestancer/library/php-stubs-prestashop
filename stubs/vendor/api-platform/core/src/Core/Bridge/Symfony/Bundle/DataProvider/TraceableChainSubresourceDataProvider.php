<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\DataProvider;

/**
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 */
final class TraceableChainSubresourceDataProvider implements \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface
{
    public function __construct(\ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface $subresourceDataProvider)
    {
    }
    public function getProvidersResponse() : array
    {
    }
    public function getContext() : array
    {
    }
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
    }
}
