<?php

namespace ApiPlatform\Core\Bridge\Symfony\Messenger;

trait DispatchTrait
{
    use \ApiPlatform\Symfony\Messenger\DispatchTrait;
}
