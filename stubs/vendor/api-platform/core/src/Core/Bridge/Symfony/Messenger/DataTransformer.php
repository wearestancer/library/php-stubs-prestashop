<?php

namespace ApiPlatform\Core\Bridge\Symfony\Messenger;

/**
 * Transforms an Input to itself. This gives the ability to send the Input to a
 * message handler and process it asynchronously.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class DataTransformer implements \ApiPlatform\Core\DataTransformer\DataTransformerInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct($resourceMetadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return object
     */
    public function transform($object, string $to, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []) : bool
    {
    }
}
