<?php

namespace ApiPlatform\Core\Bridge\Symfony\Validator\Metadata\Property;

/**
 * Decorates a metadata loader using the validator.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ValidatorPropertyMetadataFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    /**
     * @var string[] A list of constraint classes making the entity required
     */
    public const REQUIRED_CONSTRAINTS = [\Symfony\Component\Validator\Constraints\NotBlank::class, \Symfony\Component\Validator\Constraints\NotNull::class];
    public const SCHEMA_MAPPED_CONSTRAINTS = [\Symfony\Component\Validator\Constraints\Url::class => 'http://schema.org/url', \Symfony\Component\Validator\Constraints\Email::class => 'http://schema.org/email', \Symfony\Component\Validator\Constraints\Uuid::class => 'http://schema.org/identifier', \Symfony\Component\Validator\Constraints\CardScheme::class => 'http://schema.org/identifier', \Symfony\Component\Validator\Constraints\Bic::class => 'http://schema.org/identifier', \Symfony\Component\Validator\Constraints\Iban::class => 'http://schema.org/identifier', \Symfony\Component\Validator\Constraints\Date::class => 'http://schema.org/Date', \Symfony\Component\Validator\Constraints\DateTime::class => 'http://schema.org/DateTime', \Symfony\Component\Validator\Constraints\Time::class => 'http://schema.org/Time', \Symfony\Component\Validator\Constraints\Image::class => 'http://schema.org/image', \Symfony\Component\Validator\Constraints\File::class => 'http://schema.org/MediaObject', \Symfony\Component\Validator\Constraints\Currency::class => 'http://schema.org/priceCurrency', \Symfony\Component\Validator\Constraints\Isbn::class => 'http://schema.org/isbn', \Symfony\Component\Validator\Constraints\Issn::class => 'http://schema.org/issn'];
    /**
     * @param PropertySchemaRestrictionMetadataInterface[] $restrictionsMetadata
     */
    public function __construct(\Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface $validatorMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated, iterable $restrictionsMetadata = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyMetadata
    {
    }
}
