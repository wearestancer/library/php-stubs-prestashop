<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * {@inheritdoc}
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class CachedRouteNameResolver implements \ApiPlatform\Core\Bridge\Symfony\Routing\RouteNameResolverInterface
{
    use \ApiPlatform\Util\CachedTrait;
    public const CACHE_KEY_PREFIX = 'route_name_';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Core\Bridge\Symfony\Routing\RouteNameResolverInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRouteName(string $resourceClass, $operationType) : string
    {
    }
}
