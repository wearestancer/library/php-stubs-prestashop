<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Constraint;

/**
 * Asserts that a JSON document matches a given JSON Schema.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @experimental
 */
final class MatchesJsonSchema extends \PHPUnit\Framework\Constraint\Constraint
{
    /**
     * @param object|array|string $schema
     */
    public function __construct($schema, ?int $checkMode = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
