<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * Resolves the HTTP method associated with an operation, extended for Symfony routing.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 *
 * @deprecated since API Platform 2.5, use the "method" attribute instead
 */
interface OperationMethodResolverInterface extends \ApiPlatform\Core\Api\OperationMethodResolverInterface
{
    /**
     * @throws RuntimeException
     */
    public function getCollectionOperationRoute(string $resourceClass, string $operationName) : \Symfony\Component\Routing\Route;
    /**
     * @throws RuntimeException
     */
    public function getItemOperationRoute(string $resourceClass, string $operationName) : \Symfony\Component\Routing\Route;
}
