<?php

namespace ApiPlatform\Core\Bridge\Symfony\Identifier\Normalizer;

/**
 * Denormalizes an UUID string to an instance of Symfony\Component\Uid\Uuid.
 */
final class UuidNormalizer implements \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
}
