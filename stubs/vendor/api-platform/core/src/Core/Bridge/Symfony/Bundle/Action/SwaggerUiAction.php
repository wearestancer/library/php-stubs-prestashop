<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\Action;

/**
 * Displays the documentation.
 *
 * @deprecated please refer to ApiPlatform\Symfony\Bundle\SwaggerUi\SwaggerUiAction for further changes
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SwaggerUiAction
{
    /**
     * @param int[]      $swaggerVersions
     * @param mixed|null $assetPackage
     * @param mixed      $formats
     * @param mixed      $oauthEnabled
     * @param mixed      $oauthClientId
     * @param mixed      $oauthClientSecret
     * @param mixed      $oauthType
     * @param mixed      $oauthFlow
     * @param mixed      $oauthTokenUrl
     * @param mixed      $oauthAuthorizationUrl
     * @param mixed      $oauthScopes
     * @param mixed      $resourceMetadataFactory
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, $resourceMetadataFactory, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer, ?\Twig\Environment $twig, \Symfony\Component\Routing\Generator\UrlGeneratorInterface $urlGenerator, string $title = '', string $description = '', string $version = '', $formats = [], $oauthEnabled = false, $oauthClientId = '', $oauthClientSecret = '', $oauthType = '', $oauthFlow = '', $oauthTokenUrl = '', $oauthAuthorizationUrl = '', $oauthScopes = [], bool $showWebby = true, bool $swaggerUiEnabled = false, bool $reDocEnabled = false, bool $graphqlEnabled = false, bool $graphiQlEnabled = false, bool $graphQlPlaygroundEnabled = false, array $swaggerVersions = [2, 3], \ApiPlatform\Symfony\Bundle\SwaggerUi\SwaggerUiAction $swaggerUiAction = null, $assetPackage = null, array $swaggerUiExtraConfiguration = [], bool $oauthPkce = false)
    {
    }
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
