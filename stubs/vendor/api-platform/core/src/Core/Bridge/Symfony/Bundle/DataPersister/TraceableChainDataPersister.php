<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\DataPersister;

/**
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 */
final class TraceableChainDataPersister implements \ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface
{
    public function __construct(\ApiPlatform\Core\DataPersister\DataPersisterInterface $dataPersister)
    {
    }
    public function getPersistersResponse() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
    }
}
