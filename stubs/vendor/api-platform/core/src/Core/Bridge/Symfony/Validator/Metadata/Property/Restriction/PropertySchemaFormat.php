<?php

namespace ApiPlatform\Core\Bridge\Symfony\Validator\Metadata\Property\Restriction;

/**
 * Class PropertySchemaFormat.
 *
 * @author Andrii Penchuk penja7@gmail.com
 */
class PropertySchemaFormat implements \ApiPlatform\Core\Bridge\Symfony\Validator\Metadata\Property\Restriction\PropertySchemaRestrictionMetadataInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Core\Metadata\Property\PropertyMetadata $propertyMetadata) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Core\Metadata\Property\PropertyMetadata $propertyMetadata) : bool
    {
    }
}
