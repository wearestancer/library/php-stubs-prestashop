<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * Resolves the HTTP method associated with an operation, extended for Symfony routing.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 *
 * @deprecated since API Platform 2.5, use the "method" attribute instead
 */
final class OperationMethodResolver implements \ApiPlatform\Core\Bridge\Symfony\Routing\OperationMethodResolverInterface
{
    public function __construct(\Symfony\Component\Routing\RouterInterface $router, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollectionOperationMethod(string $resourceClass, string $operationName) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemOperationMethod(string $resourceClass, string $operationName) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollectionOperationRoute(string $resourceClass, string $operationName) : \Symfony\Component\Routing\Route
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemOperationRoute(string $resourceClass, string $operationName) : \Symfony\Component\Routing\Route
    {
    }
}
