<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * Generates the Symfony route name associated with an operation name and a resource short name.
 *
 * @internal
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class RouteNameGenerator
{
    public const ROUTE_NAME_PREFIX = 'api_';
    /**
     * Generates a Symfony route name.
     *
     * @param string|bool $operationType
     *
     * @throws InvalidArgumentException
     */
    public static function generate(string $operationName, string $resourceShortName, $operationType) : string
    {
    }
    /**
     * Transforms a given string to a tableized, pluralized string.
     *
     * @param string $name usually a ResourceMetadata shortname
     *
     * @return string A string that is a part of the route name
     */
    public static function inflector(string $name, bool $pluralize = true) : string
    {
    }
}
