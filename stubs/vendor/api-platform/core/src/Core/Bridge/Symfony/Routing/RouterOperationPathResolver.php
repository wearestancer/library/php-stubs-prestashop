<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * Resolves the operations path using a Symfony route.
 * TODO: remove this in 3.0.
 *
 * @author Guilhem N. <egetick@gmail.com>
 */
final class RouterOperationPathResolver implements \ApiPlatform\PathResolver\OperationPathResolverInterface
{
    public function __construct(\Symfony\Component\Routing\RouterInterface $router, \ApiPlatform\PathResolver\OperationPathResolverInterface $deferred)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function resolveOperationPath(string $resourceShortName, array $operation, $operationType) : string
    {
    }
}
