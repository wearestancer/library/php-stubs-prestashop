<?php

namespace ApiPlatform\Core\Bridge\Symfony\Maker;

class MakeDataProvider extends \Symfony\Bundle\MakerBundle\Maker\AbstractMaker
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getCommandName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getCommandDescription() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureCommand(\Symfony\Component\Console\Command\Command $command, \Symfony\Bundle\MakerBundle\InputConfiguration $inputConfig)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureDependencies(\Symfony\Bundle\MakerBundle\DependencyBuilder $dependencies)
    {
    }
    public function interact(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Bundle\MakerBundle\ConsoleStyle $io, \Symfony\Component\Console\Command\Command $command)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function generate(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Bundle\MakerBundle\ConsoleStyle $io, \Symfony\Bundle\MakerBundle\Generator $generator)
    {
    }
}
