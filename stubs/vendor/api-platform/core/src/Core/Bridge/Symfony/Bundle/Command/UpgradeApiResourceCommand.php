<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\Command;

#[\Symfony\Component\Console\Attribute\AsCommand(name: 'api:upgrade-resource')]
final class UpgradeApiResourceCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $subresourceOperationFactory, \ApiPlatform\Core\Upgrade\SubresourceTransformer $subresourceTransformer, \ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor, \Doctrine\Common\Annotations\AnnotationReader $reader = null)
    {
    }
}
