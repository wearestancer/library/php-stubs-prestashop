<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\DataProvider;

/**
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 */
final class TraceableChainItemDataProvider implements \ApiPlatform\Core\DataProvider\ItemDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    use \ApiPlatform\Core\DataProvider\RestrictDataProviderTrait;
    public function __construct(\ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider)
    {
    }
    public function getProvidersResponse() : array
    {
    }
    public function getContext() : array
    {
    }
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
    }
}
