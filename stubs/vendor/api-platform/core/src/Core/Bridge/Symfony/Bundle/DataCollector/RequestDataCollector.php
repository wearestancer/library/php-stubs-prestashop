<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\DataCollector;

/**
 * @author Julien DENIAU <julien.deniau@gmail.com>
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 */
final class RequestDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $metadataFactory, \Psr\Container\ContainerInterface $filterLocator, \ApiPlatform\Core\DataProvider\CollectionDataProviderInterface $collectionDataProvider = null, \ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider = null, \ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface $subresourceDataProvider = null, \ApiPlatform\Core\DataPersister\DataPersisterInterface $dataPersister = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Throwable $exception = null)
    {
    }
    public function getAcceptableContentTypes() : array
    {
    }
    public function getResourceClass()
    {
    }
    public function getResourceMetadata()
    {
    }
    public function getRequestAttributes() : array
    {
    }
    public function getFilters() : array
    {
    }
    public function getCounters() : array
    {
    }
    public function getCollectionDataProviders() : array
    {
    }
    public function getItemDataProviders() : array
    {
    }
    public function getSubresourceDataProviders() : array
    {
    }
    public function getDataPersisters() : array
    {
    }
    public function getVersion() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
