<?php

namespace ApiPlatform\Core\Bridge\Symfony\Validator\Metadata\Property\Restriction;

/**
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class PropertySchemaOneOfRestriction implements \ApiPlatform\Core\Bridge\Symfony\Validator\Metadata\Property\Restriction\PropertySchemaRestrictionMetadataInterface
{
    /**
     * @param iterable<PropertySchemaRestrictionMetadataInterface> $restrictionsMetadata
     */
    public function __construct(iterable $restrictionsMetadata = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param AtLeastOneOf $constraint
     */
    public function create(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Core\Metadata\Property\PropertyMetadata $propertyMetadata) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Validator\Constraint $constraint, \ApiPlatform\Core\Metadata\Property\PropertyMetadata $propertyMetadata) : bool
    {
    }
}
