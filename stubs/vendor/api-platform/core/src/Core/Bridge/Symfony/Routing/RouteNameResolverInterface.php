<?php

namespace ApiPlatform\Core\Bridge\Symfony\Routing;

/**
 * Resolves the Symfony route name associated with a resource.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
interface RouteNameResolverInterface
{
    /**
     * Finds the route name for a resource.
     *
     * @param bool|string $operationType
     *
     * @throws InvalidArgumentException
     */
    public function getRouteName(string $resourceClass, $operationType) : string;
}
