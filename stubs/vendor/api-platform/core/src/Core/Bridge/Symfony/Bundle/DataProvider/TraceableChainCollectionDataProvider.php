<?php

namespace ApiPlatform\Core\Bridge\Symfony\Bundle\DataProvider;

/**
 * @author Anthony GRASSIOT <antograssiot@free.fr>
 */
final class TraceableChainCollectionDataProvider implements \ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    use \ApiPlatform\Core\DataProvider\RestrictDataProviderTrait;
    public function __construct(\ApiPlatform\Core\DataProvider\CollectionDataProviderInterface $collectionDataProvider)
    {
    }
    public function getProvidersResponse() : array
    {
    }
    public function getContext() : array
    {
    }
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []) : iterable
    {
    }
}
