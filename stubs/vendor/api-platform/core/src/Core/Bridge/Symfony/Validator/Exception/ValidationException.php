<?php

namespace ApiPlatform\Core\Bridge\Symfony\Validator\Exception;

final class ValidationException extends \ApiPlatform\Symfony\Validator\Exception\ValidationException
{
}
