<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter;

/**
 * Abstract class with helpers for easing the implementation of a filter.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
abstract class AbstractFilter implements \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter\FilterInterface
{
    use \ApiPlatform\Core\Bridge\Elasticsearch\Util\FieldDatatypeTrait {
        getNestedFieldPath as protected;
    }
    protected $properties;
    protected $propertyNameCollectionFactory;
    protected $nameConverter;
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, ?array $properties = null)
    {
    }
    /**
     * Gets all enabled properties for the given resource class.
     */
    protected function getProperties(string $resourceClass) : \Traversable
    {
    }
    /**
     * Is the given property enabled?
     */
    protected function hasProperty(string $resourceClass, string $property) : bool
    {
    }
    /**
     * Gets info about the decomposed given property for the given resource class.
     *
     * Returns an array with the following info as values:
     *   - the {@see Type} of the decomposed given property
     *   - is the decomposed given property an association?
     *   - the resource class of the decomposed given property
     *   - the property name of the decomposed given property
     */
    protected function getMetadata(string $resourceClass, string $property) : array
    {
    }
}
