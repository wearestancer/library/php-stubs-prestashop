<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider;

/**
 * Item data provider for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ItemDataProvider implements \ApiPlatform\Core\DataProvider\ItemDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    public function __construct(\Elasticsearch\Client $client, \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $documentMetadataFactory, \ApiPlatform\Core\Bridge\Elasticsearch\Api\IdentifierExtractorInterface $identifierExtractor, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(string $resourceClass, ?string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem(string $resourceClass, $id, ?string $operationName = null, array $context = [])
    {
    }
}
