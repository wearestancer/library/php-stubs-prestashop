<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\Api;

/**
 * Extracts identifier for a given resource.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface IdentifierExtractorInterface
{
    /**
     * Finds identifier from a resource class.
     *
     * @throws NonUniqueIdentifierException
     */
    public function getIdentifierFromResourceClass(string $resourceClass) : string;
}
