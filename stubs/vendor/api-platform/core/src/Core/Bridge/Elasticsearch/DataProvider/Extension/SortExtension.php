<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension;

/**
 * Applies selected sorting while querying resource collection.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class SortExtension implements \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension\RequestBodySearchCollectionExtensionInterface
{
    use \ApiPlatform\Core\Bridge\Elasticsearch\Util\FieldDatatypeTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Bridge\Elasticsearch\Api\IdentifierExtractorInterface $identifierExtractor, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, ?string $defaultDirection = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(array $requestBody, string $resourceClass, ?string $operationName = null, array $context = []) : array
    {
    }
}
