<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\Exception;

final class IndexNotFoundException extends \ApiPlatform\Elasticsearch\Exception\IndexNotFoundException
{
}
