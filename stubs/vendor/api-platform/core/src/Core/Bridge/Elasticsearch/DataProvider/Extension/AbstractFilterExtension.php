<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension;

/**
 * Abstract class for easing the implementation of a filter extension.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
abstract class AbstractFilterExtension implements \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension\RequestBodySearchCollectionExtensionInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \Psr\Container\ContainerInterface $filterLocator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(array $requestBody, string $resourceClass, ?string $operationName = null, array $context = []) : array
    {
    }
    /**
     * Gets the related filter interface.
     */
    protected abstract function getFilterInterface() : string;
    /**
     * Alters the request body.
     */
    protected abstract function alterRequestBody(array $requestBody, array $clauseBody) : array;
}
