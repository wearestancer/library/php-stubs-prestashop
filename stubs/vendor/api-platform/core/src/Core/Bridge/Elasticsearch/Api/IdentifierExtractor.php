<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\Api;

/**
 * {@inheritdoc}
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class IdentifierExtractor implements \ApiPlatform\Core\Bridge\Elasticsearch\Api\IdentifierExtractorInterface
{
    public function __construct(\ApiPlatform\Core\Api\IdentifiersExtractorInterface $identifiersExtractor)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIdentifierFromResourceClass(string $resourceClass) : string
    {
    }
}
