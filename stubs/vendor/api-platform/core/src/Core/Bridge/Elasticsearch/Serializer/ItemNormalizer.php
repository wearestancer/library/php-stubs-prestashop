<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\Serializer;

/**
 * Item denormalizer for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ItemNormalizer extends \Symfony\Component\Serializer\Normalizer\ObjectNormalizer
{
    public const FORMAT = 'elasticsearch';
    public function __construct(\ApiPlatform\Core\Bridge\Elasticsearch\Api\IdentifierExtractorInterface $identifierExtractor, \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface $propertyTypeExtractor = null, \Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface $classDiscriminatorResolver = null, callable $objectClassResolver = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     *
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
}
