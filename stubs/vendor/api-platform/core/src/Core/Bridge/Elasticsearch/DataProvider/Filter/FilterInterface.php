<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter;

/**
 * Elasticsearch filter interface.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface FilterInterface extends \ApiPlatform\Core\Api\FilterInterface
{
    public function apply(array $clauseBody, string $resourceClass, ?string $operationName = null, array $context = []) : array;
}
