<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter;

/**
 * Elasticsearch filter interface for a constant score query.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface ConstantScoreFilterInterface extends \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter\FilterInterface
{
}
