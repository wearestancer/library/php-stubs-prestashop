<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\Exception;

final class NonUniqueIdentifierException extends \ApiPlatform\Elasticsearch\Exception\NonUniqueIdentifierException
{
}
