<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter;

/**
 * Abstract class with helpers for easing the implementation of a search filter like a term filter or a match filter.
 *
 * @experimental
 *
 * @internal
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
abstract class AbstractSearchFilter extends \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter\AbstractFilter implements \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter\ConstantScoreFilterInterface
{
    protected $identifierExtractor;
    protected $iriConverter;
    protected $propertyAccessor;
    /**
     * {@inheritdoc}
     */
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\Core\Bridge\Elasticsearch\Api\IdentifierExtractorInterface $identifierExtractor, \ApiPlatform\Core\Api\IriConverterInterface $iriConverter, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, ?array $properties = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(array $clauseBody, string $resourceClass, ?string $operationName = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
    /**
     * Gets the Elasticsearch query corresponding to the current search filter.
     */
    protected abstract function getQuery(string $property, array $values, ?string $nestedPath) : array;
    /**
     * Converts the given {@see Type} in PHP type.
     */
    protected function getPhpType(\Symfony\Component\PropertyInfo\Type $type) : string
    {
    }
    /**
     * Is the given property of the given resource class an identifier?
     */
    protected function isIdentifier(string $resourceClass, string $property) : bool
    {
    }
    /**
     * Gets the ID from an IRI or a raw ID.
     */
    protected function getIdentifierValue(string $iri, string $property)
    {
    }
    /**
     * Are the given values valid according to the given {@see Type}?
     */
    protected function hasValidValues(array $values, \Symfony\Component\PropertyInfo\Type $type) : bool
    {
    }
}
