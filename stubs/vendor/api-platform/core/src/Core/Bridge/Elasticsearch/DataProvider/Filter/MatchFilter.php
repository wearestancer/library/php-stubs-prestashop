<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter;

/**
 * Filter the collection by given properties using a full text query.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class MatchFilter extends \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter\AbstractSearchFilter
{
}
