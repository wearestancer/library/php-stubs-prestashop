<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension;

/**
 * Interface of Elasticsearch request body search extensions for collection queries.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-body.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface RequestBodySearchCollectionExtensionInterface
{
    public function applyToCollection(array $requestBody, string $resourceClass, ?string $operationName = null, array $context = []) : array;
}
