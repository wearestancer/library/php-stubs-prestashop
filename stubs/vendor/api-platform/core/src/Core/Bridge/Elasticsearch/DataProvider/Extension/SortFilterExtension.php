<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension;

/**
 * Applies filters on the sort parameter while querying resource collection.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class SortFilterExtension extends \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Extension\AbstractFilterExtension
{
}
