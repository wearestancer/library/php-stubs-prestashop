<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter;

/**
 * Elasticsearch filter interface for sorting.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface SortFilterInterface extends \ApiPlatform\Core\Bridge\Elasticsearch\DataProvider\Filter\FilterInterface
{
}
