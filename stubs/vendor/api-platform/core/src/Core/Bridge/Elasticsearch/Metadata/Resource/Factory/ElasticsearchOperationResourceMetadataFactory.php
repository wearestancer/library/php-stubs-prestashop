<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\Metadata\Resource\Factory;

/**
 * Creates read-only operations for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ElasticsearchOperationResourceMetadataFactory implements \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
}
