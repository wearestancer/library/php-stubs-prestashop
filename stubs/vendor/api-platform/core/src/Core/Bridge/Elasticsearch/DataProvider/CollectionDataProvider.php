<?php

namespace ApiPlatform\Core\Bridge\Elasticsearch\DataProvider;

/**
 * Collection data provider for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class CollectionDataProvider implements \ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface, \ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface
{
    /**
     * @param LegacyRequestBodySearchCollectionExtensionInterface[]|RequestBodySearchCollectionExtensionInterface[] $collectionExtensions
     * @param ResourceMetadataFactoryInterface|ResourceMetadataCollectionFactoryInterface                           $resourceMetadataFactory
     * @param Pagination|LegacyPagination                                                                           $pagination
     */
    public function __construct(\Elasticsearch\Client $client, \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $documentMetadataFactory, \ApiPlatform\Core\Bridge\Elasticsearch\Api\IdentifierExtractorInterface $identifierExtractor = null, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer, $pagination, $resourceMetadataFactory, iterable $collectionExtensions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(string $resourceClass, ?string $operationName = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, ?string $operationName = null, array $context = []) : iterable
    {
    }
}
