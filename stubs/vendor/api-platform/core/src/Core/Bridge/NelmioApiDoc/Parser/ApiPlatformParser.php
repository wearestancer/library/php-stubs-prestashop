<?php

namespace ApiPlatform\Core\Bridge\NelmioApiDoc\Parser;

/**
 * Extract input and output information for the NelmioApiDocBundle.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 *
 * @deprecated since version 2.2, to be removed in 3.0. NelmioApiDocBundle 3 has native support for API Platform.
 */
final class ApiPlatformParser implements \Nelmio\ApiDocBundle\Parser\ParserInterface
{
    public const IN_PREFIX = 'api_platform_in';
    public const OUT_PREFIX = 'api_platform_out';
    public const TYPE_IRI = 'IRI';
    public const TYPE_MAP = [\Symfony\Component\PropertyInfo\Type::BUILTIN_TYPE_BOOL => \Nelmio\ApiDocBundle\DataTypes::BOOLEAN, \Symfony\Component\PropertyInfo\Type::BUILTIN_TYPE_FLOAT => \Nelmio\ApiDocBundle\DataTypes::FLOAT, \Symfony\Component\PropertyInfo\Type::BUILTIN_TYPE_INT => \Nelmio\ApiDocBundle\DataTypes::INTEGER, \Symfony\Component\PropertyInfo\Type::BUILTIN_TYPE_STRING => \Nelmio\ApiDocBundle\DataTypes::STRING];
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(array $item)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function parse(array $item) : array
    {
    }
}
