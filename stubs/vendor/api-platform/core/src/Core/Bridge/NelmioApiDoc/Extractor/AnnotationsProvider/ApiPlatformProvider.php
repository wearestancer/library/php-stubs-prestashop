<?php

namespace ApiPlatform\Core\Bridge\NelmioApiDoc\Extractor\AnnotationsProvider;

/**
 * Creates Nelmio ApiDoc annotations for the api platform.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 *
 * @deprecated since version 2.2, to be removed in 3.0. NelmioApiDocBundle 3 has native support for API Platform.
 */
final class ApiPlatformProvider implements \Nelmio\ApiDocBundle\Extractor\AnnotationsProviderInterface
{
    use \ApiPlatform\Core\Api\FilterLocatorTrait;
    /**
     * @param ContainerInterface|FilterCollection $filterLocator The new filter locator or the deprecated filter collection
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $documentationNormalizer, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, $filterLocator, \ApiPlatform\Core\Bridge\Symfony\Routing\OperationMethodResolverInterface $operationMethodResolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAnnotations() : array
    {
    }
}
