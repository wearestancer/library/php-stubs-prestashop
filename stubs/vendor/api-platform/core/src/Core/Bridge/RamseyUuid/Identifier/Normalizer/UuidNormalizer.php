<?php

namespace ApiPlatform\Core\Bridge\RamseyUuid\Identifier\Normalizer;

/**
 * Denormalizes an UUID string to an instance of Ramsey\Uuid.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class UuidNormalizer implements \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param mixed  $data
     * @param string $class
     * @param null   $format
     *
     * @throws InvalidIdentifierException
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
}
