<?php

namespace ApiPlatform\Core\Exception;

class InvalidResourceException extends \ApiPlatform\Exception\InvalidResourceException
{
}
