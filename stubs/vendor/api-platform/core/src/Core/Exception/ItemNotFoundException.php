<?php

namespace ApiPlatform\Core\Exception;

class ItemNotFoundException extends \ApiPlatform\Exception\ItemNotFoundException
{
}
