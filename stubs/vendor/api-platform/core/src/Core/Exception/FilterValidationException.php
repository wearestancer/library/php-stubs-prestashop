<?php

namespace ApiPlatform\Core\Exception;

final class FilterValidationException extends \ApiPlatform\Exception\FilterValidationException
{
}
