<?php

namespace ApiPlatform\Core\Exception;

class RuntimeException extends \ApiPlatform\Exception\RuntimeException
{
}
