<?php

namespace ApiPlatform\Core\Exception;

class PropertyNotFoundException extends \ApiPlatform\Exception\PropertyNotFoundException
{
}
