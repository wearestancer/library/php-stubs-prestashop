<?php

namespace ApiPlatform\Core\Exception;

class ResourceClassNotFoundException extends \ApiPlatform\Exception\ResourceClassNotFoundException
{
}
