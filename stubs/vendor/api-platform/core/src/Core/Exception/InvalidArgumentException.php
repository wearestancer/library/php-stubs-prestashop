<?php

namespace ApiPlatform\Core\Exception;

class InvalidArgumentException extends \ApiPlatform\Exception\InvalidArgumentException
{
}
