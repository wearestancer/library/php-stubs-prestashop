<?php

namespace ApiPlatform\Core\Exception;

class DeserializationException extends \ApiPlatform\Exception\DeserializationException
{
}
