<?php

namespace ApiPlatform\Core\Exception;

class ResourceClassNotSupportedException extends \ApiPlatform\Exception\ResourceClassNotSupportedException
{
}
