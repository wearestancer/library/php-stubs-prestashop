<?php

namespace ApiPlatform\Core\Exception;

class InvalidValueException extends \ApiPlatform\Exception\InvalidValueException
{
}
