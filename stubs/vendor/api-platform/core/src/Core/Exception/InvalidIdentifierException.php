<?php

namespace ApiPlatform\Core\Exception;

final class InvalidIdentifierException extends \ApiPlatform\Exception\InvalidIdentifierException
{
}
