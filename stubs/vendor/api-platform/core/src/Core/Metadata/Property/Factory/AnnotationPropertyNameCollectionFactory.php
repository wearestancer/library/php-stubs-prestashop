<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * Creates a property name collection from {@see ApiProperty} annotations.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AnnotationPropertyNameCollectionFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface
{
    public function __construct(\Doctrine\Common\Annotations\Reader $reader = null, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyNameCollection
    {
    }
}
