<?php

namespace ApiPlatform\Core\Metadata\Property;

/**
 * Subresource metadata.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class SubresourceMetadata
{
    public function __construct(string $resourceClass, bool $collection = false, int $maxDepth = null)
    {
    }
    public function getResourceClass() : string
    {
    }
    public function withResourceClass($resourceClass) : self
    {
    }
    public function isCollection() : bool
    {
    }
    public function withCollection(bool $collection) : self
    {
    }
    public function getMaxDepth() : ?int
    {
    }
}
