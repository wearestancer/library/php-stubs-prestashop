<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * @deprecated since 2.6, to be removed in 3.0
 */
final class InheritedPropertyMetadataFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyMetadata
    {
    }
}
