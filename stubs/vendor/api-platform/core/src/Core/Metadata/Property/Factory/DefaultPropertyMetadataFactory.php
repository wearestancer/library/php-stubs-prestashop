<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * Populates defaults values of the ressource properties using the default PHP values of properties.
 */
final class DefaultPropertyMetadataFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyMetadata
    {
    }
}
