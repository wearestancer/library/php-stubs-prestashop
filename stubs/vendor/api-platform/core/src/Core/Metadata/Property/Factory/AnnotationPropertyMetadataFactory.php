<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * Creates a property metadata from {@see ApiProperty} annotations.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AnnotationPropertyMetadataFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\Doctrine\Common\Annotations\Reader $reader = null, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyMetadata
    {
    }
}
