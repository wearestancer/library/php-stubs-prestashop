<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * Creates properties's metadata using an extractor.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ExtractorPropertyMetadataFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    /**
     * @param ResourceExtractorInterface|ExtractorInterface $extractor
     */
    public function __construct($extractor, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyMetadata
    {
    }
}
