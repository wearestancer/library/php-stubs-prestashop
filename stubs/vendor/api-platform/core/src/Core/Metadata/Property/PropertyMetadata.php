<?php

namespace ApiPlatform\Core\Metadata\Property;

/**
 * Property metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PropertyMetadata
{
    public function __construct(\Symfony\Component\PropertyInfo\Type $type = null, string $description = null, bool $readable = null, bool $writable = null, bool $readableLink = null, bool $writableLink = null, bool $required = null, bool $identifier = null, string $iri = null, $childInherited = null, array $attributes = null, \ApiPlatform\Core\Metadata\Property\SubresourceMetadata $subresource = null, bool $initializable = null, $default = null, $example = null, array $schema = null)
    {
    }
    /**
     * Gets type.
     *
     * @deprecated since 2.7, to be removed in 3.0, renamed as getBuiltinTypes
     */
    public function getType() : ?\Symfony\Component\PropertyInfo\Type
    {
    }
    /**
     * Returns a new instance with the given type.
     *
     * @deprecated since 2.7, to be removed in 3.0, renamed as withBuiltinTypes
     */
    public function withType(\Symfony\Component\PropertyInfo\Type $type) : self
    {
    }
    /**
     * Gets description.
     */
    public function getDescription() : ?string
    {
    }
    /**
     * Returns a new instance with the given description.
     */
    public function withDescription(string $description) : self
    {
    }
    /**
     * Is readable?
     */
    public function isReadable() : ?bool
    {
    }
    /**
     * Returns a new instance of Metadata with the given readable flag.
     */
    public function withReadable(bool $readable) : self
    {
    }
    /**
     * Is writable?
     */
    public function isWritable() : ?bool
    {
    }
    /**
     * Returns a new instance with the given writable flag.
     */
    public function withWritable(bool $writable) : self
    {
    }
    /**
     * Is required?
     */
    public function isRequired() : ?bool
    {
    }
    /**
     * Returns a new instance with the given required flag.
     */
    public function withRequired(bool $required) : self
    {
    }
    /**
     * Should an IRI or an object be provided in write context?
     */
    public function isWritableLink() : ?bool
    {
    }
    /**
     * Returns a new instance with the given writable link flag.
     */
    public function withWritableLink(bool $writableLink) : self
    {
    }
    /**
     * Is an IRI or an object generated in read context?
     */
    public function isReadableLink() : ?bool
    {
    }
    /**
     * Returns a new instance with the given readable link flag.
     */
    public function withReadableLink(bool $readableLink) : self
    {
    }
    /**
     * Gets IRI of this property.
     */
    public function getIri() : ?string
    {
    }
    /**
     * Returns a new instance with the given IRI.
     */
    public function withIri(string $iri = null) : self
    {
    }
    /**
     * Is this attribute an identifier?
     */
    public function isIdentifier() : ?bool
    {
    }
    /**
     * Returns a new instance with the given identifier flag.
     */
    public function withIdentifier(bool $identifier) : self
    {
    }
    /**
     * Gets attributes.
     */
    public function getAttributes() : ?array
    {
    }
    /**
     * Gets an attribute.
     *
     * @param mixed|null $defaultValue
     */
    public function getAttribute(string $key, $defaultValue = null)
    {
    }
    /**
     * Returns a new instance with the given attribute.
     */
    public function withAttributes(array $attributes) : self
    {
    }
    /**
     * @deprecated since 2.6, to be removed in 3.0
     */
    public function getChildInherited() : ?string
    {
    }
    /**
     * @deprecated since 2.6, to be removed in 3.0
     */
    public function hasChildInherited() : bool
    {
    }
    /**
     * @deprecated since 2.4, to be removed in 3.0
     */
    public function isChildInherited() : ?string
    {
    }
    /**
     * @deprecated since 2.6, to be removed in 3.0
     */
    public function withChildInherited(string $childInherited) : self
    {
    }
    /**
     * Represents whether the property has a subresource.
     */
    public function hasSubresource() : bool
    {
    }
    /**
     * Gets the subresource metadata.
     */
    public function getSubresource() : ?\ApiPlatform\Core\Metadata\Property\SubresourceMetadata
    {
    }
    /**
     * Returns a new instance with the given subresource.
     *
     * @param SubresourceMetadata $subresource
     */
    public function withSubresource(\ApiPlatform\Core\Metadata\Property\SubresourceMetadata $subresource = null) : self
    {
    }
    /**
     * Is initializable?
     */
    public function isInitializable() : ?bool
    {
    }
    /**
     * Returns a new instance with the given initializable flag.
     */
    public function withInitializable(bool $initializable) : self
    {
    }
    /**
     * Returns the default value of the property or NULL if the property doesn't have a default value.
     */
    public function getDefault()
    {
    }
    /**
     * Returns a new instance with the given default value for the property.
     *
     * @param mixed $default
     */
    public function withDefault($default) : self
    {
    }
    /**
     * Returns an example of the value of the property.
     */
    public function getExample()
    {
    }
    /**
     * Returns a new instance with the given example.
     *
     * @param mixed $example
     */
    public function withExample($example) : self
    {
    }
    /**
     * @return array
     */
    public function getSchema() : ?array
    {
    }
    /**
     * Returns a new instance with the given schema.
     */
    public function withSchema(array $schema = null) : self
    {
    }
}
