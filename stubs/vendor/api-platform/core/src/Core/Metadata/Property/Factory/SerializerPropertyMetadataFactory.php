<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * Populates read/write and link status using serialization groups.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class SerializerPropertyMetadataFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory, \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $serializerClassMetadataFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated, \ApiPlatform\Core\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyMetadata
    {
    }
}
