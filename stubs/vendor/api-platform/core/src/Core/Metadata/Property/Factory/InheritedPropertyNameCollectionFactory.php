<?php

namespace ApiPlatform\Core\Metadata\Property\Factory;

/**
 * @deprecated since 2.6, to be removed in 3.0
 */
final class InheritedPropertyNameCollectionFactory implements \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, array $options = []) : \ApiPlatform\Core\Metadata\Property\PropertyNameCollection
    {
    }
}
