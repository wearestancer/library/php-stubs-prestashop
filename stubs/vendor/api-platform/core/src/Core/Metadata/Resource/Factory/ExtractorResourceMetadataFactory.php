<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Creates resource's metadata using an extractor.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class ExtractorResourceMetadataFactory implements \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Extractor\ResourceExtractorInterface $extractor, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $decorated = null, array $defaults = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
}
