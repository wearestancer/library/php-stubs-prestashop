<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Creates a resource name collection from {@see ApiResource} annotations.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AnnotationResourceNameCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface
{
    /**
     * @param string[] $paths
     */
    public function __construct(\Doctrine\Common\Annotations\Reader $reader = null, array $paths, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create() : \ApiPlatform\Metadata\Resource\ResourceNameCollection
    {
    }
}
