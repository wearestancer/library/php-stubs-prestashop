<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Adds filters to the resource metadata {@see ApiFilter} annotation.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class AnnotationResourceFilterMetadataFactory implements \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface
{
    use \ApiPlatform\Util\AnnotationFilterExtractorTrait;
    public function __construct(?\Doctrine\Common\Annotations\Reader $reader = null, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
}
