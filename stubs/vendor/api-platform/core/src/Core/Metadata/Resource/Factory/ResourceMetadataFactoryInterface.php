<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Creates a resource metadata value object.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ResourceMetadataFactoryInterface
{
    /**
     * Creates a resource metadata.
     *
     * @throws ResourceClassNotFoundException
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
}
