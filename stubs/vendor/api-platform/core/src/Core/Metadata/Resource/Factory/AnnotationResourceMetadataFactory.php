<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Creates a resource metadata from {@see ApiResource} annotations.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class AnnotationResourceMetadataFactory implements \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface
{
    public function __construct(\Doctrine\Common\Annotations\Reader $reader = null, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $decorated = null, array $defaults = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
}
