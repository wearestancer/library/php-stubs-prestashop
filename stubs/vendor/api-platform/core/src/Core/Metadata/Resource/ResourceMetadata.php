<?php

namespace ApiPlatform\Core\Metadata\Resource;

/**
 * Resource metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ResourceMetadata
{
    public function __construct(string $shortName = null, string $description = null, string $iri = null, array $itemOperations = null, array $collectionOperations = null, array $attributes = null, array $subresourceOperations = null, array $graphql = null)
    {
    }
    /**
     * Gets the short name.
     */
    public function getShortName() : ?string
    {
    }
    /**
     * Returns a new instance with the given short name.
     */
    public function withShortName(string $shortName) : self
    {
    }
    /**
     * Gets the description.
     */
    public function getDescription() : ?string
    {
    }
    /**
     * Returns a new instance with the given description.
     */
    public function withDescription(string $description) : self
    {
    }
    /**
     * Gets the associated IRI.
     */
    public function getIri() : ?string
    {
    }
    /**
     * Returns a new instance with the given IRI.
     */
    public function withIri(string $iri) : self
    {
    }
    /**
     * Gets item operations.
     */
    public function getItemOperations() : ?array
    {
    }
    /**
     * Returns a new instance with the given item operations.
     */
    public function withItemOperations(array $itemOperations) : self
    {
    }
    /**
     * Gets collection operations.
     */
    public function getCollectionOperations() : ?array
    {
    }
    /**
     * Returns a new instance with the given collection operations.
     */
    public function withCollectionOperations(array $collectionOperations) : self
    {
    }
    /**
     * Gets subresource operations.
     */
    public function getSubresourceOperations() : ?array
    {
    }
    /**
     * Returns a new instance with the given subresource operations.
     */
    public function withSubresourceOperations(array $subresourceOperations) : self
    {
    }
    /**
     * Gets a collection operation attribute, optionally fallback to a resource attribute.
     *
     * @param mixed|null $defaultValue
     */
    public function getCollectionOperationAttribute(?string $operationName, string $key, $defaultValue = null, bool $resourceFallback = false)
    {
    }
    /**
     * Gets an item operation attribute, optionally fallback to a resource attribute.
     *
     * @param mixed|null $defaultValue
     */
    public function getItemOperationAttribute(?string $operationName, string $key, $defaultValue = null, bool $resourceFallback = false)
    {
    }
    /**
     * Gets a subresource operation attribute, optionally fallback to a resource attribute.
     *
     * @param mixed|null $defaultValue
     */
    public function getSubresourceOperationAttribute(?string $operationName, string $key, $defaultValue = null, bool $resourceFallback = false)
    {
    }
    public function getGraphqlAttribute(string $operationName, string $key, $defaultValue = null, bool $resourceFallback = false)
    {
    }
    /**
     * Gets the first available operation attribute according to the following order: collection, item, subresource, optionally fallback to a default value.
     *
     * @param mixed|null $defaultValue
     */
    public function getOperationAttribute(array $attributes, string $key, $defaultValue = null, bool $resourceFallback = false)
    {
    }
    /**
     * Gets an attribute for a given operation type and operation name.
     *
     * @param mixed|null $defaultValue
     */
    public function getTypedOperationAttribute(string $operationType, string $operationName, string $key, $defaultValue = null, bool $resourceFallback = false)
    {
    }
    /**
     * Gets attributes.
     */
    public function getAttributes() : ?array
    {
    }
    /**
     * Gets an attribute.
     *
     * @param mixed|null $defaultValue
     */
    public function getAttribute(string $key, $defaultValue = null)
    {
    }
    /**
     * Returns a new instance with the given attribute.
     */
    public function withAttributes(array $attributes) : self
    {
    }
    /**
     * Gets options of for the GraphQL query.
     */
    public function getGraphql() : ?array
    {
    }
    /**
     * Returns a new instance with the given GraphQL options.
     */
    public function withGraphql(array $graphql) : self
    {
    }
}
