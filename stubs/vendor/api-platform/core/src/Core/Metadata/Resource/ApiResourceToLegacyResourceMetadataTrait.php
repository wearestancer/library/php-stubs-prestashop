<?php

namespace ApiPlatform\Core\Metadata\Resource;

/**
 * @internal
 *
 * @deprecated
 */
trait ApiResourceToLegacyResourceMetadataTrait
{
    private $camelCaseToSnakeCaseNameConverter;
    private function transformResourceToResourceMetadata(\ApiPlatform\Metadata\ApiResource $resource) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
    private function toArray($object) : array
    {
    }
    private function transformUriVariablesToIdentifiers(array $arrayOperation) : array
    {
    }
    private function hasCompositeIdentifier(\ApiPlatform\Metadata\HttpOperation $operation) : bool
    {
    }
}
