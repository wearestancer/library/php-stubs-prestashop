<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Creates or completes operations.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class OperationResourceMetadataFactory implements \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface
{
    /**
     * @internal
     */
    public const SUPPORTED_COLLECTION_OPERATION_METHODS = ['GET' => true, 'POST' => true];
    /**
     * @internal
     */
    public const SUPPORTED_ITEM_OPERATION_METHODS = [
        'GET' => true,
        'PUT' => true,
        // PATCH is automatically supported if at least one patch format has been configured
        'DELETE' => true,
    ];
    public function __construct(\ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $decorated, array $patchFormats = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
}
