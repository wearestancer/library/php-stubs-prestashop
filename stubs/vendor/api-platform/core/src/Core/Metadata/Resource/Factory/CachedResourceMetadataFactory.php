<?php

namespace ApiPlatform\Core\Metadata\Resource\Factory;

/**
 * Caches resource metadata.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class CachedResourceMetadataFactory implements \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface
{
    use \ApiPlatform\Util\CachedTrait;
    public const CACHE_KEY_PREFIX = 'resource_metadata_';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Core\Metadata\Resource\ResourceMetadata
    {
    }
}
