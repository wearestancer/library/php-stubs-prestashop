<?php

namespace ApiPlatform\Core\Metadata\Resource;

/**
 * @internal
 * TODO: 3.0 remove the trait
 */
trait ToggleableOperationAttributeTrait
{
    /**
     * @var ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface|null
     */
    private $resourceMetadataFactory;
    private function isOperationAttributeDisabled(array $attributes, string $attribute, bool $default = false, bool $resourceFallback = true) : bool
    {
    }
}
