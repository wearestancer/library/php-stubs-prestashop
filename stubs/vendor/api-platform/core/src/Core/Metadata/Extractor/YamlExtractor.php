<?php

namespace ApiPlatform\Core\Metadata\Extractor;

/**
 * Extracts an array of metadata from a list of YAML files.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 *
 * @deprecated since 2.7, to remove in 3.0 (replaced by ApiPlatform\Metadata\Extractor\YamlExtractor)
 */
final class YamlExtractor extends \ApiPlatform\Metadata\Extractor\AbstractResourceExtractor implements \ApiPlatform\Metadata\Extractor\PropertyExtractorInterface
{
    /**
     * {@inheritdoc}
     */
    public function getProperties() : array
    {
    }
}
