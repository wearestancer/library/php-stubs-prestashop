<?php

namespace ApiPlatform\Core\Metadata\Extractor;

/**
 * Extracts an array of metadata from a list of XML files.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Antoine Bluchet <soyuka@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 *
 * @deprecated since 2.7, to remove in 3.0 (replaced by ApiPlatform\Metadata\Extractor\XmlExtractor)
 */
final class XmlExtractor extends \ApiPlatform\Metadata\Extractor\AbstractResourceExtractor implements \ApiPlatform\Metadata\Extractor\PropertyExtractorInterface
{
    public const RESOURCE_SCHEMA = __DIR__ . '/../schema/metadata.xsd';
    /**
     * {@inheritdoc}
     */
    public function getProperties() : array
    {
    }
}
