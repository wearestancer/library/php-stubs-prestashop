<?php

namespace ApiPlatform\OpenApi\Serializer;

/**
 * Removes features unsupported by Amazon API Gateway.
 *
 * @see https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-known-issues.html
 *
 * @internal
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ApiGatewayNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const API_GATEWAY = 'api_gateway';
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $documentationNormalizer, $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws UnexpectedValueException
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
