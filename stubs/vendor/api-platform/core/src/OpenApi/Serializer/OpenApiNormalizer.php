<?php

namespace ApiPlatform\OpenApi\Serializer;

/**
 * Generates an OpenAPI v3 specification.
 */
final class OpenApiNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT = 'json';
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
