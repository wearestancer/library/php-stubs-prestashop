<?php

namespace ApiPlatform\OpenApi;

final class Options
{
    public function __construct(string $title, string $description = '', string $version = '', bool $oAuthEnabled = false, ?string $oAuthType = null, ?string $oAuthFlow = null, ?string $oAuthTokenUrl = null, ?string $oAuthAuthorizationUrl = null, ?string $oAuthRefreshUrl = null, array $oAuthScopes = [], array $apiKeys = [], string $contactName = null, string $contactUrl = null, string $contactEmail = null, string $termsOfService = null, string $licenseName = null, string $licenseUrl = null)
    {
    }
    public function getTitle() : string
    {
    }
    public function getDescription() : string
    {
    }
    public function getVersion() : string
    {
    }
    public function getOAuthEnabled() : bool
    {
    }
    public function getOAuthType() : ?string
    {
    }
    public function getOAuthFlow() : ?string
    {
    }
    public function getOAuthTokenUrl() : ?string
    {
    }
    public function getOAuthAuthorizationUrl() : ?string
    {
    }
    public function getOAuthRefreshUrl() : ?string
    {
    }
    public function getOAuthScopes() : array
    {
    }
    public function getApiKeys() : array
    {
    }
    public function getContactName() : ?string
    {
    }
    public function getContactUrl() : ?string
    {
    }
    public function getContactEmail() : ?string
    {
    }
    public function getTermsOfService() : ?string
    {
    }
    public function getLicenseName() : ?string
    {
    }
    public function getLicenseUrl() : ?string
    {
    }
}
