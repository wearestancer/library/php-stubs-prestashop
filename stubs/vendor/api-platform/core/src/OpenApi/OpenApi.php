<?php

namespace ApiPlatform\OpenApi;

final class OpenApi implements \ApiPlatform\Documentation\DocumentationInterface
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    // We're actually supporting 3.1 but swagger ui has a version constraint
    // public const VERSION = '3.1.0';
    public const VERSION = '3.0.0';
    public function __construct(\ApiPlatform\OpenApi\Model\Info $info, array $servers, \ApiPlatform\OpenApi\Model\Paths $paths, \ApiPlatform\OpenApi\Model\Components $components = null, array $security = [], array $tags = [], $externalDocs = null, string $jsonSchemaDialect = null, \ArrayObject $webhooks = null)
    {
    }
    public function getOpenapi() : string
    {
    }
    public function getInfo() : \ApiPlatform\OpenApi\Model\Info
    {
    }
    public function getServers() : array
    {
    }
    public function getPaths() : \ApiPlatform\OpenApi\Model\Paths
    {
    }
    public function getComponents() : \ApiPlatform\OpenApi\Model\Components
    {
    }
    public function getSecurity() : array
    {
    }
    public function getTags() : array
    {
    }
    public function getExternalDocs() : ?array
    {
    }
    public function getJsonSchemaDialect() : ?string
    {
    }
    public function getWebhooks() : ?\ArrayObject
    {
    }
    public function withOpenapi(string $openapi) : self
    {
    }
    public function withInfo(\ApiPlatform\OpenApi\Model\Info $info) : self
    {
    }
    public function withServers(array $servers) : self
    {
    }
    public function withPaths(\ApiPlatform\OpenApi\Model\Paths $paths) : self
    {
    }
    public function withComponents(\ApiPlatform\OpenApi\Model\Components $components) : self
    {
    }
    public function withSecurity(array $security) : self
    {
    }
    public function withTags(array $tags) : self
    {
    }
    public function withExternalDocs(array $externalDocs) : self
    {
    }
    public function withJsonSchemaDialect(?string $jsonSchemaDialect) : self
    {
    }
}
