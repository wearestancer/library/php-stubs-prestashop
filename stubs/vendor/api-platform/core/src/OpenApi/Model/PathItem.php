<?php

namespace ApiPlatform\OpenApi\Model;

final class PathItem
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public static $methods = ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS', 'HEAD', 'PATCH', 'TRACE'];
    public function __construct(string $ref = null, string $summary = null, string $description = null, \ApiPlatform\OpenApi\Model\Operation $get = null, \ApiPlatform\OpenApi\Model\Operation $put = null, \ApiPlatform\OpenApi\Model\Operation $post = null, \ApiPlatform\OpenApi\Model\Operation $delete = null, \ApiPlatform\OpenApi\Model\Operation $options = null, \ApiPlatform\OpenApi\Model\Operation $head = null, \ApiPlatform\OpenApi\Model\Operation $patch = null, \ApiPlatform\OpenApi\Model\Operation $trace = null, ?array $servers = null, array $parameters = [])
    {
    }
    public function getRef() : ?string
    {
    }
    public function getSummary() : ?string
    {
    }
    public function getDescription() : ?string
    {
    }
    public function getGet() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getPut() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getPost() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getDelete() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getOptions() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getHead() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getPatch() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getTrace() : ?\ApiPlatform\OpenApi\Model\Operation
    {
    }
    public function getServers() : ?array
    {
    }
    public function getParameters() : array
    {
    }
    public function withRef(string $ref) : self
    {
    }
    public function withSummary(string $summary) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withGet(?\ApiPlatform\OpenApi\Model\Operation $get) : self
    {
    }
    public function withPut(?\ApiPlatform\OpenApi\Model\Operation $put) : self
    {
    }
    public function withPost(?\ApiPlatform\OpenApi\Model\Operation $post) : self
    {
    }
    public function withDelete(?\ApiPlatform\OpenApi\Model\Operation $delete) : self
    {
    }
    public function withOptions(\ApiPlatform\OpenApi\Model\Operation $options) : self
    {
    }
    public function withHead(\ApiPlatform\OpenApi\Model\Operation $head) : self
    {
    }
    public function withPatch(?\ApiPlatform\OpenApi\Model\Operation $patch) : self
    {
    }
    public function withTrace(\ApiPlatform\OpenApi\Model\Operation $trace) : self
    {
    }
    public function withServers(?array $servers = null) : self
    {
    }
    public function withParameters(array $parameters) : self
    {
    }
}
