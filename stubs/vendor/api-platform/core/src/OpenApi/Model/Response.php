<?php

namespace ApiPlatform\OpenApi\Model;

final class Response
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $description = '', \ArrayObject $content = null, \ArrayObject $headers = null, \ArrayObject $links = null)
    {
    }
    public function getDescription() : string
    {
    }
    public function getContent() : ?\ArrayObject
    {
    }
    public function getHeaders() : ?\ArrayObject
    {
    }
    public function getLinks() : ?\ArrayObject
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withContent(\ArrayObject $content) : self
    {
    }
    public function withHeaders(\ArrayObject $headers) : self
    {
    }
    public function withLinks(\ArrayObject $links) : self
    {
    }
}
