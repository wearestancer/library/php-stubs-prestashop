<?php

namespace ApiPlatform\OpenApi\Model;

final class Schema extends \ArrayObject
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(bool $nullable = null, $discriminator = null, bool $readOnly = false, bool $writeOnly = false, string $xml = null, $externalDocs = null, $example = null, bool $deprecated = false)
    {
    }
    public function setDefinitions(array $definitions)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getArrayCopy() : array
    {
    }
    public function getDefinitions() : \ArrayObject
    {
    }
    public function getNullable() : bool
    {
    }
    public function getDiscriminator()
    {
    }
    public function getReadOnly() : bool
    {
    }
    public function getWriteOnly() : bool
    {
    }
    public function getXml() : string
    {
    }
    public function getExternalDocs()
    {
    }
    public function getExample()
    {
    }
    public function getDeprecated() : bool
    {
    }
    public function withNullable(bool $nullable) : self
    {
    }
    public function withDiscriminator($discriminator) : self
    {
    }
    public function withReadOnly(bool $readOnly) : self
    {
    }
    public function withWriteOnly(bool $writeOnly) : self
    {
    }
    public function withXml(string $xml) : self
    {
    }
    public function withExternalDocs($externalDocs) : self
    {
    }
    public function withExample($example) : self
    {
    }
    public function withDeprecated(bool $deprecated) : self
    {
    }
}
