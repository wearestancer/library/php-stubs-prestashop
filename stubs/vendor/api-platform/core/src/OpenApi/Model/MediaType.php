<?php

namespace ApiPlatform\OpenApi\Model;

final class MediaType
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(\ArrayObject $schema = null, $example = null, \ArrayObject $examples = null, \ApiPlatform\OpenApi\Model\Encoding $encoding = null)
    {
    }
    public function getSchema() : ?\ArrayObject
    {
    }
    public function getExample()
    {
    }
    public function getExamples() : ?\ArrayObject
    {
    }
    public function getEncoding() : ?\ApiPlatform\OpenApi\Model\Encoding
    {
    }
    public function withSchema(\ArrayObject $schema) : self
    {
    }
    public function withExample($example) : self
    {
    }
    public function withExamples(\ArrayObject $examples) : self
    {
    }
    public function withEncoding(\ApiPlatform\OpenApi\Model\Encoding $encoding) : self
    {
    }
}
