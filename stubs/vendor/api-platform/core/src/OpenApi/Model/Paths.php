<?php

namespace ApiPlatform\OpenApi\Model;

final class Paths
{
    public function addPath(string $path, \ApiPlatform\OpenApi\Model\PathItem $pathItem)
    {
    }
    public function getPath(string $path) : ?\ApiPlatform\OpenApi\Model\PathItem
    {
    }
    public function getPaths() : array
    {
    }
}
