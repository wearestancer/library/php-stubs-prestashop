<?php

namespace ApiPlatform\OpenApi\Model;

final class Encoding
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $contentType = '', \ArrayObject $headers = null, string $style = '', bool $explode = false, bool $allowReserved = false)
    {
    }
    public function getContentType() : string
    {
    }
    public function getHeaders() : ?\ArrayObject
    {
    }
    public function getStyle() : string
    {
    }
    public function canExplode() : bool
    {
    }
    public function getExplode() : bool
    {
    }
    public function canAllowReserved() : bool
    {
    }
    public function getAllowReserved() : bool
    {
    }
    public function withContentType(string $contentType) : self
    {
    }
    public function withHeaders(?\ArrayObject $headers) : self
    {
    }
    public function withStyle(string $style) : self
    {
    }
    public function withExplode(bool $explode) : self
    {
    }
    public function withAllowReserved(bool $allowReserved) : self
    {
    }
}
