<?php

namespace ApiPlatform\OpenApi\Model;

final class OAuthFlows
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(\ApiPlatform\OpenApi\Model\OAuthFlow $implicit = null, \ApiPlatform\OpenApi\Model\OAuthFlow $password = null, \ApiPlatform\OpenApi\Model\OAuthFlow $clientCredentials = null, \ApiPlatform\OpenApi\Model\OAuthFlow $authorizationCode = null)
    {
    }
    public function getImplicit() : ?\ApiPlatform\OpenApi\Model\OAuthFlow
    {
    }
    public function getPassword() : ?\ApiPlatform\OpenApi\Model\OAuthFlow
    {
    }
    public function getClientCredentials() : ?\ApiPlatform\OpenApi\Model\OAuthFlow
    {
    }
    public function getAuthorizationCode() : ?\ApiPlatform\OpenApi\Model\OAuthFlow
    {
    }
    public function withImplicit(\ApiPlatform\OpenApi\Model\OAuthFlow $implicit) : self
    {
    }
    public function withPassword(\ApiPlatform\OpenApi\Model\OAuthFlow $password) : self
    {
    }
    public function withClientCredentials(\ApiPlatform\OpenApi\Model\OAuthFlow $clientCredentials) : self
    {
    }
    public function withAuthorizationCode(\ApiPlatform\OpenApi\Model\OAuthFlow $authorizationCode) : self
    {
    }
}
