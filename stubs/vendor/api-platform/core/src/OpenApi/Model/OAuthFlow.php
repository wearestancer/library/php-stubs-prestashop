<?php

namespace ApiPlatform\OpenApi\Model;

final class OAuthFlow
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $authorizationUrl = null, string $tokenUrl = null, string $refreshUrl = null, \ArrayObject $scopes = null)
    {
    }
    public function getAuthorizationUrl() : ?string
    {
    }
    public function getTokenUrl() : ?string
    {
    }
    public function getRefreshUrl() : ?string
    {
    }
    public function getScopes() : \ArrayObject
    {
    }
    public function withAuthorizationUrl(string $authorizationUrl) : self
    {
    }
    public function withTokenUrl(string $tokenUrl) : self
    {
    }
    public function withRefreshUrl(string $refreshUrl) : self
    {
    }
    public function withScopes(\ArrayObject $scopes) : self
    {
    }
}
