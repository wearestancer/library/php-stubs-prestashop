<?php

namespace ApiPlatform\OpenApi\Model;

final class Components
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(\ArrayObject $schemas = null, \ArrayObject $responses = null, \ArrayObject $parameters = null, \ArrayObject $examples = null, \ArrayObject $requestBodies = null, \ArrayObject $headers = null, \ArrayObject $securitySchemes = null, \ArrayObject $links = null, \ArrayObject $callbacks = null, \ArrayObject $pathItems = null)
    {
    }
    public function getSchemas() : ?\ArrayObject
    {
    }
    public function getResponses() : ?\ArrayObject
    {
    }
    public function getParameters() : ?\ArrayObject
    {
    }
    public function getExamples() : ?\ArrayObject
    {
    }
    public function getRequestBodies() : ?\ArrayObject
    {
    }
    public function getHeaders() : ?\ArrayObject
    {
    }
    public function getSecuritySchemes() : ?\ArrayObject
    {
    }
    public function getLinks() : ?\ArrayObject
    {
    }
    public function getCallbacks() : ?\ArrayObject
    {
    }
    public function getPathItems() : ?\ArrayObject
    {
    }
    public function withSchemas(\ArrayObject $schemas) : self
    {
    }
    public function withResponses(\ArrayObject $responses) : self
    {
    }
    public function withParameters(\ArrayObject $parameters) : self
    {
    }
    public function withExamples(\ArrayObject $examples) : self
    {
    }
    public function withRequestBodies(\ArrayObject $requestBodies) : self
    {
    }
    public function withHeaders(\ArrayObject $headers) : self
    {
    }
    public function withSecuritySchemes(\ArrayObject $securitySchemes) : self
    {
    }
    public function withLinks(\ArrayObject $links) : self
    {
    }
    public function withCallbacks(\ArrayObject $callbacks) : self
    {
    }
    public function withPathItems(\ArrayObject $pathItems) : self
    {
    }
}
