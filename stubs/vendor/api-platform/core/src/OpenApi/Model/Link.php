<?php

namespace ApiPlatform\OpenApi\Model;

final class Link
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $operationId, \ArrayObject $parameters = null, $requestBody = null, string $description = '', \ApiPlatform\OpenApi\Model\Server $server = null)
    {
    }
    public function getOperationId() : string
    {
    }
    public function getParameters() : \ArrayObject
    {
    }
    public function getRequestBody()
    {
    }
    public function getDescription() : string
    {
    }
    public function getServer() : ?\ApiPlatform\OpenApi\Model\Server
    {
    }
    public function withOperationId(string $operationId) : self
    {
    }
    public function withParameters(\ArrayObject $parameters) : self
    {
    }
    public function withRequestBody($requestBody) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withServer(\ApiPlatform\OpenApi\Model\Server $server) : self
    {
    }
}
