<?php

namespace ApiPlatform\OpenApi\Model;

final class Operation
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $operationId = null, array $tags = [], array $responses = [], string $summary = '', string $description = '', \ApiPlatform\OpenApi\Model\ExternalDocumentation $externalDocs = null, array $parameters = [], \ApiPlatform\OpenApi\Model\RequestBody $requestBody = null, \ArrayObject $callbacks = null, bool $deprecated = false, ?array $security = null, ?array $servers = null, array $extensionProperties = [])
    {
    }
    public function addResponse(\ApiPlatform\OpenApi\Model\Response $response, $status = 'default') : self
    {
    }
    public function getOperationId() : string
    {
    }
    public function getTags() : array
    {
    }
    public function getResponses() : array
    {
    }
    public function getSummary() : string
    {
    }
    public function getDescription() : string
    {
    }
    public function getExternalDocs() : ?\ApiPlatform\OpenApi\Model\ExternalDocumentation
    {
    }
    public function getParameters() : array
    {
    }
    public function getRequestBody() : ?\ApiPlatform\OpenApi\Model\RequestBody
    {
    }
    public function getCallbacks() : ?\ArrayObject
    {
    }
    public function getDeprecated() : bool
    {
    }
    public function getSecurity() : ?array
    {
    }
    public function getServers() : ?array
    {
    }
    public function withOperationId(string $operationId) : self
    {
    }
    public function withTags(array $tags) : self
    {
    }
    public function withResponses(array $responses) : self
    {
    }
    public function withSummary(string $summary) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withExternalDocs(\ApiPlatform\OpenApi\Model\ExternalDocumentation $externalDocs) : self
    {
    }
    public function withParameters(array $parameters) : self
    {
    }
    public function withRequestBody(?\ApiPlatform\OpenApi\Model\RequestBody $requestBody = null) : self
    {
    }
    public function withCallbacks(\ArrayObject $callbacks) : self
    {
    }
    public function withDeprecated(bool $deprecated) : self
    {
    }
    public function withSecurity(?array $security = null) : self
    {
    }
    public function withServers(?array $servers = null) : self
    {
    }
}
