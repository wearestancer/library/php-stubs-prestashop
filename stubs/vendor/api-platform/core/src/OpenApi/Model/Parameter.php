<?php

namespace ApiPlatform\OpenApi\Model;

final class Parameter
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $name, string $in, string $description = '', bool $required = false, bool $deprecated = false, bool $allowEmptyValue = false, array $schema = [], string $style = null, bool $explode = false, bool $allowReserved = false, $example = null, \ArrayObject $examples = null, \ArrayObject $content = null)
    {
    }
    // TODO: string not ?string
    public function getName() : ?string
    {
    }
    // TODO: string not ?string
    public function getIn() : ?string
    {
    }
    public function getDescription() : string
    {
    }
    public function getRequired() : bool
    {
    }
    public function getDeprecated() : bool
    {
    }
    public function canAllowEmptyValue() : bool
    {
    }
    public function getAllowEmptyValue() : bool
    {
    }
    public function getSchema() : array
    {
    }
    public function getStyle() : string
    {
    }
    public function canExplode() : bool
    {
    }
    public function getExplode() : bool
    {
    }
    public function canAllowReserved() : bool
    {
    }
    public function getAllowReserved() : bool
    {
    }
    public function getExample()
    {
    }
    public function getExamples() : ?\ArrayObject
    {
    }
    public function getContent() : ?\ArrayObject
    {
    }
    public function withName(string $name) : self
    {
    }
    public function withIn(string $in) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withRequired(bool $required) : self
    {
    }
    public function withDeprecated(bool $deprecated) : self
    {
    }
    public function withAllowEmptyValue(bool $allowEmptyValue) : self
    {
    }
    public function withSchema(array $schema) : self
    {
    }
    public function withStyle(string $style) : self
    {
    }
    public function withExplode(bool $explode) : self
    {
    }
    public function withAllowReserved(bool $allowReserved) : self
    {
    }
    public function withExample($example) : self
    {
    }
    public function withExamples(\ArrayObject $examples) : self
    {
    }
    public function withContent(\ArrayObject $content) : self
    {
    }
}
