<?php

namespace ApiPlatform\OpenApi\Model;

final class ExternalDocumentation
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $description = '', string $url = '')
    {
    }
    public function getDescription() : string
    {
    }
    public function getUrl() : string
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withUrl(string $url) : self
    {
    }
}
