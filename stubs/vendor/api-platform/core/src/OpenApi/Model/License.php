<?php

namespace ApiPlatform\OpenApi\Model;

final class License
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $name, string $url = null, string $identifier = null)
    {
    }
    public function getName() : string
    {
    }
    public function getUrl() : ?string
    {
    }
    public function getIdentifier() : ?string
    {
    }
    public function withName(string $name) : self
    {
    }
    public function withUrl(?string $url) : self
    {
    }
    public function withIdentifier(?string $identifier) : self
    {
    }
}
