<?php

namespace ApiPlatform\OpenApi\Model;

final class RequestBody
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $description = '', \ArrayObject $content = null, bool $required = false)
    {
    }
    public function getDescription() : string
    {
    }
    public function getContent() : \ArrayObject
    {
    }
    public function getRequired() : bool
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withContent(\ArrayObject $content) : self
    {
    }
    public function withRequired(bool $required) : self
    {
    }
}
