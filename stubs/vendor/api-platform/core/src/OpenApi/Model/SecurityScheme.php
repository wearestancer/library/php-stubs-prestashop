<?php

namespace ApiPlatform\OpenApi\Model;

final class SecurityScheme
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $type = null, string $description = '', string $name = null, string $in = null, string $scheme = null, string $bearerFormat = null, \ApiPlatform\OpenApi\Model\OAuthFlows $flows = null, string $openIdConnectUrl = null)
    {
    }
    public function getType() : string
    {
    }
    public function getDescription() : string
    {
    }
    public function getName() : ?string
    {
    }
    public function getIn() : ?string
    {
    }
    public function getScheme() : ?string
    {
    }
    public function getBearerFormat() : ?string
    {
    }
    public function getFlows() : ?\ApiPlatform\OpenApi\Model\OAuthFlows
    {
    }
    public function getOpenIdConnectUrl() : ?string
    {
    }
    public function withType(string $type) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withName(string $name) : self
    {
    }
    public function withIn(string $in) : self
    {
    }
    public function withScheme(string $scheme) : self
    {
    }
    public function withBearerFormat(string $bearerFormat) : self
    {
    }
    public function withFlows(\ApiPlatform\OpenApi\Model\OAuthFlows $flows) : self
    {
    }
    public function withOpenIdConnectUrl(string $openIdConnectUrl) : self
    {
    }
}
