<?php

namespace ApiPlatform\OpenApi\Model;

final class Contact
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $name = null, string $url = null, string $email = null)
    {
    }
    public function getName() : ?string
    {
    }
    public function getUrl() : ?string
    {
    }
    public function getEmail() : ?string
    {
    }
    public function withName(?string $name) : self
    {
    }
    public function withUrl(?string $url) : self
    {
    }
    public function withEmail(?string $email) : self
    {
    }
}
