<?php

namespace ApiPlatform\OpenApi\Model;

final class Server
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $url, string $description = '', \ArrayObject $variables = null)
    {
    }
    public function getUrl() : string
    {
    }
    public function getDescription() : string
    {
    }
    public function getVariables() : ?\ArrayObject
    {
    }
    public function withUrl(string $url) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withVariables(\ArrayObject $variables) : self
    {
    }
}
