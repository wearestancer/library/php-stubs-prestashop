<?php

namespace ApiPlatform\OpenApi\Model;

final class Info
{
    use \ApiPlatform\OpenApi\Model\ExtensionTrait;
    public function __construct(string $title, string $version, string $description = '', string $termsOfService = null, \ApiPlatform\OpenApi\Model\Contact $contact = null, \ApiPlatform\OpenApi\Model\License $license = null, string $summary = null)
    {
    }
    public function getTitle() : string
    {
    }
    public function getDescription() : string
    {
    }
    public function getTermsOfService() : ?string
    {
    }
    public function getContact() : ?\ApiPlatform\OpenApi\Model\Contact
    {
    }
    public function getLicense() : ?\ApiPlatform\OpenApi\Model\License
    {
    }
    public function getVersion() : string
    {
    }
    public function getSummary() : ?string
    {
    }
    public function withTitle(string $title) : self
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function withTermsOfService(string $termsOfService) : self
    {
    }
    public function withContact(\ApiPlatform\OpenApi\Model\Contact $contact) : self
    {
    }
    public function withLicense(\ApiPlatform\OpenApi\Model\License $license) : self
    {
    }
    public function withVersion(string $version) : self
    {
    }
    public function withSummary(string $summary) : self
    {
    }
}
