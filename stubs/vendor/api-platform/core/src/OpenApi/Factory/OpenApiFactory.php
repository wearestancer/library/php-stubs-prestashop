<?php

namespace ApiPlatform\OpenApi\Factory;

/**
 * Generates an Open API v3 specification.
 */
final class OpenApiFactory implements \ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface
{
    use \ApiPlatform\Api\FilterLocatorTrait;
    public const BASE_URL = 'base_url';
    public const OPENAPI_DEFINITION_NAME = 'openapi_definition_name';
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory, $propertyNameCollectionFactory, $propertyMetadataFactory, \ApiPlatform\JsonSchema\SchemaFactoryInterface $jsonSchemaFactory, \ApiPlatform\JsonSchema\TypeFactoryInterface $jsonSchemaTypeFactory, \ApiPlatform\PathResolver\OperationPathResolverInterface $operationPathResolver, \Psr\Container\ContainerInterface $filterLocator, array $formats = [], \ApiPlatform\OpenApi\Options $openApiOptions = null, \ApiPlatform\State\Pagination\PaginationOptions $paginationOptions = null, \Symfony\Component\Routing\RouterInterface $router = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(array $context = []) : \ApiPlatform\OpenApi\OpenApi
    {
    }
}
