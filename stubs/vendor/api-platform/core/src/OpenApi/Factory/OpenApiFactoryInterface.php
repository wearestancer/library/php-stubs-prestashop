<?php

namespace ApiPlatform\OpenApi\Factory;

interface OpenApiFactoryInterface
{
    /**
     * Creates an OpenApi class.
     */
    public function __invoke(array $context = []) : \ApiPlatform\OpenApi\OpenApi;
}
