<?php

namespace ApiPlatform\GraphQl;

/**
 * Wrapper for the GraphQL facade.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class Executor implements \ApiPlatform\GraphQl\ExecutorInterface
{
    /**
     * {@inheritdoc}
     */
    public function executeQuery(\GraphQL\Type\Schema $schema, $source, $rootValue = null, $context = null, array $variableValues = null, string $operationName = null, callable $fieldResolver = null, array $validationRules = null) : \GraphQL\Executor\ExecutionResult
    {
    }
}
