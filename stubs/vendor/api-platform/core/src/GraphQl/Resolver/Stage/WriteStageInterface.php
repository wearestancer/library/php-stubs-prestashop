<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Write stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface WriteStageInterface
{
    /**
     * @param object|null $data
     *
     * @return object|null
     */
    public function __invoke($data, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context);
}
