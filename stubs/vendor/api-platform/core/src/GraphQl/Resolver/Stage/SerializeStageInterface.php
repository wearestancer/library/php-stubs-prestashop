<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Serialize stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SerializeStageInterface
{
    /**
     * @param object|iterable|null $itemOrCollection
     */
    public function __invoke($itemOrCollection, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : ?array;
}
