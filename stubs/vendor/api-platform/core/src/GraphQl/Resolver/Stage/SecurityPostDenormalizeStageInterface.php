<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Security post deserialization stage of GraphQL resolvers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface SecurityPostDenormalizeStageInterface
{
    /**
     * @throws Error
     */
    public function __invoke(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void;
}
