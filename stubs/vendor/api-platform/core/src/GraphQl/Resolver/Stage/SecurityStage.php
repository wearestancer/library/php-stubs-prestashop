<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Security stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SecurityStage implements \ApiPlatform\GraphQl\Resolver\Stage\SecurityStageInterface
{
    public function __construct(?\ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void
    {
    }
}
