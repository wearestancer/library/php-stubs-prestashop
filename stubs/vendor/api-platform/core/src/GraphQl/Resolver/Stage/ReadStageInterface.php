<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Read stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface ReadStageInterface
{
    /**
     * @return object|iterable|null
     */
    public function __invoke(?string $resourceClass, ?string $rootClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context);
}
