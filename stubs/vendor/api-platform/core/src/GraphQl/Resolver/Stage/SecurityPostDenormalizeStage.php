<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Security post denormalize stage of GraphQL resolvers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class SecurityPostDenormalizeStage implements \ApiPlatform\GraphQl\Resolver\Stage\SecurityPostDenormalizeStageInterface
{
    public function __construct(?\ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void
    {
    }
}
