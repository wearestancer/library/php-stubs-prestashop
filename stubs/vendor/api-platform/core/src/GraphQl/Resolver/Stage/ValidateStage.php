<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Validate stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ValidateStage implements \ApiPlatform\GraphQl\Resolver\Stage\ValidateStageInterface
{
    public function __construct(\ApiPlatform\Validator\ValidatorInterface $validator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($object, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void
    {
    }
}
