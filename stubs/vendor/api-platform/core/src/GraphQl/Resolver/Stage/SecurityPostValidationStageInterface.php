<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Security post validation stage of GraphQL resolvers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
interface SecurityPostValidationStageInterface
{
    /**
     * @throws Error
     */
    public function __invoke(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void;
}
