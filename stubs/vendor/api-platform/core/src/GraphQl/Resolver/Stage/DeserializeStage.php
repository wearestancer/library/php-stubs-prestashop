<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Deserialize stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class DeserializeStage implements \ApiPlatform\GraphQl\Resolver\Stage\DeserializeStageInterface
{
    public function __construct(\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer, \ApiPlatform\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($objectToPopulate, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context)
    {
    }
}
