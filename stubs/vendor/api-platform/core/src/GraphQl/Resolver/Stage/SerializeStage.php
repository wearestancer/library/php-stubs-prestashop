<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Serialize stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SerializeStage implements \ApiPlatform\GraphQl\Resolver\Stage\SerializeStageInterface
{
    use \ApiPlatform\GraphQl\Resolver\Util\IdentifierTrait;
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer, \ApiPlatform\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder, \ApiPlatform\State\Pagination\Pagination $pagination)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($itemOrCollection, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : ?array
    {
    }
}
