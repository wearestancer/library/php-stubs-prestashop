<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Security post validation stage of GraphQL resolvers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
final class SecurityPostValidationStage implements \ApiPlatform\GraphQl\Resolver\Stage\SecurityPostValidationStageInterface
{
    public function __construct(?\ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void
    {
    }
}
