<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Deserialize stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface DeserializeStageInterface
{
    /**
     * @param object|null $objectToPopulate
     *
     * @return object|null
     */
    public function __invoke($objectToPopulate, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context);
}
