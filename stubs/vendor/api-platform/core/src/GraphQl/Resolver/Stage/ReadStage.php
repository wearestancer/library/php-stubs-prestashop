<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Read stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ReadStage implements \ApiPlatform\GraphQl\Resolver\Stage\ReadStageInterface
{
    use \ApiPlatform\Util\ArrayTrait;
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\GraphQl\Resolver\Util\IdentifierTrait;
    public function __construct(\ApiPlatform\Api\IriConverterInterface $iriConverter, \ApiPlatform\State\ProviderInterface $provider, \ApiPlatform\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder, string $nestingSeparator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(?string $resourceClass, ?string $rootClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context)
    {
    }
}
