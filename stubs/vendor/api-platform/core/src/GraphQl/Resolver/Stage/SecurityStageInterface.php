<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Security stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface SecurityStageInterface
{
    /**
     * @throws Error
     */
    public function __invoke(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void;
}
