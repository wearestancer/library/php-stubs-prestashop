<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Validate stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface ValidateStageInterface
{
    /**
     * @param object $object
     *
     * @throws Error
     */
    public function __invoke($object, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context) : void;
}
