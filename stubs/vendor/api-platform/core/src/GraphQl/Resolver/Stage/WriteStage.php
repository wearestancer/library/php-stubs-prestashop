<?php

namespace ApiPlatform\GraphQl\Resolver\Stage;

/**
 * Write stage of GraphQL resolvers.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class WriteStage implements \ApiPlatform\GraphQl\Resolver\Stage\WriteStageInterface
{
    public function __construct(\ApiPlatform\State\ProcessorInterface $processor, \ApiPlatform\GraphQl\Serializer\SerializerContextBuilderInterface $serializerContextBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke($data, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $context)
    {
    }
}
