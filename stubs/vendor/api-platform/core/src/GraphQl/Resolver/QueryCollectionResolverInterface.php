<?php

namespace ApiPlatform\GraphQl\Resolver;

/**
 * A function resolving a GraphQL query of a collection.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface QueryCollectionResolverInterface
{
    /**
     * @param iterable<object> $collection
     *
     * @return iterable<object>
     */
    public function __invoke(iterable $collection, array $context) : iterable;
}
