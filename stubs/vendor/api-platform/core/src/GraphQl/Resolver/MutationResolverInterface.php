<?php

namespace ApiPlatform\GraphQl\Resolver;

/**
 * A function resolving a GraphQL mutation.
 *
 * @author Raoul Clais <raoul.clais@gmail.com>
 */
interface MutationResolverInterface
{
    /**
     * @param object|null $item
     *
     * @return object|null The mutated item
     */
    public function __invoke($item, array $context);
}
