<?php

namespace ApiPlatform\GraphQl\Resolver;

/**
 * A function resolving a GraphQL query of an item.
 *
 * @author Lukas Lücke <lukas@luecke.me>
 */
interface QueryItemResolverInterface
{
    /**
     * @param object|null $item
     *
     * @return object
     */
    public function __invoke($item, array $context);
}
