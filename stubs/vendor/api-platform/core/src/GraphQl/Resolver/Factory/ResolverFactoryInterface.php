<?php

namespace ApiPlatform\GraphQl\Resolver\Factory;

/**
 * Builds a GraphQL resolver.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ResolverFactoryInterface
{
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?\ApiPlatform\Metadata\GraphQl\Operation $operation = null) : callable;
}
