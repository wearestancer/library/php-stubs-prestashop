<?php

namespace ApiPlatform\GraphQl\Resolver\Factory;

/**
 * Creates a function resolving a GraphQL mutation of an item.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ItemMutationResolverFactory implements \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Util\CloneTrait;
    public function __construct(\ApiPlatform\GraphQl\Resolver\Stage\ReadStageInterface $readStage, \ApiPlatform\GraphQl\Resolver\Stage\SecurityStageInterface $securityStage, \ApiPlatform\GraphQl\Resolver\Stage\SecurityPostDenormalizeStageInterface $securityPostDenormalizeStage, \ApiPlatform\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \ApiPlatform\GraphQl\Resolver\Stage\DeserializeStageInterface $deserializeStage, \ApiPlatform\GraphQl\Resolver\Stage\WriteStageInterface $writeStage, \ApiPlatform\GraphQl\Resolver\Stage\ValidateStageInterface $validateStage, \Psr\Container\ContainerInterface $mutationResolverLocator, \ApiPlatform\GraphQl\Resolver\Stage\SecurityPostValidationStageInterface $securityPostValidationStage)
    {
    }
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?\ApiPlatform\Metadata\GraphQl\Operation $operation = null) : callable
    {
    }
}
