<?php

namespace ApiPlatform\GraphQl\Resolver\Factory;

/**
 * Creates a function resolving a GraphQL subscription of an item.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ItemSubscriptionResolverFactory implements \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Util\CloneTrait;
    public function __construct(\ApiPlatform\GraphQl\Resolver\Stage\ReadStageInterface $readStage, \ApiPlatform\GraphQl\Resolver\Stage\SecurityStageInterface $securityStage, \ApiPlatform\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \ApiPlatform\GraphQl\Subscription\SubscriptionManagerInterface $subscriptionManager, ?\ApiPlatform\GraphQl\Subscription\MercureSubscriptionIriGeneratorInterface $mercureSubscriptionIriGenerator)
    {
    }
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?\ApiPlatform\Metadata\GraphQl\Operation $operation = null) : callable
    {
    }
}
