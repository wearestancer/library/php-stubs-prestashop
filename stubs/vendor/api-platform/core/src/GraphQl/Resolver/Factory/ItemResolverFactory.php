<?php

namespace ApiPlatform\GraphQl\Resolver\Factory;

/**
 * Creates a function retrieving an item to resolve a GraphQL query.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ItemResolverFactory implements \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Util\CloneTrait;
    public function __construct(\ApiPlatform\GraphQl\Resolver\Stage\ReadStageInterface $readStage, \ApiPlatform\GraphQl\Resolver\Stage\SecurityStageInterface $securityStage, \ApiPlatform\GraphQl\Resolver\Stage\SecurityPostDenormalizeStageInterface $securityPostDenormalizeStage, \ApiPlatform\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \Psr\Container\ContainerInterface $queryResolverLocator)
    {
    }
    public function __invoke(?string $resourceClass = null, ?string $rootClass = null, ?\ApiPlatform\Metadata\GraphQl\Operation $operation = null) : callable
    {
    }
}
