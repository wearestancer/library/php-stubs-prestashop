<?php

namespace ApiPlatform\GraphQl\Resolver;

/**
 * A field resolver that resolves IDs to IRIs and allow to access to the raw ID using the "#id" field.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ResourceFieldResolver
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct(\ApiPlatform\Api\IriConverterInterface $iriConverter)
    {
    }
    public function __invoke(?array $source, array $args, $context, \GraphQL\Type\Definition\ResolveInfo $info)
    {
    }
}
