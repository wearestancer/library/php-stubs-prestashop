<?php

namespace ApiPlatform\GraphQl\Resolver\Util;

/**
 * Identifier helper methods.
 *
 * @internal
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
trait IdentifierTrait
{
    private function getIdentifierFromContext(array $context) : ?string
    {
    }
}
