<?php

namespace ApiPlatform\GraphQl\Action;

/**
 * GraphQL API entrypoint.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class EntrypointAction
{
    public function __construct($schemaBuilder, \ApiPlatform\GraphQl\ExecutorInterface $executor, \ApiPlatform\GraphQl\Action\GraphiQlAction $graphiQlAction, \ApiPlatform\GraphQl\Action\GraphQlPlaygroundAction $graphQlPlaygroundAction, \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer, \ApiPlatform\GraphQl\Error\ErrorHandlerInterface $errorHandler, bool $debug = false, bool $graphiqlEnabled = false, bool $graphQlPlaygroundEnabled = false, $defaultIde = false)
    {
    }
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
