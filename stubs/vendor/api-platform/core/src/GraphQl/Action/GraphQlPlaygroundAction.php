<?php

namespace ApiPlatform\GraphQl\Action;

/**
 * GraphQL Playground entrypoint.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class GraphQlPlaygroundAction
{
    public function __construct(\Twig\Environment $twig, \Symfony\Component\Routing\RouterInterface $router, bool $graphQlPlaygroundEnabled = false, string $title = '', $assetPackage = null)
    {
    }
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
