<?php

namespace ApiPlatform\GraphQl\Action;

/**
 * GraphiQL entrypoint.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class GraphiQlAction
{
    public function __construct(\Twig\Environment $twig, \Symfony\Component\Routing\RouterInterface $router, bool $graphiqlEnabled = false, string $title = '', $assetPackage = null)
    {
    }
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
