<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Builds the GraphQL fields.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class FieldsBuilder implements \ApiPlatform\GraphQl\Type\FieldsBuilderInterface
{
    public function __construct(\ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\GraphQl\Type\TypesContainerInterface $typesContainer, \ApiPlatform\GraphQl\Type\TypeBuilderInterface $typeBuilder, \ApiPlatform\GraphQl\Type\TypeConverterInterface $typeConverter, \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface $itemResolverFactory, \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface $collectionResolverFactory, \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface $itemMutationResolverFactory, \ApiPlatform\GraphQl\Resolver\Factory\ResolverFactoryInterface $itemSubscriptionResolverFactory, \Psr\Container\ContainerInterface $filterLocator, \ApiPlatform\State\Pagination\Pagination $pagination, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter, string $nestingSeparator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNodeQueryFields() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemQueryFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $configuration) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCollectionQueryFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $configuration) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMutationFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSubscriptionFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceObjectTypeFields(?string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, bool $input, int $depth = 0, ?array $ioMetadata = null) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveResourceArgs(array $args, \ApiPlatform\Metadata\GraphQl\Operation $operation) : array
    {
    }
}
