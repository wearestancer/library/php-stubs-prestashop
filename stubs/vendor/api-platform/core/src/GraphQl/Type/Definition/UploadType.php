<?php

namespace ApiPlatform\GraphQl\Type\Definition;

trait UploadTypeParseLiteralTrait
{
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function parseLiteral(
        /* Node */
        $valueNode,
        array $variables = null
    )
    {
    }
}
/**
 * Represents an upload type.
 *
 * @author Mahmood Bazdar <mahmood@bazdar.me>
 */
final class UploadType extends \GraphQL\Type\Definition\ScalarType implements \ApiPlatform\GraphQl\Type\Definition\TypeInterface
{
    use \ApiPlatform\GraphQl\Type\Definition\UploadTypeParseLiteralTrait;
    public function __construct()
    {
    }
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function serialize($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function parseValue($value) : \Symfony\Component\HttpFoundation\File\UploadedFile
    {
    }
}
