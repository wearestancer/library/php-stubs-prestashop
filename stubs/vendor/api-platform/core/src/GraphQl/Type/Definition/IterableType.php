<?php

namespace ApiPlatform\GraphQl\Type\Definition;

trait IterableTypeParseLiteralTrait
{
    /**
     * {@inheritdoc}
     *
     * @param ObjectValueNode|ListValueNode|IntValueNode|FloatValueNode|StringValueNode|BooleanValueNode|NullValueNode $valueNode
     *
     * @return mixed
     */
    public function parseLiteral(
        /* Node */
        $valueNode,
        ?array $variables = null
    )
    {
    }
}
/**
 * Represents an iterable type.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class IterableType extends \GraphQL\Type\Definition\ScalarType implements \ApiPlatform\GraphQl\Type\Definition\TypeInterface
{
    use \ApiPlatform\GraphQl\Type\Definition\IterableTypeParseLiteralTrait;
    public function __construct()
    {
    }
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function serialize($value)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function parseValue($value)
    {
    }
}
