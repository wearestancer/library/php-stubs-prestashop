<?php

namespace ApiPlatform\GraphQl\Type\Definition;

/**
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface TypeInterface
{
    public function getName() : string;
}
