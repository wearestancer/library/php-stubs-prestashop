<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Get the GraphQL types.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface TypesFactoryInterface
{
    public function getTypes() : array;
}
