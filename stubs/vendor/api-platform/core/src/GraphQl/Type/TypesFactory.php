<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Get the registered services corresponding to GraphQL types.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypesFactory implements \ApiPlatform\GraphQl\Type\TypesFactoryInterface
{
    /**
     * @param string[] $typeIds
     */
    public function __construct(\Psr\Container\ContainerInterface $typeLocator, array $typeIds)
    {
    }
    public function getTypes() : array
    {
    }
}
