<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Container having the built GraphQL types.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypesContainer implements \ApiPlatform\GraphQl\Type\TypesContainerInterface
{
    /**
     * {@inheritdoc}
     */
    public function set(string $id, \GraphQL\Type\Definition\Type $type) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($id) : \GraphQL\Type\Definition\Type
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($id) : bool
    {
    }
}
