<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Converts a type to its GraphQL equivalent.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypeConverter implements \ApiPlatform\GraphQl\Type\TypeConverterInterface
{
    public function __construct(\ApiPlatform\GraphQl\Type\TypeBuilderInterface $typeBuilder, \ApiPlatform\GraphQl\Type\TypesContainerInterface $typesContainer, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function convertType(\Symfony\Component\PropertyInfo\Type $type, bool $input, \ApiPlatform\Metadata\GraphQl\Operation $rootOperation, string $resourceClass, string $rootResource, ?string $property, int $depth)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveType(string $type) : ?\GraphQL\Type\Definition\Type
    {
    }
}
