<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Interface implemented to build GraphQL fields.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface FieldsBuilderInterface
{
    /**
     * Gets the fields of a node for a query.
     */
    public function getNodeQueryFields() : array;
    /**
     * Gets the item query fields of the schema.
     */
    public function getItemQueryFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $configuration) : array;
    /**
     * Gets the collection query fields of the schema.
     */
    public function getCollectionQueryFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $configuration) : array;
    /**
     * Gets the mutation fields of the schema.
     */
    public function getMutationFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation) : array;
    /**
     * Gets the subscription fields of the schema.
     */
    public function getSubscriptionFields(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation) : array;
    /**
     * Gets the fields of the type of the given resource.
     */
    public function getResourceObjectTypeFields(?string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, bool $input, int $depth = 0, ?array $ioMetadata = null) : array;
    /**
     * Resolve the args of a resource by resolving its types.
     */
    public function resolveResourceArgs(array $args, \ApiPlatform\Metadata\GraphQl\Operation $operation) : array;
}
