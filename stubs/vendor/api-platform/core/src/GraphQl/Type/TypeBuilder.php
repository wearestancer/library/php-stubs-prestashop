<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Builds the GraphQL types.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypeBuilder implements \ApiPlatform\GraphQl\Type\TypeBuilderInterface
{
    public function __construct(\ApiPlatform\GraphQl\Type\TypesContainerInterface $typesContainer, callable $defaultFieldResolver, \Psr\Container\ContainerInterface $fieldsBuilderLocator, \ApiPlatform\State\Pagination\Pagination $pagination)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceObjectType(?string $resourceClass, \ApiPlatform\Metadata\Resource\ResourceMetadataCollection $resourceMetadataCollection, \ApiPlatform\Metadata\GraphQl\Operation $operation, bool $input, bool $wrapped = false, int $depth = 0) : \GraphQL\Type\Definition\Type
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNodeInterface() : \GraphQL\Type\Definition\InterfaceType
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourcePaginatedCollectionType(\GraphQL\Type\Definition\Type $resourceType, string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation) : \GraphQL\Type\Definition\Type
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isCollection(\Symfony\Component\PropertyInfo\Type $type) : bool
    {
    }
}
