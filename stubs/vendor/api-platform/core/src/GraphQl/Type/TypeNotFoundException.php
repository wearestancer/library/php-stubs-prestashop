<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Exception thrown when a type has not been found in the types container.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class TypeNotFoundException extends \InvalidArgumentException implements \Psr\Container\NotFoundExceptionInterface
{
    public function __construct(string $message, string $typeId)
    {
    }
    /**
     * Returns the type identifier causing this exception.
     */
    public function getTypeId() : string
    {
    }
}
