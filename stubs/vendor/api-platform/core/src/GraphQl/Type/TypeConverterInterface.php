<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Converts a type to its GraphQL equivalent.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface TypeConverterInterface
{
    /**
     * Converts a built-in type to its GraphQL equivalent.
     * A string can be returned for a custom registered type.
     *
     * @return string|GraphQLType|null
     */
    public function convertType(\Symfony\Component\PropertyInfo\Type $type, bool $input, \ApiPlatform\Metadata\GraphQl\Operation $rootOperation, string $resourceClass, string $rootResource, ?string $property, int $depth);
    /**
     * Resolves a type written with the GraphQL type system to its object representation.
     */
    public function resolveType(string $type) : ?\GraphQL\Type\Definition\Type;
}
