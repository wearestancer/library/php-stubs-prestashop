<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Builds the GraphQL schema.
 *
 * @author Raoul Clais <raoul.clais@gmail.com>
 * @author Alan Poulain <contact@alanpoulain.eu>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SchemaBuilder implements \ApiPlatform\GraphQl\Type\SchemaBuilderInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, \ApiPlatform\GraphQl\Type\TypesFactoryInterface $typesFactory, \ApiPlatform\GraphQl\Type\TypesContainerInterface $typesContainer, \ApiPlatform\GraphQl\Type\FieldsBuilderInterface $fieldsBuilder)
    {
    }
    public function getSchema() : \GraphQL\Type\Schema
    {
    }
}
