<?php

namespace ApiPlatform\GraphQl\Type;

/**
 * Interface implemented to contain the GraphQL types.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface TypesContainerInterface extends \Psr\Container\ContainerInterface
{
    /**
     * Sets a type.
     *
     * @param string      $id   The type identifier
     * @param GraphQLType $type The type instance
     */
    public function set(string $id, \GraphQL\Type\Definition\Type $type) : void;
    /**
     * Gets a type.
     *
     * @param string $id The type identifier
     *
     * @throws TypeNotFoundException When a type has not been found
     *
     * @return GraphQLType The type found in the container
     */
    public function get($id) : \GraphQL\Type\Definition\Type;
    /**
     * Gets all the types.
     *
     * @return array An array of types
     */
    public function all() : array;
    /**
     * Returns true if the given type is present in the container.
     *
     * @param string $id The type identifier
     *
     * @return bool true if the type is present, false otherwise
     */
    public function has($id) : bool;
}
