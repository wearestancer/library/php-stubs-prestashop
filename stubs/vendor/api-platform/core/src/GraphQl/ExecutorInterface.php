<?php

namespace ApiPlatform\GraphQl;

/**
 * Wrapper for the GraphQL facade.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface ExecutorInterface
{
    /**
     * @see http://webonyx.github.io/graphql-php/executing-queries/#using-facade-method
     *
     * @param mixed|null $rootValue
     * @param mixed|null $context
     * @param mixed      $source
     */
    public function executeQuery(\GraphQL\Type\Schema $schema, $source, $rootValue = null, $context = null, array $variableValues = null, string $operationName = null, callable $fieldResolver = null, array $validationRules = null) : \GraphQL\Executor\ExecutionResult;
}
