<?php

namespace ApiPlatform\GraphQl\Error;

/**
 * Handles the errors thrown by the GraphQL library.
 * It is responsible for applying the formatter to the errors and can be used for filtering or logging them.
 *
 * @author Ollie Harridge <code@oll.ie>
 */
interface ErrorHandlerInterface
{
    /**
     * @param Error[] $errors
     */
    public function __invoke(array $errors, callable $formatter) : array;
}
