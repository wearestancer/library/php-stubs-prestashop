<?php

namespace ApiPlatform\GraphQl\Error;

/**
 * Handles the errors thrown by the GraphQL library by applying the formatter to them (default behavior).
 *
 * @author Ollie Harridge <code@oll.ie>
 */
final class ErrorHandler implements \ApiPlatform\GraphQl\Error\ErrorHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(array $errors, callable $formatter) : array
    {
    }
}
