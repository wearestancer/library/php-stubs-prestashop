<?php

namespace ApiPlatform\GraphQl\Serializer;

/**
 * Decorates the output with GraphQL metadata when appropriate, but otherwise just
 * passes through to the decorated normalizer.
 */
final class ObjectNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public const FORMAT = 'graphql';
    public const ITEM_RESOURCE_CLASS_KEY = '#itemResourceClass';
    public const ITEM_IDENTIFIERS_KEY = '#itemIdentifiers';
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $decorated, $iriConverter, \ApiPlatform\Api\IdentifiersExtractorInterface $identifiersExtractor)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws UnexpectedValueException
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
}
