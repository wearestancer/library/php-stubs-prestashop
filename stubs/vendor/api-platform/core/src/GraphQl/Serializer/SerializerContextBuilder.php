<?php

namespace ApiPlatform\GraphQl\Serializer;

/**
 * Builds the context used by the Symfony Serializer.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SerializerContextBuilder implements \ApiPlatform\GraphQl\Serializer\SerializerContextBuilderInterface
{
    public function __construct(?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter)
    {
    }
    public function create(?string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $resolverContext, bool $normalization) : array
    {
    }
}
