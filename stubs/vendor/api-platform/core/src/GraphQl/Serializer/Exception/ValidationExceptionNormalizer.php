<?php

namespace ApiPlatform\GraphQl\Serializer\Exception;

/**
 * Normalize validation exceptions.
 *
 * @author Mahmood Bazdar <mahmood@bazdar.me>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ValidationExceptionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    public function __construct(array $exceptionToStatus = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
}
