<?php

namespace ApiPlatform\GraphQl\Serializer\Exception;

/**
 * Normalize runtime exceptions to have the right message in production mode.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class RuntimeExceptionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
}
