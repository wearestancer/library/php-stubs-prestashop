<?php

namespace ApiPlatform\GraphQl\Serializer\Exception;

/**
 * Normalize HTTP exceptions.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class HttpExceptionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
}
