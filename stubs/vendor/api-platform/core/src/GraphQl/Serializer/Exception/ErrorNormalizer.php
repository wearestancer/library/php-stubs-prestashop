<?php

namespace ApiPlatform\GraphQl\Serializer\Exception;

/**
 * Normalize GraphQL error (fallback).
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ErrorNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
}
