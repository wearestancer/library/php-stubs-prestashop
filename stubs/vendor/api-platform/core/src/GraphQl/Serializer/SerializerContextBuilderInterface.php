<?php

namespace ApiPlatform\GraphQl\Serializer;

/**
 * Builds the context used by the Symfony Serializer.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SerializerContextBuilderInterface
{
    public function create(string $resourceClass, \ApiPlatform\Metadata\GraphQl\Operation $operation, array $resolverContext, bool $normalization) : array;
}
