<?php

namespace ApiPlatform\GraphQl\Subscription;

/**
 * Generates Mercure-related IRIs from a subscription ID.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface MercureSubscriptionIriGeneratorInterface
{
    public function generateTopicIri(string $subscriptionId) : string;
    public function generateMercureUrl(string $subscriptionId, ?string $hub = null) : string;
}
