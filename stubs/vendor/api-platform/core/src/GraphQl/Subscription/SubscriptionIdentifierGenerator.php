<?php

namespace ApiPlatform\GraphQl\Subscription;

/**
 * Generates an identifier used to identify a subscription.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SubscriptionIdentifierGenerator implements \ApiPlatform\GraphQl\Subscription\SubscriptionIdentifierGeneratorInterface
{
    public function generateSubscriptionIdentifier(array $fields) : string
    {
    }
}
