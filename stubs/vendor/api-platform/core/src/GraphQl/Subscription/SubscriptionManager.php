<?php

namespace ApiPlatform\GraphQl\Subscription;

/**
 * Manages all the queried subscriptions by creating their ID
 * and saving to a cache the information needed to publish updated data.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class SubscriptionManager implements \ApiPlatform\GraphQl\Subscription\SubscriptionManagerInterface
{
    use \ApiPlatform\GraphQl\Resolver\Util\IdentifierTrait;
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    use \ApiPlatform\Util\SortTrait;
    public function __construct(\Psr\Cache\CacheItemPoolInterface $subscriptionsCache, \ApiPlatform\GraphQl\Subscription\SubscriptionIdentifierGeneratorInterface $subscriptionIdentifierGenerator, \ApiPlatform\GraphQl\Resolver\Stage\SerializeStageInterface $serializeStage, \ApiPlatform\Api\IriConverterInterface $iriConverter, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory)
    {
    }
    public function retrieveSubscriptionId(array $context, ?array $result) : ?string
    {
    }
    /**
     * @param object $object
     */
    public function getPushPayloads($object) : array
    {
    }
}
