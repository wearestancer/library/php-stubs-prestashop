<?php

namespace ApiPlatform\GraphQl\Subscription;

/**
 * Generates an identifier used to identify a subscription.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
interface SubscriptionIdentifierGeneratorInterface
{
    public function generateSubscriptionIdentifier(array $fields) : string;
}
