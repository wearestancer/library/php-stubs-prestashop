<?php

namespace ApiPlatform\Util;

/**
 * Parses and creates IRIs.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @internal
 */
final class IriHelper
{
    /**
     * Parses and standardizes the request IRI.
     *
     * @throws InvalidArgumentException
     */
    public static function parseIri(string $iri, string $pageParameterName) : array
    {
    }
    /**
     * Gets a collection IRI for the given parameters.
     *
     * @param float $page
     * @param mixed $urlGenerationStrategy
     */
    public static function createIri(array $parts, array $parameters, string $pageParameterName = null, float $page = null, $urlGenerationStrategy = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH) : string
    {
    }
}
