<?php

namespace ApiPlatform\Util;

/**
 * Clones given data if cloneable.
 *
 * @internal
 *
 * @author Quentin Barloy <quentin.barloy@gmail.com>
 */
trait CloneTrait
{
    public function clone($data)
    {
    }
}
