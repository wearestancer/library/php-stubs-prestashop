<?php

namespace ApiPlatform\Util;

/**
 * @internal
 */
trait OperationRequestInitiatorTrait
{
    /**
     * @var ResourceMetadataCollectionFactoryInterface|null
     */
    private $resourceMetadataCollectionFactory;
    /**
     * TODO: Kernel terminate remove the _api_operation attribute?
     */
    private function initializeOperation(\Symfony\Component\HttpFoundation\Request $request) : ?\ApiPlatform\Metadata\HttpOperation
    {
    }
}
