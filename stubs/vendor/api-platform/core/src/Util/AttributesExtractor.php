<?php

namespace ApiPlatform\Util;

/**
 * Extracts data used by the library form given attributes.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @internal
 */
final class AttributesExtractor
{
    /**
     * Extracts resource class, operation name and format request attributes. Returns an empty array if the request does
     * not contain required attributes.
     */
    public static function extractAttributes(array $attributes) : array
    {
    }
}
