<?php

namespace ApiPlatform\Util;

/**
 * CORS utils.
 *
 * To be removed when https://github.com/symfony/symfony/pull/34391 wil be merged.
 *
 * @internal
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait CorsTrait
{
    public function isPreflightRequest(\Symfony\Component\HttpFoundation\Request $request) : bool
    {
    }
}
