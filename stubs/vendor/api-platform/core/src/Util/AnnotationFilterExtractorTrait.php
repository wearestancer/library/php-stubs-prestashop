<?php

namespace ApiPlatform\Util;

/**
 * Generates a service id for a generic filter.
 *
 * @internal
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
trait AnnotationFilterExtractorTrait
{
    /**
     * Filters annotations to get back only ApiFilter annotations.
     *
     * @param \ReflectionClass|\ReflectionProperty $reflector
     *
     * @return \Iterator only ApiFilter annotations
     */
    private function getFilterAnnotations(\Reflector $reflector, ?\Doctrine\Common\Annotations\Reader $reader = null) : \Iterator
    {
    }
    /**
     * Given a filter annotation and reflection elements, find out the properties where the filter is applied.
     *
     * @param ApiFilter|ApiFilterMetadata $filterAnnotation
     */
    private function getFilterProperties($filterAnnotation, \ReflectionClass $reflectionClass, \ReflectionProperty $reflectionProperty = null) : array
    {
    }
    /**
     * Reads filter annotations from a ReflectionClass.
     *
     * @return array Key is the filter id. It has two values, properties and the ApiFilter instance
     */
    private function readFilterAnnotations(\ReflectionClass $reflectionClass, \Doctrine\Common\Annotations\Reader $reader = null) : array
    {
    }
    /**
     * Generates a unique, per-class and per-filter identifier prefixed by `annotated_`.
     *
     * @param \ReflectionClass $reflectionClass the reflection class of a Resource
     * @param string           $filterClass     the filter class
     * @param string|null      $filterId        the filter id
     */
    private function generateFilterId(\ReflectionClass $reflectionClass, string $filterClass, string $filterId = null) : string
    {
    }
}
