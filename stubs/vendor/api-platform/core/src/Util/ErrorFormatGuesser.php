<?php

namespace ApiPlatform\Util;

/**
 * Guesses the error format to use.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ErrorFormatGuesser
{
    /**
     * Get the error format and its associated MIME type.
     */
    public static function guessErrorFormat(\Symfony\Component\HttpFoundation\Request $request, array $errorFormats) : array
    {
    }
}
