<?php

namespace ApiPlatform\Util;

/**
 * Gets reflection classes for php files in the given directories.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @internal
 */
final class ReflectionClassRecursiveIterator
{
    public static function getReflectionClassesFromDirectories(array $directories) : \Iterator
    {
    }
}
