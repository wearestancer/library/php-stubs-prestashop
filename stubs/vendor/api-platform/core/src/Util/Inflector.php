<?php

namespace ApiPlatform\Util;

/**
 * Facade for Doctrine Inflector.
 *
 * This class allows us to maintain compatibility with Doctrine Inflector 1.3 and 2.0 at the same time.
 *
 * @internal
 */
final class Inflector
{
    /**
     * @see InflectorObject::tableize()
     */
    public static function tableize(string $word) : string
    {
    }
    /**
     * @see InflectorObject::pluralize()
     */
    public static function pluralize(string $word) : string
    {
    }
}
