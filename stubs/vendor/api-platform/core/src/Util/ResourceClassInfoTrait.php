<?php

namespace ApiPlatform\Util;

/**
 * Retrieves information about a resource class.
 *
 * @internal
 */
trait ResourceClassInfoTrait
{
    use \ApiPlatform\Util\ClassInfoTrait;
    /**
     * @var ResourceClassResolverInterface|null
     */
    private $resourceClassResolver;
    /**
     * @var ResourceMetadataFactoryInterface|ResourceMetadataCollectionFactoryInterface|null
     */
    private $resourceMetadataFactory;
    /**
     * Gets the resource class of the given object.
     *
     * @param object $object
     * @param bool   $strict If true, object class is expected to be a resource class
     *
     * @return string|null The resource class, or null if object class is not a resource class
     */
    private function getResourceClass($object, bool $strict = false) : ?string
    {
    }
    private function isResourceClass(string $class) : bool
    {
    }
}
