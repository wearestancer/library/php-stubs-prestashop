<?php

namespace ApiPlatform\Util;

/**
 * Reflection utilities.
 *
 * @internal
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class Reflection
{
    public const ACCESSOR_PREFIXES = ['get', 'is', 'has', 'can'];
    public const MUTATOR_PREFIXES = ['set', 'add', 'remove'];
    /**
     * Gets the property name associated with an accessor method.
     */
    public function getProperty(string $methodName) : ?string
    {
    }
}
