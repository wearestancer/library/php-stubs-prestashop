<?php

namespace ApiPlatform\Util;

/**
 * Retrieves information about a class.
 *
 * @internal
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait ClassInfoTrait
{
    /**
     * Get class name of the given object.
     *
     * @param object $object
     */
    private function getObjectClass($object) : string
    {
    }
    /**
     * Get the real class name of a class name that could be a proxy.
     */
    private function getRealClassName(string $className) : string
    {
    }
}
