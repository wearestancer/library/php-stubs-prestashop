<?php

namespace ApiPlatform\Util;

/**
 * Utility functions for working with Symfony's HttpFoundation request.
 *
 * @internal
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class RequestParser
{
    /**
     * Parses request parameters from the specified source.
     *
     * @author Rok Kralj
     *
     * @see https://stackoverflow.com/a/18209799/1529493
     */
    public static function parseRequestParams(string $source) : array
    {
    }
    /**
     * Generates the normalized query string for the Request.
     *
     * It builds a normalized query string, where keys/value pairs are alphabetized
     * and have consistent escaping.
     */
    public static function getQueryString(\Symfony\Component\HttpFoundation\Request $request) : ?string
    {
    }
}
