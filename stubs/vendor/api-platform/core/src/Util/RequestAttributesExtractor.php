<?php

namespace ApiPlatform\Util;

/**
 * Extracts data used by the library form a Request instance.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class RequestAttributesExtractor
{
    /**
     * Extracts resource class, operation name and format request attributes. Returns an empty array if the request does
     * not contain required attributes.
     */
    public static function extractAttributes(\Symfony\Component\HttpFoundation\Request $request) : array
    {
    }
}
