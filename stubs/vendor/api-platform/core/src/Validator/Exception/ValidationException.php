<?php

namespace ApiPlatform\Validator\Exception;

/**
 * Thrown when a validation error occurs.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ValidationException extends \ApiPlatform\Exception\RuntimeException
{
}
