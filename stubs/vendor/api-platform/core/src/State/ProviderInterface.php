<?php

namespace ApiPlatform\State;

/**
 * Retrieves data from a persistence layer.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @template T of object
 */
interface ProviderInterface
{
    /**
     * Provides data.
     *
     * @return T|PartialPaginatorInterface<T>|iterable<T>|null
     */
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = []);
}
