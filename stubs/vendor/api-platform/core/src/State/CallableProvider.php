<?php

namespace ApiPlatform\State;

final class CallableProvider implements \ApiPlatform\State\ProviderInterface
{
    public function __construct(\Psr\Container\ContainerInterface $locator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
