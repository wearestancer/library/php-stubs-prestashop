<?php

namespace ApiPlatform\State;

/**
 * Process data: send an email, persist to storage, add to queue etc.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface ProcessorInterface
{
    /**
     * Handle the state.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function process($data, \ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = []);
}
