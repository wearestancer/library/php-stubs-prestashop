<?php

namespace ApiPlatform\State\Pagination;

/**
 * Paginator for arrays.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class ArrayPaginator implements \IteratorAggregate, \ApiPlatform\State\Pagination\PaginatorInterface
{
    public function __construct(array $results, int $firstResult, int $maxResults)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLastPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTotalItems() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
}
