<?php

namespace ApiPlatform\State\Pagination;

/**
 * Partial Paginator Interface.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 *
 * @template T of object
 *
 * @extends \Traversable<T>
 */
interface PartialPaginatorInterface extends \Traversable, \Countable
{
    /**
     * Gets the current page number.
     */
    public function getCurrentPage() : float;
    /**
     * Gets the number of items by page.
     */
    public function getItemsPerPage() : float;
}
