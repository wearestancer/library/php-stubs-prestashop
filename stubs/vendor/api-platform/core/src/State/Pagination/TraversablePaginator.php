<?php

namespace ApiPlatform\State\Pagination;

final class TraversablePaginator implements \IteratorAggregate, \ApiPlatform\State\Pagination\PaginatorInterface
{
    public function __construct(\Traversable $iterator, float $currentPage, float $itemsPerPage, float $totalItems)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLastPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTotalItems() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
}
