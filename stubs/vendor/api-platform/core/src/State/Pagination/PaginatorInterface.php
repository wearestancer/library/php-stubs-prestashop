<?php

namespace ApiPlatform\State\Pagination;

/**
 * The \Countable implementation should return the number of items on the
 * current page, as an integer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @template T of object
 *
 * @extends PartialPaginatorInterface<T>
 */
interface PaginatorInterface extends \ApiPlatform\State\Pagination\PartialPaginatorInterface
{
    /**
     * Gets last page.
     */
    public function getLastPage() : float;
    /**
     * Gets the number of items in the whole collection.
     */
    public function getTotalItems() : float;
}
