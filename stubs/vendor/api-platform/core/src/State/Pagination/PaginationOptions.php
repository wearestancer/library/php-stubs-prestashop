<?php

namespace ApiPlatform\State\Pagination;

final class PaginationOptions
{
    public function __construct(bool $paginationEnabled = true, string $paginationPageParameterName = 'page', bool $clientItemsPerPage = false, string $itemsPerPageParameterName = 'itemsPerPage', bool $paginationClientEnabled = false, string $paginationClientEnabledParameterName = 'pagination', int $itemsPerPage = 30, int $maximumItemsPerPage = null, bool $partialPaginationEnabled = false, bool $clientPartialPaginationEnabled = false, string $partialPaginationParameterName = 'partial')
    {
    }
    public function isPaginationEnabled() : bool
    {
    }
    public function getPaginationPageParameterName() : string
    {
    }
    public function getClientItemsPerPage() : bool
    {
    }
    public function getItemsPerPageParameterName() : string
    {
    }
    public function getPaginationClientEnabled() : bool
    {
    }
    public function isPaginationClientEnabled() : bool
    {
    }
    public function getPaginationClientEnabledParameterName() : string
    {
    }
    public function getItemsPerPage() : int
    {
    }
    public function getMaximumItemsPerPage() : ?int
    {
    }
    public function isPartialPaginationEnabled() : bool
    {
    }
    public function isClientPartialPaginationEnabled() : bool
    {
    }
    public function getPartialPaginationParameterName() : string
    {
    }
}
