<?php

namespace ApiPlatform\State\Pagination;

/**
 * Pagination configuration.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class Pagination
{
    public function __construct(array $options = [], array $graphQlOptions = [])
    {
    }
    /**
     * Gets the current page.
     *
     * @throws InvalidArgumentException
     */
    public function getPage(array $context = []) : int
    {
    }
    /**
     * Gets the current offset.
     */
    public function getOffset(\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : int
    {
    }
    /**
     * Gets the current limit.
     *
     * @throws InvalidArgumentException
     */
    public function getLimit(\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : int
    {
    }
    /**
     * Gets info about the pagination.
     *
     * Returns an array with the following info as values:
     *   - the page {@see Pagination::getPage()}
     *   - the offset {@see Pagination::getOffset()}
     *   - the limit {@see Pagination::getLimit()}
     *
     * @throws InvalidArgumentException
     */
    public function getPagination(\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array
    {
    }
    /**
     * Is the pagination enabled?
     */
    public function isEnabled(\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool
    {
    }
    /**
     * Is the pagination enabled for GraphQL?
     */
    public function isGraphQlEnabled(?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool
    {
    }
    /**
     * Is the partial pagination enabled?
     */
    public function isPartialEnabled(\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : bool
    {
    }
    public function getOptions() : array
    {
    }
    public function getGraphQlPaginationType(\ApiPlatform\Metadata\Operation $operation) : string
    {
    }
}
