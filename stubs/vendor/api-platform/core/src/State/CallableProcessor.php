<?php

namespace ApiPlatform\State;

final class CallableProcessor implements \ApiPlatform\State\ProcessorInterface
{
    public function __construct(\Psr\Container\ContainerInterface $locator)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function process($data, \ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
