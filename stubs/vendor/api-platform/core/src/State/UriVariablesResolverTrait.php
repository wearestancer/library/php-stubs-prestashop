<?php

namespace ApiPlatform\State;

trait UriVariablesResolverTrait
{
    /** @var ContextAwareIdentifierConverterInterface|IdentifierConverterInterface|UriVariablesConverterInterface|null */
    private $uriVariablesConverter = null;
    /**
     * Resolves an operation's UriVariables to their identifiers values.
     */
    private function getOperationUriVariables(?\ApiPlatform\Metadata\HttpOperation $operation = null, array $parameters = [], ?string $resourceClass = null) : array
    {
    }
}
