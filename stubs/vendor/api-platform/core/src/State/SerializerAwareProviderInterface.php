<?php

namespace ApiPlatform\State;

/**
 * Injects serializer in providers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface SerializerAwareProviderInterface
{
    public function setSerializerLocator(\Psr\Container\ContainerInterface $serializerLocator);
}
