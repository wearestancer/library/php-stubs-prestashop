<?php

namespace ApiPlatform\State;

/**
 * Injects serializer in providers.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
trait SerializerAwareProviderTrait
{
    /**
     * @internal
     *
     * @var ContainerInterface
     */
    private $serializerLocator;
    public function setSerializerLocator(\Psr\Container\ContainerInterface $serializerLocator) : void
    {
    }
    private function getSerializer() : \Symfony\Component\Serializer\SerializerInterface
    {
    }
}
