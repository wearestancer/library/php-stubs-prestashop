<?php

namespace ApiPlatform\RamseyUuid\UriVariableTransformer;

/**
 * Transforms an UUID string to an instance of Ramsey\Uuid.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class UuidUriVariableTransformer implements \ApiPlatform\Api\UriVariableTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value, array $types, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($value, array $types, array $context = []) : bool
    {
    }
}
