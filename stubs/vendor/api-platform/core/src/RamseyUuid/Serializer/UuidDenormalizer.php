<?php

namespace ApiPlatform\RamseyUuid\Serializer;

final class UuidDenormalizer implements \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
{
    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return mixed
     */
    public function denormalize($data, $type, $format = null, array $context = [])
    {
    }
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
}
