<?php

namespace ApiPlatform\JsonApi\Serializer;

/**
 * Converts {@see \Exception} or {@see FlattenException} or {@see LegacyFlattenException}  to a JSON API error representation.
 *
 * @author Héctor Hurtarte <hectorh30@gmail.com>
 */
final class ErrorNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Problem\Serializer\ErrorNormalizerTrait;
    public const FORMAT = 'jsonapi';
    public const TITLE = 'title';
    public function __construct(bool $debug = false, array $defaultContext = [])
    {
    }
    /**
     * @param mixed      $object
     * @param mixed|null $format
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
