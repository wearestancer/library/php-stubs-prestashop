<?php

namespace ApiPlatform\JsonApi\Serializer;

/**
 * Reserved attribute name converter.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ReservedAttributeNameConverter implements \Symfony\Component\Serializer\NameConverter\AdvancedNameConverterInterface
{
    public const JSON_API_RESERVED_ATTRIBUTES = ['id' => '_id', 'type' => '_type', 'links' => '_links', 'relationships' => '_relationships', 'included' => '_included'];
    public function __construct(\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($propertyName, string $class = null, string $format = null, array $context = []) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function denormalize($propertyName, string $class = null, string $format = null, array $context = []) : string
    {
    }
}
