<?php

namespace ApiPlatform\JsonApi\Serializer;

/**
 * Converts between objects and array.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ItemNormalizer extends \ApiPlatform\Serializer\AbstractItemNormalizer
{
    use \ApiPlatform\Serializer\CacheKeyTrait;
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Serializer\ContextTrait;
    public const FORMAT = 'jsonapi';
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, $propertyMetadataFactory, $iriConverter, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, ?\Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter, $resourceMetadataFactory, array $defaultContext = [], iterable $dataTransformers = [], \ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws NotNormalizableValueException
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
}
