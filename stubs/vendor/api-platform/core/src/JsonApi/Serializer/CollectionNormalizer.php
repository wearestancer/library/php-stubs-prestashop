<?php

namespace ApiPlatform\JsonApi\Serializer;

/**
 * Normalizes collections in the JSON API format.
 *
 * @author Kevin Dunglas <dunglas@gmail.com>
 * @author Hamza Amrouche <hamza@les-tilleuls.coop>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class CollectionNormalizer extends \ApiPlatform\Serializer\AbstractCollectionNormalizer
{
    public const FORMAT = 'jsonapi';
    public function __construct(\ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, string $pageParameterName, $resourceMetadataFactory)
    {
    }
}
