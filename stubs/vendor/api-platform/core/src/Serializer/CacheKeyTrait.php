<?php

namespace ApiPlatform\Serializer;

/**
 * Used to override Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer::getCacheKey which is private
 * We need the cache_key in JsonApi and Hal before it is computed in Symfony.
 *
 * @see https://github.com/symfony/symfony/blob/49b6ab853d81e941736a1af67845efa3401e7278/src/Symfony/Component/Serializer/Normalizer/AbstractObjectNormalizer.php#L723 which isn't protected
 */
trait CacheKeyTrait
{
    /**
     * @return string|bool
     */
    private function getCacheKey(?string $format, array $context)
    {
    }
}
