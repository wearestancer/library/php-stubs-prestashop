<?php

namespace ApiPlatform\Serializer;

trait InputOutputMetadataTrait
{
    /**
     * @var ResourceMetadataCollectionFactoryInterface|null
     */
    protected $resourceMetadataCollectionFactory;
    protected function getInputClass(string $class, array $context = []) : ?string
    {
    }
    protected function getOutputClass(string $class, array $context = []) : ?string
    {
    }
}
