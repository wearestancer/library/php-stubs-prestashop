<?php

namespace ApiPlatform\Serializer;

/**
 * Base item normalizer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class AbstractItemNormalizer extends \Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer
{
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Serializer\ContextTrait;
    use \ApiPlatform\Serializer\InputOutputMetadataTrait;
    public const IS_TRANSFORMED_TO_SAME_CLASS = 'is_transformed_to_same_class';
    /**
     * @var PropertyNameCollectionFactoryInterface
     */
    protected $propertyNameCollectionFactory;
    /**
     * @var LegacyPropertyMetadataFactoryInterface|PropertyMetadataFactoryInterface
     */
    protected $propertyMetadataFactory;
    protected $resourceMetadataFactory;
    /**
     * @var LegacyIriConverterInterface|IriConverterInterface
     */
    protected $iriConverter;
    protected $resourceClassResolver;
    protected $resourceAccessChecker;
    protected $propertyAccessor;
    protected $itemDataProvider;
    protected $allowPlainIdentifiers;
    protected $dataTransformers = [];
    protected $localCache = [];
    public function __construct(\ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, $propertyMetadataFactory, $iriConverter, $resourceClassResolver, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider = null, bool $allowPlainIdentifiers = false, array $defaultContext = [], iterable $dataTransformers = [], $resourceMetadataFactory = null, \ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
    /**
     * Method copy-pasted from symfony/serializer.
     * Remove it after symfony/serializer version update @see https://github.com/symfony/symfony/pull/28263.
     *
     * {@inheritdoc}
     *
     * @internal
     *
     * @return object
     */
    protected function instantiateObject(array &$data, $class, array &$context, \ReflectionClass $reflectionClass, $allowedAttributes, string $format = null)
    {
    }
    protected function getClassDiscriminatorResolvedClass(array &$data, string $class) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function createConstructorArgument($parameterData, string $key, \ReflectionParameter $constructorParameter, array &$context, string $format = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * Unused in this context.
     *
     * @return string[]
     */
    protected function extractAttributes($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|bool
     */
    protected function getAllowedAttributes($classOrObject, array $context, $attributesAsString = false)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    protected function isAllowedAttribute($classOrObject, $attribute, $format = null, array $context = [])
    {
    }
    /**
     * Check if access to the attribute is granted.
     *
     * @param object $object
     */
    protected function canAccessAttribute($object, string $attribute, array $context = []) : bool
    {
    }
    /**
     * Check if access to the attribute is granted.
     *
     * @param object      $object
     * @param object|null $previousObject
     */
    protected function canAccessAttributePostDenormalize($object, $previousObject, string $attribute, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function setAttributeValue($object, $attribute, $value, $format = null, array $context = [])
    {
    }
    /**
     * Validates the type of the value. Allows using integers as floats for JSON formats.
     *
     * @param mixed $value
     *
     * @throws InvalidArgumentException
     */
    protected function validateType(string $attribute, \Symfony\Component\PropertyInfo\Type $type, $value, string $format = null)
    {
    }
    /**
     * Denormalizes a collection of objects.
     *
     * @param ApiProperty|PropertyMetadata $propertyMetadata
     * @param mixed                        $value
     *
     * @throws InvalidArgumentException
     */
    protected function denormalizeCollection(string $attribute, $propertyMetadata, \Symfony\Component\PropertyInfo\Type $type, string $className, $value, ?string $format, array $context) : array
    {
    }
    /**
     * Denormalizes a relation.
     *
     * @param ApiProperty|PropertyMetadata $propertyMetadata
     * @param mixed                        $value
     *
     * @throws LogicException
     * @throws UnexpectedValueException
     * @throws ItemNotFoundException
     *
     * @return object|null
     */
    protected function denormalizeRelation(string $attributeName, $propertyMetadata, string $className, $value, ?string $format, array $context)
    {
    }
    /**
     * Gets the options for the property name collection / property metadata factories.
     */
    protected function getFactoryOptions(array $context) : array
    {
    }
    /**
     * Creates the context to use when serializing a relation.
     *
     * @deprecated since version 2.1, to be removed in 3.0.
     */
    protected function createRelationSerializationContext(string $resourceClass, array $context) : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws UnexpectedValueException
     * @throws LogicException
     *
     * @return mixed
     */
    protected function getAttributeValue($object, $attribute, $format = null, array $context = [])
    {
    }
    /**
     * Normalizes a collection of relations (to-many).
     *
     * @param ApiProperty|PropertyMetadata $propertyMetadata
     * @param iterable                     $attributeValue
     *
     * @throws UnexpectedValueException
     */
    protected function normalizeCollectionOfRelations($propertyMetadata, $attributeValue, string $resourceClass, ?string $format, array $context) : array
    {
    }
    /**
     * Normalizes a relation.
     *
     * @param ApiProperty|PropertyMetadata $propertyMetadata
     * @param object|null                  $relatedObject
     *
     * @throws LogicException
     * @throws UnexpectedValueException
     *
     * @return string|array|\ArrayObject|null IRI or normalized object data
     */
    protected function normalizeRelation($propertyMetadata, $relatedObject, string $resourceClass, ?string $format, array $context)
    {
    }
    /**
     * Finds the first supported data transformer if any.
     *
     * @param object|array $data object on normalize / array on denormalize
     */
    protected function getDataTransformer($data, string $to, array $context = []) : ?\ApiPlatform\Core\DataTransformer\DataTransformerInterface
    {
    }
    /**
     * For a given resource, it returns an output representation if any
     * If not, the resource is returned.
     *
     * @param mixed $object
     */
    protected function transformOutput($object, array $context = [], string $outputClass = null)
    {
    }
}
