<?php

namespace ApiPlatform\Serializer;

/**
 * {@inheritdoc}
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class SerializerFilterContextBuilder implements \ApiPlatform\Serializer\SerializerContextBuilderInterface
{
    public function __construct($resourceMetadataFactory, \Psr\Container\ContainerInterface $filterLocator, \ApiPlatform\Serializer\SerializerContextBuilderInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createFromRequest(\Symfony\Component\HttpFoundation\Request $request, bool $normalization, array $attributes = null) : array
    {
    }
}
