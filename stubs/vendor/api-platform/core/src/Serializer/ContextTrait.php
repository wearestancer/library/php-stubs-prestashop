<?php

namespace ApiPlatform\Serializer;

/**
 * Creates and manipulates the Serializer context.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait ContextTrait
{
    /**
     * Initializes the context.
     */
    private function initContext(string $resourceClass, array $context) : array
    {
    }
}
