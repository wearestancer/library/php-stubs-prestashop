<?php

namespace ApiPlatform\Serializer\Filter;

/**
 * Symfony serializer context builder filter interface.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface FilterInterface extends \ApiPlatform\Api\FilterInterface
{
    /**
     * Apply a filter to the serializer context.
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, bool $normalization, array $attributes, array &$context);
}
