<?php

namespace ApiPlatform\Serializer\Filter;

/**
 * Property filter.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class PropertyFilter implements \ApiPlatform\Serializer\Filter\FilterInterface
{
    public function __construct(string $parameterName = 'properties', bool $overrideDefaultProperties = false, array $whitelist = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, bool $normalization, array $attributes, array &$context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
}
