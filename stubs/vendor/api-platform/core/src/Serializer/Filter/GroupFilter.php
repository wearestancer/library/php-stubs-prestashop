<?php

namespace ApiPlatform\Serializer\Filter;

/**
 * Group filter.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class GroupFilter implements \ApiPlatform\Serializer\Filter\FilterInterface
{
    public function __construct(string $parameterName = 'groups', bool $overrideDefaultGroups = false, array $whitelist = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(\Symfony\Component\HttpFoundation\Request $request, bool $normalization, array $attributes, array &$context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
}
