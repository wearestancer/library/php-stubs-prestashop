<?php

namespace ApiPlatform\Serializer;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SerializerContextBuilder implements \ApiPlatform\Serializer\SerializerContextBuilderInterface
{
    public function __construct($resourceMetadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createFromRequest(\Symfony\Component\HttpFoundation\Request $request, bool $normalization, array $attributes = null) : array
    {
    }
}
