<?php

namespace ApiPlatform\Serializer;

/**
 * Generic item normalizer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ItemNormalizer extends \ApiPlatform\Serializer\AbstractItemNormalizer
{
    /**
     * @param mixed                                             $propertyMetadataFactory
     * @param LegacyIriConverterInterface|IriConverterInterface $iriConverter
     * @param mixed                                             $resourceClassResolver
     * @param mixed|null                                        $resourceMetadataFactory
     */
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, $propertyMetadataFactory, $iriConverter, $resourceClassResolver, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \ApiPlatform\Core\DataProvider\ItemDataProviderInterface $itemDataProvider = null, bool $allowPlainIdentifiers = false, \Psr\Log\LoggerInterface $logger = null, iterable $dataTransformers = [], $resourceMetadataFactory = null, \ApiPlatform\Symfony\Security\ResourceAccessCheckerInterface $resourceAccessChecker = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws NotNormalizableValueException
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
}
