<?php

namespace ApiPlatform\Serializer\Mapping\Factory;

final class ClassMetadataFactory implements \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($value) : \Symfony\Component\Serializer\Mapping\ClassMetadataInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($value) : bool
    {
    }
}
