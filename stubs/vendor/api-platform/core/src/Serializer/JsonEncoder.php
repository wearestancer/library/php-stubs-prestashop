<?php

namespace ApiPlatform\Serializer;

/**
 * A JSON encoder with appropriate default options to embed the generated document into HTML.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class JsonEncoder implements \Symfony\Component\Serializer\Encoder\EncoderInterface, \Symfony\Component\Serializer\Encoder\DecoderInterface
{
    public function __construct(string $format, \Symfony\Component\Serializer\Encoder\JsonEncoder $jsonEncoder = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = []) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function decode($data, $format, array $context = [])
    {
    }
}
