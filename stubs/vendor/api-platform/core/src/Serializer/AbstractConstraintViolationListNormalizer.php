<?php

namespace ApiPlatform\Serializer;

/**
 * Common features regarding Constraint Violation normalization.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @internal
 */
abstract class AbstractConstraintViolationListNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT = null;
    public function __construct(array $serializePayloadFields = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    protected function getMessagesAndViolations(\Symfony\Component\Validator\ConstraintViolationListInterface $constraintViolationList) : array
    {
    }
}
