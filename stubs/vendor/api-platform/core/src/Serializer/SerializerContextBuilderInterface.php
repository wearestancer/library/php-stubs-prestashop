<?php

namespace ApiPlatform\Serializer;

/**
 * Builds the context used by the Symfony Serializer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface SerializerContextBuilderInterface
{
    /**
     * Creates a serialization context from a Request.
     *
     * @throws RuntimeException
     */
    public function createFromRequest(\Symfony\Component\HttpFoundation\Request $request, bool $normalization, array $extractedAttributes = null) : array;
}
