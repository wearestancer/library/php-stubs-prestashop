<?php

namespace ApiPlatform\Serializer;

/**
 * Base collection normalizer.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
abstract class AbstractCollectionNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Serializer\ContextTrait {
        initContext as protected;
    }
    use \Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
    /**
     * This constant must be overridden in the child class.
     */
    public const FORMAT = 'to-override';
    protected $resourceClassResolver;
    protected $pageParameterName;
    /**
     * @var ResourceMetadataCollectionFactoryInterface|ResourceMetadataFactoryInterface
     */
    protected $resourceMetadataFactory;
    public function __construct(\ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, string $pageParameterName, $resourceMetadataFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param iterable $object
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * Normalizes a raw collection (not API resources).
     *
     * @param string|null $format
     * @param mixed       $object
     */
    protected function normalizeRawCollection($object, $format = null, array $context = []) : array
    {
    }
    /**
     * Gets the pagination configuration.
     *
     * @param iterable $object
     */
    protected function getPaginationConfig($object, array $context = []) : array
    {
    }
    /**
     * Gets the pagination data.
     *
     * @param iterable $object
     */
    protected abstract function getPaginationData($object, array $context = []) : array;
    /**
     * Gets items data.
     *
     * @param iterable $object
     */
    protected abstract function getItemsData($object, string $format = null, array $context = []) : array;
}
