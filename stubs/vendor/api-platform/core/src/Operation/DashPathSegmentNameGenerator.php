<?php

namespace ApiPlatform\Operation;

/**
 * Generate a path name with a dash separator according to a string and whether it's a collection or not.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class DashPathSegmentNameGenerator implements \ApiPlatform\Operation\PathSegmentNameGeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function getSegmentName(string $name, bool $collection = true) : string
    {
    }
}
