<?php

namespace ApiPlatform\Operation;

/**
 * Generate a path name with an underscore separator according to a string and whether it's a collection or not.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class UnderscorePathSegmentNameGenerator implements \ApiPlatform\Operation\PathSegmentNameGeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function getSegmentName(string $name, bool $collection = true) : string
    {
    }
}
