<?php

namespace ApiPlatform\Elasticsearch;

/**
 * Paginator for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class Paginator implements \IteratorAggregate, \ApiPlatform\State\Pagination\PaginatorInterface
{
    public function __construct(\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer, array $documents, string $resourceClass, int $limit, int $offset, array $denormalizationContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLastPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTotalItems() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrentPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage() : float
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getIterator() : \Traversable
    {
    }
}
