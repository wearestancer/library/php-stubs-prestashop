<?php

namespace ApiPlatform\Elasticsearch\Util;

/**
 * Field datatypes helpers.
 *
 * @internal
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
trait FieldDatatypeTrait
{
    /**
     * @var PropertyMetadataFactoryInterface|LegacyPropertyMetadataFactoryInterface
     */
    private $propertyMetadataFactory;
    /**
     * @var ResourceClassResolverInterface
     */
    private $resourceClassResolver;
    /**
     * Is the decomposed given property of the given resource class potentially mapped as a nested field in Elasticsearch?
     */
    private function isNestedField(string $resourceClass, string $property) : bool
    {
    }
    /**
     * Get the nested path to the decomposed given property (e.g.: foo.bar.baz => foo.bar).
     */
    private function getNestedFieldPath(string $resourceClass, string $property) : ?string
    {
    }
}
