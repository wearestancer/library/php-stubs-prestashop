<?php

namespace ApiPlatform\Elasticsearch\Util;

class ElasticsearchVersion
{
    public const REGEX_PATTERN = '/\\d(.*)/';
    /**
     * Detect whether the current ES version supports passing mapping type as a search parameter.
     *
     * @see https://www.elastic.co/guide/en/elasticsearch/reference/7.17/removal-of-types.html#_schedule_for_removal_of_mapping_types
     */
    public static function supportsMappingType(string $version = \Elasticsearch\Client::VERSION) : bool
    {
    }
}
