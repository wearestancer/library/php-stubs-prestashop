<?php

namespace ApiPlatform\Elasticsearch\Metadata\Document\Factory;

/**
 * Creates document's metadata using indices from the cat APIs.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-indices.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class CatDocumentMetadataFactory implements \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface
{
    /**
     * @param ResourceMetadataFactoryInterface|ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory
     */
    public function __construct(\Elasticsearch\Client $client, $resourceMetadataFactory, ?\ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Elasticsearch\Metadata\Document\DocumentMetadata
    {
    }
}
