<?php

namespace ApiPlatform\Elasticsearch\Metadata\Document;

/**
 * Document metadata.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-fields.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class DocumentMetadata
{
    public const DEFAULT_TYPE = '_doc';
    public function __construct(?string $index = null, string $type = self::DEFAULT_TYPE)
    {
    }
    /**
     * Gets a new instance with the given index.
     */
    public function withIndex(string $index) : self
    {
    }
    /**
     * Gets the document index.
     */
    public function getIndex() : ?string
    {
    }
    /**
     * Gets a new instance with the given type.
     */
    public function withType(string $type) : self
    {
    }
    /**
     * Gets the document type.
     */
    public function getType() : string
    {
    }
}
