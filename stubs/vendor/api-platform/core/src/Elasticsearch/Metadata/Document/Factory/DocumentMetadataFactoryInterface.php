<?php

namespace ApiPlatform\Elasticsearch\Metadata\Document\Factory;

/**
 * Creates a document metadata value object.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface DocumentMetadataFactoryInterface
{
    /**
     * Creates document metadata.
     *
     * @throws IndexNotFoundException
     */
    public function create(string $resourceClass) : \ApiPlatform\Elasticsearch\Metadata\Document\DocumentMetadata;
}
