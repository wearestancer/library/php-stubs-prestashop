<?php

namespace ApiPlatform\Elasticsearch\Metadata\Document\Factory;

/**
 * Caches document metadata.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class CachedDocumentMetadataFactory implements \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface
{
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Elasticsearch\Metadata\Document\DocumentMetadata
    {
    }
}
