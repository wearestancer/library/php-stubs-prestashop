<?php

namespace ApiPlatform\Elasticsearch\Metadata\Document\Factory;

/**
 * Creates document's metadata using the mapping configuration.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ConfiguredDocumentMetadataFactory implements \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface
{
    public function __construct(array $mapping, ?\ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Elasticsearch\Metadata\Document\DocumentMetadata
    {
    }
}
