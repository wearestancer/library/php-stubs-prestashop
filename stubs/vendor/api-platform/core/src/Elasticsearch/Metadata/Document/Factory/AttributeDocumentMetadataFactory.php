<?php

namespace ApiPlatform\Elasticsearch\Metadata\Document\Factory;

/**
 * Creates document's metadata using the attribute configuration.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class AttributeDocumentMetadataFactory implements \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface
{
    /**
     * @param ResourceMetadataFactoryInterface|ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory
     */
    public function __construct($resourceMetadataFactory, ?\ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Elasticsearch\Metadata\Document\DocumentMetadata
    {
    }
}
