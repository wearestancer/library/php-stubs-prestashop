<?php

namespace ApiPlatform\Elasticsearch\Metadata\Resource\Factory;

final class ElasticsearchProviderResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    public function __construct(\Elasticsearch\Client $client, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
