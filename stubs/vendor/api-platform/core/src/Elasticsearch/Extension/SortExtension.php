<?php

namespace ApiPlatform\Elasticsearch\Extension;

/**
 * Applies selected sorting while querying resource collection.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class SortExtension implements \ApiPlatform\Elasticsearch\Extension\RequestBodySearchCollectionExtensionInterface
{
    use \ApiPlatform\Elasticsearch\Util\FieldDatatypeTrait;
    public function __construct($propertyMetadataFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, ?string $defaultDirection = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(array $requestBody, string $resourceClass, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array
    {
    }
}
