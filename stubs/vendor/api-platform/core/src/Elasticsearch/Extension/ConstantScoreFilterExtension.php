<?php

namespace ApiPlatform\Elasticsearch\Extension;

/**
 * Applies filter clauses while executing a constant score query.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-constant-score-query.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class ConstantScoreFilterExtension extends \ApiPlatform\Elasticsearch\Extension\AbstractFilterExtension
{
}
