<?php

namespace ApiPlatform\Elasticsearch\Extension;

/**
 * Abstract class for easing the implementation of a filter extension.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
abstract class AbstractFilterExtension implements \ApiPlatform\Elasticsearch\Extension\RequestBodySearchCollectionExtensionInterface
{
    public function __construct(\Psr\Container\ContainerInterface $filterLocator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(array $requestBody, string $resourceClass, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array
    {
    }
    /**
     * Gets the related filter interface.
     */
    protected abstract function getFilterInterface() : string;
    /**
     * Alters the request body.
     */
    protected abstract function alterRequestBody(array $requestBody, array $clauseBody) : array;
}
