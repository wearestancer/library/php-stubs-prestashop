<?php

namespace ApiPlatform\Elasticsearch\Serializer;

/**
 * Item normalizer decorator that prevents {@see \ApiPlatform\Serializer\ItemNormalizer}
 * from taking over for the {@see DocumentNormalizer::FORMAT} format because of priorities.
 *
 * @experimental
 */
final class ItemNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface, \Symfony\Component\Serializer\SerializerAwareInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT = 'elasticsearch';
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     *
     * @return mixed
     */
    public function denormalize($data, $type, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     */
    public function setSerializer(\Symfony\Component\Serializer\SerializerInterface $serializer)
    {
    }
}
