<?php

namespace ApiPlatform\Elasticsearch\Serializer\NameConverter;

/**
 * Converts inner fields with a decorated name converter.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class InnerFieldsNameConverter implements \Symfony\Component\Serializer\NameConverter\AdvancedNameConverterInterface
{
    public function __construct(?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($propertyName, string $class = null, string $format = null, array $context = []) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function denormalize($propertyName, string $class = null, string $format = null, array $context = []) : string
    {
    }
}
