<?php

namespace ApiPlatform\Elasticsearch\Filter;

/**
 * Order the collection by given properties.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class OrderFilter extends \ApiPlatform\Elasticsearch\Filter\AbstractFilter implements \ApiPlatform\Elasticsearch\Filter\SortFilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(\ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, string $orderParameterName = 'order', ?array $properties = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function apply(array $clauseBody, string $resourceClass, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass) : array
    {
    }
}
