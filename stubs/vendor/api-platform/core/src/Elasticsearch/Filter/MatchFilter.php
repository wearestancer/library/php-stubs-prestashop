<?php

namespace ApiPlatform\Elasticsearch\Filter;

/**
 * Filter the collection by given properties using a full text query.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class MatchFilter extends \ApiPlatform\Elasticsearch\Filter\AbstractSearchFilter
{
}
