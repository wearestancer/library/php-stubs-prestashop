<?php

namespace ApiPlatform\Elasticsearch\Filter;

/**
 * Elasticsearch filter interface for a constant score query.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface ConstantScoreFilterInterface extends \ApiPlatform\Elasticsearch\Filter\FilterInterface
{
}
