<?php

namespace ApiPlatform\Elasticsearch\Filter;

/**
 * Elasticsearch filter interface.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
interface FilterInterface extends \ApiPlatform\Api\FilterInterface
{
    public function apply(array $clauseBody, string $resourceClass, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array;
}
