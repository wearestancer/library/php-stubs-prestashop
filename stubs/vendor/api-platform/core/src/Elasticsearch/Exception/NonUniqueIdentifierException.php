<?php

namespace ApiPlatform\Elasticsearch\Exception;

/**
 * Non unique identifier exception.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 */
final class NonUniqueIdentifierException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
