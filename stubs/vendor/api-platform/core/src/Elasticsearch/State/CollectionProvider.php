<?php

namespace ApiPlatform\Elasticsearch\State;

/**
 * Collection provider for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class CollectionProvider implements \ApiPlatform\State\ProviderInterface
{
    /**
     * @param RequestBodySearchCollectionExtensionInterface[] $collectionExtensions
     */
    public function __construct(\Elasticsearch\Client $client, \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $documentMetadataFactory, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer, \ApiPlatform\State\Pagination\Pagination $pagination, iterable $collectionExtensions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
