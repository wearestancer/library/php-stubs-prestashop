<?php

namespace ApiPlatform\Elasticsearch\State;

/**
 * Item provider for Elasticsearch.
 *
 * @experimental
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ItemProvider implements \ApiPlatform\State\ProviderInterface
{
    public function __construct(\Elasticsearch\Client $client, \ApiPlatform\Elasticsearch\Metadata\Document\Factory\DocumentMetadataFactoryInterface $documentMetadataFactory, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function provide(\ApiPlatform\Metadata\Operation $operation, array $uriVariables = [], array $context = [])
    {
    }
}
