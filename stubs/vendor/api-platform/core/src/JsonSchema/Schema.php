<?php

namespace ApiPlatform\JsonSchema;

/**
 * Represents a JSON Schema document.
 *
 * Both the standard version and the OpenAPI flavors (v2 and v3) are supported.
 *
 * @see https://json-schema.org/latest/json-schema-core.html
 * @see https://github.com/OAI/OpenAPI-Specification
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class Schema extends \ArrayObject
{
    public const TYPE_INPUT = 'input';
    public const TYPE_OUTPUT = 'output';
    public const VERSION_JSON_SCHEMA = 'json-schema';
    public const VERSION_OPENAPI = 'openapi';
    public const VERSION_SWAGGER = 'swagger';
    public function __construct(string $version = self::VERSION_JSON_SCHEMA)
    {
    }
    /**
     * The flavor used for this document: JSON Schema, OpenAPI v2 or OpenAPI v3.
     */
    public function getVersion() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param bool $includeDefinitions if set to false, definitions will not be included in the resulting array
     */
    public function getArrayCopy(bool $includeDefinitions = true) : array
    {
    }
    /**
     * Retrieves the definitions used by this schema.
     */
    public function getDefinitions() : \ArrayObject
    {
    }
    /**
     * Associates existing definitions to this schema.
     */
    public function setDefinitions(\ArrayObject $definitions) : void
    {
    }
    /**
     * Returns the name of the root definition, if defined.
     */
    public function getRootDefinitionKey() : ?string
    {
    }
    /**
     * Returns the name of the items definition, if defined.
     */
    public function getItemsDefinitionKey() : ?string
    {
    }
    /**
     * Checks if this schema is initialized.
     */
    public function isDefined() : bool
    {
    }
}
