<?php

namespace ApiPlatform\JsonSchema;

/**
 * Factory for creating the JSON Schema document corresponding to a PHP class.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface SchemaFactoryInterface
{
    /**
     * Builds the JSON Schema document corresponding to the given PHP class.
     */
    public function buildSchema(string $className, string $format = 'json', string $type = \ApiPlatform\JsonSchema\Schema::TYPE_OUTPUT, ?\ApiPlatform\Metadata\Operation $operation = null, ?\ApiPlatform\JsonSchema\Schema $schema = null, ?array $serializerContext = null, bool $forceCollection = false) : \ApiPlatform\JsonSchema\Schema;
}
