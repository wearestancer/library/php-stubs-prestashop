<?php

namespace ApiPlatform\JsonSchema;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class TypeFactory implements \ApiPlatform\JsonSchema\TypeFactoryInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * SchemaFactoryInterface|LegacySchemaFactoryInterface.
     *
     * @param mixed $schemaFactory
     */
    public function setSchemaFactory($schemaFactory) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getType(\Symfony\Component\PropertyInfo\Type $type, string $format = 'json', ?bool $readableLink = null, ?array $serializerContext = null, \ApiPlatform\JsonSchema\Schema $schema = null) : array
    {
    }
}
