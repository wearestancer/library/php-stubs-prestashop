<?php

namespace ApiPlatform\JsonSchema;

/**
 * Factory for creating the JSON Schema document which specifies the data type corresponding to a PHP type.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface TypeFactoryInterface
{
    /**
     * Gets the JSON Schema document which specifies the data type corresponding to the given PHP type, and recursively adds needed new schema to the current schema if provided.
     */
    public function getType(\Symfony\Component\PropertyInfo\Type $type, string $format = 'json', ?bool $readableLink = null, ?array $serializerContext = null, \ApiPlatform\JsonSchema\Schema $schema = null) : array;
}
