<?php

namespace ApiPlatform\JsonSchema\Command;

/**
 * Generates a resource JSON Schema.
 *
 * @author Jacques Lefebvre <jacques@les-tilleuls.coop>
 */
final class JsonSchemaGenerateCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct($schemaFactory, array $formats)
    {
    }
    public static function getDefaultName() : string
    {
    }
}
