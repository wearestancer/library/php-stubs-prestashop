<?php

namespace ApiPlatform\JsonSchema;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class SchemaFactory implements \ApiPlatform\JsonSchema\SchemaFactoryInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\ApiPlatform\JsonSchema\TypeFactoryInterface $typeFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory, \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * When added to the list, the given format will lead to the creation of a new definition.
     *
     * @internal
     */
    public function addDistinctFormat(string $format) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildSchema(string $className, string $format = 'json', string $type = \ApiPlatform\JsonSchema\Schema::TYPE_OUTPUT, ?\ApiPlatform\Metadata\Operation $operation = null, ?\ApiPlatform\JsonSchema\Schema $schema = null, ?array $serializerContext = null, bool $forceCollection = false) : \ApiPlatform\JsonSchema\Schema
    {
    }
}
