<?php

namespace ApiPlatform\Test;

/**
 * @internal
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
abstract class DoctrineMongoDbOdmFilterTestCase extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    /**
     * @var DocumentManager
     */
    protected $manager;
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;
    /**
     * @var DocumentRepository
     */
    protected $repository;
    /**
     * @var string
     */
    protected $resourceClass;
    /**
     * @var string
     */
    protected $filterClass;
    protected function setUp() : void
    {
    }
    /**
     * @dataProvider provideApplyTestData
     */
    public function testApply(?array $properties, array $filterParameters, array $expectedPipeline, callable $factory = null, string $resourceClass = null)
    {
    }
    protected function doTestApply(?array $properties, array $filterParameters, array $expectedPipeline, callable $filterFactory = null, string $resourceClass = null)
    {
    }
    protected function buildFilter(?array $properties = null)
    {
    }
    public abstract function provideApplyTestData() : array;
}
