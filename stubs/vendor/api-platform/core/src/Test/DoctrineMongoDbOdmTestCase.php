<?php

namespace ApiPlatform\Test;

/**
 * Source: https://github.com/doctrine/DoctrineMongoDBBundle/blob/0174003844bc566bb4cb3b7d10c5528d1924d719/Tests/TestCase.php
 * Test got excluded from vendor in 4.x.
 */
class DoctrineMongoDbOdmTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @param mixed $paths
     *
     * @return DocumentManager
     */
    public static function createTestDocumentManager($paths = [])
    {
    }
}
