<?php

namespace ApiPlatform\Test;

/**
 * @internal
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class DoctrineOrmFilterTestCase extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    protected $managerRegistry;
    protected $repository;
    protected $resourceClass = \ApiPlatform\Tests\Fixtures\TestBundle\Entity\Dummy::class;
    protected $alias = 'o';
    protected $filterClass;
    protected function setUp() : void
    {
    }
    /**
     * @dataProvider provideApplyTestData
     */
    public function testApply(?array $properties, array $filterParameters, string $expectedDql, array $expectedParameters = null, callable $factory = null, string $resourceClass = null) : void
    {
    }
    protected function doTestApply(?array $properties, array $filterParameters, string $expectedDql, array $expectedParameters = null, callable $filterFactory = null, string $resourceClass = null) : void
    {
    }
    protected function buildFilter(?array $properties = null)
    {
    }
    public abstract function provideApplyTestData() : array;
}
