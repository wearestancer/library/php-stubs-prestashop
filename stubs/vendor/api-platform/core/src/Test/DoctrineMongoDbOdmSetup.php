<?php

namespace ApiPlatform\Test;

/**
 * Convenience class for setting up Doctrine from different installations and configurations.
 *
 * @internal
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
class DoctrineMongoDbOdmSetup
{
    /**
     * Creates a configuration with an annotation metadata driver.
     */
    public static function createAnnotationMetadataConfiguration(array $paths, bool $isDevMode = false, string $proxyDir = null, string $hydratorDir = null, \Doctrine\Common\Cache\Cache $cache = null) : \Doctrine\ODM\MongoDB\Configuration
    {
    }
    /**
     * Creates a configuration with a xml metadata driver.
     */
    public static function createXMLMetadataConfiguration(array $paths, bool $isDevMode = false, string $proxyDir = null, string $hydratorDir = null, \Doctrine\Common\Cache\Cache $cache = null) : \Doctrine\ODM\MongoDB\Configuration
    {
    }
    /**
     * Creates a configuration without a metadata driver.
     */
    public static function createConfiguration(bool $isDevMode = false, string $proxyDir = null, string $hydratorDir = null, \Doctrine\Common\Cache\Cache $cache = null) : \Doctrine\ODM\MongoDB\Configuration
    {
    }
}
