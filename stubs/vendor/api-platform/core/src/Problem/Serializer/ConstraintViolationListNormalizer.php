<?php

namespace ApiPlatform\Problem\Serializer;

/**
 * Converts {@see \Symfony\Component\Validator\ConstraintViolationListInterface} the API Problem spec (RFC 7807).
 *
 * @see https://tools.ietf.org/html/rfc7807
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ConstraintViolationListNormalizer extends \ApiPlatform\Serializer\AbstractConstraintViolationListNormalizer
{
    public const FORMAT = 'jsonproblem';
    public const TYPE = 'type';
    public const TITLE = 'title';
    public function __construct(array $serializePayloadFields = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
}
