<?php

namespace ApiPlatform\Problem\Serializer;

/**
 * Normalizes errors according to the API Problem spec (RFC 7807).
 *
 * @see https://tools.ietf.org/html/rfc7807
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ErrorNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \ApiPlatform\Problem\Serializer\ErrorNormalizerTrait;
    public const FORMAT = 'jsonproblem';
    public const TYPE = 'type';
    public const TITLE = 'title';
    public function __construct(bool $debug = false, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
}
