<?php

namespace ApiPlatform\Exception;

/**
 * Identifier is not valid exception.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class InvalidIdentifierException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
