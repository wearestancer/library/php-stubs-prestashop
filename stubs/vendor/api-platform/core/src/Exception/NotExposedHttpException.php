<?php

namespace ApiPlatform\Exception;

/**
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
class NotExposedHttpException extends \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
{
}
