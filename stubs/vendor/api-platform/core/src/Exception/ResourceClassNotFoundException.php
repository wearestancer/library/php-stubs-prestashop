<?php

namespace ApiPlatform\Exception;

/**
 * Resource class not found exception.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ResourceClassNotFoundException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
