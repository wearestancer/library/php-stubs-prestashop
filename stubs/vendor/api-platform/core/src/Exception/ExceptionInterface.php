<?php

namespace ApiPlatform\Exception;

/**
 * Base exception interface.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ExceptionInterface extends \Throwable
{
}
