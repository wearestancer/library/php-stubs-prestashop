<?php

namespace ApiPlatform\Exception;

/**
 * Resource class not supported exception.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ResourceClassNotSupportedException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
