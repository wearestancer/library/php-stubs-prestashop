<?php

namespace ApiPlatform\Exception;

/**
 * Runtime exception.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \ApiPlatform\Exception\ExceptionInterface
{
}
