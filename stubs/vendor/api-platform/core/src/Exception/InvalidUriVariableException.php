<?php

namespace ApiPlatform\Exception;

/**
 * Identifier is not valid exception.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class InvalidUriVariableException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
