<?php

namespace ApiPlatform\Exception;

/**
 * Operation not found exception.
 */
class OperationNotFoundException extends \InvalidArgumentException implements \ApiPlatform\Exception\ExceptionInterface
{
}
