<?php

namespace ApiPlatform\Exception;

/**
 * Deserialization exception.
 *
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class DeserializationException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface, \Symfony\Component\Serializer\Exception\ExceptionInterface
{
}
