<?php

namespace ApiPlatform\Exception;

/**
 * Property not found exception.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class PropertyNotFoundException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
