<?php

namespace ApiPlatform\Exception;

/**
 * Invalid resource exception.
 *
 * @author Paul Le Corre <paul@lecorre.me>
 */
class InvalidResourceException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
}
