<?php

namespace ApiPlatform\Exception;

/**
 * Item not found exception.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
class ItemNotFoundException extends \ApiPlatform\Exception\InvalidArgumentException
{
}
