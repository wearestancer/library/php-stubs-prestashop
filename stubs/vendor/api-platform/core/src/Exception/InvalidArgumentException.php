<?php

namespace ApiPlatform\Exception;

/**
 * Invalid argument exception.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \ApiPlatform\Exception\ExceptionInterface
{
}
