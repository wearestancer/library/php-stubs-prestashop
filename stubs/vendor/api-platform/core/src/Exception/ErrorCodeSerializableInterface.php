<?php

namespace ApiPlatform\Exception;

/**
 * An exception which has a serializable application-specific error code.
 */
interface ErrorCodeSerializableInterface
{
    /**
     * Gets the application-specific error code.
     */
    public static function getErrorCode() : string;
}
