<?php

namespace ApiPlatform\Exception;

class InvalidValueException extends \ApiPlatform\Exception\InvalidArgumentException
{
}
