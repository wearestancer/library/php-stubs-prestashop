<?php

namespace ApiPlatform\Exception;

/**
 * Filter validation exception.
 *
 * @author Julien DENIAU <julien.deniau@gmail.com>
 */
final class FilterValidationException extends \Exception implements \ApiPlatform\Exception\ExceptionInterface
{
    public function __construct(array $constraintViolationList, string $message = '', int $code = 0, \Exception $previous = null)
    {
    }
    public function __toString() : string
    {
    }
}
