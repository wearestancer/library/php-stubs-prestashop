<?php

namespace ApiPlatform\Documentation\Action;

/**
 * Generates the API documentation.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
final class DocumentationAction
{
    /**
     * @param int[]                                                 $swaggerVersions
     * @param mixed|array|FormatsProviderInterface                  $formatsProvider
     * @param LegacyOpenApiFactoryInterface|OpenApiFactoryInterface $openApiFactory
     */
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, string $title = '', string $description = '', string $version = '', $formatsProvider = null, array $swaggerVersions = [2, 3], $openApiFactory = null)
    {
    }
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request = null) : \ApiPlatform\Documentation\DocumentationInterface
    {
    }
}
