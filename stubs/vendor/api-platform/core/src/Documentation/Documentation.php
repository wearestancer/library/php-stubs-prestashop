<?php

namespace ApiPlatform\Documentation;

/**
 * Generates the API documentation.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
final class Documentation implements \ApiPlatform\Documentation\DocumentationInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\ResourceNameCollection $resourceNameCollection, string $title = '', string $description = '', string $version = '', array $formats = null)
    {
    }
    public function getMimeTypes() : array
    {
    }
    public function getVersion() : string
    {
    }
    public function getDescription() : string
    {
    }
    public function getTitle() : string
    {
    }
    public function getResourceNameCollection() : \ApiPlatform\Metadata\Resource\ResourceNameCollection
    {
    }
}
