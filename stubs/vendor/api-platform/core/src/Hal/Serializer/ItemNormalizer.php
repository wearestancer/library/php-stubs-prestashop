<?php

namespace ApiPlatform\Hal\Serializer;

/**
 * Converts between objects and array including HAL metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ItemNormalizer extends \ApiPlatform\Serializer\AbstractItemNormalizer
{
    use \ApiPlatform\Serializer\CacheKeyTrait;
    use \ApiPlatform\Util\ClassInfoTrait;
    use \ApiPlatform\Serializer\ContextTrait;
    public const FORMAT = 'jsonhal';
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
}
