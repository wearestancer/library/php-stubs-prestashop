<?php

namespace ApiPlatform\Hal\Serializer;

/**
 * Normalizes collections in the HAL format.
 *
 * @author Kevin Dunglas <dunglas@gmail.com>
 * @author Hamza Amrouche <hamza@les-tilleuls.coop>
 */
final class CollectionNormalizer extends \ApiPlatform\Serializer\AbstractCollectionNormalizer
{
    public const FORMAT = 'jsonhal';
    public function __construct(\ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, string $pageParameterName, $resourceMetadataFactory)
    {
    }
}
