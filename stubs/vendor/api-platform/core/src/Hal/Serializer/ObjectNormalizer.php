<?php

namespace ApiPlatform\Hal\Serializer;

/**
 * Decorates the output with JSON HAL metadata when appropriate, but otherwise
 * just passes through to the decorated normalizer.
 */
final class ObjectNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT = 'jsonhal';
    public function __construct(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $decorated, $iriConverter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws LogicException
     *
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    }
}
