<?php

namespace ApiPlatform\Hal\JsonSchema;

/**
 * Decorator factory which adds HAL properties to the JSON Schema document.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Jachim Coudenys <jachimcoudenys@gmail.com>
 */
final class SchemaFactory implements \ApiPlatform\JsonSchema\SchemaFactoryInterface
{
    public function __construct(\ApiPlatform\JsonSchema\SchemaFactoryInterface $schemaFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildSchema(string $className, string $format = 'jsonhal', string $type = \ApiPlatform\JsonSchema\Schema::TYPE_OUTPUT, ?\ApiPlatform\Metadata\Operation $operation = null, ?\ApiPlatform\JsonSchema\Schema $schema = null, ?array $serializerContext = null, bool $forceCollection = false) : \ApiPlatform\JsonSchema\Schema
    {
    }
    public function addDistinctFormat(string $format) : void
    {
    }
}
