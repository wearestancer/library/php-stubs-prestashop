<?php

namespace ApiPlatform\PathResolver;

/**
 * Resolves the custom operations path.
 *
 * @author Guilhem N. <egetick@gmail.com>
 */
final class CustomOperationPathResolver implements \ApiPlatform\PathResolver\OperationPathResolverInterface
{
    public function __construct(\ApiPlatform\PathResolver\OperationPathResolverInterface $deferred)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveOperationPath(string $resourceShortName, array $operation, $operationType) : string
    {
    }
}
