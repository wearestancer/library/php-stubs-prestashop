<?php

namespace ApiPlatform\PathResolver;

/**
 * Generates an operation path.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class OperationPathResolver implements \ApiPlatform\PathResolver\OperationPathResolverInterface
{
    public function __construct(\ApiPlatform\Operation\PathSegmentNameGeneratorInterface $pathSegmentNameGenerator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveOperationPath(string $resourceShortName, array $operation, $operationType) : string
    {
    }
}
