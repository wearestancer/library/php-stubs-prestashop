<?php

namespace ApiPlatform\Api;

/**
 * Guesses which resource is associated with a given object.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ResourceClassResolverInterface
{
    /**
     * Guesses the associated resource.
     *
     * @param string $resourceClass The expected resource class
     * @param bool   $strict        If true, value must match the expected resource class
     * @param mixed  $value
     *
     * @throws InvalidArgumentException
     */
    public function getResourceClass($value, string $resourceClass = null, bool $strict = false) : string;
    /**
     * Is the given class a resource class?
     */
    public function isResourceClass(string $type) : bool;
}
