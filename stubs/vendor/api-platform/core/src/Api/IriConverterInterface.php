<?php

namespace ApiPlatform\Api;

/**
 * Converts item and resources to IRI and vice versa.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface IriConverterInterface
{
    /**
     * Retrieves an item from its IRI.
     *
     * @throws InvalidArgumentException
     * @throws ItemNotFoundException
     *
     * @return object
     */
    public function getResourceFromIri(string $iri, array $context = [], ?\ApiPlatform\Metadata\Operation $operation = null);
    /**
     * Gets the IRI associated with the given item.
     *
     * @param object|class-string $resource
     *
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function getIriFromResource($resource, int $referenceType = \ApiPlatform\Api\UrlGeneratorInterface::ABS_PATH, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : ?string;
}
