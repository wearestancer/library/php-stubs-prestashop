<?php

namespace ApiPlatform\Api;

/**
 * Extracts identifiers for a given Resource according to the retrieved Metadata.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface IdentifiersExtractorInterface
{
    /**
     * Finds identifiers from an Item (object).
     *
     * @param object $item
     *
     * @throws RuntimeException
     */
    public function getIdentifiersFromItem($item, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array;
}
