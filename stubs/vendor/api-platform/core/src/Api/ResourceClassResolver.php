<?php

namespace ApiPlatform\Api;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Samuel ROZE <samuel.roze@gmail.com>
 */
final class ResourceClassResolver implements \ApiPlatform\Api\ResourceClassResolverInterface
{
    use \ApiPlatform\Util\ClassInfoTrait;
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceClass($value, string $resourceClass = null, bool $strict = false) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isResourceClass(string $type) : bool
    {
    }
}
