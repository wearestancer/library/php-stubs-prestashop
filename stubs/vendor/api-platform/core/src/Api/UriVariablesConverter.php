<?php

namespace ApiPlatform\Api;

/**
 * UriVariables converter that chains uri variables transformers.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class UriVariablesConverter implements \ApiPlatform\Api\UriVariablesConverterInterface
{
    /**
     * @param iterable<UriVariableTransformerInterface> $uriVariableTransformers
     * @param mixed                                     $propertyMetadataFactory
     */
    public function __construct($propertyMetadataFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, iterable $uriVariableTransformers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function convert(array $uriVariables, string $class, array $context = []) : array
    {
    }
}
