<?php

namespace ApiPlatform\Api;

/**
 * The first path you will see in the API.
 *
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
final class Entrypoint
{
    public function __construct(\ApiPlatform\Metadata\Resource\ResourceNameCollection $resourceNameCollection)
    {
    }
    public function getResourceNameCollection() : \ApiPlatform\Metadata\Resource\ResourceNameCollection
    {
    }
}
