<?php

namespace ApiPlatform\Api;

/**
 * {@inheritdoc}
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class IdentifiersExtractor implements \ApiPlatform\Api\IdentifiersExtractorInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver, \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * TODO: 3.0 identifiers should be stringable?
     */
    public function getIdentifiersFromItem($item, \ApiPlatform\Metadata\Operation $operation = null, array $context = []) : array
    {
    }
}
