<?php

namespace ApiPlatform\Api;

/**
 * Matches a mime type to a format.
 *
 * @internal
 */
final class FormatMatcher
{
    /**
     * @param array<string, string[]|string> $formats
     */
    public function __construct(array $formats)
    {
    }
    /**
     * Gets the format associated with the mime type.
     *
     * Adapted from {@see \Symfony\Component\HttpFoundation\Request::getFormat}.
     */
    public function getFormat(string $mimeType) : ?string
    {
    }
}
