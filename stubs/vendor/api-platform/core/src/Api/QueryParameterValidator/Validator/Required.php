<?php

namespace ApiPlatform\Api\QueryParameterValidator\Validator;

final class Required implements \ApiPlatform\Api\QueryParameterValidator\Validator\ValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate(string $name, array $filterDescription, array $queryParameters) : array
    {
    }
}
