<?php

namespace ApiPlatform\Api\QueryParameterValidator;

/**
 * Validates query parameters depending on filter description.
 *
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
class QueryParameterValidator
{
    use \ApiPlatform\Api\FilterLocatorTrait;
    public function __construct(\Psr\Container\ContainerInterface $filterLocator)
    {
    }
    public function validateFilters(string $resourceClass, array $resourceFilters, array $queryParameters) : void
    {
    }
}
