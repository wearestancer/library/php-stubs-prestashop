<?php

namespace ApiPlatform\Api;

/**
 * Manipulates filters with a backward compatibility between the new filter locator and the deprecated filter collection.
 *
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 *
 * @internal
 */
trait FilterLocatorTrait
{
    /** @var ContainerInterface */
    private $filterLocator;
    /**
     * Sets a filter locator with a backward compatibility.
     *
     * @param ContainerInterface|null $filterLocator
     */
    private function setFilterLocator($filterLocator, bool $allowNull = false) : void
    {
    }
    /**
     * Gets a filter with a backward compatibility.
     */
    private function getFilter(string $filterId) : ?\ApiPlatform\Api\FilterInterface
    {
    }
}
