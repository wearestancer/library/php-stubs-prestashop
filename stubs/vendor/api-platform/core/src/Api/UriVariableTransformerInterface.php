<?php

namespace ApiPlatform\Api;

interface UriVariableTransformerInterface
{
    /**
     * Denormalizes data back into an object of the given class.
     *
     * @param mixed $value   The uri variable value to transform
     * @param array $types   The guessed type behind the uri variable
     * @param array $context Options available to the transformer
     *
     * @throws InvalidUriVariableException Occurs when the uriVariable could not be transformed
     */
    public function transform($value, array $types, array $context = []);
    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed $value   The uri variable value to transform
     * @param array $types   The types to which the data should be transformed
     * @param array $context Options available to the transformer
     */
    public function supportsTransformation($value, array $types, array $context = []) : bool;
}
