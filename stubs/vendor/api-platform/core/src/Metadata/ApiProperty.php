<?php

namespace ApiPlatform\Metadata;

/**
 * ApiProperty annotation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::TARGET_PARAMETER)]
final class ApiProperty
{
    /**
     * @param bool|null            $readableLink            https://api-platform.com/docs/core/serialization/#force-iri-with-relations-of-the-same-type-parentchilds-relations
     * @param bool|null            $writableLink            https://api-platform.com/docs/core/serialization/#force-iri-with-relations-of-the-same-type-parentchilds-relations
     * @param bool|null            $required                https://api-platform.com/docs/admin/validation/#client-side-validation
     * @param bool|null            $identifier              https://api-platform.com/docs/core/identifiers/
     * @param string|null          $default
     * @param string|null          $example                 https://api-platform.com/docs/core/openapi/#using-the-openapi-and-swagger-contexts
     * @param string|null          $deprecationReason       https://api-platform.com/docs/core/deprecations/#deprecating-resource-classes-operations-and-properties
     * @param bool|null            $fetchEager              https://api-platform.com/docs/core/performance/#eager-loading
     * @param array|null           $jsonldContext           https://api-platform.com/docs/core/extending-jsonld-context/#extending-json-ld-and-hydra-contexts
     * @param array|null           $openapiContext          https://api-platform.com/docs/core/openapi/#using-the-openapi-and-swagger-contexts
     * @param bool|null            $push                    https://api-platform.com/docs/core/push-relations/
     * @param string|null          $security                https://api-platform.com/docs/core/security
     * @param string|null          $securityPostDenormalize https://api-platform.com/docs/core/security/#executing-access-control-rules-after-denormalization
     * @param array|null           $types                   the RDF types of this property
     * @param string|string[]|null $iris
     */
    public function __construct(
        ?string $description = null,
        ?bool $readable = null,
        ?bool $writable = null,
        ?bool $readableLink = null,
        ?bool $writableLink = null,
        ?bool $required = null,
        ?bool $identifier = null,
        $default = null,
        $example = null,
        ?string $deprecationReason = null,
        ?bool $fetchable = null,
        ?bool $fetchEager = null,
        ?array $jsonldContext = null,
        ?array $openapiContext = null,
        ?array $jsonSchemaContext = null,
        ?bool $push = null,
        ?string $security = null,
        ?string $securityPostDenormalize = null,
        $types = null,
        ?array $builtinTypes = null,
        ?array $schema = null,
        ?bool $initializable = null,
        ?bool $genId = null,
        $iris = null,
        // attributes
        array $extraProperties = []
    )
    {
    }
    public function getDescription() : ?string
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function isReadable() : ?bool
    {
    }
    public function withReadable(bool $readable) : self
    {
    }
    public function isWritable() : ?bool
    {
    }
    public function withWritable(bool $writable) : self
    {
    }
    public function isReadableLink() : ?bool
    {
    }
    public function withReadableLink(bool $readableLink) : self
    {
    }
    public function isWritableLink() : ?bool
    {
    }
    public function withWritableLink(bool $writableLink) : self
    {
    }
    public function isRequired() : ?bool
    {
    }
    public function withRequired(bool $required) : self
    {
    }
    public function isIdentifier() : ?bool
    {
    }
    public function withIdentifier(bool $identifier) : self
    {
    }
    public function getDefault()
    {
    }
    public function withDefault($default) : self
    {
    }
    public function getExample()
    {
    }
    public function withExample($example) : self
    {
    }
    public function getDeprecationReason() : ?string
    {
    }
    public function withDeprecationReason($deprecationReason) : self
    {
    }
    public function isFetchable() : ?bool
    {
    }
    public function withFetchable($fetchable) : self
    {
    }
    public function getFetchEager() : ?bool
    {
    }
    public function withFetchEager($fetchEager) : self
    {
    }
    public function getJsonldContext() : ?array
    {
    }
    public function withJsonldContext($jsonldContext) : self
    {
    }
    public function getOpenapiContext() : ?array
    {
    }
    public function withOpenapiContext($openapiContext) : self
    {
    }
    public function getJsonSchemaContext() : ?array
    {
    }
    public function withJsonSchemaContext($jsonSchemaContext) : self
    {
    }
    public function getPush() : ?bool
    {
    }
    public function withPush($push) : self
    {
    }
    public function getSecurity() : ?string
    {
    }
    public function withSecurity($security) : self
    {
    }
    public function getSecurityPostDenormalize() : ?string
    {
    }
    public function withSecurityPostDenormalize($securityPostDenormalize) : self
    {
    }
    public function getTypes() : ?array
    {
    }
    /**
     * @param string[]|string $types
     */
    public function withTypes($types = []) : self
    {
    }
    /**
     * @return Type[]
     */
    public function getBuiltinTypes() : ?array
    {
    }
    /**
     * @param Type[] $builtinTypes
     */
    public function withBuiltinTypes(array $builtinTypes = []) : self
    {
    }
    public function getSchema() : ?array
    {
    }
    public function withSchema(array $schema = []) : self
    {
    }
    public function withInitializable(?bool $initializable) : self
    {
    }
    public function isInitializable() : ?bool
    {
    }
    public function getExtraProperties() : ?array
    {
    }
    public function withExtraProperties(array $extraProperties = []) : self
    {
    }
    /**
     * Gets IRI of this property.
     */
    public function getIris()
    {
    }
    /**
     * Returns a new instance with the given IRI.
     *
     * @param string|string[] $iris
     */
    public function withIris($iris) : self
    {
    }
    /**
     * Whether to generate a skolem iri on anonymous resources.
     */
    public function getGenId()
    {
    }
    public function withGenId(bool $genId) : self
    {
    }
}
