<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Extracts an array of metadata from a list of YAML files.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class YamlResourceExtractor extends \ApiPlatform\Metadata\Extractor\AbstractResourceExtractor
{
    use \ApiPlatform\Metadata\Extractor\ResourceExtractorTrait;
}
