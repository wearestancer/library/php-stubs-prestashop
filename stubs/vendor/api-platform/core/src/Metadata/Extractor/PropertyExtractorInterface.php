<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Extracts an array of metadata from a file or a list of files.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface PropertyExtractorInterface
{
    /**
     * Parses all metadata files and convert them in an array.
     *
     * @throws InvalidArgumentException
     */
    public function getProperties() : array;
}
