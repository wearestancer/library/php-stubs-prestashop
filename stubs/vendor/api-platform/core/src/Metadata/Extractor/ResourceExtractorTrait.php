<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Utils for ResourceExtractors.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
trait ResourceExtractorTrait
{
    /**
     * @param array|\SimpleXMLElement|null $resource
     * @param mixed|null                   $default
     *
     * @return array|null
     */
    private function buildArrayValue($resource, string $key, $default = null)
    {
    }
    /**
     * Transforms an attribute's value in a PHP value.
     *
     * @param array|\SimpleXMLElement|null $resource
     * @param mixed|null                   $default
     *
     * @return string|int|bool|array|null
     */
    private function phpize($resource, string $key, string $type, $default = null)
    {
    }
    private function buildArgs(\SimpleXMLElement $resource) : ?array
    {
    }
    private function buildValues(\SimpleXMLElement $resource) : array
    {
    }
}
