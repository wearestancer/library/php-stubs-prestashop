<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Extracts an array of metadata from a list of YAML files.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class YamlPropertyExtractor extends \ApiPlatform\Metadata\Extractor\AbstractPropertyExtractor
{
}
