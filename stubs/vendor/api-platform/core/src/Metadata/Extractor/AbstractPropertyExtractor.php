<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Base file extractor.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
abstract class AbstractPropertyExtractor implements \ApiPlatform\Metadata\Extractor\PropertyExtractorInterface
{
    protected $paths;
    protected $properties;
    /**
     * @param string[] $paths
     */
    public function __construct(array $paths, \Psr\Container\ContainerInterface $container = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties() : array
    {
    }
    /**
     * Extracts metadata from a given path.
     */
    protected abstract function extractPath(string $path);
    /**
     * Recursively replaces placeholders with the service container parameters.
     *
     * @see https://github.com/symfony/symfony/blob/6fec32c/src/Symfony/Bundle/FrameworkBundle/Routing/Router.php
     *
     * @copyright (c) Fabien Potencier <fabien@symfony.com>
     *
     * @param mixed $value The source which might contain "%placeholders%"
     *
     * @throws \RuntimeException When a container value is not a string or a numeric value
     *
     * @return mixed The source with the placeholders replaced by the container
     *               parameters. Arrays are resolved recursively.
     */
    protected function resolve($value)
    {
    }
}
