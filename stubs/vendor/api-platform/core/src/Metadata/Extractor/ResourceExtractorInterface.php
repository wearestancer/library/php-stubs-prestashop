<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Extracts an array of metadata from a file or a list of files.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
interface ResourceExtractorInterface
{
    /**
     * Parses all metadata files and convert them in an array.
     *
     * @throws InvalidArgumentException
     */
    public function getResources() : array;
}
