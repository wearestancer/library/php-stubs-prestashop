<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Extracts an array of metadata from a list of XML files.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class XmlResourceExtractor extends \ApiPlatform\Metadata\Extractor\AbstractResourceExtractor
{
    use \ApiPlatform\Metadata\Extractor\ResourceExtractorTrait;
    public const SCHEMA = __DIR__ . '/schema/resources.xsd';
}
