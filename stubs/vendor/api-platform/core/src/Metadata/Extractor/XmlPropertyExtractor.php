<?php

namespace ApiPlatform\Metadata\Extractor;

/**
 * Extracts an array of metadata from a list of XML files.
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class XmlPropertyExtractor extends \ApiPlatform\Metadata\Extractor\AbstractPropertyExtractor
{
    public const SCHEMA = __DIR__ . '/schema/properties.xsd';
}
