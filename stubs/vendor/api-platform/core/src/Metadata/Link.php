<?php

namespace ApiPlatform\Metadata;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::TARGET_PARAMETER)]
final class Link
{
    public function __construct(?string $parameterName = null, ?string $fromProperty = null, ?string $toProperty = null, ?string $fromClass = null, ?string $toClass = null, ?array $identifiers = null, ?bool $compositeIdentifier = null, ?string $expandedValue = null)
    {
    }
    public function getParameterName() : ?string
    {
    }
    public function withParameterName(string $parameterName) : self
    {
    }
    public function getFromClass() : ?string
    {
    }
    public function withFromClass(string $fromClass) : self
    {
    }
    public function getToClass() : ?string
    {
    }
    public function withToClass(string $toClass) : self
    {
    }
    public function getFromProperty() : ?string
    {
    }
    public function withFromProperty(string $fromProperty) : self
    {
    }
    public function getToProperty() : ?string
    {
    }
    public function withToProperty(string $toProperty) : self
    {
    }
    public function getIdentifiers() : ?array
    {
    }
    public function withIdentifiers(array $identifiers) : self
    {
    }
    public function getCompositeIdentifier() : ?bool
    {
    }
    public function withCompositeIdentifier(bool $compositeIdentifier) : self
    {
    }
    public function getExpandedValue() : ?string
    {
    }
    public function withExpandedValue(string $expandedValue) : self
    {
    }
    public function withLink(self $link) : self
    {
    }
}
