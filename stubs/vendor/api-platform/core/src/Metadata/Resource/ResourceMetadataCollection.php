<?php

namespace ApiPlatform\Metadata\Resource;

/**
 * @extends \ArrayObject<int, ApiResource>
 */
final class ResourceMetadataCollection extends \ArrayObject
{
    public function __construct(string $resourceClass, array $input = [])
    {
    }
    public function getOperation(?string $operationName = null, bool $forceCollection = false, bool $httpOperation = false) : \ApiPlatform\Metadata\Operation
    {
    }
}
