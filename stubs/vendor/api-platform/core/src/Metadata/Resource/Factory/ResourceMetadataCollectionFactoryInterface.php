<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Creates a resource metadata value object.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
interface ResourceMetadataCollectionFactoryInterface
{
    /**
     * Creates a resource metadata.
     *
     * @throws ResourceClassNotFoundException
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection;
}
