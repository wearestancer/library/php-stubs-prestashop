<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * @author Antoine Bluchet <soyuka@gmail.com>
 *
 * @deprecated BC layer, is removed in 3.0
 */
final class AlternateUriResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
