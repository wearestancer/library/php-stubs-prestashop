<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Creates a resource name collection from {@see ApiResource} configuration files.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class ExtractorResourceNameCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Extractor\ResourceExtractorInterface $extractor, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function create() : \ApiPlatform\Metadata\Resource\ResourceNameCollection
    {
    }
}
