<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * @internal
 */
final class LinkFactory implements \ApiPlatform\Metadata\Resource\Factory\LinkFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $propertyMetadataFactory, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createLinksFromIdentifiers($operation) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createLinksFromRelations($operation) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createLinksFromAttributes($operation) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function completeLink(\ApiPlatform\Metadata\Link $link) : \ApiPlatform\Metadata\Link
    {
    }
}
