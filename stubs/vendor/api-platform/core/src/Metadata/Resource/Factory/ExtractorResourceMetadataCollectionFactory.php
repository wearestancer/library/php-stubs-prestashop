<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Creates a resource metadata from {@see Resource} extractors (XML, YAML).
 *
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ExtractorResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    use \ApiPlatform\Metadata\Resource\DeprecationMetadataTrait;
    public function __construct(\ApiPlatform\Metadata\Extractor\ResourceExtractorInterface $extractor, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated = null, array $defaults = [], \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
