<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class UriTemplateResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\LinkFactoryInterface $linkFactory, \ApiPlatform\Operation\PathSegmentNameGeneratorInterface $pathSegmentNameGenerator, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
