<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * We have to compute a local cache having all the resource => subresource matching.
 *
 * @deprecated
 */
final class LegacySubresourceMetadataResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    use \ApiPlatform\Metadata\Resource\DeprecationMetadataTrait;
    public function __construct(\ApiPlatform\Core\Operation\Factory\SubresourceOperationFactoryInterface $subresourceOperationFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated = null)
    {
    }
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
