<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Creates a resource name collection value object.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ResourceNameCollectionFactoryInterface
{
    /**
     * Creates the resource name collection.
     */
    public function create() : \ApiPlatform\Metadata\Resource\ResourceNameCollection;
}
