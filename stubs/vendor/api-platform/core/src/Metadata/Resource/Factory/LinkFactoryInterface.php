<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * @internal
 */
interface LinkFactoryInterface
{
    /**
     * Create Links by using the resource class identifiers.
     *
     * @param ApiResource|Operation $operation
     *
     * @return Link[]
     */
    public function createLinksFromIdentifiers($operation);
    /**
     * Create Links from the relations metadata information.
     *
     * @param ApiResource|Operation $operation
     *
     * @return Link[]
     */
    public function createLinksFromRelations($operation);
    /**
     * Create Links by using PHP attribute Links found on properties.
     *
     * @param ApiResource|Operation $operation
     *
     * @return Link[]
     */
    public function createLinksFromAttributes($operation) : array;
    /**
     * Complete a link with identifiers information.
     */
    public function completeLink(\ApiPlatform\Metadata\Link $link) : \ApiPlatform\Metadata\Link;
}
