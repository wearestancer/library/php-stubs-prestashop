<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Prepares graphql links.
 *
 * @author Alan Poulain <contact@alanpoulain.eu>
 */
final class LinkResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\LinkFactoryInterface $linkFactory, \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
