<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Creates a resource metadata from {@see Resource} annotations.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class OperationNameResourceMetadataCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass) : \ApiPlatform\Metadata\Resource\ResourceMetadataCollection
    {
    }
}
