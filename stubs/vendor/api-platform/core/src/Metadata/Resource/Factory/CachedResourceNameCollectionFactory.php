<?php

namespace ApiPlatform\Metadata\Resource\Factory;

/**
 * Caches resource name collection.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class CachedResourceNameCollectionFactory implements \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface
{
    use \ApiPlatform\Util\CachedTrait;
    public const CACHE_KEY = 'resource_name_collection';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create() : \ApiPlatform\Metadata\Resource\ResourceNameCollection
    {
    }
}
