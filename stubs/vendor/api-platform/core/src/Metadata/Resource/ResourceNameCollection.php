<?php

namespace ApiPlatform\Metadata\Resource;

/**
 * A collection of resource class names.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ResourceNameCollection implements \IteratorAggregate, \Countable
{
    /**
     * @param string[] $classes
     */
    public function __construct(array $classes = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return \Traversable<string>
     */
    public function getIterator() : \Traversable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
    }
}
