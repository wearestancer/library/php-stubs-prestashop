<?php

namespace ApiPlatform\Metadata;

class HttpOperation extends \ApiPlatform\Metadata\Operation
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PUT = 'PUT';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_DELETE = 'DELETE';
    public const METHOD_HEAD = 'HEAD';
    public const METHOD_OPTIONS = 'OPTIONS';
    protected $method;
    protected $uriTemplate;
    protected $types;
    /**
     * @var array|mixed|string|null
     */
    protected $formats;
    /**
     * @var array|mixed|string|null
     */
    protected $inputFormats;
    /**
     * @var array|mixed|string|null
     */
    protected $outputFormats;
    /**
     * @var array<string, Link>|array<string, array>|string[]|string|null
     */
    protected $uriVariables;
    protected $routePrefix;
    protected $routeName;
    protected $defaults;
    protected $requirements;
    protected $options;
    protected $stateless;
    protected $sunset;
    protected $acceptPatch;
    protected $cacheHeaders;
    /**
     * @var string|int|null
     */
    protected $status;
    protected $host;
    protected $schemes;
    protected $condition;
    protected $controller;
    /**
     * @var string[]
     */
    protected $hydraContext;
    protected $openapiContext;
    protected $openapi;
    protected $exceptionToStatus;
    protected $queryParameterValidationEnabled;
    /**
     * @param array|null        $types                          the RDF types of this property
     * @param array|string|null $formats                        https://api-platform.com/docs/core/content-negotiation/#configuring-formats-for-a-specific-resource-or-operation
     * @param array|string|null $inputFormats                   https://api-platform.com/docs/core/content-negotiation/#configuring-formats-for-a-specific-resource-or-operation
     * @param array|string|null $outputFormats                  https://api-platform.com/docs/core/content-negotiation/#configuring-formats-for-a-specific-resource-or-operation
     * @param mixed|null        $uriVariables
     * @param string|null       $routePrefix                    https://api-platform.com/docs/core/operations/#prefixing-all-routes-of-all-operations
     * @param string|null       $sunset                         https://api-platform.com/docs/core/deprecations/#setting-the-sunset-http-header-to-indicate-when-a-resource-or-an-operation-will-be-removed
     * @param string|int|null   $status
     * @param string|null       $deprecationReason              https://api-platform.com/docs/core/deprecations/#deprecating-resource-classes-operations-and-properties
     * @param array|null        $cacheHeaders                   https://api-platform.com/docs/core/performance/#setting-custom-http-cache-headers
     * @param array|null        $normalizationContext           https://api-platform.com/docs/core/serialization/#using-serialization-groups
     * @param array|null        $denormalizationContext         https://api-platform.com/docs/core/serialization/#using-serialization-groups
     * @param string[]|null     $hydraContext                   https://api-platform.com/docs/core/extending-jsonld-context/#hydra
     * @param array|null        $openapiContext                 https://api-platform.com/docs/core/openapi/#using-the-openapi-and-swagger-contexts
     * @param string[]|null     $filters                        https://api-platform.com/docs/core/filters/#doctrine-orm-and-mongodb-odm-filters
     * @param bool|null         $elasticsearch                  https://api-platform.com/docs/core/elasticsearch/
     * @param mixed|null        $mercure                        https://api-platform.com/docs/core/mercure
     * @param mixed|null        $messenger                      https://api-platform.com/docs/core/messenger/#dispatching-a-resource-through-the-message-bus
     * @param mixed|null        $input                          https://api-platform.com/docs/core/dto/#specifying-an-input-or-an-output-data-representation
     * @param mixed|null        $output                         https://api-platform.com/docs/core/dto/#specifying-an-input-or-an-output-data-representation
     * @param array|null        $order                          https://api-platform.com/docs/core/default-order/#overriding-default-order
     * @param bool|null         $fetchPartial                   https://api-platform.com/docs/core/performance/#fetch-partial
     * @param bool|null         $forceEager                     https://api-platform.com/docs/core/performance/#force-eager
     * @param bool|null         $paginationClientEnabled        https://api-platform.com/docs/core/pagination/#for-a-specific-resource-1
     * @param bool|null         $paginationClientItemsPerPage   https://api-platform.com/docs/core/pagination/#for-a-specific-resource-3
     * @param bool|null         $paginationClientPartial        https://api-platform.com/docs/core/pagination/#for-a-specific-resource-6
     * @param array|null        $paginationViaCursor            https://api-platform.com/docs/core/pagination/#cursor-based-pagination
     * @param bool|null         $paginationEnabled              https://api-platform.com/docs/core/pagination/#for-a-specific-resource
     * @param bool|null         $paginationFetchJoinCollection  https://api-platform.com/docs/core/pagination/#controlling-the-behavior-of-the-doctrine-orm-paginator
     * @param int|null          $paginationItemsPerPage         https://api-platform.com/docs/core/pagination/#changing-the-number-of-items-per-page
     * @param int|null          $paginationMaximumItemsPerPage  https://api-platform.com/docs/core/pagination/#changing-maximum-items-per-page
     * @param bool|null         $paginationPartial              https://api-platform.com/docs/core/performance/#partial-pagination
     * @param string|null       $paginationType                 https://api-platform.com/docs/core/graphql/#using-the-page-based-pagination
     * @param string|null       $security                       https://api-platform.com/docs/core/security
     * @param string|null       $securityMessage                https://api-platform.com/docs/core/security/#configuring-the-access-control-error-message
     * @param string|null       $securityPostDenormalize        https://api-platform.com/docs/core/security/#executing-access-control-rules-after-denormalization
     * @param string|null       $securityPostDenormalizeMessage https://api-platform.com/docs/core/security/#configuring-the-access-control-error-message
     * @param bool|null         $read                           https://api-platform.com/docs/core/events/#the-event-system
     * @param bool|null         $deserialize                    https://api-platform.com/docs/core/events/#the-event-system
     * @param bool|null         $validate                       https://api-platform.com/docs/core/events/#the-event-system
     * @param bool|null         $write                          https://api-platform.com/docs/core/events/#the-event-system
     * @param bool|null         $serialize                      https://api-platform.com/docs/core/events/#the-event-system
     * @param mixed|null        $provider
     * @param mixed|null        $processor
     */
    public function __construct(
        string $method = self::METHOD_GET,
        ?string $uriTemplate = null,
        ?array $types = null,
        $formats = null,
        $inputFormats = null,
        $outputFormats = null,
        $uriVariables = null,
        ?string $routePrefix = null,
        ?string $routeName = null,
        ?array $defaults = null,
        ?array $requirements = null,
        ?array $options = null,
        ?bool $stateless = null,
        ?string $sunset = null,
        ?string $acceptPatch = null,
        $status = null,
        ?string $host = null,
        ?array $schemes = null,
        ?string $condition = null,
        ?string $controller = null,
        ?array $cacheHeaders = null,
        ?array $hydraContext = null,
        ?array $openapiContext = null,
        ?bool $openapi = null,
        ?array $exceptionToStatus = null,
        ?bool $queryParameterValidationEnabled = null,
        // abstract operation arguments
        ?string $shortName = null,
        ?string $class = null,
        ?bool $paginationEnabled = null,
        ?string $paginationType = null,
        ?int $paginationItemsPerPage = null,
        ?int $paginationMaximumItemsPerPage = null,
        ?bool $paginationPartial = null,
        ?bool $paginationClientEnabled = null,
        ?bool $paginationClientItemsPerPage = null,
        ?bool $paginationClientPartial = null,
        ?bool $paginationFetchJoinCollection = null,
        ?bool $paginationUseOutputWalkers = null,
        ?array $paginationViaCursor = null,
        ?array $order = null,
        ?string $description = null,
        ?array $normalizationContext = null,
        ?array $denormalizationContext = null,
        ?string $security = null,
        ?string $securityMessage = null,
        ?string $securityPostDenormalize = null,
        ?string $securityPostDenormalizeMessage = null,
        ?string $securityPostValidation = null,
        ?string $securityPostValidationMessage = null,
        ?string $deprecationReason = null,
        ?array $filters = null,
        ?array $validationContext = null,
        $input = null,
        $output = null,
        $mercure = null,
        $messenger = null,
        ?bool $elasticsearch = null,
        ?int $urlGenerationStrategy = null,
        ?bool $read = null,
        ?bool $deserialize = null,
        ?bool $validate = null,
        ?bool $write = null,
        ?bool $serialize = null,
        ?bool $fetchPartial = null,
        ?bool $forceEager = null,
        ?int $priority = null,
        ?string $name = null,
        $provider = null,
        $processor = null,
        array $extraProperties = []
    )
    {
    }
    public function getMethod() : ?string
    {
    }
    public function withMethod(string $method) : self
    {
    }
    public function getUriTemplate() : ?string
    {
    }
    public function withUriTemplate(?string $uriTemplate = null)
    {
    }
    public function getTypes() : ?array
    {
    }
    /**
     * @param string[]|string $types
     */
    public function withTypes($types) : self
    {
    }
    /**
     * @return array|mixed|string|null
     */
    public function getFormats()
    {
    }
    public function withFormats($formats = null) : self
    {
    }
    /**
     * @return array|mixed|string|null
     */
    public function getInputFormats()
    {
    }
    public function withInputFormats($inputFormats = null) : self
    {
    }
    /**
     * @return array|mixed|string|null
     */
    public function getOutputFormats()
    {
    }
    public function withOutputFormats($outputFormats = null) : self
    {
    }
    /**
     * @return array<string, Link>|array<string, array>|string[]|string|null
     */
    public function getUriVariables()
    {
    }
    /**
     * @param array<string, Link>|array<string, array>|string[]|string $uriVariables
     */
    public function withUriVariables($uriVariables) : self
    {
    }
    public function getRoutePrefix() : ?string
    {
    }
    public function withRoutePrefix(string $routePrefix) : self
    {
    }
    public function getRouteName() : ?string
    {
    }
    public function withRouteName(?string $routeName) : self
    {
    }
    public function getDefaults() : ?array
    {
    }
    public function withDefaults(array $defaults) : self
    {
    }
    public function getRequirements() : ?array
    {
    }
    public function withRequirements(array $requirements) : self
    {
    }
    public function getOptions() : ?array
    {
    }
    public function withOptions(array $options) : self
    {
    }
    public function getStateless() : ?bool
    {
    }
    public function withStateless($stateless) : self
    {
    }
    public function getSunset() : ?string
    {
    }
    public function withSunset(string $sunset) : self
    {
    }
    public function getAcceptPatch() : ?string
    {
    }
    public function withAcceptPatch(string $acceptPatch) : self
    {
    }
    public function getStatus() : ?int
    {
    }
    public function withStatus(int $status) : self
    {
    }
    public function getHost() : ?string
    {
    }
    public function withHost(string $host) : self
    {
    }
    public function getSchemes() : ?array
    {
    }
    public function withSchemes(array $schemes) : self
    {
    }
    public function getCondition() : ?string
    {
    }
    public function withCondition(string $condition) : self
    {
    }
    public function getController() : ?string
    {
    }
    public function withController(string $controller) : self
    {
    }
    public function getCacheHeaders() : ?array
    {
    }
    public function withCacheHeaders(array $cacheHeaders) : self
    {
    }
    /**
     * @return string[]
     */
    public function getHydraContext() : ?array
    {
    }
    public function withHydraContext(array $hydraContext) : self
    {
    }
    public function getOpenapiContext() : ?array
    {
    }
    public function withOpenapiContext(array $openapiContext) : self
    {
    }
    public function getOpenapi() : ?bool
    {
    }
    public function withOpenapi(bool $openapi) : self
    {
    }
    public function getExceptionToStatus() : ?array
    {
    }
    public function withExceptionToStatus(array $exceptionToStatus) : self
    {
    }
    public function getQueryParameterValidationEnabled() : ?bool
    {
    }
    public function withQueryParameterValidationEnabled(bool $queryParameterValidationEnabled) : self
    {
    }
}
