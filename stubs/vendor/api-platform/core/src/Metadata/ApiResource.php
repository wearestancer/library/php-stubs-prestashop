<?php

namespace ApiPlatform\Metadata;

/**
 * Resource metadata attribute.
 *
 * @Annotation
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::IS_REPEATABLE)]
class ApiResource
{
    use \ApiPlatform\Metadata\WithResourceTrait;
    protected $operations;
    protected $uriTemplate;
    protected $shortName;
    protected $description;
    /**
     * @var string|string[]|null
     */
    protected $types;
    /**
     * @var array|mixed|string|null
     */
    protected $formats;
    /**
     * @var array|mixed|string|null
     */
    protected $inputFormats;
    /**
     * @var array|mixed|string|null
     */
    protected $outputFormats;
    /**
     * @var array<string, Link>|array<string, array>|string[]|string|null
     */
    protected $uriVariables;
    protected $routePrefix;
    protected $defaults;
    protected $requirements;
    protected $options;
    protected $stateless;
    protected $sunset;
    protected $acceptPatch;
    protected $status;
    protected $host;
    protected $schemes;
    protected $condition;
    protected $controller;
    protected $class;
    protected $urlGenerationStrategy;
    protected $deprecationReason;
    protected $cacheHeaders;
    protected $normalizationContext;
    protected $denormalizationContext;
    /**
     * @var string[]|null
     */
    protected $hydraContext;
    protected $openapiContext;
    protected $validationContext;
    /**
     * @var string[]
     */
    protected $filters;
    protected $elasticsearch;
    /**
     * @var array|bool|mixed|null
     */
    protected $mercure;
    /**
     * @var bool|mixed|null
     */
    protected $messenger;
    protected $input;
    protected $output;
    protected $order;
    protected $fetchPartial;
    protected $forceEager;
    protected $paginationClientEnabled;
    protected $paginationClientItemsPerPage;
    protected $paginationClientPartial;
    protected $paginationViaCursor;
    protected $paginationEnabled;
    protected $paginationFetchJoinCollection;
    protected $paginationUseOutputWalkers;
    protected $paginationItemsPerPage;
    protected $paginationMaximumItemsPerPage;
    protected $paginationPartial;
    protected $paginationType;
    protected $security;
    protected $securityMessage;
    protected $securityPostDenormalize;
    protected $securityPostDenormalizeMessage;
    protected $securityPostValidation;
    protected $securityPostValidationMessage;
    protected $compositeIdentifier;
    protected $exceptionToStatus;
    protected $queryParameterValidationEnabled;
    protected $graphQlOperations;
    /**
     * @var string|callable|null
     */
    protected $provider;
    /**
     * @var string|callable|null
     */
    protected $processor;
    protected $extraProperties;
    /**
     * @param string|string[]|null $types                          The RDF types of this resource
     * @param mixed|null           $operations
     * @param array|string|null    $formats                        https://api-platform.com/docs/core/content-negotiation/#configuring-formats-for-a-specific-resource-or-operation
     * @param array|string|null    $inputFormats                   https://api-platform.com/docs/core/content-negotiation/#configuring-formats-for-a-specific-resource-or-operation
     * @param array|string|null    $outputFormats                  https://api-platform.com/docs/core/content-negotiation/#configuring-formats-for-a-specific-resource-or-operation
     * @param mixed|null           $uriVariables
     * @param string|null          $routePrefix                    https://api-platform.com/docs/core/operations/#prefixing-all-routes-of-all-operations
     * @param string|null          $sunset                         https://api-platform.com/docs/core/deprecations/#setting-the-sunset-http-header-to-indicate-when-a-resource-or-an-operation-will-be-removed
     * @param string|null          $deprecationReason              https://api-platform.com/docs/core/deprecations/#deprecating-resource-classes-operations-and-properties
     * @param array|null           $cacheHeaders                   https://api-platform.com/docs/core/performance/#setting-custom-http-cache-headers
     * @param array|null           $normalizationContext           https://api-platform.com/docs/core/serialization/#using-serialization-groups
     * @param array|null           $denormalizationContext         https://api-platform.com/docs/core/serialization/#using-serialization-groups
     * @param string[]|null        $hydraContext                   https://api-platform.com/docs/core/extending-jsonld-context/#hydra
     * @param array|null           $openapiContext                 https://api-platform.com/docs/core/openapi/#using-the-openapi-and-swagger-contexts
     * @param array|null           $validationContext              https://api-platform.com/docs/core/validation/#using-validation-groups
     * @param string[]|null        $filters                        https://api-platform.com/docs/core/filters/#doctrine-orm-and-mongodb-odm-filters
     * @param bool|null            $elasticsearch                  https://api-platform.com/docs/core/elasticsearch/
     * @param mixed|null           $mercure                        https://api-platform.com/docs/core/mercure
     * @param mixed|null           $messenger                      https://api-platform.com/docs/core/messenger/#dispatching-a-resource-through-the-message-bus
     * @param mixed|null           $input                          https://api-platform.com/docs/core/dto/#specifying-an-input-or-an-output-data-representation
     * @param mixed|null           $output                         https://api-platform.com/docs/core/dto/#specifying-an-input-or-an-output-data-representation
     * @param array|null           $order                          https://api-platform.com/docs/core/default-order/#overriding-default-order
     * @param bool|null            $fetchPartial                   https://api-platform.com/docs/core/performance/#fetch-partial
     * @param bool|null            $forceEager                     https://api-platform.com/docs/core/performance/#force-eager
     * @param bool|null            $paginationClientEnabled        https://api-platform.com/docs/core/pagination/#for-a-specific-resource-1
     * @param bool|null            $paginationClientItemsPerPage   https://api-platform.com/docs/core/pagination/#for-a-specific-resource-3
     * @param bool|null            $paginationClientPartial        https://api-platform.com/docs/core/pagination/#for-a-specific-resource-6
     * @param array|null           $paginationViaCursor            https://api-platform.com/docs/core/pagination/#cursor-based-pagination
     * @param bool|null            $paginationEnabled              https://api-platform.com/docs/core/pagination/#for-a-specific-resource
     * @param bool|null            $paginationFetchJoinCollection  https://api-platform.com/docs/core/pagination/#controlling-the-behavior-of-the-doctrine-orm-paginator
     * @param int|null             $paginationItemsPerPage         https://api-platform.com/docs/core/pagination/#changing-the-number-of-items-per-page
     * @param int|null             $paginationMaximumItemsPerPage  https://api-platform.com/docs/core/pagination/#changing-maximum-items-per-page
     * @param bool|null            $paginationPartial              https://api-platform.com/docs/core/performance/#partial-pagination
     * @param string|null          $paginationType                 https://api-platform.com/docs/core/graphql/#using-the-page-based-pagination
     * @param string|null          $security                       https://api-platform.com/docs/core/security
     * @param string|null          $securityMessage                https://api-platform.com/docs/core/security/#configuring-the-access-control-error-message
     * @param string|null          $securityPostDenormalize        https://api-platform.com/docs/core/security/#executing-access-control-rules-after-denormalization
     * @param string|null          $securityPostDenormalizeMessage https://api-platform.com/docs/core/security/#configuring-the-access-control-error-message
     * @param string               $securityPostValidation         https://api-platform.com/docs/core/security/#executing-access-control-rules-after-validtion
     * @param string               $securityPostValidationMessage  https://api-platform.com/docs/core/security/#configuring-the-access-control-error-message
     * @param string|callable|null $provider
     * @param string|callable|null $processor
     */
    public function __construct(?string $uriTemplate = null, ?string $shortName = null, ?string $description = null, $types = null, $operations = null, $formats = null, $inputFormats = null, $outputFormats = null, $uriVariables = null, ?string $routePrefix = null, ?array $defaults = null, ?array $requirements = null, ?array $options = null, ?bool $stateless = null, ?string $sunset = null, ?string $acceptPatch = null, ?int $status = null, ?string $host = null, ?array $schemes = null, ?string $condition = null, ?string $controller = null, ?string $class = null, ?int $urlGenerationStrategy = null, ?string $deprecationReason = null, ?array $cacheHeaders = null, ?array $normalizationContext = null, ?array $denormalizationContext = null, ?array $hydraContext = null, ?array $openapiContext = null, ?array $validationContext = null, ?array $filters = null, ?bool $elasticsearch = null, $mercure = null, $messenger = null, $input = null, $output = null, ?array $order = null, ?bool $fetchPartial = null, ?bool $forceEager = null, ?bool $paginationClientEnabled = null, ?bool $paginationClientItemsPerPage = null, ?bool $paginationClientPartial = null, ?array $paginationViaCursor = null, ?bool $paginationEnabled = null, ?bool $paginationFetchJoinCollection = null, ?bool $paginationUseOutputWalkers = null, ?int $paginationItemsPerPage = null, ?int $paginationMaximumItemsPerPage = null, ?bool $paginationPartial = null, ?string $paginationType = null, ?string $security = null, ?string $securityMessage = null, ?string $securityPostDenormalize = null, ?string $securityPostDenormalizeMessage = null, ?string $securityPostValidation = null, ?string $securityPostValidationMessage = null, ?bool $compositeIdentifier = null, ?array $exceptionToStatus = null, ?bool $queryParameterValidationEnabled = null, ?array $graphQlOperations = null, $provider = null, $processor = null, array $extraProperties = [])
    {
    }
    public function getOperations() : ?\ApiPlatform\Metadata\Operations
    {
    }
    public function withOperations(\ApiPlatform\Metadata\Operations $operations) : self
    {
    }
    public function getUriTemplate() : ?string
    {
    }
    public function withUriTemplate(string $uriTemplate) : self
    {
    }
    public function getShortName() : ?string
    {
    }
    public function withShortName(string $shortName) : self
    {
    }
    public function getDescription() : ?string
    {
    }
    public function withDescription(string $description) : self
    {
    }
    public function getTypes() : ?array
    {
    }
    /**
     * @param string[]|string $types
     */
    public function withTypes($types) : self
    {
    }
    /**
     * @return array|mixed|string|null
     */
    public function getFormats()
    {
    }
    /**
     * @param mixed|null $formats
     */
    public function withFormats($formats) : self
    {
    }
    /**
     * @return array|mixed|string|null
     */
    public function getInputFormats()
    {
    }
    /**
     * @param mixed|null $inputFormats
     */
    public function withInputFormats($inputFormats) : self
    {
    }
    /**
     * @return array|mixed|string|null
     */
    public function getOutputFormats()
    {
    }
    /**
     * @param mixed|null $outputFormats
     */
    public function withOutputFormats($outputFormats) : self
    {
    }
    /**
     * @return array<string, Link>|array<string, array>|string[]|string|null
     */
    public function getUriVariables()
    {
    }
    /**
     * @param array<string, Link>|array<string, array>|string[]|string|null $uriVariables
     */
    public function withUriVariables($uriVariables) : self
    {
    }
    public function getRoutePrefix() : ?string
    {
    }
    public function withRoutePrefix(string $routePrefix) : self
    {
    }
    public function getDefaults() : ?array
    {
    }
    public function withDefaults(array $defaults) : self
    {
    }
    public function getRequirements() : ?array
    {
    }
    public function withRequirements(array $requirements) : self
    {
    }
    public function getOptions() : ?array
    {
    }
    public function withOptions(array $options) : self
    {
    }
    public function getStateless() : ?bool
    {
    }
    public function withStateless(bool $stateless) : self
    {
    }
    public function getSunset() : ?string
    {
    }
    public function withSunset(string $sunset) : self
    {
    }
    public function getAcceptPatch() : ?string
    {
    }
    public function withAcceptPatch(string $acceptPatch) : self
    {
    }
    public function getStatus()
    {
    }
    public function withStatus($status) : self
    {
    }
    public function getHost() : ?string
    {
    }
    public function withHost(string $host) : self
    {
    }
    public function getSchemes() : ?array
    {
    }
    public function withSchemes(array $schemes) : self
    {
    }
    public function getCondition() : ?string
    {
    }
    public function withCondition(string $condition) : self
    {
    }
    public function getController() : ?string
    {
    }
    public function withController(string $controller) : self
    {
    }
    public function getClass() : ?string
    {
    }
    public function withClass(string $class) : self
    {
    }
    public function getUrlGenerationStrategy() : ?int
    {
    }
    public function withUrlGenerationStrategy(int $urlGenerationStrategy) : self
    {
    }
    public function getDeprecationReason() : ?string
    {
    }
    public function withDeprecationReason(string $deprecationReason) : self
    {
    }
    public function getCacheHeaders() : ?array
    {
    }
    public function withCacheHeaders(array $cacheHeaders) : self
    {
    }
    public function getNormalizationContext() : ?array
    {
    }
    public function withNormalizationContext(array $normalizationContext) : self
    {
    }
    public function getDenormalizationContext() : ?array
    {
    }
    public function withDenormalizationContext(array $denormalizationContext) : self
    {
    }
    /**
     * @return string[]|null
     */
    public function getHydraContext() : ?array
    {
    }
    public function withHydraContext(array $hydraContext) : self
    {
    }
    public function getOpenapiContext() : ?array
    {
    }
    public function withOpenapiContext(array $openapiContext) : self
    {
    }
    public function getValidationContext() : ?array
    {
    }
    public function withValidationContext(array $validationContext) : self
    {
    }
    /**
     * @return string[]|null
     */
    public function getFilters() : ?array
    {
    }
    public function withFilters(array $filters) : self
    {
    }
    public function getElasticsearch() : ?bool
    {
    }
    public function withElasticsearch(bool $elasticsearch) : self
    {
    }
    /**
     * @return array|bool|mixed|null
     */
    public function getMercure()
    {
    }
    public function withMercure($mercure) : self
    {
    }
    public function getMessenger()
    {
    }
    public function withMessenger($messenger) : self
    {
    }
    public function getInput()
    {
    }
    public function withInput($input) : self
    {
    }
    public function getOutput()
    {
    }
    public function withOutput($output) : self
    {
    }
    public function getOrder() : ?array
    {
    }
    public function withOrder(array $order) : self
    {
    }
    public function getFetchPartial() : ?bool
    {
    }
    public function withFetchPartial(bool $fetchPartial) : self
    {
    }
    public function getForceEager() : ?bool
    {
    }
    public function withForceEager(bool $forceEager) : self
    {
    }
    public function getPaginationClientEnabled() : ?bool
    {
    }
    public function withPaginationClientEnabled(bool $paginationClientEnabled) : self
    {
    }
    public function getPaginationClientItemsPerPage() : ?bool
    {
    }
    public function withPaginationClientItemsPerPage(bool $paginationClientItemsPerPage) : self
    {
    }
    public function getPaginationClientPartial() : ?bool
    {
    }
    public function withPaginationClientPartial(bool $paginationClientPartial) : self
    {
    }
    public function getPaginationViaCursor() : ?array
    {
    }
    public function withPaginationViaCursor(array $paginationViaCursor) : self
    {
    }
    public function getPaginationEnabled() : ?bool
    {
    }
    public function withPaginationEnabled(bool $paginationEnabled) : self
    {
    }
    public function getPaginationFetchJoinCollection() : ?bool
    {
    }
    public function withPaginationFetchJoinCollection(bool $paginationFetchJoinCollection) : self
    {
    }
    public function getPaginationUseOutputWalkers() : ?bool
    {
    }
    public function withPaginationUseOutputWalkers(bool $paginationUseOutputWalkers) : self
    {
    }
    public function getPaginationItemsPerPage() : ?int
    {
    }
    public function withPaginationItemsPerPage(int $paginationItemsPerPage) : self
    {
    }
    public function getPaginationMaximumItemsPerPage() : ?int
    {
    }
    public function withPaginationMaximumItemsPerPage(int $paginationMaximumItemsPerPage) : self
    {
    }
    public function getPaginationPartial() : ?bool
    {
    }
    public function withPaginationPartial(bool $paginationPartial) : self
    {
    }
    public function getPaginationType() : ?string
    {
    }
    public function withPaginationType(string $paginationType) : self
    {
    }
    public function getSecurity() : ?string
    {
    }
    public function withSecurity(string $security) : self
    {
    }
    public function getSecurityMessage() : ?string
    {
    }
    public function withSecurityMessage(string $securityMessage) : self
    {
    }
    public function getSecurityPostDenormalize() : ?string
    {
    }
    public function withSecurityPostDenormalize(string $securityPostDenormalize) : self
    {
    }
    public function getSecurityPostDenormalizeMessage() : ?string
    {
    }
    public function withSecurityPostDenormalizeMessage(string $securityPostDenormalizeMessage) : self
    {
    }
    public function getSecurityPostValidation() : ?string
    {
    }
    public function withSecurityPostValidation(?string $securityPostValidation = null) : self
    {
    }
    public function getSecurityPostValidationMessage() : ?string
    {
    }
    public function withSecurityPostValidationMessage(?string $securityPostValidationMessage = null) : self
    {
    }
    public function getExceptionToStatus() : ?array
    {
    }
    public function withExceptionToStatus(array $exceptionToStatus) : self
    {
    }
    public function getQueryParameterValidationEnabled() : ?bool
    {
    }
    public function withQueryParameterValidationEnabled(bool $queryParameterValidationEnabled) : self
    {
    }
    /**
     * @return GraphQlOperation[]
     */
    public function getGraphQlOperations() : ?array
    {
    }
    public function withGraphQlOperations(array $graphQlOperations) : self
    {
    }
    /**
     * @return string|callable|null
     */
    public function getProcessor()
    {
    }
    public function withProcessor($processor) : self
    {
    }
    /**
     * @return string|callable|null
     */
    public function getProvider()
    {
    }
    public function withProvider($provider) : self
    {
    }
    public function getExtraProperties() : array
    {
    }
    public function withExtraProperties(array $extraProperties) : self
    {
    }
}
