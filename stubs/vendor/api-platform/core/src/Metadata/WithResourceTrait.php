<?php

namespace ApiPlatform\Metadata;

trait WithResourceTrait
{
    public function withResource(\ApiPlatform\Metadata\ApiResource $resource) : self
    {
    }
    /**
     * @param ApiResource|Operation $resource
     *
     * @return ApiResource|Operation
     */
    protected function copyFrom($resource)
    {
    }
}
