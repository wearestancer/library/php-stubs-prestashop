<?php

namespace ApiPlatform\Metadata\GraphQl;

class Operation extends \ApiPlatform\Metadata\Operation
{
    protected $resolver;
    protected $args;
    /** @var Link[]|null */
    protected $links;
    /**
     * @param string     $resolver
     * @param mixed|null $input
     * @param mixed|null $output
     * @param mixed|null $mercure
     * @param mixed|null $messenger
     */
    public function __construct(
        ?string $resolver = null,
        ?array $args = null,
        ?array $links = null,
        // abstract operation arguments
        ?string $shortName = null,
        ?string $class = null,
        ?bool $paginationEnabled = null,
        ?string $paginationType = null,
        ?int $paginationItemsPerPage = null,
        ?int $paginationMaximumItemsPerPage = null,
        ?bool $paginationPartial = null,
        ?bool $paginationClientEnabled = null,
        ?bool $paginationClientItemsPerPage = null,
        ?bool $paginationClientPartial = null,
        ?bool $paginationFetchJoinCollection = null,
        ?bool $paginationUseOutputWalkers = null,
        ?bool $paginationViaCursor = null,
        ?array $order = null,
        ?string $description = null,
        ?array $normalizationContext = null,
        ?array $denormalizationContext = null,
        ?string $security = null,
        ?string $securityMessage = null,
        ?string $securityPostDenormalize = null,
        ?string $securityPostDenormalizeMessage = null,
        ?string $securityPostValidation = null,
        ?string $securityPostValidationMessage = null,
        ?string $deprecationReason = null,
        ?array $filters = null,
        ?array $validationContext = null,
        $input = null,
        $output = null,
        $mercure = null,
        $messenger = null,
        ?bool $elasticsearch = null,
        ?int $urlGenerationStrategy = null,
        ?bool $read = null,
        ?bool $deserialize = null,
        ?bool $validate = null,
        ?bool $write = null,
        ?bool $serialize = null,
        ?bool $fetchPartial = null,
        ?bool $forceEager = null,
        ?int $priority = null,
        ?string $name = null,
        ?string $provider = null,
        ?string $processor = null,
        array $extraProperties = []
    )
    {
    }
    public function getResolver() : ?string
    {
    }
    public function withResolver(?string $resolver = null) : self
    {
    }
    public function getArgs() : ?array
    {
    }
    public function withArgs(?array $args = null) : self
    {
    }
    /**
     * @return Link[]|null
     */
    public function getLinks()
    {
    }
    /**
     * @param Link[] $links
     */
    public function withLinks(array $links) : self
    {
    }
}
