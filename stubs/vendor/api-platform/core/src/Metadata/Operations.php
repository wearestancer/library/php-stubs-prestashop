<?php

namespace ApiPlatform\Metadata;

final class Operations implements \IteratorAggregate, \Countable
{
    /**
     * @param array<string|int, Operation> $operations
     */
    public function __construct(array $operations = [])
    {
    }
    public function getIterator() : \Traversable
    {
    }
    public function add(string $key, \ApiPlatform\Metadata\Operation $value) : self
    {
    }
    public function remove(string $key) : self
    {
    }
    public function has(string $key) : bool
    {
    }
    public function count() : int
    {
    }
    public function sort() : self
    {
    }
}
