<?php

namespace ApiPlatform\Metadata;

/**
 * Filter annotation.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_CLASS | \Attribute::IS_REPEATABLE)]
final class ApiFilter
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $strategy;
    /**
     * @var string|FilterInterface
     */
    public $filterClass;
    /**
     * @var array
     */
    public $properties = [];
    /**
     * @var array raw arguments for the filter
     */
    public $arguments = [];
    /**
     * @param string $filterClass
     * @param string $id
     * @param string $strategy
     */
    public function __construct($filterClass, ?string $id = null, ?string $strategy = null, array $properties = [], array $arguments = [])
    {
    }
}
