<?php

namespace ApiPlatform\Metadata;

/**
 * The Operation deletes content.
 */
interface DeleteOperationInterface
{
}
