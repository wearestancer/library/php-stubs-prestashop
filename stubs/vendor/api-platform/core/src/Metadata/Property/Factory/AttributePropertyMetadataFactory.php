<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Creates a property metadata from {@see ApiProperty} attribute.
 *
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
final class AttributePropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
