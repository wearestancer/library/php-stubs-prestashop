<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Populates defaults values of the resource properties using the default PHP values of properties.
 */
final class DefaultPropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
