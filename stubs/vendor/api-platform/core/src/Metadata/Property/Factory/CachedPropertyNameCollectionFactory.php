<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Caches property name collection.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class CachedPropertyNameCollectionFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface
{
    use \ApiPlatform\Util\CachedTrait;
    public const CACHE_KEY_PREFIX = 'property_name_collection_';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, array $options = []) : \ApiPlatform\Metadata\Property\PropertyNameCollection
    {
    }
}
