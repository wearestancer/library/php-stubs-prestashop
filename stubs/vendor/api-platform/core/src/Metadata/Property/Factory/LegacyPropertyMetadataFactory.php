<?php

namespace ApiPlatform\Metadata\Property\Factory;

final class LegacyPropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    use \ApiPlatform\Metadata\Property\DeprecationMetadataTrait;
    public function __construct(\ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface $legacyPropertyMetadataFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
