<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * PropertyInfo metadata loader decorator.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PropertyInfoPropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    public function __construct(\Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface $propertyInfo, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
