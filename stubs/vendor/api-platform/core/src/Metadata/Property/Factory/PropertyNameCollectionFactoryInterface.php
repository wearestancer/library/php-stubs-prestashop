<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Creates a property name collection value object.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface PropertyNameCollectionFactoryInterface
{
    /**
     * Creates the property name collection for the given class and options.
     *
     * @throws ResourceClassNotFoundException
     */
    public function create(string $resourceClass, array $options = []) : \ApiPlatform\Metadata\Property\PropertyNameCollection;
}
