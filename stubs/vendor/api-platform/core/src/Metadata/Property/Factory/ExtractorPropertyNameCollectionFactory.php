<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Creates a property name collection using an extractor.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Baptiste Meyer <baptiste.meyer@gmail.com>
 * @author Vincent Chalamon <vincentchalamon@gmail.com>
 */
final class ExtractorPropertyNameCollectionFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface
{
    public function __construct(\ApiPlatform\Metadata\Extractor\PropertyExtractorInterface $extractor, \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface $decorated = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function create(string $resourceClass, array $options = []) : \ApiPlatform\Metadata\Property\PropertyNameCollection
    {
    }
}
