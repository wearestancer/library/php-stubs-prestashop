<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Caches property metadata.
 *
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class CachedPropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    use \ApiPlatform\Util\CachedTrait;
    public const CACHE_KEY_PREFIX = 'property_metadata_';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
