<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * PropertyInfo collection loader.
 *
 * This is not a decorator on purpose because it should always have the top priority.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PropertyInfoPropertyNameCollectionFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface
{
    public function __construct(\Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface $propertyInfo)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, array $options = []) : \ApiPlatform\Metadata\Property\PropertyNameCollection
    {
    }
}
