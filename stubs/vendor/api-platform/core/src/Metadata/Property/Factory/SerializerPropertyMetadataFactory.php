<?php

namespace ApiPlatform\Metadata\Property\Factory;

/**
 * Populates read/write and link status using serialization groups.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Teoh Han Hui <teohhanhui@gmail.com>
 */
final class SerializerPropertyMetadataFactory implements \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface
{
    use \ApiPlatform\Util\ResourceClassInfoTrait;
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $serializerClassMetadataFactory, \ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface $decorated, \ApiPlatform\Api\ResourceClassResolverInterface $resourceClassResolver = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(string $resourceClass, string $property, array $options = []) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
