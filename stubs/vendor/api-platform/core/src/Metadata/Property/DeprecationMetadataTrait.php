<?php

namespace ApiPlatform\Metadata\Property;

/**
 * @internal
 */
trait DeprecationMetadataTrait
{
    private $camelCaseToSnakeCaseNameConverter;
    private function withDeprecatedAttributes(\ApiPlatform\Metadata\ApiProperty $propertyMetadata, array $attributes) : \ApiPlatform\Metadata\ApiProperty
    {
    }
}
