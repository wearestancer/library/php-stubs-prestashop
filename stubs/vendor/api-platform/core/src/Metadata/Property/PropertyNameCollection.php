<?php

namespace ApiPlatform\Metadata\Property;

/**
 * A collection of property names for a given resource.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PropertyNameCollection implements \IteratorAggregate, \Countable
{
    /**
     * @param string[] $properties
     */
    public function __construct(array $properties = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function getIterator() : \Traversable
    {
    }
    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function count() : int
    {
    }
}
