<?php

namespace ApiPlatform\Metadata;

abstract class Operation
{
    use \ApiPlatform\Metadata\WithResourceTrait;
    protected $shortName;
    protected $class;
    protected $paginationEnabled;
    protected $paginationType;
    protected $paginationItemsPerPage;
    protected $paginationMaximumItemsPerPage;
    protected $paginationPartial;
    protected $paginationClientEnabled;
    protected $paginationClientItemsPerPage;
    protected $paginationClientPartial;
    protected $paginationFetchJoinCollection;
    protected $paginationUseOutputWalkers;
    protected $paginationViaCursor;
    protected $order;
    protected $description;
    protected $normalizationContext;
    protected $denormalizationContext;
    protected $security;
    protected $securityMessage;
    protected $securityPostDenormalize;
    protected $securityPostDenormalizeMessage;
    protected $securityPostValidation;
    protected $securityPostValidationMessage;
    protected $deprecationReason;
    /**
     * @var string[]
     */
    protected $filters;
    protected $validationContext;
    /**
     * @var null
     */
    protected $input;
    /**
     * @var null
     */
    protected $output;
    /**
     * @var string|array|bool|null
     */
    protected $mercure;
    /**
     * @var string|bool|null
     */
    protected $messenger;
    protected $elasticsearch;
    protected $urlGenerationStrategy;
    protected $read;
    protected $deserialize;
    protected $validate;
    protected $write;
    protected $serialize;
    protected $fetchPartial;
    protected $forceEager;
    protected $priority;
    protected $name;
    /**
     * @var string|callable|null
     */
    protected $provider;
    /**
     * @var string|callable|null
     */
    protected $processor;
    protected $extraProperties;
    public function __construct(?string $shortName = null, ?string $class = null, ?bool $paginationEnabled = null, ?string $paginationType = null, ?int $paginationItemsPerPage = null, ?int $paginationMaximumItemsPerPage = null, ?bool $paginationPartial = null, ?bool $paginationClientEnabled = null, ?bool $paginationClientItemsPerPage = null, ?bool $paginationClientPartial = null, ?bool $paginationFetchJoinCollection = null, ?bool $paginationUseOutputWalkers = null, ?array $paginationViaCursor = null, ?array $order = null, ?string $description = null, ?array $normalizationContext = null, ?array $denormalizationContext = null, ?string $security = null, ?string $securityMessage = null, ?string $securityPostDenormalize = null, ?string $securityPostDenormalizeMessage = null, ?string $securityPostValidation = null, ?string $securityPostValidationMessage = null, ?string $deprecationReason = null, ?array $filters = null, ?array $validationContext = null, $input = null, $output = null, $mercure = null, $messenger = null, ?bool $elasticsearch = null, ?int $urlGenerationStrategy = null, ?bool $read = null, ?bool $deserialize = null, ?bool $validate = null, ?bool $write = null, ?bool $serialize = null, ?bool $fetchPartial = null, ?bool $forceEager = null, ?int $priority = null, ?string $name = null, $provider = null, $processor = null, array $extraProperties = [])
    {
    }
    public function withOperation($operation)
    {
    }
    public function getShortName() : ?string
    {
    }
    public function withShortName(?string $shortName = null) : self
    {
    }
    public function getClass() : ?string
    {
    }
    public function withClass(?string $class = null) : self
    {
    }
    public function getPaginationEnabled() : ?bool
    {
    }
    public function withPaginationEnabled(?bool $paginationEnabled = null) : self
    {
    }
    public function getPaginationType() : ?string
    {
    }
    public function withPaginationType(?string $paginationType = null) : self
    {
    }
    public function getPaginationItemsPerPage() : ?int
    {
    }
    public function withPaginationItemsPerPage(?int $paginationItemsPerPage = null) : self
    {
    }
    public function getPaginationMaximumItemsPerPage() : ?int
    {
    }
    public function withPaginationMaximumItemsPerPage(?int $paginationMaximumItemsPerPage = null) : self
    {
    }
    public function getPaginationPartial() : ?bool
    {
    }
    public function withPaginationPartial(?bool $paginationPartial = null) : self
    {
    }
    public function getPaginationClientEnabled() : ?bool
    {
    }
    public function withPaginationClientEnabled(?bool $paginationClientEnabled = null) : self
    {
    }
    public function getPaginationClientItemsPerPage() : ?bool
    {
    }
    public function withPaginationClientItemsPerPage(?bool $paginationClientItemsPerPage = null) : self
    {
    }
    public function getPaginationClientPartial() : ?bool
    {
    }
    public function withPaginationClientPartial(?bool $paginationClientPartial = null) : self
    {
    }
    public function getPaginationFetchJoinCollection() : ?bool
    {
    }
    public function withPaginationFetchJoinCollection(?bool $paginationFetchJoinCollection = null) : self
    {
    }
    public function getPaginationUseOutputWalkers() : ?bool
    {
    }
    public function withPaginationUseOutputWalkers(?bool $paginationUseOutputWalkers = null) : self
    {
    }
    public function getPaginationViaCursor() : ?array
    {
    }
    public function withPaginationViaCursor(array $paginationViaCursor) : self
    {
    }
    public function getOrder() : ?array
    {
    }
    public function withOrder(array $order = []) : self
    {
    }
    public function getDescription() : ?string
    {
    }
    public function withDescription(?string $description = null) : self
    {
    }
    public function getNormalizationContext() : ?array
    {
    }
    public function withNormalizationContext(array $normalizationContext = []) : self
    {
    }
    public function getDenormalizationContext() : ?array
    {
    }
    public function withDenormalizationContext(array $denormalizationContext = []) : self
    {
    }
    public function getSecurity() : ?string
    {
    }
    public function withSecurity(?string $security = null) : self
    {
    }
    public function getSecurityMessage() : ?string
    {
    }
    public function withSecurityMessage(?string $securityMessage = null) : self
    {
    }
    public function getSecurityPostDenormalize() : ?string
    {
    }
    public function withSecurityPostDenormalize(?string $securityPostDenormalize = null) : self
    {
    }
    public function getSecurityPostDenormalizeMessage() : ?string
    {
    }
    public function withSecurityPostDenormalizeMessage(?string $securityPostDenormalizeMessage = null) : self
    {
    }
    public function getSecurityPostValidation() : ?string
    {
    }
    public function withSecurityPostValidation(?string $securityPostValidation = null) : self
    {
    }
    public function getSecurityPostValidationMessage() : ?string
    {
    }
    public function withSecurityPostValidationMessage(?string $securityPostValidationMessage = null) : self
    {
    }
    public function getDeprecationReason() : ?string
    {
    }
    public function withDeprecationReason(?string $deprecationReason = null) : self
    {
    }
    public function getFilters() : ?array
    {
    }
    public function withFilters(array $filters = []) : self
    {
    }
    public function getValidationContext() : ?array
    {
    }
    public function withValidationContext(array $validationContext = []) : self
    {
    }
    public function getInput()
    {
    }
    public function withInput($input = null) : self
    {
    }
    public function getOutput()
    {
    }
    public function withOutput($output = null) : self
    {
    }
    /**
     * @return bool|string|array|null
     */
    public function getMercure()
    {
    }
    /**
     * @param bool|string|array|null $mercure
     *
     * @return $this
     */
    public function withMercure($mercure = null) : self
    {
    }
    /**
     * @return bool|string|null
     */
    public function getMessenger()
    {
    }
    /**
     * @param bool|string|null $messenger
     *
     * @return $this
     */
    public function withMessenger($messenger = null) : self
    {
    }
    public function getElasticsearch() : ?bool
    {
    }
    public function withElasticsearch(?bool $elasticsearch = null) : self
    {
    }
    public function getUrlGenerationStrategy() : ?int
    {
    }
    public function withUrlGenerationStrategy(?int $urlGenerationStrategy = null) : self
    {
    }
    public function canRead() : ?bool
    {
    }
    public function withRead(bool $read = true) : self
    {
    }
    public function canDeserialize() : ?bool
    {
    }
    public function withDeserialize(bool $deserialize = true) : self
    {
    }
    public function canValidate() : ?bool
    {
    }
    public function withValidate(bool $validate = true) : self
    {
    }
    public function canWrite() : ?bool
    {
    }
    public function withWrite(bool $write = true) : self
    {
    }
    public function canSerialize() : ?bool
    {
    }
    public function withSerialize(bool $serialize = true) : self
    {
    }
    public function getFetchPartial() : ?bool
    {
    }
    public function withFetchPartial(?bool $fetchPartial = null) : self
    {
    }
    public function getForceEager() : ?bool
    {
    }
    public function withForceEager(?bool $forceEager = null) : self
    {
    }
    public function getPriority() : ?int
    {
    }
    public function withPriority(int $priority = 0) : self
    {
    }
    public function getName() : ?string
    {
    }
    public function withName(string $name = '') : self
    {
    }
    /**
     * @return string|callable|null
     */
    public function getProcessor()
    {
    }
    public function withProcessor($processor) : self
    {
    }
    /**
     * @return string|callable|null
     */
    public function getProvider()
    {
    }
    public function withProvider($provider) : self
    {
    }
    public function getExtraProperties() : array
    {
    }
    public function withExtraProperties(array $extraProperties = []) : self
    {
    }
}
