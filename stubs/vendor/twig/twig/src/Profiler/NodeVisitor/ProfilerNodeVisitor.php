<?php

namespace Twig\Profiler\NodeVisitor;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class ProfilerNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
{
    public function __construct(string $extensionName)
    {
    }
    public function enterNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public function leaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : ?\Twig\Node\Node
    {
    }
    public function getPriority() : int
    {
    }
}
