<?php

namespace Twig\Profiler;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Profile implements \IteratorAggregate, \Serializable
{
    public const ROOT = 'ROOT';
    public const BLOCK = 'block';
    public const TEMPLATE = 'template';
    public const MACRO = 'macro';
    public function __construct(string $template = 'main', string $type = self::ROOT, string $name = 'main')
    {
    }
    public function getTemplate() : string
    {
    }
    public function getType() : string
    {
    }
    public function getName() : string
    {
    }
    public function isRoot() : bool
    {
    }
    public function isTemplate() : bool
    {
    }
    public function isBlock() : bool
    {
    }
    public function isMacro() : bool
    {
    }
    /**
     * @return Profile[]
     */
    public function getProfiles() : array
    {
    }
    public function addProfile(self $profile) : void
    {
    }
    /**
     * Returns the duration in microseconds.
     */
    public function getDuration() : float
    {
    }
    /**
     * Returns the memory usage in bytes.
     */
    public function getMemoryUsage() : int
    {
    }
    /**
     * Returns the peak memory usage in bytes.
     */
    public function getPeakMemoryUsage() : int
    {
    }
    /**
     * Starts the profiling.
     */
    public function enter() : void
    {
    }
    /**
     * Stops the profiling.
     */
    public function leave() : void
    {
    }
    public function reset() : void
    {
    }
    public function getIterator() : \Traversable
    {
    }
    public function serialize() : string
    {
    }
    public function unserialize($data) : void
    {
    }
    /**
     * @internal
     */
    public function __serialize() : array
    {
    }
    /**
     * @internal
     */
    public function __unserialize(array $data) : void
    {
    }
}
