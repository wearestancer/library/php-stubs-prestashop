<?php

namespace Twig\Profiler\Dumper;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class BaseDumper
{
    public function dump(\Twig\Profiler\Profile $profile) : string
    {
    }
    protected abstract function formatTemplate(\Twig\Profiler\Profile $profile, $prefix) : string;
    protected abstract function formatNonTemplate(\Twig\Profiler\Profile $profile, $prefix) : string;
    protected abstract function formatTime(\Twig\Profiler\Profile $profile, $percent) : string;
}
