<?php

namespace Twig\Profiler\Node;

/**
 * Represents a profile enter node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class EnterProfileNode extends \Twig\Node\Node
{
    public function __construct(string $extensionName, string $type, string $name, string $varName)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
