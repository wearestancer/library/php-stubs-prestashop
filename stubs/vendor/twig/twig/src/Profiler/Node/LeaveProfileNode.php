<?php

namespace Twig\Profiler\Node;

/**
 * Represents a profile leave node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class LeaveProfileNode extends \Twig\Node\Node
{
    public function __construct(string $varName)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
