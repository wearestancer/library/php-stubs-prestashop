<?php

namespace Twig;

/**
 * Represents a template filter.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @see https://twig.symfony.com/doc/templates.html#filters
 */
final class TwigFilter
{
    /**
     * @param callable|null $callable A callable implementing the filter. If null, you need to overwrite the "node_class" option to customize compilation.
     */
    public function __construct(string $name, $callable = null, array $options = [])
    {
    }
    public function getName() : string
    {
    }
    /**
     * Returns the callable to execute for this filter.
     *
     * @return callable|null
     */
    public function getCallable()
    {
    }
    public function getNodeClass() : string
    {
    }
    public function setArguments(array $arguments) : void
    {
    }
    public function getArguments() : array
    {
    }
    public function needsEnvironment() : bool
    {
    }
    public function needsContext() : bool
    {
    }
    public function getSafe(\Twig\Node\Node $filterArgs) : ?array
    {
    }
    public function getPreservesSafety() : ?array
    {
    }
    public function getPreEscape() : ?string
    {
    }
    public function isVariadic() : bool
    {
    }
    public function isDeprecated() : bool
    {
    }
    public function getDeprecatedVersion() : string
    {
    }
    public function getAlternative() : ?string
    {
    }
}
