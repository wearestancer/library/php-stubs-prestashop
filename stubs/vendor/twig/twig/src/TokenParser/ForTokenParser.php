<?php

namespace Twig\TokenParser;

/**
 * Loops over each item of a sequence.
 *
 *   <ul>
 *    {% for user in users %}
 *      <li>{{ user.username|e }}</li>
 *    {% endfor %}
 *   </ul>
 *
 * @internal
 */
final class ForTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function decideForFork(\Twig\Token $token) : bool
    {
    }
    public function decideForEnd(\Twig\Token $token) : bool
    {
    }
    public function getTag() : string
    {
    }
}
