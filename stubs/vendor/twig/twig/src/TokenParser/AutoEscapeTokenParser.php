<?php

namespace Twig\TokenParser;

/**
 * Marks a section of a template to be escaped or not.
 *
 * @internal
 */
final class AutoEscapeTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function decideBlockEnd(\Twig\Token $token) : bool
    {
    }
    public function getTag() : string
    {
    }
}
