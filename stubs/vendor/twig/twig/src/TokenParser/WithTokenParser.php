<?php

namespace Twig\TokenParser;

/**
 * Creates a nested scope.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class WithTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function decideWithEnd(\Twig\Token $token) : bool
    {
    }
    public function getTag() : string
    {
    }
}
