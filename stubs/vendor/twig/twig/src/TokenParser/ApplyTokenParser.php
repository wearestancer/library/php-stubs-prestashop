<?php

namespace Twig\TokenParser;

/**
 * Applies filters on a section of a template.
 *
 *   {% apply upper %}
 *      This text becomes uppercase
 *   {% endapply %}
 *
 * @internal
 */
final class ApplyTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function decideApplyEnd(\Twig\Token $token) : bool
    {
    }
    public function getTag() : string
    {
    }
}
