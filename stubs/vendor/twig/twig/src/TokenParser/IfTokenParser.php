<?php

namespace Twig\TokenParser;

/**
 * Tests a condition.
 *
 *   {% if users %}
 *    <ul>
 *      {% for user in users %}
 *        <li>{{ user.username|e }}</li>
 *      {% endfor %}
 *    </ul>
 *   {% endif %}
 *
 * @internal
 */
final class IfTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function decideIfFork(\Twig\Token $token) : bool
    {
    }
    public function decideIfEnd(\Twig\Token $token) : bool
    {
    }
    public function getTag() : string
    {
    }
}
