<?php

namespace Twig\TokenParser;

/**
 * Imports macros.
 *
 *   {% import 'forms.html' as forms %}
 *
 * @internal
 */
final class ImportTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function getTag() : string
    {
    }
}
