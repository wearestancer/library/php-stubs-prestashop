<?php

namespace Twig\TokenParser;

/**
 * Embeds a template.
 *
 * @internal
 */
final class EmbedTokenParser extends \Twig\TokenParser\IncludeTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    public function decideBlockEnd(\Twig\Token $token) : bool
    {
    }
    public function getTag() : string
    {
    }
}
