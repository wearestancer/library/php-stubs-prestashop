<?php

namespace Twig\TokenParser;

/**
 * Includes a template.
 *
 *   {% include 'header.html' %}
 *     Body
 *   {% include 'footer.html' %}
 *
 * @internal
 */
class IncludeTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    public function parse(\Twig\Token $token) : \Twig\Node\Node
    {
    }
    protected function parseArguments()
    {
    }
    public function getTag() : string
    {
    }
}
