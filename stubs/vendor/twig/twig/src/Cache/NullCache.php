<?php

namespace Twig\Cache;

/**
 * Implements a no-cache strategy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class NullCache implements \Twig\Cache\CacheInterface
{
    public function generateKey(string $name, string $className) : string
    {
    }
    public function write(string $key, string $content) : void
    {
    }
    public function load(string $key) : void
    {
    }
    public function getTimestamp(string $key) : int
    {
    }
}
