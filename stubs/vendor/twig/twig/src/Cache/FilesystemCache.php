<?php

namespace Twig\Cache;

/**
 * Implements a cache on the filesystem.
 *
 * @author Andrew Tch <andrew@noop.lv>
 */
class FilesystemCache implements \Twig\Cache\CacheInterface
{
    public const FORCE_BYTECODE_INVALIDATION = 1;
    public function __construct(string $directory, int $options = 0)
    {
    }
    public function generateKey(string $name, string $className) : string
    {
    }
    public function load(string $key) : void
    {
    }
    public function write(string $key, string $content) : void
    {
    }
    public function getTimestamp(string $key) : int
    {
    }
}
