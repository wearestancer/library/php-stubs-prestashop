<?php

namespace Twig;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Parser
{
    public function __construct(\Twig\Environment $env)
    {
    }
    public function getVarName() : string
    {
    }
    public function parse(\Twig\TokenStream $stream, $test = null, bool $dropNeedle = false) : \Twig\Node\ModuleNode
    {
    }
    public function subparse($test, bool $dropNeedle = false) : \Twig\Node\Node
    {
    }
    public function getBlockStack() : array
    {
    }
    public function peekBlockStack()
    {
    }
    public function popBlockStack() : void
    {
    }
    public function pushBlockStack($name) : void
    {
    }
    public function hasBlock(string $name) : bool
    {
    }
    public function getBlock(string $name) : \Twig\Node\Node
    {
    }
    public function setBlock(string $name, \Twig\Node\BlockNode $value) : void
    {
    }
    public function hasMacro(string $name) : bool
    {
    }
    public function setMacro(string $name, \Twig\Node\MacroNode $node) : void
    {
    }
    public function addTrait($trait) : void
    {
    }
    public function hasTraits() : bool
    {
    }
    public function embedTemplate(\Twig\Node\ModuleNode $template)
    {
    }
    public function addImportedSymbol(string $type, string $alias, string $name = null, \Twig\Node\Expression\AbstractExpression $node = null) : void
    {
    }
    public function getImportedSymbol(string $type, string $alias)
    {
    }
    public function isMainScope() : bool
    {
    }
    public function pushLocalScope() : void
    {
    }
    public function popLocalScope() : void
    {
    }
    public function getExpressionParser() : \Twig\ExpressionParser
    {
    }
    public function getParent() : ?\Twig\Node\Node
    {
    }
    public function setParent(?\Twig\Node\Node $parent) : void
    {
    }
    public function getStream() : \Twig\TokenStream
    {
    }
    public function getCurrentToken() : \Twig\Token
    {
    }
}
