<?php

namespace Twig;

/**
 * Represents a template test.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @see https://twig.symfony.com/doc/templates.html#test-operator
 */
final class TwigTest
{
    /**
     * @param callable|null $callable A callable implementing the test. If null, you need to overwrite the "node_class" option to customize compilation.
     */
    public function __construct(string $name, $callable = null, array $options = [])
    {
    }
    public function getName() : string
    {
    }
    /**
     * Returns the callable to execute for this test.
     *
     * @return callable|null
     */
    public function getCallable()
    {
    }
    public function getNodeClass() : string
    {
    }
    public function setArguments(array $arguments) : void
    {
    }
    public function getArguments() : array
    {
    }
    public function isVariadic() : bool
    {
    }
    public function isDeprecated() : bool
    {
    }
    public function getDeprecatedVersion() : string
    {
    }
    public function getAlternative() : ?string
    {
    }
    public function hasOneMandatoryArgument() : bool
    {
    }
}
