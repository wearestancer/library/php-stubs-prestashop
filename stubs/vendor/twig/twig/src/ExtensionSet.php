<?php

namespace Twig;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class ExtensionSet
{
    public function __construct()
    {
    }
    public function initRuntime()
    {
    }
    public function hasExtension(string $class) : bool
    {
    }
    public function getExtension(string $class) : \Twig\Extension\ExtensionInterface
    {
    }
    /**
     * @param ExtensionInterface[] $extensions
     */
    public function setExtensions(array $extensions) : void
    {
    }
    /**
     * @return ExtensionInterface[]
     */
    public function getExtensions() : array
    {
    }
    public function getSignature() : string
    {
    }
    public function isInitialized() : bool
    {
    }
    public function getLastModified() : int
    {
    }
    public function addExtension(\Twig\Extension\ExtensionInterface $extension) : void
    {
    }
    public function addFunction(\Twig\TwigFunction $function) : void
    {
    }
    /**
     * @return TwigFunction[]
     */
    public function getFunctions() : array
    {
    }
    public function getFunction(string $name) : ?\Twig\TwigFunction
    {
    }
    public function registerUndefinedFunctionCallback(callable $callable) : void
    {
    }
    public function addFilter(\Twig\TwigFilter $filter) : void
    {
    }
    /**
     * @return TwigFilter[]
     */
    public function getFilters() : array
    {
    }
    public function getFilter(string $name) : ?\Twig\TwigFilter
    {
    }
    public function registerUndefinedFilterCallback(callable $callable) : void
    {
    }
    public function addNodeVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor) : void
    {
    }
    /**
     * @return NodeVisitorInterface[]
     */
    public function getNodeVisitors() : array
    {
    }
    public function addTokenParser(\Twig\TokenParser\TokenParserInterface $parser) : void
    {
    }
    /**
     * @return TokenParserInterface[]
     */
    public function getTokenParsers() : array
    {
    }
    public function getTokenParser(string $name) : ?\Twig\TokenParser\TokenParserInterface
    {
    }
    public function registerUndefinedTokenParserCallback(callable $callable) : void
    {
    }
    public function getGlobals() : array
    {
    }
    public function addTest(\Twig\TwigTest $test) : void
    {
    }
    /**
     * @return TwigTest[]
     */
    public function getTests() : array
    {
    }
    public function getTest(string $name) : ?\Twig\TwigTest
    {
    }
    public function getUnaryOperators() : array
    {
    }
    public function getBinaryOperators() : array
    {
    }
}
