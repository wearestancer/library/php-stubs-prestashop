<?php

namespace Twig\Loader;

/**
 * Loads template from the filesystem.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FilesystemLoader implements \Twig\Loader\LoaderInterface
{
    /** Identifier of the main namespace. */
    public const MAIN_NAMESPACE = '__main__';
    protected $paths = [];
    protected $cache = [];
    protected $errorCache = [];
    /**
     * @param string|array $paths    A path or an array of paths where to look for templates
     * @param string|null  $rootPath The root path common to all relative paths (null for getcwd())
     */
    public function __construct($paths = [], string $rootPath = null)
    {
    }
    /**
     * Returns the paths to the templates.
     */
    public function getPaths(string $namespace = self::MAIN_NAMESPACE) : array
    {
    }
    /**
     * Returns the path namespaces.
     *
     * The main namespace is always defined.
     */
    public function getNamespaces() : array
    {
    }
    /**
     * @param string|array $paths A path or an array of paths where to look for templates
     */
    public function setPaths($paths, string $namespace = self::MAIN_NAMESPACE) : void
    {
    }
    /**
     * @throws LoaderError
     */
    public function addPath(string $path, string $namespace = self::MAIN_NAMESPACE) : void
    {
    }
    /**
     * @throws LoaderError
     */
    public function prependPath(string $path, string $namespace = self::MAIN_NAMESPACE) : void
    {
    }
    public function getSourceContext(string $name) : \Twig\Source
    {
    }
    public function getCacheKey(string $name) : string
    {
    }
    /**
     * @return bool
     */
    public function exists(string $name)
    {
    }
    public function isFresh(string $name, int $time) : bool
    {
    }
    /**
     * @return string|null
     */
    protected function findTemplate(string $name, bool $throw = true)
    {
    }
}
