<?php

namespace Twig\Loader;

/**
 * Loads templates from other loaders.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class ChainLoader implements \Twig\Loader\LoaderInterface
{
    /**
     * @param LoaderInterface[] $loaders
     */
    public function __construct(array $loaders = [])
    {
    }
    public function addLoader(\Twig\Loader\LoaderInterface $loader) : void
    {
    }
    /**
     * @return LoaderInterface[]
     */
    public function getLoaders() : array
    {
    }
    public function getSourceContext(string $name) : \Twig\Source
    {
    }
    public function exists(string $name) : bool
    {
    }
    public function getCacheKey(string $name) : string
    {
    }
    public function isFresh(string $name, int $time) : bool
    {
    }
}
