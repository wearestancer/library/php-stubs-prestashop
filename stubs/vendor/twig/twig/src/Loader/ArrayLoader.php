<?php

namespace Twig\Loader;

/**
 * Loads a template from an array.
 *
 * When using this loader with a cache mechanism, you should know that a new cache
 * key is generated each time a template content "changes" (the cache key being the
 * source code of the template). If you don't want to see your cache grows out of
 * control, you need to take care of clearing the old cache file by yourself.
 *
 * This loader should only be used for unit testing.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class ArrayLoader implements \Twig\Loader\LoaderInterface
{
    /**
     * @param array $templates An array of templates (keys are the names, and values are the source code)
     */
    public function __construct(array $templates = [])
    {
    }
    public function setTemplate(string $name, string $template) : void
    {
    }
    public function getSourceContext(string $name) : \Twig\Source
    {
    }
    public function exists(string $name) : bool
    {
    }
    public function getCacheKey(string $name) : string
    {
    }
    public function isFresh(string $name, int $time) : bool
    {
    }
}
