<?php

namespace Twig;

/**
 * A node traverser.
 *
 * It visits all nodes and their children and calls the given visitor for each.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class NodeTraverser
{
    /**
     * @param NodeVisitorInterface[] $visitors
     */
    public function __construct(\Twig\Environment $env, array $visitors = [])
    {
    }
    public function addVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor) : void
    {
    }
    /**
     * Traverses a node and calls the registered visitors.
     */
    public function traverse(\Twig\Node\Node $node) : \Twig\Node\Node
    {
    }
}
