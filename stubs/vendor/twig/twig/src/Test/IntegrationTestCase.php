<?php

namespace Twig\Test;

/**
 * Integration test helper.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Karma Dordrak <drak@zikula.org>
 */
abstract class IntegrationTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @return string
     */
    protected abstract function getFixturesDir();
    /**
     * @return RuntimeLoaderInterface[]
     */
    protected function getRuntimeLoaders()
    {
    }
    /**
     * @return ExtensionInterface[]
     */
    protected function getExtensions()
    {
    }
    /**
     * @return TwigFilter[]
     */
    protected function getTwigFilters()
    {
    }
    /**
     * @return TwigFunction[]
     */
    protected function getTwigFunctions()
    {
    }
    /**
     * @return TwigTest[]
     */
    protected function getTwigTests()
    {
    }
    /**
     * @dataProvider getTests
     */
    public function testIntegration($file, $message, $condition, $templates, $exception, $outputs, $deprecation = '')
    {
    }
    /**
     * @dataProvider getLegacyTests
     * @group legacy
     */
    public function testLegacyIntegration($file, $message, $condition, $templates, $exception, $outputs, $deprecation = '')
    {
    }
    public function getTests($name, $legacyTests = false)
    {
    }
    public function getLegacyTests()
    {
    }
    protected function doIntegrationTest($file, $message, $condition, $templates, $exception, $outputs, $deprecation = '')
    {
    }
    protected static function parseTemplates($test)
    {
    }
}
