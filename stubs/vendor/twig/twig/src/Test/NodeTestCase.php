<?php

namespace Twig\Test;

abstract class NodeTestCase extends \PHPUnit\Framework\TestCase
{
    public abstract function getTests();
    /**
     * @dataProvider getTests
     */
    public function testCompile($node, $source, $environment = null, $isPattern = false)
    {
    }
    public function assertNodeCompilation($source, \Twig\Node\Node $node, \Twig\Environment $environment = null, $isPattern = false)
    {
    }
    protected function getCompiler(\Twig\Environment $environment = null)
    {
    }
    protected function getEnvironment()
    {
    }
    protected function getVariableGetter($name, $line = false)
    {
    }
    protected function getAttributeGetter()
    {
    }
}
