<?php

namespace Twig;

/**
 * Represents a token stream.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class TokenStream
{
    public function __construct(array $tokens, \Twig\Source $source = null)
    {
    }
    public function __toString()
    {
    }
    public function injectTokens(array $tokens)
    {
    }
    /**
     * Sets the pointer to the next token and returns the old one.
     */
    public function next() : \Twig\Token
    {
    }
    /**
     * Tests a token, sets the pointer to the next one and returns it or throws a syntax error.
     *
     * @return Token|null The next token if the condition is true, null otherwise
     */
    public function nextIf($primary, $secondary = null)
    {
    }
    /**
     * Tests a token and returns it or throws a syntax error.
     */
    public function expect($type, $value = null, string $message = null) : \Twig\Token
    {
    }
    /**
     * Looks at the next token.
     */
    public function look(int $number = 1) : \Twig\Token
    {
    }
    /**
     * Tests the current token.
     */
    public function test($primary, $secondary = null) : bool
    {
    }
    /**
     * Checks if end of stream was reached.
     */
    public function isEOF() : bool
    {
    }
    public function getCurrent() : \Twig\Token
    {
    }
    /**
     * Gets the source associated with this stream.
     *
     * @internal
     */
    public function getSourceContext() : \Twig\Source
    {
    }
}
