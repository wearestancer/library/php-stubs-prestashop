<?php

namespace Twig;

/**
 * Parses expressions.
 *
 * This parser implements a "Precedence climbing" algorithm.
 *
 * @see https://www.engr.mun.ca/~theo/Misc/exp_parsing.htm
 * @see https://en.wikipedia.org/wiki/Operator-precedence_parser
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class ExpressionParser
{
    public const OPERATOR_LEFT = 1;
    public const OPERATOR_RIGHT = 2;
    public function __construct(\Twig\Parser $parser, \Twig\Environment $env)
    {
    }
    public function parseExpression($precedence = 0, $allowArrow = false)
    {
    }
    public function parsePrimaryExpression()
    {
    }
    public function parseStringExpression()
    {
    }
    public function parseArrayExpression()
    {
    }
    public function parseHashExpression()
    {
    }
    public function parsePostfixExpression($node)
    {
    }
    public function getFunctionNode($name, $line)
    {
    }
    public function parseSubscriptExpression($node)
    {
    }
    public function parseFilterExpression($node)
    {
    }
    public function parseFilterExpressionRaw($node, $tag = null)
    {
    }
    /**
     * Parses arguments.
     *
     * @param bool $namedArguments Whether to allow named arguments or not
     * @param bool $definition     Whether we are parsing arguments for a function definition
     *
     * @return Node
     *
     * @throws SyntaxError
     */
    public function parseArguments($namedArguments = false, $definition = false, $allowArrow = false)
    {
    }
    public function parseAssignmentExpression()
    {
    }
    public function parseMultitargetExpression()
    {
    }
}
