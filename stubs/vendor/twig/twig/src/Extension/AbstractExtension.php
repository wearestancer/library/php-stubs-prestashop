<?php

namespace Twig\Extension;

abstract class AbstractExtension implements \Twig\Extension\ExtensionInterface
{
    public function getTokenParsers()
    {
    }
    public function getNodeVisitors()
    {
    }
    public function getFilters()
    {
    }
    public function getTests()
    {
    }
    public function getFunctions()
    {
    }
    public function getOperators()
    {
    }
}
