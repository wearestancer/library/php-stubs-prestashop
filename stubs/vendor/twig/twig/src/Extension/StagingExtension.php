<?php

namespace Twig\Extension;

/**
 * Used by \Twig\Environment as a staging area.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class StagingExtension extends \Twig\Extension\AbstractExtension
{
    public function addFunction(\Twig\TwigFunction $function) : void
    {
    }
    public function getFunctions() : array
    {
    }
    public function addFilter(\Twig\TwigFilter $filter) : void
    {
    }
    public function getFilters() : array
    {
    }
    public function addNodeVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor) : void
    {
    }
    public function getNodeVisitors() : array
    {
    }
    public function addTokenParser(\Twig\TokenParser\TokenParserInterface $parser) : void
    {
    }
    public function getTokenParsers() : array
    {
    }
    public function addTest(\Twig\TwigTest $test) : void
    {
    }
    public function getTests() : array
    {
    }
}
