<?php

namespace Twig\Extension;

class ProfilerExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Twig\Profiler\Profile $profile)
    {
    }
    /**
     * @return void
     */
    public function enter(\Twig\Profiler\Profile $profile)
    {
    }
    /**
     * @return void
     */
    public function leave(\Twig\Profiler\Profile $profile)
    {
    }
    public function getNodeVisitors() : array
    {
    }
}
