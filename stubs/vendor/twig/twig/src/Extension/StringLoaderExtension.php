<?php

namespace Twig\Extension {
    final class StringLoaderExtension extends \Twig\Extension\AbstractExtension
    {
        public function getFunctions() : array
        {
        }
    }
}
namespace {
    /**
     * Loads a template from a string.
     *
     *     {{ include(template_from_string("Hello {{ name }}")) }}
     *
     * @param string $template A template as a string or object implementing __toString()
     * @param string $name     An optional name of the template to be used in error messages
     */
    function twig_template_from_string(\Twig\Environment $env, $template, string $name = \null) : \Twig\TemplateWrapper
    {
    }
}
