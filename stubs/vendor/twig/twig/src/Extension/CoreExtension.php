<?php

namespace Twig\Extension {
    final class CoreExtension extends \Twig\Extension\AbstractExtension
    {
        /**
         * Sets the default format to be used by the date filter.
         *
         * @param string $format             The default date format string
         * @param string $dateIntervalFormat The default date interval format string
         */
        public function setDateFormat($format = null, $dateIntervalFormat = null)
        {
        }
        /**
         * Gets the default format to be used by the date filter.
         *
         * @return array The default date format string and the default date interval format string
         */
        public function getDateFormat()
        {
        }
        /**
         * Sets the default timezone to be used by the date filter.
         *
         * @param \DateTimeZone|string $timezone The default timezone string or a \DateTimeZone object
         */
        public function setTimezone($timezone)
        {
        }
        /**
         * Gets the default timezone to be used by the date filter.
         *
         * @return \DateTimeZone The default timezone currently in use
         */
        public function getTimezone()
        {
        }
        /**
         * Sets the default format to be used by the number_format filter.
         *
         * @param int    $decimal      the number of decimal places to use
         * @param string $decimalPoint the character(s) to use for the decimal point
         * @param string $thousandSep  the character(s) to use for the thousands separator
         */
        public function setNumberFormat($decimal, $decimalPoint, $thousandSep)
        {
        }
        /**
         * Get the default format used by the number_format filter.
         *
         * @return array The arguments for number_format()
         */
        public function getNumberFormat()
        {
        }
        public function getTokenParsers() : array
        {
        }
        public function getFilters() : array
        {
        }
        public function getFunctions() : array
        {
        }
        public function getTests() : array
        {
        }
        public function getNodeVisitors() : array
        {
        }
        public function getOperators() : array
        {
        }
    }
}
namespace {
    /**
     * Cycles over a value.
     *
     * @param \ArrayAccess|array $values
     * @param int                $position The cycle position
     *
     * @return string The next value in the cycle
     */
    function twig_cycle($values, $position)
    {
    }
    /**
     * Returns a random value depending on the supplied parameter type:
     * - a random item from a \Traversable or array
     * - a random character from a string
     * - a random integer between 0 and the integer parameter.
     *
     * @param \Traversable|array|int|float|string $values The values to pick a random item from
     * @param int|null                            $max    Maximum value used when $values is an int
     *
     * @throws RuntimeError when $values is an empty array (does not apply to an empty string which is returned as is)
     *
     * @return mixed A random value from the given sequence
     */
    function twig_random(\Twig\Environment $env, $values = \null, $max = \null)
    {
    }
    /**
     * Converts a date to the given format.
     *
     *   {{ post.published_at|date("m/d/Y") }}
     *
     * @param \DateTimeInterface|\DateInterval|string $date     A date
     * @param string|null                             $format   The target format, null to use the default
     * @param \DateTimeZone|string|false|null         $timezone The target timezone, null to use the default, false to leave unchanged
     *
     * @return string The formatted date
     */
    function twig_date_format_filter(\Twig\Environment $env, $date, $format = \null, $timezone = \null)
    {
    }
    /**
     * Returns a new date object modified.
     *
     *   {{ post.published_at|date_modify("-1day")|date("m/d/Y") }}
     *
     * @param \DateTimeInterface|string $date     A date
     * @param string                    $modifier A modifier string
     *
     * @return \DateTimeInterface
     */
    function twig_date_modify_filter(\Twig\Environment $env, $date, $modifier)
    {
    }
    /**
     * Returns a formatted string.
     *
     * @param string|null $format
     * @param ...$values
     *
     * @return string
     */
    function twig_sprintf($format, ...$values)
    {
    }
    /**
     * Converts an input to a \DateTime instance.
     *
     *    {% if date(user.created_at) < date('+2days') %}
     *      {# do something #}
     *    {% endif %}
     *
     * @param \DateTimeInterface|string|null  $date     A date or null to use the current time
     * @param \DateTimeZone|string|false|null $timezone The target timezone, null to use the default, false to leave unchanged
     *
     * @return \DateTimeInterface
     */
    function twig_date_converter(\Twig\Environment $env, $date = \null, $timezone = \null)
    {
    }
    /**
     * Replaces strings within a string.
     *
     * @param string|null        $str  String to replace in
     * @param array|\Traversable $from Replace values
     *
     * @return string
     */
    function twig_replace_filter($str, $from)
    {
    }
    /**
     * Rounds a number.
     *
     * @param int|float|string|null $value     The value to round
     * @param int|float             $precision The rounding precision
     * @param string                $method    The method to use for rounding
     *
     * @return int|float The rounded number
     */
    function twig_round($value, $precision = 0, $method = 'common')
    {
    }
    /**
     * Number format filter.
     *
     * All of the formatting options can be left null, in that case the defaults will
     * be used. Supplying any of the parameters will override the defaults set in the
     * environment object.
     *
     * @param mixed  $number       A float/int/string of the number to format
     * @param int    $decimal      the number of decimal points to display
     * @param string $decimalPoint the character(s) to use for the decimal point
     * @param string $thousandSep  the character(s) to use for the thousands separator
     *
     * @return string The formatted number
     */
    function twig_number_format_filter(\Twig\Environment $env, $number, $decimal = \null, $decimalPoint = \null, $thousandSep = \null)
    {
    }
    /**
     * URL encodes (RFC 3986) a string as a path segment or an array as a query string.
     *
     * @param string|array|null $url A URL or an array of query parameters
     *
     * @return string The URL encoded value
     */
    function twig_urlencode_filter($url)
    {
    }
    /**
     * Merges an array with another one.
     *
     *  {% set items = { 'apple': 'fruit', 'orange': 'fruit' } %}
     *
     *  {% set items = items|merge({ 'peugeot': 'car' }) %}
     *
     *  {# items now contains { 'apple': 'fruit', 'orange': 'fruit', 'peugeot': 'car' } #}
     *
     * @param array|\Traversable $arr1 An array
     * @param array|\Traversable $arr2 An array
     *
     * @return array The merged array
     */
    function twig_array_merge($arr1, $arr2)
    {
    }
    /**
     * Slices a variable.
     *
     * @param mixed $item         A variable
     * @param int   $start        Start of the slice
     * @param int   $length       Size of the slice
     * @param bool  $preserveKeys Whether to preserve key or not (when the input is an array)
     *
     * @return mixed The sliced variable
     */
    function twig_slice(\Twig\Environment $env, $item, $start, $length = \null, $preserveKeys = \false)
    {
    }
    /**
     * Returns the first element of the item.
     *
     * @param mixed $item A variable
     *
     * @return mixed The first element of the item
     */
    function twig_first(\Twig\Environment $env, $item)
    {
    }
    /**
     * Returns the last element of the item.
     *
     * @param mixed $item A variable
     *
     * @return mixed The last element of the item
     */
    function twig_last(\Twig\Environment $env, $item)
    {
    }
    /**
     * Joins the values to a string.
     *
     * The separators between elements are empty strings per default, you can define them with the optional parameters.
     *
     *  {{ [1, 2, 3]|join(', ', ' and ') }}
     *  {# returns 1, 2 and 3 #}
     *
     *  {{ [1, 2, 3]|join('|') }}
     *  {# returns 1|2|3 #}
     *
     *  {{ [1, 2, 3]|join }}
     *  {# returns 123 #}
     *
     * @param array       $value An array
     * @param string      $glue  The separator
     * @param string|null $and   The separator for the last pair
     *
     * @return string The concatenated string
     */
    function twig_join_filter($value, $glue = '', $and = \null)
    {
    }
    /**
     * Splits the string into an array.
     *
     *  {{ "one,two,three"|split(',') }}
     *  {# returns [one, two, three] #}
     *
     *  {{ "one,two,three,four,five"|split(',', 3) }}
     *  {# returns [one, two, "three,four,five"] #}
     *
     *  {{ "123"|split('') }}
     *  {# returns [1, 2, 3] #}
     *
     *  {{ "aabbcc"|split('', 2) }}
     *  {# returns [aa, bb, cc] #}
     *
     * @param string|null $value     A string
     * @param string      $delimiter The delimiter
     * @param int         $limit     The limit
     *
     * @return array The split string as an array
     */
    function twig_split_filter(\Twig\Environment $env, $value, $delimiter, $limit = \null)
    {
    }
    // The '_default' filter is used internally to avoid using the ternary operator
    // which costs a lot for big contexts (before PHP 5.4). So, on average,
    // a function call is cheaper.
    /**
     * @internal
     */
    function _twig_default_filter($value, $default = '')
    {
    }
    /**
     * Returns the keys for the given array.
     *
     * It is useful when you want to iterate over the keys of an array:
     *
     *  {% for key in array|keys %}
     *      {# ... #}
     *  {% endfor %}
     *
     * @param array $array An array
     *
     * @return array The keys
     */
    function twig_get_array_keys_filter($array)
    {
    }
    /**
     * Reverses a variable.
     *
     * @param array|\Traversable|string|null $item         An array, a \Traversable instance, or a string
     * @param bool                           $preserveKeys Whether to preserve key or not
     *
     * @return mixed The reversed input
     */
    function twig_reverse_filter(\Twig\Environment $env, $item, $preserveKeys = \false)
    {
    }
    /**
     * Sorts an array.
     *
     * @param array|\Traversable $array
     *
     * @return array
     */
    function twig_sort_filter(\Twig\Environment $env, $array, $arrow = \null)
    {
    }
    /**
     * @internal
     */
    function twig_in_filter($value, $compare)
    {
    }
    /**
     * Compares two values using a more strict version of the PHP non-strict comparison operator.
     *
     * @see https://wiki.php.net/rfc/string_to_number_comparison
     * @see https://wiki.php.net/rfc/trailing_whitespace_numerics
     *
     * @internal
     */
    function twig_compare($a, $b)
    {
    }
    /**
     * Returns a trimmed string.
     *
     * @param string|null $string
     * @param string|null $characterMask
     * @param string      $side
     *
     * @return string
     *
     * @throws RuntimeError When an invalid trimming side is used (not a string or not 'left', 'right', or 'both')
     */
    function twig_trim_filter($string, $characterMask = \null, $side = 'both')
    {
    }
    /**
     * Inserts HTML line breaks before all newlines in a string.
     *
     * @param string|null $string
     *
     * @return string
     */
    function twig_nl2br($string)
    {
    }
    /**
     * Removes whitespaces between HTML tags.
     *
     * @param string|null $string
     *
     * @return string
     */
    function twig_spaceless($content)
    {
    }
    /**
     * @param string|null $string
     * @param string      $to
     * @param string      $from
     *
     * @return string
     */
    function twig_convert_encoding($string, $to, $from)
    {
    }
    /**
     * Returns the length of a variable.
     *
     * @param mixed $thing A variable
     *
     * @return int The length of the value
     */
    function twig_length_filter(\Twig\Environment $env, $thing)
    {
    }
    /**
     * Converts a string to uppercase.
     *
     * @param string|null $string A string
     *
     * @return string The uppercased string
     */
    function twig_upper_filter(\Twig\Environment $env, $string)
    {
    }
    /**
     * Converts a string to lowercase.
     *
     * @param string|null $string A string
     *
     * @return string The lowercased string
     */
    function twig_lower_filter(\Twig\Environment $env, $string)
    {
    }
    /**
     * Strips HTML and PHP tags from a string.
     *
     * @param string|null $string
     * @param string[]|string|null $string
     *
     * @return string
     */
    function twig_striptags($string, $allowable_tags = \null)
    {
    }
    /**
     * Returns a titlecased string.
     *
     * @param string|null $string A string
     *
     * @return string The titlecased string
     */
    function twig_title_string_filter(\Twig\Environment $env, $string)
    {
    }
    /**
     * Returns a capitalized string.
     *
     * @param string|null $string A string
     *
     * @return string The capitalized string
     */
    function twig_capitalize_string_filter(\Twig\Environment $env, $string)
    {
    }
    /**
     * @internal
     */
    function twig_call_macro(\Twig\Template $template, string $method, array $args, int $lineno, array $context, \Twig\Source $source)
    {
    }
    /**
     * @internal
     */
    function twig_ensure_traversable($seq)
    {
    }
    /**
     * @internal
     */
    function twig_to_array($seq, $preserveKeys = \true)
    {
    }
    /**
     * Checks if a variable is empty.
     *
     *    {# evaluates to true if the foo variable is null, false, or the empty string #}
     *    {% if foo is empty %}
     *        {# ... #}
     *    {% endif %}
     *
     * @param mixed $value A variable
     *
     * @return bool true if the value is empty, false otherwise
     */
    function twig_test_empty($value)
    {
    }
    /**
     * Checks if a variable is traversable.
     *
     *    {# evaluates to true if the foo variable is an array or a traversable object #}
     *    {% if foo is iterable %}
     *        {# ... #}
     *    {% endif %}
     *
     * @param mixed $value A variable
     *
     * @return bool true if the value is traversable
     */
    function twig_test_iterable($value)
    {
    }
    /**
     * Renders a template.
     *
     * @param array        $context
     * @param string|array $template      The template to render or an array of templates to try consecutively
     * @param array        $variables     The variables to pass to the template
     * @param bool         $withContext
     * @param bool         $ignoreMissing Whether to ignore missing templates or not
     * @param bool         $sandboxed     Whether to sandbox the template or not
     *
     * @return string The rendered template
     */
    function twig_include(\Twig\Environment $env, $context, $template, $variables = [], $withContext = \true, $ignoreMissing = \false, $sandboxed = \false)
    {
    }
    /**
     * Returns a template content without rendering it.
     *
     * @param string $name          The template name
     * @param bool   $ignoreMissing Whether to ignore missing templates or not
     *
     * @return string The template source
     */
    function twig_source(\Twig\Environment $env, $name, $ignoreMissing = \false)
    {
    }
    /**
     * Provides the ability to get constants from instances as well as class/global constants.
     *
     * @param string      $constant The name of the constant
     * @param object|null $object   The object to get the constant from
     *
     * @return string
     */
    function twig_constant($constant, $object = \null)
    {
    }
    /**
     * Checks if a constant exists.
     *
     * @param string      $constant The name of the constant
     * @param object|null $object   The object to get the constant from
     *
     * @return bool
     */
    function twig_constant_is_defined($constant, $object = \null)
    {
    }
    /**
     * Batches item.
     *
     * @param array $items An array of items
     * @param int   $size  The size of the batch
     * @param mixed $fill  A value used to fill missing items
     *
     * @return array
     */
    function twig_array_batch($items, $size, $fill = \null, $preserveKeys = \true)
    {
    }
    /**
     * Returns the attribute value for a given array/object.
     *
     * @param mixed  $object            The object or array from where to get the item
     * @param mixed  $item              The item to get from the array or object
     * @param array  $arguments         An array of arguments to pass if the item is an object method
     * @param string $type              The type of attribute (@see \Twig\Template constants)
     * @param bool   $isDefinedTest     Whether this is only a defined check
     * @param bool   $ignoreStrictCheck Whether to ignore the strict attribute check or not
     * @param int    $lineno            The template line where the attribute was called
     *
     * @return mixed The attribute value, or a Boolean when $isDefinedTest is true, or null when the attribute is not set and $ignoreStrictCheck is true
     *
     * @throws RuntimeError if the attribute does not exist and Twig is running in strict mode and $isDefinedTest is false
     *
     * @internal
     */
    function twig_get_attribute(\Twig\Environment $env, \Twig\Source $source, $object, $item, array $arguments = [], $type = 'any', $isDefinedTest = \false, $ignoreStrictCheck = \false, $sandboxed = \false, int $lineno = -1)
    {
    }
    /**
     * Returns the values from a single column in the input array.
     *
     * <pre>
     *  {% set items = [{ 'fruit' : 'apple'}, {'fruit' : 'orange' }] %}
     *
     *  {% set fruits = items|column('fruit') %}
     *
     *  {# fruits now contains ['apple', 'orange'] #}
     * </pre>
     *
     * @param array|Traversable $array An array
     * @param mixed             $name  The column name
     * @param mixed             $index The column to use as the index/keys for the returned array
     *
     * @return array The array of values
     */
    function twig_array_column($array, $name, $index = \null) : array
    {
    }
    function twig_array_filter(\Twig\Environment $env, $array, $arrow)
    {
    }
    function twig_array_map(\Twig\Environment $env, $array, $arrow)
    {
    }
    function twig_array_reduce(\Twig\Environment $env, $array, $arrow, $initial = \null)
    {
    }
    function twig_check_arrow_in_sandbox(\Twig\Environment $env, $arrow, $thing, $type)
    {
    }
}
