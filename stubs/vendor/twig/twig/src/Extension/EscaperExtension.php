<?php

namespace Twig\Extension {
    final class EscaperExtension extends \Twig\Extension\AbstractExtension
    {
        /** @internal */
        public $safeClasses = [];
        /** @internal */
        public $safeLookup = [];
        /**
         * @param string|false|callable $defaultStrategy An escaping strategy
         *
         * @see setDefaultStrategy()
         */
        public function __construct($defaultStrategy = 'html')
        {
        }
        public function getTokenParsers() : array
        {
        }
        public function getNodeVisitors() : array
        {
        }
        public function getFilters() : array
        {
        }
        /**
         * Sets the default strategy to use when not defined by the user.
         *
         * The strategy can be a valid PHP callback that takes the template
         * name as an argument and returns the strategy to use.
         *
         * @param string|false|callable $defaultStrategy An escaping strategy
         */
        public function setDefaultStrategy($defaultStrategy) : void
        {
        }
        /**
         * Gets the default strategy to use when not defined by the user.
         *
         * @param string $name The template name
         *
         * @return string|false The default strategy to use for the template
         */
        public function getDefaultStrategy(string $name)
        {
        }
        /**
         * Defines a new escaper to be used via the escape filter.
         *
         * @param string   $strategy The strategy name that should be used as a strategy in the escape call
         * @param callable $callable A valid PHP callable
         */
        public function setEscaper($strategy, callable $callable)
        {
        }
        /**
         * Gets all defined escapers.
         *
         * @return callable[] An array of escapers
         */
        public function getEscapers()
        {
        }
        public function setSafeClasses(array $safeClasses = [])
        {
        }
        public function addSafeClass(string $class, array $strategies)
        {
        }
    }
}
namespace {
    /**
     * Marks a variable as being safe.
     *
     * @param string $string A PHP variable
     */
    function twig_raw_filter($string)
    {
    }
    /**
     * Escapes a string.
     *
     * @param mixed  $string     The value to be escaped
     * @param string $strategy   The escaping strategy
     * @param string $charset    The charset
     * @param bool   $autoescape Whether the function is called by the auto-escaping feature (true) or by the developer (false)
     *
     * @return string
     */
    function twig_escape_filter(\Twig\Environment $env, $string, $strategy = 'html', $charset = \null, $autoescape = \false)
    {
    }
    /**
     * @internal
     */
    function twig_escape_filter_is_safe(\Twig\Node\Node $filterArgs)
    {
    }
}
