<?php

namespace Twig\Extension;

final class SandboxExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Twig\Sandbox\SecurityPolicyInterface $policy, $sandboxed = false)
    {
    }
    public function getTokenParsers() : array
    {
    }
    public function getNodeVisitors() : array
    {
    }
    public function enableSandbox() : void
    {
    }
    public function disableSandbox() : void
    {
    }
    public function isSandboxed() : bool
    {
    }
    public function isSandboxedGlobally() : bool
    {
    }
    public function setSecurityPolicy(\Twig\Sandbox\SecurityPolicyInterface $policy)
    {
    }
    public function getSecurityPolicy() : \Twig\Sandbox\SecurityPolicyInterface
    {
    }
    public function checkSecurity($tags, $filters, $functions) : void
    {
    }
    public function checkMethodAllowed($obj, $method, int $lineno = -1, \Twig\Source $source = null) : void
    {
    }
    public function checkPropertyAllowed($obj, $property, int $lineno = -1, \Twig\Source $source = null) : void
    {
    }
    public function ensureToStringAllowed($obj, int $lineno = -1, \Twig\Source $source = null)
    {
    }
}
