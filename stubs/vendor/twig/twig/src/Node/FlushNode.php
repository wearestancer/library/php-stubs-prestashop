<?php

namespace Twig\Node;

/**
 * Represents a flush node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FlushNode extends \Twig\Node\Node
{
    public function __construct(int $lineno, string $tag)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
