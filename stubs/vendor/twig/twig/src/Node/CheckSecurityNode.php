<?php

namespace Twig\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class CheckSecurityNode extends \Twig\Node\Node
{
    public function __construct(array $usedFilters, array $usedTags, array $usedFunctions)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
