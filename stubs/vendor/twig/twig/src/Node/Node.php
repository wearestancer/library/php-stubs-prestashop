<?php

namespace Twig\Node;

/**
 * Represents a node in the AST.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Node implements \Countable, \IteratorAggregate
{
    protected $nodes;
    protected $attributes;
    protected $lineno;
    protected $tag;
    /**
     * @param array  $nodes      An array of named nodes
     * @param array  $attributes An array of attributes (should not be nodes)
     * @param int    $lineno     The line number
     * @param string $tag        The tag name associated with the Node
     */
    public function __construct(array $nodes = [], array $attributes = [], int $lineno = 0, string $tag = null)
    {
    }
    public function __toString()
    {
    }
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
    public function getTemplateLine() : int
    {
    }
    public function getNodeTag() : ?string
    {
    }
    public function hasAttribute(string $name) : bool
    {
    }
    public function getAttribute(string $name)
    {
    }
    public function setAttribute(string $name, $value) : void
    {
    }
    public function removeAttribute(string $name) : void
    {
    }
    public function hasNode(string $name) : bool
    {
    }
    public function getNode(string $name) : self
    {
    }
    public function setNode(string $name, self $node) : void
    {
    }
    public function removeNode(string $name) : void
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    public function getIterator() : \Traversable
    {
    }
    public function getTemplateName() : ?string
    {
    }
    public function setSourceContext(\Twig\Source $source) : void
    {
    }
    public function getSourceContext() : ?\Twig\Source
    {
    }
}
