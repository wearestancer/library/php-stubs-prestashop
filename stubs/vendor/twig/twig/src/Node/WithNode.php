<?php

namespace Twig\Node;

/**
 * Represents a nested "with" scope.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class WithNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $body, ?\Twig\Node\Node $variables, bool $only, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
