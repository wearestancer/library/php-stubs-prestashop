<?php

namespace Twig\Node\Expression\Filter;

/**
 * Returns the value or the default value when it is undefined or empty.
 *
 *  {{ var.foo|default('foo item on var is not defined') }}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DefaultFilter extends \Twig\Node\Expression\FilterExpression
{
    public function __construct(\Twig\Node\Node $node, \Twig\Node\Expression\ConstantExpression $filterName, \Twig\Node\Node $arguments, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
