<?php

namespace Twig\Node\Expression;

/**
 * Represents an arrow function.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ArrowFunctionExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr, \Twig\Node\Node $names, $lineno, $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
