<?php

namespace Twig\Node\Expression\Test;

/**
 * Checks if a variable is the exact same value as a constant.
 *
 *    {% if post.status is constant('Post::PUBLISHED') %}
 *      the status attribute is exactly the same as Post::PUBLISHED
 *    {% endif %}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ConstantTest extends \Twig\Node\Expression\TestExpression
{
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
