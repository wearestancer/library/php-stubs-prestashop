<?php

namespace Twig\Node\Expression\Test;

/**
 * Checks if a number is odd.
 *
 *  {{ var is odd }}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class OddTest extends \Twig\Node\Expression\TestExpression
{
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
