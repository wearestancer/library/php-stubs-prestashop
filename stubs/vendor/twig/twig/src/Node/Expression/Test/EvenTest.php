<?php

namespace Twig\Node\Expression\Test;

/**
 * Checks if a number is even.
 *
 *  {{ var is even }}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class EvenTest extends \Twig\Node\Expression\TestExpression
{
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
