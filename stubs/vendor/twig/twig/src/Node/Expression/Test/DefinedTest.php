<?php

namespace Twig\Node\Expression\Test;

/**
 * Checks if a variable is defined in the current context.
 *
 *    {# defined works with variable names and variable attributes #}
 *    {% if foo is defined %}
 *        {# ... #}
 *    {% endif %}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DefinedTest extends \Twig\Node\Expression\TestExpression
{
    public function __construct(\Twig\Node\Node $node, string $name, ?\Twig\Node\Node $arguments, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
