<?php

namespace Twig\Node\Expression\Test;

/**
 * Checks if a variable is divisible by a number.
 *
 *  {% if loop.index is divisible by(3) %}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DivisiblebyTest extends \Twig\Node\Expression\TestExpression
{
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
