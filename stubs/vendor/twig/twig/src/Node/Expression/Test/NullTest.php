<?php

namespace Twig\Node\Expression\Test;

/**
 * Checks that a variable is null.
 *
 *  {{ var is none }}
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class NullTest extends \Twig\Node\Expression\TestExpression
{
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
