<?php

namespace Twig\Node\Expression;

class ArrayExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(array $elements, int $lineno)
    {
    }
    public function getKeyValuePairs() : array
    {
    }
    public function hasElement(\Twig\Node\Expression\AbstractExpression $key) : bool
    {
    }
    public function addElement(\Twig\Node\Expression\AbstractExpression $value, \Twig\Node\Expression\AbstractExpression $key = null) : void
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
