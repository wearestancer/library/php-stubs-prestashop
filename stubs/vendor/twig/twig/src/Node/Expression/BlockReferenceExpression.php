<?php

namespace Twig\Node\Expression;

/**
 * Represents a block call node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class BlockReferenceExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Node $name, ?\Twig\Node\Node $template, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
