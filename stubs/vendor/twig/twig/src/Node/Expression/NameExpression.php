<?php

namespace Twig\Node\Expression;

class NameExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(string $name, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
    public function isSpecial()
    {
    }
    public function isSimple()
    {
    }
}
