<?php

namespace Twig\Node\Expression;

abstract class CallExpression extends \Twig\Node\Expression\AbstractExpression
{
    protected function compileCallable(\Twig\Compiler $compiler)
    {
    }
    protected function compileArguments(\Twig\Compiler $compiler, $isArray = false) : void
    {
    }
    protected function getArguments($callable, $arguments)
    {
    }
    protected function normalizeName(string $name) : string
    {
    }
}
