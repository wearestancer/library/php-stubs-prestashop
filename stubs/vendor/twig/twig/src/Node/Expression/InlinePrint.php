<?php

namespace Twig\Node\Expression;

/**
 * @internal
 */
final class InlinePrint extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Node $node, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
