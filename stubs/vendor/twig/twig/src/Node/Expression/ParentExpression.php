<?php

namespace Twig\Node\Expression;

/**
 * Represents a parent node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ParentExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(string $name, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
