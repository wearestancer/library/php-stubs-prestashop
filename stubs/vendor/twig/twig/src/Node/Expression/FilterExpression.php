<?php

namespace Twig\Node\Expression;

class FilterExpression extends \Twig\Node\Expression\CallExpression
{
    public function __construct(\Twig\Node\Node $node, \Twig\Node\Expression\ConstantExpression $filterName, \Twig\Node\Node $arguments, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
