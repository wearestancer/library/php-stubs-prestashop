<?php

namespace Twig\Node\Expression;

class MethodCallExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $node, string $method, \Twig\Node\Expression\ArrayExpression $arguments, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
