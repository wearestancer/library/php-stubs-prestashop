<?php

namespace Twig\Node\Expression;

class NullCoalesceExpression extends \Twig\Node\Expression\ConditionalExpression
{
    public function __construct(\Twig\Node\Node $left, \Twig\Node\Node $right, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
