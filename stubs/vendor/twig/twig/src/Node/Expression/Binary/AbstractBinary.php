<?php

namespace Twig\Node\Expression\Binary;

abstract class AbstractBinary extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Node $left, \Twig\Node\Node $right, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
    public abstract function operator(\Twig\Compiler $compiler) : \Twig\Compiler;
}
