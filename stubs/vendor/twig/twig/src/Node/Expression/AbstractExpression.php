<?php

namespace Twig\Node\Expression;

/**
 * Abstract class for all nodes that represents an expression.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractExpression extends \Twig\Node\Node
{
}
