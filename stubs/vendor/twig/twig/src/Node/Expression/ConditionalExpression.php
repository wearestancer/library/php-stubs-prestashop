<?php

namespace Twig\Node\Expression;

class ConditionalExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr1, \Twig\Node\Expression\AbstractExpression $expr2, \Twig\Node\Expression\AbstractExpression $expr3, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
