<?php

namespace Twig\Node\Expression\Unary;

abstract class AbstractUnary extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Node $node, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
    public abstract function operator(\Twig\Compiler $compiler) : \Twig\Compiler;
}
