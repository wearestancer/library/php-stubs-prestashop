<?php

namespace Twig\Node\Expression;

class GetAttrExpression extends \Twig\Node\Expression\AbstractExpression
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $node, \Twig\Node\Expression\AbstractExpression $attribute, ?\Twig\Node\Expression\AbstractExpression $arguments, string $type, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
