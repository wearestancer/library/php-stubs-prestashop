<?php

namespace Twig\Node;

/**
 * Represents an include node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class IncludeNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr, ?\Twig\Node\Expression\AbstractExpression $variables, bool $only, bool $ignoreMissing, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
    protected function addGetTemplate(\Twig\Compiler $compiler)
    {
    }
    protected function addTemplateArguments(\Twig\Compiler $compiler)
    {
    }
}
