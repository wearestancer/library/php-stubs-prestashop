<?php

namespace Twig\Node;

/**
 * Represents a deprecated node.
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class DeprecatedNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
