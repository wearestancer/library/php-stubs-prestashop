<?php

namespace Twig\Node;

/**
 * Represents a macro node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MacroNode extends \Twig\Node\Node
{
    public const VARARGS_NAME = 'varargs';
    public function __construct(string $name, \Twig\Node\Node $body, \Twig\Node\Node $arguments, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
