<?php

namespace Twig\Node;

/**
 * Represents a block node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class BlockNode extends \Twig\Node\Node
{
    public function __construct(string $name, \Twig\Node\Node $body, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
