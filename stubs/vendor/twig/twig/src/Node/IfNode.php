<?php

namespace Twig\Node;

/**
 * Represents an if node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class IfNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $tests, ?\Twig\Node\Node $else, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
