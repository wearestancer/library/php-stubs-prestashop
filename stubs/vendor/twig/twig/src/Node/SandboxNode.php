<?php

namespace Twig\Node;

/**
 * Represents a sandbox node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SandboxNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $body, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
