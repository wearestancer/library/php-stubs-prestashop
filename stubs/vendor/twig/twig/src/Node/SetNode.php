<?php

namespace Twig\Node;

/**
 * Represents a set node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SetNode extends \Twig\Node\Node implements \Twig\Node\NodeCaptureInterface
{
    public function __construct(bool $capture, \Twig\Node\Node $names, \Twig\Node\Node $values, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
