<?php

namespace Twig\Node;

/**
 * Represents a module node.
 *
 * Consider this class as being final. If you need to customize the behavior of
 * the generated class, consider adding nodes to the following nodes: display_start,
 * display_end, constructor_start, constructor_end, and class_end.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class ModuleNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $body, ?\Twig\Node\Expression\AbstractExpression $parent, \Twig\Node\Node $blocks, \Twig\Node\Node $macros, \Twig\Node\Node $traits, $embeddedTemplates, \Twig\Source $source)
    {
    }
    public function setIndex($index)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
