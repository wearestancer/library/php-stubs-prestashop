<?php

namespace Twig\Node;

/**
 * Represents a node that outputs an expression.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PrintNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
