<?php

namespace Twig\Node;

/**
 * Represents a block call node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class BlockReferenceNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
{
    public function __construct(string $name, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
