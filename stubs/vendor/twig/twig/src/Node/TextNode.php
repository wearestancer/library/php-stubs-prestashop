<?php

namespace Twig\Node;

/**
 * Represents a text node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TextNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
{
    public function __construct(string $data, int $lineno)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
