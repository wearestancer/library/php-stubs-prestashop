<?php

namespace Twig\Node;

/**
 * Represents an import node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ImportNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr, \Twig\Node\Expression\AbstractExpression $var, int $lineno, string $tag = null, bool $global = true)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
