<?php

namespace Twig\Node;

/**
 * Represents a displayable node in the AST.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface NodeOutputInterface
{
}
