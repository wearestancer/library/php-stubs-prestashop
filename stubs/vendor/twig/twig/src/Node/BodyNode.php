<?php

namespace Twig\Node;

/**
 * Represents a body node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class BodyNode extends \Twig\Node\Node
{
}
