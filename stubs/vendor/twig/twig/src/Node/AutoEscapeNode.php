<?php

namespace Twig\Node;

/**
 * Represents an autoescape node.
 *
 * The value is the escaping strategy (can be html, js, ...)
 *
 * The true value is equivalent to html.
 *
 * If autoescaping is disabled, then the value is false.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AutoEscapeNode extends \Twig\Node\Node
{
    public function __construct($value, \Twig\Node\Node $body, int $lineno, string $tag = 'autoescape')
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
