<?php

namespace Twig\Node;

/**
 * Represents an embed node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class EmbedNode extends \Twig\Node\IncludeNode
{
    // we don't inject the module to avoid node visitors to traverse it twice (as it will be already visited in the main module)
    public function __construct(string $name, int $index, ?\Twig\Node\Expression\AbstractExpression $variables, bool $only, bool $ignoreMissing, int $lineno, string $tag = null)
    {
    }
    protected function addGetTemplate(\Twig\Compiler $compiler) : void
    {
    }
}
