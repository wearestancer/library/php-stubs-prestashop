<?php

namespace Twig\Node;

/**
 * Represents a do node.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DoNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Expression\AbstractExpression $expr, int $lineno, string $tag = null)
    {
    }
    public function compile(\Twig\Compiler $compiler) : void
    {
    }
}
