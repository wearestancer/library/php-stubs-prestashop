<?php

namespace Twig\Util;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TemplateDirIterator extends \IteratorIterator
{
    /**
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
    }
    /**
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
    }
}
