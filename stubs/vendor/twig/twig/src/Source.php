<?php

namespace Twig;

/**
 * Holds information about a non-compiled Twig template.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Source
{
    /**
     * @param string $code The template source code
     * @param string $name The template logical name
     * @param string $path The filesystem path of the template if any
     */
    public function __construct(string $code, string $name, string $path = '')
    {
    }
    public function getCode() : string
    {
    }
    public function getName() : string
    {
    }
    public function getPath() : string
    {
    }
}
