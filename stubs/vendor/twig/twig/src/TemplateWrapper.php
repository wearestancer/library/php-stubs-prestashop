<?php

namespace Twig;

/**
 * Exposes a template to userland.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class TemplateWrapper
{
    /**
     * This method is for internal use only and should never be called
     * directly (use Twig\Environment::load() instead).
     *
     * @internal
     */
    public function __construct(\Twig\Environment $env, \Twig\Template $template)
    {
    }
    public function render(array $context = []) : string
    {
    }
    public function display(array $context = [])
    {
    }
    public function hasBlock(string $name, array $context = []) : bool
    {
    }
    /**
     * @return string[] An array of defined template block names
     */
    public function getBlockNames(array $context = []) : array
    {
    }
    public function renderBlock(string $name, array $context = []) : string
    {
    }
    public function displayBlock(string $name, array $context = [])
    {
    }
    public function getSourceContext() : \Twig\Source
    {
    }
    public function getTemplateName() : string
    {
    }
    /**
     * @internal
     *
     * @return Template
     */
    public function unwrap()
    {
    }
}
