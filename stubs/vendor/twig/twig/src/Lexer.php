<?php

namespace Twig;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Lexer
{
    public const STATE_DATA = 0;
    public const STATE_BLOCK = 1;
    public const STATE_VAR = 2;
    public const STATE_STRING = 3;
    public const STATE_INTERPOLATION = 4;
    public const REGEX_NAME = '/[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]*/A';
    public const REGEX_NUMBER = '/[0-9]+(?:\\.[0-9]+)?([Ee][\\+\\-][0-9]+)?/A';
    public const REGEX_STRING = '/"([^#"\\\\]*(?:\\\\.[^#"\\\\]*)*)"|\'([^\'\\\\]*(?:\\\\.[^\'\\\\]*)*)\'/As';
    public const REGEX_DQ_STRING_DELIM = '/"/A';
    public const REGEX_DQ_STRING_PART = '/[^#"\\\\]*(?:(?:\\\\.|#(?!\\{))[^#"\\\\]*)*/As';
    public const PUNCTUATION = '()[]{}?:.,|';
    public function __construct(\Twig\Environment $env, array $options = [])
    {
    }
    public function tokenize(\Twig\Source $source) : \Twig\TokenStream
    {
    }
}
