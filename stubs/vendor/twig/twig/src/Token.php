<?php

namespace Twig;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Token
{
    public const EOF_TYPE = -1;
    public const TEXT_TYPE = 0;
    public const BLOCK_START_TYPE = 1;
    public const VAR_START_TYPE = 2;
    public const BLOCK_END_TYPE = 3;
    public const VAR_END_TYPE = 4;
    public const NAME_TYPE = 5;
    public const NUMBER_TYPE = 6;
    public const STRING_TYPE = 7;
    public const OPERATOR_TYPE = 8;
    public const PUNCTUATION_TYPE = 9;
    public const INTERPOLATION_START_TYPE = 10;
    public const INTERPOLATION_END_TYPE = 11;
    public const ARROW_TYPE = 12;
    public function __construct(int $type, $value, int $lineno)
    {
    }
    public function __toString()
    {
    }
    /**
     * Tests the current token for a type and/or a value.
     *
     * Parameters may be:
     *  * just type
     *  * type and value (or array of possible values)
     *  * just value (or array of possible values) (NAME_TYPE is used as type)
     *
     * @param array|string|int  $type   The type to test
     * @param array|string|null $values The token value
     */
    public function test($type, $values = null) : bool
    {
    }
    public function getLine() : int
    {
    }
    public function getType() : int
    {
    }
    public function getValue()
    {
    }
    public static function typeToString(int $type, bool $short = false) : string
    {
    }
    public static function typeToEnglish(int $type) : string
    {
    }
}
