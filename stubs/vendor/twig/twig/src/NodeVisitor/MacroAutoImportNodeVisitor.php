<?php

namespace Twig\NodeVisitor;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class MacroAutoImportNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
{
    public function enterNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public function leaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public function getPriority() : int
    {
    }
}
