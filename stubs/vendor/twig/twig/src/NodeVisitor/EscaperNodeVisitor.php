<?php

namespace Twig\NodeVisitor;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class EscaperNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
{
    public function __construct()
    {
    }
    public function enterNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public function leaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : ?\Twig\Node\Node
    {
    }
    public function getPriority() : int
    {
    }
}
