<?php

namespace Twig\NodeVisitor;

/**
 * Tries to optimize the AST.
 *
 * This visitor is always the last registered one.
 *
 * You can configure which optimizations you want to activate via the
 * optimizer mode.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class OptimizerNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
{
    public const OPTIMIZE_ALL = -1;
    public const OPTIMIZE_NONE = 0;
    public const OPTIMIZE_FOR = 2;
    public const OPTIMIZE_RAW_FILTER = 4;
    /**
     * @param int $optimizers The optimizer mode
     */
    public function __construct(int $optimizers = -1)
    {
    }
    public function enterNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public function leaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : ?\Twig\Node\Node
    {
    }
    public function getPriority() : int
    {
    }
}
