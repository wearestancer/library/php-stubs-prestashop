<?php

namespace Twig\NodeVisitor;

/**
 * @internal
 */
final class SafeAnalysisNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
{
    public function setSafeVars(array $safeVars) : void
    {
    }
    public function getSafe(\Twig\Node\Node $node)
    {
    }
    public function enterNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public function leaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : ?\Twig\Node\Node
    {
    }
    public function getPriority() : int
    {
    }
}
