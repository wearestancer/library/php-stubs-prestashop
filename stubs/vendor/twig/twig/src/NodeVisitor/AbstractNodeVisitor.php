<?php

namespace Twig\NodeVisitor;

/**
 * Used to make node visitors compatible with Twig 1.x and 2.x.
 *
 * To be removed in Twig 3.1.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
{
    public final function enterNode(\Twig\Node\Node $node, \Twig\Environment $env) : \Twig\Node\Node
    {
    }
    public final function leaveNode(\Twig\Node\Node $node, \Twig\Environment $env) : ?\Twig\Node\Node
    {
    }
    /**
     * Called before child nodes are visited.
     *
     * @return Node The modified node
     */
    protected abstract function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env);
    /**
     * Called after child nodes are visited.
     *
     * @return Node|null The modified node or null if the node must be removed
     */
    protected abstract function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env);
}
