<?php

namespace Twig;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Compiler
{
    public function __construct(\Twig\Environment $env)
    {
    }
    public function getEnvironment() : \Twig\Environment
    {
    }
    public function getSource() : string
    {
    }
    /**
     * @return $this
     */
    public function compile(\Twig\Node\Node $node, int $indentation = 0)
    {
    }
    /**
     * @return $this
     */
    public function subcompile(\Twig\Node\Node $node, bool $raw = true)
    {
    }
    /**
     * Adds a raw string to the compiled code.
     *
     * @return $this
     */
    public function raw(string $string)
    {
    }
    /**
     * Writes a string to the compiled code by adding indentation.
     *
     * @return $this
     */
    public function write(...$strings)
    {
    }
    /**
     * Adds a quoted string to the compiled code.
     *
     * @return $this
     */
    public function string(string $value)
    {
    }
    /**
     * Returns a PHP representation of a given value.
     *
     * @return $this
     */
    public function repr($value)
    {
    }
    /**
     * @return $this
     */
    public function addDebugInfo(\Twig\Node\Node $node)
    {
    }
    public function getDebugInfo() : array
    {
    }
    /**
     * @return $this
     */
    public function indent(int $step = 1)
    {
    }
    /**
     * @return $this
     *
     * @throws \LogicException When trying to outdent too much so the indentation would become negative
     */
    public function outdent(int $step = 1)
    {
    }
    public function getVarName() : string
    {
    }
}
