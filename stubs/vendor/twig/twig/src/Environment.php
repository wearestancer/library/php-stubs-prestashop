<?php

namespace Twig;

/**
 * Stores the Twig configuration and renders templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Environment
{
    public const VERSION = '3.4.3';
    public const VERSION_ID = 30403;
    public const MAJOR_VERSION = 3;
    public const MINOR_VERSION = 4;
    public const RELEASE_VERSION = 3;
    public const EXTRA_VERSION = '';
    /**
     * Constructor.
     *
     * Available options:
     *
     *  * debug: When set to true, it automatically set "auto_reload" to true as
     *           well (default to false).
     *
     *  * charset: The charset used by the templates (default to UTF-8).
     *
     *  * cache: An absolute path where to store the compiled templates,
     *           a \Twig\Cache\CacheInterface implementation,
     *           or false to disable compilation cache (default).
     *
     *  * auto_reload: Whether to reload the template if the original source changed.
     *                 If you don't provide the auto_reload option, it will be
     *                 determined automatically based on the debug value.
     *
     *  * strict_variables: Whether to ignore invalid variables in templates
     *                      (default to false).
     *
     *  * autoescape: Whether to enable auto-escaping (default to html):
     *                  * false: disable auto-escaping
     *                  * html, js: set the autoescaping to one of the supported strategies
     *                  * name: set the autoescaping strategy based on the template name extension
     *                  * PHP callback: a PHP callback that returns an escaping strategy based on the template "name"
     *
     *  * optimizations: A flag that indicates which optimizations to apply
     *                   (default to -1 which means that all optimizations are enabled;
     *                   set it to 0 to disable).
     */
    public function __construct(\Twig\Loader\LoaderInterface $loader, $options = [])
    {
    }
    /**
     * Enables debugging mode.
     */
    public function enableDebug()
    {
    }
    /**
     * Disables debugging mode.
     */
    public function disableDebug()
    {
    }
    /**
     * Checks if debug mode is enabled.
     *
     * @return bool true if debug mode is enabled, false otherwise
     */
    public function isDebug()
    {
    }
    /**
     * Enables the auto_reload option.
     */
    public function enableAutoReload()
    {
    }
    /**
     * Disables the auto_reload option.
     */
    public function disableAutoReload()
    {
    }
    /**
     * Checks if the auto_reload option is enabled.
     *
     * @return bool true if auto_reload is enabled, false otherwise
     */
    public function isAutoReload()
    {
    }
    /**
     * Enables the strict_variables option.
     */
    public function enableStrictVariables()
    {
    }
    /**
     * Disables the strict_variables option.
     */
    public function disableStrictVariables()
    {
    }
    /**
     * Checks if the strict_variables option is enabled.
     *
     * @return bool true if strict_variables is enabled, false otherwise
     */
    public function isStrictVariables()
    {
    }
    /**
     * Gets the current cache implementation.
     *
     * @param bool $original Whether to return the original cache option or the real cache instance
     *
     * @return CacheInterface|string|false A Twig\Cache\CacheInterface implementation,
     *                                     an absolute path to the compiled templates,
     *                                     or false to disable cache
     */
    public function getCache($original = true)
    {
    }
    /**
     * Sets the current cache implementation.
     *
     * @param CacheInterface|string|false $cache A Twig\Cache\CacheInterface implementation,
     *                                           an absolute path to the compiled templates,
     *                                           or false to disable cache
     */
    public function setCache($cache)
    {
    }
    /**
     * Gets the template class associated with the given string.
     *
     * The generated template class is based on the following parameters:
     *
     *  * The cache key for the given template;
     *  * The currently enabled extensions;
     *  * Whether the Twig C extension is available or not;
     *  * PHP version;
     *  * Twig version;
     *  * Options with what environment was created.
     *
     * @param string   $name  The name for which to calculate the template class name
     * @param int|null $index The index if it is an embedded template
     *
     * @internal
     */
    public function getTemplateClass(string $name, int $index = null) : string
    {
    }
    /**
     * Renders a template.
     *
     * @param string|TemplateWrapper $name The template name
     *
     * @throws LoaderError  When the template cannot be found
     * @throws SyntaxError  When an error occurred during compilation
     * @throws RuntimeError When an error occurred during rendering
     */
    public function render($name, array $context = []) : string
    {
    }
    /**
     * Displays a template.
     *
     * @param string|TemplateWrapper $name The template name
     *
     * @throws LoaderError  When the template cannot be found
     * @throws SyntaxError  When an error occurred during compilation
     * @throws RuntimeError When an error occurred during rendering
     */
    public function display($name, array $context = []) : void
    {
    }
    /**
     * Loads a template.
     *
     * @param string|TemplateWrapper $name The template name
     *
     * @throws LoaderError  When the template cannot be found
     * @throws RuntimeError When a previously generated cache is corrupted
     * @throws SyntaxError  When an error occurred during compilation
     */
    public function load($name) : \Twig\TemplateWrapper
    {
    }
    /**
     * Loads a template internal representation.
     *
     * This method is for internal use only and should never be called
     * directly.
     *
     * @param string $name  The template name
     * @param int    $index The index if it is an embedded template
     *
     * @throws LoaderError  When the template cannot be found
     * @throws RuntimeError When a previously generated cache is corrupted
     * @throws SyntaxError  When an error occurred during compilation
     *
     * @internal
     */
    public function loadTemplate(string $cls, string $name, int $index = null) : \Twig\Template
    {
    }
    /**
     * Creates a template from source.
     *
     * This method should not be used as a generic way to load templates.
     *
     * @param string $template The template source
     * @param string $name     An optional name of the template to be used in error messages
     *
     * @throws LoaderError When the template cannot be found
     * @throws SyntaxError When an error occurred during compilation
     */
    public function createTemplate(string $template, string $name = null) : \Twig\TemplateWrapper
    {
    }
    /**
     * Returns true if the template is still fresh.
     *
     * Besides checking the loader for freshness information,
     * this method also checks if the enabled extensions have
     * not changed.
     *
     * @param int $time The last modification time of the cached template
     */
    public function isTemplateFresh(string $name, int $time) : bool
    {
    }
    /**
     * Tries to load a template consecutively from an array.
     *
     * Similar to load() but it also accepts instances of \Twig\Template and
     * \Twig\TemplateWrapper, and an array of templates where each is tried to be loaded.
     *
     * @param string|TemplateWrapper|array $names A template or an array of templates to try consecutively
     *
     * @throws LoaderError When none of the templates can be found
     * @throws SyntaxError When an error occurred during compilation
     */
    public function resolveTemplate($names) : \Twig\TemplateWrapper
    {
    }
    public function setLexer(\Twig\Lexer $lexer)
    {
    }
    /**
     * @throws SyntaxError When the code is syntactically wrong
     */
    public function tokenize(\Twig\Source $source) : \Twig\TokenStream
    {
    }
    public function setParser(\Twig\Parser $parser)
    {
    }
    /**
     * Converts a token stream to a node tree.
     *
     * @throws SyntaxError When the token stream is syntactically or semantically wrong
     */
    public function parse(\Twig\TokenStream $stream) : \Twig\Node\ModuleNode
    {
    }
    public function setCompiler(\Twig\Compiler $compiler)
    {
    }
    /**
     * Compiles a node and returns the PHP code.
     */
    public function compile(\Twig\Node\Node $node) : string
    {
    }
    /**
     * Compiles a template source code.
     *
     * @throws SyntaxError When there was an error during tokenizing, parsing or compiling
     */
    public function compileSource(\Twig\Source $source) : string
    {
    }
    public function setLoader(\Twig\Loader\LoaderInterface $loader)
    {
    }
    public function getLoader() : \Twig\Loader\LoaderInterface
    {
    }
    public function setCharset(string $charset)
    {
    }
    public function getCharset() : string
    {
    }
    public function hasExtension(string $class) : bool
    {
    }
    public function addRuntimeLoader(\Twig\RuntimeLoader\RuntimeLoaderInterface $loader)
    {
    }
    /**
     * @template TExtension of ExtensionInterface
     *
     * @param class-string<TExtension> $class
     *
     * @return TExtension
     */
    public function getExtension(string $class) : \Twig\Extension\ExtensionInterface
    {
    }
    /**
     * Returns the runtime implementation of a Twig element (filter/function/tag/test).
     *
     * @template TRuntime of object
     *
     * @param class-string<TRuntime> $class A runtime class name
     *
     * @return TRuntime The runtime implementation
     *
     * @throws RuntimeError When the template cannot be found
     */
    public function getRuntime(string $class)
    {
    }
    public function addExtension(\Twig\Extension\ExtensionInterface $extension)
    {
    }
    /**
     * @param ExtensionInterface[] $extensions An array of extensions
     */
    public function setExtensions(array $extensions)
    {
    }
    /**
     * @return ExtensionInterface[] An array of extensions (keys are for internal usage only and should not be relied on)
     */
    public function getExtensions() : array
    {
    }
    public function addTokenParser(\Twig\TokenParser\TokenParserInterface $parser)
    {
    }
    /**
     * @return TokenParserInterface[]
     *
     * @internal
     */
    public function getTokenParsers() : array
    {
    }
    /**
     * @internal
     */
    public function getTokenParser(string $name) : ?\Twig\TokenParser\TokenParserInterface
    {
    }
    public function registerUndefinedTokenParserCallback(callable $callable) : void
    {
    }
    public function addNodeVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor)
    {
    }
    /**
     * @return NodeVisitorInterface[]
     *
     * @internal
     */
    public function getNodeVisitors() : array
    {
    }
    public function addFilter(\Twig\TwigFilter $filter)
    {
    }
    /**
     * @internal
     */
    public function getFilter(string $name) : ?\Twig\TwigFilter
    {
    }
    public function registerUndefinedFilterCallback(callable $callable) : void
    {
    }
    /**
     * Gets the registered Filters.
     *
     * Be warned that this method cannot return filters defined with registerUndefinedFilterCallback.
     *
     * @return TwigFilter[]
     *
     * @see registerUndefinedFilterCallback
     *
     * @internal
     */
    public function getFilters() : array
    {
    }
    public function addTest(\Twig\TwigTest $test)
    {
    }
    /**
     * @return TwigTest[]
     *
     * @internal
     */
    public function getTests() : array
    {
    }
    /**
     * @internal
     */
    public function getTest(string $name) : ?\Twig\TwigTest
    {
    }
    public function addFunction(\Twig\TwigFunction $function)
    {
    }
    /**
     * @internal
     */
    public function getFunction(string $name) : ?\Twig\TwigFunction
    {
    }
    public function registerUndefinedFunctionCallback(callable $callable) : void
    {
    }
    /**
     * Gets registered functions.
     *
     * Be warned that this method cannot return functions defined with registerUndefinedFunctionCallback.
     *
     * @return TwigFunction[]
     *
     * @see registerUndefinedFunctionCallback
     *
     * @internal
     */
    public function getFunctions() : array
    {
    }
    /**
     * Registers a Global.
     *
     * New globals can be added before compiling or rendering a template;
     * but after, you can only update existing globals.
     *
     * @param mixed $value The global value
     */
    public function addGlobal(string $name, $value)
    {
    }
    /**
     * @internal
     */
    public function getGlobals() : array
    {
    }
    public function mergeGlobals(array $context) : array
    {
    }
    /**
     * @internal
     */
    public function getUnaryOperators() : array
    {
    }
    /**
     * @internal
     */
    public function getBinaryOperators() : array
    {
    }
}
