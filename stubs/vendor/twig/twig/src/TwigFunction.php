<?php

namespace Twig;

/**
 * Represents a template function.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @see https://twig.symfony.com/doc/templates.html#functions
 */
final class TwigFunction
{
    /**
     * @param callable|null $callable A callable implementing the function. If null, you need to overwrite the "node_class" option to customize compilation.
     */
    public function __construct(string $name, $callable = null, array $options = [])
    {
    }
    public function getName() : string
    {
    }
    /**
     * Returns the callable to execute for this function.
     *
     * @return callable|null
     */
    public function getCallable()
    {
    }
    public function getNodeClass() : string
    {
    }
    public function setArguments(array $arguments) : void
    {
    }
    public function getArguments() : array
    {
    }
    public function needsEnvironment() : bool
    {
    }
    public function needsContext() : bool
    {
    }
    public function getSafe(\Twig\Node\Node $functionArgs) : ?array
    {
    }
    public function isVariadic() : bool
    {
    }
    public function isDeprecated() : bool
    {
    }
    public function getDeprecatedVersion() : string
    {
    }
    public function getAlternative() : ?string
    {
    }
}
