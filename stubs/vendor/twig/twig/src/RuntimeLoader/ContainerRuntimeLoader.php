<?php

namespace Twig\RuntimeLoader;

/**
 * Lazily loads Twig runtime implementations from a PSR-11 container.
 *
 * Note that the runtime services MUST use their class names as identifiers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
class ContainerRuntimeLoader implements \Twig\RuntimeLoader\RuntimeLoaderInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    public function load(string $class)
    {
    }
}
