<?php

namespace Twig\Sandbox;

/**
 * Exception thrown when a not allowed function is used in a template.
 *
 * @author Martin Hasoň <martin.hason@gmail.com>
 */
final class SecurityNotAllowedFunctionError extends \Twig\Sandbox\SecurityError
{
    public function __construct(string $message, string $functionName)
    {
    }
    public function getFunctionName() : string
    {
    }
}
