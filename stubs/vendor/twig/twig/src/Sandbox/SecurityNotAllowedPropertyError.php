<?php

namespace Twig\Sandbox;

/**
 * Exception thrown when a not allowed class property is used in a template.
 *
 * @author Kit Burton-Senior <mail@kitbs.com>
 */
final class SecurityNotAllowedPropertyError extends \Twig\Sandbox\SecurityError
{
    public function __construct(string $message, string $className, string $propertyName)
    {
    }
    public function getClassName() : string
    {
    }
    public function getPropertyName()
    {
    }
}
