<?php

namespace Twig\Sandbox;

/**
 * Exception thrown when a not allowed filter is used in a template.
 *
 * @author Martin Hasoň <martin.hason@gmail.com>
 */
final class SecurityNotAllowedFilterError extends \Twig\Sandbox\SecurityError
{
    public function __construct(string $message, string $functionName)
    {
    }
    public function getFilterName() : string
    {
    }
}
