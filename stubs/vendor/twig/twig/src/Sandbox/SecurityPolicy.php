<?php

namespace Twig\Sandbox;

/**
 * Represents a security policy which need to be enforced when sandbox mode is enabled.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class SecurityPolicy implements \Twig\Sandbox\SecurityPolicyInterface
{
    public function __construct(array $allowedTags = [], array $allowedFilters = [], array $allowedMethods = [], array $allowedProperties = [], array $allowedFunctions = [])
    {
    }
    public function setAllowedTags(array $tags) : void
    {
    }
    public function setAllowedFilters(array $filters) : void
    {
    }
    public function setAllowedMethods(array $methods) : void
    {
    }
    public function setAllowedProperties(array $properties) : void
    {
    }
    public function setAllowedFunctions(array $functions) : void
    {
    }
    public function checkSecurity($tags, $filters, $functions) : void
    {
    }
    public function checkMethodAllowed($obj, $method) : void
    {
    }
    public function checkPropertyAllowed($obj, $property) : void
    {
    }
}
