<?php

namespace Twig\Sandbox;

/**
 * Exception thrown when a security error occurs at runtime.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SecurityError extends \Twig\Error\Error
{
}
