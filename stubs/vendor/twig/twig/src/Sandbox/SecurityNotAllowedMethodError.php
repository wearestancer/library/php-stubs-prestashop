<?php

namespace Twig\Sandbox;

/**
 * Exception thrown when a not allowed class method is used in a template.
 *
 * @author Kit Burton-Senior <mail@kitbs.com>
 */
final class SecurityNotAllowedMethodError extends \Twig\Sandbox\SecurityError
{
    public function __construct(string $message, string $className, string $methodName)
    {
    }
    public function getClassName() : string
    {
    }
    public function getMethodName()
    {
    }
}
