<?php

namespace Twig\Sandbox;

/**
 * Exception thrown when a not allowed tag is used in a template.
 *
 * @author Martin Hasoň <martin.hason@gmail.com>
 */
final class SecurityNotAllowedTagError extends \Twig\Sandbox\SecurityError
{
    public function __construct(string $message, string $tagName)
    {
    }
    public function getTagName() : string
    {
    }
}
