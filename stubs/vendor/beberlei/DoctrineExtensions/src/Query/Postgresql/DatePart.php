<?php

namespace DoctrineExtensions\Query\Postgresql;

/**
 * @author Geovani Roggeo
 */
class DatePart extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $dateString = null;
    public $dateFormat = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
