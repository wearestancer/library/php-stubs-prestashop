<?php

namespace DoctrineExtensions\Query\Postgresql;

class ExtractFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @param SqlWalker $sqlWalker
     *
     * @throws ASTException
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker) : string
    {
    }
    /**
     * @param Parser $parser
     *
     * @throws QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser) : void
    {
    }
}
