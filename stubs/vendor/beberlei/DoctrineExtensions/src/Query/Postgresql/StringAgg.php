<?php

namespace DoctrineExtensions\Query\Postgresql;

/**
 * @author Roberto Júnior <me@robertojunior.net>
 * @author Vaskevich Eugeniy <wbrframe@gmail.com>
 */
class StringAgg extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
