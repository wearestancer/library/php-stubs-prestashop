<?php

namespace DoctrineExtensions\Query\Postgresql;

/**
 * AtTimeZoneFunction ::= "AT_TIME_ZONE" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
 */
class AtTimeZoneFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $dateExpression = null;
    public $timezoneExpression = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
