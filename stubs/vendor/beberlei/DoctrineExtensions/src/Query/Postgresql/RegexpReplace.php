<?php

namespace DoctrineExtensions\Query\Postgresql;

/**
 * @example SELECT REGEXP_REPLACE(string, search, replace)
 * @link https://www.postgresql.org/docs/current/functions-matching.html#FUNCTIONS-POSIX-TABLE
 */
class RegexpReplace extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
