<?php

namespace DoctrineExtensions\Query\Postgresql;

/**
 * CountFilterFunction ::= "COUNT_FILTER" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
 */
class CountFilterFunction extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $countExpression = null;
    public $whereExpression = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
