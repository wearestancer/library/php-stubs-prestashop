<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Wally Noveno <wally.noveno@gmail.com>
 */
class Format extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $numberExpression = null;
    public $patternExpression = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
