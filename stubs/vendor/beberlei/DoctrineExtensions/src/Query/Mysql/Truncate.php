<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Evgeny Savich <jack.savich@gmail.com>
 */
class Truncate extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $numberExpression = null;
    public $patternExpression = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
