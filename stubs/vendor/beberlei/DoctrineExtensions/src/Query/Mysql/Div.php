<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @link https://dev.mysql.com/doc/refman/en/arithmetic-functions.html#operator_div
 */
class Div extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
