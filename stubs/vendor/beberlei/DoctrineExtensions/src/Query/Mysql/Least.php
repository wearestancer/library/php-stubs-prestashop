<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Vas N <phpvas@gmail.com>
 */
class Least extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @param Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
