<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @link https://dev.mysql.com/doc/refman/en/date-and-time-functions.html#function_convert-tz
 */
class ConvertTz extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    protected $dateExpression;
    protected $fromTz;
    protected $toTz;
    /**
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
