<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @link https://dev.mysql.com/doc/refman/en/charset-collate.html
 * @author Peter Tanath <peter.tanath@gmail.com>
 */
class Collate extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @var null
     */
    public $stringPrimary = null;
    /**
     * @var null
     */
    public $collation = null;
    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
