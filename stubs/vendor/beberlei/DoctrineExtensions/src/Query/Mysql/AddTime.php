<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Pascal Wacker <hello@pascalwacker.ch>
 */
class AddTime extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $date;
    public $time;
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
