<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @example SELECT SEC_TO_TIME(2378);
 * @link https://dev.mysql.com/doc/refman/en/date-and-time-functions.html#function_sec-to-time
 * @author Clemens Bastian <clemens.bastian@gmail.com>
 */
class SecToTime extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $time;
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
