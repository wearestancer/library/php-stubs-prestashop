<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Przemek Sobstel <przemek@sobstel.org>
 */
class TimestampDiff extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $firstDatetimeExpression = null;
    public $secondDatetimeExpression = null;
    public $unit = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sql_walker)
    {
    }
}
