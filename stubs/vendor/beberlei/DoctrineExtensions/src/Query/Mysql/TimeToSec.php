<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @example SELECT TIME_TO_SEC('22:23:00');
 * @link https://dev.mysql.com/doc/refman/en/date-and-time-functions.html#function_time-to-sec
 * @author Pawel Stasicki <p.stasicki@gmail.com>
 */
class TimeToSec extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $time;
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
