<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Rafael Kassner <kassner@gmail.com>
 * @author Sarjono Mukti Aji <me@simukti.net>
 * @author Łukasz Nowicki <lukasz.mnowicki@gmail.com>
 */
class Week extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $date;
    public $mode;
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
