<?php

namespace DoctrineExtensions\Query\Mysql;

class GroupConcat extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $isDistinct = false;
    public $pathExp = null;
    public $separator = null;
    public $orderBy = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
