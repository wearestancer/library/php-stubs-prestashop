<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * "CAST" "(" "$fieldIdentifierExpression" "AS" "$castingTypeExpression" ")"
 *
 * @example SELECT CAST(foo.bar AS SIGNED) FROM dual;
 *
 * @link https://dev.mysql.com/doc/refman/en/cast-functions.html#function_cast
 */
class Cast extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /** @var Node */
    protected $fieldIdentifierExpression;
    /** @var string */
    protected $castingTypeExpression;
    /**
     * @param Parser $parser
     *
     * @throws QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
