<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Steve Lacey <steve@steve.ly>
 */
class DateFormat extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $dateExpression = null;
    public $patternExpression = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
