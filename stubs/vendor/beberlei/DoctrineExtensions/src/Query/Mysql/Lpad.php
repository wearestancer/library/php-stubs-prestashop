<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Giulia Santoiemma <giuliaries@gmail.com>
 */
class Lpad extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $string = null;
    public $length = null;
    public $padstring = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
