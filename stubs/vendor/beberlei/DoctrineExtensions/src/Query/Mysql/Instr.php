<?php

namespace DoctrineExtensions\Query\Mysql;

class Instr extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $originalString = null;
    public $subString = null;
    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
