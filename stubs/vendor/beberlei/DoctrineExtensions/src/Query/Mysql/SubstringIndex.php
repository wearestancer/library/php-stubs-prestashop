<?php

namespace DoctrineExtensions\Query\Mysql;

class SubstringIndex extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $string = null;
    public $delimiter = null;
    public $count = null;
    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
