<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * "FROM_BASE64" "(" "$fieldIdentifierExpression" ")"
 *
 * @example SELECT FROM_BASE64(foo) FROM dual;
 *
 * @link https://dev.mysql.com/doc/refman/en/string-functions.html#function_from-base64
 */
class FromBase64 extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $field = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
