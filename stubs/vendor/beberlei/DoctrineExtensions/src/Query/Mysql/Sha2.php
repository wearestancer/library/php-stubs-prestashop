<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Andreas Gallien <gallien@seleos.de>
 */
class Sha2 extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $stringPrimary;
    public $simpleArithmeticExpression;
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
