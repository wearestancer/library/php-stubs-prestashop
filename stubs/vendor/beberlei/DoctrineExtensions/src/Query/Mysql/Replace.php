<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * @author Jarek Kostrz <jkostrz@gmail.com>
 */
class Replace extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $search = null;
    public $replace = null;
    public $subject = null;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
