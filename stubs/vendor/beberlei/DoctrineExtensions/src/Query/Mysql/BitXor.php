<?php

namespace DoctrineExtensions\Query\Mysql;

/**
 * "BIT_XOR" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
 */
class BitXor extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $firstArithmetic;
    public $secondArithmetic;
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
