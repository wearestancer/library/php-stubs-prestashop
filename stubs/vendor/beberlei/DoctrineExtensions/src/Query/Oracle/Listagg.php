<?php

namespace DoctrineExtensions\Query\Oracle;

/**
 * @author Alexey Kalinin <nitso@yandex.ru>
 */
class Listagg extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @var Node
     */
    public $separator = null;
    /**
     * @var Node
     */
    public $listaggField = null;
    /**
     * @var OrderByClause
     */
    public $orderBy;
    /**
     * @var Node[]
     */
    public $partitionBy = [];
    /**
     * @inheritdoc
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    /**
     * @inheritdoc
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
}
