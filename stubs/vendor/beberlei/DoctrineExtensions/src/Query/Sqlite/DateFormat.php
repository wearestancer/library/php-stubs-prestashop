<?php

namespace DoctrineExtensions\Query\Sqlite;

/**
 * This class fakes a DATE_FORMAT method for SQLite, so that we can use sqlite as drop-in replacement
 * database in our tests.
 *
 * @author Bas de Ruiter
 */
class DateFormat extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
