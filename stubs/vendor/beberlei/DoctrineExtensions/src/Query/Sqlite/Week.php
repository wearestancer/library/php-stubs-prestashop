<?php

namespace DoctrineExtensions\Query\Sqlite;

/**
 * @author Aleksandr Klimenkov <alx.devel@gmail.com>
 */
class Week extends \DoctrineExtensions\Query\Sqlite\NumberFromStrfTime
{
    /**
     * Currently not in use
     * @var int
     */
    public $mode;
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
    protected function getFormat()
    {
    }
}
