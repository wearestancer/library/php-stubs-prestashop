<?php

namespace DoctrineExtensions\Query\Sqlite;

/**
 * @author Tarjei Huse <tarjei.huse@gmail.com>
 */
class StrfTime extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    public $date;
    public $formatter;
    /**
     * @param SqlWalker $sqlWalker
     *
     * @throws \Doctrine\ORM\Query\QueryException
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
    }
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
    }
}
