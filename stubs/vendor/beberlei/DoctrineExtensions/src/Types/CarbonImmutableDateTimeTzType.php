<?php

namespace DoctrineExtensions\Types;

class CarbonImmutableDateTimeTzType extends \Doctrine\DBAL\Types\DateTimeTzType
{
    const CARBONDATETIMETZ = 'carbondatetimetz_immutable';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
