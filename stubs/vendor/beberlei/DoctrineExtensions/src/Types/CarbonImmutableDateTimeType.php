<?php

namespace DoctrineExtensions\Types;

class CarbonImmutableDateTimeType extends \Doctrine\DBAL\Types\DateTimeType
{
    const CARBONDATETIME = 'carbondatetime_immutable';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
