<?php

namespace DoctrineExtensions\Types;

class CarbonImmutableDateType extends \Doctrine\DBAL\Types\DateType
{
    const CARBONDATE = 'carbondate_immutable';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
