<?php

namespace DoctrineExtensions\Types;

class CarbonDateTimeTzType extends \Doctrine\DBAL\Types\DateTimeTzType
{
    const CARBONDATETIMETZ = 'carbondatetimetz';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
