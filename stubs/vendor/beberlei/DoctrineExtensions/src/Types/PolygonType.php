<?php

namespace DoctrineExtensions\Types;

class PolygonType extends \Doctrine\DBAL\Types\Type
{
    const FIELD = 'polygon';
    public function getSQLDeclaration(array $fieldDeclaration, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function canRequireSQLConversion()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
    }
    public function convertToDatabaseValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function convertToDatabaseValueSQL($sqlExpr, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function getName()
    {
    }
}
