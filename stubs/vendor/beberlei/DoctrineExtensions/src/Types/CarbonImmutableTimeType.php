<?php

namespace DoctrineExtensions\Types;

class CarbonImmutableTimeType extends \Doctrine\DBAL\Types\TimeType
{
    const CARBONTIME = 'carbontime_immutable';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
