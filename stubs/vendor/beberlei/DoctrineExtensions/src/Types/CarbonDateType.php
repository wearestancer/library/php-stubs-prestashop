<?php

namespace DoctrineExtensions\Types;

class CarbonDateType extends \Doctrine\DBAL\Types\DateType
{
    const CARBONDATE = 'carbondate';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
