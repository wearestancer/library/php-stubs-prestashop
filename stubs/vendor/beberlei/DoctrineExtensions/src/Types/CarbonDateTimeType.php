<?php

namespace DoctrineExtensions\Types;

class CarbonDateTimeType extends \Doctrine\DBAL\Types\DateTimeType
{
    const CARBONDATETIME = 'carbondatetime';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
