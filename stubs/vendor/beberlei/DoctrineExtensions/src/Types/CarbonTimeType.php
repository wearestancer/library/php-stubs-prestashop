<?php

namespace DoctrineExtensions\Types;

class CarbonTimeType extends \Doctrine\DBAL\Types\TimeType
{
    const CARBONTIME = 'carbontime';
    public function getName()
    {
    }
    public function convertToPHPValue($value, \Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
    public function requiresSQLCommentHint(\Doctrine\DBAL\Platforms\AbstractPlatform $platform)
    {
    }
}
