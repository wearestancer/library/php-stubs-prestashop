<?php

namespace Monolog;

/**
 * Monolog POSIX signal handler
 *
 * @author Robert Gust-Bardon <robert@gust-bardon.org>
 */
class SignalHandler
{
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
    }
    public function registerSignalHandler($signo, $level = \Psr\Log\LogLevel::CRITICAL, $callPrevious = true, $restartSyscalls = true, $async = true)
    {
    }
    public function handleSignal($signo, array $siginfo = null)
    {
    }
}
