<?php

namespace Monolog\Formatter;

/**
 * Formats a record for use with the MongoDBHandler.
 *
 * @author Florian Plattner <me@florianplattner.de>
 */
class MongoDBFormatter implements \Monolog\Formatter\FormatterInterface
{
    /**
     * @param int  $maxNestingLevel        0 means infinite nesting, the $record itself is level 1, $record['context'] is 2
     * @param bool $exceptionTraceAsString set to false to log exception traces as a sub documents instead of strings
     */
    public function __construct($maxNestingLevel = 3, $exceptionTraceAsString = true)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function format(array $record)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function formatBatch(array $records)
    {
    }
    protected function formatArray(array $record, $nestingLevel = 0)
    {
    }
    protected function formatObject($value, $nestingLevel)
    {
    }
    protected function formatException(\Exception $exception, $nestingLevel)
    {
    }
    protected function formatDate(\DateTime $value, $nestingLevel)
    {
    }
}
