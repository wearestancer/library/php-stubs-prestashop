<?php

namespace Monolog\Formatter;

/**
 * Formats data into an associative array of scalar values.
 * Objects and arrays will be JSON encoded.
 *
 * @author Andrew Lawson <adlawson@gmail.com>
 */
class ScalarFormatter extends \Monolog\Formatter\NormalizerFormatter
{
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    /**
     * @param  mixed $value
     * @return mixed
     */
    protected function normalizeValue($value)
    {
    }
}
