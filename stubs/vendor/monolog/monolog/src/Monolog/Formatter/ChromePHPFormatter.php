<?php

namespace Monolog\Formatter;

/**
 * Formats a log message according to the ChromePHP array format
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ChromePHPFormatter implements \Monolog\Formatter\FormatterInterface
{
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    public function formatBatch(array $records)
    {
    }
}
