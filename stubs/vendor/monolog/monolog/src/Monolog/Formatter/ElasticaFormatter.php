<?php

namespace Monolog\Formatter;

/**
 * Format a log message into an Elastica Document
 *
 * @author Jelle Vink <jelle.vink@gmail.com>
 */
class ElasticaFormatter extends \Monolog\Formatter\NormalizerFormatter
{
    /**
     * @var string Elastic search index name
     */
    protected $index;
    /**
     * @var string Elastic search document type
     */
    protected $type;
    /**
     * @param string $index Elastic Search index name
     * @param string $type  Elastic Search document type
     */
    public function __construct($index, $type)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    /**
     * Getter index
     * @return string
     */
    public function getIndex()
    {
    }
    /**
     * Getter type
     * @return string
     */
    public function getType()
    {
    }
    /**
     * Convert a log message into an Elastica Document
     *
     * @param  array    $record Log message
     * @return Document
     */
    protected function getDocument($record)
    {
    }
}
