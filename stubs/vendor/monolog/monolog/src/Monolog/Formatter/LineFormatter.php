<?php

namespace Monolog\Formatter;

/**
 * Formats incoming records into a one-line string
 *
 * This is especially useful for logging to files
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 * @author Christophe Coevoet <stof@notk.org>
 */
class LineFormatter extends \Monolog\Formatter\NormalizerFormatter
{
    const SIMPLE_FORMAT = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n";
    protected $format;
    protected $allowInlineLineBreaks;
    protected $ignoreEmptyContextAndExtra;
    protected $includeStacktraces;
    /**
     * @param string $format                     The format of the message
     * @param string $dateFormat                 The format of the timestamp: one supported by DateTime::format
     * @param bool   $allowInlineLineBreaks      Whether to allow inline line breaks in log entries
     * @param bool   $ignoreEmptyContextAndExtra
     */
    public function __construct($format = null, $dateFormat = null, $allowInlineLineBreaks = false, $ignoreEmptyContextAndExtra = false)
    {
    }
    public function includeStacktraces($include = true)
    {
    }
    public function allowInlineLineBreaks($allow = true)
    {
    }
    public function ignoreEmptyContextAndExtra($ignore = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    public function formatBatch(array $records)
    {
    }
    public function stringify($value)
    {
    }
    protected function normalizeException($e)
    {
    }
    protected function convertToString($data)
    {
    }
    protected function replaceNewlines($str)
    {
    }
}
