<?php

namespace Monolog\Formatter;

/**
 * formats the record to be used in the FlowdockHandler
 *
 * @author Dominik Liebler <liebler.dominik@gmail.com>
 */
class FlowdockFormatter implements \Monolog\Formatter\FormatterInterface
{
    /**
     * @param string $source
     * @param string $sourceEmail
     */
    public function __construct($source, $sourceEmail)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function formatBatch(array $records)
    {
    }
    /**
     * @param string $message
     *
     * @return string
     */
    public function getShortMessage($message)
    {
    }
}
