<?php

namespace Monolog\Formatter;

/**
 * Serializes a log message according to Wildfire's header requirements
 *
 * @author Eric Clemmons (@ericclemmons) <eric@uxdriven.com>
 * @author Christophe Coevoet <stof@notk.org>
 * @author Kirill chEbba Chebunin <iam@chebba.org>
 */
class WildfireFormatter extends \Monolog\Formatter\NormalizerFormatter
{
    const TABLE = 'table';
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    public function formatBatch(array $records)
    {
    }
    protected function normalize($data, $depth = 0)
    {
    }
}
