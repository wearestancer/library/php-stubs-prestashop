<?php

namespace Monolog\Formatter;

/**
 * Formats incoming records into an HTML table
 *
 * This is especially useful for html email logging
 *
 * @author Tiago Brito <tlfbrito@gmail.com>
 */
class HtmlFormatter extends \Monolog\Formatter\NormalizerFormatter
{
    /**
     * Translates Monolog log levels to html color priorities.
     */
    protected $logLevels = array(\Monolog\Logger::DEBUG => '#cccccc', \Monolog\Logger::INFO => '#468847', \Monolog\Logger::NOTICE => '#3a87ad', \Monolog\Logger::WARNING => '#c09853', \Monolog\Logger::ERROR => '#f0ad4e', \Monolog\Logger::CRITICAL => '#FF7708', \Monolog\Logger::ALERT => '#C12A19', \Monolog\Logger::EMERGENCY => '#000000');
    /**
     * @param string $dateFormat The format of the timestamp: one supported by DateTime::format
     */
    public function __construct($dateFormat = null)
    {
    }
    /**
     * Creates an HTML table row
     *
     * @param  string $th       Row header content
     * @param  string $td       Row standard cell content
     * @param  bool   $escapeTd false if td content must not be html escaped
     * @return string
     */
    protected function addRow($th, $td = ' ', $escapeTd = true)
    {
    }
    /**
     * Create a HTML h1 tag
     *
     * @param  string $title Text to be in the h1
     * @param  int    $level Error level
     * @return string
     */
    protected function addTitle($title, $level)
    {
    }
    /**
     * Formats a log record.
     *
     * @param  array $record A record to format
     * @return mixed The formatted record
     */
    public function format(array $record)
    {
    }
    /**
     * Formats a set of log records.
     *
     * @param  array $records A set of records to format
     * @return mixed The formatted set of records
     */
    public function formatBatch(array $records)
    {
    }
    protected function convertToString($data)
    {
    }
}
