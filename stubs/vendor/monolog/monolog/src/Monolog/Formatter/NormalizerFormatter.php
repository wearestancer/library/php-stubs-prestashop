<?php

namespace Monolog\Formatter;

/**
 * Normalizes incoming records to remove objects/resources so it's easier to dump to various targets
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class NormalizerFormatter implements \Monolog\Formatter\FormatterInterface
{
    const SIMPLE_DATE = "Y-m-d H:i:s";
    protected $dateFormat;
    protected $maxDepth;
    /**
     * @param string $dateFormat The format of the timestamp: one supported by DateTime::format
     * @param int $maxDepth
     */
    public function __construct($dateFormat = null, $maxDepth = 9)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function formatBatch(array $records)
    {
    }
    /**
     * @return int
     */
    public function getMaxDepth()
    {
    }
    /**
     * @param int $maxDepth
     */
    public function setMaxDepth($maxDepth)
    {
    }
    protected function normalize($data, $depth = 0)
    {
    }
    protected function normalizeException($e)
    {
    }
    /**
     * Return the JSON representation of a value
     *
     * @param  mixed             $data
     * @param  bool              $ignoreErrors
     * @throws \RuntimeException if encoding fails and errors are not ignored
     * @return string
     */
    protected function toJson($data, $ignoreErrors = false)
    {
    }
}
