<?php

namespace Monolog\Processor;

/**
 * Processes a record's message according to PSR-3 rules
 *
 * It replaces {foo} with the value from $context['foo']
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class PsrLogMessageProcessor implements \Monolog\Processor\ProcessorInterface
{
    const SIMPLE_DATE = "Y-m-d\\TH:i:s.uP";
    /**
     * @param string|null $dateFormat              The format of the timestamp: one supported by DateTime::format
     * @param bool        $removeUsedContextFields If set to true the fields interpolated into message gets unset
     */
    public function __construct($dateFormat = null, $removeUsedContextFields = false)
    {
    }
    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
    }
}
