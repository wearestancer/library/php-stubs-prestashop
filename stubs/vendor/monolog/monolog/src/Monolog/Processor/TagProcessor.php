<?php

namespace Monolog\Processor;

/**
 * Adds a tags array into record
 *
 * @author Martijn Riemers
 */
class TagProcessor implements \Monolog\Processor\ProcessorInterface
{
    public function __construct(array $tags = array())
    {
    }
    public function addTags(array $tags = array())
    {
    }
    public function setTags(array $tags = array())
    {
    }
    public function __invoke(array $record)
    {
    }
}
