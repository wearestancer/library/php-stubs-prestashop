<?php

namespace Monolog\Processor;

/**
 * Injects line/file:class/function where the log message came from
 *
 * Warning: This only works if the handler processes the logs directly.
 * If you put the processor on a handler that is behind a FingersCrossedHandler
 * for example, the processor will only be called once the trigger level is reached,
 * and all the log records will have the same file/line/.. data from the call that
 * triggered the FingersCrossedHandler.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class IntrospectionProcessor implements \Monolog\Processor\ProcessorInterface
{
    public function __construct($level = \Monolog\Logger::DEBUG, array $skipClassesPartials = array(), $skipStackFramesCount = 0)
    {
    }
    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
    }
}
