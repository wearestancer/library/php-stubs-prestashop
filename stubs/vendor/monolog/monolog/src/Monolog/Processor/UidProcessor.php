<?php

namespace Monolog\Processor;

/**
 * Adds a unique identifier into records
 *
 * @author Simon Mönch <sm@webfactory.de>
 */
class UidProcessor implements \Monolog\Processor\ProcessorInterface, \Monolog\ResettableInterface
{
    public function __construct($length = 7)
    {
    }
    public function __invoke(array $record)
    {
    }
    /**
     * @return string
     */
    public function getUid()
    {
    }
    public function reset()
    {
    }
}
