<?php

namespace Monolog\Processor;

/**
 * Injects Git branch and Git commit SHA in all records
 *
 * @author Nick Otter
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class GitProcessor implements \Monolog\Processor\ProcessorInterface
{
    public function __construct($level = \Monolog\Logger::DEBUG)
    {
    }
    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
    }
}
