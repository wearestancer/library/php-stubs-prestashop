<?php

namespace Monolog\Processor;

/**
 * Injects Hg branch and Hg revision number in all records
 *
 * @author Jonathan A. Schweder <jonathanschweder@gmail.com>
 */
class MercurialProcessor implements \Monolog\Processor\ProcessorInterface
{
    public function __construct($level = \Monolog\Logger::DEBUG)
    {
    }
    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
    }
}
