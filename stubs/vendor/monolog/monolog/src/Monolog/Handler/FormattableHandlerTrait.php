<?php

namespace Monolog\Handler;

/**
 * Helper trait for implementing FormattableInterface
 *
 * This trait is present in monolog 1.x to ease forward compatibility.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
trait FormattableHandlerTrait
{
    /**
     * @var FormatterInterface
     */
    protected $formatter;
    /**
     * {@inheritdoc}
     * @suppress PhanTypeMismatchReturn
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter) : \Monolog\Handler\HandlerInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormatter() : \Monolog\Formatter\FormatterInterface
    {
    }
    /**
     * Gets the default formatter.
     *
     * Overwrite this if the LineFormatter is not a good default for your handler.
     */
    protected function getDefaultFormatter() : \Monolog\Formatter\FormatterInterface
    {
    }
}
