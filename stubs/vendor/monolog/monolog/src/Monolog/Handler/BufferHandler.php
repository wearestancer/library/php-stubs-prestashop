<?php

namespace Monolog\Handler;

/**
 * Buffers all records until closing the handler and then pass them as batch.
 *
 * This is useful for a MailHandler to send only one mail per request instead of
 * sending one per log message.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class BufferHandler extends \Monolog\Handler\AbstractHandler
{
    protected $handler;
    protected $bufferSize = 0;
    protected $bufferLimit;
    protected $flushOnOverflow;
    protected $buffer = array();
    protected $initialized = false;
    /**
     * @param HandlerInterface $handler         Handler.
     * @param int              $bufferLimit     How many entries should be buffered at most, beyond that the oldest items are removed from the buffer.
     * @param int              $level           The minimum logging level at which this handler will be triggered
     * @param bool             $bubble          Whether the messages that are handled can bubble up the stack or not
     * @param bool             $flushOnOverflow If true, the buffer is flushed when the max size has been reached, by default oldest entries are discarded
     */
    public function __construct(\Monolog\Handler\HandlerInterface $handler, $bufferLimit = 0, $level = \Monolog\Logger::DEBUG, $bubble = true, $flushOnOverflow = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(array $record)
    {
    }
    public function flush()
    {
    }
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * Clears the buffer without flushing any messages down to the wrapped handler.
     */
    public function clear()
    {
    }
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormatter()
    {
    }
}
