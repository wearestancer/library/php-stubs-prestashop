<?php

namespace Monolog\Handler;

/**
 * Blackhole
 *
 * Any record it can handle will be thrown away. This can be used
 * to put on top of an existing stack to override it temporarily.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class NullHandler extends \Monolog\Handler\AbstractHandler
{
    /**
     * @param int $level The minimum logging level at which this handler will be triggered
     */
    public function __construct($level = \Monolog\Logger::DEBUG)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(array $record)
    {
    }
}
