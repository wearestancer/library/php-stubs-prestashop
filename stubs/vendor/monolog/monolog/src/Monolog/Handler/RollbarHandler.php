<?php

namespace Monolog\Handler;

/**
 * Sends errors to Rollbar
 *
 * If the context data contains a `payload` key, that is used as an array
 * of payload options to RollbarNotifier's report_message/report_exception methods.
 *
 * Rollbar's context info will contain the context + extra keys from the log record
 * merged, and then on top of that a few keys:
 *
 *  - level (rollbar level name)
 *  - monolog_level (monolog level name, raw level, as rollbar only has 5 but monolog 8)
 *  - channel
 *  - datetime (unix timestamp)
 *
 * @author Paul Statezny <paulstatezny@gmail.com>
 */
class RollbarHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    /**
     * Rollbar notifier
     *
     * @var RollbarNotifier
     */
    protected $rollbarNotifier;
    protected $levelMap = array(\Monolog\Logger::DEBUG => 'debug', \Monolog\Logger::INFO => 'info', \Monolog\Logger::NOTICE => 'info', \Monolog\Logger::WARNING => 'warning', \Monolog\Logger::ERROR => 'error', \Monolog\Logger::CRITICAL => 'critical', \Monolog\Logger::ALERT => 'critical', \Monolog\Logger::EMERGENCY => 'critical');
    protected $initialized = false;
    /**
     * @param RollbarNotifier $rollbarNotifier RollbarNotifier object constructed with valid token
     * @param int             $level           The minimum logging level at which this handler will be triggered
     * @param bool            $bubble          Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct(\RollbarNotifier $rollbarNotifier, $level = \Monolog\Logger::ERROR, $bubble = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
    }
    public function flush()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
