<?php

namespace Monolog\Handler;

/**
 * Logs to syslog service.
 *
 * usage example:
 *
 *   $log = new Logger('application');
 *   $syslog = new SyslogHandler('myfacility', 'local6');
 *   $formatter = new LineFormatter("%channel%.%level_name%: %message% %extra%");
 *   $syslog->setFormatter($formatter);
 *   $log->pushHandler($syslog);
 *
 * @author Sven Paulus <sven@karlsruhe.org>
 */
class SyslogHandler extends \Monolog\Handler\AbstractSyslogHandler
{
    protected $ident;
    protected $logopts;
    /**
     * @param string $ident
     * @param mixed  $facility
     * @param int    $level    The minimum logging level at which this handler will be triggered
     * @param bool   $bubble   Whether the messages that are handled can bubble up the stack or not
     * @param int    $logopts  Option flags for the openlog() call, defaults to LOG_PID
     */
    public function __construct($ident, $facility = LOG_USER, $level = \Monolog\Logger::DEBUG, $bubble = true, $logopts = LOG_PID)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
    }
}
