<?php

namespace Monolog\Handler;

/**
 * Forwards records to multiple handlers
 *
 * @author Lenar Lõhmus <lenar@city.ee>
 */
class GroupHandler extends \Monolog\Handler\AbstractHandler
{
    protected $handlers;
    /**
     * @param array $handlers Array of Handlers.
     * @param bool  $bubble   Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct(array $handlers, $bubble = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
    }
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
    }
}
