<?php

namespace Monolog\Handler;

/**
 * Stores to any stream resource
 *
 * Can be used to store into php://stderr, remote and local files, etc.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class StreamHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    /** @private 512KB */
    const CHUNK_SIZE = 524288;
    /** @var resource|null */
    protected $stream;
    protected $url;
    protected $filePermission;
    protected $useLocking;
    /**
     * @param resource|string $stream
     * @param int             $level          The minimum logging level at which this handler will be triggered
     * @param bool            $bubble         Whether the messages that are handled can bubble up the stack or not
     * @param int|null        $filePermission Optional file permissions (default (0644) are only for owner read/write)
     * @param bool            $useLocking     Try to lock log file before doing any writes
     *
     * @throws \Exception                If a missing directory is not buildable
     * @throws \InvalidArgumentException If stream is not a resource or string
     */
    public function __construct($stream, $level = \Monolog\Logger::DEBUG, $bubble = true, $filePermission = null, $useLocking = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * Return the currently active stream if it is open
     *
     * @return resource|null
     */
    public function getStream()
    {
    }
    /**
     * Return the stream URL if it was configured with a URL and not an active resource
     *
     * @return string|null
     */
    public function getUrl()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
    }
    /**
     * Write to stream
     * @param resource $stream
     * @param array $record
     */
    protected function streamWrite($stream, array $record)
    {
    }
    protected function streamSetChunkSize()
    {
    }
}
