<?php

namespace Monolog\Handler;

/**
 * Logs to a Redis key using rpush
 *
 * usage example:
 *
 *   $log = new Logger('application');
 *   $redis = new RedisHandler(new Predis\Client("tcp://localhost:6379"), "logs", "prod");
 *   $log->pushHandler($redis);
 *
 * @author Thomas Tourlourat <thomas@tourlourat.com>
 */
class RedisHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    protected $capSize;
    /**
     * @param \Predis\Client|\Redis $redis   The redis instance
     * @param string                $key     The key name to push records to
     * @param int                   $level   The minimum logging level at which this handler will be triggered
     * @param bool                  $bubble  Whether the messages that are handled can bubble up the stack or not
     * @param int|false             $capSize Number of entries to limit list size to
     */
    public function __construct($redis, $key, $level = \Monolog\Logger::DEBUG, $bubble = true, $capSize = false)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
    }
    /**
     * Write and cap the collection
     * Writes the record to the redis list and caps its
     *
     * @param  array $record associative record array
     * @return void
     */
    protected function writeCapped(array $record)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter()
    {
    }
}
