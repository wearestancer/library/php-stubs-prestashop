<?php

namespace Monolog\Handler;

/**
 * A Handler for logging to a remote syslogd server.
 *
 * @author Jesper Skovgaard Nielsen <nulpunkt@gmail.com>
 * @author Dominik Kukacka <dominik.kukacka@gmail.com>
 */
class SyslogUdpHandler extends \Monolog\Handler\AbstractSyslogHandler
{
    const RFC3164 = 0;
    const RFC5424 = 1;
    protected $socket;
    protected $ident;
    protected $rfc;
    /**
     * @param string $host
     * @param int    $port
     * @param mixed  $facility
     * @param int    $level    The minimum logging level at which this handler will be triggered
     * @param bool   $bubble   Whether the messages that are handled can bubble up the stack or not
     * @param string $ident    Program name or tag for each log message.
     * @param int    $rfc      RFC to format the message for.
     */
    public function __construct($host, $port = 514, $facility = LOG_USER, $level = \Monolog\Logger::DEBUG, $bubble = true, $ident = 'php', $rfc = self::RFC5424)
    {
    }
    protected function write(array $record)
    {
    }
    public function close()
    {
    }
    /**
     * Make common syslog header (see rfc5424 or rfc3164)
     */
    protected function makeCommonSyslogHeader($severity)
    {
    }
    protected function getDateTime()
    {
    }
    /**
     * Inject your own socket, mainly used for testing
     */
    public function setSocket($socket)
    {
    }
}
