<?php

namespace Monolog\Handler;

/**
 * Stores to any socket - uses fsockopen() or pfsockopen().
 *
 * @author Pablo de Leon Belloc <pablolb@gmail.com>
 * @see    http://php.net/manual/en/function.fsockopen.php
 */
class SocketHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    /**
     * @param string $connectionString Socket connection string
     * @param int    $level            The minimum logging level at which this handler will be triggered
     * @param bool   $bubble           Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct($connectionString, $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * Connect (if necessary) and write to the socket
     *
     * @param array $record
     *
     * @throws \UnexpectedValueException
     * @throws \RuntimeException
     */
    protected function write(array $record)
    {
    }
    /**
     * We will not close a PersistentSocket instance so it can be reused in other requests.
     */
    public function close()
    {
    }
    /**
     * Close socket, if open
     */
    public function closeSocket()
    {
    }
    /**
     * Set socket connection to nbe persistent. It only has effect before the connection is initiated.
     *
     * @param bool $persistent
     */
    public function setPersistent($persistent)
    {
    }
    /**
     * Set connection timeout.  Only has effect before we connect.
     *
     * @param float $seconds
     *
     * @see http://php.net/manual/en/function.fsockopen.php
     */
    public function setConnectionTimeout($seconds)
    {
    }
    /**
     * Set write timeout. Only has effect before we connect.
     *
     * @param float $seconds
     *
     * @see http://php.net/manual/en/function.stream-set-timeout.php
     */
    public function setTimeout($seconds)
    {
    }
    /**
     * Set writing timeout. Only has effect during connection in the writing cycle.
     *
     * @param float $seconds 0 for no timeout
     */
    public function setWritingTimeout($seconds)
    {
    }
    /**
     * Set chunk size. Only has effect during connection in the writing cycle.
     *
     * @param float $bytes
     */
    public function setChunkSize($bytes)
    {
    }
    /**
     * Get current connection string
     *
     * @return string
     */
    public function getConnectionString()
    {
    }
    /**
     * Get persistent setting
     *
     * @return bool
     */
    public function isPersistent()
    {
    }
    /**
     * Get current connection timeout setting
     *
     * @return float
     */
    public function getConnectionTimeout()
    {
    }
    /**
     * Get current in-transfer timeout
     *
     * @return float
     */
    public function getTimeout()
    {
    }
    /**
     * Get current local writing timeout
     *
     * @return float
     */
    public function getWritingTimeout()
    {
    }
    /**
     * Get current chunk size
     *
     * @return float
     */
    public function getChunkSize()
    {
    }
    /**
     * Check to see if the socket is currently available.
     *
     * UDP might appear to be connected but might fail when writing.  See http://php.net/fsockopen for details.
     *
     * @return bool
     */
    public function isConnected()
    {
    }
    /**
     * Wrapper to allow mocking
     */
    protected function pfsockopen()
    {
    }
    /**
     * Wrapper to allow mocking
     */
    protected function fsockopen()
    {
    }
    /**
     * Wrapper to allow mocking
     *
     * @see http://php.net/manual/en/function.stream-set-timeout.php
     */
    protected function streamSetTimeout()
    {
    }
    /**
     * Wrapper to allow mocking
     *
     * @see http://php.net/manual/en/function.stream-set-chunk-size.php
     */
    protected function streamSetChunkSize()
    {
    }
    /**
     * Wrapper to allow mocking
     */
    protected function fwrite($data)
    {
    }
    /**
     * Wrapper to allow mocking
     */
    protected function streamGetMetadata()
    {
    }
    protected function generateDataStream($record)
    {
    }
    /**
     * @return resource|null
     */
    protected function getResource()
    {
    }
}
