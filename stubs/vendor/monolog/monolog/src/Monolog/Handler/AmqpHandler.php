<?php

namespace Monolog\Handler;

class AmqpHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    /**
     * @var AMQPExchange|AMQPChannel $exchange
     */
    protected $exchange;
    /**
     * @var string
     */
    protected $exchangeName;
    /**
     * @param AMQPExchange|AMQPChannel $exchange     AMQPExchange (php AMQP ext) or PHP AMQP lib channel, ready for use
     * @param string                   $exchangeName
     * @param int                      $level
     * @param bool                     $bubble       Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct($exchange, $exchangeName = 'log', $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function handleBatch(array $records)
    {
    }
    /**
     * Gets the routing key for the AMQP exchange
     *
     * @param  array  $record
     * @return string
     */
    protected function getRoutingKey(array $record)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter()
    {
    }
}
