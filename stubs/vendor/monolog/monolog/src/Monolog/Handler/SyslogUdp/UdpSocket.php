<?php

namespace Monolog\Handler\SyslogUdp;

class UdpSocket
{
    const DATAGRAM_MAX_LENGTH = 65023;
    protected $ip;
    protected $port;
    protected $socket;
    public function __construct($ip, $port = 514)
    {
    }
    public function write($line, $header = "")
    {
    }
    public function close()
    {
    }
    protected function send($chunk)
    {
    }
    protected function assembleMessage($line, $header)
    {
    }
}
