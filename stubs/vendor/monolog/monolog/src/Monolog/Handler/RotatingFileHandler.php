<?php

namespace Monolog\Handler;

/**
 * Stores logs to files that are rotated every day and a limited number of files are kept.
 *
 * This rotation is only intended to be used as a workaround. Using logrotate to
 * handle the rotation is strongly encouraged when you can use it.
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class RotatingFileHandler extends \Monolog\Handler\StreamHandler
{
    const FILE_PER_DAY = 'Y-m-d';
    const FILE_PER_MONTH = 'Y-m';
    const FILE_PER_YEAR = 'Y';
    protected $filename;
    protected $maxFiles;
    protected $mustRotate;
    protected $nextRotation;
    protected $filenameFormat;
    protected $dateFormat;
    /**
     * @param string   $filename
     * @param int      $maxFiles       The maximal amount of files to keep (0 means unlimited)
     * @param int      $level          The minimum logging level at which this handler will be triggered
     * @param bool     $bubble         Whether the messages that are handled can bubble up the stack or not
     * @param int|null $filePermission Optional file permissions (default (0644) are only for owner read/write)
     * @param bool     $useLocking     Try to lock log file before doing any writes
     */
    public function __construct($filename, $maxFiles = 0, $level = \Monolog\Logger::DEBUG, $bubble = true, $filePermission = null, $useLocking = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    public function setFilenameFormat($filenameFormat, $dateFormat)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
    }
    /**
     * Rotates the files.
     */
    protected function rotate()
    {
    }
    protected function getTimedFilename()
    {
    }
    protected function getGlobPattern()
    {
    }
}
