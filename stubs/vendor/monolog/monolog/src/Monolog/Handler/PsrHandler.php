<?php

namespace Monolog\Handler;

/**
 * Proxies log messages to an existing PSR-3 compliant logger.
 *
 * @author Michael Moussa <michael.moussa@gmail.com>
 */
class PsrHandler extends \Monolog\Handler\AbstractHandler
{
    /**
     * PSR-3 compliant logger
     *
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @param LoggerInterface $logger The underlying PSR-3 compliant logger to which messages will be proxied
     * @param int             $level  The minimum logging level at which this handler will be triggered
     * @param bool            $bubble Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct(\Psr\Log\LoggerInterface $logger, $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function handle(array $record)
    {
    }
}
