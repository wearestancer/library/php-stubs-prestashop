<?php

namespace Monolog\Handler;

/**
 * Sampling handler
 *
 * A sampled event stream can be useful for logging high frequency events in
 * a production environment where you only need an idea of what is happening
 * and are not concerned with capturing every occurrence. Since the decision to
 * handle or not handle a particular event is determined randomly, the
 * resulting sampled log is not guaranteed to contain 1/N of the events that
 * occurred in the application, but based on the Law of large numbers, it will
 * tend to be close to this ratio with a large number of attempts.
 *
 * @author Bryan Davis <bd808@wikimedia.org>
 * @author Kunal Mehta <legoktm@gmail.com>
 */
class SamplingHandler extends \Monolog\Handler\AbstractHandler
{
    /**
     * @var callable|HandlerInterface $handler
     */
    protected $handler;
    /**
     * @var int $factor
     */
    protected $factor;
    /**
     * @param callable|HandlerInterface $handler Handler or factory callable($record|null, $samplingHandler).
     * @param int                       $factor  Sample factor
     */
    public function __construct($handler, $factor)
    {
    }
    public function isHandling(array $record)
    {
    }
    public function handle(array $record)
    {
    }
    /**
     * Return the nested handler
     *
     * If the handler was provided as a factory callable, this will trigger the handler's instantiation.
     *
     * @return HandlerInterface
     */
    public function getHandler(array $record = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormatter()
    {
    }
}
