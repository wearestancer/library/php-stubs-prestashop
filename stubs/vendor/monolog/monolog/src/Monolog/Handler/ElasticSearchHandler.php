<?php

namespace Monolog\Handler;

/**
 * Elastic Search handler
 *
 * Usage example:
 *
 *    $client = new \Elastica\Client();
 *    $options = array(
 *        'index' => 'elastic_index_name',
 *        'type' => 'elastic_doc_type',
 *    );
 *    $handler = new ElasticSearchHandler($client, $options);
 *    $log = new Logger('application');
 *    $log->pushHandler($handler);
 *
 * @author Jelle Vink <jelle.vink@gmail.com>
 */
class ElasticSearchHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    /**
     * @var Client
     */
    protected $client;
    /**
     * @var array Handler config options
     */
    protected $options = array();
    /**
     * @param Client $client  Elastica Client object
     * @param array  $options Handler configuration
     * @param int    $level   The minimum logging level at which this handler will be triggered
     * @param bool   $bubble  Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct(\Elastica\Client $client, array $options = array(), $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
    }
    /**
     * Getter options
     * @return array
     */
    public function getOptions()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
    }
    /**
     * Use Elasticsearch bulk API to send list of documents
     * @param  array             $documents
     * @throws \RuntimeException
     */
    protected function bulkSend(array $documents)
    {
    }
}
