<?php

namespace Monolog\Handler;

/**
 * This simple wrapper class can be used to extend handlers functionality.
 *
 * Example: A custom filtering that can be applied to any handler.
 *
 * Inherit from this class and override handle() like this:
 *
 *   public function handle(array $record)
 *   {
 *        if ($record meets certain conditions) {
 *            return false;
 *        }
 *        return $this->handler->handle($record);
 *   }
 *
 * @author Alexey Karapetov <alexey@karapetov.com>
 */
class HandlerWrapper implements \Monolog\Handler\HandlerInterface, \Monolog\ResettableInterface
{
    /**
     * @var HandlerInterface
     */
    protected $handler;
    /**
     * HandlerWrapper constructor.
     * @param HandlerInterface $handler
     */
    public function __construct(\Monolog\Handler\HandlerInterface $handler)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function pushProcessor($callback)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function popProcessor()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormatter()
    {
    }
    public function reset()
    {
    }
}
