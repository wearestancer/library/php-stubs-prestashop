<?php

namespace Monolog\Handler;

/**
 * CouchDB handler for Doctrine CouchDB ODM
 *
 * @author Markus Bachmann <markus.bachmann@bachi.biz>
 */
class DoctrineCouchDBHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    public function __construct(\Doctrine\CouchDB\CouchDBClient $client, $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
    }
    protected function getDefaultFormatter()
    {
    }
}
