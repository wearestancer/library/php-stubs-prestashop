<?php

namespace Monolog\Handler;

/**
 * MandrillHandler uses cURL to send the emails to the Mandrill API
 *
 * @author Adam Nicholson <adamnicholson10@gmail.com>
 */
class MandrillHandler extends \Monolog\Handler\MailHandler
{
    protected $message;
    protected $apiKey;
    /**
     * @param string                  $apiKey  A valid Mandrill API key
     * @param callable|\Swift_Message $message An example message for real messages, only the body will be replaced
     * @param int                     $level   The minimum logging level at which this handler will be triggered
     * @param bool                    $bubble  Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct($apiKey, $message, $level = \Monolog\Logger::ERROR, $bubble = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function send($content, array $records)
    {
    }
}
