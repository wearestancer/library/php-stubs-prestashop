<?php

namespace Monolog\Handler;

/**
 * NativeMailerHandler uses the mail() function to send the emails
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Mark Garrett <mark@moderndeveloperllc.com>
 */
class NativeMailerHandler extends \Monolog\Handler\MailHandler
{
    /**
     * The email addresses to which the message will be sent
     * @var array
     */
    protected $to;
    /**
     * The subject of the email
     * @var string
     */
    protected $subject;
    /**
     * Optional headers for the message
     * @var array
     */
    protected $headers = array();
    /**
     * Optional parameters for the message
     * @var array
     */
    protected $parameters = array();
    /**
     * The wordwrap length for the message
     * @var int
     */
    protected $maxColumnWidth;
    /**
     * The Content-type for the message
     * @var string
     */
    protected $contentType = 'text/plain';
    /**
     * The encoding for the message
     * @var string
     */
    protected $encoding = 'utf-8';
    /**
     * @param string|array $to             The receiver of the mail
     * @param string       $subject        The subject of the mail
     * @param string       $from           The sender of the mail
     * @param int          $level          The minimum logging level at which this handler will be triggered
     * @param bool         $bubble         Whether the messages that are handled can bubble up the stack or not
     * @param int          $maxColumnWidth The maximum column width that the message lines will have
     */
    public function __construct($to, $subject, $from, $level = \Monolog\Logger::ERROR, $bubble = true, $maxColumnWidth = 70)
    {
    }
    /**
     * Add headers to the message
     *
     * @param  string|array $headers Custom added headers
     * @return self
     */
    public function addHeader($headers)
    {
    }
    /**
     * Add parameters to the message
     *
     * @param  string|array $parameters Custom added parameters
     * @return self
     */
    public function addParameter($parameters)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function send($content, array $records)
    {
    }
    /**
     * @return string $contentType
     */
    public function getContentType()
    {
    }
    /**
     * @return string $encoding
     */
    public function getEncoding()
    {
    }
    /**
     * @param  string $contentType The content type of the email - Defaults to text/plain. Use text/html for HTML
     *                             messages.
     * @return self
     */
    public function setContentType($contentType)
    {
    }
    /**
     * @param  string $encoding
     * @return self
     */
    public function setEncoding($encoding)
    {
    }
}
