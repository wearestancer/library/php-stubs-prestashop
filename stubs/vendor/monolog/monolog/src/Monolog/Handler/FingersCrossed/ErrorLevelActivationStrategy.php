<?php

namespace Monolog\Handler\FingersCrossed;

/**
 * Error level based activation strategy.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ErrorLevelActivationStrategy implements \Monolog\Handler\FingersCrossed\ActivationStrategyInterface
{
    public function __construct($actionLevel)
    {
    }
    public function isHandlerActivated(array $record)
    {
    }
}
