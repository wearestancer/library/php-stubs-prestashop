<?php

namespace Monolog\Handler;

/**
 * CouchDB handler
 *
 * @author Markus Bachmann <markus.bachmann@bachi.biz>
 */
class CouchDBHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    public function __construct(array $options = array(), $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter()
    {
    }
}
