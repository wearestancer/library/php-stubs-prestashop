<?php

namespace Monolog\Handler;

/**
 * Simple handler wrapper that filters records based on a list of levels
 *
 * It can be configured with an exact list of levels to allow, or a min/max level.
 *
 * @author Hennadiy Verkh
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class FilterHandler extends \Monolog\Handler\AbstractHandler
{
    /**
     * Handler or factory callable($record, $this)
     *
     * @var callable|\Monolog\Handler\HandlerInterface
     */
    protected $handler;
    /**
     * Minimum level for logs that are passed to handler
     *
     * @var int[]
     */
    protected $acceptedLevels;
    /**
     * Whether the messages that are handled can bubble up the stack or not
     *
     * @var bool
     */
    protected $bubble;
    /**
     * @param callable|HandlerInterface $handler        Handler or factory callable($record|null, $filterHandler).
     * @param int|array                 $minLevelOrList A list of levels to accept or a minimum level if maxLevel is provided
     * @param int                       $maxLevel       Maximum level to accept, only used if $minLevelOrList is not an array
     * @param bool                      $bubble         Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct($handler, $minLevelOrList = \Monolog\Logger::DEBUG, $maxLevel = \Monolog\Logger::EMERGENCY, $bubble = true)
    {
    }
    /**
     * @return array
     */
    public function getAcceptedLevels()
    {
    }
    /**
     * @param int|string|array $minLevelOrList A list of levels to accept or a minimum level or level name if maxLevel is provided
     * @param int|string       $maxLevel       Maximum level or level name to accept, only used if $minLevelOrList is not an array
     */
    public function setAcceptedLevels($minLevelOrList = \Monolog\Logger::DEBUG, $maxLevel = \Monolog\Logger::EMERGENCY)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
    }
    /**
     * Return the nested handler
     *
     * If the handler was provided as a factory callable, this will trigger the handler's instantiation.
     *
     * @return HandlerInterface
     */
    public function getHandler(array $record = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormatter()
    {
    }
}
