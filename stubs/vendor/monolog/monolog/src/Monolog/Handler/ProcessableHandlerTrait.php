<?php

namespace Monolog\Handler;

/**
 * Helper trait for implementing ProcessableInterface
 *
 * This trait is present in monolog 1.x to ease forward compatibility.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
trait ProcessableHandlerTrait
{
    /**
     * @var callable[]
     */
    protected $processors = [];
    /**
     * {@inheritdoc}
     * @suppress PhanTypeMismatchReturn
     */
    public function pushProcessor($callback) : \Monolog\Handler\HandlerInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function popProcessor() : callable
    {
    }
    /**
     * Processes a record.
     */
    protected function processRecord(array $record) : array
    {
    }
    protected function resetProcessors() : void
    {
    }
}
