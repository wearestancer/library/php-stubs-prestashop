<?php

namespace Monolog\Handler;

/**
 * Common syslog functionality
 */
abstract class AbstractSyslogHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    protected $facility;
    /**
     * Translates Monolog log levels to syslog log priorities.
     */
    protected $logLevels = array(\Monolog\Logger::DEBUG => LOG_DEBUG, \Monolog\Logger::INFO => LOG_INFO, \Monolog\Logger::NOTICE => LOG_NOTICE, \Monolog\Logger::WARNING => LOG_WARNING, \Monolog\Logger::ERROR => LOG_ERR, \Monolog\Logger::CRITICAL => LOG_CRIT, \Monolog\Logger::ALERT => LOG_ALERT, \Monolog\Logger::EMERGENCY => LOG_EMERG);
    /**
     * List of valid log facility names.
     */
    protected $facilities = array('auth' => LOG_AUTH, 'authpriv' => LOG_AUTHPRIV, 'cron' => LOG_CRON, 'daemon' => LOG_DAEMON, 'kern' => LOG_KERN, 'lpr' => LOG_LPR, 'mail' => LOG_MAIL, 'news' => LOG_NEWS, 'syslog' => LOG_SYSLOG, 'user' => LOG_USER, 'uucp' => LOG_UUCP);
    /**
     * @param mixed $facility
     * @param int   $level The minimum logging level at which this handler will be triggered
     * @param bool  $bubble Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct($facility = LOG_USER, $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDefaultFormatter()
    {
    }
}
