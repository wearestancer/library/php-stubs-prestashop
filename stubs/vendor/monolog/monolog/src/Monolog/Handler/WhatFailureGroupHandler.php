<?php

namespace Monolog\Handler;

/**
 * Forwards records to multiple handlers suppressing failures of each handler
 * and continuing through to give every handler a chance to succeed.
 *
 * @author Craig D'Amelio <craig@damelio.ca>
 */
class WhatFailureGroupHandler extends \Monolog\Handler\GroupHandler
{
    /**
     * {@inheritdoc}
     */
    public function handle(array $record)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
    }
}
