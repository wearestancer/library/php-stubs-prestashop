<?php

namespace Monolog\Handler;

/**
 * Base class for all mail handlers
 *
 * @author Gyula Sallai
 */
abstract class MailHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
    }
    /**
     * Send a mail with the given content
     *
     * @param string $content formatted email body to be sent
     * @param array  $records the array of log records that formed this content
     */
    protected abstract function send($content, array $records);
    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
    }
    protected function getHighestRecord(array $records)
    {
    }
}
