<?php

namespace Monolog\Handler;

/**
 * Sends errors to Loggly.
 *
 * @author Przemek Sobstel <przemek@sobstel.org>
 * @author Adam Pancutt <adam@pancutt.com>
 * @author Gregory Barchard <gregory@barchard.net>
 */
class LogglyHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    const HOST = 'logs-01.loggly.com';
    const ENDPOINT_SINGLE = 'inputs';
    const ENDPOINT_BATCH = 'bulk';
    protected $token;
    protected $tag = array();
    public function __construct($token, $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
    }
    public function setTag($tag)
    {
    }
    public function addTag($tag)
    {
    }
    protected function write(array $record)
    {
    }
    public function handleBatch(array $records)
    {
    }
    protected function send($data, $endpoint)
    {
    }
    protected function getDefaultFormatter()
    {
    }
}
