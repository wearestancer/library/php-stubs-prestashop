<?php

namespace Monolog\Handler;

/**
 * Stores to PHP error_log() handler.
 *
 * @author Elan Ruusamäe <glen@delfi.ee>
 */
class ErrorLogHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    const OPERATING_SYSTEM = 0;
    const SAPI = 4;
    protected $messageType;
    protected $expandNewlines;
    /**
     * @param int  $messageType    Says where the error should go.
     * @param int  $level          The minimum logging level at which this handler will be triggered
     * @param bool $bubble         Whether the messages that are handled can bubble up the stack or not
     * @param bool $expandNewlines If set to true, newlines in the message will be expanded to be take multiple log entries
     */
    public function __construct($messageType = self::OPERATING_SYSTEM, $level = \Monolog\Logger::DEBUG, $bubble = true, $expandNewlines = false)
    {
    }
    /**
     * @return array With all available types
     */
    public static function getAvailableTypes()
    {
    }
    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
    }
}
