<?php

namespace Monolog;

/**
 * Monolog error handler
 *
 * A facility to enable logging of runtime errors, exceptions and fatal errors.
 *
 * Quick setup: <code>ErrorHandler::register($logger);</code>
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class ErrorHandler
{
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
    }
    /**
     * Registers a new ErrorHandler for a given Logger
     *
     * By default it will handle errors, exceptions and fatal errors
     *
     * @param  LoggerInterface $logger
     * @param  array|false     $errorLevelMap  an array of E_* constant to LogLevel::* constant mapping, or false to disable error handling
     * @param  int|false       $exceptionLevel a LogLevel::* constant, or false to disable exception handling
     * @param  int|false       $fatalLevel     a LogLevel::* constant, or false to disable fatal error handling
     * @return ErrorHandler
     */
    public static function register(\Psr\Log\LoggerInterface $logger, $errorLevelMap = array(), $exceptionLevel = null, $fatalLevel = null)
    {
    }
    public function registerExceptionHandler($level = null, $callPrevious = true)
    {
    }
    public function registerErrorHandler(array $levelMap = array(), $callPrevious = true, $errorTypes = -1, $handleOnlyReportedErrors = true)
    {
    }
    public function registerFatalHandler($level = null, $reservedMemorySize = 20)
    {
    }
    protected function defaultErrorLevelMap()
    {
    }
    /**
     * @private
     */
    public function handleException($e)
    {
    }
    /**
     * @private
     */
    public function handleError($code, $message, $file = '', $line = 0, $context = array())
    {
    }
    /**
     * @private
     */
    public function handleFatalError()
    {
    }
}
