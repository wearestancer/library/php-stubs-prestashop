<?php

/**
 * Class for converting between different unit-lengths as specified by
 * CSS.
 */
class HTMLPurifier_UnitConverter
{
    const ENGLISH = 1;
    const METRIC = 2;
    const DIGITAL = 3;
    /**
     * Units information array. Units are grouped into measuring systems
     * (English, Metric), and are assigned an integer representing
     * the conversion factor between that unit and the smallest unit in
     * the system. Numeric indexes are actually magical constants that
     * encode conversion data from one system to the next, with a O(n^2)
     * constraint on memory (this is generally not a problem, since
     * the number of measuring systems is small.)
     */
    protected static $units = array(self::ENGLISH => array(
        'px' => 3,
        // This is as per CSS 2.1 and Firefox. Your mileage may vary
        'pt' => 4,
        'pc' => 48,
        'in' => 288,
        self::METRIC => array('pt', '0.352777778', 'mm'),
    ), self::METRIC => array('mm' => 1, 'cm' => 10, self::ENGLISH => array('mm', '2.83464567', 'pt')));
    /**
     * Minimum bcmath precision for output.
     * @type int
     */
    protected $outputPrecision;
    /**
     * Bcmath precision for internal calculations.
     * @type int
     */
    protected $internalPrecision;
    public function __construct($output_precision = 4, $internal_precision = 10, $force_no_bcmath = \false)
    {
    }
    /**
     * Converts a length object of one unit into another unit.
     * @param HTMLPurifier_Length $length
     *      Instance of HTMLPurifier_Length to convert. You must validate()
     *      it before passing it here!
     * @param string $to_unit
     *      Unit to convert to.
     * @return HTMLPurifier_Length|bool
     * @note
     *      About precision: This conversion function pays very special
     *      attention to the incoming precision of values and attempts
     *      to maintain a number of significant figure. Results are
     *      fairly accurate up to nine digits. Some caveats:
     *          - If a number is zero-padded as a result of this significant
     *            figure tracking, the zeroes will be eliminated.
     *          - If a number contains less than four sigfigs ($outputPrecision)
     *            and this causes some decimals to be excluded, those
     *            decimals will be added on.
     */
    public function convert($length, $to_unit)
    {
    }
    /**
     * Returns the number of significant figures in a string number.
     * @param string $n Decimal number
     * @return int number of sigfigs
     */
    public function getSigFigs($n)
    {
    }
}
