<?php

// does not support network paths
class HTMLPurifier_URIFilter_MakeAbsolute extends \HTMLPurifier_URIFilter
{
    /**
     * @type string
     */
    public $name = 'MakeAbsolute';
    /**
     * @type
     */
    protected $base;
    /**
     * @type array
     */
    protected $basePathStack = array();
    /**
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function prepare($config)
    {
    }
    /**
     * @param HTMLPurifier_URI $uri
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool
     */
    public function filter(&$uri, $config, $context)
    {
    }
}
