<?php

class HTMLPurifier_URIFilter_Munge extends \HTMLPurifier_URIFilter
{
    /**
     * @type string
     */
    public $name = 'Munge';
    /**
     * @type bool
     */
    public $post = \true;
    /**
     * @type array
     */
    protected $replace = array();
    /**
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function prepare($config)
    {
    }
    /**
     * @param HTMLPurifier_URI $uri
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool
     */
    public function filter(&$uri, $config, $context)
    {
    }
    /**
     * @param HTMLPurifier_URI $uri
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     */
    protected function makeReplace($uri, $config, $context)
    {
    }
}
