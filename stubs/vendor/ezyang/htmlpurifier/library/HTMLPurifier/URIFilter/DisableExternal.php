<?php

class HTMLPurifier_URIFilter_DisableExternal extends \HTMLPurifier_URIFilter
{
    /**
     * @type string
     */
    public $name = 'DisableExternal';
    /**
     * @type array
     */
    protected $ourHostParts = \false;
    /**
     * @param HTMLPurifier_Config $config
     * @return void
     */
    public function prepare($config)
    {
    }
    /**
     * @param HTMLPurifier_URI $uri Reference
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool
     */
    public function filter(&$uri, $config, $context)
    {
    }
}
