<?php

/**
 * Validates the HTML attribute ID.
 * @warning Even though this is the id processor, it
 *          will ignore the directive Attr:IDBlacklist, since it will only
 *          go according to the ID accumulator. Since the accumulator is
 *          automatically generated, it will have already absorbed the
 *          blacklist. If you're hacking around, make sure you use load()!
 */
class HTMLPurifier_AttrDef_HTML_ID extends \HTMLPurifier_AttrDef
{
    // selector is NOT a valid thing to use for IDREFs, because IDREFs
    // *must* target IDs that exist, whereas selector #ids do not.
    /**
     * Determines whether or not we're validating an ID in a CSS
     * selector context.
     * @type bool
     */
    protected $selector;
    /**
     * @param bool $selector
     */
    public function __construct($selector = \false)
    {
    }
    /**
     * @param string $id
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool|string
     */
    public function validate($id, $config, $context)
    {
    }
}
