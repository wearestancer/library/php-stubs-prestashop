<?php

/**
 * Validates an integer representation of pixels according to the HTML spec.
 */
class HTMLPurifier_AttrDef_HTML_Pixels extends \HTMLPurifier_AttrDef
{
    /**
     * @type int
     */
    protected $max;
    /**
     * @param int $max
     */
    public function __construct($max = \null)
    {
    }
    /**
     * @param string $string
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool|string
     */
    public function validate($string, $config, $context)
    {
    }
    /**
     * @param string $string
     * @return HTMLPurifier_AttrDef
     */
    public function make($string)
    {
    }
}
