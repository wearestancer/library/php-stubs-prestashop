<?php

/**
 * Validates ftp (File Transfer Protocol) URIs as defined by generic RFC 1738.
 */
class HTMLPurifier_URIScheme_ftp extends \HTMLPurifier_URIScheme
{
    /**
     * @type int
     */
    public $default_port = 21;
    /**
     * @type bool
     */
    public $browsable = \true;
    // usually
    /**
     * @type bool
     */
    public $hierarchical = \true;
    /**
     * @param HTMLPurifier_URI $uri
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool
     */
    public function doValidate(&$uri, $config, $context)
    {
    }
}
