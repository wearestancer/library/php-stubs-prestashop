<?php

/**
 * Injector that removes spans with no attributes
 */
class HTMLPurifier_Injector_RemoveSpansWithoutAttributes extends \HTMLPurifier_Injector
{
    /**
     * @type string
     */
    public $name = 'RemoveSpansWithoutAttributes';
    /**
     * @type array
     */
    public $needed = array('span');
    public function prepare($config, $context)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleElement(&$token)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleEnd(&$token)
    {
    }
}
