<?php

/**
 * Injector that converts configuration directive syntax %Namespace.Directive
 * to links
 */
class HTMLPurifier_Injector_PurifierLinkify extends \HTMLPurifier_Injector
{
    /**
     * @type string
     */
    public $name = 'PurifierLinkify';
    /**
     * @type string
     */
    public $docURL;
    /**
     * @type array
     */
    public $needed = array('a' => array('href'));
    /**
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return string
     */
    public function prepare($config, $context)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleText(&$token)
    {
    }
}
