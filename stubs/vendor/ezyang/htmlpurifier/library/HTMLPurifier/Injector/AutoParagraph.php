<?php

/**
 * Injector that auto paragraphs text in the root node based on
 * double-spacing.
 * @todo Ensure all states are unit tested, including variations as well.
 * @todo Make a graph of the flow control for this Injector.
 */
class HTMLPurifier_Injector_AutoParagraph extends \HTMLPurifier_Injector
{
    /**
     * @type string
     */
    public $name = 'AutoParagraph';
    /**
     * @type array
     */
    public $needed = array('p');
    /**
     * @param HTMLPurifier_Token_Text $token
     */
    public function handleText(&$token)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleElement(&$token)
    {
    }
}
