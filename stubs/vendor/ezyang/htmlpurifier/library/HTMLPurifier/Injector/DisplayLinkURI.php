<?php

/**
 * Injector that displays the URL of an anchor instead of linking to it, in addition to showing the text of the link.
 */
class HTMLPurifier_Injector_DisplayLinkURI extends \HTMLPurifier_Injector
{
    /**
     * @type string
     */
    public $name = 'DisplayLinkURI';
    /**
     * @type array
     */
    public $needed = array('a');
    /**
     * @param $token
     */
    public function handleElement(&$token)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleEnd(&$token)
    {
    }
}
