<?php

class HTMLPurifier_Injector_RemoveEmpty extends \HTMLPurifier_Injector
{
    /**
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return void
     */
    public function prepare($config, $context)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleElement(&$token)
    {
    }
}
