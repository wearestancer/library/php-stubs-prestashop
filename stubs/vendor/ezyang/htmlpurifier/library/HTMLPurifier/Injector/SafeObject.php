<?php

/**
 * Adds important param elements to inside of object in order to make
 * things safe.
 */
class HTMLPurifier_Injector_SafeObject extends \HTMLPurifier_Injector
{
    /**
     * @type string
     */
    public $name = 'SafeObject';
    /**
     * @type array
     */
    public $needed = array('object', 'param');
    /**
     * @type array
     */
    protected $objectStack = array();
    /**
     * @type array
     */
    protected $paramStack = array();
    /**
     * Keep this synchronized with AttrTransform/SafeParam.php.
     * @type array
     */
    protected $addParam = array('allowScriptAccess' => 'never', 'allowNetworking' => 'internal');
    /**
     * These are all lower-case keys.
     * @type array
     */
    protected $allowedParam = array('wmode' => \true, 'movie' => \true, 'flashvars' => \true, 'src' => \true, 'allowfullscreen' => \true);
    /**
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return void
     */
    public function prepare($config, $context)
    {
    }
    /**
     * @param HTMLPurifier_Token $token
     */
    public function handleElement(&$token)
    {
    }
    public function handleEnd(&$token)
    {
    }
}
