<?php

/**
 * This filter extracts <style> blocks from input HTML, cleans them up
 * using CSSTidy, and then places them in $purifier->context->get('StyleBlocks')
 * so they can be used elsewhere in the document.
 *
 * @note
 *      See tests/HTMLPurifier/Filter/ExtractStyleBlocksTest.php for
 *      sample usage.
 *
 * @note
 *      This filter can also be used on stylesheets not included in the
 *      document--something purists would probably prefer. Just directly
 *      call HTMLPurifier_Filter_ExtractStyleBlocks->cleanCSS()
 */
class HTMLPurifier_Filter_ExtractStyleBlocks extends \HTMLPurifier_Filter
{
    /**
     * @type string
     */
    public $name = 'ExtractStyleBlocks';
    public function __construct()
    {
    }
    /**
     * Save the contents of CSS blocks to style matches
     * @param array $matches preg_replace style $matches array
     */
    protected function styleCallback($matches)
    {
    }
    /**
     * Removes inline <style> tags from HTML, saves them for later use
     * @param string $html
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return string
     * @todo Extend to indicate non-text/css style blocks
     */
    public function preFilter($html, $config, $context)
    {
    }
    /**
     * Takes CSS (the stuff found in <style>) and cleans it.
     * @warning Requires CSSTidy <http://csstidy.sourceforge.net/>
     * @param string $css CSS styling to clean
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @throws HTMLPurifier_Exception
     * @return string Cleaned CSS
     */
    public function cleanCSS($css, $config, $context)
    {
    }
}
// why is this a top level function? Because PHP 5.2.0 doesn't seem to
// understand how to interpret this filter if it's a static method.
// It's all really silly, but if we go this route it might be reasonable
// to coalesce all of these methods into one.
function htmlpurifier_filter_extractstyleblocks_muteerrorhandler()
{
}
