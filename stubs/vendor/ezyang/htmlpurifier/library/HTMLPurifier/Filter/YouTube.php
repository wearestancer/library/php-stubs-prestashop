<?php

class HTMLPurifier_Filter_YouTube extends \HTMLPurifier_Filter
{
    /**
     * @type string
     */
    public $name = 'YouTube';
    /**
     * @param string $html
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return string
     */
    public function preFilter($html, $config, $context)
    {
    }
    /**
     * @param string $html
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return string
     */
    public function postFilter($html, $config, $context)
    {
    }
    /**
     * @param $url
     * @return string
     */
    protected function armorUrl($url)
    {
    }
    /**
     * @param array $matches
     * @return string
     */
    protected function postFilterCallback($matches)
    {
    }
}
