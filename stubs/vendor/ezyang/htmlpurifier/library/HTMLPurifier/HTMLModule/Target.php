<?php

/**
 * XHTML 1.1 Target Module, defines target attribute in link elements.
 */
class HTMLPurifier_HTMLModule_Target extends \HTMLPurifier_HTMLModule
{
    /**
     * @type string
     */
    public $name = 'Target';
    /**
     * @param HTMLPurifier_Config $config
     */
    public function setup($config)
    {
    }
}
