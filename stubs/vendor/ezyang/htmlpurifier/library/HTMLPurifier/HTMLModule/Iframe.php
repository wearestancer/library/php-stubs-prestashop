<?php

/**
 * XHTML 1.1 Iframe Module provides inline frames.
 *
 * @note This module is not considered safe unless an Iframe
 * whitelisting mechanism is specified.  Currently, the only
 * such mechanism is %URL.SafeIframeRegexp
 */
class HTMLPurifier_HTMLModule_Iframe extends \HTMLPurifier_HTMLModule
{
    /**
     * @type string
     */
    public $name = 'Iframe';
    /**
     * @type bool
     */
    public $safe = \false;
    /**
     * @param HTMLPurifier_Config $config
     */
    public function setup($config)
    {
    }
}
