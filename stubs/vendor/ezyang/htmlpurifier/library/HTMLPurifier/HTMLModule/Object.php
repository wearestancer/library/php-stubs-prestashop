<?php

/**
 * XHTML 1.1 Object Module, defines elements for generic object inclusion
 * @warning Users will commonly use <embed> to cater to legacy browsers: this
 *      module does not allow this sort of behavior
 */
class HTMLPurifier_HTMLModule_Object extends \HTMLPurifier_HTMLModule
{
    /**
     * @type string
     */
    public $name = 'Object';
    /**
     * @type bool
     */
    public $safe = \false;
    /**
     * @param HTMLPurifier_Config $config
     */
    public function setup($config)
    {
    }
}
