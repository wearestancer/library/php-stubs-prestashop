<?php

/**
 * XHTML 1.1 List Module, defines list-oriented elements. Core Module.
 */
class HTMLPurifier_HTMLModule_List extends \HTMLPurifier_HTMLModule
{
    /**
     * @type string
     */
    public $name = 'List';
    // According to the abstract schema, the List content set is a fully formed
    // one or more expr, but it invariably occurs in an optional declaration
    // so we're not going to do that subtlety. It might cause trouble
    // if a user defines "List" and expects that multiple lists are
    // allowed to be specified, but then again, that's not very intuitive.
    // Furthermore, the actual XML Schema may disagree. Regardless,
    // we don't have support for such nested expressions without using
    // the incredibly inefficient and draconic Custom ChildDef.
    /**
     * @type array
     */
    public $content_sets = array('Flow' => 'List');
    /**
     * @param HTMLPurifier_Config $config
     */
    public function setup($config)
    {
    }
}
