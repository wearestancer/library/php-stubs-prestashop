<?php

class HTMLPurifier_HTMLModule_Tidy_Proprietary extends \HTMLPurifier_HTMLModule_Tidy
{
    /**
     * @type string
     */
    public $name = 'Tidy_Proprietary';
    /**
     * @type string
     */
    public $defaultLevel = 'light';
    /**
     * @return array
     */
    public function makeFixes()
    {
    }
}
