<?php

class HTMLPurifier_HTMLModule_Tidy_Transitional extends \HTMLPurifier_HTMLModule_Tidy_XHTMLAndHTML4
{
    /**
     * @type string
     */
    public $name = 'Tidy_Transitional';
    /**
     * @type string
     */
    public $defaultLevel = 'heavy';
}
