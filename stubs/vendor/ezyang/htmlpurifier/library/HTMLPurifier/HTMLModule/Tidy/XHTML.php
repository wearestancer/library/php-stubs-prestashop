<?php

class HTMLPurifier_HTMLModule_Tidy_XHTML extends \HTMLPurifier_HTMLModule_Tidy
{
    /**
     * @type string
     */
    public $name = 'Tidy_XHTML';
    /**
     * @type string
     */
    public $defaultLevel = 'medium';
    /**
     * @return array
     */
    public function makeFixes()
    {
    }
}
