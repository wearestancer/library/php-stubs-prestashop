<?php

class HTMLPurifier_HTMLModule_Tidy_Strict extends \HTMLPurifier_HTMLModule_Tidy_XHTMLAndHTML4
{
    /**
     * @type string
     */
    public $name = 'Tidy_Strict';
    /**
     * @type string
     */
    public $defaultLevel = 'light';
    /**
     * @return array
     */
    public function makeFixes()
    {
    }
    /**
     * @type bool
     */
    public $defines_child_def = \true;
    /**
     * @param HTMLPurifier_ElementDef $def
     * @return HTMLPurifier_ChildDef_StrictBlockquote
     */
    public function getChildDef($def)
    {
    }
}
