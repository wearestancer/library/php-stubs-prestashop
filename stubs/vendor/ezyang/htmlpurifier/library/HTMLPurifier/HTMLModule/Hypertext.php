<?php

/**
 * XHTML 1.1 Hypertext Module, defines hypertext links. Core Module.
 */
class HTMLPurifier_HTMLModule_Hypertext extends \HTMLPurifier_HTMLModule
{
    /**
     * @type string
     */
    public $name = 'Hypertext';
    /**
     * @param HTMLPurifier_Config $config
     */
    public function setup($config)
    {
    }
}
