<?php

class HTMLPurifier_Printer_HTMLDefinition extends \HTMLPurifier_Printer
{
    /**
     * @type HTMLPurifier_HTMLDefinition, for easy access
     */
    protected $def;
    /**
     * @param HTMLPurifier_Config $config
     * @return string
     */
    public function render($config)
    {
    }
    /**
     * Renders the Doctype table
     * @return string
     */
    protected function renderDoctype()
    {
    }
    /**
     * Renders environment table, which is miscellaneous info
     * @return string
     */
    protected function renderEnvironment()
    {
    }
    /**
     * Renders the Content Sets table
     * @return string
     */
    protected function renderContentSets()
    {
    }
    /**
     * Renders the Elements ($info) table
     * @return string
     */
    protected function renderInfo()
    {
    }
    /**
     * Renders a row describing the allowed children of an element
     * @param HTMLPurifier_ChildDef $def HTMLPurifier_ChildDef of pertinent element
     * @return string
     */
    protected function renderChildren($def)
    {
    }
    /**
     * Listifies a tag lookup table.
     * @param array $array Tag lookup array in form of array('tagname' => true)
     * @return string
     */
    protected function listifyTagLookup($array)
    {
    }
    /**
     * Listifies a list of objects by retrieving class names and internal state
     * @param array $array List of objects
     * @return string
     * @todo Also add information about internal state
     */
    protected function listifyObjectList($array)
    {
    }
    /**
     * Listifies a hash of attributes to AttrDef classes
     * @param array $array Array hash in form of array('attrname' => HTMLPurifier_AttrDef)
     * @return string
     */
    protected function listifyAttr($array)
    {
    }
    /**
     * Creates a heavy header row
     * @param string $text
     * @param int $num
     * @return string
     */
    protected function heavyHeader($text, $num = 1)
    {
    }
}
