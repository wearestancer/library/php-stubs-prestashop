<?php

/**
 * Concrete comment node class.
 */
class HTMLPurifier_Node_Comment extends \HTMLPurifier_Node
{
    /**
     * Character data within comment.
     * @type string
     */
    public $data;
    /**
     * @type bool
     */
    public $is_whitespace = \true;
    /**
     * Transparent constructor.
     *
     * @param string $data String comment data.
     * @param int $line
     * @param int $col
     */
    public function __construct($data, $line = \null, $col = \null)
    {
    }
    public function toTokenPair()
    {
    }
}
