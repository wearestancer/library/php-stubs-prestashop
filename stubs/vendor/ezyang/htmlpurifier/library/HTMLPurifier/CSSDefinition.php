<?php

/**
 * Defines allowed CSS attributes and what their values are.
 * @see HTMLPurifier_HTMLDefinition
 */
class HTMLPurifier_CSSDefinition extends \HTMLPurifier_Definition
{
    public $type = 'CSS';
    /**
     * Assoc array of attribute name to definition object.
     * @type HTMLPurifier_AttrDef[]
     */
    public $info = array();
    /**
     * Constructs the info array.  The meat of this class.
     * @param HTMLPurifier_Config $config
     */
    protected function doSetup($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     */
    protected function doSetupProprietary($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     */
    protected function doSetupTricky($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     */
    protected function doSetupTrusted($config)
    {
    }
    /**
     * Performs extra config-based processing. Based off of
     * HTMLPurifier_HTMLDefinition.
     * @param HTMLPurifier_Config $config
     * @todo Refactor duplicate elements into common class (probably using
     *       composition, not inheritance).
     */
    protected function setupConfigStuff($config)
    {
    }
}
