<?php

/**
 * Property list iterator. Do not instantiate this class directly.
 */
class HTMLPurifier_PropertyListIterator extends \FilterIterator
{
    /**
     * @type int
     */
    protected $l;
    /**
     * @type string
     */
    protected $filter;
    /**
     * @param Iterator $iterator Array of data to iterate over
     * @param string $filter Optional prefix to only allow values of
     */
    public function __construct(\Iterator $iterator, $filter = \null)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function accept()
    {
    }
}
