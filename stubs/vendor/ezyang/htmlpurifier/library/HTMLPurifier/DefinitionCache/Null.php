<?php

/**
 * Null cache object to use when no caching is on.
 */
class HTMLPurifier_DefinitionCache_Null extends \HTMLPurifier_DefinitionCache
{
    /**
     * @param HTMLPurifier_Definition $def
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function add($def, $config)
    {
    }
    /**
     * @param HTMLPurifier_Definition $def
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function set($def, $config)
    {
    }
    /**
     * @param HTMLPurifier_Definition $def
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function replace($def, $config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function remove($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function get($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function flush($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return bool
     */
    public function cleanup($config)
    {
    }
}
