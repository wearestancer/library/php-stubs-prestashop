<?php

class HTMLPurifier_DefinitionCache_Decorator extends \HTMLPurifier_DefinitionCache
{
    /**
     * Cache object we are decorating
     * @type HTMLPurifier_DefinitionCache
     */
    public $cache;
    /**
     * The name of the decorator
     * @var string
     */
    public $name;
    public function __construct()
    {
    }
    /**
     * Lazy decorator function
     * @param HTMLPurifier_DefinitionCache $cache Reference to cache object to decorate
     * @return HTMLPurifier_DefinitionCache_Decorator
     */
    public function decorate(&$cache)
    {
    }
    /**
     * Cross-compatible clone substitute
     * @return HTMLPurifier_DefinitionCache_Decorator
     */
    public function copy()
    {
    }
    /**
     * @param HTMLPurifier_Definition $def
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function add($def, $config)
    {
    }
    /**
     * @param HTMLPurifier_Definition $def
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function set($def, $config)
    {
    }
    /**
     * @param HTMLPurifier_Definition $def
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function replace($def, $config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function get($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function remove($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function flush($config)
    {
    }
    /**
     * @param HTMLPurifier_Config $config
     * @return mixed
     */
    public function cleanup($config)
    {
    }
}
