<?php

/**
 * Class for handling width/height length attribute transformations to CSS
 */
class HTMLPurifier_AttrTransform_Length extends \HTMLPurifier_AttrTransform
{
    /**
     * @type string
     */
    protected $name;
    /**
     * @type string
     */
    protected $cssName;
    public function __construct($name, $css_name = \null)
    {
    }
    /**
     * @param array $attr
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return array
     */
    public function transform($attr, $config, $context)
    {
    }
}
