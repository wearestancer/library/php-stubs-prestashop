<?php

/**
 * Pre-transform that changes deprecated hspace and vspace attributes to CSS
 */
class HTMLPurifier_AttrTransform_ImgSpace extends \HTMLPurifier_AttrTransform
{
    /**
     * @type string
     */
    protected $attr;
    /**
     * @type array
     */
    protected $css = array('hspace' => array('left', 'right'), 'vspace' => array('top', 'bottom'));
    /**
     * @param string $attr
     */
    public function __construct($attr)
    {
    }
    /**
     * @param array $attr
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return array
     */
    public function transform($attr, $config, $context)
    {
    }
}
