<?php

/**
 * Represents a measurable length, with a string numeric magnitude
 * and a unit. This object is immutable.
 */
class HTMLPurifier_Length
{
    /**
     * String numeric magnitude.
     * @type string
     */
    protected $n;
    /**
     * String unit. False is permitted if $n = 0.
     * @type string|bool
     */
    protected $unit;
    /**
     * Whether or not this length is valid. Null if not calculated yet.
     * @type bool
     */
    protected $isValid;
    /**
     * Array Lookup array of units recognized by CSS 3
     * @type array
     */
    protected static $allowedUnits = array('em' => \true, 'ex' => \true, 'px' => \true, 'in' => \true, 'cm' => \true, 'mm' => \true, 'pt' => \true, 'pc' => \true, 'ch' => \true, 'rem' => \true, 'vw' => \true, 'vh' => \true, 'vmin' => \true, 'vmax' => \true);
    /**
     * @param string $n Magnitude
     * @param bool|string $u Unit
     */
    public function __construct($n = '0', $u = \false)
    {
    }
    /**
     * @param string $s Unit string, like '2em' or '3.4in'
     * @return HTMLPurifier_Length
     * @warning Does not perform validation.
     */
    public static function make($s)
    {
    }
    /**
     * Validates the number and unit.
     * @return bool
     */
    protected function validate()
    {
    }
    /**
     * Returns string representation of number.
     * @return string
     */
    public function toString()
    {
    }
    /**
     * Retrieves string numeric magnitude.
     * @return string
     */
    public function getN()
    {
    }
    /**
     * Retrieves string unit.
     * @return string
     */
    public function getUnit()
    {
    }
    /**
     * Returns true if this length unit is valid.
     * @return bool
     */
    public function isValid()
    {
    }
    /**
     * Compares two lengths, and returns 1 if greater, -1 if less and 0 if equal.
     * @param HTMLPurifier_Length $l
     * @return int
     * @warning If both values are too large or small, this calculation will
     *          not work properly
     */
    public function compareTo($l)
    {
    }
}
