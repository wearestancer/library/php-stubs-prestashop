<?php

/**
 * Represents a directive ID in the interchange format.
 */
class HTMLPurifier_ConfigSchema_Interchange_Id
{
    /**
     * @type string
     */
    public $key;
    /**
     * @param string $key
     */
    public function __construct($key)
    {
    }
    /**
     * @return string
     * @warning This is NOT magic, to ensure that people don't abuse SPL and
     *          cause problems for PHP 5.0 support.
     */
    public function toString()
    {
    }
    /**
     * @return string
     */
    public function getRootNamespace()
    {
    }
    /**
     * @return string
     */
    public function getDirective()
    {
    }
    /**
     * @param string $id
     * @return HTMLPurifier_ConfigSchema_Interchange_Id
     */
    public static function make($id)
    {
    }
}
