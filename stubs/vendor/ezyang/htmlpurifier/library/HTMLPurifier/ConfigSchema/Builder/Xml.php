<?php

/**
 * Converts HTMLPurifier_ConfigSchema_Interchange to an XML format,
 * which can be further processed to generate documentation.
 */
class HTMLPurifier_ConfigSchema_Builder_Xml extends \XMLWriter
{
    /**
     * @type HTMLPurifier_ConfigSchema_Interchange
     */
    protected $interchange;
    /**
     * @param string $html
     */
    protected function writeHTMLDiv($html)
    {
    }
    /**
     * @param mixed $var
     * @return string
     */
    protected function export($var)
    {
    }
    /**
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange
     */
    public function build($interchange)
    {
    }
    /**
     * @param HTMLPurifier_ConfigSchema_Interchange_Directive $directive
     */
    public function buildDirective($directive)
    {
    }
}
