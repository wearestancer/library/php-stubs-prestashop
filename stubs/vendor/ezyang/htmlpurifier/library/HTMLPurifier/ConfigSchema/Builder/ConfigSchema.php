<?php

/**
 * Converts HTMLPurifier_ConfigSchema_Interchange to our runtime
 * representation used to perform checks on user configuration.
 */
class HTMLPurifier_ConfigSchema_Builder_ConfigSchema
{
    /**
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange
     * @return HTMLPurifier_ConfigSchema
     */
    public function build($interchange)
    {
    }
}
