<?php

/**
 * Performs validations on HTMLPurifier_ConfigSchema_Interchange
 *
 * @note If you see '// handled by InterchangeBuilder', that means a
 *       design decision in that class would prevent this validation from
 *       ever being necessary. We have them anyway, however, for
 *       redundancy.
 */
class HTMLPurifier_ConfigSchema_Validator
{
    /**
     * @type HTMLPurifier_ConfigSchema_Interchange
     */
    protected $interchange;
    /**
     * @type array
     */
    protected $aliases;
    /**
     * Context-stack to provide easy to read error messages.
     * @type array
     */
    protected $context = array();
    /**
     * to test default's type.
     * @type HTMLPurifier_VarParser
     */
    protected $parser;
    public function __construct()
    {
    }
    /**
     * Validates a fully-formed interchange object.
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange
     * @return bool
     */
    public function validate($interchange)
    {
    }
    /**
     * Validates a HTMLPurifier_ConfigSchema_Interchange_Id object.
     * @param HTMLPurifier_ConfigSchema_Interchange_Id $id
     */
    public function validateId($id)
    {
    }
    /**
     * Validates a HTMLPurifier_ConfigSchema_Interchange_Directive object.
     * @param HTMLPurifier_ConfigSchema_Interchange_Directive $d
     */
    public function validateDirective($d)
    {
    }
    /**
     * Extra validation if $allowed member variable of
     * HTMLPurifier_ConfigSchema_Interchange_Directive is defined.
     * @param HTMLPurifier_ConfigSchema_Interchange_Directive $d
     */
    public function validateDirectiveAllowed($d)
    {
    }
    /**
     * Extra validation if $valueAliases member variable of
     * HTMLPurifier_ConfigSchema_Interchange_Directive is defined.
     * @param HTMLPurifier_ConfigSchema_Interchange_Directive $d
     */
    public function validateDirectiveValueAliases($d)
    {
    }
    /**
     * Extra validation if $aliases member variable of
     * HTMLPurifier_ConfigSchema_Interchange_Directive is defined.
     * @param HTMLPurifier_ConfigSchema_Interchange_Directive $d
     */
    public function validateDirectiveAliases($d)
    {
    }
    // protected helper functions
    /**
     * Convenience function for generating HTMLPurifier_ConfigSchema_ValidatorAtom
     * for validating simple member variables of objects.
     * @param $obj
     * @param $member
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    protected function with($obj, $member)
    {
    }
    /**
     * Emits an error, providing helpful context.
     * @throws HTMLPurifier_ConfigSchema_Exception
     */
    protected function error($target, $msg)
    {
    }
    /**
     * Returns a formatted context string.
     * @return string
     */
    protected function getFormattedContext()
    {
    }
}
