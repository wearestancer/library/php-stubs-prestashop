<?php

class HTMLPurifier_ConfigSchema_InterchangeBuilder
{
    /**
     * Used for processing DEFAULT, nothing else.
     * @type HTMLPurifier_VarParser
     */
    protected $varParser;
    /**
     * @param HTMLPurifier_VarParser $varParser
     */
    public function __construct($varParser = \null)
    {
    }
    /**
     * @param string $dir
     * @return HTMLPurifier_ConfigSchema_Interchange
     */
    public static function buildFromDirectory($dir = \null)
    {
    }
    /**
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange
     * @param string $dir
     * @return HTMLPurifier_ConfigSchema_Interchange
     */
    public function buildDir($interchange, $dir = \null)
    {
    }
    /**
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange
     * @param string $file
     */
    public function buildFile($interchange, $file)
    {
    }
    /**
     * Builds an interchange object based on a hash.
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange HTMLPurifier_ConfigSchema_Interchange object to build
     * @param HTMLPurifier_StringHash $hash source data
     * @throws HTMLPurifier_ConfigSchema_Exception
     */
    public function build($interchange, $hash)
    {
    }
    /**
     * @param HTMLPurifier_ConfigSchema_Interchange $interchange
     * @param HTMLPurifier_StringHash $hash
     * @throws HTMLPurifier_ConfigSchema_Exception
     */
    public function buildDirective($interchange, $hash)
    {
    }
    /**
     * Evaluates an array PHP code string without array() wrapper
     * @param string $contents
     */
    protected function evalArray($contents)
    {
    }
    /**
     * Converts an array list into a lookup array.
     * @param array $array
     * @return array
     */
    protected function lookup($array)
    {
    }
    /**
     * Convenience function that creates an HTMLPurifier_ConfigSchema_Interchange_Id
     * object based on a string Id.
     * @param string $id
     * @return HTMLPurifier_ConfigSchema_Interchange_Id
     */
    protected function id($id)
    {
    }
    /**
     * Triggers errors for any unused keys passed in the hash; such keys
     * may indicate typos, missing values, etc.
     * @param HTMLPurifier_StringHash $hash Hash to check.
     */
    protected function _findUnused($hash)
    {
    }
}
