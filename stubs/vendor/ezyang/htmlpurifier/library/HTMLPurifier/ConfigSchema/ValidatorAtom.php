<?php

/**
 * Fluent interface for validating the contents of member variables.
 * This should be immutable. See HTMLPurifier_ConfigSchema_Validator for
 * use-cases. We name this an 'atom' because it's ONLY for validations that
 * are independent and usually scalar.
 */
class HTMLPurifier_ConfigSchema_ValidatorAtom
{
    /**
     * @type string
     */
    protected $context;
    /**
     * @type object
     */
    protected $obj;
    /**
     * @type string
     */
    protected $member;
    /**
     * @type mixed
     */
    protected $contents;
    public function __construct($context, $obj, $member)
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertIsString()
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertIsBool()
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertIsArray()
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertNotNull()
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertAlnum()
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertNotEmpty()
    {
    }
    /**
     * @return HTMLPurifier_ConfigSchema_ValidatorAtom
     */
    public function assertIsLookup()
    {
    }
    /**
     * @param string $msg
     * @throws HTMLPurifier_ConfigSchema_Exception
     */
    protected function error($msg)
    {
    }
}
