<?php

/**
 * Abstract base token class that all others inherit from.
 */
abstract class HTMLPurifier_Token
{
    /**
     * Line number node was on in source document. Null if unknown.
     * @type int
     */
    public $line;
    /**
     * Column of line node was on in source document. Null if unknown.
     * @type int
     */
    public $col;
    /**
     * Lookup array of processing that this token is exempt from.
     * Currently, valid values are "ValidateAttributes" and
     * "MakeWellFormed_TagClosedError"
     * @type array
     */
    public $armor = array();
    /**
     * Used during MakeWellFormed.  See Note [Injector skips]
     * @type
     */
    public $skip;
    /**
     * @type
     */
    public $rewind;
    /**
     * @type
     */
    public $carryover;
    /**
     * @param string $n
     * @return null|string
     */
    public function __get($n)
    {
    }
    /**
     * Sets the position of the token in the source document.
     * @param int $l
     * @param int $c
     */
    public function position($l = \null, $c = \null)
    {
    }
    /**
     * Convenience function for DirectLex settings line/col position.
     * @param int $l
     * @param int $c
     */
    public function rawPosition($l, $c)
    {
    }
    /**
     * Converts a token into its corresponding node.
     */
    public abstract function toNode();
}
