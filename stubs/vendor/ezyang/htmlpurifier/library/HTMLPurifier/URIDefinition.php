<?php

class HTMLPurifier_URIDefinition extends \HTMLPurifier_Definition
{
    public $type = 'URI';
    protected $filters = array();
    protected $postFilters = array();
    protected $registeredFilters = array();
    /**
     * HTMLPurifier_URI object of the base specified at %URI.Base
     */
    public $base;
    /**
     * String host to consider "home" base, derived off of $base
     */
    public $host;
    /**
     * Name of default scheme based on %URI.DefaultScheme and %URI.Base
     */
    public $defaultScheme;
    public function __construct()
    {
    }
    public function registerFilter($filter)
    {
    }
    public function addFilter($filter, $config)
    {
    }
    protected function doSetup($config)
    {
    }
    protected function setupFilters($config)
    {
    }
    protected function setupMemberVariables($config)
    {
    }
    public function getDefaultScheme($config, $context)
    {
    }
    public function filter(&$uri, $config, $context)
    {
    }
    public function postFilter(&$uri, $config, $context)
    {
    }
}
