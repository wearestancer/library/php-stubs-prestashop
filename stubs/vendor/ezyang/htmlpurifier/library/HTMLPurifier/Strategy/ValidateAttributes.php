<?php

/**
 * Validate all attributes in the tokens.
 */
class HTMLPurifier_Strategy_ValidateAttributes extends \HTMLPurifier_Strategy
{
    /**
     * @param HTMLPurifier_Token[] $tokens
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return HTMLPurifier_Token[]
     */
    public function execute($tokens, $config, $context)
    {
    }
}
