<?php

// OUT OF DATE, NEEDS UPDATING!
// USE XMLWRITER!
class HTMLPurifier_Printer
{
    /**
     * For HTML generation convenience funcs.
     * @type HTMLPurifier_Generator
     */
    protected $generator;
    /**
     * For easy access.
     * @type HTMLPurifier_Config
     */
    protected $config;
    /**
     * Initialize $generator.
     */
    public function __construct()
    {
    }
    /**
     * Give generator necessary configuration if possible
     * @param HTMLPurifier_Config $config
     */
    public function prepareGenerator($config)
    {
    }
    /**
     * Main function that renders object or aspect of that object
     * @note Parameters vary depending on printer
     */
    // function render() {}
    /**
     * Returns a start tag
     * @param string $tag Tag name
     * @param array $attr Attribute array
     * @return string
     */
    protected function start($tag, $attr = array())
    {
    }
    /**
     * Returns an end tag
     * @param string $tag Tag name
     * @return string
     */
    protected function end($tag)
    {
    }
    /**
     * Prints a complete element with content inside
     * @param string $tag Tag name
     * @param string $contents Element contents
     * @param array $attr Tag attributes
     * @param bool $escape whether or not to escape contents
     * @return string
     */
    protected function element($tag, $contents, $attr = array(), $escape = \true)
    {
    }
    /**
     * @param string $tag
     * @param array $attr
     * @return string
     */
    protected function elementEmpty($tag, $attr = array())
    {
    }
    /**
     * @param string $text
     * @return string
     */
    protected function text($text)
    {
    }
    /**
     * Prints a simple key/value row in a table.
     * @param string $name Key
     * @param mixed $value Value
     * @return string
     */
    protected function row($name, $value)
    {
    }
    /**
     * Escapes a string for HTML output.
     * @param string $string String to escape
     * @return string
     */
    protected function escape($string)
    {
    }
    /**
     * Takes a list of strings and turns them into a single list
     * @param string[] $array List of strings
     * @param bool $polite Bool whether or not to add an end before the last
     * @return string
     */
    protected function listify($array, $polite = \false)
    {
    }
    /**
     * Retrieves the class of an object without prefixes, as well as metadata
     * @param object $obj Object to determine class of
     * @param string $sec_prefix Further prefix to remove
     * @return string
     */
    protected function getClass($obj, $sec_prefix = '')
    {
    }
}
