<?php

namespace Egulias\EmailValidator;

class EmailParser extends \Egulias\EmailValidator\Parser
{
    const EMAIL_MAX_LENGTH = 254;
    /**
     * @var string
     */
    protected $domainPart = '';
    /**
     * @var string
     */
    protected $localPart = '';
    public function parse(string $str) : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function preLeftParsing() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function parseLeftFromAt() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function parseRightFromAt() : \Egulias\EmailValidator\Result\Result
    {
    }
    public function getDomainPart() : string
    {
    }
    public function getLocalPart() : string
    {
    }
}
