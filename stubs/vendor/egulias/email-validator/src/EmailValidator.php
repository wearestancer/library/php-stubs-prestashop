<?php

namespace Egulias\EmailValidator;

class EmailValidator
{
    public function __construct()
    {
    }
    /**
     * @param string          $email
     * @param EmailValidation $emailValidation
     * @return bool
     */
    public function isValid(string $email, \Egulias\EmailValidator\Validation\EmailValidation $emailValidation)
    {
    }
    /**
     * @return boolean
     */
    public function hasWarnings()
    {
    }
    /**
     * @return array
     */
    public function getWarnings()
    {
    }
    /**
     * @return InvalidEmail|null
     */
    public function getError()
    {
    }
}
