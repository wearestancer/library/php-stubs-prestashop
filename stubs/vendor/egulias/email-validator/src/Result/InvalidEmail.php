<?php

namespace Egulias\EmailValidator\Result;

class InvalidEmail implements \Egulias\EmailValidator\Result\Result
{
    /**
     * @var Reason
     */
    protected $reason;
    public function __construct(\Egulias\EmailValidator\Result\Reason\Reason $reason, string $token)
    {
    }
    public function isValid() : bool
    {
    }
    public function isInvalid() : bool
    {
    }
    public function description() : string
    {
    }
    public function code() : int
    {
    }
    public function reason() : \Egulias\EmailValidator\Result\Reason\Reason
    {
    }
}
