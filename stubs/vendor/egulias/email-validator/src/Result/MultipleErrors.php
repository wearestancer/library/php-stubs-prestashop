<?php

namespace Egulias\EmailValidator\Result;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class MultipleErrors extends \Egulias\EmailValidator\Result\InvalidEmail
{
    public function __construct()
    {
    }
    public function addReason(\Egulias\EmailValidator\Result\Reason\Reason $reason) : void
    {
    }
    /**
     * @return Reason[]
     */
    public function getReasons() : array
    {
    }
    public function reason() : \Egulias\EmailValidator\Result\Reason\Reason
    {
    }
    public function description() : string
    {
    }
    public function code() : int
    {
    }
}
