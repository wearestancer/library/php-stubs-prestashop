<?php

namespace Egulias\EmailValidator\Result\Reason;

class EmptyReason implements \Egulias\EmailValidator\Result\Reason\Reason
{
    public function code() : int
    {
    }
    public function description() : string
    {
    }
}
