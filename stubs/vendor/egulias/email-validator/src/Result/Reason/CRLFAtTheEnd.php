<?php

namespace Egulias\EmailValidator\Result\Reason;

class CRLFAtTheEnd implements \Egulias\EmailValidator\Result\Reason\Reason
{
    const CODE = 149;
    const REASON = "CRLF at the end";
    public function code() : int
    {
    }
    public function description() : string
    {
    }
}
