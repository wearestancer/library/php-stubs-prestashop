<?php

namespace Egulias\EmailValidator\Result\Reason;

class ExceptionFound implements \Egulias\EmailValidator\Result\Reason\Reason
{
    public function __construct(\Exception $exception)
    {
    }
    public function code() : int
    {
    }
    public function description() : string
    {
    }
}
