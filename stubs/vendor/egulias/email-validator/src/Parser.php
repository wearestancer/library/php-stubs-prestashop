<?php

namespace Egulias\EmailValidator;

abstract class Parser
{
    /**
     * @var Warning\Warning[]
     */
    protected $warnings = [];
    /**
     * @var EmailLexer
     */
    protected $lexer;
    /**
     * id-left "@" id-right
     */
    protected abstract function parseRightFromAt() : \Egulias\EmailValidator\Result\Result;
    protected abstract function parseLeftFromAt() : \Egulias\EmailValidator\Result\Result;
    protected abstract function preLeftParsing() : \Egulias\EmailValidator\Result\Result;
    public function __construct(\Egulias\EmailValidator\EmailLexer $lexer)
    {
    }
    public function parse(string $str) : \Egulias\EmailValidator\Result\Result
    {
    }
    /**
     * @return Warning\Warning[]
     */
    public function getWarnings() : array
    {
    }
    protected function hasAtToken() : bool
    {
    }
}
