<?php

namespace Egulias\EmailValidator;

class EmailLexer extends \Doctrine\Common\Lexer\AbstractLexer
{
    //ASCII values
    const S_EMPTY = null;
    const C_NUL = 0;
    const S_HTAB = 9;
    const S_LF = 10;
    const S_CR = 13;
    const S_SP = 32;
    const EXCLAMATION = 33;
    const S_DQUOTE = 34;
    const NUMBER_SIGN = 35;
    const DOLLAR = 36;
    const PERCENTAGE = 37;
    const AMPERSAND = 38;
    const S_SQUOTE = 39;
    const S_OPENPARENTHESIS = 40;
    const S_CLOSEPARENTHESIS = 41;
    const ASTERISK = 42;
    const S_PLUS = 43;
    const S_COMMA = 44;
    const S_HYPHEN = 45;
    const S_DOT = 46;
    const S_SLASH = 47;
    const S_COLON = 58;
    const S_SEMICOLON = 59;
    const S_LOWERTHAN = 60;
    const S_EQUAL = 61;
    const S_GREATERTHAN = 62;
    const QUESTIONMARK = 63;
    const S_AT = 64;
    const S_OPENBRACKET = 91;
    const S_BACKSLASH = 92;
    const S_CLOSEBRACKET = 93;
    const CARET = 94;
    const S_UNDERSCORE = 95;
    const S_BACKTICK = 96;
    const S_OPENCURLYBRACES = 123;
    const S_PIPE = 124;
    const S_CLOSECURLYBRACES = 125;
    const S_TILDE = 126;
    const C_DEL = 127;
    const INVERT_QUESTIONMARK = 168;
    const INVERT_EXCLAMATION = 173;
    const GENERIC = 300;
    const S_IPV6TAG = 301;
    const INVALID = 302;
    const CRLF = 1310;
    const S_DOUBLECOLON = 5858;
    const ASCII_INVALID_FROM = 127;
    const ASCII_INVALID_TO = 199;
    /**
     * US-ASCII visible characters not valid for atext (@link http://tools.ietf.org/html/rfc5322#section-3.2.3)
     *
     * @var array
     */
    protected $charValue = array('{' => self::S_OPENCURLYBRACES, '}' => self::S_CLOSECURLYBRACES, '(' => self::S_OPENPARENTHESIS, ')' => self::S_CLOSEPARENTHESIS, '<' => self::S_LOWERTHAN, '>' => self::S_GREATERTHAN, '[' => self::S_OPENBRACKET, ']' => self::S_CLOSEBRACKET, ':' => self::S_COLON, ';' => self::S_SEMICOLON, '@' => self::S_AT, '\\' => self::S_BACKSLASH, '/' => self::S_SLASH, ',' => self::S_COMMA, '.' => self::S_DOT, "'" => self::S_SQUOTE, "`" => self::S_BACKTICK, '"' => self::S_DQUOTE, '-' => self::S_HYPHEN, '::' => self::S_DOUBLECOLON, ' ' => self::S_SP, "\t" => self::S_HTAB, "\r" => self::S_CR, "\n" => self::S_LF, "\r\n" => self::CRLF, 'IPv6' => self::S_IPV6TAG, '' => self::S_EMPTY, '\\0' => self::C_NUL, '*' => self::ASTERISK, '!' => self::EXCLAMATION, '&' => self::AMPERSAND, '^' => self::CARET, '$' => self::DOLLAR, '%' => self::PERCENTAGE, '~' => self::S_TILDE, '|' => self::S_PIPE, '_' => self::S_UNDERSCORE, '=' => self::S_EQUAL, '+' => self::S_PLUS, '¿' => self::INVERT_QUESTIONMARK, '?' => self::QUESTIONMARK, '#' => self::NUMBER_SIGN, '¡' => self::INVERT_EXCLAMATION);
    /**
     * @var bool
     */
    protected $hasInvalidTokens = false;
    /**
     * @var array
     *
     * @psalm-var array{value:string, type:null|int, position:int}|array<empty, empty>
     */
    protected $previous = [];
    /**
     * The last matched/seen token.
     *
     * @var array
     *
     * @psalm-suppress NonInvariantDocblockPropertyType
     * @psalm-var array{value:string, type:null|int, position:int}
     * @psalm-suppress NonInvariantDocblockPropertyType
     */
    public $token;
    /**
     * The next token in the input.
     *
     * @var array|null
     */
    public $lookahead;
    public function __construct()
    {
    }
    /**
     * @return void
     */
    public function reset()
    {
    }
    /**
     * @return bool
     */
    public function hasInvalidTokens()
    {
    }
    /**
     * @param int $type
     * @throws \UnexpectedValueException
     * @return boolean
     *
     * @psalm-suppress InvalidScalarArgument
     */
    public function find($type)
    {
    }
    /**
     * getPrevious
     *
     * @return array
     */
    public function getPrevious()
    {
    }
    /**
     * moveNext
     *
     * @return boolean
     */
    public function moveNext()
    {
    }
    /**
     * Lexical catchable patterns.
     *
     * @return string[]
     */
    protected function getCatchablePatterns()
    {
    }
    /**
     * Lexical non-catchable patterns.
     *
     * @return string[]
     */
    protected function getNonCatchablePatterns()
    {
    }
    /**
     * Retrieve token type. Also processes the token value if necessary.
     *
     * @param string $value
     * @throws \InvalidArgumentException
     * @return integer
     */
    protected function getType(&$value)
    {
    }
    protected function isInvalidChar(string $value) : bool
    {
    }
    protected function isValid(string $value) : bool
    {
    }
    /**
     * @param string $value
     * @return bool
     */
    protected function isNullType($value)
    {
    }
    protected function isUTF8Invalid(string $value) : bool
    {
    }
    /**
     * @return string
     */
    protected function getModifiers()
    {
    }
    public function getAccumulatedValues() : string
    {
    }
    public function startRecording() : void
    {
    }
    public function stopRecording() : void
    {
    }
    public function clearRecorded() : void
    {
    }
}
