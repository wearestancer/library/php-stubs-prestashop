<?php

namespace Egulias\EmailValidator\Parser;

class Comment extends \Egulias\EmailValidator\Parser\PartParser
{
    public function __construct(\Egulias\EmailValidator\EmailLexer $lexer, \Egulias\EmailValidator\Parser\CommentStrategy\CommentStrategy $commentStrategy)
    {
    }
    public function parse() : \Egulias\EmailValidator\Result\Result
    {
    }
}
