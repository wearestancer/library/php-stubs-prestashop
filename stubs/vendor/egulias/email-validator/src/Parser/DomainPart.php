<?php

namespace Egulias\EmailValidator\Parser;

class DomainPart extends \Egulias\EmailValidator\Parser\PartParser
{
    const DOMAIN_MAX_LENGTH = 253;
    const LABEL_MAX_LENGTH = 63;
    /**
     * @var string
     */
    protected $domainPart = '';
    /**
     * @var string
     */
    protected $label = '';
    public function parse() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function parseComments() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function doParseDomainPart() : \Egulias\EmailValidator\Result\Result
    {
    }
    /**
     * @return Result
     */
    protected function parseDomainLiteral() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function checkDomainPartExceptions(array $prev, bool $hasComments) : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function validateTokens(bool $hasComments) : \Egulias\EmailValidator\Result\Result
    {
    }
    public function domainPart() : string
    {
    }
}
