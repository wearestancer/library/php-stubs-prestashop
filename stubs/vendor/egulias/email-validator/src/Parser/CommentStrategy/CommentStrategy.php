<?php

namespace Egulias\EmailValidator\Parser\CommentStrategy;

interface CommentStrategy
{
    /**
     * Return "true" to continue, "false" to exit
     */
    public function exitCondition(\Egulias\EmailValidator\EmailLexer $lexer, int $openedParenthesis) : bool;
    public function endOfLoopValidations(\Egulias\EmailValidator\EmailLexer $lexer) : \Egulias\EmailValidator\Result\Result;
    public function getWarnings() : array;
}
