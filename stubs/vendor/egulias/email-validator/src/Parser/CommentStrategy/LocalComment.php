<?php

namespace Egulias\EmailValidator\Parser\CommentStrategy;

class LocalComment implements \Egulias\EmailValidator\Parser\CommentStrategy\CommentStrategy
{
    public function exitCondition(\Egulias\EmailValidator\EmailLexer $lexer, int $openedParenthesis) : bool
    {
    }
    public function endOfLoopValidations(\Egulias\EmailValidator\EmailLexer $lexer) : \Egulias\EmailValidator\Result\Result
    {
    }
    public function getWarnings() : array
    {
    }
}
