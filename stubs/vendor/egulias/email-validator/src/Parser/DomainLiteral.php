<?php

namespace Egulias\EmailValidator\Parser;

class DomainLiteral extends \Egulias\EmailValidator\Parser\PartParser
{
    public function parse() : \Egulias\EmailValidator\Result\Result
    {
    }
    /**
     * @param string $addressLiteral
     * @param int $maxGroups
     */
    public function checkIPV6Tag($addressLiteral, $maxGroups = 8) : void
    {
    }
    public function convertIPv4ToIPv6(string $addressLiteralIPv4) : string
    {
    }
    /**
     * @param string $addressLiteral
     *
     * @return bool
     */
    protected function checkIPV4Tag($addressLiteral) : bool
    {
    }
}
