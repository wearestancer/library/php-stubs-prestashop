<?php

namespace Egulias\EmailValidator\Parser;

abstract class PartParser
{
    /**
     * @var array
     */
    protected $warnings = [];
    /**
     * @var EmailLexer
     */
    protected $lexer;
    public function __construct(\Egulias\EmailValidator\EmailLexer $lexer)
    {
    }
    public abstract function parse() : \Egulias\EmailValidator\Result\Result;
    /**
     * @return \Egulias\EmailValidator\Warning\Warning[]
     */
    public function getWarnings()
    {
    }
    protected function parseFWS() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function checkConsecutiveDots() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function escaped() : bool
    {
    }
}
