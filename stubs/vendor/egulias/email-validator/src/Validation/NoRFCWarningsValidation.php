<?php

namespace Egulias\EmailValidator\Validation;

class NoRFCWarningsValidation extends \Egulias\EmailValidator\Validation\RFCValidation
{
    /**
     * {@inheritdoc}
     */
    public function isValid(string $email, \Egulias\EmailValidator\EmailLexer $emailLexer) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getError() : ?\Egulias\EmailValidator\Result\InvalidEmail
    {
    }
}
