<?php

namespace Egulias\EmailValidator\Validation;

class DNSCheckValidation implements \Egulias\EmailValidator\Validation\EmailValidation
{
    /**
     * @var int
     */
    protected const DNS_RECORD_TYPES_TO_CHECK = DNS_MX + DNS_A + DNS_AAAA;
    public function __construct()
    {
    }
    public function isValid(string $email, \Egulias\EmailValidator\EmailLexer $emailLexer) : bool
    {
    }
    public function getError() : ?\Egulias\EmailValidator\Result\InvalidEmail
    {
    }
    public function getWarnings() : array
    {
    }
    /**
     * @param string $host
     *
     * @return bool
     */
    protected function checkDns($host)
    {
    }
}
