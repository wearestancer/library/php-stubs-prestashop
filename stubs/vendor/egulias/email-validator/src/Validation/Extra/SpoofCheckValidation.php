<?php

namespace Egulias\EmailValidator\Validation\Extra;

class SpoofCheckValidation implements \Egulias\EmailValidator\Validation\EmailValidation
{
    public function __construct()
    {
    }
    /**
     * @psalm-suppress InvalidArgument
     */
    public function isValid(string $email, \Egulias\EmailValidator\EmailLexer $emailLexer) : bool
    {
    }
    /**
     * @return InvalidEmail
     */
    public function getError() : ?\Egulias\EmailValidator\Result\InvalidEmail
    {
    }
    public function getWarnings() : array
    {
    }
}
