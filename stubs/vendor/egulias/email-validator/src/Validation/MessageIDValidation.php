<?php

namespace Egulias\EmailValidator\Validation;

class MessageIDValidation implements \Egulias\EmailValidator\Validation\EmailValidation
{
    public function isValid(string $email, \Egulias\EmailValidator\EmailLexer $emailLexer) : bool
    {
    }
    public function getWarnings() : array
    {
    }
    public function getError() : ?\Egulias\EmailValidator\Result\InvalidEmail
    {
    }
}
