<?php

namespace Egulias\EmailValidator\Validation;

class MultipleValidationWithAnd implements \Egulias\EmailValidator\Validation\EmailValidation
{
    /**
     * If one of validations fails, the remaining validations will be skept.
     * This means MultipleErrors will only contain a single error, the first found.
     */
    const STOP_ON_ERROR = 0;
    /**
     * All of validations will be invoked even if one of them got failure.
     * So MultipleErrors will contain all causes.
     */
    const ALLOW_ALL_ERRORS = 1;
    /**
     * @param EmailValidation[] $validations The validations.
     * @param int               $mode        The validation mode (one of the constants).
     */
    public function __construct(array $validations, $mode = self::ALLOW_ALL_ERRORS)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isValid(string $email, \Egulias\EmailValidator\EmailLexer $emailLexer) : bool
    {
    }
    /**
     * Returns the validation errors.
     */
    public function getError() : ?\Egulias\EmailValidator\Result\InvalidEmail
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getWarnings() : array
    {
    }
}
