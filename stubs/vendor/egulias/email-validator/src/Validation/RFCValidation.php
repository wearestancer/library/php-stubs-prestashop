<?php

namespace Egulias\EmailValidator\Validation;

class RFCValidation implements \Egulias\EmailValidator\Validation\EmailValidation
{
    public function isValid(string $email, \Egulias\EmailValidator\EmailLexer $emailLexer) : bool
    {
    }
    public function getError() : ?\Egulias\EmailValidator\Result\InvalidEmail
    {
    }
    public function getWarnings() : array
    {
    }
}
