<?php

namespace Egulias\EmailValidator;

class MessageIDParser extends \Egulias\EmailValidator\Parser
{
    const EMAILID_MAX_LENGTH = 254;
    /**
     * @var string
     */
    protected $idLeft = '';
    /**
     * @var string
     */
    protected $idRight = '';
    public function parse(string $str) : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function preLeftParsing() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function parseLeftFromAt() : \Egulias\EmailValidator\Result\Result
    {
    }
    protected function parseRightFromAt() : \Egulias\EmailValidator\Result\Result
    {
    }
    public function getLeftPart() : string
    {
    }
    public function getRightPart() : string
    {
    }
}
