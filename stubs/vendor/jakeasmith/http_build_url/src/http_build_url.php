<?php

\define('HTTP_URL_REPLACE', 1);
\define('HTTP_URL_JOIN_PATH', 2);
\define('HTTP_URL_JOIN_QUERY', 4);
\define('HTTP_URL_STRIP_USER', 8);
\define('HTTP_URL_STRIP_PASS', 16);
\define('HTTP_URL_STRIP_AUTH', 32);
\define('HTTP_URL_STRIP_PORT', 64);
\define('HTTP_URL_STRIP_PATH', 128);
\define('HTTP_URL_STRIP_QUERY', 256);
\define('HTTP_URL_STRIP_FRAGMENT', 512);
\define('HTTP_URL_STRIP_ALL', 1024);
/**
 * Build a URL.
 *
 * The parts of the second URL will be merged into the first according to
 * the flags argument.
 *
 * @param mixed $url     (part(s) of) an URL in form of a string or
 *                       associative array like parse_url() returns
 * @param mixed $parts   same as the first argument
 * @param int   $flags   a bitmask of binary or'ed HTTP_URL constants;
 *                       HTTP_URL_REPLACE is the default
 * @param array $new_url if set, it will be filled with the parts of the
 *                       composed url like parse_url() would return
 * @return string
 */
function http_build_url($url, $parts = array(), $flags = \HTTP_URL_REPLACE, &$new_url = array())
{
}
