<?php

namespace Pimple\Psr11;

/**
 * Pimple PSR-11 service locator.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
class ServiceLocator implements \Psr\Container\ContainerInterface
{
    /**
     * @param PimpleContainer $container The Container instance used to locate services
     * @param array           $ids       Array of service ids that can be located. String keys can be used to define aliases
     */
    public function __construct(\Pimple\Container $container, array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(string $id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has(string $id) : bool
    {
    }
}
