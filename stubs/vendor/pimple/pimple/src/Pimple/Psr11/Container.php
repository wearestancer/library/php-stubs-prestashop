<?php

namespace Pimple\Psr11;

/**
 * PSR-11 compliant wrapper.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
final class Container implements \Psr\Container\ContainerInterface
{
    public function __construct(\Pimple\Container $pimple)
    {
    }
    public function get(string $id)
    {
    }
    public function has(string $id) : bool
    {
    }
}
