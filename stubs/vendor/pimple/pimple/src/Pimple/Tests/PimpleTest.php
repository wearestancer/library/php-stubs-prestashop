<?php

namespace Pimple\Tests;

/**
 * @author Igor Wiedler <igor@wiedler.ch>
 */
class PimpleTest extends \PHPUnit\Framework\TestCase
{
    public function testWithString()
    {
    }
    public function testWithClosure()
    {
    }
    public function testServicesShouldBeDifferent()
    {
    }
    public function testShouldPassContainerAsParameter()
    {
    }
    public function testIsset()
    {
    }
    public function testConstructorInjection()
    {
    }
    public function testOffsetGetValidatesKeyIsPresent()
    {
    }
    /**
     * @group legacy
     */
    public function testLegacyOffsetGetValidatesKeyIsPresent()
    {
    }
    public function testOffsetGetHonorsNullValues()
    {
    }
    public function testUnset()
    {
    }
    /**
     * @dataProvider serviceDefinitionProvider
     */
    public function testShare($service)
    {
    }
    /**
     * @dataProvider serviceDefinitionProvider
     */
    public function testProtect($service)
    {
    }
    public function testGlobalFunctionNameAsParameterValue()
    {
    }
    public function testRaw()
    {
    }
    public function testRawHonorsNullValues()
    {
    }
    public function testFluentRegister()
    {
    }
    public function testRawValidatesKeyIsPresent()
    {
    }
    /**
     * @group legacy
     */
    public function testLegacyRawValidatesKeyIsPresent()
    {
    }
    /**
     * @dataProvider serviceDefinitionProvider
     */
    public function testExtend($service)
    {
    }
    public function testExtendDoesNotLeakWithFactories()
    {
    }
    public function testExtendValidatesKeyIsPresent()
    {
    }
    /**
     * @group legacy
     */
    public function testLegacyExtendValidatesKeyIsPresent()
    {
    }
    public function testKeys()
    {
    }
    /** @test */
    public function settingAnInvokableObjectShouldTreatItAsFactory()
    {
    }
    /** @test */
    public function settingNonInvokableObjectShouldTreatItAsParameter()
    {
    }
    /**
     * @dataProvider badServiceDefinitionProvider
     */
    public function testFactoryFailsForInvalidServiceDefinitions($service)
    {
    }
    /**
     * @group legacy
     * @dataProvider badServiceDefinitionProvider
     */
    public function testLegacyFactoryFailsForInvalidServiceDefinitions($service)
    {
    }
    /**
     * @dataProvider badServiceDefinitionProvider
     */
    public function testProtectFailsForInvalidServiceDefinitions($service)
    {
    }
    /**
     * @group legacy
     * @dataProvider badServiceDefinitionProvider
     */
    public function testLegacyProtectFailsForInvalidServiceDefinitions($service)
    {
    }
    /**
     * @dataProvider badServiceDefinitionProvider
     */
    public function testExtendFailsForKeysNotContainingServiceDefinitions($service)
    {
    }
    /**
     * @group legacy
     * @dataProvider badServiceDefinitionProvider
     */
    public function testLegacyExtendFailsForKeysNotContainingServiceDefinitions($service)
    {
    }
    /**
     * @group legacy
     * @expectedDeprecation How Pimple behaves when extending protected closures will be fixed in Pimple 4. Are you sure "foo" should be protected?
     */
    public function testExtendingProtectedClosureDeprecation()
    {
    }
    /**
     * @dataProvider badServiceDefinitionProvider
     */
    public function testExtendFailsForInvalidServiceDefinitions($service)
    {
    }
    /**
     * @group legacy
     * @dataProvider badServiceDefinitionProvider
     */
    public function testLegacyExtendFailsForInvalidServiceDefinitions($service)
    {
    }
    public function testExtendFailsIfFrozenServiceIsNonInvokable()
    {
    }
    public function testExtendFailsIfFrozenServiceIsInvokable()
    {
    }
    /**
     * Provider for invalid service definitions.
     */
    public function badServiceDefinitionProvider()
    {
    }
    /**
     * Provider for service definitions.
     */
    public function serviceDefinitionProvider()
    {
    }
    public function testDefiningNewServiceAfterFreeze()
    {
    }
    public function testOverridingServiceAfterFreeze()
    {
    }
    /**
     * @group legacy
     */
    public function testLegacyOverridingServiceAfterFreeze()
    {
    }
    public function testRemovingServiceAfterFreeze()
    {
    }
    public function testExtendingService()
    {
    }
    public function testExtendingServiceAfterOtherServiceFreeze()
    {
    }
}
