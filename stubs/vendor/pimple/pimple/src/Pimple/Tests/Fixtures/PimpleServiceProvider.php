<?php

namespace Pimple\Tests\Fixtures;

class PimpleServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     */
    public function register(\Pimple\Container $pimple)
    {
    }
}
