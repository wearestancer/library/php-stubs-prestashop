<?php

namespace Pimple\Tests\Psr11;

/**
 * ServiceLocator test case.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
class ServiceLocatorTest extends \PHPUnit\Framework\TestCase
{
    public function testCanAccessServices()
    {
    }
    public function testCanAccessAliasedServices()
    {
    }
    public function testCannotAccessAliasedServiceUsingRealIdentifier()
    {
    }
    public function testGetValidatesServiceCanBeLocated()
    {
    }
    public function testGetValidatesTargetServiceExists()
    {
    }
    public function testHasValidatesServiceCanBeLocated()
    {
    }
    public function testHasChecksIfTargetServiceExists()
    {
    }
}
