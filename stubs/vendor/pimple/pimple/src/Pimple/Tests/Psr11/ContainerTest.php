<?php

namespace Pimple\Tests\Psr11;

class ContainerTest extends \PHPUnit\Framework\TestCase
{
    public function testGetReturnsExistingService()
    {
    }
    public function testGetThrowsExceptionIfServiceIsNotFound()
    {
    }
    public function testHasReturnsTrueIfServiceExists()
    {
    }
    public function testHasReturnsFalseIfServiceDoesNotExist()
    {
    }
}
