<?php

namespace Pimple;

/**
 * Lazy service iterator.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
final class ServiceIterator implements \Iterator
{
    public function __construct(\Pimple\Container $container, array $ids)
    {
    }
    /**
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function rewind()
    {
    }
    /**
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
    }
    /**
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
    }
    /**
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function next()
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function valid()
    {
    }
}
