<?php

namespace Pimple\Exception;

/**
 * The identifier of a valid service or parameter was expected.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
class UnknownIdentifierException extends \InvalidArgumentException implements \Psr\Container\NotFoundExceptionInterface
{
    /**
     * @param string $id The unknown identifier
     */
    public function __construct($id)
    {
    }
}
