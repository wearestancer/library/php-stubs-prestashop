<?php

namespace Pimple\Exception;

/**
 * A closure or invokable object was expected.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
class ExpectedInvokableException extends \InvalidArgumentException implements \Psr\Container\ContainerExceptionInterface
{
}
