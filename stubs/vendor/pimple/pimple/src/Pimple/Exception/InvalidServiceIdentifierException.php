<?php

namespace Pimple\Exception;

/**
 * An attempt to perform an operation that requires a service identifier was made.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
class InvalidServiceIdentifierException extends \InvalidArgumentException implements \Psr\Container\NotFoundExceptionInterface
{
    /**
     * @param string $id The invalid identifier
     */
    public function __construct($id)
    {
    }
}
