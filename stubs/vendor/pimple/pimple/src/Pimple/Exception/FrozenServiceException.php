<?php

namespace Pimple\Exception;

/**
 * An attempt to modify a frozen service was made.
 *
 * @author Pascal Luna <skalpa@zetareticuli.org>
 */
class FrozenServiceException extends \RuntimeException implements \Psr\Container\ContainerExceptionInterface
{
    /**
     * @param string $id Identifier of the frozen service
     */
    public function __construct($id)
    {
    }
}
