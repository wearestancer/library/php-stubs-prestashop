<?php

namespace ZipStream;

class DeflateStream extends \ZipStream\Stream
{
    protected $filter;
    /**
     * @var Option\File
     */
    protected $options;
    /**
     * Rewind stream
     *
     * @return void
     */
    public function rewind() : void
    {
    }
    /**
     * Remove the deflate filter
     *
     * @return void
     */
    public function removeDeflateFilter() : void
    {
    }
    /**
     * Add a deflate filter
     *
     * @param Option\File $options
     * @return void
     */
    public function addDeflateFilter(\ZipStream\Option\File $options) : void
    {
    }
}
