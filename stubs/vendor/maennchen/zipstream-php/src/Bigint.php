<?php

namespace ZipStream;

class Bigint
{
    /**
     * Initialize the bytes array
     *
     * @param int $value
     */
    public function __construct(int $value = 0)
    {
    }
    /**
     * Fill the bytes field with int
     *
     * @param int $value
     * @param int $start
     * @param int $count
     * @return void
     */
    protected function fillBytes(int $value, int $start, int $count) : void
    {
    }
    /**
     * Get an instance
     *
     * @param int $value
     * @return Bigint
     */
    public static function init(int $value = 0) : self
    {
    }
    /**
     * Fill bytes from low to high
     *
     * @param int $low
     * @param int $high
     * @return Bigint
     */
    public static function fromLowHigh(int $low, int $high) : self
    {
    }
    /**
     * Get high 32
     *
     * @return int
     */
    public function getHigh32() : int
    {
    }
    /**
     * Get value from bytes array
     *
     * @param int $end
     * @param int $length
     * @return int
     */
    public function getValue(int $end = 0, int $length = 8) : int
    {
    }
    /**
     * Get low FF
     *
     * @param bool $force
     * @return float
     */
    public function getLowFF(bool $force = false) : float
    {
    }
    /**
     * Check if is over 32
     *
     * @param bool $force
     * @return bool
     */
    public function isOver32(bool $force = false) : bool
    {
    }
    /**
     * Get low 32
     *
     * @return int
     */
    public function getLow32() : int
    {
    }
    /**
     * Get hexadecimal
     *
     * @return string
     */
    public function getHex64() : string
    {
    }
    /**
     * Add
     *
     * @param Bigint $other
     * @return Bigint
     */
    public function add(\ZipStream\Bigint $other) : \ZipStream\Bigint
    {
    }
}
