<?php

namespace ZipStream;

/**
 * This class is only for inheriting
 */
abstract class Exception extends \Exception
{
}
