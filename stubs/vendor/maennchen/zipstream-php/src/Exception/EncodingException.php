<?php

namespace ZipStream\Exception;

/**
 * This Exception gets invoked if file or comment encoding is incorrect
 */
class EncodingException extends \ZipStream\Exception
{
}
