<?php

namespace ZipStream\Exception;

/**
 * This Exception gets invoked if a counter value exceeds storage size
 */
class OverflowException extends \ZipStream\Exception
{
    public function __construct()
    {
    }
}
