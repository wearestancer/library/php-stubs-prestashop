<?php

namespace ZipStream\Exception;

/**
 * This Exception gets invoked if options are incompatible
 */
class IncompatibleOptionsException extends \ZipStream\Exception
{
}
