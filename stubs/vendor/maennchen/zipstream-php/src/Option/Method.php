<?php

namespace ZipStream\Option;

/**
 * Methods enum
 *
 * @method static STORE(): Method
 * @method static DEFLATE(): Method
 * @psalm-immutable
 */
class Method extends \MyCLabs\Enum\Enum
{
    const STORE = 0x0;
    const DEFLATE = 0x8;
}
