<?php

namespace ZipStream\Option;

final class Archive
{
    const DEFAULT_DEFLATE_LEVEL = 6;
    /**
     * Options constructor.
     */
    public function __construct()
    {
    }
    public function getComment() : string
    {
    }
    public function setComment(string $comment) : void
    {
    }
    public function getLargeFileSize() : int
    {
    }
    public function setLargeFileSize(int $largeFileSize) : void
    {
    }
    public function getLargeFileMethod() : \ZipStream\Option\Method
    {
    }
    public function setLargeFileMethod(\ZipStream\Option\Method $largeFileMethod) : void
    {
    }
    public function isSendHttpHeaders() : bool
    {
    }
    public function setSendHttpHeaders(bool $sendHttpHeaders) : void
    {
    }
    public function getHttpHeaderCallback() : callable
    {
    }
    public function setHttpHeaderCallback(callable $httpHeaderCallback) : void
    {
    }
    public function isEnableZip64() : bool
    {
    }
    public function setEnableZip64(bool $enableZip64) : void
    {
    }
    public function isZeroHeader() : bool
    {
    }
    public function setZeroHeader(bool $zeroHeader) : void
    {
    }
    public function isFlushOutput() : bool
    {
    }
    public function setFlushOutput(bool $flushOutput) : void
    {
    }
    public function isStatFiles() : bool
    {
    }
    public function setStatFiles(bool $statFiles) : void
    {
    }
    public function getContentDisposition() : string
    {
    }
    public function setContentDisposition(string $contentDisposition) : void
    {
    }
    public function getContentType() : string
    {
    }
    public function setContentType(string $contentType) : void
    {
    }
    /**
     * @return resource
     */
    public function getOutputStream()
    {
    }
    /**
     * @param resource $outputStream
     */
    public function setOutputStream($outputStream) : void
    {
    }
    /**
     * @return int
     */
    public function getDeflateLevel() : int
    {
    }
    /**
     * @param int $deflateLevel
     */
    public function setDeflateLevel(int $deflateLevel) : void
    {
    }
}
