<?php

namespace ZipStream\Option;

final class File
{
    public function defaultTo(\ZipStream\Option\Archive $archiveOptions) : void
    {
    }
    /**
     * @return string
     */
    public function getComment() : string
    {
    }
    /**
     * @param string $comment
     */
    public function setComment(string $comment) : void
    {
    }
    /**
     * @return Method
     */
    public function getMethod() : \ZipStream\Option\Method
    {
    }
    /**
     * @param Method $method
     */
    public function setMethod(\ZipStream\Option\Method $method) : void
    {
    }
    /**
     * @return int
     */
    public function getDeflateLevel() : int
    {
    }
    /**
     * @param int $deflateLevel
     */
    public function setDeflateLevel(int $deflateLevel) : void
    {
    }
    /**
     * @return DateTime
     */
    public function getTime() : \DateTime
    {
    }
    /**
     * @param DateTime $time
     */
    public function setTime(\DateTime $time) : void
    {
    }
    /**
     * @return int
     */
    public function getSize() : int
    {
    }
    /**
     * @param int $size
     */
    public function setSize(int $size) : void
    {
    }
}
