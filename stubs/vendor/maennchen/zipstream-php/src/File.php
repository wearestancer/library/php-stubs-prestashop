<?php

namespace ZipStream;

class File
{
    const HASH_ALGORITHM = 'crc32b';
    const BIT_ZERO_HEADER = 0x8;
    const BIT_EFS_UTF8 = 0x800;
    const COMPUTE = 1;
    const SEND = 2;
    /**
     * @var string
     */
    public $name;
    /**
     * @var FileOptions
     */
    public $opt;
    /**
     * @var Bigint
     */
    public $len;
    /**
     * @var Bigint
     */
    public $zlen;
    /** @var  int */
    public $crc;
    /**
     * @var Bigint
     */
    public $hlen;
    /**
     * @var Bigint
     */
    public $ofs;
    /**
     * @var int
     */
    public $bits;
    /**
     * @var Version
     */
    public $version;
    /**
     * @var ZipStream
     */
    public $zip;
    public function __construct(\ZipStream\ZipStream $zip, string $name, ?\ZipStream\Option\File $opt = null)
    {
    }
    public function processPath(string $path) : void
    {
    }
    public function processData(string $data) : void
    {
    }
    /**
     * Create and send zip header for this file.
     *
     * @return void
     * @throws \ZipStream\Exception\EncodingException
     */
    public function addFileHeader() : void
    {
    }
    /**
     * Strip characters that are not legal in Windows filenames
     * to prevent compatibility issues
     *
     * @param string $filename Unprocessed filename
     * @return string
     */
    public static function filterFilename(string $filename) : string
    {
    }
    /**
     * Convert a UNIX timestamp to a DOS timestamp.
     *
     * @param int $when
     * @return int DOS Timestamp
     */
    protected static final function dosTime(int $when) : int
    {
    }
    protected function buildZip64ExtraBlock(bool $force = false) : string
    {
    }
    /**
     * Create and send data descriptor footer for this file.
     *
     * @return void
     */
    public function addFileFooter() : void
    {
    }
    public function processStream(\Psr\Http\Message\StreamInterface $stream) : void
    {
    }
    protected function processStreamWithZeroHeader(\Psr\Http\Message\StreamInterface $stream) : void
    {
    }
    protected function readStream(\Psr\Http\Message\StreamInterface $stream, ?int $options = null) : void
    {
    }
    protected function deflateInit() : void
    {
    }
    protected function deflateData(\Psr\Http\Message\StreamInterface $stream, string &$data, ?int $options = null) : void
    {
    }
    protected function deflateFinish(?int $options = null) : void
    {
    }
    protected function processStreamWithComputedHeader(\Psr\Http\Message\StreamInterface $stream) : void
    {
    }
    /**
     * Send CDR record for specified file.
     *
     * @return string
     */
    public function getCdrFile() : string
    {
    }
    /**
     * @return Bigint
     */
    public function getTotalLength() : \ZipStream\Bigint
    {
    }
}
