<?php

namespace Symfony\Polyfill\Iconv;

/**
 * iconv implementation in pure PHP, UTF-8 centric.
 *
 * Implemented:
 * - iconv              - Convert string to requested character encoding
 * - iconv_mime_decode  - Decodes a MIME header field
 * - iconv_mime_decode_headers - Decodes multiple MIME header fields at once
 * - iconv_get_encoding - Retrieve internal configuration variables of iconv extension
 * - iconv_set_encoding - Set current setting for character encoding conversion
 * - iconv_mime_encode  - Composes a MIME header field
 * - iconv_strlen       - Returns the character count of string
 * - iconv_strpos       - Finds position of first occurrence of a needle within a haystack
 * - iconv_strrpos      - Finds the last occurrence of a needle within a haystack
 * - iconv_substr       - Cut out part of a string
 *
 * Charsets available for conversion are defined by files
 * in the charset/ directory and by Iconv::$alias below.
 * You're welcome to send back any addition you make.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class Iconv
{
    public const ERROR_ILLEGAL_CHARACTER = 'iconv(): Detected an illegal character in input string';
    public const ERROR_WRONG_CHARSET = 'iconv(): Wrong charset, conversion from `%s\' to `%s\' is not allowed';
    public static $inputEncoding = 'utf-8';
    public static $outputEncoding = 'utf-8';
    public static $internalEncoding = 'utf-8';
    public static function iconv($inCharset, $outCharset, $str)
    {
    }
    public static function iconv_mime_decode_headers($str, $mode = 0, $charset = null)
    {
    }
    public static function iconv_mime_decode($str, $mode = 0, $charset = null)
    {
    }
    public static function iconv_get_encoding($type = 'all')
    {
    }
    public static function iconv_set_encoding($type, $charset)
    {
    }
    public static function iconv_mime_encode($fieldName, $fieldValue, $pref = null)
    {
    }
    public static function iconv_strlen($s, $encoding = null)
    {
    }
    public static function strlen1($s, $encoding = null)
    {
    }
    public static function strlen2($s, $encoding = null)
    {
    }
    public static function iconv_strpos($haystack, $needle, $offset = 0, $encoding = null)
    {
    }
    public static function iconv_strrpos($haystack, $needle, $encoding = null)
    {
    }
    public static function iconv_substr($s, $start, $length = 2147483647, $encoding = null)
    {
    }
}
