<?php

namespace Symfony\Bundle\MonologBundle;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class MonologBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * @internal
     */
    public static function includeStacktraces(\Monolog\Handler\HandlerInterface $handler)
    {
    }
}
