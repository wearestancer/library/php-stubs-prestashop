<?php

namespace Symfony\Bundle\MonologBundle\DependencyInjection\Compiler;

/**
 * Fixes loggers with no handlers (by registering a "null" one).
 *
 * Monolog 1.x adds a default handler logging on STDERR when a logger has
 * no registered handlers. This is NOT what what we want in Symfony, so in such
 * cases, we add a "null" handler to avoid the issue.
 *
 * Note that Monolog 2.x does not register a default handler anymore, so this pass can
 * be removed when MonologBundle minimum version of Monolog is bumped to 2.0.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @see https://github.com/Seldaek/monolog/commit/ad37b7b2d11f300cbace9f5e84f855d329519e28
 */
class FixEmptyLoggerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(\Symfony\Bundle\MonologBundle\DependencyInjection\Compiler\LoggerChannelPass $channelPass)
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
