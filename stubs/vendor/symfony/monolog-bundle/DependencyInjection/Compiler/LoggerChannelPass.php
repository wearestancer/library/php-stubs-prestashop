<?php

namespace Symfony\Bundle\MonologBundle\DependencyInjection\Compiler;

/**
 * Replaces the default logger by another one with its own channel for tagged services.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class LoggerChannelPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    protected $channels = ['app'];
    /**
     * {@inheritDoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * @return array
     */
    public function getChannels()
    {
    }
    /**
     * @param array $configuration
     *
     * @return array
     */
    protected function processChannels($configuration)
    {
    }
    /**
     * Create new logger from the monolog.logger_prototype
     *
     * @param string $channel
     * @param string $loggerId
     * @param ContainerBuilder $container
     */
    protected function createLogger($channel, $loggerId, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
