<?php

namespace Symfony\Bundle\MonologBundle\SwiftMailer;

/**
 * Helps create Swift_Message objects, lazily
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class MessageFactory
{
    public function __construct(\Swift_Mailer $mailer, $fromEmail, $toEmail, $subject, $contentType = null)
    {
    }
    /**
     * Creates a Swift_Message template that will be used to send the log message
     *
     * @param string $content formatted email body to be sent
     * @param array  $records Log records that formed the content
     * @return \Swift_Message
     */
    public function createMessage($content, array $records)
    {
    }
}
