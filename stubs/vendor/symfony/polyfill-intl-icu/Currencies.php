<?php

namespace Symfony\Polyfill\Intl\Icu;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class Currencies
{
    public static function getSymbol(string $currency) : ?string
    {
    }
    public static function getFractionDigits(string $currency) : int
    {
    }
    public static function getRoundingIncrement(string $currency) : int
    {
    }
}
