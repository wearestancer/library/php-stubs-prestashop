<?php

/**
 * Stub implementation for the Collator class of the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Collator extends \Symfony\Polyfill\Intl\Icu\Collator
{
}
