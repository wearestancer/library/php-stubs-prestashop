<?php

/**
 * Stub implementation for the Locale class of the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Locale extends \Symfony\Polyfill\Intl\Icu\Locale
{
}
