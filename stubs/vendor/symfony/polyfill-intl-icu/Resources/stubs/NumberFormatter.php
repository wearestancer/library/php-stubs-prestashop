<?php

/**
 * Stub implementation for the NumberFormatter class of the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see IntlNumberFormatter
 */
class NumberFormatter extends \Symfony\Polyfill\Intl\Icu\NumberFormatter
{
}
