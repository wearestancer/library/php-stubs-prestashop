<?php

/**
 * Stub implementation for the IntlDateFormatter class of the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IntlDateFormatter extends \Symfony\Polyfill\Intl\Icu\IntlDateFormatter
{
}
