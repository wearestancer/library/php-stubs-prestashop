<?php

namespace Symfony\Polyfill\Intl\Icu;

/**
 * Replacement for PHP's native {@link \IntlDateFormatter} class.
 *
 * The only methods currently supported in this class are:
 *
 *  - {@link __construct}
 *  - {@link create}
 *  - {@link format}
 *  - {@link getCalendar}
 *  - {@link getDateType}
 *  - {@link getErrorCode}
 *  - {@link getErrorMessage}
 *  - {@link getLocale}
 *  - {@link getPattern}
 *  - {@link getTimeType}
 *  - {@link getTimeZoneId}
 *  - {@link isLenient}
 *  - {@link parse}
 *  - {@link setLenient}
 *  - {@link setPattern}
 *  - {@link setTimeZoneId}
 *  - {@link setTimeZone}
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
abstract class IntlDateFormatter
{
    /**
     * The error code from the last operation.
     *
     * @var int
     */
    protected $errorCode = \Symfony\Polyfill\Intl\Icu\Icu::U_ZERO_ERROR;
    /**
     * The error message from the last operation.
     *
     * @var string
     */
    protected $errorMessage = 'U_ZERO_ERROR';
    /* date/time format types */
    public const NONE = -1;
    public const FULL = 0;
    public const LONG = 1;
    public const MEDIUM = 2;
    public const SHORT = 3;
    /* date format types */
    public const RELATIVE_FULL = 128;
    public const RELATIVE_LONG = 129;
    public const RELATIVE_MEDIUM = 130;
    public const RELATIVE_SHORT = 131;
    /* calendar formats */
    public const TRADITIONAL = 0;
    public const GREGORIAN = 1;
    /**
     * @param string|null                             $locale   The locale code. The only currently supported locale is "en" (or null using the default locale, i.e. "en")
     * @param \IntlTimeZone|\DateTimeZone|string|null $timezone Timezone identifier
     * @param \IntlCalendar|int|null                  $calendar Calendar to use for formatting or parsing. The only currently
     *                                                          supported value is IntlDateFormatter::GREGORIAN (or null using the default calendar, i.e. "GREGORIAN")
     *
     * @see https://php.net/intldateformatter.create
     * @see http://userguide.icu-project.org/formatparse/datetime
     *
     * @throws MethodArgumentValueNotImplementedException When $locale different than "en" or null is passed
     * @throws MethodArgumentValueNotImplementedException When $calendar different than GREGORIAN is passed
     */
    public function __construct(?string $locale, ?int $dateType, ?int $timeType, $timezone = null, $calendar = null, ?string $pattern = '')
    {
    }
    /**
     * Static constructor.
     *
     * @param string|null                             $locale   The locale code. The only currently supported locale is "en" (or null using the default locale, i.e. "en")
     * @param \IntlTimeZone|\DateTimeZone|string|null $timezone Timezone identifier
     * @param \IntlCalendar|int|null                  $calendar Calendar to use for formatting or parsing; default is Gregorian
     *                                                          One of the calendar constants
     *
     * @return static
     *
     * @see https://php.net/intldateformatter.create
     * @see http://userguide.icu-project.org/formatparse/datetime
     *
     * @throws MethodArgumentValueNotImplementedException When $locale different than "en" or null is passed
     * @throws MethodArgumentValueNotImplementedException When $calendar different than GREGORIAN is passed
     */
    public static function create(?string $locale, ?int $dateType, ?int $timeType, $timezone = null, int $calendar = null, ?string $pattern = '')
    {
    }
    /**
     * Format the date/time value (timestamp) as a string.
     *
     * @param int|string|\DateTimeInterface $datetime The timestamp to format
     *
     * @return string|bool The formatted value or false if formatting failed
     *
     * @see https://php.net/intldateformatter.format
     *
     * @throws MethodArgumentValueNotImplementedException If one of the formatting characters is not implemented
     */
    public function format($datetime)
    {
    }
    /**
     * Not supported. Formats an object.
     *
     * @return string The formatted value
     *
     * @see https://php.net/intldateformatter.formatobject
     *
     * @throws MethodNotImplementedException
     */
    public static function formatObject($datetime, $format = null, string $locale = null)
    {
    }
    /**
     * Returns the formatter's calendar.
     *
     * @return int The calendar being used by the formatter. Currently always returns
     *             IntlDateFormatter::GREGORIAN.
     *
     * @see https://php.net/intldateformatter.getcalendar
     */
    public function getCalendar()
    {
    }
    /**
     * Not supported. Returns the formatter's calendar object.
     *
     * @return object The calendar's object being used by the formatter
     *
     * @see https://php.net/intldateformatter.getcalendarobject
     *
     * @throws MethodNotImplementedException
     */
    public function getCalendarObject()
    {
    }
    /**
     * Returns the formatter's datetype.
     *
     * @return int The current value of the formatter
     *
     * @see https://php.net/intldateformatter.getdatetype
     */
    public function getDateType()
    {
    }
    /**
     * Returns formatter's last error code. Always returns the U_ZERO_ERROR class constant value.
     *
     * @return int The error code from last formatter call
     *
     * @see https://php.net/intldateformatter.geterrorcode
     */
    public function getErrorCode()
    {
    }
    /**
     * Returns formatter's last error message. Always returns the U_ZERO_ERROR_MESSAGE class constant value.
     *
     * @return string The error message from last formatter call
     *
     * @see https://php.net/intldateformatter.geterrormessage
     */
    public function getErrorMessage()
    {
    }
    /**
     * Returns the formatter's locale.
     *
     * @param int $type Not supported. The locale name type to return (Locale::VALID_LOCALE or Locale::ACTUAL_LOCALE)
     *
     * @return string The locale used to create the formatter. Currently always
     *                returns "en".
     *
     * @see https://php.net/intldateformatter.getlocale
     */
    public function getLocale(int $type = \Symfony\Polyfill\Intl\Icu\Locale::ACTUAL_LOCALE)
    {
    }
    /**
     * Returns the formatter's pattern.
     *
     * @return string The pattern string used by the formatter
     *
     * @see https://php.net/intldateformatter.getpattern
     */
    public function getPattern()
    {
    }
    /**
     * Returns the formatter's time type.
     *
     * @return int The time type used by the formatter
     *
     * @see https://php.net/intldateformatter.gettimetype
     */
    public function getTimeType()
    {
    }
    /**
     * Returns the formatter's timezone identifier.
     *
     * @return string The timezone identifier used by the formatter
     *
     * @see https://php.net/intldateformatter.gettimezoneid
     */
    public function getTimeZoneId()
    {
    }
    /**
     * Not supported. Returns the formatter's timezone.
     *
     * @return mixed The timezone used by the formatter
     *
     * @see https://php.net/intldateformatter.gettimezone
     *
     * @throws MethodNotImplementedException
     */
    public function getTimeZone()
    {
    }
    /**
     * Returns whether the formatter is lenient.
     *
     * @return bool Currently always returns false
     *
     * @see https://php.net/intldateformatter.islenient
     *
     * @throws MethodNotImplementedException
     */
    public function isLenient()
    {
    }
    /**
     * Not supported. Parse string to a field-based time value.
     *
     * @return string Localtime compatible array of integers: contains 24 hour clock value in tm_hour field
     *
     * @see https://php.net/intldateformatter.localtime
     *
     * @throws MethodNotImplementedException
     */
    public function localtime(string $string, &$offset = null)
    {
    }
    /**
     * Parse string to a timestamp value.
     *
     * @return int|false Parsed value as a timestamp
     *
     * @see https://php.net/intldateformatter.parse
     *
     * @throws MethodArgumentNotImplementedException When $offset different than null, behavior not implemented
     */
    public function parse(string $string, &$offset = null)
    {
    }
    /**
     * Not supported. Set the formatter's calendar.
     *
     * @param \IntlCalendar|int|null $calendar
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/intldateformatter.setcalendar
     *
     * @throws MethodNotImplementedException
     */
    public function setCalendar($calendar)
    {
    }
    /**
     * Set the leniency of the parser.
     *
     * Define if the parser is strict or lenient in interpreting inputs that do not match the pattern
     * exactly. Enabling lenient parsing allows the parser to accept otherwise flawed date or time
     * patterns, parsing as much as possible to obtain a value. Extra space, unrecognized tokens, or
     * invalid values ("February 30th") are not accepted.
     *
     * @param bool $lenient Sets whether the parser is lenient or not. Currently
     *                      only false (strict) is supported.
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/intldateformatter.setlenient
     *
     * @throws MethodArgumentValueNotImplementedException When $lenient is true
     */
    public function setLenient(bool $lenient)
    {
    }
    /**
     * Set the formatter's pattern.
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/intldateformatter.setpattern
     * @see http://userguide.icu-project.org/formatparse/datetime
     */
    public function setPattern(string $pattern)
    {
    }
    /**
     * Sets formatterʼs timezone.
     *
     * @param \IntlTimeZone|\DateTimeZone|string|null $timezone
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/intldateformatter.settimezone
     */
    public function setTimeZone($timezone)
    {
    }
    /**
     * Create and returns a DateTime object with the specified timestamp and with the
     * current time zone.
     *
     * @return \DateTime
     */
    protected function createDateTime($timestamp)
    {
    }
    /**
     * Returns a pattern string based in the datetype and timetype values.
     *
     * @return string
     */
    protected function getDefaultPattern()
    {
    }
}
