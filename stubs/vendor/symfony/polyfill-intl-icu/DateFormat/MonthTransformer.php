<?php

namespace Symfony\Polyfill\Intl\Icu\DateFormat;

/**
 * Parser and formatter for month format.
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class MonthTransformer extends \Symfony\Polyfill\Intl\Icu\DateFormat\Transformer
{
    protected static $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    /**
     * Short months names (first 3 letters).
     */
    protected static $shortMonths = [];
    /**
     * Flipped $months array, $name => $index.
     */
    protected static $flippedMonths = [];
    /**
     * Flipped $shortMonths array, $name => $index.
     */
    protected static $flippedShortMonths = [];
    public function __construct()
    {
    }
    public function format(\DateTime $dateTime, int $length) : string
    {
    }
    public function getReverseMatchingRegExp(int $length) : string
    {
    }
    public function extractDateOptions(string $matched, int $length) : array
    {
    }
}
