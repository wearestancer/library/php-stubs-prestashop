<?php

namespace Symfony\Polyfill\Intl\Icu\DateFormat;

/**
 * Parser and formatter for AM/PM markers format.
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class AmPmTransformer extends \Symfony\Polyfill\Intl\Icu\DateFormat\Transformer
{
    public function format(\DateTime $dateTime, int $length) : string
    {
    }
    public function getReverseMatchingRegExp(int $length) : string
    {
    }
    public function extractDateOptions(string $matched, int $length) : array
    {
    }
}
