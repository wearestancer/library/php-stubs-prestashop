<?php

namespace Symfony\Polyfill\Intl\Icu\DateFormat;

/**
 * Parser and formatter for 12 hour format (0-11).
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class Hour1200Transformer extends \Symfony\Polyfill\Intl\Icu\DateFormat\HourTransformer
{
    public function format(\DateTime $dateTime, int $length) : string
    {
    }
    public function normalizeHour(int $hour, string $marker = null) : int
    {
    }
    public function getReverseMatchingRegExp(int $length) : string
    {
    }
    public function extractDateOptions(string $matched, int $length) : array
    {
    }
}
