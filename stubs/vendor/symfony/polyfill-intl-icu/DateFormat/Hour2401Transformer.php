<?php

namespace Symfony\Polyfill\Intl\Icu\DateFormat;

/**
 * Parser and formatter for 24 hour format (1-24).
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class Hour2401Transformer extends \Symfony\Polyfill\Intl\Icu\DateFormat\HourTransformer
{
    public function format(\DateTime $dateTime, int $length) : string
    {
    }
    public function normalizeHour(int $hour, string $marker = null) : int
    {
    }
    public function getReverseMatchingRegExp(int $length) : string
    {
    }
    public function extractDateOptions(string $matched, int $length) : array
    {
    }
}
