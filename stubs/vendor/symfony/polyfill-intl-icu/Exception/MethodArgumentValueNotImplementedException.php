<?php

namespace Symfony\Polyfill\Intl\Icu\Exception;

/**
 * @author Eriksen Costa <eriksen.costa@infranology.com.br>
 */
class MethodArgumentValueNotImplementedException extends \Symfony\Polyfill\Intl\Icu\Exception\NotImplementedException
{
    /**
     * @param string $methodName        The method name that raised the exception
     * @param string $argName           The argument name
     * @param mixed  $argValue          The argument value that is not implemented
     * @param string $additionalMessage An optional additional message to append to the exception message
     */
    public function __construct(string $methodName, string $argName, $argValue, string $additionalMessage = '')
    {
    }
}
