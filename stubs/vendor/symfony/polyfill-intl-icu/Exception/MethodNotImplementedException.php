<?php

namespace Symfony\Polyfill\Intl\Icu\Exception;

/**
 * @author Eriksen Costa <eriksen.costa@infranology.com.br>
 */
class MethodNotImplementedException extends \Symfony\Polyfill\Intl\Icu\Exception\NotImplementedException
{
    /**
     * @param string $methodName The name of the method
     */
    public function __construct(string $methodName)
    {
    }
}
