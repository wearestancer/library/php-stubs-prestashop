<?php

namespace Symfony\Polyfill\Intl\Icu\Exception;

/**
 * RuntimeException for the Intl component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Polyfill\Intl\Icu\Exception\ExceptionInterface
{
}
