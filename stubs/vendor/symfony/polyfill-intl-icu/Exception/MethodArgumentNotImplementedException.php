<?php

namespace Symfony\Polyfill\Intl\Icu\Exception;

/**
 * @author Eriksen Costa <eriksen.costa@infranology.com.br>
 */
class MethodArgumentNotImplementedException extends \Symfony\Polyfill\Intl\Icu\Exception\NotImplementedException
{
    /**
     * @param string $methodName The method name that raised the exception
     * @param string $argName    The argument name that is not implemented
     */
    public function __construct(string $methodName, string $argName)
    {
    }
}
