<?php

namespace Symfony\Bundle\SwiftmailerBundle\DependencyInjection;

/**
 * Factory to create a \Swift_Transport object.
 *
 * @author Romain Gautier <mail@romain.sh>
 */
class SwiftmailerTransportFactory
{
    /**
     * @return \Swift_Transport
     *
     * @throws \InvalidArgumentException if the scheme is not a built-in Swiftmailer transport
     */
    public static function createTransport(array $options, \Symfony\Component\Routing\RequestContext $requestContext = null, \Swift_Events_EventDispatcher $eventDispatcher)
    {
    }
    /**
     * @return array options
     */
    public static function resolveOptions(array $options)
    {
    }
    /**
     * @throws \InvalidArgumentException if the encryption is not valid
     */
    public static function validateConfig($options)
    {
    }
}
