<?php

namespace Symfony\Bundle\SwiftmailerBundle\DependencyInjection;

class SmtpTransportConfigurator
{
    public function __construct($localDomain, \Symfony\Component\Routing\RequestContext $requestContext = null)
    {
    }
    /**
     * Sets the local domain based on the current request context.
     */
    public function configure(\Swift_Transport_AbstractSmtpTransport $transport)
    {
    }
}
