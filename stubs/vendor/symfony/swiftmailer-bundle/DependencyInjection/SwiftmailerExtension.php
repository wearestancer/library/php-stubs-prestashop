<?php

namespace Symfony\Bundle\SwiftmailerBundle\DependencyInjection;

/**
 * SwiftmailerExtension is an extension for the SwiftMailer library.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SwiftmailerExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension
{
    /**
     * Loads the Swift Mailer configuration.
     *
     * Usage example:
     *
     *      <swiftmailer:config transport="gmail">
     *        <swiftmailer:username>fabien</swift:username>
     *        <swiftmailer:password>xxxxx</swift:password>
     *        <swiftmailer:spool path="/path/to/spool/" />
     *      </swiftmailer:config>
     *
     * @param array            $configs   An array of configuration settings
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    protected function configureMailer($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $isDefaultMailer = false)
    {
    }
    protected function configureMailerTransport($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $transport, $isDefaultMailer = false)
    {
    }
    protected function configureMailerSpool($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $transport, $isDefaultMailer = false)
    {
    }
    protected function configureMailerSenderAddress($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $isDefaultMailer = false)
    {
    }
    protected function configureMailerAntiFlood($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $isDefaultMailer = false)
    {
    }
    protected function configureMailerDeliveryAddress($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $isDefaultMailer = false)
    {
    }
    protected function configureMailerLogging($name, array $mailer, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $isDefaultMailer = false)
    {
    }
    /**
     * Returns the base path for the XSD files.
     *
     * @return string The XSD base path
     */
    public function getXsdValidationBasePath()
    {
    }
    /**
     * Returns the namespace to be used for this extension (XML namespace).
     *
     * @return string The XML namespace
     */
    public function getNamespace()
    {
    }
    public function getConfiguration(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
