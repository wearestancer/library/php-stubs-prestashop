<?php

namespace Symfony\Bundle\SwiftmailerBundle\DependencyInjection;

/**
 * This class contains the configuration information for the bundle.
 *
 * This information is solely responsible for how the different configuration
 * sections are normalized, and merged.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class Configuration implements \Symfony\Component\Config\Definition\ConfigurationInterface
{
    /**
     * @param bool $debug The kernel.debug value
     */
    public function __construct($debug)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
    }
}
