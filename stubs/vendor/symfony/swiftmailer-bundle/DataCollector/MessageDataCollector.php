<?php

namespace Symfony\Bundle\SwiftmailerBundle\DataCollector;

/**
 * MessageDataCollector.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Clément JOBEILI <clement.jobeili@gmail.com>
 * @author Jérémy Romey <jeremy@free-agent.fr>
 */
final class MessageDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector
{
    /**
     * We don't inject the message logger and mailer here
     * to avoid the creation of these objects when no emails are sent.
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Throwable $exception = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * Returns the mailer names.
     *
     * @return array the mailer names
     */
    public function getMailers()
    {
    }
    /**
     * Returns the data collected of a mailer.
     *
     * @return array the data of the mailer
     */
    public function getMailerData($name)
    {
    }
    /**
     * Returns the message count of a mailer or the total.
     *
     * @return int the number of messages
     */
    public function getMessageCount($name = null)
    {
    }
    /**
     * Returns the messages of a mailer.
     *
     * @return \Swift_Message[] the messages
     */
    public function getMessages($name = 'default')
    {
    }
    /**
     * Returns if the mailer has spool.
     *
     * @return bool
     */
    public function isSpool($name)
    {
    }
    /**
     * Returns if the mailer is the default mailer.
     *
     * @return bool
     */
    public function isDefaultMailer($name)
    {
    }
    public function extractAttachments(\Swift_Message $message)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
