<?php

namespace Symfony\Bundle\SwiftmailerBundle;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SwiftmailerBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function registerCommands(\Symfony\Component\Console\Application $application)
    {
    }
}
