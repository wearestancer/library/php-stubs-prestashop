<?php

namespace Symfony\Bundle\SwiftmailerBundle\Command;

/**
 * Send Emails from the spool.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Clément JOBEILI <clement.jobeili@gmail.com>
 * @author Toni Uebernickel <tuebernickel@gmail.com>
 */
class SendEmailCommand extends \Symfony\Bundle\SwiftmailerBundle\Command\AbstractSwiftMailerCommand
{
    protected static $defaultName = 'swiftmailer:spool:send';
    protected function configure()
    {
    }
    /**
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
