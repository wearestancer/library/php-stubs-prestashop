<?php

namespace Symfony\Bundle\SwiftmailerBundle\Command;

/**
 * @internal
 */
abstract class AbstractSwiftMailerCommand extends \Symfony\Component\Console\Command\Command
{
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
    }
    /**
     * @return ContainerInterface
     *
     * @throws \LogicException
     */
    protected function getContainer()
    {
    }
}
