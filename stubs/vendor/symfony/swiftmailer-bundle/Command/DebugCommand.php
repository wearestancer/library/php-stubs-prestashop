<?php

namespace Symfony\Bundle\SwiftmailerBundle\Command;

/**
 * A console command for retrieving information about mailers.
 *
 * @author Jérémy Romey <jeremy@free-agent.fr>
 */
class DebugCommand extends \Symfony\Bundle\SwiftmailerBundle\Command\AbstractSwiftMailerCommand
{
    protected static $defaultName = 'debug:swiftmailer';
    /**
     * @see Command
     */
    protected function configure()
    {
    }
    /**
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    protected function outputMailers($routes = null)
    {
    }
    /**
     * @throws \InvalidArgumentException When route does not exist
     */
    protected function outputMailer($name)
    {
    }
}
