<?php

namespace Symfony\Bundle\SwiftmailerBundle\Command;

/**
 * A console command for creating and sending simple emails.
 *
 * @author Gusakov Nikita <dev@nkt.me>
 */
class NewEmailCommand extends \Symfony\Bundle\SwiftmailerBundle\Command\AbstractSwiftMailerCommand
{
    protected static $defaultName = 'swiftmailer:email:send';
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initialize(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * @return int
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function interact(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
    }
}
