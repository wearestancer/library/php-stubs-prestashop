<?php

namespace Symfony\Bundle\SwiftmailerBundle\EventListener;

/**
 * Sends emails for the memory spool.
 *
 * Emails are sent on the kernel.terminate event.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class EmailSenderListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onException()
    {
    }
    public function onTerminate()
    {
    }
    public static function getSubscribedEvents()
    {
    }
    public function reset()
    {
    }
}
