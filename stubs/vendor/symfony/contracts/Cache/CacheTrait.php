<?php

namespace Symfony\Contracts\Cache;

/**
 * An implementation of CacheInterface for PSR-6 CacheItemPoolInterface classes.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
trait CacheTrait
{
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(string $key) : bool
    {
    }
    private function doGet(\Psr\Cache\CacheItemPoolInterface $pool, string $key, callable $callback, ?float $beta, array &$metadata = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
