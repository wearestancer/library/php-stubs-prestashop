<?php

namespace Symfony\Contracts\Translation\Test;

/**
 * Test should cover all languages mentioned on http://translate.sourceforge.net/wiki/l10n/pluralforms
 * and Plural forms mentioned on http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms.
 *
 * See also https://developer.mozilla.org/en/Localization_and_Plurals which mentions 15 rules having a maximum of 6 forms.
 * The mozilla code is also interesting to check for.
 *
 * As mentioned by chx http://drupal.org/node/1273968 we can cover all by testing number from 0 to 199
 *
 * The goal to cover all languages is to far fetched so this test case is smaller.
 *
 * @author Clemens Tolboom clemens@build2be.nl
 */
class TranslatorTest extends \PHPUnit\Framework\TestCase
{
    protected function setUp() : void
    {
    }
    protected function tearDown() : void
    {
    }
    /**
     * @return TranslatorInterface
     */
    public function getTranslator()
    {
    }
    /**
     * @dataProvider getTransTests
     */
    public function testTrans($expected, $id, $parameters)
    {
    }
    /**
     * @dataProvider getTransChoiceTests
     */
    public function testTransChoiceWithExplicitLocale($expected, $id, $number)
    {
    }
    /**
     * @dataProvider getTransChoiceTests
     */
    public function testTransChoiceWithDefaultLocale($expected, $id, $number)
    {
    }
    /**
     * @dataProvider getTransChoiceTests
     */
    public function testTransChoiceWithEnUsPosix($expected, $id, $number)
    {
    }
    public function testGetSetLocale()
    {
    }
    /**
     * @requires extension intl
     */
    public function testGetLocaleReturnsDefaultLocaleIfNotSet()
    {
    }
    public function getTransTests()
    {
    }
    public function getTransChoiceTests()
    {
    }
    /**
     * @dataProvider getInternal
     */
    public function testInterval($expected, $number, $interval)
    {
    }
    public function getInternal()
    {
    }
    /**
     * @dataProvider getChooseTests
     */
    public function testChoose($expected, $id, $number)
    {
    }
    public function testReturnMessageIfExactlyOneStandardRuleIsGiven()
    {
    }
    /**
     * @dataProvider getNonMatchingMessages
     */
    public function testThrowExceptionIfMatchingMessageCannotBeFound($id, $number)
    {
    }
    public function getNonMatchingMessages()
    {
    }
    public function getChooseTests()
    {
    }
    /**
     * @dataProvider failingLangcodes
     */
    public function testFailedLangcodes($nplural, $langCodes)
    {
    }
    /**
     * @dataProvider successLangcodes
     */
    public function testLangcodes($nplural, $langCodes)
    {
    }
    /**
     * This array should contain all currently known langcodes.
     *
     * As it is impossible to have this ever complete we should try as hard as possible to have it almost complete.
     *
     * @return array
     */
    public function successLangcodes()
    {
    }
    /**
     * This array should be at least empty within the near future.
     *
     * This both depends on a complete list trying to add above as understanding
     * the plural rules of the current failing languages.
     *
     * @return array with nplural together with langcodes
     */
    public function failingLangcodes()
    {
    }
    /**
     * We validate only on the plural coverage. Thus the real rules is not tested.
     *
     * @param string $nplural       Plural expected
     * @param array  $matrix        Containing langcodes and their plural index values
     * @param bool   $expectSuccess
     */
    protected function validateMatrix($nplural, $matrix, $expectSuccess = true)
    {
    }
    protected function generateTestData($langCodes)
    {
    }
}
