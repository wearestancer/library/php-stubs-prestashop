<?php

namespace Symfony\Contracts\HttpClient\Test;

/**
 * @experimental in 1.1
 */
class TestHttpServer
{
    /**
     * @return Process
     */
    public static function start(int $port = 8057)
    {
    }
}
