<?php

namespace Symfony\Contracts\HttpClient\Test;

/**
 * A reference test suite for HttpClientInterface implementations.
 *
 * @experimental in 1.1
 */
abstract class HttpClientTestCase extends \PHPUnit\Framework\TestCase
{
    public static function setUpBeforeClass() : void
    {
    }
    protected abstract function getHttpClient(string $testCase) : \Symfony\Contracts\HttpClient\HttpClientInterface;
    public function testGetRequest()
    {
    }
    public function testHeadRequest()
    {
    }
    public function testNonBufferedGetRequest()
    {
    }
    public function testBufferSink()
    {
    }
    public function testConditionalBuffering()
    {
    }
    public function testReentrantBufferCallback()
    {
    }
    public function testThrowingBufferCallback()
    {
    }
    public function testUnsupportedOption()
    {
    }
    public function testHttpVersion()
    {
    }
    public function testChunkedEncoding()
    {
    }
    public function testClientError()
    {
    }
    public function testIgnoreErrors()
    {
    }
    public function testDnsError()
    {
    }
    public function testInlineAuth()
    {
    }
    public function testBadRequestBody()
    {
    }
    public function test304()
    {
    }
    /**
     * @testWith [[]]
     *           [["Content-Length: 7"]]
     */
    public function testRedirects(array $headers = [])
    {
    }
    public function testInvalidRedirect()
    {
    }
    public function testRelativeRedirects()
    {
    }
    public function testRedirect307()
    {
    }
    public function testMaxRedirects()
    {
    }
    public function testStream()
    {
    }
    public function testAddToStream()
    {
    }
    public function testCompleteTypeError()
    {
    }
    public function testOnProgress()
    {
    }
    public function testPostJson()
    {
    }
    public function testPostArray()
    {
    }
    public function testPostResource()
    {
    }
    public function testPostCallback()
    {
    }
    public function testCancel()
    {
    }
    public function testInfoOnCanceledResponse()
    {
    }
    public function testCancelInStream()
    {
    }
    public function testOnProgressCancel()
    {
    }
    public function testOnProgressError()
    {
    }
    public function testResolve()
    {
    }
    public function testIdnResolve()
    {
    }
    public function testNotATimeout()
    {
    }
    public function testTimeoutOnAccess()
    {
    }
    public function testTimeoutOnStream()
    {
    }
    public function testUncheckedTimeoutThrows()
    {
    }
    public function testTimeoutWithActiveConcurrentStream()
    {
    }
    public function testTimeoutOnInitialize()
    {
    }
    public function testTimeoutOnDestruct()
    {
    }
    public function testDestruct()
    {
    }
    public function testGetContentAfterDestruct()
    {
    }
    public function testGetEncodedContentAfterDestruct()
    {
    }
    public function testProxy()
    {
    }
    public function testNoProxy()
    {
    }
    /**
     * @requires extension zlib
     */
    public function testAutoEncodingRequest()
    {
    }
    public function testBaseUri()
    {
    }
    public function testQuery()
    {
    }
    public function testInformationalResponse()
    {
    }
    public function testInformationalResponseStream()
    {
    }
    /**
     * @requires extension zlib
     */
    public function testUserlandEncodingRequest()
    {
    }
    /**
     * @requires extension zlib
     */
    public function testGzipBroken()
    {
    }
    public function testMaxDuration()
    {
    }
}
