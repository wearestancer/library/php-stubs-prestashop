<?php

namespace Symfony\Contracts\HttpClient\Exception;

/**
 * When a content-type cannot be decoded to the expected representation.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @experimental in 1.1
 */
interface DecodingExceptionInterface extends \Symfony\Contracts\HttpClient\Exception\ExceptionInterface
{
}
