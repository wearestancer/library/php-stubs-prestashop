<?php

namespace Symfony\Contracts\HttpClient\Exception;

/**
 * Base interface for HTTP-related exceptions.
 *
 * @author Anton Chernikov <anton_ch1989@mail.ru>
 *
 * @experimental in 1.1
 */
interface HttpExceptionInterface extends \Symfony\Contracts\HttpClient\Exception\ExceptionInterface
{
    public function getResponse() : \Symfony\Contracts\HttpClient\ResponseInterface;
}
