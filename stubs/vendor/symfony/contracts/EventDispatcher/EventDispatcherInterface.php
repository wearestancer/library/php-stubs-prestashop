<?php

namespace Symfony\Contracts\EventDispatcher;

/**
 * Allows providing hooks on domain-specific lifecycles by dispatching events.
 */
interface EventDispatcherInterface extends \Psr\EventDispatcher\EventDispatcherInterface
{
    /**
     * Dispatches an event to all registered listeners.
     *
     * For BC with Symfony 4, the $eventName argument is not declared explicitly on the
     * signature of the method. Implementations that are not bound by this BC constraint
     * MUST declare it explicitly, as allowed by PHP.
     *
     * @param object      $event     The event to pass to the event handlers/listeners
     * @param string|null $eventName The name of the event to dispatch. If not supplied,
     *                               the class of $event should be used instead.
     *
     * @return object The passed $event MUST be returned
     */
    public function dispatch($event);
}
