<?php

namespace Symfony\Polyfill\Intl\Idn;

/**
 * @internal
 */
class Info
{
    public $bidiDomain = false;
    public $errors = 0;
    public $validBidiDomain = true;
    public $transitionalDifferent = false;
}
