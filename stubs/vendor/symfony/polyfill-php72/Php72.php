<?php

namespace Symfony\Polyfill\Php72;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Dariusz Rumiński <dariusz.ruminski@gmail.com>
 *
 * @internal
 */
final class Php72
{
    public static function utf8_encode($s)
    {
    }
    public static function utf8_decode($s)
    {
    }
    public static function php_os_family()
    {
    }
    public static function spl_object_id($object)
    {
    }
    public static function sapi_windows_vt100_support($stream, $enable = null)
    {
    }
    public static function stream_isatty($stream)
    {
    }
    public static function mb_chr($code, $encoding = null)
    {
    }
    public static function mb_ord($s, $encoding = null)
    {
    }
}
