<?php

namespace Symfony\Bridge\Twig\NodeVisitor;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TranslationDefaultDomainNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Node
     */
    protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Node|null
     */
    protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getPriority()
    {
    }
}
