<?php

namespace Symfony\Bridge\Twig\TokenParser;

/**
 * Token Parser for the 'trans' tag.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TransTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    /**
     * {@inheritdoc}
     *
     * @return Node
     */
    public function parse(\Twig\Token $token)
    {
    }
    public function decideTransFork($token)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTag()
    {
    }
}
