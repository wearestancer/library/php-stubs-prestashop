<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * Provides integration with the HttpKernel component.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class HttpKernelExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    public static function controller($controller, $attributes = [], $query = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
