<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * Provides integration of the Translation component with Twig.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.2
 */
class TranslationExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct($translator = null, \Twig\NodeVisitor\NodeVisitorInterface $translationNodeVisitor = null)
    {
    }
    /**
     * @return TranslatorInterface|null
     */
    public function getTranslator()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFilter[]
     */
    public function getFilters()
    {
    }
    /**
     * Returns the token parser instance to add to the existing list.
     *
     * @return AbstractTokenParser[]
     */
    public function getTokenParsers()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return NodeVisitorInterface[]
     */
    public function getNodeVisitors()
    {
    }
    public function getTranslationNodeVisitor()
    {
    }
    public function trans($message, array $arguments = [], $domain = null, $locale = null, $count = null)
    {
    }
    /**
     * @deprecated since Symfony 4.2, use the trans() method instead with a %count% parameter
     */
    public function transchoice($message, $count, array $arguments = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
