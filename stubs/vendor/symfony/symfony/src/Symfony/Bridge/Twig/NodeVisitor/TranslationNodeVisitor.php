<?php

namespace Symfony\Bridge\Twig\NodeVisitor;

/**
 * TranslationNodeVisitor extracts translation messages.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TranslationNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
{
    public const UNDEFINED_DOMAIN = '_undefined';
    /**
     * @return void
     */
    public function enable()
    {
    }
    /**
     * @return void
     */
    public function disable()
    {
    }
    public function getMessages()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Node
     */
    protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Node|null
     */
    protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getPriority()
    {
    }
}
