<?php

namespace Symfony\Bridge\Twig\ErrorRenderer;

/**
 * Provides the ability to render custom Twig-based HTML error pages
 * in non-debug mode, otherwise falls back to HtmlErrorRenderer.
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class TwigErrorRenderer implements \Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface
{
    /**
     * @param bool|callable $debug The debugging mode as a boolean or a callable that should return it
     */
    public function __construct(\Twig\Environment $twig, \Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer $fallbackErrorRenderer = null, $debug = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render(\Throwable $exception) : \Symfony\Component\ErrorHandler\Exception\FlattenException
    {
    }
    public static function isDebug(\Symfony\Component\HttpFoundation\RequestStack $requestStack, bool $debug) : \Closure
    {
    }
}
