<?php

namespace Symfony\Bridge\Twig\Node;

/**
 * Represents a stopwatch node.
 *
 * @author Wouter J <wouter@wouterj.nl>
 *
 * @final since Symfony 4.4
 */
class StopwatchNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $name, \Twig\Node\Node $body, \Twig\Node\Expression\AssignNameExpression $var, int $lineno = 0, string $tag = null)
    {
    }
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
}
