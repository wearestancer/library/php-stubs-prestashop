<?php

namespace Symfony\Bridge\Twig\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class NotificationEmail extends \Symfony\Bridge\Twig\Mime\TemplatedEmail
{
    public const IMPORTANCE_URGENT = 'urgent';
    public const IMPORTANCE_HIGH = 'high';
    public const IMPORTANCE_MEDIUM = 'medium';
    public const IMPORTANCE_LOW = 'low';
    public function __construct(\Symfony\Component\Mime\Header\Headers $headers = null, \Symfony\Component\Mime\Part\AbstractPart $body = null)
    {
    }
    /**
     * @return $this
     */
    public function markdown(string $content)
    {
    }
    /**
     * @return $this
     */
    public function content(string $content, bool $raw = false)
    {
    }
    /**
     * @return $this
     */
    public function action(string $text, string $url)
    {
    }
    /**
     * @return $this
     */
    public function importance(string $importance)
    {
    }
    /**
     * @param \Throwable|FlattenException $exception
     *
     * @return $this
     */
    public function exception($exception)
    {
    }
    /**
     * @return $this
     */
    public function theme(string $theme)
    {
    }
    public function getTextTemplate() : ?string
    {
    }
    public function getHtmlTemplate() : ?string
    {
    }
    public function getContext() : array
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    /**
     * @internal
     */
    public function __serialize() : array
    {
    }
    /**
     * @internal
     */
    public function __unserialize(array $data) : void
    {
    }
}
