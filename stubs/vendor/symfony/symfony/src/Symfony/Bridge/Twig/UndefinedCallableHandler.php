<?php

namespace Symfony\Bridge\Twig;

/**
 * @internal
 */
class UndefinedCallableHandler
{
    public static function onUndefinedFilter(string $name) : bool
    {
    }
    public static function onUndefinedFunction(string $name) : bool
    {
    }
}
