<?php

namespace Symfony\Bridge\Twig\DataCollector;

/**
 * TwigDataCollector.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TwigDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct(\Twig\Profiler\Profile $profile, \Twig\Environment $twig = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    public function getTime()
    {
    }
    public function getTemplateCount()
    {
    }
    public function getTemplatePaths()
    {
    }
    public function getTemplates()
    {
    }
    public function getBlockCount()
    {
    }
    public function getMacroCount()
    {
    }
    public function getHtmlCallGraph()
    {
    }
    public function getProfile()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
