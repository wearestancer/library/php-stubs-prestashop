<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * WorkflowExtension.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @final since Symfony 4.4
 */
class WorkflowExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Symfony\Component\Workflow\Registry $workflowRegistry)
    {
    }
    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    /**
     * Returns true if the transition is enabled.
     *
     * @param object $subject        A subject
     * @param string $transitionName A transition
     * @param string $name           A workflow name
     *
     * @return bool true if the transition is enabled
     */
    public function canTransition($subject, $transitionName, $name = null)
    {
    }
    /**
     * Returns all enabled transitions.
     *
     * @param object $subject A subject
     * @param string $name    A workflow name
     *
     * @return Transition[] All enabled transitions
     */
    public function getEnabledTransitions($subject, $name = null)
    {
    }
    /**
     * Returns true if the place is marked.
     *
     * @param object $subject   A subject
     * @param string $placeName A place name
     * @param string $name      A workflow name
     *
     * @return bool true if the transition is enabled
     */
    public function hasMarkedPlace($subject, $placeName, $name = null)
    {
    }
    /**
     * Returns marked places.
     *
     * @param object $subject        A subject
     * @param bool   $placesNameOnly If true, returns only places name. If false returns the raw representation
     * @param string $name           A workflow name
     *
     * @return string[]|int[]
     */
    public function getMarkedPlaces($subject, $placesNameOnly = true, $name = null)
    {
    }
    /**
     * Returns the metadata for a specific subject.
     *
     * @param object                 $subject         A subject
     * @param string|Transition|null $metadataSubject Use null to get workflow metadata
     *                                                Use a string (the place name) to get place metadata
     *                                                Use a Transition instance to get transition metadata
     */
    public function getMetadata($subject, string $key, $metadataSubject = null, string $name = null)
    {
    }
    public function buildTransitionBlockerList($subject, string $transitionName, string $name = null) : \Symfony\Component\Workflow\TransitionBlockerList
    {
    }
    public function getName()
    {
    }
}
