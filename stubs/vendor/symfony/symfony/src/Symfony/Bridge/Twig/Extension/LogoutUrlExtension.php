<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * LogoutUrlHelper provides generator functions for the logout URL to Twig.
 *
 * @author Jeremy Mikola <jmikola@gmail.com>
 *
 * @final since Symfony 4.4
 */
class LogoutUrlExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Symfony\Component\Security\Http\Logout\LogoutUrlGenerator $generator)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    /**
     * Generates the relative logout URL for the firewall.
     *
     * @param string|null $key The firewall key or null to use the current firewall key
     *
     * @return string The relative logout URL
     */
    public function getLogoutPath($key = null)
    {
    }
    /**
     * Generates the absolute logout URL for the firewall.
     *
     * @param string|null $key The firewall key or null to use the current firewall key
     *
     * @return string The absolute logout URL
     */
    public function getLogoutUrl($key = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
