<?php

namespace Symfony\Bridge\Twig\Node;

/**
 * @author Julien Galenski <julien.galenski@gmail.com>
 *
 * @final since Symfony 4.4
 */
class DumpNode extends \Twig\Node\Node
{
    public function __construct($varPrefix, \Twig\Node\Node $values = null, int $lineno, string $tag = null)
    {
    }
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
}
