<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 * @author Titouan Galopin <galopintitouan@gmail.com>
 *
 * @final since Symfony 4.4
 */
class CsrfRuntime
{
    public function __construct(\Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager)
    {
    }
    public function getCsrfToken(string $tokenId) : string
    {
    }
}
