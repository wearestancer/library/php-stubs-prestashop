<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * Provides integration of the Yaml component with Twig.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class YamlExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     *
     * @return TwigFilter[]
     */
    public function getFilters()
    {
    }
    public function encode($input, $inline = 0, $dumpObjects = 0)
    {
    }
    public function dump($value, $inline = 0, $dumpObjects = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
