<?php

namespace Symfony\Bridge\Twig\Node;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @final since Symfony 4.4
 */
class SearchAndRenderBlockNode extends \Twig\Node\Expression\FunctionExpression
{
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
}
