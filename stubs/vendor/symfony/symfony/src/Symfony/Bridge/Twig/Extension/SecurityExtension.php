<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * SecurityExtension exposes security context features.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class SecurityExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $securityChecker = null)
    {
    }
    public function isGranted($role, $object = null, $field = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
