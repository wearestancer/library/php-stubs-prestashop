<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * Provides integration of the dump() function with Twig.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @final since Symfony 4.4
 */
class DumpExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Symfony\Component\VarDumper\Cloner\ClonerInterface $cloner, \Symfony\Component\VarDumper\Dumper\HtmlDumper $dumper = null)
    {
    }
    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    /**
     * @return TokenParserInterface[]
     */
    public function getTokenParsers()
    {
    }
    public function getName()
    {
    }
    public function dump(\Twig\Environment $env, $context)
    {
    }
}
