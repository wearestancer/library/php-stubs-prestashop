<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class ProfilerExtension extends \Twig\Extension\ProfilerExtension
{
    public function __construct(\Twig\Profiler\Profile $profile, \Symfony\Component\Stopwatch\Stopwatch $stopwatch = null)
    {
    }
    /**
     * @return void
     */
    public function enter(\Twig\Profiler\Profile $profile)
    {
    }
    /**
     * @return void
     */
    public function leave(\Twig\Profiler\Profile $profile)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
