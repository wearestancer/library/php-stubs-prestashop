<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * FormExtension extends Twig with form capabilities.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @final since Symfony 4.4
 */
class FormExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     *
     * @return TokenParserInterface[]
     */
    public function getTokenParsers()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFilter[]
     */
    public function getFilters()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigTest[]
     */
    public function getTests()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Symfony\Bridge\Twig\Extension;

/**
 * Returns whether a choice is selected for a given form value.
 *
 * This is a function and not callable due to performance reasons.
 *
 * @param string|array $selectedValue The selected value to compare
 *
 * @return bool Whether the choice is selected
 *
 * @see ChoiceView::isSelected()
 */
function twig_is_selected_choice(\Symfony\Component\Form\ChoiceList\View\ChoiceView $choice, $selectedValue) : bool
{
}
/**
 * @internal
 */
function twig_is_root_form(\Symfony\Component\Form\FormView $formView) : bool
{
}
/**
 * @internal
 */
function twig_get_form_parent(\Symfony\Component\Form\FormView $formView) : ?\Symfony\Component\Form\FormView
{
}
