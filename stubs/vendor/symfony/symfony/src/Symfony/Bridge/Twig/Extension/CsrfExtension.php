<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 * @author Titouan Galopin <galopintitouan@gmail.com>
 *
 * @final since Symfony 4.4
 */
class CsrfExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions() : array
    {
    }
}
