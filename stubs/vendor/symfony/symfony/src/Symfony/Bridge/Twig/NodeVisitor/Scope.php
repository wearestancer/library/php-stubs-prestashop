<?php

namespace Symfony\Bridge\Twig\NodeVisitor;

/**
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 */
class Scope
{
    public function __construct(self $parent = null)
    {
    }
    /**
     * Opens a new child scope.
     *
     * @return self
     */
    public function enter()
    {
    }
    /**
     * Closes current scope and returns parent one.
     *
     * @return self|null
     */
    public function leave()
    {
    }
    /**
     * Stores data into current scope.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     *
     * @throws \LogicException
     */
    public function set($key, $value)
    {
    }
    /**
     * Tests if a data is visible from current scope.
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
    }
    /**
     * Returns data visible from current scope.
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
    }
}
