<?php

namespace Symfony\Bridge\Twig\Node;

/**
 * Compiles a call to {@link \Symfony\Component\Form\FormRendererInterface::renderBlock()}.
 *
 * The function name is used as block name. For example, if the function name
 * is "foo", the block "foo" will be rendered.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @final since Symfony 4.4
 */
class RenderBlockNode extends \Twig\Node\Expression\FunctionExpression
{
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
}
