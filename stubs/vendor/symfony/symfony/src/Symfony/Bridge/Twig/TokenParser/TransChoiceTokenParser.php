<?php

namespace Symfony\Bridge\Twig\TokenParser;

/**
 * Token Parser for the 'transchoice' tag.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.2, use the "trans" tag with a "%count%" parameter instead
 *
 * @final since Symfony 4.4
 */
class TransChoiceTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    /**
     * {@inheritdoc}
     *
     * @return Node
     */
    public function parse(\Twig\Token $token)
    {
    }
    public function decideTransChoiceFork($token)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTag()
    {
    }
}
