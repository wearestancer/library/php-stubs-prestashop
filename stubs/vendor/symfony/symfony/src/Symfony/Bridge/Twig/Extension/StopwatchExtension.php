<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * Twig extension for the stopwatch helper.
 *
 * @author Wouter J <wouter@wouterj.nl>
 *
 * @final since Symfony 4.4
 */
class StopwatchExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(\Symfony\Component\Stopwatch\Stopwatch $stopwatch = null, bool $enabled = true)
    {
    }
    public function getStopwatch()
    {
    }
    /**
     * @return TokenParserInterface[]
     */
    public function getTokenParsers()
    {
    }
    public function getName()
    {
    }
}
