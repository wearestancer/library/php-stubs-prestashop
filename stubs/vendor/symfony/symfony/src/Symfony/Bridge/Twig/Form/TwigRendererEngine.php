<?php

namespace Symfony\Bridge\Twig\Form;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class TwigRendererEngine extends \Symfony\Component\Form\AbstractRendererEngine
{
    public function __construct(array $defaultThemes, \Twig\Environment $environment)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderBlock(\Symfony\Component\Form\FormView $view, $resource, $blockName, array $variables = [])
    {
    }
    /**
     * Loads the cache with the resource for a given block name.
     *
     * This implementation eagerly loads all blocks of the themes assigned to the given view
     * and all of its ancestors views. This is necessary, because Twig receives the
     * list of blocks later. At that point, all blocks must already be loaded, for the
     * case that the function "block()" is used in the Twig template.
     *
     * @see getResourceForBlock()
     *
     * @param string   $cacheKey  The cache key of the form view
     * @param FormView $view      The form view for finding the applying themes
     * @param string   $blockName The name of the block to load
     *
     * @return bool True if the resource could be loaded, false otherwise
     */
    protected function loadResourceForBlockName($cacheKey, \Symfony\Component\Form\FormView $view, $blockName)
    {
    }
    /**
     * Loads the resources for all blocks in a theme.
     *
     * @param string $cacheKey The cache key for storing the resource
     * @param mixed  $theme    The theme to load the block from. This parameter
     *                         is passed by reference, because it might be necessary
     *                         to initialize the theme first. Any changes made to
     *                         this variable will be kept and be available upon
     *                         further calls to this method using the same theme.
     */
    protected function loadResourcesFromTheme($cacheKey, &$theme)
    {
    }
}
