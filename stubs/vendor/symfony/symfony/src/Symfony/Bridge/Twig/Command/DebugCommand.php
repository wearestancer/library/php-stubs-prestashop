<?php

namespace Symfony\Bridge\Twig\Command;

/**
 * Lists twig functions, filters, globals and tests present in the current project.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class DebugCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'debug:twig';
    /**
     * @param FileLinkFormatter|null $fileLinkFormatter
     * @param string|null            $rootDir
     */
    public function __construct(\Twig\Environment $twig, string $projectDir = null, array $bundlesMetadata = [], string $twigDefaultPath = null, $fileLinkFormatter = null, $rootDir = null)
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
