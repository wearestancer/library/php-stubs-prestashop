<?php

namespace Symfony\Bridge\Twig\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TransNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $body, \Twig\Node\Node $domain = null, \Twig\Node\Expression\AbstractExpression $count = null, \Twig\Node\Expression\AbstractExpression $vars = null, \Twig\Node\Expression\AbstractExpression $locale = null, int $lineno = 0, string $tag = null)
    {
    }
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
    protected function compileString(\Twig\Node\Node $body, \Twig\Node\Expression\ArrayExpression $vars, $ignoreStrictCheck = false)
    {
    }
}
