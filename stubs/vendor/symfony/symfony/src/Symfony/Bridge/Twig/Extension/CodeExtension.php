<?php

namespace Symfony\Bridge\Twig\Extension;

/**
 * Twig extension relate to PHP code and used by the profiler and the default exception templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class CodeExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * @param string|FileLinkFormatter $fileLinkFormat The format for links to source files
     * @param string                   $projectDir     The project directory
     * @param string                   $charset        The charset
     */
    public function __construct($fileLinkFormat, string $projectDir, string $charset)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFilter[]
     */
    public function getFilters()
    {
    }
    public function abbrClass($class)
    {
    }
    public function abbrMethod($method)
    {
    }
    /**
     * Formats an array as a string.
     *
     * @param array $args The argument array
     *
     * @return string
     */
    public function formatArgs($args)
    {
    }
    /**
     * Formats an array as a string.
     *
     * @param array $args The argument array
     *
     * @return string
     */
    public function formatArgsAsText($args)
    {
    }
    /**
     * Returns an excerpt of a code file around the given line number.
     *
     * @param string $file       A file path
     * @param int    $line       The selected line number
     * @param int    $srcContext The number of displayed lines around or -1 for the whole file
     *
     * @return string An HTML string
     */
    public function fileExcerpt($file, $line, $srcContext = 3)
    {
    }
    /**
     * Formats a file path.
     *
     * @param string $file An absolute file path
     * @param int    $line The line number
     * @param string $text Use this text for the link rather than the file path
     *
     * @return string
     */
    public function formatFile($file, $line, $text = null)
    {
    }
    /**
     * Returns the link for a given file/line pair.
     *
     * @param string $file An absolute file path
     * @param int    $line The line number
     *
     * @return string|false A link or false
     */
    public function getFileLink($file, $line)
    {
    }
    public function getFileRelative(string $file) : ?string
    {
    }
    public function formatFileFromText($text)
    {
    }
    /**
     * @internal
     */
    public function formatLogMessage(string $message, array $context) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    protected static function fixCodeMarkup($line)
    {
    }
}
