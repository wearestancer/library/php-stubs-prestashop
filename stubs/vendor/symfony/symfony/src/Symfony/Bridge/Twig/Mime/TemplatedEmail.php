<?php

namespace Symfony\Bridge\Twig\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TemplatedEmail extends \Symfony\Component\Mime\Email
{
    /**
     * @return $this
     */
    public function textTemplate(?string $template)
    {
    }
    /**
     * @return $this
     */
    public function htmlTemplate(?string $template)
    {
    }
    public function getTextTemplate() : ?string
    {
    }
    public function getHtmlTemplate() : ?string
    {
    }
    /**
     * @return $this
     */
    public function context(array $context)
    {
    }
    public function getContext() : array
    {
    }
    /**
     * @internal
     */
    public function __serialize() : array
    {
    }
    /**
     * @internal
     */
    public function __unserialize(array $data) : void
    {
    }
}
