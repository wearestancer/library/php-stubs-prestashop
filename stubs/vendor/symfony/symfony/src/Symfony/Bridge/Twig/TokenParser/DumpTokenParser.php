<?php

namespace Symfony\Bridge\Twig\TokenParser;

/**
 * Token Parser for the 'dump' tag.
 *
 * Dump variables with:
 *
 *     {% dump %}
 *     {% dump foo %}
 *     {% dump foo, bar %}
 *
 * @author Julien Galenski <julien.galenski@gmail.com>
 *
 * @final since Symfony 4.4
 */
class DumpTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    /**
     * {@inheritdoc}
     *
     * @return Node
     */
    public function parse(\Twig\Token $token)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTag()
    {
    }
}
