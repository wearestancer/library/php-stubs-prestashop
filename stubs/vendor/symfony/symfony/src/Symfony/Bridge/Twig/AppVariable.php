<?php

namespace Symfony\Bridge\Twig;

/**
 * Exposes some Symfony parameters and services as an "app" global variable.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AppVariable
{
    public function setTokenStorage(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage)
    {
    }
    public function setRequestStack(\Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    public function setEnvironment($environment)
    {
    }
    public function setDebug($debug)
    {
    }
    /**
     * Returns the current token.
     *
     * @return TokenInterface|null
     *
     * @throws \RuntimeException When the TokenStorage is not available
     */
    public function getToken()
    {
    }
    /**
     * Returns the current user.
     *
     * @return object|null
     *
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {
    }
    /**
     * Returns the current request.
     *
     * @return Request|null The HTTP request object
     */
    public function getRequest()
    {
    }
    /**
     * Returns the current session.
     *
     * @return Session|null The session
     */
    public function getSession()
    {
    }
    /**
     * Returns the current app environment.
     *
     * @return string The current environment string (e.g 'dev')
     */
    public function getEnvironment()
    {
    }
    /**
     * Returns the current app debug mode.
     *
     * @return bool The current debug mode
     */
    public function getDebug()
    {
    }
    /**
     * Returns some or all the existing flash messages:
     *  * getFlashes() returns all the flash messages
     *  * getFlashes('notice') returns a simple array with flash messages of that type
     *  * getFlashes(['notice', 'error']) returns a nested array of type => messages.
     *
     * @return array
     */
    public function getFlashes($types = null)
    {
    }
}
