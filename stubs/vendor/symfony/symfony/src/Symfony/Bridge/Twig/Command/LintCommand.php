<?php

namespace Symfony\Bridge\Twig\Command;

/**
 * Command that will validate your template syntax and output encountered errors.
 *
 * @author Marc Weistroff <marc.weistroff@sensiolabs.com>
 * @author Jérôme Tamarelle <jerome@tamarelle.net>
 */
class LintCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'lint:twig';
    public function __construct(\Twig\Environment $twig)
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    protected function findFiles($filename)
    {
    }
}
