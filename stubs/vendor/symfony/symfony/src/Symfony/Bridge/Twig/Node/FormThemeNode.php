<?php

namespace Symfony\Bridge\Twig\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class FormThemeNode extends \Twig\Node\Node
{
    public function __construct(\Twig\Node\Node $form, \Twig\Node\Node $resources, int $lineno, string $tag = null, bool $only = false)
    {
    }
    /**
     * @return void
     */
    public function compile(\Twig\Compiler $compiler)
    {
    }
}
