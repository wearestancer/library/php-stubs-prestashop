<?php

namespace Symfony\Bridge\Twig\TokenParser;

/**
 * Token Parser for the 'trans_default_domain' tag.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TransDefaultDomainTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    /**
     * {@inheritdoc}
     *
     * @return Node
     */
    public function parse(\Twig\Token $token)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTag()
    {
    }
}
