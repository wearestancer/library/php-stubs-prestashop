<?php

namespace Symfony\Bridge\Twig\Translation;

/**
 * TwigExtractor extracts translation messages from a twig template.
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TwigExtractor extends \Symfony\Component\Translation\Extractor\AbstractFileExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    public function __construct(\Twig\Environment $twig)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($resource, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
    }
    protected function extractTemplate($template, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
    /**
     * @param string $file
     *
     * @return bool
     */
    protected function canBeExtracted($file)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractFromDirectory($directory)
    {
    }
}
