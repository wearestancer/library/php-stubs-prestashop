<?php

namespace Symfony\Bridge\Twig\TokenParser;

/**
 * Token Parser for the stopwatch tag.
 *
 * @author Wouter J <wouter@wouterj.nl>
 *
 * @final since Symfony 4.4
 */
class StopwatchTokenParser extends \Twig\TokenParser\AbstractTokenParser
{
    protected $stopwatchIsAvailable;
    public function __construct(bool $stopwatchIsAvailable)
    {
    }
    /**
     * @return Node
     */
    public function parse(\Twig\Token $token)
    {
    }
    public function decideStopwatchEnd(\Twig\Token $token)
    {
    }
    /**
     * @return string
     */
    public function getTag()
    {
    }
}
