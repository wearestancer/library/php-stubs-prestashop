<?php

namespace Symfony\Bridge\Twig\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class BodyRenderer implements \Symfony\Component\Mime\BodyRendererInterface
{
    public function __construct(\Twig\Environment $twig, array $context = [])
    {
    }
    public function render(\Symfony\Component\Mime\Message $message) : void
    {
    }
}
