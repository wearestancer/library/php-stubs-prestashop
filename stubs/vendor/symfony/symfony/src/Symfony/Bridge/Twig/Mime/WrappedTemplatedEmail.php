<?php

namespace Symfony\Bridge\Twig\Mime;

/**
 * @internal
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class WrappedTemplatedEmail
{
    public function __construct(\Twig\Environment $twig, \Symfony\Bridge\Twig\Mime\TemplatedEmail $message)
    {
    }
    public function toName() : string
    {
    }
    public function image(string $image, string $contentType = null) : string
    {
    }
    public function attach(string $file, string $name = null, string $contentType = null) : void
    {
    }
    /**
     * @return $this
     */
    public function setSubject(string $subject) : self
    {
    }
    public function getSubject() : ?string
    {
    }
    /**
     * @return $this
     */
    public function setReturnPath(string $address) : self
    {
    }
    public function getReturnPath() : string
    {
    }
    /**
     * @return $this
     */
    public function addFrom(string $address, string $name = '') : self
    {
    }
    /**
     * @return Address[]
     */
    public function getFrom() : array
    {
    }
    /**
     * @return $this
     */
    public function addReplyTo(string $address) : self
    {
    }
    /**
     * @return Address[]
     */
    public function getReplyTo() : array
    {
    }
    /**
     * @return $this
     */
    public function addTo(string $address, string $name = '') : self
    {
    }
    /**
     * @return Address[]
     */
    public function getTo() : array
    {
    }
    /**
     * @return $this
     */
    public function addCc(string $address, string $name = '') : self
    {
    }
    /**
     * @return Address[]
     */
    public function getCc() : array
    {
    }
    /**
     * @return $this
     */
    public function addBcc(string $address, string $name = '') : self
    {
    }
    /**
     * @return Address[]
     */
    public function getBcc() : array
    {
    }
    /**
     * @return $this
     */
    public function setPriority(int $priority) : self
    {
    }
    public function getPriority() : int
    {
    }
}
