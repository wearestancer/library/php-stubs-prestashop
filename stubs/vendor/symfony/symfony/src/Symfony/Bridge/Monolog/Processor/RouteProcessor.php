<?php

namespace Symfony\Bridge\Monolog\Processor;

/**
 * Adds the current route information to the log entry.
 *
 * @author Piotr Stankowski <git@trakos.pl>
 *
 * @final since Symfony 4.4
 */
class RouteProcessor implements \Symfony\Component\EventDispatcher\EventSubscriberInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(bool $includeParams = true)
    {
    }
    public function __invoke(array $records)
    {
    }
    public function reset()
    {
    }
    public function addRouteData(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    public function removeRouteData(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
