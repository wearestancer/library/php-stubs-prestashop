<?php

namespace Symfony\Bridge\Monolog\Handler;

/**
 * Extended SwiftMailerHandler that flushes mail queue if necessary.
 *
 * @author Philipp Kräutli <pkraeutli@astina.ch>
 *
 * @final since Symfony 4.3
 */
class SwiftMailerHandler extends \Monolog\Handler\SwiftMailerHandler
{
    protected $transport;
    protected $instantFlush = false;
    public function setTransport(\Swift_Transport $transport)
    {
    }
    /**
     * After the kernel has been terminated we will always flush messages.
     */
    public function onKernelTerminate(\Symfony\Component\HttpKernel\Event\PostResponseEvent $event)
    {
    }
    /**
     * After the CLI application has been terminated we will always flush messages.
     */
    public function onCliTerminate(\Symfony\Component\Console\Event\ConsoleTerminateEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function send($content, array $records)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
