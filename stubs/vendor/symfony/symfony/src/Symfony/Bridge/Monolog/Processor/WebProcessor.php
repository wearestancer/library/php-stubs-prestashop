<?php

namespace Symfony\Bridge\Monolog\Processor;

/**
 * WebProcessor override to read from the HttpFoundation's Request.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @final since Symfony 4.3
 */
class WebProcessor extends \Monolog\Processor\WebProcessor implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(array $extraFields = null)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
