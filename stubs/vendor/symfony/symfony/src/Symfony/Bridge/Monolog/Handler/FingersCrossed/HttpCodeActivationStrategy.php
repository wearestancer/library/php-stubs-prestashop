<?php

namespace Symfony\Bridge\Monolog\Handler\FingersCrossed;

/**
 * Activation strategy that ignores certain HTTP codes.
 *
 * @author Shaun Simmons <shaun@envysphere.com>
 */
class HttpCodeActivationStrategy extends \Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy
{
    /**
     * @param array $exclusions each exclusion must have a "code" and "urls" keys
     */
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack, array $exclusions, $actionLevel)
    {
    }
    /**
     * @return bool
     */
    public function isHandlerActivated(array $record)
    {
    }
}
