<?php

namespace Symfony\Bridge\Monolog\Formatter;

/**
 * Formats incoming records for console output by coloring them depending on log level.
 *
 * @author Tobias Schultze <http://tobion.de>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class ConsoleFormatter implements \Monolog\Formatter\FormatterInterface
{
    public const SIMPLE_FORMAT = "%datetime% %start_tag%%level_name%%end_tag% [%channel%] %message%%context%%extra%\n";
    public const SIMPLE_DATE = 'H:i:s';
    /**
     * Available options:
     *   * format: The format of the outputted log string. The following placeholders are supported: %datetime%, %start_tag%, %level_name%, %end_tag%, %channel%, %message%, %context%, %extra%;
     *   * date_format: The format of the outputted date string;
     *   * colors: If true, the log string contains ANSI code to add color;
     *   * multiline: If false, "context" and "extra" are dumped on one line.
     */
    public function __construct(array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function formatBatch(array $records)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function format(array $record)
    {
    }
    /**
     * @internal
     */
    public function echoLine(string $line, int $depth, string $indentPad)
    {
    }
    /**
     * @internal
     */
    public function castObject($v, array $a, \Symfony\Component\VarDumper\Cloner\Stub $s, bool $isNested) : array
    {
    }
}
