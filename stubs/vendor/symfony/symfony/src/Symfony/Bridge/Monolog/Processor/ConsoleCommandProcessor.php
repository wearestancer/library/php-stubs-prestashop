<?php

namespace Symfony\Bridge\Monolog\Processor;

/**
 * Adds the current console command information to the log entry.
 *
 * @author Piotr Stankowski <git@trakos.pl>
 */
class ConsoleCommandProcessor implements \Symfony\Component\EventDispatcher\EventSubscriberInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(bool $includeArguments = true, bool $includeOptions = false)
    {
    }
    public function __invoke(array $records)
    {
    }
    public function reset()
    {
    }
    public function addCommandData(\Symfony\Component\Console\Event\ConsoleEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
