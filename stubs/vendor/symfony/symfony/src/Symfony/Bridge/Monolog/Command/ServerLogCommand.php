<?php

namespace Symfony\Bridge\Monolog\Command;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class ServerLogCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'server:log';
    public function isEnabled()
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
