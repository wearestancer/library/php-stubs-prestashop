<?php

namespace Symfony\Bridge\Monolog\Handler;

/**
 * Push logs directly to Elasticsearch and format them according to Logstash specification.
 *
 * This handler dials directly with the HTTP interface of Elasticsearch. This
 * means it will slow down your application if Elasticsearch takes times to
 * answer. Even if all HTTP calls are done asynchronously.
 *
 * In a development environment, it's fine to keep the default configuration:
 * for each log, an HTTP request will be made to push the log to Elasticsearch.
 *
 * In a production environment, it's highly recommended to wrap this handler
 * in a handler with buffering capabilities (like the FingersCrossedHandler, or
 * BufferHandler) in order to call Elasticsearch only once with a bulk push. For
 * even better performance and fault tolerance, a proper ELK (https://www.elastic.co/what-is/elk-stack)
 * stack is recommended.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class ElasticsearchLogstashHandler extends \Monolog\Handler\AbstractHandler
{
    use \Monolog\Handler\FormattableHandlerTrait;
    use \Monolog\Handler\ProcessableHandlerTrait;
    /**
     * @param string|int $level The minimum logging level at which this handler will be triggered
     */
    public function __construct(string $endpoint = 'http://127.0.0.1:9200', string $index = 'monolog', \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, $level = \Monolog\Logger::DEBUG, bool $bubble = true, string $elasticsearchVersion = '1.0.0')
    {
    }
    public function handle(array $record) : bool
    {
    }
    public function handleBatch(array $records) : void
    {
    }
    protected function getDefaultFormatter() : \Monolog\Formatter\FormatterInterface
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
