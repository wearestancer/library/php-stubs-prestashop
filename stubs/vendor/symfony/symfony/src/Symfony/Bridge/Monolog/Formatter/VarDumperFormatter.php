<?php

namespace Symfony\Bridge\Monolog\Formatter;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class VarDumperFormatter implements \Monolog\Formatter\FormatterInterface
{
    public function __construct(\Symfony\Component\VarDumper\Cloner\VarCloner $cloner = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function format(array $record)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function formatBatch(array $records)
    {
    }
}
