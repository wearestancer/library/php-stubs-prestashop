<?php

namespace Symfony\Bridge\Monolog\Processor;

/**
 * Adds the current security token to the log entry.
 *
 * @author Dany Maillard <danymaillard93b@gmail.com>
 */
class TokenProcessor
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage)
    {
    }
    public function __invoke(array $records)
    {
    }
}
