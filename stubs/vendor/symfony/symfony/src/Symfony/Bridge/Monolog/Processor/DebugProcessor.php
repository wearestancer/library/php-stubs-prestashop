<?php

namespace Symfony\Bridge\Monolog\Processor;

class DebugProcessor implements \Symfony\Component\HttpKernel\Log\DebugLoggerInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack = null)
    {
    }
    public function __invoke(array $record)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request|null $request
     */
    public function getLogs()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request|null $request
     */
    public function countErrors()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
