<?php

namespace Symfony\Bridge\Monolog\Handler;

/**
 * FirePHPHandler.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @final since Symfony 4.3
 */
class FirePHPHandler extends \Monolog\Handler\FirePHPHandler
{
    /**
     * Adds the headers to the response once it's created.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function sendHeader($header, $content)
    {
    }
    /**
     * Override default behavior since we check the user agent in onKernelResponse.
     *
     * @return bool
     */
    protected function headersAccepted()
    {
    }
}
