<?php

namespace Symfony\Bridge\Monolog;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Logger extends \Monolog\Logger implements \Symfony\Component\HttpKernel\Log\DebugLoggerInterface, \Symfony\Contracts\Service\ResetInterface
{
    /**
     * {@inheritdoc}
     *
     * @param Request|null $request
     */
    public function getLogs()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request|null $request
     */
    public function countErrors()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    public function removeDebugLogger()
    {
    }
}
