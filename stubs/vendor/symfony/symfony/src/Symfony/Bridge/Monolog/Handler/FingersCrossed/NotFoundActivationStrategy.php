<?php

namespace Symfony\Bridge\Monolog\Handler\FingersCrossed;

/**
 * Activation strategy that ignores 404s for certain URLs.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 * @author Fabien Potencier <fabien@symfony.com>
 */
class NotFoundActivationStrategy extends \Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack, array $excludedUrls, $actionLevel)
    {
    }
    /**
     * @return bool
     */
    public function isHandlerActivated(array $record)
    {
    }
}
