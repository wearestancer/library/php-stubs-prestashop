<?php

namespace Symfony\Bridge\Monolog\Handler;

/**
 * Writes logs to the console output depending on its verbosity setting.
 *
 * It is disabled by default and gets activated as soon as a command is executed.
 * Instead of listening to the console events, the output can also be set manually.
 *
 * The minimum logging level at which this handler will be triggered depends on the
 * verbosity setting of the console output. The default mapping is:
 * - OutputInterface::VERBOSITY_NORMAL will show all WARNING and higher logs
 * - OutputInterface::VERBOSITY_VERBOSE (-v) will show all NOTICE and higher logs
 * - OutputInterface::VERBOSITY_VERY_VERBOSE (-vv) will show all INFO and higher logs
 * - OutputInterface::VERBOSITY_DEBUG (-vvv) will show all DEBUG and higher logs, i.e. all logs
 *
 * This mapping can be customized with the $verbosityLevelMap constructor parameter.
 *
 * @author Tobias Schultze <http://tobion.de>
 */
class ConsoleHandler extends \Monolog\Handler\AbstractProcessingHandler implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param OutputInterface|null $output            The console output to use (the handler remains disabled when passing null
     *                                                until the output is set, e.g. by using console events)
     * @param bool                 $bubble            Whether the messages that are handled can bubble up the stack
     * @param array                $verbosityLevelMap Array that maps the OutputInterface verbosity to a minimum logging
     *                                                level (leave empty to use the default mapping)
     */
    public function __construct(\Symfony\Component\Console\Output\OutputInterface $output = null, bool $bubble = true, array $verbosityLevelMap = [], array $consoleFormatterOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isHandling(array $record)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function handle(array $record)
    {
    }
    /**
     * Sets the console output to use for printing logs.
     */
    public function setOutput(\Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * Disables the output.
     */
    public function close()
    {
    }
    /**
     * Before a command is executed, the handler gets activated and the console output
     * is set in order to know where to write the logs.
     */
    public function onCommand(\Symfony\Component\Console\Event\ConsoleCommandEvent $event)
    {
    }
    /**
     * After a command has been executed, it disables the output.
     */
    public function onTerminate(\Symfony\Component\Console\Event\ConsoleTerminateEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function write(array $record)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return FormatterInterface
     */
    protected function getDefaultFormatter()
    {
    }
}
