<?php

namespace Symfony\Bridge\Monolog\Handler;

/**
 * ChromePhpHandler.
 *
 * @author Christophe Coevoet <stof@notk.org>
 *
 * @final since Symfony 4.3
 */
class ChromePhpHandler extends \Monolog\Handler\ChromePHPHandler
{
    /**
     * Adds the headers to the response once it's created.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function sendHeader($header, $content)
    {
    }
    /**
     * Override default behavior since we check it in onKernelResponse.
     *
     * @return bool
     */
    protected function headersAccepted()
    {
    }
}
