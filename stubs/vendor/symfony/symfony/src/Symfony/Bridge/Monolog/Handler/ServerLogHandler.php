<?php

namespace Symfony\Bridge\Monolog\Handler;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class ServerLogHandler extends \Monolog\Handler\AbstractHandler
{
    /**
     * @param string|int $level The minimum logging level at which this handler will be triggered
     */
    public function __construct(string $host, $level = \Monolog\Logger::DEBUG, bool $bubble = true, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function handle(array $record)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return FormatterInterface
     */
    protected function getDefaultFormatter()
    {
    }
}
