<?php

namespace Symfony\Bridge\ProxyManager\LazyProxy\PhpDumper;

/**
 * @internal
 */
class LazyLoadingValueHolderGenerator extends \ProxyManager\ProxyGenerator\LazyLoadingValueHolderGenerator
{
    /**
     * {@inheritdoc}
     */
    public function generate(\ReflectionClass $originalClass, \Laminas\Code\Generator\ClassGenerator $classGenerator, array $proxyOptions = []) : void
    {
    }
    public function getProxifiedClass(\Symfony\Component\DependencyInjection\Definition $definition) : ?string
    {
    }
}
