<?php

namespace Symfony\Bridge\ProxyManager\LazyProxy\Instantiator;

/**
 * Runtime lazy loading proxy generator.
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 */
class RuntimeInstantiator implements \Symfony\Component\DependencyInjection\LazyProxy\Instantiator\InstantiatorInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function instantiateProxy(\Symfony\Component\DependencyInjection\ContainerInterface $container, \Symfony\Component\DependencyInjection\Definition $definition, $id, $realInstantiator)
    {
    }
}
