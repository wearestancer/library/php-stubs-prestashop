<?php

namespace Symfony\Bridge\ProxyManager\LazyProxy\PhpDumper;

/**
 * Generates dumped PHP code of proxies via reflection.
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 *
 * @final
 */
class ProxyDumper implements \Symfony\Component\DependencyInjection\LazyProxy\PhpDumper\DumperInterface
{
    public function __construct(string $salt = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isProxyCandidate(\Symfony\Component\DependencyInjection\Definition $definition) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProxyFactoryCode(\Symfony\Component\DependencyInjection\Definition $definition, $id, $factoryCode = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProxyCode(\Symfony\Component\DependencyInjection\Definition $definition) : string
    {
    }
}
