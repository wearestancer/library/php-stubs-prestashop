<?php

namespace Symfony\Bridge\ProxyManager\LazyProxy\Instantiator;

/**
 * @internal
 */
class LazyLoadingValueHolderFactory extends \ProxyManager\Factory\LazyLoadingValueHolderFactory
{
    /**
     * {@inheritdoc}
     */
    public function getGenerator() : \ProxyManager\ProxyGenerator\ProxyGeneratorInterface
    {
    }
}
