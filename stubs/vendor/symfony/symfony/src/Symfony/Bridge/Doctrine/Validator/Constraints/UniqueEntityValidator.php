<?php

namespace Symfony\Bridge\Doctrine\Validator\Constraints;

/**
 * Unique Entity Validator checks if one or a set of fields contain unique values.
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 */
class UniqueEntityValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
    }
    /**
     * @param object $entity
     *
     * @throws UnexpectedTypeException
     * @throws ConstraintDefinitionException
     */
    public function validate($entity, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
