<?php

namespace Symfony\Bridge\Doctrine\Validator;

/**
 * Guesses and loads the appropriate constraints using Doctrine's metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class DoctrineLoader implements \Symfony\Component\Validator\Mapping\Loader\LoaderInterface
{
    use \Symfony\Component\Validator\Mapping\Loader\AutoMappingTrait;
    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager, string $classValidatorRegexp = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata) : bool
    {
    }
}
