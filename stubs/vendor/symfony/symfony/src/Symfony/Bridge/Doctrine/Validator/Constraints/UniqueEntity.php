<?php

namespace Symfony\Bridge\Doctrine\Validator\Constraints;

/**
 * Constraint for the Unique Entity validator.
 *
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 */
class UniqueEntity extends \Symfony\Component\Validator\Constraint
{
    public const NOT_UNIQUE_ERROR = '23bd9dbf-6b9b-41cd-a99e-4844bcf3077f';
    public $message = 'This value is already used.';
    public $service = 'doctrine.orm.validator.unique';
    public $em = null;
    public $entityClass = null;
    public $repositoryMethod = 'findBy';
    public $fields = [];
    public $errorPath = null;
    public $ignoreNull = true;
    protected static $errorNames = [self::NOT_UNIQUE_ERROR => 'NOT_UNIQUE_ERROR'];
    public function getRequiredOptions()
    {
    }
    /**
     * The validator must be defined as a service with this name.
     *
     * @return string
     */
    public function validatedBy()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
    }
    public function getDefaultOption()
    {
    }
}
