<?php

namespace Symfony\Bridge\Doctrine\DependencyInjection\Security\UserProvider;

/**
 * EntityFactory creates services for Doctrine user provider.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class EntityFactory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider\UserProviderFactoryInterface
{
    public function __construct(string $key, string $providerId)
    {
    }
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config)
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
}
