<?php

namespace Symfony\Bridge\Doctrine\DependencyInjection\CompilerPass;

/**
 * Base class for the doctrine bundles to provide a compiler pass class that
 * helps to register doctrine mappings.
 *
 * The compiler pass is meant to register the mappings with the metadata
 * chain driver corresponding to one of the object managers.
 *
 * For concrete implementations, see the RegisterXyMappingsPass classes
 * in the DoctrineBundle resp.
 * DoctrineMongodbBundle, DoctrineCouchdbBundle and DoctrinePhpcrBundle.
 *
 * @author David Buchmann <david@liip.ch>
 */
abstract class RegisterMappingsPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * DI object for the driver to use, either a service definition for a
     * private service or a reference for a public service.
     *
     * @var Definition|Reference
     */
    protected $driver;
    /**
     * List of namespaces handled by the driver.
     *
     * @var string[]
     */
    protected $namespaces;
    /**
     * List of potential container parameters that hold the object manager name
     * to register the mappings with the correct metadata driver, for example
     * ['acme.manager', 'doctrine.default_entity_manager'].
     *
     * @var string[]
     */
    protected $managerParameters;
    /**
     * Naming pattern of the metadata chain driver service ids, for example
     * 'doctrine.orm.%s_metadata_driver'.
     *
     * @var string
     */
    protected $driverPattern;
    /**
     * A name for a parameter in the container. If set, this compiler pass will
     * only do anything if the parameter is present. (But regardless of the
     * value of that parameter.
     *
     * @var string|false
     */
    protected $enabledParameter;
    /**
     * The $managerParameters is an ordered list of container parameters that could provide the
     * name of the manager to register these namespaces and alias on. The first non-empty name
     * is used, the others skipped.
     *
     * The $aliasMap parameter can be used to define bundle namespace shortcuts like the
     * DoctrineBundle provides automatically for objects in the default Entity/Document folder.
     *
     * @param Definition|Reference $driver                  Driver DI definition or reference
     * @param string[]             $namespaces              List of namespaces handled by $driver
     * @param string[]             $managerParameters       list of container parameters that could
     *                                                      hold the manager name
     * @param string               $driverPattern           Pattern for the metadata driver service name
     * @param string|false         $enabledParameter        Service container parameter that must be
     *                                                      present to enable the mapping. Set to false
     *                                                      to not do any check, optional.
     * @param string               $configurationPattern    Pattern for the Configuration service name
     * @param string               $registerAliasMethodName Name of Configuration class method to
     *                                                      register alias
     * @param string[]             $aliasMap                Map of alias to namespace
     */
    public function __construct($driver, array $namespaces, array $managerParameters, string $driverPattern, $enabledParameter = false, string $configurationPattern = '', string $registerAliasMethodName = '', array $aliasMap = [])
    {
    }
    /**
     * Register mappings and alias with the metadata drivers.
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Get the service name of the metadata chain driver that the mappings
     * should be registered with.
     *
     * @return string The name of the chain driver service
     *
     * @throws InvalidArgumentException if non of the managerParameters has a
     *                                  non-empty value
     */
    protected function getChainDriverServiceName(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Create the service definition for the metadata driver.
     *
     * @param ContainerBuilder $container Passed on in case an extending class
     *                                    needs access to the container
     *
     * @return Definition|Reference the metadata driver to add to all chain drivers
     */
    protected function getDriver(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Determine whether this mapping should be activated or not. This allows
     * to take this decision with the container builder available.
     *
     * This default implementation checks if the class has the enabledParameter
     * configured and if so if that parameter is present in the container.
     *
     * @return bool whether this compiler pass really should register the mappings
     */
    protected function enabled(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
