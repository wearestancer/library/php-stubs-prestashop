<?php

namespace Symfony\Bridge\Doctrine\DependencyInjection;

/**
 * This abstract classes groups common code that Doctrine Object Manager extensions (ORM, MongoDB, CouchDB) need.
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 */
abstract class AbstractDoctrineExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension
{
    /**
     * Used inside metadata driver method to simplify aggregation of data.
     */
    protected $aliasMap = [];
    /**
     * Used inside metadata driver method to simplify aggregation of data.
     */
    protected $drivers = [];
    /**
     * @param array $objectManager A configured object manager
     *
     * @throws \InvalidArgumentException
     */
    protected function loadMappingInformation(array $objectManager, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Register the alias for this mapping driver.
     *
     * Aliases can be used in the Query languages of all the Doctrine object managers to simplify writing tasks.
     *
     * @param array  $mappingConfig
     * @param string $mappingName
     */
    protected function setMappingDriverAlias($mappingConfig, $mappingName)
    {
    }
    /**
     * Register the mapping driver configuration for later use with the object managers metadata driver chain.
     *
     * @param string $mappingName
     *
     * @throws \InvalidArgumentException
     */
    protected function setMappingDriverConfig(array $mappingConfig, $mappingName)
    {
    }
    /**
     * If this is a bundle controlled mapping all the missing information can be autodetected by this method.
     *
     * Returns false when autodetection failed, an array of the completed information otherwise.
     *
     * @return array|false
     */
    protected function getMappingDriverBundleConfigDefaults(array $bundleConfig, \ReflectionClass $bundle, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Register all the collected mapping information with the object manager by registering the appropriate mapping drivers.
     *
     * @param array $objectManager
     */
    protected function registerMappingDrivers($objectManager, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Assertion if the specified mapping information is valid.
     *
     * @param string $objectManagerName
     *
     * @throws \InvalidArgumentException
     */
    protected function assertValidMappingConfiguration(array $mappingConfig, $objectManagerName)
    {
    }
    /**
     * Detects what metadata driver to use for the supplied directory.
     *
     * @param string $dir A directory path
     *
     * @return string|null A metadata driver short name, if one can be detected
     */
    protected function detectMetadataDriver($dir, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Loads a configured object manager metadata, query or result cache driver.
     *
     * @param array  $objectManager A configured object manager
     * @param string $cacheName
     *
     * @throws \InvalidArgumentException in case of unknown driver type
     */
    protected function loadObjectManagerCacheDriver(array $objectManager, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $cacheName)
    {
    }
    /**
     * Loads a cache driver.
     *
     * @param string $cacheName         The cache driver name
     * @param string $objectManagerName The object manager name
     * @param array  $cacheDriver       The cache driver mapping
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function loadCacheDriver($cacheName, $objectManagerName, array $cacheDriver, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Returns a modified version of $managerConfigs.
     *
     * The manager called $autoMappedManager will map all bundles that are not mapped by other managers.
     *
     * @return array The modified version of $managerConfigs
     */
    protected function fixManagersAutoMappings(array $managerConfigs, array $bundles)
    {
    }
    /**
     * Prefixes the relative dependency injection container path with the object manager prefix.
     *
     * @example $name is 'entity_manager' then the result would be 'doctrine.orm.entity_manager'
     *
     * @param string $name
     *
     * @return string
     */
    protected abstract function getObjectManagerElementName($name);
    /**
     * Noun that describes the mapped objects such as Entity or Document.
     *
     * Will be used for autodetection of persistent objects directory.
     *
     * @return string
     */
    protected abstract function getMappingObjectDefaultName();
    /**
     * Relative path from the bundle root to the directory where mapping files reside.
     *
     * @return string
     */
    protected abstract function getMappingResourceConfigDirectory();
    /**
     * Extension used by the mapping files.
     *
     * @return string
     */
    protected abstract function getMappingResourceExtension();
    /**
     * The class name used by the various mapping drivers.
     */
    protected function getMetadataDriverClass(string $driverType) : string
    {
    }
}
