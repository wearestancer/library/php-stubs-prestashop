<?php

namespace Symfony\Bridge\Doctrine\Form\ChoiceList;

/**
 * Loads choices using a Doctrine object manager.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DoctrineChoiceLoader implements \Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface
{
    /**
     * Creates a new choice loader.
     *
     * Optionally, an implementation of {@link EntityLoaderInterface} can be
     * passed which optimizes the object loading for one of the Doctrine
     * mapper implementations.
     *
     * @param string $class The class name of the loaded objects
     */
    public function __construct(\Doctrine\Persistence\ObjectManager $manager, string $class, \Symfony\Bridge\Doctrine\Form\ChoiceList\IdReader $idReader = null, \Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface $objectLoader = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadChoiceList($value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadValuesForChoices(array $choices, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadChoicesForValues(array $values, $value = null)
    {
    }
}
