<?php

namespace Symfony\Bridge\Doctrine\Form\DataTransformer;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CollectionToArrayTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * Transforms a collection into an array.
     *
     * @throws TransformationFailedException
     */
    public function transform($collection)
    {
    }
    /**
     * Transforms an array into a collection.
     *
     * @return Collection
     */
    public function reverseTransform($array)
    {
    }
}
