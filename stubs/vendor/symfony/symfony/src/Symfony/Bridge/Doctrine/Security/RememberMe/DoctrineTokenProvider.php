<?php

namespace Symfony\Bridge\Doctrine\Security\RememberMe;

/**
 * This class provides storage for the tokens that is set in "remember me"
 * cookies. This way no password secrets will be stored in the cookies on
 * the client machine, and thus the security is improved.
 *
 * This depends only on doctrine in order to get a database connection
 * and to do the conversion of the datetime column.
 *
 * In order to use this class, you need the following table in your database:
 *
 *     CREATE TABLE `rememberme_token` (
 *         `series`   char(88)     UNIQUE PRIMARY KEY NOT NULL,
 *         `value`    char(88)     NOT NULL,
 *         `lastUsed` datetime     NOT NULL,
 *         `class`    varchar(100) NOT NULL,
 *         `username` varchar(200) NOT NULL
 *     );
 */
class DoctrineTokenProvider implements \Symfony\Component\Security\Core\Authentication\RememberMe\TokenProviderInterface
{
    public function __construct(\Doctrine\DBAL\Connection $conn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadTokenBySeries($series)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function deleteTokenBySeries($series)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateToken($series, $tokenValue, \DateTime $lastUsed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNewToken(\Symfony\Component\Security\Core\Authentication\RememberMe\PersistentTokenInterface $token)
    {
    }
}
