<?php

namespace Symfony\Bridge\Doctrine\PropertyInfo;

/**
 * Extracts data using Doctrine ORM and ODM metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class DoctrineExtractor implements \Symfony\Component\PropertyInfo\PropertyListExtractorInterface, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface, \Symfony\Component\PropertyInfo\PropertyAccessExtractorInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct($entityManager)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties($class, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypes($class, $property, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isReadable($class, $property, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isWritable($class, $property, array $context = [])
    {
    }
}
