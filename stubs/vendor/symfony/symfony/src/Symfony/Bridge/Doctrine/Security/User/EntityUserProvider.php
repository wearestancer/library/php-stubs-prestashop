<?php

namespace Symfony\Bridge\Doctrine\Security\User;

/**
 * Wrapper around a Doctrine ObjectManager.
 *
 * Provides provisioning for Doctrine entity users.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class EntityUserProvider implements \Symfony\Component\Security\Core\User\UserProviderInterface, \Symfony\Component\Security\Core\User\PasswordUpgraderInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry, string $classOrAlias, string $property = null, string $managerName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function upgradePassword(\Symfony\Component\Security\Core\User\UserInterface $user, string $newEncodedPassword) : void
    {
    }
}
