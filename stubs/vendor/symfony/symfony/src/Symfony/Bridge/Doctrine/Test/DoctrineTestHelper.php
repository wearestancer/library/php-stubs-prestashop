<?php

namespace Symfony\Bridge\Doctrine\Test;

/**
 * Provides utility functions needed in tests.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DoctrineTestHelper
{
    /**
     * Returns an entity manager for testing.
     *
     * @return EntityManager
     */
    public static function createTestEntityManager(\Doctrine\ORM\Configuration $config = null)
    {
    }
    /**
     * @return Configuration
     */
    public static function createTestConfiguration()
    {
    }
    /**
     * @return Configuration
     */
    public static function createTestConfigurationWithXmlLoader()
    {
    }
}
