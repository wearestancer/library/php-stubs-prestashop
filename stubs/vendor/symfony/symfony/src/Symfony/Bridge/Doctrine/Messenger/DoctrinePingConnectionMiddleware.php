<?php

namespace Symfony\Bridge\Doctrine\Messenger;

/**
 * Checks whether the connection is still open or reconnects otherwise.
 *
 * @author Fuong <insidestyles@gmail.com>
 */
class DoctrinePingConnectionMiddleware extends \Symfony\Bridge\Doctrine\Messenger\AbstractDoctrineMiddleware
{
    protected function handleForManager(\Doctrine\ORM\EntityManagerInterface $entityManager, \Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
