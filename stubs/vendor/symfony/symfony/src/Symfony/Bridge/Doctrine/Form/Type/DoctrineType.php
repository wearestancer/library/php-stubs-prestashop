<?php

namespace Symfony\Bridge\Doctrine\Form\Type;

abstract class DoctrineType extends \Symfony\Component\Form\AbstractType implements \Symfony\Contracts\Service\ResetInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $registry;
    /**
     * Creates the label for a choice.
     *
     * For backwards compatibility, objects are cast to strings by default.
     *
     * @param object $choice The object
     *
     * @internal This method is public to be usable as callback. It should not
     *           be used in user code.
     */
    public static function createChoiceLabel($choice) : string
    {
    }
    /**
     * Creates the field name for a choice.
     *
     * This method is used to generate field names if the underlying object has
     * a single-column integer ID. In that case, the value of the field is
     * the ID of the object. That ID is also used as field name.
     *
     * @param object     $choice The object
     * @param int|string $key    The choice key
     * @param string     $value  The choice value. Corresponds to the object's
     *                           ID here.
     *
     * @internal This method is public to be usable as callback. It should not
     *           be used in user code.
     */
    public static function createChoiceName($choice, $key, $value) : string
    {
    }
    /**
     * Gets important parts from QueryBuilder that will allow to cache its results.
     * For instance in ORM two query builders with an equal SQL string and
     * equal parameters are considered to be equal.
     *
     * @param object $queryBuilder A query builder, type declaration is not present here as there
     *                             is no common base class for the different implementations
     *
     * @return array|null Array with important QueryBuilder parts or null if
     *                    they can't be determined
     *
     * @internal This method is public to be usable as callback. It should not
     *           be used in user code.
     */
    public function getQueryBuilderPartsForCachingHash($queryBuilder) : ?array
    {
    }
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
    }
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * Return the default loader object.
     *
     * @param mixed  $queryBuilder
     * @param string $class
     *
     * @return EntityLoaderInterface
     */
    public abstract function getLoader(\Doctrine\Persistence\ObjectManager $manager, $queryBuilder, $class);
    public function getParent()
    {
    }
    public function reset()
    {
    }
}
