<?php

namespace Symfony\Bridge\Doctrine\Form;

class DoctrineOrmTypeGuesser implements \Symfony\Component\Form\FormTypeGuesserInterface
{
    protected $registry;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessType($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessRequired($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessMaxLength($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessPattern($class, $property)
    {
    }
    protected function getMetadata($class)
    {
    }
}
