<?php

namespace Symfony\Bridge\Doctrine;

/**
 * Allows lazy loading of listener and subscriber services.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ContainerAwareEventManager extends \Doctrine\Common\EventManager
{
    public function __construct(\Psr\Container\ContainerInterface $container, array $subscriberIds = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function dispatchEvent($eventName, \Doctrine\Common\EventArgs $eventArgs = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return object[][]
     */
    public function getListeners($event = null)
    {
    }
    public function getAllListeners() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasListeners($event)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function addEventListener($events, $listener)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function removeEventListener($events, $listener)
    {
    }
}
