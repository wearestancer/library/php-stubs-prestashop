<?php

namespace Symfony\Bridge\Doctrine\DataCollector;

final class ObjectParameter
{
    /**
     * @param object $object
     */
    public function __construct($object, ?\Throwable $error)
    {
    }
    /**
     * @return object
     */
    public function getObject()
    {
    }
    public function getError() : ?\Throwable
    {
    }
    public function isStringable() : bool
    {
    }
    public function getClass() : string
    {
    }
}
