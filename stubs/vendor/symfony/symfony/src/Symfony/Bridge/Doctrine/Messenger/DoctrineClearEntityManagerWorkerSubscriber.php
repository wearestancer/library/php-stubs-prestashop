<?php

namespace Symfony\Bridge\Doctrine\Messenger;

/**
 * Clears entity managers between messages being handled to avoid outdated data.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class DoctrineClearEntityManagerWorkerSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    public function onWorkerMessageHandled()
    {
    }
    public function onWorkerMessageFailed()
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
