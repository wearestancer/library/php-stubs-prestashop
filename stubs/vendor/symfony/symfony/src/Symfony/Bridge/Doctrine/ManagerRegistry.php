<?php

namespace Symfony\Bridge\Doctrine;

/**
 * References Doctrine connections and entity/document managers.
 *
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 */
abstract class ManagerRegistry extends \Doctrine\Persistence\AbstractManagerRegistry
{
    /**
     * @var Container
     */
    protected $container;
    /**
     * {@inheritdoc}
     *
     * @return object
     */
    protected function getService($name)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function resetService($name)
    {
    }
}
