<?php

namespace Symfony\Bridge\Doctrine\DataCollector;

/**
 * DoctrineDataCollector.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DoctrineDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
    }
    /**
     * Adds the stack logger for a connection.
     *
     * @param string $name
     */
    public function addLogger($name, \Doctrine\DBAL\Logging\DebugStack $logger)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function reset()
    {
    }
    public function getManagers()
    {
    }
    public function getConnections()
    {
    }
    public function getQueryCount()
    {
    }
    public function getQueries()
    {
    }
    public function getTime()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getCasters()
    {
    }
}
