<?php

namespace Symfony\Bridge\Doctrine\Messenger;

/**
 * Closes connection and therefore saves number of connections.
 *
 * @author Fuong <insidestyles@gmail.com>
 */
class DoctrineCloseConnectionMiddleware extends \Symfony\Bridge\Doctrine\Messenger\AbstractDoctrineMiddleware
{
    protected function handleForManager(\Doctrine\ORM\EntityManagerInterface $entityManager, \Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
