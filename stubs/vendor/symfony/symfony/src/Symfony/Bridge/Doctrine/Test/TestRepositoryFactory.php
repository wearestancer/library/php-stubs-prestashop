<?php

namespace Symfony\Bridge\Doctrine\Test;

/**
 * @author Andreas Braun <alcaeus@alcaeus.org>
 */
final class TestRepositoryFactory implements \Doctrine\ORM\Repository\RepositoryFactory
{
    /**
     * {@inheritdoc}
     *
     * @return ObjectRepository
     */
    public function getRepository(\Doctrine\ORM\EntityManagerInterface $entityManager, $entityName)
    {
    }
    public function setRepository(\Doctrine\ORM\EntityManagerInterface $entityManager, string $entityName, \Doctrine\Persistence\ObjectRepository $repository)
    {
    }
}
