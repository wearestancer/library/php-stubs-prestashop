<?php

namespace Symfony\Bridge\Doctrine\Logger;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DbalLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    public const MAX_STRING_LENGTH = 32;
    public const BINARY_DATA_VALUE = '(binary value)';
    protected $logger;
    protected $stopwatch;
    public function __construct(\Psr\Log\LoggerInterface $logger = null, \Symfony\Component\Stopwatch\Stopwatch $stopwatch = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function startQuery($sql, array $params = null, array $types = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function stopQuery()
    {
    }
    /**
     * Logs a message.
     *
     * @param string $message A message to log
     * @param array  $params  The context
     */
    protected function log($message, array $params)
    {
    }
}
