<?php

namespace Symfony\Bridge\Doctrine\Form\ChoiceList;

/**
 * A utility for reading object IDs.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class IdReader
{
    public function __construct(\Doctrine\Persistence\ObjectManager $om, \Doctrine\Persistence\Mapping\ClassMetadata $classMetadata)
    {
    }
    /**
     * Returns whether the class has a single-column ID.
     *
     * @return bool returns `true` if the class has a single-column ID and
     *              `false` otherwise
     */
    public function isSingleId() : bool
    {
    }
    /**
     * Returns whether the class has a single-column integer ID.
     *
     * @return bool returns `true` if the class has a single-column integer ID
     *              and `false` otherwise
     */
    public function isIntId() : bool
    {
    }
    /**
     * Returns the ID value for an object.
     *
     * This method assumes that the object has a single-column ID.
     *
     * @param object $object The object
     *
     * @return mixed The ID value
     */
    public function getIdValue($object)
    {
    }
    /**
     * Returns the name of the ID field.
     *
     * This method assumes that the object has a single-column ID.
     *
     * @return string The name of the ID field
     */
    public function getIdField() : string
    {
    }
}
