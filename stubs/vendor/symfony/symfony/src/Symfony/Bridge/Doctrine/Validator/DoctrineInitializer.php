<?php

namespace Symfony\Bridge\Doctrine\Validator;

/**
 * Automatically loads proxy object before validation.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DoctrineInitializer implements \Symfony\Component\Validator\ObjectInitializerInterface
{
    protected $registry;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
    }
    public function initialize($object)
    {
    }
}
