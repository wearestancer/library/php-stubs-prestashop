<?php

namespace Symfony\Bridge\Doctrine\Messenger;

/**
 * Wraps all handlers in a single doctrine transaction.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class DoctrineTransactionMiddleware extends \Symfony\Bridge\Doctrine\Messenger\AbstractDoctrineMiddleware
{
    protected function handleForManager(\Doctrine\ORM\EntityManagerInterface $entityManager, \Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
