<?php

namespace Symfony\Bridge\Doctrine\Form\Type;

class EntityType extends \Symfony\Bridge\Doctrine\Form\Type\DoctrineType
{
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * Return the default loader object.
     *
     * @param QueryBuilder $queryBuilder
     * @param string       $class
     *
     * @return ORMQueryBuilderLoader
     */
    public function getLoader(\Doctrine\Persistence\ObjectManager $manager, $queryBuilder, $class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
    /**
     * We consider two query builders with an equal SQL string and
     * equal parameters to be equal.
     *
     * @param QueryBuilder $queryBuilder
     *
     * @internal This method is public to be usable as callback. It should not
     *           be used in user code.
     */
    public function getQueryBuilderPartsForCachingHash($queryBuilder) : ?array
    {
    }
}
