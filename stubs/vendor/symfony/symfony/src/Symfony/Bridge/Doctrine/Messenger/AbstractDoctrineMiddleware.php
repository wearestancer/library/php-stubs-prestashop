<?php

namespace Symfony\Bridge\Doctrine\Messenger;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 *
 * @internal
 */
abstract class AbstractDoctrineMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    protected $managerRegistry;
    protected $entityManagerName;
    public function __construct(\Doctrine\Persistence\ManagerRegistry $managerRegistry, string $entityManagerName = null)
    {
    }
    public final function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
    protected abstract function handleForManager(\Doctrine\ORM\EntityManagerInterface $entityManager, \Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope;
}
