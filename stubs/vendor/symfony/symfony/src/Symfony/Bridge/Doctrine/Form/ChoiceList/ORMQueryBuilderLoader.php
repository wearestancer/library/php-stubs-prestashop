<?php

namespace Symfony\Bridge\Doctrine\Form\ChoiceList;

/**
 * Loads entities using a {@link QueryBuilder} instance.
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ORMQueryBuilderLoader implements \Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface
{
    public function __construct(\Doctrine\ORM\QueryBuilder $queryBuilder)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEntities()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEntitiesByIds($identifier, array $values)
    {
    }
}
