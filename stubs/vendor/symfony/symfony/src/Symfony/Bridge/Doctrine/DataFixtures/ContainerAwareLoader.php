<?php

namespace Symfony\Bridge\Doctrine\DataFixtures;

/**
 * Doctrine data fixtures loader that injects the service container into
 * fixture objects that implement ContainerAwareInterface.
 *
 * Note: Use of this class requires the Doctrine data fixtures extension, which
 * is a suggested dependency for Symfony.
 */
class ContainerAwareLoader extends \Doctrine\Common\DataFixtures\Loader
{
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addFixture(\Doctrine\Common\DataFixtures\FixtureInterface $fixture)
    {
    }
}
