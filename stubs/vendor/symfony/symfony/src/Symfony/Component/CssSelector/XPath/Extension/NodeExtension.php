<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator node extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class NodeExtension extends \Symfony\Component\CssSelector\XPath\Extension\AbstractExtension
{
    public const ELEMENT_NAME_IN_LOWER_CASE = 1;
    public const ATTRIBUTE_NAME_IN_LOWER_CASE = 2;
    public const ATTRIBUTE_VALUE_IN_LOWER_CASE = 4;
    public function __construct(int $flags = 0)
    {
    }
    /**
     * @return $this
     */
    public function setFlag(int $flag, bool $on) : self
    {
    }
    public function hasFlag(int $flag) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNodeTranslators() : array
    {
    }
    public function translateSelector(\Symfony\Component\CssSelector\Node\SelectorNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateCombinedSelector(\Symfony\Component\CssSelector\Node\CombinedSelectorNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateNegation(\Symfony\Component\CssSelector\Node\NegationNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateFunction(\Symfony\Component\CssSelector\Node\FunctionNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translatePseudo(\Symfony\Component\CssSelector\Node\PseudoNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateAttribute(\Symfony\Component\CssSelector\Node\AttributeNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateClass(\Symfony\Component\CssSelector\Node\ClassNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateHash(\Symfony\Component\CssSelector\Node\HashNode $node, \Symfony\Component\CssSelector\XPath\Translator $translator) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateElement(\Symfony\Component\CssSelector\Node\ElementNode $node) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
}
