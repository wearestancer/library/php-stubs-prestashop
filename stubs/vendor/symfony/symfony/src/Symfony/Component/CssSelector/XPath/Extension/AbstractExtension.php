<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator abstract extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
abstract class AbstractExtension implements \Symfony\Component\CssSelector\XPath\Extension\ExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getNodeTranslators() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCombinationTranslators() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFunctionTranslators() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPseudoClassTranslators() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAttributeMatchingTranslators() : array
    {
    }
}
