<?php

namespace Symfony\Component\CssSelector\XPath;

/**
 * XPath expression translator interface.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class Translator implements \Symfony\Component\CssSelector\XPath\TranslatorInterface
{
    public function __construct(\Symfony\Component\CssSelector\Parser\ParserInterface $parser = null)
    {
    }
    public static function getXpathLiteral(string $element) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function cssToXPath(string $cssExpr, string $prefix = 'descendant-or-self::') : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function selectorToXPath(\Symfony\Component\CssSelector\Node\SelectorNode $selector, string $prefix = 'descendant-or-self::') : string
    {
    }
    /**
     * @return $this
     */
    public function registerExtension(\Symfony\Component\CssSelector\XPath\Extension\ExtensionInterface $extension) : self
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function getExtension(string $name) : \Symfony\Component\CssSelector\XPath\Extension\ExtensionInterface
    {
    }
    /**
     * @return $this
     */
    public function registerParserShortcut(\Symfony\Component\CssSelector\Parser\ParserInterface $shortcut) : self
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function nodeToXPath(\Symfony\Component\CssSelector\Node\NodeInterface $node) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function addCombination(string $combiner, \Symfony\Component\CssSelector\Node\NodeInterface $xpath, \Symfony\Component\CssSelector\Node\NodeInterface $combinedXpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function addFunction(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function addPseudoClass(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $pseudoClass) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function addAttributeMatching(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $operator, string $attribute, $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
}
