<?php

namespace Symfony\Component\CssSelector\Parser;

/**
 * CSS selector token stream.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class TokenStream
{
    /**
     * Pushes a token.
     *
     * @return $this
     */
    public function push(\Symfony\Component\CssSelector\Parser\Token $token) : self
    {
    }
    /**
     * Freezes stream.
     *
     * @return $this
     */
    public function freeze() : self
    {
    }
    /**
     * Returns next token.
     *
     * @throws InternalErrorException If there is no more token
     */
    public function getNext() : \Symfony\Component\CssSelector\Parser\Token
    {
    }
    /**
     * Returns peeked token.
     */
    public function getPeek() : \Symfony\Component\CssSelector\Parser\Token
    {
    }
    /**
     * Returns used tokens.
     *
     * @return Token[]
     */
    public function getUsed() : array
    {
    }
    /**
     * Returns nex identifier token.
     *
     * @return string The identifier token value
     *
     * @throws SyntaxErrorException If next token is not an identifier
     */
    public function getNextIdentifier() : string
    {
    }
    /**
     * Returns nex identifier or star delimiter token.
     *
     * @return string|null The identifier token value or null if star found
     *
     * @throws SyntaxErrorException If next token is not an identifier or a star delimiter
     */
    public function getNextIdentifierOrStar() : ?string
    {
    }
    /**
     * Skips next whitespace if any.
     */
    public function skipWhitespace()
    {
    }
}
