<?php

namespace Symfony\Component\CssSelector\Parser;

/**
 * CSS selector reader.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class Reader
{
    public function __construct(string $source)
    {
    }
    public function isEOF() : bool
    {
    }
    public function getPosition() : int
    {
    }
    public function getRemainingLength() : int
    {
    }
    public function getSubstring(int $length, int $offset = 0) : string
    {
    }
    public function getOffset(string $string)
    {
    }
    /**
     * @return array|false
     */
    public function findPattern(string $pattern)
    {
    }
    public function moveForward(int $length)
    {
    }
    public function moveToEnd()
    {
    }
}
