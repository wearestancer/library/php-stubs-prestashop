<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator combination extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class CombinationExtension extends \Symfony\Component\CssSelector\XPath\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getCombinationTranslators() : array
    {
    }
    public function translateDescendant(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\XPath\XPathExpr $combinedXpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateChild(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\XPath\XPathExpr $combinedXpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateDirectAdjacent(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\XPath\XPathExpr $combinedXpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateIndirectAdjacent(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\XPath\XPathExpr $combinedXpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
}
