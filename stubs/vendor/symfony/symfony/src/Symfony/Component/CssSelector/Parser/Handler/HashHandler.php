<?php

namespace Symfony\Component\CssSelector\Parser\Handler;

/**
 * CSS selector comment handler.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class HashHandler implements \Symfony\Component\CssSelector\Parser\Handler\HandlerInterface
{
    public function __construct(\Symfony\Component\CssSelector\Parser\Tokenizer\TokenizerPatterns $patterns, \Symfony\Component\CssSelector\Parser\Tokenizer\TokenizerEscaping $escaping)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\CssSelector\Parser\Reader $reader, \Symfony\Component\CssSelector\Parser\TokenStream $stream) : bool
    {
    }
}
