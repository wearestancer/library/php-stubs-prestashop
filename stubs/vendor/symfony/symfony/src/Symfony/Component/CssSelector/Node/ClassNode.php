<?php

namespace Symfony\Component\CssSelector\Node;

/**
 * Represents a "<selector>.<name>" node.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class ClassNode extends \Symfony\Component\CssSelector\Node\AbstractNode
{
    public function __construct(\Symfony\Component\CssSelector\Node\NodeInterface $selector, string $name)
    {
    }
    public function getSelector() : \Symfony\Component\CssSelector\Node\NodeInterface
    {
    }
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSpecificity() : \Symfony\Component\CssSelector\Node\Specificity
    {
    }
    public function __toString() : string
    {
    }
}
