<?php

namespace Symfony\Component\CssSelector\Node;

/**
 * Represents a combined node.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class CombinedSelectorNode extends \Symfony\Component\CssSelector\Node\AbstractNode
{
    public function __construct(\Symfony\Component\CssSelector\Node\NodeInterface $selector, string $combinator, \Symfony\Component\CssSelector\Node\NodeInterface $subSelector)
    {
    }
    public function getSelector() : \Symfony\Component\CssSelector\Node\NodeInterface
    {
    }
    public function getCombinator() : string
    {
    }
    public function getSubSelector() : \Symfony\Component\CssSelector\Node\NodeInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSpecificity() : \Symfony\Component\CssSelector\Node\Specificity
    {
    }
    public function __toString() : string
    {
    }
}
