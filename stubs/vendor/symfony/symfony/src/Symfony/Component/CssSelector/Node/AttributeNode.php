<?php

namespace Symfony\Component\CssSelector\Node;

/**
 * Represents a "<selector>[<namespace>|<attribute> <operator> <value>]" node.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class AttributeNode extends \Symfony\Component\CssSelector\Node\AbstractNode
{
    public function __construct(\Symfony\Component\CssSelector\Node\NodeInterface $selector, ?string $namespace, string $attribute, string $operator, ?string $value)
    {
    }
    public function getSelector() : \Symfony\Component\CssSelector\Node\NodeInterface
    {
    }
    public function getNamespace() : ?string
    {
    }
    public function getAttribute() : string
    {
    }
    public function getOperator() : string
    {
    }
    public function getValue() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSpecificity() : \Symfony\Component\CssSelector\Node\Specificity
    {
    }
    public function __toString() : string
    {
    }
}
