<?php

namespace Symfony\Component\CssSelector\Node;

/**
 * Represents a "<selector>(::|:)<pseudoElement>" node.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class SelectorNode extends \Symfony\Component\CssSelector\Node\AbstractNode
{
    public function __construct(\Symfony\Component\CssSelector\Node\NodeInterface $tree, string $pseudoElement = null)
    {
    }
    public function getTree() : \Symfony\Component\CssSelector\Node\NodeInterface
    {
    }
    public function getPseudoElement() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSpecificity() : \Symfony\Component\CssSelector\Node\Specificity
    {
    }
    public function __toString() : string
    {
    }
}
