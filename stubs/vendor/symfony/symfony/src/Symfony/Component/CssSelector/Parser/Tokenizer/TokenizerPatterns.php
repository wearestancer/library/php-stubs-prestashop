<?php

namespace Symfony\Component\CssSelector\Parser\Tokenizer;

/**
 * CSS selector tokenizer patterns builder.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class TokenizerPatterns
{
    public function __construct()
    {
    }
    public function getNewLineEscapePattern() : string
    {
    }
    public function getSimpleEscapePattern() : string
    {
    }
    public function getUnicodeEscapePattern() : string
    {
    }
    public function getIdentifierPattern() : string
    {
    }
    public function getHashPattern() : string
    {
    }
    public function getNumberPattern() : string
    {
    }
    public function getQuotedStringPattern(string $quote) : string
    {
    }
}
