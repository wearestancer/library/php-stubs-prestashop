<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator HTML extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class HtmlExtension extends \Symfony\Component\CssSelector\XPath\Extension\AbstractExtension
{
    public function __construct(\Symfony\Component\CssSelector\XPath\Translator $translator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPseudoClassTranslators() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFunctionTranslators() : array
    {
    }
    public function translateChecked(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateLink(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateDisabled(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateEnabled(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateLang(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateSelected(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateInvalid(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateHover(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateVisited(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
}
