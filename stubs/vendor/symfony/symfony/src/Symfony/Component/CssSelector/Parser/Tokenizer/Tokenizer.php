<?php

namespace Symfony\Component\CssSelector\Parser\Tokenizer;

/**
 * CSS selector tokenizer.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class Tokenizer
{
    public function __construct()
    {
    }
    /**
     * Tokenize selector source code.
     */
    public function tokenize(\Symfony\Component\CssSelector\Parser\Reader $reader) : \Symfony\Component\CssSelector\Parser\TokenStream
    {
    }
}
