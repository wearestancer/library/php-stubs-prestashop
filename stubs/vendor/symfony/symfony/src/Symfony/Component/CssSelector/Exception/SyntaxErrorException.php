<?php

namespace Symfony\Component\CssSelector\Exception;

/**
 * ParseException is thrown when a CSS selector syntax is not valid.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 */
class SyntaxErrorException extends \Symfony\Component\CssSelector\Exception\ParseException
{
    /**
     * @param string $expectedValue
     *
     * @return self
     */
    public static function unexpectedToken($expectedValue, \Symfony\Component\CssSelector\Parser\Token $foundToken)
    {
    }
    /**
     * @param string $pseudoElement
     * @param string $unexpectedLocation
     *
     * @return self
     */
    public static function pseudoElementFound($pseudoElement, $unexpectedLocation)
    {
    }
    /**
     * @param int $position
     *
     * @return self
     */
    public static function unclosedString($position)
    {
    }
    /**
     * @return self
     */
    public static function nestedNot()
    {
    }
    /**
     * @return self
     */
    public static function stringAsFunctionArgument()
    {
    }
}
