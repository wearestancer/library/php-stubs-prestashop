<?php

namespace Symfony\Component\CssSelector\Parser\Handler;

/**
 * CSS selector whitespace handler.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class WhitespaceHandler implements \Symfony\Component\CssSelector\Parser\Handler\HandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\CssSelector\Parser\Reader $reader, \Symfony\Component\CssSelector\Parser\TokenStream $stream) : bool
    {
    }
}
