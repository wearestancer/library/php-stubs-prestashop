<?php

namespace Symfony\Component\CssSelector\Parser\Shortcut;

/**
 * CSS selector hash parser shortcut.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class HashParser implements \Symfony\Component\CssSelector\Parser\ParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function parse(string $source) : array
    {
    }
}
