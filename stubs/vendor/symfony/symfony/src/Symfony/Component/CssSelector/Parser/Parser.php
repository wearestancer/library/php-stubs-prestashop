<?php

namespace Symfony\Component\CssSelector\Parser;

/**
 * CSS selector parser.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class Parser implements \Symfony\Component\CssSelector\Parser\ParserInterface
{
    public function __construct(\Symfony\Component\CssSelector\Parser\Tokenizer\Tokenizer $tokenizer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function parse(string $source) : array
    {
    }
    /**
     * Parses the arguments for ":nth-child()" and friends.
     *
     * @param Token[] $tokens
     *
     * @throws SyntaxErrorException
     */
    public static function parseSeries(array $tokens) : array
    {
    }
}
