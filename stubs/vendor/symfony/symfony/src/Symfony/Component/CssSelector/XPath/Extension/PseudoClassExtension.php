<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator pseudo-class extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class PseudoClassExtension extends \Symfony\Component\CssSelector\XPath\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getPseudoClassTranslators() : array
    {
    }
    public function translateRoot(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateFirstChild(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateLastChild(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateFirstOfType(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateLastOfType(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateOnlyChild(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateOnlyOfType(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateEmpty(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
}
