<?php

namespace Symfony\Component\CssSelector\XPath;

/**
 * XPath expression translator interface.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class XPathExpr
{
    public function __construct(string $path = '', string $element = '*', string $condition = '', bool $starPrefix = false)
    {
    }
    public function getElement() : string
    {
    }
    public function addCondition(string $condition) : self
    {
    }
    public function getCondition() : string
    {
    }
    public function addNameTest() : self
    {
    }
    public function addStarPrefix() : self
    {
    }
    /**
     * Joins another XPathExpr with a combiner.
     *
     * @return $this
     */
    public function join(string $combiner, self $expr) : self
    {
    }
    public function __toString() : string
    {
    }
}
