<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator function extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class FunctionExtension extends \Symfony\Component\CssSelector\XPath\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctionTranslators() : array
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateNthChild(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function, bool $last = false, bool $addNameTest = true) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateNthLastChild(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateNthOfType(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateNthLastOfType(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateContains(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * @throws ExpressionErrorException
     */
    public function translateLang(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, \Symfony\Component\CssSelector\Node\FunctionNode $function) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
}
