<?php

namespace Symfony\Component\CssSelector\XPath\Extension;

/**
 * XPath expression translator attribute extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class AttributeMatchingExtension extends \Symfony\Component\CssSelector\XPath\Extension\AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getAttributeMatchingTranslators() : array
    {
    }
    public function translateExists(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateEquals(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateIncludes(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateDashMatch(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translatePrefixMatch(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateSuffixMatch(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateSubstringMatch(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    public function translateDifferent(\Symfony\Component\CssSelector\XPath\XPathExpr $xpath, string $attribute, ?string $value) : \Symfony\Component\CssSelector\XPath\XPathExpr
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
}
