<?php

namespace Symfony\Component\Ldap\Adapter;

/**
 * Entry manager interface.
 *
 * @author Charles Sarrazin <charles@sarraz.in>
 * @author Bob van de Vijver <bobvandevijver@hotmail.com>
 * @author Kevin Schuurmans <kevin.schuurmans@freshheads.com>
 *
 * The move() methods must be added to the interface in Symfony 5.0
 *
 * @method void move(Entry $entry, string $newParent) Moves an entry on the Ldap server
 */
interface EntryManagerInterface
{
    /**
     * Adds a new entry in the Ldap server.
     *
     * @throws NotBoundException
     * @throws LdapException
     */
    public function add(\Symfony\Component\Ldap\Entry $entry);
    /**
     * Updates an entry from the Ldap server.
     *
     * @throws NotBoundException
     * @throws LdapException
     */
    public function update(\Symfony\Component\Ldap\Entry $entry);
    /**
     * Renames an entry on the Ldap server.
     *
     * @param string $newRdn
     * @param bool   $removeOldRdn
     */
    public function rename(\Symfony\Component\Ldap\Entry $entry, $newRdn, $removeOldRdn = true);
    /**
     * Removes an entry from the Ldap server.
     *
     * @throws NotBoundException
     * @throws LdapException
     */
    public function remove(\Symfony\Component\Ldap\Entry $entry);
}
