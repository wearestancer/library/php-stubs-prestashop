<?php

namespace Symfony\Component\Ldap\Adapter\ExtLdap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class Connection extends \Symfony\Component\Ldap\Adapter\AbstractConnection
{
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isBound()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $password WARNING: When the LDAP server allows unauthenticated binds, a blank $password will always be valid
     */
    public function bind($dn = null, $password = null)
    {
    }
    /**
     * @return resource|LDAPConnection
     *
     * @internal
     */
    public function getResource()
    {
    }
    public function setOption($name, $value)
    {
    }
    public function getOption($name)
    {
    }
    protected function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
