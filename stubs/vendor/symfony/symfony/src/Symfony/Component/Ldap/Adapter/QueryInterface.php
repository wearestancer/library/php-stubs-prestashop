<?php

namespace Symfony\Component\Ldap\Adapter;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 * @author Bob van de Vijver <bobvandevijver@hotmail.com>
 */
interface QueryInterface
{
    public const DEREF_NEVER = 0x0;
    public const DEREF_SEARCHING = 0x1;
    public const DEREF_FINDING = 0x2;
    public const DEREF_ALWAYS = 0x3;
    public const SCOPE_BASE = 'base';
    public const SCOPE_ONE = 'one';
    public const SCOPE_SUB = 'sub';
    /**
     * Executes a query and returns the list of Ldap entries.
     *
     * @return CollectionInterface|Entry[]
     *
     * @throws NotBoundException
     * @throws LdapException
     */
    public function execute();
}
