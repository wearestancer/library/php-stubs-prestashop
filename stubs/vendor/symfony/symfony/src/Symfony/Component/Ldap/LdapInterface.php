<?php

namespace Symfony\Component\Ldap;

/**
 * Ldap interface.
 *
 * @author Charles Sarrazin <charles@sarraz.in>
 */
interface LdapInterface
{
    public const ESCAPE_FILTER = 0x1;
    public const ESCAPE_DN = 0x2;
    /**
     * Return a connection bound to the ldap.
     *
     * @param string $dn       A LDAP dn
     * @param string $password A password
     *
     * @throws ConnectionException if dn / password could not be bound
     */
    public function bind($dn = null, $password = null);
    /**
     * Queries a ldap server for entries matching the given criteria.
     *
     * @param string $dn
     * @param string $query
     *
     * @return QueryInterface
     */
    public function query($dn, $query, array $options = []);
    /**
     * @return EntryManagerInterface
     */
    public function getEntryManager();
    /**
     * Escape a string for use in an LDAP filter or DN.
     *
     * @param string $subject
     * @param string $ignore
     * @param int    $flags
     *
     * @return string
     */
    public function escape($subject, $ignore = '', $flags = 0);
}
