<?php

namespace Symfony\Component\Ldap\Adapter\ExtLdap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class Adapter implements \Symfony\Component\Ldap\Adapter\AdapterInterface
{
    public function __construct(array $config = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEntryManager()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createQuery($dn, $query, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function escape($subject, $ignore = '', $flags = 0)
    {
    }
}
