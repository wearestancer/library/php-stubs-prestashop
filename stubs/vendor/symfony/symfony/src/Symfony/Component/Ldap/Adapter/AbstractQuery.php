<?php

namespace Symfony\Component\Ldap\Adapter;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
abstract class AbstractQuery implements \Symfony\Component\Ldap\Adapter\QueryInterface
{
    protected $connection;
    protected $dn;
    protected $query;
    protected $options;
    public function __construct(\Symfony\Component\Ldap\Adapter\ConnectionInterface $connection, string $dn, string $query, array $options = [])
    {
    }
}
