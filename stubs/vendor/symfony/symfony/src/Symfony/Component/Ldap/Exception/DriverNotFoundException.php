<?php

namespace Symfony\Component\Ldap\Exception;

/**
 * LdapException is thrown if php ldap module is not loaded.
 *
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class DriverNotFoundException extends \RuntimeException implements \Symfony\Component\Ldap\Exception\ExceptionInterface
{
}
