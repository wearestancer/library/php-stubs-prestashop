<?php

namespace Symfony\Component\Ldap\Adapter\ExtLdap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 * @author Bob van de Vijver <bobvandevijver@hotmail.com>
 */
class EntryManager implements \Symfony\Component\Ldap\Adapter\EntryManagerInterface
{
    public function __construct(\Symfony\Component\Ldap\Adapter\ExtLdap\Connection $connection)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add(\Symfony\Component\Ldap\Entry $entry)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update(\Symfony\Component\Ldap\Entry $entry)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove(\Symfony\Component\Ldap\Entry $entry)
    {
    }
    /**
     * Adds values to an entry's multi-valued attribute from the LDAP server.
     *
     * @throws NotBoundException
     * @throws LdapException
     */
    public function addAttributeValues(\Symfony\Component\Ldap\Entry $entry, string $attribute, array $values)
    {
    }
    /**
     * Removes values from an entry's multi-valued attribute from the LDAP server.
     *
     * @throws NotBoundException
     * @throws LdapException
     */
    public function removeAttributeValues(\Symfony\Component\Ldap\Entry $entry, string $attribute, array $values)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rename(\Symfony\Component\Ldap\Entry $entry, $newRdn, $removeOldRdn = true)
    {
    }
    /**
     * Moves an entry on the Ldap server.
     *
     * @throws NotBoundException if the connection has not been previously bound
     * @throws LdapException     if an error is thrown during the rename operation
     */
    public function move(\Symfony\Component\Ldap\Entry $entry, string $newParent)
    {
    }
    /**
     * @param iterable|UpdateOperation[] $operations An array or iterable of UpdateOperation instances
     *
     * @throws UpdateOperationException in case of an error
     */
    public function applyOperations(string $dn, iterable $operations) : void
    {
    }
}
