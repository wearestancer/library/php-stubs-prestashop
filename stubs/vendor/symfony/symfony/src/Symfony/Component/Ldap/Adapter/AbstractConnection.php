<?php

namespace Symfony\Component\Ldap\Adapter;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
abstract class AbstractConnection implements \Symfony\Component\Ldap\Adapter\ConnectionInterface
{
    protected $config;
    public function __construct(array $config = [])
    {
    }
    protected function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
