<?php

namespace Symfony\Component\Ldap\Adapter\ExtLdap;

class UpdateOperation
{
    /**
     * @param int    $operationType An LDAP_MODIFY_BATCH_* constant
     * @param string $attribute     The attribute to batch modify on
     *
     * @throws UpdateOperationException on consistency errors during construction
     */
    public function __construct(int $operationType, string $attribute, ?array $values)
    {
    }
    public function toArray() : array
    {
    }
}
