<?php

namespace Symfony\Component\Ldap\Security;

/**
 * LdapUserProvider is a simple user provider on top of LDAP.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Charles Sarrazin <charles@sarraz.in>
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
class LdapUserProvider implements \Symfony\Component\Security\Core\User\UserProviderInterface, \Symfony\Component\Security\Core\User\PasswordUpgraderInterface
{
    public function __construct(\Symfony\Component\Ldap\LdapInterface $ldap, string $baseDn, string $searchDn = null, string $searchPassword = null, array $defaultRoles = [], string $uidKey = null, string $filter = null, string $passwordAttribute = null, array $extraFields = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function upgradePassword(\Symfony\Component\Security\Core\User\UserInterface $user, string $newEncodedPassword) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
    }
    /**
     * Loads a user from an LDAP entry.
     *
     * @return UserInterface
     */
    protected function loadUser($username, \Symfony\Component\Ldap\Entry $entry)
    {
    }
}
