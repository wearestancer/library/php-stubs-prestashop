<?php

namespace Symfony\Component\Ldap\Adapter\ExtLdap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 * @author Bob van de Vijver <bobvandevijver@hotmail.com>
 */
class Query extends \Symfony\Component\Ldap\Adapter\AbstractQuery
{
    // As of PHP 7.2, we can use LDAP_CONTROL_PAGEDRESULTS instead of this
    public const PAGINATION_OID = '1.2.840.113556.1.4.319';
    /** @var Connection */
    protected $connection;
    public function __construct(\Symfony\Component\Ldap\Adapter\ExtLdap\Connection $connection, string $dn, string $query, array $options = [])
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function execute()
    {
    }
    /**
     * Returns an LDAP search resource. If this query resulted in multiple searches, only the first
     * page will be returned.
     *
     * @return resource|Result
     *
     * @internal
     */
    public function getResource($idx = 0)
    {
    }
    /**
     * Returns all LDAP search resources.
     *
     * @return resource[]|Result[]
     *
     * @internal
     */
    public function getResources() : array
    {
    }
}
