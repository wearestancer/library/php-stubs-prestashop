<?php

namespace Symfony\Component\Ldap\Security;

/**
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @final
 */
class LdapUser implements \Symfony\Component\Security\Core\User\UserInterface, \Symfony\Component\Security\Core\User\EquatableInterface
{
    public function __construct(\Symfony\Component\Ldap\Entry $entry, string $username, ?string $password, array $roles = [], array $extraFields = [])
    {
    }
    public function getEntry() : \Symfony\Component\Ldap\Entry
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoles() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPassword() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSalt() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUsername() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }
    public function getExtraFields() : array
    {
    }
    public function setPassword(string $password)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isEqualTo(\Symfony\Component\Security\Core\User\UserInterface $user) : bool
    {
    }
}
