<?php

namespace Symfony\Component\Ldap\Exception;

/**
 * LdapException is thrown if php ldap module is not loaded.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class LdapException extends \RuntimeException implements \Symfony\Component\Ldap\Exception\ExceptionInterface
{
}
