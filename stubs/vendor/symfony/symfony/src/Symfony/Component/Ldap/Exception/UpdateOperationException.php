<?php

namespace Symfony\Component\Ldap\Exception;

class UpdateOperationException extends \Symfony\Component\Ldap\Exception\LdapException
{
}
