<?php

namespace Symfony\Component\Ldap\Adapter\ExtLdap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class Collection implements \Symfony\Component\Ldap\Adapter\CollectionInterface
{
    public function __construct(\Symfony\Component\Ldap\Adapter\ExtLdap\Connection $connection, \Symfony\Component\Ldap\Adapter\ExtLdap\Query $search)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * @return \Traversable
     */
    public function getIterator()
    {
    }
    /**
     * @return bool
     */
    public function offsetExists($offset)
    {
    }
    /**
     * @return mixed
     */
    public function offsetGet($offset)
    {
    }
    /**
     * @return void
     */
    public function offsetSet($offset, $value)
    {
    }
    /**
     * @return void
     */
    public function offsetUnset($offset)
    {
    }
}
