<?php

namespace Symfony\Component\Ldap\Exception;

/**
 * ConnectionException is thrown if binding to ldap can not be established.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class ConnectionException extends \RuntimeException implements \Symfony\Component\Ldap\Exception\ExceptionInterface
{
}
