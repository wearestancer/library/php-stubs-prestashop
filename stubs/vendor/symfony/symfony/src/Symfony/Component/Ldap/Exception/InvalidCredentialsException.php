<?php

namespace Symfony\Component\Ldap\Exception;

/**
 * InvalidCredentialsException is thrown if binding to ldap has been done with invalid credentials.
 *
 * @author Hamza Amrouche <hamza.simperfit@gmail.com>
 */
class InvalidCredentialsException extends \Symfony\Component\Ldap\Exception\ConnectionException
{
}
