<?php

namespace Symfony\Component\Ldap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
final class Ldap implements \Symfony\Component\Ldap\LdapInterface
{
    public function __construct(\Symfony\Component\Ldap\Adapter\AdapterInterface $adapter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function bind($dn = null, $password = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function query($dn, $query, array $options = []) : \Symfony\Component\Ldap\Adapter\QueryInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEntryManager() : \Symfony\Component\Ldap\Adapter\EntryManagerInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function escape($subject, $ignore = '', $flags = 0) : string
    {
    }
    /**
     * Creates a new Ldap instance.
     *
     * @param string $adapter The adapter name
     * @param array  $config  The adapter's configuration
     *
     * @return static
     */
    public static function create($adapter, array $config = []) : self
    {
    }
}
