<?php

namespace Symfony\Component\Ldap;

/**
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class Entry
{
    public function __construct(string $dn, array $attributes = [])
    {
    }
    /**
     * Returns the entry's DN.
     *
     * @return string
     */
    public function getDn()
    {
    }
    /**
     * Returns whether an attribute exists.
     *
     * @param string $name The name of the attribute
     *
     * @return bool
     */
    public function hasAttribute($name)
    {
    }
    /**
     * Returns a specific attribute's value.
     *
     * As LDAP can return multiple values for a single attribute,
     * this value is returned as an array.
     *
     * @param string $name The name of the attribute
     *
     * @return array|null
     */
    public function getAttribute($name)
    {
    }
    /**
     * Returns the complete list of attributes.
     *
     * @return array
     */
    public function getAttributes()
    {
    }
    /**
     * Sets a value for the given attribute.
     *
     * @param string $name
     */
    public function setAttribute($name, array $value)
    {
    }
    /**
     * Removes a given attribute.
     *
     * @param string $name
     */
    public function removeAttribute($name)
    {
    }
}
