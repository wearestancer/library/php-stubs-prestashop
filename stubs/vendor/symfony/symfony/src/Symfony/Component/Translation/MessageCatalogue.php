<?php

namespace Symfony\Component\Translation;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MessageCatalogue implements \Symfony\Component\Translation\MessageCatalogueInterface, \Symfony\Component\Translation\MetadataAwareInterface
{
    /**
     * @param array $messages An array of messages classified by domain
     */
    public function __construct(?string $locale, array $messages = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDomains()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all($domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($id, $translation, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($id, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function defines($id, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($id, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function replace($messages, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add($messages, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addCatalogue(\Symfony\Component\Translation\MessageCatalogueInterface $catalogue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addFallbackCatalogue(\Symfony\Component\Translation\MessageCatalogueInterface $catalogue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFallbackCatalogue()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResources()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addResource(\Symfony\Component\Config\Resource\ResourceInterface $resource)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadata($key = '', $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMetadata($key, $value, $domain = 'messages')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function deleteMetadata($key = '', $domain = 'messages')
    {
    }
}
