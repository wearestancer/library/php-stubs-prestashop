<?php

namespace Symfony\Component\Translation\Extractor;

/**
 * PhpExtractor extracts translation messages from a PHP template.
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 */
class PhpExtractor extends \Symfony\Component\Translation\Extractor\AbstractFileExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    public const MESSAGE_TOKEN = 300;
    public const METHOD_ARGUMENTS_TOKEN = 1000;
    public const DOMAIN_TOKEN = 1001;
    /**
     * The sequence that captures translation messages.
     *
     * @var array
     */
    protected $sequences = [['->', 'trans', '(', self::MESSAGE_TOKEN, ',', self::METHOD_ARGUMENTS_TOKEN, ',', self::DOMAIN_TOKEN], ['->', 'transChoice', '(', self::MESSAGE_TOKEN, ',', self::METHOD_ARGUMENTS_TOKEN, ',', self::METHOD_ARGUMENTS_TOKEN, ',', self::DOMAIN_TOKEN], ['->', 'trans', '(', self::MESSAGE_TOKEN], ['->', 'transChoice', '(', self::MESSAGE_TOKEN]];
    /**
     * {@inheritdoc}
     */
    public function extract($resource, \Symfony\Component\Translation\MessageCatalogue $catalog)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
    }
    /**
     * Normalizes a token.
     *
     * @param mixed $token
     *
     * @return string|null
     */
    protected function normalizeToken($token)
    {
    }
    /**
     * Extracts trans message from PHP tokens.
     *
     * @param array  $tokens
     * @param string $filename
     */
    protected function parseTokens($tokens, \Symfony\Component\Translation\MessageCatalogue $catalog)
    {
    }
    /**
     * @param string $file
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    protected function canBeExtracted($file)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractFromDirectory($directory)
    {
    }
}
