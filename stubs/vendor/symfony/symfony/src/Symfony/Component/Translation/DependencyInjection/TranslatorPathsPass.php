<?php

namespace Symfony\Component\Translation\DependencyInjection;

/**
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class TranslatorPathsPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    public function __construct(string $translatorServiceId = 'translator', string $debugCommandServiceId = 'console.command.translation_debug', string $updateCommandServiceId = 'console.command.translation_update', string $resolverServiceId = 'argument_resolver.service')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    protected function processValue($value, $isRoot = false)
    {
    }
}
