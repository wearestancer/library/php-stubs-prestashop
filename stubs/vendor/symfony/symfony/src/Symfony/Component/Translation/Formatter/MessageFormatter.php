<?php

namespace Symfony\Component\Translation\Formatter;

/**
 * @author Abdellatif Ait boudad <a.aitboudad@gmail.com>
 */
class MessageFormatter implements \Symfony\Component\Translation\Formatter\MessageFormatterInterface, \Symfony\Component\Translation\Formatter\IntlFormatterInterface, \Symfony\Component\Translation\Formatter\ChoiceMessageFormatterInterface
{
    /**
     * @param TranslatorInterface|null $translator An identity translator to use as selector for pluralization
     */
    public function __construct($translator = null, \Symfony\Component\Translation\Formatter\IntlFormatterInterface $intlFormatter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function format($message, $locale, array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function formatIntl(string $message, string $locale, array $parameters = []) : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2, use format() with a %count% parameter instead
     */
    public function choiceFormat($message, $number, $locale, array $parameters = [])
    {
    }
}
