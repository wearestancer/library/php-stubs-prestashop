<?php

namespace Symfony\Component\Translation;

/**
 * @author Abdellatif Ait boudad <a.aitboudad@gmail.com>
 */
class LoggingTranslator implements \Symfony\Contracts\Translation\TranslatorInterface, \Symfony\Component\Translation\TranslatorInterface, \Symfony\Component\Translation\TranslatorBagInterface
{
    /**
     * @param TranslatorInterface $translator The translator must implement TranslatorBagInterface
     */
    public function __construct($translator, \Psr\Log\LoggerInterface $logger)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2, use the trans() method instead with a %count% parameter
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCatalogue($locale = null)
    {
    }
    /**
     * Gets the fallback locales.
     *
     * @return array The fallback locales
     */
    public function getFallbackLocales()
    {
    }
    /**
     * Passes through all unknown calls onto the translator object.
     */
    public function __call($method, $args)
    {
    }
}
