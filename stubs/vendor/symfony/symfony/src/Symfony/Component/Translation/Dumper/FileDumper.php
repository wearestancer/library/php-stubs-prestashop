<?php

namespace Symfony\Component\Translation\Dumper;

/**
 * FileDumper is an implementation of DumperInterface that dump a message catalogue to file(s).
 *
 * Options:
 * - path (mandatory): the directory where the files should be saved
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 */
abstract class FileDumper implements \Symfony\Component\Translation\Dumper\DumperInterface
{
    /**
     * A template for the relative paths to files.
     *
     * @var string
     */
    protected $relativePathTemplate = '%domain%.%locale%.%extension%';
    /**
     * Sets the template for the relative paths to files.
     *
     * @param string $relativePathTemplate A template for the relative paths to files
     */
    public function setRelativePathTemplate($relativePathTemplate)
    {
    }
    /**
     * Sets backup flag.
     *
     * @param bool $backup
     *
     * @deprecated since Symfony 4.1
     */
    public function setBackup($backup)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dump(\Symfony\Component\Translation\MessageCatalogue $messages, $options = [])
    {
    }
    /**
     * Transforms a domain of a message catalogue to its string representation.
     *
     * @param string $domain
     *
     * @return string representation
     */
    public abstract function formatCatalogue(\Symfony\Component\Translation\MessageCatalogue $messages, $domain, array $options = []);
    /**
     * Gets the file extension of the dumper.
     *
     * @return string file extension
     */
    protected abstract function getExtension();
}
