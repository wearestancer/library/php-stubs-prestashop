<?php

namespace Symfony\Component\Translation\Extractor;

/**
 * ChainExtractor extracts translation messages from template files.
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 */
class ChainExtractor implements \Symfony\Component\Translation\Extractor\ExtractorInterface
{
    /**
     * Adds a loader to the translation extractor.
     *
     * @param string $format The format of the loader
     */
    public function addExtractor($format, \Symfony\Component\Translation\Extractor\ExtractorInterface $extractor)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extract($directory, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
}
