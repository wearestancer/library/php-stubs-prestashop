<?php

namespace Symfony\Component\Translation\Extractor;

/**
 * Base class used by classes that extract translation messages from files.
 *
 * @author Marcos D. Sánchez <marcosdsanchez@gmail.com>
 */
abstract class AbstractFileExtractor
{
    /**
     * @param string|iterable $resource Files, a file or a directory
     *
     * @return iterable
     */
    protected function extractFiles($resource)
    {
    }
    /**
     * @param string $file
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    protected function isFile($file)
    {
    }
    /**
     * @param string $file
     *
     * @return bool
     */
    protected abstract function canBeExtracted($file);
    /**
     * @param string|array $resource Files, a file or a directory
     *
     * @return iterable files to be extracted
     */
    protected abstract function extractFromDirectory($resource);
}
