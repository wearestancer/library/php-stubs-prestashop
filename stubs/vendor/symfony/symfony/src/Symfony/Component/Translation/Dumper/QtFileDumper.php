<?php

namespace Symfony\Component\Translation\Dumper;

/**
 * QtFileDumper generates ts files from a message catalogue.
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 */
class QtFileDumper extends \Symfony\Component\Translation\Dumper\FileDumper
{
    /**
     * {@inheritdoc}
     */
    public function formatCatalogue(\Symfony\Component\Translation\MessageCatalogue $messages, $domain, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getExtension()
    {
    }
}
