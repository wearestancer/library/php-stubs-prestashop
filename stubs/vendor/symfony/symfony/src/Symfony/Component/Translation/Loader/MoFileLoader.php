<?php

namespace Symfony\Component\Translation\Loader;

/**
 * @copyright Copyright (c) 2010, Union of RAD http://union-of-rad.org (http://lithify.me/)
 */
class MoFileLoader extends \Symfony\Component\Translation\Loader\FileLoader
{
    /**
     * Magic used for validating the format of an MO file as well as
     * detecting if the machine used to create that file was little endian.
     */
    public const MO_LITTLE_ENDIAN_MAGIC = 0x950412de;
    /**
     * Magic used for validating the format of an MO file as well as
     * detecting if the machine used to create that file was big endian.
     */
    public const MO_BIG_ENDIAN_MAGIC = 0xde120495;
    /**
     * The size of the header of an MO file in bytes.
     */
    public const MO_HEADER_SIZE = 28;
    /**
     * Parses machine object (MO) format, independent of the machine's endian it
     * was created on. Both 32bit and 64bit systems are supported.
     *
     * {@inheritdoc}
     */
    protected function loadResource($resource)
    {
    }
}
