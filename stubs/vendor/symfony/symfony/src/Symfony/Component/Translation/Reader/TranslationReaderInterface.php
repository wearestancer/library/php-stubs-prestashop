<?php

namespace Symfony\Component\Translation\Reader;

/**
 * TranslationReader reads translation messages from translation files.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
interface TranslationReaderInterface
{
    /**
     * Reads translation messages from a directory to the catalogue.
     *
     * @param string $directory
     */
    public function read($directory, \Symfony\Component\Translation\MessageCatalogue $catalogue);
}
