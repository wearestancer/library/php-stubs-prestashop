<?php

namespace Symfony\Component\Translation\Loader;

/**
 * IcuResFileLoader loads translations from a resource bundle.
 *
 * @author stealth35
 */
class IcuDatFileLoader extends \Symfony\Component\Translation\Loader\IcuResFileLoader
{
    /**
     * {@inheritdoc}
     */
    public function load($resource, $locale, $domain = 'messages')
    {
    }
}
