<?php

namespace Symfony\Component\Translation\DependencyInjection;

/**
 * Adds tagged translation.extractor services to translation extractor.
 */
class TranslationExtractorPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $extractorServiceId = 'translation.extractor', string $extractorTag = 'translation.extractor')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
