<?php

namespace Symfony\Component\Translation\Writer;

/**
 * TranslationWriter writes translation messages.
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 */
class TranslationWriter implements \Symfony\Component\Translation\Writer\TranslationWriterInterface
{
    /**
     * Adds a dumper to the writer.
     *
     * @param string $format The format of the dumper
     */
    public function addDumper($format, \Symfony\Component\Translation\Dumper\DumperInterface $dumper)
    {
    }
    /**
     * Disables dumper backup.
     *
     * @deprecated since Symfony 4.1
     */
    public function disableBackup()
    {
    }
    /**
     * Obtains the list of supported formats.
     *
     * @return array
     */
    public function getFormats()
    {
    }
    /**
     * Writes translation from the catalogue according to the selected format.
     *
     * @param string $format  The format to use to dump the messages
     * @param array  $options Options that are passed to the dumper
     *
     * @throws InvalidArgumentException
     */
    public function write(\Symfony\Component\Translation\MessageCatalogue $catalogue, $format, $options = [])
    {
    }
}
