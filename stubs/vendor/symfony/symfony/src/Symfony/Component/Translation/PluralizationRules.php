<?php

namespace Symfony\Component\Translation;

/**
 * Returns the plural rules for a given locale.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.2, use IdentityTranslator instead
 */
class PluralizationRules
{
    /**
     * Returns the plural position to use for the given locale and number.
     *
     * @param float  $number The number
     * @param string $locale The locale
     *
     * @return int The plural position
     */
    public static function get($number, $locale)
    {
    }
    /**
     * Overrides the default plural rule for a given locale.
     *
     * @param callable $rule   A PHP callable
     * @param string   $locale The locale
     */
    public static function set(callable $rule, $locale)
    {
    }
}
