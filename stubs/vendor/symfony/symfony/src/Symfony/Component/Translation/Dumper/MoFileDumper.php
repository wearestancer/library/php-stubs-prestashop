<?php

namespace Symfony\Component\Translation\Dumper;

/**
 * MoFileDumper generates a gettext formatted string representation of a message catalogue.
 *
 * @author Stealth35
 */
class MoFileDumper extends \Symfony\Component\Translation\Dumper\FileDumper
{
    /**
     * {@inheritdoc}
     */
    public function formatCatalogue(\Symfony\Component\Translation\MessageCatalogue $messages, $domain, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getExtension()
    {
    }
}
