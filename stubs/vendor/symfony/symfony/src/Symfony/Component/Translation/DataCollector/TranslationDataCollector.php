<?php

namespace Symfony\Component\Translation\DataCollector;

/**
 * @author Abdellatif Ait boudad <a.aitboudad@gmail.com>
 *
 * @final since Symfony 4.4
 */
class TranslationDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct(\Symfony\Component\Translation\DataCollectorTranslator $translator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * @return array|Data
     */
    public function getMessages()
    {
    }
    /**
     * @return int
     */
    public function getCountMissings()
    {
    }
    /**
     * @return int
     */
    public function getCountFallbacks()
    {
    }
    /**
     * @return int
     */
    public function getCountDefines()
    {
    }
    public function getLocale()
    {
    }
    /**
     * @internal since Symfony 4.2
     */
    public function getFallbackLocales()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
