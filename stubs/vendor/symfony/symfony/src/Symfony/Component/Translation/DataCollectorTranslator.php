<?php

namespace Symfony\Component\Translation;

/**
 * @author Abdellatif Ait boudad <a.aitboudad@gmail.com>
 */
class DataCollectorTranslator implements \Symfony\Component\Translation\TranslatorInterface, \Symfony\Contracts\Translation\TranslatorInterface, \Symfony\Component\Translation\TranslatorBagInterface, \Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface
{
    public const MESSAGE_DEFINED = 0;
    public const MESSAGE_MISSING = 1;
    public const MESSAGE_EQUALS_FALLBACK = 2;
    /**
     * @param TranslatorInterface $translator The translator must implement TranslatorBagInterface
     */
    public function __construct($translator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2, use the trans() method instead with a %count% parameter
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCatalogue($locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * Gets the fallback locales.
     *
     * @return array The fallback locales
     */
    public function getFallbackLocales()
    {
    }
    /**
     * Passes through all unknown calls onto the translator object.
     */
    public function __call($method, $args)
    {
    }
    /**
     * @return array
     */
    public function getCollectedMessages()
    {
    }
}
