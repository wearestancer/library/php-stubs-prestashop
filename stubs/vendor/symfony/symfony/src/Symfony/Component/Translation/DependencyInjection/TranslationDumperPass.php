<?php

namespace Symfony\Component\Translation\DependencyInjection;

/**
 * Adds tagged translation.formatter services to translation writer.
 */
class TranslationDumperPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $writerServiceId = 'translation.writer', string $dumperTag = 'translation.dumper')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
