<?php

namespace Symfony\Component\Translation\Loader;

/**
 * IniFileLoader loads translations from an ini file.
 *
 * @author stealth35
 */
class IniFileLoader extends \Symfony\Component\Translation\Loader\FileLoader
{
    /**
     * {@inheritdoc}
     */
    protected function loadResource($resource)
    {
    }
}
