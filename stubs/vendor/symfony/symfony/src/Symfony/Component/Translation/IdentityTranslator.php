<?php

namespace Symfony\Component\Translation;

/**
 * IdentityTranslator does not translate anything.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class IdentityTranslator implements \Symfony\Component\Translation\TranslatorInterface, \Symfony\Contracts\Translation\TranslatorInterface
{
    use \Symfony\Contracts\Translation\TranslatorTrait {
        trans as private doTrans;
        setLocale as private doSetLocale;
    }
    public function __construct(\Symfony\Component\Translation\MessageSelector $selector = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2, use the trans() method instead with a %count% parameter
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
    }
}
