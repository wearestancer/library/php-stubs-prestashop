<?php

namespace Symfony\Component\Translation;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Translator implements \Symfony\Component\Translation\TranslatorInterface, \Symfony\Contracts\Translation\TranslatorInterface, \Symfony\Component\Translation\TranslatorBagInterface
{
    /**
     * @var MessageCatalogueInterface[]
     */
    protected $catalogues = [];
    /**
     * @throws InvalidArgumentException If a locale contains invalid characters
     */
    public function __construct(?string $locale, \Symfony\Component\Translation\Formatter\MessageFormatterInterface $formatter = null, string $cacheDir = null, bool $debug = false, array $cacheVary = [])
    {
    }
    public function setConfigCacheFactory(\Symfony\Component\Config\ConfigCacheFactoryInterface $configCacheFactory)
    {
    }
    /**
     * Adds a Loader.
     *
     * @param string $format The name of the loader (@see addResource())
     */
    public function addLoader($format, \Symfony\Component\Translation\Loader\LoaderInterface $loader)
    {
    }
    /**
     * Adds a Resource.
     *
     * @param string $format   The name of the loader (@see addLoader())
     * @param mixed  $resource The resource name
     * @param string $locale   The locale
     * @param string $domain   The domain
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    public function addResource($format, $resource, $locale, $domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
    }
    /**
     * Sets the fallback locales.
     *
     * @throws InvalidArgumentException If a locale contains invalid characters
     */
    public function setFallbackLocales(array $locales)
    {
    }
    /**
     * Gets the fallback locales.
     *
     * @internal since Symfony 4.2
     *
     * @return array The fallback locales
     */
    public function getFallbackLocales()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2, use the trans() method instead with a %count% parameter
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCatalogue($locale = null)
    {
    }
    /**
     * Gets the loaders.
     *
     * @return array LoaderInterface[]
     */
    protected function getLoaders()
    {
    }
    /**
     * @param string $locale
     */
    protected function loadCatalogue($locale)
    {
    }
    /**
     * @param string $locale
     */
    protected function initializeCatalogue($locale)
    {
    }
    /**
     * @internal
     */
    protected function doLoadCatalogue(string $locale) : void
    {
    }
    protected function computeFallbackLocales($locale)
    {
    }
    /**
     * Asserts that the locale is valid, throws an Exception if not.
     *
     * @param string $locale Locale to tests
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    protected function assertValidLocale($locale)
    {
    }
}
