<?php

namespace Symfony\Component\Translation\Reader;

/**
 * TranslationReader reads translation messages from translation files.
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 */
class TranslationReader implements \Symfony\Component\Translation\Reader\TranslationReaderInterface
{
    /**
     * Adds a loader to the translation extractor.
     *
     * @param string $format The format of the loader
     */
    public function addLoader($format, \Symfony\Component\Translation\Loader\LoaderInterface $loader)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function read($directory, \Symfony\Component\Translation\MessageCatalogue $catalogue)
    {
    }
}
