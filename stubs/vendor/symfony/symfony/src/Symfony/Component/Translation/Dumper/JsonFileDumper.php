<?php

namespace Symfony\Component\Translation\Dumper;

/**
 * JsonFileDumper generates an json formatted string representation of a message catalogue.
 *
 * @author singles
 */
class JsonFileDumper extends \Symfony\Component\Translation\Dumper\FileDumper
{
    /**
     * {@inheritdoc}
     */
    public function formatCatalogue(\Symfony\Component\Translation\MessageCatalogue $messages, $domain, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getExtension()
    {
    }
}
