<?php

function findTranslationFiles($originalFilePath, $localeToAnalyze)
{
}
function calculateTranslationStatus($originalFilePath, $translationFilePaths)
{
}
function isTranslationCompleted(array $translationStatus) : bool
{
}
function printTranslationStatus($originalFilePath, $translationStatus, $verboseOutput, $includeCompletedLanguages)
{
}
function extractLocaleFromFilePath($filePath)
{
}
function extractTranslationKeys($filePath)
{
}
/**
 * Check whether the trans-unit id and source match with the base translation.
 */
function findTransUnitMismatches(array $baseTranslationKeys, array $translatedKeys) : array
{
}
function printTitle($title)
{
}
function printTable($translations, $verboseOutput, bool $includeCompletedLanguages)
{
}
function textColorGreen()
{
}
function textColorRed()
{
}
function textColorNormal()
{
}
