<?php

namespace Symfony\Component\Translation\Catalogue;

/**
 * Base catalogues binary operation class.
 *
 * A catalogue binary operation performs operation on
 * source (the left argument) and target (the right argument) catalogues.
 *
 * @author Jean-François Simon <contact@jfsimon.fr>
 */
abstract class AbstractOperation implements \Symfony\Component\Translation\Catalogue\OperationInterface
{
    protected $source;
    protected $target;
    protected $result;
    /**
     * This array stores 'all', 'new' and 'obsolete' messages for all valid domains.
     *
     * The data structure of this array is as follows:
     *
     *     [
     *         'domain 1' => [
     *             'all' => [...],
     *             'new' => [...],
     *             'obsolete' => [...]
     *         ],
     *         'domain 2' => [
     *             'all' => [...],
     *             'new' => [...],
     *             'obsolete' => [...]
     *         ],
     *         ...
     *     ]
     *
     * @var array The array that stores 'all', 'new' and 'obsolete' messages
     */
    protected $messages;
    /**
     * @throws LogicException
     */
    public function __construct(\Symfony\Component\Translation\MessageCatalogueInterface $source, \Symfony\Component\Translation\MessageCatalogueInterface $target)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDomains()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessages($domain)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNewMessages($domain)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getObsoleteMessages($domain)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResult()
    {
    }
    /**
     * Performs operation on source and target catalogues for the given domain and
     * stores the results.
     *
     * @param string $domain The domain which the operation will be performed for
     */
    protected abstract function processDomain($domain);
}
