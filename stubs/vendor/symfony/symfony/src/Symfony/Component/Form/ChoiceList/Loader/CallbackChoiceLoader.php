<?php

namespace Symfony\Component\Form\ChoiceList\Loader;

/**
 * Loads an {@link ArrayChoiceList} instance from a callable returning an array of choices.
 *
 * @author Jules Pietri <jules@heahprod.com>
 */
class CallbackChoiceLoader implements \Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface
{
    /**
     * @param callable $callback The callable returning an array of choices
     */
    public function __construct(callable $callback)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadChoiceList($value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadChoicesForValues(array $values, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadValuesForChoices(array $choices, $value = null)
    {
    }
}
