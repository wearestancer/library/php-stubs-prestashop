<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ValueToDuplicatesTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(array $keys)
    {
    }
    /**
     * Duplicates the given value through the array.
     *
     * @param mixed $value The value
     *
     * @return array The array
     */
    public function transform($value)
    {
    }
    /**
     * Extracts the duplicated value from an array.
     *
     * @return mixed The value
     *
     * @throws TransformationFailedException if the given value is not an array or
     *                                       if the given array can not be transformed
     */
    public function reverseTransform($array)
    {
    }
}
