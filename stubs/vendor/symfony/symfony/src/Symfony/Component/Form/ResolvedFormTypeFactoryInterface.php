<?php

namespace Symfony\Component\Form;

/**
 * Creates ResolvedFormTypeInterface instances.
 *
 * This interface allows you to use your custom ResolvedFormTypeInterface
 * implementation, within which you can customize the concrete FormBuilderInterface
 * implementations or FormView subclasses that are used by the framework.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface ResolvedFormTypeFactoryInterface
{
    /**
     * Resolves a form type.
     *
     * @param FormTypeExtensionInterface[] $typeExtensions
     *
     * @return ResolvedFormTypeInterface
     *
     * @throws Exception\UnexpectedTypeException  if the types parent {@link FormTypeInterface::getParent()} is not a string
     * @throws Exception\InvalidArgumentException if the types parent can not be retrieved from any extension
     */
    public function createResolvedType(\Symfony\Component\Form\FormTypeInterface $type, array $typeExtensions, \Symfony\Component\Form\ResolvedFormTypeInterface $parent = null);
}
