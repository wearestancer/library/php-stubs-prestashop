<?php

namespace Symfony\Component\Form\ChoiceList\View;

/**
 * Represents a choice in templates.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ChoiceView
{
    public $label;
    public $value;
    public $data;
    /**
     * Additional attributes for the HTML tag.
     */
    public $attr;
    /**
     * Creates a new choice view.
     *
     * @param mixed        $data  The original choice
     * @param string       $value The view representation of the choice
     * @param string|false $label The label displayed to humans; pass false to discard the label
     * @param array        $attr  Additional attributes for the HTML tag
     */
    public function __construct($data, string $value, $label, array $attr = [])
    {
    }
}
