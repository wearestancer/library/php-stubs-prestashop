<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ChoicesToValuesTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(\Symfony\Component\Form\ChoiceList\ChoiceListInterface $choiceList)
    {
    }
    /**
     * @return array
     *
     * @throws TransformationFailedException if the given value is not an array
     */
    public function transform($array)
    {
    }
    /**
     * @return array
     *
     * @throws TransformationFailedException if the given value is not an array
     *                                       or if no matching choice could be
     *                                       found for some given value
     */
    public function reverseTransform($array)
    {
    }
}
