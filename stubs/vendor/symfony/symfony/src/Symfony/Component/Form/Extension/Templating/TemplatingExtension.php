<?php

namespace Symfony\Component\Form\Extension\Templating;

/**
 * Integrates the Templating component with the Form library.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplatingExtension extends \Symfony\Component\Form\AbstractExtension
{
    public function __construct(\Symfony\Component\Templating\PhpEngine $engine, \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager = null, array $defaultThemes = [])
    {
    }
}
