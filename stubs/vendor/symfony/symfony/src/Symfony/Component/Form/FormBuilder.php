<?php

namespace Symfony\Component\Form;

/**
 * A builder for creating {@link Form} instances.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormBuilder extends \Symfony\Component\Form\FormConfigBuilder implements \IteratorAggregate, \Symfony\Component\Form\FormBuilderInterface
{
    public function __construct(?string $name, ?string $dataClass, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher, \Symfony\Component\Form\FormFactoryInterface $factory, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add($child, $type = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create($name, $type = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormConfig()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getForm()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return FormBuilderInterface[]|\Traversable
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
