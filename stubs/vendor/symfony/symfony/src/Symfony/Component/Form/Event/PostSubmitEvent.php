<?php

namespace Symfony\Component\Form\Event;

/**
 * This event is dispatched after the Form::submit()
 * once the model and view data have been denormalized.
 *
 * It can be used to fetch data after denormalization.
 *
 * @final since Symfony 4.4
 */
class PostSubmitEvent extends \Symfony\Component\Form\FormEvent
{
}
