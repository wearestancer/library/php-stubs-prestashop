<?php

namespace Symfony\Component\Form\Util;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ServerParams
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack = null)
    {
    }
    /**
     * Returns true if the POST max size has been exceeded in the request.
     *
     * @return bool
     */
    public function hasPostMaxSizeBeenExceeded()
    {
    }
    /**
     * Returns maximum post size in bytes.
     *
     * @return int|null The maximum post size in bytes
     */
    public function getPostMaxSize()
    {
    }
    /**
     * Returns the normalized "post_max_size" ini setting.
     *
     * @return string
     */
    public function getNormalizedIniPostMaxSize()
    {
    }
    /**
     * Returns the content length of the request.
     *
     * @return mixed The request content length
     */
    public function getContentLength()
    {
    }
}
