<?php

namespace Symfony\Component\Form\Extension\Validator\ViolationMapper;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class MappingRule
{
    public function __construct(\Symfony\Component\Form\FormInterface $origin, string $propertyPath, string $targetPath)
    {
    }
    /**
     * @return FormInterface
     */
    public function getOrigin()
    {
    }
    /**
     * Matches a property path against the rule path.
     *
     * If the rule matches, the form mapped by the rule is returned.
     * Otherwise this method returns false.
     *
     * @param string $propertyPath The property path to match against the rule
     *
     * @return FormInterface|null The mapped form or null
     */
    public function match($propertyPath)
    {
    }
    /**
     * Matches a property path against a prefix of the rule path.
     *
     * @param string $propertyPath The property path to match against the rule
     *
     * @return bool Whether the property path is a prefix of the rule or not
     */
    public function isPrefix($propertyPath)
    {
    }
    /**
     * @return FormInterface
     *
     * @throws ErrorMappingException
     */
    public function getTarget()
    {
    }
}
