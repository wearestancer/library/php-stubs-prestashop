<?php

namespace Symfony\Component\Form\Guess;

/**
 * Contains a guessed class name and a list of options for creating an instance
 * of that class.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class TypeGuess extends \Symfony\Component\Form\Guess\Guess
{
    /**
     * @param string $type       The guessed field type
     * @param array  $options    The options for creating instances of the
     *                           guessed class
     * @param int    $confidence The confidence that the guessed class name
     *                           is correct
     */
    public function __construct(string $type, array $options, int $confidence)
    {
    }
    /**
     * Returns the guessed field type.
     *
     * @return string
     */
    public function getType()
    {
    }
    /**
     * Returns the guessed options for creating instances of the guessed type.
     *
     * @return array
     */
    public function getOptions()
    {
    }
}
