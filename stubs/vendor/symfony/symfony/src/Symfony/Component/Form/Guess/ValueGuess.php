<?php

namespace Symfony\Component\Form\Guess;

/**
 * Contains a guessed value.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ValueGuess extends \Symfony\Component\Form\Guess\Guess
{
    /**
     * @param string|int|bool|null $value      The guessed value
     * @param int                  $confidence The confidence that the guessed class name
     *                                         is correct
     */
    public function __construct($value, int $confidence)
    {
    }
    /**
     * Returns the guessed value.
     *
     * @return string|int|bool|null
     */
    public function getValue()
    {
    }
}
