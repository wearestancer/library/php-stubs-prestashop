<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a number type and a localized number with grouping
 * (each thousand) and comma separators.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Florian Eckerstorfer <florian@eckerstorfer.org>
 */
class NumberToLocalizedStringTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * Rounds a number towards positive infinity.
     *
     * Rounds 1.4 to 2 and -1.4 to -1.
     */
    public const ROUND_CEILING = \NumberFormatter::ROUND_CEILING;
    /**
     * Rounds a number towards negative infinity.
     *
     * Rounds 1.4 to 1 and -1.4 to -2.
     */
    public const ROUND_FLOOR = \NumberFormatter::ROUND_FLOOR;
    /**
     * Rounds a number away from zero.
     *
     * Rounds 1.4 to 2 and -1.4 to -2.
     */
    public const ROUND_UP = \NumberFormatter::ROUND_UP;
    /**
     * Rounds a number towards zero.
     *
     * Rounds 1.4 to 1 and -1.4 to -1.
     */
    public const ROUND_DOWN = \NumberFormatter::ROUND_DOWN;
    /**
     * Rounds to the nearest number and halves to the next even number.
     *
     * Rounds 2.5, 1.6 and 1.5 to 2 and 1.4 to 1.
     */
    public const ROUND_HALF_EVEN = \NumberFormatter::ROUND_HALFEVEN;
    /**
     * Rounds to the nearest number and halves away from zero.
     *
     * Rounds 2.5 to 3, 1.6 and 1.5 to 2 and 1.4 to 1.
     */
    public const ROUND_HALF_UP = \NumberFormatter::ROUND_HALFUP;
    /**
     * Rounds to the nearest number and halves towards zero.
     *
     * Rounds 2.5 and 1.6 to 2, 1.5 and 1.4 to 1.
     */
    public const ROUND_HALF_DOWN = \NumberFormatter::ROUND_HALFDOWN;
    protected $grouping;
    protected $roundingMode;
    public function __construct(int $scale = null, ?bool $grouping = false, ?int $roundingMode = self::ROUND_HALF_UP, string $locale = null)
    {
    }
    /**
     * Transforms a number type into localized number.
     *
     * @param int|float|null $value Number value
     *
     * @return string Localized value
     *
     * @throws TransformationFailedException if the given value is not numeric
     *                                       or if the value can not be transformed
     */
    public function transform($value)
    {
    }
    /**
     * Transforms a localized number into an integer or float.
     *
     * @param string $value The localized value
     *
     * @return int|float|null The numeric value
     *
     * @throws TransformationFailedException if the given value is not a string
     *                                       or if the value can not be transformed
     */
    public function reverseTransform($value)
    {
    }
    /**
     * Returns a preconfigured \NumberFormatter instance.
     *
     * @return \NumberFormatter
     */
    protected function getNumberFormatter()
    {
    }
    /**
     * @internal
     */
    protected function castParsedValue($value)
    {
    }
}
