<?php

namespace Symfony\Component\Form;

/**
 * Default implementation of {@link FormRendererEngineInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class AbstractRendererEngine implements \Symfony\Component\Form\FormRendererEngineInterface, \Symfony\Contracts\Service\ResetInterface
{
    /**
     * The variable in {@link FormView} used as cache key.
     */
    public const CACHE_KEY_VAR = 'cache_key';
    /**
     * @var array
     */
    protected $defaultThemes;
    /**
     * @var array[]
     */
    protected $themes = [];
    /**
     * @var bool[]
     */
    protected $useDefaultThemes = [];
    /**
     * @var array[]
     */
    protected $resources = [];
    /**
     * Creates a new renderer engine.
     *
     * @param array $defaultThemes The default themes. The type of these
     *                             themes is open to the implementation.
     */
    public function __construct(array $defaultThemes = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setTheme(\Symfony\Component\Form\FormView $view, $themes, $useDefaultThemes = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceForBlockName(\Symfony\Component\Form\FormView $view, $blockName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceForBlockNameHierarchy(\Symfony\Component\Form\FormView $view, array $blockNameHierarchy, $hierarchyLevel)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResourceHierarchyLevel(\Symfony\Component\Form\FormView $view, array $blockNameHierarchy, $hierarchyLevel)
    {
    }
    /**
     * Loads the cache with the resource for a given block name.
     *
     * @see getResourceForBlock()
     *
     * @param string   $cacheKey  The cache key of the form view
     * @param FormView $view      The form view for finding the applying themes
     * @param string   $blockName The name of the block to load
     *
     * @return bool True if the resource could be loaded, false otherwise
     */
    protected abstract function loadResourceForBlockName($cacheKey, \Symfony\Component\Form\FormView $view, $blockName);
    public function reset() : void
    {
    }
}
