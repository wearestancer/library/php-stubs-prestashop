<?php

namespace Symfony\Component\Form\Exception;

class ErrorMappingException extends \Symfony\Component\Form\Exception\RuntimeException
{
}
