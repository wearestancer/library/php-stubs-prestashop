<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a date string and a DateInterval object.
 *
 * @author Steffen Roßkamp <steffen.rosskamp@gimmickmedia.de>
 */
class DateIntervalToStringTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * Transforms a \DateInterval instance to a string.
     *
     * @see \DateInterval::format() for supported formats
     *
     * @param string $format The date format
     */
    public function __construct(string $format = 'P%yY%mM%dDT%hH%iM%sS')
    {
    }
    /**
     * Transforms a DateInterval object into a date string with the configured format.
     *
     * @param \DateInterval|null $value A DateInterval object
     *
     * @return string An ISO 8601 or relative date string like date interval presentation
     *
     * @throws UnexpectedTypeException if the given value is not a \DateInterval instance
     */
    public function transform($value)
    {
    }
    /**
     * Transforms a date string in the configured format into a DateInterval object.
     *
     * @param string $value An ISO 8601 or date string like date interval presentation
     *
     * @return \DateInterval|null An instance of \DateInterval
     *
     * @throws UnexpectedTypeException       if the given value is not a string
     * @throws TransformationFailedException if the date interval could not be parsed
     */
    public function reverseTransform($value)
    {
    }
}
