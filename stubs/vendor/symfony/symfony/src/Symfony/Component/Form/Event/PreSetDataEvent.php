<?php

namespace Symfony\Component\Form\Event;

/**
 * This event is dispatched at the beginning of the Form::setData() method.
 *
 * It can be used to:
 *  - Modify the data given during pre-population;
 *  - Modify a form depending on the pre-populated data (adding or removing fields dynamically).
 *
 * @final since Symfony 4.4
 */
class PreSetDataEvent extends \Symfony\Component\Form\FormEvent
{
}
