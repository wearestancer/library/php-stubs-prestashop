<?php

namespace Symfony\Component\Form\Extension\Csrf;

/**
 * This extension protects forms by using a CSRF token.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CsrfExtension extends \Symfony\Component\Form\AbstractExtension
{
    /**
     * @param TranslatorInterface|null $translator        The translator for translating error messages
     * @param string|null              $translationDomain The translation domain for translating
     */
    public function __construct(\Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $tokenManager, $translator = null, string $translationDomain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function loadTypeExtensions()
    {
    }
}
