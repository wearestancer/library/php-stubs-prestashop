<?php

namespace Symfony\Component\Form\Extension\Templating;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplatingRendererEngine extends \Symfony\Component\Form\AbstractRendererEngine
{
    public function __construct(\Symfony\Component\Templating\EngineInterface $engine, array $defaultThemes = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderBlock(\Symfony\Component\Form\FormView $view, $resource, $blockName, array $variables = [])
    {
    }
    /**
     * Loads the cache with the resource for a given block name.
     *
     * This implementation tries to load as few blocks as possible, since each block
     * is represented by a template on the file system.
     *
     * @see getResourceForBlock()
     *
     * @param string   $cacheKey  The cache key of the form view
     * @param FormView $view      The form view for finding the applying themes
     * @param string   $blockName The name of the block to load
     *
     * @return bool True if the resource could be loaded, false otherwise
     */
    protected function loadResourceForBlockName($cacheKey, \Symfony\Component\Form\FormView $view, $blockName)
    {
    }
    /**
     * Tries to load the resource for a block from a theme.
     *
     * @param string $cacheKey  The cache key for storing the resource
     * @param string $blockName The name of the block to load a resource for
     * @param mixed  $theme     The theme to load the block from
     *
     * @return bool True if the resource could be loaded, false otherwise
     */
    protected function loadResourceFromTheme($cacheKey, $blockName, $theme)
    {
    }
}
