<?php

namespace Symfony\Component\Form\Extension\Core\EventListener;

/**
 * Trims string data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class TrimListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function preSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
