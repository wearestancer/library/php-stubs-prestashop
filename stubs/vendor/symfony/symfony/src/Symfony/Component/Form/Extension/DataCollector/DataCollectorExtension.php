<?php

namespace Symfony\Component\Form\Extension\DataCollector;

/**
 * Extension for collecting data of the forms on a page.
 *
 * @author Robert Schönthal <robert.schoenthal@gmail.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DataCollectorExtension extends \Symfony\Component\Form\AbstractExtension
{
    public function __construct(\Symfony\Component\Form\Extension\DataCollector\FormDataCollectorInterface $dataCollector)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function loadTypeExtensions()
    {
    }
}
