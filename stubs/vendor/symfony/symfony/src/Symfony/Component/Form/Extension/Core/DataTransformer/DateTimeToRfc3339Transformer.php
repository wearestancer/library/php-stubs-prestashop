<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DateTimeToRfc3339Transformer extends \Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer
{
    /**
     * Transforms a normalized date into a localized date.
     *
     * @param \DateTimeInterface $dateTime A DateTimeInterface object
     *
     * @return string The formatted date
     *
     * @throws TransformationFailedException If the given value is not a \DateTimeInterface
     */
    public function transform($dateTime)
    {
    }
    /**
     * Transforms a formatted string following RFC 3339 into a normalized date.
     *
     * @param string $rfc3339 Formatted string
     *
     * @return \DateTime|null Normalized date
     *
     * @throws TransformationFailedException If the given value is not a string,
     *                                       if the value could not be transformed
     */
    public function reverseTransform($rfc3339)
    {
    }
}
