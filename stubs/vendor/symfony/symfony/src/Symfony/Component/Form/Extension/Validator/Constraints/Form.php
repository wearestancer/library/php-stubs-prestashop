<?php

namespace Symfony\Component\Form\Extension\Validator\Constraints;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Form extends \Symfony\Component\Validator\Constraint
{
    public const NOT_SYNCHRONIZED_ERROR = '1dafa156-89e1-4736-b832-419c2e501fca';
    public const NO_SUCH_FIELD_ERROR = '6e5212ed-a197-4339-99aa-5654798a4854';
    protected static $errorNames = [self::NOT_SYNCHRONIZED_ERROR => 'NOT_SYNCHRONIZED_ERROR', self::NO_SUCH_FIELD_ERROR => 'NO_SUCH_FIELD_ERROR'];
    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
    }
}
