<?php

namespace Symfony\Component\Form;

/**
 * A form element whose errors can be cleared.
 *
 * @author Colin O'Dell <colinodell@gmail.com>
 */
interface ClearableErrorsInterface
{
    /**
     * Removes all the errors of this form.
     *
     * @param bool $deep Whether to remove errors from child forms as well
     */
    public function clearErrors(bool $deep = false);
}
