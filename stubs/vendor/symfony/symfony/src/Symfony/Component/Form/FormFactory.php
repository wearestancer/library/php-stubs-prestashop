<?php

namespace Symfony\Component\Form;

class FormFactory implements \Symfony\Component\Form\FormFactoryInterface
{
    public function __construct(\Symfony\Component\Form\FormRegistryInterface $registry)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create($type = \Symfony\Component\Form\Extension\Core\Type\FormType::class, $data = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNamed($name, $type = \Symfony\Component\Form\Extension\Core\Type\FormType::class, $data = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createForProperty($class, $property, $data = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createBuilder($type = \Symfony\Component\Form\Extension\Core\Type\FormType::class, $data = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNamedBuilder($name, $type = \Symfony\Component\Form\Extension\Core\Type\FormType::class, $data = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createBuilderForProperty($class, $property, $data = null, array $options = [])
    {
    }
}
