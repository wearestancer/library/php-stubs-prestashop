<?php

namespace Symfony\Component\Form\Extension\Core\DataMapper;

/**
 * Maps choices to/from radio forms.
 *
 * A {@link ChoiceListInterface} implementation is used to find the
 * corresponding string values for the choices. The radio form whose "value"
 * option corresponds to the selected value is marked as selected.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RadioListMapper implements \Symfony\Component\Form\DataMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function mapDataToForms($choice, $radios)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function mapFormsToData($radios, &$choice)
    {
    }
}
