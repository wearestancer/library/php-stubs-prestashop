<?php

namespace Symfony\Component\Form\Extension\Validator;

/**
 * Extension supporting the Symfony Validator component in forms.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ValidatorExtension extends \Symfony\Component\Form\AbstractExtension
{
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator)
    {
    }
    public function loadTypeGuesser()
    {
    }
    protected function loadTypeExtensions()
    {
    }
}
