<?php

namespace Symfony\Component\Form;

/**
 * A wrapper for a form type and its extensions.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface ResolvedFormTypeInterface
{
    /**
     * Returns the prefix of the template block name for this type.
     *
     * @return string The prefix of the template block name
     */
    public function getBlockPrefix();
    /**
     * Returns the parent type.
     *
     * @return self|null The parent type or null
     */
    public function getParent();
    /**
     * Returns the wrapped form type.
     *
     * @return FormTypeInterface The wrapped form type
     */
    public function getInnerType();
    /**
     * Returns the extensions of the wrapped form type.
     *
     * @return FormTypeExtensionInterface[] An array of {@link FormTypeExtensionInterface} instances
     */
    public function getTypeExtensions();
    /**
     * Creates a new form builder for this type.
     *
     * @param string $name The name for the builder
     *
     * @return FormBuilderInterface The created form builder
     */
    public function createBuilder(\Symfony\Component\Form\FormFactoryInterface $factory, $name, array $options = []);
    /**
     * Creates a new form view for a form of this type.
     *
     * @return FormView The created form view
     */
    public function createView(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $parent = null);
    /**
     * Configures a form builder for the type hierarchy.
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options);
    /**
     * Configures a form view for the type hierarchy.
     *
     * It is called before the children of the view are built.
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options);
    /**
     * Finishes a form view for the type hierarchy.
     *
     * It is called after the children of the view have been built.
     */
    public function finishView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options);
    /**
     * Returns the configured options resolver used for this type.
     *
     * @return OptionsResolver The options resolver
     */
    public function getOptionsResolver();
}
