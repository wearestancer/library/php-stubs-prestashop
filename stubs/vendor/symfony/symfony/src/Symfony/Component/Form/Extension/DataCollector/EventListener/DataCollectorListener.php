<?php

namespace Symfony\Component\Form\Extension\DataCollector\EventListener;

/**
 * Listener that invokes a data collector for the {@link FormEvents::POST_SET_DATA}
 * and {@link FormEvents::POST_SUBMIT} events.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DataCollectorListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\Form\Extension\DataCollector\FormDataCollectorInterface $dataCollector)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
    /**
     * Listener for the {@link FormEvents::POST_SET_DATA} event.
     */
    public function postSetData(\Symfony\Component\Form\FormEvent $event)
    {
    }
    /**
     * Listener for the {@link FormEvents::POST_SUBMIT} event.
     */
    public function postSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
}
