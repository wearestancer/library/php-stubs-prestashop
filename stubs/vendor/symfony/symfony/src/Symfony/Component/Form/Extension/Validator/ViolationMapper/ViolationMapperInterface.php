<?php

namespace Symfony\Component\Form\Extension\Validator\ViolationMapper;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface ViolationMapperInterface
{
    /**
     * Maps a constraint violation to a form in the form tree under
     * the given form.
     *
     * @param bool $allowNonSynchronized Whether to allow mapping to non-synchronized forms
     */
    public function mapViolation(\Symfony\Component\Validator\ConstraintViolation $violation, \Symfony\Component\Form\FormInterface $form, $allowNonSynchronized = false);
}
