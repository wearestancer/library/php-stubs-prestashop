<?php

namespace Symfony\Component\Form\Extension\HttpFoundation;

/**
 * Integrates the HttpFoundation component with the Form library.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class HttpFoundationExtension extends \Symfony\Component\Form\AbstractExtension
{
    protected function loadTypeExtensions()
    {
    }
}
