<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a normalized date interval and an interval string/array.
 *
 * @author Steffen Roßkamp <steffen.rosskamp@gimmickmedia.de>
 */
class DateIntervalToArrayTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public const YEARS = 'years';
    public const MONTHS = 'months';
    public const DAYS = 'days';
    public const HOURS = 'hours';
    public const MINUTES = 'minutes';
    public const SECONDS = 'seconds';
    public const INVERT = 'invert';
    /**
     * @param string[]|null $fields The date fields
     * @param bool          $pad    Whether to use padding
     */
    public function __construct(array $fields = null, bool $pad = false)
    {
    }
    /**
     * Transforms a normalized date interval into an interval array.
     *
     * @param \DateInterval $dateInterval Normalized date interval
     *
     * @return array Interval array
     *
     * @throws UnexpectedTypeException if the given value is not a \DateInterval instance
     */
    public function transform($dateInterval)
    {
    }
    /**
     * Transforms an interval array into a normalized date interval.
     *
     * @param array $value Interval array
     *
     * @return \DateInterval|null Normalized date interval
     *
     * @throws UnexpectedTypeException       if the given value is not an array
     * @throws TransformationFailedException if the value could not be transformed
     */
    public function reverseTransform($value)
    {
    }
}
