<?php

namespace Symfony\Component\Form\Event;

/**
 * This event is dispatched just before the Form::submit() method
 * transforms back the normalized data to the model and view data.
 *
 * It can be used to change data from the normalized representation of the data.
 *
 * @final since Symfony 4.4
 */
class SubmitEvent extends \Symfony\Component\Form\FormEvent
{
}
