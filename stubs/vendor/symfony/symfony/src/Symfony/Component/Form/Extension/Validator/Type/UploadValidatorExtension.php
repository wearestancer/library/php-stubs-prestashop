<?php

namespace Symfony\Component\Form\Extension\Validator\Type;

/**
 * @author Abdellatif Ait boudad <a.aitboudad@gmail.com>
 * @author David Badura <d.a.badura@gmail.com>
 */
class UploadValidatorExtension extends \Symfony\Component\Form\AbstractTypeExtension
{
    /**
     * @param TranslatorInterface $translator
     */
    public function __construct($translator, string $translationDomain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes() : iterable
    {
    }
}
