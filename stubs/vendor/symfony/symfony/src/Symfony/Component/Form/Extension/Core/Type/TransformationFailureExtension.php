<?php

namespace Symfony\Component\Form\Extension\Core\Type;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
class TransformationFailureExtension extends \Symfony\Component\Form\AbstractTypeExtension
{
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct($translator = null)
    {
    }
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes() : iterable
    {
    }
}
