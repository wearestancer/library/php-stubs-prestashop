<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a Boolean and a string.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Florian Eckerstorfer <florian@eckerstorfer.org>
 */
class BooleanToStringTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * @param string $trueValue The value emitted upon transform if the input is true
     */
    public function __construct(string $trueValue, array $falseValues = [null])
    {
    }
    /**
     * Transforms a Boolean into a string.
     *
     * @param bool $value Boolean value
     *
     * @return string|null String value
     *
     * @throws TransformationFailedException if the given value is not a Boolean
     */
    public function transform($value)
    {
    }
    /**
     * Transforms a string into a Boolean.
     *
     * @param string $value String value
     *
     * @return bool Boolean value
     *
     * @throws TransformationFailedException if the given value is not a string
     */
    public function reverseTransform($value)
    {
    }
}
