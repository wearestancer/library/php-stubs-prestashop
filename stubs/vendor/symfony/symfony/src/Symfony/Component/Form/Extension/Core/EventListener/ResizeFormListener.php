<?php

namespace Symfony\Component\Form\Extension\Core\EventListener;

/**
 * Resize a collection form element based on the data sent from the client.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResizeFormListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    protected $type;
    protected $options;
    protected $allowAdd;
    protected $allowDelete;
    /**
     * @param bool          $allowAdd    Whether children could be added to the group
     * @param bool          $allowDelete Whether children could be removed from the group
     * @param bool|callable $deleteEmpty
     */
    public function __construct(string $type, array $options = [], bool $allowAdd = false, bool $allowDelete = false, $deleteEmpty = false)
    {
    }
    public static function getSubscribedEvents()
    {
    }
    public function preSetData(\Symfony\Component\Form\FormEvent $event)
    {
    }
    public function preSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
    public function onSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
}
