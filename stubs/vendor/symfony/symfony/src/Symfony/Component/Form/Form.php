<?php

namespace Symfony\Component\Form;

/**
 * Form represents a form.
 *
 * To implement your own form fields, you need to have a thorough understanding
 * of the data flow within a form. A form stores its data in three different
 * representations:
 *
 *   (1) the "model" format required by the form's object
 *   (2) the "normalized" format for internal processing
 *   (3) the "view" format used for display simple fields
 *       or map children model data for compound fields
 *
 * A date field, for example, may store a date as "Y-m-d" string (1) in the
 * object. To facilitate processing in the field, this value is normalized
 * to a DateTime object (2). In the HTML representation of your form, a
 * localized string (3) may be presented to and modified by the user, or it could be an array of values
 * to be mapped to choices fields.
 *
 * In most cases, format (1) and format (2) will be the same. For example,
 * a checkbox field uses a Boolean value for both internal processing and
 * storage in the object. In these cases you need to set a view transformer
 * to convert between formats (2) and (3). You can do this by calling
 * addViewTransformer().
 *
 * In some cases though it makes sense to make format (1) configurable. To
 * demonstrate this, let's extend our above date field to store the value
 * either as "Y-m-d" string or as timestamp. Internally we still want to
 * use a DateTime object for processing. To convert the data from string/integer
 * to DateTime you can set a model transformer by calling
 * addModelTransformer(). The normalized data is then converted to the displayed
 * data as described before.
 *
 * The conversions (1) -> (2) -> (3) use the transform methods of the transformers.
 * The conversions (3) -> (2) -> (1) use the reverseTransform methods of the transformers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Form implements \IteratorAggregate, \Symfony\Component\Form\FormInterface, \Symfony\Component\Form\ClearableErrorsInterface
{
    /**
     * @throws LogicException if a data mapper is not provided for a compound form
     */
    public function __construct(\Symfony\Component\Form\FormConfigInterface $config)
    {
    }
    public function __clone()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyPath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isRequired()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isDisabled()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setParent(\Symfony\Component\Form\FormInterface $parent = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isRoot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData($modelData)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNormData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getViewData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExtraData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initialize()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleRequest($request = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function submit($submittedData, $clearMissing = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addError(\Symfony\Component\Form\FormError $error)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isSubmitted()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isSynchronized()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTransformationFailure()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isValid()
    {
    }
    /**
     * Returns the button that was used to submit the form.
     *
     * @return FormInterface|ClickableInterface|null
     */
    public function getClickedButton()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getErrors($deep = false, $flatten = true)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function clearErrors(bool $deep = false) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add($child, $type = null, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * Returns whether a child with the given name exists (implements the \ArrayAccess interface).
     *
     * @param string $name The name of the child
     *
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($name)
    {
    }
    /**
     * Returns the child with the given name (implements the \ArrayAccess interface).
     *
     * @param string $name The name of the child
     *
     * @return FormInterface The child form
     *
     * @throws OutOfBoundsException if the named child does not exist
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($name)
    {
    }
    /**
     * Adds a child to the form (implements the \ArrayAccess interface).
     *
     * @param string        $name  Ignored. The name of the child is used
     * @param FormInterface $child The child to be added
     *
     * @return void
     *
     * @throws AlreadySubmittedException if the form has already been submitted
     * @throws LogicException            when trying to add a child to a non-compound form
     *
     * @see self::add()
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($name, $child)
    {
    }
    /**
     * Removes the child with the given name from the form (implements the \ArrayAccess interface).
     *
     * @param string $name The name of the child to remove
     *
     * @return void
     *
     * @throws AlreadySubmittedException if the form has already been submitted
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($name)
    {
    }
    /**
     * Returns the iterator for this group.
     *
     * @return \Traversable<FormInterface>
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * Returns the number of form children (implements the \Countable interface).
     *
     * @return int The number of embedded form children
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createView(\Symfony\Component\Form\FormView $parent = null)
    {
    }
}
