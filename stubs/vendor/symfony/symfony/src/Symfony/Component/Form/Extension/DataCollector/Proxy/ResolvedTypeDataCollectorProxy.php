<?php

namespace Symfony\Component\Form\Extension\DataCollector\Proxy;

/**
 * Proxy that invokes a data collector when creating a form and its view.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResolvedTypeDataCollectorProxy implements \Symfony\Component\Form\ResolvedFormTypeInterface
{
    public function __construct(\Symfony\Component\Form\ResolvedFormTypeInterface $proxiedType, \Symfony\Component\Form\Extension\DataCollector\FormDataCollectorInterface $dataCollector)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInnerType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeExtensions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createBuilder(\Symfony\Component\Form\FormFactoryInterface $factory, $name, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createView(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $parent = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function finishView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOptionsResolver()
    {
    }
}
