<?php

namespace Symfony\Component\Form\Extension\Core\DataMapper;

/**
 * Maps arrays/objects to/from forms using property paths.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class PropertyPathMapper implements \Symfony\Component\Form\DataMapperInterface
{
    public function __construct(\Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function mapDataToForms($data, $forms)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function mapFormsToData($forms, &$data)
    {
    }
}
