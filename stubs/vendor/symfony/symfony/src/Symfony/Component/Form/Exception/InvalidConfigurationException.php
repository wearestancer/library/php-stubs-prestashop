<?php

namespace Symfony\Component\Form\Exception;

class InvalidConfigurationException extends \Symfony\Component\Form\Exception\InvalidArgumentException
{
}
