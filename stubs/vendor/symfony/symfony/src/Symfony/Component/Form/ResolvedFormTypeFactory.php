<?php

namespace Symfony\Component\Form;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResolvedFormTypeFactory implements \Symfony\Component\Form\ResolvedFormTypeFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createResolvedType(\Symfony\Component\Form\FormTypeInterface $type, array $typeExtensions, \Symfony\Component\Form\ResolvedFormTypeInterface $parent = null)
    {
    }
}
