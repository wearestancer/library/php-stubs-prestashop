<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ChoiceToValueTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(\Symfony\Component\Form\ChoiceList\ChoiceListInterface $choiceList)
    {
    }
    public function transform($choice)
    {
    }
    public function reverseTransform($value)
    {
    }
}
