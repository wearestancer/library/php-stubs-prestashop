<?php

namespace Symfony\Component\Form\Test;

/**
 * Base class for performance tests.
 *
 * Copied from Doctrine 2's OrmPerformanceTestCase.
 *
 * @author robo
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class FormPerformanceTestCase extends \Symfony\Component\Form\Test\FormIntegrationTestCase
{
    use \Symfony\Component\Form\Tests\VersionAwareTest;
    /**
     * @var int
     */
    protected $maxRunningTime = 0;
    /**
     * {@inheritdoc}
     */
    protected function runTest()
    {
    }
    /**
     * @param int $maxRunningTime
     *
     * @throws \InvalidArgumentException
     */
    public function setMaxRunningTime($maxRunningTime)
    {
    }
    /**
     * @return int
     */
    public function getMaxRunningTime()
    {
    }
}
