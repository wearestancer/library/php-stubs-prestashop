<?php

namespace Symfony\Component\Form\Extension\HttpFoundation;

/**
 * A request processor using the {@link Request} class of the HttpFoundation
 * component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class HttpFoundationRequestHandler implements \Symfony\Component\Form\RequestHandlerInterface
{
    public function __construct(\Symfony\Component\Form\Util\ServerParams $serverParams = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handleRequest(\Symfony\Component\Form\FormInterface $form, $request = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFileUpload($data)
    {
    }
    /**
     * @return int|null
     */
    public function getUploadFileError($data)
    {
    }
}
