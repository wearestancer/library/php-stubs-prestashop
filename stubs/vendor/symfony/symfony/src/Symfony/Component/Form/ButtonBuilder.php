<?php

namespace Symfony\Component\Form;

/**
 * A builder for {@link Button} instances.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ButtonBuilder implements \IteratorAggregate, \Symfony\Component\Form\FormBuilderInterface
{
    protected $locked = false;
    /**
     * @throws InvalidArgumentException if the name is empty
     */
    public function __construct(?string $name, array $options = [])
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @throws BadMethodCallException
     */
    public function add($child, $type = null, array $options = [])
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @throws BadMethodCallException
     */
    public function create($name, $type = null, array $options = [])
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function get($name)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function remove($name)
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function has($name)
    {
    }
    /**
     * Returns the children.
     *
     * @return array Always returns an empty array
     */
    public function all()
    {
    }
    /**
     * Creates the button.
     *
     * @return Button The button
     */
    public function getForm()
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function addEventListener($eventName, $listener, $priority = 0)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function addEventSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function addViewTransformer(\Symfony\Component\Form\DataTransformerInterface $viewTransformer, $forcePrepend = false)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function resetViewTransformers()
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function addModelTransformer(\Symfony\Component\Form\DataTransformerInterface $modelTransformer, $forceAppend = false)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function resetModelTransformers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAttribute($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAttributes(array $attributes)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setDataMapper(\Symfony\Component\Form\DataMapperInterface $dataMapper = null)
    {
    }
    /**
     * Set whether the button is disabled.
     *
     * @param bool $disabled Whether the button is disabled
     *
     * @return $this
     */
    public function setDisabled($disabled)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setEmptyData($emptyData)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setErrorBubbling($errorBubbling)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setRequired($required)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setPropertyPath($propertyPath)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setMapped($mapped)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setByReference($byReference)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setCompound($compound)
    {
    }
    /**
     * Sets the type of the button.
     *
     * @return $this
     */
    public function setType(\Symfony\Component\Form\ResolvedFormTypeInterface $type)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setData($data)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setDataLocked($locked)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setFormFactory(\Symfony\Component\Form\FormFactoryInterface $formFactory)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setAction($action)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setMethod($method)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setRequestHandler(\Symfony\Component\Form\RequestHandlerInterface $requestHandler)
    {
    }
    /**
     * Unsupported method.
     *
     * @return $this
     *
     * @throws BadMethodCallException
     */
    public function setAutoInitialize($initialize)
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function setInheritData($inheritData)
    {
    }
    /**
     * Builds and returns the button configuration.
     *
     * @return FormConfigInterface
     */
    public function getFormConfig()
    {
    }
    /**
     * Unsupported method.
     */
    public function getEventDispatcher()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * Unsupported method.
     */
    public function getPropertyPath()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getMapped()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getByReference()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getCompound()
    {
    }
    /**
     * Returns the form type used to construct the button.
     *
     * @return ResolvedFormTypeInterface The button's type
     */
    public function getType()
    {
    }
    /**
     * Unsupported method.
     *
     * @return array Always returns an empty array
     */
    public function getViewTransformers()
    {
    }
    /**
     * Unsupported method.
     *
     * @return array Always returns an empty array
     */
    public function getModelTransformers()
    {
    }
    /**
     * Unsupported method.
     */
    public function getDataMapper()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getRequired()
    {
    }
    /**
     * Returns whether the button is disabled.
     *
     * @return bool Whether the button is disabled
     */
    public function getDisabled()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getErrorBubbling()
    {
    }
    /**
     * Unsupported method.
     */
    public function getEmptyData()
    {
    }
    /**
     * Returns additional attributes of the button.
     *
     * @return array An array of key-value combinations
     */
    public function getAttributes()
    {
    }
    /**
     * Returns whether the attribute with the given name exists.
     *
     * @param string $name The attribute name
     *
     * @return bool Whether the attribute exists
     */
    public function hasAttribute($name)
    {
    }
    /**
     * Returns the value of the given attribute.
     *
     * @param string $name    The attribute name
     * @param mixed  $default The value returned if the attribute does not exist
     *
     * @return mixed The attribute value
     */
    public function getAttribute($name, $default = null)
    {
    }
    /**
     * Unsupported method.
     */
    public function getData()
    {
    }
    /**
     * Unsupported method.
     */
    public function getDataClass()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getDataLocked()
    {
    }
    /**
     * Unsupported method.
     */
    public function getFormFactory()
    {
    }
    /**
     * Unsupported method.
     */
    public function getAction()
    {
    }
    /**
     * Unsupported method.
     */
    public function getMethod()
    {
    }
    /**
     * Unsupported method.
     */
    public function getRequestHandler()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getAutoInitialize()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function getInheritData()
    {
    }
    /**
     * Returns all options passed during the construction of the button.
     *
     * @return array The passed options
     */
    public function getOptions()
    {
    }
    /**
     * Returns whether a specific option exists.
     *
     * @param string $name The option name,
     *
     * @return bool Whether the option exists
     */
    public function hasOption($name)
    {
    }
    /**
     * Returns the value of a specific option.
     *
     * @param string $name    The option name
     * @param mixed  $default The value returned if the option does not exist
     *
     * @return mixed The option value
     */
    public function getOption($name, $default = null)
    {
    }
    /**
     * Unsupported method.
     *
     * @return int Always returns 0
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * Unsupported method.
     *
     * @return \EmptyIterator Always returns an empty iterator
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
