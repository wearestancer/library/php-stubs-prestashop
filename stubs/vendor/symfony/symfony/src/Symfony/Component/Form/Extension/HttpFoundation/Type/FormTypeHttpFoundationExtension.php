<?php

namespace Symfony\Component\Form\Extension\HttpFoundation\Type;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormTypeHttpFoundationExtension extends \Symfony\Component\Form\AbstractTypeExtension
{
    public function __construct(\Symfony\Component\Form\RequestHandlerInterface $requestHandler = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes() : iterable
    {
    }
}
