<?php

namespace Symfony\Component\Form;

/**
 * Renders a form into HTML using a rendering engine.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormRenderer implements \Symfony\Component\Form\FormRendererInterface
{
    public const CACHE_KEY_VAR = 'unique_block_prefix';
    public function __construct(\Symfony\Component\Form\FormRendererEngineInterface $engine, \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEngine()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setTheme(\Symfony\Component\Form\FormView $view, $themes, $useDefaultThemes = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderCsrfToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderBlock(\Symfony\Component\Form\FormView $view, $blockName, array $variables = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function searchAndRenderBlock(\Symfony\Component\Form\FormView $view, $blockNameSuffix, array $variables = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function humanize($text)
    {
    }
    /**
     * @internal
     */
    public function encodeCurrency(\Twig\Environment $environment, string $text, string $widget = '') : string
    {
    }
}
