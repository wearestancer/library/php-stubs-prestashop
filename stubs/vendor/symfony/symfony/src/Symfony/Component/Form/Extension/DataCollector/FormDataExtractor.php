<?php

namespace Symfony\Component\Form\Extension\DataCollector;

/**
 * Default implementation of {@link FormDataExtractorInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormDataExtractor implements \Symfony\Component\Form\Extension\DataCollector\FormDataExtractorInterface
{
    /**
     * {@inheritdoc}
     */
    public function extractConfiguration(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extractDefaultData(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extractSubmittedData(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extractViewVariables(\Symfony\Component\Form\FormView $view)
    {
    }
}
