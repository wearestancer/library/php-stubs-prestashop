<?php

namespace Symfony\Component\Form;

/**
 * The central registry of the Form component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormRegistry implements \Symfony\Component\Form\FormRegistryInterface
{
    /**
     * @param FormExtensionInterface[] $extensions
     *
     * @throws UnexpectedTypeException if any extension does not implement FormExtensionInterface
     */
    public function __construct(array $extensions, \Symfony\Component\Form\ResolvedFormTypeFactoryInterface $resolvedTypeFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getType($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasType($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeGuesser()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExtensions()
    {
    }
}
