<?php

namespace Symfony\Component\Form;

/**
 * A form button.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Button implements \IteratorAggregate, \Symfony\Component\Form\FormInterface
{
    /**
     * Creates a new button from a form configuration.
     */
    public function __construct(\Symfony\Component\Form\FormConfigInterface $config)
    {
    }
    /**
     * Unsupported method.
     *
     * @param mixed $offset
     *
     * @return bool Always returns false
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @param mixed $offset
     *
     * @return mixed
     *
     * @throws BadMethodCallException
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @param mixed $offset
     * @param mixed $value
     *
     * @return void
     *
     * @throws BadMethodCallException
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @param mixed $offset
     *
     * @return void
     *
     * @throws BadMethodCallException
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setParent(\Symfony\Component\Form\FormInterface $parent = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @throws BadMethodCallException
     */
    public function add($child, $type = null, array $options = [])
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @param string $name
     *
     * @throws BadMethodCallException
     */
    public function get($name)
    {
    }
    /**
     * Unsupported method.
     *
     * @param string $name
     *
     * @return bool Always returns false
     */
    public function has($name)
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @param string $name
     *
     * @throws BadMethodCallException
     */
    public function remove($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getErrors($deep = false, $flatten = true)
    {
    }
    /**
     * Unsupported method.
     *
     * This method should not be invoked.
     *
     * @param mixed $modelData
     *
     * @return $this
     */
    public function setData($modelData)
    {
    }
    /**
     * Unsupported method.
     */
    public function getData()
    {
    }
    /**
     * Unsupported method.
     */
    public function getNormData()
    {
    }
    /**
     * Unsupported method.
     */
    public function getViewData()
    {
    }
    /**
     * Unsupported method.
     *
     * @return array Always returns an empty array
     */
    public function getExtraData()
    {
    }
    /**
     * Returns the button's configuration.
     *
     * @return FormConfigInterface The configuration instance
     */
    public function getConfig()
    {
    }
    /**
     * Returns whether the button is submitted.
     *
     * @return bool true if the button was submitted
     */
    public function isSubmitted()
    {
    }
    /**
     * Returns the name by which the button is identified in forms.
     *
     * @return string The name of the button
     */
    public function getName()
    {
    }
    /**
     * Unsupported method.
     */
    public function getPropertyPath()
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function addError(\Symfony\Component\Form\FormError $error)
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns true
     */
    public function isValid()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns false
     */
    public function isRequired()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isDisabled()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns true
     */
    public function isEmpty()
    {
    }
    /**
     * Unsupported method.
     *
     * @return bool Always returns true
     */
    public function isSynchronized()
    {
    }
    /**
     * Unsupported method.
     */
    public function getTransformationFailure()
    {
    }
    /**
     * Unsupported method.
     *
     * @throws BadMethodCallException
     */
    public function initialize()
    {
    }
    /**
     * Unsupported method.
     *
     * @param mixed $request
     *
     * @throws BadMethodCallException
     */
    public function handleRequest($request = null)
    {
    }
    /**
     * Submits data to the button.
     *
     * @param array|string|null $submittedData Not used
     * @param bool              $clearMissing  Not used
     *
     * @return $this
     *
     * @throws Exception\AlreadySubmittedException if the button has already been submitted
     */
    public function submit($submittedData, $clearMissing = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isRoot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createView(\Symfony\Component\Form\FormView $parent = null)
    {
    }
    /**
     * Unsupported method.
     *
     * @return int Always returns 0
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * Unsupported method.
     *
     * @return \EmptyIterator Always returns an empty iterator
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
