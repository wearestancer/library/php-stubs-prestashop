<?php

namespace Symfony\Component\Form\Util;

/**
 * Iterator for {@link OrderedHashMap} objects.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class OrderedHashMapIterator implements \Iterator
{
    /**
     * @param array $elements       The elements of the map, indexed by their
     *                              keys
     * @param array $orderedKeys    The keys of the map in the order in which
     *                              they should be iterated
     * @param array $managedCursors An array from which to reference the
     *                              iterator's cursor as long as it is alive.
     *                              This array is managed by the corresponding
     *                              {@link OrderedHashMap} instance to support
     *                              recognizing the deletion of elements.
     */
    public function __construct(array &$elements, array &$orderedKeys, array &$managedCursors)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    /**
     * Removes the iterator's cursors from the managed cursors of the
     * corresponding {@link OrderedHashMap} instance.
     */
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function next() : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function valid() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function rewind() : void
    {
    }
}
