<?php

namespace Symfony\Component\Form;

/**
 * A wrapper for a form type and its extensions.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResolvedFormType implements \Symfony\Component\Form\ResolvedFormTypeInterface
{
    /**
     * @param FormTypeExtensionInterface[] $typeExtensions
     */
    public function __construct(\Symfony\Component\Form\FormTypeInterface $innerType, array $typeExtensions = [], \Symfony\Component\Form\ResolvedFormTypeInterface $parent = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInnerType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeExtensions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createBuilder(\Symfony\Component\Form\FormFactoryInterface $factory, $name, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createView(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $parent = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function finishView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOptionsResolver()
    {
    }
    /**
     * Creates a new builder instance.
     *
     * Override this method if you want to customize the builder class.
     *
     * @param string      $name      The name of the builder
     * @param string|null $dataClass The data class
     *
     * @return FormBuilderInterface The new builder instance
     */
    protected function newBuilder($name, $dataClass, \Symfony\Component\Form\FormFactoryInterface $factory, array $options)
    {
    }
    /**
     * Creates a new view instance.
     *
     * Override this method if you want to customize the view class.
     *
     * @return FormView A new view instance
     */
    protected function newView(\Symfony\Component\Form\FormView $parent = null)
    {
    }
}
