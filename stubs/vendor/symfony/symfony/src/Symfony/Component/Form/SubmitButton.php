<?php

namespace Symfony\Component\Form;

/**
 * A button that submits the form.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class SubmitButton extends \Symfony\Component\Form\Button implements \Symfony\Component\Form\ClickableInterface
{
    /**
     * {@inheritdoc}
     */
    public function isClicked()
    {
    }
    /**
     * Submits data to the button.
     *
     * @param array|string|null $submittedData The data
     * @param bool              $clearMissing  Not used
     *
     * @return $this
     *
     * @throws Exception\AlreadySubmittedException if the form has already been submitted
     */
    public function submit($submittedData, $clearMissing = true)
    {
    }
}
