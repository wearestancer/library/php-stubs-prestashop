<?php

namespace Symfony\Component\Form\ChoiceList\Factory;

/**
 * Adds property path support to a choice list factory.
 *
 * Pass the decorated factory to the constructor:
 *
 *     $decorator = new PropertyAccessDecorator($factory);
 *
 * You can now pass property paths for generating choice values, labels, view
 * indices, HTML attributes and for determining the preferred choices and the
 * choice groups:
 *
 *     // extract values from the $value property
 *     $list = $createListFromChoices($objects, 'value');
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class PropertyAccessDecorator implements \Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface
{
    public function __construct(\Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface $decoratedFactory, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * Returns the decorated factory.
     *
     * @return ChoiceListFactoryInterface The decorated factory
     */
    public function getDecoratedFactory()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param mixed $value
     *
     * @return ChoiceListInterface
     */
    public function createListFromChoices($choices, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param mixed $value
     *
     * @return ChoiceListInterface
     */
    public function createListFromLoader(\Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface $loader, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param mixed $preferredChoices
     * @param mixed $label
     * @param mixed $index
     * @param mixed $groupBy
     * @param mixed $attr
     *
     * @return ChoiceListView
     */
    public function createView(\Symfony\Component\Form\ChoiceList\ChoiceListInterface $list, $preferredChoices = null, $label = null, $index = null, $groupBy = null, $attr = null)
    {
    }
}
