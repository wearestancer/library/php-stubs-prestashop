<?php

namespace Symfony\Component\Form\Test;

interface FormBuilderInterface extends \Iterator, \Symfony\Component\Form\FormBuilderInterface
{
}
