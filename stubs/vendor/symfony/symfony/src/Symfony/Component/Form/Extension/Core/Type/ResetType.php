<?php

namespace Symfony\Component\Form\Extension\Core\Type;

/**
 * A reset button.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResetType extends \Symfony\Component\Form\AbstractType implements \Symfony\Component\Form\ButtonTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
