<?php

namespace Symfony\Component\Form\Exception;

class UnexpectedTypeException extends \Symfony\Component\Form\Exception\InvalidArgumentException
{
    public function __construct($value, string $expectedType)
    {
    }
}
