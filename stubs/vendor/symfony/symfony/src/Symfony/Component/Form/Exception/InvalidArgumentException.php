<?php

namespace Symfony\Component\Form\Exception;

/**
 * Base InvalidArgumentException for the Form component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Form\Exception\ExceptionInterface
{
}
