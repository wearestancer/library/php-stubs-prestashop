<?php

namespace Symfony\Component\Form\ChoiceList\Factory;

/**
 * Default implementation of {@link ChoiceListFactoryInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DefaultChoiceListFactory implements \Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createListFromChoices($choices, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createListFromLoader(\Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface $loader, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createView(\Symfony\Component\Form\ChoiceList\ChoiceListInterface $list, $preferredChoices = null, $label = null, $index = null, $groupBy = null, $attr = null)
    {
    }
}
