<?php

namespace Symfony\Component\Form\Extension\Validator\ViolationMapper;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ViolationPathIterator extends \Symfony\Component\PropertyAccess\PropertyPathIterator
{
    public function __construct(\Symfony\Component\Form\Extension\Validator\ViolationMapper\ViolationPath $violationPath)
    {
    }
    public function mapsForm()
    {
    }
}
