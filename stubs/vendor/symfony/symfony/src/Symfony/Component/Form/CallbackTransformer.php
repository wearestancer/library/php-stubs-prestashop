<?php

namespace Symfony\Component\Form;

class CallbackTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * @param callable $transform        The forward transform callback
     * @param callable $reverseTransform The reverse transform callback
     */
    public function __construct(callable $transform, callable $reverseTransform)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function transform($data)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reverseTransform($data)
    {
    }
}
