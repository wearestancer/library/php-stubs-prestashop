<?php

namespace Symfony\Component\Form;

/**
 * Reverses a transformer.
 *
 * When the transform() method is called, the reversed transformer's
 * reverseTransform() method is called and vice versa.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ReversedTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    protected $reversedTransformer;
    public function __construct(\Symfony\Component\Form\DataTransformerInterface $reversedTransformer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
    }
}
