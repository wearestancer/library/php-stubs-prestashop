<?php

namespace Symfony\Component\Form\Extension\DataCollector;

/**
 * Collects and structures information about forms.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface FormDataCollectorInterface extends \Symfony\Component\HttpKernel\DataCollector\DataCollectorInterface
{
    /**
     * Stores configuration data of the given form and its children.
     */
    public function collectConfiguration(\Symfony\Component\Form\FormInterface $form);
    /**
     * Stores the default data of the given form and its children.
     */
    public function collectDefaultData(\Symfony\Component\Form\FormInterface $form);
    /**
     * Stores the submitted data of the given form and its children.
     */
    public function collectSubmittedData(\Symfony\Component\Form\FormInterface $form);
    /**
     * Stores the view variables of the given form view and its children.
     */
    public function collectViewVariables(\Symfony\Component\Form\FormView $view);
    /**
     * Specifies that the given objects represent the same conceptual form.
     */
    public function associateFormWithView(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $view);
    /**
     * Assembles the data collected about the given form and its children as
     * a tree-like data structure.
     *
     * The result can be queried using {@link getData()}.
     */
    public function buildPreliminaryFormTree(\Symfony\Component\Form\FormInterface $form);
    /**
     * Assembles the data collected about the given form and its children as
     * a tree-like data structure.
     *
     * The result can be queried using {@link getData()}.
     *
     * Contrary to {@link buildPreliminaryFormTree()}, a {@link FormView}
     * object has to be passed. The tree structure of this view object will be
     * used for structuring the resulting data. That means, if a child is
     * present in the view, but not in the form, it will be present in the final
     * data array anyway.
     *
     * When {@link FormView} instances are present in the view tree, for which
     * no corresponding {@link FormInterface} objects can be found in the form
     * tree, only the view data will be included in the result. If a
     * corresponding {@link FormInterface} exists otherwise, call
     * {@link associateFormWithView()} before calling this method.
     */
    public function buildFinalFormTree(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $view);
    /**
     * Returns all collected data.
     *
     * @return array|Data
     */
    public function getData();
}
