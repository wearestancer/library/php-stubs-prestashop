<?php

namespace Symfony\Component\Form;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormEvent extends \Symfony\Component\EventDispatcher\Event
{
    protected $data;
    /**
     * @param mixed $data The data
     */
    public function __construct(\Symfony\Component\Form\FormInterface $form, $data)
    {
    }
    /**
     * Returns the form at the source of the event.
     *
     * @return FormInterface
     */
    public function getForm()
    {
    }
    /**
     * Returns the data associated with this event.
     *
     * @return mixed
     */
    public function getData()
    {
    }
    /**
     * Allows updating with some filtered data.
     *
     * @param mixed $data
     */
    public function setData($data)
    {
    }
}
