<?php

namespace Symfony\Component\Form\ChoiceList\Factory;

/**
 * Caches the choice lists created by the decorated factory.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CachingFactoryDecorator implements \Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface, \Symfony\Contracts\Service\ResetInterface
{
    /**
     * Generates a SHA-256 hash for the given value.
     *
     * Optionally, a namespace string can be passed. Calling this method will
     * the same values, but different namespaces, will return different hashes.
     *
     * @param mixed $value The value to hash
     *
     * @return string The SHA-256 hash
     *
     * @internal
     */
    public static function generateHash($value, string $namespace = '') : string
    {
    }
    public function __construct(\Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface $decoratedFactory)
    {
    }
    /**
     * Returns the decorated factory.
     *
     * @return ChoiceListFactoryInterface The decorated factory
     */
    public function getDecoratedFactory()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createListFromChoices($choices, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createListFromLoader(\Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface $loader, $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createView(\Symfony\Component\Form\ChoiceList\ChoiceListInterface $list, $preferredChoices = null, $label = null, $index = null, $groupBy = null, $attr = null)
    {
    }
    public function reset()
    {
    }
}
