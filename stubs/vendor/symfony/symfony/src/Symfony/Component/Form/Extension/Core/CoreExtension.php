<?php

namespace Symfony\Component\Form\Extension\Core;

/**
 * Represents the main form extension, which loads the core functionality.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CoreExtension extends \Symfony\Component\Form\AbstractExtension
{
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct(\Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface $choiceListFactory = null, $translator = null)
    {
    }
    protected function loadTypes()
    {
    }
    protected function loadTypeExtensions()
    {
    }
}
