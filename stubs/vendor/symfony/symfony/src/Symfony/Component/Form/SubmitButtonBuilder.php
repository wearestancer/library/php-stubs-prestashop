<?php

namespace Symfony\Component\Form;

/**
 * A builder for {@link SubmitButton} instances.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class SubmitButtonBuilder extends \Symfony\Component\Form\ButtonBuilder
{
    /**
     * Creates the button.
     *
     * @return SubmitButton The button
     */
    public function getForm()
    {
    }
}
