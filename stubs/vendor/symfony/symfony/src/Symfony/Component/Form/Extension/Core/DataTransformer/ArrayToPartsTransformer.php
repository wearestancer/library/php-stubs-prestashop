<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ArrayToPartsTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(array $partMapping)
    {
    }
    public function transform($array)
    {
    }
    public function reverseTransform($array)
    {
    }
}
