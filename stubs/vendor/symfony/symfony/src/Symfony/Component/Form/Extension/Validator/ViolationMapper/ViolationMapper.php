<?php

namespace Symfony\Component\Form\Extension\Validator\ViolationMapper;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ViolationMapper implements \Symfony\Component\Form\Extension\Validator\ViolationMapper\ViolationMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function mapViolation(\Symfony\Component\Validator\ConstraintViolation $violation, \Symfony\Component\Form\FormInterface $form, $allowNonSynchronized = false)
    {
    }
}
