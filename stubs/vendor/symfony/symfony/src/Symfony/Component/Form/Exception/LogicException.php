<?php

namespace Symfony\Component\Form\Exception;

/**
 * Base LogicException for Form component.
 *
 * @author Alexander Kotynia <aleksander.kot@gmail.com>
 */
class LogicException extends \LogicException implements \Symfony\Component\Form\Exception\ExceptionInterface
{
}
