<?php

namespace Symfony\Component\Form;

/**
 * A type that should be converted into a {@link SubmitButton} instance.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface SubmitButtonTypeInterface extends \Symfony\Component\Form\FormTypeInterface
{
}
