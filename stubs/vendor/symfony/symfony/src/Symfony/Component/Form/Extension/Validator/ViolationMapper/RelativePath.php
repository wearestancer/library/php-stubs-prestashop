<?php

namespace Symfony\Component\Form\Extension\Validator\ViolationMapper;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RelativePath extends \Symfony\Component\PropertyAccess\PropertyPath
{
    public function __construct(\Symfony\Component\Form\FormInterface $root, string $propertyPath)
    {
    }
    /**
     * @return FormInterface
     */
    public function getRoot()
    {
    }
}
