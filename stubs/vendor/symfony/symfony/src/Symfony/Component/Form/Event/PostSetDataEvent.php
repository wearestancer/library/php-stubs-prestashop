<?php

namespace Symfony\Component\Form\Event;

/**
 * This event is dispatched at the end of the Form::setData() method.
 *
 * This event is mostly here for reading data after having pre-populated the form.
 *
 * @final since Symfony 4.4
 */
class PostSetDataEvent extends \Symfony\Component\Form\FormEvent
{
}
