<?php

namespace Symfony\Component\Form\ChoiceList;

/**
 * A list of choices with arbitrary data types.
 *
 * The user of this class is responsible for assigning string values to the
 * choices and for their uniqueness.
 * Both the choices and their values are passed to the constructor.
 * Each choice must have a corresponding value (with the same key) in
 * the values array.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ArrayChoiceList implements \Symfony\Component\Form\ChoiceList\ChoiceListInterface
{
    /**
     * The choices in the list.
     *
     * @var array
     */
    protected $choices;
    /**
     * The values indexed by the original keys.
     *
     * @var array
     */
    protected $structuredValues;
    /**
     * The original keys of the choices array.
     *
     * @var int[]|string[]
     */
    protected $originalKeys;
    protected $valueCallback;
    /**
     * Creates a list with the given choices and values.
     *
     * The given choice array must have the same array keys as the value array.
     *
     * @param iterable      $choices The selectable choices
     * @param callable|null $value   The callable for creating the value
     *                               for a choice. If `null` is passed,
     *                               incrementing integers are used as
     *                               values
     */
    public function __construct(iterable $choices, callable $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValues()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStructuredValues()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOriginalKeys()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoicesForValues(array $values)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValuesForChoices(array $choices)
    {
    }
    /**
     * Flattens an array into the given output variables.
     *
     * @param array      $choices          The array to flatten
     * @param callable   $value            The callable for generating choice values
     * @param array|null $choicesByValues  The flattened choices indexed by the
     *                                     corresponding values
     * @param array|null $keysByValues     The original keys indexed by the
     *                                     corresponding values
     * @param array|null $structuredValues The values indexed by the original keys
     *
     * @internal
     */
    protected function flatten(array $choices, callable $value, ?array &$choicesByValues, ?array &$keysByValues, ?array &$structuredValues)
    {
    }
}
