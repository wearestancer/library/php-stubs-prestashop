<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

class StringToFloatTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(int $scale = null)
    {
    }
    /**
     * @param mixed $value
     *
     * @return float|null
     */
    public function transform($value)
    {
    }
    /**
     * @param mixed $value
     *
     * @return string|null
     */
    public function reverseTransform($value)
    {
    }
}
