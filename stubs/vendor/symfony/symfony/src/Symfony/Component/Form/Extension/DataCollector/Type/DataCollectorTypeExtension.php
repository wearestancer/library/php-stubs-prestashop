<?php

namespace Symfony\Component\Form\Extension\DataCollector\Type;

/**
 * Type extension for collecting data of a form with this type.
 *
 * @author Robert Schönthal <robert.schoenthal@gmail.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DataCollectorTypeExtension extends \Symfony\Component\Form\AbstractTypeExtension
{
    public function __construct(\Symfony\Component\Form\Extension\DataCollector\FormDataCollectorInterface $dataCollector)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes() : iterable
    {
    }
}
