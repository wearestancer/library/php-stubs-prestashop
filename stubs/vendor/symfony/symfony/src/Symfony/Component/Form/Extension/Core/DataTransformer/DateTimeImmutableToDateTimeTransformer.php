<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a DateTimeImmutable object and a DateTime object.
 *
 * @author Valentin Udaltsov <udaltsov.valentin@gmail.com>
 */
final class DateTimeImmutableToDateTimeTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * Transforms a DateTimeImmutable into a DateTime object.
     *
     * @param \DateTimeImmutable|null $value A DateTimeImmutable object
     *
     * @return \DateTime|null A \DateTime object
     *
     * @throws TransformationFailedException If the given value is not a \DateTimeImmutable
     */
    public function transform($value) : ?\DateTime
    {
    }
    /**
     * Transforms a DateTime object into a DateTimeImmutable object.
     *
     * @param \DateTime|null $value A DateTime object
     *
     * @return \DateTimeImmutable|null A DateTimeImmutable object
     *
     * @throws TransformationFailedException If the given value is not a \DateTime
     */
    public function reverseTransform($value) : ?\DateTimeImmutable
    {
    }
}
