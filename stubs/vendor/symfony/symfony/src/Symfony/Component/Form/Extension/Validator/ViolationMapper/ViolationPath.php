<?php

namespace Symfony\Component\Form\Extension\Validator\ViolationMapper;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ViolationPath implements \IteratorAggregate, \Symfony\Component\PropertyAccess\PropertyPathInterface
{
    /**
     * Creates a new violation path from a string.
     *
     * @param string $violationPath The property path of a {@link \Symfony\Component\Validator\ConstraintViolation} object
     */
    public function __construct(string $violationPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getElements()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getElement($index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isProperty($index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isIndex($index)
    {
    }
    /**
     * Returns whether an element maps directly to a form.
     *
     * Consider the following violation path:
     *
     *     children[address].children[office].data.street
     *
     * In this example, "address" and "office" map to forms, while
     * "street does not.
     *
     * @param int $index The element index
     *
     * @return bool Whether the element maps to a form
     *
     * @throws OutOfBoundsException if the offset is invalid
     */
    public function mapsForm($index)
    {
    }
    /**
     * Returns a new iterator for this path.
     *
     * @return ViolationPathIterator
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
