<?php

namespace Symfony\Component\Form\Exception;

/**
 * Base BadMethodCallException for the Form component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class BadMethodCallException extends \BadMethodCallException implements \Symfony\Component\Form\Exception\ExceptionInterface
{
}
