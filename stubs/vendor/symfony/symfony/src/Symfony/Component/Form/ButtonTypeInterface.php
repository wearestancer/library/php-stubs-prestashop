<?php

namespace Symfony\Component\Form;

/**
 * A type that should be converted into a {@link Button} instance.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface ButtonTypeInterface extends \Symfony\Component\Form\FormTypeInterface
{
}
