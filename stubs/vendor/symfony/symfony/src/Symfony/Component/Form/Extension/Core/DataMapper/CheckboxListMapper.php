<?php

namespace Symfony\Component\Form\Extension\Core\DataMapper;

/**
 * Maps choices to/from checkbox forms.
 *
 * A {@link ChoiceListInterface} implementation is used to find the
 * corresponding string values for the choices. Each checkbox form whose "value"
 * option corresponds to any of the selected values is marked as selected.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CheckboxListMapper implements \Symfony\Component\Form\DataMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function mapDataToForms($choices, $checkboxes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function mapFormsToData($checkboxes, &$choices)
    {
    }
}
