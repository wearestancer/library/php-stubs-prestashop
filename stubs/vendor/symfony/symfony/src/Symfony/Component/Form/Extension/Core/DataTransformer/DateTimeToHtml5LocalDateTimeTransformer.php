<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * @author Franz Wilding <franz.wilding@me.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Fred Cox <mcfedr@gmail.com>
 */
class DateTimeToHtml5LocalDateTimeTransformer extends \Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer
{
    public const HTML5_FORMAT = 'Y-m-d\\TH:i:s';
    /**
     * Transforms a \DateTime into a local date and time string.
     *
     * According to the HTML standard, the input string of a datetime-local
     * input is an RFC3339 date followed by 'T', followed by an RFC3339 time.
     * https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-local-date-and-time-string
     *
     * @param \DateTime|\DateTimeInterface $dateTime A DateTime object
     *
     * @return string The formatted date
     *
     * @throws TransformationFailedException If the given value is not an
     *                                       instance of \DateTime or \DateTimeInterface
     */
    public function transform($dateTime)
    {
    }
    /**
     * Transforms a local date and time string into a \DateTime.
     *
     * When transforming back to DateTime the regex is slightly laxer, taking into
     * account rules for parsing a local date and time string
     * https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#parse-a-local-date-and-time-string
     *
     * @param string $dateTimeLocal Formatted string
     *
     * @return \DateTime|null Normalized date
     *
     * @throws TransformationFailedException If the given value is not a string,
     *                                       if the value could not be transformed
     */
    public function reverseTransform($dateTimeLocal)
    {
    }
}
