<?php

namespace Symfony\Component\Form\Event;

/**
 * This event is dispatched at the beginning of the Form::submit() method.
 *
 * It can be used to:
 *  - Change data from the request, before submitting the data to the form.
 *  - Add or remove form fields, before submitting the data to the form.
 *
 * @final since Symfony 4.4
 */
class PreSubmitEvent extends \Symfony\Component\Form\FormEvent
{
}
