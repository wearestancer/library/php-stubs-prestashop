<?php

namespace Symfony\Component\Form\Extension\Core\Type;

/**
 * A form button.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ButtonType extends \Symfony\Component\Form\Extension\Core\Type\BaseType implements \Symfony\Component\Form\ButtonTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
}
