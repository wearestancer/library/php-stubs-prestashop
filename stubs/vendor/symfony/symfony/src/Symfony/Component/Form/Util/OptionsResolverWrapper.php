<?php

namespace Symfony\Component\Form\Util;

/**
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 *
 * @internal
 */
class OptionsResolverWrapper extends \Symfony\Component\OptionsResolver\OptionsResolver
{
    /**
     * @return $this
     */
    public function setNormalizer($option, \Closure $normalizer) : self
    {
    }
    /**
     * @return $this
     */
    public function setAllowedValues($option, $allowedValues) : self
    {
    }
    /**
     * @return $this
     */
    public function addAllowedValues($option, $allowedValues) : self
    {
    }
    /**
     * @param string|array $allowedTypes
     *
     * @return $this
     */
    public function setAllowedTypes($option, $allowedTypes) : self
    {
    }
    /**
     * @param string|array $allowedTypes
     *
     * @return $this
     */
    public function addAllowedTypes($option, $allowedTypes) : self
    {
    }
    public function resolve(array $options = []) : array
    {
    }
    public function getUndefinedOptions() : array
    {
    }
}
