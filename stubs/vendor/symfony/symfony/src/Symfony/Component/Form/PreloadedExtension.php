<?php

namespace Symfony\Component\Form;

/**
 * A form extension with preloaded types, type extensions and type guessers.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class PreloadedExtension implements \Symfony\Component\Form\FormExtensionInterface
{
    /**
     * Creates a new preloaded extension.
     *
     * @param FormTypeInterface[]            $types          The types that the extension should support
     * @param FormTypeExtensionInterface[][] $typeExtensions The type extensions that the extension should support
     */
    public function __construct(array $types, array $typeExtensions, \Symfony\Component\Form\FormTypeGuesserInterface $typeGuesser = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getType($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasType($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeExtensions($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasTypeExtensions($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeGuesser()
    {
    }
}
