<?php

namespace Symfony\Component\Form\Extension\Validator;

class ValidatorTypeGuesser implements \Symfony\Component\Form\FormTypeGuesserInterface
{
    public function __construct(\Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface $metadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessType($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessRequired($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessMaxLength($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessPattern($class, $property)
    {
    }
    /**
     * Guesses a field class name for a given constraint.
     *
     * @return TypeGuess|null The guessed field class and options
     */
    public function guessTypeForConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Guesses whether a field is required based on the given constraint.
     *
     * @return ValueGuess|null The guess whether the field is required
     */
    public function guessRequiredForConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Guesses a field's maximum length based on the given constraint.
     *
     * @return ValueGuess|null The guess for the maximum length
     */
    public function guessMaxLengthForConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Guesses a field's pattern based on the given constraint.
     *
     * @return ValueGuess|null The guess for the pattern
     */
    public function guessPatternForConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Iterates over the constraints of a property, executes a constraints on
     * them and returns the best guess.
     *
     * @param string   $class        The class to read the constraints from
     * @param string   $property     The property for which to find constraints
     * @param \Closure $closure      The closure that returns a guess
     *                               for a given constraint
     * @param mixed    $defaultValue The default value assumed if no other value
     *                               can be guessed
     *
     * @return Guess|null The guessed value with the highest confidence
     */
    protected function guess($class, $property, \Closure $closure, $defaultValue = null)
    {
    }
}
