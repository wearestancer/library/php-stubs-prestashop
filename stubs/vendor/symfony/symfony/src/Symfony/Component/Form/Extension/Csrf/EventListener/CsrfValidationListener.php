<?php

namespace Symfony\Component\Form\Extension\Csrf\EventListener;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CsrfValidationListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
    }
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct(string $fieldName, \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $tokenManager, string $tokenId, string $errorMessage, $translator = null, string $translationDomain = null, \Symfony\Component\Form\Util\ServerParams $serverParams = null)
    {
    }
    public function preSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
}
