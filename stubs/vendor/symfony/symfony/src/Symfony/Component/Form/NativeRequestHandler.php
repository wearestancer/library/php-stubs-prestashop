<?php

namespace Symfony\Component\Form;

/**
 * A request handler using PHP super globals $_GET, $_POST and $_SERVER.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class NativeRequestHandler implements \Symfony\Component\Form\RequestHandlerInterface
{
    public function __construct(\Symfony\Component\Form\Util\ServerParams $params = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws Exception\UnexpectedTypeException If the $request is not null
     */
    public function handleRequest(\Symfony\Component\Form\FormInterface $form, $request = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFileUpload($data)
    {
    }
    /**
     * @return int|null
     */
    public function getUploadFileError($data)
    {
    }
}
