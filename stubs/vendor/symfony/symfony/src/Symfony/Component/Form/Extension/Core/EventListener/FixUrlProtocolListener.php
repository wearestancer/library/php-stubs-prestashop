<?php

namespace Symfony\Component\Form\Extension\Core\EventListener;

/**
 * Adds a protocol to a URL if it doesn't already have one.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FixUrlProtocolListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param string|null $defaultProtocol The URL scheme to add when there is none or null to not modify the data
     */
    public function __construct(?string $defaultProtocol = 'http')
    {
    }
    public function onSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
