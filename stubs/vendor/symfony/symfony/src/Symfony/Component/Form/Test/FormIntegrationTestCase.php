<?php

namespace Symfony\Component\Form\Test;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class FormIntegrationTestCase extends \PHPUnit\Framework\TestCase
{
    use \Symfony\Component\Form\Test\ForwardCompatTestTrait;
    /**
     * @var FormFactoryInterface
     */
    protected $factory;
    protected function getExtensions()
    {
    }
    protected function getTypeExtensions()
    {
    }
    protected function getTypes()
    {
    }
    protected function getTypeGuessers()
    {
    }
}
