<?php

namespace Symfony\Component\Form;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormView implements \ArrayAccess, \IteratorAggregate, \Countable
{
    /**
     * The variables assigned to this view.
     */
    public $vars = ['value' => null, 'attr' => []];
    /**
     * The parent view.
     */
    public $parent;
    /**
     * The child views.
     *
     * @var FormView[]
     */
    public $children = [];
    public function __construct(self $parent = null)
    {
    }
    /**
     * Returns whether the view was already rendered.
     *
     * @return bool Whether this view's widget is rendered
     */
    public function isRendered()
    {
    }
    /**
     * Marks the view as rendered.
     *
     * @return $this
     */
    public function setRendered()
    {
    }
    /**
     * @return bool
     */
    public function isMethodRendered()
    {
    }
    public function setMethodRendered()
    {
    }
    /**
     * Returns a child by name (implements \ArrayAccess).
     *
     * @param string $name The child name
     *
     * @return self The child view
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($name)
    {
    }
    /**
     * Returns whether the given child exists (implements \ArrayAccess).
     *
     * @param string $name The child name
     *
     * @return bool Whether the child view exists
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($name)
    {
    }
    /**
     * Implements \ArrayAccess.
     *
     * @return void
     *
     * @throws BadMethodCallException always as setting a child by name is not allowed
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($name, $value)
    {
    }
    /**
     * Removes a child (implements \ArrayAccess).
     *
     * @param string $name The child name
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($name)
    {
    }
    /**
     * Returns an iterator to iterate over children (implements \IteratorAggregate).
     *
     * @return \ArrayIterator<string, FormView> The iterator
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * Implements \Countable.
     *
     * @return int The number of children views
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
}
