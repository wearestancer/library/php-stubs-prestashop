<?php

namespace Symfony\Component\Form\Exception;

/**
 * Base RuntimeException for the Form component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Form\Exception\ExceptionInterface
{
}
