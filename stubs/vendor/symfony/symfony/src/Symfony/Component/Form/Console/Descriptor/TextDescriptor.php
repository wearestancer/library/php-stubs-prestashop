<?php

namespace Symfony\Component\Form\Console\Descriptor;

/**
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 *
 * @internal
 */
class TextDescriptor extends \Symfony\Component\Form\Console\Descriptor\Descriptor
{
    public function __construct(\Symfony\Component\HttpKernel\Debug\FileLinkFormatter $fileLinkFormatter = null)
    {
    }
    protected function describeDefaults(array $options)
    {
    }
    protected function describeResolvedFormType(\Symfony\Component\Form\ResolvedFormTypeInterface $resolvedFormType, array $options = [])
    {
    }
    protected function describeOption(\Symfony\Component\OptionsResolver\OptionsResolver $optionsResolver, array $options)
    {
    }
}
