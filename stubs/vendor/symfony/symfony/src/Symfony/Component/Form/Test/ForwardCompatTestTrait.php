<?php

namespace Symfony\Component\Form\Test;

/**
 * @internal
 */
trait ForwardCompatTestTrait
{
    private function doSetUp() : void
    {
    }
    private function doTearDown() : void
    {
    }
    protected function setUp() : void
    {
    }
    protected function tearDown() : void
    {
    }
}
