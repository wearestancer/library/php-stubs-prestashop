<?php

namespace Symfony\Component\Form;

/**
 * The default implementation of FormFactoryBuilderInterface.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormFactoryBuilder implements \Symfony\Component\Form\FormFactoryBuilderInterface
{
    public function __construct(bool $forceCoreExtension = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setResolvedTypeFactory(\Symfony\Component\Form\ResolvedFormTypeFactoryInterface $resolvedTypeFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addExtension(\Symfony\Component\Form\FormExtensionInterface $extension)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addExtensions(array $extensions)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addType(\Symfony\Component\Form\FormTypeInterface $type)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addTypes(array $types)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addTypeExtension(\Symfony\Component\Form\FormTypeExtensionInterface $typeExtension)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addTypeExtensions(array $typeExtensions)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addTypeGuesser(\Symfony\Component\Form\FormTypeGuesserInterface $typeGuesser)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addTypeGuessers(array $typeGuessers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormFactory()
    {
    }
}
