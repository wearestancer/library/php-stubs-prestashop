<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a normalized format (integer or float) and a percentage value.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Florian Eckerstorfer <florian@eckerstorfer.org>
 */
class PercentToLocalizedStringTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public const FRACTIONAL = 'fractional';
    public const INTEGER = 'integer';
    protected static $types = [self::FRACTIONAL, self::INTEGER];
    /**
     * @see self::$types for a list of supported types
     *
     * @throws UnexpectedTypeException if the given value of type is unknown
     */
    public function __construct(int $scale = null, string $type = null)
    {
    }
    /**
     * Transforms between a normalized format (integer or float) into a percentage value.
     *
     * @param int|float $value Normalized value
     *
     * @return string Percentage value
     *
     * @throws TransformationFailedException if the given value is not numeric or
     *                                       if the value could not be transformed
     */
    public function transform($value)
    {
    }
    /**
     * Transforms between a percentage value into a normalized format (integer or float).
     *
     * @param string $value Percentage value
     *
     * @return int|float Normalized value
     *
     * @throws TransformationFailedException if the given value is not a string or
     *                                       if the value could not be transformed
     */
    public function reverseTransform($value)
    {
    }
    /**
     * Returns a preconfigured \NumberFormatter instance.
     *
     * @return \NumberFormatter
     */
    protected function getNumberFormatter()
    {
    }
}
