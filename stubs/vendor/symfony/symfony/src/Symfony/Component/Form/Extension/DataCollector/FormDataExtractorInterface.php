<?php

namespace Symfony\Component\Form\Extension\DataCollector;

/**
 * Extracts arrays of information out of forms.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface FormDataExtractorInterface
{
    /**
     * Extracts the configuration data of a form.
     *
     * @return array Information about the form's configuration
     */
    public function extractConfiguration(\Symfony\Component\Form\FormInterface $form);
    /**
     * Extracts the default data of a form.
     *
     * @return array Information about the form's default data
     */
    public function extractDefaultData(\Symfony\Component\Form\FormInterface $form);
    /**
     * Extracts the submitted data of a form.
     *
     * @return array Information about the form's submitted data
     */
    public function extractSubmittedData(\Symfony\Component\Form\FormInterface $form);
    /**
     * Extracts the view variables of a form.
     *
     * @return array Information about the view's variables
     */
    public function extractViewVariables(\Symfony\Component\Form\FormView $view);
}
