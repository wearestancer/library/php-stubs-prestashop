<?php

namespace Symfony\Component\Form\Extension\Core\EventListener;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
class TransformationFailureListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct($translator = null)
    {
    }
    public static function getSubscribedEvents()
    {
    }
    public function convertTransformationFailureToFormError(\Symfony\Component\Form\FormEvent $event)
    {
    }
}
