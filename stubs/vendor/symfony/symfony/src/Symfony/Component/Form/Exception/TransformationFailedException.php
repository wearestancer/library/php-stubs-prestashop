<?php

namespace Symfony\Component\Form\Exception;

/**
 * Indicates a value transformation error.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class TransformationFailedException extends \Symfony\Component\Form\Exception\RuntimeException
{
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null, string $invalidMessage = null, array $invalidMessageParameters = [])
    {
    }
    /**
     * Sets the message that will be shown to the user.
     *
     * @param string|null $invalidMessage           The message or message key
     * @param array       $invalidMessageParameters Data to be passed into the translator
     */
    public function setInvalidMessage(string $invalidMessage = null, array $invalidMessageParameters = []) : void
    {
    }
    public function getInvalidMessage() : ?string
    {
    }
    public function getInvalidMessageParameters() : array
    {
    }
}
