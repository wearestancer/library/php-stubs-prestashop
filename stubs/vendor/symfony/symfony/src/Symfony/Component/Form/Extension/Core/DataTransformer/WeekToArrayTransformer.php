<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between an ISO 8601 week date string and an array.
 *
 * @author Damien Fayet <damienf1521@gmail.com>
 */
class WeekToArrayTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    /**
     * Transforms a string containing an ISO 8601 week date into an array.
     *
     * @param string|null $value A week date string
     *
     * @return array A value containing year and week
     *
     * @throws TransformationFailedException If the given value is not a string,
     *                                       or if the given value does not follow the right format
     */
    public function transform($value)
    {
    }
    /**
     * Transforms an array into a week date string.
     *
     * @param array $value An array containing a year and a week number
     *
     * @return string|null A week date string following the format Y-\WW
     *
     * @throws TransformationFailedException If the given value can not be merged in a valid week date string,
     *                                       or if the obtained week date does not exists
     */
    public function reverseTransform($value)
    {
    }
}
