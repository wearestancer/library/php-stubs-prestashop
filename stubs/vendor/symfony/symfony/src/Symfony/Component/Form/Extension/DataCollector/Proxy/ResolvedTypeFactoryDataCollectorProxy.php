<?php

namespace Symfony\Component\Form\Extension\DataCollector\Proxy;

/**
 * Proxy that wraps resolved types into {@link ResolvedTypeDataCollectorProxy}
 * instances.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResolvedTypeFactoryDataCollectorProxy implements \Symfony\Component\Form\ResolvedFormTypeFactoryInterface
{
    public function __construct(\Symfony\Component\Form\ResolvedFormTypeFactoryInterface $proxiedFactory, \Symfony\Component\Form\Extension\DataCollector\FormDataCollectorInterface $dataCollector)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createResolvedType(\Symfony\Component\Form\FormTypeInterface $type, array $typeExtensions, \Symfony\Component\Form\ResolvedFormTypeInterface $parent = null)
    {
    }
}
