<?php

namespace Symfony\Component\Form;

class FormTypeGuesserChain implements \Symfony\Component\Form\FormTypeGuesserInterface
{
    /**
     * @param FormTypeGuesserInterface[] $guessers
     *
     * @throws UnexpectedTypeException if any guesser does not implement FormTypeGuesserInterface
     */
    public function __construct(iterable $guessers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessType($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessRequired($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessMaxLength($class, $property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessPattern($class, $property)
    {
    }
}
