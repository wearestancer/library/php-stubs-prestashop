<?php

namespace Symfony\Component\Form\Test;

abstract class TypeTestCase extends \Symfony\Component\Form\Test\FormIntegrationTestCase
{
    use \Symfony\Component\Form\Test\ForwardCompatTestTrait;
    /**
     * @var FormBuilder
     */
    protected $builder;
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;
    protected function getExtensions()
    {
    }
    public static function assertDateTimeEquals(\DateTime $expected, \DateTime $actual)
    {
    }
    public static function assertDateIntervalEquals(\DateInterval $expected, \DateInterval $actual)
    {
    }
}
