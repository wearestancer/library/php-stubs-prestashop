<?php

namespace Symfony\Component\Form\Extension\Csrf\Type;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormTypeCsrfExtension extends \Symfony\Component\Form\AbstractTypeExtension
{
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct(\Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $defaultTokenManager, bool $defaultEnabled = true, string $defaultFieldName = '_token', $translator = null, string $translationDomain = null, \Symfony\Component\Form\Util\ServerParams $serverParams = null)
    {
    }
    /**
     * Adds a CSRF field to the form when the CSRF protection is enabled.
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * Adds a CSRF field to the root form view.
     */
    public function finishView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes() : iterable
    {
    }
}
