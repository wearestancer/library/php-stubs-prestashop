<?php

namespace Symfony\Component\Form\ChoiceList;

/**
 * A choice list that loads its choices lazily.
 *
 * The choices are fetched using a {@link ChoiceLoaderInterface} instance.
 * If only {@link getChoicesForValues()} or {@link getValuesForChoices()} is
 * called, the choice list is only loaded partially for improved performance.
 *
 * Once {@link getChoices()} or {@link getValues()} is called, the list is
 * loaded fully.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class LazyChoiceList implements \Symfony\Component\Form\ChoiceList\ChoiceListInterface
{
    /**
     * Creates a lazily-loaded list using the given loader.
     *
     * Optionally, a callable can be passed for generating the choice values.
     * The callable receives the choice as first and the array key as the second
     * argument.
     *
     * @param callable|null $value The callable generating the choice values
     */
    public function __construct(\Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface $loader, callable $value = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValues()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStructuredValues()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOriginalKeys()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getChoicesForValues(array $values)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValuesForChoices(array $choices)
    {
    }
}
