<?php

namespace Symfony\Component\Form\Command;

/**
 * A console command for retrieving information about form types.
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class DebugCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'debug:form';
    public function __construct(\Symfony\Component\Form\FormRegistryInterface $formRegistry, array $namespaces = ['Symfony\\Component\\Form\\Extension\\Core\\Type'], array $types = [], array $extensions = [], array $guessers = [], \Symfony\Component\HttpKernel\Debug\FileLinkFormatter $fileLinkFormatter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
