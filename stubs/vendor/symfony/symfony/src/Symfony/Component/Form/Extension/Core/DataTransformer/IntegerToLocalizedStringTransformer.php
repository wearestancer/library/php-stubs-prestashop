<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between an integer and a localized number with grouping
 * (each thousand) and comma separators.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IntegerToLocalizedStringTransformer extends \Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer
{
    /**
     * Constructs a transformer.
     *
     * @param bool        $grouping     Whether thousands should be grouped
     * @param int         $roundingMode One of the ROUND_ constants in this class
     * @param string|null $locale       locale used for transforming
     */
    public function __construct($grouping = false, $roundingMode = self::ROUND_DOWN, $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
    }
    /**
     * @internal
     */
    protected function castParsedValue($value)
    {
    }
}
