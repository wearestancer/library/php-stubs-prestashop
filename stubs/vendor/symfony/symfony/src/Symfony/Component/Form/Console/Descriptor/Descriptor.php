<?php

namespace Symfony\Component\Form\Console\Descriptor;

/**
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 *
 * @internal
 */
abstract class Descriptor implements \Symfony\Component\Console\Descriptor\DescriptorInterface
{
    /** @var OutputStyle */
    protected $output;
    protected $type;
    protected $ownOptions = [];
    protected $overriddenOptions = [];
    protected $parentOptions = [];
    protected $extensionOptions = [];
    protected $requiredOptions = [];
    protected $parents = [];
    protected $extensions = [];
    /**
     * {@inheritdoc}
     */
    public function describe(\Symfony\Component\Console\Output\OutputInterface $output, $object, array $options = [])
    {
    }
    protected abstract function describeDefaults(array $options);
    protected abstract function describeResolvedFormType(\Symfony\Component\Form\ResolvedFormTypeInterface $resolvedFormType, array $options = []);
    protected abstract function describeOption(\Symfony\Component\OptionsResolver\OptionsResolver $optionsResolver, array $options);
    protected function collectOptions(\Symfony\Component\Form\ResolvedFormTypeInterface $type)
    {
    }
    protected function getOptionDefinition(\Symfony\Component\OptionsResolver\OptionsResolver $optionsResolver, $option)
    {
    }
    protected function filterOptionsByDeprecated(\Symfony\Component\Form\ResolvedFormTypeInterface $type)
    {
    }
}
