<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

abstract class BaseDateTimeTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    protected static $formats = [\IntlDateFormatter::NONE, \IntlDateFormatter::FULL, \IntlDateFormatter::LONG, \IntlDateFormatter::MEDIUM, \IntlDateFormatter::SHORT];
    protected $inputTimezone;
    protected $outputTimezone;
    /**
     * @param string|null $inputTimezone  The name of the input timezone
     * @param string|null $outputTimezone The name of the output timezone
     *
     * @throws InvalidArgumentException if a timezone is not valid
     */
    public function __construct(string $inputTimezone = null, string $outputTimezone = null)
    {
    }
}
