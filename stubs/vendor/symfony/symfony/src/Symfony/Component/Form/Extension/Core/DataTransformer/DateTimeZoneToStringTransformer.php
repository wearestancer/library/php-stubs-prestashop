<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a timezone identifier string and a DateTimeZone object.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
class DateTimeZoneToStringTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(bool $multiple = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function transform($dateTimeZone)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
    }
}
