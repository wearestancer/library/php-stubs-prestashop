<?php

namespace Symfony\Component\Form\ChoiceList\View;

/**
 * Represents a choice list in templates.
 *
 * A choice list contains choices and optionally preferred choices which are
 * displayed in the very beginning of the list. Both choices and preferred
 * choices may be grouped in {@link ChoiceGroupView} instances.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ChoiceListView
{
    public $choices;
    public $preferredChoices;
    /**
     * Creates a new choice list view.
     *
     * @param ChoiceGroupView[]|ChoiceView[] $choices          The choice views
     * @param ChoiceGroupView[]|ChoiceView[] $preferredChoices the preferred choice views
     */
    public function __construct(array $choices = [], array $preferredChoices = [])
    {
    }
    /**
     * Returns whether a placeholder is in the choices.
     *
     * A placeholder must be the first child element, not be in a group and have an empty value.
     *
     * @return bool
     */
    public function hasPlaceholder()
    {
    }
}
