<?php

namespace Symfony\Component\Form;

/**
 * Wraps errors in forms.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormError
{
    protected $messageTemplate;
    protected $messageParameters;
    protected $messagePluralization;
    /**
     * Any array key in $messageParameters will be used as a placeholder in
     * $messageTemplate.
     *
     * @param string      $message              The translated error message
     * @param string|null $messageTemplate      The template for the error message
     * @param array       $messageParameters    The parameters that should be
     *                                          substituted in the message template
     * @param int|null    $messagePluralization The value for error message pluralization
     * @param mixed       $cause                The cause of the error
     *
     * @see \Symfony\Component\Translation\Translator
     */
    public function __construct(?string $message, string $messageTemplate = null, array $messageParameters = [], int $messagePluralization = null, $cause = null)
    {
    }
    /**
     * Returns the error message.
     *
     * @return string
     */
    public function getMessage()
    {
    }
    /**
     * Returns the error message template.
     *
     * @return string
     */
    public function getMessageTemplate()
    {
    }
    /**
     * Returns the parameters to be inserted in the message template.
     *
     * @return array
     */
    public function getMessageParameters()
    {
    }
    /**
     * Returns the value for error message pluralization.
     *
     * @return int|null
     */
    public function getMessagePluralization()
    {
    }
    /**
     * Returns the cause of this error.
     *
     * @return mixed The cause of this error
     */
    public function getCause()
    {
    }
    /**
     * Sets the form that caused this error.
     *
     * This method must only be called once.
     *
     * @throws BadMethodCallException If the method is called more than once
     */
    public function setOrigin(\Symfony\Component\Form\FormInterface $origin)
    {
    }
    /**
     * Returns the form that caused this error.
     *
     * @return FormInterface|null The form that caused this error
     */
    public function getOrigin()
    {
    }
}
