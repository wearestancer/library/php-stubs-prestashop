<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a normalized time and a localized time string.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Florian Eckerstorfer <florian@eckerstorfer.org>
 */
class DateTimeToLocalizedStringTransformer extends \Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer
{
    /**
     * @see BaseDateTimeTransformer::formats for available format options
     *
     * @param string|null $inputTimezone  The name of the input timezone
     * @param string|null $outputTimezone The name of the output timezone
     * @param int|null    $dateFormat     The date format
     * @param int|null    $timeFormat     The time format
     * @param int         $calendar       One of the \IntlDateFormatter calendar constants
     * @param string|null $pattern        A pattern to pass to \IntlDateFormatter
     *
     * @throws UnexpectedTypeException If a format is not supported or if a timezone is not a string
     */
    public function __construct(string $inputTimezone = null, string $outputTimezone = null, int $dateFormat = null, int $timeFormat = null, int $calendar = \IntlDateFormatter::GREGORIAN, string $pattern = null)
    {
    }
    /**
     * Transforms a normalized date into a localized date string/array.
     *
     * @param \DateTimeInterface $dateTime A DateTimeInterface object
     *
     * @return string Localized date string
     *
     * @throws TransformationFailedException if the given value is not a \DateTimeInterface
     *                                       or if the date could not be transformed
     */
    public function transform($dateTime)
    {
    }
    /**
     * Transforms a localized date string/array into a normalized date.
     *
     * @param string|array $value Localized date string/array
     *
     * @return \DateTime|null Normalized date
     *
     * @throws TransformationFailedException if the given value is not a string,
     *                                       if the date could not be parsed
     */
    public function reverseTransform($value)
    {
    }
    /**
     * Returns a preconfigured IntlDateFormatter instance.
     *
     * @param bool $ignoreTimezone Use UTC regardless of the configured timezone
     *
     * @return \IntlDateFormatter
     *
     * @throws TransformationFailedException in case the date formatter can not be constructed
     */
    protected function getIntlDateFormatter($ignoreTimezone = false)
    {
    }
    /**
     * Checks if the pattern contains only a date.
     *
     * @return bool
     */
    protected function isPatternDateOnly()
    {
    }
}
