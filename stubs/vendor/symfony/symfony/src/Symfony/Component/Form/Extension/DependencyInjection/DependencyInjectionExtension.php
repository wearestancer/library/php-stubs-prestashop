<?php

namespace Symfony\Component\Form\Extension\DependencyInjection;

class DependencyInjectionExtension implements \Symfony\Component\Form\FormExtensionInterface
{
    /**
     * @param iterable[] $typeExtensionServices
     */
    public function __construct(\Psr\Container\ContainerInterface $typeContainer, array $typeExtensionServices, iterable $guesserServices)
    {
    }
    public function getType($name)
    {
    }
    public function hasType($name)
    {
    }
    public function getTypeExtensions($name)
    {
    }
    public function hasTypeExtensions($name)
    {
    }
    public function getTypeGuesser()
    {
    }
}
