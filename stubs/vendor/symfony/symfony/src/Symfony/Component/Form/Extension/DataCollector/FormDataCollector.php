<?php

namespace Symfony\Component\Form\Extension\DataCollector;

/**
 * Data collector for {@link FormInterface} instances.
 *
 * @author Robert Schönthal <robert.schoenthal@gmail.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @final since Symfony 4.3
 */
class FormDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\Form\Extension\DataCollector\FormDataCollectorInterface
{
    public function __construct(\Symfony\Component\Form\Extension\DataCollector\FormDataExtractorInterface $dataExtractor)
    {
    }
    /**
     * Does nothing. The data is collected during the form event listeners.
     *
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function associateFormWithView(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $view)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectConfiguration(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectDefaultData(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectSubmittedData(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function collectViewVariables(\Symfony\Component\Form\FormView $view)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildPreliminaryFormTree(\Symfony\Component\Form\FormInterface $form)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildFinalFormTree(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\Form\FormView $view)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * @internal
     */
    public function __sleep() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getCasters()
    {
    }
}
