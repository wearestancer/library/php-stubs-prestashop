<?php

namespace Symfony\Component\Form\Extension\Validator\Constraints;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($form, \Symfony\Component\Validator\Constraint $formConstraint)
    {
    }
}
