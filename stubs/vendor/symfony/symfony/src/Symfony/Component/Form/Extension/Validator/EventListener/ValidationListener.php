<?php

namespace Symfony\Component\Form\Extension\Validator\EventListener;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ValidationListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator, \Symfony\Component\Form\Extension\Validator\ViolationMapper\ViolationMapperInterface $violationMapper)
    {
    }
    public function validateForm(\Symfony\Component\Form\FormEvent $event)
    {
    }
}
