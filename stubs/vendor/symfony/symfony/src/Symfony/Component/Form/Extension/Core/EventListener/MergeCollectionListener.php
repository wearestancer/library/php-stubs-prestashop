<?php

namespace Symfony\Component\Form\Extension\Core\EventListener;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class MergeCollectionListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param bool $allowAdd    Whether values might be added to the collection
     * @param bool $allowDelete Whether values might be removed from the collection
     */
    public function __construct(bool $allowAdd = false, bool $allowDelete = false)
    {
    }
    public static function getSubscribedEvents()
    {
    }
    public function onSubmit(\Symfony\Component\Form\FormEvent $event)
    {
    }
}
