<?php

namespace Symfony\Component\Form;

/**
 * Iterates over the errors of a form.
 *
 * This class supports recursive iteration. In order to iterate recursively,
 * pass a structure of {@link FormError} and {@link FormErrorIterator} objects
 * to the $errors constructor argument.
 *
 * You can also wrap the iterator into a {@link \RecursiveIteratorIterator} to
 * flatten the recursive structure into a flat list of errors.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormErrorIterator implements \RecursiveIterator, \SeekableIterator, \ArrayAccess, \Countable
{
    /**
     * The prefix used for indenting nested error messages.
     */
    public const INDENTATION = '    ';
    /**
     * @param FormError[]|self[] $errors An array of form errors and instances
     *                                   of FormErrorIterator
     *
     * @throws InvalidArgumentException If the errors are invalid
     */
    public function __construct(\Symfony\Component\Form\FormInterface $form, array $errors)
    {
    }
    /**
     * Returns all iterated error messages as string.
     *
     * @return string The iterated error messages
     */
    public function __toString()
    {
    }
    /**
     * Returns the iterated form.
     *
     * @return FormInterface The form whose errors are iterated by this object
     */
    public function getForm()
    {
    }
    /**
     * Returns the current element of the iterator.
     *
     * @return FormError|self An error or an iterator containing nested errors
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
    }
    /**
     * Advances the iterator to the next position.
     */
    #[\ReturnTypeWillChange]
    public function next()
    {
    }
    /**
     * Returns the current position of the iterator.
     *
     * @return int The 0-indexed position
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
    }
    /**
     * Returns whether the iterator's position is valid.
     *
     * @return bool Whether the iterator is valid
     */
    #[\ReturnTypeWillChange]
    public function valid()
    {
    }
    /**
     * Sets the iterator's position to the beginning.
     *
     * This method detects if errors have been added to the form since the
     * construction of the iterator.
     */
    #[\ReturnTypeWillChange]
    public function rewind()
    {
    }
    /**
     * Returns whether a position exists in the iterator.
     *
     * @param int $position The position
     *
     * @return bool Whether that position exists
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($position)
    {
    }
    /**
     * Returns the element at a position in the iterator.
     *
     * @param int $position The position
     *
     * @return FormError|FormErrorIterator The element at the given position
     *
     * @throws OutOfBoundsException If the given position does not exist
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($position)
    {
    }
    /**
     * Unsupported method.
     *
     * @return void
     *
     * @throws BadMethodCallException
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($position, $value)
    {
    }
    /**
     * Unsupported method.
     *
     * @return void
     *
     * @throws BadMethodCallException
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($position)
    {
    }
    /**
     * Returns whether the current element of the iterator can be recursed
     * into.
     *
     * @return bool Whether the current element is an instance of this class
     */
    #[\ReturnTypeWillChange]
    public function hasChildren()
    {
    }
    /**
     * Alias of {@link current()}.
     *
     * @return self
     */
    #[\ReturnTypeWillChange]
    public function getChildren()
    {
    }
    /**
     * Returns the number of elements in the iterator.
     *
     * Note that this is not the total number of errors, if the constructor
     * parameter $deep was set to true! In that case, you should wrap the
     * iterator into a {@link \RecursiveIteratorIterator} with the standard mode
     * {@link \RecursiveIteratorIterator::LEAVES_ONLY} and count the result.
     *
     *     $iterator = new \RecursiveIteratorIterator($form->getErrors(true));
     *     $count = count(iterator_to_array($iterator));
     *
     * Alternatively, set the constructor argument $flatten to true as well.
     *
     *     $count = count($form->getErrors(true, true));
     *
     * @return int The number of iterated elements
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * Sets the position of the iterator.
     *
     * @param int $position The new position
     *
     * @return void
     *
     * @throws OutOfBoundsException If the position is invalid
     */
    #[\ReturnTypeWillChange]
    public function seek($position)
    {
    }
    /**
     * Creates iterator for errors with specific codes.
     *
     * @param string|string[] $codes The codes to find
     *
     * @return static new instance which contains only specific errors
     */
    public function findByCodes($codes)
    {
    }
}
