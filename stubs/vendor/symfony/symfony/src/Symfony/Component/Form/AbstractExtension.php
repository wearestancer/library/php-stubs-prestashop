<?php

namespace Symfony\Component\Form;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class AbstractExtension implements \Symfony\Component\Form\FormExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getType($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasType($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeExtensions($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasTypeExtensions($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeGuesser()
    {
    }
    /**
     * Registers the types.
     *
     * @return FormTypeInterface[] An array of FormTypeInterface instances
     */
    protected function loadTypes()
    {
    }
    /**
     * Registers the type extensions.
     *
     * @return FormTypeExtensionInterface[] An array of FormTypeExtensionInterface instances
     */
    protected function loadTypeExtensions()
    {
    }
    /**
     * Registers the type guesser.
     *
     * @return FormTypeGuesserInterface|null
     */
    protected function loadTypeGuesser()
    {
    }
}
