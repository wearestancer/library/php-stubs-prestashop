<?php

namespace Symfony\Component\Form;

/**
 * A basic form configuration.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FormConfigBuilder implements \Symfony\Component\Form\FormConfigBuilderInterface
{
    protected $locked = false;
    /**
     * Creates an empty form configuration.
     *
     * @param string|null $name      The form name
     * @param string|null $dataClass The class of the form's data
     *
     * @throws InvalidArgumentException if the data class is not a valid class or if
     *                                  the name contains invalid characters
     */
    public function __construct(?string $name, ?string $dataClass, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addEventListener($eventName, $listener, $priority = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addEventSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addViewTransformer(\Symfony\Component\Form\DataTransformerInterface $viewTransformer, $forcePrepend = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetViewTransformers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addModelTransformer(\Symfony\Component\Form\DataTransformerInterface $modelTransformer, $forceAppend = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetModelTransformers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEventDispatcher()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyPath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMapped()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getByReference()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInheritData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCompound()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getViewTransformers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getModelTransformers()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDataMapper()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDisabled()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getErrorBubbling()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEmptyData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAttributes()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasAttribute($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAttribute($name, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDataClass()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDataLocked()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormFactory()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAction()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequestHandler()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAutoInitialize()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasOption($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOption($name, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAttribute($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAttributes(array $attributes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setDataMapper(\Symfony\Component\Form\DataMapperInterface $dataMapper = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setDisabled($disabled)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setEmptyData($emptyData)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setErrorBubbling($errorBubbling)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setRequired($required)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPropertyPath($propertyPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMapped($mapped)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setByReference($byReference)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setInheritData($inheritData)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setCompound($compound)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setType(\Symfony\Component\Form\ResolvedFormTypeInterface $type)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setData($data)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setDataLocked($locked)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFormFactory(\Symfony\Component\Form\FormFactoryInterface $formFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAction($action)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMethod($method)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setRequestHandler(\Symfony\Component\Form\RequestHandlerInterface $requestHandler)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAutoInitialize($initialize)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFormConfig()
    {
    }
    /**
     * Validates whether the given variable is a valid form name.
     *
     * @param string|null $name The tested form name
     *
     * @throws UnexpectedTypeException  if the name is not a string or an integer
     * @throws InvalidArgumentException if the name contains invalid characters
     *
     * @internal since Symfony 4.4
     */
    public static function validateName($name)
    {
    }
    /**
     * Returns whether the given variable contains a valid form name.
     *
     * A name is accepted if it
     *
     *   * is empty
     *   * starts with a letter, digit or underscore
     *   * contains only letters, digits, numbers, underscores ("_"),
     *     hyphens ("-") and colons (":")
     *
     * @final since Symfony 4.4
     */
    public static function isValidName(?string $name) : bool
    {
    }
}
