<?php

namespace Symfony\Component\Form;

/**
 * @internal
 */
class FileUploadError extends \Symfony\Component\Form\FormError
{
}
