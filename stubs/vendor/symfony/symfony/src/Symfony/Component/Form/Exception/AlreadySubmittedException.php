<?php

namespace Symfony\Component\Form\Exception;

/**
 * Thrown when an operation is called that is not acceptable after submitting
 * a form.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class AlreadySubmittedException extends \Symfony\Component\Form\Exception\LogicException
{
}
