<?php

namespace Symfony\Component\Form\Extension\Core\DataTransformer;

/**
 * Transforms between a timezone identifier string and a IntlTimeZone object.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
class IntlTimeZoneToStringTransformer implements \Symfony\Component\Form\DataTransformerInterface
{
    public function __construct(bool $multiple = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function transform($intlTimeZone)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
    }
}
