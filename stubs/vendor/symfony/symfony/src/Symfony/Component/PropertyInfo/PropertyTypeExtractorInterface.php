<?php

namespace Symfony\Component\PropertyInfo;

/**
 * Type Extractor Interface.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface PropertyTypeExtractorInterface
{
    /**
     * Gets types of a property.
     *
     * @param string $class
     * @param string $property
     *
     * @return Type[]|null
     */
    public function getTypes($class, $property, array $context = []);
}
