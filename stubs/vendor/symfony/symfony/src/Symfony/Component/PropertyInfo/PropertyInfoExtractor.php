<?php

namespace Symfony\Component\PropertyInfo;

/**
 * Default {@see PropertyInfoExtractorInterface} implementation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class PropertyInfoExtractor implements \Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface, \Symfony\Component\PropertyInfo\PropertyInitializableExtractorInterface
{
    /**
     * @param iterable|PropertyListExtractorInterface[]          $listExtractors
     * @param iterable|PropertyTypeExtractorInterface[]          $typeExtractors
     * @param iterable|PropertyDescriptionExtractorInterface[]   $descriptionExtractors
     * @param iterable|PropertyAccessExtractorInterface[]        $accessExtractors
     * @param iterable|PropertyInitializableExtractorInterface[] $initializableExtractors
     */
    public function __construct(iterable $listExtractors = [], iterable $typeExtractors = [], iterable $descriptionExtractors = [], iterable $accessExtractors = [], iterable $initializableExtractors = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties($class, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getShortDescription($class, $property, array $context = []) : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLongDescription($class, $property, array $context = []) : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypes($class, $property, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isReadable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isWritable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isInitializable(string $class, string $property, array $context = []) : ?bool
    {
    }
}
