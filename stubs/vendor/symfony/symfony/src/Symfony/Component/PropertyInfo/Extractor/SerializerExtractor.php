<?php

namespace Symfony\Component\PropertyInfo\Extractor;

/**
 * Lists available properties using Symfony Serializer Component metadata.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class SerializerExtractor implements \Symfony\Component\PropertyInfo\PropertyListExtractorInterface
{
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties($class, array $context = []) : ?array
    {
    }
}
