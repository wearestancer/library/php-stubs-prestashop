<?php

namespace Symfony\Component\PropertyInfo;

/**
 * Guesses if the property can be accessed or mutated.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface PropertyAccessExtractorInterface
{
    /**
     * Is the property readable?
     *
     * @param string $class
     * @param string $property
     *
     * @return bool|null
     */
    public function isReadable($class, $property, array $context = []);
    /**
     * Is the property writable?
     *
     * @param string $class
     * @param string $property
     *
     * @return bool|null
     */
    public function isWritable($class, $property, array $context = []);
}
