<?php

namespace Symfony\Component\PropertyInfo;

/**
 * Gets info about PHP class properties.
 *
 * A convenient interface inheriting all specific info interfaces.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface PropertyInfoExtractorInterface extends \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface, \Symfony\Component\PropertyInfo\PropertyDescriptionExtractorInterface, \Symfony\Component\PropertyInfo\PropertyAccessExtractorInterface, \Symfony\Component\PropertyInfo\PropertyListExtractorInterface
{
}
