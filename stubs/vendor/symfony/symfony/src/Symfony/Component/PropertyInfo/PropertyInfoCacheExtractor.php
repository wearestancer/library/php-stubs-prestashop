<?php

namespace Symfony\Component\PropertyInfo;

/**
 * Adds a PSR-6 cache layer on top of an extractor.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class PropertyInfoCacheExtractor implements \Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface, \Symfony\Component\PropertyInfo\PropertyInitializableExtractorInterface
{
    public function __construct(\Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface $propertyInfoExtractor, \Psr\Cache\CacheItemPoolInterface $cacheItemPool)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isReadable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isWritable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getShortDescription($class, $property, array $context = []) : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLongDescription($class, $property, array $context = []) : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties($class, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypes($class, $property, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isInitializable(string $class, string $property, array $context = []) : ?bool
    {
    }
}
