<?php

namespace Symfony\Component\PropertyInfo\Extractor;

/**
 * Extracts data using the reflection API.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class ReflectionExtractor implements \Symfony\Component\PropertyInfo\PropertyListExtractorInterface, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface, \Symfony\Component\PropertyInfo\PropertyAccessExtractorInterface, \Symfony\Component\PropertyInfo\PropertyInitializableExtractorInterface
{
    /**
     * @internal
     */
    public static $defaultMutatorPrefixes = ['add', 'remove', 'set'];
    /**
     * @internal
     */
    public static $defaultAccessorPrefixes = ['is', 'can', 'get', 'has'];
    /**
     * @internal
     */
    public static $defaultArrayMutatorPrefixes = ['add', 'remove'];
    public const ALLOW_PRIVATE = 1;
    public const ALLOW_PROTECTED = 2;
    public const ALLOW_PUBLIC = 4;
    /**
     * @param string[]|null $mutatorPrefixes
     * @param string[]|null $accessorPrefixes
     * @param string[]|null $arrayMutatorPrefixes
     */
    public function __construct(array $mutatorPrefixes = null, array $accessorPrefixes = null, array $arrayMutatorPrefixes = null, bool $enableConstructorExtraction = true, int $accessFlags = self::ALLOW_PUBLIC)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties($class, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypes($class, $property, array $context = []) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isReadable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isWritable($class, $property, array $context = []) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isInitializable(string $class, string $property, array $context = []) : ?bool
    {
    }
}
