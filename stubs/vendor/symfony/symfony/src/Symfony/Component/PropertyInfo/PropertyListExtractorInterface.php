<?php

namespace Symfony\Component\PropertyInfo;

/**
 * Extracts the list of properties available for the given class.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface PropertyListExtractorInterface
{
    /**
     * Gets the list of properties available for the given class.
     *
     * @param string $class
     *
     * @return string[]|null
     */
    public function getProperties($class, array $context = []);
}
