<?php

namespace Symfony\Component\PropertyInfo\Extractor;

/**
 * Extracts data using a PHPDoc parser.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class PhpDocExtractor implements \Symfony\Component\PropertyInfo\PropertyDescriptionExtractorInterface, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface
{
    public const PROPERTY = 0;
    public const ACCESSOR = 1;
    public const MUTATOR = 2;
    /**
     * @param string[]|null $mutatorPrefixes
     * @param string[]|null $accessorPrefixes
     * @param string[]|null $arrayMutatorPrefixes
     */
    public function __construct(\phpDocumentor\Reflection\DocBlockFactoryInterface $docBlockFactory = null, array $mutatorPrefixes = null, array $accessorPrefixes = null, array $arrayMutatorPrefixes = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getShortDescription($class, $property, array $context = []) : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLongDescription($class, $property, array $context = []) : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypes($class, $property, array $context = []) : ?array
    {
    }
}
