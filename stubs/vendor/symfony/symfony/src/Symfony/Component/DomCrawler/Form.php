<?php

namespace Symfony\Component\DomCrawler;

/**
 * Form represents an HTML form.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Form extends \Symfony\Component\DomCrawler\Link implements \ArrayAccess
{
    /**
     * @param \DOMElement $node       A \DOMElement instance
     * @param string|null $currentUri The URI of the page where the form is embedded
     * @param string|null $method     The method to use for the link (if null, it defaults to the method defined by the form)
     * @param string|null $baseHref   The URI of the <base> used for relative links, but not for empty action
     *
     * @throws \LogicException if the node is not a button inside a form tag
     */
    public function __construct(\DOMElement $node, string $currentUri = null, string $method = null, string $baseHref = null)
    {
    }
    /**
     * Gets the form node associated with this form.
     *
     * @return \DOMElement A \DOMElement instance
     */
    public function getFormNode()
    {
    }
    /**
     * Sets the value of the fields.
     *
     * @param array $values An array of field values
     *
     * @return $this
     */
    public function setValues(array $values)
    {
    }
    /**
     * Gets the field values.
     *
     * The returned array does not include file fields (@see getFiles).
     *
     * @return array An array of field values
     */
    public function getValues()
    {
    }
    /**
     * Gets the file field values.
     *
     * @return array An array of file field values
     */
    public function getFiles()
    {
    }
    /**
     * Gets the field values as PHP.
     *
     * This method converts fields with the array notation
     * (like foo[bar] to arrays) like PHP does.
     *
     * @return array An array of field values
     */
    public function getPhpValues()
    {
    }
    /**
     * Gets the file field values as PHP.
     *
     * This method converts fields with the array notation
     * (like foo[bar] to arrays) like PHP does.
     * The returned array is consistent with the array for field values
     * (@see getPhpValues), rather than uploaded files found in $_FILES.
     * For a compound file field foo[bar] it will create foo[bar][name],
     * instead of foo[name][bar] which would be found in $_FILES.
     *
     * @return array An array of file field values
     */
    public function getPhpFiles()
    {
    }
    /**
     * Gets the URI of the form.
     *
     * The returned URI is not the same as the form "action" attribute.
     * This method merges the value if the method is GET to mimics
     * browser behavior.
     *
     * @return string The URI
     */
    public function getUri()
    {
    }
    protected function getRawUri()
    {
    }
    /**
     * Gets the form method.
     *
     * If no method is defined in the form, GET is returned.
     *
     * @return string The method
     */
    public function getMethod()
    {
    }
    /**
     * Gets the form name.
     *
     * If no name is defined on the form, an empty string is returned.
     */
    public function getName() : string
    {
    }
    /**
     * Returns true if the named field exists.
     *
     * @param string $name The field name
     *
     * @return bool true if the field exists, false otherwise
     */
    public function has($name)
    {
    }
    /**
     * Removes a field from the form.
     *
     * @param string $name The field name
     */
    public function remove($name)
    {
    }
    /**
     * Gets a named field.
     *
     * @param string $name The field name
     *
     * @return FormField|FormField[]|FormField[][] The value of the field
     *
     * @throws \InvalidArgumentException When field is not present in this form
     */
    public function get($name)
    {
    }
    /**
     * Sets a named field.
     */
    public function set(\Symfony\Component\DomCrawler\Field\FormField $field)
    {
    }
    /**
     * Gets all fields.
     *
     * @return FormField[]
     */
    public function all()
    {
    }
    /**
     * Returns true if the named field exists.
     *
     * @param string $name The field name
     *
     * @return bool true if the field exists, false otherwise
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($name)
    {
    }
    /**
     * Gets the value of a field.
     *
     * @param string $name The field name
     *
     * @return FormField|FormField[]|FormField[][] The value of the field
     *
     * @throws \InvalidArgumentException if the field does not exist
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($name)
    {
    }
    /**
     * Sets the value of a field.
     *
     * @param string       $name  The field name
     * @param string|array $value The value of the field
     *
     * @return void
     *
     * @throws \InvalidArgumentException if the field does not exist
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($name, $value)
    {
    }
    /**
     * Removes a field from the form.
     *
     * @param string $name The field name
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($name)
    {
    }
    /**
     * Disables validation.
     *
     * @return self
     */
    public function disableValidation()
    {
    }
    /**
     * Sets the node for the form.
     *
     * Expects a 'submit' button \DOMElement and finds the corresponding form element, or the form element itself.
     *
     * @throws \LogicException If given node is not a button or input or does not have a form ancestor
     */
    protected function setNode(\DOMElement $node)
    {
    }
}
