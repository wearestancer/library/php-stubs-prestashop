<?php

namespace Symfony\Component\DomCrawler;

/**
 * Image represents an HTML image (an HTML img tag).
 */
class Image extends \Symfony\Component\DomCrawler\AbstractUriElement
{
    public function __construct(\DOMElement $node, string $currentUri = null)
    {
    }
    protected function getRawUri()
    {
    }
    protected function setNode(\DOMElement $node)
    {
    }
}
