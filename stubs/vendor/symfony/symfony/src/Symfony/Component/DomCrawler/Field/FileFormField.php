<?php

namespace Symfony\Component\DomCrawler\Field;

/**
 * FileFormField represents a file form field (an HTML file input tag).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FileFormField extends \Symfony\Component\DomCrawler\Field\FormField
{
    /**
     * Sets the PHP error code associated with the field.
     *
     * @param int $error The error code (one of UPLOAD_ERR_INI_SIZE, UPLOAD_ERR_FORM_SIZE, UPLOAD_ERR_PARTIAL, UPLOAD_ERR_NO_FILE, UPLOAD_ERR_NO_TMP_DIR, UPLOAD_ERR_CANT_WRITE, or UPLOAD_ERR_EXTENSION)
     *
     * @throws \InvalidArgumentException When error code doesn't exist
     */
    public function setErrorCode($error)
    {
    }
    /**
     * Sets the value of the field.
     *
     * @param string $value The value of the field
     */
    public function upload($value)
    {
    }
    /**
     * Sets the value of the field.
     *
     * @param string|null $value The value of the field
     */
    public function setValue($value)
    {
    }
    /**
     * Sets path to the file as string for simulating HTTP request.
     *
     * @param string $path The path to the file
     */
    public function setFilePath($path)
    {
    }
    /**
     * Initializes the form field.
     *
     * @throws \LogicException When node type is incorrect
     */
    protected function initialize()
    {
    }
}
