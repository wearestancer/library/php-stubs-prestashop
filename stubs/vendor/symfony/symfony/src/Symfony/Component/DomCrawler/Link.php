<?php

namespace Symfony\Component\DomCrawler;

/**
 * Link represents an HTML link (an HTML a, area or link tag).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Link extends \Symfony\Component\DomCrawler\AbstractUriElement
{
    protected function getRawUri()
    {
    }
    protected function setNode(\DOMElement $node)
    {
    }
}
