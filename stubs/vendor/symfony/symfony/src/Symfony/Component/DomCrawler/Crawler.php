<?php

namespace Symfony\Component\DomCrawler;

/**
 * Crawler eases navigation of a list of \DOMNode objects.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Crawler implements \Countable, \IteratorAggregate
{
    /**
     * @var string|null
     */
    protected $uri;
    /**
     * @param \DOMNodeList|\DOMNode|\DOMNode[]|string|null $node A Node to use as the base for the crawling
     */
    public function __construct($node = null, string $uri = null, string $baseHref = null)
    {
    }
    /**
     * Returns the current URI.
     *
     * @return string|null
     */
    public function getUri()
    {
    }
    /**
     * Returns base href.
     *
     * @return string|null
     */
    public function getBaseHref()
    {
    }
    /**
     * Removes all the nodes.
     */
    public function clear()
    {
    }
    /**
     * Adds a node to the current list of nodes.
     *
     * This method uses the appropriate specialized add*() method based
     * on the type of the argument.
     *
     * @param \DOMNodeList|\DOMNode|\DOMNode[]|string|null $node A node
     *
     * @throws \InvalidArgumentException when node is not the expected type
     */
    public function add($node)
    {
    }
    /**
     * Adds HTML/XML content.
     *
     * If the charset is not set via the content type, it is assumed to be UTF-8,
     * or ISO-8859-1 as a fallback, which is the default charset defined by the
     * HTTP 1.1 specification.
     *
     * @param string      $content A string to parse as HTML/XML
     * @param string|null $type    The content type of the string
     */
    public function addContent($content, $type = null)
    {
    }
    /**
     * Adds an HTML content to the list of nodes.
     *
     * The libxml errors are disabled when the content is parsed.
     *
     * If you want to get parsing errors, be sure to enable
     * internal errors via libxml_use_internal_errors(true)
     * and then, get the errors via libxml_get_errors(). Be
     * sure to clear errors with libxml_clear_errors() afterward.
     *
     * @param string $content The HTML content
     * @param string $charset The charset
     */
    public function addHtmlContent($content, $charset = 'UTF-8')
    {
    }
    /**
     * Adds an XML content to the list of nodes.
     *
     * The libxml errors are disabled when the content is parsed.
     *
     * If you want to get parsing errors, be sure to enable
     * internal errors via libxml_use_internal_errors(true)
     * and then, get the errors via libxml_get_errors(). Be
     * sure to clear errors with libxml_clear_errors() afterward.
     *
     * @param string $content The XML content
     * @param string $charset The charset
     * @param int    $options Bitwise OR of the libxml option constants
     *                        LIBXML_PARSEHUGE is dangerous, see
     *                        http://symfony.com/blog/security-release-symfony-2-0-17-released
     */
    public function addXmlContent($content, $charset = 'UTF-8', $options = \LIBXML_NONET)
    {
    }
    /**
     * Adds a \DOMDocument to the list of nodes.
     *
     * @param \DOMDocument $dom A \DOMDocument instance
     */
    public function addDocument(\DOMDocument $dom)
    {
    }
    /**
     * Adds a \DOMNodeList to the list of nodes.
     *
     * @param \DOMNodeList $nodes A \DOMNodeList instance
     */
    public function addNodeList(\DOMNodeList $nodes)
    {
    }
    /**
     * Adds an array of \DOMNode instances to the list of nodes.
     *
     * @param \DOMNode[] $nodes An array of \DOMNode instances
     */
    public function addNodes(array $nodes)
    {
    }
    /**
     * Adds a \DOMNode instance to the list of nodes.
     *
     * @param \DOMNode $node A \DOMNode instance
     */
    public function addNode(\DOMNode $node)
    {
    }
    /**
     * Returns a node given its position in the node list.
     *
     * @param int $position The position
     *
     * @return static
     */
    public function eq($position)
    {
    }
    /**
     * Calls an anonymous function on each node of the list.
     *
     * The anonymous function receives the position and the node wrapped
     * in a Crawler instance as arguments.
     *
     * Example:
     *
     *     $crawler->filter('h1')->each(function ($node, $i) {
     *         return $node->text();
     *     });
     *
     * @param \Closure $closure An anonymous function
     *
     * @return array An array of values returned by the anonymous function
     */
    public function each(\Closure $closure)
    {
    }
    /**
     * Slices the list of nodes by $offset and $length.
     *
     * @param int $offset
     * @param int $length
     *
     * @return static
     */
    public function slice($offset = 0, $length = null)
    {
    }
    /**
     * Reduces the list of nodes by calling an anonymous function.
     *
     * To remove a node from the list, the anonymous function must return false.
     *
     * @param \Closure $closure An anonymous function
     *
     * @return static
     */
    public function reduce(\Closure $closure)
    {
    }
    /**
     * Returns the first node of the current selection.
     *
     * @return static
     */
    public function first()
    {
    }
    /**
     * Returns the last node of the current selection.
     *
     * @return static
     */
    public function last()
    {
    }
    /**
     * Returns the siblings nodes of the current selection.
     *
     * @return static
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function siblings()
    {
    }
    public function matches(string $selector) : bool
    {
    }
    /**
     * Return first parents (heading toward the document root) of the Element that matches the provided selector.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/closest#Polyfill
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function closest(string $selector) : ?self
    {
    }
    /**
     * Returns the next siblings nodes of the current selection.
     *
     * @return static
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function nextAll()
    {
    }
    /**
     * Returns the previous sibling nodes of the current selection.
     *
     * @return static
     *
     * @throws \InvalidArgumentException
     */
    public function previousAll()
    {
    }
    /**
     * Returns the parents nodes of the current selection.
     *
     * @return static
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function parents()
    {
    }
    /**
     * Returns the children nodes of the current selection.
     *
     * @param string|null $selector An optional CSS selector to filter children
     *
     * @return static
     *
     * @throws \InvalidArgumentException When current node is empty
     * @throws \RuntimeException         If the CssSelector Component is not available and $selector is provided
     */
    public function children()
    {
    }
    /**
     * Returns the attribute value of the first node of the list.
     *
     * @param string $attribute The attribute name
     *
     * @return string|null The attribute value or null if the attribute does not exist
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function attr($attribute)
    {
    }
    /**
     * Returns the node name of the first node of the list.
     *
     * @return string The node name
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function nodeName()
    {
    }
    /**
     * Returns the text of the first node of the list.
     *
     * Pass true as the second argument to normalize whitespaces.
     *
     * @param string|null $default             When not null: the value to return when the current node is empty
     * @param bool        $normalizeWhitespace Whether whitespaces should be trimmed and normalized to single spaces
     *
     * @return string The node value
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function text()
    {
    }
    /**
     * Returns the first node of the list as HTML.
     *
     * @param string|null $default When not null: the value to return when the current node is empty
     *
     * @return string The node html
     *
     * @throws \InvalidArgumentException When current node is empty
     */
    public function html()
    {
    }
    public function outerHtml() : string
    {
    }
    /**
     * Evaluates an XPath expression.
     *
     * Since an XPath expression might evaluate to either a simple type or a \DOMNodeList,
     * this method will return either an array of simple types or a new Crawler instance.
     *
     * @param string $xpath An XPath expression
     *
     * @return array|Crawler An array of evaluation results or a new Crawler instance
     */
    public function evaluate($xpath)
    {
    }
    /**
     * Extracts information from the list of nodes.
     *
     * You can extract attributes or/and the node value (_text).
     *
     * Example:
     *
     *     $crawler->filter('h1 a')->extract(['_text', 'href']);
     *
     * @param array $attributes An array of attributes
     *
     * @return array An array of extracted values
     */
    public function extract($attributes)
    {
    }
    /**
     * Filters the list of nodes with an XPath expression.
     *
     * The XPath expression is evaluated in the context of the crawler, which
     * is considered as a fake parent of the elements inside it.
     * This means that a child selector "div" or "./div" will match only
     * the div elements of the current crawler, not their children.
     *
     * @param string $xpath An XPath expression
     *
     * @return static
     */
    public function filterXPath($xpath)
    {
    }
    /**
     * Filters the list of nodes with a CSS selector.
     *
     * This method only works if you have installed the CssSelector Symfony Component.
     *
     * @param string $selector A CSS selector
     *
     * @return static
     *
     * @throws \RuntimeException if the CssSelector Component is not available
     */
    public function filter($selector)
    {
    }
    /**
     * Selects links by name or alt value for clickable images.
     *
     * @param string $value The link text
     *
     * @return static
     */
    public function selectLink($value)
    {
    }
    /**
     * Selects images by alt value.
     *
     * @param string $value The image alt
     *
     * @return static A new instance of Crawler with the filtered list of nodes
     */
    public function selectImage($value)
    {
    }
    /**
     * Selects a button by name or alt value for images.
     *
     * @param string $value The button text
     *
     * @return static
     */
    public function selectButton($value)
    {
    }
    /**
     * Returns a Link object for the first node in the list.
     *
     * @param string $method The method for the link (get by default)
     *
     * @return Link A Link instance
     *
     * @throws \InvalidArgumentException If the current node list is empty or the selected node is not instance of DOMElement
     */
    public function link($method = 'get')
    {
    }
    /**
     * Returns an array of Link objects for the nodes in the list.
     *
     * @return Link[] An array of Link instances
     *
     * @throws \InvalidArgumentException If the current node list contains non-DOMElement instances
     */
    public function links()
    {
    }
    /**
     * Returns an Image object for the first node in the list.
     *
     * @return Image An Image instance
     *
     * @throws \InvalidArgumentException If the current node list is empty
     */
    public function image()
    {
    }
    /**
     * Returns an array of Image objects for the nodes in the list.
     *
     * @return Image[] An array of Image instances
     */
    public function images()
    {
    }
    /**
     * Returns a Form object for the first node in the list.
     *
     * @param array  $values An array of values for the form fields
     * @param string $method The method for the form
     *
     * @return Form A Form instance
     *
     * @throws \InvalidArgumentException If the current node list is empty or the selected node is not instance of DOMElement
     */
    public function form(array $values = null, $method = null)
    {
    }
    /**
     * Overloads a default namespace prefix to be used with XPath and CSS expressions.
     *
     * @param string $prefix
     */
    public function setDefaultNamespacePrefix($prefix)
    {
    }
    /**
     * @param string $prefix
     * @param string $namespace
     */
    public function registerNamespace($prefix, $namespace)
    {
    }
    /**
     * Converts string for XPath expressions.
     *
     * Escaped characters are: quotes (") and apostrophe (').
     *
     *  Examples:
     *
     *     echo Crawler::xpathLiteral('foo " bar');
     *     //prints 'foo " bar'
     *
     *     echo Crawler::xpathLiteral("foo ' bar");
     *     //prints "foo ' bar"
     *
     *     echo Crawler::xpathLiteral('a\'b"c');
     *     //prints concat('a', "'", 'b"c')
     *
     * @param string $s String to be escaped
     *
     * @return string Converted string
     */
    public static function xpathLiteral($s)
    {
    }
    /**
     * @param int $position
     *
     * @return \DOMNode|null
     */
    public function getNode($position)
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * @return \ArrayIterator|\DOMNode[]
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * @param \DOMElement $node
     * @param string      $siblingDir
     *
     * @return array
     */
    protected function sibling($node, $siblingDir = 'nextSibling')
    {
    }
}
