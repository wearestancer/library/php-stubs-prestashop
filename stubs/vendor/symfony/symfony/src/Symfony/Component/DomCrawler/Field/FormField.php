<?php

namespace Symfony\Component\DomCrawler\Field;

/**
 * FormField is the abstract class for all form fields.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class FormField
{
    /**
     * @var \DOMElement
     */
    protected $node;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $value;
    /**
     * @var \DOMDocument
     */
    protected $document;
    /**
     * @var \DOMXPath
     */
    protected $xpath;
    /**
     * @var bool
     */
    protected $disabled;
    /**
     * @param \DOMElement $node The node associated with this field
     */
    public function __construct(\DOMElement $node)
    {
    }
    /**
     * Returns the label tag associated to the field or null if none.
     *
     * @return \DOMElement|null
     */
    public function getLabel()
    {
    }
    /**
     * Returns the name of the field.
     *
     * @return string The name of the field
     */
    public function getName()
    {
    }
    /**
     * Gets the value of the field.
     *
     * @return string|array The value of the field
     */
    public function getValue()
    {
    }
    /**
     * Sets the value of the field.
     *
     * @param string|array|bool|null $value The value of the field
     */
    public function setValue($value)
    {
    }
    /**
     * Returns true if the field should be included in the submitted values.
     *
     * @return bool true if the field should be included in the submitted values, false otherwise
     */
    public function hasValue()
    {
    }
    /**
     * Check if the current field is disabled.
     *
     * @return bool
     */
    public function isDisabled()
    {
    }
    /**
     * Initializes the form field.
     */
    protected abstract function initialize();
}
