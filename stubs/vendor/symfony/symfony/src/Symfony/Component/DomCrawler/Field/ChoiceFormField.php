<?php

namespace Symfony\Component\DomCrawler\Field;

/**
 * ChoiceFormField represents a choice form field.
 *
 * It is constructed from an HTML select tag, or an HTML checkbox, or radio inputs.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ChoiceFormField extends \Symfony\Component\DomCrawler\Field\FormField
{
    /**
     * Returns true if the field should be included in the submitted values.
     *
     * @return bool true if the field should be included in the submitted values, false otherwise
     */
    public function hasValue()
    {
    }
    /**
     * Check if the current selected option is disabled.
     *
     * @return bool
     */
    public function isDisabled()
    {
    }
    /**
     * Sets the value of the field.
     *
     * @param string|array $value The value of the field
     */
    public function select($value)
    {
    }
    /**
     * Ticks a checkbox.
     *
     * @throws \LogicException When the type provided is not correct
     */
    public function tick()
    {
    }
    /**
     * Unticks a checkbox.
     *
     * @throws \LogicException When the type provided is not correct
     */
    public function untick()
    {
    }
    /**
     * Sets the value of the field.
     *
     * @param string|array|bool $value The value of the field
     *
     * @throws \InvalidArgumentException When value type provided is not correct
     */
    public function setValue($value)
    {
    }
    /**
     * Adds a choice to the current ones.
     *
     * @throws \LogicException When choice provided is not multiple nor radio
     *
     * @internal
     */
    public function addChoice(\DOMElement $node)
    {
    }
    /**
     * Returns the type of the choice field (radio, select, or checkbox).
     *
     * @return string The type
     */
    public function getType()
    {
    }
    /**
     * Returns true if the field accepts multiple values.
     *
     * @return bool true if the field accepts multiple values, false otherwise
     */
    public function isMultiple()
    {
    }
    /**
     * Initializes the form field.
     *
     * @throws \LogicException When node type is incorrect
     */
    protected function initialize()
    {
    }
    /**
     * Checks whether given value is in the existing options.
     *
     * @param string $optionValue
     * @param array  $options
     *
     * @return bool
     */
    public function containsOption($optionValue, $options)
    {
    }
    /**
     * Returns list of available field options.
     *
     * @return array
     */
    public function availableOptionValues()
    {
    }
    /**
     * Disables the internal validation of the field.
     *
     * @return self
     */
    public function disableValidation()
    {
    }
}
