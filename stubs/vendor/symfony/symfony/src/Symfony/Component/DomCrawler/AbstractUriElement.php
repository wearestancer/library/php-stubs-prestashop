<?php

namespace Symfony\Component\DomCrawler;

/**
 * Any HTML element that can link to an URI.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractUriElement
{
    /**
     * @var \DOMElement
     */
    protected $node;
    /**
     * @var string|null The method to use for the element
     */
    protected $method;
    /**
     * @var string The URI of the page where the element is embedded (or the base href)
     */
    protected $currentUri;
    /**
     * @param \DOMElement $node       A \DOMElement instance
     * @param string|null $currentUri The URI of the page where the link is embedded (or the base href)
     * @param string|null $method     The method to use for the link (GET by default)
     *
     * @throws \InvalidArgumentException if the node is not a link
     */
    public function __construct(\DOMElement $node, string $currentUri = null, ?string $method = 'GET')
    {
    }
    /**
     * Gets the node associated with this link.
     *
     * @return \DOMElement A \DOMElement instance
     */
    public function getNode()
    {
    }
    /**
     * Gets the method associated with this link.
     *
     * @return string The method
     */
    public function getMethod()
    {
    }
    /**
     * Gets the URI associated with this link.
     *
     * @return string The URI
     */
    public function getUri()
    {
    }
    /**
     * Returns raw URI data.
     *
     * @return string
     */
    protected abstract function getRawUri();
    /**
     * Returns the canonicalized URI path (see RFC 3986, section 5.2.4).
     *
     * @param string $path URI path
     *
     * @return string
     */
    protected function canonicalizePath($path)
    {
    }
    /**
     * Sets current \DOMElement instance.
     *
     * @param \DOMElement $node A \DOMElement instance
     *
     * @throws \LogicException If given node is not an anchor
     */
    protected abstract function setNode(\DOMElement $node);
}
