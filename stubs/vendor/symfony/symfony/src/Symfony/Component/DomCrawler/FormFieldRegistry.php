<?php

namespace Symfony\Component\DomCrawler;

/**
 * This is an internal class that must not be used directly.
 *
 * @internal
 */
class FormFieldRegistry
{
    /**
     * Adds a field to the registry.
     */
    public function add(\Symfony\Component\DomCrawler\Field\FormField $field)
    {
    }
    /**
     * Removes a field based on the fully qualifed name and its children from the registry.
     */
    public function remove(string $name)
    {
    }
    /**
     * Returns the value of the field based on the fully qualifed name and its children.
     *
     * @return FormField|FormField[]|FormField[][] The value of the field
     *
     * @throws \InvalidArgumentException if the field does not exist
     */
    public function &get(string $name)
    {
    }
    /**
     * Tests whether the form has the given field based on the fully qualified name.
     *
     * @return bool Whether the form has the given field
     */
    public function has(string $name) : bool
    {
    }
    /**
     * Set the value of a field based on the fully qualified name and its children.
     *
     * @param mixed $value The value
     *
     * @throws \InvalidArgumentException if the field does not exist
     */
    public function set(string $name, $value)
    {
    }
    /**
     * Returns the list of field with their value.
     *
     * @return FormField[] The list of fields as [string] Fully qualified name => (mixed) value)
     */
    public function all() : array
    {
    }
}
