<?php

namespace Symfony\Component\Debug\Exception;

/**
 * Data Object that represents a Silenced Error.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\Exception\SilencedErrorContext instead.
 */
class SilencedErrorContext implements \JsonSerializable
{
    public $count = 1;
    public function __construct(int $severity, string $file, int $line, array $trace = [], int $count = 1)
    {
    }
    public function getSeverity()
    {
    }
    public function getFile()
    {
    }
    public function getLine()
    {
    }
    public function getTrace()
    {
    }
    public function jsonSerialize()
    {
    }
}
