<?php

namespace Symfony\Component\Debug\FatalErrorHandler;

/**
 * ErrorHandler for classes that do not exist.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\FatalErrorHandler\ClassNotFoundFatalErrorHandler instead.
 */
class ClassNotFoundFatalErrorHandler implements \Symfony\Component\Debug\FatalErrorHandler\FatalErrorHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handleError(array $error, \Symfony\Component\Debug\Exception\FatalErrorException $exception)
    {
    }
}
