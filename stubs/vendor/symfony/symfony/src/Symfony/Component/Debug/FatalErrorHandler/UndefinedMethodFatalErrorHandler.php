<?php

namespace Symfony\Component\Debug\FatalErrorHandler;

/**
 * ErrorHandler for undefined methods.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\ErrorEnhancer\UndefinedMethodErrorEnhancer instead.
 */
class UndefinedMethodFatalErrorHandler implements \Symfony\Component\Debug\FatalErrorHandler\FatalErrorHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handleError(array $error, \Symfony\Component\Debug\Exception\FatalErrorException $exception)
    {
    }
}
