<?php

namespace Symfony\Component\Debug\Exception;

/**
 * FlattenException wraps a PHP Error or Exception to be able to serialize it.
 *
 * Basically, this class removes all objects from the trace.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\Exception\FlattenException instead.
 */
class FlattenException
{
    /**
     * @return static
     */
    public static function create(\Exception $exception, $statusCode = null, array $headers = [])
    {
    }
    /**
     * @return static
     */
    public static function createFromThrowable(\Throwable $exception, int $statusCode = null, array $headers = [])
    {
    }
    public function toArray()
    {
    }
    public function getStatusCode()
    {
    }
    /**
     * @return $this
     */
    public function setStatusCode($code)
    {
    }
    public function getHeaders()
    {
    }
    /**
     * @return $this
     */
    public function setHeaders(array $headers)
    {
    }
    public function getClass()
    {
    }
    /**
     * @return $this
     */
    public function setClass($class)
    {
    }
    public function getFile()
    {
    }
    /**
     * @return $this
     */
    public function setFile($file)
    {
    }
    public function getLine()
    {
    }
    /**
     * @return $this
     */
    public function setLine($line)
    {
    }
    public function getMessage()
    {
    }
    /**
     * @return $this
     */
    public function setMessage($message)
    {
    }
    public function getCode()
    {
    }
    /**
     * @return $this
     */
    public function setCode($code)
    {
    }
    public function getPrevious()
    {
    }
    /**
     * @return $this
     */
    public function setPrevious(self $previous)
    {
    }
    public function getAllPrevious()
    {
    }
    public function getTrace()
    {
    }
    /**
     * @deprecated since 4.1, use {@see setTraceFromThrowable()} instead.
     */
    public function setTraceFromException(\Exception $exception)
    {
    }
    public function setTraceFromThrowable(\Throwable $throwable)
    {
    }
    /**
     * @return $this
     */
    public function setTrace($trace, $file, $line)
    {
    }
    public function getTraceAsString()
    {
    }
    public function getAsString()
    {
    }
}
