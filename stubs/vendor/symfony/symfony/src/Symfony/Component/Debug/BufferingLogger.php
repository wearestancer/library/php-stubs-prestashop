<?php

namespace Symfony\Component\Debug;

/**
 * A buffering logger that stacks logs for later.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\BufferingLogger instead.
 */
class BufferingLogger extends \Psr\Log\AbstractLogger
{
    /**
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
    }
    public function cleanLogs()
    {
    }
}
