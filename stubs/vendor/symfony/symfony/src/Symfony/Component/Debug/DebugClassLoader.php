<?php

namespace Symfony\Component\Debug;

/**
 * Autoloader checking if the class is really defined in the file found.
 *
 * The ClassLoader will wrap all registered autoloaders
 * and will throw an exception if a file is found but does
 * not declare the class.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Christophe Coevoet <stof@notk.org>
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Guilhem Niot <guilhem.niot@gmail.com>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\DebugClassLoader instead.
 */
class DebugClassLoader
{
    public function __construct(callable $classLoader)
    {
    }
    /**
     * Gets the wrapped class loader.
     *
     * @return callable The wrapped class loader
     */
    public function getClassLoader()
    {
    }
    /**
     * Wraps all autoloaders.
     */
    public static function enable()
    {
    }
    /**
     * Disables the wrapping.
     */
    public static function disable()
    {
    }
    /**
     * @return string|null
     */
    public function findFile($class)
    {
    }
    /**
     * Loads the given class or interface.
     *
     * @param string $class The name of the class
     *
     * @throws \RuntimeException
     */
    public function loadClass($class)
    {
    }
    public function checkAnnotations(\ReflectionClass $refl, $class)
    {
    }
    /**
     * @param string $file
     * @param string $class
     *
     * @return array|null
     */
    public function checkCase(\ReflectionClass $refl, $file, $class)
    {
    }
}
