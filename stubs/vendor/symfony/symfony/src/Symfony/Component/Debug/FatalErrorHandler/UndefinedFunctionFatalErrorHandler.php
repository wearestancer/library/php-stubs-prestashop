<?php

namespace Symfony\Component\Debug\FatalErrorHandler;

/**
 * ErrorHandler for undefined functions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\ErrorEnhancer\UndefinedFunctionErrorEnhancer instead.
 */
class UndefinedFunctionFatalErrorHandler implements \Symfony\Component\Debug\FatalErrorHandler\FatalErrorHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handleError(array $error, \Symfony\Component\Debug\Exception\FatalErrorException $exception)
    {
    }
}
