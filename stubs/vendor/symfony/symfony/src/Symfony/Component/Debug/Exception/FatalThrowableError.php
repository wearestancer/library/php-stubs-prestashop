<?php

namespace Symfony\Component\Debug\Exception;

/**
 * Fatal Throwable Error.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @deprecated since Symfony 4.4
 */
class FatalThrowableError extends \Symfony\Component\Debug\Exception\FatalErrorException
{
    public function __construct(\Throwable $e)
    {
    }
    public function getOriginalClassName() : string
    {
    }
}
