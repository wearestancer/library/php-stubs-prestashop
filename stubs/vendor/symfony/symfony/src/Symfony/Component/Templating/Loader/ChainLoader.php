<?php

namespace Symfony\Component\Templating\Loader;

/**
 * ChainLoader is a loader that calls other loaders to load templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ChainLoader extends \Symfony\Component\Templating\Loader\Loader
{
    protected $loaders = [];
    /**
     * @param LoaderInterface[] $loaders
     */
    public function __construct(array $loaders = [])
    {
    }
    public function addLoader(\Symfony\Component\Templating\Loader\LoaderInterface $loader)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(\Symfony\Component\Templating\TemplateReferenceInterface $template)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh(\Symfony\Component\Templating\TemplateReferenceInterface $template, $time)
    {
    }
}
