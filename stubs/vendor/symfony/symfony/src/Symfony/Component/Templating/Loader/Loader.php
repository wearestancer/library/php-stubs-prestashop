<?php

namespace Symfony\Component\Templating\Loader;

/**
 * Loader is the base class for all template loader classes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Loader implements \Symfony\Component\Templating\Loader\LoaderInterface
{
    /**
     * @var LoggerInterface|null
     */
    protected $logger;
    /**
     * Sets the debug logger to use for this loader.
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
    }
}
