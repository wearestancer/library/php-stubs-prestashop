<?php

namespace Symfony\Component\Templating\Helper;

/**
 * SlotsHelper manages template slots.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SlotsHelper extends \Symfony\Component\Templating\Helper\Helper
{
    protected $slots = [];
    protected $openSlots = [];
    /**
     * Starts a new slot.
     *
     * This method starts an output buffer that will be
     * closed when the stop() method is called.
     *
     * @param string $name The slot name
     *
     * @throws \InvalidArgumentException if a slot with the same name is already started
     */
    public function start($name)
    {
    }
    /**
     * Stops a slot.
     *
     * @throws \LogicException if no slot has been started
     */
    public function stop()
    {
    }
    /**
     * Returns true if the slot exists.
     *
     * @param string $name The slot name
     *
     * @return bool
     */
    public function has($name)
    {
    }
    /**
     * Gets the slot value.
     *
     * @param string      $name    The slot name
     * @param bool|string $default The default slot content
     *
     * @return string The slot content
     */
    public function get($name, $default = false)
    {
    }
    /**
     * Sets a slot value.
     *
     * @param string $name    The slot name
     * @param string $content The slot content
     */
    public function set($name, $content)
    {
    }
    /**
     * Outputs a slot.
     *
     * @param string      $name    The slot name
     * @param bool|string $default The default slot content
     *
     * @return bool true if the slot is defined or if a default content has been provided, false otherwise
     */
    public function output($name, $default = false)
    {
    }
    /**
     * Returns the canonical name of this helper.
     *
     * @return string The canonical name
     */
    public function getName()
    {
    }
}
