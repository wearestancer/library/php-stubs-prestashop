<?php

namespace Symfony\Component\Templating\Loader;

/**
 * CacheLoader is a loader that caches other loaders responses
 * on the filesystem.
 *
 * This cache only caches on disk to allow PHP accelerators to cache the opcodes.
 * All other mechanism would imply the use of `eval()`.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class CacheLoader extends \Symfony\Component\Templating\Loader\Loader
{
    protected $loader;
    protected $dir;
    /**
     * @param string $dir The directory where to store the cache files
     */
    public function __construct(\Symfony\Component\Templating\Loader\LoaderInterface $loader, string $dir)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(\Symfony\Component\Templating\TemplateReferenceInterface $template)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh(\Symfony\Component\Templating\TemplateReferenceInterface $template, $time)
    {
    }
}
