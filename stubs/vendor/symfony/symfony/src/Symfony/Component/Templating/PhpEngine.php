<?php

namespace Symfony\Component\Templating;

/**
 * PhpEngine is an engine able to render PHP templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PhpEngine implements \Symfony\Component\Templating\EngineInterface, \ArrayAccess
{
    protected $loader;
    protected $current;
    /**
     * @var HelperInterface[]
     */
    protected $helpers = [];
    protected $parents = [];
    protected $stack = [];
    protected $charset = 'UTF-8';
    protected $cache = [];
    protected $escapers = [];
    protected static $escaperCache = [];
    protected $globals = [];
    protected $parser;
    /**
     * @param HelperInterface[] $helpers An array of helper instances
     */
    public function __construct(\Symfony\Component\Templating\TemplateNameParserInterface $parser, \Symfony\Component\Templating\Loader\LoaderInterface $loader, array $helpers = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException if the template does not exist
     */
    public function render($name, array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($name)
    {
    }
    /**
     * Evaluates a template.
     *
     * @param array $parameters An array of parameters to pass to the template
     *
     * @return string|false The evaluated template, or false if the engine is unable to render the template
     *
     * @throws \InvalidArgumentException
     */
    protected function evaluate(\Symfony\Component\Templating\Storage\Storage $template, array $parameters = [])
    {
    }
    /**
     * Gets a helper value.
     *
     * @param string $name The helper name
     *
     * @return HelperInterface The helper value
     *
     * @throws \InvalidArgumentException if the helper is not defined
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($name)
    {
    }
    /**
     * Returns true if the helper is defined.
     *
     * @param string $name The helper name
     *
     * @return bool true if the helper is defined, false otherwise
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($name)
    {
    }
    /**
     * Sets a helper.
     *
     * @param HelperInterface $name  The helper instance
     * @param string          $value An alias
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($name, $value)
    {
    }
    /**
     * Removes a helper.
     *
     * @param string $name The helper name
     *
     * @return void
     *
     * @throws \LogicException
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($name)
    {
    }
    /**
     * Adds some helpers.
     *
     * @param HelperInterface[] $helpers An array of helper
     */
    public function addHelpers(array $helpers)
    {
    }
    /**
     * Sets the helpers.
     *
     * @param HelperInterface[] $helpers An array of helper
     */
    public function setHelpers(array $helpers)
    {
    }
    /**
     * Sets a helper.
     *
     * @param string $alias An alias
     */
    public function set(\Symfony\Component\Templating\Helper\HelperInterface $helper, $alias = null)
    {
    }
    /**
     * Returns true if the helper if defined.
     *
     * @param string $name The helper name
     *
     * @return bool true if the helper is defined, false otherwise
     */
    public function has($name)
    {
    }
    /**
     * Gets a helper value.
     *
     * @param string $name The helper name
     *
     * @return HelperInterface The helper instance
     *
     * @throws \InvalidArgumentException if the helper is not defined
     */
    public function get($name)
    {
    }
    /**
     * Decorates the current template with another one.
     *
     * @param string $template The decorator logical name
     */
    public function extend($template)
    {
    }
    /**
     * Escapes a string by using the current charset.
     *
     * @param mixed  $value   A variable to escape
     * @param string $context The context name
     *
     * @return mixed The escaped value
     */
    public function escape($value, $context = 'html')
    {
    }
    /**
     * Sets the charset to use.
     *
     * @param string $charset The charset
     */
    public function setCharset($charset)
    {
    }
    /**
     * Gets the current charset.
     *
     * @return string The current charset
     */
    public function getCharset()
    {
    }
    /**
     * Adds an escaper for the given context.
     *
     * @param string   $context The escaper context (html, js, ...)
     * @param callable $escaper A PHP callable
     */
    public function setEscaper($context, callable $escaper)
    {
    }
    /**
     * Gets an escaper for a given context.
     *
     * @param string $context The context name
     *
     * @return callable A PHP callable
     *
     * @throws \InvalidArgumentException
     */
    public function getEscaper($context)
    {
    }
    /**
     * @param string $name
     * @param mixed  $value
     */
    public function addGlobal($name, $value)
    {
    }
    /**
     * Returns the assigned globals.
     *
     * @return array
     */
    public function getGlobals()
    {
    }
    /**
     * Initializes the built-in escapers.
     *
     * Each function specifies a way for applying a transformation to a string
     * passed to it. The purpose is for the string to be "escaped" so it is
     * suitable for the format it is being displayed in.
     *
     * For example, the string: "It's required that you enter a username & password.\n"
     * If this were to be displayed as HTML it would be sensible to turn the
     * ampersand into '&amp;' and the apostrophe into '&aps;'. However if it were
     * going to be used as a string in JavaScript to be displayed in an alert box
     * it would be right to leave the string as-is, but c-escape the apostrophe and
     * the new line.
     *
     * For each function there is a define to avoid problems with strings being
     * incorrectly specified.
     */
    protected function initializeEscapers()
    {
    }
    /**
     * Gets the loader associated with this engine.
     *
     * @return LoaderInterface A LoaderInterface instance
     */
    public function getLoader()
    {
    }
    /**
     * Loads the given template.
     *
     * @param string|TemplateReferenceInterface $name A template name or a TemplateReferenceInterface instance
     *
     * @return Storage A Storage instance
     *
     * @throws \InvalidArgumentException if the template cannot be found
     */
    protected function load($name)
    {
    }
}
