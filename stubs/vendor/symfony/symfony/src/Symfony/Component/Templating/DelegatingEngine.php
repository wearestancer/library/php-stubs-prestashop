<?php

namespace Symfony\Component\Templating;

/**
 * DelegatingEngine selects an engine for a given template.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DelegatingEngine implements \Symfony\Component\Templating\EngineInterface, \Symfony\Component\Templating\StreamingEngineInterface
{
    /**
     * @var EngineInterface[]
     */
    protected $engines = [];
    /**
     * @param EngineInterface[] $engines An array of EngineInterface instances to add
     */
    public function __construct(array $engines = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render($name, array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($name, array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists($name)
    {
    }
    public function addEngine(\Symfony\Component\Templating\EngineInterface $engine)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($name)
    {
    }
    /**
     * Get an engine able to render the given template.
     *
     * @param string|TemplateReferenceInterface $name A template name or a TemplateReferenceInterface instance
     *
     * @return EngineInterface The engine
     *
     * @throws \RuntimeException if no engine able to work with the template is found
     */
    public function getEngine($name)
    {
    }
}
