<?php

namespace Symfony\Component\Templating\Loader;

/**
 * FilesystemLoader is a loader that read templates from the filesystem.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FilesystemLoader extends \Symfony\Component\Templating\Loader\Loader
{
    protected $templatePathPatterns;
    /**
     * @param string|string[] $templatePathPatterns An array of path patterns to look for templates
     */
    public function __construct($templatePathPatterns)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(\Symfony\Component\Templating\TemplateReferenceInterface $template)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh(\Symfony\Component\Templating\TemplateReferenceInterface $template, $time)
    {
    }
    /**
     * Returns true if the file is an existing absolute path.
     *
     * @param string $file A path
     *
     * @return bool true if the path exists and is absolute, false otherwise
     */
    protected static function isAbsolutePath($file)
    {
    }
}
