<?php

namespace Symfony\Component\Templating;

/**
 * Internal representation of a template.
 *
 * @author Victor Berchet <victor@suumit.com>
 */
class TemplateReference implements \Symfony\Component\Templating\TemplateReferenceInterface
{
    protected $parameters;
    public function __construct(string $name = null, string $engine = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLogicalName()
    {
    }
}
