<?php

namespace Symfony\Component\Templating\Storage;

/**
 * Storage is the base class for all storage classes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Storage
{
    protected $template;
    /**
     * @param string $template The template name
     */
    public function __construct(string $template)
    {
    }
    /**
     * Returns the object string representation.
     *
     * @return string The template name
     */
    public function __toString()
    {
    }
    /**
     * Returns the content of the template.
     *
     * @return string The template content
     */
    public abstract function getContent();
}
