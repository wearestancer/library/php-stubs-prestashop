<?php

namespace Symfony\Component\Templating\Loader;

/**
 * LoaderInterface is the interface all loaders must implement.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface LoaderInterface
{
    /**
     * Loads a template.
     *
     * @return Storage|false false if the template cannot be loaded, a Storage instance otherwise
     */
    public function load(\Symfony\Component\Templating\TemplateReferenceInterface $template);
    /**
     * Returns true if the template is still fresh.
     *
     * @param int $time The last modification time of the cached template (timestamp)
     *
     * @return bool
     */
    public function isFresh(\Symfony\Component\Templating\TemplateReferenceInterface $template, $time);
}
