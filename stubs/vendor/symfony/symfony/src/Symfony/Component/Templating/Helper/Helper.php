<?php

namespace Symfony\Component\Templating\Helper;

/**
 * Helper is the base class for all helper classes.
 *
 * Most of the time, a Helper is an adapter around an existing
 * class that exposes a read-only interface for templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Helper implements \Symfony\Component\Templating\Helper\HelperInterface
{
    protected $charset = 'UTF-8';
    /**
     * Sets the default charset.
     *
     * @param string $charset The charset
     */
    public function setCharset($charset)
    {
    }
    /**
     * Gets the default charset.
     *
     * @return string The default charset
     */
    public function getCharset()
    {
    }
}
