<?php

namespace Symfony\Component\Messenger;

/**
 * Leverages a message bus to expect a single, synchronous message handling and return its result.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
trait HandleTrait
{
    /** @var MessageBusInterface */
    private $messageBus;
    /**
     * Dispatches the given message, expecting to be handled by a single handler
     * and returns the result from the handler returned value.
     * This behavior is useful for both synchronous command & query buses,
     * the last one usually returning the handler result.
     *
     * @param object|Envelope $message The message or the message pre-wrapped in an envelope
     *
     * @return mixed The handler returned value
     */
    private function handle($message)
    {
    }
}
