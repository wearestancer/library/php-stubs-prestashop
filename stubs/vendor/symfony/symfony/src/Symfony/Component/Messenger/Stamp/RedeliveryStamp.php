<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Stamp applied when a messages needs to be redelivered.
 */
final class RedeliveryStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    public function __construct(int $retryCount, string $exceptionMessage = null, \Symfony\Component\ErrorHandler\Exception\FlattenException $flattenException = null)
    {
    }
    public static function getRetryCountFromEnvelope(\Symfony\Component\Messenger\Envelope $envelope) : int
    {
    }
    public function getRetryCount() : int
    {
    }
    public function getExceptionMessage() : ?string
    {
    }
    public function getFlattenException() : ?\Symfony\Component\ErrorHandler\Exception\FlattenException
    {
    }
    public function getRedeliveredAt() : \DateTimeInterface
    {
    }
}
