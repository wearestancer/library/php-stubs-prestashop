<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * Collects some data about a middleware.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class TraceableMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    public function __construct(\Symfony\Component\Stopwatch\Stopwatch $stopwatch, string $busName, string $eventCategory = 'messenger.middleware')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
/**
 * @internal
 */
class TraceableStack implements \Symfony\Component\Messenger\Middleware\StackInterface
{
    public function __construct(\Symfony\Component\Messenger\Middleware\StackInterface $stack, \Symfony\Component\Stopwatch\Stopwatch $stopwatch, string $busName, string $eventCategory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function next() : \Symfony\Component\Messenger\Middleware\MiddlewareInterface
    {
    }
    public function stop() : void
    {
    }
}
