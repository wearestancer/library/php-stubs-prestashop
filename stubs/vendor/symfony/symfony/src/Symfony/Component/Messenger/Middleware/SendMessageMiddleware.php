<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 * @author Tobias Schultze <http://tobion.de>
 */
class SendMessageMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    use \Psr\Log\LoggerAwareTrait;
    public function __construct(\Symfony\Component\Messenger\Transport\Sender\SendersLocatorInterface $sendersLocator, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
