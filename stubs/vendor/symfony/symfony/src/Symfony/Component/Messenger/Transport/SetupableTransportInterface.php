<?php

namespace Symfony\Component\Messenger\Transport;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
interface SetupableTransportInterface
{
    /**
     * Setup the transport.
     */
    public function setup() : void;
}
