<?php

namespace Symfony\Component\Messenger;

/**
 * Bus of buses that is routable using a BusNameStamp.
 *
 * This is useful when passed to Worker: messages received
 * from the transport can be sent to the correct bus.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class RoutableMessageBus implements \Symfony\Component\Messenger\MessageBusInterface
{
    public function __construct(\Psr\Container\ContainerInterface $busLocator, \Symfony\Component\Messenger\MessageBusInterface $fallbackBus = null)
    {
    }
    public function dispatch($envelope, array $stamps = []) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * @internal
     */
    public function getMessageBus(string $busName) : \Symfony\Component\Messenger\MessageBusInterface
    {
    }
}
