<?php

namespace Symfony\Component\Messenger\Transport\Sender;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
interface SenderInterface
{
    /**
     * Sends the given envelope.
     *
     * The sender can read different stamps for transport configuration,
     * like delivery delay.
     *
     * If applicable, the returned Envelope should contain a TransportMessageIdStamp.
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope;
}
