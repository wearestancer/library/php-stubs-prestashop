<?php

namespace Symfony\Component\Messenger\Transport\Sender;

/**
 * Maps a message to a list of senders.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 * @author Tobias Schultze <http://tobion.de>
 */
interface SendersLocatorInterface
{
    /**
     * Gets the senders for the given message name.
     *
     * @return iterable|SenderInterface[] Indexed by sender alias if available
     */
    public function getSenders(\Symfony\Component\Messenger\Envelope $envelope) : iterable;
}
