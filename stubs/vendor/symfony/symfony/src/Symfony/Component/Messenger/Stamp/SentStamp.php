<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Marker stamp identifying a message sent by the `SendMessageMiddleware`.
 *
 * @see \Symfony\Component\Messenger\Middleware\SendMessageMiddleware
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class SentStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
    public function __construct(string $senderClass, string $senderAlias = null)
    {
    }
    public function getSenderClass() : string
    {
    }
    public function getSenderAlias() : ?string
    {
    }
}
