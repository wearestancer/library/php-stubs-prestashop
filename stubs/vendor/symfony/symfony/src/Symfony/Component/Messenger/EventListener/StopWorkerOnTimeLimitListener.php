<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Simon Delicata <simon.delicata@free.fr>
 * @author Tobias Schultze <http://tobion.de>
 */
class StopWorkerOnTimeLimitListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(int $timeLimitInSeconds, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onWorkerStarted() : void
    {
    }
    public function onWorkerRunning(\Symfony\Component\Messenger\Event\WorkerRunningEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
