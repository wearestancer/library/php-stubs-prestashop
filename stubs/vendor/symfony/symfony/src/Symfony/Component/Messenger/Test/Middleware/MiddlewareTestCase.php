<?php

namespace Symfony\Component\Messenger\Test\Middleware;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
abstract class MiddlewareTestCase extends \PHPUnit\Framework\TestCase
{
    protected function getStackMock(bool $nextIsCalled = true)
    {
    }
    protected function getThrowingStackMock(\Throwable $throwable = null)
    {
    }
}
