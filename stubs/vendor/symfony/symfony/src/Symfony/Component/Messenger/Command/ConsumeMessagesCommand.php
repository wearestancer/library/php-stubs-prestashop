<?php

namespace Symfony\Component\Messenger\Command;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class ConsumeMessagesCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'messenger:consume';
    /**
     * @param RoutableMessageBus $routableBus
     */
    public function __construct($routableBus, \Psr\Container\ContainerInterface $receiverLocator, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, \Psr\Log\LoggerInterface $logger = null, array $receiverNames = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function interact(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
