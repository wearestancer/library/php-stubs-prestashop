<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * Symfony Messenger sender to send messages to AMQP brokers using PHP's AMQP extension.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class AmqpSender implements \Symfony\Component\Messenger\Transport\Sender\SenderInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\AmqpExt\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
}
