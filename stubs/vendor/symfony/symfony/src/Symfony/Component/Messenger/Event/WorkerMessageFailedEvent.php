<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Dispatched when a message was received from a transport and handling failed.
 *
 * The event name is the class name.
 */
final class WorkerMessageFailedEvent extends \Symfony\Component\Messenger\Event\AbstractWorkerMessageEvent
{
    public function __construct(\Symfony\Component\Messenger\Envelope $envelope, string $receiverName, \Throwable $error)
    {
    }
    public function getThrowable() : \Throwable
    {
    }
    public function willRetry() : bool
    {
    }
    public function setForRetry() : void
    {
    }
}
