<?php

namespace Symfony\Component\Messenger\Retry;

/**
 * A retry strategy with a constant or exponential retry delay.
 *
 * For example, if $delayMilliseconds=10000 & $multiplier=1 (default),
 * each retry will wait exactly 10 seconds.
 *
 * But if $delayMilliseconds=10000 & $multiplier=2:
 *      * Retry 1: 10 second delay
 *      * Retry 2: 20 second delay (10000 * 2 = 20000)
 *      * Retry 3: 40 second delay (20000 * 2 = 40000)
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 *
 * @final
 */
class MultiplierRetryStrategy implements \Symfony\Component\Messenger\Retry\RetryStrategyInterface
{
    /**
     * @param int   $maxRetries           The maximum number of times to retry
     * @param int   $delayMilliseconds    Amount of time to delay (or the initial value when multiplier is used)
     * @param float $multiplier           Multiplier to apply to the delay each time a retry occurs
     * @param int   $maxDelayMilliseconds Maximum delay to allow (0 means no maximum)
     */
    public function __construct(int $maxRetries = 3, int $delayMilliseconds = 1000, float $multiplier = 1, int $maxDelayMilliseconds = 0)
    {
    }
    public function isRetryable(\Symfony\Component\Messenger\Envelope $message) : bool
    {
    }
    public function getWaitingTime(\Symfony\Component\Messenger\Envelope $message) : int
    {
    }
}
