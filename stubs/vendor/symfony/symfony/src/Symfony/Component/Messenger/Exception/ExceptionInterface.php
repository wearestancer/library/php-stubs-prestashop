<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * Base Messenger component's exception.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
interface ExceptionInterface extends \Throwable
{
}
