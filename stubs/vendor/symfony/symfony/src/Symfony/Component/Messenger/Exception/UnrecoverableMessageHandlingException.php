<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * A concrete implementation of UnrecoverableExceptionInterface that can be used directly.
 *
 * @author Frederic Bouchery <frederic@bouchery.fr>
 */
class UnrecoverableMessageHandlingException extends \Symfony\Component\Messenger\Exception\RuntimeException implements \Symfony\Component\Messenger\Exception\UnrecoverableExceptionInterface
{
}
