<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Tobias Schultze <http://tobion.de>
 */
class StopWorkerOnSigtermSignalListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function onWorkerStarted(\Symfony\Component\Messenger\Event\WorkerStartedEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
