<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class ValidationStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    /**
     * @param string[]|GroupSequence $groups
     */
    public function __construct($groups)
    {
    }
    public function getGroups()
    {
    }
}
