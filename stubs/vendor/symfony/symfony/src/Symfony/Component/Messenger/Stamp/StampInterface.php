<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * An envelope stamp related to a message.
 *
 * Stamps must be serializable value objects for transport.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
interface StampInterface
{
}
