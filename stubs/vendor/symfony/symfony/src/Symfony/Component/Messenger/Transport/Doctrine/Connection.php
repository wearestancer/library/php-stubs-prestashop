<?php

namespace Symfony\Component\Messenger\Transport\Doctrine;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 *
 * @final
 */
class Connection
{
    public function __construct(array $configuration, \Doctrine\DBAL\Connection $driverConnection, \Doctrine\DBAL\Schema\Synchronizer\SchemaSynchronizer $schemaSynchronizer = null)
    {
    }
    public function getConfiguration() : array
    {
    }
    public static function buildConfiguration(string $dsn, array $options = []) : array
    {
    }
    /**
     * @param int $delay The delay in milliseconds
     *
     * @return string The inserted id
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception
     */
    public function send(string $body, array $headers, int $delay = 0) : string
    {
    }
    public function get() : ?array
    {
    }
    public function ack(string $id) : bool
    {
    }
    public function reject(string $id) : bool
    {
    }
    public function setup() : void
    {
    }
    public function getMessageCount() : int
    {
    }
    public function findAll(int $limit = null) : array
    {
    }
    public function find($id) : ?array
    {
    }
}
