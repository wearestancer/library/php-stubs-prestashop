<?php

namespace Symfony\Component\Messenger\Transport;

/**
 * Transport that stays in memory. Useful for testing purpose.
 *
 * @author Gary PEGEOT <garypegeot@gmail.com>
 */
class InMemoryTransport implements \Symfony\Component\Messenger\Transport\TransportInterface, \Symfony\Contracts\Service\ResetInterface
{
    /**
     * {@inheritdoc}
     */
    public function get() : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
    public function reset()
    {
    }
    /**
     * @return Envelope[]
     */
    public function getAcknowledged() : array
    {
    }
    /**
     * @return Envelope[]
     */
    public function getRejected() : array
    {
    }
    /**
     * @return Envelope[]
     */
    public function getSent() : array
    {
    }
}
