<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Stamp used to identify which bus it was passed to.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
final class BusNameStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    public function __construct(string $busName)
    {
    }
    public function getBusName() : string
    {
    }
}
