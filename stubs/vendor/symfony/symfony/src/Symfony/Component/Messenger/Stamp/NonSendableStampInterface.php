<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * A stamp that should not be included with the Envelope if sent to a transport.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
interface NonSendableStampInterface extends \Symfony\Component\Messenger\Stamp\StampInterface
{
}
