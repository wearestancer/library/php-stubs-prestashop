<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * Sends a rejected message to a "failure transport".
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class SendFailedMessageToFailureTransportListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\Sender\SenderInterface $failureSender, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onMessageFailed(\Symfony\Component\Messenger\Event\WorkerMessageFailedEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
