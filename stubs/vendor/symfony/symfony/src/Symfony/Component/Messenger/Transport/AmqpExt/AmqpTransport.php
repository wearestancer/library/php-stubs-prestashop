<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class AmqpTransport implements \Symfony\Component\Messenger\Transport\TransportInterface, \Symfony\Component\Messenger\Transport\SetupableTransportInterface, \Symfony\Component\Messenger\Transport\Receiver\MessageCountAwareInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\AmqpExt\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get() : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setup() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessageCount() : int
    {
    }
}
