<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Stamp identifying a message handled by the `HandleMessageMiddleware` middleware
 * and storing the handler returned value.
 *
 * This is used by synchronous command buses expecting a return value and the retry logic
 * to only execute handlers that didn't succeed.
 *
 * @see \Symfony\Component\Messenger\Middleware\HandleMessageMiddleware
 * @see \Symfony\Component\Messenger\HandleTrait
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class HandledStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    /**
     * @param mixed $result The returned value of the message handler
     */
    public function __construct($result, string $handlerName)
    {
    }
    /**
     * @param mixed $result The returned value of the message handler
     */
    public static function fromDescriptor(\Symfony\Component\Messenger\Handler\HandlerDescriptor $handler, $result) : self
    {
    }
    /**
     * @return mixed
     */
    public function getResult()
    {
    }
    public function getHandlerName() : string
    {
    }
}
