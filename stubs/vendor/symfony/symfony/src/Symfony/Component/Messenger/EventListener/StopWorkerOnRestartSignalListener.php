<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class StopWorkerOnRestartSignalListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public const RESTART_REQUESTED_TIMESTAMP_KEY = 'workers.restart_requested_timestamp';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cachePool, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onWorkerStarted() : void
    {
    }
    public function onWorkerRunning(\Symfony\Component\Messenger\Event\WorkerRunningEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
