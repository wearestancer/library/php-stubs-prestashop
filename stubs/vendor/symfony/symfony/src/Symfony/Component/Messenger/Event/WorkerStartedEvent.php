<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Dispatched when a worker has been started.
 *
 * @author Tobias Schultze <http://tobion.de>
 */
final class WorkerStartedEvent
{
    public function __construct(\Symfony\Component\Messenger\Worker $worker)
    {
    }
    public function getWorker() : \Symfony\Component\Messenger\Worker
    {
    }
}
