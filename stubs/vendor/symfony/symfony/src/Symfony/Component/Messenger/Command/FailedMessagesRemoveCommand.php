<?php

namespace Symfony\Component\Messenger\Command;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class FailedMessagesRemoveCommand extends \Symfony\Component\Messenger\Command\AbstractFailedMessagesCommand
{
    protected static $defaultName = 'messenger:failed:remove';
    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
