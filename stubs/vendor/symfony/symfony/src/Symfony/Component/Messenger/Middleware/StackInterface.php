<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * Implementations must be cloneable, and each clone must unstack the stack independently.
 */
interface StackInterface
{
    /**
     * Returns the next middleware to process a message.
     */
    public function next() : \Symfony\Component\Messenger\Middleware\MiddlewareInterface;
}
