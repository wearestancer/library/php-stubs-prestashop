<?php

namespace Symfony\Component\Messenger\Transport\RedisExt;

/**
 * @author Alexander Schranz <alexander@sulu.io>
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
class RedisTransport implements \Symfony\Component\Messenger\Transport\TransportInterface, \Symfony\Component\Messenger\Transport\SetupableTransportInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\RedisExt\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get() : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setup() : void
    {
    }
}
