<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Messenger\Exception\ExceptionInterface
{
}
