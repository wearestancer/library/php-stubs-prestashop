<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Messenger\Exception\ExceptionInterface
{
}
