<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
class LogicException extends \LogicException implements \Symfony\Component\Messenger\Exception\ExceptionInterface
{
}
