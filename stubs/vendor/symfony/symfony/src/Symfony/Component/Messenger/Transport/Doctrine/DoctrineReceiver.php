<?php

namespace Symfony\Component\Messenger\Transport\Doctrine;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
class DoctrineReceiver implements \Symfony\Component\Messenger\Transport\Receiver\ListableReceiverInterface, \Symfony\Component\Messenger\Transport\Receiver\MessageCountAwareInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\Doctrine\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get() : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessageCount() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all(int $limit = null) : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function find($id) : ?\Symfony\Component\Messenger\Envelope
    {
    }
}
