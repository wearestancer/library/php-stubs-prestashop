<?php

namespace Symfony\Component\Messenger\Transport;

/**
 * @author Gary PEGEOT <garypegeot@gmail.com>
 */
class InMemoryTransportFactory implements \Symfony\Component\Messenger\Transport\TransportFactoryInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface
    {
    }
    public function supports(string $dsn, array $options) : bool
    {
    }
    public function reset()
    {
    }
}
