<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * Allow to configure messages to be handled after the current bus is finished.
 *
 * I.e, messages dispatched from a handler with a DispatchAfterCurrentBus stamp
 * will actually be handled once the current message being dispatched is fully
 * handled.
 *
 * For instance, using this middleware before the DoctrineTransactionMiddleware
 * means sub-dispatched messages with a DispatchAfterCurrentBus stamp would be
 * handled after the Doctrine transaction has been committed.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class DispatchAfterCurrentBusMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
/**
 * @internal
 */
final class QueuedEnvelope
{
    public function __construct(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack)
    {
    }
    public function getEnvelope() : \Symfony\Component\Messenger\Envelope
    {
    }
    public function getStack() : \Symfony\Component\Messenger\Middleware\StackInterface
    {
    }
}
