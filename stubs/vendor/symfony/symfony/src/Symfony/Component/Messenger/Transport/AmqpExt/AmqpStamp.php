<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * @author Guillaume Gammelin <ggammelin@gmail.com>
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
final class AmqpStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
    public function __construct(string $routingKey = null, int $flags = \AMQP_NOPARAM, array $attributes = [])
    {
    }
    public function getRoutingKey() : ?string
    {
    }
    public function getFlags() : int
    {
    }
    public function getAttributes() : array
    {
    }
    public static function createFromAmqpEnvelope(\AMQPEnvelope $amqpEnvelope, self $previousStamp = null) : self
    {
    }
    public static function createWithAttributes(array $attributes, self $previousStamp = null) : self
    {
    }
}
