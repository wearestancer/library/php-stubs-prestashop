<?php

namespace Symfony\Component\Messenger;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 * @author Tobias Schultze <http://tobion.de>
 *
 * @final
 */
class Worker
{
    /**
     * @param ReceiverInterface[] $receivers Where the key is the transport name
     */
    public function __construct(array $receivers, \Symfony\Component\Messenger\MessageBusInterface $bus, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * Receive the messages and dispatch them to the bus.
     *
     * Valid options are:
     *  * sleep (default: 1000000): Time in microseconds to sleep after no messages are found
     */
    public function run(array $options = []) : void
    {
    }
    public function stop() : void
    {
    }
}
