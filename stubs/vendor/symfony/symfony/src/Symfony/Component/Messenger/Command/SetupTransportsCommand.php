<?php

namespace Symfony\Component\Messenger\Command;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
class SetupTransportsCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'messenger:setup-transports';
    public function __construct(\Psr\Container\ContainerInterface $transportLocator, array $transportNames = [])
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
