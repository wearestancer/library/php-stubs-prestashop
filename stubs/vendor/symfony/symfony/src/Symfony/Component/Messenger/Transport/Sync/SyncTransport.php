<?php

namespace Symfony\Component\Messenger\Transport\Sync;

/**
 * Transport that immediately marks messages as received and dispatches for handling.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class SyncTransport implements \Symfony\Component\Messenger\Transport\TransportInterface
{
    public function __construct(\Symfony\Component\Messenger\MessageBusInterface $messageBus)
    {
    }
    public function get() : iterable
    {
    }
    public function stop() : void
    {
    }
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
}
