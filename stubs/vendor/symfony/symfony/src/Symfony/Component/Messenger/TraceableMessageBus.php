<?php

namespace Symfony\Component\Messenger;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class TraceableMessageBus implements \Symfony\Component\Messenger\MessageBusInterface
{
    public function __construct(\Symfony\Component\Messenger\MessageBusInterface $decoratedBus)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($message, array $stamps = []) : \Symfony\Component\Messenger\Envelope
    {
    }
    public function getDispatchedMessages() : array
    {
    }
    public function reset()
    {
    }
}
