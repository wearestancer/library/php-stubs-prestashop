<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 * @author Tobias Schultze <http://tobion.de>
 */
class StopWorkerOnMessageLimitListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(int $maximumNumberOfMessages, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onWorkerRunning(\Symfony\Component\Messenger\Event\WorkerRunningEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
