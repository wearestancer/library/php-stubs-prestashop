<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 *
 * @deprecated since 4.3, pass a logger to SendMessageMiddleware instead
 */
class LoggingMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
