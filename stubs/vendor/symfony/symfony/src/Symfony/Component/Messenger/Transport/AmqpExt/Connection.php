<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * An AMQP connection.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 *
 * @final
 */
class Connection
{
    public function __construct(array $connectionOptions, array $exchangeOptions, array $queuesOptions, \Symfony\Component\Messenger\Transport\AmqpExt\AmqpFactory $amqpFactory = null)
    {
    }
    /**
     * Creates a connection based on the DSN and options.
     *
     * Available options:
     *
     *   * host: Hostname of the AMQP service
     *   * port: Port of the AMQP service
     *   * vhost: Virtual Host to use with the AMQP service
     *   * user: Username to use to connect the the AMQP service
     *   * password: Password to use the connect to the AMQP service
     *   * queues[name]: An array of queues, keyed by the name
     *     * binding_keys: The binding keys (if any) to bind to this queue
     *     * binding_arguments: Arguments to be used while binding the queue.
     *     * flags: Queue flags (Default: AMQP_DURABLE)
     *     * arguments: Extra arguments
     *   * exchange:
     *     * name: Name of the exchange
     *     * type: Type of exchange (Default: fanout)
     *     * default_publish_routing_key: Routing key to use when publishing, if none is specified on the message
     *     * flags: Exchange flags (Default: AMQP_DURABLE)
     *     * arguments: Extra arguments
     *   * delay:
     *     * queue_name_pattern: Pattern to use to create the queues (Default: "delay_%exchange_name%_%routing_key%_%delay%")
     *     * exchange_name: Name of the exchange to be used for the delayed/retried messages (Default: "delays")
     *   * auto_setup: Enable or not the auto-setup of queues and exchanges (Default: true)
     *   * prefetch_count: set channel prefetch count
     */
    public static function fromDsn(string $dsn, array $options = [], \Symfony\Component\Messenger\Transport\AmqpExt\AmqpFactory $amqpFactory = null) : self
    {
    }
    /**
     * @throws \AMQPException
     */
    public function publish(string $body, array $headers = [], int $delayInMs = 0, \Symfony\Component\Messenger\Transport\AmqpExt\AmqpStamp $amqpStamp = null) : void
    {
    }
    /**
     * Returns an approximate count of the messages in defined queues.
     */
    public function countMessagesInQueues() : int
    {
    }
    /**
     * Gets a message from the specified queue.
     *
     * @throws \AMQPException
     */
    public function get(string $queueName) : ?\AMQPEnvelope
    {
    }
    public function ack(\AMQPEnvelope $message, string $queueName) : bool
    {
    }
    public function nack(\AMQPEnvelope $message, string $queueName, int $flags = \AMQP_NOPARAM) : bool
    {
    }
    public function setup() : void
    {
    }
    /**
     * @return string[]
     */
    public function getQueueNames() : array
    {
    }
    public function channel() : \AMQPChannel
    {
    }
    public function queue(string $queueName) : \AMQPQueue
    {
    }
    public function exchange() : \AMQPExchange
    {
    }
    public function purgeQueues()
    {
    }
}
