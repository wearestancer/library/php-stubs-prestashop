<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Tobias Schultze <http://tobion.de>
 */
class SendFailedMessageForRetryListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Psr\Container\ContainerInterface $sendersLocator, \Psr\Container\ContainerInterface $retryStrategyLocator, \Psr\Log\LoggerInterface $logger = null, int $historySize = 10)
    {
    }
    public function onMessageFailed(\Symfony\Component\Messenger\Event\WorkerMessageFailedEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
