<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

class AmqpFactory
{
    public function createConnection(array $credentials) : \AMQPConnection
    {
    }
    public function createChannel(\AMQPConnection $connection) : \AMQPChannel
    {
    }
    public function createQueue(\AMQPChannel $channel) : \AMQPQueue
    {
    }
    public function createExchange(\AMQPChannel $channel) : \AMQPExchange
    {
    }
}
