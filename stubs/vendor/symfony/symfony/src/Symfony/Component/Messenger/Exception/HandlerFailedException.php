<?php

namespace Symfony\Component\Messenger\Exception;

class HandlerFailedException extends \Symfony\Component\Messenger\Exception\RuntimeException
{
    /**
     * @param \Throwable[] $exceptions
     */
    public function __construct(\Symfony\Component\Messenger\Envelope $envelope, array $exceptions)
    {
    }
    public function getEnvelope() : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * @return \Throwable[]
     */
    public function getNestedExceptions() : array
    {
    }
}
