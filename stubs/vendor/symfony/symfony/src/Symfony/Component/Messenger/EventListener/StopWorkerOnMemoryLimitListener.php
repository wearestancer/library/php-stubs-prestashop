<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Simon Delicata <simon.delicata@free.fr>
 * @author Tobias Schultze <http://tobion.de>
 */
class StopWorkerOnMemoryLimitListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(int $memoryLimit, \Psr\Log\LoggerInterface $logger = null, callable $memoryResolver = null)
    {
    }
    public function onWorkerRunning(\Symfony\Component\Messenger\Event\WorkerRunningEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
