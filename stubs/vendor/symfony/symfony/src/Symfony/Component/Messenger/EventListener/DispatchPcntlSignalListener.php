<?php

namespace Symfony\Component\Messenger\EventListener;

/**
 * @author Tobias Schultze <http://tobion.de>
 */
class DispatchPcntlSignalListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function onWorkerRunning() : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
