<?php

namespace Symfony\Component\Messenger\Transport\Receiver;

/**
 * Receiver that decorates another, but receives only 1 specific message.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 *
 * @internal
 */
class SingleMessageReceiver implements \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface $receiver, \Symfony\Component\Messenger\Envelope $envelope)
    {
    }
    public function get() : iterable
    {
    }
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
}
