<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Apply this stamp to delay delivery of your message on a transport.
 */
final class DelayStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    /**
     * @param int $delay The delay in milliseconds
     */
    public function __construct(int $delay)
    {
    }
    public function getDelay() : int
    {
    }
}
