<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class ValidationFailedException extends \Symfony\Component\Messenger\Exception\RuntimeException
{
    /**
     * @param object $violatingMessage
     */
    public function __construct($violatingMessage, \Symfony\Component\Validator\ConstraintViolationListInterface $violations)
    {
    }
    public function getViolatingMessage()
    {
    }
    public function getViolations() : \Symfony\Component\Validator\ConstraintViolationListInterface
    {
    }
}
