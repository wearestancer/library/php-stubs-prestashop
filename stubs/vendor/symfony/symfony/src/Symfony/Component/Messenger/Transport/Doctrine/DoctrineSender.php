<?php

namespace Symfony\Component\Messenger\Transport\Doctrine;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
class DoctrineSender implements \Symfony\Component\Messenger\Transport\Sender\SenderInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\Doctrine\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
}
