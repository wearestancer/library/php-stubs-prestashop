<?php

namespace Symfony\Component\Messenger\Retry;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
interface RetryStrategyInterface
{
    public function isRetryable(\Symfony\Component\Messenger\Envelope $message) : bool;
    /**
     * @return int The time to delay/wait in milliseconds
     */
    public function getWaitingTime(\Symfony\Component\Messenger\Envelope $message) : int;
}
