<?php

namespace Symfony\Component\Messenger\Handler;

/**
 * Describes a handler and the possible associated options, such as `from_transport`, `bus`, etc.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
final class HandlerDescriptor
{
    public function __construct(callable $handler, array $options = [])
    {
    }
    public function getHandler() : callable
    {
    }
    public function getName() : string
    {
    }
    public function getOption(string $option)
    {
    }
}
