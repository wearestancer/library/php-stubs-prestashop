<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class HandleMessageMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    use \Psr\Log\LoggerAwareTrait;
    public function __construct(\Symfony\Component\Messenger\Handler\HandlersLocatorInterface $handlersLocator, bool $allowNoHandlers = false)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws NoHandlerForMessageException When no handler is found and $allowNoHandlers is false
     */
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
