<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Added by a sender or receiver to indicate the id of this message in that transport.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
final class TransportMessageIdStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    /**
     * @param mixed $id some "identifier" of the message in a transport
     */
    public function __construct($id)
    {
    }
    public function getId()
    {
    }
}
