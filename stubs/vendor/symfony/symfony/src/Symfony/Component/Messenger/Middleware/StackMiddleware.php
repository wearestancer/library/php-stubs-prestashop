<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class StackMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface, \Symfony\Component\Messenger\Middleware\StackInterface
{
    /**
     * @param iterable|MiddlewareInterface[]|MiddlewareInterface|null $middlewareIterator
     */
    public function __construct($middlewareIterator = null)
    {
    }
    public function next() : \Symfony\Component\Messenger\Middleware\MiddlewareInterface
    {
    }
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
/**
 * @internal
 */
class MiddlewareStack
{
    public $iterator;
    public $stack = [];
    public function next(int $offset) : ?\Symfony\Component\Messenger\Middleware\MiddlewareInterface
    {
    }
}
