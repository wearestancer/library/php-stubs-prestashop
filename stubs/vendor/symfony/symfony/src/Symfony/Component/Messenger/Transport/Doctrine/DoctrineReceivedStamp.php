<?php

namespace Symfony\Component\Messenger\Transport\Doctrine;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
class DoctrineReceivedStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
    public function __construct(string $id)
    {
    }
    public function getId() : string
    {
    }
}
