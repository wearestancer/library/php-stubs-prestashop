<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class AmqpTransportFactory implements \Symfony\Component\Messenger\Transport\TransportFactoryInterface
{
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface
    {
    }
    public function supports(string $dsn, array $options) : bool
    {
    }
}
