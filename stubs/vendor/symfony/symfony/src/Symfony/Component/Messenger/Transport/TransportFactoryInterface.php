<?php

namespace Symfony\Component\Messenger\Transport;

/**
 * Creates a Messenger transport.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
interface TransportFactoryInterface
{
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface;
    public function supports(string $dsn, array $options) : bool;
}
