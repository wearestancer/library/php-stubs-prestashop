<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Marker stamp for a received message.
 *
 * This is mainly used by the `SendMessageMiddleware` middleware to identify
 * a message should not be sent if it was just received.
 *
 * @see SendMessageMiddleware
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
final class ReceivedStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
    public function __construct(string $transportName)
    {
    }
    public function getTransportName() : string
    {
    }
}
