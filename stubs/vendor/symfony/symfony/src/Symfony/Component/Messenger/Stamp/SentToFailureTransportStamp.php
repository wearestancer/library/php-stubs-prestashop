<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Stamp applied when a message is sent to the failure transport.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
final class SentToFailureTransportStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    public function __construct(string $originalReceiverName)
    {
    }
    public function getOriginalReceiverName() : string
    {
    }
}
