<?php

namespace Symfony\Component\Messenger\Transport\Sync;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class SyncTransportFactory implements \Symfony\Component\Messenger\Transport\TransportFactoryInterface
{
    public function __construct(\Symfony\Component\Messenger\MessageBusInterface $messageBus)
    {
    }
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface
    {
    }
    public function supports(string $dsn, array $options) : bool
    {
    }
}
