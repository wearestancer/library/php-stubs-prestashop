<?php

namespace Symfony\Component\Messenger\Transport\Receiver;

/**
 * Used when a receiver has the ability to list messages and find specific messages.
 * A receiver that implements this should add the TransportMessageIdStamp
 * to the Envelopes that it returns.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
interface ListableReceiverInterface extends \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface
{
    /**
     * Returns all the messages (up to the limit) in this receiver.
     *
     * Messages should be given the same stamps as when using ReceiverInterface::get().
     *
     * @return Envelope[]|iterable
     */
    public function all(int $limit = null) : iterable;
    /**
     * Returns the Envelope by id or none.
     *
     * Message should be given the same stamps as when using ReceiverInterface::get().
     */
    public function find($id) : ?\Symfony\Component\Messenger\Envelope;
}
