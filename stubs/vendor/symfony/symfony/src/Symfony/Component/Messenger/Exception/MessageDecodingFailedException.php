<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * Thrown when a message cannot be decoded in a serializer.
 */
class MessageDecodingFailedException extends \Symfony\Component\Messenger\Exception\InvalidArgumentException
{
}
