<?php

namespace Symfony\Component\Messenger\Transport\Serialization;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class Serializer implements \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface
{
    public function __construct(\Symfony\Component\Serializer\SerializerInterface $serializer = null, string $format = 'json', array $context = [])
    {
    }
    public static function create() : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function decode(array $encodedEnvelope) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode(\Symfony\Component\Messenger\Envelope $envelope) : array
    {
    }
}
