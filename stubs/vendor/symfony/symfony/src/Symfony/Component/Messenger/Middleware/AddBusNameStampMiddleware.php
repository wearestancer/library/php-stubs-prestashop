<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * Adds the BusNameStamp to the bus.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class AddBusNameStampMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    public function __construct(string $busName)
    {
    }
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
