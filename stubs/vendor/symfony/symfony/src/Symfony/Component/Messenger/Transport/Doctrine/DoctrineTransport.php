<?php

namespace Symfony\Component\Messenger\Transport\Doctrine;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
class DoctrineTransport implements \Symfony\Component\Messenger\Transport\TransportInterface, \Symfony\Component\Messenger\Transport\SetupableTransportInterface, \Symfony\Component\Messenger\Transport\Receiver\MessageCountAwareInterface, \Symfony\Component\Messenger\Transport\Receiver\ListableReceiverInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\Doctrine\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get() : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessageCount() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all(int $limit = null) : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function find($id) : ?\Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setup() : void
    {
    }
}
