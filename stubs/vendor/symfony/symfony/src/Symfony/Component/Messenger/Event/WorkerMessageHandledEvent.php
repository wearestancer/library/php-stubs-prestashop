<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Dispatched after a message was received from a transport and successfully handled.
 *
 * The event name is the class name.
 */
final class WorkerMessageHandledEvent extends \Symfony\Component\Messenger\Event\AbstractWorkerMessageEvent
{
}
