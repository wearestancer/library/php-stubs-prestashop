<?php

namespace Symfony\Component\Messenger\Event;

abstract class AbstractWorkerMessageEvent
{
    public function __construct(\Symfony\Component\Messenger\Envelope $envelope, string $receiverName)
    {
    }
    public function getEnvelope() : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * Returns a unique identifier for transport receiver this message was received from.
     */
    public function getReceiverName() : string
    {
    }
}
