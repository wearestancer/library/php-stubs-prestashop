<?php

namespace Symfony\Component\Messenger\Command;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class FailedMessagesRetryCommand extends \Symfony\Component\Messenger\Command\AbstractFailedMessagesCommand
{
    protected static $defaultName = 'messenger:failed:retry';
    public function __construct(string $receiverName, \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface $receiver, \Symfony\Component\Messenger\MessageBusInterface $messageBus, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
