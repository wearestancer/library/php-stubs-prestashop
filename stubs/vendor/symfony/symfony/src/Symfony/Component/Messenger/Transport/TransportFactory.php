<?php

namespace Symfony\Component\Messenger\Transport;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class TransportFactory implements \Symfony\Component\Messenger\Transport\TransportFactoryInterface
{
    /**
     * @param iterable|TransportFactoryInterface[] $factories
     */
    public function __construct(iterable $factories)
    {
    }
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface
    {
    }
    public function supports(string $dsn, array $options) : bool
    {
    }
}
