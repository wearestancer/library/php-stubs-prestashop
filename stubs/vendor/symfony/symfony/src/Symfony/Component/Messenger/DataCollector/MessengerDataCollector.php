<?php

namespace Symfony\Component\Messenger\DataCollector;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 *
 * @final since Symfony 4.4
 */
class MessengerDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function registerBus(string $name, \Symfony\Component\Messenger\TraceableMessageBus $bus)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getCasters()
    {
    }
    public function getExceptionsCount(string $bus = null) : int
    {
    }
    public function getMessages(string $bus = null) : array
    {
    }
    public function getBuses() : array
    {
    }
}
