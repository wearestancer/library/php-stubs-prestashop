<?php

namespace Symfony\Component\Messenger\Handler;

/**
 * Marker interface for message handlers.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
interface MessageHandlerInterface
{
}
