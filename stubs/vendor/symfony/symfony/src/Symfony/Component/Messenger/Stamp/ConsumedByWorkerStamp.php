<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * A marker that this message was consumed by a worker process.
 */
class ConsumedByWorkerStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
}
