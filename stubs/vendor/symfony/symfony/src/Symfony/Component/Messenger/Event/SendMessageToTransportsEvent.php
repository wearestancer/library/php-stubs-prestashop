<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Event is dispatched before a message is sent to the transport.
 *
 * The event is *only* dispatched if the message will actually
 * be sent to at least one transport. If the message is sent
 * to multiple transports, the message is dispatched only one time.
 * This message is only dispatched the first time a message
 * is sent to a transport, not also if it is retried.
 *
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
final class SendMessageToTransportsEvent
{
    public function __construct(\Symfony\Component\Messenger\Envelope $envelope)
    {
    }
    public function getEnvelope() : \Symfony\Component\Messenger\Envelope
    {
    }
    public function setEnvelope(\Symfony\Component\Messenger\Envelope $envelope)
    {
    }
}
