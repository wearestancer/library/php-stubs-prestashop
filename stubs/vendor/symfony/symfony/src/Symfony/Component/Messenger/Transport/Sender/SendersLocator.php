<?php

namespace Symfony\Component\Messenger\Transport\Sender;

/**
 * Maps a message to a list of senders.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SendersLocator implements \Symfony\Component\Messenger\Transport\Sender\SendersLocatorInterface
{
    /**
     * @param string[][]         $sendersMap     An array, keyed by "type", set to an array of sender aliases
     * @param ContainerInterface $sendersLocator Locator of senders, keyed by sender alias
     */
    public function __construct(
        array $sendersMap,
        /* ContainerInterface */
        $sendersLocator = null
    )
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSenders(\Symfony\Component\Messenger\Envelope $envelope) : iterable
    {
    }
}
