<?php

namespace Symfony\Component\Messenger;

/**
 * A message wrapped in an envelope with stamps (configurations, markers, ...).
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class Envelope
{
    /**
     * @param object           $message
     * @param StampInterface[] $stamps
     */
    public function __construct($message, array $stamps = [])
    {
    }
    /**
     * Makes sure the message is in an Envelope and adds the given stamps.
     *
     * @param object|Envelope  $message
     * @param StampInterface[] $stamps
     */
    public static function wrap($message, array $stamps = []) : self
    {
    }
    /**
     * @return static A new Envelope instance with additional stamp
     */
    public function with(\Symfony\Component\Messenger\Stamp\StampInterface ...$stamps) : self
    {
    }
    /**
     * @return static A new Envelope instance without any stamps of the given class
     */
    public function withoutAll(string $stampFqcn) : self
    {
    }
    /**
     * Removes all stamps that implement the given type.
     */
    public function withoutStampsOfType(string $type) : self
    {
    }
    public function last(string $stampFqcn) : ?\Symfony\Component\Messenger\Stamp\StampInterface
    {
    }
    /**
     * @return StampInterface[]|StampInterface[][] The stamps for the specified FQCN, or all stamps by their class name
     */
    public function all(string $stampFqcn = null) : array
    {
    }
    /**
     * @return object The original message contained in the envelope
     */
    public function getMessage()
    {
    }
}
