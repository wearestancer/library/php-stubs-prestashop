<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * Stamp applied when a message is received from Amqp.
 */
class AmqpReceivedStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
    public function __construct(\AMQPEnvelope $amqpEnvelope, string $queueName)
    {
    }
    public function getAmqpEnvelope() : \AMQPEnvelope
    {
    }
    public function getQueueName() : string
    {
    }
}
