<?php

namespace Symfony\Component\Messenger\Command;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class StopWorkersCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'messenger:stop-workers';
    public function __construct(\Psr\Cache\CacheItemPoolInterface $restartSignalCachePool)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
