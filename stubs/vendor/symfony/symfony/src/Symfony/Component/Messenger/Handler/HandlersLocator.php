<?php

namespace Symfony\Component\Messenger\Handler;

/**
 * Maps a message to a list of handlers.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class HandlersLocator implements \Symfony\Component\Messenger\Handler\HandlersLocatorInterface
{
    /**
     * @param HandlerDescriptor[][]|callable[][] $handlers
     */
    public function __construct(array $handlers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getHandlers(\Symfony\Component\Messenger\Envelope $envelope) : iterable
    {
    }
    /**
     * @internal
     */
    public static function listTypes(\Symfony\Component\Messenger\Envelope $envelope) : array
    {
    }
}
