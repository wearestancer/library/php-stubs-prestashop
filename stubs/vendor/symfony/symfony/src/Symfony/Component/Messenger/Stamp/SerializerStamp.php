<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class SerializerStamp implements \Symfony\Component\Messenger\Stamp\StampInterface
{
    public function __construct(array $context)
    {
    }
    public function getContext() : array
    {
    }
}
