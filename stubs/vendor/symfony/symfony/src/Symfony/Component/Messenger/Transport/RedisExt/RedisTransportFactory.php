<?php

namespace Symfony\Component\Messenger\Transport\RedisExt;

/**
 * @author Alexander Schranz <alexander@suluio>
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
class RedisTransportFactory implements \Symfony\Component\Messenger\Transport\TransportFactoryInterface
{
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface
    {
    }
    public function supports(string $dsn, array $options) : bool
    {
    }
}
