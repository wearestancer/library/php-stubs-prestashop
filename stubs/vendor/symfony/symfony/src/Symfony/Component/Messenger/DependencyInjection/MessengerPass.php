<?php

namespace Symfony\Component\Messenger\DependencyInjection;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class MessengerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $handlerTag = 'messenger.message_handler', string $busTag = 'messenger.bus', string $receiverTag = 'messenger.receiver')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
