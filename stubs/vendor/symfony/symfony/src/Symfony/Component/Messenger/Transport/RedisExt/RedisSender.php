<?php

namespace Symfony\Component\Messenger\Transport\RedisExt;

/**
 * @author Alexander Schranz <alexander@sulu.io>
 * @author Antoine Bluchet <soyuka@gmail.com>
 */
class RedisSender implements \Symfony\Component\Messenger\Transport\Sender\SenderInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\RedisExt\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function send(\Symfony\Component\Messenger\Envelope $envelope) : \Symfony\Component\Messenger\Envelope
    {
    }
}
