<?php

namespace Symfony\Component\Messenger\Transport\AmqpExt;

/**
 * Symfony Messenger receiver to get messages from AMQP brokers using PHP's AMQP extension.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class AmqpReceiver implements \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface, \Symfony\Component\Messenger\Transport\Receiver\MessageCountAwareInterface
{
    public function __construct(\Symfony\Component\Messenger\Transport\AmqpExt\Connection $connection, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get() : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function ack(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reject(\Symfony\Component\Messenger\Envelope $envelope) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessageCount() : int
    {
    }
}
