<?php

namespace Symfony\Component\Messenger\Command;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 *
 * @internal
 */
abstract class AbstractFailedMessagesCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(string $receiverName, \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface $receiver)
    {
    }
    protected function getReceiverName() : string
    {
    }
    /**
     * @return mixed
     */
    protected function getMessageId(\Symfony\Component\Messenger\Envelope $envelope)
    {
    }
    protected function displaySingleMessage(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Console\Style\SymfonyStyle $io)
    {
    }
    protected function printPendingMessagesMessage(\Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface $receiver, \Symfony\Component\Console\Style\SymfonyStyle $io)
    {
    }
    protected function getReceiver() : \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface
    {
    }
    protected function getLastRedeliveryStampWithException(\Symfony\Component\Messenger\Envelope $envelope) : ?\Symfony\Component\Messenger\Stamp\RedeliveryStamp
    {
    }
}
