<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Eric Masoero <em@studeal.fr>
 */
class TransportException extends \Symfony\Component\Messenger\Exception\RuntimeException
{
}
