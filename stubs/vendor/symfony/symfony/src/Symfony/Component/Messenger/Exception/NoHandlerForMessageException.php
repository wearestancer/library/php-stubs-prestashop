<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class NoHandlerForMessageException extends \Symfony\Component\Messenger\Exception\LogicException
{
}
