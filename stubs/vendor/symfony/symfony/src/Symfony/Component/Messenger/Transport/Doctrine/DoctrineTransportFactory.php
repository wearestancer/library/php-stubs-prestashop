<?php

namespace Symfony\Component\Messenger\Transport\Doctrine;

/**
 * @author Vincent Touzet <vincent.touzet@gmail.com>
 */
class DoctrineTransportFactory implements \Symfony\Component\Messenger\Transport\TransportFactoryInterface
{
    public function __construct($registry)
    {
    }
    public function createTransport(string $dsn, array $options, \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface $serializer) : \Symfony\Component\Messenger\Transport\TransportInterface
    {
    }
    public function supports(string $dsn, array $options) : bool
    {
    }
}
