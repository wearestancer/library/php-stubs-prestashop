<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Dispatched after the worker processed a message or didn't receive a message at all.
 *
 * @author Tobias Schultze <http://tobion.de>
 */
final class WorkerRunningEvent
{
    public function __construct(\Symfony\Component\Messenger\Worker $worker, bool $isWorkerIdle)
    {
    }
    public function getWorker() : \Symfony\Component\Messenger\Worker
    {
    }
    /**
     * Returns true when no message has been received by the worker.
     */
    public function isWorkerIdle() : bool
    {
    }
}
