<?php

namespace Symfony\Component\Messenger\Handler;

/**
 * Maps a message to a list of handlers.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
interface HandlersLocatorInterface
{
    /**
     * Returns the handlers for the given message name.
     *
     * @return iterable|HandlerDescriptor[] Indexed by handler alias if available
     */
    public function getHandlers(\Symfony\Component\Messenger\Envelope $envelope) : iterable;
}
