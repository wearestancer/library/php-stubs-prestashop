<?php

namespace Symfony\Component\Messenger\Stamp;

/**
 * Marker item to tell this message should be handled in after the current bus has finished.
 *
 * @see \Symfony\Component\Messenger\Middleware\DispatchAfterCurrentBusMiddleware
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
final class DispatchAfterCurrentBusStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
}
