<?php

namespace Symfony\Component\Messenger\Transport;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
interface TransportInterface extends \Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface, \Symfony\Component\Messenger\Transport\Sender\SenderInterface
{
}
