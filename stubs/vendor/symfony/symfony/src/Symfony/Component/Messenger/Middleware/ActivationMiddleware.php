<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * Execute the inner middleware according to an activation strategy.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class ActivationMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    /**
     * @param bool|callable $activated
     */
    public function __construct(\Symfony\Component\Messenger\Middleware\MiddlewareInterface $inner, $activated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
