<?php

namespace Symfony\Component\Messenger\Middleware;

/**
 * @author Ryan Weaver <ryan@symfonycasts.com>
 */
class FailedMessageProcessingMiddleware implements \Symfony\Component\Messenger\Middleware\MiddlewareInterface
{
    public function handle(\Symfony\Component\Messenger\Envelope $envelope, \Symfony\Component\Messenger\Middleware\StackInterface $stack) : \Symfony\Component\Messenger\Envelope
    {
    }
}
