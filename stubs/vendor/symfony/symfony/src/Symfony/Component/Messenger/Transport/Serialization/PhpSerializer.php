<?php

namespace Symfony\Component\Messenger\Transport\Serialization;

/**
 * @author Ryan Weaver<ryan@symfonycasts.com>
 */
class PhpSerializer implements \Symfony\Component\Messenger\Transport\Serialization\SerializerInterface
{
    /**
     * {@inheritdoc}
     */
    public function decode(array $encodedEnvelope) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode(\Symfony\Component\Messenger\Envelope $envelope) : array
    {
    }
    /**
     * @internal
     */
    public static function handleUnserializeCallback(string $class)
    {
    }
}
