<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Dispatched when a worker has been stopped.
 *
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
final class WorkerStoppedEvent
{
    public function __construct(\Symfony\Component\Messenger\Worker $worker)
    {
    }
    public function getWorker() : \Symfony\Component\Messenger\Worker
    {
    }
}
