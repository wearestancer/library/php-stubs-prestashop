<?php

namespace Symfony\Component\Messenger\Command;

/**
 * A console command to debug Messenger information.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
class DebugCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'debug:messenger';
    public function __construct(array $mapping)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
