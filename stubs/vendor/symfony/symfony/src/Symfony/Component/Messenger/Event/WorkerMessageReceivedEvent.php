<?php

namespace Symfony\Component\Messenger\Event;

/**
 * Dispatched when a message was received from a transport but before sent to the bus.
 *
 * The event name is the class name.
 */
final class WorkerMessageReceivedEvent extends \Symfony\Component\Messenger\Event\AbstractWorkerMessageEvent
{
    public function shouldHandle(bool $shouldHandle = null) : bool
    {
    }
}
