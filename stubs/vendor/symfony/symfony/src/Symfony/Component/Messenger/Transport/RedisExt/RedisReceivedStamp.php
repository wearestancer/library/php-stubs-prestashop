<?php

namespace Symfony\Component\Messenger\Transport\RedisExt;

/**
 * @author Alexander Schranz <alexander@sulu.io>
 */
class RedisReceivedStamp implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface
{
    public function __construct(string $id)
    {
    }
    public function getId() : string
    {
    }
}
