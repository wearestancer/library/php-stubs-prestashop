<?php

namespace Symfony\Component\Messenger\Transport\RedisExt;

/**
 * A Redis connection.
 *
 * @author Alexander Schranz <alexander@sulu.io>
 * @author Antoine Bluchet <soyuka@gmail.com>
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @internal
 * @final
 */
class Connection
{
    public function __construct(array $configuration, array $connectionCredentials = [], array $redisOptions = [], \Redis $redis = null)
    {
    }
    public static function fromDsn(string $dsn, array $redisOptions = [], \Redis $redis = null) : self
    {
    }
    public function get() : ?array
    {
    }
    public function ack(string $id) : void
    {
    }
    public function reject(string $id) : void
    {
    }
    public function add(string $body, array $headers, int $delayInMs = 0) : void
    {
    }
    public function setup() : void
    {
    }
    public function cleanup() : void
    {
    }
}
