<?php

namespace Symfony\Component\Messenger\Exception;

/**
 * @author Tobias Schultze <http://tobion.de>
 */
class RejectRedeliveredMessageException extends \Symfony\Component\Messenger\Exception\RuntimeException
{
}
