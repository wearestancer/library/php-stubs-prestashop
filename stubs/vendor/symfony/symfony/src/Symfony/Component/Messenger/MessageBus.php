<?php

namespace Symfony\Component\Messenger;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 * @author Matthias Noback <matthiasnoback@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class MessageBus implements \Symfony\Component\Messenger\MessageBusInterface
{
    /**
     * @param MiddlewareInterface[]|iterable $middlewareHandlers
     */
    public function __construct(iterable $middlewareHandlers = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dispatch($message, array $stamps = []) : \Symfony\Component\Messenger\Envelope
    {
    }
}
