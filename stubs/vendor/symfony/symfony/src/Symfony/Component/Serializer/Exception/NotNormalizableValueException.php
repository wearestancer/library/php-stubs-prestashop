<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
class NotNormalizableValueException extends \Symfony\Component\Serializer\Exception\UnexpectedValueException
{
}
