<?php

namespace Symfony\Component\Serializer;

/**
 * Serializer serializes and deserializes data.
 *
 * objects are turned into arrays by normalizers.
 * arrays are turned into various output formats by encoders.
 *
 *     $serializer->serialize($obj, 'xml')
 *     $serializer->decode($data, 'xml')
 *     $serializer->denormalize($data, 'Class', 'xml')
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class Serializer implements \Symfony\Component\Serializer\SerializerInterface, \Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface, \Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface, \Symfony\Component\Serializer\Encoder\ContextAwareEncoderInterface, \Symfony\Component\Serializer\Encoder\ContextAwareDecoderInterface
{
    /**
     * @var Encoder\ChainEncoder
     */
    protected $encoder;
    /**
     * @var Encoder\ChainDecoder
     */
    protected $decoder;
    /**
     * @internal since Symfony 4.1
     */
    protected $normalizers = [];
    /**
     * @param array<NormalizerInterface|DenormalizerInterface> $normalizers
     * @param array<EncoderInterface|DecoderInterface>         $encoders
     */
    public function __construct(array $normalizers = [], array $encoders = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public final function serialize($data, $format, array $context = []) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public final function deserialize($data, $type, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($data, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws NotNormalizableValueException
     */
    public function denormalize($data, $type, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public final function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public final function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format, array $context = [])
    {
    }
}
