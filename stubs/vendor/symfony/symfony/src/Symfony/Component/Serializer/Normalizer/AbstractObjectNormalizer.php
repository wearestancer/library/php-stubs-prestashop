<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * Base class for a normalizer dealing with objects.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class AbstractObjectNormalizer extends \Symfony\Component\Serializer\Normalizer\AbstractNormalizer
{
    /**
     * Set to true to respect the max depth metadata on fields.
     */
    public const ENABLE_MAX_DEPTH = 'enable_max_depth';
    /**
     * How to track the current depth in the context.
     */
    public const DEPTH_KEY_PATTERN = 'depth_%s::%s';
    /**
     * While denormalizing, we can verify that types match.
     *
     * You can disable this by setting this flag to true.
     */
    public const DISABLE_TYPE_ENFORCEMENT = 'disable_type_enforcement';
    /**
     * Flag to control whether fields with the value `null` should be output
     * when normalizing or omitted.
     */
    public const SKIP_NULL_VALUES = 'skip_null_values';
    /**
     * Callback to allow to set a value for an attribute when the max depth has
     * been reached.
     *
     * If no callback is given, the attribute is skipped. If a callable is
     * given, its return value is used (even if null).
     *
     * The arguments are:
     *
     * - mixed  $attributeValue value of this field
     * - object $object         the whole object being normalized
     * - string $attributeName  name of the attribute being normalized
     * - string $format         the requested format
     * - array  $context        the serialization context
     */
    public const MAX_DEPTH_HANDLER = 'max_depth_handler';
    /**
     * Specify which context key are not relevant to determine which attributes
     * of an object to (de)normalize.
     */
    public const EXCLUDE_FROM_CACHE_KEY = 'exclude_from_cache_key';
    /**
     * Flag to tell the denormalizer to also populate existing objects on
     * attributes of the main object.
     *
     * Setting this to true is only useful if you also specify the root object
     * in OBJECT_TO_POPULATE.
     */
    public const DEEP_OBJECT_TO_POPULATE = 'deep_object_to_populate';
    public const PRESERVE_EMPTY_OBJECTS = 'preserve_empty_objects';
    /**
     * @var ClassDiscriminatorResolverInterface|null
     */
    protected $classDiscriminatorResolver;
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface $propertyTypeExtractor = null, \Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface $classDiscriminatorResolver = null, callable $objectClassResolver = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function instantiateObject(array &$data, $class, array &$context, \ReflectionClass $reflectionClass, $allowedAttributes, string $format = null)
    {
    }
    /**
     * Gets and caches attributes for the given object, format and context.
     *
     * @param object      $object
     * @param string|null $format
     *
     * @return string[]
     */
    protected function getAttributes($object, $format, array $context)
    {
    }
    /**
     * Extracts attributes to normalize from the class of the given object, format and context.
     *
     * @param object      $object
     * @param string|null $format
     *
     * @return string[]
     */
    protected abstract function extractAttributes($object, $format = null, array $context = []);
    /**
     * Gets the attribute value.
     *
     * @param object      $object
     * @param string      $attribute
     * @param string|null $format
     *
     * @return mixed
     */
    protected abstract function getAttributeValue($object, $attribute, $format = null, array $context = []);
    /**
     * Sets a handler function that will be called when the max depth is reached.
     *
     * @deprecated since Symfony 4.2
     */
    public function setMaxDepthHandler(?callable $handler) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $type, $format = null, array $context = [])
    {
    }
    /**
     * Sets attribute value.
     *
     * @param object      $object
     * @param string      $attribute
     * @param mixed       $value
     * @param string|null $format
     */
    protected abstract function setAttributeValue($object, $attribute, $value, $format = null, array $context = []);
    /**
     * @internal
     */
    protected function denormalizeParameter(\ReflectionClass $class, \ReflectionParameter $parameter, $parameterName, $parameterData, array $context, $format = null)
    {
    }
    /**
     * Overwritten to update the cache key for the child.
     *
     * We must not mix up the attribute cache between parent and children.
     *
     * {@inheritdoc}
     *
     * @param string|null $format
     *
     * @internal
     */
    protected function createChildContext(array $parentContext, $attribute) : array
    {
    }
}
