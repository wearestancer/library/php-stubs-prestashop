<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * @author Maxime VEBER <maxime.veber@nekland.fr>
 */
class MissingConstructorArgumentsException extends \Symfony\Component\Serializer\Exception\RuntimeException
{
}
