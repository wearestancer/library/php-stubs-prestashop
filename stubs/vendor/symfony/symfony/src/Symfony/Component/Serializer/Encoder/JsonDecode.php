<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Decodes JSON data.
 *
 * @author Sander Coolen <sander@jibber.nl>
 */
class JsonDecode implements \Symfony\Component\Serializer\Encoder\DecoderInterface
{
    protected $serializer;
    /**
     * True to return the result as an associative array, false for a nested stdClass hierarchy.
     */
    public const ASSOCIATIVE = 'json_decode_associative';
    public const OPTIONS = 'json_decode_options';
    /**
     * Specifies the recursion depth.
     */
    public const RECURSION_DEPTH = 'json_decode_recursion_depth';
    /**
     * Constructs a new JsonDecode instance.
     *
     * @param array $defaultContext
     */
    public function __construct($defaultContext = [], int $depth = 512)
    {
    }
    /**
     * Decodes data.
     *
     * @param string $data    The encoded JSON string to decode
     * @param string $format  Must be set to JsonEncoder::FORMAT
     * @param array  $context An optional set of options for the JSON decoder; see below
     *
     * The $context array is a simple key=>value array, with the following supported keys:
     *
     * json_decode_associative: boolean
     *      If true, returns the object as an associative array.
     *      If false, returns the object as nested stdClass
     *      If not specified, this method will use the default set in JsonDecode::__construct
     *
     * json_decode_recursion_depth: integer
     *      Specifies the maximum recursion depth
     *      If not specified, this method will use the default set in JsonDecode::__construct
     *
     * json_decode_options: integer
     *      Specifies additional options as per documentation for json_decode
     *
     * @return mixed
     *
     * @throws NotEncodableValueException
     *
     * @see https://php.net/json_decode
     */
    public function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
    }
}
