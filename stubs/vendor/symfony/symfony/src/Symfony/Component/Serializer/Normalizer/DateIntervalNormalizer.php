<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * Normalizes an instance of {@see \DateInterval} to an interval string.
 * Denormalizes an interval string to an instance of {@see \DateInterval}.
 *
 * @author Jérôme Parmentier <jerome@prmntr.me>
 */
class DateIntervalNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    public const FORMAT_KEY = 'dateinterval_format';
    /**
     * @param array $defaultContext
     */
    public function __construct($defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     *
     * @return string
     */
    public function normalize($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     *
     * @return \DateInterval
     */
    public function denormalize($data, $type, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
    }
}
