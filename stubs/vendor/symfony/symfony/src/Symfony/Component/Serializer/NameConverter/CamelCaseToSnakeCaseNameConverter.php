<?php

namespace Symfony\Component\Serializer\NameConverter;

/**
 * CamelCase to Underscore name converter.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class CamelCaseToSnakeCaseNameConverter implements \Symfony\Component\Serializer\NameConverter\NameConverterInterface
{
    /**
     * @param array|null $attributes     The list of attributes to rename or null for all attributes
     * @param bool       $lowerCamelCase Use lowerCamelCase style
     */
    public function __construct(array $attributes = null, bool $lowerCamelCase = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($propertyName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function denormalize($propertyName)
    {
    }
}
