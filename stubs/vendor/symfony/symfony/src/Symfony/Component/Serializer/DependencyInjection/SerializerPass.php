<?php

namespace Symfony\Component\Serializer\DependencyInjection;

/**
 * Adds all services with the tags "serializer.encoder" and "serializer.normalizer" as
 * encoders and normalizers to the "serializer" service.
 *
 * @author Javier Lopez <f12loalf@gmail.com>
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
class SerializerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    public function __construct(string $serializerService = 'serializer', string $normalizerTag = 'serializer.normalizer', string $encoderTag = 'serializer.encoder')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
