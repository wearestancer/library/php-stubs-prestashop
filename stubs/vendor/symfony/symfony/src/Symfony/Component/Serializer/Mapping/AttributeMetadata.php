<?php

namespace Symfony\Component\Serializer\Mapping;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class AttributeMetadata implements \Symfony\Component\Serializer\Mapping\AttributeMetadataInterface
{
    /**
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getName()} instead.
     */
    public $name;
    /**
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getGroups()} instead.
     */
    public $groups = [];
    /**
     * @var int|null
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getMaxDepth()} instead.
     */
    public $maxDepth;
    /**
     * @var string|null
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getSerializedName()} instead.
     */
    public $serializedName;
    public function __construct(string $name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addGroup($group)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getGroups() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMaxDepth($maxDepth)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMaxDepth()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setSerializedName(string $serializedName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSerializedName() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function merge(\Symfony\Component\Serializer\Mapping\AttributeMetadataInterface $attributeMetadata)
    {
    }
    /**
     * Returns the names of the properties that should be serialized.
     *
     * @return string[]
     */
    public function __sleep()
    {
    }
}
