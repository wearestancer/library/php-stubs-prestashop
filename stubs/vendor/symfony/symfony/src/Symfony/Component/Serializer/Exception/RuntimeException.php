<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * RuntimeException.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Serializer\Exception\ExceptionInterface
{
}
