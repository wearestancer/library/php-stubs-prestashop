<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Encodes YAML data.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class YamlEncoder implements \Symfony\Component\Serializer\Encoder\EncoderInterface, \Symfony\Component\Serializer\Encoder\DecoderInterface
{
    public const FORMAT = 'yaml';
    public const PRESERVE_EMPTY_OBJECTS = 'preserve_empty_objects';
    public function __construct(\Symfony\Component\Yaml\Dumper $dumper = null, \Symfony\Component\Yaml\Parser $parser = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
    }
}
