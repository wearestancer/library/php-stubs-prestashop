<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Decoder delegating the decoding to a chain of decoders.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 *
 * @final
 */
class ChainDecoder implements \Symfony\Component\Serializer\Encoder\ContextAwareDecoderInterface
{
    protected $decoders = [];
    protected $decoderByFormat = [];
    public function __construct(array $decoders = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public final function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format, array $context = []) : bool
    {
    }
}
