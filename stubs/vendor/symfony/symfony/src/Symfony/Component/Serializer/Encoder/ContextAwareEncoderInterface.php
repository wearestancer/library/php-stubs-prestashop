<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Adds the support of an extra $context parameter for the supportsEncoding method.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareEncoderInterface extends \Symfony\Component\Serializer\Encoder\EncoderInterface
{
    /**
     * {@inheritdoc}
     *
     * @param array $context options that encoders have access to
     */
    public function supportsEncoding($format, array $context = []);
}
