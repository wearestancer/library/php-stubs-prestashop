<?php

namespace Symfony\Component\Serializer\Exception;

class BadMethodCallException extends \BadMethodCallException implements \Symfony\Component\Serializer\Exception\ExceptionInterface
{
}
