<?php

namespace Symfony\Component\Serializer;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
interface SerializerAwareInterface
{
    /**
     * Sets the owning Serializer object.
     */
    public function setSerializer(\Symfony\Component\Serializer\SerializerInterface $serializer);
}
