<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Encoder delegating the decoding to a chain of encoders.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 *
 * @final
 */
class ChainEncoder implements \Symfony\Component\Serializer\Encoder\ContextAwareEncoderInterface
{
    protected $encoders = [];
    protected $encoderByFormat = [];
    public function __construct(array $encoders = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public final function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format, array $context = []) : bool
    {
    }
    /**
     * Checks whether the normalization is needed for the given format.
     */
    public function needsNormalization(string $format, array $context = []) : bool
    {
    }
}
