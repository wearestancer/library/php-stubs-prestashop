<?php

namespace Symfony\Component\Serializer\Mapping;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ClassMetadata implements \Symfony\Component\Serializer\Mapping\ClassMetadataInterface
{
    /**
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getName()} instead.
     */
    public $name;
    /**
     * @var AttributeMetadataInterface[]
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getAttributesMetadata()} instead.
     */
    public $attributesMetadata = [];
    /**
     * @var ClassDiscriminatorMapping|null
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getClassDiscriminatorMapping()} instead.
     */
    public $classDiscriminatorMapping;
    /**
     * Constructs a metadata for the given class.
     */
    public function __construct(string $class, \Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping $classDiscriminatorMapping = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addAttributeMetadata(\Symfony\Component\Serializer\Mapping\AttributeMetadataInterface $attributeMetadata)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAttributesMetadata() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function merge(\Symfony\Component\Serializer\Mapping\ClassMetadataInterface $classMetadata)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getReflectionClass() : \ReflectionClass
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassDiscriminatorMapping() : ?\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setClassDiscriminatorMapping(\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping $mapping = null)
    {
    }
    /**
     * Returns the names of the properties that should be serialized.
     *
     * @return string[]
     */
    public function __sleep()
    {
    }
}
