<?php

namespace Symfony\Component\Serializer\Mapping\Loader;

/**
 * Loads {@link ClassMetadataInterface}.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface LoaderInterface
{
    /**
     * @return bool
     */
    public function loadClassMetadata(\Symfony\Component\Serializer\Mapping\ClassMetadataInterface $classMetadata);
}
