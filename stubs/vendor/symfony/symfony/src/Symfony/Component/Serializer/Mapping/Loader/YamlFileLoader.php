<?php

namespace Symfony\Component\Serializer\Mapping\Loader;

/**
 * YAML File Loader.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class YamlFileLoader extends \Symfony\Component\Serializer\Mapping\Loader\FileLoader
{
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Serializer\Mapping\ClassMetadataInterface $classMetadata)
    {
    }
    /**
     * Return the names of the classes mapped in this file.
     *
     * @return string[] The classes names
     */
    public function getMappedClasses()
    {
    }
}
