<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * MappingException.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class MappingException extends \Symfony\Component\Serializer\Exception\RuntimeException
{
}
