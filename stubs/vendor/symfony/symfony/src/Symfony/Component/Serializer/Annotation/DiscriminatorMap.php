<?php

namespace Symfony\Component\Serializer\Annotation;

/**
 * Annotation class for @DiscriminatorMap().
 *
 * @Annotation
 * @Target({"CLASS"})
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class DiscriminatorMap
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct(array $data)
    {
    }
    public function getTypeProperty() : string
    {
    }
    public function getMapping() : array
    {
    }
}
