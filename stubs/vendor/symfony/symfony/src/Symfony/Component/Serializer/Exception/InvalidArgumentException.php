<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * InvalidArgumentException.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Serializer\Exception\ExceptionInterface
{
}
