<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * UnsupportedException.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class UnsupportedException extends \Symfony\Component\Serializer\Exception\InvalidArgumentException
{
}
