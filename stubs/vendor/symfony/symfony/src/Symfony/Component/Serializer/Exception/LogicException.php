<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * LogicException.
 *
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 */
class LogicException extends \LogicException implements \Symfony\Component\Serializer\Exception\ExceptionInterface
{
}
