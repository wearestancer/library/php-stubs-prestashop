<?php

namespace Symfony\Component\Serializer\Mapping;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class ClassDiscriminatorFromClassMetadata implements \Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface
{
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMappingForClass(string $class) : ?\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMappingForMappedObject($object) : ?\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTypeForMappedObject($object) : ?string
    {
    }
}
