<?php

namespace Symfony\Component\Serializer\Mapping\Loader;

/**
 * Annotation loader.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class AnnotationLoader implements \Symfony\Component\Serializer\Mapping\Loader\LoaderInterface
{
    public function __construct(\Doctrine\Common\Annotations\Reader $reader)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Serializer\Mapping\ClassMetadataInterface $classMetadata)
    {
    }
}
