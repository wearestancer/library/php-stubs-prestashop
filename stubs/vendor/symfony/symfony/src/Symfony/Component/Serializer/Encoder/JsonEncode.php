<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Encodes JSON data.
 *
 * @author Sander Coolen <sander@jibber.nl>
 */
class JsonEncode implements \Symfony\Component\Serializer\Encoder\EncoderInterface
{
    public const OPTIONS = 'json_encode_options';
    /**
     * @param array $defaultContext
     */
    public function __construct($defaultContext = [])
    {
    }
    /**
     * Encodes PHP data to a JSON string.
     *
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
    }
}
