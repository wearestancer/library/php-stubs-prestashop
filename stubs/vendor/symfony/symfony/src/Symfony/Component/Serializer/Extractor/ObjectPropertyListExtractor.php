<?php

namespace Symfony\Component\Serializer\Extractor;

/**
 * @author David Maicher <mail@dmaicher.de>
 */
final class ObjectPropertyListExtractor implements \Symfony\Component\Serializer\Extractor\ObjectPropertyListExtractorInterface
{
    public function __construct(\Symfony\Component\PropertyInfo\PropertyListExtractorInterface $propertyListExtractor, callable $objectClassResolver = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProperties($object, array $context = []) : ?array
    {
    }
}
