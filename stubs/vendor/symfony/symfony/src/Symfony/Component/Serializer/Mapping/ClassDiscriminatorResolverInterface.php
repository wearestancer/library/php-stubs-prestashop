<?php

namespace Symfony\Component\Serializer\Mapping;

/**
 * Knows how to get the class discriminator mapping for classes and objects.
 *
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
interface ClassDiscriminatorResolverInterface
{
    public function getMappingForClass(string $class) : ?\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping;
    /**
     * @param object|string $object
     */
    public function getMappingForMappedObject($object) : ?\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping;
    /**
     * @param object|string $object
     */
    public function getTypeForMappedObject($object) : ?string;
}
