<?php

namespace Symfony\Component\Serializer\NameConverter;

/**
 * @author Fabien Bourigault <bourigaultfabien@gmail.com>
 */
final class MetadataAwareNameConverter implements \Symfony\Component\Serializer\NameConverter\AdvancedNameConverterInterface
{
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $metadataFactory, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $fallbackNameConverter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalize($propertyName, string $class = null, string $format = null, array $context = []) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function denormalize($propertyName, string $class = null, string $format = null, array $context = []) : string
    {
    }
}
