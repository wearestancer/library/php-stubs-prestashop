<?php

namespace Symfony\Component\Serializer\Annotation;

/**
 * Annotation class for @Groups().
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD"})
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class Groups
{
    /**
     * @param string[] $groups
     */
    public function __construct(array $data)
    {
    }
    /**
     * @return string[]
     */
    public function getGroups()
    {
    }
}
