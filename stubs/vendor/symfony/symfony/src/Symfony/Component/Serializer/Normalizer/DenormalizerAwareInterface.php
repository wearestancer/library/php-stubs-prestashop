<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * @author Joel Wurtz <joel.wurtz@gmail.com>
 */
interface DenormalizerAwareInterface
{
    /**
     * Sets the owning Denormalizer object.
     */
    public function setDenormalizer(\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer);
}
