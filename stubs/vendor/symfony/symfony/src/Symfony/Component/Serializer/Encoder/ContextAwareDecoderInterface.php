<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Adds the support of an extra $context parameter for the supportsDecoding method.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ContextAwareDecoderInterface extends \Symfony\Component\Serializer\Encoder\DecoderInterface
{
    /**
     * {@inheritdoc}
     *
     * @param array $context options that decoders have access to
     */
    public function supportsDecoding($format, array $context = []);
}
