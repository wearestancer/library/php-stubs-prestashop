<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * @author Joel Wurtz <joel.wurtz@gmail.com>
 */
interface NormalizerAwareInterface
{
    /**
     * Sets the owning Normalizer object.
     */
    public function setNormalizer(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer);
}
