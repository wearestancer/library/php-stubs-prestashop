<?php

namespace Symfony\Component\Serializer\Extractor;

/**
 * @author David Maicher <mail@dmaicher.de>
 */
interface ObjectPropertyListExtractorInterface
{
    /**
     * Gets the list of properties available for the given object.
     *
     * @param object $object
     *
     * @return string[]|null
     */
    public function getProperties($object, array $context = []) : ?array;
}
