<?php

namespace Symfony\Component\Serializer\Mapping;

/**
 * Stores metadata needed for serializing and deserializing objects of specific class.
 *
 * Primarily, the metadata stores the set of attributes to serialize or deserialize.
 *
 * There may only exist one metadata for each attribute according to its name.
 *
 * @internal
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface ClassMetadataInterface
{
    /**
     * Returns the name of the backing PHP class.
     *
     * @return string The name of the backing class
     */
    public function getName() : string;
    /**
     * Adds an {@link AttributeMetadataInterface}.
     */
    public function addAttributeMetadata(\Symfony\Component\Serializer\Mapping\AttributeMetadataInterface $attributeMetadata);
    /**
     * Gets the list of {@link AttributeMetadataInterface}.
     *
     * @return AttributeMetadataInterface[]
     */
    public function getAttributesMetadata() : array;
    /**
     * Merges a {@link ClassMetadataInterface} in the current one.
     */
    public function merge(self $classMetadata);
    /**
     * Returns a {@link \ReflectionClass} instance for this class.
     */
    public function getReflectionClass() : \ReflectionClass;
    public function getClassDiscriminatorMapping() : ?\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping;
    public function setClassDiscriminatorMapping(\Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping $mapping = null);
}
