<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * ExtraAttributesException.
 *
 * @author Julien DIDIER <julien@didier.io>
 */
class ExtraAttributesException extends \Symfony\Component\Serializer\Exception\RuntimeException
{
    public function __construct(array $extraAttributes, \Throwable $previous = null)
    {
    }
    /**
     * Get the extra attributes that are not allowed.
     *
     * @return array
     */
    public function getExtraAttributes()
    {
    }
}
