<?php

namespace Symfony\Component\Serializer\Mapping\Factory;

/**
 * Returns a {@link ClassMetadata}.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ClassMetadataFactory implements \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface
{
    use \Symfony\Component\Serializer\Mapping\Factory\ClassResolverTrait;
    public function __construct(\Symfony\Component\Serializer\Mapping\Loader\LoaderInterface $loader)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($value)
    {
    }
}
