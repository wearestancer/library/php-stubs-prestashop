<?php

namespace Symfony\Component\Serializer\Mapping;

/**
 * @author Samuel Roze <samuel.roze@gmail.com>
 */
class ClassDiscriminatorMapping
{
    public function __construct(string $typeProperty, array $typesMapping = [])
    {
    }
    public function getTypeProperty() : string
    {
    }
    public function getClassForType(string $type) : ?string
    {
    }
    /**
     * @param object|string $object
     */
    public function getMappedObjectType($object) : ?string
    {
    }
    public function getTypesMapping() : array
    {
    }
}
