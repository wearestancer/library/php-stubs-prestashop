<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * UnexpectedValueException.
 *
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 */
class UnexpectedValueException extends \UnexpectedValueException implements \Symfony\Component\Serializer\Exception\ExceptionInterface
{
}
