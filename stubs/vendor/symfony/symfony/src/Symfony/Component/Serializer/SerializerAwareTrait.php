<?php

namespace Symfony\Component\Serializer;

/**
 * @author Joel Wurtz <joel.wurtz@gmail.com>
 */
trait SerializerAwareTrait
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;
    public function setSerializer(\Symfony\Component\Serializer\SerializerInterface $serializer)
    {
    }
}
