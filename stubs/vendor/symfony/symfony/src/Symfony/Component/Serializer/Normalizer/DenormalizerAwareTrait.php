<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * @author Joel Wurtz <joel.wurtz@gmail.com>
 */
trait DenormalizerAwareTrait
{
    /**
     * @var DenormalizerInterface
     */
    protected $denormalizer;
    public function setDenormalizer(\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer)
    {
    }
}
