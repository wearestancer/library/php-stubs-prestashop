<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * @author Joel Wurtz <joel.wurtz@gmail.com>
 */
trait NormalizerAwareTrait
{
    /**
     * @var NormalizerInterface
     */
    protected $normalizer;
    public function setNormalizer(\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer)
    {
    }
}
