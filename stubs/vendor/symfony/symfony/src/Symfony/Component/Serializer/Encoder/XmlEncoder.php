<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 * @author John Wards <jwards@whiteoctober.co.uk>
 * @author Fabian Vogler <fabian@equivalence.ch>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Dany Maillard <danymaillard93b@gmail.com>
 */
class XmlEncoder implements \Symfony\Component\Serializer\Encoder\EncoderInterface, \Symfony\Component\Serializer\Encoder\DecoderInterface, \Symfony\Component\Serializer\Encoder\NormalizationAwareInterface, \Symfony\Component\Serializer\SerializerAwareInterface
{
    use \Symfony\Component\Serializer\SerializerAwareTrait;
    public const FORMAT = 'xml';
    public const AS_COLLECTION = 'as_collection';
    /**
     * An array of ignored XML node types while decoding, each one of the DOM Predefined XML_* constants.
     */
    public const DECODER_IGNORED_NODE_TYPES = 'decoder_ignored_node_types';
    /**
     * An array of ignored XML node types while encoding, each one of the DOM Predefined XML_* constants.
     */
    public const ENCODER_IGNORED_NODE_TYPES = 'encoder_ignored_node_types';
    public const ENCODING = 'xml_encoding';
    public const FORMAT_OUTPUT = 'xml_format_output';
    /**
     * A bit field of LIBXML_* constants.
     */
    public const LOAD_OPTIONS = 'load_options';
    public const REMOVE_EMPTY_TAGS = 'remove_empty_tags';
    public const ROOT_NODE_NAME = 'xml_root_node_name';
    public const STANDALONE = 'xml_standalone';
    /** @deprecated The constant TYPE_CASE_ATTRIBUTES is deprecated since version 4.4 and will be removed in version 5. Use TYPE_CAST_ATTRIBUTES instead. */
    public const TYPE_CASE_ATTRIBUTES = 'xml_type_cast_attributes';
    public const TYPE_CAST_ATTRIBUTES = 'xml_type_cast_attributes';
    public const VERSION = 'xml_version';
    /**
     * @param array $defaultContext
     */
    public function __construct($defaultContext = [], int $loadOptions = null, array $decoderIgnoredNodeTypes = [\XML_PI_NODE, \XML_COMMENT_NODE], array $encoderIgnoredNodeTypes = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
    }
    /**
     * Sets the root node name.
     *
     * @deprecated since Symfony 4.2
     *
     * @param string $name Root node name
     */
    public function setRootNodeName($name)
    {
    }
    /**
     * Returns the root node name.
     *
     * @deprecated since Symfony 4.2
     *
     * @return string
     */
    public function getRootNodeName()
    {
    }
    protected final function appendXMLString(\DOMNode $node, string $val) : bool
    {
    }
    protected final function appendText(\DOMNode $node, string $val) : bool
    {
    }
    protected final function appendCData(\DOMNode $node, string $val) : bool
    {
    }
    /**
     * @param \DOMDocumentFragment $fragment
     */
    protected final function appendDocumentFragment(\DOMNode $node, $fragment) : bool
    {
    }
    protected final function appendComment(\DOMNode $node, string $data) : bool
    {
    }
    /**
     * Checks the name is a valid xml element name.
     */
    protected final function isElementNameValid(string $name) : bool
    {
    }
}
