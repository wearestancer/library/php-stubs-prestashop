<?php

namespace Symfony\Component\Serializer\Annotation;

/**
 * Annotation class for @MaxDepth().
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD"})
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class MaxDepth
{
    public function __construct(array $data)
    {
    }
    public function getMaxDepth()
    {
    }
}
