<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Defines the interface of encoders that will normalize data themselves.
 *
 * Implementing this interface essentially just tells the Serializer that the
 * data should not be pre-normalized before being passed to this Encoder.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
interface NormalizationAwareInterface
{
}
