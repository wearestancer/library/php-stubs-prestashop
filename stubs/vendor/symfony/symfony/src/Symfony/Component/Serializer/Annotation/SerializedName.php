<?php

namespace Symfony\Component\Serializer\Annotation;

/**
 * Annotation class for @SerializedName().
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD"})
 *
 * @author Fabien Bourigault <bourigaultfabien@gmail.com>
 */
final class SerializedName
{
    public function __construct(array $data)
    {
    }
    public function getSerializedName() : string
    {
    }
}
