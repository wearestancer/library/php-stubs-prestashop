<?php

namespace Symfony\Component\Serializer\Exception;

/**
 * CircularReferenceException.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class CircularReferenceException extends \Symfony\Component\Serializer\Exception\RuntimeException
{
}
