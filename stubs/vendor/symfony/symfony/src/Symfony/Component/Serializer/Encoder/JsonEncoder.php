<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Encodes JSON data.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class JsonEncoder implements \Symfony\Component\Serializer\Encoder\EncoderInterface, \Symfony\Component\Serializer\Encoder\DecoderInterface
{
    public const FORMAT = 'json';
    protected $encodingImpl;
    protected $decodingImpl;
    public function __construct(\Symfony\Component\Serializer\Encoder\JsonEncode $encodingImpl = null, \Symfony\Component\Serializer\Encoder\JsonDecode $decodingImpl = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
    }
}
