<?php

namespace Symfony\Component\Serializer\Mapping\Factory;

/**
 * Resolves a class name.
 *
 * @internal
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait ClassResolverTrait
{
    /**
     * Gets a class name for a given class or instance.
     *
     * @param object|string $value
     *
     * @throws InvalidArgumentException If the class does not exist
     */
    private function getClass($value) : string
    {
    }
}
