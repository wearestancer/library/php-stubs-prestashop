<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * Converts between objects and arrays using the PropertyAccess component.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ObjectNormalizer extends \Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer
{
    protected $propertyAccessor;
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface $propertyTypeExtractor = null, \Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface $classDiscriminatorResolver = null, callable $objectClassResolver = null, array $defaultContext = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractAttributes($object, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getAttributeValue($object, $attribute, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function setAttributeValue($object, $attribute, $value, $format = null, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getAllowedAttributes($classOrObject, array $context, $attributesAsString = false)
    {
    }
}
