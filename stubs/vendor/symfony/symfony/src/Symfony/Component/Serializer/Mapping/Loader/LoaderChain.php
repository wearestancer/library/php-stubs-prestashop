<?php

namespace Symfony\Component\Serializer\Mapping\Loader;

/**
 * Calls multiple {@link LoaderInterface} instances in a chain.
 *
 * This class accepts multiple instances of LoaderInterface to be passed to the
 * constructor. When {@link loadClassMetadata()} is called, the same method is called
 * in <em>all</em> of these loaders, regardless of whether any of them was
 * successful or not.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class LoaderChain implements \Symfony\Component\Serializer\Mapping\Loader\LoaderInterface
{
    /**
     * Accepts a list of LoaderInterface instances.
     *
     * @param LoaderInterface[] $loaders An array of LoaderInterface instances
     *
     * @throws MappingException If any of the loaders does not implement LoaderInterface
     */
    public function __construct(array $loaders)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Serializer\Mapping\ClassMetadataInterface $metadata)
    {
    }
    /**
     * @return LoaderInterface[]
     */
    public function getLoaders()
    {
    }
}
