<?php

namespace Symfony\Component\Serializer\Mapping\Loader;

/**
 * Base class for all file based loaders.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class FileLoader implements \Symfony\Component\Serializer\Mapping\Loader\LoaderInterface
{
    protected $file;
    /**
     * @param string $file The mapping file to load
     *
     * @throws MappingException if the mapping file does not exist or is not readable
     */
    public function __construct(string $file)
    {
    }
}
