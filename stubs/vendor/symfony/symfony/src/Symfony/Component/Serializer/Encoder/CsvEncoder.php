<?php

namespace Symfony\Component\Serializer\Encoder;

/**
 * Encodes CSV data.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Oliver Hoff <oliver@hofff.com>
 */
class CsvEncoder implements \Symfony\Component\Serializer\Encoder\EncoderInterface, \Symfony\Component\Serializer\Encoder\DecoderInterface
{
    public const FORMAT = 'csv';
    public const DELIMITER_KEY = 'csv_delimiter';
    public const ENCLOSURE_KEY = 'csv_enclosure';
    public const ESCAPE_CHAR_KEY = 'csv_escape_char';
    public const KEY_SEPARATOR_KEY = 'csv_key_separator';
    public const HEADERS_KEY = 'csv_headers';
    public const ESCAPE_FORMULAS_KEY = 'csv_escape_formulas';
    public const AS_COLLECTION_KEY = 'as_collection';
    public const NO_HEADERS_KEY = 'no_headers';
    public const OUTPUT_UTF8_BOM_KEY = 'output_utf8_bom';
    /**
     * @param array $defaultContext
     */
    public function __construct($defaultContext = [], string $enclosure = '"', string $escapeChar = '', string $keySeparator = '.', bool $escapeFormulas = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
    }
}
