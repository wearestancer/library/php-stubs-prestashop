<?php

namespace Symfony\Component\Serializer\Normalizer;

/**
 * Normalizer implementation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class AbstractNormalizer implements \Symfony\Component\Serializer\Normalizer\NormalizerInterface, \Symfony\Component\Serializer\Normalizer\DenormalizerInterface, \Symfony\Component\Serializer\SerializerAwareInterface, \Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface
{
    use \Symfony\Component\Serializer\Normalizer\ObjectToPopulateTrait;
    use \Symfony\Component\Serializer\SerializerAwareTrait;
    /* constants to configure the context */
    /**
     * How many loops of circular reference to allow while normalizing.
     *
     * The default value of 1 means that when we encounter the same object a
     * second time, we consider that a circular reference.
     *
     * You can raise this value for special cases, e.g. in combination with the
     * max depth setting of the object normalizer.
     */
    public const CIRCULAR_REFERENCE_LIMIT = 'circular_reference_limit';
    /**
     * Instead of creating a new instance of an object, update the specified object.
     *
     * If you have a nested structure, child objects will be overwritten with
     * new instances unless you set DEEP_OBJECT_TO_POPULATE to true.
     */
    public const OBJECT_TO_POPULATE = 'object_to_populate';
    /**
     * Only (de)normalize attributes that are in the specified groups.
     */
    public const GROUPS = 'groups';
    /**
     * Limit (de)normalize to the specified names.
     *
     * For nested structures, this list needs to reflect the object tree.
     */
    public const ATTRIBUTES = 'attributes';
    /**
     * If ATTRIBUTES are specified, and the source has fields that are not part of that list,
     * either ignore those attributes (true) or throw an ExtraAttributesException (false).
     */
    public const ALLOW_EXTRA_ATTRIBUTES = 'allow_extra_attributes';
    /**
     * Hashmap of default values for constructor arguments.
     *
     * The names need to match the parameter names in the constructor arguments.
     */
    public const DEFAULT_CONSTRUCTOR_ARGUMENTS = 'default_constructor_arguments';
    /**
     * Hashmap of field name => callable to (de)normalize this field.
     *
     * The callable is called if the field is encountered with the arguments:
     *
     * - mixed         $attributeValue value of this field
     * - object|string $object         the whole object being normalized or the object's class being denormalized
     * - string        $attributeName  name of the attribute being (de)normalized
     * - string        $format         the requested format
     * - array         $context        the serialization context
     */
    public const CALLBACKS = 'callbacks';
    /**
     * Handler to call when a circular reference has been detected.
     *
     * If you specify no handler, a CircularReferenceException is thrown.
     *
     * The method will be called with ($object, $format, $context) and its
     * return value is returned as the result of the normalize call.
     */
    public const CIRCULAR_REFERENCE_HANDLER = 'circular_reference_handler';
    /**
     * Skip the specified attributes when normalizing an object tree.
     *
     * This list is applied to each element of nested structures.
     *
     * Note: The behaviour for nested structures is different from ATTRIBUTES
     * for historical reason. Aligning the behaviour would be a BC break.
     */
    public const IGNORED_ATTRIBUTES = 'ignored_attributes';
    /**
     * @internal
     */
    protected const CIRCULAR_REFERENCE_LIMIT_COUNTERS = 'circular_reference_limit_counters';
    protected $defaultContext = [self::ALLOW_EXTRA_ATTRIBUTES => true, self::CIRCULAR_REFERENCE_LIMIT => 1, self::IGNORED_ATTRIBUTES => []];
    /**
     * @deprecated since Symfony 4.2
     */
    protected $circularReferenceLimit = 1;
    /**
     * @deprecated since Symfony 4.2
     *
     * @var callable|null
     */
    protected $circularReferenceHandler;
    /**
     * @var ClassMetadataFactoryInterface|null
     */
    protected $classMetadataFactory;
    /**
     * @var NameConverterInterface|null
     */
    protected $nameConverter;
    /**
     * @deprecated since Symfony 4.2
     */
    protected $callbacks = [];
    /**
     * @deprecated since Symfony 4.2
     */
    protected $ignoredAttributes = [];
    /**
     * @deprecated since Symfony 4.2
     */
    protected $camelizedAttributes = [];
    /**
     * Sets the {@link ClassMetadataFactoryInterface} to use.
     */
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $classMetadataFactory = null, \Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null, array $defaultContext = [])
    {
    }
    /**
     * Sets circular reference limit.
     *
     * @deprecated since Symfony 4.2
     *
     * @param int $circularReferenceLimit Limit of iterations for the same object
     *
     * @return self
     */
    public function setCircularReferenceLimit($circularReferenceLimit)
    {
    }
    /**
     * Sets circular reference handler.
     *
     * @deprecated since Symfony 4.2
     *
     * @return self
     */
    public function setCircularReferenceHandler(callable $circularReferenceHandler)
    {
    }
    /**
     * Sets (de)normalization callbacks.
     *
     * @deprecated since Symfony 4.2
     *
     * @param callable[] $callbacks Help (de)normalize the result
     *
     * @return self
     *
     * @throws InvalidArgumentException if a non-callable callback is set
     */
    public function setCallbacks(array $callbacks)
    {
    }
    /**
     * Sets ignored attributes for normalization and denormalization.
     *
     * @deprecated since Symfony 4.2
     *
     * @return self
     */
    public function setIgnoredAttributes(array $ignoredAttributes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod() : bool
    {
    }
    /**
     * Detects if the configured circular reference limit is reached.
     *
     * @param object $object
     * @param array  $context
     *
     * @return bool
     *
     * @throws CircularReferenceException
     */
    protected function isCircularReference($object, &$context)
    {
    }
    /**
     * Handles a circular reference.
     *
     * If a circular reference handler is set, it will be called. Otherwise, a
     * {@class CircularReferenceException} will be thrown.
     *
     * @final since Symfony 4.2
     *
     * @param object      $object
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed
     *
     * @throws CircularReferenceException
     */
    protected function handleCircularReference($object)
    {
    }
    /**
     * Gets attributes to normalize using groups.
     *
     * @param string|object $classOrObject
     * @param bool          $attributesAsString If false, return an array of {@link AttributeMetadataInterface}
     *
     * @throws LogicException if the 'allow_extra_attributes' context variable is false and no class metadata factory is provided
     *
     * @return string[]|AttributeMetadataInterface[]|bool
     */
    protected function getAllowedAttributes($classOrObject, array $context, $attributesAsString = false)
    {
    }
    /**
     * Is this attribute allowed?
     *
     * @param object|string $classOrObject
     * @param string        $attribute
     * @param string|null   $format
     *
     * @return bool
     */
    protected function isAllowedAttribute($classOrObject, $attribute, $format = null, array $context = [])
    {
    }
    /**
     * Normalizes the given data to an array. It's particularly useful during
     * the denormalization process.
     *
     * @param object|array $data
     *
     * @return array
     */
    protected function prepareForDenormalization($data)
    {
    }
    /**
     * Returns the method to use to construct an object. This method must be either
     * the object constructor or static.
     *
     * @param string     $class
     * @param array|bool $allowedAttributes
     *
     * @return \ReflectionMethod|null
     */
    protected function getConstructor(array &$data, $class, array &$context, \ReflectionClass $reflectionClass, $allowedAttributes)
    {
    }
    /**
     * Instantiates an object using constructor parameters when needed.
     *
     * This method also allows to denormalize data into an existing object if
     * it is present in the context with the object_to_populate. This object
     * is removed from the context before being returned to avoid side effects
     * when recursively normalizing an object graph.
     *
     * @param string     $class
     * @param array|bool $allowedAttributes
     *
     * @return object
     *
     * @throws RuntimeException
     * @throws MissingConstructorArgumentsException
     */
    protected function instantiateObject(array &$data, $class, array &$context, \ReflectionClass $reflectionClass, $allowedAttributes, string $format = null)
    {
    }
    /**
     * @internal
     */
    protected function denormalizeParameter(\ReflectionClass $class, \ReflectionParameter $parameter, $parameterName, $parameterData, array $context, $format = null)
    {
    }
    /**
     * @param string $attribute Attribute name
     *
     * @internal
     */
    protected function createChildContext(array $parentContext, $attribute) : array
    {
    }
    /**
     * Validate callbacks set in context.
     *
     * @param string $contextType Used to specify which context is invalid in exceptions
     *
     * @throws InvalidArgumentException
     */
    protected final function validateCallbackContext(array $context, string $contextType = '') : void
    {
    }
    /**
     * Apply callbacks set in context.
     *
     * @param mixed         $value
     * @param object|string $object Can be either the object being normalizing or the object's class being denormalized
     *
     * @return mixed
     */
    protected final function applyCallbacks($value, $object, string $attribute, ?string $format, array $context)
    {
    }
}
