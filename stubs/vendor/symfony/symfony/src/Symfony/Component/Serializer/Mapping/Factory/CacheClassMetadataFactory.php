<?php

namespace Symfony\Component\Serializer\Mapping\Factory;

/**
 * Caches metadata using a PSR-6 implementation.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class CacheClassMetadataFactory implements \Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface
{
    use \Symfony\Component\Serializer\Mapping\Factory\ClassResolverTrait;
    public function __construct(\Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface $decorated, \Psr\Cache\CacheItemPoolInterface $cacheItemPool)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($value)
    {
    }
}
