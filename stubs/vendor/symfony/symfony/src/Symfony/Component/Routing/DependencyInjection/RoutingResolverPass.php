<?php

namespace Symfony\Component\Routing\DependencyInjection;

/**
 * Adds tagged routing.loader services to routing.resolver service.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RoutingResolverPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    public function __construct(string $resolverServiceId = 'routing.resolver', string $loaderTag = 'routing.loader')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
