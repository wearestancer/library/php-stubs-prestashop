<?php

namespace Symfony\Component\Routing\Generator\Dumper;

/**
 * GeneratorDumper is the base class for all built-in generator dumpers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class GeneratorDumper implements \Symfony\Component\Routing\Generator\Dumper\GeneratorDumperInterface
{
    public function __construct(\Symfony\Component\Routing\RouteCollection $routes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoutes()
    {
    }
}
