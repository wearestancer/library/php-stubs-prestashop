<?php

namespace Symfony\Component\Routing\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CollectionConfigurator
{
    use \Symfony\Component\Routing\Loader\Configurator\Traits\AddTrait;
    use \Symfony\Component\Routing\Loader\Configurator\Traits\RouteTrait;
    public function __construct(\Symfony\Component\Routing\RouteCollection $parent, string $name, self $parentConfigurator = null, array $parentPrefixes = null)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
    /**
     * Creates a sub-collection.
     */
    public final function collection(string $name = '') : self
    {
    }
    /**
     * Sets the prefix to add to the path of all child routes.
     *
     * @param string|array $prefix the prefix, or the localized prefixes
     *
     * @return $this
     */
    public final function prefix($prefix) : self
    {
    }
}
