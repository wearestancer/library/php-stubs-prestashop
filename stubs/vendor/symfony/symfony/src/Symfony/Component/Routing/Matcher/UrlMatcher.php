<?php

namespace Symfony\Component\Routing\Matcher;

/**
 * UrlMatcher matches URL based on a set of routes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UrlMatcher implements \Symfony\Component\Routing\Matcher\UrlMatcherInterface, \Symfony\Component\Routing\Matcher\RequestMatcherInterface
{
    public const REQUIREMENT_MATCH = 0;
    public const REQUIREMENT_MISMATCH = 1;
    public const ROUTE_MATCH = 2;
    /** @var RequestContext */
    protected $context;
    /**
     * Collects HTTP methods that would be allowed for the request.
     */
    protected $allow = [];
    /**
     * Collects URI schemes that would be allowed for the request.
     *
     * @internal
     */
    protected $allowSchemes = [];
    protected $routes;
    protected $request;
    protected $expressionLanguage;
    /**
     * @var ExpressionFunctionProviderInterface[]
     */
    protected $expressionLanguageProviders = [];
    public function __construct(\Symfony\Component\Routing\RouteCollection $routes, \Symfony\Component\Routing\RequestContext $context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setContext(\Symfony\Component\Routing\RequestContext $context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContext()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function match($pathinfo)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function matchRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    public function addExpressionLanguageProvider(\Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface $provider)
    {
    }
    /**
     * Tries to match a URL with a set of routes.
     *
     * @param string $pathinfo The path info to be parsed
     *
     * @return array An array of parameters
     *
     * @throws NoConfigurationException  If no routing configuration could be found
     * @throws ResourceNotFoundException If the resource could not be found
     * @throws MethodNotAllowedException If the resource was found but the request method is not allowed
     */
    protected function matchCollection($pathinfo, \Symfony\Component\Routing\RouteCollection $routes)
    {
    }
    /**
     * Returns an array of values to use as request attributes.
     *
     * As this method requires the Route object, it is not available
     * in matchers that do not have access to the matched Route instance
     * (like the PHP and Apache matcher dumpers).
     *
     * @param string $name       The name of the route
     * @param array  $attributes An array of attributes from the matcher
     *
     * @return array An array of parameters
     */
    protected function getAttributes(\Symfony\Component\Routing\Route $route, $name, array $attributes)
    {
    }
    /**
     * Handles specific route requirements.
     *
     * @param string $pathinfo The path
     * @param string $name     The route name
     *
     * @return array The first element represents the status, the second contains additional information
     */
    protected function handleRouteRequirements($pathinfo, $name, \Symfony\Component\Routing\Route $route)
    {
    }
    /**
     * Get merged default parameters.
     *
     * @param array $params   The parameters
     * @param array $defaults The defaults
     *
     * @return array Merged default parameters
     */
    protected function mergeDefaults($params, $defaults)
    {
    }
    protected function getExpressionLanguage()
    {
    }
    /**
     * @internal
     */
    protected function createRequest(string $pathinfo) : ?\Symfony\Component\HttpFoundation\Request
    {
    }
}
