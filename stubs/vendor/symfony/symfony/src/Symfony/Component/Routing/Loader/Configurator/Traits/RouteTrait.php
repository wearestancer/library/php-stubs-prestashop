<?php

namespace Symfony\Component\Routing\Loader\Configurator\Traits;

trait RouteTrait
{
    /**
     * @var RouteCollection|Route
     */
    private $route;
    /**
     * Adds defaults.
     *
     * @return $this
     */
    public final function defaults(array $defaults) : self
    {
    }
    /**
     * Adds requirements.
     *
     * @return $this
     */
    public final function requirements(array $requirements) : self
    {
    }
    /**
     * Adds options.
     *
     * @return $this
     */
    public final function options(array $options) : self
    {
    }
    /**
     * Whether paths should accept utf8 encoding.
     *
     * @return $this
     */
    public final function utf8(bool $utf8 = true) : self
    {
    }
    /**
     * Sets the condition.
     *
     * @return $this
     */
    public final function condition(string $condition) : self
    {
    }
    /**
     * Sets the pattern for the host.
     *
     * @return $this
     */
    public final function host(string $pattern) : self
    {
    }
    /**
     * Sets the schemes (e.g. 'https') this route is restricted to.
     * So an empty array means that any scheme is allowed.
     *
     * @param string[] $schemes
     *
     * @return $this
     */
    public final function schemes(array $schemes) : self
    {
    }
    /**
     * Sets the HTTP methods (e.g. 'POST') this route is restricted to.
     * So an empty array means that any method is allowed.
     *
     * @param string[] $methods
     *
     * @return $this
     */
    public final function methods(array $methods) : self
    {
    }
    /**
     * Adds the "_controller" entry to defaults.
     *
     * @param callable|string|array $controller a callable or parseable pseudo-callable
     *
     * @return $this
     */
    public final function controller($controller) : self
    {
    }
    /**
     * Adds the "_locale" entry to defaults.
     *
     * @return $this
     */
    public final function locale(string $locale) : self
    {
    }
    /**
     * Adds the "_format" entry to defaults.
     *
     * @return $this
     */
    public final function format(string $format) : self
    {
    }
}
