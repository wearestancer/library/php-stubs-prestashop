<?php

namespace Symfony\Component\Routing\Matcher\Dumper;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 *
 * @property RequestContext $context
 */
trait CompiledUrlMatcherTrait
{
    private $matchHost = false;
    private $staticRoutes = [];
    private $regexpList = [];
    private $dynamicRoutes = [];
    private $checkCondition;
    public function match($pathinfo) : array
    {
    }
    private function doMatch(string $pathinfo, array &$allow = [], array &$allowSchemes = []) : array
    {
    }
}
