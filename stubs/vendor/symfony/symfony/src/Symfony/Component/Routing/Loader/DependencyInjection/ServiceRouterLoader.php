<?php

namespace Symfony\Component\Routing\Loader\DependencyInjection;

/**
 * A route loader that executes a service to load the routes.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\Routing\Loader\ContainerLoader instead.
 */
class ServiceRouterLoader extends \Symfony\Component\Routing\Loader\ObjectRouteLoader
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    protected function getServiceObject($id)
    {
    }
}
