<?php

namespace Symfony\Component\Routing\Loader;

/**
 * A route loader that executes a service from a PSR-11 container to load the routes.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class ContainerLoader extends \Symfony\Component\Routing\Loader\ObjectLoader
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getObject(string $id)
    {
    }
}
