<?php

namespace Symfony\Component\Routing\Matcher;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class RedirectableUrlMatcher extends \Symfony\Component\Routing\Matcher\UrlMatcher implements \Symfony\Component\Routing\Matcher\RedirectableUrlMatcherInterface
{
    /**
     * {@inheritdoc}
     */
    public function match($pathinfo)
    {
    }
}
