<?php

namespace Symfony\Component\Routing\Loader;

/**
 * YamlFileLoader loads Yaml routing files.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Tobias Schultze <http://tobion.de>
 */
class YamlFileLoader extends \Symfony\Component\Config\Loader\FileLoader
{
    /**
     * Loads a Yaml file.
     *
     * @param string      $file A Yaml file path
     * @param string|null $type The resource type
     *
     * @return RouteCollection A RouteCollection instance
     *
     * @throws \InvalidArgumentException When a route can't be parsed because YAML is invalid
     */
    public function load($file, $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
    }
    /**
     * Parses a route and adds it to the RouteCollection.
     *
     * @param string $name   Route name
     * @param array  $config Route definition
     * @param string $path   Full path of the YAML file being processed
     */
    protected function parseRoute(\Symfony\Component\Routing\RouteCollection $collection, $name, array $config, $path)
    {
    }
    /**
     * Parses an import and adds the routes in the resource to the RouteCollection.
     *
     * @param array  $config Route definition
     * @param string $path   Full path of the YAML file being processed
     * @param string $file   Loaded file name
     */
    protected function parseImport(\Symfony\Component\Routing\RouteCollection $collection, array $config, $path, $file)
    {
    }
    /**
     * Validates the route configuration.
     *
     * @param array  $config A resource config
     * @param string $name   The config key
     * @param string $path   The loaded file path
     *
     * @throws \InvalidArgumentException If one of the provided config keys is not supported,
     *                                   something is missing or the combination is nonsense
     */
    protected function validate($config, $name, $path)
    {
    }
}
