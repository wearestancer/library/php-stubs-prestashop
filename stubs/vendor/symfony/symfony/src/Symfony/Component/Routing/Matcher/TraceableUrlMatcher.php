<?php

namespace Symfony\Component\Routing\Matcher;

/**
 * TraceableUrlMatcher helps debug path info matching by tracing the match.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TraceableUrlMatcher extends \Symfony\Component\Routing\Matcher\UrlMatcher
{
    public const ROUTE_DOES_NOT_MATCH = 0;
    public const ROUTE_ALMOST_MATCHES = 1;
    public const ROUTE_MATCHES = 2;
    protected $traces;
    public function getTraces($pathinfo)
    {
    }
    public function getTracesForRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    protected function matchCollection($pathinfo, \Symfony\Component\Routing\RouteCollection $routes)
    {
    }
}
