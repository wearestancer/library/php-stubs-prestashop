<?php

namespace Symfony\Component\Routing\Matcher\Dumper;

/**
 * MatcherDumper is the abstract class for all built-in matcher dumpers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class MatcherDumper implements \Symfony\Component\Routing\Matcher\Dumper\MatcherDumperInterface
{
    public function __construct(\Symfony\Component\Routing\RouteCollection $routes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoutes()
    {
    }
}
