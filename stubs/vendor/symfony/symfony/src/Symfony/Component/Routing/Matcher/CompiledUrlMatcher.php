<?php

namespace Symfony\Component\Routing\Matcher;

/**
 * Matches URLs based on rules dumped by CompiledUrlMatcherDumper.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CompiledUrlMatcher extends \Symfony\Component\Routing\Matcher\UrlMatcher
{
    use \Symfony\Component\Routing\Matcher\Dumper\CompiledUrlMatcherTrait;
    public function __construct(array $compiledRoutes, \Symfony\Component\Routing\RequestContext $context)
    {
    }
}
