<?php

namespace Symfony\Component\Routing\Loader;

/**
 * A route loader that calls a method on an object to load the routes.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
abstract class ObjectLoader extends \Symfony\Component\Config\Loader\Loader
{
    /**
     * Returns the object that the method will be called on to load routes.
     *
     * For example, if your application uses a service container,
     * the $id may be a service id.
     *
     * @return object
     */
    protected abstract function getObject(string $id);
    /**
     * Calls the object method that will load the routes.
     *
     * @param string      $resource object_id::method
     * @param string|null $type     The resource type
     *
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
    }
}
