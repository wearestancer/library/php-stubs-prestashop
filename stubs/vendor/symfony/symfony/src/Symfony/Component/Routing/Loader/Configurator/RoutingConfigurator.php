<?php

namespace Symfony\Component\Routing\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class RoutingConfigurator
{
    use \Symfony\Component\Routing\Loader\Configurator\Traits\AddTrait;
    public function __construct(\Symfony\Component\Routing\RouteCollection $collection, \Symfony\Component\Routing\Loader\PhpFileLoader $loader, string $path, string $file)
    {
    }
    /**
     * @param string|string[]|null $exclude Glob patterns to exclude from the import
     */
    public final function import($resource, string $type = null, bool $ignoreErrors = false, $exclude = null) : \Symfony\Component\Routing\Loader\Configurator\ImportConfigurator
    {
    }
    public final function collection(string $name = '') : \Symfony\Component\Routing\Loader\Configurator\CollectionConfigurator
    {
    }
}
