<?php

namespace Symfony\Component\Routing\Generator;

/**
 * Generates URLs based on rules dumped by CompiledUrlGeneratorDumper.
 */
class CompiledUrlGenerator extends \Symfony\Component\Routing\Generator\UrlGenerator
{
    public function __construct(array $compiledRoutes, \Symfony\Component\Routing\RequestContext $context, \Psr\Log\LoggerInterface $logger = null, string $defaultLocale = null)
    {
    }
    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
    }
}
