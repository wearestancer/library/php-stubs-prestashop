<?php

namespace Symfony\Component\Routing;

/**
 * CompiledRoutes are returned by the RouteCompiler class.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class CompiledRoute implements \Serializable
{
    /**
     * @param string      $staticPrefix  The static prefix of the compiled route
     * @param string      $regex         The regular expression to use to match this route
     * @param array       $tokens        An array of tokens to use to generate URL for this route
     * @param array       $pathVariables An array of path variables
     * @param string|null $hostRegex     Host regex
     * @param array       $hostTokens    Host tokens
     * @param array       $hostVariables An array of host variables
     * @param array       $variables     An array of variables (variables defined in the path and in the host patterns)
     */
    public function __construct(string $staticPrefix, string $regex, array $tokens, array $pathVariables, string $hostRegex = null, array $hostTokens = [], array $hostVariables = [], array $variables = [])
    {
    }
    public function __serialize() : array
    {
    }
    /**
     * @return string
     *
     * @internal since Symfony 4.3
     * @final since Symfony 4.3
     */
    public function serialize()
    {
    }
    public function __unserialize(array $data) : void
    {
    }
    /**
     * @internal since Symfony 4.3
     * @final since Symfony 4.3
     */
    public function unserialize($serialized)
    {
    }
    /**
     * Returns the static prefix.
     *
     * @return string The static prefix
     */
    public function getStaticPrefix()
    {
    }
    /**
     * Returns the regex.
     *
     * @return string The regex
     */
    public function getRegex()
    {
    }
    /**
     * Returns the host regex.
     *
     * @return string|null The host regex or null
     */
    public function getHostRegex()
    {
    }
    /**
     * Returns the tokens.
     *
     * @return array The tokens
     */
    public function getTokens()
    {
    }
    /**
     * Returns the host tokens.
     *
     * @return array The tokens
     */
    public function getHostTokens()
    {
    }
    /**
     * Returns the variables.
     *
     * @return array The variables
     */
    public function getVariables()
    {
    }
    /**
     * Returns the path variables.
     *
     * @return array The variables
     */
    public function getPathVariables()
    {
    }
    /**
     * Returns the host variables.
     *
     * @return array The variables
     */
    public function getHostVariables()
    {
    }
}
