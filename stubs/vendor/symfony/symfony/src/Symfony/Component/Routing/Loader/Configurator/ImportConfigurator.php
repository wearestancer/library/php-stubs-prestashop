<?php

namespace Symfony\Component\Routing\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ImportConfigurator
{
    use \Symfony\Component\Routing\Loader\Configurator\Traits\RouteTrait;
    public function __construct(\Symfony\Component\Routing\RouteCollection $parent, \Symfony\Component\Routing\RouteCollection $route)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
    /**
     * Sets the prefix to add to the path of all child routes.
     *
     * @param string|array $prefix the prefix, or the localized prefixes
     *
     * @return $this
     */
    public final function prefix($prefix, bool $trailingSlashOnRoot = true) : self
    {
    }
    /**
     * Sets the prefix to add to the name of all child routes.
     *
     * @return $this
     */
    public final function namePrefix(string $namePrefix) : self
    {
    }
}
