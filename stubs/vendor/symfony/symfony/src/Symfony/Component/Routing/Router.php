<?php

namespace Symfony\Component\Routing;

/**
 * The Router class is an example of the integration of all pieces of the
 * routing system for easier use.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Router implements \Symfony\Component\Routing\RouterInterface, \Symfony\Component\Routing\Matcher\RequestMatcherInterface
{
    /**
     * @var UrlMatcherInterface|null
     */
    protected $matcher;
    /**
     * @var UrlGeneratorInterface|null
     */
    protected $generator;
    /**
     * @var RequestContext
     */
    protected $context;
    /**
     * @var LoaderInterface
     */
    protected $loader;
    /**
     * @var RouteCollection|null
     */
    protected $collection;
    /**
     * @var mixed
     */
    protected $resource;
    /**
     * @var array
     */
    protected $options = [];
    /**
     * @var LoggerInterface|null
     */
    protected $logger;
    /**
     * @var string|null
     */
    protected $defaultLocale;
    /**
     * @param mixed $resource The main resource to load
     */
    public function __construct(\Symfony\Component\Config\Loader\LoaderInterface $loader, $resource, array $options = [], \Symfony\Component\Routing\RequestContext $context = null, \Psr\Log\LoggerInterface $logger = null, string $defaultLocale = null)
    {
    }
    /**
     * Sets options.
     *
     * Available options:
     *
     *   * cache_dir:              The cache directory (or null to disable caching)
     *   * debug:                  Whether to enable debugging or not (false by default)
     *   * generator_class:        The name of a UrlGeneratorInterface implementation
     *   * generator_dumper_class: The name of a GeneratorDumperInterface implementation
     *   * matcher_class:          The name of a UrlMatcherInterface implementation
     *   * matcher_dumper_class:   The name of a MatcherDumperInterface implementation
     *   * resource_type:          Type hint for the main resource (optional)
     *   * strict_requirements:    Configure strict requirement checking for generators
     *                             implementing ConfigurableRequirementsInterface (default is true)
     *
     * @throws \InvalidArgumentException When unsupported option is provided
     */
    public function setOptions(array $options)
    {
    }
    /**
     * Sets an option.
     *
     * @param string $key   The key
     * @param mixed  $value The value
     *
     * @throws \InvalidArgumentException
     */
    public function setOption($key, $value)
    {
    }
    /**
     * Gets an option value.
     *
     * @param string $key The key
     *
     * @return mixed The value
     *
     * @throws \InvalidArgumentException
     */
    public function getOption($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRouteCollection()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setContext(\Symfony\Component\Routing\RequestContext $context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContext()
    {
    }
    /**
     * Sets the ConfigCache factory to use.
     */
    public function setConfigCacheFactory(\Symfony\Component\Config\ConfigCacheFactoryInterface $configCacheFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function match($pathinfo)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function matchRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Gets the UrlMatcher or RequestMatcher instance associated with this Router.
     *
     * @return UrlMatcherInterface|RequestMatcherInterface
     */
    public function getMatcher()
    {
    }
    /**
     * Gets the UrlGenerator instance associated with this Router.
     *
     * @return UrlGeneratorInterface A UrlGeneratorInterface instance
     */
    public function getGenerator()
    {
    }
    public function addExpressionLanguageProvider(\Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface $provider)
    {
    }
    /**
     * @return GeneratorDumperInterface
     */
    protected function getGeneratorDumperInstance()
    {
    }
    /**
     * @return MatcherDumperInterface
     */
    protected function getMatcherDumperInstance()
    {
    }
}
