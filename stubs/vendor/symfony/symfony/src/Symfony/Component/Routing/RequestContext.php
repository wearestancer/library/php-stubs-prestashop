<?php

namespace Symfony\Component\Routing;

/**
 * Holds information about the current request.
 *
 * This class implements a fluent interface.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Tobias Schultze <http://tobion.de>
 */
class RequestContext
{
    public function __construct(string $baseUrl = '', string $method = 'GET', string $host = 'localhost', string $scheme = 'http', int $httpPort = 80, int $httpsPort = 443, string $path = '/', string $queryString = '')
    {
    }
    /**
     * Updates the RequestContext information based on a HttpFoundation Request.
     *
     * @return $this
     */
    public function fromRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Gets the base URL.
     *
     * @return string The base URL
     */
    public function getBaseUrl()
    {
    }
    /**
     * Sets the base URL.
     *
     * @param string $baseUrl The base URL
     *
     * @return $this
     */
    public function setBaseUrl($baseUrl)
    {
    }
    /**
     * Gets the path info.
     *
     * @return string The path info
     */
    public function getPathInfo()
    {
    }
    /**
     * Sets the path info.
     *
     * @param string $pathInfo The path info
     *
     * @return $this
     */
    public function setPathInfo($pathInfo)
    {
    }
    /**
     * Gets the HTTP method.
     *
     * The method is always an uppercased string.
     *
     * @return string The HTTP method
     */
    public function getMethod()
    {
    }
    /**
     * Sets the HTTP method.
     *
     * @param string $method The HTTP method
     *
     * @return $this
     */
    public function setMethod($method)
    {
    }
    /**
     * Gets the HTTP host.
     *
     * The host is always lowercased because it must be treated case-insensitive.
     *
     * @return string The HTTP host
     */
    public function getHost()
    {
    }
    /**
     * Sets the HTTP host.
     *
     * @param string $host The HTTP host
     *
     * @return $this
     */
    public function setHost($host)
    {
    }
    /**
     * Gets the HTTP scheme.
     *
     * @return string The HTTP scheme
     */
    public function getScheme()
    {
    }
    /**
     * Sets the HTTP scheme.
     *
     * @param string $scheme The HTTP scheme
     *
     * @return $this
     */
    public function setScheme($scheme)
    {
    }
    /**
     * Gets the HTTP port.
     *
     * @return int The HTTP port
     */
    public function getHttpPort()
    {
    }
    /**
     * Sets the HTTP port.
     *
     * @param int $httpPort The HTTP port
     *
     * @return $this
     */
    public function setHttpPort($httpPort)
    {
    }
    /**
     * Gets the HTTPS port.
     *
     * @return int The HTTPS port
     */
    public function getHttpsPort()
    {
    }
    /**
     * Sets the HTTPS port.
     *
     * @param int $httpsPort The HTTPS port
     *
     * @return $this
     */
    public function setHttpsPort($httpsPort)
    {
    }
    /**
     * Gets the query string.
     *
     * @return string The query string without the "?"
     */
    public function getQueryString()
    {
    }
    /**
     * Sets the query string.
     *
     * @param string $queryString The query string (after "?")
     *
     * @return $this
     */
    public function setQueryString($queryString)
    {
    }
    /**
     * Returns the parameters.
     *
     * @return array The parameters
     */
    public function getParameters()
    {
    }
    /**
     * Sets the parameters.
     *
     * @param array $parameters The parameters
     *
     * @return $this
     */
    public function setParameters(array $parameters)
    {
    }
    /**
     * Gets a parameter value.
     *
     * @param string $name A parameter name
     *
     * @return mixed The parameter value or null if nonexistent
     */
    public function getParameter($name)
    {
    }
    /**
     * Checks if a parameter value is set for the given parameter.
     *
     * @param string $name A parameter name
     *
     * @return bool True if the parameter value is set, false otherwise
     */
    public function hasParameter($name)
    {
    }
    /**
     * Sets a parameter value.
     *
     * @param string $name      A parameter name
     * @param mixed  $parameter The parameter value
     *
     * @return $this
     */
    public function setParameter($name, $parameter)
    {
    }
}
