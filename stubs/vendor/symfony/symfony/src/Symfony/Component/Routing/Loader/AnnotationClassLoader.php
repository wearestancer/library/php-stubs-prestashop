<?php

namespace Symfony\Component\Routing\Loader;

/**
 * AnnotationClassLoader loads routing information from a PHP class and its methods.
 *
 * You need to define an implementation for the configureRoute() method. Most of the
 * time, this method should define some PHP callable to be called for the route
 * (a controller in MVC speak).
 *
 * The @Route annotation can be set on the class (for global parameters),
 * and on each method.
 *
 * The @Route annotation main value is the route path. The annotation also
 * recognizes several parameters: requirements, options, defaults, schemes,
 * methods, host, and name. The name parameter is mandatory.
 * Here is an example of how you should be able to use it:
 *     /**
 *      * @Route("/Blog")
 *      * /
 *     class Blog
 *     {
 *         /**
 *          * @Route("/", name="blog_index")
 *          * /
 *         public function index()
 *         {
 *         }
 *         /**
 *          * @Route("/{id}", name="blog_post", requirements = {"id" = "\d+"})
 *          * /
 *         public function show()
 *         {
 *         }
 *     }
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AnnotationClassLoader implements \Symfony\Component\Config\Loader\LoaderInterface
{
    protected $reader;
    /**
     * @var string
     */
    protected $routeAnnotationClass = 'Symfony\\Component\\Routing\\Annotation\\Route';
    /**
     * @var int
     */
    protected $defaultRouteIndex = 0;
    public function __construct(\Doctrine\Common\Annotations\Reader $reader)
    {
    }
    /**
     * Sets the annotation class to read route properties from.
     *
     * @param string $class A fully-qualified class name
     */
    public function setRouteAnnotationClass($class)
    {
    }
    /**
     * Loads from annotations from a class.
     *
     * @param string      $class A class name
     * @param string|null $type  The resource type
     *
     * @return RouteCollection A RouteCollection instance
     *
     * @throws \InvalidArgumentException When route can't be parsed
     */
    public function load($class, $type = null)
    {
    }
    /**
     * @param RouteAnnotation $annot   or an object that exposes a similar interface
     * @param array           $globals
     */
    protected function addRoute(\Symfony\Component\Routing\RouteCollection $collection, $annot, $globals, \ReflectionClass $class, \ReflectionMethod $method)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setResolver(\Symfony\Component\Config\Loader\LoaderResolverInterface $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getResolver()
    {
    }
    /**
     * Gets the default route name for a class method.
     *
     * @return string
     */
    protected function getDefaultRouteName(\ReflectionClass $class, \ReflectionMethod $method)
    {
    }
    protected function getGlobals(\ReflectionClass $class)
    {
    }
    protected function createRoute($path, $defaults, $requirements, $options, $host, $schemes, $methods, $condition)
    {
    }
    protected abstract function configureRoute(\Symfony\Component\Routing\Route $route, \ReflectionClass $class, \ReflectionMethod $method, $annot);
}
