<?php

namespace Symfony\Component\Routing\Generator\Dumper;

/**
 * CompiledUrlGeneratorDumper creates a PHP array to be used with CompiledUrlGenerator.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Tobias Schultze <http://tobion.de>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CompiledUrlGeneratorDumper extends \Symfony\Component\Routing\Generator\Dumper\GeneratorDumper
{
    public function getCompiledRoutes() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dump(array $options = [])
    {
    }
}
