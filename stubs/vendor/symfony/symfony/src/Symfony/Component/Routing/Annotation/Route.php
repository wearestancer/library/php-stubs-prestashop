<?php

namespace Symfony\Component\Routing\Annotation;

/**
 * Annotation class for @Route().
 *
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Route
{
    /**
     * @param array $data An array of key/value parameters
     *
     * @throws \BadMethodCallException
     */
    public function __construct(array $data)
    {
    }
    public function setPath($path)
    {
    }
    public function getPath()
    {
    }
    public function setLocalizedPaths(array $localizedPaths)
    {
    }
    public function getLocalizedPaths() : array
    {
    }
    public function setHost($pattern)
    {
    }
    public function getHost()
    {
    }
    public function setName($name)
    {
    }
    public function getName()
    {
    }
    public function setRequirements($requirements)
    {
    }
    public function getRequirements()
    {
    }
    public function setOptions($options)
    {
    }
    public function getOptions()
    {
    }
    public function setDefaults($defaults)
    {
    }
    public function getDefaults()
    {
    }
    public function setSchemes($schemes)
    {
    }
    public function getSchemes()
    {
    }
    public function setMethods($methods)
    {
    }
    public function getMethods()
    {
    }
    public function setCondition($condition)
    {
    }
    public function getCondition()
    {
    }
}
