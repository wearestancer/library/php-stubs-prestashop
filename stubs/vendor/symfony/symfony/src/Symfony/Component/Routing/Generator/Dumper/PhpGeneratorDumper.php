<?php

namespace Symfony\Component\Routing\Generator\Dumper;

/**
 * PhpGeneratorDumper creates a PHP class able to generate URLs for a given set of routes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Tobias Schultze <http://tobion.de>
 *
 * @deprecated since Symfony 4.3, use CompiledUrlGeneratorDumper instead.
 */
class PhpGeneratorDumper extends \Symfony\Component\Routing\Generator\Dumper\GeneratorDumper
{
    /**
     * Dumps a set of routes to a PHP class.
     *
     * Available options:
     *
     *  * class:      The class name
     *  * base_class: The base class name
     *
     * @param array $options An array of options
     *
     * @return string A PHP class representing the generator class
     */
    public function dump(array $options = [])
    {
    }
}
