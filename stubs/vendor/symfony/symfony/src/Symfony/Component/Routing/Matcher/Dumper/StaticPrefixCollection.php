<?php

namespace Symfony\Component\Routing\Matcher\Dumper;

/**
 * Prefix tree of routes preserving routes order.
 *
 * @author Frank de Jonge <info@frankdejonge.nl>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class StaticPrefixCollection
{
    public function __construct(string $prefix = '/')
    {
    }
    public function getPrefix() : string
    {
    }
    /**
     * @return array[]|self[]
     */
    public function getRoutes() : array
    {
    }
    /**
     * Adds a route to a group.
     *
     * @param array|self $route
     */
    public function addRoute(string $prefix, $route)
    {
    }
    /**
     * Linearizes back a set of nested routes into a collection.
     */
    public function populateCollection(\Symfony\Component\Routing\RouteCollection $routes) : \Symfony\Component\Routing\RouteCollection
    {
    }
    public static function handleError(int $type, string $msg)
    {
    }
}
