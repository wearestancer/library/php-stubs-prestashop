<?php

namespace Symfony\Component\Routing;

interface RequestContextAwareInterface
{
    /**
     * Sets the request context.
     */
    public function setContext(\Symfony\Component\Routing\RequestContext $context);
    /**
     * Gets the request context.
     *
     * @return RequestContext The context
     */
    public function getContext();
}
