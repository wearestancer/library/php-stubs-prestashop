<?php

namespace Symfony\Component\Routing\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class RouteConfigurator
{
    use \Symfony\Component\Routing\Loader\Configurator\Traits\AddTrait;
    use \Symfony\Component\Routing\Loader\Configurator\Traits\RouteTrait;
    public function __construct(\Symfony\Component\Routing\RouteCollection $collection, $route, string $name = '', \Symfony\Component\Routing\Loader\Configurator\CollectionConfigurator $parentConfigurator = null, array $prefixes = null)
    {
    }
}
