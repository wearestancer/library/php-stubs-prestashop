<?php

namespace Symfony\Component\Routing;

/**
 * A Route describes a route and its parameters.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Tobias Schultze <http://tobion.de>
 */
class Route implements \Serializable
{
    /**
     * Constructor.
     *
     * Available options:
     *
     *  * compiler_class: A class name able to compile this route instance (RouteCompiler by default)
     *  * utf8:           Whether UTF-8 matching is enforced ot not
     *
     * @param string          $path         The path pattern to match
     * @param array           $defaults     An array of default parameter values
     * @param array           $requirements An array of requirements for parameters (regexes)
     * @param array           $options      An array of options
     * @param string|null     $host         The host pattern to match
     * @param string|string[] $schemes      A required URI scheme or an array of restricted schemes
     * @param string|string[] $methods      A required HTTP method or an array of restricted methods
     * @param string|null     $condition    A condition that should evaluate to true for the route to match
     */
    public function __construct(string $path, array $defaults = [], array $requirements = [], array $options = [], ?string $host = '', $schemes = [], $methods = [], ?string $condition = '')
    {
    }
    public function __serialize() : array
    {
    }
    /**
     * @return string
     *
     * @internal since Symfony 4.3
     * @final since Symfony 4.3
     */
    public function serialize()
    {
    }
    public function __unserialize(array $data) : void
    {
    }
    /**
     * @internal since Symfony 4.3
     * @final since Symfony 4.3
     */
    public function unserialize($serialized)
    {
    }
    /**
     * @return string The path pattern
     */
    public function getPath()
    {
    }
    /**
     * Sets the pattern for the path.
     *
     * @param string $pattern The path pattern
     *
     * @return $this
     */
    public function setPath($pattern)
    {
    }
    /**
     * @return string The host pattern
     */
    public function getHost()
    {
    }
    /**
     * Sets the pattern for the host.
     *
     * @param string $pattern The host pattern
     *
     * @return $this
     */
    public function setHost($pattern)
    {
    }
    /**
     * Returns the lowercased schemes this route is restricted to.
     * So an empty array means that any scheme is allowed.
     *
     * @return string[] The schemes
     */
    public function getSchemes()
    {
    }
    /**
     * Sets the schemes (e.g. 'https') this route is restricted to.
     * So an empty array means that any scheme is allowed.
     *
     * @param string|string[] $schemes The scheme or an array of schemes
     *
     * @return $this
     */
    public function setSchemes($schemes)
    {
    }
    /**
     * Checks if a scheme requirement has been set.
     *
     * @param string $scheme
     *
     * @return bool true if the scheme requirement exists, otherwise false
     */
    public function hasScheme($scheme)
    {
    }
    /**
     * Returns the uppercased HTTP methods this route is restricted to.
     * So an empty array means that any method is allowed.
     *
     * @return string[] The methods
     */
    public function getMethods()
    {
    }
    /**
     * Sets the HTTP methods (e.g. 'POST') this route is restricted to.
     * So an empty array means that any method is allowed.
     *
     * @param string|string[] $methods The method or an array of methods
     *
     * @return $this
     */
    public function setMethods($methods)
    {
    }
    /**
     * @return array The options
     */
    public function getOptions()
    {
    }
    /**
     * @return $this
     */
    public function setOptions(array $options)
    {
    }
    /**
     * @return $this
     */
    public function addOptions(array $options)
    {
    }
    /**
     * Sets an option value.
     *
     * @param string $name  An option name
     * @param mixed  $value The option value
     *
     * @return $this
     */
    public function setOption($name, $value)
    {
    }
    /**
     * Get an option value.
     *
     * @param string $name An option name
     *
     * @return mixed The option value or null when not given
     */
    public function getOption($name)
    {
    }
    /**
     * Checks if an option has been set.
     *
     * @param string $name An option name
     *
     * @return bool true if the option is set, false otherwise
     */
    public function hasOption($name)
    {
    }
    /**
     * @return array The defaults
     */
    public function getDefaults()
    {
    }
    /**
     * @return $this
     */
    public function setDefaults(array $defaults)
    {
    }
    /**
     * @return $this
     */
    public function addDefaults(array $defaults)
    {
    }
    /**
     * Gets a default value.
     *
     * @param string $name A variable name
     *
     * @return mixed The default value or null when not given
     */
    public function getDefault($name)
    {
    }
    /**
     * Checks if a default value is set for the given variable.
     *
     * @param string $name A variable name
     *
     * @return bool true if the default value is set, false otherwise
     */
    public function hasDefault($name)
    {
    }
    /**
     * Sets a default value.
     *
     * @param string $name    A variable name
     * @param mixed  $default The default value
     *
     * @return $this
     */
    public function setDefault($name, $default)
    {
    }
    /**
     * @return array The requirements
     */
    public function getRequirements()
    {
    }
    /**
     * @return $this
     */
    public function setRequirements(array $requirements)
    {
    }
    /**
     * @return $this
     */
    public function addRequirements(array $requirements)
    {
    }
    /**
     * Returns the requirement for the given key.
     *
     * @param string $key The key
     *
     * @return string|null The regex or null when not given
     */
    public function getRequirement($key)
    {
    }
    /**
     * Checks if a requirement is set for the given key.
     *
     * @param string $key A variable name
     *
     * @return bool true if a requirement is specified, false otherwise
     */
    public function hasRequirement($key)
    {
    }
    /**
     * Sets a requirement for the given key.
     *
     * @param string $key   The key
     * @param string $regex The regex
     *
     * @return $this
     */
    public function setRequirement($key, $regex)
    {
    }
    /**
     * @return string The condition
     */
    public function getCondition()
    {
    }
    /**
     * Sets the condition.
     *
     * @param string $condition The condition
     *
     * @return $this
     */
    public function setCondition($condition)
    {
    }
    /**
     * Compiles the route.
     *
     * @return CompiledRoute A CompiledRoute instance
     *
     * @throws \LogicException If the Route cannot be compiled because the
     *                         path or host pattern is invalid
     *
     * @see RouteCompiler which is responsible for the compilation process
     */
    public function compile()
    {
    }
}
