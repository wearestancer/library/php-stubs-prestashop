<?php

namespace Symfony\Component\Routing\Matcher\Dumper;

/**
 * CompiledUrlMatcherDumper creates PHP arrays to be used with CompiledUrlMatcher.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Tobias Schultze <http://tobion.de>
 * @author Arnaud Le Blanc <arnaud.lb@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CompiledUrlMatcherDumper extends \Symfony\Component\Routing\Matcher\Dumper\MatcherDumper
{
    /**
     * {@inheritdoc}
     */
    public function dump(array $options = [])
    {
    }
    public function addExpressionLanguageProvider(\Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface $provider)
    {
    }
    /**
     * Generates the arrays for CompiledUrlMatcher's constructor.
     */
    public function getCompiledRoutes(bool $forDump = false) : array
    {
    }
    /**
     * @internal
     */
    public static function export($value) : string
    {
    }
}
