<?php

namespace Symfony\Component\Routing\Loader\Configurator\Traits;

trait AddTrait
{
    /**
     * @var RouteCollection
     */
    private $collection;
    private $name = '';
    private $prefixes;
    /**
     * Adds a route.
     *
     * @param string|array $path the path, or the localized paths of the route
     */
    public final function add(string $name, $path) : \Symfony\Component\Routing\Loader\Configurator\RouteConfigurator
    {
    }
    /**
     * Adds a route.
     *
     * @param string|array $path the path, or the localized paths of the route
     */
    public final function __invoke(string $name, $path) : \Symfony\Component\Routing\Loader\Configurator\RouteConfigurator
    {
    }
    private function createRoute(string $path) : \Symfony\Component\Routing\Route
    {
    }
}
