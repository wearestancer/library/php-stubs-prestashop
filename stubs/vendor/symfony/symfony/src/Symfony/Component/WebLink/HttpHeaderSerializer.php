<?php

namespace Symfony\Component\WebLink;

/**
 * Serializes a list of Link instances to an HTTP Link header.
 *
 * @see https://tools.ietf.org/html/rfc5988
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class HttpHeaderSerializer
{
    /**
     * Builds the value of the "Link" HTTP header.
     *
     * @param LinkInterface[]|\Traversable $links
     */
    public function serialize(iterable $links) : ?string
    {
    }
}
