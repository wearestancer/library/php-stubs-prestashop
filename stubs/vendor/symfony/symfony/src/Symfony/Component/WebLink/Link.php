<?php

namespace Symfony\Component\WebLink;

class Link implements \Psr\Link\EvolvableLinkInterface
{
    // Relations defined in https://www.w3.org/TR/html5/links.html#links and applicable on link elements
    public const REL_ALTERNATE = 'alternate';
    public const REL_AUTHOR = 'author';
    public const REL_HELP = 'help';
    public const REL_ICON = 'icon';
    public const REL_LICENSE = 'license';
    public const REL_SEARCH = 'search';
    public const REL_STYLESHEET = 'stylesheet';
    public const REL_NEXT = 'next';
    public const REL_PREV = 'prev';
    // Relation defined in https://www.w3.org/TR/preload/
    public const REL_PRELOAD = 'preload';
    // Relations defined in https://www.w3.org/TR/resource-hints/
    public const REL_DNS_PREFETCH = 'dns-prefetch';
    public const REL_PRECONNECT = 'preconnect';
    public const REL_PREFETCH = 'prefetch';
    public const REL_PRERENDER = 'prerender';
    // Extra relations
    public const REL_MERCURE = 'mercure';
    public function __construct(string $rel = null, string $href = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getHref() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isTemplated() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRels() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getAttributes() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public function withHref($href)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public function withRel($rel)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public function withoutRel($rel)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|\Stringable|int|float|bool|string[] $value
     *
     * @return static
     */
    public function withAttribute($attribute, $value)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public function withoutAttribute($attribute)
    {
    }
}
