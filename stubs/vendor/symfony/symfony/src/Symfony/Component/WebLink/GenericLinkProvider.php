<?php

namespace Symfony\Component\WebLink;

class GenericLinkProvider implements \Psr\Link\EvolvableLinkProviderInterface
{
    /**
     * @param LinkInterface[] $links
     */
    public function __construct(array $links = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLinks() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLinksByRel($rel) : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public function withLink(\Psr\Link\LinkInterface $link)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public function withoutLink(\Psr\Link\LinkInterface $link)
    {
    }
}
