<?php

namespace Symfony\Component\WebLink\EventListener;

/**
 * Adds the Link HTTP header to the response.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final
 */
class AddLinkHeaderListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct()
    {
    }
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\ResponseEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() : array
    {
    }
}
