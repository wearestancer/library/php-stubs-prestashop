<?php

namespace Symfony\Component\Dotenv\Exception;

/**
 * Thrown when a file has a syntax error.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class FormatException extends \LogicException implements \Symfony\Component\Dotenv\Exception\ExceptionInterface
{
    public function __construct(string $message, \Symfony\Component\Dotenv\Exception\FormatExceptionContext $context, int $code = 0, \Throwable $previous = null)
    {
    }
    public function getContext() : \Symfony\Component\Dotenv\Exception\FormatExceptionContext
    {
    }
}
