<?php

namespace Symfony\Component\Dotenv\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class FormatExceptionContext
{
    public function __construct(string $data, string $path, int $lineno, int $cursor)
    {
    }
    public function getPath() : string
    {
    }
    public function getLineno() : int
    {
    }
    public function getDetails() : string
    {
    }
}
