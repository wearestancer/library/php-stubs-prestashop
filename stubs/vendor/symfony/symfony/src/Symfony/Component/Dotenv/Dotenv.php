<?php

namespace Symfony\Component\Dotenv;

/**
 * Manages .env files.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class Dotenv
{
    public const VARNAME_REGEX = '(?i:[A-Z][A-Z0-9_]*+)';
    public const STATE_VARNAME = 0;
    public const STATE_VALUE = 1;
    /**
     * @var bool If `putenv()` should be used to define environment variables or not.
     *           Beware that `putenv()` is not thread safe and this setting will default
     *           to `false` in Symfony 5.0.
     */
    public function __construct(bool $usePutenv = true)
    {
    }
    /**
     * Loads one or several .env files.
     *
     * @param string   $path          A file to load
     * @param string[] ...$extraPaths A list of additional files to load
     *
     * @throws FormatException when a file has a syntax error
     * @throws PathException   when a file does not exist or is not readable
     */
    public function load(string $path, string ...$extraPaths) : void
    {
    }
    /**
     * Loads a .env file and the corresponding .env.local, .env.$env and .env.$env.local files if they exist.
     *
     * .env.local is always ignored in test env because tests should produce the same results for everyone.
     * .env.dist is loaded when it exists and .env is not found.
     *
     * @param string $path       A file to load
     * @param string $varName    The name of the env vars that defines the app env
     * @param string $defaultEnv The app env to use when none is defined
     * @param array  $testEnvs   A list of app envs for which .env.local should be ignored
     *
     * @throws FormatException when a file has a syntax error
     * @throws PathException   when a file does not exist or is not readable
     */
    public function loadEnv(string $path, string $varName = 'APP_ENV', string $defaultEnv = 'dev', array $testEnvs = ['test']) : void
    {
    }
    /**
     * Loads one or several .env files and enables override existing vars.
     *
     * @param string   $path          A file to load
     * @param string[] ...$extraPaths A list of additional files to load
     *
     * @throws FormatException when a file has a syntax error
     * @throws PathException   when a file does not exist or is not readable
     */
    public function overload(string $path, string ...$extraPaths) : void
    {
    }
    /**
     * Sets values as environment variables (via putenv, $_ENV, and $_SERVER).
     *
     * @param array $values               An array of env variables
     * @param bool  $overrideExistingVars true when existing environment variables must be overridden
     */
    public function populate(array $values, bool $overrideExistingVars = false) : void
    {
    }
    /**
     * Parses the contents of an .env file.
     *
     * @param string $data The data to be parsed
     * @param string $path The original file name where data where stored (used for more meaningful error messages)
     *
     * @return array An array of env variables
     *
     * @throws FormatException when a file has a syntax error
     */
    public function parse(string $data, string $path = '.env') : array
    {
    }
}
