<?php

namespace Symfony\Component\Dotenv\Exception;

/**
 * Thrown when a file does not exist or is not readable.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class PathException extends \RuntimeException implements \Symfony\Component\Dotenv\Exception\ExceptionInterface
{
    public function __construct(string $path, int $code = 0, \Throwable $previous = null)
    {
    }
}
