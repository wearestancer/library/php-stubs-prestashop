<?php

namespace Symfony\Component\Asset;

/**
 * Basic package that adds a version to asset URLs.
 *
 * @author Kris Wallsmith <kris@symfony.com>
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Package implements \Symfony\Component\Asset\PackageInterface
{
    public function __construct(\Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface $versionStrategy, \Symfony\Component\Asset\Context\ContextInterface $context = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getVersion($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUrl($path)
    {
    }
    /**
     * @return ContextInterface
     */
    protected function getContext()
    {
    }
    /**
     * @return VersionStrategyInterface
     */
    protected function getVersionStrategy()
    {
    }
    /**
     * @return bool
     */
    protected function isAbsoluteUrl($url)
    {
    }
}
