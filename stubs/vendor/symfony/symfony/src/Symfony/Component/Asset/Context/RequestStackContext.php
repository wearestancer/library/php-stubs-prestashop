<?php

namespace Symfony\Component\Asset\Context;

/**
 * Uses a RequestStack to populate the context.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RequestStackContext implements \Symfony\Component\Asset\Context\ContextInterface
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack, string $basePath = '', bool $secure = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBasePath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isSecure()
    {
    }
}
