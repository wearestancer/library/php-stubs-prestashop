<?php

namespace Symfony\Component\Asset\VersionStrategy;

/**
 * Returns the same version for all assets.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class StaticVersionStrategy implements \Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface
{
    /**
     * @param string $version Version number
     * @param string $format  Url format
     */
    public function __construct(string $version, string $format = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getVersion($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyVersion($path)
    {
    }
}
