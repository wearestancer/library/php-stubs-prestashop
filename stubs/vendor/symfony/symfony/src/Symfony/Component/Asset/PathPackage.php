<?php

namespace Symfony\Component\Asset;

/**
 * Package that adds a base path to asset URLs in addition to a version.
 *
 * In addition to the provided base path, this package also automatically
 * prepends the current request base path if a Context is available to
 * allow a website to be hosted easily under any given path under the Web
 * Server root directory.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PathPackage extends \Symfony\Component\Asset\Package
{
    /**
     * @param string $basePath The base path to be prepended to relative paths
     */
    public function __construct(string $basePath, \Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface $versionStrategy, \Symfony\Component\Asset\Context\ContextInterface $context = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUrl($path)
    {
    }
    /**
     * Returns the base path.
     *
     * @return string The base path
     */
    public function getBasePath()
    {
    }
}
