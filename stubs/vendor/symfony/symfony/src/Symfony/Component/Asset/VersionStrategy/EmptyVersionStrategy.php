<?php

namespace Symfony\Component\Asset\VersionStrategy;

/**
 * Disable version for all assets.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class EmptyVersionStrategy implements \Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function getVersion($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function applyVersion($path)
    {
    }
}
