<?php

namespace Symfony\Component\Asset;

/**
 * Package that adds a base URL to asset URLs in addition to a version.
 *
 * The package allows to use more than one base URLs in which case
 * it randomly chooses one for each asset; it also guarantees that
 * any given path will always use the same base URL to be nice with
 * HTTP caching mechanisms.
 *
 * When the request context is available, this package can choose the
 * best base URL to use based on the current request scheme:
 *
 *  * For HTTP request, it chooses between all base URLs;
 *  * For HTTPs requests, it chooses between HTTPs base URLs and relative protocol URLs
 *    or falls back to any base URL if no secure ones are available.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UrlPackage extends \Symfony\Component\Asset\Package
{
    /**
     * @param string|string[] $baseUrls Base asset URLs
     */
    public function __construct($baseUrls, \Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface $versionStrategy, \Symfony\Component\Asset\Context\ContextInterface $context = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUrl($path)
    {
    }
    /**
     * Returns the base URL for a path.
     *
     * @param string $path
     *
     * @return string The base URL
     */
    public function getBaseUrl($path)
    {
    }
    /**
     * Determines which base URL to use for the given path.
     *
     * Override this method to change the default distribution strategy.
     * This method should always return the same base URL index for a given path.
     *
     * @param string $path
     *
     * @return int The base URL index for the given path
     */
    protected function chooseBaseUrl($path)
    {
    }
}
