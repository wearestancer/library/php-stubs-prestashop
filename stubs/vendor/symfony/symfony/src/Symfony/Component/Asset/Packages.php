<?php

namespace Symfony\Component\Asset;

/**
 * Helps manage asset URLs.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Kris Wallsmith <kris@symfony.com>
 */
class Packages
{
    /**
     * @param PackageInterface[] $packages Additional packages indexed by name
     */
    public function __construct(\Symfony\Component\Asset\PackageInterface $defaultPackage = null, array $packages = [])
    {
    }
    public function setDefaultPackage(\Symfony\Component\Asset\PackageInterface $defaultPackage)
    {
    }
    /**
     * Adds a  package.
     *
     * @param string $name The package name
     */
    public function addPackage($name, \Symfony\Component\Asset\PackageInterface $package)
    {
    }
    /**
     * Returns an asset package.
     *
     * @param string $name The name of the package or null for the default package
     *
     * @return PackageInterface An asset package
     *
     * @throws InvalidArgumentException If there is no package by that name
     * @throws LogicException           If no default package is defined
     */
    public function getPackage($name = null)
    {
    }
    /**
     * Gets the version to add to public URL.
     *
     * @param string $path        A public path
     * @param string $packageName A package name
     *
     * @return string The current version
     */
    public function getVersion($path, $packageName = null)
    {
    }
    /**
     * Returns the public path.
     *
     * Absolute paths (i.e. http://...) are returned unmodified.
     *
     * @param string $path        A public path
     * @param string $packageName The name of the asset package to use
     *
     * @return string A public path which takes into account the base path and URL path
     */
    public function getUrl($path, $packageName = null)
    {
    }
}
