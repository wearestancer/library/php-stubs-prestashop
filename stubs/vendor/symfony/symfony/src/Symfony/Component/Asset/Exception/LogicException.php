<?php

namespace Symfony\Component\Asset\Exception;

/**
 * Base LogicException for the Asset component.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class LogicException extends \LogicException implements \Symfony\Component\Asset\Exception\ExceptionInterface
{
}
