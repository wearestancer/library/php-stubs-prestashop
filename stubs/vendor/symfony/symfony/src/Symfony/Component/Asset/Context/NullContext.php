<?php

namespace Symfony\Component\Asset\Context;

/**
 * A context that does nothing.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class NullContext implements \Symfony\Component\Asset\Context\ContextInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBasePath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isSecure()
    {
    }
}
