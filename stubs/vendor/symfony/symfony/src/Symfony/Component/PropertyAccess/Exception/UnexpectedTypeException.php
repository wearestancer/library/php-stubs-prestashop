<?php

namespace Symfony\Component\PropertyAccess\Exception;

/**
 * Thrown when a value does not match an expected type.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class UnexpectedTypeException extends \Symfony\Component\PropertyAccess\Exception\RuntimeException
{
    /**
     * @param mixed $value     The unexpected value found while traversing property path
     * @param int   $pathIndex The property path index when the unexpected value was found
     */
    public function __construct($value, \Symfony\Component\PropertyAccess\PropertyPathInterface $path, int $pathIndex)
    {
    }
}
