<?php

namespace Symfony\Component\PropertyAccess;

/**
 * Default implementation of {@link PropertyPathInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class PropertyPath implements \IteratorAggregate, \Symfony\Component\PropertyAccess\PropertyPathInterface
{
    /**
     * Character used for separating between plural and singular of an element.
     */
    public const SINGULAR_SEPARATOR = '|';
    /**
     * Constructs a property path from a string.
     *
     * @param PropertyPath|string $propertyPath The property path as string or instance
     *
     * @throws InvalidArgumentException     If the given path is not a string
     * @throws InvalidPropertyPathException If the syntax of the property path is not valid
     */
    public function __construct($propertyPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLength()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    }
    /**
     * Returns a new iterator for this path.
     *
     * @return PropertyPathIteratorInterface
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getElements()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getElement($index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isProperty($index)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isIndex($index)
    {
    }
}
