<?php

namespace Symfony\Component\PropertyAccess;

/**
 * Traverses a property path and provides additional methods to find out
 * information about the current element.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class PropertyPathIterator extends \ArrayIterator implements \Symfony\Component\PropertyAccess\PropertyPathIteratorInterface
{
    protected $path;
    public function __construct(\Symfony\Component\PropertyAccess\PropertyPathInterface $path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isIndex()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isProperty()
    {
    }
}
