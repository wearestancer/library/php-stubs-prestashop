<?php

namespace Symfony\Component\PropertyAccess;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class PropertyPathBuilder
{
    /**
     * Creates a new property path builder.
     *
     * @param PropertyPathInterface|string|null $path The path to initially store
     *                                                in the builder. Optional.
     */
    public function __construct($path = null)
    {
    }
    /**
     * Appends a (sub-) path to the current path.
     *
     * @param PropertyPathInterface|string $path   The path to append
     * @param int                          $offset The offset where the appended
     *                                             piece starts in $path
     * @param int                          $length The length of the appended piece
     *                                             If 0, the full path is appended
     */
    public function append($path, $offset = 0, $length = 0)
    {
    }
    /**
     * Appends an index element to the current path.
     *
     * @param string $name The name of the appended index
     */
    public function appendIndex($name)
    {
    }
    /**
     * Appends a property element to the current path.
     *
     * @param string $name The name of the appended property
     */
    public function appendProperty($name)
    {
    }
    /**
     * Removes elements from the current path.
     *
     * @param int $offset The offset at which to remove
     * @param int $length The length of the removed piece
     *
     * @throws OutOfBoundsException if offset is invalid
     */
    public function remove($offset, $length = 1)
    {
    }
    /**
     * Replaces a sub-path by a different (sub-) path.
     *
     * @param int                          $offset     The offset at which to replace
     * @param int                          $length     The length of the piece to replace
     * @param PropertyPathInterface|string $path       The path to insert
     * @param int                          $pathOffset The offset where the inserted piece
     *                                                 starts in $path
     * @param int                          $pathLength The length of the inserted piece
     *                                                 If 0, the full path is inserted
     *
     * @throws OutOfBoundsException If the offset is invalid
     */
    public function replace($offset, $length, $path, $pathOffset = 0, $pathLength = 0)
    {
    }
    /**
     * Replaces a property element by an index element.
     *
     * @param int    $offset The offset at which to replace
     * @param string $name   The new name of the element. Optional
     *
     * @throws OutOfBoundsException If the offset is invalid
     */
    public function replaceByIndex($offset, $name = null)
    {
    }
    /**
     * Replaces an index element by a property element.
     *
     * @param int    $offset The offset at which to replace
     * @param string $name   The new name of the element. Optional
     *
     * @throws OutOfBoundsException If the offset is invalid
     */
    public function replaceByProperty($offset, $name = null)
    {
    }
    /**
     * Returns the length of the current path.
     *
     * @return int The path length
     */
    public function getLength()
    {
    }
    /**
     * Returns the current property path.
     *
     * @return PropertyPathInterface|null The constructed property path
     */
    public function getPropertyPath()
    {
    }
    /**
     * Returns the current property path as string.
     *
     * @return string The property path as string
     */
    public function __toString()
    {
    }
}
