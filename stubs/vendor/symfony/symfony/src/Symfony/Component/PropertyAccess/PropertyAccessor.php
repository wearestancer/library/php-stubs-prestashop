<?php

namespace Symfony\Component\PropertyAccess;

/**
 * Default implementation of {@link PropertyAccessorInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class PropertyAccessor implements \Symfony\Component\PropertyAccess\PropertyAccessorInterface
{
    /**
     * Should not be used by application code. Use
     * {@link PropertyAccess::createPropertyAccessor()} instead.
     */
    public function __construct(bool $magicCall = false, bool $throwExceptionOnInvalidIndex = false, \Psr\Cache\CacheItemPoolInterface $cacheItemPool = null, bool $throwExceptionOnInvalidPropertyPath = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValue($objectOrArray, $propertyPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setValue(&$objectOrArray, $propertyPath, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isReadable($objectOrArray, $propertyPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isWritable($objectOrArray, $propertyPath)
    {
    }
    /**
     * Creates the APCu adapter if applicable.
     *
     * @param string $namespace
     * @param int    $defaultLifetime
     * @param string $version
     *
     * @return AdapterInterface
     *
     * @throws \LogicException When the Cache Component isn't available
     */
    public static function createCache($namespace, $defaultLifetime, $version, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
