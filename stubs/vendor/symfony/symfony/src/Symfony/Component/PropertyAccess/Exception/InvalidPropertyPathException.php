<?php

namespace Symfony\Component\PropertyAccess\Exception;

/**
 * Thrown when a property path is malformed.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidPropertyPathException extends \Symfony\Component\PropertyAccess\Exception\RuntimeException
{
}
