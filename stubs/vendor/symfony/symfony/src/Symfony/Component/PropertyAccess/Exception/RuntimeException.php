<?php

namespace Symfony\Component\PropertyAccess\Exception;

/**
 * Base RuntimeException for the PropertyAccess component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\PropertyAccess\Exception\ExceptionInterface
{
}
