<?php

namespace Symfony\Component\PropertyAccess;

/**
 * Entry point of the PropertyAccess component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
final class PropertyAccess
{
    /**
     * Creates a property accessor with the default configuration.
     */
    public static function createPropertyAccessor() : \Symfony\Component\PropertyAccess\PropertyAccessor
    {
    }
    public static function createPropertyAccessorBuilder() : \Symfony\Component\PropertyAccess\PropertyAccessorBuilder
    {
    }
}
