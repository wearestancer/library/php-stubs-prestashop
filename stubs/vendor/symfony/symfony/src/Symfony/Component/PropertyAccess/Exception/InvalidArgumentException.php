<?php

namespace Symfony\Component\PropertyAccess\Exception;

/**
 * Base InvalidArgumentException for the PropertyAccess component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\PropertyAccess\Exception\ExceptionInterface
{
}
