<?php

namespace Symfony\Component\PropertyAccess\Exception;

/**
 * Thrown when an index cannot be found.
 *
 * @author Stéphane Escandell <stephane.escandell@gmail.com>
 */
class NoSuchIndexException extends \Symfony\Component\PropertyAccess\Exception\AccessException
{
}
