<?php

namespace Symfony\Component\Stopwatch;

/**
 * Stopwatch provides a way to profile code.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Stopwatch implements \Symfony\Contracts\Service\ResetInterface
{
    /**
     * @param bool $morePrecision If true, time is stored as float to keep the original microsecond precision
     */
    public function __construct(bool $morePrecision = false)
    {
    }
    /**
     * @return Section[]
     */
    public function getSections()
    {
    }
    /**
     * Creates a new section or re-opens an existing section.
     *
     * @param string|null $id The id of the session to re-open, null to create a new one
     *
     * @throws \LogicException When the section to re-open is not reachable
     */
    public function openSection($id = null)
    {
    }
    /**
     * Stops the last started section.
     *
     * The id parameter is used to retrieve the events from this section.
     *
     * @see getSectionEvents()
     *
     * @param string $id The identifier of the section
     *
     * @throws \LogicException When there's no started section to be stopped
     */
    public function stopSection($id)
    {
    }
    /**
     * Starts an event.
     *
     * @param string      $name     The event name
     * @param string|null $category The event category
     *
     * @return StopwatchEvent
     */
    public function start($name, $category = null)
    {
    }
    /**
     * Checks if the event was started.
     *
     * @param string $name The event name
     *
     * @return bool
     */
    public function isStarted($name)
    {
    }
    /**
     * Stops an event.
     *
     * @param string $name The event name
     *
     * @return StopwatchEvent
     */
    public function stop($name)
    {
    }
    /**
     * Stops then restarts an event.
     *
     * @param string $name The event name
     *
     * @return StopwatchEvent
     */
    public function lap($name)
    {
    }
    /**
     * Returns a specific event by name.
     *
     * @param string $name The event name
     *
     * @return StopwatchEvent
     */
    public function getEvent($name)
    {
    }
    /**
     * Gets all events for a given section.
     *
     * @param string $id A section identifier
     *
     * @return StopwatchEvent[]
     */
    public function getSectionEvents($id)
    {
    }
    /**
     * Resets the stopwatch to its original state.
     */
    public function reset()
    {
    }
}
