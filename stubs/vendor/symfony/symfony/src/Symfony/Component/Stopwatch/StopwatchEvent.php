<?php

namespace Symfony\Component\Stopwatch;

/**
 * Represents an Event managed by Stopwatch.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class StopwatchEvent
{
    /**
     * @param float       $origin        The origin time in milliseconds
     * @param string|null $category      The event category or null to use the default
     * @param bool        $morePrecision If true, time is stored as float to keep the original microsecond precision
     *
     * @throws \InvalidArgumentException When the raw time is not valid
     */
    public function __construct(float $origin, string $category = null, bool $morePrecision = false)
    {
    }
    /**
     * Gets the category.
     *
     * @return string The category
     */
    public function getCategory()
    {
    }
    /**
     * Gets the origin.
     *
     * @return float The origin in milliseconds
     */
    public function getOrigin()
    {
    }
    /**
     * Starts a new event period.
     *
     * @return $this
     */
    public function start()
    {
    }
    /**
     * Stops the last started event period.
     *
     * @return $this
     *
     * @throws \LogicException When stop() is called without a matching call to start()
     */
    public function stop()
    {
    }
    /**
     * Checks if the event was started.
     *
     * @return bool
     */
    public function isStarted()
    {
    }
    /**
     * Stops the current period and then starts a new one.
     *
     * @return $this
     */
    public function lap()
    {
    }
    /**
     * Stops all non already stopped periods.
     */
    public function ensureStopped()
    {
    }
    /**
     * Gets all event periods.
     *
     * @return StopwatchPeriod[] An array of StopwatchPeriod instances
     */
    public function getPeriods()
    {
    }
    /**
     * Gets the relative time of the start of the first period.
     *
     * @return int|float The time (in milliseconds)
     */
    public function getStartTime()
    {
    }
    /**
     * Gets the relative time of the end of the last period.
     *
     * @return int|float The time (in milliseconds)
     */
    public function getEndTime()
    {
    }
    /**
     * Gets the duration of the events (including all periods).
     *
     * @return int|float The duration (in milliseconds)
     */
    public function getDuration()
    {
    }
    /**
     * Gets the max memory usage of all periods.
     *
     * @return int The memory usage (in bytes)
     */
    public function getMemory()
    {
    }
    /**
     * Return the current time relative to origin.
     *
     * @return float Time in ms
     */
    protected function getNow()
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
