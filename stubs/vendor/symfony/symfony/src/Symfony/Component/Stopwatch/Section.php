<?php

namespace Symfony\Component\Stopwatch;

/**
 * Stopwatch section.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Section
{
    /**
     * @param float|null $origin        Set the origin of the events in this section, use null to set their origin to their start time
     * @param bool       $morePrecision If true, time is stored as float to keep the original microsecond precision
     */
    public function __construct(float $origin = null, bool $morePrecision = false)
    {
    }
    /**
     * Returns the child section.
     *
     * @param string $id The child section identifier
     *
     * @return self|null The child section or null when none found
     */
    public function get($id)
    {
    }
    /**
     * Creates or re-opens a child section.
     *
     * @param string|null $id Null to create a new section, the identifier to re-open an existing one
     *
     * @return self
     */
    public function open($id)
    {
    }
    /**
     * @return string The identifier of the section
     */
    public function getId()
    {
    }
    /**
     * Sets the session identifier.
     *
     * @param string $id The session identifier
     *
     * @return $this
     */
    public function setId($id)
    {
    }
    /**
     * Starts an event.
     *
     * @param string      $name     The event name
     * @param string|null $category The event category
     *
     * @return StopwatchEvent The event
     */
    public function startEvent($name, $category)
    {
    }
    /**
     * Checks if the event was started.
     *
     * @param string $name The event name
     *
     * @return bool
     */
    public function isEventStarted($name)
    {
    }
    /**
     * Stops an event.
     *
     * @param string $name The event name
     *
     * @return StopwatchEvent The event
     *
     * @throws \LogicException When the event has not been started
     */
    public function stopEvent($name)
    {
    }
    /**
     * Stops then restarts an event.
     *
     * @param string $name The event name
     *
     * @return StopwatchEvent The event
     *
     * @throws \LogicException When the event has not been started
     */
    public function lap($name)
    {
    }
    /**
     * Returns a specific event by name.
     *
     * @param string $name The event name
     *
     * @return StopwatchEvent The event
     *
     * @throws \LogicException When the event is not known
     */
    public function getEvent($name)
    {
    }
    /**
     * Returns the events from this section.
     *
     * @return StopwatchEvent[] An array of StopwatchEvent instances
     */
    public function getEvents()
    {
    }
}
