<?php

namespace Symfony\Component\Workflow\DependencyInjection;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 *
 * @deprecated since Symfony 4.3
 */
class ValidateWorkflowsPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $definitionTag = 'workflow.definition')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
