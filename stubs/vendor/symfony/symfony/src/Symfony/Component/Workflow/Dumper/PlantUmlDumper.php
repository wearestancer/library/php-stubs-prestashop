<?php

namespace Symfony\Component\Workflow\Dumper;

/**
 * PlantUmlDumper dumps a workflow as a PlantUML file.
 *
 * You can convert the generated puml file with the plantuml.jar utility (http://plantuml.com/):
 *
 * php bin/console workflow:dump pull_request travis --dump-format=puml | java -jar plantuml.jar -p  > workflow.png
 *
 * @author Sébastien Morel <morel.seb@gmail.com>
 */
class PlantUmlDumper implements \Symfony\Component\Workflow\Dumper\DumperInterface
{
    public const STATEMACHINE_TRANSITION = 'arrow';
    public const WORKFLOW_TRANSITION = 'square';
    public const TRANSITION_TYPES = [self::STATEMACHINE_TRANSITION, self::WORKFLOW_TRANSITION];
    public const DEFAULT_OPTIONS = ['skinparams' => ['titleBorderRoundCorner' => 15, 'titleBorderThickness' => 2, 'state' => ['BackgroundColor' . self::INITIAL => '#87b741', 'BackgroundColor' . self::MARKED => '#3887C6', 'BorderColor' => '#3887C6', 'BorderColor' . self::MARKED => 'Black', 'FontColor' . self::MARKED => 'White'], 'agent' => ['BackgroundColor' => '#ffffff', 'BorderColor' => '#3887C6']]];
    public function __construct(string $transitionType = null)
    {
    }
    public function dump(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\Marking $marking = null, array $options = []) : string
    {
    }
}
