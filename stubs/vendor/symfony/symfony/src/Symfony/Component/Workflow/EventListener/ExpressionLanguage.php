<?php

namespace Symfony\Component\Workflow\EventListener;

/**
 * Adds some function to the default Symfony Security ExpressionLanguage.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ExpressionLanguage extends \Symfony\Component\Security\Core\Authorization\ExpressionLanguage
{
    protected function registerFunctions()
    {
    }
}
