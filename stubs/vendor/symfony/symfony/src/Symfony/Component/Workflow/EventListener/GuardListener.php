<?php

namespace Symfony\Component\Workflow\EventListener;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class GuardListener
{
    public function __construct(array $configuration, \Symfony\Component\Workflow\EventListener\ExpressionLanguage $expressionLanguage, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $trustResolver, \Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy = null, \Symfony\Component\Validator\Validator\ValidatorInterface $validator = null)
    {
    }
    public function onTransition(\Symfony\Component\Workflow\Event\GuardEvent $event, $eventName)
    {
    }
}
