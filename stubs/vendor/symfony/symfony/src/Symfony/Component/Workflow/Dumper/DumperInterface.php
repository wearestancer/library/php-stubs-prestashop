<?php

namespace Symfony\Component\Workflow\Dumper;

/**
 * DumperInterface is the interface implemented by workflow dumper classes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
interface DumperInterface
{
    /**
     * Dumps a workflow definition.
     *
     * @return string The representation of the workflow
     */
    public function dump(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\Marking $marking = null, array $options = []);
}
