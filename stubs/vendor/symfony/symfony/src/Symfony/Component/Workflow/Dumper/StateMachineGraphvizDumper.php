<?php

namespace Symfony\Component\Workflow\Dumper;

class StateMachineGraphvizDumper extends \Symfony\Component\Workflow\Dumper\GraphvizDumper
{
    /**
     * {@inheritdoc}
     *
     * Dumps the workflow as a graphviz graph.
     *
     * Available options:
     *
     *  * graph: The default options for the whole graph
     *  * node: The default options for nodes (places)
     *  * edge: The default options for edges
     */
    public function dump(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\Marking $marking = null, array $options = [])
    {
    }
    /**
     * @internal
     */
    protected function findEdges(\Symfony\Component\Workflow\Definition $definition) : array
    {
    }
    /**
     * @internal
     */
    protected function addEdges(array $edges) : string
    {
    }
}
