<?php

namespace Symfony\Component\Workflow\Event;

/**
 * @final since Symfony 4.4
 */
class TransitionEvent extends \Symfony\Component\Workflow\Event\Event
{
    public function setContext(array $context)
    {
    }
    public function getContext() : array
    {
    }
}
