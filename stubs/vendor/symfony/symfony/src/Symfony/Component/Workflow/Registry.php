<?php

namespace Symfony\Component\Workflow;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class Registry
{
    /**
     * @param SupportStrategyInterface $supportStrategy
     *
     * @deprecated since Symfony 4.1, use addWorkflow() instead
     */
    public function add(\Symfony\Component\Workflow\Workflow $workflow, $supportStrategy)
    {
    }
    public function addWorkflow(\Symfony\Component\Workflow\WorkflowInterface $workflow, \Symfony\Component\Workflow\SupportStrategy\WorkflowSupportStrategyInterface $supportStrategy)
    {
    }
    /**
     * @param object      $subject
     * @param string|null $workflowName
     *
     * @return Workflow
     */
    public function get($subject, $workflowName = null)
    {
    }
    /**
     * @param object $subject
     *
     * @return Workflow[]
     */
    public function all($subject) : array
    {
    }
}
