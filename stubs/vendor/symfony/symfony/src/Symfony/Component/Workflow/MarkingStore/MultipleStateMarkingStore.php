<?php

namespace Symfony\Component\Workflow\MarkingStore;

/**
 * MultipleStateMarkingStore stores the marking into a property of the
 * subject.
 *
 * This store deals with a "multiple state" Marking. It means a subject can be
 * in many states at the same time.
 *
 * @deprecated since Symfony 4.3, use MethodMarkingStore instead.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class MultipleStateMarkingStore implements \Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface
{
    public function __construct(string $property = 'marking', \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMarking($subject)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param array $context Some context
     */
    public function setMarking($subject, \Symfony\Component\Workflow\Marking $marking)
    {
    }
    /**
     * @return string
     */
    public function getProperty()
    {
    }
}
