<?php

namespace Symfony\Component\Workflow;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class Transition
{
    /**
     * @param string|string[] $froms
     * @param string|string[] $tos
     */
    public function __construct(string $name, $froms, $tos)
    {
    }
    /**
     * @return string
     */
    public function getName()
    {
    }
    /**
     * @return string[]
     */
    public function getFroms()
    {
    }
    /**
     * @return string[]
     */
    public function getTos()
    {
    }
}
