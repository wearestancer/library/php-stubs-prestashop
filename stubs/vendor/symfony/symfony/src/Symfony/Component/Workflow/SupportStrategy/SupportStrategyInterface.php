<?php

namespace Symfony\Component\Workflow\SupportStrategy;

/**
 * @author Andreas Kleemann <akleemann@inviqa.com>
 *
 * @deprecated since Symfony 4.1, use WorkflowSupportStrategyInterface instead
 */
interface SupportStrategyInterface
{
    /**
     * @param object $subject
     *
     * @return bool
     */
    public function supports(\Symfony\Component\Workflow\Workflow $workflow, $subject);
}
