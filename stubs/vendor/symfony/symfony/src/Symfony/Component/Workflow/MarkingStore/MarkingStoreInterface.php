<?php

namespace Symfony\Component\Workflow\MarkingStore;

/**
 * MarkingStoreInterface is the interface between the Workflow Component and a
 * plain old PHP object: the subject.
 *
 * It converts the Marking into something understandable by the subject and vice
 * versa.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
interface MarkingStoreInterface
{
    /**
     * Gets a Marking from a subject.
     *
     * @param object $subject A subject
     *
     * @return Marking The marking
     */
    public function getMarking($subject);
    /**
     * Sets a Marking to a subject.
     *
     * @param object $subject A subject
     * @param array  $context Some context
     */
    public function setMarking($subject, \Symfony\Component\Workflow\Marking $marking);
}
