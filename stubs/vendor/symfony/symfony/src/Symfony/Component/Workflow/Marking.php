<?php

namespace Symfony\Component\Workflow;

/**
 * Marking contains the place of every tokens.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class Marking
{
    /**
     * @param int[] $representation Keys are the place name and values should be 1
     */
    public function __construct(array $representation = [])
    {
    }
    public function mark($place)
    {
    }
    public function unmark($place)
    {
    }
    public function has($place)
    {
    }
    public function getPlaces()
    {
    }
}
