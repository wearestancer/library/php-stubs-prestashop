<?php

namespace Symfony\Component\Workflow\Exception;

/**
 * @author Andrew Tch <andrew.tchircoff@gmail.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class TransitionException extends \Symfony\Component\Workflow\Exception\LogicException
{
    public function __construct($subject, string $transitionName, \Symfony\Component\Workflow\WorkflowInterface $workflow, string $message)
    {
    }
    public function getSubject()
    {
    }
    public function getTransitionName() : string
    {
    }
    public function getWorkflow() : \Symfony\Component\Workflow\WorkflowInterface
    {
    }
}
