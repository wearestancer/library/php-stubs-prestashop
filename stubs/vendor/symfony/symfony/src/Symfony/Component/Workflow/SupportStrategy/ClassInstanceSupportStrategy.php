<?php

namespace Symfony\Component\Workflow\SupportStrategy;

/**
 * @author Andreas Kleemann <akleemann@inviqa.com>
 *
 * @deprecated since Symfony 4.1, use InstanceOfSupportStrategy instead
 */
final class ClassInstanceSupportStrategy implements \Symfony\Component\Workflow\SupportStrategy\SupportStrategyInterface
{
    public function __construct(string $className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Workflow\Workflow $workflow, $subject) : bool
    {
    }
    public function getClassName() : string
    {
    }
}
