<?php

namespace Symfony\Component\Workflow\Event;

/**
 * @final since Symfony 4.4
 */
class EnterEvent extends \Symfony\Component\Workflow\Event\Event
{
}
