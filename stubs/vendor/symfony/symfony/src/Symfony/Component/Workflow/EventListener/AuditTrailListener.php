<?php

namespace Symfony\Component\Workflow\EventListener;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class AuditTrailListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
    }
    public function onLeave(\Symfony\Component\Workflow\Event\Event $event)
    {
    }
    public function onTransition(\Symfony\Component\Workflow\Event\Event $event)
    {
    }
    public function onEnter(\Symfony\Component\Workflow\Event\Event $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
