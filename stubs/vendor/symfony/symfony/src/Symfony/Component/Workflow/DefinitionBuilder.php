<?php

namespace Symfony\Component\Workflow;

/**
 * Builds a definition.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class DefinitionBuilder
{
    /**
     * @param string[]     $places
     * @param Transition[] $transitions
     */
    public function __construct(array $places = [], array $transitions = [])
    {
    }
    /**
     * @return Definition
     */
    public function build()
    {
    }
    /**
     * Clear all data in the builder.
     *
     * @return $this
     */
    public function clear()
    {
    }
    /**
     * @deprecated since Symfony 4.3. Use setInitialPlaces() instead.
     *
     * @param string $place
     *
     * @return $this
     */
    public function setInitialPlace($place)
    {
    }
    /**
     * @param string|string[]|null $initialPlaces
     *
     * @return $this
     */
    public function setInitialPlaces($initialPlaces)
    {
    }
    /**
     * @param string $place
     *
     * @return $this
     */
    public function addPlace($place)
    {
    }
    /**
     * @param string[] $places
     *
     * @return $this
     */
    public function addPlaces(array $places)
    {
    }
    /**
     * @param Transition[] $transitions
     *
     * @return $this
     */
    public function addTransitions(array $transitions)
    {
    }
    /**
     * @return $this
     */
    public function addTransition(\Symfony\Component\Workflow\Transition $transition)
    {
    }
    /**
     * @return $this
     */
    public function setMetadataStore(\Symfony\Component\Workflow\Metadata\MetadataStoreInterface $metadataStore)
    {
    }
    /**
     * @deprecated since Symfony 4.1, use the clear() method instead.
     *
     * @return $this
     */
    public function reset()
    {
    }
}
