<?php

namespace Symfony\Component\Workflow\Validator;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class WorkflowValidator implements \Symfony\Component\Workflow\Validator\DefinitionValidatorInterface
{
    public function __construct(bool $singlePlace = false)
    {
    }
    public function validate(\Symfony\Component\Workflow\Definition $definition, $name)
    {
    }
}
