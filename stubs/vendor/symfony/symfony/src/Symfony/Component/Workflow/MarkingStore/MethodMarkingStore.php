<?php

namespace Symfony\Component\Workflow\MarkingStore;

/**
 * MethodMarkingStore stores the marking with a subject's method.
 *
 * This store deals with a "single state" or "multiple state" Marking.
 *
 * "single state" Marking means a subject can be in one and only one state at
 * the same time. Use it with state machine.
 *
 * "multiple state" Marking means a subject can be in many states at the same
 * time. Use it with workflow.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
final class MethodMarkingStore implements \Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface
{
    /**
     * @param string $property Used to determine methods to call
     *                         The `getMarking` method will use `$subject->getProperty()`
     *                         The `setMarking` method will use `$subject->setProperty(string|array $places, array $context = array())`
     */
    public function __construct(bool $singleState = false, string $property = 'marking')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMarking($subject) : \Symfony\Component\Workflow\Marking
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMarking($subject, \Symfony\Component\Workflow\Marking $marking, array $context = [])
    {
    }
}
