<?php

namespace Symfony\Component\Workflow\Event;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @final since Symfony 4.4
 */
class GuardEvent extends \Symfony\Component\Workflow\Event\Event
{
    /**
     * {@inheritdoc}
     */
    public function __construct($subject, \Symfony\Component\Workflow\Marking $marking, \Symfony\Component\Workflow\Transition $transition, $workflowName = null)
    {
    }
    public function isBlocked()
    {
    }
    public function setBlocked($blocked)
    {
    }
    public function getTransitionBlockerList() : \Symfony\Component\Workflow\TransitionBlockerList
    {
    }
    public function addTransitionBlocker(\Symfony\Component\Workflow\TransitionBlocker $transitionBlocker) : void
    {
    }
}
