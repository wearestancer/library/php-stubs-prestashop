<?php

namespace Symfony\Component\Workflow;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class StateMachine extends \Symfony\Component\Workflow\Workflow
{
    public function __construct(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface $markingStore = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, string $name = 'unnamed')
    {
    }
}
