<?php

namespace Symfony\Component\Workflow\Metadata;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
final class InMemoryMetadataStore implements \Symfony\Component\Workflow\Metadata\MetadataStoreInterface
{
    use \Symfony\Component\Workflow\Metadata\GetMetadataTrait;
    public function __construct(array $workflowMetadata = [], array $placesMetadata = [], \SplObjectStorage $transitionsMetadata = null)
    {
    }
    public function getWorkflowMetadata() : array
    {
    }
    public function getPlaceMetadata(string $place) : array
    {
    }
    public function getTransitionMetadata(\Symfony\Component\Workflow\Transition $transition) : array
    {
    }
}
