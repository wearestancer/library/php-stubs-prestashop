<?php

namespace Symfony\Component\Workflow\Exception;

/**
 * Thrown by Workflow when an undefined transition is applied on a subject.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class UndefinedTransitionException extends \Symfony\Component\Workflow\Exception\TransitionException
{
    public function __construct($subject, string $transitionName, \Symfony\Component\Workflow\WorkflowInterface $workflow)
    {
    }
}
