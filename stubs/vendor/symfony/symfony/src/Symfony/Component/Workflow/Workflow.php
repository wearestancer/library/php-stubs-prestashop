<?php

namespace Symfony\Component\Workflow;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Workflow implements \Symfony\Component\Workflow\WorkflowInterface
{
    public function __construct(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface $markingStore = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, string $name = 'unnamed')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMarking($subject)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function can($subject, $transitionName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildTransitionBlockerList($subject, string $transitionName) : \Symfony\Component\Workflow\TransitionBlockerList
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param array $context Some context
     */
    public function apply($subject, $transitionName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEnabledTransitions($subject)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefinition()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMarkingStore()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataStore() : \Symfony\Component\Workflow\Metadata\MetadataStoreInterface
    {
    }
}
