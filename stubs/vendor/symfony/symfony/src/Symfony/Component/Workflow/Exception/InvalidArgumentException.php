<?php

namespace Symfony\Component\Workflow\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Workflow\Exception\ExceptionInterface
{
}
