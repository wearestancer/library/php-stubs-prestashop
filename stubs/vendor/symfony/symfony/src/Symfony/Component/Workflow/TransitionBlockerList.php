<?php

namespace Symfony\Component\Workflow;

/**
 * A list of transition blockers.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
final class TransitionBlockerList implements \IteratorAggregate, \Countable
{
    /**
     * @param TransitionBlocker[] $blockers
     */
    public function __construct(array $blockers = [])
    {
    }
    public function add(\Symfony\Component\Workflow\TransitionBlocker $blocker) : void
    {
    }
    public function has(string $code) : bool
    {
    }
    public function clear() : void
    {
    }
    public function isEmpty() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return \Traversable<TransitionBlocker>
     */
    public function getIterator() : \Traversable
    {
    }
    public function count() : int
    {
    }
}
