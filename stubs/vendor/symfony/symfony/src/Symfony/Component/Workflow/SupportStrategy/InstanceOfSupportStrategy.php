<?php

namespace Symfony\Component\Workflow\SupportStrategy;

/**
 * @author Andreas Kleemann <akleemann@inviqa.com>
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
final class InstanceOfSupportStrategy implements \Symfony\Component\Workflow\SupportStrategy\WorkflowSupportStrategyInterface
{
    public function __construct(string $className)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Workflow\WorkflowInterface $workflow, $subject) : bool
    {
    }
    public function getClassName() : string
    {
    }
}
