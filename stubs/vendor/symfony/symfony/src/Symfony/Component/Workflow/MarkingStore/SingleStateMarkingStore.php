<?php

namespace Symfony\Component\Workflow\MarkingStore;

/**
 * SingleStateMarkingStore stores the marking into a property of the subject.
 *
 * This store deals with a "single state" Marking. It means a subject can be in
 * one and only one state at the same time.
 *
 * @deprecated since Symfony 4.3, use MethodMarkingStore instead.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class SingleStateMarkingStore implements \Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface
{
    public function __construct(string $property = 'marking', \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMarking($subject)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param array $context Some context
     */
    public function setMarking($subject, \Symfony\Component\Workflow\Marking $marking)
    {
    }
    /**
     * @return string
     */
    public function getProperty()
    {
    }
}
