<?php

namespace Symfony\Component\Workflow\Validator;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
interface DefinitionValidatorInterface
{
    /**
     * @param string $name
     *
     * @throws InvalidDefinitionException on invalid definition
     */
    public function validate(\Symfony\Component\Workflow\Definition $definition, $name);
}
