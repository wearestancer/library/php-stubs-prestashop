<?php

namespace Symfony\Component\Workflow\Exception;

/**
 * Base RuntimeException for the Workflow component.
 *
 * @author Alain Flaus <alain.flaus@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Workflow\Exception\ExceptionInterface
{
}
