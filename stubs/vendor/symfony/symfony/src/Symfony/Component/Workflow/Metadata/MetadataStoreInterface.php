<?php

namespace Symfony\Component\Workflow\Metadata;

/**
 * MetadataStoreInterface is able to fetch metadata for a specific workflow.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
interface MetadataStoreInterface
{
    public function getWorkflowMetadata() : array;
    public function getPlaceMetadata(string $place) : array;
    public function getTransitionMetadata(\Symfony\Component\Workflow\Transition $transition) : array;
    /**
     * Returns the metadata for a specific subject.
     *
     * This is a proxy method.
     *
     * @param string|Transition|null $subject Use null to get workflow metadata
     *                                        Use a string (the place name) to get place metadata
     *                                        Use a Transition instance to get transition metadata
     */
    public function getMetadata(string $key, $subject = null);
}
