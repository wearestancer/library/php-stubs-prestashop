<?php

namespace Symfony\Component\Workflow;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
final class Definition
{
    /**
     * @param string[]             $places
     * @param Transition[]         $transitions
     * @param string|string[]|null $initialPlaces
     */
    public function __construct(array $places, array $transitions, $initialPlaces = null, \Symfony\Component\Workflow\Metadata\MetadataStoreInterface $metadataStore = null)
    {
    }
    /**
     * @deprecated since Symfony 4.3. Use getInitialPlaces() instead.
     */
    public function getInitialPlace() : ?string
    {
    }
    /**
     * @return string[]
     */
    public function getInitialPlaces() : array
    {
    }
    /**
     * @return string[]
     */
    public function getPlaces() : array
    {
    }
    /**
     * @return Transition[]
     */
    public function getTransitions() : array
    {
    }
    public function getMetadataStore() : \Symfony\Component\Workflow\Metadata\MetadataStoreInterface
    {
    }
}
