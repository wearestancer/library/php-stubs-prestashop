<?php

namespace Symfony\Component\Workflow\Event;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class Event extends \Symfony\Component\EventDispatcher\Event
{
    /**
     * @param object $subject
     */
    public function __construct($subject, \Symfony\Component\Workflow\Marking $marking, \Symfony\Component\Workflow\Transition $transition = null, $workflow = null)
    {
    }
    public function getMarking()
    {
    }
    public function getSubject()
    {
    }
    public function getTransition()
    {
    }
    public function getWorkflow() : \Symfony\Component\Workflow\WorkflowInterface
    {
    }
    public function getWorkflowName()
    {
    }
    public function getMetadata(string $key, $subject)
    {
    }
}
