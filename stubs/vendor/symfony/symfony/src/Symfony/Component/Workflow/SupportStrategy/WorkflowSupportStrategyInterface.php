<?php

namespace Symfony\Component\Workflow\SupportStrategy;

/**
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
interface WorkflowSupportStrategyInterface
{
    public function supports(\Symfony\Component\Workflow\WorkflowInterface $workflow, $subject) : bool;
}
