<?php

namespace Symfony\Component\Workflow\Dumper;

/**
 * GraphvizDumper dumps a workflow as a graphviz file.
 *
 * You can convert the generated dot file with the dot utility (https://graphviz.org/):
 *
 *   dot -Tpng workflow.dot > workflow.png
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class GraphvizDumper implements \Symfony\Component\Workflow\Dumper\DumperInterface
{
    // All values should be strings
    protected static $defaultOptions = ['graph' => ['ratio' => 'compress', 'rankdir' => 'LR'], 'node' => ['fontsize' => '9', 'fontname' => 'Arial', 'color' => '#333333', 'fillcolor' => 'lightblue', 'fixedsize' => 'false', 'width' => '1'], 'edge' => ['fontsize' => '9', 'fontname' => 'Arial', 'color' => '#333333', 'arrowhead' => 'normal', 'arrowsize' => '0.5']];
    /**
     * {@inheritdoc}
     *
     * Dumps the workflow as a graphviz graph.
     *
     * Available options:
     *
     *  * graph: The default options for the whole graph
     *  * node: The default options for nodes (places + transitions)
     *  * edge: The default options for edges
     */
    public function dump(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\Marking $marking = null, array $options = [])
    {
    }
    /**
     * @internal
     */
    protected function findPlaces(\Symfony\Component\Workflow\Definition $definition, \Symfony\Component\Workflow\Marking $marking = null) : array
    {
    }
    /**
     * @internal
     */
    protected function findTransitions(\Symfony\Component\Workflow\Definition $definition) : array
    {
    }
    /**
     * @internal
     */
    protected function addPlaces(array $places) : string
    {
    }
    /**
     * @internal
     */
    protected function addTransitions(array $transitions) : string
    {
    }
    /**
     * @internal
     */
    protected function findEdges(\Symfony\Component\Workflow\Definition $definition) : array
    {
    }
    /**
     * @internal
     */
    protected function addEdges(array $edges) : string
    {
    }
    /**
     * @internal
     */
    protected function startDot(array $options) : string
    {
    }
    /**
     * @internal
     */
    protected function endDot() : string
    {
    }
    /**
     * @internal
     */
    protected function dotize(string $id) : string
    {
    }
    /**
     * @internal
     */
    protected function escape($value) : string
    {
    }
    protected function addAttributes(array $attributes) : string
    {
    }
}
