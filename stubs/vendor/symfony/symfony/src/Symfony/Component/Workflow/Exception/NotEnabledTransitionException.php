<?php

namespace Symfony\Component\Workflow\Exception;

/**
 * Thrown by Workflow when a not enabled transition is applied on a subject.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class NotEnabledTransitionException extends \Symfony\Component\Workflow\Exception\TransitionException
{
    public function __construct($subject, string $transitionName, \Symfony\Component\Workflow\WorkflowInterface $workflow, \Symfony\Component\Workflow\TransitionBlockerList $transitionBlockerList)
    {
    }
    public function getTransitionBlockerList() : \Symfony\Component\Workflow\TransitionBlockerList
    {
    }
}
