<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class MysqliCaster
{
    public static function castMysqliDriver(\mysqli_driver $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, bool $isNested) : array
    {
    }
}
