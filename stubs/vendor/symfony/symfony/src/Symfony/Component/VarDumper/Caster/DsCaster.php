<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Casts Ds extension classes to array representation.
 *
 * @author Jáchym Toušek <enumag@gmail.com>
 *
 * @final since Symfony 4.4
 */
class DsCaster
{
    public static function castCollection(\Ds\Collection $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, bool $isNested) : array
    {
    }
    public static function castMap(\Ds\Map $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, bool $isNested) : array
    {
    }
    public static function castPair(\Ds\Pair $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, bool $isNested) : array
    {
    }
    public static function castPairStub(\Symfony\Component\VarDumper\Caster\DsPairStub $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, bool $isNested) : array
    {
    }
}
