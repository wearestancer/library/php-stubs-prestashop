<?php

namespace Symfony\Component\VarDumper\Command\Descriptor;

/**
 * Describe collected data clones for html output.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final
 */
class HtmlDescriptor implements \Symfony\Component\VarDumper\Command\Descriptor\DumpDescriptorInterface
{
    public function __construct(\Symfony\Component\VarDumper\Dumper\HtmlDumper $dumper)
    {
    }
    public function describe(\Symfony\Component\Console\Output\OutputInterface $output, \Symfony\Component\VarDumper\Cloner\Data $data, array $context, int $clientId) : void
    {
    }
}
