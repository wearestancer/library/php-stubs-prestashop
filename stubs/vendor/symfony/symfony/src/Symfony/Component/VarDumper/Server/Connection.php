<?php

namespace Symfony\Component\VarDumper\Server;

/**
 * Forwards serialized Data clones to a server.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class Connection
{
    /**
     * @param string                     $host             The server host
     * @param ContextProviderInterface[] $contextProviders Context providers indexed by context name
     */
    public function __construct(string $host, array $contextProviders = [])
    {
    }
    public function getContextProviders() : array
    {
    }
    public function write(\Symfony\Component\VarDumper\Cloner\Data $data) : bool
    {
    }
}
