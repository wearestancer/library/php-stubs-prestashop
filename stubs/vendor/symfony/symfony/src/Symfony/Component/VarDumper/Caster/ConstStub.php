<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Represents a PHP constant and its value.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ConstStub extends \Symfony\Component\VarDumper\Cloner\Stub
{
    public function __construct(string $name, $value = null)
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
