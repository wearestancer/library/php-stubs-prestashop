<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * @final since Symfony 4.4
 */
class SymfonyCaster
{
    public static function castRequest(\Symfony\Component\HttpFoundation\Request $request, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
    public static function castHttpClient($client, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
    public static function castHttpClientResponse($response, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
}
