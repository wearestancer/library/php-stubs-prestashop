<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * @author Jan Schädlich <jan.schaedlich@sensiolabs.de>
 *
 * @final since Symfony 4.4
 */
class MemcachedCaster
{
    public static function castMemcached(\Memcached $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
}
