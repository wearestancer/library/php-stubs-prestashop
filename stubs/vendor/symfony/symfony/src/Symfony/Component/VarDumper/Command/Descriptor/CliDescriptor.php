<?php

namespace Symfony\Component\VarDumper\Command\Descriptor;

/**
 * Describe collected data clones for cli output.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final
 */
class CliDescriptor implements \Symfony\Component\VarDumper\Command\Descriptor\DumpDescriptorInterface
{
    public function __construct(\Symfony\Component\VarDumper\Dumper\CliDumper $dumper)
    {
    }
    public function describe(\Symfony\Component\Console\Output\OutputInterface $output, \Symfony\Component\VarDumper\Cloner\Data $data, array $context, int $clientId) : void
    {
    }
}
