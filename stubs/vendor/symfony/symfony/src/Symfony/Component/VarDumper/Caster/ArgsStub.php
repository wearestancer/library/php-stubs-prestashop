<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Represents a list of function arguments.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ArgsStub extends \Symfony\Component\VarDumper\Caster\EnumStub
{
    public function __construct(array $args, string $function, ?string $class)
    {
    }
}
