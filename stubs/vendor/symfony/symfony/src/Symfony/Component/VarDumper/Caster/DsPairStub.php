<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class DsPairStub extends \Symfony\Component\VarDumper\Cloner\Stub
{
    public function __construct($key, $value)
    {
    }
}
