<?php

namespace Symfony\Component\VarDumper\Test;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
trait VarDumperTestTrait
{
    /**
     * @internal
     */
    private $varDumperConfig = ['casters' => [], 'flags' => null];
    protected function setUpVarDumper(array $casters, int $flags = null) : void
    {
    }
    /**
     * @after
     */
    protected function tearDownVarDumper() : void
    {
    }
    public function assertDumpEquals($expected, $data, $filter = 0, $message = '')
    {
    }
    public function assertDumpMatchesFormat($expected, $data, $filter = 0, $message = '')
    {
    }
    /**
     * @return string|null
     */
    protected function getDump($data, $key = null, $filter = 0)
    {
    }
    private function prepareExpectation($expected, int $filter) : string
    {
    }
}
