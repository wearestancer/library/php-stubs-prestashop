<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Casts XmlReader class to array representation.
 *
 * @author Baptiste Clavié <clavie.b@gmail.com>
 *
 * @final since Symfony 4.4
 */
class XmlReaderCaster
{
    public static function castXmlReader(\XMLReader $reader, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
}
