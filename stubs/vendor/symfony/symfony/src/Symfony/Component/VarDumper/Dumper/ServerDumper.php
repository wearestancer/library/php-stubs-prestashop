<?php

namespace Symfony\Component\VarDumper\Dumper;

/**
 * ServerDumper forwards serialized Data clones to a server.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class ServerDumper implements \Symfony\Component\VarDumper\Dumper\DataDumperInterface
{
    /**
     * @param string                     $host             The server host
     * @param DataDumperInterface|null   $wrappedDumper    A wrapped instance used whenever we failed contacting the server
     * @param ContextProviderInterface[] $contextProviders Context providers indexed by context name
     */
    public function __construct(string $host, \Symfony\Component\VarDumper\Dumper\DataDumperInterface $wrappedDumper = null, array $contextProviders = [])
    {
    }
    public function getContextProviders() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function dump(\Symfony\Component\VarDumper\Cloner\Data $data)
    {
    }
}
