<?php

namespace Symfony\Component\VarDumper\Dumper\ContextProvider;

/**
 * Tries to provide context from sources (class name, file, line, code excerpt, ...).
 *
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class SourceContextProvider implements \Symfony\Component\VarDumper\Dumper\ContextProvider\ContextProviderInterface
{
    public function __construct(string $charset = null, string $projectDir = null, \Symfony\Component\HttpKernel\Debug\FileLinkFormatter $fileLinkFormatter = null, int $limit = 9)
    {
    }
    public function getContext() : ?array
    {
    }
}
