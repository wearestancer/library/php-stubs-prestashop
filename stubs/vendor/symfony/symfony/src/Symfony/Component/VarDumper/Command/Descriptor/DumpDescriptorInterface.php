<?php

namespace Symfony\Component\VarDumper\Command\Descriptor;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
interface DumpDescriptorInterface
{
    public function describe(\Symfony\Component\Console\Output\OutputInterface $output, \Symfony\Component\VarDumper\Cloner\Data $data, array $context, int $clientId) : void;
}
