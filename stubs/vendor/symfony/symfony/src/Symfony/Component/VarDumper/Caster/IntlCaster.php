<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Jan Schädlich <jan.schaedlich@sensiolabs.de>
 *
 * @final since Symfony 4.4
 */
class IntlCaster
{
    public static function castMessageFormatter(\MessageFormatter $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
    public static function castNumberFormatter(\NumberFormatter $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter = 0)
    {
    }
    public static function castIntlTimeZone(\IntlTimeZone $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested)
    {
    }
    public static function castIntlCalendar(\IntlCalendar $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter = 0)
    {
    }
    public static function castIntlDateFormatter(\IntlDateFormatter $c, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter = 0)
    {
    }
}
