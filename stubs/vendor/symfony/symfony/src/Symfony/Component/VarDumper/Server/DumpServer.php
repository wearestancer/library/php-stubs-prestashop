<?php

namespace Symfony\Component\VarDumper\Server;

/**
 * A server collecting Data clones sent by a ServerDumper.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final
 */
class DumpServer
{
    public function __construct(string $host, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function start() : void
    {
    }
    public function listen(callable $callback) : void
    {
    }
    public function getHost() : string
    {
    }
}
