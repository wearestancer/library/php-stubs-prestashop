<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Casts GMP objects to array representation.
 *
 * @author Hamza Amrouche <hamza.simperfit@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @final since Symfony 4.4
 */
class GmpCaster
{
    public static function castGmp(\GMP $gmp, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter) : array
    {
    }
}
