<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class ImgStub extends \Symfony\Component\VarDumper\Caster\ConstStub
{
    public function __construct(string $data, string $contentType, string $size)
    {
    }
}
