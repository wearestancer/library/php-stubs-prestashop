<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Represents a file or a URL.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class LinkStub extends \Symfony\Component\VarDumper\Caster\ConstStub
{
    public $inVendor = false;
    public function __construct(string $label, int $line = 0, string $href = null)
    {
    }
}
