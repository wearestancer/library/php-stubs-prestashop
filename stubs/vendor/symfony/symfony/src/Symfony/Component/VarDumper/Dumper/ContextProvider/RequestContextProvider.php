<?php

namespace Symfony\Component\VarDumper\Dumper\ContextProvider;

/**
 * Tries to provide context from a request.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class RequestContextProvider implements \Symfony\Component\VarDumper\Dumper\ContextProvider\ContextProviderInterface
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    public function getContext() : ?array
    {
    }
}
