<?php

namespace Symfony\Component\VarDumper\Command;

/**
 * Starts a dump server to collect and output dumps on a single place with multiple formats support.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final
 */
class ServerDumpCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'server:dump';
    public function __construct(\Symfony\Component\VarDumper\Server\DumpServer $server, array $descriptors = [])
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
