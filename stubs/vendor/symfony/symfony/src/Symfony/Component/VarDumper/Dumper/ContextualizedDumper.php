<?php

namespace Symfony\Component\VarDumper\Dumper;

/**
 * @author Kévin Thérage <therage.kevin@gmail.com>
 */
class ContextualizedDumper implements \Symfony\Component\VarDumper\Dumper\DataDumperInterface
{
    /**
     * @param ContextProviderInterface[] $contextProviders
     */
    public function __construct(\Symfony\Component\VarDumper\Dumper\DataDumperInterface $wrappedDumper, array $contextProviders)
    {
    }
    public function dump(\Symfony\Component\VarDumper\Cloner\Data $data)
    {
    }
}
