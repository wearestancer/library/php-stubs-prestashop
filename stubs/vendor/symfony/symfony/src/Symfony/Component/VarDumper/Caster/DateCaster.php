<?php

namespace Symfony\Component\VarDumper\Caster;

/**
 * Casts DateTimeInterface related classes to array representation.
 *
 * @author Dany Maillard <danymaillard93b@gmail.com>
 *
 * @final since Symfony 4.4
 */
class DateCaster
{
    public static function castDateTime(\DateTimeInterface $d, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter)
    {
    }
    public static function castInterval(\DateInterval $interval, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter)
    {
    }
    public static function castTimeZone(\DateTimeZone $timeZone, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter)
    {
    }
    public static function castPeriod(\DatePeriod $p, array $a, \Symfony\Component\VarDumper\Cloner\Stub $stub, $isNested, $filter)
    {
    }
}
