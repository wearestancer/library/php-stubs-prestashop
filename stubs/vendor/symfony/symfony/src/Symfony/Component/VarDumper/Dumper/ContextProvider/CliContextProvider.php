<?php

namespace Symfony\Component\VarDumper\Dumper\ContextProvider;

/**
 * Tries to provide context on CLI.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
final class CliContextProvider implements \Symfony\Component\VarDumper\Dumper\ContextProvider\ContextProviderInterface
{
    public function getContext() : ?array
    {
    }
}
