<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Checks if a password has been leaked in a data breach using haveibeenpwned.com's API.
 * Use a k-anonymity model to protect the password being searched for.
 *
 * @see https://haveibeenpwned.com/API/v2#SearchingPwnedPasswordsByRange
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class NotCompromisedPasswordValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $httpClient = null, string $charset = 'UTF-8', bool $enabled = true, string $endpoint = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws ExceptionInterface
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
