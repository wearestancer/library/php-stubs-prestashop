<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Jan Schädlich <jan.schaedlich@sensiolabs.de>
 */
class PositiveOrZero extends \Symfony\Component\Validator\Constraints\GreaterThanOrEqual
{
    use \Symfony\Component\Validator\Constraints\NumberConstraintTrait;
    public $message = 'This value should be either positive or zero.';
    public function __construct($options = null)
    {
    }
    public function validatedBy() : string
    {
    }
}
