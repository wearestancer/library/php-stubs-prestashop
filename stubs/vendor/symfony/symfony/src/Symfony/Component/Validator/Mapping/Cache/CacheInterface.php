<?php

namespace Symfony\Component\Validator\Mapping\Cache;

/**
 * Persists ClassMetadata instances in a cache.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @deprecated since Symfony 4.4.
 */
interface CacheInterface
{
    /**
     * Returns whether metadata for the given class exists in the cache.
     *
     * @param string $class
     */
    public function has($class);
    /**
     * Returns the metadata for the given class from the cache.
     *
     * @param string $class Class Name
     *
     * @return ClassMetadata|false A ClassMetadata instance or false on miss
     */
    public function read($class);
    /**
     * Stores a class metadata in the cache.
     */
    public function write(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata);
}
