<?php

namespace Symfony\Component\Validator;

/**
 * Defines the interface for a group sequence provider.
 */
interface GroupSequenceProviderInterface
{
    /**
     * Returns which validation groups should be used for a certain state
     * of the object.
     *
     * @return string[]|string[][]|GroupSequence An array of validation groups
     */
    public function getGroupSequence();
}
