<?php

namespace Symfony\Component\Validator\Exception;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class UnsupportedMetadataException extends \Symfony\Component\Validator\Exception\InvalidArgumentException
{
}
