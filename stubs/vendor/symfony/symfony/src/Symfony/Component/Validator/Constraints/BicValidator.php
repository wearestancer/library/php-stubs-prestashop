<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @author Michael Hirschler <michael.vhirsch@gmail.com>
 *
 * @see https://en.wikipedia.org/wiki/ISO_9362#Structure
 */
class BicValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public function __construct(\Symfony\Component\PropertyAccess\PropertyAccessor $propertyAccessor = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
