<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Used for the comparison of values.
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class AbstractComparison extends \Symfony\Component\Validator\Constraint
{
    public $message;
    public $value;
    public $propertyPath;
    /**
     * {@inheritdoc}
     */
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }
}
