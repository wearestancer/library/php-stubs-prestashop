<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Type extends \Symfony\Component\Validator\Constraint
{
    public const INVALID_TYPE_ERROR = 'ba785a8c-82cb-4283-967c-3cf342181b40';
    protected static $errorNames = [self::INVALID_TYPE_ERROR => 'INVALID_TYPE_ERROR'];
    public $message = 'This value should be of type {{ type }}.';
    public $type;
    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
    }
}
