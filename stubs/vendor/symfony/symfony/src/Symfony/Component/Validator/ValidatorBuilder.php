<?php

namespace Symfony\Component\Validator;

/**
 * The default implementation of {@link ValidatorBuilderInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ValidatorBuilder implements \Symfony\Component\Validator\ValidatorBuilderInterface
{
    /**
     * {@inheritdoc}
     */
    public function addObjectInitializer(\Symfony\Component\Validator\ObjectInitializerInterface $initializer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addObjectInitializers(array $initializers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addXmlMapping($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addXmlMappings(array $paths)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addYamlMapping($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addYamlMappings(array $paths)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addMethodMapping($methodName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addMethodMappings(array $methodNames)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function enableAnnotationMapping(\Doctrine\Common\Annotations\Reader $annotationReader = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function disableAnnotationMapping()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setMetadataFactory(\Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface $metadataFactory)
    {
    }
    /**
     * Sets the cache for caching class metadata.
     *
     * @return $this
     *
     * @deprecated since Symfony 4.4.
     */
    public function setMetadataCache(\Symfony\Component\Validator\Mapping\Cache\CacheInterface $cache)
    {
    }
    /**
     * Sets the cache for caching class metadata.
     *
     * @return $this
     */
    public function setMappingCache(\Psr\Cache\CacheItemPoolInterface $cache)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setConstraintValidatorFactory(\Symfony\Component\Validator\ConstraintValidatorFactoryInterface $validatorFactory)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @final since Symfony 4.2
     */
    public function setTranslator(\Symfony\Component\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setTranslationDomain($translationDomain)
    {
    }
    /**
     * @return $this
     */
    public function addLoader(\Symfony\Component\Validator\Mapping\Loader\LoaderInterface $loader)
    {
    }
    /**
     * @return LoaderInterface[]
     */
    public function getLoaders()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValidator()
    {
    }
}
