<?php

namespace Symfony\Component\Validator;

/**
 * Default implementation of {@ConstraintViolationInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ConstraintViolation implements \Symfony\Component\Validator\ConstraintViolationInterface
{
    /**
     * Creates a new constraint violation.
     *
     * @param string|\Stringable $message         The violation message as a string or a stringable object
     * @param string|null        $messageTemplate The raw violation message
     * @param array              $parameters      The parameters to substitute in the
     *                                            raw violation message
     * @param mixed              $root            The value originally passed to the
     *                                            validator
     * @param string|null        $propertyPath    The property path from the root
     *                                            value to the invalid value
     * @param mixed              $invalidValue    The invalid value that caused this
     *                                            violation
     * @param int|null           $plural          The number for determining the plural
     *                                            form when translating the message
     * @param string|null        $code            The error code of the violation
     * @param Constraint|null    $constraint      The constraint whose validation
     *                                            caused the violation
     * @param mixed              $cause           The cause of the violation
     */
    public function __construct($message, ?string $messageTemplate, array $parameters, $root, ?string $propertyPath, $invalidValue, int $plural = null, $code = null, \Symfony\Component\Validator\Constraint $constraint = null, $cause = null)
    {
    }
    /**
     * Converts the violation into a string for debugging purposes.
     *
     * @return string The violation as string
     */
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessageTemplate()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParameters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPlural()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyPath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInvalidValue()
    {
    }
    /**
     * Returns the constraint whose validation caused the violation.
     *
     * @return Constraint|null The constraint or null if it is not known
     */
    public function getConstraint()
    {
    }
    /**
     * Returns the cause of the violation.
     *
     * @return mixed
     */
    public function getCause()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
    }
}
