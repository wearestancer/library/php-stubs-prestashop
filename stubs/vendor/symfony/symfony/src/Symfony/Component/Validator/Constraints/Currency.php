<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Miha Vrhovnik <miha.vrhovnik@pagein.si>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Currency extends \Symfony\Component\Validator\Constraint
{
    public const NO_SUCH_CURRENCY_ERROR = '69945ac1-2db4-405f-bec7-d2772f73df52';
    protected static $errorNames = [self::NO_SUCH_CURRENCY_ERROR => 'NO_SUCH_CURRENCY_ERROR'];
    public $message = 'This value is not a valid currency.';
    public function __construct($options = null)
    {
    }
}
