<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class DateValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public const PATTERN = '/^(\\d{4})-(\\d{2})-(\\d{2})$/';
    /**
     * Checks whether a date is valid.
     *
     * @internal
     */
    public static function checkDate(int $year, int $month, int $day) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
