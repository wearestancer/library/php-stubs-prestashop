<?php

namespace Symfony\Component\Validator\Mapping\Cache;

/**
 * PSR-6 adapter.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @deprecated since Symfony 4.4.
 */
class Psr6Cache implements \Symfony\Component\Validator\Mapping\Cache\CacheInterface
{
    public function __construct(\Psr\Cache\CacheItemPoolInterface $cacheItemPool)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function read($class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function write(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
    }
}
