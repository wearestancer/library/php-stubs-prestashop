<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Loads validation metadata using a Doctrine annotation {@link Reader}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class AnnotationLoader implements \Symfony\Component\Validator\Mapping\Loader\LoaderInterface
{
    protected $reader;
    public function __construct(\Doctrine\Common\Annotations\Reader $reader)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
    }
}
