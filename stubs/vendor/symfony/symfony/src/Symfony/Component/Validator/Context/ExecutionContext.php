<?php

namespace Symfony\Component\Validator\Context;

/**
 * The context used and created by {@link ExecutionContextFactory}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see ExecutionContextInterface
 *
 * @internal since version 2.5. Code against ExecutionContextInterface instead.
 */
class ExecutionContext implements \Symfony\Component\Validator\Context\ExecutionContextInterface
{
    /**
     * Creates a new execution context.
     *
     * @param mixed               $root              The root value of the
     *                                               validated object graph
     * @param TranslatorInterface $translator        The translator
     * @param string|null         $translationDomain The translation domain to
     *                                               use for translating
     *                                               violation messages
     *
     * @internal Called by {@link ExecutionContextFactory}. Should not be used
     *           in user code.
     */
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator, $root, $translator, string $translationDomain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setNode($value, $object, \Symfony\Component\Validator\Mapping\MetadataInterface $metadata = null, $propertyPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setGroup($group)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addViolation($message, array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildViolation($message, array $parameters = []) : \Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getViolations() : \Symfony\Component\Validator\ConstraintViolationListInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValidator() : \Symfony\Component\Validator\Validator\ValidatorInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getObject()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadata() : ?\Symfony\Component\Validator\Mapping\MetadataInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getGroup() : ?string
    {
    }
    public function getConstraint() : ?\Symfony\Component\Validator\Constraint
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassName() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyName() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyPath($subPath = '') : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function markGroupAsValidated($cacheKey, $groupHash)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isGroupValidated($cacheKey, $groupHash) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function markConstraintAsValidated($cacheKey, $constraintHash)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isConstraintValidated($cacheKey, $constraintHash) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function markObjectAsInitialized($cacheKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isObjectInitialized($cacheKey) : bool
    {
    }
    /**
     * @internal
     *
     * @param object $object
     *
     * @return string
     */
    public function generateCacheKey($object)
    {
    }
}
