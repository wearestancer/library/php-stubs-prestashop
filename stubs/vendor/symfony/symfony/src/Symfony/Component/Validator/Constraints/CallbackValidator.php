<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Validator for Callback constraint.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CallbackValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($object, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
