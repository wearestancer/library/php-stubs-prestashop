<?php

namespace Symfony\Component\Validator\Exception;

/**
 * Base BadMethodCallException for the Validator component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class BadMethodCallException extends \BadMethodCallException implements \Symfony\Component\Validator\Exception\ExceptionInterface
{
}
