<?php

namespace Symfony\Component\Validator\Exception;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
class UnexpectedValueException extends \Symfony\Component\Validator\Exception\UnexpectedTypeException
{
    public function __construct($value, string $expectedType)
    {
    }
    public function getExpectedType() : string
    {
    }
}
