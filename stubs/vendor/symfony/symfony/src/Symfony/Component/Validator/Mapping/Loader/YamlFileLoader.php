<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Loads validation metadata from a YAML file.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class YamlFileLoader extends \Symfony\Component\Validator\Mapping\Loader\FileLoader
{
    /**
     * An array of YAML class descriptions.
     *
     * @var array
     */
    protected $classes = null;
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * Return the names of the classes mapped in this file.
     *
     * @return string[] The classes names
     */
    public function getMappedClasses()
    {
    }
    /**
     * Parses a collection of YAML nodes.
     *
     * @param array $nodes The YAML nodes
     *
     * @return array An array of values or Constraint instances
     */
    protected function parseNodes(array $nodes)
    {
    }
}
