<?php

namespace Symfony\Component\Validator\Mapping;

/**
 * Stores all metadata needed for validating a class property.
 *
 * The value of the property is obtained by directly accessing the property.
 * The property will be accessed by reflection, so the access of private and
 * protected properties is supported.
 *
 * This class supports serialization and cloning.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see PropertyMetadataInterface
 */
class PropertyMetadata extends \Symfony\Component\Validator\Mapping\MemberMetadata
{
    /**
     * @param string $class The class this property is defined on
     * @param string $name  The name of this property
     *
     * @throws ValidatorException
     */
    public function __construct(string $class, string $name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyValue($object)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function newReflectionMember($objectOrClassName)
    {
    }
}
