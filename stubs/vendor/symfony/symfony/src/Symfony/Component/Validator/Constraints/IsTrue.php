<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IsTrue extends \Symfony\Component\Validator\Constraint
{
    public const NOT_TRUE_ERROR = '2beabf1c-54c0-4882-a928-05249b26e23b';
    protected static $errorNames = [self::NOT_TRUE_ERROR => 'NOT_TRUE_ERROR'];
    public $message = 'This value should be true.';
}
