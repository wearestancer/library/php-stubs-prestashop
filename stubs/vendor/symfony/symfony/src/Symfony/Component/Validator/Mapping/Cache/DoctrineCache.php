<?php

namespace Symfony\Component\Validator\Mapping\Cache;

/**
 * Adapts a Doctrine cache to a CacheInterface.
 *
 * @author Florian Voutzinos <florian@voutzinos.com>
 *
 * @deprecated since Symfony 4.4.
 */
final class DoctrineCache implements \Symfony\Component\Validator\Mapping\Cache\CacheInterface
{
    public function __construct(\Doctrine\Common\Cache\Cache $cache)
    {
    }
    public function setCache(\Doctrine\Common\Cache\Cache $cache)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($class) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function read($class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function write(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
    }
}
