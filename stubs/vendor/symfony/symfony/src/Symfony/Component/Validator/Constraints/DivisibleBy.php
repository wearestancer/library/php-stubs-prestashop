<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Colin O'Dell <colinodell@gmail.com>
 */
class DivisibleBy extends \Symfony\Component\Validator\Constraints\AbstractComparison
{
    public const NOT_DIVISIBLE_BY = '6d99d6c3-1464-4ccf-bdc7-14d083cf455c';
    protected static $errorNames = [self::NOT_DIVISIBLE_BY => 'NOT_DIVISIBLE_BY'];
    public $message = 'This value should be a multiple of {{ compared_value }}.';
}
