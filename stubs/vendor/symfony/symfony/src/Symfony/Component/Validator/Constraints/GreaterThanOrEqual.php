<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class GreaterThanOrEqual extends \Symfony\Component\Validator\Constraints\AbstractComparison
{
    public const TOO_LOW_ERROR = 'ea4e51d1-3342-48bd-87f1-9e672cd90cad';
    protected static $errorNames = [self::TOO_LOW_ERROR => 'TOO_LOW_ERROR'];
    public $message = 'This value should be greater than or equal to {{ compared_value }}.';
}
