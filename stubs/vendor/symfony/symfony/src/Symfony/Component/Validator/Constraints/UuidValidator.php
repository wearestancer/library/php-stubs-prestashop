<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Validates whether the value is a valid UUID (also known as GUID).
 *
 * Strict validation will allow a UUID as specified per RFC 4122.
 * Loose validation will allow any type of UUID.
 *
 * For better compatibility, both loose and strict, you should consider using a specialized UUID library like "ramsey/uuid" instead.
 *
 * @author Colin O'Dell <colinodell@gmail.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see http://tools.ietf.org/html/rfc4122
 * @see https://en.wikipedia.org/wiki/Universally_unique_identifier
 * @see https://github.com/ramsey/uuid
 */
class UuidValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    // The strict pattern matches UUIDs like this:
    // xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx
    // Roughly speaking:
    // x = any hexadecimal character
    // M = any allowed version {1..5}
    // N = any allowed variant {8, 9, a, b}
    public const STRICT_LENGTH = 36;
    public const STRICT_FIRST_HYPHEN_POSITION = 8;
    public const STRICT_LAST_HYPHEN_POSITION = 23;
    public const STRICT_VERSION_POSITION = 14;
    public const STRICT_VARIANT_POSITION = 19;
    // The loose pattern validates similar yet non-compliant UUIDs.
    // Hyphens are completely optional. If present, they should only appear
    // between every fourth character:
    // xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx
    // xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxx-xxxx
    // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    // The value can also be wrapped with characters like []{}:
    // {xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx}
    // Neither the version nor the variant is validated by this pattern.
    public const LOOSE_MAX_LENGTH = 39;
    public const LOOSE_FIRST_HYPHEN_POSITION = 4;
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
