<?php

namespace Symfony\Component\Validator\Exception;

class InvalidOptionsException extends \Symfony\Component\Validator\Exception\ValidatorException
{
    public function __construct(string $message, array $options)
    {
    }
    public function getOptions()
    {
    }
}
