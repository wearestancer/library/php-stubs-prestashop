<?php

namespace Symfony\Component\Validator\Context;

/**
 * Creates instances of {@link ExecutionContextInterface}.
 *
 * You can use a custom factory if you want to customize the execution context
 * that is passed through the validation run.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface ExecutionContextFactoryInterface
{
    /**
     * Creates a new execution context.
     *
     * @param mixed $root The root value of the validated
     *                    object graph
     *
     * @return ExecutionContextInterface The new execution context
     */
    public function createContext(\Symfony\Component\Validator\Validator\ValidatorInterface $validator, $root);
}
