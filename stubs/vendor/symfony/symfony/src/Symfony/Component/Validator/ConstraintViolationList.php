<?php

namespace Symfony\Component\Validator;

/**
 * Default implementation of {@ConstraintViolationListInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ConstraintViolationList implements \IteratorAggregate, \Symfony\Component\Validator\ConstraintViolationListInterface
{
    /**
     * Creates a new constraint violation list.
     *
     * @param ConstraintViolationInterface[] $violations The constraint violations to add to the list
     */
    public function __construct(array $violations = [])
    {
    }
    /**
     * Converts the violation into a string for debugging purposes.
     *
     * @return string The violation as string
     */
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add(\Symfony\Component\Validator\ConstraintViolationInterface $violation)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addAll(\Symfony\Component\Validator\ConstraintViolationListInterface $otherList)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($offset)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($offset)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($offset, \Symfony\Component\Validator\ConstraintViolationInterface $violation)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($offset)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return \ArrayIterator|ConstraintViolationInterface[]
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $violation)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
    }
    /**
     * Creates iterator for errors with specific codes.
     *
     * @param string|string[] $codes The codes to find
     *
     * @return static new instance which contains only specific errors
     */
    public function findByCodes($codes)
    {
    }
}
