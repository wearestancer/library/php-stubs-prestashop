<?php

namespace Symfony\Component\Validator\Validator;

/**
 * Recursive implementation of {@link ContextualValidatorInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RecursiveContextualValidator implements \Symfony\Component\Validator\Validator\ContextualValidatorInterface
{
    /**
     * Creates a validator for the given context.
     *
     * @param ObjectInitializerInterface[] $objectInitializers The object initializers
     */
    public function __construct(\Symfony\Component\Validator\Context\ExecutionContextInterface $context, \Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface $metadataFactory, \Symfony\Component\Validator\ConstraintValidatorFactoryInterface $validatorFactory, array $objectInitializers = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function atPath($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, $constraints = null, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateProperty($object, $propertyName, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validatePropertyValue($objectOrClass, $propertyName, $value, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getViolations()
    {
    }
    /**
     * Normalizes the given group or list of groups to an array.
     *
     * @param string|GroupSequence|array<string|GroupSequence> $groups The groups to normalize
     *
     * @return array<string|GroupSequence> A group array
     */
    protected function normalizeGroups($groups)
    {
    }
}
