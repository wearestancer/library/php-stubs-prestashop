<?php

namespace Symfony\Component\Validator\Exception;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class NoSuchMetadataException extends \Symfony\Component\Validator\Exception\ValidatorException
{
}
