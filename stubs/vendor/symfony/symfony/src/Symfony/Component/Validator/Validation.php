<?php

namespace Symfony\Component\Validator;

/**
 * Entry point for the Validator component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
final class Validation
{
    /**
     * Creates a new validator.
     *
     * If you want to configure the validator, use
     * {@link createValidatorBuilder()} instead.
     */
    public static function createValidator() : \Symfony\Component\Validator\Validator\ValidatorInterface
    {
    }
    /**
     * Creates a configurable builder for validator objects.
     */
    public static function createValidatorBuilder() : \Symfony\Component\Validator\ValidatorBuilder
    {
    }
}
