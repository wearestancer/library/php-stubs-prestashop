<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Imad ZAIRIG <imadzairig@gmail.com>
 */
class Json extends \Symfony\Component\Validator\Constraint
{
    public const INVALID_JSON_ERROR = '0789c8ad-2d2b-49a4-8356-e2ce63998504';
    protected static $errorNames = [self::INVALID_JSON_ERROR => 'INVALID_JSON_ERROR'];
    public $message = 'This value should be valid JSON.';
}
