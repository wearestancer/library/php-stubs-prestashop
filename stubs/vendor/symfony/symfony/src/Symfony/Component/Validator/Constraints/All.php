<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class All extends \Symfony\Component\Validator\Constraints\Composite
{
    public $constraints = [];
    public function getDefaultOption()
    {
    }
    public function getRequiredOptions()
    {
    }
    protected function getCompositeOption()
    {
    }
}
