<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Guesses and loads the appropriate constraints using PropertyInfo.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class PropertyInfoLoader implements \Symfony\Component\Validator\Mapping\Loader\LoaderInterface
{
    use \Symfony\Component\Validator\Mapping\Loader\AutoMappingTrait;
    public function __construct(\Symfony\Component\PropertyInfo\PropertyListExtractorInterface $listExtractor, \Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface $typeExtractor, \Symfony\Component\PropertyInfo\PropertyAccessExtractorInterface $accessExtractor, string $classValidatorRegexp = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata) : bool
    {
    }
}
