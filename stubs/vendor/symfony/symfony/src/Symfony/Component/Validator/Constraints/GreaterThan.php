<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class GreaterThan extends \Symfony\Component\Validator\Constraints\AbstractComparison
{
    public const TOO_LOW_ERROR = '778b7ae0-84d3-481a-9dec-35fdb64b1d78';
    protected static $errorNames = [self::TOO_LOW_ERROR => 'TOO_LOW_ERROR'];
    public $message = 'This value should be greater than {{ compared_value }}.';
}
