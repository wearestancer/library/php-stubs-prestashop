<?php

namespace Symfony\Component\Validator;

/**
 * Uses a service container to create constraint validators.
 *
 * @author Kris Wallsmith <kris@symfony.com>
 */
class ContainerConstraintValidatorFactory implements \Symfony\Component\Validator\ConstraintValidatorFactoryInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws ValidatorException      When the validator class does not exist
     * @throws UnexpectedTypeException When the validator is not an instance of ConstraintValidatorInterface
     */
    public function getInstance(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
