<?php

namespace Symfony\Component\Validator\Context;

/**
 * Creates new {@link ExecutionContext} instances.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal version 2.5. Code against ExecutionContextFactoryInterface instead.
 */
class ExecutionContextFactory implements \Symfony\Component\Validator\Context\ExecutionContextFactoryInterface
{
    /**
     * Creates a new context factory.
     *
     * @param TranslatorInterface $translator        The translator
     * @param string|null         $translationDomain The translation domain to
     *                                               use for translating
     *                                               violation messages
     */
    public function __construct($translator, string $translationDomain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createContext(\Symfony\Component\Validator\Validator\ValidatorInterface $validator, $root)
    {
    }
}
