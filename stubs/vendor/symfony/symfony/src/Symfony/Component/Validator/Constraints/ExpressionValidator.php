<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bernhard Schussek <bschussek@symfony.com>
 */
class ExpressionValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * @param ExpressionLanguage|null $expressionLanguage
     */
    public function __construct($expressionLanguage = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
