<?php

namespace Symfony\Component\Validator\Exception;

/**
 * Base RuntimeException for the Validator component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Validator\Exception\ExceptionInterface
{
}
