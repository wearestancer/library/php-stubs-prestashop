<?php

namespace Symfony\Component\Validator;

/**
 * Contains the properties of a constraint definition.
 *
 * A constraint can be defined on a class, a property or a getter method.
 * The Constraint class encapsulates all the configuration required for
 * validating this class, property or getter result successfully.
 *
 * Constraint instances are immutable and serializable.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class Constraint
{
    /**
     * The name of the group given to all constraints with no explicit group.
     */
    public const DEFAULT_GROUP = 'Default';
    /**
     * Marks a constraint that can be put onto classes.
     */
    public const CLASS_CONSTRAINT = 'class';
    /**
     * Marks a constraint that can be put onto properties.
     */
    public const PROPERTY_CONSTRAINT = 'property';
    /**
     * Maps error codes to the names of their constants.
     */
    protected static $errorNames = [];
    /**
     * Domain-specific data attached to a constraint.
     *
     * @var mixed
     */
    public $payload;
    /**
     * The groups that the constraint belongs to.
     *
     * @var string[]
     */
    public $groups;
    /**
     * Returns the name of the given error code.
     *
     * @param string $errorCode The error code
     *
     * @return string The name of the error code
     *
     * @throws InvalidArgumentException If the error code does not exist
     */
    public static function getErrorName($errorCode)
    {
    }
    /**
     * Initializes the constraint with options.
     *
     * You should pass an associative array. The keys should be the names of
     * existing properties in this class. The values should be the value for these
     * properties.
     *
     * Alternatively you can override the method getDefaultOption() to return the
     * name of an existing property. If no associative array is passed, this
     * property is set instead.
     *
     * You can force that certain options are set by overriding
     * getRequiredOptions() to return the names of these options. If any
     * option is not set here, an exception is thrown.
     *
     * @param mixed $options The options (as associative array)
     *                       or the value for the default
     *                       option (any other type)
     *
     * @throws InvalidOptionsException       When you pass the names of non-existing
     *                                       options
     * @throws MissingOptionsException       When you don't pass any of the options
     *                                       returned by getRequiredOptions()
     * @throws ConstraintDefinitionException When you don't pass an associative
     *                                       array, but getDefaultOption() returns
     *                                       null
     */
    public function __construct($options = null)
    {
    }
    /**
     * Sets the value of a lazily initialized option.
     *
     * Corresponding properties are added to the object on first access. Hence
     * this method will be called at most once per constraint instance and
     * option name.
     *
     * @param string $option The option name
     * @param mixed  $value  The value to set
     *
     * @throws InvalidOptionsException If an invalid option name is given
     */
    public function __set($option, $value)
    {
    }
    /**
     * Returns the value of a lazily initialized option.
     *
     * Corresponding properties are added to the object on first access. Hence
     * this method will be called at most once per constraint instance and
     * option name.
     *
     * @param string $option The option name
     *
     * @return mixed The value of the option
     *
     * @throws InvalidOptionsException If an invalid option name is given
     *
     * @internal this method should not be used or overwritten in userland code
     */
    public function __get($option)
    {
    }
    /**
     * @param string $option The option name
     *
     * @return bool
     */
    public function __isset($option)
    {
    }
    /**
     * Adds the given group if this constraint is in the Default group.
     *
     * @param string $group
     */
    public function addImplicitGroupName($group)
    {
    }
    /**
     * Returns the name of the default option.
     *
     * Override this method to define a default option.
     *
     * @return string|null
     *
     * @see __construct()
     */
    public function getDefaultOption()
    {
    }
    /**
     * Returns the name of the required options.
     *
     * Override this method if you want to define required options.
     *
     * @return string[]
     *
     * @see __construct()
     */
    public function getRequiredOptions()
    {
    }
    /**
     * Returns the name of the class that validates this constraint.
     *
     * By default, this is the fully qualified name of the constraint class
     * suffixed with "Validator". You can override this method to change that
     * behavior.
     *
     * @return string
     */
    public function validatedBy()
    {
    }
    /**
     * Returns whether the constraint can be put onto classes, properties or
     * both.
     *
     * This method should return one or more of the constants
     * Constraint::CLASS_CONSTRAINT and Constraint::PROPERTY_CONSTRAINT.
     *
     * @return string|string[] One or more constant values
     */
    public function getTargets()
    {
    }
    /**
     * Optimizes the serialized value to minimize storage space.
     *
     * @return array
     *
     * @internal
     */
    public function __sleep()
    {
    }
}
