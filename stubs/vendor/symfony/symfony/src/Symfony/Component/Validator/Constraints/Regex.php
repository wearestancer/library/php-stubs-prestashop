<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Regex extends \Symfony\Component\Validator\Constraint
{
    public const REGEX_FAILED_ERROR = 'de1e3db3-5ed4-4941-aae4-59f3667cc3a3';
    protected static $errorNames = [self::REGEX_FAILED_ERROR => 'REGEX_FAILED_ERROR'];
    public $message = 'This value is not valid.';
    public $pattern;
    public $htmlPattern;
    public $match = true;
    public $normalizer;
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
    }
    /**
     * Converts the htmlPattern to a suitable format for HTML5 pattern.
     * Example: /^[a-z]+$/ would be converted to [a-z]+
     * However, if options are specified, it cannot be converted.
     *
     * @see http://dev.w3.org/html5/spec/single-page.html#the-pattern-attribute
     *
     * @return string|null
     */
    public function getHtmlPattern()
    {
    }
}
