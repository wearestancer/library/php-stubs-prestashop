<?php

namespace Symfony\Component\Validator\Exception;

class GroupDefinitionException extends \Symfony\Component\Validator\Exception\ValidatorException
{
}
