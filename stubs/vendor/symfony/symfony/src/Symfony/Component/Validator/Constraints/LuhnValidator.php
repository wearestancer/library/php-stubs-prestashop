<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Validates a PAN using the LUHN Algorithm.
 *
 * For a list of example card numbers that are used to test this
 * class, please see the LuhnValidatorTest class.
 *
 * @see    http://en.wikipedia.org/wiki/Luhn_algorithm
 *
 * @author Tim Nagel <t.nagel@infinite.net.au>
 * @author Greg Knapp http://gregk.me/2011/php-implementation-of-bank-card-luhn-algorithm/
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class LuhnValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * Validates a credit card number with the Luhn algorithm.
     *
     * @param mixed $value
     *
     * @throws UnexpectedTypeException when the given credit card number is no string
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
