<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IsFalse extends \Symfony\Component\Validator\Constraint
{
    public const NOT_FALSE_ERROR = 'd53a91b0-def3-426a-83d7-269da7ab4200';
    protected static $errorNames = [self::NOT_FALSE_ERROR => 'NOT_FALSE_ERROR'];
    public $message = 'This value should be false.';
}
