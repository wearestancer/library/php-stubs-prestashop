<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class LessThan extends \Symfony\Component\Validator\Constraints\AbstractComparison
{
    public const TOO_HIGH_ERROR = '079d7420-2d13-460c-8756-de810eeb37d2';
    protected static $errorNames = [self::TOO_HIGH_ERROR => 'TOO_HIGH_ERROR'];
    public $message = 'This value should be less than {{ compared_value }}.';
}
