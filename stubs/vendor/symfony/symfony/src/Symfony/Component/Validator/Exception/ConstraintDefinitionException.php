<?php

namespace Symfony\Component\Validator\Exception;

class ConstraintDefinitionException extends \Symfony\Component\Validator\Exception\ValidatorException
{
}
