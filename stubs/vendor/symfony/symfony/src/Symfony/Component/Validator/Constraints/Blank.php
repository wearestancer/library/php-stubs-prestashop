<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Blank extends \Symfony\Component\Validator\Constraint
{
    public const NOT_BLANK_ERROR = '183ad2de-533d-4796-a439-6d3c3852b549';
    protected static $errorNames = [self::NOT_BLANK_ERROR => 'NOT_BLANK_ERROR'];
    public $message = 'This value should be blank.';
}
