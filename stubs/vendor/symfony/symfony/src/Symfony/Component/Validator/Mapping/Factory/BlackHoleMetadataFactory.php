<?php

namespace Symfony\Component\Validator\Mapping\Factory;

/**
 * Metadata factory that does not store metadata.
 *
 * This implementation is useful if you want to validate values against
 * constraints only and you don't need to add constraints to classes and
 * properties.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class BlackHoleMetadataFactory implements \Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($value)
    {
    }
}
