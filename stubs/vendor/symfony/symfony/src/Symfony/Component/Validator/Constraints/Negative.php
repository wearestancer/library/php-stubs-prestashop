<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Jan Schädlich <jan.schaedlich@sensiolabs.de>
 */
class Negative extends \Symfony\Component\Validator\Constraints\LessThan
{
    use \Symfony\Component\Validator\Constraints\NumberConstraintTrait;
    public $message = 'This value should be negative.';
    public function __construct($options = null)
    {
    }
    public function validatedBy() : string
    {
    }
}
