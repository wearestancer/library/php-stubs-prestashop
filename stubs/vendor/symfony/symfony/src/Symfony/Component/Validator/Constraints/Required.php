<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Required extends \Symfony\Component\Validator\Constraints\Existence
{
}
