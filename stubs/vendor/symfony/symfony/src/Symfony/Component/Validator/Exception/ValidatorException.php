<?php

namespace Symfony\Component\Validator\Exception;

class ValidatorException extends \Symfony\Component\Validator\Exception\RuntimeException
{
}
