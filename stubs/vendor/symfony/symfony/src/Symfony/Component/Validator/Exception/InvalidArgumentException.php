<?php

namespace Symfony\Component\Validator\Exception;

/**
 * Base InvalidArgumentException for the Validator component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Validator\Exception\ExceptionInterface
{
}
