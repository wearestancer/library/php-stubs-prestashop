<?php

namespace Symfony\Component\Validator;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
interface ConstraintValidatorInterface
{
    /**
     * Initializes the constraint validator.
     */
    public function initialize(\Symfony\Component\Validator\Context\ExecutionContextInterface $context);
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint);
}
