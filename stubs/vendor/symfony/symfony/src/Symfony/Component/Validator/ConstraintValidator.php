<?php

namespace Symfony\Component\Validator;

/**
 * Base class for constraint validators.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class ConstraintValidator implements \Symfony\Component\Validator\ConstraintValidatorInterface
{
    /**
     * Whether to format {@link \DateTime} objects, either with the {@link \IntlDateFormatter}
     * (if it is available) or as RFC-3339 dates ("Y-m-d H:i:s").
     */
    public const PRETTY_DATE = 1;
    /**
     * Whether to cast objects with a "__toString()" method to strings.
     */
    public const OBJECT_TO_STRING = 2;
    /**
     * @var ExecutionContextInterface
     */
    protected $context;
    /**
     * {@inheritdoc}
     */
    public function initialize(\Symfony\Component\Validator\Context\ExecutionContextInterface $context)
    {
    }
    /**
     * Returns a string representation of the type of the value.
     *
     * This method should be used if you pass the type of a value as
     * message parameter to a constraint violation. Note that such
     * parameters should usually not be included in messages aimed at
     * non-technical people.
     *
     * @param mixed $value The value to return the type of
     *
     * @return string The type of the value
     */
    protected function formatTypeOf($value)
    {
    }
    /**
     * Returns a string representation of the value.
     *
     * This method returns the equivalent PHP tokens for most scalar types
     * (i.e. "false" for false, "1" for 1 etc.). Strings are always wrapped
     * in double quotes ("). Objects, arrays and resources are formatted as
     * "object", "array" and "resource". If the $format bitmask contains
     * the PRETTY_DATE bit, then {@link \DateTime} objects will be formatted
     * with the {@link \IntlDateFormatter}. If it is not available, they will be
     * formatted as RFC-3339 dates ("Y-m-d H:i:s").
     *
     * Be careful when passing message parameters to a constraint violation
     * that (may) contain objects, arrays or resources. These parameters
     * should only be displayed for technical users. Non-technical users
     * won't know what an "object", "array" or "resource" is and will be
     * confused by the violation message.
     *
     * @param mixed $value  The value to format as string
     * @param int   $format A bitwise combination of the format
     *                      constants in this class
     *
     * @return string The string representation of the passed value
     */
    protected function formatValue($value, $format = 0)
    {
    }
    /**
     * Returns a string representation of a list of values.
     *
     * Each of the values is converted to a string using
     * {@link formatValue()}. The values are then concatenated with commas.
     *
     * @param array $values A list of values
     * @param int   $format A bitwise combination of the format
     *                      constants in this class
     *
     * @return string The string representation of the value list
     *
     * @see formatValue()
     */
    protected function formatValues(array $values, $format = 0)
    {
    }
}
