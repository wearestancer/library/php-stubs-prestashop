<?php

namespace Symfony\Component\Validator\Mapping;

/**
 * Stores all metadata needed for validating a class property via its getter
 * method.
 *
 * A property getter is any method that is equal to the property's name,
 * prefixed with "get", "is" or "has". That method will be used to access the
 * property's value.
 *
 * The getter will be invoked by reflection, so the access of private and
 * protected getters is supported.
 *
 * This class supports serialization and cloning.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see PropertyMetadataInterface
 */
class GetterMetadata extends \Symfony\Component\Validator\Mapping\MemberMetadata
{
    /**
     * @param string      $class    The class the getter is defined on
     * @param string      $property The property which the getter returns
     * @param string|null $method   The method that is called to retrieve the value being validated (null for auto-detection)
     *
     * @throws ValidatorException
     */
    public function __construct(string $class, string $property, string $method = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyValue($object)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function newReflectionMember($objectOrClassName)
    {
    }
}
