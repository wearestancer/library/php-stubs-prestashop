<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Metadata for the CardSchemeValidator.
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Tim Nagel <t.nagel@infinite.net.au>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class CardScheme extends \Symfony\Component\Validator\Constraint
{
    public const NOT_NUMERIC_ERROR = 'a2ad9231-e827-485f-8a1e-ef4d9a6d5c2e';
    public const INVALID_FORMAT_ERROR = 'a8faedbf-1c2f-4695-8d22-55783be8efed';
    protected static $errorNames = [self::NOT_NUMERIC_ERROR => 'NOT_NUMERIC_ERROR', self::INVALID_FORMAT_ERROR => 'INVALID_FORMAT_ERROR'];
    public $message = 'Unsupported card type or invalid card number.';
    public $schemes;
    public function getDefaultOption()
    {
    }
    public function getRequiredOptions()
    {
    }
}
