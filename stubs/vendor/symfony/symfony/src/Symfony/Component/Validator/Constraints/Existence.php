<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class Existence extends \Symfony\Component\Validator\Constraints\Composite
{
    public $constraints = [];
    public function getDefaultOption()
    {
    }
    protected function getCompositeOption()
    {
    }
}
