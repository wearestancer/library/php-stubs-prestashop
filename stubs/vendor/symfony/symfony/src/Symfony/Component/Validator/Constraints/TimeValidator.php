<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class TimeValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public const PATTERN = '/^(\\d{2}):(\\d{2}):(\\d{2})$/';
    /**
     * Checks whether a time is valid.
     *
     * @internal
     */
    public static function checkTime(int $hour, int $minute, float $second) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
