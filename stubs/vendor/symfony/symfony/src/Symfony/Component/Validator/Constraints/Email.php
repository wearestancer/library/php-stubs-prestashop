<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Email extends \Symfony\Component\Validator\Constraint
{
    public const VALIDATION_MODE_HTML5 = 'html5';
    public const VALIDATION_MODE_STRICT = 'strict';
    public const VALIDATION_MODE_LOOSE = 'loose';
    public const INVALID_FORMAT_ERROR = 'bd79c0ab-ddba-46cc-a703-a7a4b08de310';
    /**
     * @deprecated since Symfony 4.2.
     */
    public const MX_CHECK_FAILED_ERROR = 'bf447c1c-0266-4e10-9c6c-573df282e413';
    /**
     * @deprecated since Symfony 4.2.
     */
    public const HOST_CHECK_FAILED_ERROR = '7da53a8b-56f3-4288-bb3e-ee9ede4ef9a1';
    protected static $errorNames = [self::INVALID_FORMAT_ERROR => 'STRICT_CHECK_FAILED_ERROR', self::MX_CHECK_FAILED_ERROR => 'MX_CHECK_FAILED_ERROR', self::HOST_CHECK_FAILED_ERROR => 'HOST_CHECK_FAILED_ERROR'];
    /**
     * @var string[]
     *
     * @internal
     */
    public static $validationModes = [self::VALIDATION_MODE_HTML5, self::VALIDATION_MODE_STRICT, self::VALIDATION_MODE_LOOSE];
    public $message = 'This value is not a valid email address.';
    /**
     * @deprecated since Symfony 4.2.
     */
    public $checkMX = false;
    /**
     * @deprecated since Symfony 4.2.
     */
    public $checkHost = false;
    /**
     * @deprecated since Symfony 4.1, set mode to "strict" instead.
     */
    public $strict;
    public $mode;
    public $normalizer;
    public function __construct($options = null)
    {
    }
}
