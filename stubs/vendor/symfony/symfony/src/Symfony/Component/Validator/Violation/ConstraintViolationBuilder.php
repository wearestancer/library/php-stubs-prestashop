<?php

namespace Symfony\Component\Validator\Violation;

/**
 * Default implementation of {@link ConstraintViolationBuilderInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal since version 2.5. Code against ConstraintViolationBuilderInterface instead.
 */
class ConstraintViolationBuilder implements \Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface
{
    /**
     * @param string|object       $message    The error message as a string or a stringable object
     * @param TranslatorInterface $translator
     */
    public function __construct(\Symfony\Component\Validator\ConstraintViolationList $violations, \Symfony\Component\Validator\Constraint $constraint, $message, array $parameters, $root, string $propertyPath, $invalidValue, $translator, string $translationDomain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function atPath($path)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setParameter($key, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setParameters(array $parameters)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setTranslationDomain($translationDomain)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setInvalidValue($invalidValue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setPlural($number)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setCode($code)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setCause($cause)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addViolation()
    {
    }
}
