<?php

namespace Symfony\Component\Validator\Exception;

class UnexpectedTypeException extends \Symfony\Component\Validator\Exception\ValidatorException
{
    public function __construct($value, string $expectedType)
    {
    }
}
