<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Validates values aren't identical (!==).
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class NotIdenticalToValidator extends \Symfony\Component\Validator\Constraints\AbstractComparisonValidator
{
    /**
     * {@inheritdoc}
     */
    protected function compareValues($value1, $value2)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getErrorCode()
    {
    }
}
