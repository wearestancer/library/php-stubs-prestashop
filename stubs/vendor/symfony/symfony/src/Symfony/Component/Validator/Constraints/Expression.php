<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Expression extends \Symfony\Component\Validator\Constraint
{
    public const EXPRESSION_FAILED_ERROR = '6b3befbc-2f01-4ddf-be21-b57898905284';
    protected static $errorNames = [self::EXPRESSION_FAILED_ERROR => 'EXPRESSION_FAILED_ERROR'];
    public $message = 'This value is not valid.';
    public $expression;
    public $values = [];
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
    }
}
