<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * Disables auto mapping.
 *
 * Using the annotations on a property has higher precedence than using it on a class,
 * which has higher precedence than any configuration that might be defined outside the class.
 *
 * @Annotation
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class DisableAutoMapping extends \Symfony\Component\Validator\Constraint
{
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
    }
}
