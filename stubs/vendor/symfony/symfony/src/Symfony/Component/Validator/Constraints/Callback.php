<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Callback extends \Symfony\Component\Validator\Constraint
{
    /**
     * @var string|callable
     */
    public $callback;
    /**
     * {@inheritdoc}
     */
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
    }
}
