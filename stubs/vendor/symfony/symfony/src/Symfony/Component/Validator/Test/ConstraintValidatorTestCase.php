<?php

namespace Symfony\Component\Validator\Test;

/**
 * A test case to ease testing Constraint Validators.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
abstract class ConstraintValidatorTestCase extends \PHPUnit\Framework\TestCase
{
    use \Symfony\Component\Validator\Test\ForwardCompatTestTrait;
    /**
     * @var ExecutionContextInterface
     */
    protected $context;
    /**
     * @var ConstraintValidatorInterface
     */
    protected $validator;
    protected $group;
    protected $metadata;
    protected $object;
    protected $value;
    protected $root;
    protected $propertyPath;
    protected $constraint;
    protected $defaultTimezone;
    protected function setDefaultTimezone($defaultTimezone)
    {
    }
    protected function restoreDefaultTimezone()
    {
    }
    protected function createContext()
    {
    }
    protected function setGroup($group)
    {
    }
    protected function setObject($object)
    {
    }
    protected function setProperty($object, $property)
    {
    }
    protected function setValue($value)
    {
    }
    protected function setRoot($root)
    {
    }
    protected function setPropertyPath($propertyPath)
    {
    }
    protected function expectNoValidate()
    {
    }
    protected function expectValidateAt($i, $propertyPath, $value, $group)
    {
    }
    protected function expectValidateValueAt($i, $propertyPath, $value, $constraints, $group = null)
    {
    }
    protected function assertNoViolation()
    {
    }
    /**
     * @return ConstraintViolationAssertion
     */
    protected function buildViolation($message)
    {
    }
    protected abstract function createValidator();
}
/**
 * @internal
 */
class ConstraintViolationAssertion
{
    public function __construct(\Symfony\Component\Validator\Context\ExecutionContextInterface $context, string $message, \Symfony\Component\Validator\Constraint $constraint = null, array $assertions = [])
    {
    }
    public function atPath(string $path)
    {
    }
    public function setParameter(string $key, $value)
    {
    }
    public function setParameters(array $parameters)
    {
    }
    public function setTranslationDomain($translationDomain)
    {
    }
    public function setInvalidValue($invalidValue)
    {
    }
    public function setPlural(int $number)
    {
    }
    public function setCode(string $code)
    {
    }
    public function setCause($cause)
    {
    }
    public function buildNextViolation(string $message) : self
    {
    }
    public function assertRaised()
    {
    }
}
/**
 * @internal
 */
class AssertingContextualValidator implements \Symfony\Component\Validator\Validator\ContextualValidatorInterface
{
    public function __destruct()
    {
    }
    public function atPath($path)
    {
    }
    public function doAtPath($path)
    {
    }
    public function validate($value, $constraints = null, $groups = null)
    {
    }
    public function doValidate($value, $constraints = null, $groups = null)
    {
    }
    public function validateProperty($object, $propertyName, $groups = null)
    {
    }
    public function doValidateProperty($object, $propertyName, $groups = null)
    {
    }
    public function validatePropertyValue($objectOrClass, $propertyName, $value, $groups = null)
    {
    }
    public function doValidatePropertyValue($objectOrClass, $propertyName, $value, $groups = null)
    {
    }
    public function getViolations()
    {
    }
    public function doGetViolations()
    {
    }
    public function expectNoValidate()
    {
    }
    public function expectValidation($call, $propertyPath, $value, $group, $constraints)
    {
    }
}
