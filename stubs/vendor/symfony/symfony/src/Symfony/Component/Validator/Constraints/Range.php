<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Range extends \Symfony\Component\Validator\Constraint
{
    public const INVALID_CHARACTERS_ERROR = 'ad9a9798-7a99-4df7-8ce9-46e416a1e60b';
    public const NOT_IN_RANGE_ERROR = '04b91c99-a946-4221-afc5-e65ebac401eb';
    public const TOO_HIGH_ERROR = '2d28afcb-e32e-45fb-a815-01c431a86a69';
    public const TOO_LOW_ERROR = '76454e69-502c-46c5-9643-f447d837c4d5';
    protected static $errorNames = [self::INVALID_CHARACTERS_ERROR => 'INVALID_CHARACTERS_ERROR', self::NOT_IN_RANGE_ERROR => 'NOT_IN_RANGE_ERROR', self::TOO_HIGH_ERROR => 'TOO_HIGH_ERROR', self::TOO_LOW_ERROR => 'TOO_LOW_ERROR'];
    public $notInRangeMessage = 'This value should be between {{ min }} and {{ max }}.';
    public $minMessage = 'This value should be {{ limit }} or more.';
    public $maxMessage = 'This value should be {{ limit }} or less.';
    public $invalidMessage = 'This value should be a valid number.';
    public $min;
    public $minPropertyPath;
    public $max;
    public $maxPropertyPath;
    // BC layer, to be removed in 5.0
    /**
     * @internal
     */
    public $deprecatedMinMessageSet = false;
    /**
     * @internal
     */
    public $deprecatedMaxMessageSet = false;
    public function __construct($options = null)
    {
    }
}
