<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Loads validation metadata by calling a static method on the loaded class.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class StaticMethodLoader implements \Symfony\Component\Validator\Mapping\Loader\LoaderInterface
{
    protected $methodName;
    /**
     * Creates a new loader.
     *
     * @param string $methodName The name of the static method to call
     */
    public function __construct(string $methodName = 'loadValidatorMetadata')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
    }
}
