<?php

namespace Symfony\Component\Validator;

/**
 * Default implementation of the ConstraintValidatorFactoryInterface.
 *
 * This enforces the convention that the validatedBy() method on any
 * Constraint will return the class name of the ConstraintValidator that
 * should validate the Constraint.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ConstraintValidatorFactory implements \Symfony\Component\Validator\ConstraintValidatorFactoryInterface
{
    protected $validators = [];
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInstance(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
