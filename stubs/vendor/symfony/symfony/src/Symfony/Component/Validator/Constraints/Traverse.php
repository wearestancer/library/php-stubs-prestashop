<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Traverse extends \Symfony\Component\Validator\Constraint
{
    public $traverse = true;
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
    }
}
