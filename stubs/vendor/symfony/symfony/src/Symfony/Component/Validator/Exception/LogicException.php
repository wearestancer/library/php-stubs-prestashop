<?php

namespace Symfony\Component\Validator\Exception;

class LogicException extends \LogicException implements \Symfony\Component\Validator\Exception\ExceptionInterface
{
}
