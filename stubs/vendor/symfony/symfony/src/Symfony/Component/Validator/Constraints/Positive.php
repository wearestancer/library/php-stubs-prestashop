<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Jan Schädlich <jan.schaedlich@sensiolabs.de>
 */
class Positive extends \Symfony\Component\Validator\Constraints\GreaterThan
{
    use \Symfony\Component\Validator\Constraints\NumberConstraintTrait;
    public $message = 'This value should be positive.';
    public function __construct($options = null)
    {
    }
    public function validatedBy() : string
    {
    }
}
