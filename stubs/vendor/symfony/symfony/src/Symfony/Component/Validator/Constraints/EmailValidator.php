<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class EmailValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * @internal
     */
    public const PATTERN_HTML5 = '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/';
    /**
     * @internal
     */
    public const PATTERN_LOOSE = '/^.+\\@\\S+\\.\\S+$/';
    /**
     * @param string $defaultMode
     */
    public function __construct($defaultMode = \Symfony\Component\Validator\Constraints\Email::VALIDATION_MODE_LOOSE)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
