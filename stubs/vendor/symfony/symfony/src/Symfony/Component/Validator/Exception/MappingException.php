<?php

namespace Symfony\Component\Validator\Exception;

class MappingException extends \Symfony\Component\Validator\Exception\ValidatorException
{
}
