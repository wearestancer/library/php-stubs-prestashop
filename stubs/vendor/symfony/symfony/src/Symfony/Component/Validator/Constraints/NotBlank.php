<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class NotBlank extends \Symfony\Component\Validator\Constraint
{
    public const IS_BLANK_ERROR = 'c1051bb4-d103-4f74-8988-acbcafc7fdc3';
    protected static $errorNames = [self::IS_BLANK_ERROR => 'IS_BLANK_ERROR'];
    public $message = 'This value should not be blank.';
    public $allowNull = false;
    public $normalizer;
    public function __construct($options = null)
    {
    }
}
