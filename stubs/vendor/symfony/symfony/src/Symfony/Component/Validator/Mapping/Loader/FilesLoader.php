<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Base loader for loading validation metadata from a list of files.
 *
 * @author Bulat Shakirzyanov <mallluhuct@gmail.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see YamlFilesLoader
 * @see XmlFilesLoader
 */
abstract class FilesLoader extends \Symfony\Component\Validator\Mapping\Loader\LoaderChain
{
    /**
     * Creates a new loader.
     *
     * @param array $paths An array of file paths
     */
    public function __construct(array $paths)
    {
    }
    /**
     * Returns an array of file loaders for the given file paths.
     *
     * @param array $paths An array of file paths
     *
     * @return LoaderInterface[] The metadata loaders
     */
    protected function getFileLoaders($paths)
    {
    }
    /**
     * Creates a loader for the given file path.
     *
     * @param string $path The file path
     *
     * @return LoaderInterface The created loader
     */
    protected abstract function getFileLoaderInstance($path);
}
