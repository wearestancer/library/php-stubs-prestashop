<?php

namespace Symfony\Component\Validator\Util;

/**
 * @internal to be removed in Symfony 5.0.
 */
class LegacyTranslatorProxy implements \Symfony\Component\Translation\TranslatorInterface, \Symfony\Contracts\Translation\TranslatorInterface
{
    /**
     * @param LegacyTranslatorInterface|TranslatorInterface $translator
     */
    public function __construct($translator)
    {
    }
    /**
     * @return LegacyTranslatorInterface|TranslatorInterface
     */
    public function getTranslator()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocale() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null) : string
    {
    }
}
