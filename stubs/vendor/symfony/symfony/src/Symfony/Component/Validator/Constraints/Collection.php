<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Collection extends \Symfony\Component\Validator\Constraints\Composite
{
    public const MISSING_FIELD_ERROR = '2fa2158c-2a7f-484b-98aa-975522539ff8';
    public const NO_SUCH_FIELD_ERROR = '7703c766-b5d5-4cef-ace7-ae0dd82304e9';
    protected static $errorNames = [self::MISSING_FIELD_ERROR => 'MISSING_FIELD_ERROR', self::NO_SUCH_FIELD_ERROR => 'NO_SUCH_FIELD_ERROR'];
    public $fields = [];
    public $allowExtraFields = false;
    public $allowMissingFields = false;
    public $extraFieldsMessage = 'This field was not expected.';
    public $missingFieldsMessage = 'This field is missing.';
    /**
     * {@inheritdoc}
     */
    public function __construct($options = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initializeNestedConstraints()
    {
    }
    public function getRequiredOptions()
    {
    }
    protected function getCompositeOption()
    {
    }
}
