<?php

namespace Symfony\Component\Validator\Validator;

/**
 * A wrapper for a callable initializing a property from a getter.
 *
 * @internal
 */
class LazyProperty
{
    public function __construct(\Closure $propertyValueCallback)
    {
    }
    public function getPropertyValue()
    {
    }
}
