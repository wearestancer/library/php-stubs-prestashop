<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Base loader for loading validation metadata from a file.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see YamlFileLoader
 * @see XmlFileLoader
 */
abstract class FileLoader extends \Symfony\Component\Validator\Mapping\Loader\AbstractLoader
{
    protected $file;
    /**
     * Creates a new loader.
     *
     * @param string $file The mapping file to load
     *
     * @throws MappingException If the file does not exist or is not readable
     */
    public function __construct(string $file)
    {
    }
}
