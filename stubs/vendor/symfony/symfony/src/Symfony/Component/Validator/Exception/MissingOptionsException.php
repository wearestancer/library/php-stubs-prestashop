<?php

namespace Symfony\Component\Validator\Exception;

class MissingOptionsException extends \Symfony\Component\Validator\Exception\ValidatorException
{
    public function __construct(string $message, array $options)
    {
    }
    public function getOptions()
    {
    }
}
