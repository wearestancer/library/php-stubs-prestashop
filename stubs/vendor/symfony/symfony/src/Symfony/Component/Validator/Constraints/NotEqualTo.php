<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class NotEqualTo extends \Symfony\Component\Validator\Constraints\AbstractComparison
{
    public const IS_EQUAL_ERROR = 'aa2e33da-25c8-4d76-8c6c-812f02ea89dd';
    protected static $errorNames = [self::IS_EQUAL_ERROR => 'IS_EQUAL_ERROR'];
    public $message = 'This value should not be equal to {{ compared_value }}.';
}
