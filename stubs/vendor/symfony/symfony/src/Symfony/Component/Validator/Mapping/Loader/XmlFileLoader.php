<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Loads validation metadata from an XML file.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class XmlFileLoader extends \Symfony\Component\Validator\Mapping\Loader\FileLoader
{
    /**
     * The XML nodes of the mapping file.
     *
     * @var \SimpleXMLElement[]|null
     */
    protected $classes;
    /**
     * {@inheritdoc}
     */
    public function loadClassMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
    }
    /**
     * Return the names of the classes mapped in this file.
     *
     * @return string[] The classes names
     */
    public function getMappedClasses()
    {
    }
    /**
     * Parses a collection of "constraint" XML nodes.
     *
     * @param \SimpleXMLElement $nodes The XML nodes
     *
     * @return array The Constraint instances
     */
    protected function parseConstraints(\SimpleXMLElement $nodes)
    {
    }
    /**
     * Parses a collection of "value" XML nodes.
     *
     * @param \SimpleXMLElement $nodes The XML nodes
     *
     * @return array The values
     */
    protected function parseValues(\SimpleXMLElement $nodes)
    {
    }
    /**
     * Parses a collection of "option" XML nodes.
     *
     * @param \SimpleXMLElement $nodes The XML nodes
     *
     * @return array The options
     */
    protected function parseOptions(\SimpleXMLElement $nodes)
    {
    }
    /**
     * Loads the XML class descriptions from the given file.
     *
     * @param string $path The path of the XML file
     *
     * @return \SimpleXMLElement The class descriptions
     *
     * @throws MappingException If the file could not be loaded
     */
    protected function parseFile($path)
    {
    }
}
