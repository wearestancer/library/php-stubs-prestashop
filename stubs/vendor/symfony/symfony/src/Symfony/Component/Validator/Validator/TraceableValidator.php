<?php

namespace Symfony\Component\Validator\Validator;

/**
 * Collects some data about validator calls.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class TraceableValidator implements \Symfony\Component\Validator\Validator\ValidatorInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(\Symfony\Component\Validator\Validator\ValidatorInterface $validator)
    {
    }
    /**
     * @return array
     */
    public function getCollectedData()
    {
    }
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, $constraints = null, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateProperty($object, $propertyName, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validatePropertyValue($objectOrClass, $propertyName, $value, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function startContext()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function inContext(\Symfony\Component\Validator\Context\ExecutionContextInterface $context)
    {
    }
}
