<?php

namespace Symfony\Component\Validator\DependencyInjection;

/**
 * Injects the automapping configuration as last argument of loaders tagged with the "validator.auto_mapper" tag.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class AddAutoMappingConfigurationPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $validatorBuilderService = 'validator.builder', string $tag = 'validator.auto_mapper')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
