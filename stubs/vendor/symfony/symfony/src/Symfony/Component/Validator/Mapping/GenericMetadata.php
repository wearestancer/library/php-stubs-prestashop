<?php

namespace Symfony\Component\Validator\Mapping;

/**
 * A generic container of {@link Constraint} objects.
 *
 * This class supports serialization and cloning.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class GenericMetadata implements \Symfony\Component\Validator\Mapping\MetadataInterface
{
    /**
     * @var Constraint[]
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getConstraints()} and {@link findConstraints()} instead.
     */
    public $constraints = [];
    /**
     * @var array
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link findConstraints()} instead.
     */
    public $constraintsByGroup = [];
    /**
     * The strategy for cascading objects.
     *
     * By default, objects are not cascaded.
     *
     * @var int
     *
     * @see CascadingStrategy
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getCascadingStrategy()} instead.
     */
    public $cascadingStrategy = \Symfony\Component\Validator\Mapping\CascadingStrategy::NONE;
    /**
     * The strategy for traversing traversable objects.
     *
     * By default, traversable objects are not traversed.
     *
     * @var int
     *
     * @see TraversalStrategy
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getTraversalStrategy()} instead.
     */
    public $traversalStrategy = \Symfony\Component\Validator\Mapping\TraversalStrategy::NONE;
    /**
     * Is auto-mapping enabled?
     *
     * @var int
     *
     * @see AutoMappingStrategy
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getAutoMappingStrategy()} instead.
     */
    public $autoMappingStrategy = \Symfony\Component\Validator\Mapping\AutoMappingStrategy::NONE;
    /**
     * Returns the names of the properties that should be serialized.
     *
     * @return string[]
     */
    public function __sleep()
    {
    }
    /**
     * Clones this object.
     */
    public function __clone()
    {
    }
    /**
     * Adds a constraint.
     *
     * If the constraint {@link Valid} is added, the cascading strategy will be
     * changed to {@link CascadingStrategy::CASCADE}. Depending on the
     * $traverse property of that constraint, the traversal strategy
     * will be set to one of the following:
     *
     *  - {@link TraversalStrategy::IMPLICIT} if $traverse is enabled
     *  - {@link TraversalStrategy::NONE} if $traverse is disabled
     *
     * @return $this
     *
     * @throws ConstraintDefinitionException When trying to add the
     *                                       {@link Traverse} constraint
     */
    public function addConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Adds an list of constraints.
     *
     * @param Constraint[] $constraints The constraints to add
     *
     * @return $this
     */
    public function addConstraints(array $constraints)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConstraints()
    {
    }
    /**
     * Returns whether this element has any constraints.
     *
     * @return bool
     */
    public function hasConstraints()
    {
    }
    /**
     * {@inheritdoc}
     *
     * Aware of the global group (* group).
     */
    public function findConstraints($group)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCascadingStrategy()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTraversalStrategy()
    {
    }
    /**
     * @see AutoMappingStrategy
     */
    public function getAutoMappingStrategy() : int
    {
    }
}
