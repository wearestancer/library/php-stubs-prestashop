<?php

namespace Symfony\Component\Validator\Validator;

/**
 * Recursive implementation of {@link ValidatorInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RecursiveValidator implements \Symfony\Component\Validator\Validator\ValidatorInterface
{
    protected $contextFactory;
    protected $metadataFactory;
    protected $validatorFactory;
    protected $objectInitializers;
    /**
     * Creates a new validator.
     *
     * @param ObjectInitializerInterface[] $objectInitializers The object initializers
     */
    public function __construct(\Symfony\Component\Validator\Context\ExecutionContextFactoryInterface $contextFactory, \Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface $metadataFactory, \Symfony\Component\Validator\ConstraintValidatorFactoryInterface $validatorFactory, array $objectInitializers = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function startContext($root = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function inContext(\Symfony\Component\Validator\Context\ExecutionContextInterface $context)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($object)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($object)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, $constraints = null, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validateProperty($object, $propertyName, $groups = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validatePropertyValue($objectOrClass, $propertyName, $value, $groups = null)
    {
    }
}
