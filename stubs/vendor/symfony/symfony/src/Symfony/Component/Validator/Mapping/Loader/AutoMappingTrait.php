<?php

namespace Symfony\Component\Validator\Mapping\Loader;

/**
 * Utility methods to create auto mapping loaders.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait AutoMappingTrait
{
    private function isAutoMappingEnabledForClass(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata, string $classValidatorRegexp = null) : bool
    {
    }
}
