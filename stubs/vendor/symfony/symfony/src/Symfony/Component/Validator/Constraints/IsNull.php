<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IsNull extends \Symfony\Component\Validator\Constraint
{
    public const NOT_NULL_ERROR = '60d2f30b-8cfa-4372-b155-9656634de120';
    protected static $errorNames = [self::NOT_NULL_ERROR => 'NOT_NULL_ERROR'];
    public $message = 'This value should be null.';
}
