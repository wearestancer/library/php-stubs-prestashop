<?php

namespace Symfony\Component\Validator\Mapping;

/**
 * Default implementation of {@link ClassMetadataInterface}.
 *
 * This class supports serialization and cloning.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ClassMetadata extends \Symfony\Component\Validator\Mapping\GenericMetadata implements \Symfony\Component\Validator\Mapping\ClassMetadataInterface
{
    /**
     * @var string
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getClassName()} instead.
     */
    public $name;
    /**
     * @var string
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getDefaultGroup()} instead.
     */
    public $defaultGroup;
    /**
     * @var MemberMetadata[][]
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getPropertyMetadata()} instead.
     */
    public $members = [];
    /**
     * @var PropertyMetadata[]
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getPropertyMetadata()} instead.
     */
    public $properties = [];
    /**
     * @var GetterMetadata[]
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getPropertyMetadata()} instead.
     */
    public $getters = [];
    /**
     * @var array
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getGroupSequence()} instead.
     */
    public $groupSequence = [];
    /**
     * @var bool
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link isGroupSequenceProvider()} instead.
     */
    public $groupSequenceProvider = false;
    /**
     * The strategy for traversing traversable objects.
     *
     * By default, only instances of {@link \Traversable} are traversed.
     *
     * @var int
     *
     * @internal This property is public in order to reduce the size of the
     *           class' serialized representation. Do not access it. Use
     *           {@link getTraversalStrategy()} instead.
     */
    public $traversalStrategy = \Symfony\Component\Validator\Mapping\TraversalStrategy::IMPLICIT;
    public function __construct(string $class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __sleep()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClassName()
    {
    }
    /**
     * Returns the name of the default group for this class.
     *
     * For each class, the group "Default" is an alias for the group
     * "<ClassName>", where <ClassName> is the non-namespaced name of the
     * class. All constraints implicitly or explicitly assigned to group
     * "Default" belong to both of these groups, unless the class defines
     * a group sequence.
     *
     * If a class defines a group sequence, validating the class in "Default"
     * will validate the group sequence. The constraints assigned to "Default"
     * can still be validated by validating the class in "<ClassName>".
     *
     * @return string The name of the default group
     */
    public function getDefaultGroup()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addConstraint(\Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Adds a constraint to the given property.
     *
     * @param string $property The name of the property
     *
     * @return $this
     */
    public function addPropertyConstraint($property, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * @param string       $property
     * @param Constraint[] $constraints
     *
     * @return $this
     */
    public function addPropertyConstraints($property, array $constraints)
    {
    }
    /**
     * Adds a constraint to the getter of the given property.
     *
     * The name of the getter is assumed to be the name of the property with an
     * uppercased first letter and the prefix "get", "is" or "has".
     *
     * @param string $property The name of the property
     *
     * @return $this
     */
    public function addGetterConstraint($property, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * Adds a constraint to the getter of the given property.
     *
     * @param string $property The name of the property
     * @param string $method   The name of the getter method
     *
     * @return $this
     */
    public function addGetterMethodConstraint($property, $method, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
    /**
     * @param string       $property
     * @param Constraint[] $constraints
     *
     * @return $this
     */
    public function addGetterConstraints($property, array $constraints)
    {
    }
    /**
     * @param string       $property
     * @param string       $method
     * @param Constraint[] $constraints
     *
     * @return $this
     */
    public function addGetterMethodConstraints($property, $method, array $constraints)
    {
    }
    /**
     * Merges the constraints of the given metadata into this object.
     */
    public function mergeConstraints(self $source)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasPropertyMetadata($property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPropertyMetadata($property)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConstrainedProperties()
    {
    }
    /**
     * Sets the default group sequence for this class.
     *
     * @param string[]|GroupSequence $groupSequence An array of group names
     *
     * @return $this
     *
     * @throws GroupDefinitionException
     */
    public function setGroupSequence($groupSequence)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasGroupSequence()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getGroupSequence()
    {
    }
    /**
     * Returns a ReflectionClass instance for this class.
     *
     * @return \ReflectionClass
     */
    public function getReflectionClass()
    {
    }
    /**
     * Sets whether a group sequence provider should be used.
     *
     * @param bool $active
     *
     * @throws GroupDefinitionException
     */
    public function setGroupSequenceProvider($active)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isGroupSequenceProvider()
    {
    }
    /**
     * Class nodes are never cascaded.
     *
     * {@inheritdoc}
     */
    public function getCascadingStrategy()
    {
    }
}
