<?php

namespace Symfony\Component\Validator\DataCollector;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final since Symfony 4.4
 */
class ValidatorDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct(\Symfony\Component\Validator\Validator\TraceableValidator $validator)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    /**
     * @return Data
     */
    public function getCalls()
    {
    }
    /**
     * @return int
     */
    public function getViolationsCount()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    protected function getCasters()
    {
    }
}
