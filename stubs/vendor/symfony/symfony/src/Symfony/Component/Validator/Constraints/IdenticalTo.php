<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Daniel Holmes <daniel@danielholmes.org>
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IdenticalTo extends \Symfony\Component\Validator\Constraints\AbstractComparison
{
    public const NOT_IDENTICAL_ERROR = '2a8cc50f-58a2-4536-875e-060a2ce69ed5';
    protected static $errorNames = [self::NOT_IDENTICAL_ERROR => 'NOT_IDENTICAL_ERROR'];
    public $message = 'This value should be identical to {{ compared_value_type }} {{ compared_value }}.';
}
