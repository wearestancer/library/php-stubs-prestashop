<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class NotNull extends \Symfony\Component\Validator\Constraint
{
    public const IS_NULL_ERROR = 'ad32d13f-c3d4-423b-909a-857b961eb720';
    protected static $errorNames = [self::IS_NULL_ERROR => 'IS_NULL_ERROR'];
    public $message = 'This value should not be null.';
}
