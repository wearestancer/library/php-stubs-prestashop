<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Valid extends \Symfony\Component\Validator\Constraint
{
    public $traverse = true;
    public function __get($option)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addImplicitGroupName($group)
    {
    }
}
