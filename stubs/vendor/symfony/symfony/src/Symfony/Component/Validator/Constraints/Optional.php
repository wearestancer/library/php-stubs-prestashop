<?php

namespace Symfony\Component\Validator\Constraints;

/**
 * @Annotation
 * @Target({"ANNOTATION"})
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Optional extends \Symfony\Component\Validator\Constraints\Existence
{
}
