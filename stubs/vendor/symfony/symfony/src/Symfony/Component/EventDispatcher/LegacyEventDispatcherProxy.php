<?php

namespace Symfony\Component\EventDispatcher;

/**
 * A helper class to provide BC/FC with the legacy signature of EventDispatcherInterface::dispatch().
 *
 * This class should be deprecated in Symfony 5.1
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class LegacyEventDispatcherProxy implements \Symfony\Component\EventDispatcher\EventDispatcherInterface
{
    public static function decorate(?\Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher) : ?\Symfony\Contracts\EventDispatcher\EventDispatcherInterface
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $eventName
     *
     * @return object
     */
    public function dispatch($event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addListener($eventName, $listener, $priority = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeListener($eventName, $listener)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListeners($eventName = null) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListenerPriority($eventName, $listener) : ?int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasListeners($eventName = null) : bool
    {
    }
    /**
     * Proxies all method calls to the original event dispatcher.
     */
    public function __call($method, $arguments)
    {
    }
}
