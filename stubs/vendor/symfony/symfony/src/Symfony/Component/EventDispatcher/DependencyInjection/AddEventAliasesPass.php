<?php

namespace Symfony\Component\EventDispatcher\DependencyInjection;

/**
 * This pass allows bundles to extend the list of event aliases.
 *
 * @author Alexander M. Turek <me@derrabus.de>
 */
class AddEventAliasesPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(array $eventAliases, string $eventAliasesParameter = 'event_dispatcher.event_aliases')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container) : void
    {
    }
}
