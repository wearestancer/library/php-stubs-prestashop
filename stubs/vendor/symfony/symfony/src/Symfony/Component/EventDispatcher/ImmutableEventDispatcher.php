<?php

namespace Symfony\Component\EventDispatcher;

/**
 * A read-only proxy for an event dispatcher.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ImmutableEventDispatcher implements \Symfony\Component\EventDispatcher\EventDispatcherInterface
{
    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $eventName
     */
    public function dispatch($event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addListener($eventName, $listener, $priority = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeListener($eventName, $listener)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListeners($eventName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListenerPriority($eventName, $listener)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasListeners($eventName = null)
    {
    }
}
