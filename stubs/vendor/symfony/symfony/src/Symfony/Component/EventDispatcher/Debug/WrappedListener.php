<?php

namespace Symfony\Component\EventDispatcher\Debug;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3: the "Event" type-hint on __invoke() will be replaced by "object" in 5.0
 */
class WrappedListener
{
    public function __construct($listener, ?string $name, \Symfony\Component\Stopwatch\Stopwatch $stopwatch, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    public function getWrappedListener()
    {
    }
    public function wasCalled()
    {
    }
    public function stoppedPropagation()
    {
    }
    public function getPretty()
    {
    }
    public function getInfo($eventName)
    {
    }
    public function __invoke(\Symfony\Component\EventDispatcher\Event $event, $eventName, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
}
