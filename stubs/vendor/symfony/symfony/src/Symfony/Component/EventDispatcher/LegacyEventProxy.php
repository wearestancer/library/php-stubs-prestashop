<?php

namespace Symfony\Component\EventDispatcher;

/**
 * @internal to be removed in 5.0.
 */
final class LegacyEventProxy extends \Symfony\Component\EventDispatcher\Event
{
    /**
     * @param object $event
     */
    public function __construct($event)
    {
    }
    /**
     * @return object $event
     */
    public function getEvent()
    {
    }
    public function isPropagationStopped() : bool
    {
    }
    public function stopPropagation()
    {
    }
    public function __call($name, $args)
    {
    }
}
