<?php

namespace Symfony\Component\EventDispatcher\DependencyInjection;

/**
 * Compiler pass to register tagged services for an event dispatcher.
 */
class RegisterListenersPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    protected $dispatcherService;
    protected $listenerTag;
    protected $subscriberTag;
    protected $eventAliasesParameter;
    public function __construct(string $dispatcherService = 'event_dispatcher', string $listenerTag = 'kernel.event_listener', string $subscriberTag = 'kernel.event_subscriber', string $eventAliasesParameter = 'event_dispatcher.event_aliases')
    {
    }
    public function setHotPathEvents(array $hotPathEvents, $tagName = 'container.hot_path')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
/**
 * @internal
 */
class ExtractingEventDispatcher extends \Symfony\Component\EventDispatcher\EventDispatcher implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public $listeners = [];
    public static $aliases = [];
    public static $subscriber;
    public function addListener($eventName, $listener, $priority = 0)
    {
    }
    public static function getSubscribedEvents() : array
    {
    }
}
