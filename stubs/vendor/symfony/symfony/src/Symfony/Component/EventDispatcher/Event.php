<?php

namespace Symfony\Component\EventDispatcher;

/**
 * @deprecated since Symfony 4.3, use "Symfony\Contracts\EventDispatcher\Event" instead
 */
class Event
{
    /**
     * @return bool Whether propagation was already stopped for this event
     *
     * @deprecated since Symfony 4.3, use "Symfony\Contracts\EventDispatcher\Event" instead
     */
    public function isPropagationStopped()
    {
    }
    /**
     * @deprecated since Symfony 4.3, use "Symfony\Contracts\EventDispatcher\Event" instead
     */
    public function stopPropagation()
    {
    }
}
