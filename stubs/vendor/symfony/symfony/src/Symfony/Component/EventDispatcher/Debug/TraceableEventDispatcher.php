<?php

namespace Symfony\Component\EventDispatcher\Debug;

/**
 * Collects some data about event listeners.
 *
 * This event dispatcher delegates the dispatching to another one.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TraceableEventDispatcher implements \Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface
{
    protected $logger;
    protected $stopwatch;
    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher, \Symfony\Component\Stopwatch\Stopwatch $stopwatch, \Psr\Log\LoggerInterface $logger = null, \Symfony\Component\HttpFoundation\RequestStack $requestStack = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addListener($eventName, $listener, $priority = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeListener($eventName, $listener)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeSubscriber(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListeners($eventName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListenerPriority($eventName, $listener)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasListeners($eventName = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $eventName
     */
    public function dispatch($event)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request|null $request The request to get listeners for
     */
    public function getCalledListeners()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request|null $request The request to get listeners for
     */
    public function getNotCalledListeners()
    {
    }
    /**
     * @param Request|null $request The request to get orphaned events for
     */
    public function getOrphanedEvents() : array
    {
    }
    public function reset()
    {
    }
    /**
     * Proxies all method calls to the original event dispatcher.
     *
     * @param string $method    The method name
     * @param array  $arguments The method arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
    }
    /**
     * Called before dispatching the event.
     *
     * @param object $event
     */
    protected function beforeDispatch(string $eventName, $event)
    {
    }
    /**
     * Called after dispatching the event.
     *
     * @param object $event
     */
    protected function afterDispatch(string $eventName, $event)
    {
    }
    /**
     * @deprecated since Symfony 4.3, will be removed in 5.0, use beforeDispatch instead
     */
    protected function preDispatch($eventName, \Symfony\Component\EventDispatcher\Event $event)
    {
    }
    /**
     * @deprecated since Symfony 4.3, will be removed in 5.0, use afterDispatch instead
     */
    protected function postDispatch($eventName, \Symfony\Component\EventDispatcher\Event $event)
    {
    }
}
