<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ReferenceConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractConfigurator
{
    /** @internal */
    protected $id;
    /** @internal */
    protected $invalidBehavior = \Symfony\Component\DependencyInjection\ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE;
    public function __construct(string $id)
    {
    }
    /**
     * @return $this
     */
    public final function ignoreOnInvalid() : self
    {
    }
    /**
     * @return $this
     */
    public final function nullOnInvalid() : self
    {
    }
    /**
     * @return $this
     */
    public final function ignoreOnUninitialized() : self
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
}
