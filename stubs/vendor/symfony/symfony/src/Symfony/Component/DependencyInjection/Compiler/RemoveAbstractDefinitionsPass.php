<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Removes abstract Definitions.
 */
class RemoveAbstractDefinitionsPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * Removes abstract definitions from the ContainerBuilder.
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
