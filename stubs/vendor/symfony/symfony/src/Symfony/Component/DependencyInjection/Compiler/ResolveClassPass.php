<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ResolveClassPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
