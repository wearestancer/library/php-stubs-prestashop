<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait TagTrait
{
    /**
     * Adds a tag for this definition.
     *
     * @return $this
     */
    public final function tag(string $name, array $attributes = []) : self
    {
    }
}
