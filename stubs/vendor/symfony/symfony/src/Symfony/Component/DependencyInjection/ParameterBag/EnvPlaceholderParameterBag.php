<?php

namespace Symfony\Component\DependencyInjection\ParameterBag;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class EnvPlaceholderParameterBag extends \Symfony\Component\DependencyInjection\ParameterBag\ParameterBag
{
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * Gets the common env placeholder prefix for env vars created by this bag.
     */
    public function getEnvPlaceholderUniquePrefix() : string
    {
    }
    /**
     * Returns the map of env vars used in the resolved parameter values to their placeholders.
     *
     * @return string[][] A map of env var names to their placeholders
     */
    public function getEnvPlaceholders()
    {
    }
    public function getUnusedEnvPlaceholders() : array
    {
    }
    public function clearUnusedEnvPlaceholders()
    {
    }
    /**
     * Merges the env placeholders of another EnvPlaceholderParameterBag.
     */
    public function mergeEnvPlaceholders(self $bag)
    {
    }
    /**
     * Maps env prefixes to their corresponding PHP types.
     */
    public function setProvidedTypes(array $providedTypes)
    {
    }
    /**
     * Gets the PHP types corresponding to env() parameter prefixes.
     *
     * @return string[][]
     */
    public function getProvidedTypes()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve()
    {
    }
}
