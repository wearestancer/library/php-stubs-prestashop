<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class InlineServiceConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractConfigurator
{
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ArgumentTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AutowireTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\BindTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\FactoryTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\FileTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\LazyTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ParentTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\TagTrait;
    public const FACTORY = 'inline';
    public function __construct(\Symfony\Component\DependencyInjection\Definition $definition)
    {
    }
}
