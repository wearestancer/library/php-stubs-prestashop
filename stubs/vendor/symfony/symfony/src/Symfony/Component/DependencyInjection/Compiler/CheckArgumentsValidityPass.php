<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Checks if arguments of methods are properly configured.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CheckArgumentsValidityPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    public function __construct(bool $throwExceptions = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
