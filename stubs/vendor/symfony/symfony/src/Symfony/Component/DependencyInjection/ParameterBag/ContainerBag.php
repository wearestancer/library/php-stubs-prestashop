<?php

namespace Symfony\Component\DependencyInjection\ParameterBag;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ContainerBag extends \Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag implements \Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface
{
    public function __construct(\Symfony\Component\DependencyInjection\Container $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|bool|string|int|float|\UnitEnum|null
     */
    public function get($name)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function has($name)
    {
    }
}
