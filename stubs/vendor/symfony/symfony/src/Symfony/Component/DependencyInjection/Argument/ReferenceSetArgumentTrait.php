<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * @author Titouan Galopin <galopintitouan@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
trait ReferenceSetArgumentTrait
{
    private $values;
    /**
     * @param Reference[] $values
     */
    public function __construct(array $values)
    {
    }
    /**
     * @return Reference[] The values in the set
     */
    public function getValues()
    {
    }
    /**
     * @param Reference[] $values The service references to put in the set
     */
    public function setValues(array $values)
    {
    }
}
