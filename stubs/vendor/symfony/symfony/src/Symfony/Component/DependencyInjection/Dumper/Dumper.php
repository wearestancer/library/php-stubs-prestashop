<?php

namespace Symfony\Component\DependencyInjection\Dumper;

/**
 * Dumper is the abstract class for all built-in dumpers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Dumper implements \Symfony\Component\DependencyInjection\Dumper\DumperInterface
{
    protected $container;
    public function __construct(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
