<?php

namespace Symfony\Component\DependencyInjection\LazyProxy\Instantiator;

/**
 * Lazy proxy instantiator, capable of instantiating a proxy given a container, the
 * service definitions and a callback that produces the real service instance.
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 */
interface InstantiatorInterface
{
    /**
     * Instantiates a proxy object.
     *
     * @param string   $id               Identifier of the requested service
     * @param callable $realInstantiator Zero-argument callback that is capable of producing the real service instance
     *
     * @return object
     */
    public function instantiateProxy(\Symfony\Component\DependencyInjection\ContainerInterface $container, \Symfony\Component\DependencyInjection\Definition $definition, $id, $realInstantiator);
}
