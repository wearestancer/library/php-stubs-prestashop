<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Resolves all TaggedIteratorArgument arguments.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
class ResolveTaggedIteratorArgumentPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
