<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * This exception is thrown when a non-existent parameter is used.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ParameterNotFoundException extends \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException implements \Psr\Container\NotFoundExceptionInterface
{
    /**
     * @param string          $key                  The requested parameter key
     * @param string|null     $sourceId             The service id that references the non-existent parameter
     * @param string|null     $sourceKey            The parameter key that references the non-existent parameter
     * @param \Throwable|null $previous             The previous exception
     * @param string[]        $alternatives         Some parameter name alternatives
     * @param string|null     $nonNestedAlternative The alternative parameter name when the user expected dot notation for nested parameters
     */
    public function __construct(string $key, string $sourceId = null, string $sourceKey = null, \Throwable $previous = null, array $alternatives = [], string $nonNestedAlternative = null)
    {
    }
    public function updateRepr()
    {
    }
    public function getKey()
    {
    }
    public function getSourceId()
    {
    }
    public function getSourceKey()
    {
    }
    public function setSourceId($sourceId)
    {
    }
    public function setSourceKey($sourceKey)
    {
    }
}
