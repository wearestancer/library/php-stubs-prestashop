<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Merges extension configs into the container builder.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MergeExtensionConfigurationPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
/**
 * @internal
 */
class MergeExtensionConfigurationParameterBag extends \Symfony\Component\DependencyInjection\ParameterBag\EnvPlaceholderParameterBag
{
    public function __construct(parent $parameterBag)
    {
    }
    public function freezeAfterProcessing(\Symfony\Component\DependencyInjection\Extension\Extension $extension, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEnvPlaceholders() : array
    {
    }
    public function getUnusedEnvPlaceholders() : array
    {
    }
}
/**
 * A container builder preventing using methods that wouldn't have any effect from extensions.
 *
 * @internal
 */
class MergeExtensionConfigurationContainerBuilder extends \Symfony\Component\DependencyInjection\ContainerBuilder
{
    public function __construct(\Symfony\Component\DependencyInjection\Extension\ExtensionInterface $extension, \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addCompilerPass(\Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface $pass, $type = \Symfony\Component\DependencyInjection\Compiler\PassConfig::TYPE_BEFORE_OPTIMIZATION, int $priority = 0) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function registerExtension(\Symfony\Component\DependencyInjection\Extension\ExtensionInterface $extension)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function compile(bool $resolveEnvPlaceholders = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolveEnvPlaceholders($value, $format = null, array &$usedEnvs = null)
    {
    }
}
