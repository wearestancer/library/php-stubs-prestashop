<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * Base ExceptionInterface for Dependency Injection component.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bulat Shakirzyanov <bulat@theopenskyproject.com>
 */
interface ExceptionInterface extends \Psr\Container\ContainerExceptionInterface, \Throwable
{
}
