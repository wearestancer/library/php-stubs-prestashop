<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * @internal
 */
class RewindableGenerator implements \IteratorAggregate, \Countable
{
    /**
     * @param int|callable $count
     */
    public function __construct(callable $generator, $count)
    {
    }
    public function getIterator() : \Traversable
    {
    }
    public function count() : int
    {
    }
}
