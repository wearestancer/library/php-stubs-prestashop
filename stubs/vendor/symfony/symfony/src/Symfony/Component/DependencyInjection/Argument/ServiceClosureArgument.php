<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * Represents a service wrapped in a memoizing closure.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ServiceClosureArgument implements \Symfony\Component\DependencyInjection\Argument\ArgumentInterface
{
    public function __construct(\Symfony\Component\DependencyInjection\Reference $reference)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValues()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setValues(array $values)
    {
    }
}
