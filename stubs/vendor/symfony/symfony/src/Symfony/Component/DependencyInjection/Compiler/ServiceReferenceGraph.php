<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * This is a directed graph of your services.
 *
 * This information can be used by your compiler passes instead of collecting
 * it themselves which improves performance quite a lot.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 *
 * @final
 */
class ServiceReferenceGraph
{
    public function hasNode(string $id) : bool
    {
    }
    /**
     * Gets a node by identifier.
     *
     * @throws InvalidArgumentException if no node matches the supplied identifier
     */
    public function getNode(string $id) : \Symfony\Component\DependencyInjection\Compiler\ServiceReferenceGraphNode
    {
    }
    /**
     * Returns all nodes.
     *
     * @return ServiceReferenceGraphNode[]
     */
    public function getNodes() : array
    {
    }
    /**
     * Clears all nodes.
     */
    public function clear()
    {
    }
    /**
     * Connects 2 nodes together in the Graph.
     */
    public function connect(?string $sourceId, $sourceValue, ?string $destId, $destValue = null, $reference = null, bool $lazy = false, bool $weak = false, bool $byConstructor = false)
    {
    }
}
