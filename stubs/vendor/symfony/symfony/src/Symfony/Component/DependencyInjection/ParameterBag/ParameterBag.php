<?php

namespace Symfony\Component\DependencyInjection\ParameterBag;

/**
 * Holds parameters.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ParameterBag implements \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface
{
    protected $parameters = [];
    protected $resolved = false;
    public function __construct(array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add(array $parameters)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve()
    {
    }
    /**
     * Replaces parameter placeholders (%name%) by their values.
     *
     * @param mixed $value     A value
     * @param array $resolving An array of keys that are being resolved (used internally to detect circular references)
     *
     * @return mixed The resolved value
     *
     * @throws ParameterNotFoundException          if a placeholder references a parameter that does not exist
     * @throws ParameterCircularReferenceException if a circular reference if detected
     * @throws RuntimeException                    when a given parameter has a type problem
     */
    public function resolveValue($value, array $resolving = [])
    {
    }
    /**
     * Resolves parameters inside a string.
     *
     * @param string $value     The string to resolve
     * @param array  $resolving An array of keys that are being resolved (used internally to detect circular references)
     *
     * @return mixed The resolved string
     *
     * @throws ParameterNotFoundException          if a placeholder references a parameter that does not exist
     * @throws ParameterCircularReferenceException if a circular reference if detected
     * @throws RuntimeException                    when a given parameter has a type problem
     */
    public function resolveString($value, array $resolving = [])
    {
    }
    public function isResolved()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function escapeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function unescapeValue($value)
    {
    }
}
