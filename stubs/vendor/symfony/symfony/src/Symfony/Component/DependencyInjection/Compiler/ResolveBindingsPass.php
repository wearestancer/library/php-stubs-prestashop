<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * @author Guilhem Niot <guilhem.niot@gmail.com>
 */
class ResolveBindingsPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
