<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait FactoryTrait
{
    /**
     * Sets a factory.
     *
     * @param string|array|ReferenceConfigurator $factory A PHP callable reference
     *
     * @return $this
     */
    public final function factory($factory) : self
    {
    }
}
