<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ServicesConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractConfigurator
{
    public const FACTORY = 'services';
    public function __construct(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \Symfony\Component\DependencyInjection\Loader\PhpFileLoader $loader, array &$instanceof, string $path = null, int &$anonymousCount = 0)
    {
    }
    /**
     * Defines a set of defaults for following service definitions.
     */
    public final function defaults() : \Symfony\Component\DependencyInjection\Loader\Configurator\DefaultsConfigurator
    {
    }
    /**
     * Defines an instanceof-conditional to be applied to following service definitions.
     */
    public final function instanceof(string $fqcn) : \Symfony\Component\DependencyInjection\Loader\Configurator\InstanceofConfigurator
    {
    }
    /**
     * Registers a service.
     *
     * @param string|null $id    The service id, or null to create an anonymous service
     * @param string|null $class The class of the service, or null when $id is also the class name
     */
    public final function set(?string $id, string $class = null) : \Symfony\Component\DependencyInjection\Loader\Configurator\ServiceConfigurator
    {
    }
    /**
     * Creates an alias.
     */
    public final function alias(string $id, string $referencedId) : \Symfony\Component\DependencyInjection\Loader\Configurator\AliasConfigurator
    {
    }
    /**
     * Registers a PSR-4 namespace using a glob pattern.
     */
    public final function load(string $namespace, string $resource) : \Symfony\Component\DependencyInjection\Loader\Configurator\PrototypeConfigurator
    {
    }
    /**
     * Gets an already defined service definition.
     *
     * @throws ServiceNotFoundException if the service definition does not exist
     */
    public final function get(string $id) : \Symfony\Component\DependencyInjection\Loader\Configurator\ServiceConfigurator
    {
    }
    /**
     * Registers a service.
     */
    public final function __invoke(string $id, string $class = null) : \Symfony\Component\DependencyInjection\Loader\Configurator\ServiceConfigurator
    {
    }
    public function __destruct()
    {
    }
}
