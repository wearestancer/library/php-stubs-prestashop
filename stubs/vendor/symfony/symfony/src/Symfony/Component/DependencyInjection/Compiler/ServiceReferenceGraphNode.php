<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Represents a node in your service graph.
 *
 * Value is typically a definition, or an alias.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ServiceReferenceGraphNode
{
    /**
     * @param string $id    The node identifier
     * @param mixed  $value The node value
     */
    public function __construct(string $id, $value)
    {
    }
    public function addInEdge(\Symfony\Component\DependencyInjection\Compiler\ServiceReferenceGraphEdge $edge)
    {
    }
    public function addOutEdge(\Symfony\Component\DependencyInjection\Compiler\ServiceReferenceGraphEdge $edge)
    {
    }
    /**
     * Checks if the value of this node is an Alias.
     *
     * @return bool True if the value is an Alias instance
     */
    public function isAlias()
    {
    }
    /**
     * Checks if the value of this node is a Definition.
     *
     * @return bool True if the value is a Definition instance
     */
    public function isDefinition()
    {
    }
    /**
     * Returns the identifier.
     *
     * @return string
     */
    public function getId()
    {
    }
    /**
     * Returns the in edges.
     *
     * @return ServiceReferenceGraphEdge[]
     */
    public function getInEdges()
    {
    }
    /**
     * Returns the out edges.
     *
     * @return ServiceReferenceGraphEdge[]
     */
    public function getOutEdges()
    {
    }
    /**
     * Returns the value of this Node.
     *
     * @return mixed The value
     */
    public function getValue()
    {
    }
    /**
     * Clears all edges.
     */
    public function clear()
    {
    }
}
