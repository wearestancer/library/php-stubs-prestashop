<?php

namespace Symfony\Component\DependencyInjection\Dumper;

/**
 * YamlDumper dumps a service container as a YAML string.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class YamlDumper extends \Symfony\Component\DependencyInjection\Dumper\Dumper
{
    /**
     * Dumps the service container as an YAML string.
     *
     * @return string A YAML string representing of the service container
     */
    public function dump(array $options = [])
    {
    }
}
