<?php

namespace Symfony\Component\DependencyInjection\Extension;

/**
 * Provides useful features shared by many extensions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Extension implements \Symfony\Component\DependencyInjection\Extension\ExtensionInterface, \Symfony\Component\DependencyInjection\Extension\ConfigurationExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getXsdValidationBasePath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNamespace()
    {
    }
    /**
     * Returns the recommended alias to use in XML.
     *
     * This alias is also the mandatory prefix to use when using YAML.
     *
     * This convention is to remove the "Extension" postfix from the class
     * name and then lowercase and underscore the result. So:
     *
     *     AcmeHelloExtension
     *
     * becomes
     *
     *     acme_hello
     *
     * This can be overridden in a sub-class to specify the alias manually.
     *
     * @return string The alias
     *
     * @throws BadMethodCallException When the extension name does not follow conventions
     */
    public function getAlias()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfiguration(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    protected final function processConfiguration(\Symfony\Component\Config\Definition\ConfigurationInterface $configuration, array $configs) : array
    {
    }
    /**
     * @internal
     */
    public final function getProcessedConfigs() : array
    {
    }
    /**
     * @return bool Whether the configuration is enabled
     *
     * @throws InvalidArgumentException When the config is not enableable
     */
    protected function isConfigEnabled(\Symfony\Component\DependencyInjection\ContainerBuilder $container, array $config)
    {
    }
}
