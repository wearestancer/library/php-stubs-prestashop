<?php

namespace Symfony\Component\DependencyInjection\LazyProxy\PhpDumper;

/**
 * Lazy proxy dumper capable of generating the instantiation logic PHP code for proxied services.
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 */
interface DumperInterface
{
    /**
     * Inspects whether the given definitions should produce proxy instantiation logic in the dumped container.
     *
     * @return bool
     */
    public function isProxyCandidate(\Symfony\Component\DependencyInjection\Definition $definition);
    /**
     * Generates the code to be used to instantiate a proxy in the dumped factory code.
     *
     * @param string $id          Service identifier
     * @param string $factoryCode The code to execute to create the service
     *
     * @return string
     */
    public function getProxyFactoryCode(\Symfony\Component\DependencyInjection\Definition $definition, $id, $factoryCode);
    /**
     * Generates the code for the lazy proxy.
     *
     * @return string
     */
    public function getProxyCode(\Symfony\Component\DependencyInjection\Definition $definition);
}
