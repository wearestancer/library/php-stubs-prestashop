<?php

namespace Symfony\Component\DependencyInjection\Config;

/**
 * Tracks container parameters.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final since Symfony 4.3
 */
class ContainerParametersResource implements \Symfony\Component\Config\Resource\ResourceInterface
{
    /**
     * @param array $parameters The container parameters to track
     */
    public function __construct(array $parameters)
    {
    }
    public function __toString()
    {
    }
    /**
     * @return array Tracked parameters
     */
    public function getParameters()
    {
    }
}
