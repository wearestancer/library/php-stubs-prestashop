<?php

namespace Symfony\Component\DependencyInjection\Dumper;

/**
 * XmlDumper dumps a service container as an XML string.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Martin Hasoň <martin.hason@gmail.com>
 */
class XmlDumper extends \Symfony\Component\DependencyInjection\Dumper\Dumper
{
    /**
     * Dumps the service container as an XML string.
     *
     * @return string An xml string representing of the service container
     */
    public function dump(array $options = [])
    {
    }
    /**
     * Converts php types to xml types.
     *
     * @param mixed $value Value to convert
     *
     * @throws RuntimeException When trying to dump object or resource
     */
    public static function phpToXml($value) : string
    {
    }
}
