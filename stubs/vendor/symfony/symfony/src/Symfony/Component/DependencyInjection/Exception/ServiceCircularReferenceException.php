<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * This exception is thrown when a circular reference is detected.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ServiceCircularReferenceException extends \Symfony\Component\DependencyInjection\Exception\RuntimeException
{
    public function __construct(string $serviceId, array $path, \Throwable $previous = null)
    {
    }
    public function getServiceId()
    {
    }
    public function getPath()
    {
    }
}
