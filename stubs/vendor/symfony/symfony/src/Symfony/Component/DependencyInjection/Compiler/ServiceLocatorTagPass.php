<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Applies the "container.service_locator" tag by wrapping references into ServiceClosureArgument instances.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class ServiceLocatorTagPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    /**
     * @param Reference[] $refMap
     */
    public static function register(\Symfony\Component\DependencyInjection\ContainerBuilder $container, array $refMap, string $callerId = null) : \Symfony\Component\DependencyInjection\Reference
    {
    }
}
