<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * This exception is thrown when a non-existent service is requested.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ServiceNotFoundException extends \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException implements \Psr\Container\NotFoundExceptionInterface
{
    public function __construct(string $id, string $sourceId = null, \Throwable $previous = null, array $alternatives = [], string $msg = null)
    {
    }
    public function getId()
    {
    }
    public function getSourceId()
    {
    }
    public function getAlternatives()
    {
    }
}
