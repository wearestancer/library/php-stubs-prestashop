<?php

namespace Symfony\Component\DependencyInjection;

/**
 * Represents a PHP type-hinted service reference.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class TypedReference extends \Symfony\Component\DependencyInjection\Reference
{
    /**
     * @param string      $id              The service identifier
     * @param string      $type            The PHP type of the identified service
     * @param int         $invalidBehavior The behavior when the service does not exist
     * @param string|null $name            The name of the argument targeting the service
     */
    public function __construct(string $id, string $type, $invalidBehavior = \Symfony\Component\DependencyInjection\ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE, $name = null)
    {
    }
    public function getType()
    {
    }
    public function getName() : ?string
    {
    }
    /**
     * @deprecated since Symfony 4.1
     */
    public function getRequiringClass()
    {
    }
    /**
     * @deprecated since Symfony 4.1
     */
    public function canBeAutoregistered()
    {
    }
}
