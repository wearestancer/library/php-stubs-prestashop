<?php

namespace Symfony\Component\DependencyInjection\Extension;

interface PrependExtensionInterface
{
    /**
     * Allow an extension to prepend the extension configurations.
     */
    public function prepend(\Symfony\Component\DependencyInjection\ContainerBuilder $container);
}
