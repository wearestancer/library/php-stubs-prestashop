<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * Base BadMethodCallException for Dependency Injection component.
 */
class BadMethodCallException extends \BadMethodCallException implements \Symfony\Component\DependencyInjection\Exception\ExceptionInterface
{
}
