<?php

namespace Symfony\Component\DependencyInjection\LazyProxy\Instantiator;

/**
 * {@inheritdoc}
 *
 * Noop proxy instantiator - produces the real service instead of a proxy instance.
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 */
class RealServiceInstantiator implements \Symfony\Component\DependencyInjection\LazyProxy\Instantiator\InstantiatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function instantiateProxy(\Symfony\Component\DependencyInjection\ContainerInterface $container, \Symfony\Component\DependencyInjection\Definition $definition, $id, $realInstantiator)
    {
    }
}
