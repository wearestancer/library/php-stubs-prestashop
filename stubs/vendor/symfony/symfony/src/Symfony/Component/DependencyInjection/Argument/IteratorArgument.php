<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * Represents a collection of values to lazily iterate over.
 *
 * @author Titouan Galopin <galopintitouan@gmail.com>
 */
class IteratorArgument implements \Symfony\Component\DependencyInjection\Argument\ArgumentInterface
{
    use \Symfony\Component\DependencyInjection\Argument\ReferenceSetArgumentTrait;
}
