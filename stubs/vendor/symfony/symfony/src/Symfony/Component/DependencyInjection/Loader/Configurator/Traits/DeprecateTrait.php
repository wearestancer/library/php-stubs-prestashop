<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait DeprecateTrait
{
    /**
     * Whether this definition is deprecated, that means it should not be called anymore.
     *
     * @param string $template Template message to use if the definition is deprecated
     *
     * @return $this
     *
     * @throws InvalidArgumentException when the message template is invalid
     */
    public final function deprecate(string $template = null) : self
    {
    }
}
