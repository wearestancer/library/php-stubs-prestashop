<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait AutoconfigureTrait
{
    /**
     * Sets whether or not instanceof conditionals should be prepended with a global set.
     *
     * @return $this
     *
     * @throws InvalidArgumentException when a parent is already set
     */
    public final function autoconfigure(bool $autoconfigured = true) : self
    {
    }
}
