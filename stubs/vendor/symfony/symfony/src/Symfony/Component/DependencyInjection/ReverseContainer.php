<?php

namespace Symfony\Component\DependencyInjection;

/**
 * Turns public and "container.reversible" services back to their ids.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class ReverseContainer
{
    public function __construct(\Symfony\Component\DependencyInjection\Container $serviceContainer, \Psr\Container\ContainerInterface $reversibleLocator, string $tagName = 'container.reversible')
    {
    }
    /**
     * Returns the id of the passed object when it exists as a service.
     *
     * To be reversible, services need to be either public or be tagged with "container.reversible".
     *
     * @param object $service
     */
    public function getId($service) : ?string
    {
    }
    /**
     * @return object
     *
     * @throws ServiceNotFoundException When the service is not reversible
     */
    public function getService(string $id)
    {
    }
}
