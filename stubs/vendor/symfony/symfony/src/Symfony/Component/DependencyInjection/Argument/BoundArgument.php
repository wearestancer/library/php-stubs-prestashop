<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * @author Guilhem Niot <guilhem.niot@gmail.com>
 */
final class BoundArgument implements \Symfony\Component\DependencyInjection\Argument\ArgumentInterface
{
    public const SERVICE_BINDING = 0;
    public const DEFAULTS_BINDING = 1;
    public const INSTANCEOF_BINDING = 2;
    public function __construct($value, bool $trackUsage = true, int $type = 0, string $file = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getValues() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setValues(array $values)
    {
    }
}
