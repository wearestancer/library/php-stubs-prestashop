<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * This exception is thrown when a circular reference in a parameter is detected.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ParameterCircularReferenceException extends \Symfony\Component\DependencyInjection\Exception\RuntimeException
{
    public function __construct(array $parameters, \Throwable $previous = null)
    {
    }
    public function getParameters()
    {
    }
}
