<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

abstract class AbstractConfigurator
{
    public const FACTORY = 'unknown';
    /** @internal */
    protected $definition;
    public function __call($method, $args)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    /**
     * Checks that a value is valid, optionally replacing Definition and Reference configurators by their configure value.
     *
     * @param mixed $value
     * @param bool  $allowServices whether Definition and Reference are allowed; by default, only scalars and arrays are
     *
     * @return mixed the value, optionally cast to a Definition/Reference
     */
    public static function processValue($value, $allowServices = false)
    {
    }
}
