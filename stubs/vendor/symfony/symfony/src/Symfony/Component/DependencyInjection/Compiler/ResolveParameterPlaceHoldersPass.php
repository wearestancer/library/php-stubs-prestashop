<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Resolves all parameter placeholders "%somevalue%" to their real values.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ResolveParameterPlaceHoldersPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    public function __construct($resolveArrays = true, $throwOnResolveException = true)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws ParameterNotFoundException
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    protected function processValue($value, $isRoot = false)
    {
    }
}
