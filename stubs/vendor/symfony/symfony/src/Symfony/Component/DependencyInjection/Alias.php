<?php

namespace Symfony\Component\DependencyInjection;

class Alias
{
    public function __construct(string $id, bool $public = true)
    {
    }
    /**
     * Checks if this DI Alias should be public or not.
     *
     * @return bool
     */
    public function isPublic()
    {
    }
    /**
     * Sets if this Alias is public.
     *
     * @param bool $boolean If this Alias should be public
     *
     * @return $this
     */
    public function setPublic($boolean)
    {
    }
    /**
     * Sets if this Alias is private.
     *
     * When set, the "private" state has a higher precedence than "public".
     * In version 3.4, a "private" alias always remains publicly accessible,
     * but triggers a deprecation notice when accessed from the container,
     * so that the alias can be made really private in 4.0.
     *
     * @param bool $boolean
     *
     * @return $this
     */
    public function setPrivate($boolean)
    {
    }
    /**
     * Whether this alias is private.
     *
     * @return bool
     */
    public function isPrivate()
    {
    }
    /**
     * Whether this alias is deprecated, that means it should not be referenced
     * anymore.
     *
     * @param bool   $status   Whether this alias is deprecated, defaults to true
     * @param string $template Optional template message to use if the alias is deprecated
     *
     * @return $this
     *
     * @throws InvalidArgumentException when the message template is invalid
     */
    public function setDeprecated($status = true, $template = null)
    {
    }
    public function isDeprecated() : bool
    {
    }
    public function getDeprecationMessage(string $id) : string
    {
    }
    /**
     * Returns the Id of this alias.
     *
     * @return string The alias id
     */
    public function __toString()
    {
    }
}
