<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * Represents a closure acting as a service locator.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ServiceLocatorArgument implements \Symfony\Component\DependencyInjection\Argument\ArgumentInterface
{
    use \Symfony\Component\DependencyInjection\Argument\ReferenceSetArgumentTrait;
    /**
     * @param Reference[]|TaggedIteratorArgument $values
     */
    public function __construct($values = [])
    {
    }
    public function getTaggedIteratorArgument() : ?\Symfony\Component\DependencyInjection\Argument\TaggedIteratorArgument
    {
    }
}
