<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait CallTrait
{
    /**
     * Adds a method to call after service initialization.
     *
     * @param string $method       The method name to call
     * @param array  $arguments    An array of arguments to pass to the method call
     * @param bool   $returnsClone Whether the call returns the service instance or not
     *
     * @return $this
     *
     * @throws InvalidArgumentException on empty $method param
     */
    public final function call(string $method, array $arguments = [], bool $returnsClone = false) : self
    {
    }
}
