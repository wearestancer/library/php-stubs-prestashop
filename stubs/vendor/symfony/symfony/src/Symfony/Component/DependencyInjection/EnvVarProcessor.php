<?php

namespace Symfony\Component\DependencyInjection;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class EnvVarProcessor implements \Symfony\Component\DependencyInjection\EnvVarProcessorInterface
{
    /**
     * @param EnvVarLoaderInterface[] $loaders
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, \Traversable $loaders = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getProvidedTypes()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEnv($prefix, $name, \Closure $getEnv)
    {
    }
}
