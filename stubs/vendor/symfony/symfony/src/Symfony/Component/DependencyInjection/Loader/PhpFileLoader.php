<?php

namespace Symfony\Component\DependencyInjection\Loader;

/**
 * PhpFileLoader loads service definitions from a PHP file.
 *
 * The PHP file is required and the $container variable can be
 * used within the file to change the container.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PhpFileLoader extends \Symfony\Component\DependencyInjection\Loader\FileLoader
{
    protected $autoRegisterAliasesForSinglyImplementedInterfaces = false;
    /**
     * {@inheritdoc}
     */
    public function load($resource, $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
    }
}
/**
 * @internal
 */
final class ProtectedPhpFileLoader extends \Symfony\Component\DependencyInjection\Loader\PhpFileLoader
{
}
