<?php

namespace Symfony\Component\DependencyInjection\Config;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class ContainerParametersResourceChecker implements \Symfony\Component\Config\ResourceCheckerInterface
{
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Config\Resource\ResourceInterface $metadata)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh(\Symfony\Component\Config\Resource\ResourceInterface $resource, $timestamp)
    {
    }
}
