<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait PublicTrait
{
    /**
     * @return $this
     */
    public final function public() : self
    {
    }
    /**
     * @return $this
     */
    public final function private() : self
    {
    }
}
