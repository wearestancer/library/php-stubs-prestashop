<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * Base LogicException for Dependency Injection component.
 */
class LogicException extends \LogicException implements \Symfony\Component\DependencyInjection\Exception\ExceptionInterface
{
}
