<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Propagate "container.hot_path" tags to referenced services.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ResolveHotPathPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    public function __construct(string $tagName = 'container.hot_path')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
