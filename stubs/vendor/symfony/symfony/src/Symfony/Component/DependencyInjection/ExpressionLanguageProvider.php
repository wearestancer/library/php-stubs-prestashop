<?php

namespace Symfony\Component\DependencyInjection;

/**
 * Define some ExpressionLanguage functions.
 *
 * To get a service, use service('request').
 * To get a parameter, use parameter('kernel.debug').
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ExpressionLanguageProvider implements \Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface
{
    public function __construct(callable $serviceCompiler = null)
    {
    }
    public function getFunctions()
    {
    }
}
