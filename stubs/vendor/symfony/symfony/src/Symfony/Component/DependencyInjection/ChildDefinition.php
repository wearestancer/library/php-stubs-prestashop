<?php

namespace Symfony\Component\DependencyInjection;

/**
 * This definition extends another definition.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ChildDefinition extends \Symfony\Component\DependencyInjection\Definition
{
    /**
     * @param string $parent The id of Definition instance to decorate
     */
    public function __construct(string $parent)
    {
    }
    /**
     * Returns the Definition to inherit from.
     *
     * @return string
     */
    public function getParent()
    {
    }
    /**
     * Sets the Definition to inherit from.
     *
     * @param string $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
    }
    /**
     * Gets an argument to pass to the service constructor/factory method.
     *
     * If replaceArgument() has been used to replace an argument, this method
     * will return the replacement value.
     *
     * @param int|string $index
     *
     * @return mixed The argument value
     *
     * @throws OutOfBoundsException When the argument does not exist
     */
    public function getArgument($index)
    {
    }
    /**
     * You should always use this method when overwriting existing arguments
     * of the parent definition.
     *
     * If you directly call setArguments() keep in mind that you must follow
     * certain conventions when you want to overwrite the arguments of the
     * parent definition, otherwise your arguments will only be appended.
     *
     * @param int|string $index
     * @param mixed      $value
     *
     * @return $this
     *
     * @throws InvalidArgumentException when $index isn't an integer
     */
    public function replaceArgument($index, $value)
    {
    }
    /**
     * @internal
     */
    public function setAutoconfigured($autoconfigured) : self
    {
    }
    /**
     * @internal
     */
    public function setInstanceofConditionals(array $instanceof) : self
    {
    }
}
