<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator\Traits;

trait ParentTrait
{
    /**
     * Sets the Definition to inherit from.
     *
     * @return $this
     *
     * @throws InvalidArgumentException when parent cannot be set
     */
    public final function parent(string $parent) : self
    {
    }
}
