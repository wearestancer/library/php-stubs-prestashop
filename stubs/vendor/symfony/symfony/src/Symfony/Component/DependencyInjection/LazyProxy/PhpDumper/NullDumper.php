<?php

namespace Symfony\Component\DependencyInjection\LazyProxy\PhpDumper;

/**
 * Null dumper, negates any proxy code generation for any given service definition.
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 *
 * @final
 */
class NullDumper implements \Symfony\Component\DependencyInjection\LazyProxy\PhpDumper\DumperInterface
{
    /**
     * {@inheritdoc}
     */
    public function isProxyCandidate(\Symfony\Component\DependencyInjection\Definition $definition) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProxyFactoryCode(\Symfony\Component\DependencyInjection\Definition $definition, $id, $factoryCode = null) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProxyCode(\Symfony\Component\DependencyInjection\Definition $definition) : string
    {
    }
}
