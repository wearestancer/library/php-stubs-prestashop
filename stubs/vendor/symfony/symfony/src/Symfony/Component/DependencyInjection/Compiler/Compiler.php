<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * This class is used to remove circular dependencies between individual passes.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class Compiler
{
    public function __construct()
    {
    }
    /**
     * Returns the PassConfig.
     *
     * @return PassConfig The PassConfig instance
     */
    public function getPassConfig()
    {
    }
    /**
     * Returns the ServiceReferenceGraph.
     *
     * @return ServiceReferenceGraph The ServiceReferenceGraph instance
     */
    public function getServiceReferenceGraph()
    {
    }
    /**
     * Adds a pass to the PassConfig.
     *
     * @param string $type     The type of the pass
     * @param int    $priority Used to sort the passes
     */
    public function addPass(\Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface $pass, $type = \Symfony\Component\DependencyInjection\Compiler\PassConfig::TYPE_BEFORE_OPTIMIZATION, int $priority = 0)
    {
    }
    /**
     * @final
     */
    public function log(\Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface $pass, string $message)
    {
    }
    /**
     * Returns the log.
     *
     * @return array Log array
     */
    public function getLog()
    {
    }
    /**
     * Run the Compiler and process all Passes.
     */
    public function compile(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
