<?php

namespace Symfony\Component\DependencyInjection\ParameterBag;

/**
 * Holds read-only parameters.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FrozenParameterBag extends \Symfony\Component\DependencyInjection\ParameterBag\ParameterBag
{
    /**
     * For performance reasons, the constructor assumes that
     * all keys are already lowercased.
     *
     * This is always the case when used internally.
     *
     * @param array $parameters An array of parameters
     */
    public function __construct(array $parameters = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add(array $parameters)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
    }
}
