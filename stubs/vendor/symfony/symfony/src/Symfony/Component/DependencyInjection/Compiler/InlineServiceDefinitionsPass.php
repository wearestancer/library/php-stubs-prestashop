<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Inline service definitions where this is possible.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class InlineServiceDefinitionsPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass implements \Symfony\Component\DependencyInjection\Compiler\RepeatablePassInterface
{
    public function __construct(\Symfony\Component\DependencyInjection\Compiler\AnalyzeServiceReferencesPass $analyzingPass = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setRepeatedPass(\Symfony\Component\DependencyInjection\Compiler\RepeatedPass $repeatedPass)
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
