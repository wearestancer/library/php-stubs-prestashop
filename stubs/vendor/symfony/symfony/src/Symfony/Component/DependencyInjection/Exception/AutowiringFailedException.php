<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * Thrown when a definition cannot be autowired.
 */
class AutowiringFailedException extends \Symfony\Component\DependencyInjection\Exception\RuntimeException
{
    public function __construct(string $serviceId, $message = '', int $code = 0, \Throwable $previous = null)
    {
    }
    public function getMessageCallback() : ?\Closure
    {
    }
    public function getServiceId()
    {
    }
}
