<?php

namespace Symfony\Component\DependencyInjection\Argument;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class ServiceLocator extends \Symfony\Component\DependencyInjection\ServiceLocator
{
    public function __construct(\Closure $factory, array $serviceMap, array $serviceTypes = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function get($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getProvidedServices() : array
    {
    }
}
