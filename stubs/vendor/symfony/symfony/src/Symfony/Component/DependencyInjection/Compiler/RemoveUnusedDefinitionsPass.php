<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Removes unused service definitions from the container.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class RemoveUnusedDefinitionsPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass implements \Symfony\Component\DependencyInjection\Compiler\RepeatablePassInterface
{
    /**
     * {@inheritdoc}
     */
    public function setRepeatedPass(\Symfony\Component\DependencyInjection\Compiler\RepeatedPass $repeatedPass)
    {
    }
    /**
     * Processes the ContainerBuilder to remove unused definitions.
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
