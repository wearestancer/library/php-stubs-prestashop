<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Interface that must be implemented by compilation passes.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
interface CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container);
}
