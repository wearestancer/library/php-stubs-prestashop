<?php

namespace Symfony\Component\DependencyInjection\Exception;

/**
 * Base OutOfBoundsException for Dependency Injection component.
 */
class OutOfBoundsException extends \OutOfBoundsException implements \Symfony\Component\DependencyInjection\Exception\ExceptionInterface
{
}
