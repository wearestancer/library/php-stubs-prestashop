<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

abstract class AbstractServiceConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractConfigurator
{
    protected $parent;
    protected $id;
    public function __construct(\Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator $parent, \Symfony\Component\DependencyInjection\Definition $definition, string $id = null, array $defaultTags = [])
    {
    }
    public function __destruct()
    {
    }
    /**
     * Registers a service.
     */
    public final function set(?string $id, string $class = null) : \Symfony\Component\DependencyInjection\Loader\Configurator\ServiceConfigurator
    {
    }
    /**
     * Creates an alias.
     */
    public final function alias(string $id, string $referencedId) : \Symfony\Component\DependencyInjection\Loader\Configurator\AliasConfigurator
    {
    }
    /**
     * Registers a PSR-4 namespace using a glob pattern.
     */
    public final function load(string $namespace, string $resource) : \Symfony\Component\DependencyInjection\Loader\Configurator\PrototypeConfigurator
    {
    }
    /**
     * Gets an already defined service definition.
     *
     * @throws ServiceNotFoundException if the service definition does not exist
     */
    public final function get(string $id) : \Symfony\Component\DependencyInjection\Loader\Configurator\ServiceConfigurator
    {
    }
    /**
     * Registers a service.
     */
    public final function __invoke(string $id, string $class = null) : \Symfony\Component\DependencyInjection\Loader\Configurator\ServiceConfigurator
    {
    }
}
