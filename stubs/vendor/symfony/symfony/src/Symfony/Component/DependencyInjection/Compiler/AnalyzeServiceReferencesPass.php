<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Run this pass before passes that need to know more about the relation of
 * your services.
 *
 * This class will populate the ServiceReferenceGraph with information. You can
 * retrieve the graph in other passes from the compiler.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class AnalyzeServiceReferencesPass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass implements \Symfony\Component\DependencyInjection\Compiler\RepeatablePassInterface
{
    /**
     * @param bool $onlyConstructorArguments Sets this Service Reference pass to ignore method calls
     */
    public function __construct(bool $onlyConstructorArguments = false, bool $hasProxyDumper = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setRepeatedPass(\Symfony\Component\DependencyInjection\Compiler\RepeatedPass $repeatedPass)
    {
    }
    /**
     * Processes a ContainerBuilder object to populate the service reference graph.
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    protected function processValue($value, $isRoot = false)
    {
    }
}
