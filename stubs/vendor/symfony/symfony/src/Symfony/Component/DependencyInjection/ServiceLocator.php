<?php

namespace Symfony\Component\DependencyInjection;

/**
 * @author Robin Chalas <robin.chalas@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ServiceLocator implements \Symfony\Contracts\Service\ServiceProviderInterface
{
    use \Symfony\Contracts\Service\ServiceLocatorTrait {
        get as private doGet;
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function get($id)
    {
    }
    public function __invoke($id)
    {
    }
    /**
     * @internal
     *
     * @return static
     */
    public function withContext(string $externalId, \Symfony\Component\DependencyInjection\Container $container)
    {
    }
}
