<?php

namespace Symfony\Component\DependencyInjection\Compiler;

/**
 * Inspects existing service definitions and wires the autowired ones using the type hints of their classes.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class AutowirePass extends \Symfony\Component\DependencyInjection\Compiler\AbstractRecursivePass
{
    public function __construct(bool $throwOnAutowireException = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processValue($value, $isRoot = false)
    {
    }
}
