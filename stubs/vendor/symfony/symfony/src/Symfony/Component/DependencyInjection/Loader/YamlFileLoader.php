<?php

namespace Symfony\Component\DependencyInjection\Loader;

/**
 * YamlFileLoader loads YAML files service definitions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class YamlFileLoader extends \Symfony\Component\DependencyInjection\Loader\FileLoader
{
    protected $autoRegisterAliasesForSinglyImplementedInterfaces = false;
    /**
     * {@inheritdoc}
     */
    public function load($resource, $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
    }
    /**
     * Loads a YAML file.
     *
     * @param string $file
     *
     * @return array The file content
     *
     * @throws InvalidArgumentException when the given file is not a local file or when it does not exist
     */
    protected function loadFile($file)
    {
    }
}
