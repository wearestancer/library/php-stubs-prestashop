<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ContainerConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractConfigurator
{
    public const FACTORY = 'container';
    public function __construct(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \Symfony\Component\DependencyInjection\Loader\PhpFileLoader $loader, array &$instanceof, string $path, string $file)
    {
    }
    public final function extension(string $namespace, array $config)
    {
    }
    public final function import(string $resource, string $type = null, $ignoreErrors = false)
    {
    }
    public final function parameters() : \Symfony\Component\DependencyInjection\Loader\Configurator\ParametersConfigurator
    {
    }
    public final function services() : \Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator
    {
    }
}
/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * Creates a service reference.
 */
function ref(string $id) : \Symfony\Component\DependencyInjection\Loader\Configurator\ReferenceConfigurator
{
}
/**
 * Creates an inline service.
 */
function inline(string $class = null) : \Symfony\Component\DependencyInjection\Loader\Configurator\InlineServiceConfigurator
{
}
/**
 * Creates a service locator.
 *
 * @param ReferenceConfigurator[] $values
 */
function service_locator(array $values) : \Symfony\Component\DependencyInjection\Argument\ServiceLocatorArgument
{
}
/**
 * Creates a lazy iterator.
 *
 * @param ReferenceConfigurator[] $values
 */
function iterator(array $values) : \Symfony\Component\DependencyInjection\Argument\IteratorArgument
{
}
/**
 * Creates a lazy iterator by tag name.
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0, use "tagged_iterator" instead.
 */
function tagged(string $tag, string $indexAttribute = null, string $defaultIndexMethod = null) : \Symfony\Component\DependencyInjection\Argument\TaggedIteratorArgument
{
}
/**
 * Creates a lazy iterator by tag name.
 */
function tagged_iterator(string $tag, string $indexAttribute = null, string $defaultIndexMethod = null, string $defaultPriorityMethod = null) : \Symfony\Component\DependencyInjection\Argument\TaggedIteratorArgument
{
}
/**
 * Creates a service locator by tag name.
 */
function tagged_locator(string $tag, string $indexAttribute = null, string $defaultIndexMethod = null) : \Symfony\Component\DependencyInjection\Argument\ServiceLocatorArgument
{
}
/**
 * Creates an expression.
 */
function expr(string $expression) : \Symfony\Component\ExpressionLanguage\Expression
{
}
