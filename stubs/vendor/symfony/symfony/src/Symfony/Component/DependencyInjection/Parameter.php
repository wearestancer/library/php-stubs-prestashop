<?php

namespace Symfony\Component\DependencyInjection;

/**
 * Parameter represents a parameter reference.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Parameter
{
    public function __construct(string $id)
    {
    }
    /**
     * @return string The parameter key
     */
    public function __toString()
    {
    }
}
