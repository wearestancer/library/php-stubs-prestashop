<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class PrototypeConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractServiceConfigurator
{
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AbstractTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ArgumentTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AutoconfigureTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AutowireTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\BindTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\CallTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ConfiguratorTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\DeprecateTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\FactoryTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\LazyTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ParentTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\PropertyTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\PublicTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ShareTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\TagTrait;
    public const FACTORY = 'load';
    public function __construct(\Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator $parent, \Symfony\Component\DependencyInjection\Loader\PhpFileLoader $loader, \Symfony\Component\DependencyInjection\Definition $defaults, string $namespace, string $resource, bool $allowParent)
    {
    }
    public function __destruct()
    {
    }
    /**
     * Excludes files from registration using glob patterns.
     *
     * @param string[]|string $excludes
     *
     * @return $this
     */
    public final function exclude($excludes) : self
    {
    }
}
