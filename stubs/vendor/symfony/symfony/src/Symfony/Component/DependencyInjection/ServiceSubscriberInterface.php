<?php

namespace Symfony\Component\DependencyInjection;

/**
 * {@inheritdoc}
 *
 * @deprecated since Symfony 4.2, use Symfony\Contracts\Service\ServiceSubscriberInterface instead.
 */
interface ServiceSubscriberInterface extends \Symfony\Contracts\Service\ServiceSubscriberInterface
{
}
