<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ServiceConfigurator extends \Symfony\Component\DependencyInjection\Loader\Configurator\AbstractServiceConfigurator
{
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AbstractTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ArgumentTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AutoconfigureTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\AutowireTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\BindTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\CallTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ClassTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ConfiguratorTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\DecorateTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\DeprecateTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\FactoryTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\FileTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\LazyTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ParentTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\PropertyTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\PublicTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\ShareTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\SyntheticTrait;
    use \Symfony\Component\DependencyInjection\Loader\Configurator\Traits\TagTrait;
    public const FACTORY = 'services';
    public function __construct(\Symfony\Component\DependencyInjection\ContainerBuilder $container, array $instanceof, bool $allowParent, \Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator $parent, \Symfony\Component\DependencyInjection\Definition $definition, ?string $id, array $defaultTags, string $path = null)
    {
    }
    public function __destruct()
    {
    }
}
