<?php

namespace Symfony\Component\Yaml\Tag;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Guilhem N. <egetick@gmail.com>
 */
final class TaggedValue
{
    public function __construct(string $tag, $value)
    {
    }
    public function getTag() : string
    {
    }
    public function getValue()
    {
    }
}
