<?php

namespace Symfony\Component\HttpFoundation\File\Exception;

/**
 * Thrown when the access on a file was denied.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class AccessDeniedException extends \Symfony\Component\HttpFoundation\File\Exception\FileException
{
    public function __construct(string $path)
    {
    }
}
