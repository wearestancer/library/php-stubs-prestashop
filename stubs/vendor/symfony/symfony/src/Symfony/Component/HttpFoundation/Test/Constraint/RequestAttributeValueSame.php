<?php

namespace Symfony\Component\HttpFoundation\Test\Constraint;

final class RequestAttributeValueSame extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $name, string $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
