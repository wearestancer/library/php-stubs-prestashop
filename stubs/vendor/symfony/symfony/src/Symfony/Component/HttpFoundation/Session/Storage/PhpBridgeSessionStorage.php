<?php

namespace Symfony\Component\HttpFoundation\Session\Storage;

/**
 * Allows session to be started by PHP and managed by Symfony.
 *
 * @author Drak <drak@zikula.org>
 */
class PhpBridgeSessionStorage extends \Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage
{
    /**
     * @param AbstractProxy|\SessionHandlerInterface|null $handler
     */
    public function __construct($handler = null, \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag $metaBag = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function start()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
