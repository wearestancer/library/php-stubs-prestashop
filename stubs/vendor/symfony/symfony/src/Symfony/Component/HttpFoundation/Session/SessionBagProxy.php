<?php

namespace Symfony\Component\HttpFoundation\Session;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class SessionBagProxy implements \Symfony\Component\HttpFoundation\Session\SessionBagInterface
{
    public function __construct(\Symfony\Component\HttpFoundation\Session\SessionBagInterface $bag, array &$data, ?int &$usageIndex)
    {
    }
    public function getBag() : \Symfony\Component\HttpFoundation\Session\SessionBagInterface
    {
    }
    public function isEmpty() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initialize(array &$array) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStorageKey() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
