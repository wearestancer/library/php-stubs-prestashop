<?php

namespace Symfony\Component\HttpFoundation\Session\Storage\Handler;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class SessionHandlerFactory
{
    /**
     * @param \Redis|\RedisArray|\RedisCluster|\Predis\ClientInterface|RedisProxy|RedisClusterProxy|\Memcached|\PDO|string $connection Connection or DSN
     */
    public static function createHandler($connection) : \Symfony\Component\HttpFoundation\Session\Storage\Handler\AbstractSessionHandler
    {
    }
}
