<?php

namespace Symfony\Component\HttpFoundation\File\MimeType;

/**
 * A singleton mime type to file extension guesser.
 *
 * A default guesser is provided.
 * You can register custom guessers by calling the register()
 * method on the singleton instance:
 *
 *     $guesser = ExtensionGuesser::getInstance();
 *     $guesser->register(new MyCustomExtensionGuesser());
 *
 * The last registered guesser is preferred over previously registered ones.
 *
 * @deprecated since Symfony 4.3, use {@link MimeTypes} instead
 */
class ExtensionGuesser implements \Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesserInterface
{
    /**
     * All registered ExtensionGuesserInterface instances.
     *
     * @var array
     */
    protected $guessers = [];
    /**
     * Returns the singleton instance.
     *
     * @return self
     */
    public static function getInstance()
    {
    }
    /**
     * Registers a new extension guesser.
     *
     * When guessing, this guesser is preferred over previously registered ones.
     */
    public function register(\Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesserInterface $guesser)
    {
    }
    /**
     * Tries to guess the extension.
     *
     * The mime type is passed to each registered mime type guesser in reverse order
     * of their registration (last registered is queried first). Once a guesser
     * returns a value that is not NULL, this method terminates and returns the
     * value.
     *
     * @param string $mimeType The mime type
     *
     * @return string The guessed extension or NULL, if none could be guessed
     */
    public function guess($mimeType)
    {
    }
}
