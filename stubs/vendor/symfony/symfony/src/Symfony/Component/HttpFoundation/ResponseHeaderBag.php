<?php

namespace Symfony\Component\HttpFoundation;

/**
 * ResponseHeaderBag is a container for Response HTTP headers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ResponseHeaderBag extends \Symfony\Component\HttpFoundation\HeaderBag
{
    public const COOKIES_FLAT = 'flat';
    public const COOKIES_ARRAY = 'array';
    public const DISPOSITION_ATTACHMENT = 'attachment';
    public const DISPOSITION_INLINE = 'inline';
    protected $computedCacheControl = [];
    protected $cookies = [];
    protected $headerNames = [];
    public function __construct(array $headers = [])
    {
    }
    /**
     * Returns the headers, with original capitalizations.
     *
     * @return array An array of headers
     */
    public function allPreserveCase()
    {
    }
    public function allPreserveCaseWithoutCookies()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function replace(array $headers = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string|null $key The name of the headers to return or null to get them all
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($key, $values, $replace = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasCacheControlDirective($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCacheControlDirective($key)
    {
    }
    public function setCookie(\Symfony\Component\HttpFoundation\Cookie $cookie)
    {
    }
    /**
     * Removes a cookie from the array, but does not unset it in the browser.
     *
     * @param string $name
     * @param string $path
     * @param string $domain
     */
    public function removeCookie($name, $path = '/', $domain = null)
    {
    }
    /**
     * Returns an array with all cookies.
     *
     * @param string $format
     *
     * @return Cookie[]
     *
     * @throws \InvalidArgumentException When the $format is invalid
     */
    public function getCookies($format = self::COOKIES_FLAT)
    {
    }
    /**
     * Clears a cookie in the browser.
     *
     * @param string $name
     * @param string $path
     * @param string $domain
     * @param bool   $secure
     * @param bool   $httpOnly
     * @param string $sameSite
     */
    public function clearCookie($name, $path = '/', $domain = null, $secure = false, $httpOnly = true)
    {
    }
    /**
     * @see HeaderUtils::makeDisposition()
     */
    public function makeDisposition($disposition, $filename, $filenameFallback = '')
    {
    }
    /**
     * Returns the calculated value of the cache-control header.
     *
     * This considers several other headers and calculates or modifies the
     * cache-control header to a sensible, conservative value.
     *
     * @return string
     */
    protected function computeCacheControlValue()
    {
    }
}
