<?php

namespace Symfony\Component\HttpFoundation;

/**
 * Request represents an HTTP request from an Apache server.
 *
 * @deprecated since Symfony 4.4. Use the Request class instead.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ApacheRequest extends \Symfony\Component\HttpFoundation\Request
{
    /**
     * {@inheritdoc}
     */
    protected function prepareRequestUri()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function prepareBaseUrl()
    {
    }
}
