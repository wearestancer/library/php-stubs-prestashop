<?php

namespace Symfony\Component\HttpFoundation;

/**
 * Represents an Accept-* header.
 *
 * An accept header is compound with a list of items,
 * sorted by descending quality.
 *
 * @author Jean-François Simon <contact@jfsimon.fr>
 */
class AcceptHeader
{
    /**
     * @param AcceptHeaderItem[] $items
     */
    public function __construct(array $items)
    {
    }
    /**
     * Builds an AcceptHeader instance from a string.
     *
     * @param string $headerValue
     *
     * @return self
     */
    public static function fromString($headerValue)
    {
    }
    /**
     * Returns header value's string representation.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Tests if header has given value.
     *
     * @param string $value
     *
     * @return bool
     */
    public function has($value)
    {
    }
    /**
     * Returns given value's item, if exists.
     *
     * @param string $value
     *
     * @return AcceptHeaderItem|null
     */
    public function get($value)
    {
    }
    /**
     * Adds an item.
     *
     * @return $this
     */
    public function add(\Symfony\Component\HttpFoundation\AcceptHeaderItem $item)
    {
    }
    /**
     * Returns all items.
     *
     * @return AcceptHeaderItem[]
     */
    public function all()
    {
    }
    /**
     * Filters items on their value using given regex.
     *
     * @param string $pattern
     *
     * @return self
     */
    public function filter($pattern)
    {
    }
    /**
     * Returns first item.
     *
     * @return AcceptHeaderItem|null
     */
    public function first()
    {
    }
}
