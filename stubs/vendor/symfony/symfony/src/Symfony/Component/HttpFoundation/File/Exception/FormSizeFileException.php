<?php

namespace Symfony\Component\HttpFoundation\File\Exception;

/**
 * Thrown when an UPLOAD_ERR_FORM_SIZE error occurred with UploadedFile.
 *
 * @author Florent Mata <florentmata@gmail.com>
 */
class FormSizeFileException extends \Symfony\Component\HttpFoundation\File\Exception\FileException
{
}
