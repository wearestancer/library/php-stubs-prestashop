<?php

namespace Symfony\Component\HttpFoundation\Test\Constraint;

final class ResponseHasHeader extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $headerName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
