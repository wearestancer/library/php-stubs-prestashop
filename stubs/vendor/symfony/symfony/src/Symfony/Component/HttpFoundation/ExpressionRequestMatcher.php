<?php

namespace Symfony\Component\HttpFoundation;

/**
 * ExpressionRequestMatcher uses an expression to match a Request.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ExpressionRequestMatcher extends \Symfony\Component\HttpFoundation\RequestMatcher
{
    public function setExpression(\Symfony\Component\ExpressionLanguage\ExpressionLanguage $language, $expression)
    {
    }
    public function matches(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
