<?php

namespace Symfony\Component\HttpFoundation\Session\Storage;

/**
 * MockFileSessionStorage is used to mock sessions for
 * functional testing where you may need to persist session data
 * across separate PHP processes.
 *
 * No PHP session is actually started since a session can be initialized
 * and shutdown only once per PHP execution cycle and this class does
 * not pollute any session related globals, including session_*() functions
 * or session.* PHP ini directives.
 *
 * @author Drak <drak@zikula.org>
 */
class MockFileSessionStorage extends \Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage
{
    /**
     * @param string $savePath Path of directory to save session files
     * @param string $name     Session name
     */
    public function __construct(string $savePath = null, string $name = 'MOCKSESSID', \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag $metaBag = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function start()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function regenerate($destroy = false, $lifetime = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save()
    {
    }
}
