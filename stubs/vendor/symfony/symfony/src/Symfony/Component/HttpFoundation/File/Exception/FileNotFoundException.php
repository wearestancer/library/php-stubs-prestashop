<?php

namespace Symfony\Component\HttpFoundation\File\Exception;

/**
 * Thrown when a file was not found.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FileNotFoundException extends \Symfony\Component\HttpFoundation\File\Exception\FileException
{
    public function __construct(string $path)
    {
    }
}
