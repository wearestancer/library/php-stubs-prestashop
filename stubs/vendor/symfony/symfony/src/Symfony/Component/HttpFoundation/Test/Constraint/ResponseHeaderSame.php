<?php

namespace Symfony\Component\HttpFoundation\Test\Constraint;

final class ResponseHeaderSame extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $headerName, string $expectedValue)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
