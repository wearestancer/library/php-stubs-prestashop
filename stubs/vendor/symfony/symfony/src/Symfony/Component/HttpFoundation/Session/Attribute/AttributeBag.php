<?php

namespace Symfony\Component\HttpFoundation\Session\Attribute;

/**
 * This class relates to session attribute storage.
 */
class AttributeBag implements \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface, \IteratorAggregate, \Countable
{
    protected $attributes = [];
    /**
     * @param string $storageKey The key used to store attributes in the session
     */
    public function __construct(string $storageKey = '_sf2_attributes')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    public function setName($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initialize(array &$attributes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStorageKey()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function replace(array $attributes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * Returns an iterator for attributes.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * Returns the number of attributes.
     *
     * @return int The number of attributes
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
}
