<?php

namespace Symfony\Component\HttpFoundation\Test\Constraint;

final class ResponseStatusCodeSame extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(int $statusCode)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
