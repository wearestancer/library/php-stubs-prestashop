<?php

namespace Symfony\Component\HttpFoundation\Session\Storage;

/**
 * MockArraySessionStorage mocks the session for unit tests.
 *
 * No PHP session is actually started since a session can be initialized
 * and shutdown only once per PHP execution cycle.
 *
 * When doing functional testing, you should use MockFileSessionStorage instead.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bulat Shakirzyanov <mallluhuct@gmail.com>
 * @author Drak <drak@zikula.org>
 */
class MockArraySessionStorage implements \Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface
{
    /**
     * @var string
     */
    protected $id = '';
    /**
     * @var string
     */
    protected $name;
    /**
     * @var bool
     */
    protected $started = false;
    /**
     * @var bool
     */
    protected $closed = false;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var MetadataBag
     */
    protected $metadataBag;
    /**
     * @var array|SessionBagInterface[]
     */
    protected $bags = [];
    public function __construct(string $name = 'MOCKSESSID', \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag $metaBag = null)
    {
    }
    public function setSessionData(array $array)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function start()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function regenerate($destroy = false, $lifetime = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function registerBag(\Symfony\Component\HttpFoundation\Session\SessionBagInterface $bag)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBag($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isStarted()
    {
    }
    public function setMetadataBag(\Symfony\Component\HttpFoundation\Session\Storage\MetadataBag $bag = null)
    {
    }
    /**
     * Gets the MetadataBag.
     *
     * @return MetadataBag
     */
    public function getMetadataBag()
    {
    }
    /**
     * Generates a session ID.
     *
     * This doesn't need to be particularly cryptographically secure since this is just
     * a mock.
     *
     * @return string
     */
    protected function generateId()
    {
    }
    protected function loadSession()
    {
    }
}
