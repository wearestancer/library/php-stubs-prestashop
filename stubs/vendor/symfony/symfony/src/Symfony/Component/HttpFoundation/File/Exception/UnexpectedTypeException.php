<?php

namespace Symfony\Component\HttpFoundation\File\Exception;

class UnexpectedTypeException extends \Symfony\Component\HttpFoundation\File\Exception\FileException
{
    public function __construct($value, string $expectedType)
    {
    }
}
