<?php

namespace Symfony\Component\HttpFoundation\Session\Storage\Handler;

/**
 * Adds basic `SessionUpdateTimestampHandlerInterface` behaviors to another `SessionHandlerInterface`.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class StrictSessionHandler extends \Symfony\Component\HttpFoundation\Session\Storage\Handler\AbstractSessionHandler
{
    public function __construct(\SessionHandlerInterface $handler)
    {
    }
    /**
     * Returns true if this handler wraps an internal PHP session save handler using \SessionHandler.
     *
     * @internal
     */
    public function isWrapper() : bool
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function open($savePath, $sessionName)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doRead($sessionId)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function updateTimestamp($sessionId, $data)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doWrite($sessionId, $data)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function destroy($sessionId)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDestroy($sessionId)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function close()
    {
    }
    /**
     * @return int|false
     */
    #[\ReturnTypeWillChange]
    public function gc($maxlifetime)
    {
    }
}
