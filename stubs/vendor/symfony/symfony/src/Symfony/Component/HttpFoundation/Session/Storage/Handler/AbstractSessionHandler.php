<?php

namespace Symfony\Component\HttpFoundation\Session\Storage\Handler;

/**
 * This abstract session handler provides a generic implementation
 * of the PHP 7.0 SessionUpdateTimestampHandlerInterface,
 * enabling strict and lazy session handling.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
abstract class AbstractSessionHandler implements \SessionHandlerInterface, \SessionUpdateTimestampHandlerInterface
{
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function open($savePath, $sessionName)
    {
    }
    /**
     * @param string $sessionId
     *
     * @return string
     */
    protected abstract function doRead($sessionId);
    /**
     * @param string $sessionId
     * @param string $data
     *
     * @return bool
     */
    protected abstract function doWrite($sessionId, $data);
    /**
     * @param string $sessionId
     *
     * @return bool
     */
    protected abstract function doDestroy($sessionId);
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function validateId($sessionId)
    {
    }
    /**
     * @return string
     */
    #[\ReturnTypeWillChange]
    public function read($sessionId)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function write($sessionId, $data)
    {
    }
    /**
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function destroy($sessionId)
    {
    }
}
