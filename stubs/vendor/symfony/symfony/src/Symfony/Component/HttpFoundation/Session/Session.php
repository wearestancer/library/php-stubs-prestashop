<?php

namespace Symfony\Component\HttpFoundation\Session;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Drak <drak@zikula.org>
 */
class Session implements \Symfony\Component\HttpFoundation\Session\SessionInterface, \IteratorAggregate, \Countable
{
    protected $storage;
    public function __construct(\Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface $storage = null, \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface $attributes = null, \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface $flashes = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function start()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function replace(array $attributes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isStarted()
    {
    }
    /**
     * Returns an iterator for attributes.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
    /**
     * Returns the number of attributes.
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count()
    {
    }
    public function &getUsageIndex() : int
    {
    }
    /**
     * @internal
     */
    public function isEmpty() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function invalidate($lifetime = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function migrate($destroy = false, $lifetime = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadataBag()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function registerBag(\Symfony\Component\HttpFoundation\Session\SessionBagInterface $bag)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBag($name)
    {
    }
    /**
     * Gets the flashbag interface.
     *
     * @return FlashBagInterface
     */
    public function getFlashBag()
    {
    }
}
