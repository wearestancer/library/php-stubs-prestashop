<?php

namespace Symfony\Component\HttpFoundation;

/**
 * A helper service for manipulating URLs within and outside the request scope.
 *
 * @author Valentin Udaltsov <udaltsov.valentin@gmail.com>
 */
final class UrlHelper
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack, \Symfony\Component\Routing\RequestContext $requestContext = null)
    {
    }
    public function getAbsoluteUrl(string $path) : string
    {
    }
    public function getRelativePath(string $path) : string
    {
    }
}
