<?php

namespace Symfony\Component\HttpFoundation\Test\Constraint;

final class ResponseCookieValueSame extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $name, string $value, string $path = '/', string $domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
