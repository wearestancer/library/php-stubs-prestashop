<?php

namespace Symfony\Component\HttpFoundation;

/**
 * Represents an Accept-* header item.
 *
 * @author Jean-François Simon <contact@jfsimon.fr>
 */
class AcceptHeaderItem
{
    public function __construct(string $value, array $attributes = [])
    {
    }
    /**
     * Builds an AcceptHeaderInstance instance from a string.
     *
     * @param string $itemValue
     *
     * @return self
     */
    public static function fromString($itemValue)
    {
    }
    /**
     * Returns header value's string representation.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Set the item value.
     *
     * @param string $value
     *
     * @return $this
     */
    public function setValue($value)
    {
    }
    /**
     * Returns the item value.
     *
     * @return string
     */
    public function getValue()
    {
    }
    /**
     * Set the item quality.
     *
     * @param float $quality
     *
     * @return $this
     */
    public function setQuality($quality)
    {
    }
    /**
     * Returns the item quality.
     *
     * @return float
     */
    public function getQuality()
    {
    }
    /**
     * Set the item index.
     *
     * @param int $index
     *
     * @return $this
     */
    public function setIndex($index)
    {
    }
    /**
     * Returns the item index.
     *
     * @return int
     */
    public function getIndex()
    {
    }
    /**
     * Tests if an attribute exists.
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasAttribute($name)
    {
    }
    /**
     * Returns an attribute by its name.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getAttribute($name, $default = null)
    {
    }
    /**
     * Returns all attributes.
     *
     * @return array
     */
    public function getAttributes()
    {
    }
    /**
     * Set an attribute.
     *
     * @param string $name
     * @param string $value
     *
     * @return $this
     */
    public function setAttribute($name, $value)
    {
    }
}
