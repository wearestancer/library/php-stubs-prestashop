<?php

namespace Symfony\Component\HttpFoundation\Test\Constraint;

final class ResponseHasCookie extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $name, string $path = '/', string $domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
