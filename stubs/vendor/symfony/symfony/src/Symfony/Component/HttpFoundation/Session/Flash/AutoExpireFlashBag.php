<?php

namespace Symfony\Component\HttpFoundation\Session\Flash;

/**
 * AutoExpireFlashBag flash message container.
 *
 * @author Drak <drak@zikula.org>
 */
class AutoExpireFlashBag implements \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
{
    /**
     * @param string $storageKey The key used to store flashes in the session
     */
    public function __construct(string $storageKey = '_symfony_flashes')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    public function setName($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initialize(array &$flashes)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function add($type, $message)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function peek($type, array $default = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function peekAll()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($type, array $default = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAll(array $messages)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($type, $messages)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($type)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function keys()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStorageKey()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
