<?php

namespace Symfony\Component\ErrorHandler\Exception;

/**
 * FlattenException wraps a PHP Error or Exception to be able to serialize it.
 *
 * Basically, this class removes all objects from the trace.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FlattenException extends \Symfony\Component\Debug\Exception\FlattenException
{
    /**
     * @return static
     */
    public static function create(\Exception $exception, $statusCode = null, array $headers = []) : self
    {
    }
    /**
     * @return static
     */
    public static function createFromThrowable(\Throwable $exception, int $statusCode = null, array $headers = []) : self
    {
    }
    public function toArray() : array
    {
    }
    public function getStatusCode() : int
    {
    }
    /**
     * @param int $code
     *
     * @return $this
     */
    public function setStatusCode($code) : self
    {
    }
    public function getHeaders() : array
    {
    }
    /**
     * @return $this
     */
    public function setHeaders(array $headers) : self
    {
    }
    public function getClass() : string
    {
    }
    /**
     * @param string $class
     *
     * @return $this
     */
    public function setClass($class) : self
    {
    }
    public function getFile() : string
    {
    }
    /**
     * @param string $file
     *
     * @return $this
     */
    public function setFile($file) : self
    {
    }
    public function getLine() : int
    {
    }
    /**
     * @param int $line
     *
     * @return $this
     */
    public function setLine($line) : self
    {
    }
    public function getStatusText() : string
    {
    }
    public function setStatusText(string $statusText) : self
    {
    }
    public function getMessage() : string
    {
    }
    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message) : self
    {
    }
    /**
     * @return int|string int most of the time (might be a string with PDOException)
     */
    public function getCode()
    {
    }
    /**
     * @param int|string $code
     *
     * @return $this
     */
    public function setCode($code) : self
    {
    }
    /**
     * @return self|null
     */
    public function getPrevious()
    {
    }
    /**
     * @return $this
     */
    public final function setPrevious(?\Symfony\Component\Debug\Exception\FlattenException $previous) : self
    {
    }
    /**
     * @return self[]
     */
    public function getAllPrevious() : array
    {
    }
    public function getTrace() : array
    {
    }
    /**
     * @deprecated since 4.1, use {@see setTraceFromThrowable()} instead.
     */
    public function setTraceFromException(\Exception $exception)
    {
    }
    /**
     * @return $this
     */
    public function setTraceFromThrowable(\Throwable $throwable) : self
    {
    }
    /**
     * @param array       $trace
     * @param string|null $file
     * @param int|null    $line
     *
     * @return $this
     */
    public function setTrace($trace, $file, $line) : self
    {
    }
    public function getTraceAsString() : string
    {
    }
    /**
     * @return $this
     */
    public function setAsString(?string $asString) : self
    {
    }
    public function getAsString() : string
    {
    }
}
