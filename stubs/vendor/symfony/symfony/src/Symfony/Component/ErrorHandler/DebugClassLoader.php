<?php

namespace Symfony\Component\ErrorHandler;

/**
 * Autoloader checking if the class is really defined in the file found.
 *
 * The ClassLoader will wrap all registered autoloaders
 * and will throw an exception if a file is found but does
 * not declare the class.
 *
 * It can also patch classes to turn docblocks into actual return types.
 * This behavior is controlled by the SYMFONY_PATCH_TYPE_DECLARATIONS env var,
 * which is a url-encoded array with the follow parameters:
 *  - "force": any value enables deprecation notices - can be any of:
 *      - "docblock" to patch only docblock annotations
 *      - "object" to turn union types to the "object" type when possible (not recommended)
 *      - "1" to add all possible return types including magic methods
 *      - "0" to add possible return types excluding magic methods
 *  - "php": the target version of PHP - e.g. "7.1" doesn't generate "object" types
 *  - "deprecations": "1" to trigger a deprecation notice when a child class misses a
 *                    return type while the parent declares an "@return" annotation
 *
 * Note that patching doesn't care about any coding style so you'd better to run
 * php-cs-fixer after, with rules "phpdoc_trim_consecutive_blank_line_separation"
 * and "no_superfluous_phpdoc_tags" enabled typically.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Christophe Coevoet <stof@notk.org>
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Guilhem Niot <guilhem.niot@gmail.com>
 */
class DebugClassLoader
{
    public function __construct(callable $classLoader)
    {
    }
    /**
     * Gets the wrapped class loader.
     *
     * @return callable The wrapped class loader
     */
    public function getClassLoader() : callable
    {
    }
    /**
     * Wraps all autoloaders.
     */
    public static function enable() : void
    {
    }
    /**
     * Disables the wrapping.
     */
    public static function disable() : void
    {
    }
    public static function checkClasses() : bool
    {
    }
    public function findFile(string $class) : ?string
    {
    }
    /**
     * Loads the given class or interface.
     *
     * @throws \RuntimeException
     */
    public function loadClass(string $class) : void
    {
    }
    public function checkAnnotations(\ReflectionClass $refl, string $class) : array
    {
    }
    public function checkCase(\ReflectionClass $refl, string $file, string $class) : ?array
    {
    }
}
