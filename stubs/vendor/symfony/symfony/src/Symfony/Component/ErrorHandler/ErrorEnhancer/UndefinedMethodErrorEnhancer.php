<?php

namespace Symfony\Component\ErrorHandler\ErrorEnhancer;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class UndefinedMethodErrorEnhancer implements \Symfony\Component\ErrorHandler\ErrorEnhancer\ErrorEnhancerInterface
{
    /**
     * {@inheritdoc}
     */
    public function enhance(\Throwable $error) : ?\Throwable
    {
    }
}
