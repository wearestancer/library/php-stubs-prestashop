<?php

namespace Symfony\Component\ErrorHandler\Error;

class ClassNotFoundError extends \Error
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $message, \Throwable $previous)
    {
    }
}
