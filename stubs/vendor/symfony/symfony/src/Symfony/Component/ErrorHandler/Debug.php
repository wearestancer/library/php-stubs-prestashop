<?php

namespace Symfony\Component\ErrorHandler;

/**
 * Registers all the debug tools.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Debug
{
    public static function enable() : \Symfony\Component\ErrorHandler\ErrorHandler
    {
    }
}
