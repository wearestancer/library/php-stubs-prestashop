<?php

namespace Symfony\Component\ErrorHandler\Error;

class UndefinedMethodError extends \Error
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $message, \Throwable $previous)
    {
    }
}
