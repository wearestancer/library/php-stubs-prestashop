<?php

namespace Symfony\Component\ErrorHandler\Error;

class FatalError extends \Error
{
    /**
     * {@inheritdoc}
     *
     * @param array $error An array as returned by error_get_last()
     */
    public function __construct(string $message, int $code, array $error, int $traceOffset = null, bool $traceArgs = true, array $trace = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getError() : array
    {
    }
}
