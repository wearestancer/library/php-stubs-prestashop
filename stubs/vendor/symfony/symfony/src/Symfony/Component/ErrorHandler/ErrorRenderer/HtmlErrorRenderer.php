<?php

namespace Symfony\Component\ErrorHandler\ErrorRenderer;

/**
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class HtmlErrorRenderer implements \Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface
{
    /**
     * @param bool|callable                 $debug          The debugging mode as a boolean or a callable that should return it
     * @param string|FileLinkFormatter|null $fileLinkFormat
     * @param bool|callable                 $outputBuffer   The output buffer as a string or a callable that should return it
     */
    public function __construct($debug = false, string $charset = null, $fileLinkFormat = null, string $projectDir = null, $outputBuffer = '', \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render(\Throwable $exception) : \Symfony\Component\ErrorHandler\Exception\FlattenException
    {
    }
    /**
     * Gets the HTML content associated with the given exception.
     */
    public function getBody(\Symfony\Component\ErrorHandler\Exception\FlattenException $exception) : string
    {
    }
    /**
     * Gets the stylesheet associated with the given exception.
     */
    public function getStylesheet() : string
    {
    }
    public static function isDebug(\Symfony\Component\HttpFoundation\RequestStack $requestStack, bool $debug) : \Closure
    {
    }
    public static function getAndCleanOutputBuffer(\Symfony\Component\HttpFoundation\RequestStack $requestStack) : \Closure
    {
    }
}
