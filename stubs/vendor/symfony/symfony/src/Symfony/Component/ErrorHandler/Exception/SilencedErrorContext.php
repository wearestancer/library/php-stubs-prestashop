<?php

namespace Symfony\Component\ErrorHandler\Exception;

/**
 * Data Object that represents a Silenced Error.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
class SilencedErrorContext implements \JsonSerializable
{
    public $count = 1;
    public function __construct(int $severity, string $file, int $line, array $trace = [], int $count = 1)
    {
    }
    public function getSeverity() : int
    {
    }
    public function getFile() : string
    {
    }
    public function getLine() : int
    {
    }
    public function getTrace() : array
    {
    }
    public function jsonSerialize() : array
    {
    }
}
