<?php

namespace Symfony\Component\ErrorHandler\Error;

class UndefinedFunctionError extends \Error
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $message, \Throwable $previous)
    {
    }
}
