<?php

namespace Symfony\Component\ErrorHandler\ErrorRenderer;

/**
 * Formats an exception using Serializer for rendering.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class SerializerErrorRenderer implements \Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface
{
    /**
     * @param string|callable(FlattenException) $format The format as a string or a callable that should return it
     *                                                  formats not supported by Request::getMimeTypes() should be given as mime types
     * @param bool|callable                     $debug  The debugging mode as a boolean or a callable that should return it
     */
    public function __construct(\Symfony\Component\Serializer\SerializerInterface $serializer, $format, \Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface $fallbackErrorRenderer = null, $debug = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render(\Throwable $exception) : \Symfony\Component\ErrorHandler\Exception\FlattenException
    {
    }
    public static function getPreferredFormat(\Symfony\Component\HttpFoundation\RequestStack $requestStack) : \Closure
    {
    }
}
