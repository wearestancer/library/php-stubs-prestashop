<?php

namespace Symfony\Component\ErrorHandler\ErrorRenderer;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CliErrorRenderer implements \Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface
{
    /**
     * {@inheritdoc}
     */
    public function render(\Throwable $exception) : \Symfony\Component\ErrorHandler\Exception\FlattenException
    {
    }
}
