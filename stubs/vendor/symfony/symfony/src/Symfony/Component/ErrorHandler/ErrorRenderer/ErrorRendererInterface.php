<?php

namespace Symfony\Component\ErrorHandler\ErrorRenderer;

/**
 * Formats an exception to be used as response content.
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
interface ErrorRendererInterface
{
    /**
     * Renders a Throwable as a FlattenException.
     */
    public function render(\Throwable $exception) : \Symfony\Component\ErrorHandler\Exception\FlattenException;
}
