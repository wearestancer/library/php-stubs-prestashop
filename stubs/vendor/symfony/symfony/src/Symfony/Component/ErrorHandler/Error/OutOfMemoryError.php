<?php

namespace Symfony\Component\ErrorHandler\Error;

class OutOfMemoryError extends \Symfony\Component\ErrorHandler\Error\FatalError
{
}
