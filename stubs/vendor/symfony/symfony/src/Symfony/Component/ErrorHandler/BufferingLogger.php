<?php

namespace Symfony\Component\ErrorHandler;

/**
 * A buffering logger that stacks logs for later.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class BufferingLogger extends \Psr\Log\AbstractLogger
{
    public function log($level, $message, array $context = []) : void
    {
    }
    public function cleanLogs() : array
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
