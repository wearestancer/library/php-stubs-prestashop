<?php

namespace Symfony\Component\Lock\Strategy;

/**
 * UnanimousStrategy is a StrategyInterface implementation where 100% of elements should be successful.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class UnanimousStrategy implements \Symfony\Component\Lock\Strategy\StrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function isMet($numberOfSuccess, $numberOfItems)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canBeMet($numberOfFailure, $numberOfItems)
    {
    }
}
