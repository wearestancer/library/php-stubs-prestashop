<?php

namespace Symfony\Component\Lock\Exception;

/**
 * Base ExceptionInterface for the Lock Component.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
interface ExceptionInterface extends \Throwable
{
}
