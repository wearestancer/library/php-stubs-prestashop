<?php

namespace Symfony\Component\Lock;

/**
 * StoreInterface defines an interface to manipulate a lock store.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 *
 * @deprecated since Symfony 4.4, use PersistingStoreInterface and BlockingStoreInterface instead
 */
interface StoreInterface extends \Symfony\Component\Lock\PersistingStoreInterface
{
    /**
     * Waits until a key becomes free, then stores the resource.
     *
     * If the store does not support this feature it should throw a NotSupportedException.
     *
     * @throws LockConflictedException
     * @throws NotSupportedException
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key);
}
