<?php

namespace Symfony\Component\Lock\Exception;

/**
 * @author Amrouche Hamza <hamza.simperfit@gmail.com>
 */
class InvalidTtlException extends \Symfony\Component\Lock\Exception\InvalidArgumentException implements \Symfony\Component\Lock\Exception\ExceptionInterface
{
}
