<?php

namespace Symfony\Component\Lock\Store;

/**
 * RetryTillSaveStore is a PersistingStoreInterface implementation which decorate a non blocking PersistingStoreInterface to provide a
 * blocking storage.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class RetryTillSaveStore implements \Symfony\Component\Lock\BlockingStoreInterface, \Symfony\Component\Lock\StoreInterface, \Psr\Log\LoggerAwareInterface
{
    use \Psr\Log\LoggerAwareTrait;
    /**
     * @param int $retrySleep Duration in ms between 2 retry
     * @param int $retryCount Maximum amount of retry
     */
    public function __construct(\Symfony\Component\Lock\PersistingStoreInterface $decorated, int $retrySleep = 100, int $retryCount = \PHP_INT_MAX)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists(\Symfony\Component\Lock\Key $key)
    {
    }
}
