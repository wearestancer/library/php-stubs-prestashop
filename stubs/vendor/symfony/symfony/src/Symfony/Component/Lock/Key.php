<?php

namespace Symfony\Component\Lock;

/**
 * Key is a container for the state of the locks in stores.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
final class Key
{
    public function __construct(string $resource)
    {
    }
    public function __toString() : string
    {
    }
    public function hasState(string $stateKey) : bool
    {
    }
    public function setState(string $stateKey, $state) : void
    {
    }
    public function removeState(string $stateKey) : void
    {
    }
    public function getState(string $stateKey)
    {
    }
    public function resetLifetime()
    {
    }
    /**
     * @param float $ttl the expiration delay of locks in seconds
     */
    public function reduceLifetime(float $ttl)
    {
    }
    /**
     * Returns the remaining lifetime.
     *
     * @return float|null Remaining lifetime in seconds. Null when the key won't expire.
     */
    public function getRemainingLifetime() : ?float
    {
    }
    public function isExpired() : bool
    {
    }
}
