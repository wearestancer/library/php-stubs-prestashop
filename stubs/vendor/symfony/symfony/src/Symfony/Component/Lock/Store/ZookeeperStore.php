<?php

namespace Symfony\Component\Lock\Store;

/**
 * ZookeeperStore is a PersistingStoreInterface implementation using Zookeeper as store engine.
 *
 * @author Ganesh Chandrasekaran <gchandrasekaran@wayfair.com>
 */
class ZookeeperStore implements \Symfony\Component\Lock\StoreInterface
{
    use \Symfony\Component\Lock\Store\ExpiringStoreTrait;
    public function __construct(\Zookeeper $zookeeper)
    {
    }
    public static function createConnection(string $dsn) : \Zookeeper
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists(\Symfony\Component\Lock\Key $key) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.4.
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl)
    {
    }
}
