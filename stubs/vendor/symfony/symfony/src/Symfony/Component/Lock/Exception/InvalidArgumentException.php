<?php

namespace Symfony\Component\Lock\Exception;

/**
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Lock\Exception\ExceptionInterface
{
}
