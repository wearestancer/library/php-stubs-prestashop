<?php

namespace Symfony\Component\Lock;

/**
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
interface PersistingStoreInterface
{
    /**
     * Stores the resource if it's not locked by someone else.
     *
     * @throws LockAcquiringException
     * @throws LockConflictedException
     */
    public function save(\Symfony\Component\Lock\Key $key);
    /**
     * Removes a resource from the storage.
     *
     * @throws LockReleasingException
     */
    public function delete(\Symfony\Component\Lock\Key $key);
    /**
     * Returns whether or not the resource exists in the storage.
     *
     * @return bool
     */
    public function exists(\Symfony\Component\Lock\Key $key);
    /**
     * Extends the TTL of a resource.
     *
     * @param float $ttl amount of seconds to keep the lock in the store
     *
     * @throws LockConflictedException
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl);
}
