<?php

namespace Symfony\Component\Lock\Store;

/**
 * MemcachedStore is a PersistingStoreInterface implementation using Memcached as store engine.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class MemcachedStore implements \Symfony\Component\Lock\StoreInterface
{
    use \Symfony\Component\Lock\Store\ExpiringStoreTrait;
    public static function isSupported()
    {
    }
    /**
     * @param int $initialTtl the expiration delay of locks in seconds
     */
    public function __construct(\Memcached $memcached, int $initialTtl = 300)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.4.
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists(\Symfony\Component\Lock\Key $key)
    {
    }
}
