<?php

namespace Symfony\Component\Lock\Store;

/**
 * RedisStore is a PersistingStoreInterface implementation using Redis as store engine.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class RedisStore implements \Symfony\Component\Lock\StoreInterface
{
    use \Symfony\Component\Lock\Store\ExpiringStoreTrait;
    /**
     * @param \Redis|\RedisArray|\RedisCluster|RedisProxy|RedisClusterProxy|\Predis\ClientInterface $redis
     * @param float                                                                                 $initialTtl The expiration delay of locks in seconds
     */
    public function __construct($redis, float $initialTtl = 300.0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.4.
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists(\Symfony\Component\Lock\Key $key)
    {
    }
}
