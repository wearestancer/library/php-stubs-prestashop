<?php

namespace Symfony\Component\Lock;

/**
 * @author Hamza Amrouche <hamza.simperfit@gmail.com>
 */
interface BlockingStoreInterface extends \Symfony\Component\Lock\PersistingStoreInterface
{
    /**
     * Waits until a key becomes free, then stores the resource.
     *
     * @throws LockConflictedException
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key);
}
