<?php

namespace Symfony\Component\Lock\Exception;

/**
 * LockAcquiringException is thrown when an issue happens during the acquisition of a lock.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class LockAcquiringException extends \RuntimeException implements \Symfony\Component\Lock\Exception\ExceptionInterface
{
}
