<?php

namespace Symfony\Component\Lock;

/**
 * Factory provides method to create locks.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 * @author Hamza Amrouche <hamza.simperfit@gmail.com>
 */
class LockFactory extends \Symfony\Component\Lock\Factory
{
    /**
     * Creates a lock for the given resource.
     *
     * @param string     $resource    The resource to lock
     * @param float|null $ttl         Maximum expected lock duration in seconds
     * @param bool       $autoRelease Whether to automatically release the lock or not when the lock instance is destroyed
     */
    public function createLock($resource, $ttl = 300.0, $autoRelease = true) : \Symfony\Component\Lock\LockInterface
    {
    }
}
