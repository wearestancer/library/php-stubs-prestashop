<?php

namespace Symfony\Component\Lock\Store;

/**
 * SemaphoreStore is a PersistingStoreInterface implementation using Semaphore as store engine.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class SemaphoreStore implements \Symfony\Component\Lock\StoreInterface, \Symfony\Component\Lock\BlockingStoreInterface
{
    /**
     * Returns whether or not the store is supported.
     *
     * @internal
     */
    public static function isSupported() : bool
    {
    }
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists(\Symfony\Component\Lock\Key $key)
    {
    }
}
