<?php

namespace Symfony\Component\Lock\Exception;

/**
 * LockExpiredException is thrown when a lock may conflict due to a TTL expiration.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class LockExpiredException extends \RuntimeException implements \Symfony\Component\Lock\Exception\ExceptionInterface
{
}
