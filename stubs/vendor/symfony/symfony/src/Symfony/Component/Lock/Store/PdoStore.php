<?php

namespace Symfony\Component\Lock\Store;

/**
 * PdoStore is a PersistingStoreInterface implementation using a PDO connection.
 *
 * Lock metadata are stored in a table. You can use createTable() to initialize
 * a correctly defined table.
 * CAUTION: This store relies on all client and server nodes to have
 * synchronized clocks for lock expiry to occur at the correct time.
 * To ensure locks don't expire prematurely; the TTLs should be set with enough
 * extra time to account for any clock drift between nodes.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class PdoStore implements \Symfony\Component\Lock\StoreInterface
{
    use \Symfony\Component\Lock\Store\ExpiringStoreTrait;
    /**
     * You can either pass an existing database connection as PDO instance or
     * a Doctrine DBAL Connection or a DSN string that will be used to
     * lazy-connect to the database when the lock is actually used.
     *
     * List of available options:
     *  * db_table: The name of the table [default: lock_keys]
     *  * db_id_col: The column where to store the lock key [default: key_id]
     *  * db_token_col: The column where to store the lock token [default: key_token]
     *  * db_expiration_col: The column where to store the expiration [default: key_expiration]
     *  * db_username: The username when lazy-connect [default: '']
     *  * db_password: The password when lazy-connect [default: '']
     *  * db_connection_options: An array of driver-specific connection options [default: []]
     *
     * @param \PDO|Connection|string $connOrDsn     A \PDO or Connection instance or DSN string or null
     * @param array                  $options       An associative array of options
     * @param float                  $gcProbability Probability expressed as floating number between 0 and 1 to clean old locks
     * @param int                    $initialTtl    The expiration delay of locks in seconds
     *
     * @throws InvalidArgumentException When first argument is not PDO nor Connection nor string
     * @throws InvalidArgumentException When PDO error mode is not PDO::ERRMODE_EXCEPTION
     * @throws InvalidArgumentException When namespace contains invalid characters
     * @throws InvalidArgumentException When the initial ttl is not valid
     */
    public function __construct($connOrDsn, array $options = [], float $gcProbability = 0.01, int $initialTtl = 300)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function save(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function waitAndSave(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function putOffExpiration(\Symfony\Component\Lock\Key $key, $ttl)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function exists(\Symfony\Component\Lock\Key $key)
    {
    }
    /**
     * Creates the table to store lock keys which can be called once for setup.
     *
     * @throws \PDOException    When the table already exists
     * @throws DBALException    When the table already exists
     * @throws Exception        When the table already exists
     * @throws \DomainException When an unsupported PDO driver is used
     */
    public function createTable() : void
    {
    }
}
