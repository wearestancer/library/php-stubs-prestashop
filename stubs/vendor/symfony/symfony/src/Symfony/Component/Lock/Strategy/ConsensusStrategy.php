<?php

namespace Symfony\Component\Lock\Strategy;

/**
 * ConsensusStrategy is a StrategyInterface implementation where strictly more than 50% items should be successful.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class ConsensusStrategy implements \Symfony\Component\Lock\Strategy\StrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function isMet($numberOfSuccess, $numberOfItems)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function canBeMet($numberOfFailure, $numberOfItems)
    {
    }
}
