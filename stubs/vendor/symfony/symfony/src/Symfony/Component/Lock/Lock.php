<?php

namespace Symfony\Component\Lock;

/**
 * Lock is the default implementation of the LockInterface.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
final class Lock implements \Symfony\Component\Lock\LockInterface, \Psr\Log\LoggerAwareInterface
{
    use \Psr\Log\LoggerAwareTrait;
    /**
     * @param float|null $ttl         Maximum expected lock duration in seconds
     * @param bool       $autoRelease Whether to automatically release the lock or not when the lock instance is destroyed
     */
    public function __construct(\Symfony\Component\Lock\Key $key, \Symfony\Component\Lock\PersistingStoreInterface $store, float $ttl = null, bool $autoRelease = true)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    /**
     * Automatically releases the underlying lock when the object is destructed.
     */
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function acquire($blocking = false) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refresh($ttl = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isAcquired() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function release()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isExpired() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRemainingLifetime() : ?float
    {
    }
}
