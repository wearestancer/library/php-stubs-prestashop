<?php

namespace Symfony\Component\Lock\Exception;

/**
 * NotSupportedException is thrown when an unsupported method is called.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class NotSupportedException extends \LogicException implements \Symfony\Component\Lock\Exception\ExceptionInterface
{
}
