<?php

namespace Symfony\Component\Lock;

/**
 * Factory provides method to create locks.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 *
 * @deprecated "Symfony\Component\Lock\Factory" is deprecated since Symfony 4.4 and will be removed in 5.0 use "Symfony\Component\Lock\LockFactory" instead
 */
class Factory implements \Psr\Log\LoggerAwareInterface
{
    use \Psr\Log\LoggerAwareTrait;
    public function __construct(\Symfony\Component\Lock\PersistingStoreInterface $store)
    {
    }
    /**
     * Creates a lock for the given resource.
     *
     * @param string     $resource    The resource to lock
     * @param float|null $ttl         Maximum expected lock duration in seconds
     * @param bool       $autoRelease Whether to automatically release the lock or not when the lock instance is destroyed
     *
     * @return Lock
     */
    public function createLock($resource, $ttl = 300.0, $autoRelease = true)
    {
    }
}
