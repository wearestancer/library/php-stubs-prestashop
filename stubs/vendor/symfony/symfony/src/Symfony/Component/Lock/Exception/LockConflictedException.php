<?php

namespace Symfony\Component\Lock\Exception;

/**
 * LockConflictedException is thrown when a lock is acquired by someone else.
 *
 * In non-blocking mode it is caught by {@see Lock::acquire()} and {@see Lock::acquireRead()}.
 *
 * @author Jérémy Derussé <jeremy@derusse.com>
 */
class LockConflictedException extends \RuntimeException implements \Symfony\Component\Lock\Exception\ExceptionInterface
{
}
