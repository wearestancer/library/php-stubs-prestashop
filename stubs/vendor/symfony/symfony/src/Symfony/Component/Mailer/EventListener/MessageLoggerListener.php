<?php

namespace Symfony\Component\Mailer\EventListener;

/**
 * Logs Messages.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MessageLoggerListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    public function onMessage(\Symfony\Component\Mailer\Event\MessageEvent $event) : void
    {
    }
    public function getEvents() : \Symfony\Component\Mailer\Event\MessageEvents
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
