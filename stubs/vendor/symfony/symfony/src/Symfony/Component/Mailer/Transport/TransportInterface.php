<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * Interface for all mailer transports.
 *
 * When sending emails, you should prefer MailerInterface implementations
 * as they allow asynchronous sending.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface TransportInterface
{
    /**
     * @throws TransportExceptionInterface
     */
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage;
    public function __toString() : string;
}
