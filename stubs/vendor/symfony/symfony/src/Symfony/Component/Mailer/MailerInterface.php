<?php

namespace Symfony\Component\Mailer;

/**
 * Interface for mailers able to send emails synchronous and/or asynchronous.
 *
 * Implementations must support synchronous and asynchronous sending.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface MailerInterface
{
    /**
     * @throws TransportExceptionInterface
     */
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : void;
}
