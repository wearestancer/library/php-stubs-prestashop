<?php

namespace Symfony\Component\Mailer\Transport\Smtp\Auth;

/**
 * An Authentication mechanism.
 *
 * @author Chris Corbyn
 */
interface AuthenticatorInterface
{
    /**
     * Tries to authenticate the user.
     *
     * @throws TransportExceptionInterface
     */
    public function authenticate(\Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport $client) : void;
    /**
     * Gets the name of the AUTH mechanism this Authenticator handles.
     */
    public function getAuthKeyword() : string;
}
