<?php

namespace Symfony\Component\Mailer\Transport\Smtp\Stream;

/**
 * A stream supporting remote sockets and local processes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Chris Corbyn
 *
 * @internal
 */
abstract class AbstractStream
{
    protected $stream;
    protected $in;
    protected $out;
    public function write(string $bytes, $debug = true) : void
    {
    }
    /**
     * Flushes the contents of the stream (empty it) and set the internal pointer to the beginning.
     */
    public function flush() : void
    {
    }
    /**
     * Performs any initialization needed.
     */
    public abstract function initialize() : void;
    public function terminate() : void
    {
    }
    public function readLine() : string
    {
    }
    public function getDebug() : string
    {
    }
    public static function replace(string $from, string $to, iterable $chunks) : \Generator
    {
    }
    protected abstract function getReadConnectionDescription() : string;
}
