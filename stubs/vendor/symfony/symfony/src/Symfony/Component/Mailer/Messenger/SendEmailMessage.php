<?php

namespace Symfony\Component\Mailer\Messenger;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SendEmailMessage
{
    public function __construct(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null)
    {
    }
    public function getMessage() : \Symfony\Component\Mime\RawMessage
    {
    }
    public function getEnvelope() : ?\Symfony\Component\Mailer\Envelope
    {
    }
}
