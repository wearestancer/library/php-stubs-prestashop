<?php

namespace Symfony\Component\Mailer;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Mailer implements \Symfony\Component\Mailer\MailerInterface
{
    public function __construct(\Symfony\Component\Mailer\Transport\TransportInterface $transport, \Symfony\Component\Messenger\MessageBusInterface $bus = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : void
    {
    }
}
