<?php

namespace Symfony\Component\Mailer\Bridge\Postmark\Transport;

/**
 * @author Kevin Verschaeve
 */
class PostmarkApiTransport extends \Symfony\Component\Mailer\Transport\AbstractApiTransport
{
    public function __construct(string $key, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function __toString() : string
    {
    }
    protected function doSendApi(\Symfony\Component\Mailer\SentMessage $sentMessage, \Symfony\Component\Mime\Email $email, \Symfony\Component\Mailer\Envelope $envelope) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
}
