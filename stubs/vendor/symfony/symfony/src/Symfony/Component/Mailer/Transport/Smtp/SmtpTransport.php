<?php

namespace Symfony\Component\Mailer\Transport\Smtp;

/**
 * Sends emails over SMTP.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Chris Corbyn
 */
class SmtpTransport extends \Symfony\Component\Mailer\Transport\AbstractTransport
{
    public function __construct(\Symfony\Component\Mailer\Transport\Smtp\Stream\AbstractStream $stream = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function getStream() : \Symfony\Component\Mailer\Transport\Smtp\Stream\AbstractStream
    {
    }
    /**
     * Sets the maximum number of messages to send before re-starting the transport.
     *
     * By default, the threshold is set to 100 (and no sleep at restart).
     *
     * @param int $threshold The maximum number of messages (0 to disable)
     * @param int $sleep     The number of seconds to sleep between stopping and re-starting the transport
     */
    public function setRestartThreshold(int $threshold, int $sleep = 0) : self
    {
    }
    /**
     * Sets the minimum number of seconds required between two messages, before the server is pinged.
     * If the transport wants to send a message and the time since the last message exceeds the specified threshold,
     * the transport will ping the server first (NOOP command) to check if the connection is still alive.
     * Otherwise the message will be sent without pinging the server first.
     *
     * Do not set the threshold too low, as the SMTP server may drop the connection if there are too many
     * non-mail commands (like pinging the server with NOOP).
     *
     * By default, the threshold is set to 100 seconds.
     *
     * @param int $seconds The minimum number of seconds between two messages required to ping the server
     *
     * @return $this
     */
    public function setPingThreshold(int $seconds) : self
    {
    }
    /**
     * Sets the name of the local domain that will be used in HELO.
     *
     * This should be a fully-qualified domain name and should be truly the domain
     * you're using.
     *
     * If your server does not have a domain name, use the IP address. This will
     * automatically be wrapped in square brackets as described in RFC 5321,
     * section 4.1.3.
     */
    public function setLocalDomain(string $domain) : self
    {
    }
    /**
     * Gets the name of the domain that will be used in HELO.
     *
     * If an IP address was specified, this will be returned wrapped in square
     * brackets as described in RFC 5321, section 4.1.3.
     */
    public function getLocalDomain() : string
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage
    {
    }
    public function __toString() : string
    {
    }
    /**
     * Runs a command against the stream, expecting the given response codes.
     *
     * @param int[] $codes
     *
     * @return string The server response
     *
     * @throws TransportException when an invalid response if received
     *
     * @internal
     */
    public function executeCommand(string $command, array $codes) : string
    {
    }
    protected function doSend(\Symfony\Component\Mailer\SentMessage $message) : void
    {
    }
    protected function doHeloCommand() : void
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
