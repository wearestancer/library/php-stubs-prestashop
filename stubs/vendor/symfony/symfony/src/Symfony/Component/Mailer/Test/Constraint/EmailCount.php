<?php

namespace Symfony\Component\Mailer\Test\Constraint;

final class EmailCount extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(int $expectedValue, string $transport = null, bool $queued = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
