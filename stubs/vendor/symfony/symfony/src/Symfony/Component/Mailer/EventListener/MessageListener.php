<?php

namespace Symfony\Component\Mailer\EventListener;

/**
 * Manipulates the headers and the body of a Message.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MessageListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\Mime\Header\Headers $headers = null, \Symfony\Component\Mime\BodyRendererInterface $renderer = null)
    {
    }
    public function onMessage(\Symfony\Component\Mailer\Event\MessageEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
