<?php

namespace Symfony\Component\Mailer\Bridge\Mailgun\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
final class MailgunTransportFactory extends \Symfony\Component\Mailer\Transport\AbstractTransportFactory
{
    public function create(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
}
