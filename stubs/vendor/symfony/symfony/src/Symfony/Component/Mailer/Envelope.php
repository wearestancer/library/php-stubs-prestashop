<?php

namespace Symfony\Component\Mailer;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Envelope
{
    /**
     * @param Address[] $recipients
     */
    public function __construct(\Symfony\Component\Mime\Address $sender, array $recipients)
    {
    }
    public static function create(\Symfony\Component\Mime\RawMessage $message) : self
    {
    }
    public function setSender(\Symfony\Component\Mime\Address $sender) : void
    {
    }
    /**
     * @return Address Returns a "mailbox" as specified by RFC 2822
     *                 Must be converted to an "addr-spec" when used as a "MAIL FROM" value in SMTP (use getAddress())
     */
    public function getSender() : \Symfony\Component\Mime\Address
    {
    }
    /**
     * @param Address[] $recipients
     */
    public function setRecipients(array $recipients) : void
    {
    }
    /**
     * @return Address[]
     */
    public function getRecipients() : array
    {
    }
}
