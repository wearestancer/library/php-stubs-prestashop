<?php

namespace Symfony\Component\Mailer\Bridge\Google\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
final class GmailTransportFactory extends \Symfony\Component\Mailer\Transport\AbstractTransportFactory
{
    public function create(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
}
