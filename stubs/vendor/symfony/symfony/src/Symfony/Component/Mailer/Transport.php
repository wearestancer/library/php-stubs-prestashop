<?php

namespace Symfony\Component\Mailer;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
class Transport
{
    public static function fromDsn(string $dsn, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Psr\Log\LoggerInterface $logger = null) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
    public static function fromDsns(array $dsns, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Psr\Log\LoggerInterface $logger = null) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
    /**
     * @param TransportFactoryInterface[] $factories
     */
    public function __construct(iterable $factories)
    {
    }
    public function fromStrings(array $dsns) : \Symfony\Component\Mailer\Transport\Transports
    {
    }
    public function fromString(string $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
    public function fromDsnObject(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
}
