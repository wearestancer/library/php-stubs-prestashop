<?php

namespace Symfony\Component\Mailer;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
final class DelayedEnvelope extends \Symfony\Component\Mailer\Envelope
{
    public function __construct(\Symfony\Component\Mime\Message $message)
    {
    }
    public function setSender(\Symfony\Component\Mime\Address $sender) : void
    {
    }
    public function getSender() : \Symfony\Component\Mime\Address
    {
    }
    public function setRecipients(array $recipients) : void
    {
    }
    /**
     * @return Address[]
     */
    public function getRecipients() : array
    {
    }
}
