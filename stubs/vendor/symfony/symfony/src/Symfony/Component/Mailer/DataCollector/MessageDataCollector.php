<?php

namespace Symfony\Component\Mailer\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class MessageDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector
{
    public function __construct(\Symfony\Component\Mailer\EventListener\MessageLoggerListener $logger)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function getEvents() : \Symfony\Component\Mailer\Event\MessageEvents
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
