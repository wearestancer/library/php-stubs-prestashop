<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
class IncompleteDsnException extends \Symfony\Component\Mailer\Exception\InvalidArgumentException
{
}
