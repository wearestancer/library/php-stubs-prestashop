<?php

namespace Symfony\Component\Mailer\EventListener;

/**
 * Manipulates the Envelope of a Message.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class EnvelopeListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param Address|string        $sender
     * @param array<Address|string> $recipients
     */
    public function __construct($sender = null, array $recipients = null)
    {
    }
    public function onMessage(\Symfony\Component\Mailer\Event\MessageEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
