<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * Pretends messages have been sent, but just ignores them.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class NullTransport extends \Symfony\Component\Mailer\Transport\AbstractTransport
{
    public function __toString() : string
    {
    }
}
