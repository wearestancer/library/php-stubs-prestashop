<?php

namespace Symfony\Component\Mailer\Test;

/**
 * A test case to ease testing Transport Factory.
 *
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
abstract class TransportFactoryTestCase extends \PHPUnit\Framework\TestCase
{
    protected const USER = 'u$er';
    protected const PASSWORD = 'pa$s';
    protected $dispatcher;
    protected $client;
    protected $logger;
    public abstract function getFactory() : \Symfony\Component\Mailer\Transport\TransportFactoryInterface;
    public abstract function supportsProvider() : iterable;
    public abstract function createProvider() : iterable;
    public function unsupportedSchemeProvider() : iterable
    {
    }
    public function incompleteDsnProvider() : iterable
    {
    }
    /**
     * @dataProvider supportsProvider
     */
    public function testSupports(\Symfony\Component\Mailer\Transport\Dsn $dsn, bool $supports)
    {
    }
    /**
     * @dataProvider createProvider
     */
    public function testCreate(\Symfony\Component\Mailer\Transport\Dsn $dsn, \Symfony\Component\Mailer\Transport\TransportInterface $transport)
    {
    }
    /**
     * @dataProvider unsupportedSchemeProvider
     */
    public function testUnsupportedSchemeException(\Symfony\Component\Mailer\Transport\Dsn $dsn, string $message = null)
    {
    }
    /**
     * @dataProvider incompleteDsnProvider
     */
    public function testIncompleteDsnException(\Symfony\Component\Mailer\Transport\Dsn $dsn)
    {
    }
    protected function getDispatcher() : \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
    {
    }
    protected function getClient() : \Symfony\Contracts\HttpClient\HttpClientInterface
    {
    }
    protected function getLogger() : \Psr\Log\LoggerInterface
    {
    }
}
