<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractTransport implements \Symfony\Component\Mailer\Transport\TransportInterface
{
    public function __construct(\Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * Sets the maximum number of messages to send per second (0 to disable).
     */
    public function setMaxPerSecond(float $rate) : self
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage
    {
    }
    protected abstract function doSend(\Symfony\Component\Mailer\SentMessage $message) : void;
    /**
     * @param Address[] $addresses
     *
     * @return string[]
     */
    protected function stringifyAddresses(array $addresses) : array
    {
    }
    protected function getLogger() : \Psr\Log\LoggerInterface
    {
    }
}
