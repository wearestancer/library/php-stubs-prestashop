<?php

namespace Symfony\Component\Mailer\Transport\Smtp;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
final class EsmtpTransportFactory extends \Symfony\Component\Mailer\Transport\AbstractTransportFactory
{
    public function create(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
}
