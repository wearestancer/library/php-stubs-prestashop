<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Mailer\Exception\ExceptionInterface
{
}
