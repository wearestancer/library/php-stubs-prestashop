<?php

namespace Symfony\Component\Mailer\Event;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MessageEvents
{
    public function add(\Symfony\Component\Mailer\Event\MessageEvent $event) : void
    {
    }
    public function getTransports() : array
    {
    }
    /**
     * @return MessageEvent[]
     */
    public function getEvents(string $name = null) : array
    {
    }
    /**
     * @return RawMessage[]
     */
    public function getMessages(string $name = null) : array
    {
    }
}
