<?php

namespace Symfony\Component\Mailer\Bridge\Amazon\Transport;

/**
 * @author Kevin Verschaeve
 */
class SesSmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport
{
    /**
     * @param string|null $region Amazon SES region
     */
    public function __construct(string $username, string $password, string $region = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
