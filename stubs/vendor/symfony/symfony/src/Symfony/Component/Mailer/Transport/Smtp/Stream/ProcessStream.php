<?php

namespace Symfony\Component\Mailer\Transport\Smtp\Stream;

/**
 * A stream supporting local processes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Chris Corbyn
 *
 * @internal
 */
final class ProcessStream extends \Symfony\Component\Mailer\Transport\Smtp\Stream\AbstractStream
{
    public function setCommand(string $command)
    {
    }
    public function initialize() : void
    {
    }
    public function terminate() : void
    {
    }
}
