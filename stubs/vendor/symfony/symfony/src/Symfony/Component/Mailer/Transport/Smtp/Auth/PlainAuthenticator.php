<?php

namespace Symfony\Component\Mailer\Transport\Smtp\Auth;

/**
 * Handles PLAIN authentication.
 *
 * @author Chris Corbyn
 */
class PlainAuthenticator implements \Symfony\Component\Mailer\Transport\Smtp\Auth\AuthenticatorInterface
{
    public function getAuthKeyword() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @see https://www.ietf.org/rfc/rfc4954.txt
     */
    public function authenticate(\Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport $client) : void
    {
    }
}
