<?php

namespace Symfony\Component\Mailer\Transport\Smtp;

/**
 * Sends Emails over SMTP with ESMTP support.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Chris Corbyn
 */
class EsmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\SmtpTransport
{
    public function __construct(string $host = 'localhost', int $port = 0, bool $tls = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function setUsername(string $username) : self
    {
    }
    public function getUsername() : string
    {
    }
    public function setPassword(string $password) : self
    {
    }
    public function getPassword() : string
    {
    }
    public function addAuthenticator(\Symfony\Component\Mailer\Transport\Smtp\Auth\AuthenticatorInterface $authenticator) : void
    {
    }
    protected function doHeloCommand() : void
    {
    }
}
