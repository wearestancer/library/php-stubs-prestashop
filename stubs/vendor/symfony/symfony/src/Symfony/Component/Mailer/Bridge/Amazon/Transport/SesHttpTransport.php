<?php

namespace Symfony\Component\Mailer\Bridge\Amazon\Transport;

/**
 * @author Kevin Verschaeve
 */
class SesHttpTransport extends \Symfony\Component\Mailer\Transport\AbstractHttpTransport
{
    /**
     * @param string|null $region Amazon SES region
     */
    public function __construct(string $accessKey, string $secretKey, string $region = null, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function __toString() : string
    {
    }
    protected function doSendHttp(\Symfony\Component\Mailer\SentMessage $message) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
}
