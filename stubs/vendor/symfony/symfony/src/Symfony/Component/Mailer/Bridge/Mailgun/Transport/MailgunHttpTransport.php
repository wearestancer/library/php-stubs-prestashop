<?php

namespace Symfony\Component\Mailer\Bridge\Mailgun\Transport;

/**
 * @author Kevin Verschaeve
 */
class MailgunHttpTransport extends \Symfony\Component\Mailer\Transport\AbstractHttpTransport
{
    public function __construct(string $key, string $domain, string $region = null, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function __toString() : string
    {
    }
    protected function doSendHttp(\Symfony\Component\Mailer\SentMessage $message) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
}
