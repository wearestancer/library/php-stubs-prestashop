<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
abstract class AbstractTransportFactory implements \Symfony\Component\Mailer\Transport\TransportFactoryInterface
{
    protected $dispatcher;
    protected $client;
    protected $logger;
    public function __construct(\Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function supports(\Symfony\Component\Mailer\Transport\Dsn $dsn) : bool
    {
    }
    protected abstract function getSupportedSchemes() : array;
    protected function getUser(\Symfony\Component\Mailer\Transport\Dsn $dsn) : string
    {
    }
    protected function getPassword(\Symfony\Component\Mailer\Transport\Dsn $dsn) : string
    {
    }
}
