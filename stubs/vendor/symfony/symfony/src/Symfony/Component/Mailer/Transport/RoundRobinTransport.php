<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * Uses several Transports using a round robin algorithm.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RoundRobinTransport implements \Symfony\Component\Mailer\Transport\TransportInterface
{
    /**
     * @param TransportInterface[] $transports
     */
    public function __construct(array $transports, int $retryPeriod = 60)
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage
    {
    }
    public function __toString() : string
    {
    }
    /**
     * Rotates the transport list around and returns the first instance.
     */
    protected function getNextTransport() : ?\Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
    protected function isTransportDead(\Symfony\Component\Mailer\Transport\TransportInterface $transport) : bool
    {
    }
    protected function getNameSymbol() : string
    {
    }
}
