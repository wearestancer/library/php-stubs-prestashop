<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
class UnsupportedSchemeException extends \Symfony\Component\Mailer\Exception\LogicException
{
    public function __construct(\Symfony\Component\Mailer\Transport\Dsn $dsn, string $name = null, array $supported = [])
    {
    }
}
