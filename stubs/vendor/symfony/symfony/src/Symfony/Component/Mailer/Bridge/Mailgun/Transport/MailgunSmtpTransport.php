<?php

namespace Symfony\Component\Mailer\Bridge\Mailgun\Transport;

/**
 * @author Kevin Verschaeve
 */
class MailgunSmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport
{
    public function __construct(string $username, string $password, string $region = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
