<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
interface TransportFactoryInterface
{
    /**
     * @throws UnsupportedSchemeException
     * @throws IncompleteDsnException
     */
    public function create(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface;
    public function supports(\Symfony\Component\Mailer\Transport\Dsn $dsn) : bool;
}
