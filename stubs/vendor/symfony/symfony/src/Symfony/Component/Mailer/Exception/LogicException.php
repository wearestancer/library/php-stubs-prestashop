<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class LogicException extends \LogicException implements \Symfony\Component\Mailer\Exception\ExceptionInterface
{
}
