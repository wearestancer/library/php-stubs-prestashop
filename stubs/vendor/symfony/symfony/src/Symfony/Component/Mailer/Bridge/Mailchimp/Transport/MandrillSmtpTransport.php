<?php

namespace Symfony\Component\Mailer\Bridge\Mailchimp\Transport;

/**
 * @author Kevin Verschaeve
 */
class MandrillSmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport
{
    public function __construct(string $username, string $password, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
