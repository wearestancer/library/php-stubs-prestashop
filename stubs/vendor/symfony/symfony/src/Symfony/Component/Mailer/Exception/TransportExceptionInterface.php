<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface TransportExceptionInterface extends \Symfony\Component\Mailer\Exception\ExceptionInterface
{
    public function getDebug() : string;
    public function appendDebug(string $debug) : void;
}
