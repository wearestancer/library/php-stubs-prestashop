<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Mailer\Exception\ExceptionInterface
{
}
