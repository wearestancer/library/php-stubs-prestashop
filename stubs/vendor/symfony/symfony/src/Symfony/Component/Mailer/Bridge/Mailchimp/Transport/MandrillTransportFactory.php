<?php

namespace Symfony\Component\Mailer\Bridge\Mailchimp\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
final class MandrillTransportFactory extends \Symfony\Component\Mailer\Transport\AbstractTransportFactory
{
    public function create(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
}
