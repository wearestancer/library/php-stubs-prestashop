<?php

namespace Symfony\Component\Mailer\Bridge\Postmark\Transport;

/**
 * @author Kevin Verschaeve
 */
class PostmarkSmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport
{
    public function __construct(string $id, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage
    {
    }
}
