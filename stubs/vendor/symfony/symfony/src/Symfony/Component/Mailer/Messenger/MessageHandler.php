<?php

namespace Symfony\Component\Mailer\Messenger;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MessageHandler
{
    public function __construct(\Symfony\Component\Mailer\Transport\TransportInterface $transport)
    {
    }
    public function __invoke(\Symfony\Component\Mailer\Messenger\SendEmailMessage $message)
    {
    }
}
