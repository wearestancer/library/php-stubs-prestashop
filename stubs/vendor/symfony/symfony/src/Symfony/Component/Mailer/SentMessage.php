<?php

namespace Symfony\Component\Mailer;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SentMessage
{
    /**
     * @internal
     */
    public function __construct(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope)
    {
    }
    public function getMessage() : \Symfony\Component\Mime\RawMessage
    {
    }
    public function getOriginalMessage() : \Symfony\Component\Mime\RawMessage
    {
    }
    public function getEnvelope() : \Symfony\Component\Mailer\Envelope
    {
    }
    public function setMessageId(string $id) : void
    {
    }
    public function getMessageId() : string
    {
    }
    public function getDebug() : string
    {
    }
    public function appendDebug(string $debug) : void
    {
    }
    public function toString() : string
    {
    }
    public function toIterable() : iterable
    {
    }
}
