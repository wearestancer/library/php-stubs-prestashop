<?php

namespace Symfony\Component\Mailer\Transport\Smtp\Auth;

/**
 * Handles XOAUTH2 authentication.
 *
 * @author xu.li<AthenaLightenedMyPath@gmail.com>
 *
 * @see https://developers.google.com/google-apps/gmail/xoauth2_protocol
 */
class XOAuth2Authenticator implements \Symfony\Component\Mailer\Transport\Smtp\Auth\AuthenticatorInterface
{
    public function getAuthKeyword() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @see https://developers.google.com/google-apps/gmail/xoauth2_protocol#the_sasl_xoauth2_mechanism
     */
    public function authenticate(\Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport $client) : void
    {
    }
}
