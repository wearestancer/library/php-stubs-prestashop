<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * SendmailTransport for sending mail through a Sendmail/Postfix (etc..) binary.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Chris Corbyn
 */
class SendmailTransport extends \Symfony\Component\Mailer\Transport\AbstractTransport
{
    /**
     * Constructor.
     *
     * Supported modes are -bs and -t, with any additional flags desired.
     *
     * The recommended mode is "-bs" since it is interactive and failure notifications are hence possible.
     * Note that the -t mode does not support error reporting and does not support Bcc properly (the Bcc headers are not removed).
     *
     * If using -t mode, you are strongly advised to include -oi or -i in the flags (like /usr/sbin/sendmail -oi -t)
     *
     * -f<sender> flag will be appended automatically if one is not present.
     */
    public function __construct(string $command = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage
    {
    }
    public function __toString() : string
    {
    }
    protected function doSend(\Symfony\Component\Mailer\SentMessage $message) : void
    {
    }
}
