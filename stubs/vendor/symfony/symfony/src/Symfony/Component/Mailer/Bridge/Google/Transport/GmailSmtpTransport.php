<?php

namespace Symfony\Component\Mailer\Bridge\Google\Transport;

/**
 * @author Kevin Verschaeve
 */
class GmailSmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport
{
    public function __construct(string $username, string $password, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
