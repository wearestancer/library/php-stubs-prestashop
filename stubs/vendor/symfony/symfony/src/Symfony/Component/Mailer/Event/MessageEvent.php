<?php

namespace Symfony\Component\Mailer\Event;

/**
 * Allows the transformation of a Message and the Envelope before the email is sent.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class MessageEvent extends \Symfony\Component\EventDispatcher\Event
{
    public function __construct(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope, string $transport, bool $queued = false)
    {
    }
    public function getMessage() : \Symfony\Component\Mime\RawMessage
    {
    }
    public function setMessage(\Symfony\Component\Mime\RawMessage $message) : void
    {
    }
    public function getEnvelope() : \Symfony\Component\Mailer\Envelope
    {
    }
    public function setEnvelope(\Symfony\Component\Mailer\Envelope $envelope) : void
    {
    }
    public function getTransport() : string
    {
    }
    public function isQueued() : bool
    {
    }
}
