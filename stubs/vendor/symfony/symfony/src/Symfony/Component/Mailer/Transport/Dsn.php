<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
final class Dsn
{
    public function __construct(string $scheme, string $host, string $user = null, string $password = null, int $port = null, array $options = [])
    {
    }
    public static function fromString(string $dsn) : self
    {
    }
    public function getScheme() : string
    {
    }
    public function getHost() : string
    {
    }
    public function getUser() : ?string
    {
    }
    public function getPassword() : ?string
    {
    }
    public function getPort(int $default = null) : ?int
    {
    }
    public function getOption(string $key, $default = null)
    {
    }
}
