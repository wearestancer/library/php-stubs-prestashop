<?php

namespace Symfony\Component\Mailer\Bridge\Sendgrid\Transport;

/**
 * @author Kevin Verschaeve
 */
class SendgridSmtpTransport extends \Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport
{
    public function __construct(string $key, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
