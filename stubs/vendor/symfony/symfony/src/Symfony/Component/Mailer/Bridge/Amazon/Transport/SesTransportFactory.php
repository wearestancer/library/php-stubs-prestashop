<?php

namespace Symfony\Component\Mailer\Bridge\Amazon\Transport;

/**
 * @author Konstantin Myakshin <molodchick@gmail.com>
 */
final class SesTransportFactory extends \Symfony\Component\Mailer\Transport\AbstractTransportFactory
{
    public function create(\Symfony\Component\Mailer\Transport\Dsn $dsn) : \Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
}
