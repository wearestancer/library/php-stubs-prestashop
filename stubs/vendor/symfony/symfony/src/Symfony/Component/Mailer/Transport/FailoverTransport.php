<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * Uses several Transports using a failover algorithm.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FailoverTransport extends \Symfony\Component\Mailer\Transport\RoundRobinTransport
{
    protected function getNextTransport() : ?\Symfony\Component\Mailer\Transport\TransportInterface
    {
    }
    protected function getNameSymbol() : string
    {
    }
}
