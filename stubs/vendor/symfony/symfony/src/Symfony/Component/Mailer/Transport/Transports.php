<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Transports implements \Symfony\Component\Mailer\Transport\TransportInterface
{
    /**
     * @param TransportInterface[] $transports
     */
    public function __construct(iterable $transports)
    {
    }
    public function send(\Symfony\Component\Mime\RawMessage $message, \Symfony\Component\Mailer\Envelope $envelope = null) : ?\Symfony\Component\Mailer\SentMessage
    {
    }
    public function __toString() : string
    {
    }
}
