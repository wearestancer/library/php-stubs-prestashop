<?php

namespace Symfony\Component\Mailer\Transport\Smtp\Stream;

/**
 * A stream supporting remote sockets.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Chris Corbyn
 *
 * @internal
 */
final class SocketStream extends \Symfony\Component\Mailer\Transport\Smtp\Stream\AbstractStream
{
    public function setTimeout(float $timeout) : self
    {
    }
    public function getTimeout() : float
    {
    }
    /**
     * Literal IPv6 addresses should be wrapped in square brackets.
     */
    public function setHost(string $host) : self
    {
    }
    public function getHost() : string
    {
    }
    public function setPort(int $port) : self
    {
    }
    public function getPort() : int
    {
    }
    /**
     * Sets the TLS/SSL on the socket (disables STARTTLS).
     */
    public function disableTls() : self
    {
    }
    public function isTLS() : bool
    {
    }
    public function setStreamOptions(array $options) : self
    {
    }
    public function getStreamOptions() : array
    {
    }
    /**
     * Sets the source IP.
     *
     * IPv6 addresses should be wrapped in square brackets.
     */
    public function setSourceIp(string $ip) : self
    {
    }
    /**
     * Returns the IP used to connect to the destination.
     */
    public function getSourceIp() : ?string
    {
    }
    public function initialize() : void
    {
    }
    public function startTLS() : bool
    {
    }
    public function terminate() : void
    {
    }
}
