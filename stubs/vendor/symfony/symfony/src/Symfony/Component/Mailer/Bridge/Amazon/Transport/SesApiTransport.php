<?php

namespace Symfony\Component\Mailer\Bridge\Amazon\Transport;

/**
 * @author Kevin Verschaeve
 */
class SesApiTransport extends \Symfony\Component\Mailer\Transport\AbstractApiTransport
{
    /**
     * @param string|null $region Amazon SES region
     */
    public function __construct(string $accessKey, string $secretKey, string $region = null, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function __toString() : string
    {
    }
    protected function doSendApi(\Symfony\Component\Mailer\SentMessage $sentMessage, \Symfony\Component\Mime\Email $email, \Symfony\Component\Mailer\Envelope $envelope) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
}
