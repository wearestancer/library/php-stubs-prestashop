<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Victor Bocharsky <victor@symfonycasts.com>
 */
abstract class AbstractHttpTransport extends \Symfony\Component\Mailer\Transport\AbstractTransport
{
    protected $host;
    protected $port;
    protected $client;
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * @return $this
     */
    public function setHost(?string $host)
    {
    }
    /**
     * @return $this
     */
    public function setPort(?int $port)
    {
    }
    protected abstract function doSendHttp(\Symfony\Component\Mailer\SentMessage $message) : \Symfony\Contracts\HttpClient\ResponseInterface;
    protected function doSend(\Symfony\Component\Mailer\SentMessage $message) : void
    {
    }
}
