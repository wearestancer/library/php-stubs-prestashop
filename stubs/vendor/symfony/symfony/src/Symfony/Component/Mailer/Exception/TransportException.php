<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TransportException extends \Symfony\Component\Mailer\Exception\RuntimeException implements \Symfony\Component\Mailer\Exception\TransportExceptionInterface
{
    public function getDebug() : string
    {
    }
    public function appendDebug(string $debug) : void
    {
    }
}
