<?php

namespace Symfony\Component\Mailer\Transport;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractApiTransport extends \Symfony\Component\Mailer\Transport\AbstractHttpTransport
{
    protected abstract function doSendApi(\Symfony\Component\Mailer\SentMessage $sentMessage, \Symfony\Component\Mime\Email $email, \Symfony\Component\Mailer\Envelope $envelope) : \Symfony\Contracts\HttpClient\ResponseInterface;
    protected function doSendHttp(\Symfony\Component\Mailer\SentMessage $message) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    protected function getRecipients(\Symfony\Component\Mime\Email $email, \Symfony\Component\Mailer\Envelope $envelope) : array
    {
    }
}
