<?php

namespace Symfony\Component\Mailer\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpTransportException extends \Symfony\Component\Mailer\Exception\TransportException
{
    public function __construct(?string $message, \Symfony\Contracts\HttpClient\ResponseInterface $response, int $code = 0, \Throwable $previous = null)
    {
    }
    public function getResponse() : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
}
