<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Alessandro Chitolina <alekitto@gmail.com>
 *
 * @internal
 */
class RedisClusterProxy
{
    public function __construct(\Closure $initializer)
    {
    }
    public function __call($method, array $args)
    {
    }
    public function hscan($strKey, &$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function scan(&$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function sscan($strKey, &$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function zscan($strKey, &$iIterator, $strPattern = null, $iCount = null)
    {
    }
}
