<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * @deprecated since Symfony 4.3, use Psr16Adapter instead.
 */
class SimpleCacheAdapter extends \Symfony\Component\Cache\Adapter\Psr16Adapter
{
}
