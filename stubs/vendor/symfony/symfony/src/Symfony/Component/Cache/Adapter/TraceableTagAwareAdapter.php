<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
class TraceableTagAwareAdapter extends \Symfony\Component\Cache\Adapter\TraceableAdapter implements \Symfony\Component\Cache\Adapter\TagAwareAdapterInterface, \Symfony\Contracts\Cache\TagAwareCacheInterface
{
    public function __construct(\Symfony\Component\Cache\Adapter\TagAwareAdapterInterface $pool)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function invalidateTags(array $tags)
    {
    }
}
