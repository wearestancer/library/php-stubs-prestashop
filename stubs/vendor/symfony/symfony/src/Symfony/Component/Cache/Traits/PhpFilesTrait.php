<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Piotr Stankowski <git@trakos.pl>
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Rob Frawley 2nd <rmf@src.run>
 *
 * @internal
 */
trait PhpFilesTrait
{
    use \Symfony\Component\Cache\Traits\FilesystemCommonTrait {
        doClear as private doCommonClear;
        doDelete as private doCommonDelete;
    }
    private $includeHandler;
    private $appendOnly;
    private $values = [];
    private $files = [];
    private static $startTime;
    private static $valuesCache = [];
    public static function isSupported()
    {
    }
    /**
     * @return bool
     */
    public function prune()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, int $lifetime)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
    }
    protected function doUnlink($file)
    {
    }
    private function getFileKey(string $file) : string
    {
    }
}
/**
 * @internal
 */
class LazyValue
{
    public $file;
    public function __construct(string $file)
    {
    }
}
