<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * Turns a PSR-16 cache into a PSR-6 one.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class Psr16Adapter extends \Symfony\Component\Cache\Adapter\AbstractAdapter implements \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\ProxyTrait;
    /**
     * @internal
     */
    protected const NS_SEPARATOR = '_';
    public function __construct(\Psr\SimpleCache\CacheInterface $pool, string $namespace = '', int $defaultLifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, int $lifetime)
    {
    }
}
