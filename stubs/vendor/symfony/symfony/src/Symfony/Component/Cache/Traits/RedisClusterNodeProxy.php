<?php

namespace Symfony\Component\Cache\Traits;

/**
 * This file acts as a wrapper to the \RedisCluster implementation so it can accept the same type of calls as
 *  individual \Redis objects.
 *
 * Calls are made to individual nodes via: RedisCluster->{method}($host, ...args)'
 *  according to https://github.com/phpredis/phpredis/blob/develop/cluster.markdown#directed-node-commands
 *
 * @author Jack Thomas <jack.thomas@solidalpha.com>
 *
 * @internal
 */
class RedisClusterNodeProxy
{
    /**
     * @param \RedisCluster|RedisClusterProxy $redis
     */
    public function __construct(array $host, $redis)
    {
    }
    public function __call(string $method, array $args)
    {
    }
    public function scan(&$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function getOption($name)
    {
    }
}
