<?php

namespace Symfony\Component\Cache\Adapter;

class FilesystemAdapter extends \Symfony\Component\Cache\Adapter\AbstractAdapter implements \Symfony\Component\Cache\PruneableInterface
{
    use \Symfony\Component\Cache\Traits\FilesystemTrait;
    public function __construct(string $namespace = '', int $defaultLifetime = 0, string $directory = null, \Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller = null)
    {
    }
}
