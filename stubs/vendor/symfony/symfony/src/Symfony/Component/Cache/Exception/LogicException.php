<?php

namespace Symfony\Component\Cache\Exception;

class LogicException extends \LogicException implements \Psr\Cache\CacheException, \Psr\SimpleCache\CacheException
{
}
