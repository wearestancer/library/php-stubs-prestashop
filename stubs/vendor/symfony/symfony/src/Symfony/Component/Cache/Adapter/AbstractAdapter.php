<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
abstract class AbstractAdapter implements \Symfony\Component\Cache\Adapter\AdapterInterface, \Symfony\Contracts\Cache\CacheInterface, \Psr\Log\LoggerAwareInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\AbstractAdapterTrait;
    use \Symfony\Component\Cache\Traits\ContractsTrait;
    /**
     * @internal
     */
    protected const NS_SEPARATOR = ':';
    protected function __construct(string $namespace = '', int $defaultLifetime = 0)
    {
    }
    /**
     * Returns the best possible adapter that your runtime supports.
     *
     * Using ApcuAdapter makes system caches compatible with read-only filesystems.
     *
     * @param string $namespace
     * @param int    $defaultLifetime
     * @param string $version
     * @param string $directory
     *
     * @return AdapterInterface
     */
    public static function createSystemCache($namespace, $defaultLifetime, $version, $directory, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    public static function createConnection($dsn, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
    }
}
