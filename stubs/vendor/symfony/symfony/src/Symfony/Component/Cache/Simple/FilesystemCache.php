<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use FilesystemAdapter and type-hint for CacheInterface instead.
 */
class FilesystemCache extends \Symfony\Component\Cache\Simple\AbstractCache implements \Symfony\Component\Cache\PruneableInterface
{
    use \Symfony\Component\Cache\Traits\FilesystemTrait;
    public function __construct(string $namespace = '', int $defaultLifetime = 0, string $directory = null, \Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller = null)
    {
    }
}
