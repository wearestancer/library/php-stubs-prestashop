<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use DoctrineAdapter and type-hint for CacheInterface instead.
 */
class DoctrineCache extends \Symfony\Component\Cache\Simple\AbstractCache
{
    use \Symfony\Component\Cache\Traits\DoctrineTrait;
    public function __construct(\Doctrine\Common\Cache\CacheProvider $provider, string $namespace = '', int $defaultLifetime = 0)
    {
    }
}
