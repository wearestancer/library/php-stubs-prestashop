<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * Chains several adapters together.
 *
 * Cached items are fetched from the first adapter having them in its data store.
 * They are saved and deleted in all adapters at once.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class ChainAdapter implements \Symfony\Component\Cache\Adapter\AdapterInterface, \Symfony\Contracts\Cache\CacheInterface, \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\ContractsTrait;
    /**
     * @param CacheItemPoolInterface[] $adapters        The ordered list of adapters used to fetch cached items
     * @param int                      $defaultLifetime The default lifetime of items propagated from lower adapters to upper ones
     */
    public function __construct(array $adapters, int $defaultLifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItems(array $keys = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItems(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prune()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
