<?php

namespace Symfony\Component\Cache\Adapter;

class PhpFilesAdapter extends \Symfony\Component\Cache\Adapter\AbstractAdapter implements \Symfony\Component\Cache\PruneableInterface
{
    use \Symfony\Component\Cache\Traits\PhpFilesTrait;
    /**
     * @param $appendOnly Set to `true` to gain extra performance when the items stored in this pool never expire.
     *                    Doing so is encouraged because it fits perfectly OPcache's memory model.
     *
     * @throws CacheException if OPcache is not enabled
     */
    public function __construct(string $namespace = '', int $defaultLifetime = 0, string $directory = null, bool $appendOnly = false)
    {
    }
}
