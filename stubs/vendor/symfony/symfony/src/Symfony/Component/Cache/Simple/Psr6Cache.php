<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use Psr16Cache instead.
 */
class Psr6Cache extends \Symfony\Component\Cache\Psr16Cache
{
}
