<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * Stores tag id <> cache id relationship as a symlink, and lookup on invalidation calls.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 * @author André Rømcke <andre.romcke+symfony@gmail.com>
 */
class FilesystemTagAwareAdapter extends \Symfony\Component\Cache\Adapter\AbstractTagAwareAdapter implements \Symfony\Component\Cache\PruneableInterface
{
    use \Symfony\Component\Cache\Traits\FilesystemTrait {
        doClear as private doClearCache;
        doSave as private doSaveCache;
    }
    public function __construct(string $namespace = '', int $defaultLifetime = 0, string $directory = null, \Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, int $lifetime, array $addTagData = [], array $removeTagData = []) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDeleteYieldTags(array $ids) : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDeleteTagRelations(array $tagData) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doInvalidate(array $tagIds) : bool
    {
    }
}
