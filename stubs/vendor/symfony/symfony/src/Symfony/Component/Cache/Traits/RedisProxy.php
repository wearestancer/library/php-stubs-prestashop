<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class RedisProxy
{
    public function __construct(\Redis $redis, \Closure $initializer)
    {
    }
    public function __call($method, array $args)
    {
    }
    public function hscan($strKey, &$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function scan(&$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function sscan($strKey, &$iIterator, $strPattern = null, $iCount = null)
    {
    }
    public function zscan($strKey, &$iIterator, $strPattern = null, $iCount = null)
    {
    }
}
