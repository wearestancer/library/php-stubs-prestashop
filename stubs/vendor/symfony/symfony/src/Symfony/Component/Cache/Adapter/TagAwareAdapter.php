<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class TagAwareAdapter implements \Symfony\Component\Cache\Adapter\TagAwareAdapterInterface, \Symfony\Contracts\Cache\TagAwareCacheInterface, \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface, \Psr\Log\LoggerAwareInterface
{
    use \Symfony\Component\Cache\Traits\ContractsTrait;
    use \Psr\Log\LoggerAwareTrait;
    use \Symfony\Component\Cache\Traits\ProxyTrait;
    public const TAGS_PREFIX = "\x00tags\x00";
    public function __construct(\Symfony\Component\Cache\Adapter\AdapterInterface $itemsPool, \Symfony\Component\Cache\Adapter\AdapterInterface $tagsPool = null, float $knownTagVersionsTtl = 0.15)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function invalidateTags(array $tags)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasItem($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItems(array $keys = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItems(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
