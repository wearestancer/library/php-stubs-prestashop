<?php

namespace Symfony\Component\Cache\Adapter;

class RedisAdapter extends \Symfony\Component\Cache\Adapter\AbstractAdapter
{
    use \Symfony\Component\Cache\Traits\RedisTrait;
    /**
     * @param \Redis|\RedisArray|\RedisCluster|\Predis\ClientInterface|RedisProxy|RedisClusterProxy $redis           The redis client
     * @param string                                                                                $namespace       The default namespace
     * @param int                                                                                   $defaultLifetime The default lifetime
     */
    public function __construct($redis, string $namespace = '', int $defaultLifetime = 0, \Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller = null)
    {
    }
}
