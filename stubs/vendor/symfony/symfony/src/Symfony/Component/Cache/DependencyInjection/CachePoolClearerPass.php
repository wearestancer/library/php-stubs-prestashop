<?php

namespace Symfony\Component\Cache\DependencyInjection;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CachePoolClearerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $cachePoolClearerTag = 'cache.pool.clearer')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
