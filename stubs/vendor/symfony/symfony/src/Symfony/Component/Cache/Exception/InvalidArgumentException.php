<?php

namespace Symfony\Component\Cache\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements \Psr\Cache\InvalidArgumentException, \Psr\SimpleCache\InvalidArgumentException
{
}
