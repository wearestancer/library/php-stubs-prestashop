<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Aurimas Niekis <aurimas@niekis.lt>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
trait RedisTrait
{
    private static $defaultConnectionOptions = ['class' => null, 'persistent' => 0, 'persistent_id' => null, 'timeout' => 30, 'read_timeout' => 0, 'retry_interval' => 0, 'tcp_keepalive' => 0, 'lazy' => null, 'redis_cluster' => false, 'redis_sentinel' => null, 'dbindex' => 0, 'failover' => 'none', 'ssl' => null];
    private $redis;
    private $marshaller;
    /**
     * @param \Redis|\RedisArray|\RedisCluster|\Predis\ClientInterface|RedisProxy|RedisClusterProxy $redis
     */
    private function init($redis, string $namespace, int $defaultLifetime, ?\Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller)
    {
    }
    /**
     * Creates a Redis connection using a DSN configuration.
     *
     * Example DSN:
     *   - redis://localhost
     *   - redis://example.com:1234
     *   - redis://secret@example.com/13
     *   - redis:///var/run/redis.sock
     *   - redis://secret@/var/run/redis.sock/13
     *
     * @param string $dsn
     * @param array  $options See self::$defaultConnectionOptions
     *
     * @throws InvalidArgumentException when the DSN is invalid
     *
     * @return \Redis|\RedisCluster|RedisClusterProxy|RedisProxy|\Predis\ClientInterface According to the "class" option
     */
    public static function createConnection($dsn, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, int $lifetime)
    {
    }
    private function pipeline(\Closure $generator, $redis = null) : \Generator
    {
    }
    private function getHosts() : array
    {
    }
}
