<?php

namespace Symfony\Component\Cache\Exception;

class CacheException extends \Exception implements \Psr\Cache\CacheException, \Psr\SimpleCache\CacheException
{
}
