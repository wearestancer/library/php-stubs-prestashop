<?php

namespace Symfony\Component\Cache\Simple;

/**
 * Chains several caches together.
 *
 * Cached items are fetched from the first cache having them in its data store.
 * They are saved and deleted in all caches at once.
 *
 * @deprecated since Symfony 4.3, use ChainAdapter and type-hint for CacheInterface instead.
 */
class ChainCache implements \Psr\SimpleCache\CacheInterface, \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface
{
    /**
     * @param Psr16CacheInterface[] $caches          The ordered list of caches used to fetch cached items
     * @param int                   $defaultLifetime The lifetime of items propagated from lower caches to upper ones
     */
    public function __construct(array $caches, int $defaultLifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return iterable
     */
    public function getMultiple($keys, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function has($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function delete($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteMultiple($keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function set($key, $value, $ttl = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function setMultiple($values, $ttl = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prune()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
}
