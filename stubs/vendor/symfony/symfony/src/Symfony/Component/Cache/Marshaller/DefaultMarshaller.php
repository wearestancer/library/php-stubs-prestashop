<?php

namespace Symfony\Component\Cache\Marshaller;

/**
 * Serializes/unserializes values using igbinary_serialize() if available, serialize() otherwise.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class DefaultMarshaller implements \Symfony\Component\Cache\Marshaller\MarshallerInterface
{
    public function __construct(bool $useIgbinarySerialize = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function marshall(array $values, ?array &$failed) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function unmarshall(string $value)
    {
    }
    /**
     * @internal
     */
    public static function handleUnserializeCallback($class)
    {
    }
}
