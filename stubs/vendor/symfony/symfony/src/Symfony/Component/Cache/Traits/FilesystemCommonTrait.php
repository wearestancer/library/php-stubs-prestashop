<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
trait FilesystemCommonTrait
{
    private $directory;
    private $tmp;
    private function init(string $namespace, ?string $directory)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
    }
    protected function doUnlink($file)
    {
    }
    private function write(string $file, string $data, int $expiresAt = null)
    {
    }
    private function getFile(string $id, bool $mkdir = false, string $directory = null)
    {
    }
    private function getFileKey(string $file) : string
    {
    }
    private function scanHashDir(string $directory) : \Generator
    {
    }
    /**
     * @internal
     */
    public static function throwError($type, $message, $file, $line)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
