<?php

namespace Symfony\Component\Cache;

/**
 * Resets a pool's local state.
 */
interface ResettableInterface extends \Symfony\Contracts\Service\ResetInterface
{
}
