<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use RedisAdapter and type-hint for CacheInterface instead.
 */
class RedisCache extends \Symfony\Component\Cache\Simple\AbstractCache
{
    use \Symfony\Component\Cache\Traits\RedisTrait;
    /**
     * @param \Redis|\RedisArray|\RedisCluster|\Predis\ClientInterface|RedisProxy|RedisClusterProxy $redis
     */
    public function __construct($redis, string $namespace = '', int $defaultLifetime = 0, \Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller = null)
    {
    }
}
