<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * Abstract for native TagAware adapters.
 *
 * To keep info on tags, the tags are both serialized as part of cache value and provided as tag ids
 * to Adapters on operations when needed for storage to doSave(), doDelete() & doInvalidate().
 *
 * @author Nicolas Grekas <p@tchwork.com>
 * @author André Rømcke <andre.romcke+symfony@gmail.com>
 *
 * @internal
 */
abstract class AbstractTagAwareAdapter implements \Symfony\Component\Cache\Adapter\TagAwareAdapterInterface, \Symfony\Contracts\Cache\TagAwareCacheInterface, \Psr\Log\LoggerAwareInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\AbstractAdapterTrait;
    use \Symfony\Component\Cache\Traits\ContractsTrait;
    protected function __construct(string $namespace = '', int $defaultLifetime = 0)
    {
    }
    /**
     * Persists several cache items immediately.
     *
     * @param array   $values        The values to cache, indexed by their cache identifier
     * @param int     $lifetime      The lifetime of the cached values, 0 for persisting until manual cleaning
     * @param array[] $addTagData    Hash where key is tag id, and array value is list of cache id's to add to tag
     * @param array[] $removeTagData Hash where key is tag id, and array value is list of cache id's to remove to tag
     *
     * @return array The identifiers that failed to be cached or a boolean stating if caching succeeded or not
     */
    protected abstract function doSave(array $values, int $lifetime, array $addTagData = [], array $removeTagData = []) : array;
    /**
     * Removes multiple items from the pool and their corresponding tags.
     *
     * @param array $ids An array of identifiers that should be removed from the pool
     *
     * @return bool True if the items were successfully removed, false otherwise
     */
    protected abstract function doDelete(array $ids);
    /**
     * Removes relations between tags and deleted items.
     *
     * @param array $tagData Array of tag => key identifiers that should be removed from the pool
     */
    protected abstract function doDeleteTagRelations(array $tagData) : bool;
    /**
     * Invalidates cached items using tags.
     *
     * @param string[] $tagIds An array of tags to invalidate, key is tag and value is tag id
     *
     * @return bool True on success
     */
    protected abstract function doInvalidate(array $tagIds) : bool;
    /**
     * Delete items and yields the tags they were bound to.
     */
    protected function doDeleteYieldTags(array $ids) : iterable
    {
    }
    /**
     * {@inheritdoc}
     */
    public function commit() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function deleteItems(array $keys) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function invalidateTags(array $tags)
    {
    }
}
