<?php

namespace Symfony\Component\Cache;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class CacheItem implements \Symfony\Contracts\Cache\ItemInterface
{
    /**
     * {@inheritdoc}
     */
    public function getKey() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function get()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isHit() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function set($value) : self
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function expiresAt($expiration) : self
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function expiresAfter($time) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function tag($tags) : \Symfony\Contracts\Cache\ItemInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMetadata() : array
    {
    }
    /**
     * Returns the list of tags bound to the value coming from the pool storage if any.
     *
     * @deprecated since Symfony 4.2, use the "getMetadata()" method instead.
     */
    public function getPreviousTags() : array
    {
    }
    /**
     * Validates a cache key according to PSR-6.
     *
     * @param mixed $key The key to validate
     *
     * @throws InvalidArgumentException When $key is not valid
     */
    public static function validateKey($key) : string
    {
    }
    /**
     * Internal logging helper.
     *
     * @internal
     */
    public static function log(?\Psr\Log\LoggerInterface $logger, string $message, array $context = [])
    {
    }
}
