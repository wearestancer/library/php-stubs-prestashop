<?php

namespace Symfony\Component\Cache\Adapter;

class DoctrineAdapter extends \Symfony\Component\Cache\Adapter\AbstractAdapter
{
    use \Symfony\Component\Cache\Traits\DoctrineTrait;
    public function __construct(\Doctrine\Common\Cache\CacheProvider $provider, string $namespace = '', int $defaultLifetime = 0)
    {
    }
}
