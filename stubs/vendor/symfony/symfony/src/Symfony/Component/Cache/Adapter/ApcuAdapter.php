<?php

namespace Symfony\Component\Cache\Adapter;

class ApcuAdapter extends \Symfony\Component\Cache\Adapter\AbstractAdapter
{
    use \Symfony\Component\Cache\Traits\ApcuTrait;
    /**
     * @throws CacheException if APCu is not enabled
     */
    public function __construct(string $namespace = '', int $defaultLifetime = 0, string $version = null)
    {
    }
}
