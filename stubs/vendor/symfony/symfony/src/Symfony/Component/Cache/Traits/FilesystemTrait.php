<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Rob Frawley 2nd <rmf@src.run>
 *
 * @internal
 */
trait FilesystemTrait
{
    use \Symfony\Component\Cache\Traits\FilesystemCommonTrait;
    private $marshaller;
    /**
     * @return bool
     */
    public function prune()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, int $lifetime)
    {
    }
    private function getFileKey(string $file) : string
    {
    }
}
