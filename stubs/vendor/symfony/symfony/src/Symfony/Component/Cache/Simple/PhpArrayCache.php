<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use PhpArrayAdapter and type-hint for CacheInterface instead.
 */
class PhpArrayCache implements \Psr\SimpleCache\CacheInterface, \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\PhpArrayTrait;
    /**
     * @param string              $file         The PHP file were values are cached
     * @param Psr16CacheInterface $fallbackPool A pool to fallback on when an item is not hit
     */
    public function __construct(string $file, \Psr\SimpleCache\CacheInterface $fallbackPool)
    {
    }
    /**
     * This adapter takes advantage of how PHP stores arrays in its latest versions.
     *
     * @param string         $file         The PHP file were values are cached
     * @param CacheInterface $fallbackPool A pool to fallback on when an item is not hit
     *
     * @return Psr16CacheInterface
     */
    public static function create($file, \Psr\SimpleCache\CacheInterface $fallbackPool)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return iterable
     */
    public function getMultiple($keys, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function has($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function delete($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteMultiple($keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function set($key, $value, $ttl = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function setMultiple($values, $ttl = null)
    {
    }
}
