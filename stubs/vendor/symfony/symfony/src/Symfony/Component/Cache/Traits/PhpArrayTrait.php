<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Titouan Galopin <galopintitouan@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
trait PhpArrayTrait
{
    use \Symfony\Component\Cache\Traits\ProxyTrait;
    private $file;
    private $keys;
    private $values;
    private static $valuesCache = [];
    /**
     * Store an array of cached values.
     *
     * @param array $values The cached values
     */
    public function warmUp(array $values)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * Load the cache file.
     */
    private function initialize()
    {
    }
}
