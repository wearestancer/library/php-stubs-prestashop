<?php

namespace Symfony\Component\Cache;

/**
 * LockRegistry is used internally by existing adapters to protect against cache stampede.
 *
 * It does so by wrapping the computation of items in a pool of locks.
 * Foreach each apps, there can be at most 20 concurrent processes that
 * compute items at the same time and only one per cache-key.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class LockRegistry
{
    /**
     * Defines a set of existing files that will be used as keys to acquire locks.
     *
     * @return array The previously defined set of files
     */
    public static function setFiles(array $files) : array
    {
    }
    public static function compute(callable $callback, \Symfony\Contracts\Cache\ItemInterface $item, bool &$save, \Symfony\Contracts\Cache\CacheInterface $pool, \Closure $setMetadata = null, \Psr\Log\LoggerInterface $logger = null)
    {
    }
}
