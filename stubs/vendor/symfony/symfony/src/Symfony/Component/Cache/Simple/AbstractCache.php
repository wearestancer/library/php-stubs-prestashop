<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use AbstractAdapter and type-hint for CacheInterface instead.
 */
abstract class AbstractCache implements \Psr\SimpleCache\CacheInterface, \Psr\Log\LoggerAwareInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\AbstractTrait {
        deleteItems as private;
        \Symfony\Component\Cache\Traits\AbstractTrait::deleteItem as delete;
        \Symfony\Component\Cache\Traits\AbstractTrait::hasItem as has;
    }
    /**
     * @internal
     */
    protected const NS_SEPARATOR = ':';
    protected function __construct(string $namespace = '', int $defaultLifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function set($key, $value, $ttl = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return iterable
     */
    public function getMultiple($keys, $default = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function setMultiple($values, $ttl = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteMultiple($keys)
    {
    }
}
