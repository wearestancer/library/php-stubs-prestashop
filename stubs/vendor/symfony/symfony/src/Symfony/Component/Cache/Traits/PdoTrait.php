<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @internal
 */
trait PdoTrait
{
    private $marshaller;
    private $conn;
    private $dsn;
    private $driver;
    private $serverVersion;
    private $table = 'cache_items';
    private $idCol = 'item_id';
    private $dataCol = 'item_data';
    private $lifetimeCol = 'item_lifetime';
    private $timeCol = 'item_time';
    private $username = '';
    private $password = '';
    private $connectionOptions = [];
    private $namespace;
    private function init($connOrDsn, string $namespace, int $defaultLifetime, array $options, ?\Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller)
    {
    }
    /**
     * Creates the table to store cache items which can be called once for setup.
     *
     * Cache ID are saved in a column of maximum length 255. Cache data is
     * saved in a BLOB.
     *
     * @throws \PDOException    When the table already exists
     * @throws DBALException    When the table already exists
     * @throws Exception        When the table already exists
     * @throws \DomainException When an unsupported PDO driver is used
     */
    public function createTable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prune()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, int $lifetime)
    {
    }
    /**
     * @return \PDO|Connection
     */
    private function getConnection()
    {
    }
    private function getServerVersion() : string
    {
    }
}
