<?php

namespace Symfony\Component\Cache\Marshaller;

/**
 * Compresses values using gzdeflate().
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class DeflateMarshaller implements \Symfony\Component\Cache\Marshaller\MarshallerInterface
{
    public function __construct(\Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function marshall(array $values, ?array &$failed) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function unmarshall(string $value)
    {
    }
}
