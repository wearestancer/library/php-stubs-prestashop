<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * An adapter that collects data about all cache calls.
 *
 * @author Aaron Scherer <aequasi@gmail.com>
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 * @author Nicolas Grekas <p@tchwork.com>
 */
class TraceableAdapter implements \Symfony\Component\Cache\Adapter\AdapterInterface, \Symfony\Contracts\Cache\CacheInterface, \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface
{
    protected $pool;
    public function __construct(\Symfony\Component\Cache\Adapter\AdapterInterface $pool)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItems(array $keys = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItems(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function prune()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(string $key) : bool
    {
    }
    public function getCalls()
    {
    }
    public function clearCalls()
    {
    }
    protected function start($name)
    {
    }
}
class TraceableAdapterEvent
{
    public $name;
    public $start;
    public $end;
    public $result;
    public $hits = 0;
    public $misses = 0;
}
