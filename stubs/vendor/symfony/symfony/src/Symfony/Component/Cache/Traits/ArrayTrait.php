<?php

namespace Symfony\Component\Cache\Traits;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
trait ArrayTrait
{
    use \Psr\Log\LoggerAwareTrait;
    private $storeSerialized;
    private $values = [];
    private $expiries = [];
    /**
     * Returns all cached values, with cache miss as null.
     *
     * @return array
     */
    public function getValues()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItem($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    private function generateItems(array $keys, float $now, callable $f) : iterable
    {
    }
    private function freeze($value, $key)
    {
    }
    private function unfreeze(string $key, bool &$isHit)
    {
    }
}
