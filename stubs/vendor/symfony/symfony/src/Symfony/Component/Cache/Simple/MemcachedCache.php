<?php

namespace Symfony\Component\Cache\Simple;

/**
 * @deprecated since Symfony 4.3, use MemcachedAdapter and type-hint for CacheInterface instead.
 */
class MemcachedCache extends \Symfony\Component\Cache\Simple\AbstractCache
{
    use \Symfony\Component\Cache\Traits\MemcachedTrait;
    protected $maxIdLength = 250;
    public function __construct(\Memcached $client, string $namespace = '', int $defaultLifetime = 0, \Symfony\Component\Cache\Marshaller\MarshallerInterface $marshaller = null)
    {
    }
}
