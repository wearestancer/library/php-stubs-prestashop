<?php

namespace Symfony\Component\Cache\DependencyInjection;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CachePoolPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $cachePoolTag = 'cache.pool', string $kernelResetTag = 'kernel.reset', string $cacheClearerId = 'cache.global_clearer', string $cachePoolClearerTag = 'cache.pool.clearer', string $cacheSystemClearerId = 'cache.system_clearer', string $cacheSystemClearerTag = 'kernel.cache_clearer')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * @internal
     */
    public static function getServiceProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $name)
    {
    }
}
