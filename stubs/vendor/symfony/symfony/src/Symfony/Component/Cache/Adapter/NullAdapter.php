<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * @author Titouan Galopin <galopintitouan@gmail.com>
 */
class NullAdapter implements \Symfony\Component\Cache\Adapter\AdapterInterface, \Symfony\Contracts\Cache\CacheInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItems(array $keys = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItems(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function delete(string $key) : bool
    {
    }
}
