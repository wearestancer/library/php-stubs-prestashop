<?php

namespace Symfony\Component\Cache\Adapter;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class ProxyAdapter implements \Symfony\Component\Cache\Adapter\AdapterInterface, \Symfony\Contracts\Cache\CacheInterface, \Symfony\Component\Cache\PruneableInterface, \Symfony\Component\Cache\ResettableInterface
{
    use \Symfony\Component\Cache\Traits\ContractsTrait;
    use \Symfony\Component\Cache\Traits\ProxyTrait;
    public function __construct(\Psr\Cache\CacheItemPoolInterface $pool, string $namespace = '', int $defaultLifetime = 0)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItem($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getItems(array $keys = [])
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function hasItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function clear()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItem($key)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function deleteItems(array $keys)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
    }
}
