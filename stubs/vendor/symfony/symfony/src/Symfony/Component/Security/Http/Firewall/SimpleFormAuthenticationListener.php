<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @deprecated since Symfony 4.2, use Guard instead.
 */
class SimpleFormAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener
{
    /**
     * @throws \InvalidArgumentException In case no simple authenticator is provided
     */
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy, \Symfony\Component\Security\Http\HttpUtils $httpUtils, string $providerKey, \Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface $successHandler, \Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface $failureHandler, array $options = [], \Psr\Log\LoggerInterface $logger = null, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager = null, \Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface $simpleAuthenticator = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function requiresAuthentication(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function attemptAuthentication(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
