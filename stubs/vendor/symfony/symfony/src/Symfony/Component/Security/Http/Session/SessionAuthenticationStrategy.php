<?php

namespace Symfony\Component\Security\Http\Session;

/**
 * The default session strategy implementation.
 *
 * Supports the following strategies:
 * NONE: the session is not changed
 * MIGRATE: the session id is updated, attributes are kept
 * INVALIDATE: the session id is updated, attributes are lost
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class SessionAuthenticationStrategy implements \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface
{
    public const NONE = 'none';
    public const MIGRATE = 'migrate';
    public const INVALIDATE = 'invalidate';
    public function __construct(string $strategy, \Symfony\Component\Security\Csrf\TokenStorage\ClearableTokenStorageInterface $csrfTokenStorage = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function onAuthentication(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
