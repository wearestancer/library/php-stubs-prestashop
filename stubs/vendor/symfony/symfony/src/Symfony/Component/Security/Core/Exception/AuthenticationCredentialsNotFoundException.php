<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * AuthenticationCredentialsNotFoundException is thrown when an authentication is rejected
 * because no Token is available.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class AuthenticationCredentialsNotFoundException extends \Symfony\Component\Security\Core\Exception\AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
}
