<?php

namespace Symfony\Component\Security\Http\Event;

/**
 * SwitchUserEvent.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class SwitchUserEvent extends \Symfony\Component\EventDispatcher\Event
{
    public function __construct(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\User\UserInterface $targetUser, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null)
    {
    }
    /**
     * @return Request
     */
    public function getRequest()
    {
    }
    /**
     * @return UserInterface
     */
    public function getTargetUser()
    {
    }
    /**
     * @return TokenInterface|null
     */
    public function getToken()
    {
    }
    public function setToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
