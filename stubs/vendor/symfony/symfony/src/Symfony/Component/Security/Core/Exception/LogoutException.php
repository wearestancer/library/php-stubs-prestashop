<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * LogoutException is thrown when the account cannot be logged out.
 *
 * @author Jeremy Mikola <jmikola@gmail.com>
 */
class LogoutException extends \Symfony\Component\Security\Core\Exception\RuntimeException
{
    public function __construct(string $message = 'Logout Exception', \Throwable $previous = null)
    {
    }
}
