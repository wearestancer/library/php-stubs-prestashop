<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * AbstractPreAuthenticatedListener is the base class for all listener that
 * authenticates users based on a pre-authenticated request (like a certificate
 * for instance).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal since Symfony 4.3
 */
abstract class AbstractPreAuthenticatedListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    protected $logger;
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, string $providerKey, \Psr\Log\LoggerInterface $logger = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Handles pre-authentication.
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Call this method if your authentication token is stored to a session.
     *
     * @final
     */
    public function setSessionAuthenticationStrategy(\Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy)
    {
    }
    /**
     * Gets the user and credentials from the Request.
     *
     * @return array An array composed of the user and the credentials
     */
    protected abstract function getPreAuthenticatedData(\Symfony\Component\HttpFoundation\Request $request);
}
