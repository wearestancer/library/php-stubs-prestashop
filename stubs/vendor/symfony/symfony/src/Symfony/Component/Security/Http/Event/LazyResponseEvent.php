<?php

namespace Symfony\Component\Security\Http\Event;

/**
 * Wraps a lazily computed response in a signaling exception.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class LazyResponseEvent extends \Symfony\Component\HttpKernel\Event\RequestEvent
{
    public function __construct(parent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setResponse(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getKernel() : \Symfony\Component\HttpKernel\HttpKernelInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequest() : \Symfony\Component\HttpFoundation\Request
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRequestType() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isMasterRequest() : bool
    {
    }
}
