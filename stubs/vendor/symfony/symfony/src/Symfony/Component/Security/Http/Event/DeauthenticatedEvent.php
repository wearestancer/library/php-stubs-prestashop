<?php

namespace Symfony\Component\Security\Http\Event;

/**
 * Deauthentication happens in case the user has changed when trying to refresh the token.
 *
 * @author Hamza Amrouche <hamza.simperfit@gmail.com>
 *
 * @final since Symfony 4.4
 */
class DeauthenticatedEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $originalToken, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $refreshedToken)
    {
    }
    public function getRefreshedToken() : \Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
    }
    public function getOriginalToken() : \Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
    }
}
