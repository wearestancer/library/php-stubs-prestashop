<?php

namespace Symfony\Component\Security\Guard\Token;

/**
 * The token used by the guard auth system before authentication.
 *
 * The GuardAuthenticationListener creates this, which is then consumed
 * immediately by the GuardAuthenticationProvider. If authentication is
 * successful, a different authenticated token is returned
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class PreAuthenticationGuardToken extends \Symfony\Component\Security\Core\Authentication\Token\AbstractToken implements \Symfony\Component\Security\Guard\Token\GuardTokenInterface
{
    /**
     * @param mixed  $credentials
     * @param string $guardProviderKey Unique key that bind this token to a specific AuthenticatorInterface
     */
    public function __construct($credentials, string $guardProviderKey)
    {
    }
    public function getGuardProviderKey()
    {
    }
    /**
     * Returns the user credentials, which might be an array of anything you
     * wanted to put in there (e.g. username, password, favoriteColor).
     *
     * @return mixed The user credentials
     */
    public function getCredentials()
    {
    }
    public function setAuthenticated($authenticated)
    {
    }
}
