<?php

namespace Symfony\Component\Security\Core\Authentication\RememberMe;

/**
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 *
 * @internal
 */
final class PersistentToken implements \Symfony\Component\Security\Core\Authentication\RememberMe\PersistentTokenInterface
{
    public function __construct(string $class, string $username, string $series, string $tokenValue, \DateTime $lastUsed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getClass() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUsername() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSeries() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getTokenValue() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLastUsed() : \DateTime
    {
    }
}
