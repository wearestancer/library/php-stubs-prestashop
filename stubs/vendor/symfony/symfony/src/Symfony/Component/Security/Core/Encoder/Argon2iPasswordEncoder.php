<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * Argon2iPasswordEncoder uses the Argon2i hashing algorithm.
 *
 * @author Zan Baldwin <hello@zanbaldwin.com>
 * @author Dominik Müller <dominik.mueller@jkweb.ch>
 *
 * @deprecated since Symfony 4.3, use SodiumPasswordEncoder instead
 */
class Argon2iPasswordEncoder extends \Symfony\Component\Security\Core\Encoder\BasePasswordEncoder implements \Symfony\Component\Security\Core\Encoder\SelfSaltingEncoderInterface
{
    /**
     * Argon2iPasswordEncoder constructor.
     *
     * @param int|null $memoryCost memory usage of the algorithm
     * @param int|null $timeCost   number of iterations
     * @param int|null $threads    number of parallel threads
     */
    public function __construct(int $memoryCost = null, int $timeCost = null, int $threads = null)
    {
    }
    public static function isSupported()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encodePassword($raw, $salt)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
    }
}
