<?php

namespace Symfony\Component\Security\Core\Event;

/**
 * This event is dispatched on voter vote.
 *
 * @author Laurent VOULLEMIER <laurent.voullemier@gmail.com>
 *
 * @internal
 */
final class VoteEvent extends \Symfony\Component\EventDispatcher\Event
{
    public function __construct(\Symfony\Component\Security\Core\Authorization\Voter\VoterInterface $voter, $subject, array $attributes, int $vote)
    {
    }
    public function getVoter() : \Symfony\Component\Security\Core\Authorization\Voter\VoterInterface
    {
    }
    public function getSubject()
    {
    }
    public function getAttributes() : array
    {
    }
    public function getVote() : int
    {
    }
}
