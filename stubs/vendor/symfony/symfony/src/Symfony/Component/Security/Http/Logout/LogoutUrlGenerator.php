<?php

namespace Symfony\Component\Security\Http\Logout;

/**
 * Provides generator functions for the logout URL.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Jeremy Mikola <jmikola@gmail.com>
 */
class LogoutUrlGenerator
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack = null, \Symfony\Component\Routing\Generator\UrlGeneratorInterface $router = null, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage = null)
    {
    }
    /**
     * Registers a firewall's LogoutListener, allowing its URL to be generated.
     *
     * @param string      $key           The firewall key
     * @param string      $logoutPath    The path that starts the logout process
     * @param string|null $csrfTokenId   The ID of the CSRF token
     * @param string|null $csrfParameter The CSRF token parameter name
     * @param string|null $context       The listener context
     */
    public function registerListener($key, $logoutPath, $csrfTokenId, $csrfParameter, \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager = null, string $context = null)
    {
    }
    /**
     * Generates the absolute logout path for the firewall.
     *
     * @param string|null $key The firewall key or null to use the current firewall key
     *
     * @return string The logout path
     */
    public function getLogoutPath($key = null)
    {
    }
    /**
     * Generates the absolute logout URL for the firewall.
     *
     * @param string|null $key The firewall key or null to use the current firewall key
     *
     * @return string The logout URL
     */
    public function getLogoutUrl($key = null)
    {
    }
    /**
     * @param string|null $key     The current firewall key
     * @param string|null $context The current firewall context
     */
    public function setCurrentFirewall($key, $context = null)
    {
    }
}
