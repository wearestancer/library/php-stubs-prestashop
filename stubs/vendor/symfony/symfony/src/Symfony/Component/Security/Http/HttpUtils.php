<?php

namespace Symfony\Component\Security\Http;

/**
 * Encapsulates the logic needed to create sub-requests, redirect the user, and match URLs.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpUtils
{
    /**
     * @param UrlMatcherInterface|RequestMatcherInterface $urlMatcher         The URL or Request matcher
     * @param string|null                                 $domainRegexp       A regexp the target of HTTP redirections must match, scheme included
     * @param string|null                                 $secureDomainRegexp A regexp the target of HTTP redirections must match when the scheme is "https"
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(\Symfony\Component\Routing\Generator\UrlGeneratorInterface $urlGenerator = null, $urlMatcher = null, string $domainRegexp = null, string $secureDomainRegexp = null)
    {
    }
    /**
     * Creates a redirect Response.
     *
     * @param string $path   A path (an absolute path (/foo), an absolute URL (http://...), or a route name (foo))
     * @param int    $status The status code
     *
     * @return RedirectResponse A RedirectResponse instance
     */
    public function createRedirectResponse(\Symfony\Component\HttpFoundation\Request $request, $path, $status = 302)
    {
    }
    /**
     * Creates a Request.
     *
     * @param string $path A path (an absolute path (/foo), an absolute URL (http://...), or a route name (foo))
     *
     * @return Request A Request instance
     */
    public function createRequest(\Symfony\Component\HttpFoundation\Request $request, $path)
    {
    }
    /**
     * Checks that a given path matches the Request.
     *
     * @param string $path A path (an absolute path (/foo), an absolute URL (http://...), or a route name (foo))
     *
     * @return bool true if the path is the same as the one from the Request, false otherwise
     */
    public function checkRequestPath(\Symfony\Component\HttpFoundation\Request $request, $path)
    {
    }
    /**
     * Generates a URI, based on the given path or absolute URL.
     *
     * @param Request $request A Request instance
     * @param string  $path    A path (an absolute path (/foo), an absolute URL (http://...), or a route name (foo))
     *
     * @return string An absolute URL
     *
     * @throws \LogicException
     */
    public function generateUri($request, $path)
    {
    }
}
