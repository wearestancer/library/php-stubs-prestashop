<?php

namespace Symfony\Component\Security\Guard;

/**
 * A utility class that does much of the *work* during the guard authentication process.
 *
 * By having the logic here instead of the listener, more of the process
 * can be called directly (e.g. for manual authentication) or overridden.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 *
 * @final
 */
class GuardAuthenticatorHandler
{
    /**
     * @param array $statelessProviderKeys An array of provider/firewall keys that are "stateless" and so do not need the session migrated on success
     */
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher = null, array $statelessProviderKeys = [])
    {
    }
    /**
     * Authenticates the given token in the system.
     */
    public function authenticateWithToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, \Symfony\Component\HttpFoundation\Request $request, string $providerKey = null)
    {
    }
    /**
     * Returns the "on success" response for the given GuardAuthenticator.
     */
    public function handleAuthenticationSuccess(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, \Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Guard\AuthenticatorInterface $guardAuthenticator, string $providerKey) : ?\Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Convenience method for authenticating the user and returning the
     * Response *if any* for success.
     */
    public function authenticateUserAndHandleSuccess(\Symfony\Component\Security\Core\User\UserInterface $user, \Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Guard\AuthenticatorInterface $authenticator, string $providerKey) : ?\Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Handles an authentication failure and returns the Response for the
     * GuardAuthenticator.
     */
    public function handleAuthenticationFailure(\Symfony\Component\Security\Core\Exception\AuthenticationException $authenticationException, \Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Guard\AuthenticatorInterface $guardAuthenticator, string $providerKey) : ?\Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Call this method if your authentication token is stored to a session.
     *
     * @final
     */
    public function setSessionAuthenticationStrategy(\Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy)
    {
    }
}
