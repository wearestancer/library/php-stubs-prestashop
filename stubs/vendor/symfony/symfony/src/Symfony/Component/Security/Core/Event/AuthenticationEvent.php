<?php

namespace Symfony\Component\Security\Core\Event;

/**
 * This is a general purpose authentication event.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class AuthenticationEvent extends \Symfony\Component\EventDispatcher\Event
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    public function getAuthenticationToken()
    {
    }
}
