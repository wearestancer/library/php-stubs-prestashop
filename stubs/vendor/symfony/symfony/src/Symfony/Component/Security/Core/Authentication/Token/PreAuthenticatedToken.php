<?php

namespace Symfony\Component\Security\Core\Authentication\Token;

/**
 * PreAuthenticatedToken implements a pre-authenticated token.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PreAuthenticatedToken extends \Symfony\Component\Security\Core\Authentication\Token\AbstractToken
{
    /**
     * @param string|\Stringable|UserInterface $user
     * @param mixed                            $credentials
     * @param string[]                         $roles
     */
    public function __construct($user, $credentials, string $providerKey, array $roles = [])
    {
    }
    /**
     * Returns the provider key.
     *
     * @return string The provider key
     */
    public function getProviderKey()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
