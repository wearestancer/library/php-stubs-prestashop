<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * DisabledException is thrown when the user account is disabled.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class DisabledException extends \Symfony\Component\Security\Core\Exception\AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
}
