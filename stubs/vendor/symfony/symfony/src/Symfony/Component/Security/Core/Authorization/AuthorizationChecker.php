<?php

namespace Symfony\Component\Security\Core\Authorization;

/**
 * AuthorizationChecker is the main authorization point of the Security component.
 *
 * It gives access to the token representing the current user authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class AuthorizationChecker implements \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, \Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface $accessDecisionManager, bool $alwaysAuthenticate = false)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws AuthenticationCredentialsNotFoundException when the token storage has no authentication token
     */
    public final function isGranted($attributes, $subject = null) : bool
    {
    }
}
