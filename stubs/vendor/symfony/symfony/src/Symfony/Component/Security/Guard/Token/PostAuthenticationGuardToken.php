<?php

namespace Symfony\Component\Security\Guard\Token;

/**
 * Used as an "authenticated" token, though it could be set to not-authenticated later.
 *
 * If you're using Guard authentication, you *must* use a class that implements
 * GuardTokenInterface as your authenticated token (like this class).
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class PostAuthenticationGuardToken extends \Symfony\Component\Security\Core\Authentication\Token\AbstractToken implements \Symfony\Component\Security\Guard\Token\GuardTokenInterface
{
    /**
     * @param string   $providerKey The provider (firewall) key
     * @param string[] $roles       An array of roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(\Symfony\Component\Security\Core\User\UserInterface $user, string $providerKey, array $roles)
    {
    }
    /**
     * This is meant to be only an authenticated token, where credentials
     * have already been used and are thus cleared.
     *
     * {@inheritdoc}
     */
    public function getCredentials()
    {
    }
    /**
     * Returns the provider (firewall) key.
     *
     * @return string
     */
    public function getProviderKey()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
