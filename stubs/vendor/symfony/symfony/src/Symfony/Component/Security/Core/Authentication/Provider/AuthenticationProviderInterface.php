<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * AuthenticationProviderInterface is the interface for all authentication
 * providers.
 *
 * Concrete implementations processes specific Token instances.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface AuthenticationProviderInterface extends \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface
{
    /**
     * Use this constant for not provided username.
     *
     * @var string
     */
    public const USERNAME_NONE_PROVIDED = 'NONE_PROVIDED';
    /**
     * Checks whether this provider supports the given token.
     *
     * @return bool true if the implementation supports the Token, false otherwise
     */
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token);
}
