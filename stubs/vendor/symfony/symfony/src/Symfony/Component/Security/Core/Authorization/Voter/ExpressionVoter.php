<?php

namespace Symfony\Component\Security\Core\Authorization\Voter;

/**
 * ExpressionVoter votes based on the evaluation of an expression.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ExpressionVoter implements \Symfony\Component\Security\Core\Authorization\Voter\VoterInterface
{
    /**
     * @param AuthorizationCheckerInterface $authChecker
     */
    public function __construct(\Symfony\Component\Security\Core\Authorization\ExpressionLanguage $expressionLanguage, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $trustResolver, $authChecker = null, \Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy = null)
    {
    }
    /**
     * @deprecated since Symfony 4.1, register the provider directly on the injected ExpressionLanguage instance instead.
     */
    public function addExpressionLanguageProvider(\Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface $provider)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function vote(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, $subject, array $attributes)
    {
    }
}
