<?php

namespace Symfony\Component\Security\Http\EntryPoint;

/**
 * RetryAuthenticationEntryPoint redirects URL based on the configured scheme.
 *
 * This entry point is not intended to work with HTTP post requests.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RetryAuthenticationEntryPoint implements \Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface
{
    public function __construct(int $httpPort = 80, int $httpsPort = 443)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function start(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $authException = null)
    {
    }
}
