<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * The AbstractAuthenticationListener is the preferred base class for all
 * browser-/HTTP-based authentication requests.
 *
 * Subclasses likely have to implement the following:
 * - an TokenInterface to hold authentication related data
 * - an AuthenticationProvider to perform the actual authentication of the
 *   token, retrieve the UserInterface implementation from a database, and
 *   perform the specific account checks using the UserChecker
 *
 * By default, this listener only is active for a specific path, e.g.
 * /login_check. If you want to change this behavior, you can overwrite the
 * requiresAuthentication() method.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
abstract class AbstractAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    protected $options;
    protected $logger;
    protected $authenticationManager;
    protected $providerKey;
    protected $httpUtils;
    /**
     * @throws \InvalidArgumentException
     */
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy, \Symfony\Component\Security\Http\HttpUtils $httpUtils, string $providerKey, \Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface $successHandler, \Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface $failureHandler, array $options = [], \Psr\Log\LoggerInterface $logger = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    /**
     * Sets the RememberMeServices implementation to use.
     */
    public function setRememberMeServices(\Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface $rememberMeServices)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Handles form based authentication.
     *
     * @throws \RuntimeException
     * @throws SessionUnavailableException
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Whether this request requires authentication.
     *
     * The default implementation only processes requests to a specific path,
     * but a subclass could change this to only authenticate requests where a
     * certain parameters is present.
     *
     * @return bool
     */
    protected function requiresAuthentication(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Performs authentication.
     *
     * @return TokenInterface|Response|null The authenticated token, null if full authentication is not possible, or a Response
     *
     * @throws AuthenticationException if the authentication fails
     */
    protected abstract function attemptAuthentication(\Symfony\Component\HttpFoundation\Request $request);
}
