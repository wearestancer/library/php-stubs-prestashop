<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * AuthenticationExpiredException is thrown when an authenticated token becomes un-authenticated between requests.
 *
 * In practice, this is due to the User changing between requests (e.g. password changes),
 * causes the token to become un-authenticated.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class AuthenticationExpiredException extends \Symfony\Component\Security\Core\Exception\AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
}
