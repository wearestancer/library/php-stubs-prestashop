<?php

namespace Symfony\Component\Security\Core\User;

/**
 * UserChecker checks the user account flags.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UserChecker implements \Symfony\Component\Security\Core\User\UserCheckerInterface
{
    /**
     * {@inheritdoc}
     */
    public function checkPreAuth(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function checkPostAuth(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
}
