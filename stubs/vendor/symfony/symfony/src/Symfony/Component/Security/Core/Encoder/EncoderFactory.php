<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * A generic encoder factory implementation.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class EncoderFactory implements \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
{
    public function __construct(array $encoders)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEncoder($user)
    {
    }
}
