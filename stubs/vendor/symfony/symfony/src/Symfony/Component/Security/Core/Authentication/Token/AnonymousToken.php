<?php

namespace Symfony\Component\Security\Core\Authentication\Token;

/**
 * AnonymousToken represents an anonymous token.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AnonymousToken extends \Symfony\Component\Security\Core\Authentication\Token\AbstractToken
{
    /**
     * @param string                           $secret A secret used to make sure the token is created by the app and not by a malicious client
     * @param string|\Stringable|UserInterface $user
     * @param string[]                         $roles
     */
    public function __construct(string $secret, $user, array $roles = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
    }
    /**
     * Returns the secret.
     *
     * @return string
     */
    public function getSecret()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
