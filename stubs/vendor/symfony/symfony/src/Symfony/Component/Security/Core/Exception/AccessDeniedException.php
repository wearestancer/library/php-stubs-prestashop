<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * AccessDeniedException is thrown when the account has not the required role.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AccessDeniedException extends \Symfony\Component\Security\Core\Exception\RuntimeException
{
    public function __construct(string $message = 'Access Denied.', \Throwable $previous = null)
    {
    }
    /**
     * @return array
     */
    public function getAttributes()
    {
    }
    /**
     * @param array|string $attributes
     */
    public function setAttributes($attributes)
    {
    }
    /**
     * @return mixed
     */
    public function getSubject()
    {
    }
    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
    }
}
