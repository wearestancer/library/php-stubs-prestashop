<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * REMOTE_USER authentication listener.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Maxime Douailin <maxime.douailin@gmail.com>
 */
class RemoteUserAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractPreAuthenticatedListener
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, string $providerKey, string $userKey = 'REMOTE_USER', \Psr\Log\LoggerInterface $logger = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getPreAuthenticatedData(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
