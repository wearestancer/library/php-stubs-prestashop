<?php

namespace Symfony\Component\Security\Http\Util;

/**
 * Trait to get (and set) the URL the user last visited before being forced to authenticate.
 */
trait TargetPathTrait
{
    /**
     * Sets the target path the user should be redirected to after authentication.
     *
     * Usually, you do not need to set this directly.
     */
    private function saveTargetPath(\Symfony\Component\HttpFoundation\Session\SessionInterface $session, string $providerKey, string $uri)
    {
    }
    /**
     * Returns the URL (if any) the user visited that forced them to login.
     */
    private function getTargetPath(\Symfony\Component\HttpFoundation\Session\SessionInterface $session, string $providerKey) : ?string
    {
    }
    /**
     * Removes the target path from the session.
     */
    private function removeTargetPath(\Symfony\Component\HttpFoundation\Session\SessionInterface $session, string $providerKey)
    {
    }
}
