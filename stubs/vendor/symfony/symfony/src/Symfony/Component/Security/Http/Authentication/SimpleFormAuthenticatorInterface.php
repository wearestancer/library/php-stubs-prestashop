<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @deprecated since Symfony 4.2, use Guard instead.
 */
interface SimpleFormAuthenticatorInterface extends \Symfony\Component\Security\Core\Authentication\SimpleAuthenticatorInterface
{
    public function createToken(\Symfony\Component\HttpFoundation\Request $request, $username, $password, $providerKey);
}
