<?php

namespace Symfony\Component\Security\Core\Authentication;

/**
 * The default implementation of the authentication trust resolver.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class AuthenticationTrustResolver implements \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface
{
    public function __construct(string $anonymousClass = null, string $rememberMeClass = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isAnonymous(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isRememberMe(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFullFledged(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null)
    {
    }
}
