<?php

namespace Symfony\Component\Security\Core\Authorization;

/**
 * Decorates the original AccessDecisionManager class to log information
 * about the security voters and the decisions made by them.
 *
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 *
 * @internal
 */
class TraceableAccessDecisionManager implements \Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface
{
    // Logs being filled in
    public function __construct(\Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface $manager)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param bool $allowMultipleAttributes Whether to allow passing multiple values to the $attributes array
     */
    public function decide(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, array $attributes, $object = null) : bool
    {
    }
    /**
     * Adds voter vote and class to the voter details.
     *
     * @param array $attributes attributes used for the vote
     * @param int   $vote       vote of the voter
     */
    public function addVoterVote(\Symfony\Component\Security\Core\Authorization\Voter\VoterInterface $voter, array $attributes, int $vote)
    {
    }
    public function getStrategy() : string
    {
    }
    /**
     * @return iterable|VoterInterface[]
     */
    public function getVoters() : iterable
    {
    }
    public function getDecisionLog() : array
    {
    }
}
