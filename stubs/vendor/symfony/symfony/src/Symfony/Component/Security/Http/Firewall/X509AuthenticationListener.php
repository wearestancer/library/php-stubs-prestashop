<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * X509 authentication listener.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class X509AuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractPreAuthenticatedListener
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, string $providerKey, string $userKey = 'SSL_CLIENT_S_DN_Email', string $credentialKey = 'SSL_CLIENT_S_DN', \Psr\Log\LoggerInterface $logger = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getPreAuthenticatedData(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
