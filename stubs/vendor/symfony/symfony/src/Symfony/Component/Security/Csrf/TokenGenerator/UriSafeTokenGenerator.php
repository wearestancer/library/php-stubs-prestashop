<?php

namespace Symfony\Component\Security\Csrf\TokenGenerator;

/**
 * Generates CSRF tokens.
 *
 * @author Bernhard Schussek <bernhard.schussek@symfony.com>
 */
class UriSafeTokenGenerator implements \Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface
{
    /**
     * Generates URI-safe CSRF tokens.
     *
     * @param int $entropy The amount of entropy collected for each token (in bits)
     */
    public function __construct(int $entropy = 256)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function generateToken()
    {
    }
}
