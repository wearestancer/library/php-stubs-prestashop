<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * Hashes passwords using the best available encoder.
 * Validates them using a chain of encoders.
 *
 * /!\ Don't put a PlaintextPasswordEncoder in the list as that'd mean a leaked hash
 * could be used to authenticate successfully without knowing the cleartext password.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class MigratingPasswordEncoder extends \Symfony\Component\Security\Core\Encoder\BasePasswordEncoder implements \Symfony\Component\Security\Core\Encoder\SelfSaltingEncoderInterface
{
    public function __construct(\Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface $bestEncoder, \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface ...$extraEncoders)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encodePassword($raw, $salt) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isPasswordValid($encoded, $raw, $salt) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function needsRehash(string $encoded) : bool
    {
    }
}
