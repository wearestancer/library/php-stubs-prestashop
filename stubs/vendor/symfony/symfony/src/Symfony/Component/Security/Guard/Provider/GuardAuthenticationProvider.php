<?php

namespace Symfony\Component\Security\Guard\Provider;

/**
 * Responsible for accepting the PreAuthenticationGuardToken and calling
 * the correct authenticator to retrieve the authenticated token.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class GuardAuthenticationProvider implements \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
{
    /**
     * @param iterable|AuthenticatorInterface[] $guardAuthenticators The authenticators, with keys that match what's passed to GuardAuthenticationListener
     * @param string                            $providerKey         The provider (i.e. firewall) key
     */
    public function __construct(iterable $guardAuthenticators, \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider, string $providerKey, \Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker, \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $passwordEncoder = null)
    {
    }
    /**
     * Finds the correct authenticator for the token and calls it.
     *
     * @param GuardTokenInterface $token
     *
     * @return TokenInterface
     */
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
