<?php

namespace Symfony\Component\Security\Http\Logout;

/**
 * Default logout success handler will redirect users to a configured path.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class DefaultLogoutSuccessHandler implements \Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface
{
    protected $httpUtils;
    protected $targetUrl;
    public function __construct(\Symfony\Component\Security\Http\HttpUtils $httpUtils, string $targetUrl = '/')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function onLogoutSuccess(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
