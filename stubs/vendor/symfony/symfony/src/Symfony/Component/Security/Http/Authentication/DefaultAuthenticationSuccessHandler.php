<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * Class with the default authentication success handling logic.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class DefaultAuthenticationSuccessHandler implements \Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface
{
    use \Symfony\Component\Security\Http\Util\TargetPathTrait;
    protected $httpUtils;
    protected $logger;
    protected $options;
    protected $providerKey;
    protected $defaultOptions = ['always_use_default_target_path' => false, 'default_target_path' => '/', 'login_path' => '/login', 'target_path_parameter' => '_target_path', 'use_referer' => false];
    /**
     * @param array $options Options for processing a successful authentication attempt
     */
    public function __construct(\Symfony\Component\Security\Http\HttpUtils $httpUtils, array $options = [], \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * Gets the options.
     *
     * @return array An array of options
     */
    public function getOptions()
    {
    }
    public function setOptions(array $options)
    {
    }
    /**
     * Get the provider key.
     *
     * @return string
     */
    public function getProviderKey()
    {
    }
    /**
     * Set the provider key.
     *
     * @param string $providerKey
     */
    public function setProviderKey($providerKey)
    {
    }
    /**
     * Builds the target URL according to the defined options.
     *
     * @return string
     */
    protected function determineTargetUrl(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
