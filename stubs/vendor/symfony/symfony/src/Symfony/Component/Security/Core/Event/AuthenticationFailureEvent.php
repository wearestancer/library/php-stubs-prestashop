<?php

namespace Symfony\Component\Security\Core\Event;

/**
 * This event is dispatched on authentication failure.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 *
 * @final since Symfony 4.4
 */
class AuthenticationFailureEvent extends \Symfony\Component\Security\Core\Event\AuthenticationEvent
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, \Symfony\Component\Security\Core\Exception\AuthenticationException $ex)
    {
    }
    public function getAuthenticationException()
    {
    }
}
