<?php

namespace Symfony\Component\Security\Csrf\Exception;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class TokenNotFoundException extends \Symfony\Component\Security\Core\Exception\RuntimeException
{
}
