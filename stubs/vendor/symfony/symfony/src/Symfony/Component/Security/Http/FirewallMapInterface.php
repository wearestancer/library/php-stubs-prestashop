<?php

namespace Symfony\Component\Security\Http;

/**
 * This interface must be implemented by firewall maps.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
interface FirewallMapInterface
{
    /**
     * Returns the authentication listeners, and the exception listener to use
     * for the given request.
     *
     * If there are no authentication listeners, the first inner array must be
     * empty.
     *
     * If there is no exception listener, the second element of the outer array
     * must be null.
     *
     * If there is no logout listener, the third element of the outer array
     * must be null.
     *
     * @return array of the format [[AuthenticationListener], ExceptionListener, LogoutListener]
     */
    public function getListeners(\Symfony\Component\HttpFoundation\Request $request);
}
