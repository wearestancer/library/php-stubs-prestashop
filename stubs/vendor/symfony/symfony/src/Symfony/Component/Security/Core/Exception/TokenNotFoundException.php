<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * TokenNotFoundException is thrown if a Token cannot be found.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class TokenNotFoundException extends \Symfony\Component\Security\Core\Exception\AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
}
