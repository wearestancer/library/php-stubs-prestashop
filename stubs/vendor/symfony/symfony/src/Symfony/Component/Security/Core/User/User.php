<?php

namespace Symfony\Component\Security\Core\User;

/**
 * User is the user implementation used by the in-memory user provider.
 *
 * This should not be used for anything else.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class User implements \Symfony\Component\Security\Core\User\UserInterface, \Symfony\Component\Security\Core\User\EquatableInterface, \Symfony\Component\Security\Core\User\AdvancedUserInterface
{
    public function __construct(?string $username, ?string $password, array $roles = [], bool $enabled = true, bool $userNonExpired = true, bool $credentialsNonExpired = true, bool $userNonLocked = true, array $extraFields = [])
    {
    }
    public function __toString() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoles() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPassword() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSalt() : ?string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUsername() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isEnabled() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }
    public function getExtraFields() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isEqualTo(\Symfony\Component\Security\Core\User\UserInterface $user) : bool
    {
    }
    public function setPassword(string $password)
    {
    }
}
