<?php

namespace Symfony\Component\Security\Core\Authorization;

/**
 * Define some ExpressionLanguage functions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ExpressionLanguageProvider implements \Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface
{
    public function getFunctions()
    {
    }
}
