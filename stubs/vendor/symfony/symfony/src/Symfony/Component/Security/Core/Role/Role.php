<?php

namespace Symfony\Component\Security\Core\Role;

/**
 * Role is a simple implementation representing a role identified by a string.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.3, to be removed in 5.0. Use strings as roles instead.
 */
class Role
{
    public function __construct(string $role)
    {
    }
    /**
     * Returns a string representation of the role.
     *
     * @return string
     */
    public function getRole()
    {
    }
    public function __toString() : string
    {
    }
}
