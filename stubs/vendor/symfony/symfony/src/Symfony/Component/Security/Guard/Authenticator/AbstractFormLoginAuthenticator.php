<?php

namespace Symfony\Component\Security\Guard\Authenticator;

/**
 * A base class to make form login authentication easier!
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
abstract class AbstractFormLoginAuthenticator extends \Symfony\Component\Security\Guard\AbstractGuardAuthenticator
{
    /**
     * Return the URL to the login page.
     *
     * @return string
     */
    protected abstract function getLoginUrl();
    /**
     * Override to change what happens after a bad username/password is submitted.
     *
     * @return Response
     */
    public function onAuthenticationFailure(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $exception)
    {
    }
    public function supportsRememberMe()
    {
    }
    /**
     * Override to control what happens when the user hits a secure page
     * but isn't logged in yet.
     *
     * @return Response
     */
    public function start(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $authException = null)
    {
    }
}
