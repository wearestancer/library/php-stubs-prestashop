<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * Interface that must be implemented by firewall listeners.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 *
 * @deprecated since Symfony 4.3, turn listeners into callables instead
 */
interface ListenerInterface
{
    public function handle(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event);
}
