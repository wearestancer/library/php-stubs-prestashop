<?php

namespace Symfony\Component\Security\Http;

/**
 * @internal
 */
final class ParameterBagUtils
{
    /**
     * Returns a "parameter" value.
     *
     * Paths like foo[bar] will be evaluated to find deeper items in nested data structures.
     *
     * @param string $path The key
     *
     * @return mixed
     *
     * @throws InvalidArgumentException when the given path is malformed
     */
    public static function getParameterBagValue(\Symfony\Component\HttpFoundation\ParameterBag $parameters, $path)
    {
    }
    /**
     * Returns a request "parameter" value.
     *
     * Paths like foo[bar] will be evaluated to find deeper items in nested data structures.
     *
     * @param string $path The key
     *
     * @return mixed
     *
     * @throws InvalidArgumentException when the given path is malformed
     */
    public static function getRequestParameterValue(\Symfony\Component\HttpFoundation\Request $request, $path)
    {
    }
}
