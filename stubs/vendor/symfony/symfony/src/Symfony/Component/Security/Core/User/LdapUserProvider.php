<?php

namespace Symfony\Component\Security\Core\User;

/**
 * LdapUserProvider is a simple user provider on top of ldap.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Charles Sarrazin <charles@sarraz.in>
 *
 * @deprecated since Symfony 4.4, use "Symfony\Component\Ldap\Security\LdapUserProvider" instead
 */
class LdapUserProvider extends \Symfony\Component\Ldap\Security\LdapUserProvider
{
    /**
     * {@inheritdoc}
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
    }
    /**
     * Loads a user from an LDAP entry.
     *
     * @return User
     */
    protected function loadUser($username, \Symfony\Component\Ldap\Entry $entry)
    {
    }
}
