<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * SimplePreAuthenticationListener implements simple proxying to an authenticator.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @deprecated since Symfony 4.2, use Guard instead.
 */
class SimplePreAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, string $providerKey, \Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface $simpleAuthenticator, \Psr\Log\LoggerInterface $logger = null, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $trustResolver = null)
    {
    }
    /**
     * Call this method if your authentication token is stored to a session.
     *
     * @final
     */
    public function setSessionAuthenticationStrategy(\Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy)
    {
    }
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Handles basic authentication.
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
}
