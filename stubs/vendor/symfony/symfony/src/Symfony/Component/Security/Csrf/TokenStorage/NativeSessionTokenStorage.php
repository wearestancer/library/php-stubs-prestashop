<?php

namespace Symfony\Component\Security\Csrf\TokenStorage;

/**
 * Token storage that uses PHP's native session handling.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class NativeSessionTokenStorage implements \Symfony\Component\Security\Csrf\TokenStorage\ClearableTokenStorageInterface
{
    /**
     * The namespace used to store values in the session.
     */
    public const SESSION_NAMESPACE = '_csrf';
    /**
     * Initializes the storage with a session namespace.
     *
     * @param string $namespace The namespace under which the token is stored in the session
     */
    public function __construct(string $namespace = self::SESSION_NAMESPACE)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setToken($tokenId, $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
