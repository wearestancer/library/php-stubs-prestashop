<?php

namespace Symfony\Component\Security\Core\Role;

/**
 * SwitchUserRole is used when the current user temporarily impersonates
 * another one.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0. Use strings as roles instead.
 */
class SwitchUserRole extends \Symfony\Component\Security\Core\Role\Role
{
    /**
     * @param string $role The role as a string
     */
    public function __construct(string $role, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $source)
    {
    }
    /**
     * Returns the original Token.
     *
     * @return TokenInterface The original TokenInterface instance
     */
    public function getSource()
    {
    }
}
