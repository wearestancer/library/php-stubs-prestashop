<?php

namespace Symfony\Component\Security\Core\Validator\Constraints;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class UserPassword extends \Symfony\Component\Validator\Constraint
{
    public $message = 'This value should be the user\'s current password.';
    public $service = 'security.validator.user_password';
    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
    }
}
