<?php

namespace Symfony\Component\Security\Http\RememberMe;

/**
 * Concrete implementation of the RememberMeServicesInterface providing
 * remember-me capabilities without requiring a TokenProvider.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class TokenBasedRememberMeServices extends \Symfony\Component\Security\Http\RememberMe\AbstractRememberMeServices
{
    /**
     * {@inheritdoc}
     */
    protected function processAutoLoginCookie(array $cookieParts, \Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function onLoginSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * Generates the cookie value.
     *
     * @param string      $class
     * @param string      $username The username
     * @param int         $expires  The Unix timestamp when the cookie expires
     * @param string|null $password The encoded password
     *
     * @return string
     */
    protected function generateCookieValue($class, $username, $expires, $password)
    {
    }
    /**
     * Generates a hash for the cookie to ensure it is not being tampered with.
     *
     * @param string      $class
     * @param string      $username The username
     * @param int         $expires  The Unix timestamp when the cookie expires
     * @param string|null $password The encoded password
     *
     * @return string
     */
    protected function generateCookieHash($class, $username, $expires, $password)
    {
    }
}
