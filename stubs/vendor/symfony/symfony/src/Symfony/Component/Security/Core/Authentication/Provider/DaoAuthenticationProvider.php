<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * DaoAuthenticationProvider uses a UserProviderInterface to retrieve the user
 * for a UsernamePasswordToken.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DaoAuthenticationProvider extends \Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider
{
    public function __construct(\Symfony\Component\Security\Core\User\UserProviderInterface $userProvider, \Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker, string $providerKey, \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface $encoderFactory, bool $hideUserNotFoundExceptions = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function checkAuthentication(\Symfony\Component\Security\Core\User\UserInterface $user, \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function retrieveUser($username, \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken $token)
    {
    }
}
