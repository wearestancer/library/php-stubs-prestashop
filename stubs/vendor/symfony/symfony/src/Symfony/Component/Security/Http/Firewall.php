<?php

namespace Symfony\Component\Security\Http;

/**
 * Firewall uses a FirewallMap to register security listeners for the given
 * request.
 *
 * It allows for different security strategies within the same application
 * (a Basic authentication for the /api, and a web based authentication for
 * everything else for instance).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Firewall implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\Security\Http\FirewallMapInterface $map, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
    /**
     * @internal since Symfony 4.3
     */
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    /**
     * @internal since Symfony 4.3
     */
    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
    protected function callListeners(\Symfony\Component\HttpKernel\Event\RequestEvent $event, iterable $listeners)
    {
    }
    /**
     * @deprecated since Symfony 4.3, use callListeners instead
     */
    protected function handleRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event, $listeners)
    {
    }
}
