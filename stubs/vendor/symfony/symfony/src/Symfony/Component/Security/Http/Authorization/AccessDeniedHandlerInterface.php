<?php

namespace Symfony\Component\Security\Http\Authorization;

/**
 * This is used by the ExceptionListener to translate an AccessDeniedException
 * to a Response object.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
interface AccessDeniedHandlerInterface
{
    /**
     * Handles an access denied failure.
     *
     * @return Response|null
     */
    public function handle(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AccessDeniedException $accessDeniedException);
}
