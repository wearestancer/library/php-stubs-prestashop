<?php

namespace Symfony\Component\Security\Core\Authentication;

/**
 * AuthenticationProviderManager uses a list of AuthenticationProviderInterface
 * instances to authenticate a Token.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class AuthenticationProviderManager implements \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface
{
    /**
     * @param iterable|AuthenticationProviderInterface[] $providers        An iterable with AuthenticationProviderInterface instances as values
     * @param bool                                       $eraseCredentials Whether to erase credentials after authentication or not
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(iterable $providers, bool $eraseCredentials = true)
    {
    }
    /**
     * @final since Symfony 4.3, the type-hint will be updated to the interface from symfony/contracts in 5.0
     */
    public function setEventDispatcher(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
