<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * ContextListener manages the SecurityContext persistence through a session.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 *
 * @final since Symfony 4.3
 */
class ContextListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    /**
     * @param iterable|UserProviderInterface[] $userProviders
     */
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, iterable $userProviders, string $contextKey, \Psr\Log\LoggerInterface $logger = null, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $trustResolver = null, callable $sessionTrackerEnabler = null)
    {
    }
    /**
     * Enables deauthentication during refreshUser when the user has changed.
     *
     * @param bool $logoutOnUserChange
     *
     * @deprecated since Symfony 4.1
     */
    public function setLogoutOnUserChange($logoutOnUserChange)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Reads the Security Token from the session.
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Writes the security token into the session.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    /**
     * Refreshes the user by reloading it from the user provider.
     *
     * @return TokenInterface|null
     *
     * @throws \RuntimeException
     */
    protected function refreshUser(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * @internal
     */
    public static function handleUnserializeCallback(string $class)
    {
    }
    public function setRememberMeServices(\Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface $rememberMeServices)
    {
    }
}
