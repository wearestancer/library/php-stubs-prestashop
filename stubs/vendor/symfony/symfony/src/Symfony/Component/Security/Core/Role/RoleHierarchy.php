<?php

namespace Symfony\Component\Security\Core\Role;

/**
 * RoleHierarchy defines a role hierarchy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RoleHierarchy implements \Symfony\Component\Security\Core\Role\RoleHierarchyInterface
{
    protected $map;
    /**
     * @param array $hierarchy An array defining the hierarchy
     */
    public function __construct(array $hierarchy)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getReachableRoles(array $roles)
    {
    }
    /**
     * @param string[] $roles
     *
     * @return string[]
     */
    public function getReachableRoleNames(array $roles) : array
    {
    }
    protected function buildRoleMap()
    {
    }
}
