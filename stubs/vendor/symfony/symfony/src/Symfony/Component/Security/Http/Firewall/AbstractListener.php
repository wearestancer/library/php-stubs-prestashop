<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * A base class for listeners that can tell whether they should authenticate incoming requests.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
abstract class AbstractListener
{
    public final function __invoke(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Tells whether the authenticate() method should be called or not depending on the incoming request.
     *
     * Returning null means authenticate() can be called lazily when accessing the token storage.
     */
    public abstract function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool;
    /**
     * Does whatever is required to authenticate the request, typically calling $event->setResponse() internally.
     */
    public abstract function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event);
}
