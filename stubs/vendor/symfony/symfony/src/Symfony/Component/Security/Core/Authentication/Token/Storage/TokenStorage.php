<?php

namespace Symfony\Component\Security\Core\Authentication\Token\Storage;

/**
 * TokenStorage contains a TokenInterface.
 *
 * It gives access to the token representing the current user authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class TokenStorage implements \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface, \Symfony\Contracts\Service\ResetInterface
{
    /**
     * {@inheritdoc}
     */
    public function getToken()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null)
    {
    }
    public function setInitializer(?callable $initializer) : void
    {
    }
    public function reset()
    {
    }
}
