<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * Hashes passwords using libsodium.
 *
 * @author Robin Chalas <robin.chalas@gmail.com>
 * @author Zan Baldwin <hello@zanbaldwin.com>
 * @author Dominik Müller <dominik.mueller@jkweb.ch>
 */
final class SodiumPasswordEncoder implements \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface, \Symfony\Component\Security\Core\Encoder\SelfSaltingEncoderInterface
{
    public function __construct(int $opsLimit = null, int $memLimit = null)
    {
    }
    public static function isSupported() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encodePassword($raw, $salt) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isPasswordValid($encoded, $raw, $salt) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function needsRehash(string $encoded) : bool
    {
    }
}
