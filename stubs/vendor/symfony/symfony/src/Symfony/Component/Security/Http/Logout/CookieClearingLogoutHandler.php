<?php

namespace Symfony\Component\Security\Http\Logout;

/**
 * This handler clears the passed cookies when a user logs out.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class CookieClearingLogoutHandler implements \Symfony\Component\Security\Http\Logout\LogoutHandlerInterface
{
    /**
     * @param array $cookies An array of cookie names to unset
     */
    public function __construct(array $cookies)
    {
    }
    /**
     * Implementation for the LogoutHandlerInterface. Deletes all requested cookies.
     */
    public function logout(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
