<?php

namespace Symfony\Component\Security\Http;

/**
 * AccessMap allows configuration of different access control rules for
 * specific parts of the website.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AccessMap implements \Symfony\Component\Security\Http\AccessMapInterface
{
    /**
     * @param array       $attributes An array of attributes to pass to the access decision manager (like roles)
     * @param string|null $channel    The channel to enforce (http, https, or null)
     */
    public function add(\Symfony\Component\HttpFoundation\RequestMatcherInterface $requestMatcher, array $attributes = [], $channel = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPatterns(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
