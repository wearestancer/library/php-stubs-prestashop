<?php

namespace Symfony\Component\Security\Core\Authentication\Token\Storage;

/**
 * A token storage that increments the session usage index when the token is accessed.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class UsageTrackingTokenStorage implements \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface, \Symfony\Contracts\Service\ServiceSubscriberInterface
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $storage, \Psr\Container\ContainerInterface $sessionLocator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getToken() : ?\Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null) : void
    {
    }
    public function enableUsageTracking() : void
    {
    }
    public function disableUsageTracking() : void
    {
    }
    public static function getSubscribedServices() : array
    {
    }
}
