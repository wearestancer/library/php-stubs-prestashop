<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * Class with the default authentication failure handling logic.
 *
 * Can be optionally be extended from by the developer to alter the behavior
 * while keeping the default behavior.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class DefaultAuthenticationFailureHandler implements \Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface
{
    protected $httpKernel;
    protected $httpUtils;
    protected $logger;
    protected $options;
    protected $defaultOptions = ['failure_path' => null, 'failure_forward' => false, 'login_path' => '/login', 'failure_path_parameter' => '_failure_path'];
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $httpKernel, \Symfony\Component\Security\Http\HttpUtils $httpUtils, array $options = [], \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * Gets the options.
     *
     * @return array An array of options
     */
    public function getOptions()
    {
    }
    public function setOptions(array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $exception)
    {
    }
}
