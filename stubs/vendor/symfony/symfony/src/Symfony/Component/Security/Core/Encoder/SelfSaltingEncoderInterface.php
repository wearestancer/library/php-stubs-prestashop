<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * SelfSaltingEncoderInterface is a marker interface for encoders that do not
 * require a user-generated salt.
 *
 * @author Zan Baldwin <hello@zanbaldwin.com>
 */
interface SelfSaltingEncoderInterface
{
}
