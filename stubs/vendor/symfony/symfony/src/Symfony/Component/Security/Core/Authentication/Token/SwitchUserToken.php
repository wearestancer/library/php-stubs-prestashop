<?php

namespace Symfony\Component\Security\Core\Authentication\Token;

/**
 * Token representing a user who temporarily impersonates another one.
 *
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
class SwitchUserToken extends \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken
{
    /**
     * @param string|object $user        The username (like a nickname, email address, etc.), or a UserInterface instance or an object implementing a __toString method
     * @param mixed         $credentials This usually is the password of the user
     * @param string        $providerKey The provider key
     * @param string[]      $roles       An array of roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($user, $credentials, string $providerKey, array $roles, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $originalToken)
    {
    }
    public function getOriginalToken() : \Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
