<?php

namespace Symfony\Component\Security\Guard\Firewall;

/**
 * Authentication listener for the "guard" system.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 * @author Amaury Leroux de Lens <amaury@lerouxdelens.com>
 *
 * @final since Symfony 4.3
 */
class GuardAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    /**
     * @param string                            $providerKey         The provider (i.e. firewall) key
     * @param iterable|AuthenticatorInterface[] $guardAuthenticators The authenticators, with keys that match what's passed to GuardAuthenticationProvider
     */
    public function __construct(\Symfony\Component\Security\Guard\GuardAuthenticatorHandler $guardHandler, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, string $providerKey, iterable $guardAuthenticators, \Psr\Log\LoggerInterface $logger = null, bool $hideUserNotFoundExceptions = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Iterates over each authenticator to see if each wants to authenticate the request.
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Should be called if this listener will support remember me.
     */
    public function setRememberMeServices(\Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface $rememberMeServices)
    {
    }
}
