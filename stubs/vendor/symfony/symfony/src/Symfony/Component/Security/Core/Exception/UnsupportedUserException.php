<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * This exception is thrown when an account is reloaded from a provider which
 * doesn't support the passed implementation of UserInterface.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class UnsupportedUserException extends \Symfony\Component\Security\Core\Exception\AuthenticationServiceException
{
}
