<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @deprecated since Symfony 4.2, use Guard instead.
 */
interface SimplePreAuthenticatorInterface extends \Symfony\Component\Security\Core\Authentication\SimpleAuthenticatorInterface
{
    public function createToken(\Symfony\Component\HttpFoundation\Request $request, $providerKey);
}
