<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * A signaling exception that wraps a lazily computed response.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class LazyResponseException extends \Exception implements \Symfony\Component\Security\Core\Exception\ExceptionInterface
{
    public function __construct(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function getResponse() : \Symfony\Component\HttpFoundation\Response
    {
    }
}
