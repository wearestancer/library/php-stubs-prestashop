<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * UsernamePasswordJsonAuthenticationListener is a stateless implementation of
 * an authentication via a JSON document composed of a username and a password.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 *
 * @final since Symfony 4.3
 */
class UsernamePasswordJsonAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, \Symfony\Component\Security\Http\HttpUtils $httpUtils, string $providerKey, \Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface $successHandler = null, \Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface $failureHandler = null, array $options = [], \Psr\Log\LoggerInterface $logger = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher = null, \Symfony\Component\PropertyAccess\PropertyAccessorInterface $propertyAccessor = null)
    {
    }
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Call this method if your authentication token is stored to a session.
     *
     * @final
     */
    public function setSessionAuthenticationStrategy(\Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy)
    {
    }
}
