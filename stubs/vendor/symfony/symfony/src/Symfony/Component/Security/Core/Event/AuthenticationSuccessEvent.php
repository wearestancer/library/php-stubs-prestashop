<?php

namespace Symfony\Component\Security\Core\Event;

/**
 * @final since Symfony 4.4
 */
class AuthenticationSuccessEvent extends \Symfony\Component\Security\Core\Event\AuthenticationEvent
{
}
