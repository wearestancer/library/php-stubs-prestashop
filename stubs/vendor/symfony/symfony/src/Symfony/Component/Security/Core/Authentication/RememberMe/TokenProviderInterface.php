<?php

namespace Symfony\Component\Security\Core\Authentication\RememberMe;

/**
 * Interface for TokenProviders.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
interface TokenProviderInterface
{
    /**
     * Loads the active token for the given series.
     *
     * @param string $series
     *
     * @return PersistentTokenInterface
     *
     * @throws TokenNotFoundException if the token is not found
     */
    public function loadTokenBySeries($series);
    /**
     * Deletes all tokens belonging to series.
     *
     * @param string $series
     */
    public function deleteTokenBySeries($series);
    /**
     * Updates the token according to this data.
     *
     * @param string $series
     * @param string $tokenValue
     *
     * @throws TokenNotFoundException if the token is not found
     */
    public function updateToken($series, $tokenValue, \DateTime $lastUsed);
    /**
     * Creates a new token.
     */
    public function createNewToken(\Symfony\Component\Security\Core\Authentication\RememberMe\PersistentTokenInterface $token);
}
