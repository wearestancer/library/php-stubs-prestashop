<?php

namespace Symfony\Component\Security\Core\Authentication\Token;

/**
 * Base class for Token instances.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
abstract class AbstractToken implements \Symfony\Component\Security\Core\Authentication\Token\TokenInterface
{
    /**
     * @param string[] $roles An array of roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $roles = [])
    {
    }
    public function getRoleNames() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setUser($user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isAuthenticated()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($authenticated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }
    /**
     * Returns all the necessary state of the object for serialization purposes.
     *
     * There is no need to serialize any entry, they should be returned as-is.
     * If you extend this method, keep in mind you MUST guarantee parent data is present in the state.
     * Here is an example of how to extend this method:
     * <code>
     *     public function __serialize(): array
     *     {
     *         return [$this->childAttribute, parent::__serialize()];
     *     }
     * </code>
     *
     * @see __unserialize()
     */
    public function __serialize() : array
    {
    }
    /**
     * @return string
     *
     * @final since Symfony 4.3, use __serialize() instead
     *
     * @internal since Symfony 4.3, use __serialize() instead
     */
    public function serialize()
    {
    }
    /**
     * Restores the object state from an array given by __serialize().
     *
     * There is no need to unserialize any entry in $data, they are already ready-to-use.
     * If you extend this method, keep in mind you MUST pass the parent data to its respective class.
     * Here is an example of how to extend this method:
     * <code>
     *     public function __unserialize(array $data): void
     *     {
     *         [$this->childAttribute, $parentData] = $data;
     *         parent::__unserialize($parentData);
     *     }
     * </code>
     *
     * @see __serialize()
     */
    public function __unserialize(array $data) : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @final since Symfony 4.3, use __unserialize() instead
     *
     * @internal since Symfony 4.3, use __unserialize() instead
     */
    public function unserialize($serialized)
    {
    }
    /**
     * Returns the token attributes.
     *
     * @return array The token attributes
     */
    public function getAttributes()
    {
    }
    /**
     * Sets the token attributes.
     *
     * @param array $attributes The token attributes
     */
    public function setAttributes(array $attributes)
    {
    }
    /**
     * Returns true if the attribute exists.
     *
     * @param string $name The attribute name
     *
     * @return bool true if the attribute exists, false otherwise
     */
    public function hasAttribute($name)
    {
    }
    /**
     * Returns an attribute value.
     *
     * @param string $name The attribute name
     *
     * @return mixed The attribute value
     *
     * @throws \InvalidArgumentException When attribute doesn't exist for this token
     */
    public function getAttribute($name)
    {
    }
    /**
     * Sets an attribute.
     *
     * @param string $name  The attribute name
     * @param mixed  $value The attribute value
     */
    public function setAttribute($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
    }
}
