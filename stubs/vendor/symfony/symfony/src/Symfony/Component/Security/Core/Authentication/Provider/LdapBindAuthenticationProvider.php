<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * LdapBindAuthenticationProvider authenticates a user against an LDAP server.
 *
 * The only way to check user credentials is to try to connect the user with its
 * credentials to the ldap.
 *
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class LdapBindAuthenticationProvider extends \Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider
{
    public function __construct(\Symfony\Component\Security\Core\User\UserProviderInterface $userProvider, \Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker, string $providerKey, \Symfony\Component\Ldap\LdapInterface $ldap, string $dnString = '{username}', bool $hideUserNotFoundExceptions = true, string $searchDn = '', string $searchPassword = '')
    {
    }
    /**
     * Set a query string to use in order to find a DN for the username.
     *
     * @param string $queryString
     */
    public function setQueryString($queryString)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function retrieveUser($username, \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function checkAuthentication(\Symfony\Component\Security\Core\User\UserInterface $user, \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken $token)
    {
    }
}
