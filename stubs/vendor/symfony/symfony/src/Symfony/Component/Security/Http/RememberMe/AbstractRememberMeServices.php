<?php

namespace Symfony\Component\Security\Http\RememberMe;

/**
 * Base class implementing the RememberMeServicesInterface.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
abstract class AbstractRememberMeServices implements \Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface, \Symfony\Component\Security\Http\Logout\LogoutHandlerInterface
{
    public const COOKIE_DELIMITER = ':';
    protected $logger;
    protected $options = ['secure' => false, 'httponly' => true, 'samesite' => null];
    /**
     * @throws \InvalidArgumentException
     */
    public function __construct(array $userProviders, string $secret, string $providerKey, array $options = [], \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * Returns the parameter that is used for checking whether remember-me
     * services have been requested.
     *
     * @return string
     */
    public function getRememberMeParameter()
    {
    }
    /**
     * @return string
     */
    public function getSecret()
    {
    }
    /**
     * Implementation of RememberMeServicesInterface. Detects whether a remember-me
     * cookie was set, decodes it, and hands it to subclasses for further processing.
     *
     * @throws CookieTheftException
     * @throws \RuntimeException
     */
    public final function autoLogin(\Symfony\Component\HttpFoundation\Request $request) : ?\Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
    }
    /**
     * Implementation for LogoutHandlerInterface. Deletes the cookie.
     */
    public function logout(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * Implementation for RememberMeServicesInterface. Deletes the cookie when
     * an attempted authentication fails.
     */
    public final function loginFail(\Symfony\Component\HttpFoundation\Request $request, \Exception $exception = null)
    {
    }
    /**
     * Implementation for RememberMeServicesInterface. This is called when an
     * authentication is successful.
     */
    public final function loginSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * Subclasses should validate the cookie and do any additional processing
     * that is required. This is called from autoLogin().
     *
     * @return UserInterface
     */
    protected abstract function processAutoLoginCookie(array $cookieParts, \Symfony\Component\HttpFoundation\Request $request);
    protected function onLoginFail(\Symfony\Component\HttpFoundation\Request $request, \Exception $exception = null)
    {
    }
    /**
     * This is called after a user has been logged in successfully, and has
     * requested remember-me capabilities. The implementation usually sets a
     * cookie and possibly stores a persistent record of it.
     */
    protected abstract function onLoginSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token);
    protected final function getUserProvider(string $class) : \Symfony\Component\Security\Core\User\UserProviderInterface
    {
    }
    /**
     * Decodes the raw cookie value.
     *
     * @param string $rawCookie
     *
     * @return array
     */
    protected function decodeCookie($rawCookie)
    {
    }
    /**
     * Encodes the cookie parts.
     *
     * @return string
     *
     * @throws \InvalidArgumentException When $cookieParts contain the cookie delimiter. Extending class should either remove or escape it.
     */
    protected function encodeCookie(array $cookieParts)
    {
    }
    /**
     * Deletes the remember-me cookie.
     */
    protected function cancelCookie(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Checks whether remember-me capabilities were requested.
     *
     * @return bool
     */
    protected function isRememberMeRequested(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
