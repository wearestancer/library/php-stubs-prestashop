<?php

namespace Symfony\Component\Security\Core\Authorization;

/**
 * AccessDecisionManagerInterface makes authorization decisions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface AccessDecisionManagerInterface
{
    /**
     * Decides whether the access is possible or not.
     *
     * @param array  $attributes An array of attributes associated with the method being invoked
     * @param object $object     The object to secure
     *
     * @return bool true if the access is granted, false otherwise
     */
    public function decide(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, array $attributes, $object = null);
}
