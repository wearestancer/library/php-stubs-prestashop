<?php

namespace Symfony\Component\Security\Core\Authentication;

/**
 * Interface for resolving the authentication status of a given token.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
interface AuthenticationTrustResolverInterface
{
    /**
     * Resolves whether the passed token implementation is authenticated
     * anonymously.
     *
     * If null is passed, the method must return false.
     *
     * @return bool
     */
    public function isAnonymous(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null);
    /**
     * Resolves whether the passed token implementation is authenticated
     * using remember-me capabilities.
     *
     * @return bool
     */
    public function isRememberMe(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null);
    /**
     * Resolves whether the passed token implementation is fully authenticated.
     *
     * @return bool
     */
    public function isFullFledged(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token = null);
}
