<?php

namespace Symfony\Component\Security\Core\User;

/**
 * Chain User Provider.
 *
 * This provider calls several leaf providers in a chain until one is able to
 * handle the request.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ChainUserProvider implements \Symfony\Component\Security\Core\User\UserProviderInterface, \Symfony\Component\Security\Core\User\PasswordUpgraderInterface
{
    /**
     * @param iterable|UserProviderInterface[] $providers
     */
    public function __construct(iterable $providers)
    {
    }
    /**
     * @return array
     */
    public function getProviders()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function upgradePassword(\Symfony\Component\Security\Core\User\UserInterface $user, string $newEncodedPassword) : void
    {
    }
}
