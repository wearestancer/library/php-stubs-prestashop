<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

class RememberMeAuthenticationProvider implements \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
{
    /**
     * @param string $secret      A secret
     * @param string $providerKey A provider secret
     */
    public function __construct(\Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker, string $secret, string $providerKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
