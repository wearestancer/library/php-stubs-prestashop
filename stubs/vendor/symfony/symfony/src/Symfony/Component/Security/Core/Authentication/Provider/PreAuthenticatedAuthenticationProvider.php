<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * Processes a pre-authenticated authentication request.
 *
 * This authentication provider will not perform any checks on authentication
 * requests, as they should already be pre-authenticated. However, the
 * UserProviderInterface implementation may still throw a
 * UsernameNotFoundException, for example.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PreAuthenticatedAuthenticationProvider implements \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
{
    public function __construct(\Symfony\Component\Security\Core\User\UserProviderInterface $userProvider, \Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker, string $providerKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
