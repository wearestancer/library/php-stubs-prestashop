<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * Base InvalidArgumentException for the Security component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Security\Core\Exception\ExceptionInterface
{
}
