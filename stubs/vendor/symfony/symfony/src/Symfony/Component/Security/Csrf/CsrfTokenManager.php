<?php

namespace Symfony\Component\Security\Csrf;

/**
 * Default implementation of {@link CsrfTokenManagerInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class CsrfTokenManager implements \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface
{
    /**
     * @param string|RequestStack|callable|null $namespace
     *                                                     * null: generates a namespace using $_SERVER['HTTPS']
     *                                                     * string: uses the given string
     *                                                     * RequestStack: generates a namespace using the current master request
     *                                                     * callable: uses the result of this callable (must return a string)
     */
    public function __construct(\Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface $generator = null, \Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface $storage = null, $namespace = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refreshToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isTokenValid(\Symfony\Component\Security\Csrf\CsrfToken $token)
    {
    }
}
