<?php

namespace Symfony\Component\Security\Http\Event;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class InteractiveLoginEvent extends \Symfony\Component\EventDispatcher\Event
{
    public function __construct(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $authenticationToken)
    {
    }
    /**
     * Gets the request.
     *
     * @return Request A Request instance
     */
    public function getRequest()
    {
    }
    /**
     * Gets the authentication token.
     *
     * @return TokenInterface A TokenInterface instance
     */
    public function getAuthenticationToken()
    {
    }
}
