<?php

namespace Symfony\Component\Security\Csrf\TokenStorage;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
interface ClearableTokenStorageInterface extends \Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface
{
    /**
     * Removes all CSRF tokens.
     */
    public function clear();
}
