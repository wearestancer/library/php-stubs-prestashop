<?php

namespace Symfony\Component\Security\Http\EntryPoint;

/**
 * FormAuthenticationEntryPoint starts an authentication via a login form.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FormAuthenticationEntryPoint implements \Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface
{
    /**
     * @param string $loginPath  The path to the login form
     * @param bool   $useForward Whether to forward or redirect to the login form
     */
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, \Symfony\Component\Security\Http\HttpUtils $httpUtils, string $loginPath, bool $useForward = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function start(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $authException = null)
    {
    }
}
