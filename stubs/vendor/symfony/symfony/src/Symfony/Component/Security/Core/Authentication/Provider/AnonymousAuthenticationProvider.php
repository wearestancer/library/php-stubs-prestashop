<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * AnonymousAuthenticationProvider validates AnonymousToken instances.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AnonymousAuthenticationProvider implements \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
{
    /**
     * @param string $secret The secret shared with the AnonymousToken
     */
    public function __construct(string $secret)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
