<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * @deprecated
 *
 * @internal
 */
trait LegacyListenerTrait
{
    /**
     * @deprecated since Symfony 4.3, use __invoke() instead
     */
    public function handle(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
}
