<?php

namespace Symfony\Component\Security\Core\User;

/**
 * EquatableInterface used to test if two objects are equal in security
 * and re-authentication context.
 *
 * @author Dariusz Górecki <darek.krk@gmail.com>
 */
interface EquatableInterface
{
    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @return bool
     */
    public function isEqualTo(\Symfony\Component\Security\Core\User\UserInterface $user);
}
