<?php

namespace Symfony\Component\Security\Core\Authentication\Token;

/**
 * UsernamePasswordToken implements a username and password token.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UsernamePasswordToken extends \Symfony\Component\Security\Core\Authentication\Token\AbstractToken
{
    /**
     * @param string|\Stringable|UserInterface $user        The username (like a nickname, email address, etc.) or a UserInterface instance
     * @param mixed                            $credentials
     * @param string[]                         $roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($user, $credentials, string $providerKey, array $roles = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($isAuthenticated)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
    }
    /**
     * Returns the provider key.
     *
     * @return string The provider key
     */
    public function getProviderKey()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
