<?php

namespace Symfony\Component\Security\Core\Authentication\Token;

/**
 * Authentication Token for "Remember-Me".
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class RememberMeToken extends \Symfony\Component\Security\Core\Authentication\Token\AbstractToken
{
    /**
     * @param string $secret A secret used to make sure the token is created by the app and not by a malicious client
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(\Symfony\Component\Security\Core\User\UserInterface $user, string $providerKey, string $secret)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($authenticated)
    {
    }
    /**
     * Returns the provider secret.
     *
     * @return string The provider secret
     */
    public function getProviderKey()
    {
    }
    /**
     * Returns the secret.
     *
     * @return string
     */
    public function getSecret()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
