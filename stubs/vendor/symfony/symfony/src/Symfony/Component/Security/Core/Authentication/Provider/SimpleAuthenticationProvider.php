<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @deprecated since Symfony 4.2, use Guard instead.
 */
class SimpleAuthenticationProvider implements \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\SimpleAuthenticatorInterface $simpleAuthenticator, \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider, string $providerKey, \Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker = null)
    {
    }
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
