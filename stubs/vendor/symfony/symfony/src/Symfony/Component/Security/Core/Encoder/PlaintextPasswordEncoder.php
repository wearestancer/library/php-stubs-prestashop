<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * PlaintextPasswordEncoder does not do any encoding but is useful in testing environments.
 *
 * As this encoder is not cryptographically secure, usage of it in production environments is discouraged.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class PlaintextPasswordEncoder extends \Symfony\Component\Security\Core\Encoder\BasePasswordEncoder
{
    /**
     * @param bool $ignorePasswordCase Compare password case-insensitive
     */
    public function __construct(bool $ignorePasswordCase = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encodePassword($raw, $salt)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
    }
}
