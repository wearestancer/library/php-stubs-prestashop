<?php

namespace Symfony\Component\Security\Core\Authorization\Voter;

/**
 * RoleHierarchyVoter uses a RoleHierarchy to determine the roles granted to
 * the user before voting.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RoleHierarchyVoter extends \Symfony\Component\Security\Core\Authorization\Voter\RoleVoter
{
    public function __construct(\Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy, string $prefix = 'ROLE_')
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function extractRoles(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
