<?php

namespace Symfony\Component\Security\Core\Authentication\RememberMe;

/**
 * This class is used for testing purposes, and is not really suited for production.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class InMemoryTokenProvider implements \Symfony\Component\Security\Core\Authentication\RememberMe\TokenProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function loadTokenBySeries($series)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function updateToken($series, $tokenValue, \DateTime $lastUsed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function deleteTokenBySeries($series)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createNewToken(\Symfony\Component\Security\Core\Authentication\RememberMe\PersistentTokenInterface $token)
    {
    }
}
