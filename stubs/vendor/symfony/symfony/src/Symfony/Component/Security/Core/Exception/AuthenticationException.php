<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * AuthenticationException is the base class for all authentication exceptions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class AuthenticationException extends \Symfony\Component\Security\Core\Exception\RuntimeException
{
    /** @internal */
    protected $serialized;
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null)
    {
    }
    /**
     * Get the token.
     *
     * @return TokenInterface|null
     */
    public function getToken()
    {
    }
    public function setToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * Returns all the necessary state of the object for serialization purposes.
     *
     * There is no need to serialize any entry, they should be returned as-is.
     * If you extend this method, keep in mind you MUST guarantee parent data is present in the state.
     * Here is an example of how to extend this method:
     * <code>
     *     public function __serialize(): array
     *     {
     *         return [$this->childAttribute, parent::__serialize()];
     *     }
     * </code>
     *
     * @see __unserialize()
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     *
     * @final since Symfony 4.3, use __serialize() instead
     *
     * @internal since Symfony 4.3, use __serialize() instead
     */
    public function serialize()
    {
    }
    /**
     * Restores the object state from an array given by __serialize().
     *
     * There is no need to unserialize any entry in $data, they are already ready-to-use.
     * If you extend this method, keep in mind you MUST pass the parent data to its respective class.
     * Here is an example of how to extend this method:
     * <code>
     *     public function __unserialize(array $data): void
     *     {
     *         [$this->childAttribute, $parentData] = $data;
     *         parent::__unserialize($parentData);
     *     }
     * </code>
     *
     * @see __serialize()
     */
    public function __unserialize(array $data) : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * @final since Symfony 4.3, use __unserialize() instead
     *
     * @internal since Symfony 4.3, use __unserialize() instead
     */
    public function unserialize($serialized)
    {
    }
    /**
     * @internal
     */
    public function __sleep() : array
    {
    }
    /**
     * @internal
     */
    public function __wakeup() : void
    {
    }
    /**
     * Message key to be used by the translation component.
     *
     * @return string
     */
    public function getMessageKey()
    {
    }
    /**
     * Message data to be used by the translation component.
     *
     * @return array
     */
    public function getMessageData()
    {
    }
}
