<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * A generic password encoder.
 *
 * @author Ariel Ferrandini <arielferrandini@gmail.com>
 */
class UserPasswordEncoder implements \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
{
    public function __construct(\Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface $encoderFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encodePassword(\Symfony\Component\Security\Core\User\UserInterface $user, $plainPassword)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isPasswordValid(\Symfony\Component\Security\Core\User\UserInterface $user, $raw)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function needsRehash(\Symfony\Component\Security\Core\User\UserInterface $user) : bool
    {
    }
}
