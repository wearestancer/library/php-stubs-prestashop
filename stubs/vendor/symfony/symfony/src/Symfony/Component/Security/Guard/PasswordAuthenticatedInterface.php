<?php

namespace Symfony\Component\Security\Guard;

/**
 * An optional interface for "guard" authenticators that deal with user passwords.
 */
interface PasswordAuthenticatedInterface
{
    /**
     * Returns the clear-text password contained in credentials if any.
     *
     * @param mixed $credentials The user credentials
     */
    public function getPassword($credentials) : ?string;
}
