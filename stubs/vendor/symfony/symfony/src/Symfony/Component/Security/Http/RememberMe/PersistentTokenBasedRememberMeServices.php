<?php

namespace Symfony\Component\Security\Http\RememberMe;

/**
 * Concrete implementation of the RememberMeServicesInterface which needs
 * an implementation of TokenProviderInterface for providing remember-me
 * capabilities.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class PersistentTokenBasedRememberMeServices extends \Symfony\Component\Security\Http\RememberMe\AbstractRememberMeServices
{
    public function setTokenProvider(\Symfony\Component\Security\Core\Authentication\RememberMe\TokenProviderInterface $tokenProvider)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function cancelCookie(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function processAutoLoginCookie(array $cookieParts, \Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function onLoginSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
