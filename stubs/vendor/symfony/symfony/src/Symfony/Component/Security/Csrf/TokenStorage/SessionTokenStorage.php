<?php

namespace Symfony\Component\Security\Csrf\TokenStorage;

/**
 * Token storage that uses a Symfony Session object.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class SessionTokenStorage implements \Symfony\Component\Security\Csrf\TokenStorage\ClearableTokenStorageInterface
{
    /**
     * The namespace used to store values in the session.
     */
    public const SESSION_NAMESPACE = '_csrf';
    /**
     * Initializes the storage with a Session object and a session namespace.
     *
     * @param string $namespace The namespace under which the token is stored in the session
     */
    public function __construct(\Symfony\Component\HttpFoundation\Session\SessionInterface $session, string $namespace = self::SESSION_NAMESPACE)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setToken($tokenId, $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function removeToken($tokenId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
    }
}
