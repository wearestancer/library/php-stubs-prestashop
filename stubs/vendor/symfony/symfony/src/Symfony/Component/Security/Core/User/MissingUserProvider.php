<?php

namespace Symfony\Component\Security\Core\User;

/**
 * MissingUserProvider is a dummy user provider used to throw proper exception
 * when a firewall requires a user provider but none was defined.
 *
 * @internal
 */
class MissingUserProvider implements \Symfony\Component\Security\Core\User\UserProviderInterface
{
    /**
     * @param string $firewall the firewall missing a provider
     */
    public function __construct(string $firewall)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username) : \Symfony\Component\Security\Core\User\UserInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user) : \Symfony\Component\Security\Core\User\UserInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsClass($class) : bool
    {
    }
}
