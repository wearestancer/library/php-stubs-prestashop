<?php

namespace Symfony\Component\Security\Core\Authorization\Voter;

/**
 * Decorates voter classes to send result events.
 *
 * @author Laurent VOULLEMIER <laurent.voullemier@gmail.com>
 *
 * @internal
 */
class TraceableVoter implements \Symfony\Component\Security\Core\Authorization\Voter\VoterInterface
{
    public function __construct(\Symfony\Component\Security\Core\Authorization\Voter\VoterInterface $voter, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
    }
    public function vote(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, $subject, array $attributes)
    {
    }
    public function getDecoratedVoter() : \Symfony\Component\Security\Core\Authorization\Voter\VoterInterface
    {
    }
}
