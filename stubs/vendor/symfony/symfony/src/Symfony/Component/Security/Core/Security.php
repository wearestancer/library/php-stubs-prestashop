<?php

namespace Symfony\Component\Security\Core;

/**
 * Helper class for commonly-needed security tasks.
 *
 * @final
 */
class Security
{
    public const ACCESS_DENIED_ERROR = '_security.403_error';
    public const AUTHENTICATION_ERROR = '_security.last_error';
    public const LAST_USERNAME = '_security.last_username';
    public const MAX_USERNAME_LENGTH = 4096;
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * @return UserInterface|null
     */
    public function getUser()
    {
    }
    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied subject.
     *
     * @param mixed $attributes
     * @param mixed $subject
     */
    public function isGranted($attributes, $subject = null) : bool
    {
    }
    public function getToken() : ?\Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
    }
}
