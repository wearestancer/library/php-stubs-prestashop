<?php

namespace Symfony\Component\Security\Http\Logout;

/**
 * Handler for clearing invalidating the current session.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class SessionLogoutHandler implements \Symfony\Component\Security\Http\Logout\LogoutHandlerInterface
{
    /**
     * Invalidate the current session.
     */
    public function logout(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
