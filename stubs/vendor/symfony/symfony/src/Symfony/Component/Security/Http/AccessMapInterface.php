<?php

namespace Symfony\Component\Security\Http;

/**
 * AccessMap allows configuration of different access control rules for
 * specific parts of the website.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Kris Wallsmith <kris@symfony.com>
 */
interface AccessMapInterface
{
    /**
     * Returns security attributes and required channel for the supplied request.
     *
     * @return array A tuple of security attributes and the required channel
     */
    public function getPatterns(\Symfony\Component\HttpFoundation\Request $request);
}
