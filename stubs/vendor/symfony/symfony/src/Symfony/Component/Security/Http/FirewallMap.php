<?php

namespace Symfony\Component\Security\Http;

/**
 * FirewallMap allows configuration of different firewalls for specific parts
 * of the website.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FirewallMap implements \Symfony\Component\Security\Http\FirewallMapInterface
{
    public function add(\Symfony\Component\HttpFoundation\RequestMatcherInterface $requestMatcher = null, array $listeners = [], \Symfony\Component\Security\Http\Firewall\ExceptionListener $exceptionListener = null, \Symfony\Component\Security\Http\Firewall\LogoutListener $logoutListener = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListeners(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
