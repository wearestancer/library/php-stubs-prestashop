<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * UsernameNotFoundException is thrown if a User cannot be found by its username.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class UsernameNotFoundException extends \Symfony\Component\Security\Core\Exception\AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
    /**
     * Get the username.
     *
     * @return string
     */
    public function getUsername()
    {
    }
    /**
     * Set the username.
     *
     * @param string $username
     */
    public function setUsername($username)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMessageData()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
