<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class CustomAuthenticationSuccessHandler implements \Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface
{
    /**
     * @param array  $options     Options for processing a successful authentication attempt
     * @param string $providerKey The provider key
     */
    public function __construct(\Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface $handler, array $options, string $providerKey)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
