<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * BasicAuthenticationListener implements Basic HTTP authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class BasicAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, string $providerKey, \Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface $authenticationEntryPoint, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Handles basic authentication.
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Call this method if your authentication token is stored to a session.
     *
     * @final
     */
    public function setSessionAuthenticationStrategy(\Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy)
    {
    }
}
