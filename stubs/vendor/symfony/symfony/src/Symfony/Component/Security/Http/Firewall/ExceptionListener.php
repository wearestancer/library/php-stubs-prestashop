<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * ExceptionListener catches authentication exception and converts them to
 * Response instances.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3, EventDispatcherInterface type-hints will be updated to the interface from symfony/contracts in 5.0
 */
class ExceptionListener
{
    use \Symfony\Component\Security\Http\Util\TargetPathTrait;
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface $trustResolver, \Symfony\Component\Security\Http\HttpUtils $httpUtils, string $providerKey, \Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface $authenticationEntryPoint = null, string $errorPage = null, \Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface $accessDeniedHandler = null, \Psr\Log\LoggerInterface $logger = null, bool $stateless = false)
    {
    }
    /**
     * Registers a onKernelException listener to take care of security exceptions.
     */
    public function register(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
    /**
     * Unregisters the dispatcher.
     */
    public function unregister(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
    /**
     * Handles security related exceptions.
     */
    public function onKernelException(\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event)
    {
    }
    protected function setTargetPath(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
