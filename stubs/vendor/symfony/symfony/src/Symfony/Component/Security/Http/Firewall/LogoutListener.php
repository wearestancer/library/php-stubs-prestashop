<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * LogoutListener logout users.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class LogoutListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    /**
     * @param array $options An array of options to process a logout attempt
     */
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Http\HttpUtils $httpUtils, \Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface $successHandler, array $options = [], \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager = null)
    {
    }
    public function addHandler(\Symfony\Component\Security\Http\Logout\LogoutHandlerInterface $handler)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * Performs the logout if requested.
     *
     * If a CsrfTokenManagerInterface instance is available, it will be used to
     * validate the request.
     *
     * @throws LogoutException   if the CSRF token is invalid
     * @throws \RuntimeException if the LogoutSuccessHandlerInterface instance does not return a response
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
    /**
     * Whether this request is asking for logout.
     *
     * The default implementation only processed requests to a specific path,
     * but a subclass could change this to logout requests where
     * certain parameters is present.
     *
     * @return bool
     */
    protected function requiresLogout(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
