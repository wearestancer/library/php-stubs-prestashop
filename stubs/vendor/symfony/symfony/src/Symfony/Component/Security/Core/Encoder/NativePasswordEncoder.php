<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * Hashes passwords using password_hash().
 *
 * @author Elnur Abdurrakhimov <elnur@elnur.pro>
 * @author Terje Bråten <terje@braten.be>
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class NativePasswordEncoder implements \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface, \Symfony\Component\Security\Core\Encoder\SelfSaltingEncoderInterface
{
    /**
     * @param string|null $algo An algorithm supported by password_hash() or null to use the stronger available algorithm
     */
    public function __construct(int $opsLimit = null, int $memLimit = null, int $cost = null, string $algo = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function encodePassword($raw, $salt) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isPasswordValid($encoded, $raw, $salt) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function needsRehash(string $encoded) : bool
    {
    }
}
