<?php

namespace Symfony\Component\Security\Core\Encoder;

/**
 * UserPasswordEncoderInterface is the interface for the password encoder service.
 *
 * @author Ariel Ferrandini <arielferrandini@gmail.com>
 *
 * @method bool needsRehash(UserInterface $user)
 */
interface UserPasswordEncoderInterface
{
    /**
     * Encodes the plain password.
     *
     * @param string $plainPassword The password to encode
     *
     * @return string The encoded password
     */
    public function encodePassword(\Symfony\Component\Security\Core\User\UserInterface $user, $plainPassword);
    /**
     * @param string $raw A raw password
     *
     * @return bool true if the password is valid, false otherwise
     */
    public function isPasswordValid(\Symfony\Component\Security\Core\User\UserInterface $user, $raw);
}
