<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * UsernamePasswordFormAuthenticationListener is the default implementation of
 * an authentication via a simple form composed of a username and a password.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UsernamePasswordFormAuthenticationListener extends \Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface $authenticationManager, \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface $sessionStrategy, \Symfony\Component\Security\Http\HttpUtils $httpUtils, string $providerKey, \Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface $successHandler, \Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface $failureHandler, array $options = [], \Psr\Log\LoggerInterface $logger = null, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null, \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function requiresAuthentication(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function attemptAuthentication(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
