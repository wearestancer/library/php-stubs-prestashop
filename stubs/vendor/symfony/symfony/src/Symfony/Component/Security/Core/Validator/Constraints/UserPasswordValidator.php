<?php

namespace Symfony\Component\Security\Core\Validator\Constraints;

class UserPasswordValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage, \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface $encoderFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($password, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
