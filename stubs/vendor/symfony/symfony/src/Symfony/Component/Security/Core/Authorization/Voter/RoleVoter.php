<?php

namespace Symfony\Component\Security\Core\Authorization\Voter;

/**
 * RoleVoter votes if any attribute starts with a given prefix.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RoleVoter implements \Symfony\Component\Security\Core\Authorization\Voter\VoterInterface
{
    public function __construct(string $prefix = 'ROLE_')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function vote(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, $subject, array $attributes)
    {
    }
    protected function extractRoles(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
