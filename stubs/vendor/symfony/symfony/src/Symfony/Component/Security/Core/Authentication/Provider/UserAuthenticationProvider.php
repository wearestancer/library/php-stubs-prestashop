<?php

namespace Symfony\Component\Security\Core\Authentication\Provider;

/**
 * UserProviderInterface retrieves users for UsernamePasswordToken tokens.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class UserAuthenticationProvider implements \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
{
    /**
     * @throws \InvalidArgumentException
     */
    public function __construct(\Symfony\Component\Security\Core\User\UserCheckerInterface $userChecker, string $providerKey, bool $hideUserNotFoundExceptions = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
    /**
     * Retrieves the user from an implementation-specific location.
     *
     * @param string $username The username to retrieve
     *
     * @return UserInterface The user
     *
     * @throws AuthenticationException if the credentials could not be validated
     */
    protected abstract function retrieveUser($username, \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken $token);
    /**
     * Does additional checks on the user and token (like validating the
     * credentials).
     *
     * @throws AuthenticationException if the credentials could not be validated
     */
    protected abstract function checkAuthentication(\Symfony\Component\Security\Core\User\UserInterface $user, \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken $token);
}
