<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * CredentialsExpiredException is thrown when the user account credentials have expired.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class CredentialsExpiredException extends \Symfony\Component\Security\Core\Exception\AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
}
