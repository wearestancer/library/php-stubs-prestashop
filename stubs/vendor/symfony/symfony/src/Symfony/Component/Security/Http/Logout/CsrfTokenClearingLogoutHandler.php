<?php

namespace Symfony\Component\Security\Http\Logout;

/**
 * @author Christian Flothmann <christian.flothmann@sensiolabs.de>
 */
class CsrfTokenClearingLogoutHandler implements \Symfony\Component\Security\Http\Logout\LogoutHandlerInterface
{
    public function __construct(\Symfony\Component\Security\Csrf\TokenStorage\ClearableTokenStorageInterface $csrfTokenStorage)
    {
    }
    public function logout(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
    }
}
