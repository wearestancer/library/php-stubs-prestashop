<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * Extracts Security Errors from Request.
 *
 * @author Boris Vujicic <boris.vujicic@gmail.com>
 */
class AuthenticationUtils
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    /**
     * @param bool $clearSession
     *
     * @return AuthenticationException|null
     */
    public function getLastAuthenticationError($clearSession = true)
    {
    }
    /**
     * @return string
     */
    public function getLastUsername()
    {
    }
}
