<?php

namespace Symfony\Component\Security\Http\Authentication;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class CustomAuthenticationFailureHandler implements \Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface
{
    /**
     * @param array $options Options for processing a successful authentication attempt
     */
    public function __construct(\Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface $handler, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $exception)
    {
    }
}
