<?php

namespace Symfony\Component\Security\Http\Firewall;

/**
 * ChannelListener switches the HTTP protocol based on the access control
 * configuration.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class ChannelListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    public function __construct(\Symfony\Component\Security\Http\AccessMapInterface $map, \Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface $authenticationEntryPoint, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    /**
     * Handles channel management.
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
}
