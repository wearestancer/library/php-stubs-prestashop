<?php

namespace Symfony\Component\Security\Core\User;

/**
 * InMemoryUserProvider is a simple non persistent user provider.
 *
 * Useful for testing, demonstration, prototyping, and for simple needs
 * (a backend with a unique admin for instance)
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class InMemoryUserProvider implements \Symfony\Component\Security\Core\User\UserProviderInterface
{
    /**
     * The user array is a hash where the keys are usernames and the values are
     * an array of attributes: 'password', 'enabled', and 'roles'.
     *
     * @param array $users An array of users
     */
    public function __construct(array $users = [])
    {
    }
    /**
     * Adds a new User to the provider.
     *
     * @throws \LogicException
     */
    public function createUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
    }
}
