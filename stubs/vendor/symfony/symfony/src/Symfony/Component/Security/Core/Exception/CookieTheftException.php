<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * This exception is thrown when the RememberMeServices implementation
 * detects that a presented cookie has already been used by someone else.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
class CookieTheftException extends \Symfony\Component\Security\Core\Exception\AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
    }
}
