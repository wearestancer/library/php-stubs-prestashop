<?php

namespace Symfony\Component\Security\Core\Exception;

/**
 * AccountStatusException is the base class for authentication exceptions
 * caused by the user account status.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Alexander <iam.asm89@gmail.com>
 */
abstract class AccountStatusException extends \Symfony\Component\Security\Core\Exception\AuthenticationException
{
    /**
     * Get the user.
     *
     * @return UserInterface|null
     */
    public function getUser()
    {
    }
    public function setUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __serialize() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __unserialize(array $data) : void
    {
    }
}
