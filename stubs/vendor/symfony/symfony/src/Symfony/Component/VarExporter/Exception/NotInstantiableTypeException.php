<?php

namespace Symfony\Component\VarExporter\Exception;

class NotInstantiableTypeException extends \Exception implements \Symfony\Component\VarExporter\Exception\ExceptionInterface
{
    public function __construct(string $type, \Throwable $previous = null)
    {
    }
}
