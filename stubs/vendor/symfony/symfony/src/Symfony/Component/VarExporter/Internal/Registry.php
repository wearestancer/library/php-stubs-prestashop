<?php

namespace Symfony\Component\VarExporter\Internal;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class Registry
{
    public static $reflectors = [];
    public static $prototypes = [];
    public static $factories = [];
    public static $cloneable = [];
    public static $instantiableWithoutConstructor = [];
    public $classes = [];
    public function __construct(array $classes)
    {
    }
    public static function unserialize($objects, $serializables)
    {
    }
    public static function p($class)
    {
    }
    public static function f($class)
    {
    }
    public static function getClassReflector($class, $instantiableWithoutConstructor = false, $cloneable = null)
    {
    }
}
