<?php

namespace Symfony\Component\VarExporter\Internal;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class Hydrator
{
    public static $hydrators = [];
    public $registry;
    public $values;
    public $properties;
    public $value;
    public $wakeups;
    public function __construct(?\Symfony\Component\VarExporter\Internal\Registry $registry, ?\Symfony\Component\VarExporter\Internal\Values $values, array $properties, $value, array $wakeups)
    {
    }
    public static function hydrate($objects, $values, $properties, $value, $wakeups)
    {
    }
    public static function getHydrator($class)
    {
    }
}
