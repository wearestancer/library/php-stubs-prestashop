<?php

namespace Symfony\Component\VarExporter\Exception;

class ClassNotFoundException extends \Exception implements \Symfony\Component\VarExporter\Exception\ExceptionInterface
{
    public function __construct(string $class, \Throwable $previous = null)
    {
    }
}
