<?php

namespace Symfony\Component\ExpressionLanguage;

/**
 * Represents an already parsed expression.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ParsedExpression extends \Symfony\Component\ExpressionLanguage\Expression
{
    public function __construct(string $expression, \Symfony\Component\ExpressionLanguage\Node\Node $nodes)
    {
    }
    public function getNodes()
    {
    }
}
