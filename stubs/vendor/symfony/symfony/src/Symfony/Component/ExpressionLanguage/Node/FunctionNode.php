<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class FunctionNode extends \Symfony\Component\ExpressionLanguage\Node\Node
{
    public function __construct(string $name, \Symfony\Component\ExpressionLanguage\Node\Node $arguments)
    {
    }
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray()
    {
    }
}
