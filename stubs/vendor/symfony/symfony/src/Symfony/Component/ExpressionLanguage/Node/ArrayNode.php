<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class ArrayNode extends \Symfony\Component\ExpressionLanguage\Node\Node
{
    protected $index;
    public function __construct()
    {
    }
    public function addElement(\Symfony\Component\ExpressionLanguage\Node\Node $value, \Symfony\Component\ExpressionLanguage\Node\Node $key = null)
    {
    }
    /**
     * Compiles the node to PHP.
     */
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray()
    {
    }
    protected function getKeyValuePairs()
    {
    }
    protected function compileArguments(\Symfony\Component\ExpressionLanguage\Compiler $compiler, $withKeys = true)
    {
    }
}
