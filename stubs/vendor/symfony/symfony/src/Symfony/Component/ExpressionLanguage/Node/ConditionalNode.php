<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class ConditionalNode extends \Symfony\Component\ExpressionLanguage\Node\Node
{
    public function __construct(\Symfony\Component\ExpressionLanguage\Node\Node $expr1, \Symfony\Component\ExpressionLanguage\Node\Node $expr2, \Symfony\Component\ExpressionLanguage\Node\Node $expr3)
    {
    }
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray()
    {
    }
}
