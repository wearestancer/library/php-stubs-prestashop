<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class ArgumentsNode extends \Symfony\Component\ExpressionLanguage\Node\ArrayNode
{
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function toArray()
    {
    }
}
