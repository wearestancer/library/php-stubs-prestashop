<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * Represents a node in the AST.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Node
{
    public $nodes = [];
    public $attributes = [];
    /**
     * @param array $nodes      An array of nodes
     * @param array $attributes An array of attributes
     */
    public function __construct(array $nodes = [], array $attributes = [])
    {
    }
    /**
     * @return string
     */
    public function __toString()
    {
    }
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray()
    {
    }
    public function dump()
    {
    }
    protected function dumpString($value)
    {
    }
    protected function isHash(array $value)
    {
    }
}
