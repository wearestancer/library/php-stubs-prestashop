<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class GetAttrNode extends \Symfony\Component\ExpressionLanguage\Node\Node
{
    public const PROPERTY_CALL = 1;
    public const METHOD_CALL = 2;
    public const ARRAY_CALL = 3;
    public function __construct(\Symfony\Component\ExpressionLanguage\Node\Node $node, \Symfony\Component\ExpressionLanguage\Node\Node $attribute, \Symfony\Component\ExpressionLanguage\Node\ArrayNode $arguments, int $type)
    {
    }
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray()
    {
    }
}
