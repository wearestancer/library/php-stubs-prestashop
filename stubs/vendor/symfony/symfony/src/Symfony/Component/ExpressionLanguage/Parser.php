<?php

namespace Symfony\Component\ExpressionLanguage;

/**
 * Parsers a token stream.
 *
 * This parser implements a "Precedence climbing" algorithm.
 *
 * @see http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm
 * @see http://en.wikipedia.org/wiki/Operator-precedence_parser
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Parser
{
    public const OPERATOR_LEFT = 1;
    public const OPERATOR_RIGHT = 2;
    public function __construct(array $functions)
    {
    }
    /**
     * Converts a token stream to a node tree.
     *
     * The valid names is an array where the values
     * are the names that the user can use in an expression.
     *
     * If the variable name in the compiled PHP code must be
     * different, define it as the key.
     *
     * For instance, ['this' => 'container'] means that the
     * variable 'container' can be used in the expression
     * but the compiled code will use 'this'.
     *
     * @param array $names An array of valid names
     *
     * @return Node\Node A node tree
     *
     * @throws SyntaxError
     */
    public function parse(\Symfony\Component\ExpressionLanguage\TokenStream $stream, $names = [])
    {
    }
    public function parseExpression($precedence = 0)
    {
    }
    protected function getPrimary()
    {
    }
    protected function parseConditionalExpression($expr)
    {
    }
    public function parsePrimaryExpression()
    {
    }
    public function parseArrayExpression()
    {
    }
    public function parseHashExpression()
    {
    }
    public function parsePostfixExpression($node)
    {
    }
    /**
     * Parses arguments.
     */
    public function parseArguments()
    {
    }
}
