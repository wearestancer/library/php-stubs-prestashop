<?php

namespace Symfony\Component\ExpressionLanguage;

/**
 * Compiles a node to PHP code.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Compiler implements \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(array $functions)
    {
    }
    public function getFunction($name)
    {
    }
    /**
     * Gets the current PHP code after compilation.
     *
     * @return string The PHP code
     */
    public function getSource()
    {
    }
    public function reset()
    {
    }
    /**
     * Compiles a node.
     *
     * @return $this
     */
    public function compile(\Symfony\Component\ExpressionLanguage\Node\Node $node)
    {
    }
    public function subcompile(\Symfony\Component\ExpressionLanguage\Node\Node $node)
    {
    }
    /**
     * Adds a raw string to the compiled code.
     *
     * @param string $string The string
     *
     * @return $this
     */
    public function raw($string)
    {
    }
    /**
     * Adds a quoted string to the compiled code.
     *
     * @param string $value The string
     *
     * @return $this
     */
    public function string($value)
    {
    }
    /**
     * Returns a PHP representation of a given value.
     *
     * @param mixed $value The value to convert
     *
     * @return $this
     */
    public function repr($value)
    {
    }
}
