<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class UnaryNode extends \Symfony\Component\ExpressionLanguage\Node\Node
{
    public function __construct(string $operator, \Symfony\Component\ExpressionLanguage\Node\Node $node)
    {
    }
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray() : array
    {
    }
}
