<?php

namespace Symfony\Component\ExpressionLanguage\Node;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class BinaryNode extends \Symfony\Component\ExpressionLanguage\Node\Node
{
    public function __construct(string $operator, \Symfony\Component\ExpressionLanguage\Node\Node $left, \Symfony\Component\ExpressionLanguage\Node\Node $right)
    {
    }
    public function compile(\Symfony\Component\ExpressionLanguage\Compiler $compiler)
    {
    }
    public function evaluate($functions, $values)
    {
    }
    public function toArray()
    {
    }
}
