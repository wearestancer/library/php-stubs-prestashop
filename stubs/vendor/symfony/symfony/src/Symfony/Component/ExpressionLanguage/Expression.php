<?php

namespace Symfony\Component\ExpressionLanguage;

/**
 * Represents an expression.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Expression
{
    protected $expression;
    public function __construct(string $expression)
    {
    }
    /**
     * Gets the expression.
     *
     * @return string The expression
     */
    public function __toString()
    {
    }
}
