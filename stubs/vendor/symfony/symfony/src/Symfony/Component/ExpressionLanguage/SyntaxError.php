<?php

namespace Symfony\Component\ExpressionLanguage;

class SyntaxError extends \LogicException
{
    public function __construct(string $message, int $cursor = 0, string $expression = '', string $subject = null, array $proposals = null)
    {
    }
}
