<?php

namespace Symfony\Component\ExpressionLanguage;

/**
 * Represents a token stream.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TokenStream
{
    public $current;
    public function __construct(array $tokens, string $expression = '')
    {
    }
    /**
     * Returns a string representation of the token stream.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Sets the pointer to the next token and returns the old one.
     */
    public function next()
    {
    }
    /**
     * Tests a token.
     *
     * @param array|int   $type    The type to test
     * @param string|null $value   The token value
     * @param string|null $message The syntax error message
     */
    public function expect($type, $value = null, $message = null)
    {
    }
    /**
     * Checks if end of stream was reached.
     *
     * @return bool
     */
    public function isEOF()
    {
    }
    /**
     * @internal
     */
    public function getExpression() : string
    {
    }
}
