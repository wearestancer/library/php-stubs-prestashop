<?php

namespace Symfony\Component\ExpressionLanguage;

/**
 * Represents a Token.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Token
{
    public $value;
    public $type;
    public $cursor;
    public const EOF_TYPE = 'end of expression';
    public const NAME_TYPE = 'name';
    public const NUMBER_TYPE = 'number';
    public const STRING_TYPE = 'string';
    public const OPERATOR_TYPE = 'operator';
    public const PUNCTUATION_TYPE = 'punctuation';
    /**
     * @param string                $type   The type of the token (self::*_TYPE)
     * @param string|int|float|null $value  The token value
     * @param int                   $cursor The cursor position in the source
     */
    public function __construct(string $type, $value, ?int $cursor)
    {
    }
    /**
     * Returns a string representation of the token.
     *
     * @return string A string representation of the token
     */
    public function __toString()
    {
    }
    /**
     * Tests the current token for a type and/or a value.
     *
     * @param string      $type  The type to test
     * @param string|null $value The token value
     *
     * @return bool
     */
    public function test($type, $value = null)
    {
    }
}
