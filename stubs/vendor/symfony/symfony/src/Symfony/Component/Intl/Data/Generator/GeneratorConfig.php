<?php

namespace Symfony\Component\Intl\Data\Generator;

/**
 * Stores contextual information for resource bundle generation.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class GeneratorConfig
{
    public function __construct(string $sourceDir, string $icuVersion)
    {
    }
    /**
     * Adds a writer to be used during the data conversion.
     */
    public function addBundleWriter(string $targetDir, \Symfony\Component\Intl\Data\Bundle\Writer\BundleWriterInterface $writer)
    {
    }
    /**
     * Returns the writers indexed by their output directories.
     *
     * @return BundleWriterInterface[]
     */
    public function getBundleWriters() : array
    {
    }
    /**
     * Returns the directory where the source versions of the resource bundles
     * are stored.
     *
     * @return string An absolute path to a directory
     */
    public function getSourceDir() : string
    {
    }
    /**
     * Returns the ICU version of the bundles being converted.
     *
     * @return string The ICU version string
     */
    public function getIcuVersion() : string
    {
    }
}
