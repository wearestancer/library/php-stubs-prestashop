<?php

namespace Symfony\Component\Intl\ResourceBundle;

/**
 * Default implementation of {@link LanguageBundleInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class LanguageBundle extends \Symfony\Component\Intl\Data\Provider\LanguageDataProvider implements \Symfony\Component\Intl\ResourceBundle\LanguageBundleInterface
{
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, \Symfony\Component\Intl\Data\Provider\LocaleDataProvider $localeProvider, \Symfony\Component\Intl\Data\Provider\ScriptDataProvider $scriptProvider)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLanguageName($language, $region = null, $displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLanguageNames($displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getScriptName($script, $language = null, $displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getScriptNames($displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocales()
    {
    }
}
