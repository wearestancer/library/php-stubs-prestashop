<?php

namespace Symfony\Component\Intl\Util;

/**
 * @internal
 */
final class GitRepository
{
    public function __construct(string $path)
    {
    }
    public static function download(string $remote, string $targetDir) : self
    {
    }
    public function getPath() : string
    {
    }
    public function getUrl() : string
    {
    }
    public function getLastCommitHash() : string
    {
    }
    public function getLastAuthor() : string
    {
    }
    public function getLastAuthoredDate() : \DateTime
    {
    }
    public function getLastTag(callable $filter = null) : string
    {
    }
    public function checkout(string $branch)
    {
    }
}
