<?php

/**
 * Stub implementation for the IntlDateFormatter class of the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see BaseIntlDateFormatter
 */
class IntlDateFormatter extends \Symfony\Component\Intl\DateFormatter\IntlDateFormatter
{
}
