<?php

namespace Symfony\Component\Intl\Data\Bundle\Reader;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class BufferedBundleReader implements \Symfony\Component\Intl\Data\Bundle\Reader\BundleReaderInterface
{
    /**
     * Buffers a given reader.
     *
     * @param int $bufferSize The number of entries to store in the buffer
     */
    public function __construct(\Symfony\Component\Intl\Data\Bundle\Reader\BundleReaderInterface $reader, int $bufferSize)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function read($path, $locale)
    {
    }
}
