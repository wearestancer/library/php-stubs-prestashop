<?php

namespace Symfony\Component\Intl;

/**
 * Provides access to locale-related data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
final class Locale extends \Locale
{
    /**
     * Sets the default fallback locale.
     *
     * The default fallback locale is used as fallback for locales that have no
     * fallback otherwise.
     *
     * @param string|null $locale The default fallback locale
     *
     * @see getFallback()
     */
    public static function setDefaultFallback(?string $locale)
    {
    }
    /**
     * Returns the default fallback locale.
     *
     * @return string|null The default fallback locale
     *
     * @see setDefaultFallback()
     * @see getFallback()
     */
    public static function getDefaultFallback() : ?string
    {
    }
    /**
     * Returns the fallback locale for a given locale.
     *
     * For example, the fallback of "fr_FR" is "fr". The fallback of "fr" is
     * the default fallback locale configured with {@link setDefaultFallback()}.
     * The default fallback locale has no fallback.
     *
     * @param string $locale The ICU locale code to find the fallback for
     *
     * @return string|null The ICU locale code of the fallback locale, or null
     *                     if no fallback exists
     */
    public static function getFallback(string $locale) : ?string
    {
    }
}
