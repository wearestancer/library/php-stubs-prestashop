<?php

namespace Symfony\Component\Intl\Data\Bundle\Compiler;

/**
 * Compiles .txt resource bundles to binary .res files.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class GenrbCompiler implements \Symfony\Component\Intl\Data\Bundle\Compiler\BundleCompilerInterface
{
    /**
     * Creates a new compiler based on the "genrb" executable.
     *
     * @param string $genrb   Optional. The path to the "genrb" executable
     * @param string $envVars Optional. Environment variables to be loaded when running "genrb".
     *
     * @throws RuntimeException if the "genrb" cannot be found
     */
    public function __construct(string $genrb = 'genrb', string $envVars = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function compile($sourcePath, $targetDir)
    {
    }
}
