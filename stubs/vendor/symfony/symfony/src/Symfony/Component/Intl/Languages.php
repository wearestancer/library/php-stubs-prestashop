<?php

namespace Symfony\Component\Intl;

/**
 * Gives access to language-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
final class Languages extends \Symfony\Component\Intl\ResourceBundle
{
    /**
     * Returns all available languages as two-letter codes.
     *
     * Languages are returned as lowercase ISO 639-1 two-letter language codes.
     * For languages that don't have a two-letter code, the ISO 639-2
     * three-letter code is used instead.
     *
     * A full table of ISO 639 language codes can be found here:
     * http://www-01.sil.org/iso639-3/codes.asp
     *
     * @return string[] an array of canonical ISO 639-1 language codes
     */
    public static function getLanguageCodes() : array
    {
    }
    public static function exists(string $language) : bool
    {
    }
    /**
     * Gets the language name from its alpha2 code.
     *
     * A full locale may be passed to obtain a more localized language name, e.g. "American English" for "en_US".
     *
     * @throws MissingResourceException if the language code does not exist
     */
    public static function getName(string $language, string $displayLocale = null) : string
    {
    }
    /**
     * Gets the list of language names indexed with alpha2 codes as keys.
     *
     * @return string[]
     */
    public static function getNames(string $displayLocale = null) : array
    {
    }
    /**
     * Returns the ISO 639-2 three-letter code of a language, given a two-letter code.
     *
     * @throws MissingResourceException if the language has no corresponding three-letter code
     */
    public static function getAlpha3Code(string $language) : string
    {
    }
    /**
     * Returns the ISO 639-1 two-letter code of a language, given a three letter code.
     *
     * @throws MissingResourceException if the language has no corresponding three-letter code
     */
    public static function getAlpha2Code(string $language) : string
    {
    }
    /**
     * Returns all available languages as three-letter codes.
     *
     * Languages are returned as lowercase ISO 639-2 three-letter language codes.
     *
     * @return string[] an array of canonical ISO 639-2 language codes
     */
    public static function getAlpha3Codes() : array
    {
    }
    /**
     * @param string $language ISO 639-2 three-letter language code
     */
    public static function alpha3CodeExists(string $language) : bool
    {
    }
    /**
     * Gets the language name from its ISO 639-2 three-letter code.
     *
     * @throws MissingResourceException if the country code does not exists
     */
    public static function getAlpha3Name(string $language, string $displayLocale = null) : string
    {
    }
    /**
     * Gets the list of language names indexed with ISO 639-2 three-letter codes as keys.
     *
     * Same as method getNames, but with ISO 639-2 three-letter codes instead of ISO 639-1 codes as keys.
     *
     * @return string[]
     */
    public static function getAlpha3Names($displayLocale = null) : array
    {
    }
}
