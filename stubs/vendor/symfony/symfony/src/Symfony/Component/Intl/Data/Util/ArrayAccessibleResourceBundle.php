<?php

namespace Symfony\Component\Intl\Data\Util;

/**
 * Work-around for a bug in PHP's \ResourceBundle implementation.
 *
 * More information can be found on https://bugs.php.net/64356.
 * This class can be removed once that bug is fixed.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class ArrayAccessibleResourceBundle implements \ArrayAccess, \IteratorAggregate, \Countable
{
    public function __construct(\ResourceBundle $bundleImpl)
    {
    }
    public function get($offset)
    {
    }
    public function offsetExists($offset) : bool
    {
    }
    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
    }
    public function offsetSet($offset, $value) : void
    {
    }
    public function offsetUnset($offset) : void
    {
    }
    public function getIterator() : \Traversable
    {
    }
    public function count() : int
    {
    }
    public function getErrorCode()
    {
    }
    public function getErrorMessage()
    {
    }
}
