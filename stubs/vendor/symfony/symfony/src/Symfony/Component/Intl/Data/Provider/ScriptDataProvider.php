<?php

namespace Symfony\Component\Intl\Data\Provider;

/**
 * Data provider for script-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class ScriptDataProvider
{
    /**
     * Creates a data provider that reads locale-related data from .res files.
     *
     * @param string $path The path to the directory containing the .res files
     */
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader)
    {
    }
    public function getScripts()
    {
    }
    public function getName($script, $displayLocale = null)
    {
    }
    public function getNames($displayLocale = null)
    {
    }
}
