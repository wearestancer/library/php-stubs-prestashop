<?php

namespace Symfony\Component\Intl\Util;

/**
 * Facilitates the comparison of version strings.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class Version
{
    /**
     * Compares two versions with an operator.
     *
     * This method is identical to {@link version_compare()}, except that you
     * can pass the number of regarded version components in the last argument
     * $precision.
     *
     * Examples:
     *
     *     Version::compare('1.2.3', '1.2.4', '==')
     *     // => false
     *
     *     Version::compare('1.2.3', '1.2.4', '==', 2)
     *     // => true
     *
     * @param string   $version1  A version string
     * @param string   $version2  A version string to compare
     * @param string   $operator  The comparison operator
     * @param int|null $precision The number of components to compare. Pass
     *                            NULL to compare the versions unchanged.
     *
     * @return bool Whether the comparison succeeded
     *
     * @see normalize()
     */
    public static function compare($version1, $version2, $operator, $precision = null)
    {
    }
    /**
     * Normalizes a version string to the number of components given in the
     * parameter $precision.
     *
     * Examples:
     *
     *     Version::normalize('1.2.3', 1);
     *     // => '1'
     *
     *     Version::normalize('1.2.3', 2);
     *     // => '1.2'
     *
     * @param string   $version   A version string
     * @param int|null $precision The number of components to include. Pass
     *                            NULL to return the version unchanged.
     *
     * @return string|null the normalized version or NULL if it couldn't be
     *                     normalized
     */
    public static function normalize($version, $precision)
    {
    }
}
