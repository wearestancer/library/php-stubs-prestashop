<?php

namespace Symfony\Component\Intl\ResourceBundle;

/**
 * Gives access to ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @deprecated since Symfony 4.3, to be removed in 5.0.
 */
interface ResourceBundleInterface
{
    /**
     * Returns the list of locales that this bundle supports.
     *
     * @return string[] A list of locale codes
     */
    public function getLocales();
}
