<?php

namespace Symfony\Component\Intl\DateFormatter\DateFormat;

/**
 * Parser and formatter for 12 hour format (0-11).
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class Hour1200Transformer extends \Symfony\Component\Intl\DateFormatter\DateFormat\HourTransformer
{
    /**
     * {@inheritdoc}
     */
    public function format(\DateTime $dateTime, int $length) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function normalizeHour(int $hour, string $marker = null) : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getReverseMatchingRegExp(int $length) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extractDateOptions(string $matched, int $length) : array
    {
    }
}
