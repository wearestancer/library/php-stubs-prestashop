<?php

namespace Symfony\Component\Intl\Data\Bundle\Reader;

/**
 * Reads resource bundle files.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
interface BundleReaderInterface
{
    /**
     * Reads a resource bundle.
     *
     * @param string $path   The path to the resource bundle
     * @param string $locale The locale to read
     *
     * @return mixed returns an array or {@link \ArrayAccess} instance for
     *               complex data, a scalar value otherwise
     */
    public function read($path, $locale);
}
