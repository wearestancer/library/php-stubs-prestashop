<?php

namespace Symfony\Component\Intl\Data\Bundle\Compiler;

/**
 * Compiles a resource bundle.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
interface BundleCompilerInterface
{
    /**
     * Compiles a resource bundle at the given source to the given target
     * directory.
     *
     * @param string $sourcePath
     * @param string $targetDir
     */
    public function compile($sourcePath, $targetDir);
}
