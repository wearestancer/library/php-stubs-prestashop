<?php

namespace Symfony\Component\Intl\Data\Bundle\Writer;

/**
 * Writes .txt resource bundles.
 *
 * The resulting files can be converted to binary .res files using a
 * {@link \Symfony\Component\Intl\ResourceBundle\Compiler\BundleCompilerInterface}
 * implementation.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see http://source.icu-project.org/repos/icu/icuhtml/trunk/design/bnf_rb.txt
 *
 * @internal
 */
class TextBundleWriter implements \Symfony\Component\Intl\Data\Bundle\Writer\BundleWriterInterface
{
    /**
     * {@inheritdoc}
     */
    public function write($path, $locale, $data, $fallback = true)
    {
    }
}
