<?php

namespace Symfony\Component\Intl;

/**
 * @author Roland Franssen <franssen.roland@gmail.com>
 *
 * @internal
 */
abstract class ResourceBundle
{
    protected static abstract function getPath() : string;
    /**
     * Reads an entry from a resource bundle.
     *
     * @see BundleEntryReaderInterface::readEntry()
     *
     * @param string[] $indices  The indices to read from the bundle
     * @param string   $locale   The locale to read
     * @param bool     $fallback Whether to merge the value with the value from
     *                           the fallback locale (e.g. "en" for "en_GB").
     *                           Only applicable if the result is multivalued
     *                           (i.e. array or \ArrayAccess) or cannot be found
     *                           in the requested locale.
     *
     * @return mixed returns an array or {@link \ArrayAccess} instance for
     *               complex data and a scalar value for simple data
     */
    protected static final function readEntry(array $indices, string $locale = null, bool $fallback = true)
    {
    }
    protected static final function asort(iterable $list, string $locale = null) : array
    {
    }
}
