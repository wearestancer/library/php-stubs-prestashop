<?php

namespace Symfony\Component\Intl\Data\Bundle\Reader;

/**
 * Default implementation of {@link BundleEntryReaderInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see BundleEntryReaderInterface
 *
 * @internal
 */
class BundleEntryReader implements \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface
{
    /**
     * Creates an entry reader based on the given resource bundle reader.
     */
    public function __construct(\Symfony\Component\Intl\Data\Bundle\Reader\BundleReaderInterface $reader)
    {
    }
    /**
     * Stores a mapping of locale aliases to locales.
     *
     * This mapping is used when reading entries and merging them with their
     * fallback locales. If an entry is read for a locale alias (e.g. "mo")
     * that points to a locale with a fallback locale ("ro_MD"), the reader
     * can continue at the correct fallback locale ("ro").
     *
     * @param array $localeAliases A mapping of locale aliases to locales
     */
    public function setLocaleAliases($localeAliases)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function read($path, $locale)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function readEntry($path, $locale, array $indices, $fallback = true)
    {
    }
}
