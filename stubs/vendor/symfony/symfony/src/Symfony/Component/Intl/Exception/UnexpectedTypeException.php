<?php

namespace Symfony\Component\Intl\Exception;

/**
 * Thrown when a method argument had an unexpected type.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class UnexpectedTypeException extends \Symfony\Component\Intl\Exception\InvalidArgumentException
{
    public function __construct($value, string $expectedType)
    {
    }
}
