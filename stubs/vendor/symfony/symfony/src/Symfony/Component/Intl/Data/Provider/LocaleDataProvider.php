<?php

namespace Symfony\Component\Intl\Data\Provider;

/**
 * Data provider for locale-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class LocaleDataProvider
{
    /**
     * Creates a data provider that reads locale-related data from .res files.
     *
     * @param string $path The path to the directory containing the .res files
     */
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader)
    {
    }
    public function getLocales()
    {
    }
    public function getAliases()
    {
    }
    public function getName($locale, $displayLocale = null)
    {
    }
    public function getNames($displayLocale = null)
    {
    }
}
