<?php

namespace Symfony\Component\Intl\ResourceBundle;

/**
 * Default implementation of {@link LocaleBundleInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class LocaleBundle extends \Symfony\Component\Intl\Data\Provider\LocaleDataProvider implements \Symfony\Component\Intl\ResourceBundle\LocaleBundleInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLocales()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocaleName($locale, $displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocaleNames($displayLocale = null)
    {
    }
}
