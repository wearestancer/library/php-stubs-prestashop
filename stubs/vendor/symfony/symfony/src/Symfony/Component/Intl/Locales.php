<?php

namespace Symfony\Component\Intl;

/**
 * Gives access to locale-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
final class Locales extends \Symfony\Component\Intl\ResourceBundle
{
    /**
     * @return string[]
     */
    public static function getLocales() : array
    {
    }
    /**
     * @return string[]
     */
    public static function getAliases() : array
    {
    }
    public static function exists(string $locale) : bool
    {
    }
    /**
     * @throws MissingResourceException if the locale does not exist
     */
    public static function getName(string $locale, string $displayLocale = null) : string
    {
    }
    /**
     * @return string[]
     */
    public static function getNames($displayLocale = null) : array
    {
    }
}
