<?php

namespace Symfony\Component\Intl\Data\Util;

/**
 * Scans a directory with data files for locales.
 *
 * The name of each file with the extension ".txt" is considered, if it "looks"
 * like a locale:
 *
 *  - the name must start with two letters;
 *  - the two letters may optionally be followed by an underscore and any
 *    sequence of other symbols.
 *
 * For example, "de" and "de_DE" are considered to be locales. "root" and "meta"
 * are not.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class LocaleScanner
{
    /**
     * Returns all locales found in the given directory.
     *
     * @return array An array of locales. The result also contains locales that
     *               are in fact just aliases for other locales. Use
     *               {@link scanAliases()} to determine which of the locales
     *               are aliases
     */
    public function scanLocales(string $sourceDir) : array
    {
    }
    /**
     * Returns all locale aliases found in the given directory.
     *
     * @return array An array with the locale aliases as keys and the aliased
     *               locales as values
     */
    public function scanAliases(string $sourceDir) : array
    {
    }
    /**
     * Returns all locale parents found in the given directory.
     */
    public function scanParents(string $sourceDir) : array
    {
    }
}
