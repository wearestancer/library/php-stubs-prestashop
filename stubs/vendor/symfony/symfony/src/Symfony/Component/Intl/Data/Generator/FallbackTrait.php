<?php

namespace Symfony\Component\Intl\Data\Generator;

/**
 * @author Roland Franssen <franssen.roland@gmail.com>
 *
 * @internal
 */
trait FallbackTrait
{
    private $fallbackCache = [];
    private $generatingFallback = false;
    /**
     * @see AbstractDataGenerator::generateDataForLocale()
     */
    protected abstract function generateDataForLocale(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir, string $displayLocale) : ?array;
    /**
     * @see AbstractDataGenerator::generateDataForRoot()
     */
    protected abstract function generateDataForRoot(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array;
    private function generateFallbackData(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir, string $displayLocale) : array
    {
    }
}
