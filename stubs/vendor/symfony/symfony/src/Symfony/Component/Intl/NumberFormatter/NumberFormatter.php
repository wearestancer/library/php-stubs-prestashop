<?php

namespace Symfony\Component\Intl\NumberFormatter;

/**
 * Replacement for PHP's native {@link \NumberFormatter} class.
 *
 * The only methods currently supported in this class are:
 *
 *  - {@link __construct}
 *  - {@link create}
 *  - {@link formatCurrency}
 *  - {@link format}
 *  - {@link getAttribute}
 *  - {@link getErrorCode}
 *  - {@link getErrorMessage}
 *  - {@link getLocale}
 *  - {@link parse}
 *  - {@link setAttribute}
 *
 * @author Eriksen Costa <eriksen.costa@infranology.com.br>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
abstract class NumberFormatter
{
    /* Format style constants */
    public const PATTERN_DECIMAL = 0;
    public const DECIMAL = 1;
    public const CURRENCY = 2;
    public const PERCENT = 3;
    public const SCIENTIFIC = 4;
    public const SPELLOUT = 5;
    public const ORDINAL = 6;
    public const DURATION = 7;
    public const PATTERN_RULEBASED = 9;
    public const IGNORE = 0;
    public const DEFAULT_STYLE = 1;
    /* Format type constants */
    public const TYPE_DEFAULT = 0;
    public const TYPE_INT32 = 1;
    public const TYPE_INT64 = 2;
    public const TYPE_DOUBLE = 3;
    public const TYPE_CURRENCY = 4;
    /* Numeric attribute constants */
    public const PARSE_INT_ONLY = 0;
    public const GROUPING_USED = 1;
    public const DECIMAL_ALWAYS_SHOWN = 2;
    public const MAX_INTEGER_DIGITS = 3;
    public const MIN_INTEGER_DIGITS = 4;
    public const INTEGER_DIGITS = 5;
    public const MAX_FRACTION_DIGITS = 6;
    public const MIN_FRACTION_DIGITS = 7;
    public const FRACTION_DIGITS = 8;
    public const MULTIPLIER = 9;
    public const GROUPING_SIZE = 10;
    public const ROUNDING_MODE = 11;
    public const ROUNDING_INCREMENT = 12;
    public const FORMAT_WIDTH = 13;
    public const PADDING_POSITION = 14;
    public const SECONDARY_GROUPING_SIZE = 15;
    public const SIGNIFICANT_DIGITS_USED = 16;
    public const MIN_SIGNIFICANT_DIGITS = 17;
    public const MAX_SIGNIFICANT_DIGITS = 18;
    public const LENIENT_PARSE = 19;
    /* Text attribute constants */
    public const POSITIVE_PREFIX = 0;
    public const POSITIVE_SUFFIX = 1;
    public const NEGATIVE_PREFIX = 2;
    public const NEGATIVE_SUFFIX = 3;
    public const PADDING_CHARACTER = 4;
    public const CURRENCY_CODE = 5;
    public const DEFAULT_RULESET = 6;
    public const PUBLIC_RULESETS = 7;
    /* Format symbol constants */
    public const DECIMAL_SEPARATOR_SYMBOL = 0;
    public const GROUPING_SEPARATOR_SYMBOL = 1;
    public const PATTERN_SEPARATOR_SYMBOL = 2;
    public const PERCENT_SYMBOL = 3;
    public const ZERO_DIGIT_SYMBOL = 4;
    public const DIGIT_SYMBOL = 5;
    public const MINUS_SIGN_SYMBOL = 6;
    public const PLUS_SIGN_SYMBOL = 7;
    public const CURRENCY_SYMBOL = 8;
    public const INTL_CURRENCY_SYMBOL = 9;
    public const MONETARY_SEPARATOR_SYMBOL = 10;
    public const EXPONENTIAL_SYMBOL = 11;
    public const PERMILL_SYMBOL = 12;
    public const PAD_ESCAPE_SYMBOL = 13;
    public const INFINITY_SYMBOL = 14;
    public const NAN_SYMBOL = 15;
    public const SIGNIFICANT_DIGIT_SYMBOL = 16;
    public const MONETARY_GROUPING_SEPARATOR_SYMBOL = 17;
    /* Rounding mode values used by NumberFormatter::setAttribute() with NumberFormatter::ROUNDING_MODE attribute */
    public const ROUND_CEILING = 0;
    public const ROUND_FLOOR = 1;
    public const ROUND_DOWN = 2;
    public const ROUND_UP = 3;
    public const ROUND_HALFEVEN = 4;
    public const ROUND_HALFDOWN = 5;
    public const ROUND_HALFUP = 6;
    /* Pad position values used by NumberFormatter::setAttribute() with NumberFormatter::PADDING_POSITION attribute */
    public const PAD_BEFORE_PREFIX = 0;
    public const PAD_AFTER_PREFIX = 1;
    public const PAD_BEFORE_SUFFIX = 2;
    public const PAD_AFTER_SUFFIX = 3;
    /**
     * The error code from the last operation.
     *
     * @var int
     */
    protected $errorCode = \Symfony\Component\Intl\Globals\IntlGlobals::U_ZERO_ERROR;
    /**
     * The error message from the last operation.
     *
     * @var string
     */
    protected $errorMessage = 'U_ZERO_ERROR';
    /**
     * @param string|null $locale  The locale code. The only currently supported locale is "en" (or null using the default locale, i.e. "en")
     * @param int         $style   Style of the formatting, one of the format style constants.
     *                             The only supported styles are NumberFormatter::DECIMAL
     *                             and NumberFormatter::CURRENCY.
     * @param string      $pattern Not supported. A pattern string in case $style is NumberFormat::PATTERN_DECIMAL or
     *                             NumberFormat::PATTERN_RULEBASED. It must conform to  the syntax
     *                             described in the ICU DecimalFormat or ICU RuleBasedNumberFormat documentation
     *
     * @see https://php.net/numberformatter.create
     * @see http://www.icu-project.org/apiref/icu4c/classDecimalFormat.html#_details
     * @see http://www.icu-project.org/apiref/icu4c/classRuleBasedNumberFormat.html#_details
     *
     * @throws MethodArgumentValueNotImplementedException When $locale different than "en" or null is passed
     * @throws MethodArgumentValueNotImplementedException When the $style is not supported
     * @throws MethodArgumentNotImplementedException      When the pattern value is different than null
     */
    public function __construct(?string $locale = 'en', int $style = null, $pattern = null)
    {
    }
    /**
     * Static constructor.
     *
     * @param string|null $locale  The locale code. The only supported locale is "en" (or null using the default locale, i.e. "en")
     * @param int         $style   Style of the formatting, one of the format style constants.
     *                             The only currently supported styles are NumberFormatter::DECIMAL
     *                             and NumberFormatter::CURRENCY.
     * @param string      $pattern Not supported. A pattern string in case $style is NumberFormat::PATTERN_DECIMAL or
     *                             NumberFormat::PATTERN_RULEBASED. It must conform to  the syntax
     *                             described in the ICU DecimalFormat or ICU RuleBasedNumberFormat documentation
     *
     * @return static
     *
     * @see https://php.net/numberformatter.create
     * @see http://www.icu-project.org/apiref/icu4c/classDecimalFormat.html#_details
     * @see http://www.icu-project.org/apiref/icu4c/classRuleBasedNumberFormat.html#_details
     *
     * @throws MethodArgumentValueNotImplementedException When $locale different than "en" or null is passed
     * @throws MethodArgumentValueNotImplementedException When the $style is not supported
     * @throws MethodArgumentNotImplementedException      When the pattern value is different than null
     */
    public static function create($locale = 'en', $style = null, $pattern = null)
    {
    }
    /**
     * Format a currency value.
     *
     * @param float  $value    The numeric currency value
     * @param string $currency The 3-letter ISO 4217 currency code indicating the currency to use
     *
     * @return string The formatted currency value
     *
     * @see https://php.net/numberformatter.formatcurrency
     * @see https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     */
    public function formatCurrency($value, $currency)
    {
    }
    /**
     * Format a number.
     *
     * @param int|float $value The value to format
     * @param int       $type  Type of the formatting, one of the format type constants.
     *                         Only type NumberFormatter::TYPE_DEFAULT is currently supported.
     *
     * @return bool|string The formatted value or false on error
     *
     * @see https://php.net/numberformatter.format
     *
     * @throws NotImplementedException                    If the method is called with the class $style 'CURRENCY'
     * @throws MethodArgumentValueNotImplementedException If the $type is different than TYPE_DEFAULT
     */
    public function format($value, $type = self::TYPE_DEFAULT)
    {
    }
    /**
     * Returns an attribute value.
     *
     * @param int $attr An attribute specifier, one of the numeric attribute constants
     *
     * @return int|false The attribute value on success or false on error
     *
     * @see https://php.net/numberformatter.getattribute
     */
    public function getAttribute($attr)
    {
    }
    /**
     * Returns formatter's last error code. Always returns the U_ZERO_ERROR class constant value.
     *
     * @return int The error code from last formatter call
     *
     * @see https://php.net/numberformatter.geterrorcode
     */
    public function getErrorCode()
    {
    }
    /**
     * Returns formatter's last error message. Always returns the U_ZERO_ERROR_MESSAGE class constant value.
     *
     * @return string The error message from last formatter call
     *
     * @see https://php.net/numberformatter.geterrormessage
     */
    public function getErrorMessage()
    {
    }
    /**
     * Returns the formatter's locale.
     *
     * The parameter $type is currently ignored.
     *
     * @param int $type Not supported. The locale name type to return (Locale::VALID_LOCALE or Locale::ACTUAL_LOCALE)
     *
     * @return string The locale used to create the formatter. Currently always
     *                returns "en".
     *
     * @see https://php.net/numberformatter.getlocale
     */
    public function getLocale($type = \Symfony\Component\Intl\Locale\Locale::ACTUAL_LOCALE)
    {
    }
    /**
     * Not supported. Returns the formatter's pattern.
     *
     * @return string|false The pattern string used by the formatter or false on error
     *
     * @see https://php.net/numberformatter.getpattern
     *
     * @throws MethodNotImplementedException
     */
    public function getPattern()
    {
    }
    /**
     * Not supported. Returns a formatter symbol value.
     *
     * @param int $attr A symbol specifier, one of the format symbol constants
     *
     * @return string|false The symbol value or false on error
     *
     * @see https://php.net/numberformatter.getsymbol
     */
    public function getSymbol($attr)
    {
    }
    /**
     * Not supported. Returns a formatter text attribute value.
     *
     * @param int $attr An attribute specifier, one of the text attribute constants
     *
     * @return string|false The attribute value or false on error
     *
     * @see https://php.net/numberformatter.gettextattribute
     */
    public function getTextAttribute($attr)
    {
    }
    /**
     * Not supported. Parse a currency number.
     *
     * @param string $value    The value to parse
     * @param string $currency Parameter to receive the currency name (reference)
     * @param int    $position Offset to begin the parsing on return this value will hold the offset at which the parsing ended
     *
     * @return float|false The parsed numeric value or false on error
     *
     * @see https://php.net/numberformatter.parsecurrency
     *
     * @throws MethodNotImplementedException
     */
    public function parseCurrency($value, &$currency, &$position = null)
    {
    }
    /**
     * Parse a number.
     *
     * @param string $value    The value to parse
     * @param int    $type     Type of the formatting, one of the format type constants. NumberFormatter::TYPE_DOUBLE by default.
     * @param int    $position Offset to begin the parsing on return this value will hold the offset at which the parsing ended
     *
     * @return int|float|false The parsed value or false on error
     *
     * @see https://php.net/numberformatter.parse
     */
    public function parse($value, $type = self::TYPE_DOUBLE, &$position = 0)
    {
    }
    /**
     * Set an attribute.
     *
     * @param int $attr  An attribute specifier, one of the numeric attribute constants.
     *                   The only currently supported attributes are NumberFormatter::FRACTION_DIGITS,
     *                   NumberFormatter::GROUPING_USED and NumberFormatter::ROUNDING_MODE.
     * @param int $value The attribute value
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/numberformatter.setattribute
     *
     * @throws MethodArgumentValueNotImplementedException When the $attr is not supported
     * @throws MethodArgumentValueNotImplementedException When the $value is not supported
     */
    public function setAttribute($attr, $value)
    {
    }
    /**
     * Not supported. Set the formatter's pattern.
     *
     * @param string $pattern A pattern string in conformance with the ICU DecimalFormat documentation
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/numberformatter.setpattern
     * @see http://www.icu-project.org/apiref/icu4c/classDecimalFormat.html#_details
     *
     * @throws MethodNotImplementedException
     */
    public function setPattern($pattern)
    {
    }
    /**
     * Not supported. Set the formatter's symbol.
     *
     * @param int    $attr  A symbol specifier, one of the format symbol constants
     * @param string $value The value for the symbol
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/numberformatter.setsymbol
     *
     * @throws MethodNotImplementedException
     */
    public function setSymbol($attr, $value)
    {
    }
    /**
     * Not supported. Set a text attribute.
     *
     * @param int    $attr  An attribute specifier, one of the text attribute constants
     * @param string $value The attribute value
     *
     * @return bool true on success or false on failure
     *
     * @see https://php.net/numberformatter.settextattribute
     *
     * @throws MethodNotImplementedException
     */
    public function setTextAttribute($attr, $value)
    {
    }
    /**
     * Set the error to the default U_ZERO_ERROR.
     */
    protected function resetError()
    {
    }
}
