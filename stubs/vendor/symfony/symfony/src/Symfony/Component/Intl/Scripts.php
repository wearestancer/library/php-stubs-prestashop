<?php

namespace Symfony\Component\Intl;

/**
 * Gives access to script-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
final class Scripts extends \Symfony\Component\Intl\ResourceBundle
{
    /**
     * @return string[]
     */
    public static function getScriptCodes() : array
    {
    }
    public static function exists(string $script) : bool
    {
    }
    /**
     * @throws MissingResourceException if the script code does not exist
     */
    public static function getName(string $script, string $displayLocale = null) : string
    {
    }
    /**
     * @return string[]
     */
    public static function getNames($displayLocale = null) : array
    {
    }
}
