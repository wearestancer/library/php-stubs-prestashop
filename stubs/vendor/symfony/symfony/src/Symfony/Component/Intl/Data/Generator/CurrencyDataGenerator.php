<?php

namespace Symfony\Component\Intl\Data\Generator;

/**
 * The rule for compiling the currency bundle.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class CurrencyDataGenerator extends \Symfony\Component\Intl\Data\Generator\AbstractDataGenerator
{
    /**
     * {@inheritdoc}
     */
    protected function scanLocales(\Symfony\Component\Intl\Data\Util\LocaleScanner $scanner, string $sourceDir) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function compileTemporaryBundles(\Symfony\Component\Intl\Data\Bundle\Compiler\BundleCompilerInterface $compiler, string $sourceDir, string $tempDir)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function preGenerate()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateDataForLocale(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir, string $displayLocale) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateDataForRoot(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateDataForMeta(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array
    {
    }
}
