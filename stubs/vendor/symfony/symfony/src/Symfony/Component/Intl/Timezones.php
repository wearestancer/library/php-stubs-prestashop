<?php

namespace Symfony\Component\Intl;

/**
 * Gives access to timezone-related ICU data.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
final class Timezones extends \Symfony\Component\Intl\ResourceBundle
{
    /**
     * @return string[]
     */
    public static function getIds() : array
    {
    }
    public static function exists(string $timezone) : bool
    {
    }
    /**
     * @throws MissingResourceException if the timezone identifier does not exist or is an alias
     */
    public static function getName(string $timezone, string $displayLocale = null) : string
    {
    }
    /**
     * @return string[]
     */
    public static function getNames(string $displayLocale = null) : array
    {
    }
    /**
     * @throws \Exception       if the timezone identifier does not exist
     * @throws RuntimeException if there's no timezone DST transition information available
     */
    public static function getRawOffset(string $timezone, int $timestamp = null) : int
    {
    }
    public static function getGmtOffset(string $timezone, int $timestamp = null, string $displayLocale = null) : string
    {
    }
    /**
     * @throws MissingResourceException if the timezone identifier has no associated country code
     */
    public static function getCountryCode(string $timezone) : string
    {
    }
    /**
     * @throws MissingResourceException if the country code does not exist
     */
    public static function forCountryCode(string $country) : array
    {
    }
}
