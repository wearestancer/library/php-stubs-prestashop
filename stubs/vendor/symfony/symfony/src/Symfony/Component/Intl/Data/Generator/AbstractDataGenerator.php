<?php

namespace Symfony\Component\Intl\Data\Generator;

/**
 * The rule for compiling the currency bundle.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
abstract class AbstractDataGenerator
{
    public function __construct(\Symfony\Component\Intl\Data\Bundle\Compiler\BundleCompilerInterface $compiler, string $dirName)
    {
    }
    public function generateData(\Symfony\Component\Intl\Data\Generator\GeneratorConfig $config)
    {
    }
    /**
     * @return string[]
     */
    protected abstract function scanLocales(\Symfony\Component\Intl\Data\Util\LocaleScanner $scanner, string $sourceDir) : array;
    protected abstract function compileTemporaryBundles(\Symfony\Component\Intl\Data\Bundle\Compiler\BundleCompilerInterface $compiler, string $sourceDir, string $tempDir);
    protected abstract function preGenerate();
    protected abstract function generateDataForLocale(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir, string $displayLocale) : ?array;
    protected abstract function generateDataForRoot(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array;
    protected abstract function generateDataForMeta(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array;
}
