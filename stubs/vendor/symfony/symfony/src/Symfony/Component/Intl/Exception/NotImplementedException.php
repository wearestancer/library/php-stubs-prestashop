<?php

namespace Symfony\Component\Intl\Exception;

/**
 * Base exception class for not implemented behaviors of the intl extension in the Locale component.
 *
 * @author Eriksen Costa <eriksen.costa@infranology.com.br>
 */
class NotImplementedException extends \Symfony\Component\Intl\Exception\RuntimeException
{
    public const INTL_INSTALL_MESSAGE = 'Please install the "intl" extension for full localization capabilities.';
    /**
     * @param string $message The exception message. A note to install the intl extension is appended to this string
     */
    public function __construct(string $message)
    {
    }
}
