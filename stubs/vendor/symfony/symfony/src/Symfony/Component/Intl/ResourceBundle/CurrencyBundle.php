<?php

namespace Symfony\Component\Intl\ResourceBundle;

/**
 * Default implementation of {@link CurrencyBundleInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class CurrencyBundle extends \Symfony\Component\Intl\Data\Provider\CurrencyDataProvider implements \Symfony\Component\Intl\ResourceBundle\CurrencyBundleInterface
{
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, \Symfony\Component\Intl\Data\Provider\LocaleDataProvider $localeProvider)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrencySymbol($currency, $displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrencyName($currency, $displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCurrencyNames($displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFractionDigits($currency)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRoundingIncrement($currency)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocales()
    {
    }
}
