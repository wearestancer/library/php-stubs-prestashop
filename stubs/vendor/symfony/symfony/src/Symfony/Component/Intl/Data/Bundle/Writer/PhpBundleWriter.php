<?php

namespace Symfony\Component\Intl\Data\Bundle\Writer;

/**
 * Writes .php resource bundles.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class PhpBundleWriter implements \Symfony\Component\Intl\Data\Bundle\Writer\BundleWriterInterface
{
    /**
     * {@inheritdoc}
     */
    public function write($path, $locale, $data)
    {
    }
}
