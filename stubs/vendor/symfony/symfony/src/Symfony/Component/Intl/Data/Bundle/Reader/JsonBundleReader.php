<?php

namespace Symfony\Component\Intl\Data\Bundle\Reader;

/**
 * Reads .json resource bundles.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class JsonBundleReader implements \Symfony\Component\Intl\Data\Bundle\Reader\BundleReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read($path, $locale)
    {
    }
}
