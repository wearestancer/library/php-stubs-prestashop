<?php

namespace Symfony\Component\Intl\Exception;

/**
 * RuntimeException for the Intl component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class RuntimeException extends \RuntimeException implements \Symfony\Component\Intl\Exception\ExceptionInterface
{
}
