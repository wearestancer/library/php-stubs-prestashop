<?php

namespace Symfony\Component\Intl\Exception;

/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class ResourceBundleNotFoundException extends \Symfony\Component\Intl\Exception\RuntimeException
{
}
