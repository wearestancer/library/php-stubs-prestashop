<?php

namespace Symfony\Component\Intl\Data\Bundle\Reader;

/**
 * Reads binary .res resource bundles.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class IntlBundleReader implements \Symfony\Component\Intl\Data\Bundle\Reader\BundleReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read($path, $locale)
    {
    }
}
