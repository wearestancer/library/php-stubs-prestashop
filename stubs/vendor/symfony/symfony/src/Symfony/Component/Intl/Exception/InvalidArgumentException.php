<?php

namespace Symfony\Component\Intl\Exception;

/**
 * InvalidArgumentException for the Intl component.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\Intl\Exception\ExceptionInterface
{
}
