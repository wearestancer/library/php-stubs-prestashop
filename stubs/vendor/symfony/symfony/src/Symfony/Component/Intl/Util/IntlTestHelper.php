<?php

namespace Symfony\Component\Intl\Util;

/**
 * Helper class for preparing test cases that rely on the Intl component.
 *
 * Any test that tests functionality relying on either the intl classes or
 * the resource bundle data should call either of the methods
 * {@link requireIntl()} or {@link requireFullIntl()}. Calling
 * {@link requireFullIntl()} is only necessary if you use functionality in the
 * test that is not provided by the stub intl implementation.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class IntlTestHelper
{
    /**
     * Should be called before tests that work fine with the stub implementation.
     */
    public static function requireIntl(\PHPUnit\Framework\TestCase $testCase, $minimumIcuVersion = null)
    {
    }
    /**
     * Should be called before tests that require a feature-complete intl
     * implementation.
     */
    public static function requireFullIntl(\PHPUnit\Framework\TestCase $testCase, $minimumIcuVersion = null)
    {
    }
    /**
     * Skips the test unless the current system has a 32bit architecture.
     */
    public static function require32Bit(\PHPUnit\Framework\TestCase $testCase)
    {
    }
    /**
     * Skips the test unless the current system has a 64bit architecture.
     */
    public static function require64Bit(\PHPUnit\Framework\TestCase $testCase)
    {
    }
}
