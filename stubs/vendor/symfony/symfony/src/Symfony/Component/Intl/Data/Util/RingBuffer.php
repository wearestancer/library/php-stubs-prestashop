<?php

namespace Symfony\Component\Intl\Data\Util;

/**
 * Implements a ring buffer.
 *
 * A ring buffer is an array-like structure with a fixed size. If the buffer
 * is full, the next written element overwrites the first bucket in the buffer,
 * then the second and so on.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class RingBuffer implements \ArrayAccess
{
    public function __construct(int $size)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetExists($key) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetSet($key, $value) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function offsetUnset($key) : void
    {
    }
}
