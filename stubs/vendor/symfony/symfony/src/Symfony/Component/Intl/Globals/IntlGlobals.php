<?php

namespace Symfony\Component\Intl\Globals;

/**
 * Provides fake static versions of the global functions in the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
abstract class IntlGlobals
{
    /**
     * Indicates that no error occurred.
     */
    public const U_ZERO_ERROR = 0;
    /**
     * Indicates that an invalid argument was passed.
     */
    public const U_ILLEGAL_ARGUMENT_ERROR = 1;
    /**
     * Indicates that the parse() operation failed.
     */
    public const U_PARSE_ERROR = 9;
    /**
     * Returns whether the error code indicates a failure.
     *
     * @param int $errorCode The error code returned by IntlGlobals::getErrorCode()
     */
    public static function isFailure(int $errorCode) : bool
    {
    }
    /**
     * Returns the error code of the last operation.
     *
     * Returns IntlGlobals::U_ZERO_ERROR if no error occurred.
     *
     * @return int
     */
    public static function getErrorCode()
    {
    }
    /**
     * Returns the error message of the last operation.
     *
     * Returns "U_ZERO_ERROR" if no error occurred.
     */
    public static function getErrorMessage() : string
    {
    }
    /**
     * Returns the symbolic name for a given error code.
     *
     * @param int $code The error code returned by IntlGlobals::getErrorCode()
     */
    public static function getErrorName(int $code) : string
    {
    }
    /**
     * Sets the current error.
     *
     * @param int    $code    One of the error constants in this class
     * @param string $message The ICU class error message
     *
     * @throws \InvalidArgumentException If the code is not one of the error constants in this class
     */
    public static function setError(int $code, string $message = '')
    {
    }
}
