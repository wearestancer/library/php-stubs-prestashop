<?php

namespace Symfony\Component\Intl\Data\Provider;

/**
 * Data provider for currency-related data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class CurrencyDataProvider
{
    public const INDEX_SYMBOL = 0;
    public const INDEX_NAME = 1;
    public const INDEX_FRACTION_DIGITS = 0;
    public const INDEX_ROUNDING_INCREMENT = 1;
    /**
     * Creates a data provider that reads currency-related data from a
     * resource bundle.
     *
     * @param string $path The path to the resource bundle
     */
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader)
    {
    }
    public function getCurrencies()
    {
    }
    public function getSymbol($currency, $displayLocale = null)
    {
    }
    public function getName($currency, $displayLocale = null)
    {
    }
    public function getNames($displayLocale = null)
    {
    }
    /**
     * Data provider for {@link \Symfony\Component\Intl\Currency::getFractionDigits()}.
     */
    public function getFractionDigits($currency)
    {
    }
    /**
     * Data provider for {@link \Symfony\Component\Intl\Currency::getRoundingIncrement()}.
     */
    public function getRoundingIncrement($currency)
    {
    }
    /**
     * Data provider for {@link \Symfony\Component\Intl\Currency::getNumericCode()}.
     */
    public function getNumericCode($currency)
    {
    }
    /**
     * Data provider for {@link \Symfony\Component\Intl\Currency::forNumericCode()}.
     */
    public function forNumericCode($numericCode)
    {
    }
}
