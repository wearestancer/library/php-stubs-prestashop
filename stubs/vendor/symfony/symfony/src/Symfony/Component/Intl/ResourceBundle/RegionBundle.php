<?php

namespace Symfony\Component\Intl\ResourceBundle;

/**
 * Default implementation of {@link RegionBundleInterface}.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class RegionBundle extends \Symfony\Component\Intl\Data\Provider\RegionDataProvider implements \Symfony\Component\Intl\ResourceBundle\RegionBundleInterface
{
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, \Symfony\Component\Intl\Data\Provider\LocaleDataProvider $localeProvider)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCountryName($country, $displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCountryNames($displayLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLocales()
    {
    }
}
