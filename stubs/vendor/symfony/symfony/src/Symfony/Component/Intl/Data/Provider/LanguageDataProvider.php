<?php

namespace Symfony\Component\Intl\Data\Provider;

/**
 * Data provider for language-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal to be removed in 5.0.
 */
class LanguageDataProvider
{
    /**
     * Creates a data provider that reads locale-related data from .res files.
     *
     * @param string $path The path to the directory containing the .res files
     */
    public function __construct(string $path, \Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader)
    {
    }
    public function getLanguages()
    {
    }
    public function getAliases()
    {
    }
    public function getName($language, $displayLocale = null)
    {
    }
    public function getNames($displayLocale = null)
    {
    }
    public function getAlpha3Code($language)
    {
    }
}
