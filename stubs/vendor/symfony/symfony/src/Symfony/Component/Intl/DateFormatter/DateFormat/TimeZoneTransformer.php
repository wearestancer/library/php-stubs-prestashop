<?php

namespace Symfony\Component\Intl\DateFormatter\DateFormat;

/**
 * Parser and formatter for time zone format.
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class TimezoneTransformer extends \Symfony\Component\Intl\DateFormatter\DateFormat\Transformer
{
    /**
     * {@inheritdoc}
     *
     * @throws NotImplementedException When time zone is different than UTC or GMT (Etc/GMT)
     */
    public function format(\DateTime $dateTime, int $length) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getReverseMatchingRegExp(int $length) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function extractDateOptions(string $matched, int $length) : array
    {
    }
    /**
     * Get an Etc/GMT timezone identifier for the specified timezone.
     *
     * The PHP documentation for timezones states to not use the 'Other' time zones because them exists
     * "for backwards compatibility". However all Etc/GMT time zones are in the tz database 'etcetera' file,
     * which indicates they are not deprecated (neither are old names).
     *
     * Only GMT, Etc/Universal, Etc/Zulu, Etc/Greenwich, Etc/GMT-0, Etc/GMT+0 and Etc/GMT0 are old names and
     * are linked to Etc/GMT or Etc/UTC.
     *
     * @param string $formattedTimeZone A GMT timezone string (GMT-03:00, e.g.)
     *
     * @return string A timezone identifier
     *
     * @see https://php.net/timezones.others
     *
     * @throws NotImplementedException   When the GMT time zone have minutes offset different than zero
     * @throws \InvalidArgumentException When the value can not be matched with pattern
     */
    public static function getEtcTimeZoneId(string $formattedTimeZone) : string
    {
    }
}
