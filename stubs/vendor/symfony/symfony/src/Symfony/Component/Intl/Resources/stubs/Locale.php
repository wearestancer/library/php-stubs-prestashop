<?php

/**
 * Stub implementation for the Locale class of the intl extension.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @see IntlLocale
 */
class Locale extends \Symfony\Component\Intl\Locale\Locale
{
}
