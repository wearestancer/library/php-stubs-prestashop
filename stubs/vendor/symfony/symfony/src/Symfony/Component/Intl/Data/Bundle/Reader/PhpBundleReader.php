<?php

namespace Symfony\Component\Intl\Data\Bundle\Reader;

/**
 * Reads .php resource bundles.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class PhpBundleReader implements \Symfony\Component\Intl\Data\Bundle\Reader\BundleReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read($path, $locale)
    {
    }
}
