<?php

namespace Symfony\Component\Intl;

/**
 * Gives access to region-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
final class Countries extends \Symfony\Component\Intl\ResourceBundle
{
    /**
     * Returns all available countries.
     *
     * Countries are returned as uppercase ISO 3166 two-letter country codes.
     *
     * A full table of ISO 3166 country codes can be found here:
     * https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
     *
     * This list only contains "officially assigned ISO 3166-1 alpha-2" country codes.
     *
     * @return string[] an array of canonical ISO 3166 alpha-2 country codes
     */
    public static function getCountryCodes() : array
    {
    }
    /**
     * Returns all available countries (3 letters).
     *
     * Countries are returned as uppercase ISO 3166 three-letter country codes.
     *
     * This list only contains "officially assigned ISO 3166-1 alpha-3" country codes.
     *
     * @return string[] an array of canonical ISO 3166 alpha-3 country codes
     */
    public static function getAlpha3Codes() : array
    {
    }
    public static function getAlpha3Code(string $alpha2Code) : string
    {
    }
    public static function getAlpha2Code(string $alpha3Code) : string
    {
    }
    public static function exists(string $alpha2Code) : bool
    {
    }
    public static function alpha3CodeExists(string $alpha3Code) : bool
    {
    }
    /**
     * Gets the country name from its alpha2 code.
     *
     * @throws MissingResourceException if the country code does not exist
     */
    public static function getName(string $country, string $displayLocale = null) : string
    {
    }
    /**
     * Gets the country name from its alpha3 code.
     *
     * @throws MissingResourceException if the country code does not exist
     */
    public static function getAlpha3Name(string $alpha3Code, string $displayLocale = null) : string
    {
    }
    /**
     * Gets the list of country names indexed with alpha2 codes as keys.
     *
     * @return string[]
     */
    public static function getNames($displayLocale = null) : array
    {
    }
    /**
     * Gets the list of country names indexed with alpha3 codes as keys.
     *
     * Same as method getNames, but with alpha3 codes instead of alpha2 codes as keys.
     *
     * @return string[]
     */
    public static function getAlpha3Names($displayLocale = null) : array
    {
    }
}
