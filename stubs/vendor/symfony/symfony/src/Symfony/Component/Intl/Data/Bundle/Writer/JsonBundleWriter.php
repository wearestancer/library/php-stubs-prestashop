<?php

namespace Symfony\Component\Intl\Data\Bundle\Writer;

/**
 * Writes .json resource bundles.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @internal
 */
class JsonBundleWriter implements \Symfony\Component\Intl\Data\Bundle\Writer\BundleWriterInterface
{
    /**
     * {@inheritdoc}
     */
    public function write($path, $locale, $data)
    {
    }
}
