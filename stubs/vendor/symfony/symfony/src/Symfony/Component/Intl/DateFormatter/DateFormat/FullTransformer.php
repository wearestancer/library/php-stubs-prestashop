<?php

namespace Symfony\Component\Intl\DateFormatter\DateFormat;

/**
 * Parser and formatter for date formats.
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @internal
 */
class FullTransformer
{
    /**
     * @param string $pattern  The pattern to be used to format and/or parse values
     * @param string $timezone The timezone to perform the date/time calculations
     */
    public function __construct(string $pattern, string $timezone)
    {
    }
    /**
     * Format a DateTime using ICU dateformat pattern.
     *
     * @return string The formatted value
     */
    public function format(\DateTime $dateTime) : string
    {
    }
    /**
     * Parse a pattern based string to a timestamp value.
     *
     * @param \DateTime $dateTime A configured DateTime object to use to perform the date calculation
     * @param string    $value    String to convert to a time value
     *
     * @return int|false The corresponding Unix timestamp
     *
     * @throws \InvalidArgumentException When the value can not be matched with pattern
     */
    public function parse(\DateTime $dateTime, string $value)
    {
    }
}
