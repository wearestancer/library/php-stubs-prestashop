<?php

namespace Symfony\Component\Intl;

/**
 * Gives access to currency-related ICU data.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 * @author Roland Franssen <franssen.roland@gmail.com>
 */
final class Currencies extends \Symfony\Component\Intl\ResourceBundle
{
    /**
     * @return string[]
     */
    public static function getCurrencyCodes() : array
    {
    }
    public static function exists(string $currency) : bool
    {
    }
    /**
     * @throws MissingResourceException if the currency code does not exist
     */
    public static function getName(string $currency, string $displayLocale = null) : string
    {
    }
    /**
     * @return string[]
     */
    public static function getNames(string $displayLocale = null) : array
    {
    }
    /**
     * @throws MissingResourceException if the currency code does not exist
     */
    public static function getSymbol(string $currency, string $displayLocale = null) : string
    {
    }
    public static function getFractionDigits(string $currency) : int
    {
    }
    /**
     * @return float|int
     */
    public static function getRoundingIncrement(string $currency)
    {
    }
    /**
     * @throws MissingResourceException if the currency code has no numeric code
     */
    public static function getNumericCode(string $currency) : int
    {
    }
    /**
     * @throws MissingResourceException if the numeric code does not exist
     */
    public static function forNumericCode(int $numericCode) : array
    {
    }
}
