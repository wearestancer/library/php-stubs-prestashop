<?php

namespace Symfony\Component\Intl\Data\Generator;

/**
 * The rule for compiling the zone bundle.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 *
 * @internal
 */
class TimezoneDataGenerator extends \Symfony\Component\Intl\Data\Generator\AbstractDataGenerator
{
    use \Symfony\Component\Intl\Data\Generator\FallbackTrait;
    /**
     * {@inheritdoc}
     */
    protected function scanLocales(\Symfony\Component\Intl\Data\Util\LocaleScanner $scanner, string $sourceDir) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function compileTemporaryBundles(\Symfony\Component\Intl\Data\Bundle\Compiler\BundleCompilerInterface $compiler, string $sourceDir, string $tempDir)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function preGenerate()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateDataForLocale(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir, string $displayLocale) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateDataForRoot(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function generateDataForMeta(\Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReaderInterface $reader, string $tempDir) : ?array
    {
    }
}
