<?php

namespace Symfony\Component\Console\EventListener;

/**
 * @author James Halsall <james.t.halsall@googlemail.com>
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
class ErrorListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Psr\Log\LoggerInterface $logger = null)
    {
    }
    public function onConsoleError(\Symfony\Component\Console\Event\ConsoleErrorEvent $event)
    {
    }
    public function onConsoleTerminate(\Symfony\Component\Console\Event\ConsoleTerminateEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
