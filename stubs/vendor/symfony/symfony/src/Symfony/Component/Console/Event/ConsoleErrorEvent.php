<?php

namespace Symfony\Component\Console\Event;

/**
 * Allows to handle throwables thrown while running a command.
 *
 * @author Wouter de Jong <wouter@wouterj.nl>
 */
final class ConsoleErrorEvent extends \Symfony\Component\Console\Event\ConsoleEvent
{
    public function __construct(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output, \Throwable $error, \Symfony\Component\Console\Command\Command $command = null)
    {
    }
    public function getError(): \Throwable
    {
    }
    public function setError(\Throwable $error): void
    {
    }
    public function setExitCode(int $exitCode): void
    {
    }
    public function getExitCode(): int
    {
    }
}
