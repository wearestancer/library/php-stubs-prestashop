<?php

namespace Symfony\Component\BrowserKit;

/**
 * Simulates a browser.
 *
 * To make the actual request, you need to implement the doRequest() method.
 *
 * HttpBrowser is an implementation that uses the HttpClient component
 * to make real HTTP requests.
 *
 * If you want to be able to run requests in their own process (insulated flag),
 * you need to also implement the getScript() method.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractBrowser extends \Symfony\Component\BrowserKit\Client
{
}
