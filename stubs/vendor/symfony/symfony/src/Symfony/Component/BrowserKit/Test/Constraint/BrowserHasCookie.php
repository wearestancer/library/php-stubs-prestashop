<?php

namespace Symfony\Component\BrowserKit\Test\Constraint;

final class BrowserHasCookie extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $name, string $path = '/', string $domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
