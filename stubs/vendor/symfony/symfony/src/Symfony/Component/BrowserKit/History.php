<?php

namespace Symfony\Component\BrowserKit;

/**
 * History.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class History
{
    protected $stack = [];
    protected $position = -1;
    /**
     * Clears the history.
     */
    public function clear()
    {
    }
    /**
     * Adds a Request to the history.
     */
    public function add(\Symfony\Component\BrowserKit\Request $request)
    {
    }
    /**
     * Returns true if the history is empty.
     *
     * @return bool true if the history is empty, false otherwise
     */
    public function isEmpty()
    {
    }
    /**
     * Goes back in the history.
     *
     * @return Request A Request instance
     *
     * @throws \LogicException if the stack is already on the first page
     */
    public function back()
    {
    }
    /**
     * Goes forward in the history.
     *
     * @return Request A Request instance
     *
     * @throws \LogicException if the stack is already on the last page
     */
    public function forward()
    {
    }
    /**
     * Returns the current element in the history.
     *
     * @return Request A Request instance
     *
     * @throws \LogicException if the stack is empty
     */
    public function current()
    {
    }
}
