<?php

namespace Symfony\Component\BrowserKit;

/**
 * Client simulates a browser.
 *
 * To make the actual request, you need to implement the doRequest() method.
 *
 * If you want to be able to run requests in their own process (insulated flag),
 * you need to also implement the getScript() method.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.3, use "\Symfony\Component\BrowserKit\AbstractBrowser" instead
 */
abstract class Client
{
    protected $history;
    protected $cookieJar;
    protected $server = [];
    protected $internalRequest;
    protected $request;
    protected $internalResponse;
    protected $response;
    protected $crawler;
    protected $insulated = false;
    protected $redirect;
    protected $followRedirects = true;
    protected $followMetaRefresh = false;
    /**
     * @param array $server The server parameters (equivalent of $_SERVER)
     */
    public function __construct(array $server = [], \Symfony\Component\BrowserKit\History $history = null, \Symfony\Component\BrowserKit\CookieJar $cookieJar = null)
    {
    }
    /**
     * Sets whether to automatically follow redirects or not.
     *
     * @param bool $followRedirect Whether to follow redirects
     */
    public function followRedirects($followRedirect = true)
    {
    }
    /**
     * Sets whether to automatically follow meta refresh redirects or not.
     */
    public function followMetaRefresh(bool $followMetaRefresh = true)
    {
    }
    /**
     * Returns whether client automatically follows redirects or not.
     *
     * @return bool
     */
    public function isFollowingRedirects()
    {
    }
    /**
     * Sets the maximum number of redirects that crawler can follow.
     *
     * @param int $maxRedirects
     */
    public function setMaxRedirects($maxRedirects)
    {
    }
    /**
     * Returns the maximum number of redirects that crawler can follow.
     *
     * @return int
     */
    public function getMaxRedirects()
    {
    }
    /**
     * Sets the insulated flag.
     *
     * @param bool $insulated Whether to insulate the requests or not
     *
     * @throws \RuntimeException When Symfony Process Component is not installed
     */
    public function insulate($insulated = true)
    {
    }
    /**
     * Sets server parameters.
     *
     * @param array $server An array of server parameters
     */
    public function setServerParameters(array $server)
    {
    }
    /**
     * Sets single server parameter.
     *
     * @param string $key   A key of the parameter
     * @param string $value A value of the parameter
     */
    public function setServerParameter($key, $value)
    {
    }
    /**
     * Gets single server parameter for specified key.
     *
     * @param string $key     A key of the parameter to get
     * @param mixed  $default A default value when key is undefined
     *
     * @return mixed A value of the parameter
     */
    public function getServerParameter($key, $default = '')
    {
    }
    public function xmlHttpRequest(string $method, string $uri, array $parameters = [], array $files = [], array $server = [], string $content = null, bool $changeHistory = true) : \Symfony\Component\DomCrawler\Crawler
    {
    }
    /**
     * Returns the History instance.
     *
     * @return History A History instance
     */
    public function getHistory()
    {
    }
    /**
     * Returns the CookieJar instance.
     *
     * @return CookieJar A CookieJar instance
     */
    public function getCookieJar()
    {
    }
    /**
     * Returns the current Crawler instance.
     *
     * @return Crawler A Crawler instance
     */
    public function getCrawler()
    {
    }
    /**
     * Returns the current BrowserKit Response instance.
     *
     * @return Response A BrowserKit Response instance
     */
    public function getInternalResponse()
    {
    }
    /**
     * Returns the current origin response instance.
     *
     * The origin response is the response instance that is returned
     * by the code that handles requests.
     *
     * @return object A response instance
     *
     * @see doRequest()
     */
    public function getResponse()
    {
    }
    /**
     * Returns the current BrowserKit Request instance.
     *
     * @return Request A BrowserKit Request instance
     */
    public function getInternalRequest()
    {
    }
    /**
     * Returns the current origin Request instance.
     *
     * The origin request is the request instance that is sent
     * to the code that handles requests.
     *
     * @return object A Request instance
     *
     * @see doRequest()
     */
    public function getRequest()
    {
    }
    /**
     * Clicks on a given link.
     *
     * @return Crawler
     */
    public function click(\Symfony\Component\DomCrawler\Link $link)
    {
    }
    /**
     * Clicks the first link (or clickable image) that contains the given text.
     *
     * @param string $linkText The text of the link or the alt attribute of the clickable image
     */
    public function clickLink(string $linkText) : \Symfony\Component\DomCrawler\Crawler
    {
    }
    /**
     * Submits a form.
     *
     * @param array $values           An array of form field values
     * @param array $serverParameters An array of server parameters
     *
     * @return Crawler
     */
    public function submit(\Symfony\Component\DomCrawler\Form $form, array $values = [])
    {
    }
    /**
     * Finds the first form that contains a button with the given content and
     * uses it to submit the given form field values.
     *
     * @param string $button           The text content, id, value or name of the form <button> or <input type="submit">
     * @param array  $fieldValues      Use this syntax: ['my_form[name]' => '...', 'my_form[email]' => '...']
     * @param string $method           The HTTP method used to submit the form
     * @param array  $serverParameters These values override the ones stored in $_SERVER (HTTP headers must include an HTTP_ prefix as PHP does)
     */
    public function submitForm(string $button, array $fieldValues = [], string $method = 'POST', array $serverParameters = []) : \Symfony\Component\DomCrawler\Crawler
    {
    }
    /**
     * Calls a URI.
     *
     * @param string $method        The request method
     * @param string $uri           The URI to fetch
     * @param array  $parameters    The Request parameters
     * @param array  $files         The files
     * @param array  $server        The server parameters (HTTP headers are referenced with an HTTP_ prefix as PHP does)
     * @param string $content       The raw body data
     * @param bool   $changeHistory Whether to update the history or not (only used internally for back(), forward(), and reload())
     *
     * @return Crawler
     */
    public function request(string $method, string $uri, array $parameters = [], array $files = [], array $server = [], string $content = null, bool $changeHistory = true)
    {
    }
    /**
     * Makes a request in another process.
     *
     * @param object $request An origin request instance
     *
     * @return object An origin response instance
     *
     * @throws \RuntimeException When processing returns exit code
     */
    protected function doRequestInProcess($request)
    {
    }
    /**
     * Makes a request.
     *
     * @param object $request An origin request instance
     *
     * @return object An origin response instance
     */
    protected abstract function doRequest($request);
    /**
     * Returns the script to execute when the request must be insulated.
     *
     * @param object $request An origin request instance
     *
     * @throws \LogicException When this abstract class is not implemented
     */
    protected function getScript($request)
    {
    }
    /**
     * Filters the BrowserKit request to the origin one.
     *
     * @return object An origin request instance
     */
    protected function filterRequest(\Symfony\Component\BrowserKit\Request $request)
    {
    }
    /**
     * Filters the origin response to the BrowserKit one.
     *
     * @param object $response The origin response to filter
     *
     * @return Response An BrowserKit Response instance
     */
    protected function filterResponse($response)
    {
    }
    /**
     * Creates a crawler.
     *
     * This method returns null if the DomCrawler component is not available.
     *
     * @param string $uri     A URI
     * @param string $content Content for the crawler to use
     * @param string $type    Content type
     *
     * @return Crawler|null
     */
    protected function createCrawlerFromContent($uri, $content, $type)
    {
    }
    /**
     * Goes back in the browser history.
     *
     * @return Crawler
     */
    public function back()
    {
    }
    /**
     * Goes forward in the browser history.
     *
     * @return Crawler
     */
    public function forward()
    {
    }
    /**
     * Reloads the current browser.
     *
     * @return Crawler
     */
    public function reload()
    {
    }
    /**
     * Follow redirects?
     *
     * @return Crawler
     *
     * @throws \LogicException If request was not a redirect
     */
    public function followRedirect()
    {
    }
    /**
     * Restarts the client.
     *
     * It flushes history and all cookies.
     */
    public function restart()
    {
    }
    /**
     * Takes a URI and converts it to absolute if it is not already absolute.
     *
     * @param string $uri A URI
     *
     * @return string An absolute URI
     */
    protected function getAbsoluteUri($uri)
    {
    }
    /**
     * Makes a request from a Request object directly.
     *
     * @param bool $changeHistory Whether to update the history or not (only used internally for back(), forward(), and reload())
     *
     * @return Crawler
     */
    protected function requestFromRequest(\Symfony\Component\BrowserKit\Request $request, $changeHistory = true)
    {
    }
}
