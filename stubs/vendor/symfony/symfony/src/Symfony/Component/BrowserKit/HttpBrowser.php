<?php

namespace Symfony\Component\BrowserKit;

/**
 * An implementation of a browser using the HttpClient component
 * to make real HTTP requests.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpBrowser extends \Symfony\Component\BrowserKit\AbstractBrowser
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Symfony\Component\BrowserKit\History $history = null, \Symfony\Component\BrowserKit\CookieJar $cookieJar = null)
    {
    }
    /**
     * @param Request $request
     */
    protected function doRequest($request) : \Symfony\Component\BrowserKit\Response
    {
    }
}
