<?php

namespace Symfony\Component\BrowserKit;

/**
 * CookieJar.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class CookieJar
{
    protected $cookieJar = [];
    public function set(\Symfony\Component\BrowserKit\Cookie $cookie)
    {
    }
    /**
     * Gets a cookie by name.
     *
     * You should never use an empty domain, but if you do so,
     * this method returns the first cookie for the given name/path
     * (this behavior ensures a BC behavior with previous versions of
     * Symfony).
     *
     * @param string $name   The cookie name
     * @param string $path   The cookie path
     * @param string $domain The cookie domain
     *
     * @return Cookie|null A Cookie instance or null if the cookie does not exist
     */
    public function get($name, $path = '/', $domain = null)
    {
    }
    /**
     * Removes a cookie by name.
     *
     * You should never use an empty domain, but if you do so,
     * all cookies for the given name/path expire (this behavior
     * ensures a BC behavior with previous versions of Symfony).
     *
     * @param string $name   The cookie name
     * @param string $path   The cookie path
     * @param string $domain The cookie domain
     */
    public function expire($name, $path = '/', $domain = null)
    {
    }
    /**
     * Removes all the cookies from the jar.
     */
    public function clear()
    {
    }
    /**
     * Updates the cookie jar from a response Set-Cookie headers.
     *
     * @param string[] $setCookies Set-Cookie headers from an HTTP response
     * @param string   $uri        The base URL
     */
    public function updateFromSetCookie(array $setCookies, $uri = null)
    {
    }
    /**
     * Updates the cookie jar from a Response object.
     *
     * @param string $uri The base URL
     */
    public function updateFromResponse(\Symfony\Component\BrowserKit\Response $response, $uri = null)
    {
    }
    /**
     * Returns not yet expired cookies.
     *
     * @return Cookie[] An array of cookies
     */
    public function all()
    {
    }
    /**
     * Returns not yet expired cookie values for the given URI.
     *
     * @param string $uri             A URI
     * @param bool   $returnsRawValue Returns raw value or urldecoded value
     *
     * @return array An array of cookie values
     */
    public function allValues($uri, $returnsRawValue = false)
    {
    }
    /**
     * Returns not yet expired raw cookie values for the given URI.
     *
     * @param string $uri A URI
     *
     * @return array An array of cookie values
     */
    public function allRawValues($uri)
    {
    }
    /**
     * Removes all expired cookies.
     */
    public function flushExpiredCookies()
    {
    }
}
