<?php

namespace Symfony\Component\BrowserKit;

/**
 * Cookie represents an HTTP cookie.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Cookie
{
    protected $name;
    protected $value;
    protected $expires;
    protected $path;
    protected $domain;
    protected $secure;
    protected $httponly;
    protected $rawValue;
    /**
     * Sets a cookie.
     *
     * @param string      $name         The cookie name
     * @param string|null $value        The value of the cookie
     * @param string|null $expires      The time the cookie expires
     * @param string|null $path         The path on the server in which the cookie will be available on
     * @param string      $domain       The domain that the cookie is available
     * @param bool        $secure       Indicates that the cookie should only be transmitted over a secure HTTPS connection from the client
     * @param bool        $httponly     The cookie httponly flag
     * @param bool        $encodedValue Whether the value is encoded or not
     * @param string|null $samesite     The cookie samesite attribute
     */
    public function __construct(string $name, ?string $value, string $expires = null, string $path = null, string $domain = '', bool $secure = false, bool $httponly = true, bool $encodedValue = false, string $samesite = null)
    {
    }
    /**
     * Returns the HTTP representation of the Cookie.
     *
     * @return string
     */
    public function __toString()
    {
    }
    /**
     * Creates a Cookie instance from a Set-Cookie header value.
     *
     * @param string      $cookie A Set-Cookie header value
     * @param string|null $url    The base URL
     *
     * @return static
     *
     * @throws \InvalidArgumentException
     */
    public static function fromString($cookie, $url = null)
    {
    }
    /**
     * Gets the name of the cookie.
     *
     * @return string The cookie name
     */
    public function getName()
    {
    }
    /**
     * Gets the value of the cookie.
     *
     * @return string The cookie value
     */
    public function getValue()
    {
    }
    /**
     * Gets the raw value of the cookie.
     *
     * @return string The cookie value
     */
    public function getRawValue()
    {
    }
    /**
     * Gets the expires time of the cookie.
     *
     * @return string|null The cookie expires time
     */
    public function getExpiresTime()
    {
    }
    /**
     * Gets the path of the cookie.
     *
     * @return string The cookie path
     */
    public function getPath()
    {
    }
    /**
     * Gets the domain of the cookie.
     *
     * @return string The cookie domain
     */
    public function getDomain()
    {
    }
    /**
     * Returns the secure flag of the cookie.
     *
     * @return bool The cookie secure flag
     */
    public function isSecure()
    {
    }
    /**
     * Returns the httponly flag of the cookie.
     *
     * @return bool The cookie httponly flag
     */
    public function isHttpOnly()
    {
    }
    /**
     * Returns true if the cookie has expired.
     *
     * @return bool true if the cookie has expired, false otherwise
     */
    public function isExpired()
    {
    }
    /**
     * Gets the samesite attribute of the cookie.
     *
     * @return string|null The cookie samesite attribute
     */
    public function getSameSite() : ?string
    {
    }
}
