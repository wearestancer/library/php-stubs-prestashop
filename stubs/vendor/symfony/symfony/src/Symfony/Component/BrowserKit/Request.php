<?php

namespace Symfony\Component\BrowserKit;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Request
{
    protected $uri;
    protected $method;
    protected $parameters;
    protected $files;
    protected $cookies;
    protected $server;
    protected $content;
    /**
     * @param string $uri        The request URI
     * @param string $method     The HTTP method request
     * @param array  $parameters The request parameters
     * @param array  $files      An array of uploaded files
     * @param array  $cookies    An array of cookies
     * @param array  $server     An array of server parameters
     * @param string $content    The raw body data
     */
    public function __construct(string $uri, string $method, array $parameters = [], array $files = [], array $cookies = [], array $server = [], string $content = null)
    {
    }
    /**
     * Gets the request URI.
     *
     * @return string The request URI
     */
    public function getUri()
    {
    }
    /**
     * Gets the request HTTP method.
     *
     * @return string The request HTTP method
     */
    public function getMethod()
    {
    }
    /**
     * Gets the request parameters.
     *
     * @return array The request parameters
     */
    public function getParameters()
    {
    }
    /**
     * Gets the request server files.
     *
     * @return array The request files
     */
    public function getFiles()
    {
    }
    /**
     * Gets the request cookies.
     *
     * @return array The request cookies
     */
    public function getCookies()
    {
    }
    /**
     * Gets the request server parameters.
     *
     * @return array The request server parameters
     */
    public function getServer()
    {
    }
    /**
     * Gets the request raw body data.
     *
     * @return string|null The request raw body data
     */
    public function getContent()
    {
    }
}
