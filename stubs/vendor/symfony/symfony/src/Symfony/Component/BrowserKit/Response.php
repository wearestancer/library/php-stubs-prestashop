<?php

namespace Symfony\Component\BrowserKit;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class Response
{
    /** @internal */
    protected $content;
    /** @internal */
    protected $status;
    /** @internal */
    protected $headers;
    /**
     * The headers array is a set of key/value pairs. If a header is present multiple times
     * then the value is an array of all the values.
     *
     * @param string $content The content of the response
     * @param int    $status  The response status code
     * @param array  $headers An array of headers
     */
    public function __construct(string $content = '', int $status = 200, array $headers = [])
    {
    }
    /**
     * Converts the response object to string containing all headers and the response content.
     *
     * @return string The response with headers and content
     */
    public function __toString()
    {
    }
    /**
     * Returns the build header line.
     *
     * @param string $name  The header name
     * @param string $value The header value
     *
     * @return string The built header line
     *
     * @deprecated since Symfony 4.3
     */
    protected function buildHeader($name, $value)
    {
    }
    /**
     * Gets the response content.
     *
     * @return string The response content
     */
    public function getContent()
    {
    }
    /**
     * Gets the response status code.
     *
     * @return int The response status code
     *
     * @deprecated since Symfony 4.3, use getStatusCode() instead
     */
    public function getStatus()
    {
    }
    public function getStatusCode() : int
    {
    }
    /**
     * Gets the response headers.
     *
     * @return array The response headers
     */
    public function getHeaders()
    {
    }
    /**
     * Gets a response header.
     *
     * @param string $header The header name
     * @param bool   $first  Whether to return the first value or all header values
     *
     * @return string|array|null The first header value if $first is true, an array of values otherwise
     */
    public function getHeader($header, $first = true)
    {
    }
}
