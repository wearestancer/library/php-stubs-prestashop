<?php

namespace Symfony\Component\BrowserKit\Test\Constraint;

final class BrowserCookieValueSame extends \PHPUnit\Framework\Constraint\Constraint
{
    public function __construct(string $name, string $value, bool $raw = false, string $path = '/', string $domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
    }
}
