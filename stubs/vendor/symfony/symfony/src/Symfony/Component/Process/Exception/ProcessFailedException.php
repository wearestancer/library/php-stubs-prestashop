<?php

namespace Symfony\Component\Process\Exception;

/**
 * Exception for failed processes.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ProcessFailedException extends \Symfony\Component\Process\Exception\RuntimeException
{
    public function __construct(\Symfony\Component\Process\Process $process)
    {
    }
    public function getProcess()
    {
    }
}
