<?php

namespace Symfony\Component\Process\Pipes;

/**
 * UnixPipes implementation uses unix pipes as handles.
 *
 * @author Romain Neutron <imprec@gmail.com>
 *
 * @internal
 */
class UnixPipes extends \Symfony\Component\Process\Pipes\AbstractPipes
{
    public function __construct(?bool $ttyMode, bool $ptyMode, $input, bool $haveReadSupport)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDescriptors() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFiles() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function readAndWrite(bool $blocking, bool $close = false) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function haveReadSupport() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function areOpen() : bool
    {
    }
}
