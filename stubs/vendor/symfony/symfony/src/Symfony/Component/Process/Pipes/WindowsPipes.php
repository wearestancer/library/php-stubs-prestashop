<?php

namespace Symfony\Component\Process\Pipes;

/**
 * WindowsPipes implementation uses temporary files as handles.
 *
 * @see https://bugs.php.net/51800
 * @see https://bugs.php.net/65650
 *
 * @author Romain Neutron <imprec@gmail.com>
 *
 * @internal
 */
class WindowsPipes extends \Symfony\Component\Process\Pipes\AbstractPipes
{
    public function __construct($input, bool $haveReadSupport)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDescriptors() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFiles() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function readAndWrite(bool $blocking, bool $close = false) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function haveReadSupport() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function areOpen() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
}
