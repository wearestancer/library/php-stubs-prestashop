<?php

namespace Symfony\Component\Process\Pipes;

/**
 * @author Romain Neutron <imprec@gmail.com>
 *
 * @internal
 */
abstract class AbstractPipes implements \Symfony\Component\Process\Pipes\PipesInterface
{
    public $pipes = [];
    /**
     * @param resource|string|int|float|bool|\Iterator|null $input
     */
    public function __construct($input)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }
    /**
     * Returns true if a system call has been interrupted.
     */
    protected function hasSystemCallBeenInterrupted() : bool
    {
    }
    /**
     * Unblocks streams.
     */
    protected function unblock()
    {
    }
    /**
     * Writes input to stdin.
     *
     * @throws InvalidArgumentException When an input iterator yields a non supported value
     */
    protected function write() : ?array
    {
    }
    /**
     * @internal
     */
    public function handleError(int $type, string $msg)
    {
    }
}
