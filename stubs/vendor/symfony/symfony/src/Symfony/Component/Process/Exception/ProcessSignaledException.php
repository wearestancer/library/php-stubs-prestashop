<?php

namespace Symfony\Component\Process\Exception;

/**
 * Exception that is thrown when a process has been signaled.
 *
 * @author Sullivan Senechal <soullivaneuh@gmail.com>
 */
final class ProcessSignaledException extends \Symfony\Component\Process\Exception\RuntimeException
{
    public function __construct(\Symfony\Component\Process\Process $process)
    {
    }
    public function getProcess() : \Symfony\Component\Process\Process
    {
    }
    public function getSignal() : int
    {
    }
}
