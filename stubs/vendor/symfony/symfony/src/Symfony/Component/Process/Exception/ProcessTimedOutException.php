<?php

namespace Symfony\Component\Process\Exception;

/**
 * Exception that is thrown when a process times out.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ProcessTimedOutException extends \Symfony\Component\Process\Exception\RuntimeException
{
    public const TYPE_GENERAL = 1;
    public const TYPE_IDLE = 2;
    public function __construct(\Symfony\Component\Process\Process $process, int $timeoutType)
    {
    }
    public function getProcess()
    {
    }
    public function isGeneralTimeout()
    {
    }
    public function isIdleTimeout()
    {
    }
    public function getExceededTimeout()
    {
    }
}
