<?php

namespace Symfony\Component\HttpClient\Chunk;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class FirstChunk extends \Symfony\Component\HttpClient\Chunk\DataChunk
{
    /**
     * {@inheritdoc}
     */
    public function isFirst() : bool
    {
    }
}
