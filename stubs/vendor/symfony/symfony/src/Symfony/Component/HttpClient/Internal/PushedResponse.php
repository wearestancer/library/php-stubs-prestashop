<?php

namespace Symfony\Component\HttpClient\Internal;

/**
 * A pushed response with its request headers.
 *
 * @author Alexander M. Turek <me@derrabus.de>
 *
 * @internal
 */
final class PushedResponse
{
    public $response;
    /** @var string[] */
    public $requestHeaders;
    public $parentOptions = [];
    public $handle;
    public function __construct(\Symfony\Component\HttpClient\Response\CurlResponse $response, array $requestHeaders, array $parentOptions, $handle)
    {
    }
}
