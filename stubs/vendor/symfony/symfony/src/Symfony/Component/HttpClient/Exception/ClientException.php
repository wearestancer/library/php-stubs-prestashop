<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * Represents a 4xx response.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class ClientException extends \RuntimeException implements \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
{
    use \Symfony\Component\HttpClient\Exception\HttpExceptionTrait;
}
