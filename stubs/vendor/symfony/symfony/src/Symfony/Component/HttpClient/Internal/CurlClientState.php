<?php

namespace Symfony\Component\HttpClient\Internal;

/**
 * Internal representation of the cURL client's state.
 *
 * @author Alexander M. Turek <me@derrabus.de>
 *
 * @internal
 */
final class CurlClientState extends \Symfony\Component\HttpClient\Internal\ClientState
{
    /** @var \CurlMultiHandle|resource|null */
    public $handle;
    /** @var \CurlShareHandle|resource|null */
    public $share;
    /** @var PushedResponse[] */
    public $pushedResponses = [];
    /** @var DnsCache */
    public $dnsCache;
    /** @var LoggerInterface|null */
    public $logger;
    public static $curlVersion;
    public function __construct(int $maxHostConnections, int $maxPendingPushes)
    {
    }
    public function reset()
    {
    }
}
