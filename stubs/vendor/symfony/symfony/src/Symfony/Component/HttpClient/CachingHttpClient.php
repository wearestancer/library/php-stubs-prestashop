<?php

namespace Symfony\Component\HttpClient;

/**
 * Adds caching on top of an HTTP client.
 *
 * The implementation buffers responses in memory and doesn't stream directly from the network.
 * You can disable/enable this layer by setting option "no_cache" under "extra" to true/false.
 * By default, caching is enabled unless the "buffer" option is set to false.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class CachingHttpClient implements \Symfony\Contracts\HttpClient\HttpClientInterface
{
    use \Symfony\Component\HttpClient\HttpClientTrait;
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client, \Symfony\Component\HttpKernel\HttpCache\StoreInterface $store, array $defaultOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null) : \Symfony\Contracts\HttpClient\ResponseStreamInterface
    {
    }
}
