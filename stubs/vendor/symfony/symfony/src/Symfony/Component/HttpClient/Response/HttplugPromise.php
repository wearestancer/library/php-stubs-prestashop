<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 *
 * @internal
 */
final class HttplugPromise implements \Http\Promise\Promise
{
    public function __construct(\GuzzleHttp\Promise\PromiseInterface $promise)
    {
    }
    public function then(callable $onFulfilled = null, callable $onRejected = null) : self
    {
    }
    public function cancel() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getState() : string
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return Psr7ResponseInterface|mixed
     */
    public function wait($unwrap = true)
    {
    }
}
