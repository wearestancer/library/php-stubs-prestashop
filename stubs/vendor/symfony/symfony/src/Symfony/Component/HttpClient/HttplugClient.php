<?php

namespace Symfony\Component\HttpClient;

/**
 * An adapter to turn a Symfony HttpClientInterface into an Httplug client.
 *
 * Run "composer require nyholm/psr7" to install an efficient implementation of response
 * and stream factories with flex-provided autowiring aliases.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class HttplugClient implements \Http\Client\HttpClient, \Http\Client\HttpAsyncClient, \Http\Message\RequestFactory, \Http\Message\StreamFactory, \Http\Message\UriFactory
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Psr\Http\Message\ResponseFactoryInterface $responseFactory = null, \Psr\Http\Message\StreamFactoryInterface $streamFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function sendRequest(\Psr\Http\Message\RequestInterface $request) : \Psr\Http\Message\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return HttplugPromise
     */
    public function sendAsyncRequest(\Psr\Http\Message\RequestInterface $request) : \Http\Promise\Promise
    {
    }
    /**
     * Resolves pending promises that complete before the timeouts are reached.
     *
     * When $maxDuration is null and $idleTimeout is reached, promises are rejected.
     *
     * @return int The number of remaining pending promises
     */
    public function wait(float $maxDuration = null, float $idleTimeout = null) : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createRequest($method, $uri, array $headers = [], $body = null, $protocolVersion = '1.1') : \Psr\Http\Message\RequestInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createStream($body = null) : \Psr\Http\Message\StreamInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createUri($uri) : \Psr\Http\Message\UriInterface
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
