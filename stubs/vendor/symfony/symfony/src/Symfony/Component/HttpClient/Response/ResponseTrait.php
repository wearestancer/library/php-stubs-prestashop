<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * Implements the common logic for response classes.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
trait ResponseTrait
{
    private $logger;
    private $headers = [];
    private $canary;
    /**
     * @var callable|null A callback that initializes the two previous properties
     */
    private $initializer;
    private $info = ['response_headers' => [], 'http_code' => 0, 'error' => null, 'canceled' => false];
    /** @var object|resource */
    private $handle;
    private $id;
    private $timeout = 0;
    private $inflate;
    private $shouldBuffer;
    private $content;
    private $finalInfo;
    private $offset = 0;
    private $jsonData;
    /**
     * {@inheritdoc}
     */
    public function getStatusCode() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getHeaders(bool $throw = true) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContent(bool $throw = true) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function toArray(bool $throw = true) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function cancel() : void
    {
    }
    /**
     * Casts the response to a PHP stream resource.
     *
     * @return resource
     *
     * @throws TransportExceptionInterface   When a network error occurs
     * @throws RedirectionExceptionInterface On a 3xx when $throw is true and the "max_redirects" option has been reached
     * @throws ClientExceptionInterface      On a 4xx when $throw is true
     * @throws ServerExceptionInterface      On a 5xx when $throw is true
     */
    public function toStream(bool $throw = true)
    {
    }
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    /**
     * Closes the response and all its network handles.
     */
    private function close() : void
    {
    }
    /**
     * Adds pending responses to the activity list.
     */
    protected static abstract function schedule(self $response, array &$runningResponses) : void;
    /**
     * Performs all pending non-blocking operations.
     */
    protected static abstract function perform(\Symfony\Component\HttpClient\Internal\ClientState $multi, array &$responses) : void;
    /**
     * Waits for network activity.
     */
    protected static abstract function select(\Symfony\Component\HttpClient\Internal\ClientState $multi, float $timeout) : int;
    private static function initialize(self $response) : void
    {
    }
    private static function addResponseHeaders(array $responseHeaders, array &$info, array &$headers, string &$debug = '') : void
    {
    }
    private function checkStatusCode()
    {
    }
    /**
     * Ensures the request is always sent and that the response code was checked.
     */
    private function doDestruct()
    {
    }
    /**
     * Implements an event loop based on a buffer activity queue.
     *
     * @internal
     */
    public static function stream(iterable $responses, float $timeout = null) : \Generator
    {
    }
}
