<?php

namespace Symfony\Component\HttpClient\Internal;

/**
 * Cache for resolved DNS queries.
 *
 * @author Alexander M. Turek <me@derrabus.de>
 *
 * @internal
 */
final class DnsCache
{
    /**
     * Resolved hostnames (hostname => IP address).
     *
     * @var string[]
     */
    public $hostnames = [];
    /**
     * @var string[]
     */
    public $removals = [];
    /**
     * @var string[]
     */
    public $evictions = [];
}
