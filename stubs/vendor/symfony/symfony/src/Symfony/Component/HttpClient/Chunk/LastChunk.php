<?php

namespace Symfony\Component\HttpClient\Chunk;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class LastChunk extends \Symfony\Component\HttpClient\Chunk\DataChunk
{
    /**
     * {@inheritdoc}
     */
    public function isLast() : bool
    {
    }
}
