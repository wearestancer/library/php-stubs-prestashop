<?php

namespace Symfony\Component\HttpClient;

/**
 * A test-friendly HttpClient that doesn't make actual HTTP requests.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class MockHttpClient implements \Symfony\Contracts\HttpClient\HttpClientInterface
{
    use \Symfony\Component\HttpClient\HttpClientTrait;
    /**
     * @param callable|ResponseInterface|ResponseInterface[]|iterable|null $responseFactory
     */
    public function __construct($responseFactory = null, string $baseUri = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null) : \Symfony\Contracts\HttpClient\ResponseStreamInterface
    {
    }
}
