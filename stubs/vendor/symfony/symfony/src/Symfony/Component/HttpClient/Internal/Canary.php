<?php

namespace Symfony\Component\HttpClient\Internal;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class Canary
{
    public function __construct(\Closure $canceller)
    {
    }
    public function cancel()
    {
    }
    public function __destruct()
    {
    }
}
