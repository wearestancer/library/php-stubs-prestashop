<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class ResponseStream implements \Symfony\Contracts\HttpClient\ResponseStreamInterface
{
    public function __construct(\Generator $generator)
    {
    }
    public function key() : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    public function current() : \Symfony\Contracts\HttpClient\ChunkInterface
    {
    }
    public function next() : void
    {
    }
    public function rewind() : void
    {
    }
    public function valid() : bool
    {
    }
}
