<?php

namespace Symfony\Component\HttpClient\DependencyInjection;

final class HttpClientPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $clientTag = 'http_client.client')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
