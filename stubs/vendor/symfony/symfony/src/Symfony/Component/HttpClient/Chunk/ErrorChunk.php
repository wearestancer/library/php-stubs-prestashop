<?php

namespace Symfony\Component\HttpClient\Chunk;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class ErrorChunk implements \Symfony\Contracts\HttpClient\ChunkInterface
{
    /**
     * @param \Throwable|string $error
     */
    public function __construct(int $offset, $error)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isTimeout() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFirst() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isLast() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInformationalStatus() : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContent() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOffset() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getError() : ?string
    {
    }
    /**
     * @return bool Whether the wrapped error has been thrown or not
     */
    public function didThrow() : bool
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
    public function __destruct()
    {
    }
}
