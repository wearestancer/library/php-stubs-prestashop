<?php

namespace Symfony\Component\HttpClient;

/**
 * @author Jérémy Romey <jeremy@free-agent.fr>
 */
final class TraceableHttpClient implements \Symfony\Contracts\HttpClient\HttpClientInterface, \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null) : \Symfony\Contracts\HttpClient\ResponseStreamInterface
    {
    }
    public function getTracedRequests() : array
    {
    }
    public function reset()
    {
    }
}
