<?php

namespace Symfony\Component\HttpClient\DataCollector;

/**
 * @author Jérémy Romey <jeremy@free-agent.fr>
 */
final class HttpClientDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function registerClient(string $name, \Symfony\Component\HttpClient\TraceableHttpClient $client)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function lateCollect()
    {
    }
    public function getClients() : array
    {
    }
    public function getRequestCount() : int
    {
    }
    public function getErrorCount() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName() : string
    {
    }
    public function reset()
    {
    }
}
