<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * A test-friendly response.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class MockResponse implements \Symfony\Contracts\HttpClient\ResponseInterface
{
    use \Symfony\Component\HttpClient\Response\ResponseTrait {
        doDestruct as public __destruct;
    }
    /**
     * @param string|string[]|iterable $body The response body as a string or an iterable of strings,
     *                                       yielding an empty string simulates an idle timeout,
     *                                       throwing an exception yields an ErrorChunk
     *
     * @see ResponseInterface::getInfo() for possible info, e.g. "response_headers"
     */
    public function __construct($body = '', array $info = [])
    {
    }
    /**
     * Returns the options used when doing the request.
     */
    public function getRequestOptions() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInfo(string $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function cancel() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function close() : void
    {
    }
    /**
     * @internal
     */
    public static function fromRequest(string $method, string $url, array $options, \Symfony\Contracts\HttpClient\ResponseInterface $mock) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    protected static function schedule(self $response, array &$runningResponses) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected static function perform(\Symfony\Component\HttpClient\Internal\ClientState $multi, array &$responses) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected static function select(\Symfony\Component\HttpClient\Internal\ClientState $multi, float $timeout) : int
    {
    }
}
