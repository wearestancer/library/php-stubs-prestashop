<?php

namespace Symfony\Component\HttpClient;

/**
 * A helper providing autocompletion for available options.
 *
 * @see HttpClientInterface for a description of each options.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class HttpOptions
{
    public function toArray() : array
    {
    }
    /**
     * @return $this
     */
    public function setAuthBasic(string $user, string $password = '')
    {
    }
    /**
     * @return $this
     */
    public function setAuthBearer(string $token)
    {
    }
    /**
     * @return $this
     */
    public function setQuery(array $query)
    {
    }
    /**
     * @return $this
     */
    public function setHeaders(iterable $headers)
    {
    }
    /**
     * @param array|string|resource|\Traversable|\Closure $body
     *
     * @return $this
     */
    public function setBody($body)
    {
    }
    /**
     * @param mixed $json
     *
     * @return $this
     */
    public function setJson($json)
    {
    }
    /**
     * @return $this
     */
    public function setUserData($data)
    {
    }
    /**
     * @return $this
     */
    public function setMaxRedirects(int $max)
    {
    }
    /**
     * @return $this
     */
    public function setHttpVersion(string $version)
    {
    }
    /**
     * @return $this
     */
    public function setBaseUri(string $uri)
    {
    }
    /**
     * @return $this
     */
    public function buffer(bool $buffer)
    {
    }
    /**
     * @return $this
     */
    public function setOnProgress(callable $callback)
    {
    }
    /**
     * @return $this
     */
    public function resolve(array $hostIps)
    {
    }
    /**
     * @return $this
     */
    public function setProxy(string $proxy)
    {
    }
    /**
     * @return $this
     */
    public function setNoProxy(string $noProxy)
    {
    }
    /**
     * @return $this
     */
    public function setTimeout(float $timeout)
    {
    }
    /**
     * @return $this
     */
    public function setMaxDuration(float $maxDuration)
    {
    }
    /**
     * @return $this
     */
    public function bindTo(string $bindto)
    {
    }
    /**
     * @return $this
     */
    public function verifyPeer(bool $verify)
    {
    }
    /**
     * @return $this
     */
    public function verifyHost(bool $verify)
    {
    }
    /**
     * @return $this
     */
    public function setCaFile(string $cafile)
    {
    }
    /**
     * @return $this
     */
    public function setCaPath(string $capath)
    {
    }
    /**
     * @return $this
     */
    public function setLocalCert(string $cert)
    {
    }
    /**
     * @return $this
     */
    public function setLocalPk(string $pk)
    {
    }
    /**
     * @return $this
     */
    public function setPassphrase(string $passphrase)
    {
    }
    /**
     * @return $this
     */
    public function setCiphers(string $ciphers)
    {
    }
    /**
     * @param string|array $fingerprint
     *
     * @return $this
     */
    public function setPeerFingerprint($fingerprint)
    {
    }
    /**
     * @return $this
     */
    public function capturePeerCertChain(bool $capture)
    {
    }
    /**
     * @return $this
     */
    public function setExtra(string $name, $value)
    {
    }
}
