<?php

namespace Symfony\Component\HttpClient\Chunk;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class DataChunk implements \Symfony\Contracts\HttpClient\ChunkInterface
{
    public function __construct(int $offset = 0, string $content = '')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isTimeout() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFirst() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isLast() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInformationalStatus() : ?array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContent() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOffset() : int
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getError() : ?string
    {
    }
}
