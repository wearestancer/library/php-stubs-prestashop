<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * Represents a 5xx response.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class ServerException extends \RuntimeException implements \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
{
    use \Symfony\Component\HttpClient\Exception\HttpExceptionTrait;
}
