<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * Thrown by responses' toArray() method when their content cannot be JSON-decoded.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class JsonException extends \JsonException implements \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
{
}
