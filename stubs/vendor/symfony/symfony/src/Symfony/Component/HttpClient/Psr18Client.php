<?php

namespace Symfony\Component\HttpClient;

/**
 * An adapter to turn a Symfony HttpClientInterface into a PSR-18 ClientInterface.
 *
 * Run "composer require psr/http-client" to install the base ClientInterface. Run
 * "composer require nyholm/psr7" to install an efficient implementation of response
 * and stream factories with flex-provided autowiring aliases.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class Psr18Client implements \Psr\Http\Client\ClientInterface, \Psr\Http\Message\RequestFactoryInterface, \Psr\Http\Message\StreamFactoryInterface, \Psr\Http\Message\UriFactoryInterface
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client = null, \Psr\Http\Message\ResponseFactoryInterface $responseFactory = null, \Psr\Http\Message\StreamFactoryInterface $streamFactory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function sendRequest(\Psr\Http\Message\RequestInterface $request) : \Psr\Http\Message\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createRequest(string $method, $uri) : \Psr\Http\Message\RequestInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createStream(string $content = '') : \Psr\Http\Message\StreamInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createStreamFromFile(string $filename, string $mode = 'r') : \Psr\Http\Message\StreamInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createStreamFromResource($resource) : \Psr\Http\Message\StreamInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createUri(string $uri = '') : \Psr\Http\Message\UriInterface
    {
    }
}
/**
 * @internal
 */
class Psr18NetworkException extends \RuntimeException implements \Psr\Http\Client\NetworkExceptionInterface
{
    public function __construct(\Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface $e, \Psr\Http\Message\RequestInterface $request)
    {
    }
    public function getRequest() : \Psr\Http\Message\RequestInterface
    {
    }
}
/**
 * @internal
 */
class Psr18RequestException extends \InvalidArgumentException implements \Psr\Http\Client\RequestExceptionInterface
{
    public function __construct(\Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface $e, \Psr\Http\Message\RequestInterface $request)
    {
    }
    public function getRequest() : \Psr\Http\Message\RequestInterface
    {
    }
}
