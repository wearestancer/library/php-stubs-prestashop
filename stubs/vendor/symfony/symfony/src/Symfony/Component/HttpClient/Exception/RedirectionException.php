<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * Represents a 3xx response.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class RedirectionException extends \RuntimeException implements \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
{
    use \Symfony\Component\HttpClient\Exception\HttpExceptionTrait;
}
