<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class NativeResponse implements \Symfony\Contracts\HttpClient\ResponseInterface
{
    use \Symfony\Component\HttpClient\Response\ResponseTrait;
    /**
     * @internal
     */
    public function __construct(\Symfony\Component\HttpClient\Internal\NativeClientState $multi, $context, string $url, array $options, array &$info, callable $resolveRedirect, ?callable $onProgress, ?\Psr\Log\LoggerInterface $logger)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInfo(string $type = null)
    {
    }
    public function __destruct()
    {
    }
}
