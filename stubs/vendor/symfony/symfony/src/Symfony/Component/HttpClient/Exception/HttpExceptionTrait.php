<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
trait HttpExceptionTrait
{
    private $response;
    public function __construct(\Symfony\Contracts\HttpClient\ResponseInterface $response)
    {
    }
    public function getResponse() : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
}
