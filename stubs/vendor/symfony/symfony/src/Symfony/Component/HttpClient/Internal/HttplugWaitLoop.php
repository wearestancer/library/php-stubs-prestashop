<?php

namespace Symfony\Component\HttpClient\Internal;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class HttplugWaitLoop
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client, ?\SplObjectStorage $promisePool, \Psr\Http\Message\ResponseFactoryInterface $responseFactory, \Psr\Http\Message\StreamFactoryInterface $streamFactory)
    {
    }
    public function wait(?\Symfony\Contracts\HttpClient\ResponseInterface $pendingResponse, float $maxDuration = null, float $idleTimeout = null) : int
    {
    }
    public function createPsr7Response(\Symfony\Contracts\HttpClient\ResponseInterface $response, bool $buffer = false) : \Psr\Http\Message\ResponseInterface
    {
    }
}
