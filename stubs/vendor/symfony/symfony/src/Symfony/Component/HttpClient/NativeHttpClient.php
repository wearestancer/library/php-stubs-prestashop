<?php

namespace Symfony\Component\HttpClient;

/**
 * A portable implementation of the HttpClientInterface contracts based on PHP stream wrappers.
 *
 * PHP stream wrappers are able to fetch response bodies concurrently,
 * but each request is opened synchronously.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class NativeHttpClient implements \Symfony\Contracts\HttpClient\HttpClientInterface, \Psr\Log\LoggerAwareInterface
{
    use \Symfony\Component\HttpClient\HttpClientTrait;
    use \Psr\Log\LoggerAwareTrait;
    /**
     * @param array $defaultOptions     Default request's options
     * @param int   $maxHostConnections The maximum number of connections to open
     *
     * @see HttpClientInterface::OPTIONS_DEFAULTS for available options
     */
    public function __construct(array $defaultOptions = [], int $maxHostConnections = 6)
    {
    }
    /**
     * @see HttpClientInterface::OPTIONS_DEFAULTS for available options
     *
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null) : \Symfony\Contracts\HttpClient\ResponseStreamInterface
    {
    }
}
