<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class TransportException extends \RuntimeException implements \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
{
}
