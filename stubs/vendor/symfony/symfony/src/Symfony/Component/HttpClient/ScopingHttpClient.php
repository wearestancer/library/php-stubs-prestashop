<?php

namespace Symfony\Component\HttpClient;

/**
 * Auto-configure the default options based on the requested URL.
 *
 * @author Anthony Martin <anthony.martin@sensiolabs.com>
 */
class ScopingHttpClient implements \Symfony\Contracts\HttpClient\HttpClientInterface, \Symfony\Contracts\Service\ResetInterface
{
    use \Symfony\Component\HttpClient\HttpClientTrait;
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client, array $defaultOptionsByRegexp, string $defaultRegexp = null)
    {
    }
    public static function forBaseUri(\Symfony\Contracts\HttpClient\HttpClientInterface $client, string $baseUri, array $defaultOptions = [], $regexp = null) : self
    {
    }
    /**
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []) : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null) : \Symfony\Contracts\HttpClient\ResponseStreamInterface
    {
    }
    public function reset()
    {
    }
}
