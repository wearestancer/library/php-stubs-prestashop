<?php

namespace Symfony\Component\HttpClient\Internal;

/**
 * Internal representation of the native client's state.
 *
 * @author Alexander M. Turek <me@derrabus.de>
 *
 * @internal
 */
final class NativeClientState extends \Symfony\Component\HttpClient\Internal\ClientState
{
    /** @var int */
    public $id;
    /** @var int */
    public $maxHostConnections = \PHP_INT_MAX;
    /** @var int */
    public $responseCount = 0;
    /** @var string[] */
    public $dnsCache = [];
    /** @var bool */
    public $sleep = false;
    public function __construct()
    {
    }
}
