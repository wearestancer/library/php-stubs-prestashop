<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * Allows turning ResponseInterface instances to PHP streams.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class StreamWrapper
{
    /** @var resource|null */
    public $context;
    /**
     * Creates a PHP stream resource from a ResponseInterface.
     *
     * @return resource
     */
    public static function createResource(\Symfony\Contracts\HttpClient\ResponseInterface $response, \Symfony\Contracts\HttpClient\HttpClientInterface $client = null)
    {
    }
    public function getResponse() : \Symfony\Contracts\HttpClient\ResponseInterface
    {
    }
    /**
     * @param resource|null $handle  The resource handle that should be monitored when
     *                               stream_select() is used on the created stream
     * @param resource|null $content The seekable resource where the response body is buffered
     */
    public function bindHandles(&$handle, &$content) : void
    {
    }
    public function stream_open(string $path, string $mode, int $options) : bool
    {
    }
    public function stream_read(int $count)
    {
    }
    public function stream_set_option(int $option, int $arg1, ?int $arg2) : bool
    {
    }
    public function stream_tell() : int
    {
    }
    public function stream_eof() : bool
    {
    }
    public function stream_seek(int $offset, int $whence = \SEEK_SET) : bool
    {
    }
    public function stream_cast(int $castAs)
    {
    }
    public function stream_stat() : array
    {
    }
}
