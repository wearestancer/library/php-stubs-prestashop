<?php

namespace Symfony\Component\HttpClient\Exception;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
{
}
