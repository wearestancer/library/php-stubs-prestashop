<?php

namespace Symfony\Component\HttpClient\Chunk;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class InformationalChunk extends \Symfony\Component\HttpClient\Chunk\DataChunk
{
    public function __construct(int $statusCode, array $headers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInformationalStatus() : ?array
    {
    }
}
