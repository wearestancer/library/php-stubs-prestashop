<?php

namespace Symfony\Component\HttpClient\Response;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class CurlResponse implements \Symfony\Contracts\HttpClient\ResponseInterface
{
    use \Symfony\Component\HttpClient\Response\ResponseTrait {
        getContent as private doGetContent;
    }
    /**
     * @param \CurlHandle|resource|string $ch
     *
     * @internal
     */
    public function __construct(\Symfony\Component\HttpClient\Internal\CurlClientState $multi, $ch, array $options = null, \Psr\Log\LoggerInterface $logger = null, string $method = 'GET', callable $resolveRedirect = null, int $curlVersion = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInfo(string $type = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContent(bool $throw = true) : string
    {
    }
    public function __destruct()
    {
    }
}
