<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Sets the session in the request.
 *
 * When the passed container contains a "session_storage" entry which
 * holds a NativeSessionStorage instance, the "cookie_secure" option
 * will be set to true whenever the current master request is secure.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class SessionListener extends \Symfony\Component\HttpKernel\EventListener\AbstractSessionListener
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    protected function getSession() : ?\Symfony\Component\HttpFoundation\Session\SessionInterface
    {
    }
}
