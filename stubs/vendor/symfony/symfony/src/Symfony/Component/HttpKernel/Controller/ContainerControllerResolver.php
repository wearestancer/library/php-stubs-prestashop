<?php

namespace Symfony\Component\HttpKernel\Controller;

/**
 * A controller resolver searching for a controller in a psr-11 container when using the "service:method" notation.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class ContainerControllerResolver extends \Symfony\Component\HttpKernel\Controller\ControllerResolver
{
    protected $container;
    public function __construct(\Psr\Container\ContainerInterface $container, \Psr\Log\LoggerInterface $logger = null)
    {
    }
    protected function createController($controller)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function instantiateController($class)
    {
    }
}
