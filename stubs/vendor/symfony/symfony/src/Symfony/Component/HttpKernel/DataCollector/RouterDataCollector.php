<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RouterDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector
{
    /**
     * @var \SplObjectStorage
     */
    protected $controllers;
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     *
     * @final since Symfony 4.4
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function reset()
    {
    }
    protected function guessRoute(\Symfony\Component\HttpFoundation\Request $request, $controller)
    {
    }
    /**
     * Remembers the controller associated to each request.
     *
     * @final since Symfony 4.3
     */
    public function onKernelController(\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event)
    {
    }
    /**
     * @return bool Whether this request will result in a redirect
     */
    public function getRedirect()
    {
    }
    /**
     * @return string|null The target URL
     */
    public function getTargetUrl()
    {
    }
    /**
     * @return string|null The target route
     */
    public function getTargetRoute()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
