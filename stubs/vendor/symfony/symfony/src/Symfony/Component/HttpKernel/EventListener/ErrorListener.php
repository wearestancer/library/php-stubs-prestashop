<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ErrorListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    protected $controller;
    protected $logger;
    protected $debug;
    public function __construct($controller, \Psr\Log\LoggerInterface $logger = null, $debug = false)
    {
    }
    public function logKernelException(\Symfony\Component\HttpKernel\Event\ExceptionEvent $event)
    {
    }
    public function onKernelException(\Symfony\Component\HttpKernel\Event\ExceptionEvent $event, string $eventName = null, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher = null)
    {
    }
    public function onControllerArguments(\Symfony\Component\HttpKernel\Event\ControllerArgumentsEvent $event)
    {
    }
    public static function getSubscribedEvents() : array
    {
    }
    /**
     * Logs an exception.
     */
    protected function logException(\Throwable $exception, string $message) : void
    {
    }
    /**
     * Clones the request for the exception.
     */
    protected function duplicateRequest(\Throwable $exception, \Symfony\Component\HttpFoundation\Request $request) : \Symfony\Component\HttpFoundation\Request
    {
    }
}
