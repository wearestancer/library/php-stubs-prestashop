<?php

namespace Symfony\Component\HttpKernel\CacheClearer;

/**
 * ChainCacheClearer.
 *
 * @author Dustin Dobervich <ddobervich@gmail.com>
 *
 * @final
 */
class ChainCacheClearer implements \Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface
{
    public function __construct(iterable $clearers = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear($cacheDir)
    {
    }
}
