<?php

namespace Symfony\Component\HttpKernel\Profiler;

/**
 * Storage for profiler using files.
 *
 * @author Alexandre Salomé <alexandre.salome@gmail.com>
 */
class FileProfilerStorage implements \Symfony\Component\HttpKernel\Profiler\ProfilerStorageInterface
{
    /**
     * Constructs the file storage using a "dsn-like" path.
     *
     * Example : "file:/path/to/the/storage/folder"
     *
     * @throws \RuntimeException
     */
    public function __construct(string $dsn)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function find($ip, $url, $limit, $method, $start = null, $end = null, $statusCode = null) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function purge()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function read($token) : ?\Symfony\Component\HttpKernel\Profiler\Profile
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws \RuntimeException
     */
    public function write(\Symfony\Component\HttpKernel\Profiler\Profile $profile) : bool
    {
    }
    /**
     * Gets filename to store data, associated to the token.
     *
     * @param string $token
     *
     * @return string The profile filename
     */
    protected function getFilename($token)
    {
    }
    /**
     * Gets the index filename.
     *
     * @return string The index filename
     */
    protected function getIndexFilename()
    {
    }
    /**
     * Reads a line in the file, backward.
     *
     * This function automatically skips the empty lines and do not include the line return in result value.
     *
     * @param resource $file The file resource, with the pointer placed at the end of the line to read
     *
     * @return mixed A string representing the line or null if beginning of file is reached
     */
    protected function readLineFromFile($file)
    {
    }
    protected function createProfileFromData($token, $data, $parent = null)
    {
    }
}
