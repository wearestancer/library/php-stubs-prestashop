<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * @deprecated since Symfony 4.4, use ErrorListener instead
 */
class ExceptionListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    protected $controller;
    protected $logger;
    protected $debug;
    public function __construct($controller, \Psr\Log\LoggerInterface $logger = null, $debug = false)
    {
    }
    public function logKernelException(\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event)
    {
    }
    public function onKernelException(\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
    /**
     * Logs an exception.
     *
     * @param \Exception $exception The \Exception instance
     * @param string     $message   The error message to log
     */
    protected function logException(\Exception $exception, $message)
    {
    }
    /**
     * Clones the request for the exception.
     *
     * @return Request The cloned request
     */
    protected function duplicateRequest(\Exception $exception, \Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
