<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * @deprecated since Symfony 4.3, use RequestEvent instead
 */
class GetResponseEvent extends \Symfony\Component\HttpKernel\Event\KernelEvent
{
    /**
     * Returns the response object.
     *
     * @return Response|null
     */
    public function getResponse()
    {
    }
    /**
     * Sets a response and stops event propagation.
     */
    public function setResponse(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * Returns whether a response was set.
     *
     * @return bool Whether a response was set
     */
    public function hasResponse()
    {
    }
}
