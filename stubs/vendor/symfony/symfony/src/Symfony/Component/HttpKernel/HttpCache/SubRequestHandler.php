<?php

namespace Symfony\Component\HttpKernel\HttpCache;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class SubRequestHandler
{
    public static function handle(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, \Symfony\Component\HttpFoundation\Request $request, $type, $catch) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
