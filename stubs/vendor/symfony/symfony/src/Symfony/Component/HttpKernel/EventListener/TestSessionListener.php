<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Sets the session in the request.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class TestSessionListener extends \Symfony\Component\HttpKernel\EventListener\AbstractTestSessionListener
{
    public function __construct(\Psr\Container\ContainerInterface $container, array $sessionOptions = [])
    {
    }
    protected function getSession() : ?\Symfony\Component\HttpFoundation\Session\SessionInterface
    {
    }
}
