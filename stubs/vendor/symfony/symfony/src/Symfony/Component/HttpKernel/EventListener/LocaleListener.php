<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Initializes the locale based on the current request.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class LocaleListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack, string $defaultLocale = 'en', \Symfony\Component\Routing\RequestContextAwareInterface $router = null)
    {
    }
    public function setDefaultLocale(\Symfony\Component\HttpKernel\Event\KernelEvent $event)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
