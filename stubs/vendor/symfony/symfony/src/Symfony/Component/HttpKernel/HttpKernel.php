<?php

namespace Symfony\Component\HttpKernel;

/**
 * HttpKernel notifies events to convert a Request object to a Response one.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpKernel implements \Symfony\Component\HttpKernel\HttpKernelInterface, \Symfony\Component\HttpKernel\TerminableInterface
{
    protected $dispatcher;
    protected $resolver;
    protected $requestStack;
    public function __construct(\Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher, \Symfony\Component\HttpKernel\Controller\ControllerResolverInterface $resolver, \Symfony\Component\HttpFoundation\RequestStack $requestStack = null, \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argumentResolver = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\HttpFoundation\Request $request, $type = \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function terminate(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * @internal
     */
    public function terminateWithException(\Throwable $exception, \Symfony\Component\HttpFoundation\Request $request = null)
    {
    }
}
