<?php

namespace Symfony\Component\HttpKernel;

/**
 * The Kernel is the heart of the Symfony system.
 *
 * It manages an environment made of bundles.
 *
 * Environment names must always start with a letter and
 * they must only contain letters and numbers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Kernel implements \Symfony\Component\HttpKernel\KernelInterface, \Symfony\Component\HttpKernel\RebootableInterface, \Symfony\Component\HttpKernel\TerminableInterface
{
    /**
     * @var BundleInterface[]
     */
    protected $bundles = [];
    protected $container;
    /**
     * @deprecated since Symfony 4.2
     */
    protected $rootDir;
    protected $environment;
    protected $debug;
    protected $booted = false;
    /**
     * @deprecated since Symfony 4.2
     */
    protected $name;
    protected $startTime;
    public const VERSION = '4.4.51';
    public const VERSION_ID = 40451;
    public const MAJOR_VERSION = 4;
    public const MINOR_VERSION = 4;
    public const RELEASE_VERSION = 51;
    public const EXTRA_VERSION = '';
    public const END_OF_MAINTENANCE = '11/2022';
    public const END_OF_LIFE = '11/2023';
    public function __construct(string $environment, bool $debug)
    {
    }
    public function __clone()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reboot($warmupDir)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function terminate(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function shutdown()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function handle(\Symfony\Component\HttpFoundation\Request $request, $type = \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
    }
    /**
     * Gets an HTTP kernel from the container.
     *
     * @return HttpKernelInterface
     */
    protected function getHttpKernel()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBundles()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBundle($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function locateResource($name)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2
     */
    public function getName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEnvironment()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isDebug()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @deprecated since Symfony 4.2, use getProjectDir() instead
     */
    public function getRootDir()
    {
    }
    /**
     * Gets the application root dir (path of the project's composer file).
     *
     * @return string The project root dir
     */
    public function getProjectDir()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContainer()
    {
    }
    /**
     * @internal
     */
    public function setAnnotatedClassCache(array $annotatedClasses)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getStartTime()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCacheDir()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLogDir()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getCharset()
    {
    }
    /**
     * Gets the patterns defining the classes to parse and cache for annotations.
     */
    public function getAnnotatedClassesToCompile() : array
    {
    }
    /**
     * Initializes bundles.
     *
     * @throws \LogicException if two bundles share a common name
     */
    protected function initializeBundles()
    {
    }
    /**
     * The extension point similar to the Bundle::build() method.
     *
     * Use this method to register compiler passes and manipulate the container during the building process.
     */
    protected function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Gets the container class.
     *
     * @throws \InvalidArgumentException If the generated classname is invalid
     *
     * @return string The container class
     */
    protected function getContainerClass()
    {
    }
    /**
     * Gets the container's base class.
     *
     * All names except Container must be fully qualified.
     *
     * @return string
     */
    protected function getContainerBaseClass()
    {
    }
    /**
     * Initializes the service container.
     *
     * The cached version of the service container is used when fresh, otherwise the
     * container is built.
     */
    protected function initializeContainer()
    {
    }
    /**
     * Returns the kernel parameters.
     *
     * @return array An array of kernel parameters
     */
    protected function getKernelParameters()
    {
    }
    /**
     * Builds the service container.
     *
     * @return ContainerBuilder The compiled service container
     *
     * @throws \RuntimeException
     */
    protected function buildContainer()
    {
    }
    /**
     * Prepares the ContainerBuilder before it is compiled.
     */
    protected function prepareContainer(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Gets a new ContainerBuilder instance used to build the service container.
     *
     * @return ContainerBuilder
     */
    protected function getContainerBuilder()
    {
    }
    /**
     * Dumps the service container to PHP code in the cache.
     *
     * @param string $class     The name of the class to generate
     * @param string $baseClass The name of the container's base class
     */
    protected function dumpContainer(\Symfony\Component\Config\ConfigCache $cache, \Symfony\Component\DependencyInjection\ContainerBuilder $container, $class, $baseClass)
    {
    }
    /**
     * Returns a loader for the container.
     *
     * @return DelegatingLoader The loader
     */
    protected function getContainerLoader(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
    }
    /**
     * Removes comments from a PHP source string.
     *
     * We don't use the PHP php_strip_whitespace() function
     * as we want the content to be readable and well-formatted.
     *
     * @param string $source A PHP string
     *
     * @return string The PHP string with the comments removed
     */
    public static function stripComments($source)
    {
    }
    /**
     * @deprecated since Symfony 4.3
     */
    public function serialize()
    {
    }
    /**
     * @deprecated since Symfony 4.3
     */
    public function unserialize($data)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
}
