<?php

namespace Symfony\Component\HttpKernel\Fragment;

/**
 * Implements Surrogate rendering strategy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractSurrogateFragmentRenderer extends \Symfony\Component\HttpKernel\Fragment\RoutableFragmentRenderer
{
    /**
     * The "fallback" strategy when surrogate is not available should always be an
     * instance of InlineFragmentRenderer.
     *
     * @param FragmentRendererInterface $inlineStrategy The inline strategy to use when the surrogate is not supported
     */
    public function __construct(\Symfony\Component\HttpKernel\HttpCache\SurrogateInterface $surrogate = null, \Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface $inlineStrategy, \Symfony\Component\HttpKernel\UriSigner $signer = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * Note that if the current Request has no surrogate capability, this method
     * falls back to use the inline rendering strategy.
     *
     * Additional available options:
     *
     *  * alt: an alternative URI to render in case of an error
     *  * comment: a comment to add when returning the surrogate tag
     *
     * Note, that not all surrogate strategies support all options. For now
     * 'alt' and 'comment' are only supported by ESI.
     *
     * @see Symfony\Component\HttpKernel\HttpCache\SurrogateInterface
     */
    public function render($uri, \Symfony\Component\HttpFoundation\Request $request, array $options = [])
    {
    }
}
