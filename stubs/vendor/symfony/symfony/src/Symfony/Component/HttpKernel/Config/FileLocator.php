<?php

namespace Symfony\Component\HttpKernel\Config;

/**
 * FileLocator uses the KernelInterface to locate resources in bundles.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FileLocator extends \Symfony\Component\Config\FileLocator
{
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function locate($file, $currentPath = null, $first = true)
    {
    }
}
