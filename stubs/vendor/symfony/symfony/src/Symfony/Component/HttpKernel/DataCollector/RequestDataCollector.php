<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class RequestDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\EventDispatcher\EventSubscriberInterface, \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    protected $controllers;
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function lateCollect()
    {
    }
    public function reset()
    {
    }
    public function getMethod()
    {
    }
    public function getPathInfo()
    {
    }
    public function getRequestRequest()
    {
    }
    public function getRequestQuery()
    {
    }
    public function getRequestFiles()
    {
    }
    public function getRequestHeaders()
    {
    }
    public function getRequestServer($raw = false)
    {
    }
    public function getRequestCookies($raw = false)
    {
    }
    public function getRequestAttributes()
    {
    }
    public function getResponseHeaders()
    {
    }
    public function getResponseCookies()
    {
    }
    public function getSessionMetadata()
    {
    }
    public function getSessionAttributes()
    {
    }
    public function getFlashes()
    {
    }
    public function getContent()
    {
    }
    public function isJsonRequest()
    {
    }
    public function getPrettyJson()
    {
    }
    public function getContentType()
    {
    }
    public function getStatusText()
    {
    }
    public function getStatusCode()
    {
    }
    public function getFormat()
    {
    }
    public function getLocale()
    {
    }
    public function getDotenvVars()
    {
    }
    /**
     * Gets the route name.
     *
     * The _route request attributes is automatically set by the Router Matcher.
     *
     * @return string The route
     */
    public function getRoute()
    {
    }
    public function getIdentifier()
    {
    }
    /**
     * Gets the route parameters.
     *
     * The _route_params request attributes is automatically set by the RouterListener.
     *
     * @return array The parameters
     */
    public function getRouteParams()
    {
    }
    /**
     * Gets the parsed controller.
     *
     * @return array|string The controller as a string or array of data
     *                      with keys 'class', 'method', 'file' and 'line'
     */
    public function getController()
    {
    }
    /**
     * Gets the previous request attributes.
     *
     * @return array|bool A legacy array of data from the previous redirection response
     *                    or false otherwise
     */
    public function getRedirect()
    {
    }
    public function getForwardToken()
    {
    }
    /**
     * @final since Symfony 4.3
     */
    public function onKernelController(\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event)
    {
    }
    /**
     * @final since Symfony 4.3
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * Parse a controller.
     *
     * @param string|object|array|null $controller The controller to parse
     *
     * @return array|string An array of controller data or a simple string
     */
    protected function parseController($controller)
    {
    }
}
