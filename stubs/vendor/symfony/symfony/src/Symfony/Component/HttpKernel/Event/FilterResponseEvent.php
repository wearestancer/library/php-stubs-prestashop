<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * @deprecated since Symfony 4.3, use ResponseEvent instead
 */
class FilterResponseEvent extends \Symfony\Component\HttpKernel\Event\KernelEvent
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, \Symfony\Component\HttpFoundation\Request $request, int $requestType, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * Returns the current response object.
     *
     * @return Response
     */
    public function getResponse()
    {
    }
    /**
     * Sets a new response object.
     */
    public function setResponse(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
}
