<?php

namespace Symfony\Component\HttpKernel\CacheWarmer;

/**
 * Aggregates several cache warmers into a single one.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class CacheWarmerAggregate implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface
{
    public function __construct(iterable $warmers = [], bool $debug = false, string $deprecationLogsFilepath = null)
    {
    }
    public function enableOptionalWarmers()
    {
    }
    public function enableOnlyOptionalWarmers()
    {
    }
    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * Checks whether this warmer is optional or not.
     *
     * @return bool always false
     */
    public function isOptional() : bool
    {
    }
}
