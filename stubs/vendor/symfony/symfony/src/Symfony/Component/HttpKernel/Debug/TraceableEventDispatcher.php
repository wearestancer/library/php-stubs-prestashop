<?php

namespace Symfony\Component\HttpKernel\Debug;

/**
 * Collects some data about event listeners.
 *
 * This event dispatcher delegates the dispatching to another one.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TraceableEventDispatcher extends \Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher
{
    /**
     * {@inheritdoc}
     */
    protected function beforeDispatch(string $eventName, $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function afterDispatch(string $eventName, $event)
    {
    }
}
