<?php

namespace Symfony\Component\HttpKernel\DependencyInjection;

/**
 * Resets provided services.
 *
 * @author Alexander M. Turek <me@derrabus.de>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class ServicesResetter implements \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(\Traversable $resettableServices, array $resetMethods)
    {
    }
    public function reset()
    {
    }
}
