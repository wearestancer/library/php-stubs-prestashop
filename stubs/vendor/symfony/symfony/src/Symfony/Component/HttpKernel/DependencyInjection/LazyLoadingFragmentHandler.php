<?php

namespace Symfony\Component\HttpKernel\DependencyInjection;

/**
 * Lazily loads fragment renderers from the dependency injection container.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class LazyLoadingFragmentHandler extends \Symfony\Component\HttpKernel\Fragment\FragmentHandler
{
    public function __construct(\Psr\Container\ContainerInterface $container, \Symfony\Component\HttpFoundation\RequestStack $requestStack, bool $debug = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render($uri, $renderer = 'inline', array $options = [])
    {
    }
}
