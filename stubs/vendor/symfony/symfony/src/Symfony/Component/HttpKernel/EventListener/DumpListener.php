<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Configures dump() handler.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class DumpListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\VarDumper\Cloner\ClonerInterface $cloner, \Symfony\Component\VarDumper\Dumper\DataDumperInterface $dumper, \Symfony\Component\VarDumper\Server\Connection $connection = null)
    {
    }
    public function configure()
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
