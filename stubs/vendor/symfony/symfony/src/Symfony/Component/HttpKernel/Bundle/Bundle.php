<?php

namespace Symfony\Component\HttpKernel\Bundle;

/**
 * An implementation of BundleInterface that adds a few conventions for DependencyInjection extensions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Bundle implements \Symfony\Component\HttpKernel\Bundle\BundleInterface
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;
    protected $name;
    protected $extension;
    protected $path;
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function shutdown()
    {
    }
    /**
     * {@inheritdoc}
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     */
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * Returns the bundle's container extension.
     *
     * @return ExtensionInterface|null The container extension
     *
     * @throws \LogicException
     */
    public function getContainerExtension()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNamespace()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
    }
    /**
     * Returns the bundle name (the class short name).
     */
    public final function getName() : string
    {
    }
    public function registerCommands(\Symfony\Component\Console\Application $application)
    {
    }
    /**
     * Returns the bundle's container extension class.
     *
     * @return string
     */
    protected function getContainerExtensionClass()
    {
    }
    /**
     * Creates the bundle's container extension.
     *
     * @return ExtensionInterface|null
     */
    protected function createContainerExtension()
    {
    }
}
