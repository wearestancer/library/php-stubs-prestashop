<?php

namespace Symfony\Component\HttpKernel\Controller\ArgumentResolver;

/**
 * Provides timing information via the stopwatch.
 *
 * @author Iltar van der Berg <kjarli@gmail.com>
 */
final class TraceableValueResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface
{
    public function __construct(\Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface $inner, \Symfony\Component\Stopwatch\Stopwatch $stopwatch)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : iterable
    {
    }
}
