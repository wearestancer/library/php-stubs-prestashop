<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * @deprecated since Symfony 4.3, use ControllerArgumentsEvent instead
 */
class FilterControllerArgumentsEvent extends \Symfony\Component\HttpKernel\Event\FilterControllerEvent
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, callable $controller, array $arguments, \Symfony\Component\HttpFoundation\Request $request, ?int $requestType)
    {
    }
    /**
     * @return array
     */
    public function getArguments()
    {
    }
    public function setArguments(array $arguments)
    {
    }
}
