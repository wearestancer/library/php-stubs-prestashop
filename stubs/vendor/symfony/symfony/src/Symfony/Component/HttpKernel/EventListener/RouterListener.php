<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Initializes the context from the request and sets request attributes based on a matching route.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 *
 * @final since Symfony 4.3
 */
class RouterListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param UrlMatcherInterface|RequestMatcherInterface $matcher    The Url or Request matcher
     * @param RequestContext|null                         $context    The RequestContext (can be null when $matcher implements RequestContextAwareInterface)
     * @param string                                      $projectDir
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($matcher, \Symfony\Component\HttpFoundation\RequestStack $requestStack, \Symfony\Component\Routing\RequestContext $context = null, \Psr\Log\LoggerInterface $logger = null, string $projectDir = null, bool $debug = true)
    {
    }
    /**
     * After a sub-request is done, we need to reset the routing context to the parent request so that the URL generator
     * operates on the correct context again.
     */
    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    public function onKernelException(\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
