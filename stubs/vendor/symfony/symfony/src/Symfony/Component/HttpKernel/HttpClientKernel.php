<?php

namespace Symfony\Component\HttpKernel;

/**
 * An implementation of a Symfony HTTP kernel using a "real" HTTP client.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class HttpClientKernel implements \Symfony\Component\HttpKernel\HttpKernelInterface
{
    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $client = null)
    {
    }
    public function handle(\Symfony\Component\HttpFoundation\Request $request, $type = \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST, $catch = true) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
