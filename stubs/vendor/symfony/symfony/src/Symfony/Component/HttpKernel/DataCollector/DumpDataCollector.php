<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @final since Symfony 4.3
 */
class DumpDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\VarDumper\Dumper\DataDumperInterface
{
    /**
     * @param string|FileLinkFormatter|null       $fileLinkFormat
     * @param DataDumperInterface|Connection|null $dumper
     */
    public function __construct(\Symfony\Component\Stopwatch\Stopwatch $stopwatch = null, $fileLinkFormat = null, string $charset = null, \Symfony\Component\HttpFoundation\RequestStack $requestStack = null, $dumper = null)
    {
    }
    public function __clone()
    {
    }
    public function dump(\Symfony\Component\VarDumper\Cloner\Data $data)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function reset()
    {
    }
    /**
     * @internal
     */
    public function __sleep() : array
    {
    }
    /**
     * @internal
     */
    public function __wakeup()
    {
    }
    public function getDumpsCount()
    {
    }
    public function getDumps($format, $maxDepthLimit = -1, $maxItemsPerDepth = -1)
    {
    }
    public function getName()
    {
    }
    public function __destruct()
    {
    }
}
