<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Pass the current locale to the provided services.
 *
 * @author Pierre Bobiet <pierrebobiet@gmail.com>
 */
class LocaleAwareListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param LocaleAwareInterface[] $localeAwareServices
     */
    public function __construct(iterable $localeAwareServices, \Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event) : void
    {
    }
    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event) : void
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
