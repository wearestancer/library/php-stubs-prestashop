<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * SurrogateListener adds a Surrogate-Control HTTP header when the Response needs to be parsed for Surrogates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class SurrogateListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\HttpKernel\HttpCache\SurrogateInterface $surrogate = null)
    {
    }
    /**
     * Filters the Response.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
