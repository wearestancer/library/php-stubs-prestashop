<?php

namespace Symfony\Component\HttpKernel;

/**
 * Client simulates a browser and makes requests to an HttpKernel instance.
 *
 * @method Request  getRequest()  A Request instance
 * @method Response getResponse() A Response instance
 *
 * @deprecated since Symfony 4.3, use HttpKernelBrowser instead.
 */
class Client extends \Symfony\Component\BrowserKit\AbstractBrowser
{
    protected $kernel;
    /**
     * @param array $server The server parameters (equivalent of $_SERVER)
     */
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, array $server = [], \Symfony\Component\BrowserKit\History $history = null, \Symfony\Component\BrowserKit\CookieJar $cookieJar = null)
    {
    }
    /**
     * Sets whether to catch exceptions when the kernel is handling a request.
     *
     * @param bool $catchExceptions Whether to catch exceptions
     */
    public function catchExceptions($catchExceptions)
    {
    }
    /**
     * Makes a request.
     *
     * @return Response A Response instance
     */
    protected function doRequest($request)
    {
    }
    /**
     * Returns the script to execute when the request must be insulated.
     *
     * @return string
     */
    protected function getScript($request)
    {
    }
    protected function getHandleScript()
    {
    }
    /**
     * Converts the BrowserKit request to a HttpKernel request.
     *
     * @return Request A Request instance
     */
    protected function filterRequest(\Symfony\Component\BrowserKit\Request $request)
    {
    }
    /**
     * Filters an array of files.
     *
     * This method created test instances of UploadedFile so that the move()
     * method can be called on those instances.
     *
     * If the size of a file is greater than the allowed size (from php.ini) then
     * an invalid UploadedFile is returned with an error set to UPLOAD_ERR_INI_SIZE.
     *
     * @see UploadedFile
     *
     * @return array An array with all uploaded files marked as already moved
     */
    protected function filterFiles(array $files)
    {
    }
    /**
     * Converts the HttpKernel response to a BrowserKit response.
     *
     * @return DomResponse A DomResponse instance
     */
    protected function filterResponse($response)
    {
    }
}
