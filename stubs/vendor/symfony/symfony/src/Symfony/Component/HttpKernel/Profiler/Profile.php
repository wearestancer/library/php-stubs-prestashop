<?php

namespace Symfony\Component\HttpKernel\Profiler;

/**
 * Profile.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Profile
{
    public function __construct(string $token)
    {
    }
    /**
     * Sets the token.
     *
     * @param string $token The token
     */
    public function setToken($token)
    {
    }
    /**
     * Gets the token.
     *
     * @return string The token
     */
    public function getToken()
    {
    }
    /**
     * Sets the parent token.
     */
    public function setParent(self $parent)
    {
    }
    /**
     * Returns the parent profile.
     *
     * @return self
     */
    public function getParent()
    {
    }
    /**
     * Returns the parent token.
     *
     * @return string|null The parent token
     */
    public function getParentToken()
    {
    }
    /**
     * Returns the IP.
     *
     * @return string|null The IP
     */
    public function getIp()
    {
    }
    /**
     * Sets the IP.
     *
     * @param string $ip
     */
    public function setIp($ip)
    {
    }
    /**
     * Returns the request method.
     *
     * @return string|null The request method
     */
    public function getMethod()
    {
    }
    public function setMethod($method)
    {
    }
    /**
     * Returns the URL.
     *
     * @return string|null The URL
     */
    public function getUrl()
    {
    }
    /**
     * @param string $url
     */
    public function setUrl($url)
    {
    }
    /**
     * Returns the time.
     *
     * @return int The time
     */
    public function getTime()
    {
    }
    /**
     * @param int $time The time
     */
    public function setTime($time)
    {
    }
    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
    }
    /**
     * @return int|null
     */
    public function getStatusCode()
    {
    }
    /**
     * Finds children profilers.
     *
     * @return self[]
     */
    public function getChildren()
    {
    }
    /**
     * Sets children profiler.
     *
     * @param Profile[] $children
     */
    public function setChildren(array $children)
    {
    }
    /**
     * Adds the child token.
     */
    public function addChild(self $child)
    {
    }
    public function getChildByToken(string $token) : ?self
    {
    }
    /**
     * Gets a Collector by name.
     *
     * @param string $name A collector name
     *
     * @return DataCollectorInterface A DataCollectorInterface instance
     *
     * @throws \InvalidArgumentException if the collector does not exist
     */
    public function getCollector($name)
    {
    }
    /**
     * Gets the Collectors associated with this profile.
     *
     * @return DataCollectorInterface[]
     */
    public function getCollectors()
    {
    }
    /**
     * Sets the Collectors associated with this profile.
     *
     * @param DataCollectorInterface[] $collectors
     */
    public function setCollectors(array $collectors)
    {
    }
    /**
     * Adds a Collector.
     */
    public function addCollector(\Symfony\Component\HttpKernel\DataCollector\DataCollectorInterface $collector)
    {
    }
    /**
     * Returns true if a Collector for the given name exists.
     *
     * @param string $name A collector name
     *
     * @return bool
     */
    public function hasCollector($name)
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
}
