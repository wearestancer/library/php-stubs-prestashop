<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Synchronizes the locale between the request and the translator.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.3, use LocaleAwareListener instead
 */
class TranslatorListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param LocaleAwareInterface $translator
     */
    public function __construct($translator, \Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
