<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * @deprecated since Symfony 4.3, use ControllerEvent instead
 */
class FilterControllerEvent extends \Symfony\Component\HttpKernel\Event\KernelEvent
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, callable $controller, \Symfony\Component\HttpFoundation\Request $request, ?int $requestType)
    {
    }
    /**
     * Returns the current controller.
     *
     * @return callable
     */
    public function getController()
    {
    }
    public function setController(callable $controller)
    {
    }
}
