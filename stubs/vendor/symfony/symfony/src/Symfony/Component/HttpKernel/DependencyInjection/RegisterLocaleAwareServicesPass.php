<?php

namespace Symfony\Component\HttpKernel\DependencyInjection;

/**
 * Register all services that have the "kernel.locale_aware" tag into the listener.
 *
 * @author Pierre Bobiet <pierrebobiet@gmail.com>
 */
class RegisterLocaleAwareServicesPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $listenerServiceId = 'locale_aware_listener', string $localeAwareTag = 'kernel.locale_aware')
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
