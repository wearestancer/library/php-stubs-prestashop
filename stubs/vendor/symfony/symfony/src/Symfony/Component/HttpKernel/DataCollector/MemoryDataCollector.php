<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * MemoryDataCollector.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class MemoryDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    /**
     * Gets the memory.
     *
     * @return int The memory
     */
    public function getMemory()
    {
    }
    /**
     * Gets the PHP memory limit.
     *
     * @return int The memory limit
     */
    public function getMemoryLimit()
    {
    }
    /**
     * Updates the memory usage data.
     */
    public function updateMemoryUsage()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
