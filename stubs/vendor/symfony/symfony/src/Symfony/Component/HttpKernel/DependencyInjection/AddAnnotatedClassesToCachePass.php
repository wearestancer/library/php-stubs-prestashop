<?php

namespace Symfony\Component\HttpKernel\DependencyInjection;

/**
 * Sets the classes to compile in the cache for the container.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class AddAnnotatedClassesToCachePass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(\Symfony\Component\HttpKernel\Kernel $kernel)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
