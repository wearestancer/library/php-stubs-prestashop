<?php

namespace Symfony\Component\HttpKernel\HttpCache;

/**
 * ResponseCacheStrategy knows how to compute the Response cache HTTP header
 * based on the different response cache headers.
 *
 * This implementation changes the master response TTL to the smallest TTL received
 * or force validation if one of the surrogates has validation cache strategy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ResponseCacheStrategy implements \Symfony\Component\HttpKernel\HttpCache\ResponseCacheStrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function add(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
}
