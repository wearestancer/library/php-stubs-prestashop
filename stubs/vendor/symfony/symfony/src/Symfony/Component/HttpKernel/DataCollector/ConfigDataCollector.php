<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class ConfigDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct(string $name = null, string $version = null)
    {
    }
    /**
     * Sets the Kernel associated with this Request.
     */
    public function setKernel(\Symfony\Component\HttpKernel\KernelInterface $kernel = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    public function lateCollect()
    {
    }
    /**
     * @deprecated since Symfony 4.2
     */
    public function getApplicationName()
    {
    }
    /**
     * @deprecated since Symfony 4.2
     */
    public function getApplicationVersion()
    {
    }
    /**
     * Gets the token.
     *
     * @return string|null The token
     */
    public function getToken()
    {
    }
    /**
     * Gets the Symfony version.
     *
     * @return string The Symfony version
     */
    public function getSymfonyVersion()
    {
    }
    /**
     * Returns the state of the current Symfony release.
     *
     * @return string One of: unknown, dev, stable, eom, eol
     */
    public function getSymfonyState()
    {
    }
    /**
     * Returns the minor Symfony version used (without patch numbers of extra
     * suffix like "RC", "beta", etc.).
     *
     * @return string
     */
    public function getSymfonyMinorVersion()
    {
    }
    /**
     * Returns if the current Symfony version is a Long-Term Support one.
     */
    public function isSymfonyLts() : bool
    {
    }
    /**
     * Returns the human readable date when this Symfony version ends its
     * maintenance period.
     *
     * @return string
     */
    public function getSymfonyEom()
    {
    }
    /**
     * Returns the human readable date when this Symfony version reaches its
     * "end of life" and won't receive bugs or security fixes.
     *
     * @return string
     */
    public function getSymfonyEol()
    {
    }
    /**
     * Gets the PHP version.
     *
     * @return string The PHP version
     */
    public function getPhpVersion()
    {
    }
    /**
     * Gets the PHP version extra part.
     *
     * @return string|null The extra part
     */
    public function getPhpVersionExtra()
    {
    }
    /**
     * @return int The PHP architecture as number of bits (e.g. 32 or 64)
     */
    public function getPhpArchitecture()
    {
    }
    /**
     * @return string
     */
    public function getPhpIntlLocale()
    {
    }
    /**
     * @return string
     */
    public function getPhpTimezone()
    {
    }
    /**
     * Gets the application name.
     *
     * @return string The application name
     *
     * @deprecated since Symfony 4.2
     */
    public function getAppName()
    {
    }
    /**
     * Gets the environment.
     *
     * @return string The environment
     */
    public function getEnv()
    {
    }
    /**
     * Returns true if the debug is enabled.
     *
     * @return bool true if debug is enabled, false otherwise
     */
    public function isDebug()
    {
    }
    /**
     * Returns true if the XDebug is enabled.
     *
     * @return bool true if XDebug is enabled, false otherwise
     */
    public function hasXDebug()
    {
    }
    /**
     * Returns true if APCu is enabled.
     *
     * @return bool true if APCu is enabled, false otherwise
     */
    public function hasApcu()
    {
    }
    /**
     * Returns true if Zend OPcache is enabled.
     *
     * @return bool true if Zend OPcache is enabled, false otherwise
     */
    public function hasZendOpcache()
    {
    }
    public function getBundles()
    {
    }
    /**
     * Gets the PHP SAPI name.
     *
     * @return string The environment
     */
    public function getSapiName()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
