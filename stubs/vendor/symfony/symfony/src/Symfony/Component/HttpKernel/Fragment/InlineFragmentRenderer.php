<?php

namespace Symfony\Component\HttpKernel\Fragment;

/**
 * Implements the inline rendering strategy where the Request is rendered by the current HTTP kernel.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class InlineFragmentRenderer extends \Symfony\Component\HttpKernel\Fragment\RoutableFragmentRenderer
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * Additional available options:
     *
     *  * alt: an alternative URI to render in case of an error
     */
    public function render($uri, \Symfony\Component\HttpFoundation\Request $request, array $options = [])
    {
    }
    protected function createSubRequest($uri, \Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
