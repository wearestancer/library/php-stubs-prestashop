<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * Allows to execute logic after a response was sent.
 *
 * Since it's only triggered on master requests, the `getRequestType()` method
 * will always return the value of `HttpKernelInterface::MASTER_REQUEST`.
 *
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @final since Symfony 4.4
 */
class TerminateEvent extends \Symfony\Component\HttpKernel\Event\PostResponseEvent
{
}
