<?php

namespace Symfony\Component\HttpKernel\DependencyInjection;

/**
 * @author Alexander M. Turek <me@derrabus.de>
 */
class ResettableServicePass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function __construct(string $tagName = 'kernel.reset')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
