<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class LoggerDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct($logger = null, string $containerPathPrefix = null, \Symfony\Component\HttpFoundation\RequestStack $requestStack = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    public function getLogs()
    {
    }
    public function getPriorities()
    {
    }
    public function countErrors()
    {
    }
    public function countDeprecations()
    {
    }
    public function countWarnings()
    {
    }
    public function countScreams()
    {
    }
    public function getCompilerLogs()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
