<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * ProfilerListener collects data for the current request by listening to the kernel events.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class ProfilerListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    protected $profiler;
    protected $matcher;
    protected $onlyException;
    protected $onlyMasterRequests;
    protected $exception;
    protected $profiles;
    protected $requestStack;
    protected $parents;
    /**
     * @param bool $onlyException      True if the profiler only collects data when an exception occurs, false otherwise
     * @param bool $onlyMasterRequests True if the profiler only collects data when the request is a master request, false otherwise
     */
    public function __construct(\Symfony\Component\HttpKernel\Profiler\Profiler $profiler, \Symfony\Component\HttpFoundation\RequestStack $requestStack, \Symfony\Component\HttpFoundation\RequestMatcherInterface $matcher = null, bool $onlyException = false, bool $onlyMasterRequests = false)
    {
    }
    /**
     * Handles the onKernelException event.
     */
    public function onKernelException(\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event)
    {
    }
    /**
     * Handles the onKernelResponse event.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    public function onKernelTerminate(\Symfony\Component\HttpKernel\Event\PostResponseEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
