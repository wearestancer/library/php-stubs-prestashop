<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * @author Tobias Schultze <http://tobion.de>
 *
 * @deprecated since Symfony 4.1, use AbstractSessionListener instead
 */
class SaveSessionListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
