<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * ExceptionDataCollector.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class ExceptionDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector
{
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * Checks if the exception is not null.
     *
     * @return bool true if the exception is not null, false otherwise
     */
    public function hasException()
    {
    }
    /**
     * Gets the exception.
     *
     * @return \Exception|FlattenException
     */
    public function getException()
    {
    }
    /**
     * Gets the exception message.
     *
     * @return string The exception message
     */
    public function getMessage()
    {
    }
    /**
     * Gets the exception code.
     *
     * @return int The exception code
     */
    public function getCode()
    {
    }
    /**
     * Gets the status code.
     *
     * @return int The status code
     */
    public function getStatusCode()
    {
    }
    /**
     * Gets the exception trace.
     *
     * @return array The exception trace
     */
    public function getTrace()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
