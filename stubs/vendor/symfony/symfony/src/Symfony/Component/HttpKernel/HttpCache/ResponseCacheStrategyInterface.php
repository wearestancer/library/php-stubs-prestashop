<?php

namespace Symfony\Component\HttpKernel\HttpCache;

/**
 * ResponseCacheStrategyInterface implementations know how to compute the
 * Response cache HTTP header based on the different response cache headers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface ResponseCacheStrategyInterface
{
    /**
     * Adds a Response.
     */
    public function add(\Symfony\Component\HttpFoundation\Response $response);
    /**
     * Updates the Response HTTP headers based on the embedded Responses.
     */
    public function update(\Symfony\Component\HttpFoundation\Response $response);
}
