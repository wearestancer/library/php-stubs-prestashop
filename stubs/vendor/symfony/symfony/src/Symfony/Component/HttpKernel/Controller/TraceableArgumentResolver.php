<?php

namespace Symfony\Component\HttpKernel\Controller;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TraceableArgumentResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface
{
    public function __construct(\Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $resolver, \Symfony\Component\Stopwatch\Stopwatch $stopwatch)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getArguments(\Symfony\Component\HttpFoundation\Request $request, $controller)
    {
    }
}
