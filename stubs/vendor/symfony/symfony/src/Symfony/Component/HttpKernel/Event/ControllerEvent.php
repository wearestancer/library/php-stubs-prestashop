<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * Allows filtering of a controller callable.
 *
 * You can call getController() to retrieve the current controller. With
 * setController() you can set a new controller that is used in the processing
 * of the request.
 *
 * Controllers should be callables.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @final since Symfony 4.4
 */
class ControllerEvent extends \Symfony\Component\HttpKernel\Event\FilterControllerEvent
{
}
