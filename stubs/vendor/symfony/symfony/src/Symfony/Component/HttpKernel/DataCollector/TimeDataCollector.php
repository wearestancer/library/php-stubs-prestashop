<?php

namespace Symfony\Component\HttpKernel\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class TimeDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    protected $kernel;
    protected $stopwatch;
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel = null, \Symfony\Component\Stopwatch\Stopwatch $stopwatch = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function lateCollect()
    {
    }
    /**
     * Sets the request events.
     *
     * @param StopwatchEvent[] $events The request events
     */
    public function setEvents(array $events)
    {
    }
    /**
     * Gets the request events.
     *
     * @return StopwatchEvent[] The request events
     */
    public function getEvents()
    {
    }
    /**
     * Gets the request elapsed time.
     *
     * @return float The elapsed time
     */
    public function getDuration()
    {
    }
    /**
     * Gets the initialization time.
     *
     * This is the time spent until the beginning of the request handling.
     *
     * @return float The elapsed time
     */
    public function getInitTime()
    {
    }
    /**
     * Gets the request time.
     *
     * @return float
     */
    public function getStartTime()
    {
    }
    /**
     * @return bool whether or not the stopwatch component is installed
     */
    public function isStopwatchInstalled()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
