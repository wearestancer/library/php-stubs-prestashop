<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Ensures that the application is not indexed by search engines.
 *
 * @author Gary PEGEOT <garypegeot@gmail.com>
 */
class DisallowRobotsIndexingListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function onResponse(\Symfony\Component\HttpKernel\Event\ResponseEvent $event) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
