<?php

namespace Symfony\Component\HttpKernel\CacheClearer;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class Psr6CacheClearer implements \Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface
{
    public function __construct(array $pools = [])
    {
    }
    public function hasPool($name)
    {
    }
    public function getPool($name)
    {
    }
    public function clearPool($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function clear($cacheDir)
    {
    }
}
