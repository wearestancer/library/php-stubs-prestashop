<?php

namespace Symfony\Component\HttpKernel\Debug;

/**
 * Formats debug file links.
 *
 * @author Jérémy Romey <jeremy@free-agent.fr>
 *
 * @final since Symfony 4.3
 */
class FileLinkFormatter
{
    /**
     * @param string|array|null $fileLinkFormat
     * @param string|\Closure   $urlFormat the URL format, or a closure that returns it on-demand
     */
    public function __construct($fileLinkFormat = null, \Symfony\Component\HttpFoundation\RequestStack $requestStack = null, string $baseDir = null, $urlFormat = null)
    {
    }
    public function format($file, $line)
    {
    }
    /**
     * @internal
     */
    public function __sleep() : array
    {
    }
    /**
     * @internal
     */
    public static function generateUrlFormat(\Symfony\Component\Routing\Generator\UrlGeneratorInterface $router, $routeName, $queryString)
    {
    }
}
