<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Sets the session onto the request on the "kernel.request" event and saves
 * it on the "kernel.response" event.
 *
 * In addition, if the session has been started it overrides the Cache-Control
 * header in such a way that all caching is disabled in that case.
 * If you have a scenario where caching responses with session information in
 * them makes sense, you can disable this behaviour by setting the header
 * AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER on the response.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Tobias Schultze <http://tobion.de>
 *
 * @internal since Symfony 4.3
 */
abstract class AbstractSessionListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public const NO_AUTO_CACHE_CONTROL_HEADER = 'Symfony-Session-NoAutoCacheControl';
    protected $container;
    public function __construct(\Psr\Container\ContainerInterface $container = null)
    {
    }
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    /**
     * @internal
     */
    public function onFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
    /**
     * Gets the session object.
     *
     * @return SessionInterface|null A SessionInterface instance or null if no session is available
     */
    protected abstract function getSession();
}
