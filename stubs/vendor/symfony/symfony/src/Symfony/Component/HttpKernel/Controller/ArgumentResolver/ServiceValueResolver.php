<?php

namespace Symfony\Component\HttpKernel\Controller\ArgumentResolver;

/**
 * Yields a service keyed by _controller and argument name.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class ServiceValueResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : iterable
    {
    }
}
