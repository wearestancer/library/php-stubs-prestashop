<?php

namespace Symfony\Component\HttpKernel\Controller;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TraceableControllerResolver implements \Symfony\Component\HttpKernel\Controller\ControllerResolverInterface
{
    public function __construct(\Symfony\Component\HttpKernel\Controller\ControllerResolverInterface $resolver, \Symfony\Component\Stopwatch\Stopwatch $stopwatch)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getController(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
