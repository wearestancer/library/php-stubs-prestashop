<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * Allows to filter a Response object.
 *
 * You can call getResponse() to retrieve the current response. With
 * setResponse() you can set a new response that will be returned to the
 * browser.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @final since Symfony 4.4
 */
class ResponseEvent extends \Symfony\Component\HttpKernel\Event\FilterResponseEvent
{
}
