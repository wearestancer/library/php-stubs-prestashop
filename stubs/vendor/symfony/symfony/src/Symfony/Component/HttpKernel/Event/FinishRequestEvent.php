<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * Triggered whenever a request is fully processed.
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 *
 * @final since Symfony 4.4
 */
class FinishRequestEvent extends \Symfony\Component\HttpKernel\Event\KernelEvent
{
}
