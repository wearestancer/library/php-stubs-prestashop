<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * Allows filtering of controller arguments.
 *
 * You can call getController() to retrieve the controller and getArguments
 * to retrieve the current arguments. With setArguments() you can replace
 * arguments that are used to call the controller.
 *
 * Arguments set in the event must be compatible with the signature of the
 * controller.
 *
 * @author Christophe Coevoet <stof@notk.org>
 *
 * @final since Symfony 4.4
 */
class ControllerArgumentsEvent extends \Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent
{
}
