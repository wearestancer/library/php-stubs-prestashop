<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * ResponseListener fixes the Response headers based on the Request.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class ResponseListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(string $charset)
    {
    }
    /**
     * Filters the Response.
     */
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
