<?php

namespace Symfony\Component\HttpKernel\EventListener;

/**
 * Handles content fragments represented by special URIs.
 *
 * All URL paths starting with /_fragment are handled as
 * content fragments by this listener.
 *
 * Throws an AccessDeniedHttpException exception if the request
 * is not signed or if it is not an internal sub-request.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class FragmentListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @param string $fragmentPath The path that triggers this listener
     */
    public function __construct(\Symfony\Component\HttpKernel\UriSigner $signer, string $fragmentPath = '/_fragment')
    {
    }
    /**
     * Fixes request attributes when the path is '/_fragment'.
     *
     * @throws AccessDeniedHttpException if the request does not come from a trusted IP
     */
    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    protected function validateRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
