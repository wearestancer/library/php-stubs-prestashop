<?php

namespace Symfony\Component\HttpKernel\HttpCache;

/**
 * Store implements all the logic for storing cache metadata (Request and Response headers).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Store implements \Symfony\Component\HttpKernel\HttpCache\StoreInterface
{
    protected $root;
    /**
     * Constructor.
     *
     * The available options are:
     *
     *   * private_headers  Set of response headers that should not be stored
     *                      when a response is cached. (default: Set-Cookie)
     *
     * @throws \RuntimeException
     */
    public function __construct(string $root, array $options = [])
    {
    }
    /**
     * Cleanups storage.
     */
    public function cleanup()
    {
    }
    /**
     * Tries to lock the cache for a given Request, without blocking.
     *
     * @return bool|string true if the lock is acquired, the path to the current lock otherwise
     */
    public function lock(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Releases the lock for the given Request.
     *
     * @return bool False if the lock file does not exist or cannot be unlocked, true otherwise
     */
    public function unlock(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    public function isLocked(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Locates a cached Response for the Request provided.
     *
     * @return Response|null A Response instance, or null if no cache entry was found
     */
    public function lookup(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Writes a cache entry to the store for the given Request and Response.
     *
     * Existing entries are read and any that match the response are removed. This
     * method calls write with the new list of cache entries.
     *
     * @return string The key under which the response is stored
     *
     * @throws \RuntimeException
     */
    public function write(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * Returns content digest for $response.
     *
     * @return string
     */
    protected function generateContentDigest(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * Invalidates all cache entries that match the request.
     *
     * @throws \RuntimeException
     */
    public function invalidate(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Purges data for the given URL.
     *
     * This method purges both the HTTP and the HTTPS version of the cache entry.
     *
     * @param string $url A URL
     *
     * @return bool true if the URL exists with either HTTP or HTTPS scheme and has been purged, false otherwise
     */
    public function purge($url)
    {
    }
    public function getPath($key)
    {
    }
    /**
     * Generates a cache key for the given Request.
     *
     * This method should return a key that must only depend on a
     * normalized version of the request URI.
     *
     * If the same URI can have more than one representation, based on some
     * headers, use a Vary header to indicate them, and each representation will
     * be stored independently under the same cache key.
     *
     * @return string A key for the given Request
     */
    protected function generateCacheKey(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
