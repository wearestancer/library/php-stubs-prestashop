<?php

namespace Symfony\Component\HttpKernel\Log;

/**
 * Minimalist PSR-3 logger designed to write in stderr or any other stream.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class Logger extends \Psr\Log\AbstractLogger
{
    public function __construct(string $minLevel = null, $output = null, callable $formatter = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
    }
}
