<?php

namespace Symfony\Component\HttpKernel\DependencyInjection;

/**
 * Ensures certain extensions are always loaded.
 *
 * @author Kris Wallsmith <kris@symfony.com>
 */
class MergeExtensionConfigurationPass extends \Symfony\Component\DependencyInjection\Compiler\MergeExtensionConfigurationPass
{
    public function __construct(array $extensions)
    {
    }
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
