<?php

namespace Symfony\Component\HttpKernel\Profiler;

/**
 * Profiler.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Profiler implements \Symfony\Contracts\Service\ResetInterface
{
    public function __construct(\Symfony\Component\HttpKernel\Profiler\ProfilerStorageInterface $storage, \Psr\Log\LoggerInterface $logger = null, bool $enable = true)
    {
    }
    /**
     * Disables the profiler.
     */
    public function disable()
    {
    }
    /**
     * Enables the profiler.
     */
    public function enable()
    {
    }
    /**
     * Loads the Profile for the given Response.
     *
     * @return Profile|null A Profile instance
     */
    public function loadProfileFromResponse(\Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * Loads the Profile for the given token.
     *
     * @param string $token A token
     *
     * @return Profile|null A Profile instance
     */
    public function loadProfile($token)
    {
    }
    /**
     * Saves a Profile.
     *
     * @return bool
     */
    public function saveProfile(\Symfony\Component\HttpKernel\Profiler\Profile $profile)
    {
    }
    /**
     * Purges all data from the storage.
     */
    public function purge()
    {
    }
    /**
     * Finds profiler tokens for the given criteria.
     *
     * @param string $ip         The IP
     * @param string $url        The URL
     * @param string $limit      The maximum number of tokens to return
     * @param string $method     The request method
     * @param string $start      The start date to search from
     * @param string $end        The end date to search to
     * @param string $statusCode The request status code
     *
     * @return array An array of tokens
     *
     * @see https://php.net/datetime.formats for the supported date/time formats
     */
    public function find($ip, $url, $limit, $method, $start, $end, $statusCode = null)
    {
    }
    /**
     * Collects data for the given Response.
     *
     * @param \Throwable|null $exception
     *
     * @return Profile|null A Profile instance or null if the profiler is disabled
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    public function reset()
    {
    }
    /**
     * Gets the Collectors associated with this profiler.
     *
     * @return array An array of collectors
     */
    public function all()
    {
    }
    /**
     * Sets the Collectors associated with this profiler.
     *
     * @param DataCollectorInterface[] $collectors An array of collectors
     */
    public function set(array $collectors = [])
    {
    }
    /**
     * Adds a Collector.
     */
    public function add(\Symfony\Component\HttpKernel\DataCollector\DataCollectorInterface $collector)
    {
    }
    /**
     * Returns true if a Collector for the given name exists.
     *
     * @param string $name A collector name
     *
     * @return bool
     */
    public function has($name)
    {
    }
    /**
     * Gets a Collector by name.
     *
     * @param string $name A collector name
     *
     * @return DataCollectorInterface A DataCollectorInterface instance
     *
     * @throws \InvalidArgumentException if the collector does not exist
     */
    public function get($name)
    {
    }
}
