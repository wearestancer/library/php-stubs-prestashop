<?php

namespace Symfony\Component\HttpKernel\Event;

/**
 * @deprecated since Symfony 4.3, use ExceptionEvent instead
 */
class GetResponseForExceptionEvent extends \Symfony\Component\HttpKernel\Event\RequestEvent
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, \Symfony\Component\HttpFoundation\Request $request, int $requestType, \Throwable $e)
    {
    }
    public function getThrowable() : \Throwable
    {
    }
    /**
     * Replaces the thrown exception.
     *
     * This exception will be thrown if no response is set in the event.
     */
    public function setThrowable(\Throwable $exception) : void
    {
    }
    /**
     * @deprecated since Symfony 4.4, use getThrowable instead
     *
     * @return \Exception The thrown exception
     */
    public function getException()
    {
    }
    /**
     * @deprecated since Symfony 4.4, use setThrowable instead
     *
     * @param \Exception $exception The thrown exception
     */
    public function setException(\Exception $exception)
    {
    }
    /**
     * Mark the event as allowing a custom response code.
     */
    public function allowCustomResponseCode()
    {
    }
    /**
     * Returns true if the event allows a custom response code.
     *
     * @return bool
     */
    public function isAllowingCustomResponseCode()
    {
    }
}
