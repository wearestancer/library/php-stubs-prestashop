<?php

namespace Symfony\Component\HttpKernel\CacheWarmer;

/**
 * Abstract cache warmer that knows how to write a file to the cache.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class CacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface
{
    protected function writeCacheFile($file, $content)
    {
    }
}
