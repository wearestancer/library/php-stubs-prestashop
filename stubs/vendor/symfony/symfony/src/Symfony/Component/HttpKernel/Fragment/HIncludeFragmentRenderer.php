<?php

namespace Symfony\Component\HttpKernel\Fragment;

/**
 * Implements the Hinclude rendering strategy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HIncludeFragmentRenderer extends \Symfony\Component\HttpKernel\Fragment\RoutableFragmentRenderer
{
    /**
     * @param EngineInterface|Environment $templating            An EngineInterface or a Twig instance
     * @param string                      $globalDefaultTemplate The global default content (it can be a template name or the content)
     */
    public function __construct($templating = null, \Symfony\Component\HttpKernel\UriSigner $signer = null, string $globalDefaultTemplate = null, string $charset = 'utf-8')
    {
    }
    /**
     * Sets the templating engine to use to render the default content.
     *
     * @param EngineInterface|Environment|null $templating An EngineInterface or an Environment instance
     *
     * @throws \InvalidArgumentException
     *
     * @internal
     */
    public function setTemplating($templating)
    {
    }
    /**
     * Checks if a templating engine has been set.
     *
     * @return bool true if the templating engine has been set, false otherwise
     */
    public function hasTemplating()
    {
    }
    /**
     * {@inheritdoc}
     *
     * Additional available options:
     *
     *  * default:    The default content (it can be a template name or the content)
     *  * id:         An optional hx:include tag id attribute
     *  * attributes: An optional array of hx:include tag attributes
     */
    public function render($uri, \Symfony\Component\HttpFoundation\Request $request, array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
