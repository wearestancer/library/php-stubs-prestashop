<?php

namespace Symfony\Component\HttpKernel\Controller;

/**
 * Responsible for resolving the arguments passed to an action.
 *
 * @author Iltar van der Berg <kjarli@gmail.com>
 */
final class ArgumentResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface
{
    public function __construct(\Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactoryInterface $argumentMetadataFactory = null, iterable $argumentValueResolvers = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getArguments(\Symfony\Component\HttpFoundation\Request $request, $controller) : array
    {
    }
    public static function getDefaultArgumentValueResolvers() : iterable
    {
    }
}
