<?php

namespace Symfony\Component\HttpKernel\Controller;

/**
 * Renders error or exception pages from a given FlattenException.
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 * @author Matthias Pigulla <mp@webfactory.de>
 */
class ErrorController
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, $controller, \Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface $errorRenderer)
    {
    }
    public function __invoke(\Throwable $exception) : \Symfony\Component\HttpFoundation\Response
    {
    }
    public function preview(\Symfony\Component\HttpFoundation\Request $request, int $code) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
