<?php

namespace Symfony\Component\OptionsResolver\Exception;

/**
 * Thrown when an argument is invalid.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Symfony\Component\OptionsResolver\Exception\ExceptionInterface
{
}
