<?php

namespace Symfony\Component\OptionsResolver\Debug;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @final
 */
class OptionsResolverIntrospector
{
    public function __construct(\Symfony\Component\OptionsResolver\OptionsResolver $optionsResolver)
    {
    }
    /**
     * @return mixed
     *
     * @throws NoConfigurationException on no configured value
     */
    public function getDefault(string $option)
    {
    }
    /**
     * @return \Closure[]
     *
     * @throws NoConfigurationException on no configured closures
     */
    public function getLazyClosures(string $option) : array
    {
    }
    /**
     * @return string[]
     *
     * @throws NoConfigurationException on no configured types
     */
    public function getAllowedTypes(string $option) : array
    {
    }
    /**
     * @return mixed[]
     *
     * @throws NoConfigurationException on no configured values
     */
    public function getAllowedValues(string $option) : array
    {
    }
    /**
     * @throws NoConfigurationException on no configured normalizer
     */
    public function getNormalizer(string $option) : \Closure
    {
    }
    /**
     * @throws NoConfigurationException when no normalizer is configured
     */
    public function getNormalizers(string $option) : array
    {
    }
    /**
     * @return string|\Closure
     *
     * @throws NoConfigurationException on no configured deprecation
     */
    public function getDeprecationMessage(string $option)
    {
    }
}
