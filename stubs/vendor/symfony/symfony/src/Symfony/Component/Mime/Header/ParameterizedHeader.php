<?php

namespace Symfony\Component\Mime\Header;

/**
 * @author Chris Corbyn
 */
final class ParameterizedHeader extends \Symfony\Component\Mime\Header\UnstructuredHeader
{
    /**
     * RFC 2231's definition of a token.
     *
     * @var string
     */
    public const TOKEN_REGEX = '(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2E\\x30-\\x39\\x41-\\x5A\\x5E-\\x7E]+)';
    public function __construct(string $name, string $value, array $parameters = [])
    {
    }
    public function setParameter(string $parameter, ?string $value)
    {
    }
    public function getParameter(string $parameter) : string
    {
    }
    /**
     * @param string[] $parameters
     */
    public function setParameters(array $parameters)
    {
    }
    /**
     * @return string[]
     */
    public function getParameters() : array
    {
    }
    public function getBodyAsString() : string
    {
    }
}
