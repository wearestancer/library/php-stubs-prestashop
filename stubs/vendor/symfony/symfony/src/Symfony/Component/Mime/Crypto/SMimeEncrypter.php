<?php

namespace Symfony\Component\Mime\Crypto;

/**
 * @author Sebastiaan Stok <s.stok@rollerscapes.net>
 */
final class SMimeEncrypter extends \Symfony\Component\Mime\Crypto\SMime
{
    /**
     * @param string|string[] $certificate The path (or array of paths) of the file(s) containing the X.509 certificate(s)
     * @param int|null        $cipher      A set of algorithms used to encrypt the message. Must be one of these PHP constants: https://www.php.net/manual/en/openssl.ciphers.php
     */
    public function __construct($certificate, int $cipher = null)
    {
    }
    public function encrypt(\Symfony\Component\Mime\Message $message) : \Symfony\Component\Mime\Message
    {
    }
}
