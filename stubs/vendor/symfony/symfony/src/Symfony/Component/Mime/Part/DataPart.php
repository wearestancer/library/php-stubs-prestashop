<?php

namespace Symfony\Component\Mime\Part;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DataPart extends \Symfony\Component\Mime\Part\TextPart
{
    /** @internal */
    protected $_parent;
    /**
     * @param resource|string $body
     */
    public function __construct($body, string $filename = null, string $contentType = null, string $encoding = null)
    {
    }
    public static function fromPath(string $path, string $name = null, string $contentType = null) : self
    {
    }
    /**
     * @return $this
     */
    public function asInline()
    {
    }
    public function getContentId() : string
    {
    }
    public function hasContentId() : bool
    {
    }
    public function getMediaType() : string
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function asDebugString() : string
    {
    }
    public function __destruct()
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
}
