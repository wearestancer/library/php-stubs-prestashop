<?php

namespace Symfony\Component\Mime\Encoder;

/**
 * @author Chris Corbyn
 */
final class Rfc2231Encoder implements \Symfony\Component\Mime\Encoder\EncoderInterface
{
    /**
     * Takes an unencoded string and produces a string encoded according to RFC 2231 from it.
     */
    public function encodeString(string $string, ?string $charset = 'utf-8', int $firstLineOffset = 0, int $maxLineLength = 0) : string
    {
    }
}
