<?php

namespace Symfony\Component\Mime;

/**
 * Guesses the MIME type using the PECL extension FileInfo.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FileinfoMimeTypeGuesser implements \Symfony\Component\Mime\MimeTypeGuesserInterface
{
    /**
     * @param string $magicFile A magic file to use with the finfo instance
     *
     * @see http://www.php.net/manual/en/function.finfo-open.php
     */
    public function __construct(string $magicFile = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isGuesserSupported() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessMimeType(string $path) : ?string
    {
    }
}
