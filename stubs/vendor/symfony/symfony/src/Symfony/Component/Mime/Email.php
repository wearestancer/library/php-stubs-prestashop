<?php

namespace Symfony\Component\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Email extends \Symfony\Component\Mime\Message
{
    public const PRIORITY_HIGHEST = 1;
    public const PRIORITY_HIGH = 2;
    public const PRIORITY_NORMAL = 3;
    public const PRIORITY_LOW = 4;
    public const PRIORITY_LOWEST = 5;
    // Used to avoid wrong body hash in DKIM signatures with multiple parts (e.g. HTML + TEXT) due to multiple boundaries.
    /**
     * @return $this
     */
    public function subject(string $subject)
    {
    }
    public function getSubject() : ?string
    {
    }
    /**
     * @return $this
     */
    public function date(\DateTimeInterface $dateTime)
    {
    }
    public function getDate() : ?\DateTimeImmutable
    {
    }
    /**
     * @param Address|string $address
     *
     * @return $this
     */
    public function returnPath($address)
    {
    }
    public function getReturnPath() : ?\Symfony\Component\Mime\Address
    {
    }
    /**
     * @param Address|string $address
     *
     * @return $this
     */
    public function sender($address)
    {
    }
    public function getSender() : ?\Symfony\Component\Mime\Address
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function addFrom(...$addresses)
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function from(...$addresses)
    {
    }
    /**
     * @return Address[]
     */
    public function getFrom() : array
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function addReplyTo(...$addresses)
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function replyTo(...$addresses)
    {
    }
    /**
     * @return Address[]
     */
    public function getReplyTo() : array
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function addTo(...$addresses)
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function to(...$addresses)
    {
    }
    /**
     * @return Address[]
     */
    public function getTo() : array
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function addCc(...$addresses)
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function cc(...$addresses)
    {
    }
    /**
     * @return Address[]
     */
    public function getCc() : array
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function addBcc(...$addresses)
    {
    }
    /**
     * @param Address|string ...$addresses
     *
     * @return $this
     */
    public function bcc(...$addresses)
    {
    }
    /**
     * @return Address[]
     */
    public function getBcc() : array
    {
    }
    /**
     * Sets the priority of this message.
     *
     * The value is an integer where 1 is the highest priority and 5 is the lowest.
     *
     * @return $this
     */
    public function priority(int $priority)
    {
    }
    /**
     * Get the priority of this message.
     *
     * The returned value is an integer where 1 is the highest priority and 5
     * is the lowest.
     */
    public function getPriority() : int
    {
    }
    /**
     * @param resource|string|null $body
     *
     * @return $this
     */
    public function text($body, string $charset = 'utf-8')
    {
    }
    /**
     * @return resource|string|null
     */
    public function getTextBody()
    {
    }
    public function getTextCharset() : ?string
    {
    }
    /**
     * @param resource|string|null $body
     *
     * @return $this
     */
    public function html($body, string $charset = 'utf-8')
    {
    }
    /**
     * @return resource|string|null
     */
    public function getHtmlBody()
    {
    }
    public function getHtmlCharset() : ?string
    {
    }
    /**
     * @param resource|string $body
     *
     * @return $this
     */
    public function attach($body, string $name = null, string $contentType = null)
    {
    }
    /**
     * @return $this
     */
    public function attachFromPath(string $path, string $name = null, string $contentType = null)
    {
    }
    /**
     * @param resource|string $body
     *
     * @return $this
     */
    public function embed($body, string $name = null, string $contentType = null)
    {
    }
    /**
     * @return $this
     */
    public function embedFromPath(string $path, string $name = null, string $contentType = null)
    {
    }
    /**
     * @return $this
     */
    public function attachPart(\Symfony\Component\Mime\Part\DataPart $part)
    {
    }
    /**
     * @return DataPart[]
     */
    public function getAttachments() : array
    {
    }
    public function getBody() : \Symfony\Component\Mime\Part\AbstractPart
    {
    }
    public function ensureValidity()
    {
    }
    /**
     * @internal
     */
    public function __serialize() : array
    {
    }
    /**
     * @internal
     */
    public function __unserialize(array $data) : void
    {
    }
}
