<?php

namespace Symfony\Component\Mime\Part\Multipart;

/**
 * Implements RFC 7578.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class FormDataPart extends \Symfony\Component\Mime\Part\AbstractMultipartPart
{
    /**
     * @param array<string|array|DataPart> $fields
     */
    public function __construct(array $fields = [])
    {
    }
    public function getMediaSubtype() : string
    {
    }
    public function getParts() : array
    {
    }
}
