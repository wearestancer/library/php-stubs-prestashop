<?php

namespace Symfony\Component\Mime\Header;

/**
 * A Date MIME Header.
 *
 * @author Chris Corbyn
 */
final class DateHeader extends \Symfony\Component\Mime\Header\AbstractHeader
{
    public function __construct(string $name, \DateTimeInterface $date)
    {
    }
    /**
     * @param \DateTimeInterface $body
     */
    public function setBody($body)
    {
    }
    public function getBody() : \DateTimeImmutable
    {
    }
    public function getDateTime() : \DateTimeImmutable
    {
    }
    /**
     * Set the date-time of the Date in this Header.
     *
     * If a DateTime instance is provided, it is converted to DateTimeImmutable.
     */
    public function setDateTime(\DateTimeInterface $dateTime)
    {
    }
    public function getBodyAsString() : string
    {
    }
}
