<?php

namespace Symfony\Component\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Address
{
    public function __construct(string $address, string $name = '')
    {
    }
    public function getAddress() : string
    {
    }
    public function getName() : string
    {
    }
    public function getEncodedAddress() : string
    {
    }
    public function toString() : string
    {
    }
    public function getEncodedName() : string
    {
    }
    /**
     * @param Address|string $address
     */
    public static function create($address) : self
    {
    }
    /**
     * @param array<Address|string> $addresses
     *
     * @return Address[]
     */
    public static function createArray(array $addresses) : array
    {
    }
    public static function fromString(string $string) : self
    {
    }
}
