<?php

namespace Symfony\Component\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Message extends \Symfony\Component\Mime\RawMessage
{
    public function __construct(\Symfony\Component\Mime\Header\Headers $headers = null, \Symfony\Component\Mime\Part\AbstractPart $body = null)
    {
    }
    public function __clone()
    {
    }
    /**
     * @return $this
     */
    public function setBody(\Symfony\Component\Mime\Part\AbstractPart $body = null)
    {
    }
    public function getBody() : ?\Symfony\Component\Mime\Part\AbstractPart
    {
    }
    /**
     * @return $this
     */
    public function setHeaders(\Symfony\Component\Mime\Header\Headers $headers)
    {
    }
    public function getHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function toString() : string
    {
    }
    public function toIterable() : iterable
    {
    }
    public function ensureValidity()
    {
    }
    public function generateMessageId() : string
    {
    }
    public function __serialize() : array
    {
    }
    public function __unserialize(array $data) : void
    {
    }
}
