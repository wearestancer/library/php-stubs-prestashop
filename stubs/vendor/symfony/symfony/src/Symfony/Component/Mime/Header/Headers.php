<?php

namespace Symfony\Component\Mime\Header;

/**
 * A collection of headers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Headers
{
    public function __construct(\Symfony\Component\Mime\Header\HeaderInterface ...$headers)
    {
    }
    public function __clone()
    {
    }
    public function setMaxLineLength(int $lineLength)
    {
    }
    public function getMaxLineLength() : int
    {
    }
    /**
     * @param array<Address|string> $addresses
     *
     * @return $this
     */
    public function addMailboxListHeader(string $name, array $addresses) : self
    {
    }
    /**
     * @param Address|string $address
     *
     * @return $this
     */
    public function addMailboxHeader(string $name, $address) : self
    {
    }
    /**
     * @param string|array $ids
     *
     * @return $this
     */
    public function addIdHeader(string $name, $ids) : self
    {
    }
    /**
     * @param Address|string $path
     *
     * @return $this
     */
    public function addPathHeader(string $name, $path) : self
    {
    }
    /**
     * @return $this
     */
    public function addDateHeader(string $name, \DateTimeInterface $dateTime) : self
    {
    }
    /**
     * @return $this
     */
    public function addTextHeader(string $name, string $value) : self
    {
    }
    /**
     * @return $this
     */
    public function addParameterizedHeader(string $name, string $value, array $params = []) : self
    {
    }
    public function has(string $name) : bool
    {
    }
    /**
     * @return $this
     */
    public function add(\Symfony\Component\Mime\Header\HeaderInterface $header) : self
    {
    }
    public function get(string $name) : ?\Symfony\Component\Mime\Header\HeaderInterface
    {
    }
    public function all(string $name = null) : iterable
    {
    }
    public function getNames() : array
    {
    }
    public function remove(string $name) : void
    {
    }
    public static function isUniqueHeader(string $name) : bool
    {
    }
    public function toString() : string
    {
    }
    public function toArray() : array
    {
    }
    /**
     * @internal
     */
    public function getHeaderBody(string $name)
    {
    }
    /**
     * @internal
     */
    public function setHeaderBody(string $type, string $name, $body) : void
    {
    }
    /**
     * @internal
     */
    public function getHeaderParameter(string $name, string $parameter) : ?string
    {
    }
    /**
     * @internal
     */
    public function setHeaderParameter(string $name, string $parameter, ?string $value) : void
    {
    }
}
