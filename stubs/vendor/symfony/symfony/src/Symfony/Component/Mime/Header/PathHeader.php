<?php

namespace Symfony\Component\Mime\Header;

/**
 * A Path Header, such a Return-Path (one address).
 *
 * @author Chris Corbyn
 */
final class PathHeader extends \Symfony\Component\Mime\Header\AbstractHeader
{
    public function __construct(string $name, \Symfony\Component\Mime\Address $address)
    {
    }
    /**
     * @param Address $body
     *
     * @throws RfcComplianceException
     */
    public function setBody($body)
    {
    }
    public function getBody() : \Symfony\Component\Mime\Address
    {
    }
    public function setAddress(\Symfony\Component\Mime\Address $address)
    {
    }
    public function getAddress() : \Symfony\Component\Mime\Address
    {
    }
    public function getBodyAsString() : string
    {
    }
}
