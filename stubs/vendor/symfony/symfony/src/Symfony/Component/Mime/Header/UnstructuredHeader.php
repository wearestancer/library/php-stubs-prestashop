<?php

namespace Symfony\Component\Mime\Header;

/**
 * A Simple MIME Header.
 *
 * @author Chris Corbyn
 */
class UnstructuredHeader extends \Symfony\Component\Mime\Header\AbstractHeader
{
    public function __construct(string $name, string $value)
    {
    }
    /**
     * @param string $body
     */
    public function setBody($body)
    {
    }
    /**
     * @return string
     */
    public function getBody()
    {
    }
    /**
     * Get the (unencoded) value of this header.
     */
    public function getValue() : string
    {
    }
    /**
     * Set the (unencoded) value of this header.
     */
    public function setValue(string $value)
    {
    }
    /**
     * Get the value of this header prepared for rendering.
     */
    public function getBodyAsString() : string
    {
    }
}
