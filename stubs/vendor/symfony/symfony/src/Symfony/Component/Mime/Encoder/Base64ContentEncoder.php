<?php

namespace Symfony\Component\Mime\Encoder;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Base64ContentEncoder extends \Symfony\Component\Mime\Encoder\Base64Encoder implements \Symfony\Component\Mime\Encoder\ContentEncoderInterface
{
    public function encodeByteStream($stream, int $maxLineLength = 0) : iterable
    {
    }
    public function getName() : string
    {
    }
}
