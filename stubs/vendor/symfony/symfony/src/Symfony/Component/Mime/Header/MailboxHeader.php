<?php

namespace Symfony\Component\Mime\Header;

/**
 * A Mailbox MIME Header for something like Sender (one named address).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class MailboxHeader extends \Symfony\Component\Mime\Header\AbstractHeader
{
    public function __construct(string $name, \Symfony\Component\Mime\Address $address)
    {
    }
    /**
     * @param Address $body
     *
     * @throws RfcComplianceException
     */
    public function setBody($body)
    {
    }
    /**
     * @throws RfcComplianceException
     */
    public function getBody() : \Symfony\Component\Mime\Address
    {
    }
    /**
     * @throws RfcComplianceException
     */
    public function setAddress(\Symfony\Component\Mime\Address $address)
    {
    }
    public function getAddress() : \Symfony\Component\Mime\Address
    {
    }
    public function getBodyAsString() : string
    {
    }
}
