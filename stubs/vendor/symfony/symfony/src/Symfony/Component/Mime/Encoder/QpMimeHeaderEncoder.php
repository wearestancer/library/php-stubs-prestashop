<?php

namespace Symfony\Component\Mime\Encoder;

/**
 * @author Chris Corbyn
 */
final class QpMimeHeaderEncoder extends \Symfony\Component\Mime\Encoder\QpEncoder implements \Symfony\Component\Mime\Encoder\MimeHeaderEncoderInterface
{
    public function getName() : string
    {
    }
    public function encodeString(string $string, ?string $charset = 'utf-8', int $firstLineOffset = 0, int $maxLineLength = 0) : string
    {
    }
}
