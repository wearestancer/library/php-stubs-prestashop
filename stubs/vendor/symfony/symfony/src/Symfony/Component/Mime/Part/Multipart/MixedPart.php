<?php

namespace Symfony\Component\Mime\Part\Multipart;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class MixedPart extends \Symfony\Component\Mime\Part\AbstractMultipartPart
{
    public function getMediaSubtype() : string
    {
    }
}
