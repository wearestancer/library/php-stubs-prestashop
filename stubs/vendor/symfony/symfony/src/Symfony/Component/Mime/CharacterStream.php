<?php

namespace Symfony\Component\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Xavier De Cock <xdecock@gmail.com>
 *
 * @internal
 */
final class CharacterStream
{
    /**
     * @param resource|string $input
     */
    public function __construct($input, ?string $charset = 'utf-8')
    {
    }
    public function read(int $length) : ?string
    {
    }
    public function readBytes(int $length) : ?array
    {
    }
    public function setPointer(int $charOffset) : void
    {
    }
    public function write(string $chars) : void
    {
    }
}
