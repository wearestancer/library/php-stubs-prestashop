<?php

namespace Symfony\Component\Mime\Exception;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class LogicException extends \LogicException implements \Symfony\Component\Mime\Exception\ExceptionInterface
{
}
