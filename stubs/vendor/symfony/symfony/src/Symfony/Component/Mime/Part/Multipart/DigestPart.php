<?php

namespace Symfony\Component\Mime\Part\Multipart;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class DigestPart extends \Symfony\Component\Mime\Part\AbstractMultipartPart
{
    public function __construct(\Symfony\Component\Mime\Part\MessagePart ...$parts)
    {
    }
    public function getMediaSubtype() : string
    {
    }
}
