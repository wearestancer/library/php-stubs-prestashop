<?php

namespace Symfony\Component\Mime\Encoder;

/**
 * @author Chris Corbyn
 */
class Base64Encoder implements \Symfony\Component\Mime\Encoder\EncoderInterface
{
    /**
     * Takes an unencoded string and produces a Base64 encoded string from it.
     *
     * Base64 encoded strings have a maximum line length of 76 characters.
     * If the first line needs to be shorter, indicate the difference with
     * $firstLineOffset.
     */
    public function encodeString(string $string, ?string $charset = 'utf-8', int $firstLineOffset = 0, int $maxLineLength = 0) : string
    {
    }
}
