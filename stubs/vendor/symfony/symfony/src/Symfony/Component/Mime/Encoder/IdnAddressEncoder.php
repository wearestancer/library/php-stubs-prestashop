<?php

namespace Symfony\Component\Mime\Encoder;

/**
 * An IDN email address encoder.
 *
 * Encodes the domain part of an address using IDN. This is compatible will all
 * SMTP servers.
 *
 * This encoder does not support email addresses with non-ASCII characters in
 * local-part (the substring before @).
 *
 * @author Christian Schmidt
 */
final class IdnAddressEncoder implements \Symfony\Component\Mime\Encoder\AddressEncoderInterface
{
    /**
     * Encodes the domain part of an address using IDN.
     *
     * @throws AddressEncoderException If local-part contains non-ASCII characters
     */
    public function encodeString(string $address) : string
    {
    }
}
