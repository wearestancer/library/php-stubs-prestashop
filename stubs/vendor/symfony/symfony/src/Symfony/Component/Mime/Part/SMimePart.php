<?php

namespace Symfony\Component\Mime\Part;

/**
 * @author Sebastiaan Stok <s.stok@rollerscapes.net>
 */
class SMimePart extends \Symfony\Component\Mime\Part\AbstractPart
{
    /** @internal */
    protected $_headers;
    /**
     * @param iterable|string $body
     */
    public function __construct($body, string $type, string $subtype, array $parameters)
    {
    }
    public function getMediaType() : string
    {
    }
    public function getMediaSubtype() : string
    {
    }
    public function bodyToString() : string
    {
    }
    public function bodyToIterable() : iterable
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function __sleep() : array
    {
    }
    public function __wakeup() : void
    {
    }
}
