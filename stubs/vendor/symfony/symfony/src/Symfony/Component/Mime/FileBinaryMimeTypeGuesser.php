<?php

namespace Symfony\Component\Mime;

/**
 * Guesses the MIME type with the binary "file" (only available on *nix).
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class FileBinaryMimeTypeGuesser implements \Symfony\Component\Mime\MimeTypeGuesserInterface
{
    /**
     * The $cmd pattern must contain a "%s" string that will be replaced
     * with the file name to guess.
     *
     * The command output must start with the MIME type of the file.
     *
     * @param string $cmd The command to run to get the MIME type of a file
     */
    public function __construct(string $cmd = 'file -b --mime -- %s 2>/dev/null')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isGuesserSupported() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function guessMimeType(string $path) : ?string
    {
    }
}
