<?php

namespace Symfony\Component\Mime\Header;

/**
 * A Mailbox list MIME Header for something like From, To, Cc, and Bcc (one or more named addresses).
 *
 * @author Chris Corbyn
 */
final class MailboxListHeader extends \Symfony\Component\Mime\Header\AbstractHeader
{
    /**
     * @param Address[] $addresses
     */
    public function __construct(string $name, array $addresses)
    {
    }
    /**
     * @param Address[] $body
     *
     * @throws RfcComplianceException
     */
    public function setBody($body)
    {
    }
    /**
     * @throws RfcComplianceException
     *
     * @return Address[]
     */
    public function getBody() : array
    {
    }
    /**
     * Sets a list of addresses to be shown in this Header.
     *
     * @param Address[] $addresses
     *
     * @throws RfcComplianceException
     */
    public function setAddresses(array $addresses)
    {
    }
    /**
     * Sets a list of addresses to be shown in this Header.
     *
     * @param Address[] $addresses
     *
     * @throws RfcComplianceException
     */
    public function addAddresses(array $addresses)
    {
    }
    /**
     * @throws RfcComplianceException
     */
    public function addAddress(\Symfony\Component\Mime\Address $address)
    {
    }
    /**
     * @return Address[]
     */
    public function getAddresses() : array
    {
    }
    /**
     * Gets the full mailbox list of this Header as an array of valid RFC 2822 strings.
     *
     * @throws RfcComplianceException
     *
     * @return string[]
     */
    public function getAddressStrings() : array
    {
    }
    public function getBodyAsString() : string
    {
    }
}
