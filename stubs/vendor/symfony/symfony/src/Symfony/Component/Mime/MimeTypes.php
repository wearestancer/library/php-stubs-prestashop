<?php

namespace Symfony\Component\Mime;

/**
 * Manages MIME types and file extensions.
 *
 * For MIME type guessing, you can register custom guessers
 * by calling the registerGuesser() method.
 * Custom guessers are always called before any default ones:
 *
 *     $guesser = new MimeTypes();
 *     $guesser->registerGuesser(new MyCustomMimeTypeGuesser());
 *
 * If you want to change the order of the default guessers, just re-register your
 * preferred one as a custom one. The last registered guesser is preferred over
 * previously registered ones.
 *
 * Re-registering a built-in guesser also allows you to configure it:
 *
 *     $guesser = new MimeTypes();
 *     $guesser->registerGuesser(new FileinfoMimeTypeGuesser('/path/to/magic/file'));
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class MimeTypes implements \Symfony\Component\Mime\MimeTypesInterface
{
    public function __construct(array $map = [])
    {
    }
    public static function setDefault(self $default)
    {
    }
    public static function getDefault() : self
    {
    }
    /**
     * Registers a MIME type guesser.
     *
     * The last registered guesser has precedence over the other ones.
     */
    public function registerGuesser(\Symfony\Component\Mime\MimeTypeGuesserInterface $guesser)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getExtensions(string $mimeType) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMimeTypes(string $ext) : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isGuesserSupported() : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * The file is passed to each registered MIME type guesser in reverse order
     * of their registration (last registered is queried first). Once a guesser
     * returns a value that is not null, this method terminates and returns the
     * value.
     */
    public function guessMimeType(string $path) : ?string
    {
    }
}
