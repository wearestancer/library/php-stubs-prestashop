<?php

namespace Symfony\Component\Mime\Part;

/**
 * @final
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MessagePart extends \Symfony\Component\Mime\Part\DataPart
{
    public function __construct(\Symfony\Component\Mime\RawMessage $message)
    {
    }
    public function getMediaType() : string
    {
    }
    public function getMediaSubtype() : string
    {
    }
    public function getBody() : string
    {
    }
    public function bodyToString() : string
    {
    }
    public function bodyToIterable() : iterable
    {
    }
}
