<?php

namespace Symfony\Component\Mime\Encoder;

/**
 * @author Chris Corbyn
 */
class QpEncoder implements \Symfony\Component\Mime\Encoder\EncoderInterface
{
    /**
     * A map of non-encoded ascii characters.
     *
     * @var string[]
     *
     * @internal
     */
    protected $safeMap = [];
    public function __construct()
    {
    }
    protected function initSafeMap() : void
    {
    }
    /**
     * {@inheritdoc}
     *
     * Takes an unencoded string and produces a QP encoded string from it.
     *
     * QP encoded strings have a maximum line length of 76 characters.
     * If the first line needs to be shorter, indicate the difference with
     * $firstLineOffset.
     */
    public function encodeString(string $string, ?string $charset = 'utf-8', int $firstLineOffset = 0, int $maxLineLength = 0) : string
    {
    }
}
