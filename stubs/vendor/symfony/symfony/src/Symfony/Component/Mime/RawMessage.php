<?php

namespace Symfony\Component\Mime;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RawMessage implements \Serializable
{
    /**
     * @param iterable|string $message
     */
    public function __construct($message)
    {
    }
    public function toString() : string
    {
    }
    public function toIterable() : iterable
    {
    }
    /**
     * @throws LogicException if the message is not valid
     */
    public function ensureValidity()
    {
    }
    /**
     * @internal
     */
    public final function serialize() : string
    {
    }
    /**
     * @internal
     */
    public final function unserialize($serialized)
    {
    }
    public function __serialize() : array
    {
    }
    public function __unserialize(array $data) : void
    {
    }
}
