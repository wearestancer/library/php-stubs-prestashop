<?php

namespace Symfony\Component\Mime\Part;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractPart
{
    public function __construct()
    {
    }
    public function getHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function toString() : string
    {
    }
    public function toIterable() : iterable
    {
    }
    public function asDebugString() : string
    {
    }
    public abstract function bodyToString() : string;
    public abstract function bodyToIterable() : iterable;
    public abstract function getMediaType() : string;
    public abstract function getMediaSubtype() : string;
}
