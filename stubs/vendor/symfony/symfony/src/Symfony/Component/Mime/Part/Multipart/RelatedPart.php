<?php

namespace Symfony\Component\Mime\Part\Multipart;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class RelatedPart extends \Symfony\Component\Mime\Part\AbstractMultipartPart
{
    public function __construct(\Symfony\Component\Mime\Part\AbstractPart $mainPart, \Symfony\Component\Mime\Part\AbstractPart $part, \Symfony\Component\Mime\Part\AbstractPart ...$parts)
    {
    }
    public function getParts() : array
    {
    }
    public function getMediaSubtype() : string
    {
    }
}
