<?php

namespace Symfony\Component\Mime\Part;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractMultipartPart extends \Symfony\Component\Mime\Part\AbstractPart
{
    public function __construct(\Symfony\Component\Mime\Part\AbstractPart ...$parts)
    {
    }
    /**
     * @return AbstractPart[]
     */
    public function getParts() : array
    {
    }
    public function getMediaType() : string
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function bodyToString() : string
    {
    }
    public function bodyToIterable() : iterable
    {
    }
    public function asDebugString() : string
    {
    }
}
