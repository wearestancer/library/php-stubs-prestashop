<?php

namespace Symfony\Component\Mime\Part;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TextPart extends \Symfony\Component\Mime\Part\AbstractPart
{
    /** @internal */
    protected $_headers;
    /**
     * @param resource|string $body
     */
    public function __construct($body, ?string $charset = 'utf-8', string $subtype = 'plain', string $encoding = null)
    {
    }
    public function getMediaType() : string
    {
    }
    public function getMediaSubtype() : string
    {
    }
    /**
     * @param string $disposition one of attachment, inline, or form-data
     *
     * @return $this
     */
    public function setDisposition(string $disposition)
    {
    }
    /**
     * Sets the name of the file (used by FormDataPart).
     *
     * @return $this
     */
    public function setName($name)
    {
    }
    public function getBody() : string
    {
    }
    public function bodyToString() : string
    {
    }
    public function bodyToIterable() : iterable
    {
    }
    public function getPreparedHeaders() : \Symfony\Component\Mime\Header\Headers
    {
    }
    public function asDebugString() : string
    {
    }
    /**
     * @return array
     */
    public function __sleep()
    {
    }
    public function __wakeup()
    {
    }
}
