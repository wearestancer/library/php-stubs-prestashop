<?php

namespace Symfony\Component\Mime\Header;

/**
 * An ID MIME Header for something like Message-ID or Content-ID (one or more addresses).
 *
 * @author Chris Corbyn
 */
final class IdentificationHeader extends \Symfony\Component\Mime\Header\AbstractHeader
{
    /**
     * @param string|array $ids
     */
    public function __construct(string $name, $ids)
    {
    }
    /**
     * @param string|array $body a string ID or an array of IDs
     *
     * @throws RfcComplianceException
     */
    public function setBody($body)
    {
    }
    public function getBody() : array
    {
    }
    /**
     * Set the ID used in the value of this header.
     *
     * @param string|array $id
     *
     * @throws RfcComplianceException
     */
    public function setId($id)
    {
    }
    /**
     * Get the ID used in the value of this Header.
     *
     * If multiple IDs are set only the first is returned.
     */
    public function getId() : ?string
    {
    }
    /**
     * Set a collection of IDs to use in the value of this Header.
     *
     * @param string[] $ids
     *
     * @throws RfcComplianceException
     */
    public function setIds(array $ids)
    {
    }
    /**
     * Get the list of IDs used in this Header.
     *
     * @return string[]
     */
    public function getIds() : array
    {
    }
    public function getBodyAsString() : string
    {
    }
}
