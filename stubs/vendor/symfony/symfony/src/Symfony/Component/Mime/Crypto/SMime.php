<?php

namespace Symfony\Component\Mime\Crypto;

/**
 * @author Sebastiaan Stok <s.stok@rollerscapes.net>
 *
 * @internal
 */
abstract class SMime
{
    protected function normalizeFilePath(string $path) : string
    {
    }
    protected function iteratorToFile(iterable $iterator, $stream) : void
    {
    }
    protected function convertMessageToSMimePart($stream, string $type, string $subtype) : \Symfony\Component\Mime\Part\SMimePart
    {
    }
    protected function getStreamIterator($stream) : iterable
    {
    }
}
