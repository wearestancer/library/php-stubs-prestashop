<?php

namespace Symfony\Component\Config\Resource;

/**
 * ComposerResource tracks the PHP version and Composer dependencies.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @final since Symfony 4.3
 */
class ComposerResource implements \Symfony\Component\Config\Resource\SelfCheckingResourceInterface
{
    public function __construct()
    {
    }
    public function getVendors()
    {
    }
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh($timestamp)
    {
    }
}
