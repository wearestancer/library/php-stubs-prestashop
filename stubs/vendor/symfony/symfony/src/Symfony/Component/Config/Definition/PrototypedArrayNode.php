<?php

namespace Symfony\Component\Config\Definition;

/**
 * Represents a prototyped Array node in the config tree.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class PrototypedArrayNode extends \Symfony\Component\Config\Definition\ArrayNode
{
    protected $prototype;
    protected $keyAttribute;
    protected $removeKeyAttribute = false;
    protected $minNumberOfElements = 0;
    protected $defaultValue = [];
    protected $defaultChildren;
    /**
     * Sets the minimum number of elements that a prototype based node must
     * contain. By default this is zero, meaning no elements.
     *
     * @param int $number
     */
    public function setMinNumberOfElements($number)
    {
    }
    /**
     * Sets the attribute which value is to be used as key.
     *
     * This is useful when you have an indexed array that should be an
     * associative array. You can select an item from within the array
     * to be the key of the particular item. For example, if "id" is the
     * "key", then:
     *
     *     [
     *         ['id' => 'my_name', 'foo' => 'bar'],
     *     ];
     *
     *  becomes
     *
     *      [
     *          'my_name' => ['foo' => 'bar'],
     *      ];
     *
     * If you'd like "'id' => 'my_name'" to still be present in the resulting
     * array, then you can set the second argument of this method to false.
     *
     * @param string $attribute The name of the attribute which value is to be used as a key
     * @param bool   $remove    Whether or not to remove the key
     */
    public function setKeyAttribute($attribute, $remove = true)
    {
    }
    /**
     * Retrieves the name of the attribute which value should be used as key.
     *
     * @return string|null The name of the attribute
     */
    public function getKeyAttribute()
    {
    }
    /**
     * Sets the default value of this node.
     *
     * @param string $value
     *
     * @throws \InvalidArgumentException if the default value is not an array
     */
    public function setDefaultValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasDefaultValue()
    {
    }
    /**
     * Adds default children when none are set.
     *
     * @param int|string|array|null $children The number of children|The child name|The children names to be added
     */
    public function setAddChildrenIfNoneSet($children = ['defaults'])
    {
    }
    /**
     * {@inheritdoc}
     *
     * The default value could be either explicited or derived from the prototype
     * default value.
     */
    public function getDefaultValue()
    {
    }
    /**
     * Sets the node prototype.
     */
    public function setPrototype(\Symfony\Component\Config\Definition\PrototypeNodeInterface $node)
    {
    }
    /**
     * Retrieves the prototype.
     *
     * @return PrototypeNodeInterface The prototype
     */
    public function getPrototype()
    {
    }
    /**
     * Disable adding concrete children for prototyped nodes.
     *
     * @throws Exception
     */
    public function addChild(\Symfony\Component\Config\Definition\NodeInterface $node)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function finalizeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws DuplicateKeyException
     */
    protected function normalizeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function mergeValues($leftSide, $rightSide)
    {
    }
}
