<?php

namespace Symfony\Component\Config\Util\Exception;

/**
 * Exception class for when XML cannot be parsed properly.
 *
 * @author Ole Rößner <ole@roessner.it>
 */
class XmlParsingException extends \InvalidArgumentException
{
}
