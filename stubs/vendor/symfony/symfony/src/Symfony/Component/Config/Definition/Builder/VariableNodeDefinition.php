<?php

namespace Symfony\Component\Config\Definition\Builder;

/**
 * This class provides a fluent interface for defining a node.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class VariableNodeDefinition extends \Symfony\Component\Config\Definition\Builder\NodeDefinition
{
    /**
     * Instantiate a Node.
     *
     * @return VariableNode The node
     */
    protected function instantiateNode()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function createNode()
    {
    }
}
