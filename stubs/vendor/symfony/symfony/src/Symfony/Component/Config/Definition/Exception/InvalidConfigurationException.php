<?php

namespace Symfony\Component\Config\Definition\Exception;

/**
 * A very general exception which can be thrown whenever non of the more specific
 * exceptions is suitable.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class InvalidConfigurationException extends \Symfony\Component\Config\Definition\Exception\Exception
{
    public function setPath($path)
    {
    }
    public function getPath()
    {
    }
    /**
     * Adds extra information that is suffixed to the original exception message.
     *
     * @param string $hint
     */
    public function addHint($hint)
    {
    }
}
