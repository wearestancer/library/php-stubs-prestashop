<?php

namespace Symfony\Component\Config\Resource;

/**
 * FileExistenceResource represents a resource stored on the filesystem.
 * Freshness is only evaluated against resource creation or deletion.
 *
 * The resource can be a file or a directory.
 *
 * @author Charles-Henri Bruyand <charleshenri.bruyand@gmail.com>
 *
 * @final since Symfony 4.3
 */
class FileExistenceResource implements \Symfony\Component\Config\Resource\SelfCheckingResourceInterface
{
    /**
     * @param string $resource The file path to the resource
     */
    public function __construct(string $resource)
    {
    }
    public function __toString()
    {
    }
    /**
     * @return string The file path to the resource
     */
    public function getResource()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh($timestamp)
    {
    }
}
