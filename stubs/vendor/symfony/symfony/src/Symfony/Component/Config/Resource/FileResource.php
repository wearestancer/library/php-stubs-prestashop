<?php

namespace Symfony\Component\Config\Resource;

/**
 * FileResource represents a resource stored on the filesystem.
 *
 * The resource can be a file or a directory.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class FileResource implements \Symfony\Component\Config\Resource\SelfCheckingResourceInterface
{
    /**
     * @param string $resource The file path to the resource
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $resource)
    {
    }
    public function __toString()
    {
    }
    /**
     * @return string The canonicalized, absolute path to the resource
     */
    public function getResource()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh($timestamp)
    {
    }
}
