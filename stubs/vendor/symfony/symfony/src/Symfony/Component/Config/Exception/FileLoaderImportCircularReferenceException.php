<?php

namespace Symfony\Component\Config\Exception;

/**
 * Exception class for when a circular reference is detected when importing resources.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FileLoaderImportCircularReferenceException extends \Symfony\Component\Config\Exception\LoaderLoadException
{
    public function __construct(array $resources, ?int $code = 0, \Throwable $previous = null)
    {
    }
}
