<?php

namespace Symfony\Component\Config\Definition;

/**
 * Node which only allows a finite set of values.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class EnumNode extends \Symfony\Component\Config\Definition\ScalarNode
{
    public function __construct(?string $name, \Symfony\Component\Config\Definition\NodeInterface $parent = null, array $values = [], string $pathSeparator = \Symfony\Component\Config\Definition\BaseNode::DEFAULT_PATH_SEPARATOR)
    {
    }
    public function getValues()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function finalizeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function allowPlaceholders() : bool
    {
    }
}
