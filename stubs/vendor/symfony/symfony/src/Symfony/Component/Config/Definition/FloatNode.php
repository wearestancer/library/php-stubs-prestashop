<?php

namespace Symfony\Component\Config\Definition;

/**
 * This node represents a float value in the config tree.
 *
 * @author Jeanmonod David <david.jeanmonod@gmail.com>
 */
class FloatNode extends \Symfony\Component\Config\Definition\NumericNode
{
    /**
     * {@inheritdoc}
     */
    protected function validateType($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getValidPlaceholderTypes() : array
    {
    }
}
