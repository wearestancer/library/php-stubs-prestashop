<?php

namespace Symfony\Component\Config\Resource;

/**
 * DirectoryResource represents a resources stored in a subdirectory tree.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class DirectoryResource implements \Symfony\Component\Config\Resource\SelfCheckingResourceInterface
{
    /**
     * @param string      $resource The file path to the resource
     * @param string|null $pattern  A pattern to restrict monitored files
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $resource, string $pattern = null)
    {
    }
    public function __toString()
    {
    }
    /**
     * @return string The file path to the resource
     */
    public function getResource()
    {
    }
    /**
     * Returns the pattern to restrict monitored files.
     *
     * @return string|null
     */
    public function getPattern()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh($timestamp)
    {
    }
}
