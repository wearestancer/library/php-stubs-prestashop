<?php

namespace Symfony\Component\Config\Definition\Builder;

/**
 * This is the entry class for building a config tree.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class TreeBuilder implements \Symfony\Component\Config\Definition\Builder\NodeParentInterface
{
    protected $tree;
    protected $root;
    public function __construct(string $name = null, string $type = 'array', \Symfony\Component\Config\Definition\Builder\NodeBuilder $builder = null)
    {
    }
    /**
     * Creates the root node.
     *
     * @param string $name The name of the root node
     * @param string $type The type of the root node
     *
     * @return ArrayNodeDefinition|NodeDefinition The root node (as an ArrayNodeDefinition when the type is 'array')
     *
     * @throws \RuntimeException When the node type is not supported
     *
     * @deprecated since Symfony 4.3, pass the root name to the constructor instead
     */
    public function root($name, $type = 'array', \Symfony\Component\Config\Definition\Builder\NodeBuilder $builder = null)
    {
    }
    /**
     * @return NodeDefinition|ArrayNodeDefinition The root node (as an ArrayNodeDefinition when the type is 'array')
     */
    public function getRootNode() : \Symfony\Component\Config\Definition\Builder\NodeDefinition
    {
    }
    /**
     * Builds the tree.
     *
     * @return NodeInterface
     *
     * @throws \RuntimeException
     */
    public function buildTree()
    {
    }
    public function setPathSeparator(string $separator)
    {
    }
}
