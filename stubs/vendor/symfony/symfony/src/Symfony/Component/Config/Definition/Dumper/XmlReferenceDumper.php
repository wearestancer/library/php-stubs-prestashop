<?php

namespace Symfony\Component\Config\Definition\Dumper;

/**
 * Dumps an XML reference configuration for the given configuration/node instance.
 *
 * @author Wouter J <waldio.webdesign@gmail.com>
 */
class XmlReferenceDumper
{
    public function dump(\Symfony\Component\Config\Definition\ConfigurationInterface $configuration, $namespace = null)
    {
    }
    public function dumpNode(\Symfony\Component\Config\Definition\NodeInterface $node, $namespace = null)
    {
    }
}
