<?php

namespace Symfony\Component\Config\Resource;

/**
 * Resource checker for instances of SelfCheckingResourceInterface.
 *
 * As these resources perform the actual check themselves, we can provide
 * this class as a standard way of validating them.
 *
 * @author Matthias Pigulla <mp@webfactory.de>
 */
class SelfCheckingResourceChecker implements \Symfony\Component\Config\ResourceCheckerInterface
{
    public function supports(\Symfony\Component\Config\Resource\ResourceInterface $metadata)
    {
    }
    public function isFresh(\Symfony\Component\Config\Resource\ResourceInterface $resource, $timestamp)
    {
    }
}
