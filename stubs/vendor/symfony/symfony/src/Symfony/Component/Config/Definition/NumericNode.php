<?php

namespace Symfony\Component\Config\Definition;

/**
 * This node represents a numeric value in the config tree.
 *
 * @author David Jeanmonod <david.jeanmonod@gmail.com>
 */
class NumericNode extends \Symfony\Component\Config\Definition\ScalarNode
{
    protected $min;
    protected $max;
    /**
     * @param int|float|null $min
     * @param int|float|null $max
     */
    public function __construct(?string $name, \Symfony\Component\Config\Definition\NodeInterface $parent = null, $min = null, $max = null, string $pathSeparator = \Symfony\Component\Config\Definition\BaseNode::DEFAULT_PATH_SEPARATOR)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function finalizeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function isValueEmpty($value)
    {
    }
}
