<?php

namespace Symfony\Component\Config\Definition\Dumper;

/**
 * Dumps a Yaml reference configuration for the given configuration/node instance.
 *
 * @author Kevin Bond <kevinbond@gmail.com>
 */
class YamlReferenceDumper
{
    public function dump(\Symfony\Component\Config\Definition\ConfigurationInterface $configuration)
    {
    }
    public function dumpAtPath(\Symfony\Component\Config\Definition\ConfigurationInterface $configuration, $path)
    {
    }
    public function dumpNode(\Symfony\Component\Config\Definition\NodeInterface $node)
    {
    }
}
