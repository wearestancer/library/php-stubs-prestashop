<?php

namespace Symfony\Component\Config\Resource;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @final since Symfony 4.3
 */
class ReflectionClassResource implements \Symfony\Component\Config\Resource\SelfCheckingResourceInterface
{
    public function __construct(\ReflectionClass $classReflector, array $excludedVendors = [])
    {
    }
    public function isFresh($timestamp)
    {
    }
    public function __toString()
    {
    }
    /**
     * @internal
     */
    public function __sleep() : array
    {
    }
}
