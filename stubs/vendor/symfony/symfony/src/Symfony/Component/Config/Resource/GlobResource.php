<?php

namespace Symfony\Component\Config\Resource;

/**
 * GlobResource represents a set of resources stored on the filesystem.
 *
 * Only existence/removal is tracked (not mtimes.)
 *
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @final since Symfony 4.3
 */
class GlobResource implements \IteratorAggregate, \Symfony\Component\Config\Resource\SelfCheckingResourceInterface
{
    /**
     * @param string $prefix    A directory prefix
     * @param string $pattern   A glob pattern
     * @param bool   $recursive Whether directories should be scanned recursively or not
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $prefix, string $pattern, bool $recursive, bool $forExclusion = false, array $excludedPrefixes = [])
    {
    }
    public function getPrefix()
    {
    }
    public function __toString()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh($timestamp)
    {
    }
    /**
     * @internal
     */
    public function __sleep() : array
    {
    }
    /**
     * @internal
     */
    public function __wakeup() : void
    {
    }
    /**
     * @return \Traversable
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
