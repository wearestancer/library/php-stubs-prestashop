<?php

namespace Symfony\Component\Config\Loader;

/**
 * Loader is the abstract class used by all built-in loaders.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Loader implements \Symfony\Component\Config\Loader\LoaderInterface
{
    protected $resolver;
    /**
     * {@inheritdoc}
     */
    public function getResolver()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setResolver(\Symfony\Component\Config\Loader\LoaderResolverInterface $resolver)
    {
    }
    /**
     * Imports a resource.
     *
     * @param mixed       $resource A resource
     * @param string|null $type     The resource type or null if unknown
     *
     * @return mixed
     */
    public function import($resource, $type = null)
    {
    }
    /**
     * Finds a loader able to load an imported resource.
     *
     * @param mixed       $resource A resource
     * @param string|null $type     The resource type or null if unknown
     *
     * @return $this|LoaderInterface
     *
     * @throws LoaderLoadException If no loader is found
     */
    public function resolve($resource, $type = null)
    {
    }
}
