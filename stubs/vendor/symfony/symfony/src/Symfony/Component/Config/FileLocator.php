<?php

namespace Symfony\Component\Config;

/**
 * FileLocator uses an array of pre-defined paths to find files.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FileLocator implements \Symfony\Component\Config\FileLocatorInterface
{
    protected $paths;
    /**
     * @param string|string[] $paths A path or an array of paths where to look for resources
     */
    public function __construct($paths = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function locate($name, $currentPath = null, $first = true)
    {
    }
}
