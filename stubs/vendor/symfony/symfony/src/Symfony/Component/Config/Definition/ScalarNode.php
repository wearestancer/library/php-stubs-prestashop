<?php

namespace Symfony\Component\Config\Definition;

/**
 * This node represents a scalar value in the config tree.
 *
 * The following values are considered scalars:
 *   * booleans
 *   * strings
 *   * null
 *   * integers
 *   * floats
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class ScalarNode extends \Symfony\Component\Config\Definition\VariableNode
{
    /**
     * {@inheritdoc}
     */
    protected function validateType($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function isValueEmpty($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getValidPlaceholderTypes() : array
    {
    }
}
