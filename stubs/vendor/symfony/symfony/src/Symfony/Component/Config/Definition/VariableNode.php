<?php

namespace Symfony\Component\Config\Definition;

/**
 * This node represents a value of variable type in the config tree.
 *
 * This node is intended for values of arbitrary type.
 * Any PHP type is accepted as a value.
 *
 * @author Jeremy Mikola <jmikola@gmail.com>
 */
class VariableNode extends \Symfony\Component\Config\Definition\BaseNode implements \Symfony\Component\Config\Definition\PrototypeNodeInterface
{
    protected $defaultValueSet = false;
    protected $defaultValue;
    protected $allowEmptyValue = true;
    public function setDefaultValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasDefaultValue()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultValue()
    {
    }
    /**
     * Sets if this node is allowed to have an empty value.
     *
     * @param bool $boolean True if this entity will accept empty values
     */
    public function setAllowEmptyValue($boolean)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function validateType($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function finalizeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function normalizeValue($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function mergeValues($leftSide, $rightSide)
    {
    }
    /**
     * Evaluates if the given value is to be treated as empty.
     *
     * By default, PHP's empty() function is used to test for emptiness. This
     * method may be overridden by subtypes to better match their understanding
     * of empty data.
     *
     * @param mixed $value
     *
     * @return bool
     *
     * @see finalizeValue()
     */
    protected function isValueEmpty($value)
    {
    }
}
