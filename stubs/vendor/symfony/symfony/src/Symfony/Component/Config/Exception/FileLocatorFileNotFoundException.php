<?php

namespace Symfony\Component\Config\Exception;

/**
 * File locator exception if a file does not exist.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class FileLocatorFileNotFoundException extends \InvalidArgumentException
{
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null, array $paths = [])
    {
    }
    public function getPaths()
    {
    }
}
