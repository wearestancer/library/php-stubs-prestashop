<?php

namespace Symfony\Component\Config\Definition\Builder;

/**
 * Abstract class that contains common code of integer and float node definitions.
 *
 * @author David Jeanmonod <david.jeanmonod@gmail.com>
 */
abstract class NumericNodeDefinition extends \Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition
{
    protected $min;
    protected $max;
    /**
     * Ensures that the value is smaller than the given reference.
     *
     * @param int|float $max
     *
     * @return $this
     *
     * @throws \InvalidArgumentException when the constraint is inconsistent
     */
    public function max($max)
    {
    }
    /**
     * Ensures that the value is bigger than the given reference.
     *
     * @param int|float $min
     *
     * @return $this
     *
     * @throws \InvalidArgumentException when the constraint is inconsistent
     */
    public function min($min)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws InvalidDefinitionException
     */
    public function cannotBeEmpty()
    {
    }
}
