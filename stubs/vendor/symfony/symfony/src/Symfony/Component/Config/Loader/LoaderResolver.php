<?php

namespace Symfony\Component\Config\Loader;

/**
 * LoaderResolver selects a loader for a given resource.
 *
 * A resource can be anything (e.g. a full path to a config file or a Closure).
 * Each loader determines whether it can load a resource and how.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class LoaderResolver implements \Symfony\Component\Config\Loader\LoaderResolverInterface
{
    /**
     * @param LoaderInterface[] $loaders An array of loaders
     */
    public function __construct(array $loaders = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve($resource, $type = null)
    {
    }
    public function addLoader(\Symfony\Component\Config\Loader\LoaderInterface $loader)
    {
    }
    /**
     * Returns the registered loaders.
     *
     * @return LoaderInterface[] An array of LoaderInterface instances
     */
    public function getLoaders()
    {
    }
}
