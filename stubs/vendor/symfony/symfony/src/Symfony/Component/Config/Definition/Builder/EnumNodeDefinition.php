<?php

namespace Symfony\Component\Config\Definition\Builder;

/**
 * Enum Node Definition.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class EnumNodeDefinition extends \Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition
{
    /**
     * @return $this
     */
    public function values(array $values)
    {
    }
    /**
     * Instantiate a Node.
     *
     * @return EnumNode The node
     *
     * @throws \RuntimeException
     */
    protected function instantiateNode()
    {
    }
}
