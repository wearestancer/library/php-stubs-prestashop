<?php

namespace Symfony\Component\Config\Definition;

/**
 * This node represents a Boolean value in the config tree.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class BooleanNode extends \Symfony\Component\Config\Definition\ScalarNode
{
    /**
     * {@inheritdoc}
     */
    protected function validateType($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function isValueEmpty($value)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getValidPlaceholderTypes() : array
    {
    }
}
