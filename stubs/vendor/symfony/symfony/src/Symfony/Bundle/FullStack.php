<?php

namespace Symfony\Bundle;

/**
 * A marker to be able to check if symfony/symfony is installed instead of the individual components/bundles.
 *
 * @internal
 */
final class FullStack
{
}
