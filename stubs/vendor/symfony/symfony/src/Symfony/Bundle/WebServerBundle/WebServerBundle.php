<?php

namespace Symfony\Bundle\WebServerBundle;

/**
 * @deprecated since Symfony 4.4, to be removed in 5.0; the new Symfony local server has more features, you can use it instead.
 */
class WebServerBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function boot()
    {
    }
}
