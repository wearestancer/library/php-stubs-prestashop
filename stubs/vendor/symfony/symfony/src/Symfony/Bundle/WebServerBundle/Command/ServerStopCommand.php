<?php

namespace Symfony\Bundle\WebServerBundle\Command;

/**
 * Stops a background process running a local web server.
 *
 * @author Christian Flothmann <christian.flothmann@xabbuh.de>
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0; the new Symfony local server has more features, you can use it instead.
 */
class ServerStopCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'server:stop';
    public function __construct(string $pidFileDirectory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
