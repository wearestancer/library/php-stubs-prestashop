<?php

namespace Symfony\Bundle\WebServerBundle\Command;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0; use ServerLogCommand from symfony/monolog-bridge instead
 */
class ServerLogCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'server:log';
    public function isEnabled()
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
