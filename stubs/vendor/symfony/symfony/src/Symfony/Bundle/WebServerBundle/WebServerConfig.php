<?php

namespace Symfony\Bundle\WebServerBundle;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0; the new Symfony local server has more features, you can use it instead.
 */
class WebServerConfig
{
    public function __construct(string $documentRoot, string $env, string $address = null, string $router = null)
    {
    }
    public function getDocumentRoot()
    {
    }
    public function getEnv()
    {
    }
    public function getRouter()
    {
    }
    public function getHostname()
    {
    }
    public function getPort()
    {
    }
    public function getAddress()
    {
    }
    /**
     * @return string contains resolved hostname if available, empty string otherwise
     */
    public function getDisplayAddress()
    {
    }
}
