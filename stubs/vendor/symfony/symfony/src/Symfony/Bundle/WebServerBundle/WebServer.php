<?php

namespace Symfony\Bundle\WebServerBundle;

/**
 * Manages a local HTTP web server.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0; the new Symfony local server has more features, you can use it instead.
 */
class WebServer
{
    public const STARTED = 0;
    public const STOPPED = 1;
    public function __construct(string $pidFileDirectory = null)
    {
    }
    public function run(\Symfony\Bundle\WebServerBundle\WebServerConfig $config, $disableOutput = true, callable $callback = null)
    {
    }
    public function start(\Symfony\Bundle\WebServerBundle\WebServerConfig $config, $pidFile = null)
    {
    }
    public function stop($pidFile = null)
    {
    }
    public function getAddress($pidFile = null)
    {
    }
    public function isRunning($pidFile = null)
    {
    }
}
