<?php

namespace Symfony\Bundle\WebServerBundle\Command;

/**
 * Runs Symfony application using a local web server.
 *
 * @author Michał Pipa <michal.pipa.xsolve@gmail.com>
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0; the new Symfony local server has more features, you can use it instead.
 */
class ServerRunCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'server:run';
    public function __construct(string $documentRoot = null, string $environment = null, string $pidFileDirectory = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
}
