<?php

namespace Symfony\Bundle\WebServerBundle\DependencyInjection;

/**
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @deprecated since Symfony 4.4, to be removed in 5.0; the new Symfony local server has more features, you can use it instead.
 */
class WebServerExtension extends \Symfony\Component\DependencyInjection\Extension\Extension
{
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
