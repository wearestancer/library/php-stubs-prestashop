<?php

namespace Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * ExceptionController.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.4, use the ExceptionPanelController instead.
 */
class ExceptionController
{
    protected $twig;
    protected $debug;
    protected $profiler;
    public function __construct(\Symfony\Component\HttpKernel\Profiler\Profiler $profiler = null, \Twig\Environment $twig, bool $debug, \Symfony\Component\HttpKernel\Debug\FileLinkFormatter $fileLinkFormat = null, \Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer $errorRenderer = null)
    {
    }
    /**
     * Renders the exception panel for the given token.
     *
     * @param string $token The profiler token
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function showAction($token)
    {
    }
    /**
     * Renders the exception panel stylesheet for the given token.
     *
     * @param string $token The profiler token
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function cssAction($token)
    {
    }
    protected function getTemplate()
    {
    }
    protected function templateExists($template)
    {
    }
}
