<?php

namespace Symfony\Bundle\WebProfilerBundle\DependencyInjection;

/**
 * WebProfilerExtension.
 *
 * Usage:
 *
 *     <webprofiler:config
 *        toolbar="true"
 *        intercept-redirects="true"
 *     />
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class WebProfilerExtension extends \Symfony\Component\DependencyInjection\Extension\Extension
{
    /**
     * Loads the web profiler configuration.
     *
     * @param array $configs An array of configuration settings
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getXsdValidationBasePath()
    {
    }
    public function getNamespace()
    {
    }
}
