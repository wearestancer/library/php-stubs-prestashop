<?php

namespace Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal since Symfony 4.4
 */
class ProfilerController
{
    public function __construct(\Symfony\Component\Routing\Generator\UrlGeneratorInterface $generator, \Symfony\Component\HttpKernel\Profiler\Profiler $profiler = null, \Twig\Environment $twig, array $templates, \Symfony\Bundle\WebProfilerBundle\Csp\ContentSecurityPolicyHandler $cspHandler = null, string $baseDir = null)
    {
    }
    /**
     * Redirects to the last profiles.
     *
     * @return RedirectResponse A RedirectResponse instance
     *
     * @throws NotFoundHttpException
     */
    public function homeAction()
    {
    }
    /**
     * Renders a profiler panel for the given token.
     *
     * @param string $token The profiler token
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function panelAction(\Symfony\Component\HttpFoundation\Request $request, $token)
    {
    }
    /**
     * Renders the Web Debug Toolbar.
     *
     * @param string $token The profiler token
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function toolbarAction(\Symfony\Component\HttpFoundation\Request $request, $token)
    {
    }
    /**
     * Renders the profiler search bar.
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function searchBarAction(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Renders the search results.
     *
     * @param string $token The token
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function searchResultsAction(\Symfony\Component\HttpFoundation\Request $request, $token)
    {
    }
    /**
     * Narrows the search bar.
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function searchAction(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Displays the PHP info.
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function phpinfoAction()
    {
    }
    /**
     * Displays the source of a file.
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function openAction(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * Gets the Template Manager.
     *
     * @return TemplateManager The Template Manager
     */
    protected function getTemplateManager()
    {
    }
}
