<?php

namespace Symfony\Bundle\WebProfilerBundle;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class WebProfilerBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function boot()
    {
    }
}
