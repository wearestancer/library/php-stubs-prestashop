<?php

namespace Symfony\Bundle\WebProfilerBundle\Profiler;

/**
 * Profiler Templates Manager.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Artur Wielogórski <wodor@wodor.net>
 *
 * @internal since Symfony 4.4
 */
class TemplateManager
{
    protected $twig;
    protected $templates;
    protected $profiler;
    public function __construct(\Symfony\Component\HttpKernel\Profiler\Profiler $profiler, \Twig\Environment $twig, array $templates)
    {
    }
    /**
     * Gets the template name for a given panel.
     *
     * @param string $panel
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     */
    public function getName(\Symfony\Component\HttpKernel\Profiler\Profile $profile, $panel)
    {
    }
    /**
     * Gets template names of templates that are present in the viewed profile.
     *
     * @return array
     *
     * @throws \UnexpectedValueException
     */
    public function getNames(\Symfony\Component\HttpKernel\Profiler\Profile $profile)
    {
    }
    /**
     * @deprecated since Symfony 4.4
     */
    protected function templateExists($template)
    {
    }
}
