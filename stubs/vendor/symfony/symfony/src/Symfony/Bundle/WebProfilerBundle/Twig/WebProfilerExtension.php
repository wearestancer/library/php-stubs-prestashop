<?php

namespace Symfony\Bundle\WebProfilerBundle\Twig;

/**
 * Twig extension for the profiler.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal since Symfony 4.4
 */
class WebProfilerExtension extends \Twig\Extension\ProfilerExtension
{
    public function __construct(\Symfony\Component\VarDumper\Dumper\HtmlDumper $dumper = null)
    {
    }
    /**
     * @return void
     */
    public function enter(\Twig\Profiler\Profile $profile)
    {
    }
    /**
     * @return void
     */
    public function leave(\Twig\Profiler\Profile $profile)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
    }
    public function dumpData(\Twig\Environment $env, \Symfony\Component\VarDumper\Cloner\Data $data, $maxDepth = 0)
    {
    }
    public function dumpLog(\Twig\Environment $env, $message, \Symfony\Component\VarDumper\Cloner\Data $context = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
