<?php

namespace Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * Renders the exception panel.
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 *
 * @internal
 */
class ExceptionPanelController
{
    public function __construct(\Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer $errorRenderer, \Symfony\Component\HttpKernel\Profiler\Profiler $profiler = null)
    {
    }
    /**
     * Renders the exception panel stacktrace for the given token.
     */
    public function body(string $token) : \Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Renders the exception panel stylesheet.
     */
    public function stylesheet() : \Symfony\Component\HttpFoundation\Response
    {
    }
}
