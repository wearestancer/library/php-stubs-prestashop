<?php

namespace Symfony\Bundle\WebProfilerBundle\Csp;

/**
 * Generates Content-Security-Policy nonce.
 *
 * @author Romain Neutron <imprec@gmail.com>
 *
 * @internal
 */
class NonceGenerator
{
    public function generate() : string
    {
    }
}
