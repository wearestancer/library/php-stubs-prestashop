<?php

namespace Symfony\Bundle\WebProfilerBundle\EventListener;

/**
 * WebDebugToolbarListener injects the Web Debug Toolbar.
 *
 * The onKernelResponse method must be connected to the kernel.response event.
 *
 * The WDT is only injected on well-formed HTML (with a proper </body> tag).
 * This means that the WDT is never included in sub-requests or ESI requests.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.3
 */
class WebDebugToolbarListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public const DISABLED = 1;
    public const ENABLED = 2;
    protected $twig;
    protected $urlGenerator;
    protected $interceptRedirects;
    protected $mode;
    protected $excludedAjaxPaths;
    public function __construct(\Twig\Environment $twig, bool $interceptRedirects = false, int $mode = self::ENABLED, \Symfony\Component\Routing\Generator\UrlGeneratorInterface $urlGenerator = null, string $excludedAjaxPaths = '^/bundles|^/_wdt', \Symfony\Bundle\WebProfilerBundle\Csp\ContentSecurityPolicyHandler $cspHandler = null)
    {
    }
    public function isEnabled()
    {
    }
    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
    }
    /**
     * Injects the web debug toolbar into the given Response.
     */
    protected function injectToolbar(\Symfony\Component\HttpFoundation\Response $response, \Symfony\Component\HttpFoundation\Request $request, array $nonces)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
