<?php

namespace Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * RouterController.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal since Symfony 4.4
 */
class RouterController
{
    public function __construct(\Symfony\Component\HttpKernel\Profiler\Profiler $profiler = null, \Twig\Environment $twig, \Symfony\Component\Routing\Matcher\UrlMatcherInterface $matcher = null, \Symfony\Component\Routing\RouteCollection $routes = null, iterable $expressionLanguageProviders = [])
    {
    }
    /**
     * Renders the profiler panel for the given token.
     *
     * @param string $token The profiler token
     *
     * @return Response A Response instance
     *
     * @throws NotFoundHttpException
     */
    public function panelAction($token)
    {
    }
}
