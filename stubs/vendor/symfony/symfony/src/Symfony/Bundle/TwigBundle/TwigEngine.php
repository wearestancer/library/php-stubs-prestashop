<?php

namespace Symfony\Bundle\TwigBundle;

/**
 * This engine renders Twig templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TwigEngine extends \Symfony\Bridge\Twig\TwigEngine implements \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
{
    protected $locator;
    public function __construct(\Twig\Environment $environment, \Symfony\Component\Templating\TemplateNameParserInterface $parser, \Symfony\Component\Config\FileLocatorInterface $locator)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws Error if something went wrong like a thrown exception while rendering the template
     */
    public function renderResponse($view, array $parameters = [], \Symfony\Component\HttpFoundation\Response $response = null)
    {
    }
}
