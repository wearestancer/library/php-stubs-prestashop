<?php

namespace Symfony\Bundle\TwigBundle\DependencyInjection\Compiler;

/**
 * Adds tagged twig.extension services to twig service.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TwigEnvironmentPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
