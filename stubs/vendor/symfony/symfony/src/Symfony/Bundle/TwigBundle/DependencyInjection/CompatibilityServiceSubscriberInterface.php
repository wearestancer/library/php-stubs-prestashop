<?php

namespace Symfony\Bundle\TwigBundle\DependencyInjection;

/**
 * @internal
 */
interface CompatibilityServiceSubscriberInterface extends \Symfony\Component\DependencyInjection\ServiceSubscriberInterface
{
}
