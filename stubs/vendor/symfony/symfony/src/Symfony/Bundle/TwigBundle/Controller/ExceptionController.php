<?php

namespace Symfony\Bundle\TwigBundle\Controller;

/**
 * ExceptionController renders error or exception pages for a given
 * FlattenException.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Matthias Pigulla <mp@webfactory.de>
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\HttpKernel\Controller\ErrorController instead.
 */
class ExceptionController
{
    protected $twig;
    protected $debug;
    /**
     * @param bool $debug Show error (false) or exception (true) pages by default
     */
    public function __construct(\Twig\Environment $twig, bool $debug)
    {
    }
    /**
     * Converts an Exception to a Response.
     *
     * A "showException" request parameter can be used to force display of an error page (when set to false) or
     * the exception page (when true). If it is not present, the "debug" value passed into the constructor will
     * be used.
     *
     * @return Response
     *
     * @throws \InvalidArgumentException When the exception template does not exist
     */
    public function showAction(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Debug\Exception\FlattenException $exception, \Symfony\Component\HttpKernel\Log\DebugLoggerInterface $logger = null)
    {
    }
    /**
     * @param int $startObLevel
     *
     * @return string
     */
    protected function getAndCleanOutputBuffering($startObLevel)
    {
    }
    /**
     * @param string $format
     * @param int    $code          An HTTP response status code
     * @param bool   $showException
     *
     * @return string
     */
    protected function findTemplate(\Symfony\Component\HttpFoundation\Request $request, $format, $code, $showException)
    {
    }
    // to be removed when the minimum required version of Twig is >= 2.0
    protected function templateExists($template)
    {
    }
}
