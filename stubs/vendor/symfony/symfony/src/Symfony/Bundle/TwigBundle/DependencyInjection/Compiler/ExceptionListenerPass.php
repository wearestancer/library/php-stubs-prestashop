<?php

namespace Symfony\Bundle\TwigBundle\DependencyInjection\Compiler;

/**
 * Registers the Twig exception listener if Twig is registered as a templating engine.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class ExceptionListenerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
