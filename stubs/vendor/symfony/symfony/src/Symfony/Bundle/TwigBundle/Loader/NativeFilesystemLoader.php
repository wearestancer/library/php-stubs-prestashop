<?php

namespace Symfony\Bundle\TwigBundle\Loader;

/**
 * @author Behnoush Norouzali <behnoush.norouzi@gmail.com>
 *
 * @internal
 */
class NativeFilesystemLoader extends \Twig\Loader\FilesystemLoader
{
    /**
     * {@inheritdoc}
     *
     * @return string|null
     */
    protected function findTemplate($template, $throw = true)
    {
    }
}
