<?php

namespace Symfony\Bundle\TwigBundle\Command;

/**
 * Command that will validate your template syntax and output encountered errors.
 *
 * @author Marc Weistroff <marc.weistroff@sensiolabs.com>
 * @author Jérôme Tamarelle <jerome@tamarelle.net>
 */
final class LintCommand extends \Symfony\Bridge\Twig\Command\LintCommand
{
}
