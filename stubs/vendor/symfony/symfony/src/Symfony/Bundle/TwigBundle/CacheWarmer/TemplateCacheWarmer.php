<?php

namespace Symfony\Bundle\TwigBundle\CacheWarmer;

/**
 * Generates the Twig cache for all templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TemplateCacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface, \Symfony\Bundle\TwigBundle\DependencyInjection\CompatibilityServiceSubscriberInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container, iterable $iterator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isOptional()
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
    }
}
