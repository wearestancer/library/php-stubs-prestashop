<?php

namespace Symfony\Bundle\TwigBundle;

/**
 * Bundle.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class TwigBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function registerCommands(\Symfony\Component\Console\Application $application)
    {
    }
}
