<?php

namespace Symfony\Bundle\TwigBundle\DependencyInjection\Configurator;

/**
 * Twig environment configurator.
 *
 * @author Christian Flothmann <christian.flothmann@xabbuh.de>
 */
class EnvironmentConfigurator
{
    public function __construct(string $dateFormat, string $intervalFormat, ?string $timezone, int $decimals, string $decimalPoint, string $thousandsSeparator)
    {
    }
    public function configure(\Twig\Environment $environment)
    {
    }
}
