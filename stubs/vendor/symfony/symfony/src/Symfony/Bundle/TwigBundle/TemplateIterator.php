<?php

namespace Symfony\Bundle\TwigBundle;

/**
 * Iterator for all templates in bundles and in the application Resources directory.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal since Symfony 4.4
 */
class TemplateIterator implements \IteratorAggregate
{
    /**
     * @param string      $rootDir     The directory where global templates can be stored
     * @param array       $paths       Additional Twig paths to warm
     * @param string|null $defaultPath The directory where global templates can be stored
     */
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, string $rootDir, array $paths = [], string $defaultPath = null)
    {
    }
    /**
     * @return \Traversable
     */
    #[\ReturnTypeWillChange]
    public function getIterator()
    {
    }
}
