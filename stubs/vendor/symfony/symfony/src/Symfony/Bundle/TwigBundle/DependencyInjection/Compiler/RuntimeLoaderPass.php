<?php

namespace Symfony\Bundle\TwigBundle\DependencyInjection\Compiler;

/**
 * Registers Twig runtime services.
 */
class RuntimeLoaderPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
