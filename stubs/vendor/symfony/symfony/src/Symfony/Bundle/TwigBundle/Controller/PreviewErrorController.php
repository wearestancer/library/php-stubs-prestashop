<?php

namespace Symfony\Bundle\TwigBundle\Controller;

/**
 * PreviewErrorController can be used to test error pages.
 *
 * It will create a test exception and forward it to another controller.
 *
 * @author Matthias Pigulla <mp@webfactory.de>
 *
 * @deprecated since Symfony 4.4, use the Symfony\Component\HttpKernel\Controller\ErrorController instead.
 */
class PreviewErrorController
{
    protected $kernel;
    protected $controller;
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel, $controller)
    {
    }
    public function previewErrorPageAction(\Symfony\Component\HttpFoundation\Request $request, $code)
    {
    }
}
