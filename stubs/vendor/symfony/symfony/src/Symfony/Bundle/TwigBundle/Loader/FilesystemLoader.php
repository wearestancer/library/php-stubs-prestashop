<?php

namespace Symfony\Bundle\TwigBundle\Loader;

/**
 * FilesystemLoader extends the default Twig filesystem loader
 * to work with the Symfony paths and template references.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig notation for templates instead.
 */
class FilesystemLoader extends \Twig\Loader\FilesystemLoader
{
    protected $locator;
    protected $parser;
    /**
     * @param string|null $rootPath The root path common to all relative paths (null for getcwd())
     */
    public function __construct(\Symfony\Component\Config\FileLocatorInterface $locator, \Symfony\Component\Templating\TemplateNameParserInterface $parser, string $rootPath = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * The name parameter might also be a TemplateReferenceInterface.
     *
     * @return bool
     */
    public function exists($name)
    {
    }
    /**
     * Returns the path to the template file.
     *
     * The file locator is used to locate the template when the naming convention
     * is the symfony one (i.e. the name can be parsed).
     * Otherwise the template is located using the locator from the twig library.
     *
     * @param string|TemplateReferenceInterface $template The template
     * @param bool                              $throw    When true, a LoaderError exception will be thrown if a template could not be found
     *
     * @return string The path to the template file
     *
     * @throws LoaderError if the template could not be found
     */
    protected function findTemplate($template, $throw = true)
    {
    }
}
