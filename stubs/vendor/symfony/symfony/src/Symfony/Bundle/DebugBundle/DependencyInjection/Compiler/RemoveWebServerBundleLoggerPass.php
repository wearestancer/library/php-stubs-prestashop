<?php

namespace Symfony\Bundle\DebugBundle\DependencyInjection\Compiler;

/**
 * @author Jérémy Derussé <jeremy@derusse.com>
 *
 * @internal
 */
final class RemoveWebServerBundleLoggerPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
