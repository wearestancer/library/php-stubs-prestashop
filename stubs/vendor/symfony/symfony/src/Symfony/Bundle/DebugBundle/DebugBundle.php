<?php

namespace Symfony\Bundle\DebugBundle;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class DebugBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function boot()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function registerCommands(\Symfony\Component\Console\Application $application)
    {
    }
}
