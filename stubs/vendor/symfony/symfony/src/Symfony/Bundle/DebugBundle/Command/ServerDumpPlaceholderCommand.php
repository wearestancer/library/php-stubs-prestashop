<?php

namespace Symfony\Bundle\DebugBundle\Command;

/**
 * A placeholder command easing VarDumper server discovery.
 *
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 *
 * @internal
 */
class ServerDumpPlaceholderCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Symfony\Component\VarDumper\Server\DumpServer $server = null, array $descriptors = [])
    {
    }
    protected function configure()
    {
    }
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
