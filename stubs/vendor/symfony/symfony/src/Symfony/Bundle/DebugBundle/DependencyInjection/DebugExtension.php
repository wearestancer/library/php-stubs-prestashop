<?php

namespace Symfony\Bundle\DebugBundle\DependencyInjection;

/**
 * DebugExtension.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class DebugExtension extends \Symfony\Component\DependencyInjection\Extension\Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getXsdValidationBasePath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getNamespace()
    {
    }
}
