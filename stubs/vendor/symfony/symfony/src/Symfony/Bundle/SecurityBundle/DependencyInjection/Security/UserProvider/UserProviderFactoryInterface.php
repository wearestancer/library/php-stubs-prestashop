<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider;

/**
 * UserProviderFactoryInterface is the interface for all user provider factories.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
interface UserProviderFactoryInterface
{
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config);
    public function getKey();
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $builder);
}
