<?php

namespace Symfony\Bundle\SecurityBundle\Debug;

/**
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @internal
 */
trait TraceableListenerTrait
{
    use \Symfony\Component\Security\Http\Firewall\LegacyListenerTrait;
    private $response;
    private $listener;
    private $time;
    private $stub;
    /**
     * Proxies all method calls to the original listener.
     */
    public function __call(string $method, array $arguments)
    {
    }
    public function getWrappedListener()
    {
    }
    public function getInfo() : array
    {
    }
}
