<?php

namespace Symfony\Bundle\SecurityBundle\Security;

/**
 * This is a wrapper around the actual firewall configuration which allows us
 * to lazy load the context for one specific firewall only when we need it.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class FirewallContext
{
    /**
     * @param LogoutListener|null $logoutListener
     */
    public function __construct(iterable $listeners, \Symfony\Component\Security\Http\Firewall\ExceptionListener $exceptionListener = null, $logoutListener = null, \Symfony\Bundle\SecurityBundle\Security\FirewallConfig $config = null)
    {
    }
    public function getConfig()
    {
    }
    public function getListeners() : iterable
    {
    }
    public function getExceptionListener()
    {
    }
    public function getLogoutListener()
    {
    }
}
