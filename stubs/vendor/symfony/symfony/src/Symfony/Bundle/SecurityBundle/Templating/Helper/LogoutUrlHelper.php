<?php

namespace Symfony\Bundle\SecurityBundle\Templating\Helper;

/**
 * LogoutUrlHelper provides generator functions for the logout URL.
 *
 * @author Jeremy Mikola <jmikola@gmail.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class LogoutUrlHelper extends \Symfony\Component\Templating\Helper\Helper
{
    public function __construct(\Symfony\Component\Security\Http\Logout\LogoutUrlGenerator $generator)
    {
    }
    /**
     * Generates the absolute logout path for the firewall.
     *
     * @param string|null $key The firewall key or null to use the current firewall key
     *
     * @return string The logout path
     */
    public function getLogoutPath($key)
    {
    }
    /**
     * Generates the absolute logout URL for the firewall.
     *
     * @param string|null $key The firewall key or null to use the current firewall key
     *
     * @return string The logout URL
     */
    public function getLogoutUrl($key)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
