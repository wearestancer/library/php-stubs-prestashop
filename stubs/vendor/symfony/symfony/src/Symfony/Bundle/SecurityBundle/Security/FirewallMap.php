<?php

namespace Symfony\Bundle\SecurityBundle\Security;

/**
 * This is a lazy-loading firewall map implementation.
 *
 * Listeners will only be initialized if we really need them.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class FirewallMap implements \Symfony\Component\Security\Http\FirewallMapInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container, iterable $map)
    {
    }
    public function getListeners(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    /**
     * @return FirewallConfig|null
     */
    public function getFirewallConfig(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
}
