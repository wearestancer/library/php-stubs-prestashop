<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * SecurityFactoryInterface is the interface for all security authentication listener.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
interface SecurityFactoryInterface
{
    /**
     * Configures the container services required to use the authentication listener.
     *
     * @param string      $id                  The unique id of the firewall
     * @param array       $config              The options array for the listener
     * @param string      $userProviderId      The service id of the user provider
     * @param string|null $defaultEntryPointId
     *
     * @return array containing three values:
     *               - the provider id
     *               - the listener id
     *               - the entry point id
     */
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId, $defaultEntryPointId);
    /**
     * Defines the position at which the provider is called.
     * Possible values: pre_auth, form, http, and remember_me.
     *
     * @return string
     */
    public function getPosition();
    /**
     * Defines the configuration key used to reference the provider
     * in the firewall configuration.
     *
     * @return string
     */
    public function getKey();
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $builder);
}
