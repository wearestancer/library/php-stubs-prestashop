<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * X509Factory creates services for X509 certificate authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class X509Factory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface
{
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
    }
    public function getPosition()
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
}
