<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * AbstractFactory is the base class for all classes inheriting from
 * AbstractAuthenticationListener.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Lukas Kahwe Smith <smith@pooteeweet.org>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
abstract class AbstractFactory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface
{
    protected $options = ['check_path' => '/login_check', 'use_forward' => false, 'require_previous_session' => false];
    protected $defaultSuccessHandlerOptions = ['always_use_default_target_path' => false, 'default_target_path' => '/', 'login_path' => '/login', 'target_path_parameter' => '_target_path', 'use_referer' => false];
    protected $defaultFailureHandlerOptions = ['failure_path' => null, 'failure_forward' => false, 'login_path' => '/login', 'failure_path_parameter' => '_failure_path'];
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId, $defaultEntryPointId)
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
    public final function addOption(string $name, $default = null)
    {
    }
    /**
     * Subclasses must return the id of a service which implements the
     * AuthenticationProviderInterface.
     *
     * @param string $id             The unique id of the firewall
     * @param array  $config         The options array for this listener
     * @param string $userProviderId The id of the user provider
     *
     * @return string never null, the id of the authentication provider
     */
    protected abstract function createAuthProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId);
    /**
     * Subclasses must return the id of the abstract listener template.
     *
     * Listener definitions should inherit from the AbstractAuthenticationListener
     * like this:
     *
     *    <service id="my.listener.id"
     *             class="My\Concrete\Classname"
     *             parent="security.authentication.listener.abstract"
     *             abstract="true" />
     *
     * In the above case, this method would return "my.listener.id".
     *
     * @return string
     */
    protected abstract function getListenerId();
    /**
     * Subclasses may create an entry point of their as they see fit. The
     * default implementation does not change the default entry point.
     *
     * @param ContainerBuilder $container
     * @param string           $id
     * @param array            $config
     * @param string|null      $defaultEntryPointId
     *
     * @return string|null the entry point id
     */
    protected function createEntryPoint($container, $id, $config, $defaultEntryPointId)
    {
    }
    /**
     * Subclasses may disable remember-me features for the listener, by
     * always returning false from this method.
     *
     * @return bool Whether a possibly configured RememberMeServices should be set for this listener
     */
    protected function isRememberMeAware($config)
    {
    }
    protected function createListener($container, $id, $config, $userProvider)
    {
    }
    protected function createAuthenticationSuccessHandler($container, $id, $config)
    {
    }
    protected function createAuthenticationFailureHandler($container, $id, $config)
    {
    }
    protected function getSuccessHandlerId($id)
    {
    }
    protected function getFailureHandlerId($id)
    {
    }
}
