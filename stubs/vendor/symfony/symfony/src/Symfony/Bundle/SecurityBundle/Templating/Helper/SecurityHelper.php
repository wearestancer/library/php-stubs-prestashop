<?php

namespace Symfony\Bundle\SecurityBundle\Templating\Helper;

/**
 * SecurityHelper provides read-only access to the security checker.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class SecurityHelper extends \Symfony\Component\Templating\Helper\Helper
{
    public function __construct(\Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $securityChecker = null)
    {
    }
    public function isGranted($role, $object = null, $field = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
