<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * JsonLoginFactory creates services for JSON login authentication.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class JsonLoginFactory extends \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory
{
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function createAuthProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getListenerId()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function isRememberMeAware($config)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function createListener($container, $id, $config, $userProvider)
    {
    }
}
