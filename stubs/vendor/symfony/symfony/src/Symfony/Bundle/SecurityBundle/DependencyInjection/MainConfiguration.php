<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection;

/**
 * SecurityExtension configuration structure.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class MainConfiguration implements \Symfony\Component\Config\Definition\ConfigurationInterface
{
    public function __construct(array $factories, array $userProviderFactories)
    {
    }
    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
    }
}
