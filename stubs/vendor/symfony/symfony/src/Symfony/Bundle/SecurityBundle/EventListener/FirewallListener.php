<?php

namespace Symfony\Bundle\SecurityBundle\EventListener;

/**
 * @author Maxime Steinhausser <maxime.steinhausser@gmail.com>
 */
class FirewallListener extends \Symfony\Component\Security\Http\Firewall
{
    public function __construct(\Symfony\Component\Security\Http\FirewallMapInterface $map, \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher, \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator $logoutUrlGenerator)
    {
    }
    /**
     * @internal
     */
    public function configureLogoutUrlGenerator(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
    }
    /**
     * @internal since Symfony 4.3
     */
    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
    }
}
