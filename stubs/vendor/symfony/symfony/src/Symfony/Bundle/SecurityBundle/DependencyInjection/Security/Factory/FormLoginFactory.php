<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * FormLoginFactory creates services for form login authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class FormLoginFactory extends \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory
{
    public function __construct()
    {
    }
    public function getPosition()
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
    protected function getListenerId()
    {
    }
    protected function createAuthProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId)
    {
    }
    protected function createListener($container, $id, $config, $userProvider)
    {
    }
    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
    }
}
