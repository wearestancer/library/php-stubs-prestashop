<?php

namespace Symfony\Bundle\SecurityBundle\Command;

/**
 * Encode a user's password.
 *
 * @author Sarah Khalil <mkhalil.sarah@gmail.com>
 *
 * @final
 */
class UserPasswordEncoderCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'security:encode-password';
    public function __construct(\Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface $encoderFactory, array $userClasses = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
