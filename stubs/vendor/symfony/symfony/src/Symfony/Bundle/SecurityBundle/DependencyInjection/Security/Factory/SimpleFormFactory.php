<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 *
 * @deprecated since Symfony 4.2, use Guard instead.
 */
class SimpleFormFactory extends \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\FormLoginFactory
{
    public function __construct(bool $triggerDeprecation = true)
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
    protected function getListenerId()
    {
    }
    protected function createAuthProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId)
    {
    }
    protected function createListener($container, $id, $config, $userProvider)
    {
    }
}
