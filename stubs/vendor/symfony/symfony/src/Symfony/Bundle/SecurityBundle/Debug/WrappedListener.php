<?php

namespace Symfony\Bundle\SecurityBundle\Debug;

/**
 * Wraps a security listener for calls record.
 *
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @internal since Symfony 4.3
 */
final class WrappedListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Bundle\SecurityBundle\Debug\TraceableListenerTrait;
    /**
     * @param callable $listener
     */
    public function __construct($listener)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
}
