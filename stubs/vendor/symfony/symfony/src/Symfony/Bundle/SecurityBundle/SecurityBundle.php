<?php

namespace Symfony\Bundle\SecurityBundle;

/**
 * Bundle.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SecurityBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
