<?php

namespace Symfony\Bundle\SecurityBundle\EventListener;

/**
 * Listen to vote events from traceable voters.
 *
 * @author Laurent VOULLEMIER <laurent.voullemier@gmail.com>
 *
 * @internal
 */
class VoteListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager $traceableAccessDecisionManager)
    {
    }
    /**
     * Event dispatched by a voter during access manager decision.
     */
    public function onVoterVote(\Symfony\Component\Security\Core\Event\VoteEvent $event)
    {
    }
    public static function getSubscribedEvents() : array
    {
    }
}
