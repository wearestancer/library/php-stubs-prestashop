<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * HttpBasicFactory creates services for HTTP basic authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpBasicFactory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface
{
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
    }
    public function getPosition()
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
    }
}
