<?php

namespace Symfony\Bundle\SecurityBundle\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class SecurityDataCollector extends \Symfony\Component\HttpKernel\DataCollector\DataCollector implements \Symfony\Component\HttpKernel\DataCollector\LateDataCollectorInterface
{
    public function __construct(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage = null, \Symfony\Component\Security\Core\Role\RoleHierarchyInterface $roleHierarchy = null, \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator $logoutUrlGenerator = null, \Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface $accessDecisionManager = null, \Symfony\Component\Security\Http\FirewallMapInterface $firewallMap = null, \Symfony\Bundle\SecurityBundle\Debug\TraceableFirewallListener $firewall = null)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param \Throwable|null $exception
     */
    public function collect(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    public function lateCollect()
    {
    }
    /**
     * Checks if security is enabled.
     *
     * @return bool true if security is enabled, false otherwise
     */
    public function isEnabled()
    {
    }
    /**
     * Gets the user.
     *
     * @return string The user
     */
    public function getUser()
    {
    }
    /**
     * Gets the roles of the user.
     *
     * @return array|Data
     */
    public function getRoles()
    {
    }
    /**
     * Gets the inherited roles of the user.
     *
     * @return array|Data
     */
    public function getInheritedRoles()
    {
    }
    /**
     * Checks if the data contains information about inherited roles. Still the inherited
     * roles can be an empty array.
     *
     * @return bool true if the profile was contains inherited role information
     */
    public function supportsRoleHierarchy()
    {
    }
    /**
     * Checks if the user is authenticated or not.
     *
     * @return bool true if the user is authenticated, false otherwise
     */
    public function isAuthenticated()
    {
    }
    /**
     * @return bool
     */
    public function isImpersonated()
    {
    }
    /**
     * @return string|null
     */
    public function getImpersonatorUser()
    {
    }
    /**
     * @return string|null
     */
    public function getImpersonationExitPath()
    {
    }
    /**
     * Get the class name of the security token.
     *
     * @return string|Data|null The token
     */
    public function getTokenClass()
    {
    }
    /**
     * Get the full security token class as Data object.
     *
     * @return Data|null
     */
    public function getToken()
    {
    }
    /**
     * Get the logout URL.
     *
     * @return string|null The logout URL
     */
    public function getLogoutUrl()
    {
    }
    /**
     * Returns the FQCN of the security voters enabled in the application.
     *
     * @return string[]|Data
     */
    public function getVoters()
    {
    }
    /**
     * Returns the strategy configured for the security voters.
     *
     * @return string
     */
    public function getVoterStrategy()
    {
    }
    /**
     * Returns the log of the security decisions made by the access decision manager.
     *
     * @return array|Data
     */
    public function getAccessDecisionLog()
    {
    }
    /**
     * Returns the configuration of the current firewall context.
     *
     * @return array|Data|null
     */
    public function getFirewall()
    {
    }
    /**
     * @return array|Data
     */
    public function getListeners()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
