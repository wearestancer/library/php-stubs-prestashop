<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection;

/**
 * SecurityExtension.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class SecurityExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension implements \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface
{
    public function __construct()
    {
    }
    public function prepend(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    public function addSecurityListenerFactory(\Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface $factory)
    {
    }
    public function addUserProviderFactory(\Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider\UserProviderFactoryInterface $factory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getXsdValidationBasePath()
    {
    }
    public function getNamespace()
    {
    }
    public function getConfiguration(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
