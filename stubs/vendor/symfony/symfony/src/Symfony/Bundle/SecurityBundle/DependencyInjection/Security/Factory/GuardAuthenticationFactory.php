<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * Configures the "guard" authentication provider key under a firewall.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 */
class GuardAuthenticationFactory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface
{
    public function getPosition()
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
    }
}
