<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Compiler;

/**
 * Adds all configured security voters to the access decision manager.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class AddSecurityVotersPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
    /**
     * {@inheritdoc}
     */
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
