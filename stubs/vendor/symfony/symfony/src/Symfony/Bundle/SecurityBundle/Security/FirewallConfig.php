<?php

namespace Symfony\Bundle\SecurityBundle\Security;

/**
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
final class FirewallConfig
{
    public function __construct(string $name, string $userChecker, string $requestMatcher = null, bool $securityEnabled = true, bool $stateless = false, string $provider = null, string $context = null, string $entryPoint = null, string $accessDeniedHandler = null, string $accessDeniedUrl = null, array $listeners = [], $switchUser = null)
    {
    }
    public function getName() : string
    {
    }
    /**
     * @return string|null The request matcher service id or null if neither the request matcher, pattern or host
     *                     options were provided
     */
    public function getRequestMatcher() : ?string
    {
    }
    public function isSecurityEnabled() : bool
    {
    }
    public function allowsAnonymous() : bool
    {
    }
    public function isStateless() : bool
    {
    }
    public function getProvider() : ?string
    {
    }
    /**
     * @return string|null The context key (will be null if the firewall is stateless)
     */
    public function getContext() : ?string
    {
    }
    public function getEntryPoint() : ?string
    {
    }
    public function getUserChecker() : string
    {
    }
    public function getAccessDeniedHandler() : ?string
    {
    }
    public function getAccessDeniedUrl() : ?string
    {
    }
    public function getListeners() : array
    {
    }
    public function getSwitchUser() : ?array
    {
    }
}
