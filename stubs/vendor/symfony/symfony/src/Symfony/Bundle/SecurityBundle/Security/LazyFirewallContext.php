<?php

namespace Symfony\Bundle\SecurityBundle\Security;

/**
 * Lazily calls authentication listeners when actually required by the access listener.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
class LazyFirewallContext extends \Symfony\Bundle\SecurityBundle\Security\FirewallContext
{
    public function __construct(iterable $listeners, ?\Symfony\Component\Security\Http\Firewall\ExceptionListener $exceptionListener, ?\Symfony\Component\Security\Http\Firewall\LogoutListener $logoutListener, ?\Symfony\Bundle\SecurityBundle\Security\FirewallConfig $config, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage)
    {
    }
    public function getListeners() : iterable
    {
    }
    public function __invoke(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
}
