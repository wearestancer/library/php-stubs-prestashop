<?php

namespace Symfony\Bundle\SecurityBundle\Debug;

/**
 * Firewall collecting called listeners.
 *
 * @author Robin Chalas <robin.chalas@gmail.com>
 */
final class TraceableFirewallListener extends \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener
{
    public function getWrappedListeners()
    {
    }
}
