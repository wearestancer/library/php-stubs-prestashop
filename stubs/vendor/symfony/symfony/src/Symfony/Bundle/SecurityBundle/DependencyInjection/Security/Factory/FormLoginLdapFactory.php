<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * FormLoginLdapFactory creates services for form login ldap authentication.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class FormLoginLdapFactory extends \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\FormLoginFactory
{
    protected function createAuthProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId)
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
    public function getKey()
    {
    }
}
