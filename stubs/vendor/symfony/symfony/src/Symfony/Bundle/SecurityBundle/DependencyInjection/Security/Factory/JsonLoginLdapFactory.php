<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

/**
 * JsonLoginLdapFactory creates services for json login ldap authentication.
 */
class JsonLoginLdapFactory extends \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\JsonLoginFactory
{
    public function getKey()
    {
    }
    protected function createAuthProvider(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProviderId)
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
}
