<?php

namespace Symfony\Bundle\SecurityBundle\CacheWarmer;

class ExpressionCacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface
{
    /**
     * @param iterable|Expression[] $expressions
     */
    public function __construct(iterable $expressions, \Symfony\Component\Security\Core\Authorization\ExpressionLanguage $expressionLanguage)
    {
    }
    public function isOptional()
    {
    }
    public function warmUp($cacheDir)
    {
    }
}
