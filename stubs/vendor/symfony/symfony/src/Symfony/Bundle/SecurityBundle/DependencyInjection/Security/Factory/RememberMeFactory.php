<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory;

class RememberMeFactory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface
{
    protected $options = ['name' => 'REMEMBERME', 'lifetime' => 31536000, 'path' => '/', 'domain' => null, 'secure' => false, 'httponly' => true, 'samesite' => null, 'always_remember_me' => false, 'remember_me_parameter' => '_remember_me'];
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
    }
    public function getPosition()
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
}
