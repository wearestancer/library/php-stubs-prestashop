<?php

namespace Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider;

/**
 * InMemoryFactory creates services for the memory provider.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class InMemoryFactory implements \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider\UserProviderFactoryInterface
{
    public function create(\Symfony\Component\DependencyInjection\ContainerBuilder $container, $id, $config)
    {
    }
    public function getKey()
    {
    }
    public function addConfiguration(\Symfony\Component\Config\Definition\Builder\NodeDefinition $node)
    {
    }
}
