<?php

namespace Symfony\Bundle\SecurityBundle\Debug;

/**
 * Wraps a lazy security listener.
 *
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @internal
 */
final class WrappedLazyListener extends \Symfony\Component\Security\Http\Firewall\AbstractListener implements \Symfony\Component\Security\Http\Firewall\ListenerInterface
{
    use \Symfony\Bundle\SecurityBundle\Debug\TraceableListenerTrait;
    public function __construct(\Symfony\Component\Security\Http\Firewall\AbstractListener $listener)
    {
    }
    public function supports(\Symfony\Component\HttpFoundation\Request $request) : ?bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function authenticate(\Symfony\Component\HttpKernel\Event\RequestEvent $event)
    {
    }
}
