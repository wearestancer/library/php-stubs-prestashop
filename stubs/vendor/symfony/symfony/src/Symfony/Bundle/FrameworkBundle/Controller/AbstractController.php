<?php

namespace Symfony\Bundle\FrameworkBundle\Controller;

/**
 * Provides common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class AbstractController implements \Symfony\Contracts\Service\ServiceSubscriberInterface
{
    use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @internal
     * @required
     */
    public function setContainer(\Psr\Container\ContainerInterface $container) : ?\Psr\Container\ContainerInterface
    {
    }
    /**
     * Gets a container parameter by its name.
     *
     * @return array|bool|float|int|string|\UnitEnum|null
     *
     * @final
     */
    protected function getParameter(string $name)
    {
    }
    public static function getSubscribedServices()
    {
    }
}
