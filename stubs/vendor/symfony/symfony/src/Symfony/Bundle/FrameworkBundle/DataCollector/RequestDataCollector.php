<?php

namespace Symfony\Bundle\FrameworkBundle\DataCollector;

/**
 * RequestDataCollector.
 *
 * @author Jules Pietri <jusles@heahprod.com>
 *
 * @deprecated since Symfony 4.1
 */
class RequestDataCollector extends \Symfony\Component\HttpKernel\DataCollector\RequestDataCollector
{
}
