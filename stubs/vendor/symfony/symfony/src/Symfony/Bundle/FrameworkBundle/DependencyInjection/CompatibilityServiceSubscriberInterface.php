<?php

namespace Symfony\Bundle\FrameworkBundle\DependencyInjection;

/**
 * @internal
 */
interface CompatibilityServiceSubscriberInterface extends \Symfony\Component\DependencyInjection\ServiceSubscriberInterface
{
}
