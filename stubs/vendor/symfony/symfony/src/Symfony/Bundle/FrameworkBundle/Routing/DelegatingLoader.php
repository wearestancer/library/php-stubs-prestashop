<?php

namespace Symfony\Bundle\FrameworkBundle\Routing;

/**
 * DelegatingLoader delegates route loading to other loaders using a loader resolver.
 *
 * This implementation resolves the _controller attribute from the short notation
 * to the fully-qualified form (from a:b:c to class::method).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class DelegatingLoader extends \Symfony\Component\Config\Loader\DelegatingLoader
{
    /**
     * @deprecated since Symfony 4.4
     */
    protected $parser;
    /**
     * @param LoaderResolverInterface $resolver
     * @param array                   $defaultOptions
     */
    public function __construct($resolver, $defaultOptions = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load($resource, $type = null)
    {
    }
}
