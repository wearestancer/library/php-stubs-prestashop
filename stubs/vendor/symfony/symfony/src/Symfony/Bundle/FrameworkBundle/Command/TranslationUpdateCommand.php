<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A command that parses templates to extract translation messages and adds them
 * into the translation files.
 *
 * @author Michel Salib <michelsalib@hotmail.com>
 *
 * @final
 */
class TranslationUpdateCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'translation:update';
    public function __construct(\Symfony\Component\Translation\Writer\TranslationWriterInterface $writer, \Symfony\Component\Translation\Reader\TranslationReaderInterface $reader, \Symfony\Component\Translation\Extractor\ExtractorInterface $extractor, string $defaultLocale, string $defaultTransPath = null, string $defaultViewsPath = null, array $transPaths = [], array $viewsPaths = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
