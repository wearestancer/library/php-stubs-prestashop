<?php

namespace Symfony\Bundle\FrameworkBundle\Routing;

/**
 * Marker interface for service route loaders.
 */
interface RouteLoaderInterface
{
}
