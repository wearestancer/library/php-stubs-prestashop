<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

/**
 * Ideas borrowed from Laravel Dusk's assertions.
 *
 * @see https://laravel.com/docs/5.7/dusk#available-assertions
 */
trait BrowserKitAssertionsTrait
{
    public static function assertResponseIsSuccessful(string $message = '') : void
    {
    }
    public static function assertResponseStatusCodeSame(int $expectedCode, string $message = '') : void
    {
    }
    public static function assertResponseRedirects(string $expectedLocation = null, int $expectedCode = null, string $message = '') : void
    {
    }
    public static function assertResponseHasHeader(string $headerName, string $message = '') : void
    {
    }
    public static function assertResponseNotHasHeader(string $headerName, string $message = '') : void
    {
    }
    public static function assertResponseHeaderSame(string $headerName, string $expectedValue, string $message = '') : void
    {
    }
    public static function assertResponseHeaderNotSame(string $headerName, string $expectedValue, string $message = '') : void
    {
    }
    public static function assertResponseHasCookie(string $name, string $path = '/', string $domain = null, string $message = '') : void
    {
    }
    public static function assertResponseNotHasCookie(string $name, string $path = '/', string $domain = null, string $message = '') : void
    {
    }
    public static function assertResponseCookieValueSame(string $name, string $expectedValue, string $path = '/', string $domain = null, string $message = '') : void
    {
    }
    public static function assertBrowserHasCookie(string $name, string $path = '/', string $domain = null, string $message = '') : void
    {
    }
    public static function assertBrowserNotHasCookie(string $name, string $path = '/', string $domain = null, string $message = '') : void
    {
    }
    public static function assertBrowserCookieValueSame(string $name, string $expectedValue, bool $raw = false, string $path = '/', string $domain = null, string $message = '') : void
    {
    }
    public static function assertRequestAttributeValueSame(string $name, string $expectedValue, string $message = '') : void
    {
    }
    public static function assertRouteSame($expectedRoute, array $parameters = [], string $message = '') : void
    {
    }
    private static function getClient(\Symfony\Component\BrowserKit\AbstractBrowser $newClient = null) : ?\Symfony\Component\BrowserKit\AbstractBrowser
    {
    }
    private static function getResponse() : \Symfony\Component\HttpFoundation\Response
    {
    }
    private static function getRequest() : \Symfony\Component\HttpFoundation\Request
    {
    }
}
