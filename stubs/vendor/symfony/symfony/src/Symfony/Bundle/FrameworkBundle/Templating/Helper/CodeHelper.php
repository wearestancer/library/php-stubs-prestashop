<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal since Symfony 4.2
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class CodeHelper extends \Symfony\Component\Templating\Helper\Helper
{
    protected $fileLinkFormat;
    protected $rootDir;
    // to be renamed $projectDir in 5.0
    protected $charset;
    /**
     * @param string|FileLinkFormatter $fileLinkFormat The format for links to source files
     * @param string                   $projectDir     The project root directory
     * @param string                   $charset        The charset
     */
    public function __construct($fileLinkFormat, string $projectDir, string $charset)
    {
    }
    /**
     * Formats an array as a string.
     *
     * @param array $args The argument array
     *
     * @return string
     */
    public function formatArgsAsText(array $args)
    {
    }
    public function abbrClass($class)
    {
    }
    public function abbrMethod($method)
    {
    }
    /**
     * Formats an array as a string.
     *
     * @param array $args The argument array
     *
     * @return string
     */
    public function formatArgs(array $args)
    {
    }
    /**
     * Returns an excerpt of a code file around the given line number.
     *
     * @param string $file A file path
     * @param int    $line The selected line number
     *
     * @return string|null An HTML string
     */
    public function fileExcerpt($file, $line)
    {
    }
    /**
     * Formats a file path.
     *
     * @param string $file An absolute file path
     * @param int    $line The line number
     * @param string $text Use this text for the link rather than the file path
     *
     * @return string
     */
    public function formatFile($file, $line, $text = null)
    {
    }
    /**
     * Returns the link for a given file/line pair.
     *
     * @param string $file An absolute file path
     * @param int    $line The line number
     *
     * @return string A link of false
     */
    public function getFileLink($file, $line)
    {
    }
    public function formatFileFromText($text)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    protected static function fixCodeMarkup($line)
    {
    }
}
