<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Cache pool pruner command.
 *
 * @author Rob Frawley 2nd <rmf@src.run>
 */
final class CachePoolPruneCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @param iterable|PruneableInterface[] $pools
     */
    public function __construct(iterable $pools)
    {
    }
}
