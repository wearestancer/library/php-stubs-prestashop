<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

/**
 * Ideas borrowed from Laravel Dusk's assertions.
 *
 * @see https://laravel.com/docs/5.7/dusk#available-assertions
 */
trait DomCrawlerAssertionsTrait
{
    public static function assertSelectorExists(string $selector, string $message = '') : void
    {
    }
    public static function assertSelectorNotExists(string $selector, string $message = '') : void
    {
    }
    public static function assertSelectorTextContains(string $selector, string $text, string $message = '') : void
    {
    }
    public static function assertSelectorTextSame(string $selector, string $text, string $message = '') : void
    {
    }
    public static function assertSelectorTextNotContains(string $selector, string $text, string $message = '') : void
    {
    }
    public static function assertPageTitleSame(string $expectedTitle, string $message = '') : void
    {
    }
    public static function assertPageTitleContains(string $expectedTitle, string $message = '') : void
    {
    }
    public static function assertInputValueSame(string $fieldName, string $expectedValue, string $message = '') : void
    {
    }
    public static function assertInputValueNotSame(string $fieldName, string $expectedValue, string $message = '') : void
    {
    }
    private static function getCrawler() : \Symfony\Component\DomCrawler\Crawler
    {
    }
}
