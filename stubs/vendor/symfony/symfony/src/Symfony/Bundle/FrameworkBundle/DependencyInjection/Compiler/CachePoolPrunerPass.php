<?php

namespace Symfony\Bundle\FrameworkBundle\DependencyInjection\Compiler;

/**
 * @author Rob Frawley 2nd <rmf@src.run>
 *
 * @deprecated since Symfony 4.2, use Symfony\Component\Cache\DependencyInjection\CachePoolPrunerPass instead.
 */
class CachePoolPrunerPass extends \Symfony\Component\Cache\DependencyInjection\CachePoolPrunerPass
{
}
