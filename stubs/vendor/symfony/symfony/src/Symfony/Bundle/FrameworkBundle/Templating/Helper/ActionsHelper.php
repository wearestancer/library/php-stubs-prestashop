<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * ActionsHelper manages action inclusions.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class ActionsHelper extends \Symfony\Component\Templating\Helper\Helper
{
    public function __construct(\Symfony\Component\HttpKernel\Fragment\FragmentHandler $handler)
    {
    }
    /**
     * Returns the fragment content for a given URI.
     *
     * @param string $uri
     *
     * @return string The fragment content
     *
     * @see FragmentHandler::render()
     */
    public function render($uri, array $options = [])
    {
    }
    public function controller($controller, $attributes = [], $query = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
