<?php

namespace Symfony\Bundle\FrameworkBundle\Templating;

/**
 * This engine knows how to render Symfony templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class PhpEngine extends \Symfony\Component\Templating\PhpEngine implements \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
{
    protected $container;
    public function __construct(\Symfony\Component\Templating\TemplateNameParserInterface $parser, \Psr\Container\ContainerInterface $container, \Symfony\Component\Templating\Loader\LoaderInterface $loader, \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $globals = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setHelpers(array $helpers)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderResponse($view, array $parameters = [], \Symfony\Component\HttpFoundation\Response $response = null)
    {
    }
}
