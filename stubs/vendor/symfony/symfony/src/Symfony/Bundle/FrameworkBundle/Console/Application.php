<?php

namespace Symfony\Bundle\FrameworkBundle\Console;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Application extends \Symfony\Component\Console\Application
{
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel)
    {
    }
    /**
     * Gets the Kernel associated with this Console.
     *
     * @return KernelInterface A KernelInterface instance
     */
    public function getKernel()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * Runs the current application.
     *
     * @return int 0 if everything went fine, or an error code
     */
    public function doRun(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doRunCommand(\Symfony\Component\Console\Command\Command $command, \Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function find($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function all($namespace = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLongVersion()
    {
    }
    public function add(\Symfony\Component\Console\Command\Command $command)
    {
    }
    protected function registerCommands()
    {
    }
}
