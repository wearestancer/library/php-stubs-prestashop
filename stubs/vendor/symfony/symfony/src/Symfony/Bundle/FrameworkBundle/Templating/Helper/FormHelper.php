<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * FormHelper provides helpers to help display forms.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bernhard Schussek <bschussek@gmail.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class FormHelper extends \Symfony\Component\Templating\Helper\Helper
{
    public function __construct(\Symfony\Component\Form\FormRendererInterface $renderer)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
    /**
     * Sets a theme for a given view.
     *
     * The theme format is "<Bundle>:<Controller>".
     *
     * @param string|array $themes           A theme or an array of theme
     * @param bool         $useDefaultThemes If true, will use default themes defined in the renderer
     */
    public function setTheme(\Symfony\Component\Form\FormView $view, $themes, $useDefaultThemes = true)
    {
    }
    /**
     * Renders the HTML for a form.
     *
     * Example usage:
     *
     *     <?php echo view['form']->form($form) ?>
     *
     * You can pass options during the call:
     *
     *     <?php echo view['form']->form($form, ['attr' => ['class' => 'foo']]) ?>
     *
     *     <?php echo view['form']->form($form, ['separator' => '+++++']) ?>
     *
     * This method is mainly intended for prototyping purposes. If you want to
     * control the layout of a form in a more fine-grained manner, you are
     * advised to use the other helper methods for rendering the parts of the
     * form individually. You can also create a custom form theme to adapt
     * the look of the form.
     *
     * @param array $variables Additional variables passed to the template
     *
     * @return string The HTML markup
     */
    public function form(\Symfony\Component\Form\FormView $view, array $variables = [])
    {
    }
    /**
     * Renders the form start tag.
     *
     * Example usage templates:
     *
     *     <?php echo $view['form']->start($form) ?>>
     *
     * @param array $variables Additional variables passed to the template
     *
     * @return string The HTML markup
     */
    public function start(\Symfony\Component\Form\FormView $view, array $variables = [])
    {
    }
    /**
     * Renders the form end tag.
     *
     * Example usage templates:
     *
     *     <?php echo $view['form']->end($form) ?>>
     *
     * @param array $variables Additional variables passed to the template
     *
     * @return string The HTML markup
     */
    public function end(\Symfony\Component\Form\FormView $view, array $variables = [])
    {
    }
    /**
     * Renders the HTML for a given view.
     *
     * Example usage:
     *
     *     <?php echo $view['form']->widget($form) ?>
     *
     * You can pass options during the call:
     *
     *     <?php echo $view['form']->widget($form, ['attr' => ['class' => 'foo']]) ?>
     *
     *     <?php echo $view['form']->widget($form, ['separator' => '+++++']) ?>
     *
     * @param array $variables Additional variables passed to the template
     *
     * @return string The HTML markup
     */
    public function widget(\Symfony\Component\Form\FormView $view, array $variables = [])
    {
    }
    /**
     * Renders the entire form field "row".
     *
     * @param array $variables Additional variables passed to the template
     *
     * @return string The HTML markup
     */
    public function row(\Symfony\Component\Form\FormView $view, array $variables = [])
    {
    }
    /**
     * Renders the label of the given view.
     *
     * @param string $label     The label
     * @param array  $variables Additional variables passed to the template
     *
     * @return string The HTML markup
     */
    public function label(\Symfony\Component\Form\FormView $view, $label = null, array $variables = [])
    {
    }
    /**
     * Renders the help of the given view.
     *
     * @return string The HTML markup
     */
    public function help(\Symfony\Component\Form\FormView $view) : string
    {
    }
    /**
     * Renders the errors of the given view.
     *
     * @return string The HTML markup
     */
    public function errors(\Symfony\Component\Form\FormView $view)
    {
    }
    /**
     * Renders views which have not already been rendered.
     *
     * @param array $variables An array of variables
     *
     * @return string The HTML markup
     */
    public function rest(\Symfony\Component\Form\FormView $view, array $variables = [])
    {
    }
    /**
     * Renders a block of the template.
     *
     * @param string $blockName The name of the block to render
     * @param array  $variables The variable to pass to the template
     *
     * @return string The HTML markup
     */
    public function block(\Symfony\Component\Form\FormView $view, $blockName, array $variables = [])
    {
    }
    /**
     * Returns a CSRF token.
     *
     * Use this helper for CSRF protection without the overhead of creating a
     * form.
     *
     *     echo $view['form']->csrfToken('rm_user_'.$user->getId());
     *
     * Check the token in your action using the same CSRF token id.
     *
     *     // $csrfProvider being an instance of Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface
     *     if (!$csrfProvider->isCsrfTokenValid('rm_user_'.$user->getId(), $token)) {
     *         throw new \RuntimeException('CSRF attack detected.');
     *     }
     *
     * @param string $tokenId The CSRF token id of the protected action
     *
     * @return string A CSRF token
     *
     * @throws \BadMethodCallException when no CSRF provider was injected in the constructor
     */
    public function csrfToken($tokenId)
    {
    }
    public function humanize($text)
    {
    }
    /**
     * @internal
     */
    public function formEncodeCurrency($text, $widget = '')
    {
    }
}
