<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Delete an item from a cache pool.
 *
 * @author Pierre du Plessis <pdples@gmail.com>
 */
final class CachePoolDeleteCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer $poolClearer)
    {
    }
}
