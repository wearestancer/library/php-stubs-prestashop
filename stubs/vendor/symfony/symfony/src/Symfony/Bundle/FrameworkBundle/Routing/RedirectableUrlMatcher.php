<?php

namespace Symfony\Bundle\FrameworkBundle\Routing;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.3
 */
class RedirectableUrlMatcher extends \Symfony\Component\Routing\Matcher\RedirectableUrlMatcher
{
    /**
     * Redirects the user to another URL.
     *
     * @param string $path   The path info to redirect to
     * @param string $route  The route that matched
     * @param string $scheme The URL scheme (null to keep the current one)
     *
     * @return array An array of parameters
     */
    public function redirect($path, $route, $scheme = null)
    {
    }
}
