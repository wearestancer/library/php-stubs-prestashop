<?php

namespace Symfony\Bundle\FrameworkBundle\Templating;

/**
 * TemplateNameParser converts template names from the short notation
 * "bundle:section:template.format.engine" to TemplateReferenceInterface
 * instances.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplateNameParser extends \Symfony\Component\Templating\TemplateNameParser
{
    protected $kernel;
    protected $cache = [];
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function parse($name)
    {
    }
}
