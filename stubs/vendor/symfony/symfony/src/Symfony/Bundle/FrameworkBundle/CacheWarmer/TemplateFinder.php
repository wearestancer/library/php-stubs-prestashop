<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Finds all the templates.
 *
 * @author Victor Berchet <victor@suumit.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplateFinder implements \Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplateFinderInterface
{
    /**
     * @param string $rootDir The directory where global templates can be stored
     */
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, \Symfony\Component\Templating\TemplateNameParserInterface $parser, string $rootDir)
    {
    }
    /**
     * Find all the templates in the bundle and in the kernel Resources folder.
     *
     * @return TemplateReferenceInterface[]
     */
    public function findAllTemplates()
    {
    }
}
