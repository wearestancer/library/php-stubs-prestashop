<?php

namespace Symfony\Bundle\FrameworkBundle\Templating;

/**
 * Times the time spent to render a template.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TimedPhpEngine extends \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine
{
    protected $stopwatch;
    public function __construct(\Symfony\Component\Templating\TemplateNameParserInterface $parser, \Psr\Container\ContainerInterface $container, \Symfony\Component\Templating\Loader\LoaderInterface $loader, \Symfony\Component\Stopwatch\Stopwatch $stopwatch, \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $globals = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function render($name, array $parameters = [])
    {
    }
}
