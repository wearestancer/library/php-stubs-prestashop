<?php

namespace Symfony\Bundle\FrameworkBundle\Secrets;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
abstract class AbstractVault
{
    protected $lastMessage;
    public function getLastMessage() : ?string
    {
    }
    public abstract function generateKeys(bool $override = false) : bool;
    public abstract function seal(string $name, string $value) : void;
    public abstract function reveal(string $name) : ?string;
    public abstract function remove(string $name) : bool;
    public abstract function list(bool $reveal = false) : array;
    protected function validateName(string $name) : void
    {
    }
    protected function getPrettyPath(string $path)
    {
    }
}
