<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * @author Tobias Schultze <http://tobion.de>
 * @author Jérémy Derussé <jeremy@derusse.com>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class SecretsListCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault $vault, \Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault $localVault = null)
    {
    }
}
