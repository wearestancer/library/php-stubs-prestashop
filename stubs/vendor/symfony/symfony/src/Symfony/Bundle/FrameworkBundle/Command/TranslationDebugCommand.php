<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Helps finding unused or missing translation messages in a given locale
 * and comparing them with the fallback ones.
 *
 * @author Florian Voutzinos <florian@voutzinos.com>
 *
 * @final
 */
class TranslationDebugCommand extends \Symfony\Component\Console\Command\Command
{
    public const MESSAGE_MISSING = 0;
    public const MESSAGE_UNUSED = 1;
    public const MESSAGE_EQUALS_FALLBACK = 2;
    protected static $defaultName = 'debug:translation';
    /**
     * @param TranslatorInterface $translator
     */
    public function __construct($translator, \Symfony\Component\Translation\Reader\TranslationReaderInterface $reader, \Symfony\Component\Translation\Extractor\ExtractorInterface $extractor, string $defaultTransPath = null, string $defaultViewsPath = null, array $transPaths = [], array $viewsPaths = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
