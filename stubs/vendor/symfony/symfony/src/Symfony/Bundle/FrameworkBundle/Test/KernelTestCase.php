<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

/**
 * KernelTestCase is the base class for tests needing a Kernel.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class KernelTestCase extends \PHPUnit\Framework\TestCase
{
    use \Symfony\Bundle\FrameworkBundle\Test\ForwardCompatTestTrait;
    protected static $class;
    /**
     * @var KernelInterface
     */
    protected static $kernel;
    /**
     * @var ContainerInterface
     */
    protected static $container;
    protected static $booted = false;
    /**
     * @return string The Kernel class name
     *
     * @throws \RuntimeException
     * @throws \LogicException
     */
    protected static function getKernelClass()
    {
    }
    /**
     * Boots the Kernel for this test.
     *
     * @return KernelInterface A KernelInterface instance
     */
    protected static function bootKernel(array $options = [])
    {
    }
    /**
     * Creates a Kernel.
     *
     * Available options:
     *
     *  * environment
     *  * debug
     *
     * @return KernelInterface A KernelInterface instance
     */
    protected static function createKernel(array $options = [])
    {
    }
    /**
     * Shuts the kernel down if it was used in the test - called by the tearDown method by default.
     */
    protected static function ensureKernelShutdown()
    {
    }
}
