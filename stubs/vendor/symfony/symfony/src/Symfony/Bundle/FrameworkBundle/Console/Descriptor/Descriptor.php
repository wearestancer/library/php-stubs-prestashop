<?php

namespace Symfony\Bundle\FrameworkBundle\Console\Descriptor;

/**
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
abstract class Descriptor implements \Symfony\Component\Console\Descriptor\DescriptorInterface
{
    /**
     * @var OutputInterface
     */
    protected $output;
    /**
     * {@inheritdoc}
     */
    public function describe(\Symfony\Component\Console\Output\OutputInterface $output, $object, array $options = [])
    {
    }
    protected function getOutput() : \Symfony\Component\Console\Output\OutputInterface
    {
    }
    protected function write(string $content, bool $decorated = false)
    {
    }
    protected abstract function describeRouteCollection(\Symfony\Component\Routing\RouteCollection $routes, array $options = []);
    protected abstract function describeRoute(\Symfony\Component\Routing\Route $route, array $options = []);
    protected abstract function describeContainerParameters(\Symfony\Component\DependencyInjection\ParameterBag\ParameterBag $parameters, array $options = []);
    protected abstract function describeContainerTags(\Symfony\Component\DependencyInjection\ContainerBuilder $builder, array $options = []);
    /**
     * Describes a container service by its name.
     *
     * Common options are:
     * * name: name of described service
     *
     * @param Definition|Alias|object $service
     */
    protected abstract function describeContainerService($service, array $options = [], \Symfony\Component\DependencyInjection\ContainerBuilder $builder = null);
    /**
     * Describes container services.
     *
     * Common options are:
     * * tag: filters described services by given tag
     */
    protected abstract function describeContainerServices(\Symfony\Component\DependencyInjection\ContainerBuilder $builder, array $options = []);
    protected abstract function describeContainerDefinition(\Symfony\Component\DependencyInjection\Definition $definition, array $options = []);
    protected abstract function describeContainerAlias(\Symfony\Component\DependencyInjection\Alias $alias, array $options = [], \Symfony\Component\DependencyInjection\ContainerBuilder $builder = null);
    protected abstract function describeContainerParameter($parameter, array $options = []);
    protected abstract function describeContainerEnvVars(array $envs, array $options = []);
    /**
     * Describes event dispatcher listeners.
     *
     * Common options are:
     * * name: name of listened event
     */
    protected abstract function describeEventDispatcherListeners(\Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, array $options = []);
    /**
     * Describes a callable.
     *
     * @param mixed $callable
     */
    protected abstract function describeCallable($callable, array $options = []);
    /**
     * Formats a value as string.
     *
     * @param mixed $value
     */
    protected function formatValue($value) : string
    {
    }
    /**
     * Formats a parameter.
     *
     * @param mixed $value
     */
    protected function formatParameter($value) : string
    {
    }
    /**
     * @return mixed
     */
    protected function resolveServiceDefinition(\Symfony\Component\DependencyInjection\ContainerBuilder $builder, string $serviceId)
    {
    }
    protected function findDefinitionsByTag(\Symfony\Component\DependencyInjection\ContainerBuilder $builder, bool $showHidden) : array
    {
    }
    protected function sortParameters(\Symfony\Component\DependencyInjection\ParameterBag\ParameterBag $parameters)
    {
    }
    protected function sortServiceIds(array $serviceIds)
    {
    }
    protected function sortTaggedServicesByPriority(array $services) : array
    {
    }
    protected function sortTagsByPriority(array $tags) : array
    {
    }
    protected function sortByPriority(array $tag) : array
    {
    }
    public static function getClassDescription(string $class, string &$resolvedClass = null) : string
    {
    }
}
