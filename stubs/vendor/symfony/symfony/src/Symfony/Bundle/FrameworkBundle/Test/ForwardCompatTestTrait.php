<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

/**
 * @internal
 */
trait ForwardCompatTestTrait
{
    private function doSetUp() : void
    {
    }
    private function doTearDown() : void
    {
    }
    protected function setUp() : void
    {
    }
    protected function tearDown() : void
    {
    }
}
