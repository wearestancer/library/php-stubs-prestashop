<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Command.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since Symfony 4.2, use {@see Command} instead.
 */
abstract class ContainerAwareCommand extends \Symfony\Component\Console\Command\Command implements \Symfony\Component\DependencyInjection\ContainerAwareInterface
{
    /**
     * @return ContainerInterface
     *
     * @throws \LogicException
     */
    protected function getContainer()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
    }
}
