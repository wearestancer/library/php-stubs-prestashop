<?php

namespace Symfony\Bundle\FrameworkBundle\Templating;

/**
 * GlobalVariables is the entry point for Symfony global variables in PHP templates.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class GlobalVariables
{
    protected $container;
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
    }
    /**
     * @return TokenInterface|null
     */
    public function getToken()
    {
    }
    public function getUser()
    {
    }
    /**
     * @return Request|null The HTTP request object
     */
    public function getRequest()
    {
    }
    /**
     * @return Session|null The session
     */
    public function getSession()
    {
    }
    /**
     * @return string The current environment string (e.g 'dev')
     */
    public function getEnvironment()
    {
    }
    /**
     * @return bool The current debug mode
     */
    public function getDebug()
    {
    }
}
