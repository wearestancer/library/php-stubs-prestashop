<?php

namespace Symfony\Bundle\FrameworkBundle\DependencyInjection\Compiler;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @deprecated since version 4.2, use Symfony\Component\Cache\DependencyInjection\CachePoolPass instead.
 */
class CachePoolPass extends \Symfony\Component\Cache\DependencyInjection\CachePoolPass
{
}
