<?php

namespace Symfony\Bundle\FrameworkBundle\HttpCache;

/**
 * Manages HTTP cache objects in a Container.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class HttpCache extends \Symfony\Component\HttpKernel\HttpCache\HttpCache
{
    protected $cacheDir;
    protected $kernel;
    /**
     * @param string $cacheDir The cache directory (default used if null)
     */
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, string $cacheDir = null)
    {
    }
    /**
     * Forwards the Request to the backend and returns the Response.
     *
     * @param bool     $raw   Whether to catch exceptions or not
     * @param Response $entry A Response instance (the stale entry if present, null otherwise)
     *
     * @return Response A Response instance
     */
    protected function forward(\Symfony\Component\HttpFoundation\Request $request, $raw = false, \Symfony\Component\HttpFoundation\Response $entry = null)
    {
    }
    /**
     * Returns an array of options to customize the Cache configuration.
     *
     * @return array An array of options
     */
    protected function getOptions()
    {
    }
    protected function createSurrogate()
    {
    }
    protected function createStore()
    {
    }
}
