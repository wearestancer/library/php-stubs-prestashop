<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Clear cache pools.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 */
final class CachePoolClearCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer $poolClearer)
    {
    }
}
