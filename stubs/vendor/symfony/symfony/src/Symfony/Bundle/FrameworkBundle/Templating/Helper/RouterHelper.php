<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * RouterHelper manages links between pages in a template context.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class RouterHelper extends \Symfony\Component\Templating\Helper\Helper
{
    protected $generator;
    public function __construct(\Symfony\Component\Routing\Generator\UrlGeneratorInterface $router)
    {
    }
    /**
     * Generates a URL reference (as an absolute or relative path) to the route with the given parameters.
     *
     * @param string $name       The name of the route
     * @param mixed  $parameters An array of parameters
     * @param bool   $relative   Whether to generate a relative or absolute path
     *
     * @return string The generated URL reference
     *
     * @see UrlGeneratorInterface
     */
    public function path($name, $parameters = [], $relative = false)
    {
    }
    /**
     * Generates a URL reference (as an absolute URL or network path) to the route with the given parameters.
     *
     * @param string $name           The name of the route
     * @param mixed  $parameters     An array of parameters
     * @param bool   $schemeRelative Whether to omit the scheme in the generated URL reference
     *
     * @return string The generated URL reference
     *
     * @see UrlGeneratorInterface
     */
    public function url($name, $parameters = [], $schemeRelative = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
