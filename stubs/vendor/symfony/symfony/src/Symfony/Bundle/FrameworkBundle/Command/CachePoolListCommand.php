<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * List available cache pools.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
final class CachePoolListCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(array $poolNames)
    {
    }
}
