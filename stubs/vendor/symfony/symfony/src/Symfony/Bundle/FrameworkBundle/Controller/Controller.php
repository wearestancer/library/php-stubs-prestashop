<?php

namespace Symfony\Bundle\FrameworkBundle\Controller;

/**
 * Controller is a simple implementation of a Controller.
 *
 * It provides methods to common features needed in controllers.
 *
 * @deprecated since Symfony 4.2, use "Symfony\Bundle\FrameworkBundle\Controller\AbstractController" instead.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class Controller implements \Symfony\Component\DependencyInjection\ContainerAwareInterface
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;
    use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
    /**
     * Gets a container configuration parameter by its name.
     *
     * @return array|bool|float|int|string|\UnitEnum|null
     *
     * @final
     */
    protected function getParameter(string $name)
    {
    }
}
