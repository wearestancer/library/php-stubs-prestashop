<?php

namespace Symfony\Bundle\FrameworkBundle\Controller;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class ControllerResolver extends \Symfony\Component\HttpKernel\Controller\ContainerControllerResolver
{
    /**
     * @deprecated since Symfony 4.4
     */
    protected $parser;
    /**
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $logger = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function createController($controller)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function instantiateController($class)
    {
    }
}
