<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command for dumping available configuration reference.
 *
 * @author Kevin Bond <kevinbond@gmail.com>
 * @author Wouter J <waldio.webdesign@gmail.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
abstract class AbstractConfigCommand extends \Symfony\Bundle\FrameworkBundle\Command\ContainerDebugCommand
{
    /**
     * @param OutputInterface|StyleInterface $output
     */
    protected function listBundles($output)
    {
    }
    /**
     * @return ExtensionInterface
     */
    protected function findExtension($name)
    {
    }
    public function validateConfiguration(\Symfony\Component\DependencyInjection\Extension\ExtensionInterface $extension, $configuration)
    {
    }
}
