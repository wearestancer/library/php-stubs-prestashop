<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command for retrieving information about services.
 *
 * @author Ryan Weaver <ryan@thatsquality.com>
 *
 * @internal
 */
class ContainerDebugCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'debug:container';
    /**
     * @var ContainerBuilder|null
     */
    protected $containerBuilder;
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
    /**
     * Validates input arguments and options.
     *
     * @throws \InvalidArgumentException
     */
    protected function validateInput(\Symfony\Component\Console\Input\InputInterface $input)
    {
    }
    /**
     * Loads the ContainerBuilder from the cache.
     *
     * @throws \LogicException
     */
    protected function getContainerBuilder() : \Symfony\Component\DependencyInjection\ContainerBuilder
    {
    }
    /**
     * @internal
     */
    public function filterToServiceTypes(string $serviceId) : bool
    {
    }
}
