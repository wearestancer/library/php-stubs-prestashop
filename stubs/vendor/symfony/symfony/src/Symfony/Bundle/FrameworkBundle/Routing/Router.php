<?php

namespace Symfony\Bundle\FrameworkBundle\Routing;

/**
 * This Router creates the Loader only when the cache is empty.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Router extends \Symfony\Component\Routing\Router implements \Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface, \Symfony\Bundle\FrameworkBundle\DependencyInjection\CompatibilityServiceSubscriberInterface
{
    /**
     * @param mixed $resource The main resource to load
     */
    public function __construct(\Psr\Container\ContainerInterface $container, $resource, array $options = [], \Symfony\Component\Routing\RequestContext $context = null, \Psr\Container\ContainerInterface $parameters = null, \Psr\Log\LoggerInterface $logger = null, string $defaultLocale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRouteCollection()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
    }
}
