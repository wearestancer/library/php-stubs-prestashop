<?php

namespace Symfony\Bundle\FrameworkBundle\EventListener;

/**
 * Guarantees that the _controller key is parsed into its final format.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 *
 * @method onKernelRequest(RequestEvent $event)
 *
 * @deprecated since Symfony 4.1
 */
class ResolveControllerNameSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser $parser, bool $triggerDeprecation = true)
    {
    }
    /**
     * @internal
     */
    public function resolveControllerName(...$args)
    {
    }
    public function __call(string $method, array $args)
    {
    }
    public static function getSubscribedEvents()
    {
    }
}
