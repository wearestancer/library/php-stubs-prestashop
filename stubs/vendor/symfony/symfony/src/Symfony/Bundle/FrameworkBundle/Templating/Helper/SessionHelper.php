<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * SessionHelper provides read-only access to the session attributes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class SessionHelper extends \Symfony\Component\Templating\Helper\Helper
{
    protected $session;
    protected $requestStack;
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    /**
     * Returns an attribute.
     *
     * @param string $name    The attribute name
     * @param mixed  $default The default value
     *
     * @return mixed
     */
    public function get($name, $default = null)
    {
    }
    public function getFlash($name, array $default = [])
    {
    }
    public function getFlashes()
    {
    }
    public function hasFlash($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
