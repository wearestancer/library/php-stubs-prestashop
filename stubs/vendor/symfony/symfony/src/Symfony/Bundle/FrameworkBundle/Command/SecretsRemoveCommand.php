<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * @author Jérémy Derussé <jeremy@derusse.com>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class SecretsRemoveCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault $vault, \Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault $localVault = null)
    {
    }
}
