<?php

namespace Symfony\Bundle\FrameworkBundle\DependencyInjection;

/**
 * Process the configuration and prepare the dependency injection container with
 * parameters and services.
 */
class FrameworkExtension extends \Symfony\Component\HttpKernel\DependencyInjection\Extension
{
    /**
     * Responds to the app.config configuration parameter.
     *
     * @throws LogicException
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getConfiguration(array $config, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getXsdValidationBasePath()
    {
    }
    public function getNamespace()
    {
    }
}
