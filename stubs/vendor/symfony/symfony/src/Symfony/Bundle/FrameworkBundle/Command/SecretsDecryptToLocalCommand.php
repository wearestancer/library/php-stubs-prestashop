<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
final class SecretsDecryptToLocalCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(\Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault $vault, \Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault $localVault = null)
    {
    }
}
