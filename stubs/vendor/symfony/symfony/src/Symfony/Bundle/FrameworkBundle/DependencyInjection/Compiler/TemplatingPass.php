<?php

namespace Symfony\Bundle\FrameworkBundle\DependencyInjection\Compiler;

/**
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplatingPass implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    public function process(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
