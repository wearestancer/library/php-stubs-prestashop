<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command to display information about the current installation.
 *
 * @author Roland Franssen <franssen.roland@gmail.com>
 *
 * @final
 */
class AboutCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'about';
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
