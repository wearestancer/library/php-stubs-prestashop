<?php

namespace Symfony\Bundle\FrameworkBundle\EventListener;

/**
 * Suggests a package, that should be installed (via composer),
 * if the package is missing, and the input command namespace can be mapped to a Symfony bundle.
 *
 * @author Przemysław Bogusz <przemyslaw.bogusz@tubotax.pl>
 *
 * @internal
 */
final class SuggestMissingPackageSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function onConsoleError(\Symfony\Component\Console\Event\ConsoleErrorEvent $event) : void
    {
    }
    public static function getSubscribedEvents() : array
    {
    }
}
