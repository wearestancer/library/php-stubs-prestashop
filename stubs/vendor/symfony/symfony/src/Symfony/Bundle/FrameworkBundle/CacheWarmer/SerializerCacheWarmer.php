<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Warms up XML and YAML serializer metadata.
 *
 * @author Titouan Galopin <galopintitouan@gmail.com>
 */
class SerializerCacheWarmer extends \Symfony\Bundle\FrameworkBundle\CacheWarmer\AbstractPhpFileCacheWarmer
{
    /**
     * @param LoaderInterface[] $loaders      The serializer metadata loaders
     * @param string            $phpArrayFile The PHP file where metadata are cached
     */
    public function __construct(array $loaders, string $phpArrayFile)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doWarmUp($cacheDir, \Symfony\Component\Cache\Adapter\ArrayAdapter $arrayAdapter)
    {
    }
}
