<?php

namespace Symfony\Bundle\FrameworkBundle\Templating;

/**
 * Internal representation of a template.
 *
 * @author Victor Berchet <victor@suumit.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplateReference extends \Symfony\Component\Templating\TemplateReference
{
    public function __construct(string $bundle = null, string $controller = null, string $name = null, string $format = null, string $engine = null)
    {
    }
    /**
     * Returns the path to the template
     *  - as a path when the template is not part of a bundle
     *  - as a resource when the template is part of a bundle.
     *
     * @return string A path to the template or a resource
     */
    public function getPath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLogicalName()
    {
    }
}
