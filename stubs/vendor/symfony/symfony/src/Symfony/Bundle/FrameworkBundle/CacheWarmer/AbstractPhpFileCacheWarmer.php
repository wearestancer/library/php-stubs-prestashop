<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

abstract class AbstractPhpFileCacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface
{
    /**
     * @param string $phpArrayFile The PHP file where metadata are cached
     */
    public function __construct(string $phpArrayFile)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isOptional()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
    }
    protected function warmUpPhpArrayAdapter(\Symfony\Component\Cache\Adapter\PhpArrayAdapter $phpArrayAdapter, array $values)
    {
    }
    /**
     * @internal
     */
    protected final function ignoreAutoloadException(string $class, \Exception $exception) : void
    {
    }
    /**
     * @param string $cacheDir
     *
     * @return bool false if there is nothing to warm-up
     */
    protected abstract function doWarmUp($cacheDir, \Symfony\Component\Cache\Adapter\ArrayAdapter $arrayAdapter);
}
