<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

/**
 * WebTestCase is the base class for functional tests.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class WebTestCase extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    use \Symfony\Bundle\FrameworkBundle\Test\ForwardCompatTestTrait;
    use \Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
    use \Symfony\Bundle\FrameworkBundle\Test\WebTestAssertionsTrait;
    /**
     * Creates a KernelBrowser.
     *
     * @param array $options An array of options to pass to the createKernel method
     * @param array $server  An array of server parameters
     *
     * @return KernelBrowser A KernelBrowser instance
     */
    protected static function createClient(array $options = [], array $server = [])
    {
    }
}
