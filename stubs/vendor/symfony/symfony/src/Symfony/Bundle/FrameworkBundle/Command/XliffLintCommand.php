<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Validates XLIFF files syntax and outputs encountered errors.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Robin Chalas <robin.chalas@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 *
 * @final
 */
class XliffLintCommand extends \Symfony\Component\Translation\Command\XliffLintCommand
{
    protected static $defaultName = 'lint:xliff';
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
}
