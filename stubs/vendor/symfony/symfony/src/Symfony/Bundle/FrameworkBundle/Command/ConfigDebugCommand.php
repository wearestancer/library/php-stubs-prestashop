<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command for dumping available configuration reference.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @final
 */
class ConfigDebugCommand extends \Symfony\Bundle\FrameworkBundle\Command\AbstractConfigCommand
{
    protected static $defaultName = 'debug:config';
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
