<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * RequestHelper provides access to the current request parameters.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class RequestHelper extends \Symfony\Component\Templating\Helper\Helper
{
    protected $requestStack;
    public function __construct(\Symfony\Component\HttpFoundation\RequestStack $requestStack)
    {
    }
    /**
     * Returns a parameter from the current request object.
     *
     * @param string $key     The name of the parameter
     * @param string $default A default value
     *
     * @return mixed
     *
     * @see Request::get()
     */
    public function getParameter($key, $default = null)
    {
    }
    /**
     * Returns the locale.
     *
     * @return string
     */
    public function getLocale()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
