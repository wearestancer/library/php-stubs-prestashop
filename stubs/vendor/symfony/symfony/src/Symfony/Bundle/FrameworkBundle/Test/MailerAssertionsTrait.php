<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

trait MailerAssertionsTrait
{
    public static function assertEmailCount(int $count, string $transport = null, string $message = '') : void
    {
    }
    public static function assertQueuedEmailCount(int $count, string $transport = null, string $message = '') : void
    {
    }
    public static function assertEmailIsQueued(\Symfony\Component\Mailer\Event\MessageEvent $event, string $message = '') : void
    {
    }
    public static function assertEmailIsNotQueued(\Symfony\Component\Mailer\Event\MessageEvent $event, string $message = '') : void
    {
    }
    public static function assertEmailAttachmentCount(\Symfony\Component\Mime\RawMessage $email, int $count, string $message = '') : void
    {
    }
    public static function assertEmailTextBodyContains(\Symfony\Component\Mime\RawMessage $email, string $text, string $message = '') : void
    {
    }
    public static function assertEmailTextBodyNotContains(\Symfony\Component\Mime\RawMessage $email, string $text, string $message = '') : void
    {
    }
    public static function assertEmailHtmlBodyContains(\Symfony\Component\Mime\RawMessage $email, string $text, string $message = '') : void
    {
    }
    public static function assertEmailHtmlBodyNotContains(\Symfony\Component\Mime\RawMessage $email, string $text, string $message = '') : void
    {
    }
    public static function assertEmailHasHeader(\Symfony\Component\Mime\RawMessage $email, string $headerName, string $message = '') : void
    {
    }
    public static function assertEmailNotHasHeader(\Symfony\Component\Mime\RawMessage $email, string $headerName, string $message = '') : void
    {
    }
    public static function assertEmailHeaderSame(\Symfony\Component\Mime\RawMessage $email, string $headerName, string $expectedValue, string $message = '') : void
    {
    }
    public static function assertEmailHeaderNotSame(\Symfony\Component\Mime\RawMessage $email, string $headerName, string $expectedValue, string $message = '') : void
    {
    }
    public static function assertEmailAddressContains(\Symfony\Component\Mime\RawMessage $email, string $headerName, string $expectedValue, string $message = '') : void
    {
    }
    /**
     * @return MessageEvent[]
     */
    public static function getMailerEvents(string $transport = null) : array
    {
    }
    public static function getMailerEvent(int $index = 0, string $transport = null) : ?\Symfony\Component\Mailer\Event\MessageEvent
    {
    }
    /**
     * @return RawMessage[]
     */
    public static function getMailerMessages(string $transport = null) : array
    {
    }
    public static function getMailerMessage(int $index = 0, string $transport = null) : ?\Symfony\Component\Mime\RawMessage
    {
    }
    private static function getMessageMailerEvents() : \Symfony\Component\Mailer\Event\MessageEvents
    {
    }
}
