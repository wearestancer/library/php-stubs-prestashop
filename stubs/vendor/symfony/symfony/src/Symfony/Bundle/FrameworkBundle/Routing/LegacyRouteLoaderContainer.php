<?php

namespace Symfony\Bundle\FrameworkBundle\Routing;

/**
 * @internal to be removed in Symfony 5.0
 */
class LegacyRouteLoaderContainer implements \Psr\Container\ContainerInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container, \Psr\Container\ContainerInterface $serviceLocator)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function get($id)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function has($id)
    {
    }
}
