<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Loader;

/**
 * FilesystemLoader is a loader that read templates from the filesystem.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class FilesystemLoader implements \Symfony\Component\Templating\Loader\LoaderInterface
{
    protected $locator;
    public function __construct(\Symfony\Component\Config\FileLocatorInterface $locator)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function load(\Symfony\Component\Templating\TemplateReferenceInterface $template)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isFresh(\Symfony\Component\Templating\TemplateReferenceInterface $template, $time)
    {
    }
}
