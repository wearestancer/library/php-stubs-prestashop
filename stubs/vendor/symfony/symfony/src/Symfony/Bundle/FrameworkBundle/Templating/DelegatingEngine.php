<?php

namespace Symfony\Bundle\FrameworkBundle\Templating;

/**
 * DelegatingEngine selects an engine for a given template.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class DelegatingEngine extends \Symfony\Component\Templating\DelegatingEngine implements \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
{
    protected $container;
    public function __construct(\Psr\Container\ContainerInterface $container, array $engineIds)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getEngine($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderResponse($view, array $parameters = [], \Symfony\Component\HttpFoundation\Response $response = null)
    {
    }
}
