<?php

namespace Symfony\Bundle\FrameworkBundle\Routing;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 */
class RedirectableCompiledUrlMatcher extends \Symfony\Component\Routing\Matcher\CompiledUrlMatcher implements \Symfony\Component\Routing\Matcher\RedirectableUrlMatcherInterface
{
    /**
     * {@inheritdoc}
     */
    public function redirect($path, $route, $scheme = null) : array
    {
    }
}
