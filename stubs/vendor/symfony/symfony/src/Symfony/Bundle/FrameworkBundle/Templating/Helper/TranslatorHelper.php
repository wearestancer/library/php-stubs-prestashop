<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TranslatorHelper extends \Symfony\Component\Templating\Helper\Helper
{
    use \Symfony\Contracts\Translation\TranslatorTrait {
        getLocale as private;
        setLocale as private;
        trans as private doTrans;
    }
    protected $translator;
    /**
     * @param TranslatorInterface|null $translator
     */
    public function __construct($translator = null)
    {
    }
    /**
     * @see TranslatorInterface::trans()
     */
    public function trans($id, array $parameters = [], $domain = 'messages', $locale = null)
    {
    }
    /**
     * @see TranslatorInterface::transChoice()
     * @deprecated since Symfony 4.2, use the trans() method instead with a %count% parameter
     */
    public function transChoice($id, $number, array $parameters = [], $domain = 'messages', $locale = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
    }
}
