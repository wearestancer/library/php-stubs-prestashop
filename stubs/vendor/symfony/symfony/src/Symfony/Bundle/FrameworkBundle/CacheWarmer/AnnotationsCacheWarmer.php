<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Warms up annotation caches for classes found in composer's autoload class map
 * and declared in DI bundle extensions using the addAnnotatedClassesToCache method.
 *
 * @author Titouan Galopin <galopintitouan@gmail.com>
 */
class AnnotationsCacheWarmer extends \Symfony\Bundle\FrameworkBundle\CacheWarmer\AbstractPhpFileCacheWarmer
{
    /**
     * @param string $phpArrayFile  The PHP file where annotations are cached
     * @param string $excludeRegexp
     * @param bool   $debug
     */
    public function __construct(\Doctrine\Common\Annotations\Reader $annotationReader, string $phpArrayFile, $excludeRegexp = null, $debug = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doWarmUp($cacheDir, \Symfony\Component\Cache\Adapter\ArrayAdapter $arrayAdapter)
    {
    }
    protected function warmUpPhpArrayAdapter(\Symfony\Component\Cache\Adapter\PhpArrayAdapter $phpArrayAdapter, array $values)
    {
    }
}
