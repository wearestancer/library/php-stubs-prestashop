<?php

namespace Symfony\Bundle\FrameworkBundle\Translation;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 */
class Translator extends \Symfony\Component\Translation\Translator implements \Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface
{
    protected $container;
    protected $loaderIds;
    protected $options = ['cache_dir' => null, 'debug' => false, 'resource_files' => [], 'scanned_directories' => [], 'cache_vary' => []];
    /**
     * Constructor.
     *
     * Available options:
     *
     *   * cache_dir:      The cache directory (or null to disable caching)
     *   * debug:          Whether to enable debugging or not (false by default)
     *   * resource_files: List of translation resources available grouped by locale.
     *   * cache_vary:     An array of data that is serialized to generate the cached catalogue name.
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Psr\Container\ContainerInterface $container, \Symfony\Component\Translation\Formatter\MessageFormatterInterface $formatter, string $defaultLocale, array $loaderIds = [], array $options = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
    }
    public function addResource($format, $resource, $locale, $domain = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function initializeCatalogue($locale)
    {
    }
    /**
     * @internal
     */
    protected function doLoadCatalogue(string $locale) : void
    {
    }
    protected function initialize()
    {
    }
}
