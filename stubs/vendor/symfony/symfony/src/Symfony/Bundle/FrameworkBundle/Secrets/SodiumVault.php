<?php

namespace Symfony\Bundle\FrameworkBundle\Secrets;

/**
 * @author Tobias Schultze <http://tobion.de>
 * @author Jérémy Derussé <jeremy@derusse.com>
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class SodiumVault extends \Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault implements \Symfony\Component\DependencyInjection\EnvVarLoaderInterface
{
    /**
     * @param string|\Stringable|null $decryptionKey A string or a stringable object that defines the private key to use to decrypt the vault
     *                                               or null to store generated keys in the provided $secretsDir
     */
    public function __construct(string $secretsDir, $decryptionKey = null)
    {
    }
    public function generateKeys(bool $override = false) : bool
    {
    }
    public function seal(string $name, string $value) : void
    {
    }
    public function reveal(string $name) : ?string
    {
    }
    public function remove(string $name) : bool
    {
    }
    public function list(bool $reveal = false) : array
    {
    }
    public function loadEnvVars() : array
    {
    }
}
