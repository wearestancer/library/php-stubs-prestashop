<?php

namespace Symfony\Bundle\FrameworkBundle\DataCollector;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final since Symfony 4.4
 */
class RouterDataCollector extends \Symfony\Component\HttpKernel\DataCollector\RouterDataCollector
{
    public function guessRoute(\Symfony\Component\HttpFoundation\Request $request, $controller)
    {
    }
}
