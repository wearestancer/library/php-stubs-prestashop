<?php

namespace Symfony\Bundle\FrameworkBundle\Controller;

/**
 * Common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @internal
 *
 * @property ContainerInterface $container
 */
trait ControllerTrait
{
    /**
     * Returns true if the service id is defined.
     *
     * @final
     */
    protected function has(string $id) : bool
    {
    }
    /**
     * Gets a container service by its id.
     *
     * @return object The service
     *
     * @final
     */
    protected function get(string $id)
    {
    }
    /**
     * Generates a URL from the given parameters.
     *
     * @see UrlGeneratorInterface
     *
     * @final
     */
    protected function generateUrl(string $route, array $parameters = [], int $referenceType = \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_PATH) : string
    {
    }
    /**
     * Forwards the request to another controller.
     *
     * @param string $controller The controller name (a string like Bundle\BlogBundle\Controller\PostController::indexAction)
     *
     * @final
     */
    protected function forward(string $controller, array $path = [], array $query = []) : \Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Returns a RedirectResponse to the given URL.
     *
     * @final
     */
    protected function redirect(string $url, int $status = 302) : \Symfony\Component\HttpFoundation\RedirectResponse
    {
    }
    /**
     * Returns a RedirectResponse to the given route with the given parameters.
     *
     * @final
     */
    protected function redirectToRoute(string $route, array $parameters = [], int $status = 302) : \Symfony\Component\HttpFoundation\RedirectResponse
    {
    }
    /**
     * Returns a JsonResponse that uses the serializer component if enabled, or json_encode.
     *
     * @final
     */
    protected function json($data, int $status = 200, array $headers = [], array $context = []) : \Symfony\Component\HttpFoundation\JsonResponse
    {
    }
    /**
     * Returns a BinaryFileResponse object with original or customized file name and disposition header.
     *
     * @param \SplFileInfo|string $file File object or path to file to be sent as response
     *
     * @final
     */
    protected function file($file, string $fileName = null, string $disposition = \Symfony\Component\HttpFoundation\ResponseHeaderBag::DISPOSITION_ATTACHMENT) : \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
    }
    /**
     * Adds a flash message to the current session for type.
     *
     * @throws \LogicException
     *
     * @final
     */
    protected function addFlash(string $type, $message)
    {
    }
    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied subject.
     *
     * @throws \LogicException
     *
     * @final
     */
    protected function isGranted($attributes, $subject = null) : bool
    {
    }
    /**
     * Throws an exception unless the attributes are granted against the current authentication token and optionally
     * supplied subject.
     *
     * @throws AccessDeniedException
     *
     * @final
     */
    protected function denyAccessUnlessGranted($attributes, $subject = null, string $message = 'Access Denied.')
    {
    }
    /**
     * Returns a rendered view.
     *
     * @final
     */
    protected function renderView(string $view, array $parameters = []) : string
    {
    }
    /**
     * Renders a view.
     *
     * @final
     */
    protected function render(string $view, array $parameters = [], \Symfony\Component\HttpFoundation\Response $response = null) : \Symfony\Component\HttpFoundation\Response
    {
    }
    /**
     * Streams a view.
     *
     * @final
     */
    protected function stream(string $view, array $parameters = [], \Symfony\Component\HttpFoundation\StreamedResponse $response = null) : \Symfony\Component\HttpFoundation\StreamedResponse
    {
    }
    /**
     * Returns a NotFoundHttpException.
     *
     * This will result in a 404 response code. Usage example:
     *
     *     throw $this->createNotFoundException('Page not found!');
     *
     * @final
     */
    protected function createNotFoundException(string $message = 'Not Found', \Throwable $previous = null) : \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
    {
    }
    /**
     * Returns an AccessDeniedException.
     *
     * This will result in a 403 response code. Usage example:
     *
     *     throw $this->createAccessDeniedException('Unable to access this page!');
     *
     * @throws \LogicException If the Security component is not available
     *
     * @final
     */
    protected function createAccessDeniedException(string $message = 'Access Denied.', \Throwable $previous = null) : \Symfony\Component\Security\Core\Exception\AccessDeniedException
    {
    }
    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @final
     */
    protected function createForm(string $type, $data = null, array $options = []) : \Symfony\Component\Form\FormInterface
    {
    }
    /**
     * Creates and returns a form builder instance.
     *
     * @final
     */
    protected function createFormBuilder($data = null, array $options = []) : \Symfony\Component\Form\FormBuilderInterface
    {
    }
    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return ManagerRegistry
     *
     * @throws \LogicException If DoctrineBundle is not available
     *
     * @final
     */
    protected function getDoctrine()
    {
    }
    /**
     * Get a user from the Security Token Storage.
     *
     * @return UserInterface|object|null
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     *
     * @final
     */
    protected function getUser()
    {
    }
    /**
     * Checks the validity of a CSRF token.
     *
     * @param string      $id    The id used when generating the token
     * @param string|null $token The actual token sent with the request that should be validated
     *
     * @final
     */
    protected function isCsrfTokenValid(string $id, ?string $token) : bool
    {
    }
    /**
     * Dispatches a message to the bus.
     *
     * @param object|Envelope  $message The message or the message pre-wrapped in an envelope
     * @param StampInterface[] $stamps
     *
     * @final
     */
    protected function dispatchMessage($message, array $stamps = []) : \Symfony\Component\Messenger\Envelope
    {
    }
    /**
     * Adds a Link HTTP header to the current response.
     *
     * @see https://tools.ietf.org/html/rfc5988
     *
     * @final
     */
    protected function addLink(\Symfony\Component\HttpFoundation\Request $request, \Psr\Link\LinkInterface $link)
    {
    }
}
