<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Generates the router matcher and generator classes.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class RouterCacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface, \Symfony\Bundle\FrameworkBundle\DependencyInjection\CompatibilityServiceSubscriberInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * Checks whether this warmer is optional or not.
     *
     * @return bool always true
     */
    public function isOptional() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices() : array
    {
    }
}
