<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class TestContainer extends \Symfony\Component\DependencyInjection\Container
{
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, string $privateServicesLocatorId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function compile()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isCompiled() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getParameterBag() : \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return array|bool|float|int|string|\UnitEnum|null
     */
    public function getParameter($name)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function hasParameter($name) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setParameter($name, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function set($id, $service)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function has($id) : bool
    {
    }
    /**
     * {@inheritdoc}
     *
     * @return object|null
     */
    public function get($id, $invalidBehavior = 1)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initialized($id) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function reset()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getServiceIds() : array
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRemovedIds() : array
    {
    }
}
