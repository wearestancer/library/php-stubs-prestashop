<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command to test route matching.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class RouterMatchCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'router:match';
    public function __construct(\Symfony\Component\Routing\RouterInterface $router)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
