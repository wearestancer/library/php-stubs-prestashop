<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command for retrieving information about event dispatcher.
 *
 * @author Matthieu Auger <mail@matthieuauger.com>
 *
 * @final
 */
class EventDispatcherDebugCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'debug:event-dispatcher';
    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
