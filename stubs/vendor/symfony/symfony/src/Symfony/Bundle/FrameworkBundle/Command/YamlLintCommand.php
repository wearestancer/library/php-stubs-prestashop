<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Validates YAML files syntax and outputs encountered errors.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Robin Chalas <robin.chalas@gmail.com>
 *
 * @final
 */
class YamlLintCommand extends \Symfony\Component\Yaml\Command\LintCommand
{
    protected static $defaultName = 'lint:yaml';
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
}
