<?php

namespace Symfony\Bundle\FrameworkBundle;

/**
 * Client simulates a browser and makes requests to a Kernel object.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class KernelBrowser extends \Symfony\Bundle\FrameworkBundle\Client
{
}
