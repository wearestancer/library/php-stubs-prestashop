<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Helper;

/**
 * StopwatchHelper provides methods time your PHP templates.
 *
 * @author Wouter J <wouter@wouterj.nl>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class StopwatchHelper extends \Symfony\Component\Templating\Helper\Helper
{
    public function __construct(\Symfony\Component\Stopwatch\Stopwatch $stopwatch = null)
    {
    }
    public function getName()
    {
    }
    public function __call($method, $arguments = [])
    {
    }
}
