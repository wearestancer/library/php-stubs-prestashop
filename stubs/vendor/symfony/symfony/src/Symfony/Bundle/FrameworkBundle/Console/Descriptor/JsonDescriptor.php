<?php

namespace Symfony\Bundle\FrameworkBundle\Console\Descriptor;

/**
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class JsonDescriptor extends \Symfony\Bundle\FrameworkBundle\Console\Descriptor\Descriptor
{
    protected function describeRouteCollection(\Symfony\Component\Routing\RouteCollection $routes, array $options = [])
    {
    }
    protected function describeRoute(\Symfony\Component\Routing\Route $route, array $options = [])
    {
    }
    protected function describeContainerParameters(\Symfony\Component\DependencyInjection\ParameterBag\ParameterBag $parameters, array $options = [])
    {
    }
    protected function describeContainerTags(\Symfony\Component\DependencyInjection\ContainerBuilder $builder, array $options = [])
    {
    }
    protected function describeContainerService($service, array $options = [], \Symfony\Component\DependencyInjection\ContainerBuilder $builder = null)
    {
    }
    protected function describeContainerServices(\Symfony\Component\DependencyInjection\ContainerBuilder $builder, array $options = [])
    {
    }
    protected function describeContainerDefinition(\Symfony\Component\DependencyInjection\Definition $definition, array $options = [])
    {
    }
    protected function describeContainerAlias(\Symfony\Component\DependencyInjection\Alias $alias, array $options = [], \Symfony\Component\DependencyInjection\ContainerBuilder $builder = null)
    {
    }
    protected function describeEventDispatcherListeners(\Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, array $options = [])
    {
    }
    protected function describeCallable($callable, array $options = [])
    {
    }
    protected function describeContainerParameter($parameter, array $options = [])
    {
    }
    protected function describeContainerEnvVars(array $envs, array $options = [])
    {
    }
    protected function getRouteData(\Symfony\Component\Routing\Route $route) : array
    {
    }
}
