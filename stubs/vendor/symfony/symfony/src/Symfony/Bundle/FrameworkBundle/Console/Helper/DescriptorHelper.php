<?php

namespace Symfony\Bundle\FrameworkBundle\Console\Helper;

/**
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */
class DescriptorHelper extends \Symfony\Component\Console\Helper\DescriptorHelper
{
    public function __construct(\Symfony\Component\HttpKernel\Debug\FileLinkFormatter $fileLinkFormatter = null)
    {
    }
}
