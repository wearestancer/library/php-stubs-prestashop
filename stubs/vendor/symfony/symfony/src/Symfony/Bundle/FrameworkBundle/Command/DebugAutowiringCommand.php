<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * A console command for autowiring information.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 *
 * @internal
 */
class DebugAutowiringCommand extends \Symfony\Bundle\FrameworkBundle\Command\ContainerDebugCommand
{
    protected static $defaultName = 'debug:autowiring';
    public function __construct(string $name = null, \Symfony\Component\HttpKernel\Debug\FileLinkFormatter $fileLinkFormatter = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
