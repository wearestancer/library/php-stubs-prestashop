<?php

namespace Symfony\Bundle\FrameworkBundle\Controller;

/**
 * TemplateController.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class TemplateController
{
    public function __construct(\Twig\Environment $twig = null, \Symfony\Component\Templating\EngineInterface $templating = null)
    {
    }
    /**
     * Renders a template.
     *
     * @param string    $template  The template name
     * @param int|null  $maxAge    Max age for client caching
     * @param int|null  $sharedAge Max age for shared (proxy) caching
     * @param bool|null $private   Whether or not caching should apply for client caches only
     */
    public function templateAction(string $template, int $maxAge = null, int $sharedAge = null, bool $private = null) : \Symfony\Component\HttpFoundation\Response
    {
    }
    public function __invoke(string $template, int $maxAge = null, int $sharedAge = null, bool $private = null) : \Symfony\Component\HttpFoundation\Response
    {
    }
}
