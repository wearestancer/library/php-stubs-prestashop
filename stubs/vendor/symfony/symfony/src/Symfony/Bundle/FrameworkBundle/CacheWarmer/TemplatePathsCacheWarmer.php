<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Computes the association between template names and their paths on the disk.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplatePathsCacheWarmer extends \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmer
{
    protected $finder;
    protected $locator;
    public function __construct(\Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplateFinderInterface $finder, \Symfony\Bundle\FrameworkBundle\Templating\Loader\TemplateLocator $locator)
    {
    }
    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * Checks whether this warmer is optional or not.
     *
     * @return bool always true
     */
    public function isOptional()
    {
    }
}
