<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Generates the catalogues for translations.
 *
 * @author Xavier Leune <xavier.leune@gmail.com>
 */
class TranslationsCacheWarmer implements \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface, \Symfony\Bundle\FrameworkBundle\DependencyInjection\CompatibilityServiceSubscriberInterface
{
    public function __construct(\Psr\Container\ContainerInterface $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function isOptional()
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
    }
}
