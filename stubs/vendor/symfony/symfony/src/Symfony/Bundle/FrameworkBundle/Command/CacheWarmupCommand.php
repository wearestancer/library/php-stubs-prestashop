<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Warmup the cache.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class CacheWarmupCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'cache:warmup';
    public function __construct(\Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate $cacheWarmer)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
