<?php

namespace Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * Warms up XML and YAML validator metadata.
 *
 * @author Titouan Galopin <galopintitouan@gmail.com>
 */
class ValidatorCacheWarmer extends \Symfony\Bundle\FrameworkBundle\CacheWarmer\AbstractPhpFileCacheWarmer
{
    /**
     * @param ValidatorBuilder $validatorBuilder
     * @param string           $phpArrayFile     The PHP file where metadata are cached
     */
    public function __construct($validatorBuilder, string $phpArrayFile)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function doWarmUp($cacheDir, \Symfony\Component\Cache\Adapter\ArrayAdapter $arrayAdapter)
    {
    }
    protected function warmUpPhpArrayAdapter(\Symfony\Component\Cache\Adapter\PhpArrayAdapter $phpArrayAdapter, array $values)
    {
    }
}
