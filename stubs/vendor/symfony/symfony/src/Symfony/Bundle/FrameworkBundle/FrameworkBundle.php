<?php

namespace Symfony\Bundle\FrameworkBundle;

/**
 * Bundle.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class FrameworkBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function boot()
    {
    }
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
    }
}
