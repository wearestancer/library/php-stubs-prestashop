<?php

namespace Symfony\Bundle\FrameworkBundle\Command;

/**
 * Clear and Warmup the cache.
 *
 * @author Francis Besset <francis.besset@gmail.com>
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @final
 */
class CacheClearCommand extends \Symfony\Component\Console\Command\Command
{
    protected static $defaultName = 'cache:clear';
    public function __construct(\Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface $cacheClearer, \Symfony\Component\Filesystem\Filesystem $filesystem = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) : int
    {
    }
}
