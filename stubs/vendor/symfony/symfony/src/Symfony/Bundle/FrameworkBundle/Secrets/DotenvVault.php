<?php

namespace Symfony\Bundle\FrameworkBundle\Secrets;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 *
 * @internal
 */
class DotenvVault extends \Symfony\Bundle\FrameworkBundle\Secrets\AbstractVault
{
    public function __construct(string $dotenvFile)
    {
    }
    public function generateKeys(bool $override = false) : bool
    {
    }
    public function seal(string $name, string $value) : void
    {
    }
    public function reveal(string $name) : ?string
    {
    }
    public function remove(string $name) : bool
    {
    }
    public function list(bool $reveal = false) : array
    {
    }
}
