<?php

namespace Symfony\Bundle\FrameworkBundle;

/**
 * Client simulates a browser and makes requests to a Kernel object.
 *
 * @deprecated since Symfony 4.3, use KernelBrowser instead.
 */
class Client extends \Symfony\Component\HttpKernel\HttpKernelBrowser
{
    /**
     * {@inheritdoc}
     */
    public function __construct(\Symfony\Component\HttpKernel\KernelInterface $kernel, array $server = [], \Symfony\Component\BrowserKit\History $history = null, \Symfony\Component\BrowserKit\CookieJar $cookieJar = null)
    {
    }
    /**
     * Returns the container.
     *
     * @return ContainerInterface|null Returns null when the Kernel has been shutdown or not started yet
     */
    public function getContainer()
    {
    }
    /**
     * Returns the kernel.
     *
     * @return KernelInterface
     */
    public function getKernel()
    {
    }
    /**
     * Gets the profile associated with the current Response.
     *
     * @return HttpProfile|false|null A Profile instance
     */
    public function getProfile()
    {
    }
    /**
     * Enables the profiler for the very next request.
     *
     * If the profiler is not enabled, the call to this method does nothing.
     */
    public function enableProfiler()
    {
    }
    /**
     * Disables kernel reboot between requests.
     *
     * By default, the Client reboots the Kernel for each request. This method
     * allows to keep the same kernel across requests.
     */
    public function disableReboot()
    {
    }
    /**
     * Enables kernel reboot between requests.
     */
    public function enableReboot()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request $request A Request instance
     *
     * @return Response A Response instance
     */
    protected function doRequest($request)
    {
    }
    /**
     * {@inheritdoc}
     *
     * @param Request $request A Request instance
     *
     * @return Response A Response instance
     */
    protected function doRequestInProcess($request)
    {
    }
    /**
     * Returns the script to execute when the request must be insulated.
     *
     * It assumes that the autoloader is named 'autoload.php' and that it is
     * stored in the same directory as the kernel (this is the case for the
     * Symfony Standard Edition). If this is not your case, create your own
     * client and override this method.
     *
     * @param Request $request A Request instance
     *
     * @return string The script content
     */
    protected function getScript($request)
    {
    }
}
