<?php

namespace Symfony\Bundle\FrameworkBundle\Test;

trait WebTestAssertionsTrait
{
    use \Symfony\Bundle\FrameworkBundle\Test\BrowserKitAssertionsTrait;
    use \Symfony\Bundle\FrameworkBundle\Test\DomCrawlerAssertionsTrait;
}
