<?php

namespace Symfony\Bundle\FrameworkBundle\Templating\Loader;

/**
 * TemplateLocator locates templates in bundles.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 *
 * @deprecated since version 4.3, to be removed in 5.0; use Twig instead.
 */
class TemplateLocator implements \Symfony\Component\Config\FileLocatorInterface
{
    protected $locator;
    protected $cache;
    /**
     * @param string $cacheDir The cache path
     */
    public function __construct(\Symfony\Component\Config\FileLocatorInterface $locator, string $cacheDir = null)
    {
    }
    /**
     * Returns a full path for a given file.
     *
     * @return string The full path for the file
     */
    protected function getCacheKey($template)
    {
    }
    /**
     * Returns a full path for a given file.
     *
     * @param TemplateReferenceInterface $template    A template
     * @param string                     $currentPath Unused
     * @param bool                       $first       Unused
     *
     * @return string The full path for the file
     *
     * @throws \InvalidArgumentException When the template is not an instance of TemplateReferenceInterface
     * @throws \InvalidArgumentException When the template file can not be found
     */
    public function locate($template, $currentPath = null, $first = true)
    {
    }
}
