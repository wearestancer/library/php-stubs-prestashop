<?php

namespace Symfony\Bundle\FrameworkBundle\Kernel;

/**
 * A Kernel that provides configuration hooks.
 *
 * @author Ryan Weaver <ryan@knpuniversity.com>
 * @author Fabien Potencier <fabien@symfony.com>
 */
trait MicroKernelTrait
{
    /**
     * Add or import routes into your application.
     *
     *     $routes->import('config/routing.yml');
     *     $routes->add('/admin', 'App\Controller\AdminController::dashboard', 'admin_dashboard');
     */
    protected abstract function configureRoutes(\Symfony\Component\Routing\RouteCollectionBuilder $routes);
    /**
     * Configures the container.
     *
     * You can register extensions:
     *
     *     $container->loadFromExtension('framework', [
     *         'secret' => '%secret%'
     *     ]);
     *
     * Or services:
     *
     *     $container->register('halloween', 'FooBundle\HalloweenProvider');
     *
     * Or parameters:
     *
     *     $container->setParameter('halloween', 'lot of fun');
     */
    protected abstract function configureContainer(\Symfony\Component\DependencyInjection\ContainerBuilder $container, \Symfony\Component\Config\Loader\LoaderInterface $loader);
    /**
     * {@inheritdoc}
     */
    public function registerContainerConfiguration(\Symfony\Component\Config\Loader\LoaderInterface $loader)
    {
    }
    /**
     * @internal
     */
    public function loadRoutes(\Symfony\Component\Config\Loader\LoaderInterface $loader)
    {
    }
}
