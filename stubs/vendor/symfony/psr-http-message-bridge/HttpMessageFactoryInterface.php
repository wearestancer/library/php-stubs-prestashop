<?php

namespace Symfony\Bridge\PsrHttpMessage;

/**
 * Creates PSR HTTP Request and Response instances from Symfony ones.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface HttpMessageFactoryInterface
{
    /**
     * Creates a PSR-7 Request instance from a Symfony one.
     *
     * @return ServerRequestInterface
     */
    public function createRequest(\Symfony\Component\HttpFoundation\Request $symfonyRequest);
    /**
     * Creates a PSR-7 Response instance from a Symfony one.
     *
     * @return ResponseInterface
     */
    public function createResponse(\Symfony\Component\HttpFoundation\Response $symfonyResponse);
}
