<?php

namespace Symfony\Bridge\PsrHttpMessage\EventListener;

/**
 * Converts PSR-7 Response to HttpFoundation Response using the bridge.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Alexander M. Turek <me@derrabus.de>
 */
final class PsrResponseListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public function __construct(\Symfony\Bridge\PsrHttpMessage\HttpFoundationFactoryInterface $httpFoundationFactory = null)
    {
    }
    /**
     * Do the conversion if applicable and update the response of the event.
     */
    public function onKernelView(\Symfony\Component\HttpKernel\Event\ViewEvent $event) : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() : array
    {
    }
}
