<?php

namespace Symfony\Bridge\PsrHttpMessage\Factory;

/**
 * {@inheritdoc}
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class HttpFoundationFactory implements \Symfony\Bridge\PsrHttpMessage\HttpFoundationFactoryInterface
{
    public function __construct(int $responseBufferMaxLength = 16372)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createRequest(\Psr\Http\Message\ServerRequestInterface $psrRequest, bool $streamed = false)
    {
    }
    /**
     * Gets a temporary file path.
     *
     * @return string
     */
    protected function getTemporaryPath()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createResponse(\Psr\Http\Message\ResponseInterface $psrResponse, bool $streamed = false)
    {
    }
}
