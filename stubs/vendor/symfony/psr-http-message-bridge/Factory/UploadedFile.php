<?php

namespace Symfony\Bridge\PsrHttpMessage\Factory;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class UploadedFile extends \Symfony\Component\HttpFoundation\File\UploadedFile
{
    public function __construct(\Psr\Http\Message\UploadedFileInterface $psrUploadedFile, callable $getTemporaryPath)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function move($directory, $name = null) : \Symfony\Component\HttpFoundation\File\File
    {
    }
}
