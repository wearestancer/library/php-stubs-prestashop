<?php

namespace Symfony\Bridge\PsrHttpMessage\Factory;

/**
 * Builds Psr\HttpMessage instances using a PSR-17 implementation.
 *
 * @author Antonio J. García Lagar <aj@garcialagar.es>
 */
class PsrHttpFactory implements \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface
{
    public function __construct(\Psr\Http\Message\ServerRequestFactoryInterface $serverRequestFactory, \Psr\Http\Message\StreamFactoryInterface $streamFactory, \Psr\Http\Message\UploadedFileFactoryInterface $uploadedFileFactory, \Psr\Http\Message\ResponseFactoryInterface $responseFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createRequest(\Symfony\Component\HttpFoundation\Request $symfonyRequest)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function createResponse(\Symfony\Component\HttpFoundation\Response $symfonyResponse)
    {
    }
}
