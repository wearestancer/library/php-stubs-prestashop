<?php

namespace Symfony\Bridge\PsrHttpMessage;

/**
 * Creates Symfony Request and Response instances from PSR-7 ones.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface HttpFoundationFactoryInterface
{
    /**
     * Creates a Symfony Request instance from a PSR-7 one.
     *
     * @return Request
     */
    public function createRequest(\Psr\Http\Message\ServerRequestInterface $psrRequest, bool $streamed = false);
    /**
     * Creates a Symfony Response instance from a PSR-7 one.
     *
     * @return Response
     */
    public function createResponse(\Psr\Http\Message\ResponseInterface $psrResponse, bool $streamed = false);
}
