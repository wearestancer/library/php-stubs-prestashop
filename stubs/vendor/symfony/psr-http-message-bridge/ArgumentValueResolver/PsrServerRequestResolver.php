<?php

namespace Symfony\Bridge\PsrHttpMessage\ArgumentValueResolver;

/**
 * Injects the RequestInterface, MessageInterface or ServerRequestInterface when requested.
 *
 * @author Iltar van der Berg <kjarli@gmail.com>
 * @author Alexander M. Turek <me@derrabus.de>
 */
final class PsrServerRequestResolver implements \Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface
{
    public function __construct(\Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface $httpMessageFactory)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function supports(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resolve(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata $argument) : \Traversable
    {
    }
}
