<?php

class AppKernel extends \Symfony\Component\HttpKernel\Kernel
{
    const VERSION = \PrestaShop\PrestaShop\Core\Version::VERSION;
    const MAJOR_VERSION_STRING = \PrestaShop\PrestaShop\Core\Version::MAJOR_VERSION_STRING;
    const MAJOR_VERSION = 8;
    const MINOR_VERSION = 2;
    const RELEASE_VERSION = 0;
    /**
     * Lock stream is saved as static field, this way if multiple AppKernel are instanciated (this can happen in
     * test environment, they will be able to detect that a lock has already been made by the current process).
     *
     * @var resource|null
     */
    protected static $lockStream = \null;
    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
    }
    /**
     * Perform a lock on a file before cache clear is performed, this lock will be unlocked once the cache has been cleared.
     * Until then any other process will have to wait until the file is unlocked.
     *
     * @return bool Returns boolean indicating if the lock file was successfully locked.
     */
    public function locksCacheClear() : bool
    {
    }
    public function unlocksCacheClear() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function shutdown()
    {
    }
    /**
     * The kernel and especially its container is cached in several PrestaShop classes, services or components So we
     * need to clear this cache everytime the kernel is shutdown, rebooted, reset, ...
     *
     * This is very important in test environment to avoid invalid mocks to stay accessible and used, but it's also
     * important because we may need to reboot the kernel (during module installation, after currency is installed
     * to reset CLDR cache, ...)
     */
    protected function cleanKernelReferences() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getKernelParameters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getRootDir()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getLogDir()
    {
    }
    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function registerContainerConfiguration(\Symfony\Component\Config\Loader\LoaderInterface $loader)
    {
    }
    /**
     * Gets the application root dir.
     * Override Kernel due to the fact that we remove the composer.json in
     * downloaded package. More we are not a framework and the root directory
     * should always be the parent of this file.
     *
     * @return string The project root dir
     */
    public function getProjectDir()
    {
    }
    protected function getContainerClearCacheLockPath() : string
    {
    }
    protected function waitUntilCacheClearIsOver() : void
    {
    }
    /**
     * @param resource $lockStream
     */
    protected function unlockCacheStream($lockStream) : void
    {
    }
}
