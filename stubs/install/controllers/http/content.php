<?php

/**
 * Step 5: configure content
 */
class InstallControllerHttpContent extends \InstallControllerHttp implements \HttpConfigureInterface
{
    public const MODULES_ALL = 0;
    public const MODULES_SELECTED = 1;
    /**
     * Modules present on the disk
     *
     * @var array
     */
    public $modules = [];
    /**
     * Themes present on the disk
     *
     * @var array
     */
    public $themes = [];
    /**
     * Define the current action for modules
     *
     * @var int
     */
    public $moduleAction = self::MODULES_ALL;
    /**
     * Define if the select all modules is selected
     *
     * @var bool
     */
    public $selectAllButton = \false;
    public function init() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function processNextStep() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function display() : void
    {
    }
    public function getModulesPerCategories() : array
    {
    }
    protected function sortModulesByDisplayName(\PrestaShop\PrestaShop\Core\Util\ArrayFinder $a, \PrestaShop\PrestaShop\Core\Util\ArrayFinder $b) : int
    {
    }
    protected function findModuleCategory(\PrestaShop\PrestaShop\Core\Util\ArrayFinder $module, array $categories)
    {
    }
}
