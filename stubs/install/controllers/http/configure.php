<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
/**
 * Step 4 : configure the shop and admin access
 */
class InstallControllerHttpConfigure extends \InstallControllerHttp implements \HttpConfigureInterface
{
    /**
     * @var array
     */
    public $list_activities = [];
    /**
     * @var array
     */
    public $list_countries = [];
    /**
     * @var string
     */
    public $install_type;
    /**
     * @var string
     */
    public $translatedStrings;
    /**
     * {@inheritdoc}
     */
    public function processNextStep() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process() : void
    {
    }
    /**
     * Obtain the timezone associated to an iso
     */
    public function processTimezoneByIso() : void
    {
    }
    /**
     * Get list of timezones
     *
     * @return array
     */
    public function getTimezones() : array
    {
    }
    /**
     * Get a timezone associated to an iso
     *
     * @param string $iso
     *
     * @return string
     */
    public function getTimezoneByIso($iso) : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function display() : void
    {
    }
    /**
     * Helper to display error for a field
     *
     * @param string $field
     *
     * @return string|null
     */
    public function displayError(string $field) : ?string
    {
    }
}
