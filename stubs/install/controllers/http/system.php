<?php

/**
 * Step 2 : check system configuration (permissions on folders, PHP version, etc.)
 */
class InstallControllerHttpSystem extends \InstallControllerHttp implements \HttpConfigureInterface
{
    public $tests = [];
    /**
     * @var System
     */
    public $model_system;
    /**
     * @var array
     */
    public $tests_render;
    /**
     * {@inheritdoc}
     */
    public function init() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function display() : void
    {
    }
}
