<?php

/**
 * Step 3 : configure database
 */
class InstallControllerHttpDatabase extends \InstallControllerHttp implements \HttpConfigureInterface
{
    /**
     * @var Database
     */
    public $model_database;
    /**
     * @var string
     */
    public $database_server;
    /**
     * @var string
     */
    public $database_name;
    /**
     * @var string
     */
    public $database_login;
    /**
     * @var string
     */
    public $database_password;
    /**
     * @var string
     */
    public $database_engine;
    /**
     * @var string
     */
    public $database_prefix;
    /**
     * @var bool
     */
    public $database_clear;
    /**
     * @var bool
     */
    public $use_smtp;
    /**
     * @var string
     */
    public $smtp_encryption;
    /**
     * @var int
     */
    public $smtp_port;
    /**
     * {@inheritdoc}
     */
    public function init() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function processNextStep() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate() : bool
    {
    }
    /**
     * {@inheritdoc}
     */
    public function process() : void
    {
    }
    /**
     * Check if a connection to database is possible with these data
     */
    public function processCheckDb() : void
    {
    }
    /**
     * Attempt to create the database
     */
    public function processCreateDb() : void
    {
    }
    /**
     * {@inheritdoc}
     */
    public function display() : void
    {
    }
}
