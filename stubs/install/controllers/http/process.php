<?php

class InstallControllerHttpProcess extends \InstallControllerHttp implements \HttpConfigureInterface
{
    /** @var Install */
    protected $model_install;
    public $process_steps = [];
    public $previous_button = \false;
    public function init() : void
    {
    }
    /**
     * @see HttpConfigureInterface::processNextStep()
     */
    public function processNextStep() : void
    {
    }
    /**
     * @see HttpConfigureInterface::validate()
     */
    public function validate() : bool
    {
    }
    public function initializeContext()
    {
    }
    public function process() : void
    {
    }
    /**
     * PROCESS : generateSettingsFile
     */
    public function processGenerateSettingsFile()
    {
    }
    /**
     * PROCESS : installDatabase
     * Create database structure
     */
    public function processInstallDatabase()
    {
    }
    /**
     * PROCESS : installDefaultData
     * Create default shop and languages
     */
    public function processInstallDefaultData()
    {
    }
    /**
     * PROCESS : populateDatabase
     * Populate database with default data
     */
    public function processPopulateDatabase()
    {
    }
    /**
     * PROCESS : configureShop
     * Set default shop configuration
     */
    public function processConfigureShop()
    {
    }
    /**
     * PROCESS : installModules
     * Install all modules in ~/modules/ directory
     */
    public function processInstallModules()
    {
    }
    /**
     * PROCESS: Post installation execution
     */
    public function processPostInstall() : void
    {
    }
    /**
     * PROCESS : installFixtures
     * Install fixtures (E.g. demo products)
     */
    public function processInstallFixtures()
    {
    }
    /**
     * PROCESS : installTheme
     * Install theme
     */
    public function processInstallTheme()
    {
    }
    /**
     * @see HttpConfigureInterface::display()
     */
    public function display() : void
    {
    }
}
