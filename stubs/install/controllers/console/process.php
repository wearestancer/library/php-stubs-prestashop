<?php

class InstallControllerConsoleProcess extends \InstallControllerConsole implements \HttpConfigureInterface
{
    public $process_steps = [];
    public $previous_button = \false;
    /**
     * @var Install
     */
    protected $model_install;
    /**
     * @var Database
     */
    protected $model_database;
    public function init()
    {
    }
    /**
     * @see HttpConfigureInterface::processNextStep()
     */
    public function processNextStep()
    {
    }
    public function display()
    {
    }
    /**
     * @see HttpConfigureInterface::validate()
     */
    public function validate()
    {
    }
    public function initializeContext()
    {
    }
    public function process()
    {
    }
    /**
     * PROCESS : generateSettingsFile
     */
    public function processGenerateSettingsFile()
    {
    }
    /**
     * PROCESS : installDatabase
     * Create database structure
     */
    public function processInstallDatabase()
    {
    }
    /**
     * PROCESS : installDefaultData
     * Create default shop and languages
     */
    public function processInstallDefaultData()
    {
    }
    /**
     * PROCESS : populateDatabase
     * Populate database with default data
     */
    public function processPopulateDatabase()
    {
    }
    /**
     * PROCESS : configureShop
     * Set default shop configuration
     */
    public function processConfigureShop()
    {
    }
    /**
     * PROCESS : installModules
     * Install all modules in ~/modules/ directory
     */
    public function processInstallModules()
    {
    }
    /**
     * PROCESS : installFixtures
     * Install fixtures (E.g. demo products)
     */
    public function processInstallFixtures()
    {
    }
    /**
     * Process post install execution
     */
    public function processPostInstall() : bool
    {
    }
    /**
     * PROCESS : installTheme
     * Install theme
     */
    public function processInstallTheme()
    {
    }
}
