<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
/**
 * Manage session for install script
 *
 * @property string $last_step
 * @property string|null $lang
 * @property array $process_validated
 * @property string $install_type
 * @property bool $database_clear
 * @property string $step
 * @property string $database_server
 * @property string $database_login
 * @property string $database_password
 * @property string $database_name
 * @property string $database_prefix
 * @property string $database_engine
 * @property string $shop_name
 * @property array $xml_loader_ids
 * @property int $shop_activity
 * @property string $shop_country
 * @property string $admin_firstname
 * @property string $admin_lastname
 * @property string $admin_password
 * @property string $admin_password_confirm
 * @property string $admin_email
 * @property string $shop_timezone
 * @property bool $configuration_agrement
 * @property bool $licence_agrement
 * @property bool $enable_ssl
 * @property int $rewrite_engine
 * @property bool $use_smtp
 * @property string $smtp_encryption
 * @property int $smtp_port
 * @property array $content_modules
 * @property string $content_theme
 * @property bool $content_install_fixtures
 * @property int $moduleAction
 */
class InstallSession
{
    protected static $_instance;
    protected static $_cookie_mode = \false;
    protected static $_cookie = \false;
    public static function getInstance()
    {
    }
    public function __construct()
    {
    }
    public function clean()
    {
    }
    public function &__get($varname)
    {
    }
    public function __set($varname, $value)
    {
    }
    public function __isset($varname)
    {
    }
    public function __unset($varname)
    {
    }
}
