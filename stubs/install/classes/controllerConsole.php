<?php

abstract class InstallControllerConsole
{
    /**
     * @var array List of installer steps
     */
    protected static $steps = ['process'];
    protected static $instances = [];
    /**
     * @var string Current step
     */
    public $step;
    /**
     * @var array List of errors
     */
    public $errors = [];
    public $controller;
    /**
     * @var InstallSession
     */
    public $session;
    /**
     * @var LanguageList
     */
    public $language;
    /**
     * @var AbstractInstall
     */
    protected $model_install;
    /**
     * @var \PrestaShopBundle\Install\Database
     */
    protected $model_database;
    /**
     * @var Datas|null
     */
    public $datas;
    /**
     * @var TranslatorComponent|null
     */
    public $translator;
    /**
     * Validate current step.
     */
    public abstract function validate();
    public static final function execute($argc, $argv)
    {
    }
    public final function __construct($step)
    {
    }
    /**
     * Initialize model.
     */
    public function init()
    {
    }
    public function printErrors()
    {
    }
    public function process()
    {
    }
}
