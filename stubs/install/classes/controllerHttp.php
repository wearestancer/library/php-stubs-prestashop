<?php

class InstallControllerHttp
{
    /**
     * @var StepList List of installer steps
     */
    protected static $steps;
    /**
     * @var string
     */
    protected $content;
    /**
     * @var array
     */
    protected static $instances = [];
    /**
     * @var string Current step
     */
    public $step;
    /**
     * @var array List of errors
     */
    public $errors = [];
    public $controller;
    /**
     * @var InstallSession
     */
    public $session;
    /**
     * LanguageList
     */
    public $language;
    /**
     * @var \Symfony\Component\Translation\Translator
     */
    public $translator;
    /**
     * @var bool If false, disable next button access
     */
    public $next_button = \true;
    /**
     * @var bool If false, disable previous button access
     */
    public $previous_button = \true;
    /**
     * @var \PrestaShopBundle\Install\AbstractInstall
     */
    public $model;
    /**
     * @var array Magic vars
     */
    protected $__vars = [];
    /**
     * @var array Configuration
     */
    protected static $config;
    public function __construct()
    {
    }
    public function setCurrentStep($step)
    {
    }
    public static final function execute()
    {
    }
    /**
     * Display controller view
     */
    public function display() : void
    {
    }
    /**
     * Validate current step
     */
    public function validate() : bool
    {
    }
    /**
     * Initialize
     */
    public function init() : void
    {
    }
    /**
     * Process installation
     */
    public function process() : void
    {
    }
    /**
     * Process next step
     */
    public function processNextStep() : void
    {
    }
    /**
     * Get steps list
     *
     * @return StepList
     */
    public static function getSteps() : ?\StepList
    {
    }
    public function getLastStep()
    {
    }
    /**
     * Find offset of a step by name
     *
     * @param string $step Step name
     *
     * @return int
     */
    public function getStepOffset($step)
    {
    }
    /**
     * Make a HTTP redirection to a step
     *
     * @param string $step
     */
    public function redirect(string $step)
    {
    }
    /**
     * Check if current step is first step in list of steps
     *
     * @return bool
     */
    public function isFirstStep()
    {
    }
    /**
     * Check if current step is last step in list of steps
     *
     * @return bool
     */
    public function isLastStep()
    {
    }
    /**
     * Check is given step is already finished
     *
     * @param string $step
     *
     * @return bool
     */
    public function isStepFinished(string $step) : bool
    {
    }
    /**
     * Send AJAX response in JSON format {success: bool, message: string}
     *
     * @param bool $success
     * @param string $message
     */
    public function ajaxJsonAnswer(bool $success, $message = '') : void
    {
    }
    /**
     * Display a template
     *
     * @param string $template Template name
     */
    public function getTemplate(string $template) : string
    {
    }
    /**
     * Display a hook template
     *
     * @param string $template Template name
     */
    public function getHook(string $template) : string
    {
    }
    protected function loadConfiguration() : void
    {
    }
    public function getConfig(string $element)
    {
    }
    public function displayContent(string $content) : void
    {
    }
    protected function setContent(string $content) : void
    {
    }
    protected function getContent() : string
    {
    }
    protected function renderTemplate(string $path, string $template) : string
    {
    }
    public function &__get($varname)
    {
    }
    public function __set($varname, $value)
    {
    }
    public function __isset($varname)
    {
    }
    public function __unset($varname)
    {
    }
}
