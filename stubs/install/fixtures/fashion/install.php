<?php

/**
 * This class is only here to show the possibility of extending InstallXmlLoader, which is the
 * class parsing all XML files, copying all images, etc.
 *
 * Please read documentation in ~/install/dev/ folder if you want to customize PrestaShop install / fixtures.
 */
class InstallFixturesFashion extends \PrestaShopBundle\Install\XmlLoader
{
    public function createEntityCustomer($identifier, array $data, array $data_lang)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function populateFromXmlFiles()
    {
    }
}
