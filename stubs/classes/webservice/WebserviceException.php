<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
class WebserviceExceptionCore extends \Exception
{
    protected $status;
    /**
     * @var string
     */
    protected $wrong_value;
    /**
     * @var array
     */
    protected $available_values;
    protected $type;
    public const SIMPLE = 0;
    public const DID_YOU_MEAN = 1;
    public function __construct($message, $code)
    {
    }
    public function getType()
    {
    }
    public function setType($type)
    {
    }
    public function setStatus($status)
    {
    }
    public function getStatus()
    {
    }
    /**
     * @return string
     */
    public function getWrongValue()
    {
    }
    /**
     * @param string $wrong_value
     * @param array $available_values
     *
     * @return self
     */
    public function setDidYouMean($wrong_value, $available_values)
    {
    }
    public function getAvailableValues()
    {
    }
}
