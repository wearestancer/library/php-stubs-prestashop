<?php

class CheckoutDeliveryStepCore extends \AbstractCheckoutStep
{
    protected $template = 'checkout/_partials/steps/shipping.tpl';
    public function setRecyclablePackAllowed($recyclablePackAllowed)
    {
    }
    public function isRecyclablePackAllowed()
    {
    }
    public function setGiftAllowed($giftAllowed)
    {
    }
    public function isGiftAllowed()
    {
    }
    public function setGiftCost($giftCost)
    {
    }
    public function getGiftCost()
    {
    }
    public function setIncludeTaxes($includeTaxes)
    {
    }
    public function getIncludeTaxes()
    {
    }
    public function setDisplayTaxesLabel($displayTaxesLabel)
    {
    }
    public function getDisplayTaxesLabel()
    {
    }
    public function getGiftCostForLabel()
    {
    }
    public function handleRequest(array $requestParams = [])
    {
    }
    public function render(array $extraParams = [])
    {
    }
    protected function isModuleComplete($requestParams)
    {
    }
}
