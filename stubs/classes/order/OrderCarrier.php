<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
class OrderCarrierCore extends \ObjectModel
{
    /** @var int */
    public $id_order_carrier;
    /** @var int */
    public $id_order;
    /** @var int */
    public $id_carrier;
    /** @var int */
    public $id_order_invoice;
    /** @var float */
    public $weight;
    /** @var float */
    public $shipping_cost_tax_excl;
    /** @var float */
    public $shipping_cost_tax_incl;
    /** @var string */
    public $tracking_number;
    /** @var string Object creation date */
    public $date_add;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'order_carrier', 'primary' => 'id_order_carrier', 'fields' => ['id_order' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => \true], 'id_carrier' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => \true], 'id_order_invoice' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'], 'weight' => ['type' => self::TYPE_FLOAT, 'validate' => 'isFloat'], 'shipping_cost_tax_excl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isFloat'], 'shipping_cost_tax_incl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isFloat'], 'tracking_number' => ['type' => self::TYPE_STRING, 'validate' => 'isTrackingNumber'], 'date_add' => ['type' => self::TYPE_DATE, 'validate' => 'isDate']]];
    protected $webserviceParameters = ['objectMethods' => ['update' => 'updateWs'], 'fields' => ['id_order' => ['xlink_resource' => 'orders'], 'id_carrier' => ['xlink_resource' => 'carriers']]];
    /**
     * @param Order $order Required
     *
     * @return bool
     */
    public function sendInTransitEmail($order)
    {
    }
    public function updateWs()
    {
    }
}
