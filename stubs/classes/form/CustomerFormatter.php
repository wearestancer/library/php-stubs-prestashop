<?php

class CustomerFormatterCore implements \FormFormatterInterface
{
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator, \Language $language)
    {
    }
    public function setAskForBirthdate($ask_for_birthdate)
    {
    }
    public function setAskForPartnerOptin($ask_for_partner_optin)
    {
    }
    public function setPartnerOptinRequired($partner_optin_is_required)
    {
    }
    public function setAskForPassword($ask_for_password)
    {
    }
    public function setAskForNewPassword($ask_for_new_password)
    {
    }
    public function setPasswordRequired($password_is_required)
    {
    }
    public function getFormat()
    {
    }
}
