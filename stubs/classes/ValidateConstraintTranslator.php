<?php

/**
 * Class ValidateConstraintTranslatorCore.
 */
class ValidateConstraintTranslatorCore
{
    /**
     * ValidateConstraintTranslatorCore constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * @param string $validator
     *
     * @return string
     */
    public function translate($validator)
    {
    }
}
