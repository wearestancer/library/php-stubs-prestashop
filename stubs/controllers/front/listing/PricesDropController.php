<?php

class PricesDropControllerCore extends \ProductListingFrontController
{
    /** @var string */
    public $php_self = 'prices-drop';
    public function getCanonicalURL() : string
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initContent()
    {
    }
    /**
     * @return ProductSearchQuery
     */
    protected function getProductSearchQuery()
    {
    }
    /**
     * @return PricesDropProductSearchProvider
     */
    protected function getDefaultProductSearchProvider()
    {
    }
    public function getListingLabel()
    {
    }
    public function getBreadcrumbLinks()
    {
    }
}
