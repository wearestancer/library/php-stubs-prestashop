<?php

class BestSalesControllerCore extends \ProductListingFrontController
{
    /** @var string */
    public $php_self = 'best-sales';
    public function getCanonicalURL() : string
    {
    }
    /**
     * Initializes controller.
     *
     * @see FrontController::init()
     *
     * @throws PrestaShopException
     */
    public function init()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initContent()
    {
    }
    /**
     * @return ProductSearchQuery
     */
    protected function getProductSearchQuery()
    {
    }
    /**
     * @return BestSalesProductSearchProvider
     */
    protected function getDefaultProductSearchProvider()
    {
    }
    public function getListingLabel()
    {
    }
    public function getBreadcrumbLinks()
    {
    }
}
