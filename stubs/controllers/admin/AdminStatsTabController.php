<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
abstract class AdminStatsTabControllerCore extends \AdminController
{
    public function init()
    {
    }
    public function initContent()
    {
    }
    public function initPageHeaderToolbar()
    {
    }
    public function displayCalendar()
    {
    }
    public static function displayCalendarForm($translations, $token, $action = \null, $table = \null, $identifier = \null, $id = \null)
    {
    }
    /* Not used anymore, but still work */
    protected function displayEngines()
    {
    }
    public function displayMenu()
    {
    }
    public function checkModulesNames($a, $b) : int
    {
    }
    protected function getModules()
    {
    }
    public function displayStats()
    {
    }
    public function postProcess()
    {
    }
    public function processDateRange()
    {
    }
    public function ajaxProcessSetDashboardDateRange()
    {
    }
    protected function getDate()
    {
    }
}
