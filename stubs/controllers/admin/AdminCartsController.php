<?php

/**
 * @property Cart $object
 */
class AdminCartsControllerCore extends \AdminController
{
    public function __construct()
    {
    }
    public function initPageHeaderToolbar()
    {
    }
    public function renderKpis()
    {
    }
    /**
     * @return string|void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws LocalizationException
     */
    public function renderView()
    {
    }
    public function ajaxPreProcess()
    {
    }
    public function ajaxProcessDeleteProduct()
    {
    }
    public function ajaxProcessUpdateCustomizationFields()
    {
    }
    public function ajaxProcessUpdateQty()
    {
    }
    public function ajaxProcessUpdateDeliveryOption()
    {
    }
    public function ajaxProcessUpdateOrderMessage()
    {
    }
    public function ajaxProcessUpdateCurrency()
    {
    }
    public function ajaxProcessUpdateLang()
    {
    }
    public function ajaxProcessDuplicateOrder()
    {
    }
    public function ajaxProcessDeleteVoucher()
    {
    }
    public function ajaxProcessupdateFreeShipping()
    {
    }
    public function ajaxProcessAddVoucher()
    {
    }
    public function ajaxProcessUpdateAddress()
    {
    }
    public function ajaxProcessUpdateAddresses()
    {
    }
    protected function getCartSummary()
    {
    }
    protected function getDeliveryOptionList()
    {
    }
    public function displayAjaxSearchCarts()
    {
    }
    public function ajaxReturnVars()
    {
    }
    public function initToolbar()
    {
    }
    /**
     * Display an image as a download.
     */
    public function displayAjaxCustomizationImage()
    {
    }
    public function displayAjaxGetSummary()
    {
    }
    public function ajaxProcessUpdateProductPrice()
    {
    }
    public static function getOrderTotalUsingTaxCalculationMethod($id_cart)
    {
    }
    public function displayDeleteLink($token, $id, $name = \null)
    {
    }
    public function renderList()
    {
    }
    /**
     * @param string|null $orderBy
     * @param string|null $orderDirection
     *
     * @return string
     *
     * @throws PrestaShopException
     */
    protected function getOrderByClause($orderBy, $orderDirection)
    {
    }
}
