<?php

/**
 * @property Store $object
 */
class AdminStoresControllerCore extends \AdminController
{
    public function __construct()
    {
    }
    public function renderOptions()
    {
    }
    public function initToolbar()
    {
    }
    public function initPageHeaderToolbar()
    {
    }
    public function renderList()
    {
    }
    /**
     * @return string|void
     *
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function renderForm()
    {
    }
    public function postProcess()
    {
    }
    protected function postImage($id)
    {
    }
    protected function _getDefaultFieldsContent()
    {
    }
    protected function _buildOrderedFieldsShop($formFields)
    {
    }
    public function beforeUpdateOptions()
    {
    }
    public function updateOptionPsShopCountryId($value)
    {
    }
    public function updateOptionPsShopStateId($value)
    {
    }
    /**
     * Adapt the format of hours.
     *
     * @param array $value
     *
     * @return array
     */
    protected function adaptHoursFormat($value)
    {
    }
}
