<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
/**
 * @property Country $object
 */
class AdminCountriesControllerCore extends \AdminController
{
    public function __construct()
    {
    }
    public function initPageHeaderToolbar()
    {
    }
    /**
     * AdminController::setMedia() override.
     *
     * @see AdminController::setMedia()
     */
    public function setMedia($isNewTheme = \false)
    {
    }
    public function renderList()
    {
    }
    /**
     * @return string|void
     *
     * @throws SmartyException
     */
    public function renderForm()
    {
    }
    public function processUpdate()
    {
    }
    public function postProcess()
    {
    }
    public function processSave()
    {
    }
    /**
     * @return bool|ObjectModel|null
     *
     * @throws PrestaShopException
     */
    public function processStatus()
    {
    }
    /**
     * Allow the assignation of zone only if the form is displayed.
     *
     * @return bool|void
     */
    protected function processBulkAffectZone()
    {
    }
    protected function displayValidFields()
    {
    }
    public static function displayCallPrefix($prefix)
    {
    }
}
