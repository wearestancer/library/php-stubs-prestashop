<?php

class AdminLoginControllerCore extends \AdminController
{
    public function __construct()
    {
    }
    public function setMedia($isNewTheme = \false)
    {
    }
    public function initContent()
    {
    }
    public function checkToken()
    {
    }
    /**
     * All BO users can access the login page.
     *
     * @return bool
     */
    public function viewAccess($disable = \false)
    {
    }
    public function postProcess()
    {
    }
    public function processLogin()
    {
    }
    public function processForgot()
    {
    }
    public function processReset()
    {
    }
}
