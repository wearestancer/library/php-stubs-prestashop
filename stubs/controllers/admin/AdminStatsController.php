<?php

class AdminStatsControllerCore extends \AdminStatsTabController
{
    public static function getVisits($unique, $date_from, $date_to, $granularity = \false)
    {
    }
    public static function getAbandonedCarts($date_from, $date_to)
    {
    }
    public static function getInstalledModules()
    {
    }
    public static function getDisabledModules()
    {
    }
    public static function getModulesToUpdate()
    {
    }
    public static function getPercentProductStock()
    {
    }
    public static function getPercentProductOutOfStock()
    {
    }
    public static function getProductAverageGrossMargin()
    {
    }
    public static function getDisabledCategories()
    {
    }
    public static function getTotalCategories()
    {
    }
    public static function getDisabledProducts()
    {
    }
    public static function getTotalProducts()
    {
    }
    public static function getTotalSales($date_from, $date_to, $granularity = \false)
    {
    }
    public static function get8020SalesCatalog($date_from, $date_to)
    {
    }
    public static function getOrders($date_from, $date_to, $granularity = \false)
    {
    }
    public static function getEmptyCategories()
    {
    }
    public static function getCustomerMainGender()
    {
    }
    public static function getBestCategory($date_from, $date_to)
    {
    }
    public static function getMainCountry($date_from, $date_to)
    {
    }
    public static function getAverageCustomerAge()
    {
    }
    public static function getPendingMessages()
    {
    }
    public static function getAverageMessageResponseTime($date_from, $date_to)
    {
    }
    public static function getMessagesPerThread($date_from, $date_to)
    {
    }
    public static function getPurchases($date_from, $date_to, $granularity = \false)
    {
    }
    public static function getExpenses($date_from, $date_to, $granularity = \false)
    {
    }
    public function displayAjaxGetKpi()
    {
    }
    /**
     * Display graphs on the stats page from module data.
     */
    public function displayAjaxGraphDraw()
    {
    }
    /**
     * Display grid with module data on the stats page.
     */
    public function displayAjaxGraphGrid()
    {
    }
}
