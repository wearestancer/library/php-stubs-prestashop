<?php

class GridHtml extends \ModuleGridEngine
{
    /**
     * @var string
     */
    public $_title;
    /**
     * @var int
     */
    public $_width;
    /**
     * @var int
     */
    public $_height;
    /**
     * @var int
     */
    public $_totalCount;
    /**
     * @var int
     */
    public $_start;
    /**
     * @var int
     */
    public $_limit;
    /**
     * GridHtml constructor.
     *
     * @param string|null $type
     */
    public function __construct($type = \null)
    {
    }
    public function install()
    {
    }
    /**
     * @param array $params
     * @param string $grider
     */
    public static function hookGridEngine($params, $grider)
    {
    }
    public function setColumnsInfos(&$infos)
    {
    }
    /**
     * @param array $values
     */
    public function setValues($values)
    {
    }
    /**
     * @param string $title
     */
    public function setTitle($title)
    {
    }
    /**
     * @param int $width
     * @param int $height
     */
    public function setSize($width, $height)
    {
    }
    /**
     * @param int $totalCount
     */
    public function setTotalCount($totalCount)
    {
    }
    /**
     * @param int $start
     * @param int $limit
     */
    public function setLimit($start, $limit)
    {
    }
    public function render()
    {
    }
}
