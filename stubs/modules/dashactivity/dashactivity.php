<?php

class dashactivity extends \Module
{
    protected static $colors = ['#1F77B4', '#FF7F0E', '#2CA02C'];
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function hookActionAdminControllerSetMedia()
    {
    }
    public function hookDashboardZoneOne($params)
    {
    }
    public function hookDashboardData($params)
    {
    }
    protected function getChartTrafficSource($date_from, $date_to)
    {
    }
    protected function getTrafficSources($date_from, $date_to)
    {
    }
    protected function getReferer($date_from, $date_to, $limit = 3)
    {
    }
    public function renderConfigForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
}
