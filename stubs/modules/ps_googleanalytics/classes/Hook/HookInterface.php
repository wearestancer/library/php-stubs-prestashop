<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Hooks;

interface HookInterface
{
    /**
     * @param Ps_Googleanalytics $module
     * @param Context $context
     */
    public function __construct(\Ps_Googleanalytics $module, \Context $context);
    public function run();
}
