<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Hooks;

class HookDisplayHeader implements \PrestaShop\Module\Ps_Googleanalytics\Hooks\HookInterface
{
    public function __construct(\Ps_Googleanalytics $module, \Context $context)
    {
    }
    /**
     * @return false|string
     */
    public function run()
    {
    }
    /**
     * @param bool $backOffice
     */
    public function setBackOffice($backOffice)
    {
    }
}
