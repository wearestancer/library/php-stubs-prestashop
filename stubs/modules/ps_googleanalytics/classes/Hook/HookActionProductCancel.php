<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Hooks;

class HookActionProductCancel implements \PrestaShop\Module\Ps_Googleanalytics\Hooks\HookInterface
{
    public function __construct(\Ps_Googleanalytics $module, \Context $context)
    {
    }
    /**
     * run
     *
     * @return void
     */
    public function run()
    {
    }
    /**
     * setParams
     *
     * @param array $params
     */
    public function setParams($params)
    {
    }
    /**
     * @param int $idOrder
     * @param string $idProduct
     * @param float $quantity
     * @param string $nameProduct
     */
    protected function getGoogleAnalytics4($idOrder, $idProduct, $quantity, $nameProduct)
    {
    }
}
