<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Hooks;

class HookActionValidateOrder implements \PrestaShop\Module\Ps_Googleanalytics\Hooks\HookInterface
{
    public function __construct(\Ps_Googleanalytics $module, \Context $context)
    {
    }
    /**
     * run
     *
     * @return void
     */
    public function run()
    {
    }
    /**
     * setParams
     *
     * @param array $params
     */
    public function setParams($params)
    {
    }
}
