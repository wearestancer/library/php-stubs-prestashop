<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Hooks;

class HookDisplayBeforeBodyClosingTag implements \PrestaShop\Module\Ps_Googleanalytics\Hooks\HookInterface
{
    public function __construct(\Ps_Googleanalytics $module, \Context $context)
    {
    }
    /**
     * run
     *
     * @return string
     */
    public function run()
    {
    }
}
