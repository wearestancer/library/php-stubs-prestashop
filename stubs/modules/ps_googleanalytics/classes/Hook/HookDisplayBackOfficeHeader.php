<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Hooks;

class HookDisplayBackOfficeHeader implements \PrestaShop\Module\Ps_Googleanalytics\Hooks\HookInterface
{
    public function __construct(\Ps_Googleanalytics $module, \Context $context)
    {
    }
    /**
     * run
     *
     * @return string
     */
    public function run()
    {
    }
    /**
     * Checks if there are any orders that failed to be sent normally through front office and processes them
     */
    protected function processFailedOrders()
    {
    }
    /**
     * Checks if there are any manual orders in cookie and processes them
     */
    protected function processManualOrders()
    {
    }
    /**
     * Renders tracking code for given order
     *
     * @param int $idOrder
     */
    public function processOrder($idOrder)
    {
    }
}
