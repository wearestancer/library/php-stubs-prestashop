<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Database;

class Install
{
    public function __construct(\Ps_Googleanalytics $module)
    {
    }
    /**
     * installTables
     *
     * @return bool
     */
    public function installTables()
    {
    }
    /**
     * Insert default data to database
     *
     * @return bool
     */
    public function setDefaultConfiguration()
    {
    }
    /**
     * Register Module hooks
     *
     * @return bool
     */
    public function registerHooks()
    {
    }
    /**
     * Installs hidden tab for our ajax controller
     *
     * @return bool
     */
    public function installTab()
    {
    }
}
