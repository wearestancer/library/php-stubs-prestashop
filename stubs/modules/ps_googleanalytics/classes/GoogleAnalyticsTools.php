<?php

namespace PrestaShop\Module\Ps_Googleanalytics;

class GoogleAnalyticsTools
{
    /**
     * Renders purchase event for order
     *
     * @param array $orderProducts
     * @param array $orderData
     * @param string $callbackUrl
     *
     * @return string|void
     */
    public function renderPurchaseEvent($orderProducts, $orderData, $callbackUrl)
    {
    }
    /**
     * Encodes array of data into JSON, optionally ignoring some of the values
     *
     * @param array $data Data pairs
     * @param array $ignoredKeys Values of these keys won't be encoded, for literal output of functions
     *
     * @return string json encoded data
     */
    public function jsonEncodeWithBlacklist($data, $ignoredKeys = [])
    {
    }
    /**
     * Renders gtag event and encodes the data. You can optionally pass which data keys you want to
     * output in a raw way - callbacks for example.
     *
     * @param string $eventName
     * @param array $eventData
     * @param array $ignoredKeys Values of these keys won't be encoded, for literal output of functions
     *
     * @return string render gtag event for output
     */
    public function renderEvent($eventName, $eventData, $ignoredKeys = [])
    {
    }
}
