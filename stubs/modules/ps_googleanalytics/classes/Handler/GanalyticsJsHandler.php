<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Handler;

class GanalyticsJsHandler
{
    public function __construct(\Ps_Googleanalytics $module, \Context $context)
    {
    }
    /**
     * Generate Google Analytics js
     *
     * @param string $jsCode
     *
     * @return void|string
     */
    public function generate($jsCode)
    {
    }
}
