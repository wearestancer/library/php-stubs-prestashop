<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Handler;

class GanalyticsDataHandler
{
    /**
     * __construct
     *
     * @param int $cartId
     * @param int $shopId
     */
    public function __construct($cartId, $shopId)
    {
    }
    /**
     * readData
     *
     * @return array
     */
    public function readData()
    {
    }
    /**
     * Deletes all persisted data, probably because it was flushed.
     *
     * @return bool
     */
    public function deleteData()
    {
    }
    /**
     * Stores event into data repository so we can output it
     * on first available chance.
     *
     * @param string $dataToPersist
     *
     * @return bool
     */
    public function persistData($dataToPersist)
    {
    }
    /**
     * Check if the json is valid and returns an empty array if not
     *
     * @param string $json
     *
     * @return array
     */
    protected function jsonDecodeValidJson($json)
    {
    }
}
