<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Handler;

class ModuleHandler
{
    /**
     * @param string $moduleName
     *
     * @return bool
     */
    public function isModuleEnabled($moduleName)
    {
    }
    /**
     * @param string $moduleName
     * @param string $hookName
     *
     * @return bool
     */
    public function isModuleEnabledAndHookedOn($moduleName, $hookName)
    {
    }
    /**
     * @param string $moduleName
     *
     * @return bool
     */
    public function uninstallModule($moduleName)
    {
    }
}
