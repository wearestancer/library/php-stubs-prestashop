<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Form;

class ConfigurationForm
{
    public function __construct(\Ps_Googleanalytics $module)
    {
    }
    /**
     * generate
     *
     * @return string
     */
    public function generate()
    {
    }
    /**
     * treat the form datas if submited
     *
     * @return string
     */
    public function treat()
    {
    }
}
