<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Repository;

class GanalyticsRepository
{
    const TABLE_NAME = 'ganalytics';
    /**
     * Finds if we have a record for this order ID.
     *
     * @param int $orderId
     *
     * @return mixed
     */
    public function findGaOrderByOrderId($orderId)
    {
    }
    /**
     * Checks if order is already sent to GA
     *
     * @param int $idOrder
     *
     * @return bool
     */
    public function hasOrderBeenAlreadySent($idOrder)
    {
    }
    /**
     * findAllByShopIdAndDateAdd
     *
     * @param int $shopId
     *
     * @return array
     */
    public function findAllByShopIdAndDateAdd($shopId)
    {
    }
    /**
     * addNewRow
     *
     * @param array $data
     * @param int $type
     *
     * @return bool
     */
    public function addNewRow(array $data, $type = \Db::INSERT_IGNORE)
    {
    }
    /**
     * Adds new order into repository
     *
     * @param int $idOrder
     * @param int $idShop
     *
     * @return bool
     */
    public function addOrder(int $idOrder, int $idShop)
    {
    }
    /**
     * updateData
     *
     * @param array $data
     * @param string $where
     * @param int $limit
     *
     * @return bool
     */
    public function updateData($data, $where, $limit = 0)
    {
    }
    /**
     * Marks order as successfully sent to GA via callback
     *
     * @param int $idOrder
     *
     * @return bool
     */
    public function markOrderAsSent($idOrder)
    {
    }
}
