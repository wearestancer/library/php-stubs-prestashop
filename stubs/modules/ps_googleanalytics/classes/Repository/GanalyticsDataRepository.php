<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Repository;

class GanalyticsDataRepository
{
    const TABLE_NAME = 'ganalytics_data';
    /**
     * findByCartId
     *
     * @param int $cartId
     * @param int $shopId
     *
     * @return mixed
     */
    public function findDataByCartIdAndShopId($cartId, $shopId)
    {
    }
    /**
     * addNewRow
     *
     * @param int $cartId
     * @param int $shopId
     * @param string $data
     *
     * @return bool
     */
    public function addNewRow($cartId, $shopId, $data)
    {
    }
    /**
     * deleteRow
     *
     * @param int $cartId
     * @param int $shopId
     *
     * @return bool
     */
    public function deleteRow($cartId, $shopId)
    {
    }
}
