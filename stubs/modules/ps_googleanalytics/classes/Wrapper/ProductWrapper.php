<?php

namespace PrestaShop\Module\Ps_Googleanalytics\Wrapper;

class ProductWrapper
{
    public function __construct(\Context $context)
    {
    }
    /**
     * Takes provided list of product (lazy) arrays and converts it to a format that GA4 requires.
     *
     * @param array $productList
     * @param bool $useProvidedQuantity Should provided quantity be used, usually for cart related events
     *
     * @return array Item data standardized for GA
     */
    public function prepareItemListFromProductList($productList, $useProvidedQuantity = false)
    {
    }
    /**
     * Takes provided (lazy) array and converts it to a format that GA4 requires. It can handle:
     * - ProductLazyArray from product page
     * - ProductListingLazyArray from presented listings
     * - ProductListingLazyArray from presented cart
     * - Raw $cart->getProducts()
     * - Legacy product object converted to an array enriched with Product::getProductProperties
     *
     * @param ProductLazyArray|ProductListingLazyArray|array $product
     * @param bool $useProvidedQuantity Should provided quantity be used, usually for cart related events
     *
     * @return array Item data standardized for GA
     */
    public function prepareItemFromProduct($product, $useProvidedQuantity = false)
    {
    }
    /**
     * Method that will provide product combination attribute in the same format and order as cart does.
     *
     * @param int $id_product_attribute ID of the combination
     *
     * @return string Attribute list
     */
    public function getProductVariant($id_product_attribute)
    {
    }
}
