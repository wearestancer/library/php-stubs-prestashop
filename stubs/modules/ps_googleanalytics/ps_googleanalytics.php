<?php

class Ps_Googleanalytics extends \Module
{
    public $name;
    public $tab;
    public $version;
    public $ps_versions_compliancy;
    public $author;
    public $module_key;
    public $bootstrap;
    public $displayName;
    public $description;
    public $confirmUninstall;
    public $products = [];
    public $_debug = 0;
    public function __construct()
    {
    }
    /**
     * Back office module configuration page content
     */
    public function getContent()
    {
    }
    public function hookDisplayHeader($params, $back_office = \false)
    {
    }
    /**
     * Confirmation page hook.
     * This function is run to track transactions.
     */
    public function hookDisplayOrderConfirmation($params)
    {
    }
    /**
     * Footer hook
     * This function is run to load JS script for standards actions such as product clicks
     */
    public function hookDisplayBeforeBodyClosingTag()
    {
    }
    /**
     * Product page footer hook
     * This function is run to load JS for product details view
     */
    public function hookDisplayFooterProduct()
    {
    }
    /**
     * Hook admin order.
     * This function is run to send transactions and refunds details
     */
    public function hookDisplayAdminOrder()
    {
    }
    /**
     * Admin office header hook.
     * This function is run to add Google Analytics JavaScript
     */
    public function hookDisplayBackOfficeHeader()
    {
    }
    /**
     * Product cancel action hook (in Back office).
     * This function is run to add Google Analytics JavaScript
     */
    public function hookActionProductCancel($params)
    {
    }
    /**
     * Hook used to detect backoffice orders and store their IDs into cookie.
     */
    public function hookActionValidateOrder($params)
    {
    }
    /**
     * Hook called after order status change, used to "refund" order after cancelling it
     */
    public function hookActionOrderStatusPostUpdate($params)
    {
    }
    /**
     * Hook to process add and remove items from cart events
     * This function is run to implement 'add to cart' and 'remove from cart' functionalities
     */
    public function hookActionCartUpdateQuantityBefore($params)
    {
    }
    /**
     * Hook to process remove items from cart events
     * This function is run to implement 'remove from cart' functionalities
     */
    public function hookActionObjectProductInCartDeleteBefore($params)
    {
    }
    public function hookActionCarrierProcess($params)
    {
    }
    protected function _debugLog($function, $log)
    {
    }
    /**
     * This method is triggered at the installation of the module
     * - it installs all module tables
     * - it registers the hooks used by this module
     *
     * @return bool
     */
    public function install()
    {
    }
    /**
     * Triggered at the uninstall of the module
     * - erases this module SQL tables
     *
     * @return bool
     */
    public function uninstall()
    {
    }
    /**
     * Returns instance of GoogleAnalyticsTools
     */
    public function getTools()
    {
    }
    /**
     * Returns instance of GanalyticsDataHandler
     */
    public function getDataHandler()
    {
    }
}
