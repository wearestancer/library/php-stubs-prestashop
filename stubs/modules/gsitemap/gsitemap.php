<?php

class Gsitemap extends \Module
{
    const HOOK_ADD_URLS = 'gSitemapAppendUrls';
    /**
     * @var bool
     */
    public $cron = \false;
    /**
     * @var array
     */
    protected $sql_checks = [];
    /**
     * @var array<int, string>
     */
    protected $type_array = [];
    /**
     * @var array
     */
    protected $disallow_controllers = ['addresses', 'address', 'authentication', 'cart', 'discount', 'footer', 'get-file', 'header', 'history', 'identity', 'images.inc', 'init', 'my-account', 'order', 'order-slip', 'order-detail', 'order-follow', 'order-return', 'order-confirmation', 'pagination', 'password', 'pdf-invoice', 'pdf-order-return', 'pdf-order-slip', 'product-sort', 'registration', 'search', 'statistics', 'attachment', 'guest-tracking'];
    public function __construct()
    {
    }
    /**
     * Google sitemap installation process:
     *
     * Step 1 - Pre-set Configuration option values
     * Step 2 - Install the Addon and create a database table to store sitemap files name by shop
     *
     * @return bool Installation result
     */
    public function install()
    {
    }
    /**
     * Check if the hook is present in the system or add it
     *
     * @return bool
     */
    protected function installHook()
    {
    }
    /**
     * Google sitemap uninstallation process:
     *
     * Step 1 - Remove Configuration option values from database
     * Step 2 - Remove the database containing the generated sitemap files names
     * Step 3 - Uninstallation of the Addon itself
     *
     * @return bool Uninstallation result
     */
    public function uninstall()
    {
    }
    /**
     * Delete all the generated sitemap files  and drop the addon table.
     *
     * @return bool
     */
    public function removeSitemap()
    {
    }
    public function getContent()
    {
    }
    /**
     * Delete all the generated sitemap files from the files system and the database.
     *
     * @param int $id_shop
     *
     * @return bool
     */
    public function emptySitemap($id_shop = 0)
    {
    }
    /**
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param array $new_link contain the link elements
     * @param string $lang language of link to add
     * @param int $index index of the current Google sitemap file
     * @param int $i count of elements added to sitemap main array
     * @param int $id_obj identifier of the object of the link to be added to the Gogle sitemap file
     *
     * @return bool
     */
    public function addLinkToSitemap(&$link_sitemap, $new_link, $lang, &$index, &$i, $id_obj)
    {
    }
    /**
     * Hydrate $link_sitemap with home link
     *
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param array $lang language of link to add
     * @param int $index index of the current Google sitemap file
     * @param int $i count of elements added to sitemap main array
     *
     * @return bool
     */
    protected function getHomeLink(&$link_sitemap, $lang, &$index, &$i)
    {
    }
    /**
     * Hydrate $link_sitemap with meta link
     *
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param array $lang language of link to add
     * @param int $index index of the current Google sitemap file
     * @param int $i count of elements added to sitemap main array
     * @param int $id_meta meta object identifier
     *
     * @return bool
     */
    protected function getMetaLink(&$link_sitemap, $lang, &$index, &$i, $id_meta = 0)
    {
    }
    /**
     * Hydrate $link_sitemap with products link
     *
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param array $lang language of link to add
     * @param int $index index of the current Google sitemap file
     * @param int $i count of elements added to sitemap main array
     * @param int $id_product product object identifier
     *
     * @return bool
     */
    protected function getProductLink(&$link_sitemap, $lang, &$index, &$i, $id_product = 0)
    {
    }
    /**
     * Hydrate $link_sitemap with categories link
     *
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param array $lang language of link to add
     * @param int $index index of the current Google sitemap file
     * @param int $i count of elements added to sitemap main array
     * @param int $id_category category object identifier
     *
     * @return bool
     */
    protected function getCategoryLink(&$link_sitemap, $lang, &$index, &$i, $id_category = 0)
    {
    }
    /**
     * return the link elements for the manufacturer object
     *
     * @param array $link_sitemap contain all the links for the Google Sitemap file to be generated
     * @param array $lang language of link to add
     * @param int $index index of the current Google Sitemap file
     * @param int $i count of elements added to sitemap main array
     * @param int $id_manufacturer manufacturer object identifier
     *
     * @return bool
     */
    protected function getManufacturerLink(&$link_sitemap, $lang, &$index, &$i, $id_manufacturer = 0)
    {
    }
    /**
     * return the link elements for the CMS object
     *
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param array $lang the language of link to add
     * @param int $index the index of the current Google sitemap file
     * @param int $i the count of elements added to sitemap main array
     * @param int $id_cms the CMS object identifier
     *
     * @return bool
     */
    protected function getCmsLink(&$link_sitemap, $lang, &$index, &$i, $id_cms = 0)
    {
    }
    /**
     * Returns link elements generated by modules subscribes to hook gsitemap::HOOK_ADD_URLS
     *
     * The hook expects modules to return a vector of associative arrays each of them being acceptable by
     *   the gsitemap::_addLinkToSitemap() second attribute (minus the 'type' index).
     * The 'type' index is automatically set to 'module' (not sure here, should we be safe or trust modules?).
     *
     * @param array $link_sitemap by ref. accumulator for all the links for the Google sitemap file to be generated
     * @param array $lang the language being processed
     * @param int $index the index of the current Google sitemap file
     * @param int $i the count of elements added to sitemap main array
     * @param int $num_link restart at link number #$num_link
     *
     * @return bool
     */
    protected function getModuleLink(&$link_sitemap, $lang, &$index, &$i, $num_link = 0)
    {
    }
    /**
     * Create the Google sitemap by Shop
     *
     * @param int $id_shop Shop identifier
     *
     * @return bool
     */
    public function createSitemap($id_shop = 0)
    {
    }
    /**
     * Store the generated sitemap file to the database
     *
     * @param string $sitemap the name of the generated Google sitemap file
     *
     * @return bool
     */
    protected function saveSitemapLink($sitemap)
    {
    }
    /**
     * @param array $link_sitemap contain all the links for the Google sitemap file to be generated
     * @param string $lang the language of link to add
     * @param int $index the index of the current Google sitemap file
     *
     * @return bool
     */
    protected function recursiveSitemapCreator($link_sitemap, $lang, &$index)
    {
    }
    /**
     * Returns the priority value set in the configuration parameters.
     * Falls back to 0.1 for things that don't have priority.
     *
     * @param string $page
     *
     * @return float|string|bool
     */
    protected function getPriorityPage($page)
    {
    }
    /**
     * Add a new line to the sitemap file
     *
     * @param resource $fd file system object resource
     * @param string $loc string the URL of the object page
     * @param string $priority
     * @param string $change_freq
     * @param string $last_mod the last modification date/time as a timestamp
     */
    protected function addSitemapNode($fd, $loc, $priority, $change_freq, $last_mod = \null)
    {
    }
    protected function addSitemapNodeImage($fd, $link, $title, $caption)
    {
    }
    /**
     * Create the index file for all generated sitemaps
     *
     * @return bool
     */
    protected function createIndexSitemap()
    {
    }
    protected function normalizeDirectory($directory)
    {
    }
    protected function removeControlCharacters($text)
    {
    }
}
