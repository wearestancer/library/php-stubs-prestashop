<?php

class Contactform extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /** @var string */
    const SEND_CONFIRMATION_EMAIL = 'CONTACTFORM_SEND_CONFIRMATION_EMAIL';
    /** @var string */
    const SEND_NOTIFICATION_EMAIL = 'CONTACTFORM_SEND_NOTIFICATION_EMAIL';
    /** @var string */
    const MESSAGE_PLACEHOLDER_FOR_OLDER_VERSION = '(hidden)';
    /** @var string */
    const SUBMIT_NAME = 'update-configuration';
    /** @var Contact */
    protected $contact;
    /** @var array */
    protected $customer_thread;
    public function __construct()
    {
    }
    /**
     * @return bool
     */
    public function install()
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @return string
     */
    protected function renderForm()
    {
    }
    /**
     * @return string
     */
    protected function getModuleConfigurationPageLink()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    /**
     * @return $this
     */
    protected function createNewToken()
    {
    }
    /**
     * @return array
     */
    public function getTemplateVarContact()
    {
    }
    /**
     * @return array
     *
     * @throws Exception
     */
    public function getTemplateVarOrders()
    {
    }
    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function sendMessage()
    {
    }
    /**
     * empty listener for registerGDPRConsent hook
     */
    public function hookRegisterGDPRConsent()
    {
    }
}
