<?php

namespace PrestaShop\Module\BlockWishList\Grid\Data;

class CurrentDayStatisticsGridDataFactory extends \PrestaShop\Module\BlockWishList\Grid\Data\BaseGridDataFactory implements \PrestaShop\PrestaShop\Core\Grid\Data\Factory\GridDataFactoryInterface
{
    // 1 day
    const CACHE_LIFETIME_SECONDS = 86400;
    public function getData(\PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface $searchCriteria)
    {
    }
}
