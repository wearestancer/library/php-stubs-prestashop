<?php

namespace PrestaShop\Module\BlockWishList\Grid\Definition;

class BaseStatisticsGridDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractGridDefinitionFactory
{
    protected function getId()
    {
    }
    protected function getName()
    {
    }
    protected function getColumns()
    {
    }
}
