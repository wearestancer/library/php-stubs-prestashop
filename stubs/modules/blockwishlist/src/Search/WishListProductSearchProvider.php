<?php

namespace PrestaShop\Module\BlockWishList\Search;

/**
 * Responsible of getting products for specific wishlist.
 */
class WishListProductSearchProvider implements \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchProviderInterface
{
    /**
     * @param Db $db
     * @param WishList $wishList
     */
    public function __construct(\Db $db, \WishList $wishList, \PrestaShop\PrestaShop\Core\Product\Search\SortOrdersCollection $sortOrdersCollection, \Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * @param ProductSearchContext $context
     * @param ProductSearchQuery $query
     *
     * @return ProductSearchResult
     */
    public function runQuery(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext $context, \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query)
    {
    }
}
