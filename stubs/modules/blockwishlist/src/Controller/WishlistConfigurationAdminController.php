<?php

namespace PrestaShop\Module\BlockWishList\Controller;

class WishlistConfigurationAdminController extends \PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController
{
    public function __construct(\Doctrine\Common\Cache\CacheProvider $cache, $shopId)
    {
    }
    public function configurationAction(\Symfony\Component\HttpFoundation\Request $request)
    {
    }
    public function statisticsAction()
    {
    }
    public function resetStatisticsCacheAction()
    {
    }
}
