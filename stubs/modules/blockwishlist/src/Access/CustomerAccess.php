<?php

namespace PrestaShop\Module\BlockWishList\Access;

class CustomerAccess
{
    public function __construct(\Customer $customer)
    {
    }
    /**
     * @return bool
     */
    public function hasReadAccessToWishlist(\WishList $wishlist)
    {
    }
    /**
     * @return bool
     */
    public function hasWriteAccessToWishlist(\WishList $wishlist)
    {
    }
}
