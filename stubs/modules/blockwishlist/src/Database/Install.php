<?php

namespace PrestaShop\Module\BlockWishList\Database;

class Install
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    public function run()
    {
    }
    public function installTables()
    {
    }
    public function installConfiguration()
    {
    }
    public function installTabs()
    {
    }
}
