<?php

namespace PrestaShop\Module\BlockWishList\Calculator;

class StatisticsCalculator
{
    const ARRAY_KEYS_STATS = ['allTime', 'currentYear', 'currentMonth', 'currentDay'];
    public function __construct(\PrestaShop\PrestaShop\Adapter\LegacyContext $context, \PrestaShop\PrestaShop\Core\Localization\Locale $locale)
    {
    }
    /**
     * computeStatsFor
     *
     * @param string|null $statsRange
     *
     * @return array
     */
    public function computeStatsFor($statsRange = null)
    {
    }
    /**
     * computeconversionRate
     *
     * @param array $stats
     * @param string|null $dateStart
     *
     * @return void
     */
    public function computeConversionRate(&$stats, $dateStart = null)
    {
    }
    /**
     * getProductImage
     *
     * @param array $productDetails
     *
     * @return array
     */
    public function getProductImage($productDetails)
    {
    }
    /**
     * computeConversionByProduct
     *
     * @param string $id_product
     * @param string $id_product_attribute
     * @param string $dateStart (Y-m-d H:i:s)
     *
     * @return float
     */
    public function computeConversionByProduct($id_product, $id_product_attribute, $dateStart = null)
    {
    }
}
