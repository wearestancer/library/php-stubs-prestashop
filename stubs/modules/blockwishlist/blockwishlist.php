<?php

class BlockWishList extends \Module
{
    const HOOKS = ['actionAdminControllerSetMedia', 'actionFrontControllerSetMedia', 'actionAttributeDelete', 'actionProductDelete', 'actionProductAttributeDelete', 'deleteProductAttribute', 'displayProductActions', 'displayCustomerAccount', 'displayFooter', 'displayAdminCustomers', 'displayMyAccountBlock'];
    const MODULE_ADMIN_CONTROLLERS = [['class_name' => 'WishlistConfigurationAdminParentController', 'visible' => \false, 'parent_class_name' => 'AdminParentModulesSf', 'name' => 'Wishlist Module'], ['class_name' => 'WishlistConfigurationAdminController', 'visible' => \true, 'parent_class_name' => 'WishlistConfigurationAdminParentController', 'name' => 'Configuration'], ['class_name' => 'WishlistStatisticsAdminController', 'visible' => \true, 'parent_class_name' => 'WishlistConfigurationAdminParentController', 'name' => 'Statistics']];
    public function __construct()
    {
    }
    /**
     * @return bool
     */
    public function install()
    {
    }
    /**
     * @return bool
     */
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    /**
     * Add asset for Administration
     *
     * @param array $params
     */
    public function hookActionAdminControllerSetMedia(array $params)
    {
    }
    /**
     * Add asset for Shop Front Office
     *
     * @see https://devdocs.prestashop.com/1.7/themes/getting-started/asset-management/#without-a-front-controller-module
     *
     * @param array $params
     */
    public function hookActionFrontControllerSetMedia(array $params)
    {
    }
    /**
     * This hook allow additional action button, near the add to cart button on the product page
     *
     * @param array $params
     *
     * @return string
     */
    public function hookDisplayProductActions(array $params)
    {
    }
    public function hookActionProductDelete(array $params)
    {
    }
    public function hookActionProductAttributeDelete(array $params)
    {
    }
    public function hookActionAttributeDelete(array $params)
    {
    }
    public function hookDeleteProductAttribute(array $params)
    {
    }
    /**
     * This hook displays new elements on the customer account page
     *
     * @param array $params
     *
     * @return string
     */
    public function hookDisplayCustomerAccount(array $params)
    {
    }
    /**
     * This hook displays a new block on the admin customer page
     *
     * @param array $params
     *
     * @return string
     */
    public function hookDisplayAdminCustomers(array $params)
    {
    }
    /**
     * Display additional information inside the "my account" block
     *
     * @param array $params
     *
     * @return string
     */
    public function hookDisplayMyAccountBlock(array $params)
    {
    }
    /**
     * This hook adds additional elements in the footer section of your pages
     *
     * @param array $params
     *
     * @return string
     */
    public function hookDisplayFooter(array $params)
    {
    }
}
