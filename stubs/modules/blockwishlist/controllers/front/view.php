<?php

/**
 * View the content of a personal wishlist
 */
class BlockWishlistViewModuleFrontController extends \ProductListingFrontController
{
    /**
     * Made public as the core considers this class as an ModuleFrontController
     * and other modules expects to find the $module property.
     *
     * @var BlockWishList
     */
    public $module;
    /**
     * @var WishList
     */
    protected $wishlist;
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function init()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function initContent()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getListingLabel()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getProductSearchQuery()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getDefaultProductSearchProvider()
    {
    }
    /**
     * {@inheritdoc}
     */
    protected function getAjaxProductSearchVariables()
    {
    }
    public function getBreadcrumbLinks()
    {
    }
}
