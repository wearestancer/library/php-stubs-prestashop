<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */
class WishList extends \ObjectModel
{
    /** @var int Wishlist ID */
    public $id;
    /** @var int Customer ID */
    public $id_customer;
    /** @var int Token */
    public $token;
    /** @var string Name */
    public $name;
    /** @var string Object creation date */
    public $date_add;
    /** @var string Object last modification date */
    public $date_upd;
    /** @var int Object last modification date */
    public $id_shop;
    /** @var int Object last modification date */
    public $id_shop_group;
    /** @var int default */
    public $default;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'wishlist', 'primary' => 'id_wishlist', 'fields' => ['id_customer' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => \true], 'token' => ['type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => \true], 'name' => ['type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => \true], 'date_add' => ['type' => self::TYPE_DATE, 'validate' => 'isDate'], 'date_upd' => ['type' => self::TYPE_DATE, 'validate' => 'isDate'], 'id_shop' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'], 'id_shop_group' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'], 'default' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool']]];
    /**
     * Get Customers having a wishlist
     *
     * @return array Results
     */
    public static function getCustomers()
    {
    }
    /**
     * Return true if wishlist exists else false
     *
     * @param int $id_wishlist
     * @param int $id_customer
     *
     * @return bool exists
     */
    public static function exists($id_wishlist, $id_customer)
    {
    }
    /**
     * Set current WishList as default
     *
     * @return bool
     */
    public function setDefault()
    {
    }
    /**
     * Return if there is a default already set
     *
     * @param int $id_customer
     *
     * @return bool
     */
    public static function isDefault($id_customer)
    {
    }
    /**
     * @param int $id_customer
     *
     * @return int
     */
    public static function getDefault($id_customer)
    {
    }
    /**
     * Add product to ID wishlist
     *
     * @param int $id_wishlist
     * @param int $id_customer
     * @param int $id_product
     * @param int $id_product_attribute
     * @param int $quantity
     *
     * @return bool succeed
     */
    public static function addProduct($id_wishlist, $id_customer, $id_product, $id_product_attribute, $quantity)
    {
    }
    /**
     * Remove product from wishlist
     *
     * @param int $id_wishlist
     * @param int $id_customer
     * @param int $id_product
     * @param int $id_product_attribute
     *
     * @return bool
     */
    public static function removeProduct($id_wishlist, $id_customer, $id_product, $id_product_attribute)
    {
    }
    /**
     * @param int|null $id_product
     * @param int|null $id_product_attribute
     *
     * @return bool
     */
    public static function removeProductFromWishlist($id_product = \null, $id_product_attribute = \null)
    {
    }
    /**
     * @return void
     */
    public static function removeNonExistingProductAttributesFromWishlist()
    {
    }
    /**
     * Update product to wishlist
     *
     * @param int $id_wishlist
     * @param int $id_product
     * @param int $id_product_attribute
     * @param int $priority
     * @param int $quantity
     *
     * @return bool succeed
     */
    public static function updateProduct($id_wishlist, $id_product, $id_product_attribute, $priority, $quantity)
    {
    }
    /**
     * Get all Wishlists by Customer ID
     *
     * @param int $id_customer
     *
     * @return array Results
     */
    public static function getAllWishlistsByIdCustomer($id_customer)
    {
    }
    /**
     * Get products by Wishlist
     *
     * @param int $id_wishlist
     *
     * @return array|false Results
     */
    public static function getProductsByWishlist($id_wishlist)
    {
    }
    /**
     * Get Wishlist products by Customer ID
     *
     * @param int $id_wishlist
     * @param int $id_customer
     * @param int $id_lang
     * @param int|null $id_product
     * @param bool $quantity
     *
     * @return array Results
     */
    public static function getProductByIdCustomer($id_wishlist, $id_customer, $id_lang, $id_product = \null, $quantity = \false)
    {
    }
    /**
     * Add bought product
     *
     * @param int $id_wishlist
     * @param int $id_product
     * @param int $id_product_attribute
     * @param int $id_cart
     * @param int $quantity
     *
     * @return bool succeed
     */
    public static function addBoughtProduct($id_wishlist, $id_product, $id_product_attribute, $id_cart, $quantity)
    {
    }
    /**
     * @param int $id_customer
     * @param int $idShop
     *
     * @return array|false
     */
    public static function getAllProductByCustomer($id_customer, $idShop)
    {
    }
    /**
     * Get ID wishlist by Token
     *
     * @param string $token
     *
     * @return array Results
     *
     * @throws PrestaShopException
     */
    public static function getByToken($token)
    {
    }
    public static function refreshWishList($id_wishlist)
    {
    }
    /**
     * Increment counter
     *
     * @param int $id_wishlist
     *
     * @return bool succeed
     */
    public static function incCounter($id_wishlist)
    {
    }
    /**
     * @param int $id_wishlist
     *
     * @return int
     */
    public static function getWishlistCounter($id_wishlist)
    {
    }
    /**
     * Get Wishlists by Customer ID
     *
     * @param int $id_customer
     *
     * @return array Results
     */
    public static function getByIdCustomer($id_customer)
    {
    }
}
