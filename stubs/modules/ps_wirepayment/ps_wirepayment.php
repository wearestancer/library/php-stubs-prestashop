<?php

class Ps_Wirepayment extends \PaymentModule
{
    const FLAG_DISPLAY_PAYMENT_INVITE = 'BANK_WIRE_PAYMENT_INVITE';
    protected $_html = '';
    protected $_postErrors = [];
    public $details;
    public $owner;
    public $address;
    public $extra_mail_vars;
    /**
     * @var int
     */
    public $is_eu_compatible;
    /**
     * @var false|int
     */
    public $reservation_days;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    protected function _postValidation()
    {
    }
    protected function _postProcess()
    {
    }
    protected function _displayBankWire()
    {
    }
    public function getContent()
    {
    }
    public function hookPaymentOptions($params)
    {
    }
    public function hookDisplayPaymentReturn($params)
    {
    }
    public function checkCurrency($cart)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getTemplateVarInfos()
    {
    }
}
