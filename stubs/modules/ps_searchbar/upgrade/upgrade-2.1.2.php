<?php

// In the latest version PrestaShop 1.7.8 the override for this module in classic theme was removed
// because the module is now self-sufficient, so we disable it from the theme to make sure the correct
// internal template is used (we only clean the theme from core classic theme, and only if it was not
// updated). Since this version of the module is only compatible starting PS 1.7.8 this clean can always
// be performed regardless of the current PrestaShop version.
// We don't delete the file but rather rename it so that the merchant can perform a rollback in case important
// changes were present in the file
function upgrade_module_2_1_2($module)
{
}
