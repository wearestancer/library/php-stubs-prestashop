<?php

class Ps_Categoryproducts extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    protected $html;
    protected $templateFile;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    public function hookAddProduct($params)
    {
    }
    public function hookUpdateProduct($params)
    {
    }
    public function hookDeleteProduct($params)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
}
