<?php

class dashproducts extends \Module
{
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function hookDashboardZoneTwo($params)
    {
    }
    public function hookDashboardData($params)
    {
    }
    public function getTableRecentOrders()
    {
    }
    public function getTableBestSellers($date_from, $date_to)
    {
    }
    public function getTableMostViewed($date_from, $date_to)
    {
    }
    public function getTableTop10MostSearch($date_from, $date_to)
    {
    }
    public function getTotalProductSales($date_from, $date_to, $id_product)
    {
    }
    public function getTotalProductAddedCart($date_from, $date_to, $id_product)
    {
    }
    public function getTotalProductPurchased($date_from, $date_to, $id_product)
    {
    }
    public function getTotalViewed($date_from, $date_to, $limit = 10)
    {
    }
    public function getMostSearchTerms($date_from, $date_to, $limit = 10)
    {
    }
    public function renderConfigForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    /**
     * Validate dashboard configuration
     *
     * @param array $config
     *
     * @return array
     */
    public function validateDashConfig(array $config)
    {
    }
    /**
     * Save dashboard configuration
     *
     * @param array $config
     *
     * @return bool determines if there are errors or not
     */
    public function saveDashConfig(array $config)
    {
    }
}
