<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */
class ReassuranceActivity extends \ObjectModel
{
    const TYPE_LINK_NONE = 0;
    const TYPE_LINK_CMS_PAGE = 1;
    const TYPE_LINK_URL = 2;
    public $id;
    public $icon;
    public $custom_icon;
    public $title;
    public $description;
    public $status;
    public $position;
    public $type_link;
    public $link;
    public $id_cms;
    public $date_add;
    public $date_upd;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'psreassurance', 'primary' => 'id_psreassurance', 'multilang' => \true, 'fields' => [
        'icon' => ['type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 255],
        'custom_icon' => ['type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 255],
        'status' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => \true],
        'position' => ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => \false],
        'type_link' => ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => \false],
        'id_cms' => ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => \false],
        'date_add' => ['type' => self::TYPE_DATE, 'validate' => 'isDate'],
        'date_upd' => ['type' => self::TYPE_DATE, 'validate' => 'isDate'],
        // lang fields
        'title' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isCleanHtml', 'size' => 255],
        'description' => ['type' => self::TYPE_HTML, 'lang' => \true, 'validate' => 'isCleanHtml', 'size' => 2000],
        'link' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isUrl', 'required' => \false, 'size' => 255],
    ]];
    /**
     * @param array $psr_languages
     * @param int $type_link
     * @param int $id_cms
     */
    public function handleBlockValues($psr_languages, $type_link, $id_cms)
    {
    }
    /**
     * @return array
     *
     * @throws PrestaShopDatabaseException
     */
    public static function getAllBlock()
    {
    }
    /**
     * @param int $id_lang
     *
     * @return array
     *
     * @throws PrestaShopDatabaseException
     */
    public static function getAllBlockByStatus($id_lang = 1)
    {
    }
    /**
     * @return string|bool
     */
    public static function getMimeType(string $filename)
    {
    }
}
