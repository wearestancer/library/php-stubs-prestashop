<?php

class blockreassurance extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    const ALLOWED_CONTROLLERS_CHECKOUT = ['cart', 'order'];
    const ALLOWED_CONTROLLERS_PRODUCT = ['product'];
    const POSITION_NONE = 0;
    const POSITION_BELOW_HEADER = 1;
    const POSITION_ABOVE_HEADER = 2;
    const PSR_HOOK_HEADER = 'PSR_HOOK_HEADER';
    const PSR_HOOK_FOOTER = 'PSR_HOOK_FOOTER';
    const PSR_HOOK_PRODUCT = 'PSR_HOOK_PRODUCT';
    const PSR_HOOK_CHECKOUT = 'PSR_HOOK_CHECKOUT';
    /** @var string */
    public $name;
    /** @var string */
    public $version;
    /** @var string */
    public $author;
    /** @var bool */
    public $need_instance;
    /** @var string */
    public $controller_name;
    /** @var bool */
    public $bootstrap;
    /** @var string */
    public $displayName;
    /** @var string */
    public $description;
    /** @var string */
    public $js_path;
    /** @var string */
    public $css_path;
    /** @var string */
    public $img_path;
    /** @var string */
    public $old_path_img;
    /** @var string */
    public $img_path_perso;
    /** @var string */
    public $lib_path;
    /** @var string */
    public $docs_path;
    /** @var string */
    public $logo_path;
    /** @var string */
    public $module_path;
    /** @var string Text to display when ask for confirmation on uninstall action */
    public $confirmUninstall;
    /** @var string */
    public $ps_url;
    /** @var string */
    public $folder_file_upload;
    public function __construct()
    {
    }
    /**
     * install pre-config
     *
     * @return bool
     */
    public function install()
    {
    }
    /**
     * Uninstall module configuration
     *
     * @return bool
     */
    public function uninstall()
    {
    }
    /**
     * load dependencies
     */
    public function loadAsset()
    {
    }
    /**
     * @return string
     *
     * @throws PrestaShopException
     */
    public function getContent()
    {
    }
    /**
     * @param array $params
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     */
    public function hookdisplayAfterBodyOpeningTag($params)
    {
    }
    /**
     * @param array $params
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     */
    public function hookdisplayNavFullWidth($params)
    {
    }
    /**
     * @param array $params
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     */
    public function hookdisplayFooterAfter($params)
    {
    }
    /**
     * @param array $params
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     */
    public function hookdisplayFooterBefore($params)
    {
    }
    /**
     * @param array $params
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     */
    public function hookdisplayReassurance($params)
    {
    }
    public function hookActionFrontControllerSetMedia()
    {
    }
    /**
     * @param string $hookName
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     */
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    /**
     * @param string $hookName
     *
     * @return array
     *
     * @throws PrestaShopDatabaseException
     */
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    /**
     * @throws PrestaShopException
     */
    protected function addJsDefList()
    {
    }
}
