<?php

class Ps_Socialfollow extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    const SOCIAL_NETWORKS = ['FACEBOOK', 'TWITTER', 'RSS', 'YOUTUBE', 'PINTEREST', 'VIMEO', 'INSTAGRAM', 'LINKEDIN', 'TIKTOK', 'DISCORD'];
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    public function _clearCache($template, $cache_id = \null, $compile_id = \null)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    /**
     * This upgrades the configuration of the module from simple values to localized values. This assures that the
     * upgrade of the module keeps the old configurations, and that the change is transparent to the user.
     *
     * This function is only run once during upgrade, i.e. the first time the user accesses the configuration in the BO
     * after an upgrade of the module to the localized version.
     *
     * @param string $name Name of the configuration setting
     *
     * @return array Configuration value, now localized
     */
    protected function upgradeConfiguration($name)
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    public function hookActionFrontControllerSetMedia()
    {
    }
    /**
     * Update form fields.
     * Check all social networks form value and verify the URL is valid.
     * Do nothing if a violation is spotted.
     *
     * @return array|bool true on success, errors on failure
     */
    protected function updateFields()
    {
    }
}
