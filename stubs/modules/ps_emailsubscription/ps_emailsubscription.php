<?php

class Ps_Emailsubscription extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blocknewsletter';
    const GUEST_NOT_REGISTERED = -1;
    const CUSTOMER_NOT_REGISTERED = 0;
    const GUEST_REGISTERED = 1;
    const CUSTOMER_REGISTERED = 2;
    const NEWSLETTER_SUBSCRIPTION = 0;
    const NEWSLETTER_UNSUBSCRIPTION = 1;
    const LEGAL_PRIVACY = 'LEGAL_PRIVACY';
    protected $_origin_newsletter;
    const TPL_COLUMN = 'ps_emailsubscription-column.tpl';
    const TPL_DEFAULT = 'ps_emailsubscription.tpl';
    /**
     * @var bool|string
     */
    public $error;
    /**
     * @var bool|string
     */
    public $valid;
    /**
     * @var array<string, array<int, string>>
     */
    public $_files;
    /**
     * @var string|null
     */
    public $_searched_email;
    /**
     * @var string
     */
    public $_html;
    /**
     * @var string
     */
    public $file;
    /**
     * @var array
     */
    public $post_errors;
    /**
     * @var HelperList
     */
    public $_helperlist;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function getContent()
    {
    }
    public function renderList()
    {
    }
    public function displayViewCustomerLink($token = \null, $id = \null, $name = \null)
    {
    }
    public function displayEnableLink($token, $id, $value, $active, $id_category = \null, $id_product = \null, $ajax = \false)
    {
    }
    public function displayUnsubscribeLink($token = \null, $id = \null, $name = \null)
    {
    }
    /**
     * Check if this mail is registered for newsletters.
     *
     * @param string $customer_email
     *
     * @return int -1 = not a customer and not registered
     *             0 = customer not registered
     *             1 = registered in block
     *             2 = registered in customer
     */
    public function isNewsletterRegistered($customer_email)
    {
    }
    /**
     * Register in email subscription.
     *
     * @param string|null $hookName For widgets displayed by a hook, hook name must be passed
     *                              as multiple hooks might be used, so it is necessary to know which one was used for
     *                              submitting the form
     *
     * @return bool|string
     */
    public function newsletterRegistration($hookName = \null)
    {
    }
    public function getSubscribers()
    {
    }
    public function paginateSubscribers($subscribers, $page = 1, $pagination = 50)
    {
    }
    /**
     * Return true if the registered status correspond to a registered user.
     *
     * @param int $register_status
     *
     * @return bool
     */
    protected function isRegistered($register_status)
    {
    }
    /**
     * Subscribe an email to the newsletter. It will create an entry in the newsletter table
     * or update the customer table depending of the register status.
     *
     * @param string $email
     * @param int $register_status
     */
    protected function register($email, $register_status)
    {
    }
    protected function unregister($email, $register_status)
    {
    }
    /**
     * Subscribe a customer to the newsletter.
     *
     * @param string $email
     *
     * @return bool
     */
    protected function registerUser($email)
    {
    }
    /**
     * Subscribe a guest to the newsletter.
     *
     * @param string $email
     * @param bool $active
     *
     * @return bool
     */
    protected function registerGuest($email, $active = \true)
    {
    }
    public function activateGuest($email)
    {
    }
    /**
     * Returns a guest email by token.
     *
     * @param string $token
     *
     * @return string email
     */
    protected function getGuestEmailByToken($token)
    {
    }
    /**
     * Returns a customer email by token.
     *
     * @param string $token
     *
     * @return string email
     */
    protected function getUserEmailByToken($token)
    {
    }
    /**
     * Return a token associated to an user.
     *
     * @param string $email
     * @param int $register_status
     */
    protected function getToken($email, $register_status)
    {
    }
    /**
     * Ends the registration process to the newsletter.
     *
     * @param string $token
     *
     * @return string
     */
    public function confirmEmail($token)
    {
    }
    /**
     * Send the confirmation mails to the given $email address if needed.
     *
     * @param string $email Email where to send the confirmation
     *
     * @note the email has been verified and might not yet been registered. Called by AuthController::processCustomerNewsletter
     */
    public function confirmSubscription($email)
    {
    }
    /**
     * Send an email containing a voucher code.
     *
     * @param string $email
     * @param string $code
     *
     * @return bool|int
     */
    protected function sendVoucher($email, $code)
    {
    }
    /**
     * Send a confirmation email.
     *
     * @param string $email
     *
     * @return bool
     */
    protected function sendConfirmationEmail($email)
    {
    }
    /**
     * Send a verification email.
     *
     * @param string $email
     * @param string $token
     *
     * @return bool
     */
    protected function sendVerificationEmail($email, $token)
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    public function hookActionFrontControllerSetMedia()
    {
    }
    /**
     * Deletes duplicates email in newsletter table.
     *
     * @param array $params
     *
     * @return bool
     */
    public function hookActionCustomerAccountAdd($params)
    {
    }
    public function hookActionObjectCustomerUpdateBefore($params)
    {
    }
    public function hookActionCustomerAccountUpdate($params)
    {
    }
    /**
     * Add an extra FormField to ask for newsletter registration.
     *
     * @param array $params
     *
     * @return array<FormField>
     */
    public function hookAdditionalCustomerFormFields($params)
    {
    }
    public function renderForm()
    {
    }
    public function renderExportForm()
    {
    }
    public function renderSearchForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function export_csv()
    {
    }
    /**
     * This hook allow you to add new fields in the admin customer form
     *
     * @return string
     */
    public function hookDisplayAdminCustomersForm()
    {
    }
    public function hookActionDeleteGDPRCustomer($customer)
    {
    }
    public function hookActionExportGDPRData($customer)
    {
    }
}
