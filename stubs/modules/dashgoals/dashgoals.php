<?php

class dashgoals extends \Module
{
    protected static $month_labels = [];
    protected static $types = ['traffic', 'conversion', 'avg_cart_value'];
    protected static $real_color = ['#9E5BA1', '#00A89C', '#3AC4ED', '#F99031'];
    protected static $more_color = ['#803E84', '#008E7E', '#20B2E7', '#F66E1B'];
    protected static $less_color = ['#BC77BE', '#00C2BB', '#51D6F2', '#FBB244'];
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function hookActionAdminControllerSetMedia()
    {
    }
    public function setMonths($year)
    {
    }
    public function hookDashboardZoneTwo($params)
    {
    }
    public function hookDashboardData($params)
    {
    }
    protected function fakeConfigurationKPI_get($key)
    {
    }
    public function getChartData($year)
    {
    }
    protected function getValuesFromGoals($average_goal, $month_goal, $value, $label)
    {
    }
}
