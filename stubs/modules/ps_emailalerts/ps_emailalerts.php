<?php

class Ps_EmailAlerts extends \Module
{
    /** @var string Page name */
    public $page_name;
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'mailalerts';
    protected $html = '';
    protected $merchant_new_order_emails;
    protected $merchant_oos_emails;
    protected $merchant_return_slip_emails;
    protected $merchant_order;
    protected $merchant_oos;
    protected $customer_qty;
    protected $merchant_coverage;
    protected $product_coverage;
    protected $order_edited;
    protected $return_slip;
    const __MA_MAIL_DELIMITER__ = ',';
    public function __construct()
    {
    }
    protected function init()
    {
    }
    public function install($delete_params = \true)
    {
    }
    public function uninstall($delete_params = \true)
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function reset()
    {
    }
    public function getContent()
    {
    }
    protected function postProcess()
    {
    }
    public function getAllMessages($id)
    {
    }
    /**
     * Return current locale
     *
     * @param Context $context
     *
     * @return \PrestaShop\PrestaShop\Core\Localization\Locale|null
     *
     * @throws Exception
     */
    public static function getContextLocale(\Context $context)
    {
    }
    public function hookActionValidateOrder($params)
    {
    }
    public function hookDisplayProductAdditionalInfo($params)
    {
    }
    public function hookActionUpdateQuantity($params)
    {
    }
    public function hookActionProductAttributeUpdate($params)
    {
    }
    public function hookDisplayCustomerAccount($params)
    {
    }
    public function hookDisplayMyAccountBlock($params)
    {
    }
    public function hookActionProductDelete($params)
    {
    }
    public function hookActionProductAttributeDelete($params)
    {
    }
    public function hookActionProductCoverage($params)
    {
    }
    public function hookActionFrontControllerSetMedia()
    {
    }
    public function hookActionAdminControllerSetMedia()
    {
    }
    /**
     * Send a mail when a customer return an order.
     *
     * @param array $params Hook params
     */
    public function hookActionOrderReturn($params)
    {
    }
    /**
     * Send a mail when an order is modified.
     *
     * @param array $params Hook params
     */
    public function hookActionOrderEdited($params)
    {
    }
    public function renderForm()
    {
    }
    public function hookActionDeleteGDPRCustomer($customer)
    {
    }
    public function hookActionExportGDPRData($customer)
    {
    }
    public function getConfigFieldsValues()
    {
    }
    /**
     * empty listener for registerGDPRConsent hook
     */
    public function hookRegisterGDPRConsent()
    {
    }
    public function isUsingNewTranslationSystem()
    {
    }
}
