<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */
/**
 * @since 1.5.0
 */
class Ps_EmailAlertsActionsModuleFrontController extends \ModuleFrontController
{
    /**
     * @var int
     */
    public $id_product;
    public $id_product_attribute;
    public function init()
    {
    }
    public function postProcess()
    {
    }
    /**
     * Remove product alert.
     * Prints 0 if success
     */
    public function processRemove()
    {
    }
    /**
     * Add a favorite product.
     */
    public function processAdd()
    {
    }
    /**
     * Add a favorite product.
     */
    public function processCheck()
    {
    }
}
