<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */
class MailAlert extends \ObjectModel
{
    public $id_customer;
    public $customer_email;
    public $id_product;
    public $id_product_attribute;
    public $id_shop;
    public $id_lang;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'mailalert_customer_oos', 'primary' => 'id_customer', 'fields' => ['id_customer' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => \true], 'customer_email' => ['type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => \true], 'id_product' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => \true], 'id_product_attribute' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => \true], 'id_shop' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => \true], 'id_lang' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => \true]]];
    public static function customerHasNotification($id_customer, $id_product, $id_product_attribute, $id_shop = \null, $id_lang = \null, $guest_email = '')
    {
    }
    public static function deleteAlert($id_customer, $customer_email, $id_product, $id_product_attribute, $id_shop = \null)
    {
    }
    /*
     * Get objects that will be viewed on "My alerts" page
     */
    public static function getMailAlerts($id_customer, $id_lang, \Shop $shop = \null)
    {
    }
    public static function sendCustomerAlert($id_product, $id_product_attribute)
    {
    }
    /*
     * Generate correctly the address for an email
     */
    public static function getFormatedAddress(\Address $address, $line_sep, $fields_style = [])
    {
    }
    /*
     * Get products according to alerts
     */
    public static function getProducts($customer, $id_lang)
    {
    }
    /*
     * Get product combinations
     */
    public static function getProductAttributeCombination($id_product_attribute, $id_lang)
    {
    }
    /*
     * Get customers waiting for alert on the specified product/product attribute
     * in shop `$id_shop` and if the shop group shares the stock in all shops of the shop group
     */
    public static function getCustomers($id_product, $id_product_attribute, $id_shop)
    {
    }
}
