<?php

class Ps_Customtext extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    // Equivalent module on PrestaShop 1.6, sharing the same data
    const MODULE_16 = 'blockcmsinfo';
    public function __construct()
    {
    }
    /**
     * @return bool
     */
    public function install()
    {
    }
    /**
     * @return bool
     */
    public function runInstallSteps()
    {
    }
    /**
     * @return bool
     */
    public function installFrom16Version()
    {
    }
    /**
     * @return bool
     */
    public function uninstall()
    {
    }
    /**
     * @return bool
     */
    public function installDB()
    {
    }
    /**
     * @param bool $drop_table
     *
     * @return bool
     */
    public function uninstallDB($drop_table = \true)
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @return bool
     */
    public function processSaveCustomText()
    {
    }
    /**
     * @return string
     */
    protected function renderForm()
    {
    }
    /**
     * @return array<string, mixed>
     */
    public function getFormValues()
    {
    }
    /**
     * @param string|null $hookName
     * @param array $configuration
     *
     * @return string|false
     */
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    /**
     * @param string|null $hookName
     * @param array $configuration
     *
     * @return array<string, mixed>|false
     */
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    /**
     * @return bool
     */
    public function installFixtures()
    {
    }
    /**
     * Add CustomText when adding a new Shop
     *
     * @param array{cookie: Cookie, cart: Cart, altern: int, old_id_shop: int, new_id_shop: int} $params
     */
    public function hookActionShopDataDuplication(array $params)
    {
    }
}
