<?php

class AdminPsThemeCustoConfigurationController extends \ModuleAdminController
{
    public $isPsVersion174Plus;
    public $controller_quick_name;
    public $aModuleActions;
    public $moduleActionsNames;
    public $categoryList;
    public function __construct()
    {
    }
    /**
     * Get homepage list of modules to show
     *
     * @return array
     */
    public function getHomepageListConfiguration()
    {
    }
    /**
     * Get category list of modules to show
     *
     * @return array
     */
    public function getCategoryListConfiguration()
    {
    }
    /**
     * Get product list of modules to show
     *
     * @return array
     */
    public function getProductListConfiguration()
    {
    }
    /**
     * Initialize the content by adding Boostrap and loading the TPL
     */
    public function initContent()
    {
    }
    /**
     * AJAX : Do a module action like Install, disable, enable ...
     *
     * @return mixed
     */
    public function ajaxProcessUpdateModule()
    {
    }
    /**
     * get list to show
     *
     * @param array $aList
     *
     * @return array
     */
    public function setFinalList($aList)
    {
    }
    /**
     * Render final list of modules
     *
     * @param Module $oModuleInstance
     * @param bool $bIsInstalled
     *
     * @return array
     */
    public function setModuleFinalList($oModuleInstance, $bIsInstalled)
    {
    }
    /**
     * Order Final array for having installed module first
     *
     * @param array $a
     * @param array $b
     *
     * @return int
     */
    public function sortArrayInstalledModulesFirst($a, $b)
    {
    }
}
