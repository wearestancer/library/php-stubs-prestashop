<?php

class AdminPsThemeCustoAdvancedController extends \ModuleAdminController
{
    /**
     * @var string
     */
    public $skeleton_name;
    /**
     * @var string
     */
    public $childtheme_skeleton;
    /**
     * @var string
     */
    public $sandbox_path;
    /**
     * @var string
     */
    public $controller_quick_name;
    /**
     * @var ThemeManager
     */
    public $theme_manager;
    /**
     * @var ThemeRepository
     */
    public $theme_repository;
    public function __construct()
    {
    }
    /**
     * Initialize the content by adding Boostrap and loading the TPL
     */
    public function initContent()
    {
    }
    /**
     * Clone a theme and modify the config to set the parent theme
     *
     * @return bool
     */
    public function ajaxProcessDownloadChildTheme()
    {
    }
    /**
     * AJAX getting a file attachment and will upload the file, install it, check if there's modules in it ...
     *
     * @return string
     */
    public function ajaxProcessUploadChildTheme()
    {
    }
    /**
     * Check upload file on upload
     *
     * @param array $aChildThemeReturned
     * @param string $dest
     *
     * @return string|bool
     */
    public function processUploadFileChild($aChildThemeReturned, $dest)
    {
    }
    /**
     * Check zip file and modify it if necessary
     *
     * @param array $aChildThemeReturned
     * @param string $sZipPath
     *
     * @return bool
     */
    public function checkZipFile($aChildThemeReturned, $sZipPath)
    {
    }
    /**
     * Get the mime type of the file
     *
     * @param string $tmp_name
     *
     * @return string
     */
    public function processCheckMimeType($tmp_name)
    {
    }
    /**
     * We check if the Zip is valid. The root folder must have all the theme element, we check it with the folder Config.
     *
     * @param string $sZipPath
     *
     * @return bool $bZipIsValid
     */
    public function processCheckZipFormat($sZipPath)
    {
    }
    /**
     * We unzip the child theme zip in a sandbox to check it
     *
     * @param string $sZipSource
     * @param string $sSandboxPath
     *
     * @return string|bool
     */
    public function processCheckFiles($sZipSource, $sSandboxPath)
    {
    }
    /**
     * We install the child theme and we return the folder child theme's name
     *
     * @param string $dest
     *
     * @return string|bool
     */
    public function postProcessInstall($dest)
    {
    }
    /**
     * We check in theme.yml if this theme is a child theme of the current main theme.
     *
     * @param string $sFolderPath
     *
     * @return bool
     */
    public function checkIfIsChildTheme($sFolderPath)
    {
    }
    /**
     * the child theme has modules. We can't keep it.
     *
     * @param string $sFolderPath
     *
     * @return bool
     */
    public function recursiveDelete($sFolderPath)
    {
    }
}
