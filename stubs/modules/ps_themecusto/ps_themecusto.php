<?php

class ps_themecusto extends \Module
{
    public $author_address;
    public $bootstrap;
    public $controller_name;
    public $front_controller = [];
    public $template_dir;
    public $js_path;
    public $css_path;
    public $img_path;
    public $logo_path;
    public $module_path;
    public $ps_uri;
    public function __construct()
    {
    }
    /**
     * @return bool
     */
    public function install()
    {
    }
    /**
     * @return bool
     */
    public function uninstall()
    {
    }
    /**
     * Assign all sub menu on Admin tab variable
     */
    public function assignTabList()
    {
    }
    /**
     * Get all tab names by lang ISO
     */
    public function getTabNameByLangISO()
    {
    }
    /**
     * Install all admin tab
     *
     * @return bool
     */
    public function installTabList()
    {
    }
    /**
     * uninstall tab
     *
     * @return bool
     */
    public function uninstallTabList()
    {
    }
    /**
     * set JS and CSS media
     */
    public function setMedia($aJsDef, $aJs, $aCss)
    {
    }
    /**
     * check if the employee has the right to use this admin controller
     *
     * @return bool
     */
    public function hasEditRight()
    {
    }
}
