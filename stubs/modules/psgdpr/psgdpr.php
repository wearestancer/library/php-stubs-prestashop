<?php

class Psgdpr extends \Module
{
    public $adminControllers = ['adminAjax' => 'AdminAjaxPsgdpr', 'adminDownloadInvoices' => 'AdminDownloadInvoicesPsgdpr'];
    public function __construct()
    {
    }
    /**
     * install()
     *
     * @return bool
     *
     * @throws PrestaShopException
     */
    public function install()
    {
    }
    /**
     * uninstall()
     *
     * @return bool
     */
    public function uninstall()
    {
    }
    /**
     * This method is often use to create an ajax controller
     *
     * @return bool
     */
    public function installTab()
    {
    }
    /**
     * uninstall tab
     *
     * @return bool
     */
    public function uninstallTab()
    {
    }
    /**
     * load dependencies in the configuration of the module
     */
    public function loadAsset()
    {
    }
    /**
     * @return array
     */
    public function loadFaq()
    {
    }
    /**
     * Load the configuration form
     *
     * @return string
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function getContent()
    {
    }
    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function postProcess()
    {
    }
    /**
     * save data consent tab
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function submitDataConsent()
    {
    }
    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionAdminControllerSetMedia()
    {
    }
    /**
     * @return array|FormField[]
     */
    public function hookAdditionalCustomerFormFields()
    {
    }
    /**
     * @param array $params
     */
    public function hookActionCustomerAccountAdd(array $params)
    {
    }
    /**
     * load all the registered modules and add the displayname and logopath in each module
     *
     * @return array who contains id_module, message, displayName, logoPath
     */
    public function loadRegisteredModules()
    {
    }
    /**
     * @return string
     */
    public function hookDisplayCustomerAccount()
    {
    }
    /**
     * Allow to return the checkbox to display in modules
     *
     * @param array $params
     *
     * @return string html content to display
     */
    public function hookDisplayGDPRConsent($params)
    {
    }
    /**
     * Get a module list of module trying to register to GDPR
     */
    public function getRegisteredModules()
    {
    }
    /**
     * register the module in database
     *
     * @param array $module module to register in database
     */
    public function addModuleConsent($module)
    {
    }
    /**
     * @param string $delete
     * @param mixed $value
     *
     * @return array
     */
    public function getCustomerData($delete, $value)
    {
    }
    /**
     * @param Customer $customer
     *
     * @return array
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getCustomerDataFromPrestashop(\Customer $customer)
    {
    }
    /**
     * @param mixed $customer
     *
     * @return array
     *
     * @throws PrestaShopException
     */
    public function getCustomerDataFromModules($customer)
    {
    }
    /**
     * @param string $delete
     * @param mixed $value
     */
    public function deleteCustomer($delete, $value)
    {
    }
    /**
     * @param Customer $customer
     *
     * @return bool
     */
    public function deleteDataFromPrestashop(\Customer $customer)
    {
    }
    /**
     * @param mixed $customer
     *
     * @throws PrestaShopException
     */
    public function deleteDataFromModules($customer)
    {
    }
    /**
     * @return bool
     *
     * @throws PrestaShopException
     */
    public function createAnonymousCustomer()
    {
    }
    /**
     * Return customer name for the given id.
     *
     * @param int $id_customer
     *
     * @return string Customer lastname + firstname
     */
    public function getCustomerNameById($id_customer)
    {
    }
    /**
     * Return the age of the customer
     *
     * @param int $id_customer
     *
     * @return int customer age
     */
    public function getAgeCustomer($id_customer)
    {
    }
}
