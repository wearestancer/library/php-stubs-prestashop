<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */
// require _PS_MODULE_DIR_.'psgdpr/psgdpr.php';
class HTMLTemplatePSGDPRModule extends \HTMLTemplate
{
    /**
     * @var array
     */
    public $personalData;
    /**
     * @var bool
     */
    public $available_in_your_account = \false;
    /**
     * @var Context
     */
    public $context;
    /**
     * @param array $personalData
     * @param Smarty $smarty
     *
     * @throws PrestaShopException
     */
    public function __construct($personalData, \Smarty $smarty)
    {
    }
    /**
     * Returns the template's HTML footer
     *
     * @return string HTML footer
     *
     * @throws SmartyException
     */
    public function getFooter()
    {
    }
    /**
     * Returns the template's HTML content
     *
     * @return string HTML content
     *
     * @throws SmartyException
     */
    public function getContent()
    {
    }
    /**
     * Returns the template filename
     *
     * @return string filename
     */
    public function getFilename()
    {
    }
    /**
     * Returns the template filename
     *
     * @return string filename
     */
    public function getBulkFilename()
    {
    }
    /**
     * If the template is not present in the theme directory, it will return the default template
     * in _PS_PDF_DIR_ directory
     *
     * @param string $template_name
     *
     * @return string
     */
    protected function getGDPRTemplate($template_name)
    {
    }
}
