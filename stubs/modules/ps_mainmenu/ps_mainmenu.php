<?php

class Ps_MainMenu extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blocktopmenu';
    const MENU_JSON_CACHE_KEY = 'MOD_BLOCKTOPMENU_MENU_JSON';
    protected $_menu = '';
    protected $_html = '';
    protected $user_groups;
    /*
     * Pattern for matching config values
     */
    protected $pattern = '/^([A-Z_]*)[0-9]+/';
    /*
     * Name of the controller
     * Used to set item selected or not in top menu
     */
    protected $page_name = '';
    /*
     * Spaces per depth in BO
     */
    protected $spacer_size = '5';
    public function __construct()
    {
    }
    public function install($delete_params = \true)
    {
    }
    public function installDb()
    {
    }
    public function uninstall($delete_params = \true)
    {
    }
    protected function uninstallDb()
    {
    }
    public function reset()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function getContent()
    {
    }
    protected function getWarningMultishopHtml()
    {
    }
    protected function getCurrentShopInfoMsg()
    {
    }
    protected function getMenuItems()
    {
    }
    protected function makeMenuOption()
    {
    }
    protected function makeNode(array $fields)
    {
    }
    protected function generateCMSCategoriesMenu($id_cms_category, $id_lang)
    {
    }
    protected function makeMenu()
    {
    }
    protected function mapTree(callable $cb, array $node, $depth = 0)
    {
    }
    protected function generateCategoriesOption($categories, $items_to_skip = \null)
    {
    }
    protected function generateCategoriesMenu($categories, $is_children = 0)
    {
    }
    protected function getCMSOptions($parent = 0, $depth = 1, $id_lang = \false, $items_to_skip = \null, $id_shop = \false)
    {
    }
    protected function getCacheId($name = \null)
    {
    }
    protected function getCMSCategories($recursive = \false, $parent = 1, $id_lang = \false, $id_shop = \false)
    {
    }
    protected function getCMSPages($id_cms_category, $id_shop = \false, $id_lang = \false)
    {
    }
    public function hookActionObjectCategoryAddAfter($params)
    {
    }
    public function hookActionObjectCategoryUpdateAfter($params)
    {
    }
    public function hookActionObjectCategoryDeleteAfter($params)
    {
    }
    public function hookActionObjectCmsUpdateAfter($params)
    {
    }
    public function hookActionObjectCmsDeleteAfter($params)
    {
    }
    public function hookActionObjectCmsAddAfter($params)
    {
    }
    public function hookActionObjectSupplierUpdateAfter($params)
    {
    }
    public function hookActionObjectSupplierDeleteAfter($params)
    {
    }
    public function hookActionObjectSupplierAddAfter($params)
    {
    }
    public function hookActionObjectManufacturerUpdateAfter($params)
    {
    }
    public function hookActionObjectManufacturerDeleteAfter($params)
    {
    }
    public function hookActionObjectManufacturerAddAfter($params)
    {
    }
    public function hookActionObjectProductUpdateAfter($params)
    {
    }
    public function hookActionObjectProductDeleteAfter($params)
    {
    }
    public function hookActionObjectProductAddAfter($params)
    {
    }
    public function hookActionCategoryUpdate($params)
    {
    }
    protected function getCacheDirectory()
    {
    }
    protected function clearMenuCache()
    {
    }
    public function hookActionShopDataDuplication($params)
    {
    }
    public function renderForm()
    {
    }
    public function renderAddForm()
    {
    }
    public function renderChoicesSelect()
    {
    }
    public function customGetNestedCategories($shop_id, $root_category = \null, $id_lang = \false, $active = \false, $groups = \null, $use_shop_restriction = \true, $sql_filter = '', $sql_sort = '', $sql_limit = '')
    {
    }
    public function getAddLinkFieldsValues()
    {
    }
    public function renderList()
    {
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
    public function renderWidget($hookName, array $configuration)
    {
    }
}
