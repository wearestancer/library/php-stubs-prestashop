<?php

class dashtrends extends \Module
{
    protected $dashboard_data;
    protected $dashboard_data_compare;
    protected $dashboard_data_sum;
    protected $dashboard_data_sum_compare;
    protected $data_trends;
    /**
     * @var Currency
     */
    public $currency;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function hookActionAdminControllerSetMedia()
    {
    }
    public function hookDashboardZoneTwo($params)
    {
    }
    protected function getData($date_from, $date_to)
    {
    }
    protected function refineData($date_from, $date_to, $gross_data)
    {
    }
    protected function addupData($data)
    {
    }
    protected function compareData($data1, $data2)
    {
    }
    public function hookDashboardData($params)
    {
    }
    protected function addTaxSuffix()
    {
    }
    protected function translateCompareData($normal, $compare)
    {
    }
    public function getChartTrends()
    {
    }
    /**
     * @param string $date_from
     * @param string $date_to
     * @param string|bool $granularity
     *
     * @return array|false|string
     *
     * @throws PrestaShopDatabaseException
     */
    protected function getTotalSalesWithRefunds($date_from, $date_to, $granularity = \false)
    {
    }
    /**
     * @param string $date_from
     * @param string $date_to
     * @param string|bool $granularity
     *
     * @return array|false|string
     *
     * @throws PrestaShopDatabaseException
     */
    protected function getRefunds($date_from, $date_to, $granularity = \false)
    {
    }
}
