<?php

class Ps_Banner extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockbanner';
    public function __construct()
    {
    }
    public function install()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function hookActionObjectLanguageAddAfter($params)
    {
    }
    protected function installFixtures()
    {
    }
    protected function installFixture($id_lang, $image = \null)
    {
    }
    public function uninstall()
    {
    }
    public function postProcess()
    {
    }
    public function getContent()
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function renderWidget($hookName, array $params)
    {
    }
    public function getWidgetVariables($hookName, array $params)
    {
    }
}
