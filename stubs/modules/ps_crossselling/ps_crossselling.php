<?php

class Ps_Crossselling extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    const LIMIT_FACTOR = 50;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    public function hookActionOrderStatusPostUpdate($params)
    {
    }
    protected function _clearCache($template, $cacheId = \null, $compileId = \null)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getCacheIdKey($productIds)
    {
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
    public function renderWidget($hookName, array $configuration)
    {
    }
    protected function getOrderProducts(array $productIds = [])
    {
    }
}
