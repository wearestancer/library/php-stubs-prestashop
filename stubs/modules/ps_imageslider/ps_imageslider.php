<?php

class Ps_ImageSlider extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    protected $_html = '';
    protected $default_speed = 5000;
    protected $default_pause_on_hover = 1;
    protected $default_wrap = 1;
    protected $templateFile;
    /**
     * @var string
     */
    public $secure_key;
    public function __construct()
    {
    }
    /**
     * @see Module::install()
     */
    public function install()
    {
    }
    /**
     * Adds samples
     */
    protected function installSamples()
    {
    }
    /**
     * @see Module::uninstall()
     */
    public function uninstall()
    {
    }
    /**
     * Creates tables
     */
    protected function createTables()
    {
    }
    /**
     * deletes tables
     */
    protected function deleteTables()
    {
    }
    public function getContent()
    {
    }
    protected function _postValidation()
    {
    }
    protected function _postProcess()
    {
    }
    public function hookdisplayHeader($params)
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    protected function validateUrl($link)
    {
    }
    public function clearCache()
    {
    }
    public function hookActionShopDataDuplication($params)
    {
    }
    public function headerHTML()
    {
    }
    public function getNextPosition()
    {
    }
    public function getSlides($active = \null)
    {
    }
    public function getAllImagesBySlidesId($id_slides, $active = \null, $id_shop = \null)
    {
    }
    public function displayStatus($id_slide, $active)
    {
    }
    public function slideExists($id_slide)
    {
    }
    public function renderList()
    {
    }
    public function renderAddForm()
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getAddFieldsValues()
    {
    }
    protected function getMultiLanguageInfoMsg()
    {
    }
    protected function getWarningMultishopHtml()
    {
    }
    protected function getShopContextError($shop_contextualized_name, $mode)
    {
    }
    protected function getShopAssociationError($id_slide)
    {
    }
    protected function getCurrentShopInfoMsg()
    {
    }
    protected function getSharedSlideWarning()
    {
    }
}
