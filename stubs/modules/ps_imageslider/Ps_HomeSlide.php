<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */
class Ps_HomeSlide extends \ObjectModel
{
    public $title;
    public $description;
    public $url;
    public $legend;
    public $image;
    public $active;
    public $position;
    public $id_shop;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'homeslider_slides', 'primary' => 'id_homeslider_slides', 'multilang' => \true, 'fields' => [
        'active' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => \true],
        'position' => ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => \true],
        // Lang fields
        'description' => ['type' => self::TYPE_HTML, 'lang' => \true, 'validate' => 'isCleanHtml', 'size' => 4000],
        'title' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isCleanHtml', 'size' => 255],
        'legend' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isCleanHtml', 'size' => 255],
        'url' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isUrl', 'size' => 255],
        'image' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isCleanHtml', 'size' => 255],
    ]];
    public function add($autodate = \true, $null_values = \false)
    {
    }
    public function delete()
    {
    }
    public function reOrderPositions()
    {
    }
    public static function getAssociatedIdsShop($id_slide)
    {
    }
}
