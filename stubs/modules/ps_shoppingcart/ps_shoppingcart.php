<?php

class Ps_Shoppingcart extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockcart';
    public function __construct()
    {
    }
    /**
     * @return void
     */
    public function hookDisplayHeader()
    {
    }
    /**
     * @param string|null $hookName
     * @param array<string,mixed> $params
     *
     * @return array<string,mixed>
     */
    public function getWidgetVariables($hookName, array $params)
    {
    }
    /**
     * @param string|null $hookName
     * @param array<string,mixed> $params
     *
     * @return string
     */
    public function renderWidget($hookName, array $params)
    {
    }
    /**
     * @param int $id_product
     * @param int $id_product_attribute
     * @param int $id_customization
     *
     * @return string
     *
     * @throws Exception
     */
    public function renderModal($id_product, $id_product_attribute, $id_customization)
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @return bool
     */
    public function install()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     *
     * @return bool
     */
    public function uninstallPrestaShop16Module()
    {
    }
    /**
     * @return string
     */
    public function renderForm()
    {
    }
    /**
     * @return bool[]
     */
    public function getConfigFieldsValues()
    {
    }
}
