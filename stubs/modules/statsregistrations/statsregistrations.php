<?php

class statsregistrations extends \ModuleGraph
{
    public function __construct()
    {
    }
    /**
     * Called during module installation
     */
    public function install()
    {
    }
    /**
     * @return int Get total of registration in date range
     */
    public function getTotalRegistrations()
    {
    }
    /**
     * @return int Get total of blocked visitors during registration process
     */
    public function getBlockedVisitors()
    {
    }
    public function getFirstBuyers()
    {
    }
    public function hookDisplayAdminStatsModules()
    {
    }
    protected function getData($layers)
    {
    }
    protected function setAllTimeValues($layers)
    {
    }
    protected function setYearValues($layers)
    {
    }
    protected function setMonthValues($layers)
    {
    }
    protected function setDayValues($layers)
    {
    }
}
