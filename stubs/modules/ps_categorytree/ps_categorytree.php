<?php

class Ps_CategoryTree extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockcategories';
    /**
     * @var int A way to display the category tree: Home category
     */
    const CATEGORY_ROOT_HOME = 0;
    /**
     * @var int A way to display the category tree: Current category
     */
    const CATEGORY_ROOT_CURRENT = 1;
    /**
     * @var int A way to display the category tree: Parent category
     */
    const CATEGORY_ROOT_PARENT = 2;
    /**
     * @var int A way to display the category tree: Current category and its parent (if exists)
     */
    const CATEGORY_ROOT_CURRENT_PARENT = 3;
    public function __construct()
    {
    }
    public function install()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    public function getTree($resultParents, $resultIds, $maxDepth, $id_category = \null, $currentDepth = 0)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function setLastVisitedCategory()
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
}
