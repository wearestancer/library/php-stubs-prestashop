<?php

class Ps_Viewedproduct extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function hookActionObjectProductDeleteAfter($params)
    {
    }
    public function hookActionObjectProductUpdateAfter($params)
    {
    }
    public function getContent()
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getCacheId($name = \null)
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    protected function addViewedProduct($idProduct)
    {
    }
    protected function getViewedProductIds()
    {
    }
    protected function getViewedProducts()
    {
    }
}
