<?php

class Ps_faviconnotificationbo extends \Module
{
    const CONFIG_COUNT_ORDER_NOTIFICATION = 'CHECKBOX_ORDER';
    const CONFIG_COUNT_CUSTOMER_NOTIFICATION = 'CHECKBOX_CUSTOMER';
    const CONFIG_COUNT_MSG_NOTIFICATION = 'CHECKBOX_MESSAGE';
    const CONFIG_FAVICON_BACKGROUND_COLOR = 'BACKGROUND_COLOR_FAVICONBO';
    const CONFIG_FAVICON_TXT_COLOR = 'TEXT_COLOR_FAVICONBO';
    const HOOKS = ['displayBackOfficeHeader'];
    const ADMINCONTROLLERS = ['adminConfigure' => 'AdminConfigureFaviconBo'];
    public function __construct()
    {
    }
    /**
     * @return bool
     */
    public function install()
    {
    }
    /**
     * @return bool
     */
    public function installConfiguration()
    {
    }
    /**
     * @return bool
     */
    public function installTabs()
    {
    }
    /**
     * @return bool
     */
    public function uninstall()
    {
    }
    /**
     * @return bool
     */
    public function uninstallConfiguration()
    {
    }
    /**
     * @return bool
     */
    public function uninstallTabs()
    {
    }
    /**
     * Redirect to our ModuleAdminController when click on Configure button
     */
    public function getContent()
    {
    }
    /**
     * @param array $params
     *
     * @return string
     */
    public function hookDisplayBackOfficeHeader(array $params)
    {
    }
}
