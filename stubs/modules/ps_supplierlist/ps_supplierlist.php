<?php

class Ps_Supplierlist extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blocksupplier';
    protected $templateFile;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function getContent()
    {
    }
    public function hookActionObjectSupplierUpdateAfter($params)
    {
    }
    public function hookActionObjectSupplierAddAfter($params)
    {
    }
    public function hookActionObjectSupplierDeleteAfter($params)
    {
    }
    public function _clearCache($template, $id_cache = \null, $id_compile = \null)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function renderWidget($hookName, array $configuration)
    {
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
}
