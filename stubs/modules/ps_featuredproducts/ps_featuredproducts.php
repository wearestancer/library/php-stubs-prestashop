<?php

class Ps_FeaturedProducts extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function hookActionProductAdd($params)
    {
    }
    public function hookActionProductUpdate($params)
    {
    }
    public function hookActionProductDelete($params)
    {
    }
    public function hookActionCategoryUpdate($params)
    {
    }
    public function hookActionAdminGroupsControllerSaveAfter($params)
    {
    }
    public function _clearCache($template, $cache_id = \null, $compile_id = \null)
    {
    }
    public function getContent()
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    protected function getProducts()
    {
    }
    protected function getCacheId($name = \null)
    {
    }
}
