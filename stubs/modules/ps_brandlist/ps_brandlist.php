<?php

class Ps_Brandlist extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    protected $templateFile;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    public function hookActionObjectManufacturerUpdateAfter($params)
    {
    }
    public function hookActionObjectManufacturerAddAfter($params)
    {
    }
    public function hookActionObjectManufacturerDeleteAfter($params)
    {
    }
    public function _clearCache($template, $cache_id = \null, $compile_id = \null)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function renderWidget($hookName = \null, array $configuration = array())
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = array())
    {
    }
}
