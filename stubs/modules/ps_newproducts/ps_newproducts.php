<?php

class Ps_NewProducts extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function hookActionProductAdd($params)
    {
    }
    public function hookActionProductUpdate($params)
    {
    }
    public function hookActionProductDelete($params)
    {
    }
    public function _clearCache($template, $cache_id = \null, $compile_id = \null)
    {
    }
    public function getContent()
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function renderWidget($hookName, array $configuration)
    {
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
    protected function getNewProducts()
    {
    }
}
