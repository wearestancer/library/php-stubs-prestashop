<?php

/**
 * Removes files or directories.
 *
 * @param array $files An array of files to remove
 *
 * @return true|string True if everything goes fine, error details otherwise
 */
function removeFromFsDuringUpgrade(array $files)
{
}
/**
 * This upgrade file removes the folder vendor/phpunit, when added from a previous release installed on the shop.
 *
 * @return bool
 */
function upgrade_module_3_4_1($module)
{
}
