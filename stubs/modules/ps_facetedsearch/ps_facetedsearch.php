<?php

class Ps_Facetedsearch extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blocklayered';
    /**
     * Lock indexation if too many products
     *
     * @var int
     */
    const LOCK_TOO_MANY_PRODUCTS = 5000;
    /**
     * Lock template filter creation if too many products
     *
     * @var int
     */
    const LOCK_TEMPLATE_CREATION = 20000;
    /**
     * US iso code, used to prevent taxes usage while computing prices
     *
     * @var array
     */
    const ISO_CODE_TAX_FREE = ['US'];
    /**
     * Number of digits for MySQL DECIMAL
     *
     * @var int
     */
    const DECIMAL_DIGITS = 6;
    /**
     * @var array List of controllers supported by this module
     */
    protected $supportedControllers = [];
    public function __construct()
    {
    }
    /**
     * Check if method is an ajax request.
     * This check is an old behavior and only check for _GET value.
     *
     * @return bool
     */
    public function isAjax()
    {
    }
    /**
     * Return the current database instance
     *
     * @return Db
     */
    public function getDatabase()
    {
    }
    /**
     * Return current context
     *
     * @return Context
     */
    public function getContext()
    {
    }
    protected function getDefaultFilters()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    /**
     * @return HookDispatcher
     */
    public function getHookDispatcher()
    {
    }
    /*
     * Generate data product attributes
     *
     * @param int $idProduct
     *
     * @return boolean
     */
    public function indexAttributes($idProduct = \null)
    {
    }
    /*
     * Generate data for product features
     *
     * @return boolean
     */
    public function indexFeatures()
    {
    }
    /*
     * Generate data for product attribute group
     *
     * @return boolean
     */
    public function indexAttributeGroup()
    {
    }
    /**
     * Full prices index process
     *
     * @param int $cursor in order to restart indexing from the last state
     * @param bool $ajax
     * @param bool $smart
     */
    public function fullPricesIndexProcess($cursor = 0, $ajax = \false, $smart = \false)
    {
    }
    /**
     * Prices index process
     *
     * @param int $cursor in order to restart indexing from the last state
     * @param bool $ajax
     */
    public function pricesIndexProcess($cursor = 0, $ajax = \false)
    {
    }
    /**
     * Index product prices
     *
     * @param int $idProduct
     * @param bool $smart Delete before reindex
     */
    public function indexProductPrices($idProduct, $smart = \true)
    {
    }
    /**
     * Get page content
     */
    public function getContent()
    {
    }
    /**
     * Returns content for main module configuration screen
     */
    public function renderAdminMain()
    {
    }
    /**
     * Returns content for filter template creation or editing
     */
    public function renderAdminTemplateEdit($template = \null)
    {
    }
    public function displayLimitPostWarning($count)
    {
    }
    /**
     * Rebuild layered structure
     */
    public function rebuildLayeredStructure()
    {
    }
    /**
     * This method creates the first initial filter after installing the module,
     * from all available features and attributes.
     */
    public function createDefaultTemplate()
    {
    }
    /**
     * This method gets serialized data of filter templates from layered_filter table and builds detailed
     * information, one category = one line.
     */
    public function buildLayeredCategories()
    {
    }
    /**
     * Render template
     *
     * @param string $template
     * @param array $params
     *
     * @return string
     */
    public function render($template, array $params = [])
    {
    }
    /**
     * Check if link rewrite are availables and corrects
     *
     * @param array $params
     */
    public function checkLinksRewrite($params)
    {
    }
    /**
     * Dispatch hooks
     *
     * @param string $methodName
     * @param array $arguments
     */
    public function __call($methodName, array $arguments)
    {
    }
    /**
     * Invalid filter block cache
     */
    public function invalidateLayeredFilterBlockCache()
    {
    }
    /**
     * Install price indexes table
     */
    public function rebuildPriceIndexTable()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function renderWidget($hookName, array $configuration)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
    /**
     * Provides data about single filter template.
     *
     * @param int $idFilterTemplate ID of filter template
     *
     * @return array Filter data
     */
    public function getFilterTemplate($idFilterTemplate)
    {
    }
    /**
     * Returns array with all controllers supported by this module
     */
    public function getSupportedControllers()
    {
    }
    public function setSupportedControllers($supportedControllers)
    {
    }
    /**
     * Returns array with all controllers supported by this module
     */
    public function isControllerSupported($controller)
    {
    }
    /**
     * Should this controller filter blocks be cached?
     */
    public function shouldCacheController(string $controller)
    {
    }
    public function initializeSupportedControllers()
    {
    }
}
