<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class AttributeGroup extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = [
        'actionAttributeGroupDelete',
        'actionAttributeGroupSave',
        'displayAttributeGroupForm',
        'displayAttributeGroupPostProcess',
        // Hooks for migrated page
        'actionAttributeGroupFormBuilderModifier',
        'actionAttributeGroupFormDataProviderData',
        'actionAfterCreateAttributeGroupFormHandler',
        'actionAfterUpdateAttributeGroupFormHandler',
    ];
    /**
     * Hook for modifying attribute group form formBuilder
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAttributeGroupFormBuilderModifier(array $params)
    {
    }
    /**
     * Hook that provides extra data in the form.
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAttributeGroupFormDataProviderData(array $params)
    {
    }
    /**
     * Hook after creation form is handled in migrated page.
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAfterCreateAttributeGroupFormHandler(array $params) : void
    {
    }
    /**
     * Hook after edition form is handled in migrated page.
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAfterUpdateAttributeGroupFormHandler(array $params) : void
    {
    }
    /**
     * After save Attributes group
     *
     * @param array $params
     */
    public function actionAttributeGroupSave(array $params)
    {
    }
    /**
     * After delete attribute group
     *
     * @param array $params
     */
    public function actionAttributeGroupDelete(array $params)
    {
    }
    /**
     * Post process attribute group
     *
     * @param array $params
     */
    public function displayAttributeGroupPostProcess(array $params)
    {
    }
    /**
     * Attribute group form
     *
     * @param array $params
     *
     * @return string
     */
    public function displayAttributeGroupForm(array $params)
    {
    }
}
