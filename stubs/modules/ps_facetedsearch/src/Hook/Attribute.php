<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class Attribute extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = [
        'actionAttributeGroupDelete',
        'actionAttributeSave',
        'displayAttributeForm',
        'actionAttributePostProcess',
        // Hooks for migrated page
        'actionAttributeFormBuilderModifier',
        'actionAttributeFormDataProviderData',
        'actionAfterCreateAttributeFormHandler',
        'actionAfterUpdateAttributeFormHandler',
    ];
    /**
     * Hook for modifying attribute form formBuilder
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAttributeFormBuilderModifier(array $params)
    {
    }
    /**
     * Hook that provides extra data in the form.
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAttributeFormDataProviderData(array $params)
    {
    }
    /**
     * Hook after creation form is handled in migrated page.
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAfterCreateAttributeFormHandler(array $params) : void
    {
    }
    /**
     * Hook after edition form is handled in migrated page.
     *
     * @since PrestaShop 9.0.0
     *
     * @param array $params
     */
    public function actionAfterUpdateAttributeFormHandler(array $params) : void
    {
    }
    /**
     * After save attribute
     *
     * @param array $params
     */
    public function actionAttributeSave(array $params)
    {
    }
    /**
     * After delete attribute
     *
     * @param array $params
     */
    public function actionAttributeGroupDelete(array $params)
    {
    }
    /**
     * Post process attribute
     *
     * @param array $params
     */
    public function actionAttributePostProcess(array $params)
    {
    }
    /**
     * Attribute form
     *
     * @param array $params
     */
    public function displayAttributeForm(array $params)
    {
    }
}
