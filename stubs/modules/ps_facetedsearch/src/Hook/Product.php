<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class Product extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = ['actionProductSave'];
    /**
     * After save product
     *
     * @param array $params
     */
    public function actionProductSave(array $params)
    {
    }
}
