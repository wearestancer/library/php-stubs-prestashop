<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class Design extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = ['displayLeftColumn'];
    /**
     * Force this hook to be called here instance of using WidgetInterface
     * because Hook::isHookCallableOn before the instanceof function.
     * Which means is_callable always returns true with a __call usage.
     *
     * @param array $params
     */
    public function displayLeftColumn(array $params)
    {
    }
}
