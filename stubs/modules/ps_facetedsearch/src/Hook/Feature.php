<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class Feature extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    public function __construct(\Ps_Facetedsearch $module)
    {
    }
    const AVAILABLE_HOOKS = ['actionFeatureSave', 'actionFeatureDelete', 'displayFeatureForm', 'displayFeaturePostProcess', 'actionFeatureFormBuilderModifier', 'actionAfterCreateFeatureFormHandler', 'actionAfterUpdateFeatureFormHandler'];
    /**
     * Hook for modifying feature form formBuilder
     *
     * @param array $params
     *
     * @throws PrestaShopDatabaseException
     */
    public function actionFeatureFormBuilderModifier(array $params)
    {
    }
    /**
     * Hook after create feature.
     *
     * @since PrestaShop 1.7.8.0
     *
     * @param array $params
     */
    public function actionAfterCreateFeatureFormHandler(array $params)
    {
    }
    /**
     * Hook after update feature.
     *
     * @since PrestaShop 1.7.8.0
     *
     * @param array $params
     */
    public function actionAfterUpdateFeatureFormHandler(array $params)
    {
    }
    /**
     * Hook after delete a feature
     *
     * @param array $params
     */
    public function actionFeatureDelete(array $params)
    {
    }
    /**
     * Hook post process feature
     *
     * @param array $params
     */
    public function displayFeaturePostProcess(array $params)
    {
    }
    /**
     * Hook feature form
     *
     * @param array $params
     */
    public function displayFeatureForm(array $params)
    {
    }
    /**
     * After save feature
     *
     * @param array $params
     */
    public function actionFeatureSave(array $params)
    {
    }
}
