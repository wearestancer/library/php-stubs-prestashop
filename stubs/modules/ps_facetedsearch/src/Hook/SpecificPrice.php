<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class SpecificPrice extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    /**
     * @var array
     */
    protected $productsBefore = null;
    const AVAILABLE_HOOKS = ['actionObjectSpecificPriceRuleUpdateBefore', 'actionAdminSpecificPriceRuleControllerSaveAfter'];
    /**
     * Before saving a specific price rule
     *
     * @param array $params
     */
    public function actionObjectSpecificPriceRuleUpdateBefore(array $params)
    {
    }
    /**
     * After saving a specific price rule
     *
     * @param array $params
     */
    public function actionAdminSpecificPriceRuleControllerSaveAfter(array $params)
    {
    }
}
