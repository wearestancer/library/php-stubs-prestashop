<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class FeatureValue extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = ['actionFeatureValueSave', 'actionFeatureValueDelete', 'displayFeatureValueForm', 'displayFeatureValuePostProcess', 'actionFeatureValueFormBuilderModifier', 'actionAfterCreateFeatureValueFormHandler', 'actionAfterUpdateFeatureValueFormHandler'];
    public function __construct(\Ps_Facetedsearch $module)
    {
    }
    /**
     * Hook for modifying feature form formBuilder
     *
     * @since PrestaShop 9.0
     *
     * @param array $params
     */
    public function actionFeatureValueFormBuilderModifier(array $params)
    {
    }
    /**
     * Hook after create feature.
     *
     * @since PrestaShop 9.0
     *
     * @param array $params
     */
    public function actionAfterCreateFeatureValueFormHandler(array $params)
    {
    }
    /**
     * Hook after update feature.
     *
     * @since PrestaShop 9.0
     *
     * @param array $params
     */
    public function actionAfterUpdateFeatureValueFormHandler(array $params)
    {
    }
    /**
     * After save feature value
     *
     * @param array $params
     */
    public function actionFeatureValueSave(array $params)
    {
    }
    /**
     * After delete Feature value
     *
     * @param array $params
     */
    public function actionFeatureValueDelete(array $params)
    {
    }
    /**
     * Post process feature value
     *
     * @param array $params
     */
    public function displayFeatureValuePostProcess(array $params)
    {
    }
    /**
     * Display feature value form
     *
     * @param array $params
     *
     * @return string
     */
    public function displayFeatureValueForm(array $params)
    {
    }
}
