<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class Configuration extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = ['actionProductPreferencesPageStockSave'];
    /**
     * After save of product stock preferences form
     *
     * @param array $params
     */
    public function actionProductPreferencesPageStockSave(array $params)
    {
    }
}
