<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

abstract class AbstractHook
{
    const AVAILABLE_HOOKS = [];
    /**
     * @var Context
     */
    protected $context;
    /**
     * @var Ps_Facetedsearch
     */
    protected $module;
    /**
     * @var Db
     */
    protected $database;
    public function __construct(\Ps_Facetedsearch $module)
    {
    }
    /**
     * @return array
     */
    public function getAvailableHooks()
    {
    }
}
