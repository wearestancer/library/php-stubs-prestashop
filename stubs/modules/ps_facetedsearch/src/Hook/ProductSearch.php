<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class ProductSearch extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = ['productSearchProvider'];
    /**
     * This method returns the search provider to the controller who requested it.
     *
     * @param array $params
     *
     * @return SearchProvider|null
     */
    public function productSearchProvider(array $params)
    {
    }
}
