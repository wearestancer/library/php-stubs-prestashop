<?php

namespace PrestaShop\Module\FacetedSearch\Hook;

class Category extends \PrestaShop\Module\FacetedSearch\Hook\AbstractHook
{
    const AVAILABLE_HOOKS = ['actionCategoryAdd', 'actionCategoryDelete', 'actionCategoryUpdate'];
    /**
     * Category addition
     *
     * @param array $params
     */
    public function actionCategoryAdd(array $params)
    {
    }
    /**
     * Category update
     *
     * @param array $params
     */
    public function actionCategoryUpdate(array $params)
    {
    }
    /**
     * Category deletion
     *
     * @param array $params
     */
    public function actionCategoryDelete(array $params)
    {
    }
    /**
     * Checks if module is configured to automatically add some filter to new categories.
     * If so, it adds the new category.
     *
     * @param int $idCategory ID of category being created
     */
    public function addCategoryToDefaultFilter(int $idCategory)
    {
    }
}
