<?php

namespace PrestaShop\Module\FacetedSearch;

/**
 * Class works with Hook\AbstractHook instances in order to reduce ps_facetedsearch.php size.
 *
 * The dispatch method is called from the __call method in the module class.
 */
class HookDispatcher
{
    const CLASSES = [\PrestaShop\Module\FacetedSearch\Hook\Attribute::class, \PrestaShop\Module\FacetedSearch\Hook\AttributeGroup::class, \PrestaShop\Module\FacetedSearch\Hook\Category::class, \PrestaShop\Module\FacetedSearch\Hook\Configuration::class, \PrestaShop\Module\FacetedSearch\Hook\Design::class, \PrestaShop\Module\FacetedSearch\Hook\Feature::class, \PrestaShop\Module\FacetedSearch\Hook\FeatureValue::class, \PrestaShop\Module\FacetedSearch\Hook\Product::class, \PrestaShop\Module\FacetedSearch\Hook\ProductSearch::class, \PrestaShop\Module\FacetedSearch\Hook\SpecificPrice::class];
    /**
     * Init hooks
     *
     * @param Ps_Facetedsearch $module
     */
    public function __construct(\Ps_Facetedsearch $module)
    {
    }
    /**
     * Get available hooks
     *
     * @return string[]
     */
    public function getAvailableHooks()
    {
    }
    /**
     * Find hook and dispatch it
     *
     * @param string $hookName
     * @param array $params
     *
     * @return mixed
     */
    public function dispatch($hookName, array $params = [])
    {
    }
}
