<?php

namespace PrestaShop\Module\FacetedSearch\Constraint;

class UrlSegment extends \Symfony\Component\Validator\Constraint
{
    public $message = '%s is invalid.';
    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
    }
}
