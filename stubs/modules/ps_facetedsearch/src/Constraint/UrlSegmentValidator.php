<?php

namespace PrestaShop\Module\FacetedSearch\Constraint;

/**
 * Class UrlSegmentValidator responsible for validating an URL segment.
 */
class UrlSegmentValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    /**
     * @param Tools $tools
     */
    public function __construct(\PrestaShop\PrestaShop\Adapter\Tools $tools)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
    }
}
