<?php

namespace PrestaShop\Module\FacetedSearch;

class URLSerializer
{
    /**
     * Add filter
     *
     * @param array $facetFilters
     * @param Filter $facetFilter
     * @param Facet $facet
     *
     * @return array
     */
    public function addFilterToFacetFilters(array $facetFilters, \PrestaShop\PrestaShop\Core\Product\Search\Filter $facetFilter, \PrestaShop\PrestaShop\Core\Product\Search\Facet $facet)
    {
    }
    /**
     * Remove filter
     *
     * @param array $facetFilters
     * @param Filter $facetFilter
     * @param Facet $facet
     *
     * @return array
     */
    public function removeFilterFromFacetFilters(array $facetFilters, \PrestaShop\PrestaShop\Core\Product\Search\Filter $facetFilter, $facet)
    {
    }
    /**
     * Get active facet filters
     *
     * @return array
     */
    public function getActiveFacetFiltersFromFacets(array $facets)
    {
    }
    /**
     * @param array $fragment
     *
     * @return string
     */
    public function serialize(array $fragment)
    {
    }
    /**
     * @param string $string
     *
     * @return array
     */
    public function unserialize($string)
    {
    }
}
