<?php

namespace PrestaShop\Module\FacetedSearch\Adapter;

class MySQL extends \PrestaShop\Module\FacetedSearch\Adapter\AbstractAdapter
{
    /**
     * @var string
     */
    const TYPE = 'MySQL';
    /**
     * @var string
     */
    const LEFT_JOIN = 'LEFT JOIN';
    /**
     * @var string
     */
    const INNER_JOIN = 'INNER JOIN';
    /**
     * {@inheritdoc}
     */
    public function getMinMaxPriceValue()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilteredSearchAdapter($resetFilter = null, $skipInitialPopulation = false)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function execute()
    {
    }
    /**
     * Construct the final sql query
     *
     * @return string
     */
    public function getQuery()
    {
    }
    /**
     * Define the mapping between fields and tables
     *
     * @return array
     */
    protected function getFieldMapping()
    {
    }
    /**
     * Get the joined and escaped value from an multi-dimensional array
     *
     * @param string $separator
     * @param array $values
     *
     * @return string Escaped string value
     */
    protected function getJoinedEscapedValue($separator, array $values)
    {
    }
    /**
     * Compute the orderby fields, adding the proper alias that will be added to the final query
     *
     * @param array $filterToTableMapping
     *
     * @return string
     */
    protected function computeOrderByField(array $filterToTableMapping)
    {
    }
    /**
     * Sort product list: InStock, OOPS with qty 0, OutOfStock
     *
     * @param string $orderField
     * @param array $filterToTableMapping
     *
     * @return string
     */
    protected function computeShowLast($orderField, $filterToTableMapping)
    {
    }
    /**
     * Add alias to table field name
     *
     * @param string $fieldName
     * @param array $filterToTableMapping
     *
     * @return string Table Field name with an alias
     */
    protected function computeFieldName($fieldName, $filterToTableMapping, $sortByField = false)
    {
    }
    /**
     * Compute the select fields, adding the proper alias that will be added to the final query
     *
     * @param array $filterToTableMapping
     *
     * @return array
     */
    protected function computeSelectFields(array $filterToTableMapping)
    {
    }
    /**
     * Computer the where conditions that will be added to the final query
     *
     * @param array $filterToTableMapping
     *
     * @return array
     */
    protected function computeWhereConditions(array $filterToTableMapping)
    {
    }
    /**
     * Compute the joinConditions needed depending on the fields required in select, where, groupby & orderby fields
     *
     * @param array $filterToTableMapping
     *
     * @return ArrayCollection
     */
    protected function computeJoinConditions(array $filterToTableMapping)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getMinMaxValue($fieldName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function count()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function valueCount($fieldName = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function useFiltersAsInitialPopulation()
    {
    }
    /**
     * @return Context
     */
    protected function getContext()
    {
    }
    /**
     * @return Db
     */
    protected function getDatabase()
    {
    }
    /**
     * Copy stock management operation filters
     * to make sure quantity is also used
     */
    protected function copyOperationsFilters()
    {
    }
}
