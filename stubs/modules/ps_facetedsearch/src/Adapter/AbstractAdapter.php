<?php

namespace PrestaShop\Module\FacetedSearch\Adapter;

abstract class AbstractAdapter implements \PrestaShop\Module\FacetedSearch\Adapter\InterfaceAdapter
{
    /**
     * @var ArrayCollection
     */
    protected $filters;
    /**
     * @var ArrayCollection
     */
    protected $operationsFilters;
    /**
     * @var ArrayCollection
     */
    protected $selectFields;
    /**
     * @var ArrayCollection
     */
    protected $groupFields;
    protected $orderField = 'id_product';
    protected $orderDirection = 'DESC';
    /** @var InterfaceAdapter */
    protected $initialPopulation = null;
    public function __construct()
    {
    }
    public function __clone()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getInitialPopulation()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetFilter($filterName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetOperationsFilter($filterName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetOperationsFilters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetAll()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilter($filterName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOrderDirection()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOrderField()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getGroupFields()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getSelectFields()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getOperationsFilters()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function copyFilters(\PrestaShop\Module\FacetedSearch\Adapter\InterfaceAdapter $adapter)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addFilter($filterName, $values, $operator = '=')
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addOperationsFilter($filterName, array $operations = [])
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addSelectField($fieldName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setSelectFields($selectFields)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetSelectField()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function addGroupBy($groupField)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setGroupFields($groupFields)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function resetGroupBy()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setFilter($filterName, $value)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setOrderField($fieldName)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function setOrderDirection($direction)
    {
    }
}
