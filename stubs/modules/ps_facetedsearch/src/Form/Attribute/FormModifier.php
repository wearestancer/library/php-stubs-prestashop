<?php

namespace PrestaShop\Module\FacetedSearch\Form\Attribute;

class FormModifier
{
    /**
     * @param DataCollectorTranslator|TranslatorComponent $translator
     */
    public function __construct($translator)
    {
    }
    public function modify(\Symfony\Component\Form\FormBuilderInterface $formBuilder)
    {
    }
}
