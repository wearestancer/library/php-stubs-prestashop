<?php

namespace PrestaShop\Module\FacetedSearch\Form\Attribute;

class FormDataProvider
{
    public function __construct(\Db $database)
    {
    }
    /**
     * Fills form data
     *
     * @param array $params
     *
     * @return array
     *
     * @throws PrestaShopDatabaseException
     */
    public function getData(array $params)
    {
    }
}
