<?php

namespace PrestaShop\Module\FacetedSearch\Form\FeatureValue;

/**
 * Provides form data
 */
class FormDataProvider
{
    public function __construct(\Db $database)
    {
    }
    /**
     * Fills form data
     *
     * @param array $params
     *
     * @return array
     */
    public function getData(array $params)
    {
    }
}
