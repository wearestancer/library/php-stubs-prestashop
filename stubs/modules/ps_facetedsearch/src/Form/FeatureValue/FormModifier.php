<?php

namespace PrestaShop\Module\FacetedSearch\Form\FeatureValue;

/**
 * Adds module specific fields to BO form
 */
class FormModifier
{
    public function __construct(\Context $context)
    {
    }
    public function modify(\Symfony\Component\Form\FormBuilderInterface $formBuilder, array $data)
    {
    }
}
