<?php

namespace PrestaShop\Module\FacetedSearch\Product;

class SearchProvider implements \PrestaShop\PrestaShop\Core\Product\Search\FacetsRendererInterface, \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchProviderInterface
{
    public function __construct(\Ps_Facetedsearch $module, \PrestaShop\Module\FacetedSearch\Filters\Converter $converter, \PrestaShop\Module\FacetedSearch\URLSerializer $serializer, \PrestaShop\Module\FacetedSearch\Filters\DataAccessor $dataAccessor, \PrestaShop\Module\FacetedSearch\Product\SearchFactory $searchFactory = null, \PrestaShop\Module\FacetedSearch\Filters\Provider $provider)
    {
    }
    /**
     * Instance of this class was previously passed to frontend controller, so we are now
     * ready to accept runQuery requests. The query object contains all the important information
     * about what we should get.
     *
     * @param ProductSearchContext $context
     * @param ProductSearchQuery $query
     *
     * @return ProductSearchResult
     */
    public function runQuery(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext $context, \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query)
    {
    }
    /**
     * Renders an product search result.
     *
     * @param ProductSearchContext $context
     * @param ProductSearchResult $result
     *
     * @return string the HTML of the facets
     */
    public function renderFacets(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext $context, \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult $result)
    {
    }
    /**
     * Renders an product search result of active filters.
     *
     * @param ProductSearchContext $context
     * @param ProductSearchResult $result
     *
     * @return string the HTML of the facets
     */
    public function renderActiveFilters(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext $context, \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult $result)
    {
    }
    /**
     * Converts a Facet to an array with all necessary
     * information for templating.
     *
     * @param Facet $facet
     *
     * @return array ready for templating
     */
    protected function prepareFacetForTemplate(\PrestaShop\PrestaShop\Core\Product\Search\Facet $facet)
    {
    }
}
