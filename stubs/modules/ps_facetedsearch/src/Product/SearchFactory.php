<?php

namespace PrestaShop\Module\FacetedSearch\Product;

class SearchFactory
{
    /**
     * Returns an instance of Search for this context
     *
     * @param Context $context
     *
     * @return Search
     */
    public function build(\Context $context)
    {
    }
}
