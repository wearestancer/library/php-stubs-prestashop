<?php

namespace PrestaShop\Module\FacetedSearch\Product;

/**
 * PrestaShop core does not provide a reasonable (fast) way to get product pool to to search
 * without extra performance overhead we don't need. This class contains fast backports of
 * Search::find method of every major for this purpose.
 *
 * This class will be removed when we are able to get product pool from Search class directly.
 */
class CoreSearchBackport
{
    /**
     * Returns a pool of product IDs to use when filtering products on search controller.
     *
     * @param ProductSearchQuery $query
     *
     * @return array Pool of product IDs
     */
    public function getProductPool(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query)
    {
    }
    /**
     * Backported from 1.7.6.9
     *
     * @param string $expr
     *
     * @return array Pool of product IDs
     */
    public function get176($expr)
    {
    }
    /**
     * Backported from 1.7.7.8
     *
     * @param string $expr
     *
     * @return array Pool of product IDs
     */
    public function get177($expr)
    {
    }
    /**
     * Backported from 1.7.8.8
     *
     * @param string $expr
     *
     * @return array Pool of product IDs
     */
    public function get178($expr)
    {
    }
    /**
     * Backported 8.0.1
     *
     * @param string $expr
     *
     * @return array Pool of product IDs
     */
    public function get80($expr)
    {
    }
}
