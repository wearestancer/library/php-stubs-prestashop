<?php

namespace PrestaShop\Module\FacetedSearch\Product;

class Search
{
    const STOCK_MANAGEMENT_FILTER = 'with_stock_management';
    const HIGHLIGHTS_FILTER = 'extras';
    /**
     * @var bool
     */
    protected $psStockManagement;
    /**
     * @var bool
     */
    protected $psOrderOutOfStock;
    /**
     * @var AbstractAdapter
     */
    protected $searchAdapter;
    /**
     * @var Context
     */
    protected $context;
    /**
     * @var ProductSearchQuery
     */
    protected $query;
    /**
     * Search constructor.
     *
     * @param Context $context
     * @param string $adapterType
     */
    public function __construct(\Context $context, $adapterType = \PrestaShop\Module\FacetedSearch\Adapter\MySQL::TYPE)
    {
    }
    /**
     * @return AbstractAdapter
     */
    public function getSearchAdapter()
    {
    }
    /**
     * @return ProductSearchQuery
     */
    public function getQuery()
    {
    }
    /**
     * @param ProductSearchQuery $query
     *
     * @return $this
     */
    public function setQuery(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query)
    {
    }
    /**
     * Init the initial population of the search filter
     *
     * @param array $selectedFilters
     */
    public function initSearch($selectedFilters)
    {
    }
    /**
     * Add a filter with the filterValues extracted from the selectedFilters
     *
     * @param string $filterName
     * @param array $filterValues
     */
    public function addFilter($filterName, array $filterValues)
    {
    }
}
