<?php

namespace PrestaShop\Module\FacetedSearch\Definition;

class Availability
{
    const IN_STOCK = 2;
    const AVAILABLE = 1;
    const NOT_AVAILABLE = 0;
}
