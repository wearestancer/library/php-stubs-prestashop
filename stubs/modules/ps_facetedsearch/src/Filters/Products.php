<?php

namespace PrestaShop\Module\FacetedSearch\Filters;

class Products
{
    public function __construct(\PrestaShop\Module\FacetedSearch\Product\Search $productSearch)
    {
    }
    /**
     * Get the products associated with the current filters.
     *
     * @param ProductSearchQuery $query
     * @param array $selectedFilters
     *
     * @return array
     */
    public function getProductByFilters(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query, array $selectedFilters = [])
    {
    }
}
