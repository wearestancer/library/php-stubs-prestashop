<?php

namespace PrestaShop\Module\FacetedSearch\Filters;

/**
 * Class responsible for providing filters configured for current search query
 */
class Provider
{
    public function __construct(\Db $database)
    {
    }
    /**
     * Get filters for current search query
     *
     * @param ProductSearchQuery $query
     * @param int $idShop
     *
     * @return array Filters
     */
    public function getFiltersForQuery(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query, int $idShop)
    {
    }
}
