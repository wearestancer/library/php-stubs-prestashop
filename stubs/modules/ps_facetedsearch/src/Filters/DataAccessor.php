<?php

namespace PrestaShop\Module\FacetedSearch\Filters;

/**
 * Data accessor for features and attributes
 */
class DataAccessor
{
    public function __construct(\Db $database)
    {
    }
    /**
     * Get all attributes for a given language and attribute group.
     *
     * @param int $idLang
     *
     * @return array Attributes
     */
    public function getAttributes($idLang, $idAttributeGroup)
    {
    }
    /**
     * Get all attributes groups for a given language.
     *
     * @param int $idLang Language id
     *
     * @return array Attributes groups
     */
    public function getAttributesGroups($idLang)
    {
    }
    /**
     * Get features with their associated layered information.
     *
     * @param int $idLang
     *
     * @return array Features
     */
    public function getFeatures($idLang)
    {
    }
    /**
     * Get feature values for given feature, with their associated layered information.
     *
     * @param int $idFeature
     * @param int $idLang
     *
     * @return array Feature values
     */
    public function getFeatureValues($idFeature, $idLang)
    {
    }
}
