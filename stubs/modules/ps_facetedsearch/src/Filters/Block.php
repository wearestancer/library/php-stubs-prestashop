<?php

namespace PrestaShop\Module\FacetedSearch\Filters;

/**
 * Display filters block on navigation
 */
class Block
{
    public function __construct(\PrestaShop\Module\FacetedSearch\Adapter\InterfaceAdapter $searchAdapter, \Context $context, \Db $database, \PrestaShop\Module\FacetedSearch\Filters\DataAccessor $dataAccessor, \PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query, \PrestaShop\Module\FacetedSearch\Filters\Provider $provider)
    {
    }
    /**
     * @param int $nbProducts
     * @param array $selectedFilters
     *
     * @return array
     */
    public function getFilterBlock($nbProducts, $selectedFilters)
    {
    }
    protected function showPriceFilter()
    {
    }
    /**
     * Get the filter block from the cache table
     *
     * @param string $filterHash
     *
     * @return array|null
     */
    public function getFromCache($filterHash)
    {
    }
    /**
     * Insert the filter block into the cache table
     *
     * @param string $filterHash
     * @param array $data
     */
    public function insertIntoCache($filterHash, $data)
    {
    }
}
