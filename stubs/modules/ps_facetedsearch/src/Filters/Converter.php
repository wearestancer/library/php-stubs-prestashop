<?php

namespace PrestaShop\Module\FacetedSearch\Filters;

class Converter
{
    const WIDGET_TYPE_CHECKBOX = 0;
    const WIDGET_TYPE_RADIO = 1;
    const WIDGET_TYPE_DROPDOWN = 2;
    const WIDGET_TYPE_SLIDER = 3;
    const TYPE_ATTRIBUTE_GROUP = 'id_attribute_group';
    const TYPE_AVAILABILITY = 'availability';
    const TYPE_CATEGORY = 'category';
    const TYPE_CONDITION = 'condition';
    const TYPE_FEATURE = 'id_feature';
    const TYPE_MANUFACTURER = 'manufacturer';
    const TYPE_PRICE = 'price';
    const TYPE_WEIGHT = 'weight';
    const TYPE_EXTRAS = 'extras';
    const PROPERTY_URL_NAME = 'url_name';
    const PROPERTY_COLOR = 'color';
    const PROPERTY_TEXTURE = 'texture';
    /**
     * @var array
     */
    const RANGE_FILTERS = [self::TYPE_PRICE, self::TYPE_WEIGHT];
    /**
     * @var Context
     */
    protected $context;
    /**
     * @var Db
     */
    protected $database;
    /**
     * @var URLSerializer
     */
    protected $urlSerializer;
    public function __construct(\Context $context, \Db $database, \PrestaShop\Module\FacetedSearch\URLSerializer $urlSerializer, \PrestaShop\Module\FacetedSearch\Filters\DataAccessor $dataAccessor, \PrestaShop\Module\FacetedSearch\Filters\Provider $provider)
    {
    }
    public function getFacetsFromFilterBlocks(array $filterBlocks)
    {
    }
    /**
     * This method is responsible of parsing the search filters sent in the query.
     * These filters come from the URL in 99 % of cases.
     *
     * It will unserialize it and convert it to actual unique and valid values that
     * we will later use to construct the database query. All invalid filters in the
     * query (unknown value, deleted in shop etc.) are ignored.
     *
     * Filters that are found (if any) will be later used in initSearch method, along
     * with some predefined ones related the the controller we are on.
     *
     * @param ProductSearchQuery $query
     *
     * @return array
     */
    public function createFacetedSearchFiltersFromQuery(\PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery $query)
    {
    }
}
