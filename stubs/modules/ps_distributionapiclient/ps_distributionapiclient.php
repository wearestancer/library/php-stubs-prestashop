<?php

class Ps_Distributionapiclient extends \Module
{
    public function __construct()
    {
    }
    public function install() : bool
    {
    }
    /**
     * @return array<array<string, string>>
     */
    public function hookActionListModules() : array
    {
    }
    /**
     * @param string[] $params
     *
     * @return void
     */
    public function hookActionBeforeInstallModule(array $params) : void
    {
    }
    /**
     * @param string[] $params
     *
     * @return void
     */
    public function hookActionBeforeUpgradeModule(array $params) : void
    {
    }
}
