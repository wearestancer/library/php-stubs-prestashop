<?php

namespace PrestaShop\Module\DistributionApiClient;

/**
 * Provides information about the shop, to be added to API calls
 */
class ShopDataProvider
{
    /**
     * Returns the default URL to shop's Front office
     *
     * @return string
     */
    public function getShopUrl() : string
    {
    }
}
