<?php

namespace PrestaShop\Module\DistributionApiClient\Middleware;

class CachedResponse
{
    /**
     * @param string[][] $headers
     * @param string $body
     */
    public function __construct(array $headers, string $body)
    {
    }
    /**
     * @return string[][]
     */
    public function getHeaders() : array
    {
    }
    public function getBody() : string
    {
    }
}
