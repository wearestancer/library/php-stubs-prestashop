<?php

namespace PrestaShop\Module\DistributionApiClient;

class DistributionApi
{
    public const ALLOWED_FAILURES = 2;
    public const TIMEOUT_IN_SECONDS = 3;
    public const THRESHOLD_SECONDS = 86400;
    // 24 hours
    public const CACHE_LIFETIME_SECONDS = 86400;
    // 24 hours
    public const URL_TRACKING_ENV_NAME = 'PS_URL_TRACKING';
    public function __construct(\PrestaShop\CircuitBreaker\Contract\CircuitBreakerInterface $circruitBreaker, \PrestaShop\PrestaShop\Core\Module\SourceHandler\SourceHandlerFactory $sourceHandlerFactory, \PrestaShop\PrestaShop\Adapter\Module\ModuleDataProvider $moduleDataProvider, \PrestaShop\Module\DistributionApiClient\ShopDataProvider $shopDataProvider, string $prestashopVersion, string $downloadDirectory)
    {
    }
    /**
     * @return array<array<string, string>>
     */
    public function getModuleList() : array
    {
    }
    public function downloadModule(string $moduleName) : void
    {
    }
    public function isModuleOnDisk(string $moduleName) : bool
    {
    }
    /**
     * Extracts the download URL from a module data structure
     *
     * @param array{download_url?: string} $module Module data structure, from API response
     *
     * @return string Download URL
     */
    protected function getModuleDownloadUrl(array $module) : string
    {
    }
}
