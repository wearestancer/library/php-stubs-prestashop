<?php

class GraphNvD3 extends \ModuleGraphEngine
{
    public function __construct($type = \null)
    {
    }
    public function install()
    {
    }
    public function hookActionAdminControllerSetMedia($params)
    {
    }
    public static function hookGraphEngine($params, $drawer)
    {
    }
    public function createValues($values)
    {
    }
    public function setSize($width, $height)
    {
    }
    public function setLegend($legend)
    {
    }
    public function setTitles($titles)
    {
    }
    public function draw()
    {
    }
}
