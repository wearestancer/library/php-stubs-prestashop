<?php

/**
 * Class Ps_Linklist.
 */
class Ps_Linklist extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockcms';
    const MODULE_NAME = 'ps_linklist';
    protected $_html;
    protected $_display;
    public $templateFile;
    /**
     * @var string
     */
    public $templateFileColumn;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function hookActionUpdateLangAfter($params)
    {
    }
    public function hookActionGeneralPageSave()
    {
    }
    public function _clearCache($template, $cache_id = \null, $compile_id = \null)
    {
    }
    public function getContent()
    {
    }
    public function renderWidget($hookName, array $configuration)
    {
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
}
