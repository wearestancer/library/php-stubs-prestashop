<?php

namespace PrestaShop\Module\LinkList;

/**
 * Class used to migrate data from the 1.6 module
 */
class DataMigration
{
    public function __construct(\Db $db)
    {
    }
    /**
     * Retrieve content from 1.6 module, then cleanup
     */
    public function migrateData()
    {
    }
}
