<?php

namespace PrestaShop\Module\LinkList\Filter;

class LinkFilter
{
    public function __construct(array $routeFilters = [])
    {
    }
    public function addRouteFilter(\PrestaShop\Module\LinkList\Filter\RouteFilterInterface ...$routeFilters) : void
    {
    }
    public function isRouteEnabled(string $routeId) : bool
    {
    }
}
