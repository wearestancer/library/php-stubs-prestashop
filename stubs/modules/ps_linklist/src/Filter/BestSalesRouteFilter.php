<?php

namespace PrestaShop\Module\LinkList\Filter;

class BestSalesRouteFilter implements \PrestaShop\Module\LinkList\Filter\RouteFilterInterface
{
    public function supports(string $routeId) : bool
    {
    }
    public function isRouteEnabled(string $routeId) : bool
    {
    }
}
