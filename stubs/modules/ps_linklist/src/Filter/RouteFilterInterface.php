<?php

namespace PrestaShop\Module\LinkList\Filter;

interface RouteFilterInterface
{
    public function supports(string $routeId) : bool;
    public function isRouteEnabled(string $routeId) : bool;
}
