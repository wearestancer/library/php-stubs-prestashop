<?php

namespace PrestaShop\Module\LinkList\Adapter;

class ObjectModelHandler extends \PrestaShop\PrestaShop\Adapter\Domain\AbstractObjectModelHandler
{
    /**
     * @param int $linkBlockId
     * @param array $associatedShops
     * @param bool $forceAssociate
     */
    public function handleMultiShopAssociation(int $linkBlockId, array $associatedShops, bool $forceAssociate = false) : void
    {
    }
}
