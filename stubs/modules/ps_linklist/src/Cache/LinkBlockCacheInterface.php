<?php

namespace PrestaShop\Module\LinkList\Cache;

/**
 * Interface LinkBlockCacheInterface.
 */
interface LinkBlockCacheInterface
{
    /**
     * Clear module cache.
     */
    public function clearModuleCache();
}
