<?php

namespace PrestaShop\Module\LinkList\Cache;

/**
 * Class LegacyBlockCache.
 */
final class LegacyLinkBlockCache implements \PrestaShop\Module\LinkList\Cache\LinkBlockCacheInterface
{
    /**
     * {@inheritdoc}
     */
    public function clearModuleCache()
    {
    }
}
