<?php

namespace PrestaShop\Module\LinkList;

/**
 * Class LegacyLinkBlockRepository.
 */
class LegacyLinkBlockRepository
{
    /**
     * @param Db $db
     * @param Shop $shop
     * @param Translator $translator
     */
    public function __construct(\Db $db, \Shop $shop, \Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
    }
    /**
     * @param int $id_hook
     *
     * @return array
     *
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function getByIdHook($id_hook)
    {
    }
    /**
     * @return bool
     */
    public function createTables()
    {
    }
    public function dropTables()
    {
    }
    /**
     * @return bool
     */
    public function installFixtures()
    {
    }
}
