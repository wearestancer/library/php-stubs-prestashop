<?php

namespace PrestaShop\Module\LinkList\Form\Type;

class LinkBlockType extends \PrestaShopBundle\Form\Admin\Type\TranslatorAwareType
{
    /**
     * LinkBlockType constructor.
     *
     * @param TranslatorInterface $translator
     * @param array $locales
     * @param array $hookChoices
     * @param array $cmsPageChoices
     * @param array $productPageChoices
     * @param array $staticPageChoices
     * @param array $categoryChoices
     */
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator, array $locales, array $hookChoices, array $cmsPageChoices, array $productPageChoices, array $staticPageChoices, array $categoryChoices, bool $isMultiStoreUsed)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }
}
