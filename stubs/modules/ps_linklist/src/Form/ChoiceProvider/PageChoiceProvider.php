<?php

namespace PrestaShop\Module\LinkList\Form\ChoiceProvider;

/**
 * Class PageChoiceProvider.
 */
final class PageChoiceProvider extends \PrestaShop\Module\LinkList\Form\ChoiceProvider\AbstractDatabaseChoiceProvider
{
    /**
     * PageChoiceProvider constructor.
     *
     * @param Connection $connection
     * @param string $dbPrefix
     * @param int $idLang
     * @param array $shopIds
     * @param array $pageNames
     */
    public function __construct(\Doctrine\DBAL\Connection $connection, $dbPrefix, $idLang, array $shopIds, array $pageNames)
    {
    }
    /**
     * @return array
     *
     * @throws EntityNotFoundException
     */
    public function getChoices()
    {
    }
}
