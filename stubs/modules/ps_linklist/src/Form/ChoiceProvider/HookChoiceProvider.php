<?php

namespace PrestaShop\Module\LinkList\Form\ChoiceProvider;

/**
 * Class HookChoiceProvider.
 */
final class HookChoiceProvider extends \PrestaShop\Module\LinkList\Form\ChoiceProvider\AbstractDatabaseChoiceProvider
{
    /**
     * @return mixed
     */
    public function getChoices()
    {
    }
}
