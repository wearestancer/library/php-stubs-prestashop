<?php

namespace PrestaShop\Module\LinkList\Form\ChoiceProvider;

/**
 * Class CMSPageChoiceProvider.
 */
final class CMSPageChoiceProvider extends \PrestaShop\Module\LinkList\Form\ChoiceProvider\AbstractDatabaseChoiceProvider
{
    /**
     * CMSPageChoiceProvider constructor.
     *
     * @param Connection $connection
     * @param string $dbPrefix
     * @param array $categories
     * @param int $idLang
     * @param array $shopIds
     */
    public function __construct(\Doctrine\DBAL\Connection $connection, $dbPrefix, array $categories, $idLang, $shopIds)
    {
    }
    /**
     * @return array
     */
    public function getChoices()
    {
    }
}
