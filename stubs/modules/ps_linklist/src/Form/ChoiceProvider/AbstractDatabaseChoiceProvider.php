<?php

namespace PrestaShop\Module\LinkList\Form\ChoiceProvider;

/**
 * Class AbstractDatabaseChoiceProvider.
 */
abstract class AbstractDatabaseChoiceProvider implements \PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface
{
    /**
     * @var Connection
     */
    protected $connection;
    /**
     * @var string
     */
    protected $dbPrefix;
    /**
     * @var int
     */
    protected $idLang;
    /**
     * @var array
     */
    protected $shopIds;
    /**
     * AbstractDatabaseChoiceProvider constructor.
     *
     * @param Connection $connection
     * @param string $dbPrefix
     * @param int|null $idLang
     * @param array|null $shopIds
     */
    public function __construct(\Doctrine\DBAL\Connection $connection, $dbPrefix, $idLang = null, array $shopIds = null)
    {
    }
    /**
     * {@inheritdoc}
     */
    public abstract function getChoices();
}
