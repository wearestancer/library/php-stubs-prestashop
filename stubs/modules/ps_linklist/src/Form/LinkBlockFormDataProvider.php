<?php

namespace PrestaShop\Module\LinkList\Form;

/**
 * Class LinkBlockFormDataProvider.
 */
class LinkBlockFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface
{
    /**
     * LinkBlockFormDataProvider constructor.
     *
     * @param LinkBlockRepository $repository
     * @param LinkBlockCacheInterface $cache
     * @param array $languages
     * @param Context $shopContext
     * @param Configuration $configuration
     */
    public function __construct(\PrestaShop\Module\LinkList\Repository\LinkBlockRepository $repository, \PrestaShop\Module\LinkList\Cache\LinkBlockCacheInterface $cache, array $languages, \PrestaShop\PrestaShop\Adapter\Shop\Context $shopContext, \PrestaShop\PrestaShop\Adapter\Configuration $configuration)
    {
    }
    /**
     * @return array
     *
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function getData()
    {
    }
    /**
     * Make sure to fill empty multilang fields if value for default is available
     *
     * @param array $linkBlock
     *
     * @return array
     */
    public function prepareData(array $linkBlock) : array
    {
    }
    /**
     * @param array $data
     *
     * @return array
     *
     * @throws \PrestaShop\PrestaShop\Adapter\Entity\PrestaShopDatabaseException
     */
    public function setData(array $data)
    {
    }
    /**
     * @return int
     */
    public function getIdLinkBlock()
    {
    }
    /**
     * @param int $idLinkBlock
     *
     * @return LinkBlockFormDataProvider
     */
    public function setIdLinkBlock($idLinkBlock)
    {
    }
}
