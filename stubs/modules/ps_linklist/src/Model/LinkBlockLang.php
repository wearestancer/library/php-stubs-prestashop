<?php

namespace PrestaShop\Module\LinkList\Model;

/**
 * Class LinkBlockLang.
 */
class LinkBlockLang extends \DataLangCore
{
    // Don't replace domain in init() with $this->domain for translation parsing
    protected $domain = 'Modules.Linklist.Shop';
    protected $keys = ['id_link_block'];
    protected $fieldsToUpdate = ['name'];
    /**
     * @var array<string, array<string, string>>
     */
    public $fieldNames = [];
    protected function init()
    {
    }
}
