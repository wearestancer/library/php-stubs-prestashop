<?php

namespace PrestaShop\Module\LinkList\Model;

/**
 * Class LinkBlock.
 */
class LinkBlock extends \ObjectModel
{
    /**
     * @var int
     */
    public $id_link_block;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $id_hook;
    /**
     * @var int
     */
    public $position;
    /**
     * @var array|string|null
     */
    public $content;
    /**
     * @var array
     */
    public $custom_content;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'link_block', 'primary' => 'id_link_block', 'multilang' => true, 'fields' => ['name' => ['type' => self::TYPE_STRING, 'lang' => true, 'required' => true, 'size' => 40], 'id_hook' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true], 'position' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true], 'content' => ['type' => self::TYPE_STRING, 'validate' => 'isJson'], 'custom_content' => ['type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isJson']]];
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
    }
    public function add($auto_date = true, $null_values = false)
    {
    }
    public function update($auto_date = true, $null_values = false)
    {
    }
    public function toArray()
    {
    }
}
