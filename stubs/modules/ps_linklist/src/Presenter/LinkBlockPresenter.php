<?php

namespace PrestaShop\Module\LinkList\Presenter;

/**
 * Class LinkBlockPresenter.
 */
class LinkBlockPresenter
{
    /**
     * LinkBlockPresenter constructor.
     *
     * @param \Link $link
     * @param \Language $language
     */
    public function __construct(\Link $link, \Language $language, \PrestaShop\Module\LinkList\Filter\LinkFilter $linkFilter = null)
    {
    }
    /**
     * @param LinkBlock $cmsBlock
     *
     * @return array
     *
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function present(\PrestaShop\Module\LinkList\Model\LinkBlock $cmsBlock)
    {
    }
    /**
     * Check the url if is an external link.
     *
     * @param string $url
     *
     * @return bool
     */
    public function isExternalLink($url)
    {
    }
}
