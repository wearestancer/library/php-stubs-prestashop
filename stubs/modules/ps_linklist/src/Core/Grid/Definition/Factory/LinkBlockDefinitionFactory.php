<?php

namespace PrestaShop\Module\LinkList\Core\Grid\Definition\Factory;

/**
 * Class LinkBlockDefinitionFactory.
 */
final class LinkBlockDefinitionFactory extends \PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractGridDefinitionFactory
{
    const FACTORY_ID = 'link_widget_grid_';
    /**
     * LinkBlockDefinitionFactory constructor.
     *
     * @param array $hook
     * @param MultistoreContextCheckerInterface $multistoreContextChecker
     */
    public function __construct(array $hook, \PrestaShop\PrestaShop\Core\Multistore\MultistoreContextCheckerInterface $multistoreContextChecker, \PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface $hookDispatcher)
    {
    }
}
