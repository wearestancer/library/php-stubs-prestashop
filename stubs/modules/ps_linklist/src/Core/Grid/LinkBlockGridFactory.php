<?php

namespace PrestaShop\Module\LinkList\Core\Grid;

/**
 * Class LinkBlockGridFactory.
 */
final class LinkBlockGridFactory
{
    /**
     * HookGridFactory constructor.
     *
     * @param TranslatorInterface $translator
     * @param HookDispatcherInterface $hookDispatcher
     * @param GridDataFactoryInterface $dataFactory
     * @param GridFilterFormFactoryInterface $filterFormFactory
     * @param Context $shopContext
     */
    public function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator, \PrestaShop\PrestaShop\Core\Grid\Data\Factory\GridDataFactoryInterface $dataFactory, \PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface $hookDispatcher, \PrestaShop\PrestaShop\Core\Grid\Filter\GridFilterFormFactoryInterface $filterFormFactory, \PrestaShop\PrestaShop\Adapter\Shop\Context $shopContext)
    {
    }
    /**
     * @param array $hooks
     * @param array $filtersParams
     *
     * @return GridInterface[]
     */
    public function getGrids(array $hooks, array $filtersParams)
    {
    }
}
