<?php

namespace PrestaShop\Module\LinkList\Repository;

/**
 * Class LinkBlockRepository.
 */
class LinkBlockRepository
{
    /**
     * LinkBlockRepository constructor.
     *
     * @param Connection $connection
     * @param string $dbPrefix
     * @param array $languages
     * @param TranslatorInterface $translator
     */
    public function __construct(\Doctrine\DBAL\Connection $connection, $dbPrefix, array $languages, \Symfony\Contracts\Translation\TranslatorInterface $translator, bool $isMultiStoreUsed, \PrestaShop\PrestaShop\Adapter\Shop\Context $multiStoreContext, \PrestaShop\Module\LinkList\Adapter\ObjectModelHandler $objectModelHandler)
    {
    }
    /**
     * Returns the list of hook with associated Link blocks.
     *
     * @return array
     */
    public function getHooksWithLinks()
    {
    }
    /**
     * @param array $data
     *
     * @return string
     *
     * @throws DatabaseException
     */
    public function create(array $data)
    {
    }
    /**
     * @param int $linkBlockId
     * @param array $data
     *
     * @throws DatabaseException
     */
    public function update($linkBlockId, array $data)
    {
    }
    /**
     * @param int $idLinkBlock
     *
     * @throws DatabaseException
     */
    public function delete($idLinkBlock) : void
    {
    }
    /**
     * @return array
     */
    public function createTables()
    {
    }
    /**
     * @return array
     */
    public function installFixtures()
    {
    }
    /**
     * @return array
     */
    public function dropTables()
    {
    }
    /**
     * @param int $shopId
     * @param array $positionsData
     *
     * @return void
     */
    public function updatePositions(int $shopId, array $positionsData = []) : void
    {
    }
}
