<?php

class Ps_Cashondelivery extends \PaymentModule
{
    const HOOKS = ['displayOrderConfirmation', 'paymentOptions'];
    const CONFIG_OS_CASH_ON_DELIVERY = 'PS_OS_COD_VALIDATION';
    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
    }
    /**
     * {@inheritdoc}
     */
    public function install()
    {
    }
    /**
     * @param array{cookie: Cookie, cart: Cart, altern: int} $params
     *
     * @return array|PaymentOption[] Should always returns an array to avoid issue
     */
    public function hookPaymentOptions(array $params)
    {
    }
    /**
     * @param array{cookie: Cookie, cart: Cart, altern: int, order: Order, objOrder: Order} $params
     *
     * @return string
     */
    public function hookDisplayOrderConfirmation(array $params)
    {
    }
    /**
     * @return bool
     */
    public function installOrderState()
    {
    }
}
