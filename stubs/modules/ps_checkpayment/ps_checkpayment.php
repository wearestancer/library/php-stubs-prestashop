<?php

class Ps_Checkpayment extends \PaymentModule
{
    public $checkName;
    public $address;
    public $extra_mail_vars;
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function uninstall()
    {
    }
    public function getContent()
    {
    }
    public function hookPaymentOptions($params)
    {
    }
    public function hookDisplayPaymentReturn($params)
    {
    }
    public function checkCurrency($cart)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getTemplateVars()
    {
    }
}
