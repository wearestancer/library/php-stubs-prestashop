<?php

class Ps_Dataprivacy extends \Module
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockcustomerprivacy';
    public function __construct()
    {
    }
    public function install()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function getContent()
    {
    }
    /**
     * Add an extra FormField to ask for data privacy consent.
     *
     * @param array $params
     *
     * @return array<int, FormField>
     */
    public function hookAdditionalCustomerFormFields($params)
    {
    }
    public function renderForm()
    {
    }
    public function getConfigFieldsValues()
    {
    }
}
