<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */
class ProductCommentCriterion extends \ObjectModel
{
    const NAME_MAX_LENGTH = 64;
    public $id;
    public $id_product_comment_criterion_type;
    public $name;
    public $active = \true;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'product_comment_criterion', 'primary' => 'id_product_comment_criterion', 'multilang' => \true, 'fields' => [
        'id_product_comment_criterion_type' => ['type' => self::TYPE_INT],
        'active' => ['type' => self::TYPE_BOOL],
        // Lang fields
        'name' => ['type' => self::TYPE_STRING, 'lang' => \true, 'validate' => 'isGenericName', 'required' => \true, 'size' => self::NAME_MAX_LENGTH],
    ]];
    /**
     * @deprecated 6.0.0 - migrated to src/Repository/ProductCommentCriterionRepository
     */
    public function delete()
    {
    }
    public function update($nullValues = \false)
    {
    }
    /**
     * Link a Comment Criterion to a product
     *
     * @return bool succeed
     *
     * @deprecated 6.0.0 - migrated to src/Repository/ProductCommentCriterionRepository
     */
    public function addProduct($id_product)
    {
    }
    /**
     * Link a Comment Criterion to a category
     *
     * @return bool succeed
     *
     * @deprecated 6.0.0 - migrated to src/Repository/ProductCommentCriterionRepository
     */
    public function addCategory($id_category)
    {
    }
    /**
     * Add grade to a criterion
     *
     * @return bool succeed
     *
     * @deprecated 4.0.0
     */
    public function addGrade($id_product_comment, $grade)
    {
    }
    /**
     * Get criterion by Product
     *
     * @return array Criterion
     *
     * @deprecated 4.0.0
     */
    public static function getByProduct($id_product, $id_lang)
    {
    }
    /**
     * Get Criterions
     *
     * @return array Criterions
     *
     * @deprecated 6.0.0
     */
    public static function getCriterions($id_lang, $type = \false, $active = \false)
    {
    }
    /**
     * @deprecated 6.0.0
     */
    public function getProducts()
    {
    }
    /**
     * @deprecated 6.0.0
     */
    public function getCategories()
    {
    }
    /**
     * @deprecated 6.0.0
     */
    public function deleteCategories()
    {
    }
    /**
     * @deprecated 6.0.0
     */
    public function deleteProducts()
    {
    }
    /**
     * @deprecated 6.0.0
     */
    public static function getTypes()
    {
    }
}
