<?php

class ProductComment extends \ObjectModel
{
    /** @var int */
    public $id;
    /** @var int */
    public $id_product;
    /** @var int */
    public $id_customer;
    /** @var int */
    public $id_guest;
    /** @var int */
    public $customer_name;
    /** @var string */
    public $title;
    /** @var string */
    public $content;
    /** @var int */
    public $grade;
    /** @var bool */
    public $validate = \false;
    /** @var bool */
    public $deleted = \false;
    /** @var string Object creation date */
    public $date_add;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = ['table' => 'product_comment', 'primary' => 'id_product_comment', 'fields' => ['id_product' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => \true], 'id_customer' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => \true], 'id_guest' => ['type' => self::TYPE_INT], 'customer_name' => ['type' => self::TYPE_STRING], 'title' => ['type' => self::TYPE_STRING], 'content' => ['type' => self::TYPE_STRING, 'validate' => 'isMessage', 'size' => 65535, 'required' => \true], 'grade' => ['type' => self::TYPE_FLOAT, 'validate' => 'isFloat'], 'validate' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool'], 'deleted' => ['type' => self::TYPE_BOOL], 'date_add' => ['type' => self::TYPE_DATE]]];
    /**
     * Get comments by IdProduct
     *
     * @return array|bool
     */
    public static function getByProduct($id_product, $p = 1, $n = \null, $id_customer = \null)
    {
    }
    /**
     * Return customer's comment
     *
     * @return array Comments
     */
    public static function getByCustomer($id_product, $id_customer, $get_last = \false, $id_guest = \false)
    {
    }
    /**
     * Get Grade By product
     *
     * @return array|bool
     */
    public static function getGradeByProduct($id_product, $id_lang)
    {
    }
    public static function getRatings($id_product)
    {
    }
    /**
     * @deprecated 4.0.0
     */
    public static function getAverageGrade($id_product)
    {
    }
    public static function getAveragesByProduct($id_product, $id_lang)
    {
    }
    /**
     * Return number of comments and average grade by products
     *
     * @return int|false
     *
     * @deprecated 4.0.0
     */
    public static function getCommentNumber($id_product)
    {
    }
    /**
     * Return number of comments and average grade by products
     *
     * @return int|bool
     */
    public static function getGradedCommentNumber($id_product)
    {
    }
    /**
     * Get comments by Validation
     *
     * @return array Comments
     *
     * @deprecated 6.0.0
     */
    public static function getByValidate($validate = '0', $deleted = \false, $p = \null, $limit = \null, $skip_validate = \false)
    {
    }
    /**
     * Get numbers of comments by Validation
     *
     * @return int Count of comments
     *
     * @deprecated 6.0.0
     */
    public static function getCountByValidate($validate = '0', $skip_validate = \false)
    {
    }
    /**
     * Get all comments
     *
     * @return array Comments
     */
    public static function getAll()
    {
    }
    /**
     * Validate a comment
     *
     * @return bool succeed
     */
    public function validate($validate = '1')
    {
    }
    /**
     * Delete a comment, grade and report data
     *
     * @return bool succeed
     *
     * @deprecated 6.0.0
     */
    public function delete()
    {
    }
    /**
     * Delete Grades
     *
     * @return bool succeed
     *
     * @deprecated 6.0.0
     */
    public static function deleteGrades($id_product_comment)
    {
    }
    /**
     * Delete Reports
     *
     * @return bool succeed
     *
     * @deprecated 6.0.0
     */
    public static function deleteReports($id_product_comment)
    {
    }
    /**
     * Delete usefulness
     *
     * @return bool succeed
     *
     * @deprecated 6.0.0
     */
    public static function deleteUsefulness($id_product_comment)
    {
    }
    /**
     * Report comment
     *
     * @return bool
     *
     * @deprecated 4.0.0 - migrated to controllers/front/ReportComment and src/Entity/ProductCommentReport
     */
    public static function reportComment($id_product_comment, $id_customer)
    {
    }
    /**
     * Comment already report
     *
     * @return bool
     *
     * @deprecated 4.0.0 - migrated to controllers/front/ReportComment and src/Entity/ProductCommentReport
     */
    public static function isAlreadyReport($id_product_comment, $id_customer)
    {
    }
    /**
     * Set comment usefulness
     *
     * @return bool
     *
     * @deprecated 4.0.0 - migrated to controllers/front/UpdateCommentUsefulness and src/Entity/ProductCommentUsefulness
     */
    public static function setCommentUsefulness($id_product_comment, $usefulness, $id_customer)
    {
    }
    /**
     * Usefulness already set
     *
     * @return bool
     *
     * @deprecated 4.0.0 - migrated to controllers/front/UpdateCommentUsefulness and src/Entity/ProductCommentUsefulness
     */
    public static function isAlreadyUsefulness($id_product_comment, $id_customer)
    {
    }
    /**
     * Get reported comments
     *
     * @return array Comments
     *
     * @deprecated 6.0.0
     */
    public static function getReportedComments()
    {
    }
}
