<?php

class ProductComments extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    const INSTALL_SQL_FILE = 'install.sql';
    public function __construct()
    {
    }
    public function install($keep = \true)
    {
    }
    public function uninstall($keep = \true)
    {
    }
    public function reset()
    {
    }
    public function deleteTables()
    {
    }
    public function getCacheId($id_product = \null)
    {
    }
    protected function _postProcess()
    {
    }
    public function getContent()
    {
    }
    public function renderConfigForm()
    {
    }
    public function renderModerateLists()
    {
    }
    /**
     * Method used by the HelperList to render the approve link
     *
     * @param string $token
     * @param int $id
     * @param string|null $name
     *
     * @return false|string
     */
    public function displayApproveLink($token, $id, $name = \null)
    {
    }
    /**
     * Method used by the HelperList to render the approve link
     *
     * @param string $token
     * @param int $id
     * @param string|null $name
     *
     * @return false|string
     */
    public function displayNoabuseLink($token, $id, $name = \null)
    {
    }
    public function renderCriterionList()
    {
    }
    public function renderCommentsList()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getCriterionFieldsValues(int $id = 0)
    {
    }
    public function getStandardFieldList()
    {
    }
    /**
     * Renders author name for the list, with the link if the author is a customer.
     *
     * @param string $value
     * @param array $row
     *
     * @return string
     */
    public function renderAuthorName($value, $row)
    {
    }
    public function renderCriterionForm($id_criterion = 0)
    {
    }
    public function initCategoriesAssociation($id_root = \null, $id_criterion = 0)
    {
    }
    public function hookActionDeleteGDPRCustomer($customer)
    {
    }
    public function hookActionExportGDPRData($customer)
    {
    }
    /**
     *  Inject the needed javascript and css files in the appropriate pages
     */
    public function hookDisplayHeader()
    {
    }
    /**
     * Display the comment list with the post modal at the bottom of the page
     *
     * @param array $params
     *
     * @return string
     *
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function hookDisplayFooterProduct($params)
    {
    }
    /**
     * Inject data about productcomments in the product object for frontoffice
     *
     * @param array $params
     *
     * @return array
     */
    public function hookFilterProductContent(array $params)
    {
    }
    public function getWidgetVariables($hookName = \null, array $configuration = [])
    {
    }
    public function renderWidget($hookName = \null, array $configuration = [])
    {
    }
    /**
     * empty listener for registerGDPRConsent hook
     */
    public function hookRegisterGDPRConsent()
    {
    }
}
