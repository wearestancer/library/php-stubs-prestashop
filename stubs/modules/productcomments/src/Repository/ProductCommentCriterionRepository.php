<?php

namespace PrestaShop\Module\ProductComment\Repository;

/**
 * @extends ServiceEntityRepository<ProductCommentCriterion>
 *
 * @method ProductCommentCriterion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCommentCriterion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCommentCriterion[] findAll()
 * @method ProductCommentCriterion[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCommentCriterionRepository extends \Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * @param Connection $connection
     * @param string $databasePrefix
     */
    public function __construct($registry, $connection, $databasePrefix)
    {
    }
    public function add(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $entity, bool $flush = false) : void
    {
    }
    public function remove(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $entity, bool $flush = false) : void
    {
    }
    /* Remove a criterion and Delete its manual relation _category, _product, _grade */
    public function delete(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $criterion) : int
    {
    }
    /* Update a criterion and Update its manual relation _category, _product */
    public function update(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $criterion) : int
    {
    }
    public function updateGeneral(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $criterion) : void
    {
    }
    /**
     * @return array
     *
     * @throws \PrestaShopException
     */
    public function getByProduct(int $idProduct, int $idLang)
    {
    }
    /**
     * @return array Criterions
     */
    public function getCriterions(int $id_lang, $type = false, $active = false)
    {
    }
    /**
     * @return array
     */
    public function getProducts(int $id_criterion)
    {
    }
    /**
     * @return array
     */
    public function getCategories(int $id_criterion)
    {
    }
    /**
     * @return array
     */
    public function getTypes()
    {
    }
    /**
     * @return ProductCommentCriterion
     *
     * @deprecated 7.0.0 - use standard find() instead
     */
    public function findRelation($id_criterion)
    {
    }
}
