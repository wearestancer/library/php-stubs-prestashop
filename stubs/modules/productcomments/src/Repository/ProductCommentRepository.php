<?php

namespace PrestaShop\Module\ProductComment\Repository;

/**
 * @extends ServiceEntityRepository<ProductComment>
 *
 * @method ProductComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductComment[] findAll()
 * @method ProductComment[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCommentRepository extends \Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository
{
    const DEFAULT_COMMENTS_PER_PAGE = 5;
    /**
     * @param ManagerRegistry $registry
     * @param Connection $connection
     * @param string $databasePrefix
     * @param bool $guestCommentsAllowed
     * @param int $commentsMinimalTime
     */
    public function __construct($registry, $connection, $databasePrefix, $guestCommentsAllowed, $commentsMinimalTime)
    {
    }
    public function add(\PrestaShop\Module\ProductComment\Entity\ProductComment $entity, bool $flush = false) : void
    {
    }
    public function remove(\PrestaShop\Module\ProductComment\Entity\ProductComment $entity, bool $flush = false) : void
    {
    }
    public function delete(\PrestaShop\Module\ProductComment\Entity\ProductComment $entity) : void
    {
    }
    /**
     * @param int $productId
     * @param int $page
     * @param int $commentsPerPage
     * @param bool $validatedOnly
     *
     * @return array
     */
    public function paginate($productId, $page, $commentsPerPage, $validatedOnly)
    {
    }
    /**
     * @param int $productCommentId
     *
     * @return array
     */
    public function getProductCommentUsefulness($productCommentId)
    {
    }
    /**
     * @param int $productId
     * @param bool $validatedOnly
     *
     * @return float
     */
    public function getAverageGrade($productId, $validatedOnly)
    {
    }
    /**
     * @param int $langId
     * @param int $shopId
     * @param int $validate
     * @param bool $deleted
     * @param int $page
     * @param int $limit
     * @param bool $skip_validate
     *
     * @return array
     */
    public function getByValidate($langId, $shopId, $validate = 0, $deleted = false, $page = null, $limit = null, $skip_validate = false)
    {
    }
    /**
     * @param int $validate
     * @param bool $skip_validate
     *
     * @return int
     */
    public function getCountByValidate($validate = 0, $skip_validate = false)
    {
    }
    /**
     * @param array $productIds
     * @param bool $validatedOnly
     *
     * @return array
     */
    public function getAverageGrades(array $productIds, $validatedOnly)
    {
    }
    /**
     * @param int $productId
     * @param bool $validatedOnly
     *
     * @return int
     */
    public function getCommentsNumber($productId, $validatedOnly)
    {
    }
    /**
     * @param array $productIds
     * @param bool $validatedOnly
     *
     * @return array
     */
    public function getCommentsNumberForProducts(array $productIds, $validatedOnly)
    {
    }
    /**
     * @param int $productId
     * @param int $idCustomer
     * @param int $idGuest
     *
     * @return bool
     */
    public function isPostAllowed($productId, $idCustomer, $idGuest)
    {
    }
    /**
     * @param int $productId
     * @param int $idCustomer
     *
     * @return array
     */
    public function getLastCustomerComment($productId, $idCustomer)
    {
    }
    /**
     * @param int $productId
     * @param int $idGuest
     *
     * @return array
     */
    public function getLastGuestComment($productId, $idGuest)
    {
    }
    /**
     * @param int $customerId
     */
    public function cleanCustomerData($customerId)
    {
    }
    /**
     * @param int $customerId
     * @param int $langId
     *
     * @return array
     */
    public function getCustomerData($customerId, $langId)
    {
    }
    /**
     * @param ProductComment $productComment
     * @param int $validate
     *
     * @return bool
     */
    public function validate($productComment, $validate = 1)
    {
    }
    /**
     * @param int $id_product_comment
     *
     * @return bool
     */
    public function deleteGrades($id_product_comment)
    {
    }
    /**
     * @param int $id_product_comment
     *
     * @return bool
     */
    public function deleteReports($id_product_comment)
    {
    }
    /**
     * @param int $id_product_comment
     *
     * @return bool
     */
    public function deleteUsefulness($id_product_comment)
    {
    }
    /**
     * @param int $langId
     * @param int $shopId
     *
     * @return array
     */
    public function getReportedComments($langId, $shopId)
    {
    }
}
