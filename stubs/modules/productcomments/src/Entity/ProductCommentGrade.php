<?php

namespace PrestaShop\Module\ProductComment\Entity;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class ProductCommentGrade
{
    /**
     * @param ProductComment $comment
     * @param ProductCommentCriterion $criterion
     * @param int $grade
     */
    public function __construct(\PrestaShop\Module\ProductComment\Entity\ProductComment $comment, \PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $criterion, $grade)
    {
    }
    /**
     * @return mixed
     */
    public function getComment()
    {
    }
    /**
     * @return mixed
     */
    public function getCriterion()
    {
    }
}
