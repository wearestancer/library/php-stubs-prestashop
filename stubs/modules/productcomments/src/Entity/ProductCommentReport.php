<?php

namespace PrestaShop\Module\ProductComment\Entity;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class ProductCommentReport
{
    /**
     * @param ProductComment $comment
     * @param int $customerId
     */
    public function __construct(\PrestaShop\Module\ProductComment\Entity\ProductComment $comment, $customerId)
    {
    }
    /**
     * @return ProductComment
     */
    public function getComment()
    {
    }
    /**
     * @return int
     */
    public function getCustomerId()
    {
    }
}
