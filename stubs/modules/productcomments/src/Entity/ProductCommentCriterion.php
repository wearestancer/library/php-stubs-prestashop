<?php

namespace PrestaShop\Module\ProductComment\Entity;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class ProductCommentCriterion
{
    const NAME_MAX_LENGTH = 64;
    const ENTIRE_CATALOG_TYPE = 1;
    const CATEGORIES_TYPE = 2;
    const PRODUCTS_TYPE = 3;
    public function __construct()
    {
    }
    /**
     * @return ArrayCollection
     */
    public function getCriterionLangs()
    {
    }
    /**
     * @return ProductCommentCriterionLang|null
     */
    public function getCriterionLangByLangId(int $langId)
    {
    }
    public function addCriterionLang(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterionLang $criterionLang) : self
    {
    }
    public function getCriterionName() : string
    {
    }
    /**
     * @return array
     *
     * @deprecated 7.0.0 - migrated to Form\ProductCommentCriterionFormDataProvider
     */
    public function getNames()
    {
    }
    /**
     * @param array $langNames
     *
     * @return ProductCommentCriterion
     *
     * @deprecated 7.0.0
     */
    public function setNames($langNames)
    {
    }
    /**
     * @return array
     */
    public function getCategories()
    {
    }
    /**
     * @param array $selectedCategories
     */
    public function setCategories($selectedCategories) : self
    {
    }
    /**
     * @return array
     */
    public function getProducts()
    {
    }
    /**
     * @param array $selectedProducts
     */
    public function setProducts($selectedProducts) : self
    {
    }
    public function getId() : int
    {
    }
    public function getType() : int
    {
    }
    public function setType(int $type) : self
    {
    }
    public function isActive() : bool
    {
    }
    public function setActive(bool $active) : self
    {
    }
    public function isValid() : bool
    {
    }
}
