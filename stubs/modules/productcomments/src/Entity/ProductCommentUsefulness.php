<?php

namespace PrestaShop\Module\ProductComment\Entity;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class ProductCommentUsefulness
{
    /**
     * @param ProductComment $comment
     * @param int $customerId
     * @param bool $usefulness
     */
    public function __construct(\PrestaShop\Module\ProductComment\Entity\ProductComment $comment, $customerId, $usefulness)
    {
    }
    /**
     * @return mixed
     */
    public function getComment()
    {
    }
    /**
     * @return int
     */
    public function getCustomerId()
    {
    }
    /**
     * @return bool
     */
    public function isUsefulness()
    {
    }
    /**
     * @param bool $usefulness
     *
     * @return ProductCommentUsefulness
     */
    public function setUsefulness($usefulness)
    {
    }
}
