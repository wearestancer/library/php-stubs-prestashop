<?php

namespace PrestaShop\Module\ProductComment\Entity;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class ProductComment
{
    const TITLE_MAX_LENGTH = 64;
    const CUSTOMER_NAME_MAX_LENGTH = 64;
    /**
     * @return int
     */
    public function getId()
    {
    }
    /**
     * @return int
     */
    public function getProductId()
    {
    }
    /**
     * @param int $productId
     *
     * @return ProductComment
     */
    public function setProductId($productId)
    {
    }
    /**
     * @return int
     */
    public function getCustomerId()
    {
    }
    /**
     * @param int $customerId
     *
     * @return ProductComment
     */
    public function setCustomerId($customerId)
    {
    }
    /**
     * @return int
     */
    public function getGuestId()
    {
    }
    /**
     * @param int $guestId
     *
     * @return ProductComment
     */
    public function setGuestId($guestId)
    {
    }
    /**
     * @return string
     */
    public function getCustomerName()
    {
    }
    /**
     * @param string $customerName
     *
     * @return ProductComment
     */
    public function setCustomerName($customerName)
    {
    }
    /**
     * @return string
     */
    public function getTitle()
    {
    }
    /**
     * @param string $title
     *
     * @return ProductComment
     */
    public function setTitle($title)
    {
    }
    /**
     * @return string
     */
    public function getContent()
    {
    }
    /**
     * @param string $content
     *
     * @return ProductComment
     */
    public function setContent($content)
    {
    }
    /**
     * @return int
     */
    public function getGrade()
    {
    }
    /**
     * @param int $grade
     *
     * @return ProductComment
     */
    public function setGrade($grade)
    {
    }
    /**
     * @return bool
     */
    public function isValidate()
    {
    }
    /**
     * @param bool $validate
     *
     * @return ProductComment
     */
    public function setValidate($validate)
    {
    }
    /**
     * @return bool
     */
    public function isDeleted()
    {
    }
    /**
     * @param bool $deleted
     *
     * @return ProductComment
     */
    public function setDeleted($deleted)
    {
    }
    /**
     * @return \DateTime
     */
    public function getDateAdd()
    {
    }
    /**
     * Date is stored in UTC timezone
     *
     * @param \DateTime $dateAdd
     *
     * @return ProductComment
     */
    public function setDateAdd($dateAdd)
    {
    }
    /**
     * @return array
     */
    public function toArray()
    {
    }
}
