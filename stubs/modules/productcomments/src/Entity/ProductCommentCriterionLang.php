<?php

namespace PrestaShop\Module\ProductComment\Entity;

/**
 * @ORM\Table()
 *
 * @ORM\Entity()
 */
class ProductCommentCriterionLang
{
    /**
     * @return ProductCommentCriterion
     */
    public function getProductCommentCriterion()
    {
    }
    public function setProductCommentCriterion(\PrestaShop\Module\ProductComment\Entity\ProductCommentCriterion $productcommentcriterion) : self
    {
    }
    /**
     * @return Lang
     */
    public function getLang()
    {
    }
    /**
     * @param Lang $lang
     */
    public function setLang(\PrestaShopBundle\Entity\Lang $lang) : self
    {
    }
    public function getName() : string
    {
    }
    public function setName(string $name) : self
    {
    }
}
