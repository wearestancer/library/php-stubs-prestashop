<?php

namespace PrestaShop\Module\ProductComment\Form;

class ProductCommentCriterionFormDataProvider implements \PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataProvider\FormDataProviderInterface
{
    /**
     * @param ProductCommentCriterionRepository $pccriterionRepository
     * @param LangRepository $langRepository
     */
    public function __construct(\PrestaShop\Module\ProductComment\Repository\ProductCommentCriterionRepository $pccriterionRepository, \PrestaShopBundle\Entity\Repository\LangRepository $langRepository)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getData($criterionId)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultData()
    {
    }
}
