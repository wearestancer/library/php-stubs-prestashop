<?php

namespace PrestaShop\Module\ProductComment\Form;

class ProductCommentCriterionFormDataHandler implements \PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataHandler\FormDataHandlerInterface
{
    /**
     * @param ProductCommentCriterionRepository $pccriterionRepository
     * @param LangRepository $langRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(\PrestaShop\Module\ProductComment\Repository\ProductCommentCriterionRepository $pccriterionRepository, \PrestaShopBundle\Entity\Repository\LangRepository $langRepository, \Doctrine\ORM\EntityManagerInterface $entityManager)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
    }
    /**
     * @param ProductCommentCriterion $pccriterion
     * @param array $pcc_languages
     *
     * @todo migrate this temporary function to above standard function create
     */
    public function createLangs($pccriterion, $pcc_languages) : void
    {
    }
    /**
     * @param ProductCommentCriterion $pccriterion
     * @param array $pcc_languages
     *
     * @todo migrate this temporary function to above standard function update
     */
    public function updateLangs($pccriterion, $pcc_languages) : void
    {
    }
}
