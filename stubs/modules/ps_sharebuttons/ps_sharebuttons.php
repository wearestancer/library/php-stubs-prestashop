<?php

class Ps_Sharebuttons extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'socialsharing';
    protected static $networks = ['Facebook', 'Twitter', 'Pinterest'];
    public function __construct()
    {
    }
    public function install()
    {
    }
    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     */
    public function uninstallPrestaShop16Module()
    {
    }
    public function getConfigFieldsValues()
    {
    }
    public function getContent()
    {
    }
    public function renderWidget($hookName, array $params)
    {
    }
    public function getWidgetVariables($hookName, array $params)
    {
    }
}
