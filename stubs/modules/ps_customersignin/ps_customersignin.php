<?php

class Ps_CustomerSignIn extends \Module implements \PrestaShop\PrestaShop\Core\Module\WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockuserinfo';
    public function __construct()
    {
    }
    public function install()
    {
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
    public function renderWidget($hookName, array $configuration)
    {
    }
}
