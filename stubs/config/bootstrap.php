<?php

\define('_DB_SERVER_', $database_host);
\define('_DB_USER_', $config['parameters']['database_user']);
\define('_DB_PASSWD_', $config['parameters']['database_password']);
\define('_DB_PREFIX_', $config['parameters']['database_prefix']);
\define('_MYSQL_ENGINE_', $config['parameters']['database_engine']);
\define('_PS_CACHING_SYSTEM_', $config['parameters']['ps_caching']);
\define('_PS_CREATION_DATE_', $config['parameters']['ps_creation_date']);
