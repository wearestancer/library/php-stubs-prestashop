<?php

$file = $argv[1];
$content = file_get_contents($file);
$clean = preg_replace("|/\*\*?.+?@copyright.+?\*/|sm", "", $content);

file_put_contents($file, $clean);
