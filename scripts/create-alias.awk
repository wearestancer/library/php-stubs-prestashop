#! /usr/bin/awk -f

BEGIN {
  print "<?php"
  print ""
}

{
  if ($3) {
    print substr($0, 0, length($0) - 4) " extends " $3 " {}"
  } else {
    print substr($0, 0, length($0) - 4) " extends " $2 " {}"
  }
}
