#! /bin/bash

set -eu -o pipefail

work_dir="$PWD"
scripts_dir=$(realpath $(dirname $0))

clean_prestashop () {
  local version=$1

  ps_files="${work_dir}/tmp/${tag}/files"

  show_step "$version" "Cleaning"

  cd "$ps_files"

  rm -rf ./tools/ ./var/

  if [ -d ./admin/ajaxfilemanager/langs ]; then
    find ./admin/ajaxfilemanager/langs -type f -not -name 'en.php' -delete
  fi

  if [ -d ./admin/filemanager/lang ]; then
    find ./admin/filemanager/lang -type f -not -name 'en.php' -delete
  fi

  if [ -d ./mails ]; then
    find ./mails/* -type d -not -name en | xargs rm -rf
  fi

  if [ -d ./vendor/composer ]; then
    find ./vendor/composer -maxdepth 1 -name '*.php' -delete
  fi

  if [ -d ./vendor/phpoffice/phpexcel/Documentation ]; then
    rm -rf ./vendor/phpoffice/phpexcel/Documentation
  fi

  if [ -d ./vendor/phpoffice/phpexcel/Examples ]; then
    rm -rf ./vendor/phpoffice/phpexcel/Examples
  fi

  if [ -d ./vendor/symfony/contracts/Service/Test ]; then
    rm -rf ./vendor/symfony/contracts/Service/Test
  fi

  if [ -d ./vendor/tecnickcom/tcpdf/fonts ]; then
    find ./vendor/tecnickcom/tcpdf/fonts -name '*.php' -delete
  fi

  find . -type f \( -name index.php -or -name '.*.php' -or \( -not -name '*.php' -and -not -name '.git' \) \) -delete
  find . -type d -empty -delete

  cd "$work_dir"
}

prestashop_dl () {
  local version=$1

  mkdir -p "./tmp/${version}"

  if [ ! -f "./tmp/${version}/prestashop.zip" ]; then
    show_step "$tag" "Downloading PrestaShop"
    curl -fsSL https://github.com/PrestaShop/PrestaShop/releases/download/${version}/prestashop_${version}.zip \
      -o "./tmp/${version}/prestashop.zip"
  fi

  if [ ! -d "./tmp/${version}/files" ]; then
    show_step "$tag" "Unpacking"
    unzip -qo "./tmp/${version}/prestashop.zip" -d "./tmp/${version}/zip"

    if [ -f "./tmp/${version}/zip/prestashop.zip" ]; then
      unzip -qo "./tmp/${version}/zip/prestashop.zip" -d "./tmp/${version}/files"
    elif [ -d "./tmp/${version}/zip/prestashop" ]; then
      rm -rf "./tmp/${version}/files"
      mv "./tmp/${version}/zip/prestashop" "./tmp/${version}/files"
    fi

    clean_prestashop "$tag"
  fi
}

process () {
  local version=$1

  ps_dir="${work_dir}/tmp/${tag}"
  ps_files="${ps_dir}/files"
  cache_dir="${work_dir}/tmp/cache"

  show_step "$tag" "Processing"

  rm -rf "${ps_dir}/stubs"

  files=$(find "$ps_files" -name '*.php' | sort)

  for file in $files; do
    stub=$(realpath --relative-to="$ps_files" "$file")
    checksum=$(shasum "$file" | cut -d' ' -f1)
    dest_checksum="${cache_dir}/${checksum:0:2}/${checksum:2:2}/${checksum:4}"
    ignored="${dest_checksum}.ignore"
    dest_stub="${ps_dir}/stubs/${stub}"

    show_step "$tag" "Processing ${stub} (SHA: ${checksum})"

    mkdir -p $(dirname "$dest_checksum")
    mkdir -p $(dirname "$dest_stub")

    if [ -f "${ignored}" ]; then
      continue
    fi

    if [ ! -f "$dest_checksum" ]; then
      generate-stubs "$file" | sed 's/\s*$//g' > "$dest_checksum"

      check=$(cat "$dest_checksum" | tail -n +2)

      if [ -z "$check" ]; then
        rm "$dest_checksum"
        touch "$ignored"
        continue
      fi
    fi

    cp "$dest_checksum" "$dest_stub"
  done
}

show_step () {
  local tag=$1
  local text=$2

  max_width=$(tput cols)

  printf "\r%-${max_width}s" "[${tag}] ${text}"
}

tags=$(curl -fsSL https://api.github.com/repos/PrestaShop/PrestaShop/git/matching-refs/tags \
  | jq -rc '.[] | select(.ref | test("\\d+\\.\\d+\\.\\d+$")) | .ref | match("\\d+\\.\\d+\\.\\d+(\\.\\d+)?") | .string' \
  | sort -V)

for tag in $tags; do
  version=$(echo "$tag" | awk -F. '{ print $1"."$2 }')
  branch="ps-${version}"
  ps_dir="${work_dir}/tmp/${tag}"
  ps_files="${ps_dir}/files"

  if [ "$version" = "1.5" ] || [ $(echo "$tag" | awk -F. '{ print $1"."$2"."$3 }') = "1.6.0" ] \
  || [ "$tag" = "1.6.1.0" ] || [ "$tag" = "1.6.1.1" ] || [ "$tag" = "1.6.1.5" ] || [ "$tag" = "1.7.0.0" ]; then
    echo "[$tag] Skip"

    continue
  fi

  exists=$(git tag -l | grep -wc "v$tag" || true)

  if [ "$exists" -eq 1 ]; then
    echo "[$tag] Done"
    continue
  fi

  show_step "$tag" "Starting"

  git switch $branch 1> /dev/null 2>&1

  prestashop_dl "$tag"
  process "$tag"

  show_step "$tag" "Copying files"

  rm -rf "${work_dir}/stubs"
  cp -r "${ps_dir}/stubs" "${work_dir}/stubs"

  changes=$(git status --porcelain --ignore-submodules)

  if [ -n "$changes" ]; then
    show_step "$tag" "Create aliases"

    # Create aliases
    grep -Erho '(abstract )?class \w+Core\b' "${work_dir}/stubs" | sort | uniq \
      | awk -f "${scripts_dir}/create-alias.awk" > "${work_dir}/stubs/class_index.php"

    show_step "$tag" "Commit"

    git add "${work_dir}/stubs"
    git commit -m "Update to $tag" 1> /dev/null
  fi

  git tag "v${tag}"

  show_step "$tag" "Done"
  echo
done
