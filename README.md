# PrestaShop PHP Stubs

This package provides stub declarations for [PrestaShop](https://www.prestashop-project.org)
functions, classes and interfaces.
These stubs can help plugin and theme developers leverage IDE completion and static analysis tools like
[PHPStan](http://phpstan.org).

The stubs are generated directly from the source using [php-stubs/generator](https://github.com/php-stubs/generator).


## Installation

Require this package as a development dependency with [Composer](https://getcomposer.org).

```bash
composer require --dev stancer/php-stubs-prestashop
```


## Usage in PHPStan

Include stubs in PHPStan configuration file.

```yaml
parameters:
  scanDirectories:
    - vendor/stancer/php-stubs-prestashop/stubs/
```
